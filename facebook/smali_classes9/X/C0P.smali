.class public LX/C0P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0P",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840580
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1840581
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C0P;->b:LX/0Zi;

    .line 1840582
    iput-object p1, p0, LX/C0P;->a:LX/0Ot;

    .line 1840583
    return-void
.end method

.method public static a(LX/0QB;)LX/C0P;
    .locals 4

    .prologue
    .line 1840520
    const-class v1, LX/C0P;

    monitor-enter v1

    .line 1840521
    :try_start_0
    sget-object v0, LX/C0P;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840522
    sput-object v2, LX/C0P;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840523
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840524
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840525
    new-instance v3, LX/C0P;

    const/16 p0, 0x1e68

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0P;-><init>(LX/0Ot;)V

    .line 1840526
    move-object v0, v3

    .line 1840527
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1840549
    check-cast p2, LX/C0O;

    .line 1840550
    iget-object v0, p0, LX/C0P;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;

    iget-object v1, p2, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p2, LX/C0O;->c:I

    iget-boolean v3, p2, LX/C0O;->d:Z

    const/4 p0, 0x6

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 1840551
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b00bf

    invoke-interface {v4, p0, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b00f4

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x0

    .line 1840552
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1840553
    if-eqz v5, :cond_0

    .line 1840554
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1840555
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    move-object v5, v6

    .line 1840556
    :goto_0
    move-object v5, v5

    .line 1840557
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b00d2

    invoke-interface {v5, p0, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v7, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    invoke-static {v0, p1, v1, v3}, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->a(Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x1

    .line 1840558
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    .line 1840559
    iget-object v7, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1840560
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v7}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v7

    move-object v7, v7

    .line 1840561
    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a0162

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, p0, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 1840562
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    .line 1840563
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    .line 1840564
    const v6, 0x3ae5d9b9

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1840565
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1840566
    return-object v0

    .line 1840567
    :cond_1
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1840568
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 1840569
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p2

    if-lt p2, v2, :cond_3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p2

    if-lt p2, v2, :cond_3

    .line 1840570
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    .line 1840571
    :goto_1
    move-object v5, p2

    .line 1840572
    if-nez v5, :cond_2

    move-object v5, v6

    goto/16 :goto_0

    :cond_2
    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->d:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    sget-object p2, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, LX/1nw;->c(F)LX/1nw;

    move-result-object v5

    goto/16 :goto_0

    .line 1840573
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p2

    if-lt p2, v2, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p2

    if-lt p2, v2, :cond_4

    .line 1840574
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    goto :goto_1

    .line 1840575
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p2

    if-lt p2, v2, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p2

    if-lt p2, v2, :cond_5

    .line 1840576
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    goto :goto_1

    .line 1840577
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p2

    if-lt p2, v2, :cond_6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p2

    if-lt p2, v2, :cond_6

    .line 1840578
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    goto/16 :goto_1

    .line 1840579
    :cond_6
    const/4 p2, 0x0

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1840539
    invoke-static {}, LX/1dS;->b()V

    .line 1840540
    iget v0, p1, LX/1dQ;->b:I

    .line 1840541
    packed-switch v0, :pswitch_data_0

    .line 1840542
    :goto_0
    return-object v2

    .line 1840543
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1840544
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1840545
    check-cast v1, LX/C0O;

    .line 1840546
    iget-object v3, p0, LX/C0P;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;

    iget-object p1, v1, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/C0O;->b:LX/1Pq;

    .line 1840547
    iget-object p0, v3, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->c:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1840548
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3ae5d9b9
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C0N;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C0P",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1840531
    new-instance v1, LX/C0O;

    invoke-direct {v1, p0}, LX/C0O;-><init>(LX/C0P;)V

    .line 1840532
    iget-object v2, p0, LX/C0P;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C0N;

    .line 1840533
    if-nez v2, :cond_0

    .line 1840534
    new-instance v2, LX/C0N;

    invoke-direct {v2, p0}, LX/C0N;-><init>(LX/C0P;)V

    .line 1840535
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C0N;->a$redex0(LX/C0N;LX/1De;IILX/C0O;)V

    .line 1840536
    move-object v1, v2

    .line 1840537
    move-object v0, v1

    .line 1840538
    return-object v0
.end method
