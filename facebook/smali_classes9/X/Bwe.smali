.class public final LX/Bwe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/157;


# instance fields
.field public final synthetic a:LX/Bwf;


# direct methods
.method public constructor <init>(LX/Bwf;)V
    .locals 0

    .prologue
    .line 1834131
    iput-object p1, p0, LX/Bwe;->a:LX/Bwf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1834132
    iget-object v0, p0, LX/Bwe;->a:LX/Bwf;

    iget-object v0, v0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1834133
    if-nez v0, :cond_0

    .line 1834134
    :goto_0
    return-void

    .line 1834135
    :cond_0
    iget-object v1, p0, LX/Bwe;->a:LX/Bwf;

    iget-object v1, v1, LX/Bwf;->d:LX/3Hf;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    .line 1834136
    iget-object v0, p0, LX/Bwe;->a:LX/Bwf;

    .line 1834137
    iget-object v1, v0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1834138
    iget-object v1, v0, LX/Bwf;->b:LX/14v;

    iget-object v2, v0, LX/Bwf;->e:LX/157;

    invoke-virtual {v1, p1, v2}, LX/14v;->a(Ljava/lang/String;LX/157;)V

    .line 1834139
    iget-object v1, v0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834140
    :cond_1
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 2
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1834141
    if-nez p3, :cond_0

    .line 1834142
    :goto_0
    return-void

    .line 1834143
    :cond_0
    iget-object v0, p0, LX/Bwe;->a:LX/Bwf;

    iget-object v0, v0, LX/Bwf;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;-><init>(LX/Bwe;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
