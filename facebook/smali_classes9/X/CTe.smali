.class public final LX/CTe;
.super LX/CTJ;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/CTJ;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSr;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1895895
    invoke-direct {p0, p1, p3}, LX/CTJ;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    .line 1895896
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, -0x47e2e9de

    invoke-static {v3, v0, v4}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1895897
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895898
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895899
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895900
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895901
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895902
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CTe;->b:Ljava/lang/String;

    .line 1895903
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->R()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1895904
    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-static {v0}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v0

    invoke-interface {p2, v0, p3}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    iput-object v0, p0, LX/CTe;->a:LX/CTJ;

    .line 1895905
    return-void

    :cond_0
    move v0, v2

    .line 1895906
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1895907
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1895908
    goto :goto_2
.end method
