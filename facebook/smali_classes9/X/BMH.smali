.class public LX/BMH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasComposerSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasSelectedCoverPhotoInfo;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/AzL;

.field private final c:LX/AzQ;

.field private final d:LX/5oY;

.field private e:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/AzL;LX/AzQ;LX/5oY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776982
    iput-object p1, p0, LX/BMH;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1776983
    iput-object p2, p0, LX/BMH;->b:LX/AzL;

    .line 1776984
    iput-object p3, p0, LX/BMH;->c:LX/AzQ;

    .line 1776985
    iput-object p4, p0, LX/BMH;->d:LX/5oY;

    .line 1776986
    return-void
.end method

.method public static a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 2

    .prologue
    .line 1776976
    invoke-static {p0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1776977
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1776978
    check-cast v0, LX/1kW;

    .line 1776979
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1776980
    return-object v0
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 3
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1776987
    iget-object v0, p0, LX/BMH;->c:LX/AzQ;

    iget-object v1, p0, LX/BMH;->d:LX/5oY;

    iget-object v2, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1776988
    iget-object v2, v0, LX/AzQ;->a:LX/0Zb;

    sget-object p0, LX/AzP;->DISMISS:LX/AzP;

    invoke-virtual {p0}, LX/AzP;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776989
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1776941
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Activity;

    move-object v0, p3

    .line 1776942
    check-cast v0, LX/B5p;

    .line 1776943
    iget-object v1, v0, LX/B5p;->h:Landroid/net/Uri;

    move-object v7, v1

    .line 1776944
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    move-object v2, p3

    check-cast v2, LX/B5p;

    .line 1776945
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1776946
    move-object v3, p3

    check-cast v3, LX/B5p;

    .line 1776947
    iget-object v4, v3, LX/B5p;->j:LX/0am;

    move-object v3, v4

    .line 1776948
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 1776949
    if-nez v7, :cond_1

    move-object v0, p3

    .line 1776950
    check-cast v0, LX/B5p;

    .line 1776951
    iget v2, v0, LX/B5p;->i:I

    move v0, v2

    .line 1776952
    const/4 v2, -0x2

    if-ne v0, v2, :cond_0

    .line 1776953
    iget-object v2, p0, LX/BMH;->c:LX/AzQ;

    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1776954
    iget-object v3, v0, LX/B5p;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1776955
    iget-object v3, v2, LX/AzQ;->a:LX/0Zb;

    sget-object v4, LX/AzP;->CLICK_MORE:LX/AzP;

    invoke-virtual {v4}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776956
    :goto_0
    iget-object v0, p0, LX/BMH;->a:Lcom/facebook/content/SecureContextHelper;

    check-cast p3, LX/B5p;

    .line 1776957
    iget-object v2, p3, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1776958
    invoke-static {p2}, LX/BMH;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v3

    .line 1776959
    invoke-virtual {v3}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v3

    invoke-static {v2, v6, v3, v1}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->a(Ljava/lang/String;Landroid/content/Context;LX/0Px;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Landroid/content/Intent;

    move-result-object v3

    move-object v1, v3

    .line 1776960
    const/16 v2, 0x20b5

    invoke-interface {v0, v1, v2, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1776961
    :goto_1
    return-void

    .line 1776962
    :cond_0
    iget-object v2, p0, LX/BMH;->c:LX/AzQ;

    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1776963
    iget-object v3, v0, LX/B5p;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1776964
    iget-object v3, v2, LX/AzQ;->a:LX/0Zb;

    sget-object v4, LX/AzP;->CLICK_PROMPT_CONTAINER:LX/AzP;

    invoke-virtual {v4}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776965
    goto :goto_0

    .line 1776966
    :cond_1
    iget-object v2, p0, LX/BMH;->c:LX/AzQ;

    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1776967
    iget-object v3, v0, LX/B5p;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1776968
    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1776969
    iget v4, v0, LX/B5p;->i:I

    move v0, v4

    .line 1776970
    iget-object v4, v2, LX/AzQ;->a:LX/0Zb;

    sget-object v5, LX/AzP;->CLICK_PHOTO_PREVIEW:LX/AzP;

    invoke-virtual {v5}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "position"

    invoke-virtual {v5, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776971
    iget-object v0, p0, LX/BMH;->e:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    if-nez v0, :cond_2

    .line 1776972
    iget-object v0, p0, LX/BMH;->b:LX/AzL;

    check-cast p3, LX/B5p;

    .line 1776973
    iget-object v2, p3, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1776974
    invoke-virtual {v0, v6, v2, v1}, LX/AzL;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    move-result-object v0

    iput-object v0, p0, LX/BMH;->e:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    .line 1776975
    :cond_2
    iget-object v0, p0, LX/BMH;->e:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-virtual {v0, v7}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->a(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final b(LX/1RN;)Z
    .locals 2

    .prologue
    .line 1776939
    invoke-static {p1}, LX/BMH;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    .line 1776940
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
