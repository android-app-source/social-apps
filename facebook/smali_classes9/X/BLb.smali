.class public final LX/BLb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)V
    .locals 0

    .prologue
    .line 1776166
    iput-object p1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x3934d265

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776167
    iget-object v1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->a:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "target_privacy_picker_donebutton"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776168
    iget-object v1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->e:LX/BJV;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->d:LX/BJW;

    if-eqz v1, :cond_0

    .line 1776169
    iget-object v1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b()Z

    .line 1776170
    iget-object v1, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1776171
    if-eqz v1, :cond_0

    .line 1776172
    iget-object v2, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->e:LX/BJV;

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, LX/BJV;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1776173
    iget-object v2, p0, LX/BLb;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->d:LX/BJW;

    invoke-virtual {v2, v1}, LX/BJW;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1776174
    :cond_0
    const v1, 0x1a56034a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
