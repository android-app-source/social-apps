.class public final LX/BM0;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/BM1;


# direct methods
.method public constructor <init>(LX/BM1;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1776691
    iput-object p1, p0, LX/BM0;->d:LX/BM1;

    iput-object p2, p0, LX/BM0;->a:Landroid/app/Activity;

    iput-object p3, p0, LX/BM0;->b:Ljava/lang/String;

    iput-object p4, p0, LX/BM0;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x26

    const v1, -0x713fdfb3

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776692
    iget-object v1, p0, LX/BM0;->d:LX/BM1;

    iget-object v2, p0, LX/BM0;->a:Landroid/app/Activity;

    .line 1776693
    iget-object v3, v1, LX/BM1;->f:Landroid/content/BroadcastReceiver;

    if-nez v3, :cond_1

    .line 1776694
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BM0;->b:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1776695
    iget-object v1, p0, LX/BM0;->d:LX/BM1;

    const-string v2, "install_from_google_play"

    iget-object v3, p0, LX/BM0;->c:Ljava/lang/String;

    iget-object v4, p0, LX/BM0;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/BM1;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1776696
    iget-object v1, p0, LX/BM0;->d:LX/BM1;

    iget-object v2, p0, LX/BM0;->b:Ljava/lang/String;

    iget-object v3, p0, LX/BM0;->a:Landroid/app/Activity;

    invoke-static {v1, v2, v3}, LX/BM1;->a$redex0(LX/BM1;Ljava/lang/String;Landroid/app/Activity;)V

    .line 1776697
    :cond_0
    const/16 v1, 0x27

    const v2, -0x161e5d1e

    invoke-static {p2, v5, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void

    .line 1776698
    :cond_1
    iget-object v3, v1, LX/BM1;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1776699
    const/4 v3, 0x0

    iput-object v3, v1, LX/BM1;->f:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method
