.class public LX/CDF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Sj;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/15X;

.field private final c:LX/0tQ;

.field public final d:LX/5JV;

.field private final e:LX/0kL;


# direct methods
.method public constructor <init>(LX/15X;LX/0tQ;LX/5JV;LX/0kL;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1858888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858889
    iput-object p1, p0, LX/CDF;->b:LX/15X;

    .line 1858890
    iput-object p2, p0, LX/CDF;->c:LX/0tQ;

    .line 1858891
    iput-object p3, p0, LX/CDF;->d:LX/5JV;

    .line 1858892
    iput-object p4, p0, LX/CDF;->e:LX/0kL;

    .line 1858893
    return-void
.end method

.method public static a(LX/0QB;)LX/CDF;
    .locals 7

    .prologue
    .line 1858894
    const-class v1, LX/CDF;

    monitor-enter v1

    .line 1858895
    :try_start_0
    sget-object v0, LX/CDF;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858896
    sput-object v2, LX/CDF;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858899
    new-instance p0, LX/CDF;

    invoke-static {v0}, LX/15X;->a(LX/0QB;)LX/15X;

    move-result-object v3

    check-cast v3, LX/15X;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-static {v0}, LX/5JV;->a(LX/0QB;)LX/5JV;

    move-result-object v5

    check-cast v5, LX/5JV;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-direct {p0, v3, v4, v5, v6}, LX/CDF;-><init>(LX/15X;LX/0tQ;LX/5JV;LX/0kL;)V

    .line 1858900
    const/16 v3, 0x1129

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 1858901
    iput-object v3, p0, LX/CDF;->a:LX/0Or;

    .line 1858902
    move-object v0, p0

    .line 1858903
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858904
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858905
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;LX/1De;ZLcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1De;",
            "Z",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1858907
    iget-object v0, p0, LX/CDF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sj;

    .line 1858908
    new-instance v1, LX/CDE;

    invoke-direct {v1, p0, p2, p3}, LX/CDE;-><init>(LX/CDF;LX/1De;Z)V

    .line 1858909
    if-eqz p3, :cond_1

    .line 1858910
    const-string v2, "overlay_toggle_button"

    const-string v3, "native_story"

    .line 1858911
    const/4 v6, 0x0

    move-object v4, v0

    move-object v5, p4

    move-object v7, v2

    move-object v8, v3

    move-object v9, v1

    invoke-static/range {v4 .. v9}, LX/1Sj;->a(LX/1Sj;Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 1858912
    :cond_0
    :goto_0
    if-nez p3, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-static {p2, v0}, LX/CDD;->a(LX/1De;Z)V

    .line 1858913
    return-void

    .line 1858914
    :cond_1
    const-string v2, "overlay_toggle_button"

    const-string v3, "native_story"

    invoke-virtual {v0, p4, v2, v3, v1}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 1858915
    iget-object v0, p0, LX/CDF;->c:LX/0tQ;

    const/4 v1, 0x0

    .line 1858916
    invoke-virtual {v0}, LX/0tQ;->y()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, LX/0tQ;->w()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, v0, LX/0tQ;->a:LX/0ad;

    sget-short v3, LX/0wh;->n:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    move v0, v1

    .line 1858917
    if-eqz v0, :cond_0

    .line 1858918
    iget-object v0, p0, LX/CDF;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/CDF;->b:LX/15X;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/15X;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 1858919
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
