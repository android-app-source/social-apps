.class public LX/BWk;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private final a:Landroid/widget/FrameLayout;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1792323
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1792324
    const v0, 0x7f0306f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1792325
    const v0, 0x7f0d129e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/BWk;->a:Landroid/widget/FrameLayout;

    .line 1792326
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BWk;->b:Landroid/widget/TextView;

    .line 1792327
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1792328
    iget-object v0, p0, LX/BWk;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792329
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/BWk;->setVisibility(I)V

    .line 1792330
    return-void

    .line 1792331
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 1792332
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setVisibility(I)V

    .line 1792333
    iget-object v0, p0, LX/BWk;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1792334
    return-void
.end method
