.class public final enum LX/C1E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C1E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C1E;

.field public static final enum CHOOSE_MULTIPLE:LX/C1E;

.field public static final enum CHOOSE_ONE:LX/C1E;

.field public static final enum NON_POLL:LX/C1E;

.field public static final enum RANKED:LX/C1E;


# instance fields
.field private final mResponseType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1842118
    new-instance v0, LX/C1E;

    const-string v1, "NON_POLL"

    const-string v2, "NON_POLL"

    invoke-direct {v0, v1, v3, v2}, LX/C1E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/C1E;->NON_POLL:LX/C1E;

    .line 1842119
    new-instance v0, LX/C1E;

    const-string v1, "CHOOSE_MULTIPLE"

    const-string v2, "CHOOSE_MULTIPLE"

    invoke-direct {v0, v1, v4, v2}, LX/C1E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/C1E;->CHOOSE_MULTIPLE:LX/C1E;

    .line 1842120
    new-instance v0, LX/C1E;

    const-string v1, "CHOOSE_ONE"

    const-string v2, "CHOOSE_ONE"

    invoke-direct {v0, v1, v5, v2}, LX/C1E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/C1E;->CHOOSE_ONE:LX/C1E;

    .line 1842121
    new-instance v0, LX/C1E;

    const-string v1, "RANKED"

    const-string v2, "RANKED"

    invoke-direct {v0, v1, v6, v2}, LX/C1E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/C1E;->RANKED:LX/C1E;

    .line 1842122
    const/4 v0, 0x4

    new-array v0, v0, [LX/C1E;

    sget-object v1, LX/C1E;->NON_POLL:LX/C1E;

    aput-object v1, v0, v3

    sget-object v1, LX/C1E;->CHOOSE_MULTIPLE:LX/C1E;

    aput-object v1, v0, v4

    sget-object v1, LX/C1E;->CHOOSE_ONE:LX/C1E;

    aput-object v1, v0, v5

    sget-object v1, LX/C1E;->RANKED:LX/C1E;

    aput-object v1, v0, v6

    sput-object v0, LX/C1E;->$VALUES:[LX/C1E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1842123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1842124
    iput-object p3, p0, LX/C1E;->mResponseType:Ljava/lang/String;

    .line 1842125
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C1E;
    .locals 1

    .prologue
    .line 1842126
    const-class v0, LX/C1E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C1E;

    return-object v0
.end method

.method public static values()[LX/C1E;
    .locals 1

    .prologue
    .line 1842127
    sget-object v0, LX/C1E;->$VALUES:[LX/C1E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C1E;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1842128
    iget-object v0, p0, LX/C1E;->mResponseType:Ljava/lang/String;

    return-object v0
.end method
