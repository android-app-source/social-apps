.class public LX/B6y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Lcom/facebook/widget/FbScrollView;

.field private final d:LX/B6V;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/facebook/widget/FbScrollView;)V
    .locals 1

    .prologue
    .line 1747467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747468
    new-instance v0, LX/B6w;

    invoke-direct {v0, p0}, LX/B6w;-><init>(LX/B6y;)V

    iput-object v0, p0, LX/B6y;->d:LX/B6V;

    .line 1747469
    iput-object p2, p0, LX/B6y;->b:Landroid/view/View;

    .line 1747470
    iput-object p3, p0, LX/B6y;->c:Lcom/facebook/widget/FbScrollView;

    .line 1747471
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B6y;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object v0, p0, LX/B6y;->a:LX/B7W;

    .line 1747472
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1747476
    iget-object v0, p0, LX/B6y;->a:LX/B7W;

    iget-object v1, p0, LX/B6y;->d:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1747477
    iget-object v0, p0, LX/B6y;->b:Landroid/view/View;

    new-instance v1, LX/B6x;

    invoke-direct {v1, p0}, LX/B6x;-><init>(LX/B6y;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1747478
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1747473
    iget-object v0, p0, LX/B6y;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1747474
    iget-object v0, p0, LX/B6y;->a:LX/B7W;

    iget-object v1, p0, LX/B6y;->d:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1747475
    return-void
.end method
