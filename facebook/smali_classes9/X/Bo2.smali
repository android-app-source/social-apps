.class public final LX/Bo2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/47G;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/47G;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1821501
    iput-object p1, p0, LX/Bo2;->e:LX/1dt;

    iput-object p2, p0, LX/Bo2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Bo2;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Bo2;->c:LX/47G;

    iput-object p5, p0, LX/Bo2;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1821502
    iget-object v0, p0, LX/Bo2;->e:LX/1dt;

    iget-object v1, p0, LX/Bo2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Bo2;->b:Ljava/lang/String;

    .line 1821503
    invoke-virtual {v0, v1, v2, v3, v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1821504
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v0

    iget-object v1, p0, LX/Bo2;->c:LX/47G;

    iget-object v1, v1, LX/47G;->o:Ljava/lang/String;

    .line 1821505
    iput-object v1, v0, LX/47H;->a:Ljava/lang/String;

    .line 1821506
    move-object v0, v0

    .line 1821507
    iget-object v1, p0, LX/Bo2;->c:LX/47G;

    .line 1821508
    iput-object v1, v0, LX/47H;->d:LX/47G;

    .line 1821509
    move-object v0, v0

    .line 1821510
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v4

    .line 1821511
    iget-object v0, p0, LX/Bo2;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1821512
    iget-object v0, p0, LX/Bo2;->e:LX/1dt;

    iget-object v0, v0, LX/1dt;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Q1;

    const-class v1, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    iget-object v3, p0, LX/Bo2;->c:LX/47G;

    .line 1821513
    iget-object p1, v4, LX/47I;->c:LX/0P1;

    move-object v4, p1

    .line 1821514
    invoke-virtual/range {v0 .. v5}, LX/6Q1;->a(Ljava/lang/Class;Landroid/content/Context;LX/47G;LX/0P1;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1821515
    if-eqz v1, :cond_0

    .line 1821516
    iget-object v0, p0, LX/Bo2;->e:LX/1dt;

    iget-object v0, v0, LX/1dt;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1821517
    :cond_0
    return v5
.end method
