.class public final LX/BiK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BiJ;


# instance fields
.field public final synthetic a:LX/BiM;


# direct methods
.method public constructor <init>(LX/BiM;)V
    .locals 0

    .prologue
    .line 1810143
    iput-object p1, p0, LX/BiK;->a:LX/BiM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 1810144
    new-instance v0, LX/0ht;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 1810145
    new-instance v1, LX/D3m;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/D3m;-><init>(Landroid/content/Context;)V

    .line 1810146
    new-instance v2, LX/BiI;

    invoke-direct {v2, p0, v0}, LX/BiI;-><init>(LX/BiK;LX/0ht;)V

    .line 1810147
    iget-object v3, v1, LX/D3m;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810148
    iget-object v2, p0, LX/BiK;->a:LX/BiM;

    iget-boolean v2, v2, LX/BiM;->i:Z

    .line 1810149
    iput-boolean v2, v1, LX/D3m;->f:Z

    .line 1810150
    iget-object v4, v1, LX/D3m;->c:Lcom/facebook/fig/listitem/FigListItem;

    iget-boolean v3, v1, LX/D3m;->f:Z

    if-eqz v3, :cond_0

    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setVisibility(I)V

    .line 1810151
    iget-boolean v3, v1, LX/D3m;->f:Z

    if-eqz v3, :cond_1

    .line 1810152
    iget-object v3, v1, LX/D3m;->d:Lcom/facebook/fig/listitem/FigListItem;

    const v4, 0x7f02033f

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundResource(I)V

    .line 1810153
    :goto_1
    invoke-virtual {v0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 1810154
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1810155
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ht;->c(Z)V

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 1810156
    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 1810157
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1810158
    return-void

    .line 1810159
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 1810160
    :cond_1
    iget-object v3, v1, LX/D3m;->d:Lcom/facebook/fig/listitem/FigListItem;

    const v4, 0x7f0a0115

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundResource(I)V

    goto :goto_1
.end method
