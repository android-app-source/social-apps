.class public LX/C00;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/1qa;

.field public final c:LX/Ap5;

.field public final d:LX/AnV;

.field public final e:LX/C0D;

.field public final f:LX/1qb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1qa;LX/Ap5;LX/AnV;LX/C0D;LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1839764
    iput-object p1, p0, LX/C00;->a:Landroid/content/res/Resources;

    .line 1839765
    iput-object p2, p0, LX/C00;->b:LX/1qa;

    .line 1839766
    iput-object p3, p0, LX/C00;->c:LX/Ap5;

    .line 1839767
    iput-object p4, p0, LX/C00;->d:LX/AnV;

    .line 1839768
    iput-object p5, p0, LX/C00;->e:LX/C0D;

    .line 1839769
    iput-object p6, p0, LX/C00;->f:LX/1qb;

    .line 1839770
    return-void
.end method

.method public static a(LX/0QB;)LX/C00;
    .locals 10

    .prologue
    .line 1839771
    const-class v1, LX/C00;

    monitor-enter v1

    .line 1839772
    :try_start_0
    sget-object v0, LX/C00;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839773
    sput-object v2, LX/C00;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839774
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839775
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839776
    new-instance v3, LX/C00;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v5

    check-cast v5, LX/1qa;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v6

    check-cast v6, LX/Ap5;

    invoke-static {v0}, LX/AnV;->a(LX/0QB;)LX/AnV;

    move-result-object v7

    check-cast v7, LX/AnV;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v8

    check-cast v8, LX/C0D;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v9

    check-cast v9, LX/1qb;

    invoke-direct/range {v3 .. v9}, LX/C00;-><init>(Landroid/content/res/Resources;LX/1qa;LX/Ap5;LX/AnV;LX/C0D;LX/1qb;)V

    .line 1839777
    move-object v0, v3

    .line 1839778
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839779
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C00;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839780
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
