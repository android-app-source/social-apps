.class public final LX/C94;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1853597
    iput-object p1, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iput-object p2, p0, LX/C94;->a:Ljava/lang/String;

    iput-object p3, p0, LX/C94;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x67e2ff12

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853598
    iget-object v1, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->e:LX/C93;

    iget-object v2, p0, LX/C94;->a:Ljava/lang/String;

    iget-object v3, p0, LX/C94;->b:Ljava/lang/String;

    .line 1853599
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "moments_app_photo_overlay_call_to_action"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1853600
    const-string v5, "event_type"

    const-string p1, "click"

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1853601
    const-string v5, "feed_name"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1853602
    if-eqz v3, :cond_0

    .line 1853603
    const-string v5, "promotion_tag"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1853604
    :cond_0
    iget-object v5, v1, LX/C93;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1853605
    iget-object v1, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->b:LX/1Vr;

    invoke-virtual {v1}, LX/1Vr;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1853606
    iget-object v1, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->b:LX/1Vr;

    const-string v2, "moments://"

    iget-object v3, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, LX/1Vr;->b(Ljava/lang/String;Landroid/content/Context;)V

    .line 1853607
    :goto_0
    const v1, -0x6790e40b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1853608
    :cond_1
    iget-object v1, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->b:LX/1Vr;

    iget-object v2, p0, LX/C94;->c:Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/momentscalltoaction/MomentsCallToActionPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1Vr;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
