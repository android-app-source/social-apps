.class public final LX/CQx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I

.field public e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1889406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v1, 0x0

    .line 1889407
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 1889408
    iget-object v2, p0, LX/CQx;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1889409
    iget-object v2, p0, LX/CQx;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1889410
    iget-object v2, p0, LX/CQx;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1889411
    iget-object v2, p0, LX/CQx;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1889412
    iget-object v2, p0, LX/CQx;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1889413
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1889414
    iget-wide v2, p0, LX/CQx;->a:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1889415
    invoke-virtual {v0, v12, v6}, LX/186;->b(II)V

    .line 1889416
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1889417
    const/4 v2, 0x3

    iget v3, p0, LX/CQx;->d:I

    invoke-virtual {v0, v2, v3, v1}, LX/186;->a(III)V

    .line 1889418
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1889419
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1889420
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1889421
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 1889422
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 1889423
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1889424
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1889425
    new-instance v0, LX/15i;

    move-object v1, v2

    move-object v2, v11

    move-object v3, v11

    move v4, v12

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1889426
    new-instance v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;-><init>(LX/15i;)V

    .line 1889427
    return-object v1
.end method
