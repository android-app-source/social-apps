.class public LX/BXi;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:D

.field private static final b:D

.field private static final c:D

.field private static final d:LX/0wT;


# instance fields
.field public final e:Landroid/widget/ScrollView;

.field private final f:LX/0wd;

.field private final g:LX/0wT;

.field public h:LX/BXg;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    .line 1793659
    invoke-static {v2, v3}, LX/0wV;->b(D)D

    move-result-wide v0

    sput-wide v0, LX/BXi;->a:D

    .line 1793660
    const-wide v0, 0x406b800000000000L    # 220.0

    invoke-static {v0, v1}, LX/0wV;->b(D)D

    move-result-wide v0

    sput-wide v0, LX/BXi;->b:D

    .line 1793661
    invoke-static {v2, v3}, LX/0wV;->b(D)D

    move-result-wide v0

    sput-wide v0, LX/BXi;->c:D

    .line 1793662
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    sget-wide v2, LX/BXi;->b:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/BXi;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/widget/ScrollView;LX/0wW;)V
    .locals 3

    .prologue
    .line 1793649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1793650
    iput-object p1, p0, LX/BXi;->e:Landroid/widget/ScrollView;

    .line 1793651
    invoke-virtual {p2}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/BXi;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1793652
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1793653
    move-object v0, v0

    .line 1793654
    new-instance v1, LX/BXh;

    invoke-direct {v1, p0}, LX/BXh;-><init>(LX/BXi;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/BXi;->f:LX/0wd;

    .line 1793655
    iget-object v0, p0, LX/BXi;->f:LX/0wd;

    .line 1793656
    iget-object v1, v0, LX/0wd;->b:LX/0wT;

    move-object v0, v1

    .line 1793657
    iput-object v0, p0, LX/BXi;->g:LX/0wT;

    .line 1793658
    return-void
.end method

.method private a(F)F
    .locals 12

    .prologue
    .line 1793639
    iget-object v0, p0, LX/BXi;->h:LX/BXg;

    iget v0, v0, LX/BXg;->b:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 1793640
    sub-float v1, v0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v0, v1

    .line 1793641
    iget-object v2, p0, LX/BXi;->h:LX/BXg;

    iget v2, v2, LX/BXg;->b:I

    int-to-float v2, v2

    const v3, 0x3ecccccd    # 0.4f

    mul-float/2addr v2, v3

    .line 1793642
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_1

    .line 1793643
    const/4 v0, 0x0

    .line 1793644
    :cond_0
    :goto_0
    return v0

    .line 1793645
    :cond_1
    iget-object v3, p0, LX/BXi;->h:LX/BXg;

    iget v3, v3, LX/BXg;->b:I

    int-to-float v3, v3

    const v4, 0x3e19999a    # 0.15f

    mul-float/2addr v4, v3

    .line 1793646
    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    .line 1793647
    cmpg-float v3, v1, v4

    if-lez v3, :cond_0

    .line 1793648
    float-to-double v10, v0

    float-to-double v0, v1

    float-to-double v2, v2

    float-to-double v4, v4

    const-wide/16 v6, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    mul-double/2addr v0, v10

    double-to-float v0, v0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1793637
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BXi;->i:Z

    .line 1793638
    return-void
.end method

.method public static d(LX/BXi;)I
    .locals 6

    .prologue
    .line 1793663
    iget-object v0, p0, LX/BXi;->f:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    iget-object v4, p0, LX/BXi;->h:LX/BXg;

    iget v4, v4, LX/BXg;->h:I

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1793628
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BXi;->i:Z

    .line 1793629
    iget-object v0, p0, LX/BXi;->f:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1793630
    return-void
.end method

.method public final a(I)V
    .locals 14

    .prologue
    .line 1793614
    int-to-float v0, p1

    invoke-direct {p0, v0}, LX/BXi;->a(F)F

    move-result v10

    .line 1793615
    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-nez v0, :cond_1

    .line 1793616
    invoke-virtual {p0}, LX/BXi;->a()V

    .line 1793617
    :cond_0
    :goto_0
    return-void

    .line 1793618
    :cond_1
    invoke-static {p0}, LX/BXi;->d(LX/BXi;)I

    move-result v0

    .line 1793619
    iget-object v1, p0, LX/BXi;->h:LX/BXg;

    iget v1, v1, LX/BXg;->h:I

    .line 1793620
    if-nez v0, :cond_2

    const/4 v2, 0x0

    cmpg-float v2, v10, v2

    if-ltz v2, :cond_0

    :cond_2
    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-gtz v0, :cond_0

    .line 1793621
    :cond_3
    iget-object v0, p0, LX/BXi;->h:LX/BXg;

    iget v11, v0, LX/BXg;->b:I

    .line 1793622
    iget-object v0, p0, LX/BXi;->h:LX/BXg;

    iget v12, v0, LX/BXg;->g:I

    .line 1793623
    iget-boolean v0, p0, LX/BXi;->i:Z

    if-nez v0, :cond_4

    .line 1793624
    iget-object v0, p0, LX/BXi;->f:LX/0wd;

    iget-object v1, p0, LX/BXi;->e:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1793625
    invoke-direct {p0}, LX/BXi;->c()V

    .line 1793626
    :cond_4
    iget-object v13, p0, LX/BXi;->g:LX/0wT;

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sget-wide v6, LX/BXi;->b:D

    sget-wide v8, LX/BXi;->a:D

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    iput-wide v0, v13, LX/0wT;->a:D

    .line 1793627
    iget-object v2, p0, LX/BXi;->f:LX/0wd;

    const/4 v0, 0x0

    cmpg-float v0, v10, v0

    if-gez v0, :cond_5

    neg-int v0, v11

    int-to-double v0, v0

    :goto_1
    invoke-virtual {v2, v0, v1}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    :cond_5
    int-to-double v0, v12

    goto :goto_1
.end method

.method public final a(LX/BXg;)V
    .locals 4

    .prologue
    .line 1793632
    invoke-virtual {p0}, LX/BXi;->a()V

    .line 1793633
    iput-object p1, p0, LX/BXi;->h:LX/BXg;

    .line 1793634
    iget-object v0, p0, LX/BXi;->g:LX/0wT;

    sget-wide v2, LX/BXi;->b:D

    iput-wide v2, v0, LX/0wT;->a:D

    .line 1793635
    iget-object v0, p0, LX/BXi;->f:LX/0wd;

    iget-object v1, p0, LX/BXi;->h:LX/BXg;

    iget v1, v1, LX/BXg;->i:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1793636
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 1793631
    iget-boolean v0, p0, LX/BXi;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BXi;->g:LX/0wT;

    iget-wide v0, v0, LX/0wT;->a:D

    sget-wide v2, LX/BXi;->c:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
