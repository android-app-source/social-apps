.class public LX/Arx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1719928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719929
    return-void
.end method

.method public static a(LX/0il;LX/0jK;LX/ArL;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
            "<TMutation;>;Services::",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/0jK;",
            "LX/ArL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719930
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    .line 1719931
    check-cast p0, LX/0im;

    invoke-interface {p0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setIsCameraFrontFacing(Z)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1719932
    sget-object v0, LX/ArH;->CAMERA_FLIP:LX/ArH;

    invoke-static {p2, v0}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p2, v1}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p2, v0}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719933
    return-void

    .line 1719934
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ">(TModelData;TModelData;)Z"
        }
    .end annotation

    .prologue
    .line 1719935
    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v1, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v1, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ">(TModelData;)Z"
        }
    .end annotation

    .prologue
    .line 1719936
    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v1, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ">(TModelData;)Z"
        }
    .end annotation

    .prologue
    .line 1719937
    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    .line 1719938
    sget-object v1, LX/86q;->CAPTURE_PHOTO_REQUESTED:LX/86q;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
