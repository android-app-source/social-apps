.class public LX/BfK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Bez;


# direct methods
.method public constructor <init>(LX/Bez;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805895
    iput-object p1, p0, LX/BfK;->a:LX/Bez;

    .line 1805896
    return-void
.end method

.method public static a(LX/Bex;IJJ)LX/Bex;
    .locals 4

    .prologue
    .line 1805897
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1805898
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1805899
    if-ne v0, p1, :cond_0

    .line 1805900
    new-instance v2, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-direct {v2, p2, p3, p4, p5}, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;-><init>(JJ)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1805901
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1805902
    :cond_0
    iget-object v2, p0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1805903
    :cond_1
    new-instance v0, LX/Bex;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Bex;-><init>(LX/0Px;)V

    return-object v0
.end method

.method public static a(LX/Bex;JJ)LX/Bex;
    .locals 3

    .prologue
    .line 1805904
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1805905
    iget-object v1, p0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1805906
    new-instance v1, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;-><init>(JJ)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1805907
    new-instance v1, LX/Bex;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Bex;-><init>(LX/0Px;)V

    return-object v1
.end method

.method public static c(LX/BfK;Lcom/facebook/crowdsourcing/helper/HoursData;II)J
    .locals 4

    .prologue
    .line 1805908
    invoke-static {p1, p2, p3}, LX/BfK;->e(Lcom/facebook/crowdsourcing/helper/HoursData;II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget-object v0, v0, LX/Bex;->a:LX/0Px;

    add-int/lit8 v1, p3, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v0, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    const-wide/16 v2, 0xe10

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/BfK;->a:LX/Bez;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2}, LX/Bez;->a(III)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/crowdsourcing/helper/HoursData;II)Z
    .locals 2

    .prologue
    .line 1805909
    if-lez p2, :cond_0

    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v1

    iget-object v1, v1, LX/Bex;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/crowdsourcing/helper/HoursData;I)V
    .locals 9

    .prologue
    .line 1805910
    invoke-virtual {p1, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget-object v0, v0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1805911
    invoke-static {p0, p1, p2, v0}, LX/BfK;->c(LX/BfK;Lcom/facebook/crowdsourcing/helper/HoursData;II)J

    move-result-wide v2

    .line 1805912
    invoke-static {p1, p2, v0}, LX/BfK;->e(Lcom/facebook/crowdsourcing/helper/HoursData;II)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0, p1, p2, v0}, LX/BfK;->c(LX/BfK;Lcom/facebook/crowdsourcing/helper/HoursData;II)J

    move-result-wide v5

    const-wide/16 v7, 0xe10

    add-long/2addr v5, v7

    :goto_0
    move-wide v0, v5

    .line 1805913
    invoke-virtual {p1, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    invoke-static {v4, v2, v3, v0, v1}, LX/BfK;->a(LX/Bex;JJ)LX/Bex;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(ILX/Bex;)V

    .line 1805914
    return-void

    :cond_0
    iget-object v5, p0, LX/BfK;->a:LX/Bez;

    const/16 v6, 0x11

    const/4 v7, 0x0

    invoke-virtual {v5, p2, v6, v7}, LX/Bez;->a(III)J

    move-result-wide v5

    goto :goto_0
.end method
