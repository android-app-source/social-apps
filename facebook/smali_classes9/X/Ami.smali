.class public LX/Ami;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ami;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711358
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1711359
    const-string v0, "comment/{%s}?feedback={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "comment_id"

    const-string v2, "feedback_id"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->COMMENT_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1711360
    return-void
.end method

.method public static a(LX/0QB;)LX/Ami;
    .locals 3

    .prologue
    .line 1711361
    sget-object v0, LX/Ami;->a:LX/Ami;

    if-nez v0, :cond_1

    .line 1711362
    const-class v1, LX/Ami;

    monitor-enter v1

    .line 1711363
    :try_start_0
    sget-object v0, LX/Ami;->a:LX/Ami;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1711364
    if-eqz v2, :cond_0

    .line 1711365
    :try_start_1
    new-instance v0, LX/Ami;

    invoke-direct {v0}, LX/Ami;-><init>()V

    .line 1711366
    move-object v0, v0

    .line 1711367
    sput-object v0, LX/Ami;->a:LX/Ami;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711368
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1711369
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1711370
    :cond_1
    sget-object v0, LX/Ami;->a:LX/Ami;

    return-object v0

    .line 1711371
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1711372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
