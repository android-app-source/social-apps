.class public final LX/AlM;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:LX/AlS;


# direct methods
.method public constructor <init>(LX/AlS;)V
    .locals 0

    .prologue
    .line 1709645
    iput-object p1, p0, LX/AlM;->a:LX/AlS;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1709646
    check-cast p1, LX/1Zj;

    .line 1709647
    iget-object v0, p0, LX/AlM;->a:LX/AlS;

    iget-object v0, v0, LX/AlS;->n:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1709648
    if-eqz v2, :cond_0

    .line 1709649
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1709650
    if-eqz v0, :cond_0

    .line 1709651
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1709652
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1709653
    :cond_0
    :goto_0
    return-void

    .line 1709654
    :cond_1
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1709655
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1709656
    iget-object v1, p0, LX/AlM;->a:LX/AlS;

    iget-object v3, v1, LX/AlS;->c:LX/189;

    iget-object v1, p0, LX/AlM;->a:LX/AlS;

    iget-object v1, v1, LX/AlS;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1709657
    invoke-static {v1}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1709658
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_2

    .line 1709659
    iget-object v2, p0, LX/AlM;->a:LX/AlS;

    iget-object v2, v2, LX/AlS;->f:LX/0QK;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v2, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709660
    :cond_2
    iget-object v1, p0, LX/AlM;->a:LX/AlS;

    iget-object v1, v1, LX/AlS;->b:LX/3iK;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, p0, LX/AlM;->a:LX/AlS;

    iget-object v3, v3, LX/AlS;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v1, v2, v3}, LX/3iK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1709661
    iget-object v1, p0, LX/AlM;->a:LX/AlS;

    iget-object v1, v1, LX/AlS;->a:LX/3iR;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iget-object v2, p0, LX/AlM;->a:LX/AlS;

    iget-object v2, v2, LX/AlS;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1709662
    const-string v3, "story_like"

    invoke-static {v1, v3, v0, v2}, LX/3iR;->a(LX/3iR;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1709663
    goto :goto_0
.end method
