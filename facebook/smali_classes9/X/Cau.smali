.class public final LX/Cau;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Caz;


# direct methods
.method public constructor <init>(LX/Caz;)V
    .locals 0

    .prologue
    .line 1918669
    iput-object p1, p0, LX/Cau;->a:LX/Caz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/CbA;Z)V
    .locals 11

    .prologue
    .line 1918670
    iget-object v0, p0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0}, LX/CbI;->a()V

    .line 1918671
    if-eqz p2, :cond_1

    .line 1918672
    iget-object v0, p1, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {v0}, Lcom/facebook/widget/PhotoButton;->isShown()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1918673
    iget-object v0, p1, LX/CbA;->o:LX/8Hm;

    invoke-virtual {p1, v0}, LX/CbA;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1918674
    :cond_0
    :goto_0
    return-void

    .line 1918675
    :cond_1
    iget-object v0, p0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0, p1}, LX/CbI;->a(LX/CbA;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    move-result-object v9

    .line 1918676
    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918677
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x2e4d6bd6

    if-eq v0, v1, :cond_0

    .line 1918678
    iget-object v0, p0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->g:LX/CbC;

    iget-object v1, p0, LX/Cau;->a:LX/Caz;

    iget-object v1, v1, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1918679
    new-instance v4, LX/CbB;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v2

    check-cast v2, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v4, v1, v2, v3}, LX/CbB;-><init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V

    .line 1918680
    move-object v0, v4

    .line 1918681
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x64687ce

    if-ne v1, v2, :cond_2

    .line 1918682
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1918683
    sget-object v2, LX/0ax;->bw:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/CbB;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918684
    goto :goto_0

    .line 1918685
    :cond_2
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_3

    .line 1918686
    sget-object v1, LX/0ax;->aE:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CbB;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1918687
    :cond_3
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-ne v1, v2, :cond_4

    .line 1918688
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1918689
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/CbB;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918690
    goto/16 :goto_0

    .line 1918691
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0xa7c5482

    if-ne v0, v1, :cond_0

    .line 1918692
    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->b()Ljava/lang/String;

    move-result-object v10

    .line 1918693
    if-nez v10, :cond_5

    sget-object v4, LX/7iM;->PDP_PRODUCT_TAG_CLICK:LX/7iM;

    .line 1918694
    :goto_1
    iget-object v0, p0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->l:LX/7iV;

    iget-object v1, p0, LX/Cau;->a:LX/Caz;

    iget-object v1, v1, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v2

    iget-object v3, p0, LX/Cau;->a:LX/Caz;

    iget-object v3, v3, LX/Caz;->y:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v3

    sget-object v5, LX/7iP;->PRODUCT_TAG:LX/7iP;

    sget-object v6, LX/7iN;->COMMERCE_NEWS_FEED:LX/7iN;

    iget-object v7, p0, LX/Cau;->a:LX/Caz;

    iget-object v7, v7, LX/Caz;->y:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v7

    iget-object v8, p0, LX/Cau;->a:LX/Caz;

    iget-object v8, v8, LX/Caz;->y:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v8

    if-nez v8, :cond_6

    const/4 v8, 0x0

    :goto_2
    invoke-virtual/range {v0 .. v8}, LX/7iV;->a(Landroid/view/View;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;LX/162;LX/7iM;LX/7iP;LX/7iN;ZLjava/lang/String;)V

    .line 1918695
    iget-object v0, p0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->m:LX/7j7;

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    iget-object v2, p0, LX/Cau;->a:LX/Caz;

    iget-object v2, v2, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/7iP;->PRODUCT_TAG:LX/7iP;

    .line 1918696
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->d()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1918697
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->aF_()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v2}, LX/7j7;->a(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V

    .line 1918698
    :goto_3
    goto/16 :goto_0

    .line 1918699
    :cond_5
    sget-object v4, LX/7iM;->NON_PDP_PRODUCT_TAG_CLICK:LX/7iM;

    goto :goto_1

    .line 1918700
    :cond_6
    iget-object v8, p0, LX/Cau;->a:LX/Caz;

    iget-object v8, v8, LX/Caz;->y:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 1918701
    :cond_7
    iget-object v0, p1, LX/CbA;->n:LX/8Hm;

    invoke-virtual {p1, v0}, LX/CbA;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 1918702
    :cond_8
    if-eqz v10, :cond_9

    .line 1918703
    invoke-static {v0, v10, v2}, LX/7j7;->b(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_3

    .line 1918704
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v3}, LX/7j7;->a(LX/7j7;Ljava/lang/String;LX/7iP;)V

    goto :goto_3
.end method
