.class public final enum LX/Bek;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bek;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bek;

.field public static final enum MY_EDITS:LX/Bek;

.field public static final enum SCOREBOARD:LX/Bek;

.field public static final enum VOTE:LX/Bek;


# instance fields
.field private final position:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1805274
    new-instance v0, LX/Bek;

    const-string v1, "VOTE"

    invoke-direct {v0, v1, v2, v2}, LX/Bek;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Bek;->VOTE:LX/Bek;

    new-instance v0, LX/Bek;

    const-string v1, "SCOREBOARD"

    invoke-direct {v0, v1, v3, v3}, LX/Bek;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Bek;->SCOREBOARD:LX/Bek;

    new-instance v0, LX/Bek;

    const-string v1, "MY_EDITS"

    invoke-direct {v0, v1, v4, v4}, LX/Bek;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Bek;->MY_EDITS:LX/Bek;

    .line 1805275
    const/4 v0, 0x3

    new-array v0, v0, [LX/Bek;

    sget-object v1, LX/Bek;->VOTE:LX/Bek;

    aput-object v1, v0, v2

    sget-object v1, LX/Bek;->SCOREBOARD:LX/Bek;

    aput-object v1, v0, v3

    sget-object v1, LX/Bek;->MY_EDITS:LX/Bek;

    aput-object v1, v0, v4

    sput-object v0, LX/Bek;->$VALUES:[LX/Bek;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1805276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1805277
    iput p3, p0, LX/Bek;->position:I

    .line 1805278
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bek;
    .locals 1

    .prologue
    .line 1805279
    const-class v0, LX/Bek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bek;

    return-object v0
.end method

.method public static values()[LX/Bek;
    .locals 1

    .prologue
    .line 1805280
    sget-object v0, LX/Bek;->$VALUES:[LX/Bek;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bek;

    return-object v0
.end method


# virtual methods
.method public final toPosition()I
    .locals 1

    .prologue
    .line 1805281
    iget v0, p0, LX/Bek;->position:I

    return v0
.end method
