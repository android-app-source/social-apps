.class public LX/AVo;
.super LX/AVj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVj",
        "<",
        "Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/AVw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680364
    invoke-direct {p0}, LX/AVj;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1680365
    if-nez p1, :cond_1

    .line 1680366
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, LX/AVo;->c(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0xbb8

    add-long/2addr v2, v4

    iget v1, p0, LX/AVj;->c:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)I
    .locals 2

    .prologue
    .line 1680367
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->a()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->j()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/AW2;)Z
    .locals 1

    .prologue
    .line 1680368
    check-cast p1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-direct {p0, p1}, LX/AVo;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 8

    .prologue
    .line 1680369
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-eqz v0, :cond_0

    iget v1, p0, LX/AVj;->c:I

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-static {v0}, LX/AVo;->c(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)I

    move-result v0

    if-le v1, v0, :cond_0

    .line 1680370
    iget v2, p0, LX/AVj;->c:I

    iget v3, p0, LX/AVo;->f:I

    if-ge v2, v3, :cond_5

    .line 1680371
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-direct {p0, v0}, LX/AVo;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1680372
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVo;->b:LX/AW2;

    .line 1680373
    :cond_1
    :goto_1
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v1, p0, LX/AVj;->c:I

    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->a()I

    move-result v0

    if-le v1, v0, :cond_2

    .line 1680374
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    iput-object v0, p0, LX/AVo;->b:LX/AW2;

    .line 1680375
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-static {v0}, LX/AVo;->c(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)I

    move-result v0

    iput v0, p0, LX/AVo;->f:I

    .line 1680376
    const/4 v0, 0x3

    iput v0, p0, LX/AVo;->e:I

    .line 1680377
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680378
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680379
    iget-object v1, v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->a:Lcom/facebook/widget/text/BetterTextView;

    move-object v1, v1

    .line 1680380
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1680381
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680382
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680383
    iget-object v1, v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->a:Lcom/facebook/widget/text/BetterTextView;

    move-object v0, v1

    .line 1680384
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1680385
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680386
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->setVisibility(I)V

    .line 1680387
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680388
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    iget-object v1, p0, LX/AVo;->d:LX/AVw;

    iget-object v1, v1, LX/AVw;->a:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1680389
    :cond_2
    return-void

    .line 1680390
    :cond_3
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    const/4 v2, 0x1

    .line 1680391
    if-nez v0, :cond_7

    .line 1680392
    :cond_4
    :goto_2
    move v0, v2

    .line 1680393
    if-eqz v0, :cond_1

    .line 1680394
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680395
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    iget-object v1, p0, LX/AVo;->d:LX/AVw;

    iget-object v1, v1, LX/AVw;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 1680396
    :cond_5
    iget v2, p0, LX/AVo;->e:I

    if-gtz v2, :cond_6

    .line 1680397
    iget-object v2, p0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1680398
    check-cast v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680399
    iget-object v3, v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->b:Lcom/facebook/widget/text/BetterTextView;

    move-object v2, v3

    .line 1680400
    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1680401
    const/4 v2, 0x0

    iput-object v2, p0, LX/AVo;->b:LX/AW2;

    .line 1680402
    const v2, 0x7fffffff

    iput v2, p0, LX/AVo;->f:I

    goto/16 :goto_0

    .line 1680403
    :cond_6
    iget-object v2, p0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1680404
    check-cast v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680405
    iget-object v3, v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->b:Lcom/facebook/widget/text/BetterTextView;

    move-object v2, v3

    .line 1680406
    iget v3, p0, LX/AVo;->e:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, LX/AVo;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1680407
    iget-object v2, p0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1680408
    check-cast v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680409
    iget-object v3, v2, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->b:Lcom/facebook/widget/text/BetterTextView;

    move-object v2, v3

    .line 1680410
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1680411
    iget v2, p0, LX/AVo;->f:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, LX/AVo;->f:I

    goto/16 :goto_0

    :cond_7
    invoke-static {v0}, LX/AVo;->c(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;)I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0xbb8

    add-long/2addr v4, v6

    iget v3, p0, LX/AVj;->c:I

    add-int/lit16 v3, v3, 0xc8

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_4

    const/4 v2, 0x0

    goto :goto_2
.end method
