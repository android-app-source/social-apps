.class public LX/Aw6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:Landroid/view/View;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724952
    iput-object p1, p0, LX/Aw6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1724953
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1724950
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 1724949
    iget-object v0, p0, LX/Aw6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AwE;->g:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1724948
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1724939
    iget-object v0, p0, LX/Aw6;->b:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1724940
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1724941
    const v1, 0x7f0827a5

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1724942
    const/4 v1, -0x1

    .line 1724943
    iput v1, v0, LX/0hs;->t:I

    .line 1724944
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1724945
    iget-object v1, p0, LX/Aw6;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1724946
    iget-object v0, p0, LX/Aw6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AwE;->g:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1724947
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1724934
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1724938
    const-string v0, "4493"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1724935
    iget-object v0, p0, LX/Aw6;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 1724936
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA_IN_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Aw6;->c:LX/0Px;

    .line 1724937
    :cond_0
    iget-object v0, p0, LX/Aw6;->c:LX/0Px;

    return-object v0
.end method
