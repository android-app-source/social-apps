.class public final LX/ByC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ByD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1aZ;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:F

.field public d:Ljava/lang/String;

.field public e:LX/1Pt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/ByD;


# direct methods
.method public constructor <init>(LX/ByD;)V
    .locals 1

    .prologue
    .line 1836903
    iput-object p1, p0, LX/ByC;->f:LX/ByD;

    .line 1836904
    move-object v0, p1

    .line 1836905
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1836906
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1836929
    const-string v0, "EventAttachmentPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1836907
    if-ne p0, p1, :cond_1

    .line 1836908
    :cond_0
    :goto_0
    return v0

    .line 1836909
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1836910
    goto :goto_0

    .line 1836911
    :cond_3
    check-cast p1, LX/ByC;

    .line 1836912
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1836913
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1836914
    if-eq v2, v3, :cond_0

    .line 1836915
    iget-object v2, p0, LX/ByC;->a:LX/1aZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ByC;->a:LX/1aZ;

    iget-object v3, p1, LX/ByC;->a:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1836916
    goto :goto_0

    .line 1836917
    :cond_5
    iget-object v2, p1, LX/ByC;->a:LX/1aZ;

    if-nez v2, :cond_4

    .line 1836918
    :cond_6
    iget-object v2, p0, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1836919
    goto :goto_0

    .line 1836920
    :cond_8
    iget-object v2, p1, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1836921
    :cond_9
    iget v2, p0, LX/ByC;->c:F

    iget v3, p1, LX/ByC;->c:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    .line 1836922
    goto :goto_0

    .line 1836923
    :cond_a
    iget-object v2, p0, LX/ByC;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/ByC;->d:Ljava/lang/String;

    iget-object v3, p1, LX/ByC;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1836924
    goto :goto_0

    .line 1836925
    :cond_c
    iget-object v2, p1, LX/ByC;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1836926
    :cond_d
    iget-object v2, p0, LX/ByC;->e:LX/1Pt;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/ByC;->e:LX/1Pt;

    iget-object v3, p1, LX/ByC;->e:LX/1Pt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1836927
    goto :goto_0

    .line 1836928
    :cond_e
    iget-object v2, p1, LX/ByC;->e:LX/1Pt;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
