.class public final LX/Bcq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bcn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bcn",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BcP;

.field public final synthetic b:LX/Bcs;


# direct methods
.method public constructor <init>(LX/Bcs;LX/BcP;)V
    .locals 0

    .prologue
    .line 1802858
    iput-object p1, p0, LX/Bcq;->b:LX/Bcs;

    iput-object p2, p0, LX/Bcq;->a:LX/BcP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1802843
    iget-object v0, p0, LX/Bcq;->a:LX/BcP;

    const/4 v1, 0x0

    sget-object v2, LX/BcL;->INITIAL_LOAD:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1802844
    return-void
.end method

.method public final a(LX/Bcm;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bcm",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1802851
    iget-object v0, p0, LX/Bcq;->a:LX/BcP;

    .line 1802852
    invoke-virtual {v0}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    .line 1802853
    if-nez v1, :cond_0

    .line 1802854
    :goto_0
    return-void

    .line 1802855
    :cond_0
    check-cast v1, LX/Bch;

    .line 1802856
    new-instance v2, LX/Bcj;

    iget-object p0, v1, LX/Bch;->m:LX/Bck;

    invoke-direct {v2, p0, p1}, LX/Bcj;-><init>(LX/Bck;LX/Bcm;)V

    move-object v1, v2

    .line 1802857
    invoke-virtual {v0, v1}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1802849
    iget-object v0, p0, LX/Bcq;->a:LX/BcP;

    sget-object v1, LX/BcL;->SUCCEEDED:LX/BcL;

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1802850
    return-void
.end method

.method public final a(ZLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1802847
    iget-object v0, p0, LX/Bcq;->a:LX/BcP;

    sget-object v1, LX/BcL;->FAILED:LX/BcL;

    invoke-static {v0, p1, v1, p2}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1802848
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1802845
    iget-object v0, p0, LX/Bcq;->a:LX/BcP;

    const/4 v1, 0x0

    sget-object v2, LX/BcL;->LOADING:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1802846
    return-void
.end method
