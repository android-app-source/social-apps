.class public LX/CFd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lB;

.field private final b:LX/7mC;


# direct methods
.method public constructor <init>(LX/7mC;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1863770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863771
    iput-object p2, p0, LX/CFd;->a:LX/0lB;

    .line 1863772
    iput-object p1, p0, LX/CFd;->b:LX/7mC;

    .line 1863773
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1863774
    check-cast p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;

    .line 1863775
    iget-object v0, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1863776
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1863777
    iget-object v0, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863778
    iget-object v3, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1863779
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1863780
    :cond_0
    new-instance v0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;

    iget-object v2, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 1863781
    new-instance v1, LX/5M9;

    iget-object v2, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {v1, v2}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    iget-object v2, p0, LX/CFd;->a:LX/0lB;

    invoke-virtual {v2, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1863782
    iput-object v0, v1, LX/5M9;->a:Ljava/lang/String;

    .line 1863783
    move-object v0, v1

    .line 1863784
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    iput-object v0, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1863785
    :cond_1
    iget-object v0, p0, LX/CFd;->b:LX/7mC;

    iget-object v1, p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {v0, v1}, LX/7mC;->a(Lcom/facebook/composer/publish/common/PublishPostParams;)LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1863786
    check-cast p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;

    .line 1863787
    invoke-static {p2}, LX/7mC;->a(LX/1pN;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
