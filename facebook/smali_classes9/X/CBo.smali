.class public final enum LX/CBo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBo;

.field public static final enum CENTERED:LX/CBo;

.field public static final enum CENTERED_LARGE:LX/CBo;

.field public static final enum UNKNOWN:LX/CBo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856833
    new-instance v0, LX/CBo;

    const-string v1, "CENTERED"

    invoke-direct {v0, v1, v2}, LX/CBo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBo;->CENTERED:LX/CBo;

    .line 1856834
    new-instance v0, LX/CBo;

    const-string v1, "CENTERED_LARGE"

    invoke-direct {v0, v1, v3}, LX/CBo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBo;->CENTERED_LARGE:LX/CBo;

    .line 1856835
    new-instance v0, LX/CBo;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/CBo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBo;->UNKNOWN:LX/CBo;

    .line 1856836
    const/4 v0, 0x3

    new-array v0, v0, [LX/CBo;

    sget-object v1, LX/CBo;->CENTERED:LX/CBo;

    aput-object v1, v0, v2

    sget-object v1, LX/CBo;->CENTERED_LARGE:LX/CBo;

    aput-object v1, v0, v3

    sget-object v1, LX/CBo;->UNKNOWN:LX/CBo;

    aput-object v1, v0, v4

    sput-object v0, LX/CBo;->$VALUES:[LX/CBo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBo;
    .locals 1

    .prologue
    .line 1856837
    if-nez p0, :cond_0

    .line 1856838
    :try_start_0
    sget-object v0, LX/CBo;->UNKNOWN:LX/CBo;

    .line 1856839
    :goto_0
    return-object v0

    .line 1856840
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBo;->valueOf(Ljava/lang/String;)LX/CBo;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856841
    :catch_0
    sget-object v0, LX/CBo;->UNKNOWN:LX/CBo;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBo;
    .locals 1

    .prologue
    .line 1856831
    const-class v0, LX/CBo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBo;

    return-object v0
.end method

.method public static values()[LX/CBo;
    .locals 1

    .prologue
    .line 1856830
    sget-object v0, LX/CBo;->$VALUES:[LX/CBo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBo;

    return-object v0
.end method
