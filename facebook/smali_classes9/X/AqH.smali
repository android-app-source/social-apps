.class public final LX/AqH;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/AqH;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AqF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/AqJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717421
    const/4 v0, 0x0

    sput-object v0, LX/AqH;->a:LX/AqH;

    .line 1717422
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AqH;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1717476
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717477
    new-instance v0, LX/AqJ;

    invoke-direct {v0}, LX/AqJ;-><init>()V

    iput-object v0, p0, LX/AqH;->c:LX/AqJ;

    .line 1717478
    return-void
.end method

.method public static c(LX/1De;)LX/AqF;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1717468
    new-instance v1, LX/AqG;

    invoke-direct {v1}, LX/AqG;-><init>()V

    .line 1717469
    sget-object v2, LX/AqH;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AqF;

    .line 1717470
    if-nez v2, :cond_0

    .line 1717471
    new-instance v2, LX/AqF;

    invoke-direct {v2}, LX/AqF;-><init>()V

    .line 1717472
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/AqF;->a$redex0(LX/AqF;LX/1De;IILX/AqG;)V

    .line 1717473
    move-object v1, v2

    .line 1717474
    move-object v0, v1

    .line 1717475
    return-object v0
.end method

.method public static declared-synchronized q()LX/AqH;
    .locals 2

    .prologue
    .line 1717464
    const-class v1, LX/AqH;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/AqH;->a:LX/AqH;

    if-nez v0, :cond_0

    .line 1717465
    new-instance v0, LX/AqH;

    invoke-direct {v0}, LX/AqH;-><init>()V

    sput-object v0, LX/AqH;->a:LX/AqH;

    .line 1717466
    :cond_0
    sget-object v0, LX/AqH;->a:LX/AqH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1717467
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717462
    invoke-static {}, LX/1dS;->b()V

    .line 1717463
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x81f504c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1717450
    check-cast p6, LX/AqG;

    .line 1717451
    iget-boolean v1, p6, LX/AqG;->a:Z

    .line 1717452
    sget-object v2, LX/AqJ;->a:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_2

    sget-object v2, LX/AqJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/SwitchCompat;

    .line 1717453
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/widget/SwitchCompat;->getContext()Landroid/content/Context;

    move-result-object p0

    if-eq p0, p1, :cond_1

    .line 1717454
    :cond_0
    new-instance v2, Lcom/facebook/widget/SwitchCompat;

    invoke-direct {v2, p1}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 1717455
    new-instance p0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p0, LX/AqJ;->a:Ljava/lang/ref/WeakReference;

    .line 1717456
    :cond_1
    invoke-virtual {v2, v1}, Lcom/facebook/widget/SwitchCompat;->setCheckedNoAnimation(Z)V

    .line 1717457
    invoke-static {p3}, LX/1oC;->a(I)I

    move-result p0

    invoke-static {p4}, LX/1oC;->a(I)I

    move-result p2

    invoke-virtual {v2, p0, p2}, Lcom/facebook/widget/SwitchCompat;->measure(II)V

    .line 1717458
    invoke-virtual {v2}, Lcom/facebook/widget/SwitchCompat;->getMeasuredWidth()I

    move-result p0

    iput p0, p5, LX/1no;->a:I

    .line 1717459
    invoke-virtual {v2}, Lcom/facebook/widget/SwitchCompat;->getMeasuredHeight()I

    move-result v2

    iput v2, p5, LX/1no;->b:I

    .line 1717460
    const/16 v1, 0x1f

    const v2, -0x708ecc83

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1717461
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717448
    new-instance v0, LX/AqI;

    invoke-direct {v0, p1}, LX/AqI;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1717449
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1717447
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1717436
    check-cast p3, LX/AqG;

    .line 1717437
    check-cast p2, LX/AqI;

    iget-boolean v0, p3, LX/AqG;->a:Z

    .line 1717438
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717439
    if-nez p0, :cond_0

    .line 1717440
    const/4 p0, 0x0

    .line 1717441
    :goto_0
    move-object p0, p0

    .line 1717442
    iput-object p0, p2, LX/AqI;->a:LX/1dQ;

    .line 1717443
    invoke-virtual {p2, v0}, Lcom/facebook/widget/SwitchCompat;->setCheckedNoAnimation(Z)V

    .line 1717444
    return-void

    .line 1717445
    :cond_0
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717446
    check-cast p0, LX/AqG;

    iget-object p0, p0, LX/AqG;->b:LX/1dQ;

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1717435
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717431
    check-cast p2, LX/AqI;

    .line 1717432
    const/4 p0, 0x0

    .line 1717433
    iput-object p0, p2, LX/AqI;->a:LX/1dQ;

    .line 1717434
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717428
    check-cast p2, LX/AqI;

    .line 1717429
    invoke-virtual {p2, p2}, LX/AqI;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717430
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717425
    check-cast p2, LX/AqI;

    .line 1717426
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/AqI;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717427
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1717424
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1717423
    const/16 v0, 0xf

    return v0
.end method
