.class public final LX/ApN;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2g9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:I

.field public e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/2g9;


# direct methods
.method public constructor <init>(LX/2g9;)V
    .locals 1

    .prologue
    .line 1715908
    iput-object p1, p0, LX/ApN;->f:LX/2g9;

    .line 1715909
    move-object v0, p1

    .line 1715910
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1715911
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715912
    const-string v0, "FigButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1715913
    if-ne p0, p1, :cond_1

    .line 1715914
    :cond_0
    :goto_0
    return v0

    .line 1715915
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1715916
    goto :goto_0

    .line 1715917
    :cond_3
    check-cast p1, LX/ApN;

    .line 1715918
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1715919
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1715920
    if-eq v2, v3, :cond_0

    .line 1715921
    iget v2, p0, LX/ApN;->a:I

    iget v3, p1, LX/ApN;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1715922
    goto :goto_0

    .line 1715923
    :cond_4
    iget-object v2, p0, LX/ApN;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/ApN;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApN;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1715924
    goto :goto_0

    .line 1715925
    :cond_6
    iget-object v2, p1, LX/ApN;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 1715926
    :cond_7
    iget-object v2, p0, LX/ApN;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/ApN;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApN;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1715927
    goto :goto_0

    .line 1715928
    :cond_9
    iget-object v2, p1, LX/ApN;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_8

    .line 1715929
    :cond_a
    iget v2, p0, LX/ApN;->d:I

    iget v3, p1, LX/ApN;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1715930
    goto :goto_0

    .line 1715931
    :cond_b
    iget-object v2, p0, LX/ApN;->e:Landroid/util/SparseArray;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/ApN;->e:Landroid/util/SparseArray;

    iget-object v3, p1, LX/ApN;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1715932
    goto :goto_0

    .line 1715933
    :cond_c
    iget-object v2, p1, LX/ApN;->e:Landroid/util/SparseArray;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
