.class public LX/Cf6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gh;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/Cf6;


# instance fields
.field public final a:LX/2H7;

.field private final c:LX/00H;

.field public final d:Landroid/content/Context;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/2Gs;

.field private final g:LX/0kb;

.field public final h:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field public final i:LX/2H0;

.field public final j:LX/0SG;

.field public final k:LX/2H4;

.field public final l:LX/2H3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925484
    const-class v0, LX/Cf6;

    sput-object v0, LX/Cf6;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;LX/00H;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925472
    iput-object p1, p0, LX/Cf6;->d:Landroid/content/Context;

    .line 1925473
    iput-object p2, p0, LX/Cf6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1925474
    iput-object p3, p0, LX/Cf6;->f:LX/2Gs;

    .line 1925475
    iput-object p4, p0, LX/Cf6;->g:LX/0kb;

    .line 1925476
    iput-object p5, p0, LX/Cf6;->h:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 1925477
    iput-object p7, p0, LX/Cf6;->j:LX/0SG;

    .line 1925478
    iput-object p10, p0, LX/Cf6;->c:LX/00H;

    .line 1925479
    sget-object v0, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {p6, v0}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    iput-object v0, p0, LX/Cf6;->i:LX/2H0;

    .line 1925480
    sget-object v0, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {p8, v0}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v0

    iput-object v0, p0, LX/Cf6;->l:LX/2H3;

    .line 1925481
    sget-object v0, LX/2Ge;->NNA:LX/2Ge;

    iget-object v1, p0, LX/Cf6;->l:LX/2H3;

    iget-object v2, p0, LX/Cf6;->i:LX/2H0;

    invoke-virtual {p9, v0, v1, v2}, LX/2Gp;->a(LX/2Ge;LX/2H3;LX/2H0;)LX/2H4;

    move-result-object v0

    iput-object v0, p0, LX/Cf6;->k:LX/2H4;

    .line 1925482
    new-instance v0, LX/Cf2;

    invoke-direct {v0, p0}, LX/Cf2;-><init>(LX/Cf6;)V

    iput-object v0, p0, LX/Cf6;->a:LX/2H7;

    .line 1925483
    return-void
.end method

.method public static a(LX/0QB;)LX/Cf6;
    .locals 14

    .prologue
    .line 1925458
    sget-object v0, LX/Cf6;->m:LX/Cf6;

    if-nez v0, :cond_1

    .line 1925459
    const-class v1, LX/Cf6;

    monitor-enter v1

    .line 1925460
    :try_start_0
    sget-object v0, LX/Cf6;->m:LX/Cf6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925461
    if-eqz v2, :cond_0

    .line 1925462
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1925463
    new-instance v3, LX/Cf6;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v6

    check-cast v6, LX/2Gs;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v8

    check-cast v8, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {v0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v9

    check-cast v9, LX/2Gq;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v11

    check-cast v11, LX/2Gr;

    const-class v12, LX/2Gp;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2Gp;

    const-class v13, LX/00H;

    invoke-interface {v0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/00H;

    invoke-direct/range {v3 .. v13}, LX/Cf6;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/0kb;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/0SG;LX/2Gr;LX/2Gp;LX/00H;)V

    .line 1925464
    move-object v0, v3

    .line 1925465
    sput-object v0, LX/Cf6;->m:LX/Cf6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925468
    :cond_1
    sget-object v0, LX/Cf6;->m:LX/Cf6;

    return-object v0

    .line 1925469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Cf6;LX/Cf5;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1925485
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.nokia.pushnotifications.intent.REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1925486
    sget-object v1, LX/Cf5;->UNREGISTER:LX/Cf5;

    invoke-virtual {v1, p1}, LX/Cf5;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1925487
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.nokia.pushnotifications.intent.UNREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1925488
    :cond_0
    iget-object v1, p0, LX/Cf6;->d:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v1, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1925489
    const-string v2, "app"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1925490
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    iget-object v2, p0, LX/Cf6;->c:LX/00H;

    .line 1925491
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 1925492
    invoke-virtual {v1, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1925493
    const-string v1, "sender"

    const-string v2, "fb-messenger-aol"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1925494
    :cond_1
    :goto_0
    const-string v1, "com.nokia.pushnotifications.service"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1925495
    return-object v0

    .line 1925496
    :cond_2
    sget-object v1, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, LX/Cf6;->c:LX/00H;

    .line 1925497
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 1925498
    invoke-virtual {v1, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1925499
    const-string v1, "sender"

    const-string v2, "fb-app-aol"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 13

    .prologue
    .line 1925437
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1925438
    iget-object v5, p0, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v5}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v5

    .line 1925439
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1925440
    sget-object v5, LX/Cf4;->NONE:LX/Cf4;

    .line 1925441
    :goto_0
    move-object v0, v5

    .line 1925442
    iget-object v1, p0, LX/Cf6;->f:LX/2Gs;

    sget-object v2, LX/3B4;->NNA:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/Cf4;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925443
    sget-object v1, LX/Cf3;->a:[I

    invoke-virtual {v0}, LX/Cf4;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1925444
    :cond_0
    :goto_1
    return-void

    .line 1925445
    :pswitch_0
    if-eqz p1, :cond_1

    .line 1925446
    iget-object v0, p0, LX/Cf6;->h:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    iget-object v2, p0, LX/Cf6;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 1925447
    :cond_1
    iget-object v0, p0, LX/Cf6;->h:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    iget-object v2, p0, LX/Cf6;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;LX/2H7;)V

    goto :goto_1

    .line 1925448
    :pswitch_1
    iget-object v0, p0, LX/Cf6;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1925449
    :pswitch_2
    invoke-virtual {p0}, LX/Cf6;->b()V

    goto :goto_1

    .line 1925450
    :cond_2
    iget-object v5, p0, LX/Cf6;->j:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 1925451
    iget-object v7, p0, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v7}, LX/2H0;->l()J

    move-result-wide v7

    .line 1925452
    iget-object v9, p0, LX/Cf6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v10, p0, LX/Cf6;->l:LX/2H3;

    .line 1925453
    iget-object v11, v10, LX/2H3;->g:LX/0Tn;

    move-object v10, v11

    .line 1925454
    const-wide/16 v11, 0x0

    invoke-interface {v9, v10, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    .line 1925455
    sub-long v7, v5, v7

    const-wide/32 v11, 0x240c8400

    cmp-long v7, v7, v11

    if-lez v7, :cond_3

    sub-long/2addr v5, v9

    const-wide/32 v7, 0xa4cb800

    cmp-long v5, v5, v7

    if-lez v5, :cond_3

    .line 1925456
    sget-object v5, LX/Cf4;->EXPIRED:LX/Cf4;

    goto :goto_0

    .line 1925457
    :cond_3
    sget-object v5, LX/Cf4;->CURRENT:LX/Cf4;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1925428
    iget-object v0, p0, LX/Cf6;->k:LX/2H4;

    sget-object v1, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925429
    iget-object v0, p0, LX/Cf6;->k:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->a()V

    .line 1925430
    sget-object v0, LX/Cf5;->REGISTER:LX/Cf5;

    invoke-static {p0, v0}, LX/Cf6;->a(LX/Cf6;LX/Cf5;)Landroid/content/Intent;

    move-result-object v0

    .line 1925431
    :try_start_0
    iget-object v1, p0, LX/Cf6;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 1925432
    if-nez v0, :cond_0

    .line 1925433
    iget-object v0, p0, LX/Cf6;->k:LX/2H4;

    sget-object v1, LX/2gP;->MISSING_COMPONENT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925434
    :cond_0
    :goto_0
    return-void

    .line 1925435
    :catch_0
    move-exception v0

    .line 1925436
    iget-object v1, p0, LX/Cf6;->k:LX/2H4;

    sget-object v2, LX/2gP;->FAILED:LX/2gP;

    invoke-virtual {v2}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()LX/2H7;
    .locals 1

    .prologue
    .line 1925427
    iget-object v0, p0, LX/Cf6;->a:LX/2H7;

    return-object v0
.end method
