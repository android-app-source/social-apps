.class public LX/Cf1;
.super LX/1qk;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cf1;


# direct methods
.method public constructor <init>(LX/1r1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925394
    const-string v0, "NNA_LIB"

    invoke-direct {p0, p1, v0}, LX/1qk;-><init>(LX/1r1;Ljava/lang/String;)V

    .line 1925395
    return-void
.end method

.method public static a(LX/0QB;)LX/Cf1;
    .locals 4

    .prologue
    .line 1925396
    sget-object v0, LX/Cf1;->b:LX/Cf1;

    if-nez v0, :cond_1

    .line 1925397
    const-class v1, LX/Cf1;

    monitor-enter v1

    .line 1925398
    :try_start_0
    sget-object v0, LX/Cf1;->b:LX/Cf1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925399
    if-eqz v2, :cond_0

    .line 1925400
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1925401
    new-instance p0, LX/Cf1;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v3

    check-cast v3, LX/1r1;

    invoke-direct {p0, v3}, LX/Cf1;-><init>(LX/1r1;)V

    .line 1925402
    move-object v0, p0

    .line 1925403
    sput-object v0, LX/Cf1;->b:LX/Cf1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925404
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925405
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925406
    :cond_1
    sget-object v0, LX/Cf1;->b:LX/Cf1;

    return-object v0

    .line 1925407
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
