.class public final LX/B5x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1746074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746075
    iput-object p1, p0, LX/B5x;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1746076
    iput-object p2, p0, LX/B5x;->b:Ljava/lang/String;

    .line 1746077
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V
    .locals 1

    .prologue
    .line 1746078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746079
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, LX/B5x;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1746080
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    iput-object v0, p0, LX/B5x;->b:Ljava/lang/String;

    .line 1746081
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    iput-object v0, p0, LX/B5x;->c:Ljava/lang/String;

    .line 1746082
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->d:Ljava/lang/String;

    iput-object v0, p0, LX/B5x;->d:Ljava/lang/String;

    .line 1746083
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    iput-object v0, p0, LX/B5x;->e:Ljava/lang/String;

    .line 1746084
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
    .locals 2

    .prologue
    .line 1746085
    new-instance v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;-><init>(LX/B5x;)V

    return-object v0
.end method
