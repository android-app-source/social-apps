.class public final LX/Btx;
.super LX/1NN;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1829648
    iput-object p1, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-direct {p0}, LX/1NN;-><init>()V

    return-void
.end method

.method private a(LX/1Nd;)V
    .locals 4

    .prologue
    .line 1829649
    iget-object v0, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/1Nd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Nd;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_0

    .line 1829650
    iget-object v0, p0, LX/Btx;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 1829651
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829652
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1829647
    check-cast p1, LX/1Nd;

    invoke-direct {p0, p1}, LX/Btx;->a(LX/1Nd;)V

    return-void
.end method
