.class public final LX/CD8;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CD8;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CD6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CD9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1858744
    const/4 v0, 0x0

    sput-object v0, LX/CD8;->a:LX/CD8;

    .line 1858745
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CD8;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1858746
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1858747
    new-instance v0, LX/CD9;

    invoke-direct {v0}, LX/CD9;-><init>()V

    iput-object v0, p0, LX/CD8;->c:LX/CD9;

    .line 1858748
    return-void
.end method

.method public static c(LX/1De;)LX/CD6;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1858749
    new-instance v1, LX/CD7;

    invoke-direct {v1}, LX/CD7;-><init>()V

    .line 1858750
    sget-object v2, LX/CD8;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CD6;

    .line 1858751
    if-nez v2, :cond_0

    .line 1858752
    new-instance v2, LX/CD6;

    invoke-direct {v2}, LX/CD6;-><init>()V

    .line 1858753
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/CD6;->a$redex0(LX/CD6;LX/1De;IILX/CD7;)V

    .line 1858754
    move-object v1, v2

    .line 1858755
    move-object v0, v1

    .line 1858756
    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1858757
    const v0, 0x6137d2cc

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/CD8;
    .locals 2

    .prologue
    .line 1858758
    const-class v1, LX/CD8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CD8;->a:LX/CD8;

    if-nez v0, :cond_0

    .line 1858759
    new-instance v0, LX/CD8;

    invoke-direct {v0}, LX/CD8;-><init>()V

    sput-object v0, LX/CD8;->a:LX/CD8;

    .line 1858760
    :cond_0
    sget-object v0, LX/CD8;->a:LX/CD8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1858761
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1858762
    check-cast p2, LX/CD7;

    .line 1858763
    iget-object v0, p2, LX/CD7;->a:LX/1X1;

    .line 1858764
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object p0

    .line 1858765
    const p2, 0x6137d2cc

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1858766
    invoke-interface {p0, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1858767
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1858768
    invoke-static {}, LX/1dS;->b()V

    .line 1858769
    iget v0, p1, LX/1dQ;->b:I

    .line 1858770
    packed-switch v0, :pswitch_data_0

    .line 1858771
    :goto_0
    return-object v2

    .line 1858772
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1858773
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 1858774
    invoke-static {}, LX/1dS;->b()V

    .line 1858775
    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 1858776
    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1858777
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->callOnClick()Z

    .line 1858778
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6137d2cc
        :pswitch_0
    .end packed-switch
.end method
