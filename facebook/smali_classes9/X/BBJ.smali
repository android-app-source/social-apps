.class public final LX/BBJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1756950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1756951
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1756952
    :goto_0
    return v1

    .line 1756953
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1756954
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1756955
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1756956
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1756957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1756958
    const-string v5, "deltas"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1756959
    invoke-static {p0, p1}, LX/BBb;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1756960
    :cond_2
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1756961
    invoke-static {p0, p1}, LX/BBf;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1756962
    :cond_3
    const-string v5, "page_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1756963
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1756964
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_a

    .line 1756965
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1756966
    :goto_2
    move v0, v4

    .line 1756967
    goto :goto_1

    .line 1756968
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1756969
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1756970
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1756971
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1756972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1756973
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1756974
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1756975
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1756976
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 1756977
    const-string v8, "has_previous_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1756978
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v5

    goto :goto_3

    .line 1756979
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1756980
    :cond_8
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1756981
    if-eqz v0, :cond_9

    .line 1756982
    invoke-virtual {p1, v4, v6}, LX/186;->a(IZ)V

    .line 1756983
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v0, v4

    move v6, v4

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1756984
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1756985
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1756986
    if-eqz v0, :cond_0

    .line 1756987
    const-string v1, "deltas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1756988
    invoke-static {p0, v0, p2, p3}, LX/BBb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1756989
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1756990
    if-eqz v0, :cond_1

    .line 1756991
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1756992
    invoke-static {p0, v0, p2, p3}, LX/BBf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1756993
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1756994
    if-eqz v0, :cond_3

    .line 1756995
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1756996
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1756997
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1756998
    if-eqz v1, :cond_2

    .line 1756999
    const-string p1, "has_previous_page"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757000
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1757001
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757002
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757003
    return-void
.end method
