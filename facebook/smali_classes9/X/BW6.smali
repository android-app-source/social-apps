.class public LX/BW6;
.super Landroid/webkit/WebViewClient;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/BWW;

.field private final d:LX/BWV;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/44G;

.field private final g:LX/03R;

.field private final h:LX/48V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1791729
    const-class v0, LX/BW6;

    sput-object v0, LX/BW6;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V
    .locals 0

    .prologue
    .line 1791730
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 1791731
    iput-object p1, p0, LX/BW6;->b:Landroid/content/Context;

    .line 1791732
    iput-object p2, p0, LX/BW6;->c:LX/BWW;

    .line 1791733
    iput-object p3, p0, LX/BW6;->d:LX/BWV;

    .line 1791734
    iput-object p4, p0, LX/BW6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1791735
    iput-object p5, p0, LX/BW6;->f:LX/44G;

    .line 1791736
    iput-object p6, p0, LX/BW6;->g:LX/03R;

    .line 1791737
    iput-object p7, p0, LX/BW6;->h:LX/48V;

    .line 1791738
    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1791727
    iget-object v0, p0, LX/BW6;->f:LX/44G;

    invoke-virtual {v0, p2}, LX/44G;->a(Ljava/lang/String;)V

    .line 1791728
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1791704
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1791705
    instance-of v0, p1, Lcom/facebook/webview/BasicWebView;

    if-nez v0, :cond_1

    .line 1791706
    :cond_0
    :goto_0
    return-void

    .line 1791707
    :cond_1
    iget-object v0, p0, LX/BW6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791708
    iget-object v0, p0, LX/BW6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->s:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1791709
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1791710
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:(function() { var script = document.createElement(\'script\'); script.setAttribute(\'src\', \'http://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/target/target-script-min.js\'); script.setAttribute(\'type\', \'text/javascript\'); "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "document.body.appendChild(script); })()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1791711
    iget-object v1, p0, LX/BW6;->h:LX/48V;

    invoke-virtual {v1, p1, v0}, LX/48V;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 1791718
    iget-object v0, p0, LX/BW6;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1791719
    iget-object v0, p0, LX/BW6;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1791720
    if-nez v0, :cond_0

    .line 1791721
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 1791722
    :goto_0
    return-void

    .line 1791723
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1791724
    if-nez v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/BW6;->g:LX/03R;

    if-ne v0, v1, :cond_2

    .line 1791725
    :cond_1
    iget-object v0, p0, LX/BW6;->b:Landroid/content/Context;

    const v1, 0x7f082715

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1791726
    :cond_2
    iget-object v0, p0, LX/BW6;->b:Landroid/content/Context;

    const v1, 0x7f082714

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1791712
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1791713
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "/login.php"

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1791714
    if-eqz v0, :cond_0

    .line 1791715
    iget-object v0, p0, LX/BW6;->c:LX/BWW;

    iget-object v1, p0, LX/BW6;->b:Landroid/content/Context;

    iget-object v2, p0, LX/BW6;->d:LX/BWV;

    invoke-interface {v0, v1, v2}, LX/BWW;->a(Landroid/content/Context;LX/BWV;)V

    .line 1791716
    const/4 v0, 0x1

    .line 1791717
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
