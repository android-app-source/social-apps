.class public final LX/BRo;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

.field public final synthetic c:LX/BRr;


# direct methods
.method public constructor <init>(LX/BRr;Landroid/view/View;Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;)V
    .locals 0

    .prologue
    .line 1784484
    iput-object p1, p0, LX/BRo;->c:LX/BRr;

    iput-object p2, p0, LX/BRo;->a:Landroid/view/View;

    iput-object p3, p0, LX/BRo;->b:Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 1784485
    iget-object v0, p0, LX/BRo;->c:LX/BRr;

    iget-boolean v0, v0, LX/BRr;->h:Z

    if-eqz v0, :cond_0

    .line 1784486
    :goto_0
    return-void

    .line 1784487
    :cond_0
    iget-object v0, p0, LX/BRo;->b:Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    iget-object v1, p0, LX/BRo;->c:LX/BRr;

    .line 1784488
    iget-object v2, v1, LX/BRr;->f:Ljava/lang/Runnable;

    if-nez v2, :cond_1

    .line 1784489
    new-instance v2, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;

    invoke-direct {v2, v1}, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;-><init>(LX/BRr;)V

    iput-object v2, v1, LX/BRr;->f:Ljava/lang/Runnable;

    .line 1784490
    :cond_1
    iget-object v2, v1, LX/BRr;->f:Ljava/lang/Runnable;

    move-object v1, v2

    .line 1784491
    const-wide/16 v2, 0x992

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1784492
    iget-object v0, p0, LX/BRo;->c:LX/BRr;

    iget-boolean v0, v0, LX/BRr;->h:Z

    if-eqz v0, :cond_0

    .line 1784493
    :goto_0
    return-void

    .line 1784494
    :cond_0
    iget-object v0, p0, LX/BRo;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1784495
    iget-object v0, p0, LX/BRo;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
