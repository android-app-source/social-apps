.class public LX/BfX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/BfX;


# instance fields
.field public a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

.field public final b:LX/Bez;

.field public final c:LX/BfK;

.field public final d:Landroid/content/res/Resources;

.field public e:Landroid/view/ViewGroup;

.field public f:I

.field public g:Lcom/facebook/crowdsourcing/helper/HoursData;


# direct methods
.method public constructor <init>(LX/Bez;LX/BfK;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1806218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1806219
    iput-object p1, p0, LX/BfX;->b:LX/Bez;

    .line 1806220
    iput-object p2, p0, LX/BfX;->c:LX/BfK;

    .line 1806221
    iput-object p3, p0, LX/BfX;->d:Landroid/content/res/Resources;

    .line 1806222
    return-void
.end method

.method public static a(LX/0QB;)LX/BfX;
    .locals 6

    .prologue
    .line 1806202
    sget-object v0, LX/BfX;->h:LX/BfX;

    if-nez v0, :cond_1

    .line 1806203
    const-class v1, LX/BfX;

    monitor-enter v1

    .line 1806204
    :try_start_0
    sget-object v0, LX/BfX;->h:LX/BfX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1806205
    if-eqz v2, :cond_0

    .line 1806206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1806207
    new-instance p0, LX/BfX;

    invoke-static {v0}, LX/Bez;->b(LX/0QB;)LX/Bez;

    move-result-object v3

    check-cast v3, LX/Bez;

    .line 1806208
    new-instance v5, LX/BfK;

    invoke-static {v0}, LX/Bez;->b(LX/0QB;)LX/Bez;

    move-result-object v4

    check-cast v4, LX/Bez;

    invoke-direct {v5, v4}, LX/BfK;-><init>(LX/Bez;)V

    .line 1806209
    move-object v4, v5

    .line 1806210
    check-cast v4, LX/BfK;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/BfX;-><init>(LX/Bez;LX/BfK;Landroid/content/res/Resources;)V

    .line 1806211
    move-object v0, p0

    .line 1806212
    sput-object v0, LX/BfX;->h:LX/BfX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1806213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1806214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1806215
    :cond_1
    sget-object v0, LX/BfX;->h:LX/BfX;

    return-object v0

    .line 1806216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1806217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;
    .locals 2

    .prologue
    .line 1806201
    iget-object v0, p0, LX/Bex;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    return-object v0
.end method

.method public static a(LX/BfX;I)V
    .locals 2

    .prologue
    .line 1806149
    iget-object v1, p0, LX/BfX;->e:Landroid/view/ViewGroup;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1806150
    return-void

    .line 1806151
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a$redex0(LX/BfX;Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;J)V
    .locals 7

    .prologue
    .line 1806199
    new-instance v0, LX/D9C;

    iget-object v1, p0, LX/BfX;->b:LX/Bez;

    invoke-virtual {v1, p3, p4}, LX/Bez;->b(J)I

    move-result v3

    iget-object v1, p0, LX/BfX;->b:LX/Bez;

    invoke-virtual {v1, p3, p4}, LX/Bez;->c(J)I

    move-result v4

    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/D9C;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    invoke-virtual {v0}, LX/D9C;->show()V

    .line 1806200
    return-void
.end method

.method public static a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V
    .locals 13

    .prologue
    .line 1806167
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1806168
    :goto_0
    return-void

    .line 1806169
    :cond_0
    invoke-static {p0, p2}, LX/BfX;->b(LX/BfX;I)I

    move-result v0

    .line 1806170
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 1806171
    iget-object v0, p0, LX/BfX;->c:LX/BfK;

    iget-object v1, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v0, v1, p2}, LX/BfK;->a(Lcom/facebook/crowdsourcing/helper/HoursData;I)V

    .line 1806172
    iget-object v0, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    invoke-static {v0}, LX/BfX;->a(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806173
    :goto_1
    iget-object v0, p0, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806174
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1806175
    const-string p0, "extra_hours_selected_option"

    iget-object p1, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806176
    iget p2, p1, LX/BfX;->f:I

    move p1, p2

    .line 1806177
    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806178
    iget-object p0, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806179
    iget p1, p0, LX/BfX;->f:I

    move p0, p1

    .line 1806180
    if-nez p0, :cond_1

    .line 1806181
    const-string p0, "extra_hours_data"

    iget-object p1, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806182
    iget-object p2, p1, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object p1, p2

    .line 1806183
    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806184
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p0

    const/4 p1, -0x1

    invoke-virtual {p0, p1, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806185
    goto :goto_0

    .line 1806186
    :cond_2
    iget-object v2, p0, LX/BfX;->c:LX/BfK;

    iget-object v3, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    .line 1806187
    invoke-virtual {v3, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v6

    .line 1806188
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    iget-object v4, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 1806189
    iget-object v4, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v8, v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    .line 1806190
    iget-object v4, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v10, v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    .line 1806191
    iget-object v4, v2, LX/BfK;->a:LX/Bez;

    iget-object v7, v2, LX/BfK;->a:LX/Bez;

    invoke-virtual {v7, v8, v9}, LX/Bez;->b(J)I

    move-result v7

    iget-object v12, v2, LX/BfK;->a:LX/Bez;

    invoke-virtual {v12, v8, v9}, LX/Bez;->c(J)I

    move-result v8

    invoke-virtual {v4, p2, v7, v8}, LX/Bez;->a(III)J

    move-result-wide v8

    .line 1806192
    iget-object v4, v2, LX/BfK;->a:LX/Bez;

    iget-object v7, v2, LX/BfK;->a:LX/Bez;

    invoke-virtual {v7, v10, v11}, LX/Bez;->b(J)I

    move-result v7

    iget-object v12, v2, LX/BfK;->a:LX/Bez;

    invoke-virtual {v12, v10, v11}, LX/Bez;->c(J)I

    move-result v10

    invoke-virtual {v4, p2, v7, v10}, LX/Bez;->a(III)J

    move-result-wide v10

    .line 1806193
    invoke-virtual {v3, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    invoke-static {v4, v8, v9, v10, v11}, LX/BfK;->a(LX/Bex;JJ)LX/Bex;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(ILX/Bex;)V

    .line 1806194
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 1806195
    :cond_3
    iget-object v2, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v2, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/BfX;->a(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    move-result-object v2

    invoke-static {p0, p1, v2}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806196
    iget-object v2, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v2, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v2

    iget-object v2, v2, LX/Bex;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    .line 1806197
    iget-object v2, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v2, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/BfX;->b(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    move-result-object v2

    invoke-static {p0, p1, v2}, LX/BfX;->b(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806198
    :cond_4
    goto/16 :goto_1
.end method

.method public static a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/Bex;I)V
    .locals 1

    .prologue
    .line 1806163
    if-nez p3, :cond_0

    .line 1806164
    iget-object v0, p2, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-static {p0, p1, v0}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806165
    :goto_0
    return-void

    .line 1806166
    :cond_0
    iget-object v0, p2, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-static {p0, p1, v0}, LX/BfX;->b(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V
    .locals 4

    .prologue
    .line 1806223
    iget-object v0, p0, LX/BfX;->b:LX/Bez;

    iget-wide v2, p2, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v0, v2, v3}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/BfX;->b:LX/Bez;

    iget-wide v2, p2, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v1, v2, v3}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806224
    return-void
.end method

.method public static a$redex0(LX/BfX;II)Z
    .locals 1

    .prologue
    .line 1806162
    if-ltz p2, :cond_0

    iget-object v0, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget-object v0, v0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/BfX;I)I
    .locals 2

    .prologue
    .line 1806153
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x7

    .line 1806154
    :goto_0
    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 1806155
    iget-object v1, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v1

    .line 1806156
    iget-boolean p1, v1, LX/Bex;->b:Z

    move v1, p1

    .line 1806157
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-virtual {v1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v1

    iget-object v1, v1, LX/Bex;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1806158
    :goto_1
    return v0

    .line 1806159
    :cond_0
    add-int/lit8 v0, p1, -0x1

    goto :goto_0

    .line 1806160
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1806161
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static b(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;
    .locals 2

    .prologue
    .line 1806152
    iget-object v0, p0, LX/Bex;->a:LX/0Px;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    return-object v0
.end method

.method public static b(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V
    .locals 4

    .prologue
    .line 1806147
    iget-object v0, p0, LX/BfX;->b:LX/Bez;

    iget-wide v2, p2, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v0, v2, v3}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/BfX;->b:LX/Bez;

    iget-wide v2, p2, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v1, v2, v3}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806148
    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1806130
    iget v0, p0, LX/BfX;->f:I

    .line 1806131
    iput p3, p0, LX/BfX;->f:I

    .line 1806132
    invoke-static {p0, p3}, LX/BfX;->a(LX/BfX;I)V

    .line 1806133
    iget v1, p0, LX/BfX;->f:I

    if-eq v1, v0, :cond_1

    .line 1806134
    iget-object v0, p0, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806135
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1806136
    const-string p0, "extra_hours_selected_option"

    iget-object p1, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806137
    iget p2, p1, LX/BfX;->f:I

    move p1, p2

    .line 1806138
    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806139
    iget-object p0, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806140
    iget p1, p0, LX/BfX;->f:I

    move p0, p1

    .line 1806141
    if-nez p0, :cond_0

    .line 1806142
    const-string p0, "extra_hours_data"

    iget-object p1, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806143
    iget-object p2, p1, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object p1, p2

    .line 1806144
    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806145
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p0

    const/4 p1, -0x1

    invoke-virtual {p0, p1, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806146
    :cond_1
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1806129
    return-void
.end method
