.class public final LX/AaH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V
    .locals 0

    .prologue
    .line 1688242
    iput-object p1, p0, LX/AaH;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x68586273

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688243
    iget-object v1, p0, LX/AaH;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    .line 1688244
    iget-object p1, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, p1

    .line 1688245
    if-eqz v1, :cond_0

    .line 1688246
    iget-object v1, p0, LX/AaH;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->s:LX/AaB;

    .line 1688247
    iget-object v3, v1, LX/AaB;->d:LX/0if;

    sget-object v4, LX/0ig;->E:LX/0ih;

    const-string p1, "cancel_selecting_fundraiser"

    invoke-virtual {v3, v4, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1688248
    iget-object v1, p0, LX/AaH;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    .line 1688249
    iget-object v3, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v3

    .line 1688250
    invoke-virtual {v1}, Landroid/app/Dialog;->onBackPressed()V

    .line 1688251
    :cond_0
    const v1, 0x3d98f73c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
