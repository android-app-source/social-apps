.class public final LX/C3K;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C3M;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C3L;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C3M",
            "<TE;>.CondensedStoryInline",
            "LinkshareComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C3M;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C3M;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1845434
    iput-object p1, p0, LX/C3K;->b:LX/C3M;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1845435
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C3K;->c:[Ljava/lang/String;

    .line 1845436
    iput v3, p0, LX/C3K;->d:I

    .line 1845437
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C3K;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C3K;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C3K;LX/1De;IILX/C3L;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C3M",
            "<TE;>.CondensedStoryInline",
            "LinkshareComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1845430
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1845431
    iput-object p4, p0, LX/C3K;->a:LX/C3L;

    .line 1845432
    iget-object v0, p0, LX/C3K;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1845433
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/C3K;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C3M",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845438
    iget-object v0, p0, LX/C3K;->a:LX/C3L;

    iput-object p1, v0, LX/C3L;->b:LX/1Pb;

    .line 1845439
    iget-object v0, p0, LX/C3K;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845440
    return-object p0
.end method

.method public final a(LX/C33;)LX/C3K;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C33;",
            ")",
            "LX/C3M",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845427
    iget-object v0, p0, LX/C3K;->a:LX/C3L;

    iput-object p1, v0, LX/C3L;->a:LX/C33;

    .line 1845428
    iget-object v0, p0, LX/C3K;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845429
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1845423
    invoke-super {p0}, LX/1X5;->a()V

    .line 1845424
    const/4 v0, 0x0

    iput-object v0, p0, LX/C3K;->a:LX/C3L;

    .line 1845425
    iget-object v0, p0, LX/C3K;->b:LX/C3M;

    iget-object v0, v0, LX/C3M;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1845426
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C3M;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845413
    iget-object v1, p0, LX/C3K;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C3K;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C3K;->d:I

    if-ge v1, v2, :cond_2

    .line 1845414
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1845415
    :goto_0
    iget v2, p0, LX/C3K;->d:I

    if-ge v0, v2, :cond_1

    .line 1845416
    iget-object v2, p0, LX/C3K;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1845417
    iget-object v2, p0, LX/C3K;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1845418
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1845419
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1845420
    :cond_2
    iget-object v0, p0, LX/C3K;->a:LX/C3L;

    .line 1845421
    invoke-virtual {p0}, LX/C3K;->a()V

    .line 1845422
    return-object v0
.end method
