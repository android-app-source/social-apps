.class public LX/BTk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1787943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/media/MediaMetadataRetriever;I)I
    .locals 1

    .prologue
    .line 1787960
    invoke-virtual {p0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 1787961
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1787962
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/media/MediaMetadataRetriever;)J
    .locals 2

    .prologue
    .line 1787957
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 1787958
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1787959
    :goto_0
    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/media/MediaMetadataRetriever;)F
    .locals 3

    .prologue
    .line 1787947
    invoke-static {p0}, LX/BTk;->c(Landroid/media/MediaMetadataRetriever;)I

    move-result v0

    .line 1787948
    invoke-static {p0}, LX/BTk;->d(Landroid/media/MediaMetadataRetriever;)I

    move-result v1

    .line 1787949
    invoke-static {p0}, LX/BTk;->e(Landroid/media/MediaMetadataRetriever;)I

    move-result v2

    .line 1787950
    rem-int/lit16 v2, v2, 0xb4

    if-nez v2, :cond_0

    .line 1787951
    if-lez v1, :cond_1

    .line 1787952
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1787953
    :goto_0
    return v0

    .line 1787954
    :cond_0
    if-lez v0, :cond_1

    .line 1787955
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 1787956
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public static c(Landroid/media/MediaMetadataRetriever;)I
    .locals 1

    .prologue
    .line 1787946
    const/16 v0, 0x12

    invoke-static {p0, v0}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;I)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/media/MediaMetadataRetriever;)I
    .locals 1

    .prologue
    .line 1787945
    const/16 v0, 0x13

    invoke-static {p0, v0}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;I)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/media/MediaMetadataRetriever;)I
    .locals 1

    .prologue
    .line 1787944
    const/16 v0, 0x18

    invoke-static {p0, v0}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;I)I

    move-result v0

    return v0
.end method
