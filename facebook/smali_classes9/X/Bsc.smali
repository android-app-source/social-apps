.class public final LX/Bsc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/32J;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/32J;


# direct methods
.method public constructor <init>(LX/32J;)V
    .locals 1

    .prologue
    .line 1827960
    iput-object p1, p0, LX/Bsc;->d:LX/32J;

    .line 1827961
    move-object v0, p1

    .line 1827962
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1827963
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1827964
    const-string v0, "FundraiserUpsellHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1827944
    if-ne p0, p1, :cond_1

    .line 1827945
    :cond_0
    :goto_0
    return v0

    .line 1827946
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1827947
    goto :goto_0

    .line 1827948
    :cond_3
    check-cast p1, LX/Bsc;

    .line 1827949
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1827950
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1827951
    if-eq v2, v3, :cond_0

    .line 1827952
    iget-boolean v2, p0, LX/Bsc;->a:Z

    iget-boolean v3, p1, LX/Bsc;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1827953
    goto :goto_0

    .line 1827954
    :cond_4
    iget-object v2, p0, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1827955
    goto :goto_0

    .line 1827956
    :cond_6
    iget-object v2, p1, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_5

    .line 1827957
    :cond_7
    iget-object v2, p0, LX/Bsc;->c:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bsc;->c:LX/1Pn;

    iget-object v3, p1, LX/Bsc;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1827958
    goto :goto_0

    .line 1827959
    :cond_8
    iget-object v2, p1, LX/Bsc;->c:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1827941
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Bsc;

    .line 1827942
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Bsc;->a:Z

    .line 1827943
    return-object v0
.end method
