.class public final LX/AzF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/AzG;


# direct methods
.method public constructor <init>(LX/AzG;ILandroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1732329
    iput-object p1, p0, LX/AzF;->c:LX/AzG;

    iput p2, p0, LX/AzF;->a:I

    iput-object p3, p0, LX/AzF;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x4fad015b    # 5.8050944E9f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732322
    iget-object v1, p0, LX/AzF;->c:LX/AzG;

    iget-object v1, v1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->j:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    if-nez v1, :cond_0

    .line 1732323
    iget-object v1, p0, LX/AzF;->c:LX/AzG;

    iget-object v1, v1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v2, p0, LX/AzF;->c:LX/AzG;

    iget-object v2, v2, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->f:LX/AzL;

    iget-object v3, p0, LX/AzF;->c:LX/AzG;

    iget-object v3, v3, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v3, v3, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, LX/AzF;->c:LX/AzG;

    iget-object v4, v4, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v4, v4, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->h:Ljava/lang/String;

    iget-object v5, p0, LX/AzF;->c:LX/AzG;

    iget-object v5, v5, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v5, v5, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->i:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v2, v3, v4, v5}, LX/AzL;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    move-result-object v2

    .line 1732324
    iput-object v2, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->j:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    .line 1732325
    :cond_0
    iget-object v1, p0, LX/AzF;->c:LX/AzG;

    iget-object v1, v1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->g:LX/AzQ;

    iget-object v2, p0, LX/AzF;->c:LX/AzG;

    iget-object v2, v2, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->h:Ljava/lang/String;

    iget v3, p0, LX/AzF;->a:I

    .line 1732326
    iget-object v4, v1, LX/AzQ;->a:LX/0Zb;

    sget-object v5, LX/AzP;->CLICK_PICKER_PHOTO:LX/AzP;

    invoke-virtual {v5}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "position"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732327
    iget-object v1, p0, LX/AzF;->c:LX/AzG;

    iget-object v1, v1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->j:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v2, p0, LX/AzF;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->a(Landroid/net/Uri;)V

    .line 1732328
    const v1, -0x5d8f6134

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
