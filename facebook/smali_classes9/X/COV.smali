.class public LX/COV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1883139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;LX/0Px;LX/1De;LX/CNc;)Ljava/lang/CharSequence;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/1De;",
            "LX/CNc;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1883140
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1883141
    :goto_0
    return-object p0

    .line 1883142
    :cond_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1883143
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_9

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1883144
    :try_start_0
    const-string v5, "offset"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "length"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v6

    invoke-static {p0, v5, v6}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v5

    .line 1883145
    iget v6, v5, LX/1yN;->a:I

    move v6, v6

    .line 1883146
    invoke-virtual {v5}, LX/1yN;->c()I
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 1883147
    const-string v7, "background-color"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1883148
    const-string v7, "background-color"

    invoke-virtual {v0, v7, v3}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v7

    .line 1883149
    new-instance v8, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v8, v7}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-interface {v1, v8, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883150
    :cond_1
    const-string v7, "color"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1883151
    const-string v7, "color"

    const/high16 v8, -0x1000000

    invoke-virtual {v0, v7, v8}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v7

    .line 1883152
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1, v8, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883153
    :cond_2
    const-string v7, "font-size"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1883154
    const-string v7, "font-size"

    .line 1883155
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-static {v0, v7, v8}, LX/CNb;->b(LX/CNb;Ljava/lang/String;F)I

    move-result v8

    move v7, v8

    .line 1883156
    new-instance v8, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v8, v7}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-interface {v1, v8, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883157
    :cond_3
    const-string v7, "font-family"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1883158
    const-string v7, "font-weight"

    invoke-static {v0, v7}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v7

    .line 1883159
    const-string v8, "font-family"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1883160
    invoke-static {v8, v7}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    .line 1883161
    new-instance v8, LX/7Gs;

    const-string v9, "roboto"

    invoke-direct {v8, v9, v7}, LX/7Gs;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    invoke-interface {v1, v8, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883162
    :cond_4
    :goto_2
    const-string v7, "strikethrough"

    invoke-virtual {v0, v7, v3}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1883163
    new-instance v7, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v7}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-interface {v1, v7, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883164
    :cond_5
    const-string v7, "underline"

    invoke-virtual {v0, v7, v3}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1883165
    new-instance v7, Landroid/text/style/UnderlineSpan;

    invoke-direct {v7}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-interface {v1, v7, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883166
    :cond_6
    const-string v7, "touch-up-inside-actions"

    invoke-virtual {v0, v7}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v0

    .line 1883167
    if-eqz v0, :cond_7

    .line 1883168
    invoke-static {v0, p3}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    .line 1883169
    new-instance v7, LX/COU;

    invoke-direct {v7, v0}, LX/COU;-><init>(LX/CNe;)V

    .line 1883170
    invoke-interface {v1, v7, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1883171
    :cond_7
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1883172
    :catch_0
    move-exception v0

    .line 1883173
    const-class v5, LX/COV;

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v6

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5, v0, v6, v7}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 1883174
    :cond_8
    const-string v7, "font-weight"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1883175
    const-string v7, "font-weight"

    invoke-static {v0, v7}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v7

    .line 1883176
    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {v1, v8, v6, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    :cond_9
    move-object p0, v1

    .line 1883177
    goto/16 :goto_0
.end method
