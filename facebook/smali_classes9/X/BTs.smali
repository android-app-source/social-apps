.class public final LX/BTs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1788039
    iput-object p1, p0, LX/BTs;->c:LX/BUA;

    iput-object p2, p0, LX/BTs;->a:Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    iput-object p3, p0, LX/BTs;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1788040
    :try_start_0
    iget-object v0, p0, LX/BTs;->a:Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1788041
    const-string v1, "remote-uri"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1788042
    if-eqz v1, :cond_0

    .line 1788043
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1788044
    :cond_0
    iget-object v1, p0, LX/BTs;->c:LX/BUA;

    iget-object v1, v1, LX/BUA;->e:LX/19w;

    iget-object v2, p0, LX/BTs;->b:Ljava/lang/String;

    iget-object v3, p0, LX/BTs;->a:Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    invoke-virtual {v3}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->k()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v2, v0, v4, v5}, LX/19w;->a(Ljava/lang/String;Landroid/net/Uri;J)V

    .line 1788045
    iget-object v0, p0, LX/BTs;->c:LX/BUA;

    iget-object v1, p0, LX/BTs;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BUA;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788046
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1788047
    :catch_0
    move-exception v0

    .line 1788048
    iget-object v1, p0, LX/BTs;->c:LX/BUA;

    iget-object v2, p0, LX/BTs;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
