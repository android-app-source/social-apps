.class public LX/COm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/2mu;

.field public final b:LX/D3w;


# direct methods
.method public constructor <init>(LX/2mu;LX/D3w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1883557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1883558
    iput-object p1, p0, LX/COm;->a:LX/2mu;

    .line 1883559
    iput-object p2, p0, LX/COm;->b:LX/D3w;

    .line 1883560
    return-void
.end method

.method public static a(LX/0QB;)LX/COm;
    .locals 5

    .prologue
    .line 1883561
    const-class v1, LX/COm;

    monitor-enter v1

    .line 1883562
    :try_start_0
    sget-object v0, LX/COm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1883563
    sput-object v2, LX/COm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1883564
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1883565
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1883566
    new-instance p0, LX/COm;

    const-class v3, LX/2mu;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mu;

    invoke-static {v0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v4

    check-cast v4, LX/D3w;

    invoke-direct {p0, v3, v4}, LX/COm;-><init>(LX/2mu;LX/D3w;)V

    .line 1883567
    move-object v0, p0

    .line 1883568
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1883569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/COm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1883570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1883571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(IILX/1no;Lcom/facebook/graphql/model/GraphQLVideo;)V
    .locals 6

    .prologue
    .line 1883572
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v0, v1

    .line 1883573
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v2

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v3

    move v0, p0

    move v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/1oC;->a(IIIIFLX/1no;)V

    .line 1883574
    return-void
.end method
