.class public final LX/Awk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1727139
    iput-object p1, p0, LX/Awk;->a:Landroid/view/View;

    iput-object p2, p0, LX/Awk;->b:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 1727140
    iget-object v0, p0, LX/Awk;->a:Landroid/view/View;

    invoke-static {v0}, LX/Awl;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727141
    iget-object v0, p0, LX/Awk;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1727142
    iget-object v0, p0, LX/Awk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1727143
    :cond_0
    return-void
.end method
