.class public LX/BEv;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

.field public b:LX/BEu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1765144
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1765145
    const v0, 0x7f030c70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1765146
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/BEv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1765147
    invoke-virtual {p0}, LX/BEv;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0571

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1765148
    invoke-virtual {p0, v0, v0, v0, v0}, LX/BEv;->setPadding(IIII)V

    .line 1765149
    invoke-virtual {p0, p0}, LX/BEv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1765150
    const v0, 0x7f0d1e8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iput-object v0, p0, LX/BEv;->a:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    .line 1765151
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x2eaadca6    # 7.7699E-11f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765152
    iget-object v1, p0, LX/BEv;->b:LX/BEu;

    iget-object v1, v1, LX/BEu;->d:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1765153
    iget-object v1, p0, LX/BEv;->b:LX/BEu;

    iget-object v1, v1, LX/BEu;->d:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;)V

    .line 1765154
    :cond_0
    const v1, 0x6b60202

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
