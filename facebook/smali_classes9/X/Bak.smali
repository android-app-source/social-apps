.class public LX/Bak;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public b:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/drawee/drawable/AutoRotateDrawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1af;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/1af;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799493
    iput-object p1, p0, LX/Bak;->a:Landroid/content/res/Resources;

    .line 1799494
    iget-object v0, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0887

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Bak;->f:I

    .line 1799495
    iget-object v0, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0888

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Bak;->g:I

    .line 1799496
    return-void
.end method

.method public static a(LX/0QB;)LX/Bak;
    .locals 4

    .prologue
    .line 1799497
    const-class v1, LX/Bak;

    monitor-enter v1

    .line 1799498
    :try_start_0
    sget-object v0, LX/Bak;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1799499
    sput-object v2, LX/Bak;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1799500
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799501
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1799502
    new-instance p0, LX/Bak;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Bak;-><init>(Landroid/content/res/Resources;)V

    .line 1799503
    move-object v0, p0

    .line 1799504
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1799505
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bak;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1799506
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1799507
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zw;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1799508
    if-nez p2, :cond_0

    .line 1799509
    invoke-virtual {p1}, LX/0zw;->c()V

    .line 1799510
    :goto_0
    return-void

    .line 1799511
    :cond_0
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1799512
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 1799513
    :pswitch_0
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1799514
    iget-object v1, p0, LX/Bak;->d:LX/1af;

    if-nez v1, :cond_2

    .line 1799515
    iget-object v1, p0, LX/Bak;->a:Landroid/content/res/Resources;

    invoke-static {v1}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v1

    .line 1799516
    iget-object v3, p0, LX/Bak;->b:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_1

    .line 1799517
    iget-object v3, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const p2, 0x7f021522

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, LX/Bak;->b:Landroid/graphics/drawable/Drawable;

    .line 1799518
    :cond_1
    iget-object v3, p0, LX/Bak;->b:Landroid/graphics/drawable/Drawable;

    move-object v3, v3

    .line 1799519
    iput-object v3, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1799520
    move-object v1, v1

    .line 1799521
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    iput-object v1, p0, LX/Bak;->d:LX/1af;

    .line 1799522
    :cond_2
    iget-object v1, p0, LX/Bak;->d:LX/1af;

    move-object v1, v1

    .line 1799523
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1799524
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v1, p0, LX/Bak;->g:I

    invoke-virtual {v0, v2, v1, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1799525
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f6b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1799526
    :pswitch_1
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1799527
    iget-object v1, p0, LX/Bak;->e:LX/1af;

    if-nez v1, :cond_4

    .line 1799528
    iget-object v1, p0, LX/Bak;->a:Landroid/content/res/Resources;

    invoke-static {v1}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v1

    .line 1799529
    iget-object v3, p0, LX/Bak;->c:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    if-nez v3, :cond_3

    .line 1799530
    new-instance v3, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object v4, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const p2, 0x7f020a2b

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 p2, 0x3e8

    invoke-direct {v3, v4, p2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    iput-object v3, p0, LX/Bak;->c:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1799531
    :cond_3
    iget-object v3, p0, LX/Bak;->c:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-object v3, v3

    .line 1799532
    sget-object v4, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v3, v4}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    iput-object v1, p0, LX/Bak;->e:LX/1af;

    .line 1799533
    :cond_4
    iget-object v1, p0, LX/Bak;->e:LX/1af;

    move-object v1, v1

    .line 1799534
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1799535
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v1, p0, LX/Bak;->f:I

    invoke-virtual {v0, v2, v1, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1799536
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/Bak;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f6c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
