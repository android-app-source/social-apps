.class public LX/BJ7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BJ7;


# instance fields
.field private final a:LX/0Zb;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1771601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771602
    iput-object p1, p0, LX/BJ7;->a:LX/0Zb;

    .line 1771603
    iput-object p2, p0, LX/BJ7;->b:LX/0ad;

    .line 1771604
    return-void
.end method

.method public static a(LX/0QB;)LX/BJ7;
    .locals 5

    .prologue
    .line 1771605
    sget-object v0, LX/BJ7;->c:LX/BJ7;

    if-nez v0, :cond_1

    .line 1771606
    const-class v1, LX/BJ7;

    monitor-enter v1

    .line 1771607
    :try_start_0
    sget-object v0, LX/BJ7;->c:LX/BJ7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1771608
    if-eqz v2, :cond_0

    .line 1771609
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1771610
    new-instance p0, LX/BJ7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/BJ7;-><init>(LX/0Zb;LX/0ad;)V

    .line 1771611
    move-object v0, p0

    .line 1771612
    sput-object v0, LX/BJ7;->c:LX/BJ7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1771613
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1771614
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1771615
    :cond_1
    sget-object v0, LX/BJ7;->c:LX/BJ7;

    return-object v0

    .line 1771616
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1771617
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1771618
    iget-object v0, p0, LX/BJ7;->b:LX/0ad;

    sget-short v1, LX/1EB;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1771619
    iget-object v1, p0, LX/BJ7;->b:LX/0ad;

    sget-short v2, LX/1EB;->l:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 1771620
    iget-object v2, p0, LX/BJ7;->b:LX/0ad;

    sget-short v3, LX/1EB;->k:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 1771621
    iget-object v3, p0, LX/BJ7;->a:LX/0Zb;

    const-string v4, "is_external_sharing_enabled"

    invoke-virtual {p1, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v4, "is_save_draft_default"

    invoke-virtual {v0, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "has_keep_button"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1771622
    return-void
.end method
