.class public LX/Blu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/events/protocol/CreateEventParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1816734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816735
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1816736
    check-cast p1, Lcom/facebook/events/protocol/CreateEventParams;

    const-wide/16 v8, 0x0

    .line 1816737
    invoke-virtual {p1}, Lcom/facebook/events/protocol/ContextParams;->a()Ljava/util/List;

    move-result-object v4

    .line 1816738
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1816739
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required event name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1816740
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816741
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->e:Ljava/util/Date;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->i:Lcom/facebook/events/model/EventType;

    sget-object v1, Lcom/facebook/events/model/EventType;->QUICK_INVITE:Lcom/facebook/events/model/EventType;

    if-eq v0, v1, :cond_1

    .line 1816742
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required start time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1816743
    :cond_1
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->e:Ljava/util/Date;

    if-eqz v0, :cond_2

    .line 1816744
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->e:Ljava/util/Date;

    iget-object v1, p1, Lcom/facebook/events/protocol/CreateEventParams;->h:Ljava/util/TimeZone;

    iget-boolean v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->f:Z

    invoke-static {v0, v1, v2}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    .line 1816745
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "start_time"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816746
    :cond_2
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1816747
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816748
    :cond_3
    iget-wide v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->d:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_9

    .line 1816749
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "location_id"

    iget-wide v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816750
    :cond_4
    :goto_0
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    if-eqz v0, :cond_5

    .line 1816751
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "end_time"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    iget-object v3, p1, Lcom/facebook/events/protocol/CreateEventParams;->h:Ljava/util/TimeZone;

    invoke-static {v2, v3}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816752
    :cond_5
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    if-eqz v0, :cond_6

    .line 1816753
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy_type"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v2}, LX/Bm4;->a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816754
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "can_guests_invite_friends"

    iget-boolean v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816755
    :cond_6
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->i:Lcom/facebook/events/model/EventType;

    if-eqz v0, :cond_7

    .line 1816756
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event_type"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->i:Lcom/facebook/events/model/EventType;

    invoke-virtual {v2}, Lcom/facebook/events/model/EventType;->getContentValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816757
    :cond_7
    iget-boolean v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->m:Z

    if-eqz v0, :cond_8

    .line 1816758
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "admin_posts_only"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816759
    :cond_8
    new-instance v0, LX/14N;

    const-string v1, "graphEventCreate"

    const-string v2, "POST"

    const-string v5, "%s/events"

    iget-object v3, p1, Lcom/facebook/events/protocol/CreateEventParams;->a:Ljava/lang/Long;

    if-eqz v3, :cond_a

    iget-object v3, p1, Lcom/facebook/events/protocol/CreateEventParams;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v8

    if-lez v3, :cond_a

    iget-object v3, p1, Lcom/facebook/events/protocol/CreateEventParams;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-static {v5, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1816760
    :cond_9
    iget-object v0, p1, Lcom/facebook/events/protocol/CreateEventParams;->l:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1816761
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "location"

    iget-object v2, p1, Lcom/facebook/events/protocol/CreateEventParams;->l:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1816762
    :cond_a
    const-string v3, "me"

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1816763
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1816764
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
