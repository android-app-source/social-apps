.class public LX/BgZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1807711
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/BgZ;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1807712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/97f;ILcom/facebook/crowdsourcing/helper/HoursData;)LX/97f;
    .locals 3
    .param p2    # Lcom/facebook/crowdsourcing/helper/HoursData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1807713
    sget-object v0, LX/BgZ;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1807714
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to set invalid selected option for suggest edits field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1807715
    :cond_0
    if-nez p2, :cond_1

    .line 1807716
    :goto_0
    invoke-interface {p0}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v0

    sget-object v1, LX/BgZ;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1807717
    :goto_1
    return-object p0

    .line 1807718
    :cond_1
    invoke-static {p0, p2}, LX/BgZ;->a(LX/97f;Lcom/facebook/crowdsourcing/helper/HoursData;)LX/97f;

    move-result-object p0

    goto :goto_0

    .line 1807719
    :cond_2
    sget-object v0, LX/BgZ;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-static {p0, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)LX/97f;

    move-result-object p0

    goto :goto_1
.end method

.method private static a(LX/97f;Lcom/facebook/crowdsourcing/helper/HoursData;)LX/97f;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1807720
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1807721
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    .line 1807722
    iget-boolean v0, v4, LX/Bex;->b:Z

    move v0, v0

    .line 1807723
    if-eqz v0, :cond_0

    move v1, v2

    .line 1807724
    :goto_0
    iget-object v0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1807725
    new-instance v5, LX/97q;

    invoke-direct {v5}, LX/97q;-><init>()V

    iget-object v0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v5, v6, v7}, LX/97q;->b(J)LX/97q;

    move-result-object v5

    iget-object v0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v5, v6, v7}, LX/97q;->a(J)LX/97q;

    move-result-object v0

    invoke-virtual {v0}, LX/97q;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807726
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1807727
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1807728
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v5

    .line 1807729
    iget-boolean v0, v5, LX/Bex;->b:Z

    move v0, v0

    .line 1807730
    if-eqz v0, :cond_1

    move v1, v2

    .line 1807731
    :goto_1
    iget-object v0, v5, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1807732
    new-instance v6, LX/97o;

    invoke-direct {v6}, LX/97o;-><init>()V

    iget-object v0, v5, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v6, v8, v9}, LX/97o;->b(J)LX/97o;

    move-result-object v6

    iget-object v0, v5, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v6, v8, v9}, LX/97o;->a(J)LX/97o;

    move-result-object v0

    invoke-virtual {v0}, LX/97o;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$MonModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807733
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1807734
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1807735
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v6

    .line 1807736
    iget-boolean v0, v6, LX/Bex;->b:Z

    move v0, v0

    .line 1807737
    if-eqz v0, :cond_2

    move v1, v2

    .line 1807738
    :goto_2
    iget-object v0, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1807739
    new-instance v7, LX/97s;

    invoke-direct {v7}, LX/97s;-><init>()V

    iget-object v0, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v7, v8, v9}, LX/97s;->b(J)LX/97s;

    move-result-object v7

    iget-object v0, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v7, v8, v9}, LX/97s;->a(J)LX/97s;

    move-result-object v0

    invoke-virtual {v0}, LX/97s;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$TueModel;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807740
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1807741
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1807742
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v7

    .line 1807743
    iget-boolean v0, v7, LX/Bex;->b:Z

    move v0, v0

    .line 1807744
    if-eqz v0, :cond_3

    move v1, v2

    .line 1807745
    :goto_3
    iget-object v0, v7, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1807746
    new-instance v8, LX/97t;

    invoke-direct {v8}, LX/97t;-><init>()V

    iget-object v0, v7, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v8, v10, v11}, LX/97t;->b(J)LX/97t;

    move-result-object v8

    iget-object v0, v7, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v8, v10, v11}, LX/97t;->a(J)LX/97t;

    move-result-object v0

    invoke-virtual {v0}, LX/97t;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$WedModel;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807747
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1807748
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1807749
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v8

    .line 1807750
    iget-boolean v0, v8, LX/Bex;->b:Z

    move v0, v0

    .line 1807751
    if-eqz v0, :cond_4

    move v1, v2

    .line 1807752
    :goto_4
    iget-object v0, v8, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1807753
    new-instance v9, LX/97r;

    invoke-direct {v9}, LX/97r;-><init>()V

    iget-object v0, v8, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v9, v10, v11}, LX/97r;->b(J)LX/97r;

    move-result-object v9

    iget-object v0, v8, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v9, v10, v11}, LX/97r;->a(J)LX/97r;

    move-result-object v0

    invoke-virtual {v0}, LX/97r;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$ThuModel;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807754
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1807755
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1807756
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v9

    .line 1807757
    iget-boolean v0, v9, LX/Bex;->b:Z

    move v0, v0

    .line 1807758
    if-eqz v0, :cond_5

    move v1, v2

    .line 1807759
    :goto_5
    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1807760
    new-instance v10, LX/97n;

    invoke-direct {v10}, LX/97n;-><init>()V

    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v12, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v10, v12, v13}, LX/97n;->b(J)LX/97n;

    move-result-object v10

    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v12, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v10, v12, v13}, LX/97n;->a(J)LX/97n;

    move-result-object v0

    invoke-virtual {v0}, LX/97n;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$FriModel;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807761
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1807762
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1807763
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v9

    .line 1807764
    iget-boolean v0, v9, LX/Bex;->b:Z

    move v0, v0

    .line 1807765
    if-eqz v0, :cond_6

    .line 1807766
    :goto_6
    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1807767
    new-instance v10, LX/97p;

    invoke-direct {v10}, LX/97p;-><init>()V

    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v12, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v10, v12, v13}, LX/97p;->b(J)LX/97p;

    move-result-object v10

    iget-object v0, v9, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v12, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v10, v12, v13}, LX/97p;->a(J)LX/97p;

    move-result-object v0

    invoke-virtual {v0}, LX/97p;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807768
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1807769
    :cond_6
    new-instance v0, LX/97m;

    invoke-direct {v0}, LX/97m;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807770
    iput-object v2, v0, LX/97m;->l:LX/0Px;

    .line 1807771
    move-object v0, v0

    .line 1807772
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807773
    iput-object v2, v0, LX/97m;->f:LX/0Px;

    .line 1807774
    move-object v0, v0

    .line 1807775
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807776
    iput-object v2, v0, LX/97m;->o:LX/0Px;

    .line 1807777
    move-object v0, v0

    .line 1807778
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807779
    iput-object v2, v0, LX/97m;->p:LX/0Px;

    .line 1807780
    move-object v0, v0

    .line 1807781
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807782
    iput-object v2, v0, LX/97m;->n:LX/0Px;

    .line 1807783
    move-object v0, v0

    .line 1807784
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1807785
    iput-object v2, v0, LX/97m;->e:LX/0Px;

    .line 1807786
    move-object v0, v0

    .line 1807787
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1807788
    iput-object v1, v0, LX/97m;->j:LX/0Px;

    .line 1807789
    move-object v0, v0

    .line 1807790
    invoke-virtual {v0}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1807791
    invoke-static {p0, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97f;

    move-result-object v0

    return-object v0
.end method
