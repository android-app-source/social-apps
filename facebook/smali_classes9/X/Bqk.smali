.class public LX/Bqk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bql;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bqk",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bql;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825117
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1825118
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bqk;->b:LX/0Zi;

    .line 1825119
    iput-object p1, p0, LX/Bqk;->a:LX/0Ot;

    .line 1825120
    return-void
.end method

.method public static a(LX/0QB;)LX/Bqk;
    .locals 4

    .prologue
    .line 1825066
    const-class v1, LX/Bqk;

    monitor-enter v1

    .line 1825067
    :try_start_0
    sget-object v0, LX/Bqk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825068
    sput-object v2, LX/Bqk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825069
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825070
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825071
    new-instance v3, LX/Bqk;

    const/16 p0, 0x1d06

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bqk;-><init>(LX/0Ot;)V

    .line 1825072
    move-object v0, v3

    .line 1825073
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825074
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825075
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825076
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1825116
    const v0, -0x3ee2c3de

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1825092
    check-cast p2, LX/Bqj;

    .line 1825093
    iget-object v0, p0, LX/Bqk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bql;

    iget-object v1, p2, LX/Bqj;->a:Landroid/net/Uri;

    iget-object v2, p2, LX/Bqj;->b:Ljava/lang/String;

    iget-object v3, p2, LX/Bqj;->c:Ljava/lang/String;

    .line 1825094
    iget-object v4, v0, LX/Bql;->b:LX/Br8;

    const/4 p0, 0x0

    .line 1825095
    new-instance p2, LX/Br7;

    invoke-direct {p2, v4}, LX/Br7;-><init>(LX/Br8;)V

    .line 1825096
    sget-object v0, LX/Br8;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br6;

    .line 1825097
    if-nez v0, :cond_0

    .line 1825098
    new-instance v0, LX/Br6;

    invoke-direct {v0}, LX/Br6;-><init>()V

    .line 1825099
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/Br6;->a$redex0(LX/Br6;LX/1De;IILX/Br7;)V

    .line 1825100
    move-object p2, v0

    .line 1825101
    move-object p0, p2

    .line 1825102
    move-object v4, p0

    .line 1825103
    iget-object p0, v4, LX/Br6;->a:LX/Br7;

    iput-object v1, p0, LX/Br7;->a:Landroid/net/Uri;

    .line 1825104
    iget-object p0, v4, LX/Br6;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1825105
    move-object v4, v4

    .line 1825106
    iget-object p0, v4, LX/Br6;->a:LX/Br7;

    iput-object v2, p0, LX/Br7;->b:Ljava/lang/String;

    .line 1825107
    iget-object p0, v4, LX/Br6;->d:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1825108
    move-object v4, v4

    .line 1825109
    iget-object p0, v4, LX/Br6;->a:LX/Br7;

    iput-object v3, p0, LX/Br7;->c:Ljava/lang/String;

    .line 1825110
    iget-object p0, v4, LX/Br6;->d:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1825111
    move-object v4, v4

    .line 1825112
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 1825113
    const p0, -0x3ee2c3de

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1825114
    invoke-interface {v4, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1825115
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1825077
    invoke-static {}, LX/1dS;->b()V

    .line 1825078
    iget v0, p1, LX/1dQ;->b:I

    .line 1825079
    packed-switch v0, :pswitch_data_0

    .line 1825080
    :goto_0
    return-object v2

    .line 1825081
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1825082
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1825083
    check-cast v1, LX/Bqj;

    .line 1825084
    iget-object v3, p0, LX/Bqk;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bql;

    iget-object v4, v1, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object p1, v1, LX/Bqj;->e:Ljava/lang/String;

    .line 1825085
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, v3, LX/Bql;->d:LX/0ad;

    sget-short p0, LX/0wi;->m:S

    const/4 v1, 0x0

    invoke-interface {p2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1825086
    iget-object p2, v3, LX/Bql;->c:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p2, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1825087
    iget-object p2, v3, LX/Bql;->a:LX/BqL;

    const-string p0, "post_footer"

    const-string v1, "post_footer"

    .line 1825088
    const-string v3, "CLICK"

    invoke-static {p2, v4, p0, v3, v1}, LX/BqL;->a(LX/BqL;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1825089
    :cond_0
    :goto_1
    goto :goto_0

    .line 1825090
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    const-string p0, "action uri can\'t be handled: "

    invoke-direct {p2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1825091
    iget-object p0, v3, LX/Bql;->a:LX/BqL;

    const-string v1, "post_footer"

    invoke-virtual {p0, v1, p2}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3ee2c3de
        :pswitch_0
    .end packed-switch
.end method
