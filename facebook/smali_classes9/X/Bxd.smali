.class public LX/Bxd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bxg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bxd",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bxg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835814
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1835815
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bxd;->b:LX/0Zi;

    .line 1835816
    iput-object p1, p0, LX/Bxd;->a:LX/0Ot;

    .line 1835817
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxd;
    .locals 4

    .prologue
    .line 1835803
    const-class v1, LX/Bxd;

    monitor-enter v1

    .line 1835804
    :try_start_0
    sget-object v0, LX/Bxd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835805
    sput-object v2, LX/Bxd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835806
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835807
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835808
    new-instance v3, LX/Bxd;

    const/16 p0, 0x1e0d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bxd;-><init>(LX/0Ot;)V

    .line 1835809
    move-object v0, v3

    .line 1835810
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835811
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835812
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1835776
    check-cast p2, LX/Bxb;

    .line 1835777
    iget-object v0, p0, LX/Bxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bxg;

    iget-object v1, p2, LX/Bxb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bxb;->b:LX/1Pp;

    .line 1835778
    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1835779
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v4

    new-instance v5, LX/Bxe;

    invoke-direct {v5, v0, v3}, LX/Bxe;-><init>(LX/Bxg;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1835780
    iput-object v5, v4, LX/3mP;->g:LX/25K;

    .line 1835781
    move-object v4, v4

    .line 1835782
    const/4 v5, 0x0

    .line 1835783
    iput-boolean v5, v4, LX/3mP;->a:Z

    .line 1835784
    move-object v4, v4

    .line 1835785
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v5

    .line 1835786
    iput-object v5, v4, LX/3mP;->d:LX/25L;

    .line 1835787
    move-object v4, v4

    .line 1835788
    iput-object v3, v4, LX/3mP;->e:LX/0jW;

    .line 1835789
    move-object v3, v4

    .line 1835790
    const/16 v4, 0x8

    .line 1835791
    iput v4, v3, LX/3mP;->b:I

    .line 1835792
    move-object v3, v3

    .line 1835793
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 1835794
    iget-object v3, v0, LX/Bxg;->a:LX/3mL;

    invoke-virtual {v3, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v10

    new-instance v3, LX/Bxf;

    iget-object v5, v0, LX/Bxg;->c:Landroid/content/Context;

    .line 1835795
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1835796
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v6

    move-object v4, v0

    move-object v7, v2

    move-object v9, v1

    invoke-direct/range {v3 .. v9}, LX/Bxf;-><init>(LX/Bxg;Landroid/content/Context;LX/0Px;LX/1Pp;LX/25M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v10, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    const/4 v4, 0x1

    .line 1835797
    iget-object v5, v3, LX/3ml;->a:LX/3mk;

    iput-boolean v4, v5, LX/3mk;->b:Z

    .line 1835798
    move-object v3, v3

    .line 1835799
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1835800
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1835801
    invoke-static {}, LX/1dS;->b()V

    .line 1835802
    const/4 v0, 0x0

    return-object v0
.end method
