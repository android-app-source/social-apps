.class public final LX/ATx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:LX/7mj;

.field public final synthetic d:LX/ATy;


# direct methods
.method public constructor <init>(LX/ATy;Ljava/lang/String;ZLX/7mj;)V
    .locals 0

    .prologue
    .line 1677361
    iput-object p1, p0, LX/ATx;->d:LX/ATy;

    iput-object p2, p0, LX/ATx;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/ATx;->b:Z

    iput-object p4, p0, LX/ATx;->c:LX/7mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1677362
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error saving the draft for session <"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/ATx;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1677363
    check-cast p1, Ljava/lang/Boolean;

    const/4 v1, 0x0

    .line 1677364
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error saving the draft for session <"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/ATx;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1677365
    iget-boolean v0, p0, LX/ATx;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ATx;->d:LX/ATy;

    iget-object v0, v0, LX/ATy;->f:LX/0ad;

    sget-short v2, LX/1aO;->w:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677366
    :cond_0
    iget-object v0, p0, LX/ATx;->d:LX/ATy;

    iget-object v0, v0, LX/ATy;->j:LX/8Ln;

    iget-object v2, p0, LX/ATx;->c:LX/7mj;

    const/4 v8, 0x0

    .line 1677367
    invoke-virtual {v2}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v6

    .line 1677368
    invoke-virtual {v2}, LX/7mi;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 1677369
    iget-object v10, v0, LX/8Ln;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v0, LX/8Ln;->a:Landroid/content/Context;

    const-string v5, "push_notification"

    iget-object v9, v0, LX/8Ln;->b:LX/0ad;

    sget-char v11, LX/1aO;->r:C

    invoke-interface {v9, v11, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, v0, LX/8Ln;->a:Landroid/content/Context;

    invoke-interface {v10, v4, v5}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 1677370
    :cond_1
    iget-boolean v0, p0, LX/ATx;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ATx;->d:LX/ATy;

    iget-object v0, v0, LX/ATy;->f:LX/0ad;

    sget-short v2, LX/1aO;->v:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1677371
    iget-object v0, p0, LX/ATx;->d:LX/ATy;

    iget-object v0, v0, LX/ATy;->i:LX/8Ms;

    iget-object v1, p0, LX/ATx;->c:LX/7mj;

    .line 1677372
    new-instance v2, LX/8Mk;

    invoke-direct {v2}, LX/8Mk;-><init>()V

    move-object v2, v2

    .line 1677373
    const-string v3, "input"

    invoke-static {v0, v1}, LX/8Ms;->b(LX/8Ms;LX/7mj;)LX/4ED;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1677374
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1677375
    iget-object v3, v0, LX/8Ms;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1677376
    new-instance v3, LX/8Mr;

    invoke-direct {v3, v0}, LX/8Mr;-><init>(LX/8Ms;)V

    move-object v3, v3

    .line 1677377
    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1677378
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 1677379
    goto/16 :goto_0
.end method
