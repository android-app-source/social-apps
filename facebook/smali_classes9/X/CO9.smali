.class public LX/CO9;
.super LX/CO8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CO8",
        "<",
        "LX/CO7;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/CNb;


# direct methods
.method public constructor <init>(LX/CNb;)V
    .locals 0

    .prologue
    .line 1882642
    invoke-direct {p0}, LX/CO8;-><init>()V

    .line 1882643
    iput-object p1, p0, LX/CO9;->a:LX/CNb;

    .line 1882644
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1882659
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1882645
    check-cast p1, LX/CO7;

    .line 1882646
    iget-object v0, p0, LX/CO9;->a:LX/CNb;

    const/4 p2, 0x0

    .line 1882647
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    if-ne v0, v1, :cond_0

    .line 1882648
    :goto_0
    return-void

    .line 1882649
    :cond_0
    iput-object v0, p1, LX/CO7;->c:LX/CNb;

    .line 1882650
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "inside-color"

    invoke-virtual {v1, v2, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v1

    iput v1, p1, LX/CO7;->d:I

    .line 1882651
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-color"

    invoke-virtual {v1, v2, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v1

    iput v1, p1, LX/CO7;->e:I

    .line 1882652
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-width"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->f:I

    .line 1882653
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "corner-radius"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->g:I

    .line 1882654
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-inset-left"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->h:I

    .line 1882655
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-inset-right"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->i:I

    .line 1882656
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-inset-top"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->j:I

    .line 1882657
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "border-inset-bottom"

    iget-object p0, p1, LX/CO7;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    iput v1, p1, LX/CO7;->k:I

    .line 1882658
    iget-object v1, p1, LX/CO7;->c:LX/CNb;

    const-string v2, "outside-color"

    invoke-virtual {v1, v2, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v1

    iput v1, p1, LX/CO7;->l:I

    goto :goto_0
.end method
