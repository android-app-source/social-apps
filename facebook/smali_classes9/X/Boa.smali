.class public final LX/Boa;
.super LX/BoF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BoF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 1

    .prologue
    .line 1821863
    iput-object p1, p0, LX/Boa;->b:LX/1dt;

    invoke-direct {p0, p1}, LX/BoF;-><init>(LX/1dt;)V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1821862
    iget-object v1, p0, LX/Boa;->b:LX/1dt;

    iget-object v1, v1, LX/1dt;->q:LX/0Uh;

    const/16 v2, 0x5d6

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v1

    invoke-static {v1}, LX/1wH;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821834
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v0

    .line 1821835
    check-cast v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 1821836
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    .line 1821837
    invoke-static {p2}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    .line 1821838
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object p0

    .line 1821839
    if-nez p0, :cond_0

    .line 1821840
    :goto_0
    return-void

    .line 1821841
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavable;->k()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object p1

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq p1, p2, :cond_1

    .line 1821842
    invoke-static {v0, v1, v3}, LX/1wH;->a(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;

    move-result-object p1

    .line 1821843
    new-instance p2, LX/Ako;

    invoke-direct {p2, v0, p0, v5, v4}, LX/Ako;-><init>(LX/1wH;Lcom/facebook/graphql/model/GraphQLSavable;LX/162;Landroid/view/View;)V

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1821844
    iget-object p0, v0, LX/1wH;->a:LX/1SX;

    .line 1821845
    const p2, 0x7f020781

    move p2, p2

    .line 1821846
    invoke-virtual {p0, p1, p2, v2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0

    .line 1821847
    :cond_1
    invoke-static {v0, v1, v3}, LX/1wH;->b(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;

    move-result-object p1

    .line 1821848
    new-instance p2, LX/Akp;

    invoke-direct {p2, v0, p0, v5}, LX/Akp;-><init>(LX/1wH;Lcom/facebook/graphql/model/GraphQLSavable;LX/162;)V

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1821849
    iget-object p0, v0, LX/1wH;->a:LX/1SX;

    .line 1821850
    const p2, 0x7f020781

    move p2, p2

    .line 1821851
    invoke-virtual {p0, p1, p2, v2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 1821861
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821855
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821856
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 1821857
    invoke-direct {p0, v0}, LX/Boa;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821858
    invoke-direct {p0, p1, p2, p3}, LX/Boa;->c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821859
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/BoF;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821860
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1821852
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821853
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    .line 1821854
    invoke-super {p0, p1}, LX/BoF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, LX/Boa;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
