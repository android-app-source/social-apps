.class public final enum LX/AkV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AkV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AkV;

.field public static final enum ATTEMPT:LX/AkV;

.field public static final enum CLICK:LX/AkV;

.field public static final enum EVENT:LX/AkV;

.field public static final enum FAILURE:LX/AkV;

.field public static final enum INVALID:LX/AkV;

.field public static final enum NO_MATCH:LX/AkV;

.field public static final enum SUCCESS:LX/AkV;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1709153
    new-instance v0, LX/AkV;

    const-string v1, "CLICK"

    invoke-direct {v0, v1, v3}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->CLICK:LX/AkV;

    .line 1709154
    new-instance v0, LX/AkV;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v4}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->EVENT:LX/AkV;

    .line 1709155
    new-instance v0, LX/AkV;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v5}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->SUCCESS:LX/AkV;

    .line 1709156
    new-instance v0, LX/AkV;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v6}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->FAILURE:LX/AkV;

    .line 1709157
    new-instance v0, LX/AkV;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v7}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->INVALID:LX/AkV;

    .line 1709158
    new-instance v0, LX/AkV;

    const-string v1, "NO_MATCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->NO_MATCH:LX/AkV;

    .line 1709159
    new-instance v0, LX/AkV;

    const-string v1, "ATTEMPT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/AkV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkV;->ATTEMPT:LX/AkV;

    .line 1709160
    const/4 v0, 0x7

    new-array v0, v0, [LX/AkV;

    sget-object v1, LX/AkV;->CLICK:LX/AkV;

    aput-object v1, v0, v3

    sget-object v1, LX/AkV;->EVENT:LX/AkV;

    aput-object v1, v0, v4

    sget-object v1, LX/AkV;->SUCCESS:LX/AkV;

    aput-object v1, v0, v5

    sget-object v1, LX/AkV;->FAILURE:LX/AkV;

    aput-object v1, v0, v6

    sget-object v1, LX/AkV;->INVALID:LX/AkV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/AkV;->NO_MATCH:LX/AkV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AkV;->ATTEMPT:LX/AkV;

    aput-object v2, v0, v1

    sput-object v0, LX/AkV;->$VALUES:[LX/AkV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1709150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AkV;
    .locals 1

    .prologue
    .line 1709152
    const-class v0, LX/AkV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AkV;

    return-object v0
.end method

.method public static values()[LX/AkV;
    .locals 1

    .prologue
    .line 1709151
    sget-object v0, LX/AkV;->$VALUES:[LX/AkV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AkV;

    return-object v0
.end method
