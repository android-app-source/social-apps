.class public final LX/CEh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;)V
    .locals 0

    .prologue
    .line 1861722
    iput-object p1, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x2

    const v0, -0x341b8df7    # -2.9942802E7f

    invoke-static {v3, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1861723
    iget-object v0, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->e:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1861724
    :cond_0
    const v0, -0x3c43767

    invoke-static {v3, v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1861725
    :goto_0
    return-void

    .line 1861726
    :cond_1
    instance-of v0, p1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1861727
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861728
    iget-object v3, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-object v3, v3

    .line 1861729
    if-eqz v3, :cond_3

    .line 1861730
    iget-object v3, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-object v0, v3

    .line 1861731
    invoke-static {v0}, LX/AzA;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1861732
    if-eqz v0, :cond_3

    .line 1861733
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 1861734
    :goto_1
    new-instance v3, LX/9hF;

    invoke-direct {v3}, LX/9hF;-><init>()V

    iget-object v3, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->e:LX/0Px;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/9hF;->b(LX/0Px;)LX/9hE;

    move-result-object v3

    sget-object v4, LX/74S;->SOUVENIRS:LX/74S;

    invoke-virtual {v3, v4}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v3

    .line 1861735
    iput-boolean v5, v3, LX/9hD;->m:Z

    .line 1861736
    move-object v3, v3

    .line 1861737
    invoke-virtual {v3, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    iget-object v3, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->j:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v3}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v3

    .line 1861738
    iget-object v0, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0, v4, v3, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 1861739
    const v0, -0x6e8a6a7d

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1861740
    check-cast v0, LX/CEc;

    .line 1861741
    iget-object v3, v0, LX/CEc;->c:Landroid/net/Uri;

    move-object v0, v3

    .line 1861742
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1861743
    iget-object v3, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1861744
    iget-object v3, p0, LX/CEh;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
