.class public final enum LX/CG1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CG1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CG1;

.field public static final enum DRAGGING:LX/CG1;

.field public static final enum IDLE:LX/CG1;

.field public static final enum SPRINGING:LX/CG1;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1864780
    new-instance v0, LX/CG1;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/CG1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CG1;->IDLE:LX/CG1;

    .line 1864781
    new-instance v0, LX/CG1;

    const-string v1, "DRAGGING"

    invoke-direct {v0, v1, v3}, LX/CG1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CG1;->DRAGGING:LX/CG1;

    .line 1864782
    new-instance v0, LX/CG1;

    const-string v1, "SPRINGING"

    invoke-direct {v0, v1, v4}, LX/CG1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CG1;->SPRINGING:LX/CG1;

    .line 1864783
    const/4 v0, 0x3

    new-array v0, v0, [LX/CG1;

    sget-object v1, LX/CG1;->IDLE:LX/CG1;

    aput-object v1, v0, v2

    sget-object v1, LX/CG1;->DRAGGING:LX/CG1;

    aput-object v1, v0, v3

    sget-object v1, LX/CG1;->SPRINGING:LX/CG1;

    aput-object v1, v0, v4

    sput-object v0, LX/CG1;->$VALUES:[LX/CG1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1864784
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CG1;
    .locals 1

    .prologue
    .line 1864785
    const-class v0, LX/CG1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CG1;

    return-object v0
.end method

.method public static values()[LX/CG1;
    .locals 1

    .prologue
    .line 1864786
    sget-object v0, LX/CG1;->$VALUES:[LX/CG1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CG1;

    return-object v0
.end method
