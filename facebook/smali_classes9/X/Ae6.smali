.class public LX/Ae6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ipc/composer/launch/ShareComposerLauncher;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0pf;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Or;LX/0Ot;LX/0pf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/ipc/composer/launch/ShareComposerLauncher;",
            ">;",
            "LX/0pf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1696228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1696229
    iput-object p1, p0, LX/Ae6;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1696230
    iput-object p2, p0, LX/Ae6;->b:LX/0Or;

    .line 1696231
    iput-object p3, p0, LX/Ae6;->c:LX/0Ot;

    .line 1696232
    iput-object p4, p0, LX/Ae6;->d:LX/0pf;

    .line 1696233
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1696234
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1696235
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1696236
    invoke-static {p1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1696237
    iget-object v2, p0, LX/Ae6;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v3, LX/21D;->LIVE_VIDEO:LX/21D;

    const-string v4, "shareButton"

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 1696238
    iget-object v5, p0, LX/Ae6;->d:LX/0pf;

    invoke-virtual {v5, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v6

    .line 1696239
    if-eqz v6, :cond_0

    .line 1696240
    iget-object v5, v6, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v5, v5

    .line 1696241
    if-eqz v5, :cond_0

    .line 1696242
    iget-object v5, v6, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v5, v5

    .line 1696243
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1696244
    iget-object v5, v6, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v5, v5

    .line 1696245
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v7

    iget-object v5, p0, LX/Ae6;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1696246
    iget-object v8, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v8

    .line 1696247
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1696248
    iget-object v5, v6, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v5, v5

    .line 1696249
    new-instance v7, LX/89I;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    sget-object v8, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v7, v9, v10, v8}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v8

    .line 1696250
    iput-object v8, v7, LX/89I;->c:Ljava/lang/String;

    .line 1696251
    move-object v7, v7

    .line 1696252
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    .line 1696253
    iput-object v8, v7, LX/89I;->d:Ljava/lang/String;

    .line 1696254
    move-object v7, v7

    .line 1696255
    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    .line 1696256
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v5

    .line 1696257
    iget-object v8, v6, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v6, v8

    .line 1696258
    invoke-virtual {v5, v6}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v5

    .line 1696259
    invoke-virtual {v2, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1696260
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "live_video"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1696261
    const-class v0, Landroid/app/Activity;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1696262
    if-eqz v0, :cond_1

    .line 1696263
    iget-object v1, p0, LX/Ae6;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/89P;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x6dc

    invoke-virtual {v1, v2, v3, v0}, LX/89P;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1696264
    :cond_1
    return-void
.end method
