.class public LX/Bgv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;",
        "LX/97f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/03V;

.field private final d:LX/Bez;

.field public final e:Ljava/util/Locale;

.field public final f:LX/BfJ;

.field public final g:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 1808620
    const-class v0, LX/Bgv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bgv;->a:Ljava/lang/String;

    .line 1808621
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Bgv;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/Bez;Ljava/util/Locale;LX/BfJ;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808614
    iput-object p1, p0, LX/Bgv;->c:LX/03V;

    .line 1808615
    iput-object p2, p0, LX/Bgv;->d:LX/Bez;

    .line 1808616
    iput-object p3, p0, LX/Bgv;->e:Ljava/util/Locale;

    .line 1808617
    iput-object p4, p0, LX/Bgv;->f:LX/BfJ;

    .line 1808618
    iput-object p5, p0, LX/Bgv;->g:LX/0Uh;

    .line 1808619
    return-void
.end method

.method public static a(LX/Bgv;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)Landroid/view/View$OnClickListener;
    .locals 6
    .param p2    # LX/BgS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808610
    if-nez p3, :cond_0

    .line 1808611
    const/4 v0, 0x0

    .line 1808612
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Bgs;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Bgs;-><init>(LX/Bgv;LX/97f;LX/BeD;LX/BgS;Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public static a(LX/Bgv;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1808601
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1808602
    const-string v0, ""

    .line 1808603
    invoke-static {p1, p2}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v1

    .line 1808604
    iget-object v5, v1, LX/Bex;->a:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    move-object v3, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    .line 1808605
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1808606
    const v3, 0x7f08290f

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, LX/Bgv;->d:LX/Bez;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v8, v10, v11}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x1

    iget-object v9, p0, LX/Bgv;->d:LX/Bez;

    iget-wide v10, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v9, v10, v11}, LX/Bez;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {p3, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1808607
    const-string v3, "\n"

    .line 1808608
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1808609
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/97c;)Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 1808557
    sget-object v4, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1808558
    invoke-static {v0, p0}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v0

    iget-object v6, v0, LX/Bex;->a:LX/0Px;

    .line 1808559
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    :goto_1
    if-ge v2, v7, :cond_2

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    .line 1808560
    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    cmp-long v8, v8, v10

    if-nez v8, :cond_0

    iget-wide v8, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    cmp-long v0, v8, v10

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1808561
    :goto_2
    return v0

    .line 1808562
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1808563
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1808564
    :cond_3
    const/4 v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808600
    sget-object v0, LX/BeE;->HOURS_FIELD:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808598
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808599
    const v1, 0x7f03141e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808596
    check-cast p2, LX/97f;

    .line 1808597
    return-object p2
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808565
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;

    move-object v2, p2

    check-cast v2, LX/97f;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p6

    move-object v5, p7

    .line 1808566
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a()V

    .line 1808567
    sget-object p0, LX/Bgu;->a:[I

    invoke-interface {v2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1808568
    iget-object p0, v0, LX/Bgv;->c:LX/03V;

    sget-object p1, LX/Bgv;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Trying to bind view with unsupported option selected: "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808569
    :cond_0
    :goto_0
    return-void

    .line 1808570
    :pswitch_0
    invoke-static {v2}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object p0

    .line 1808571
    if-eqz p0, :cond_4

    invoke-static {p0}, LX/Bgv;->a(LX/97c;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 1808572
    invoke-static {v2}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object p0

    .line 1808573
    if-eqz p0, :cond_2

    .line 1808574
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    .line 1808575
    new-instance p1, Ljava/text/DateFormatSymbols;

    iget-object p2, v0, LX/Bgv;->e:Ljava/util/Locale;

    invoke-direct {p1, p2}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-virtual {p1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object p4

    .line 1808576
    sget-object p1, LX/Bgv;->b:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p5

    const/4 p1, 0x0

    move p2, p1

    :goto_1
    if-ge p2, p5, :cond_2

    sget-object p1, LX/Bgv;->b:LX/0Px;

    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 1808577
    invoke-static {v0, p1, p0, p3}, LX/Bgv;->a(LX/Bgv;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p6

    .line 1808578
    invoke-static {p6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p7

    if-eqz p7, :cond_1

    .line 1808579
    aget-object p1, p4, p1

    invoke-static {p1}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a(Ljava/lang/String;)V

    .line 1808580
    :goto_2
    add-int/lit8 p1, p2, 0x1

    move p2, p1

    goto :goto_1

    .line 1808581
    :cond_1
    aget-object p1, p4, p1

    invoke-static {p1}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1, p6}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1808582
    :cond_2
    invoke-static {v2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->setFieldIcon(Ljava/lang/String;)V

    .line 1808583
    invoke-static {v0, v2, v3, v4, v5}, LX/Bgv;->a(LX/Bgv;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808584
    :goto_3
    invoke-interface {v2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-eq p0, p1, :cond_0

    .line 1808585
    iget-object p0, v0, LX/Bgv;->g:LX/0Uh;

    const/16 p1, 0x453

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1808586
    new-instance p0, LX/Bgt;

    invoke-direct {p0, v0, v2, v3}, LX/Bgt;-><init>(LX/Bgv;LX/97f;LX/BgS;)V

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a(Landroid/view/View$OnClickListener;)V

    .line 1808587
    :cond_3
    goto/16 :goto_0

    .line 1808588
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b()V

    .line 1808589
    invoke-static {v0, v2, v3, v4, v5}, LX/Bgv;->a(LX/Bgv;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808590
    goto :goto_3

    .line 1808591
    :pswitch_1
    invoke-static {v2}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object p0

    .line 1808592
    invoke-static {v2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object p1

    .line 1808593
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object p0

    .line 1808594
    invoke-static {v0, v2, v3, v4, v5}, LX/Bgv;->a(LX/Bgv;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)Landroid/view/View$OnClickListener;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808595
    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
