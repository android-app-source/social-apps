.class public LX/B9H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/B9F;


# direct methods
.method public constructor <init>(LX/0Or;LX/B9F;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/B9F;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1751441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1751442
    iput-object p1, p0, LX/B9H;->a:LX/0Or;

    .line 1751443
    iput-object p2, p0, LX/B9H;->b:LX/B9F;

    .line 1751444
    return-void
.end method

.method public static a(LX/0QB;)LX/B9H;
    .locals 5

    .prologue
    .line 1751445
    const-class v1, LX/B9H;

    monitor-enter v1

    .line 1751446
    :try_start_0
    sget-object v0, LX/B9H;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1751447
    sput-object v2, LX/B9H;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1751448
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1751449
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1751450
    new-instance v4, LX/B9H;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1751451
    new-instance v3, LX/B9F;

    invoke-direct {v3}, LX/B9F;-><init>()V

    .line 1751452
    move-object v3, v3

    .line 1751453
    move-object v3, v3

    .line 1751454
    check-cast v3, LX/B9F;

    invoke-direct {v4, p0, v3}, LX/B9H;-><init>(LX/0Or;LX/B9F;)V

    .line 1751455
    move-object v0, v4

    .line 1751456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1751457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B9H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1751458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1751459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1751460
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1751461
    const-string v1, "log_megaphone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1751462
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1751463
    const-string v1, "logMegaphoneParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/megaphone/api/LogMegaphoneParams;

    .line 1751464
    iget-object v1, p0, LX/B9H;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/B9H;->b:LX/B9F;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1751465
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1751466
    move-object v0, v0

    .line 1751467
    return-object v0

    .line 1751468
    :cond_0
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
