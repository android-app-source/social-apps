.class public final LX/BuA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;)V
    .locals 0

    .prologue
    .line 1830443
    iput-object p1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;B)V
    .locals 0

    .prologue
    .line 1830428
    invoke-direct {p0, p1}, LX/BuA;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    .line 1830429
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 1830430
    packed-switch v1, :pswitch_data_0

    .line 1830431
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1830432
    :pswitch_0
    sget-object v1, LX/0ax;->dv:Ljava/lang/String;

    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->D:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v4, LX/0wD;->FULLSCREEN_VIDEO_PLAYER:LX/0wD;

    invoke-virtual {v4}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1830433
    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->o:LX/17Y;

    iget-object v3, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v3}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1830434
    if-eqz v1, :cond_0

    .line 1830435
    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->p:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v3}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1830436
    :cond_0
    iget-object v1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2ot;

    invoke-direct {v2}, LX/2ot;-><init>()V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 1830437
    :pswitch_1
    iget-object v1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    if-nez v1, :cond_1

    .line 1830438
    iget-object v1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->D:Ljava/lang/String;

    new-instance v3, LX/Bu5;

    invoke-direct {v3, p0}, LX/Bu5;-><init>(LX/BuA;)V

    new-instance v4, LX/Bu7;

    invoke-direct {v4, p0}, LX/Bu7;-><init>(LX/BuA;)V

    iget-object v5, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v5, v5, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    iget-object v6, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v6, v6, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnDismissListener;LX/Bu6;LX/0Px;Lcom/facebook/prefs/shared/FbSharedPreferences;)Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    move-result-object v2

    .line 1830439
    iput-object v2, v1, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    .line 1830440
    :cond_1
    iget-object v1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->q:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v1, v2, v7}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1830441
    iget-object v1, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2os;

    invoke-direct {v2, v0}, LX/2os;-><init>(Z)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 1830442
    :pswitch_2
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v2}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08104d

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f081050

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2, v7}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f08001c

    new-instance v3, LX/Bu9;

    invoke-direct {v3, p0}, LX/Bu9;-><init>(LX/BuA;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
