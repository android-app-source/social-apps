.class public LX/BNP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/1nC;

.field private final b:LX/1Kf;

.field private final c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/79D;

.field public final f:LX/BNL;

.field private final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0kL;

.field public i:LX/4BY;


# direct methods
.method public constructor <init>(LX/1nC;LX/1Kf;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Landroid/content/res/Resources;LX/BNL;LX/79D;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778637
    iput-object p1, p0, LX/BNP;->a:LX/1nC;

    .line 1778638
    iput-object p2, p0, LX/BNP;->b:LX/1Kf;

    .line 1778639
    iput-object p4, p0, LX/BNP;->d:Landroid/content/res/Resources;

    .line 1778640
    iput-object p6, p0, LX/BNP;->e:LX/79D;

    .line 1778641
    iput-object p5, p0, LX/BNP;->f:LX/BNL;

    .line 1778642
    iput-object p3, p0, LX/BNP;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1778643
    iput-object p7, p0, LX/BNP;->g:LX/1Ck;

    .line 1778644
    iput-object p8, p0, LX/BNP;->h:LX/0kL;

    .line 1778645
    return-void
.end method

.method public static a(LX/0QB;)LX/BNP;
    .locals 12

    .prologue
    .line 1778646
    const-class v1, LX/BNP;

    monitor-enter v1

    .line 1778647
    :try_start_0
    sget-object v0, LX/BNP;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1778648
    sput-object v2, LX/BNP;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1778649
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1778650
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1778651
    new-instance v3, LX/BNP;

    invoke-static {v0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v4

    check-cast v4, LX/1nC;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/BNL;->a(LX/0QB;)LX/BNL;

    move-result-object v8

    check-cast v8, LX/BNL;

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v9

    check-cast v9, LX/79D;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-direct/range {v3 .. v11}, LX/BNP;-><init>(LX/1nC;LX/1Kf;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Landroid/content/res/Resources;LX/BNL;LX/79D;LX/1Ck;LX/0kL;)V

    .line 1778652
    move-object v0, v3

    .line 1778653
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1778654
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BNP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778655
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1778656
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V
    .locals 12
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1778657
    if-eqz p7, :cond_1

    const/4 v5, 0x1

    :goto_0
    move-object v3, p3

    move-object/from16 v4, p4

    move-wide/from16 v6, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    invoke-static/range {v3 .. v10}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    new-instance v3, LX/89K;

    invoke-direct {v3}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v3

    invoke-static {v3}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCacheId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 1778658
    if-eqz p11, :cond_0

    .line 1778659
    invoke-static/range {p11 .. p11}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1778660
    :cond_0
    iget-object v3, p0, LX/BNP;->b:LX/1Kf;

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-interface {v3, v4, v2, p1, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1778661
    return-void

    .line 1778662
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "LX/0am",
            "<+",
            "Lcom/facebook/reviews/util/intent/ReviewComposerLauncherAndHandler$PostReviewCallback;",
            ">;",
            "LX/0am",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1778663
    if-nez p1, :cond_2

    .line 1778664
    iget-object v0, p0, LX/BNP;->i:LX/4BY;

    if-eqz v0, :cond_0

    .line 1778665
    iget-object v0, p0, LX/BNP;->i:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1778666
    :cond_0
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1778667
    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNO;

    invoke-virtual {v0}, LX/BNO;->a()V

    .line 1778668
    :cond_1
    :goto_0
    return-void

    .line 1778669
    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1778670
    const-string v0, "publishReviewParams"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1778671
    if-nez v0, :cond_3

    .line 1778672
    iget-object v0, p0, LX/BNP;->h:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/BNP;->d:Landroid/content/res/Resources;

    const v3, 0x7f0814e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1778673
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1778674
    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNO;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Returned intent does not contain PostReviewParams to post the review"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/BNO;->b()V

    goto :goto_0

    .line 1778675
    :cond_3
    invoke-virtual {p5}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1778676
    invoke-virtual {p5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {p5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const v3, 0x7f0814e3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const v4, 0x7f0814e4

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v3, v2, v4, v5}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v1

    iput-object v1, p0, LX/BNP;->i:LX/4BY;

    .line 1778677
    :cond_4
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1778678
    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    .line 1778679
    :cond_5
    iget-object v1, p0, LX/BNP;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1778680
    new-instance v2, LX/BNN;

    invoke-direct {v2, p0, p3, v0, p4}, LX/BNN;-><init>(LX/BNP;Ljava/lang/String;Lcom/facebook/composer/protocol/PostReviewParams;LX/0am;)V

    .line 1778681
    iget-object v3, p0, LX/BNP;->g:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "task_key_post_review"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method
