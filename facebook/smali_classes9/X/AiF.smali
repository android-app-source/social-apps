.class public final LX/AiF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1705802
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1705803
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1705804
    :goto_0
    return v1

    .line 1705805
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1705806
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1705807
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1705808
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1705809
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1705810
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1705811
    :cond_1
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1705812
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1705813
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1705814
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1705815
    invoke-static {p0, p1}, LX/AiE;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1705816
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1705817
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1705818
    goto :goto_1

    .line 1705819
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1705820
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1705821
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v8, :cond_d

    .line 1705822
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1705823
    :goto_3
    move v3, v6

    .line 1705824
    goto :goto_1

    .line 1705825
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1705826
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1705827
    if-eqz v0, :cond_6

    .line 1705828
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1705829
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1705830
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1705831
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1705832
    :cond_8
    const-string v11, "has_next_page"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1705833
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v7

    .line 1705834
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1705835
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1705836
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1705837
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_9

    if-eqz v10, :cond_9

    .line 1705838
    const-string v11, "end_cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1705839
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 1705840
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1705841
    :cond_b
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1705842
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1705843
    if-eqz v3, :cond_c

    .line 1705844
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 1705845
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_d
    move v3, v6

    move v8, v6

    move v9, v6

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1705774
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1705775
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1705776
    if-eqz v0, :cond_0

    .line 1705777
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1705778
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1705779
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1705780
    if-eqz v0, :cond_2

    .line 1705781
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1705782
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1705783
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1705784
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/AiE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1705785
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1705786
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1705787
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1705788
    if-eqz v0, :cond_5

    .line 1705789
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1705790
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1705791
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1705792
    if-eqz v1, :cond_3

    .line 1705793
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1705794
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1705795
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1705796
    if-eqz v1, :cond_4

    .line 1705797
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1705798
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1705799
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1705800
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1705801
    return-void
.end method
