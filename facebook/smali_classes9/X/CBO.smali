.class public final LX/CBO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0aC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0aC",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

.field public final synthetic c:LX/CBP;


# direct methods
.method public constructor <init>(LX/CBP;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V
    .locals 0

    .prologue
    .line 1856389
    iput-object p1, p0, LX/CBO;->c:LX/CBP;

    iput-object p2, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    iput-object p3, p0, LX/CBO;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1856390
    check-cast p1, Ljava/lang/String;

    .line 1856391
    const-string v0, "primary"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1856392
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->b:LX/2mJ;

    iget-object v1, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856393
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->c:LX/13P;

    sget-object v1, LX/77m;->PRIMARY_ACTION:LX/77m;

    iget-object v2, p0, LX/CBO;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    .line 1856394
    :cond_0
    :goto_0
    return-void

    .line 1856395
    :cond_1
    const-string v0, "secondary"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1856396
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->b:LX/2mJ;

    iget-object v1, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856397
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->c:LX/13P;

    sget-object v1, LX/77m;->SECONDARY_ACTION:LX/77m;

    iget-object v2, p0, LX/CBO;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    goto :goto_0

    .line 1856398
    :cond_2
    const-string v0, "dismiss"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856399
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->b:LX/2mJ;

    iget-object v1, p0, LX/CBO;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856400
    iget-object v0, p0, LX/CBO;->c:LX/CBP;

    iget-object v0, v0, LX/CBP;->c:LX/13P;

    sget-object v1, LX/77m;->DISMISS_ACTION:LX/77m;

    iget-object v2, p0, LX/CBO;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    goto :goto_0
.end method
