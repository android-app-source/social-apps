.class public LX/BKy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BKm;

.field private final b:Lcom/facebook/user/model/User;

.field private final c:LX/0oz;

.field public final d:LX/0SG;

.field public final e:LX/75Q;

.field public final f:LX/75F;

.field public final g:LX/1EZ;

.field public final h:LX/0kL;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:LX/8LV;

.field private final k:LX/03V;

.field private final l:LX/74n;

.field private final m:LX/7Dh;

.field private final n:LX/AR1;

.field private final o:LX/BJi;

.field private final p:J

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/BKm;Lcom/facebook/user/model/User;LX/AR1;LX/0oz;LX/0SG;LX/75Q;LX/75F;LX/0kL;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1EZ;LX/8LV;LX/03V;LX/74n;LX/7Dh;LX/BJi;)V
    .locals 4
    .param p1    # LX/BKm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774803
    iput-object p1, p0, LX/BKy;->a:LX/BKm;

    .line 1774804
    iput-object p2, p0, LX/BKy;->b:Lcom/facebook/user/model/User;

    .line 1774805
    iput-object p3, p0, LX/BKy;->n:LX/AR1;

    .line 1774806
    iput-object p4, p0, LX/BKy;->c:LX/0oz;

    .line 1774807
    iput-object p5, p0, LX/BKy;->d:LX/0SG;

    .line 1774808
    iput-object p6, p0, LX/BKy;->e:LX/75Q;

    .line 1774809
    iput-object p7, p0, LX/BKy;->f:LX/75F;

    .line 1774810
    iput-object p8, p0, LX/BKy;->h:LX/0kL;

    .line 1774811
    iput-object p9, p0, LX/BKy;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1774812
    iput-object p10, p0, LX/BKy;->g:LX/1EZ;

    .line 1774813
    iput-object p11, p0, LX/BKy;->j:LX/8LV;

    .line 1774814
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    iput-object v2, p0, LX/BKy;->q:LX/0Px;

    .line 1774815
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    iput-object v2, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774816
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->g()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/BKy;->s:Ljava/lang/String;

    .line 1774817
    move-object/from16 v0, p12

    iput-object v0, p0, LX/BKy;->k:LX/03V;

    .line 1774818
    move-object/from16 v0, p13

    iput-object v0, p0, LX/BKy;->l:LX/74n;

    .line 1774819
    move-object/from16 v0, p14

    iput-object v0, p0, LX/BKy;->m:LX/7Dh;

    .line 1774820
    move-object/from16 v0, p15

    iput-object v0, p0, LX/BKy;->o:LX/BJi;

    .line 1774821
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, LX/BKy;->p:J

    .line 1774822
    invoke-direct {p0}, LX/BKy;->c()V

    .line 1774823
    return-void

    .line 1774824
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method private static b(LX/BKy;)Lcom/facebook/composer/publish/common/PublishPostParams;
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1774825
    iget-object v0, p0, LX/BKy;->a:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1774826
    iget-object v0, p0, LX/BKy;->a:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1774827
    :goto_0
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->t()LX/0Rf;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 1774828
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v6, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1774829
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->m()Ljava/lang/Long;

    move-result-object v5

    .line 1774830
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1774831
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v2

    .line 1774832
    :goto_1
    iget-object v7, p0, LX/BKy;->a:LX/BKm;

    iget-object v7, v7, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v7}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1774833
    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v1

    .line 1774834
    :cond_0
    new-instance v7, LX/5M9;

    invoke-direct {v7}, LX/5M9;-><init>()V

    iget-object v8, p0, LX/BKy;->a:LX/BKm;

    iget-object v8, v8, LX/BKm;->a:Ljava/lang/String;

    .line 1774835
    iput-object v8, v7, LX/5M9;->G:Ljava/lang/String;

    .line 1774836
    move-object v7, v7

    .line 1774837
    iget-object v8, p0, LX/BKy;->a:LX/BKm;

    iget-object v8, v8, LX/BKm;->a:Ljava/lang/String;

    .line 1774838
    iput-object v8, v7, LX/5M9;->s:Ljava/lang/String;

    .line 1774839
    move-object v7, v7

    .line 1774840
    iget-object v8, p0, LX/BKy;->d:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 1774841
    iput-wide v8, v7, LX/5M9;->u:J

    .line 1774842
    move-object v7, v7

    .line 1774843
    iget-object v8, p0, LX/BKy;->a:LX/BKm;

    iget-object v8, v8, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v8, v8, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1774844
    iput-wide v8, v7, LX/5M9;->b:J

    .line 1774845
    move-object v7, v7

    .line 1774846
    iget-object v8, p0, LX/BKy;->s:Ljava/lang/String;

    .line 1774847
    iput-object v8, v7, LX/5M9;->c:Ljava/lang/String;

    .line 1774848
    move-object v7, v7

    .line 1774849
    iput-object v0, v7, LX/5M9;->d:Ljava/lang/String;

    .line 1774850
    move-object v0, v7

    .line 1774851
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v7

    .line 1774852
    iput-object v7, v0, LX/5M9;->q:LX/2rt;

    .line 1774853
    move-object v7, v0

    .line 1774854
    iget-object v0, p0, LX/BKy;->a:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v3

    .line 1774855
    :goto_2
    iput-boolean v0, v7, LX/5M9;->t:Z

    .line 1774856
    move-object v0, v7

    .line 1774857
    invoke-virtual {v0, v4}, LX/5M9;->d(LX/0Px;)LX/5M9;

    move-result-object v0

    iget-object v4, p0, LX/BKy;->b:Lcom/facebook/user/model/User;

    .line 1774858
    iget-object v7, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v7

    .line 1774859
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1774860
    iput-wide v8, v0, LX/5M9;->k:J

    .line 1774861
    move-object v0, v0

    .line 1774862
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getNectarModule()Ljava/lang/String;

    move-result-object v4

    .line 1774863
    iput-object v4, v0, LX/5M9;->n:Ljava/lang/String;

    .line 1774864
    move-object v0, v0

    .line 1774865
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->q()Z

    move-result v4

    .line 1774866
    iput-boolean v4, v0, LX/5M9;->w:Z

    .line 1774867
    move-object v0, v0

    .line 1774868
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    .line 1774869
    iput-object v4, v0, LX/5M9;->A:Ljava/lang/String;

    .line 1774870
    move-object v0, v0

    .line 1774871
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1774872
    iput-object v4, v0, LX/5M9;->L:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1774873
    move-object v0, v0

    .line 1774874
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v4

    .line 1774875
    iput-object v4, v0, LX/5M9;->p:LX/5Rn;

    .line 1774876
    move-object v0, v0

    .line 1774877
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v4

    .line 1774878
    iput-object v4, v0, LX/5M9;->T:Ljava/lang/String;

    .line 1774879
    move-object v0, v0

    .line 1774880
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/5M9;->a(Ljava/lang/Long;)LX/5M9;

    move-result-object v0

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isThrowbackPost()Z

    move-result v4

    .line 1774881
    iput-boolean v4, v0, LX/5M9;->P:Z

    .line 1774882
    move-object v0, v0

    .line 1774883
    iput-object v2, v0, LX/5M9;->H:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1774884
    move-object v0, v0

    .line 1774885
    iget-object v2, p0, LX/BKy;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    .line 1774886
    iput-object v2, v0, LX/5M9;->R:Ljava/lang/String;

    .line 1774887
    move-object v0, v0

    .line 1774888
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v2

    .line 1774889
    iput-object v2, v0, LX/5M9;->h:Ljava/lang/String;

    .line 1774890
    move-object v0, v0

    .line 1774891
    iput-boolean v3, v0, LX/5M9;->K:Z

    .line 1774892
    move-object v0, v0

    .line 1774893
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    .line 1774894
    iput-object v2, v0, LX/5M9;->U:Ljava/lang/String;

    .line 1774895
    move-object v0, v0

    .line 1774896
    iput-object v1, v0, LX/5M9;->J:Ljava/lang/String;

    .line 1774897
    move-object v0, v0

    .line 1774898
    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->v()Z

    move-result v1

    .line 1774899
    iput-boolean v1, v0, LX/5M9;->V:Z

    .line 1774900
    move-object v0, v0

    .line 1774901
    iget-object v1, p0, LX/BKy;->o:LX/BJi;

    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    invoke-virtual {v1, v2}, LX/BJi;->a(LX/BKm;)Z

    move-result v1

    .line 1774902
    iput-boolean v1, v0, LX/5M9;->W:Z

    .line 1774903
    move-object v0, v0

    .line 1774904
    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 1774905
    if-eqz v1, :cond_3

    .line 1774906
    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1774907
    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1774908
    iput-object v2, v0, LX/5M9;->j:Ljava/lang/String;

    .line 1774909
    move-object v2, v0

    .line 1774910
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    .line 1774911
    iput-object v3, v2, LX/5M9;->B:Ljava/lang/String;

    .line 1774912
    move-object v2, v2

    .line 1774913
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    .line 1774914
    iput-object v3, v2, LX/5M9;->C:Ljava/lang/String;

    .line 1774915
    move-object v2, v2

    .line 1774916
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    .line 1774917
    iput-object v3, v2, LX/5M9;->D:Ljava/lang/String;

    .line 1774918
    move-object v2, v2

    .line 1774919
    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    .line 1774920
    iput-object v3, v2, LX/5M9;->F:Ljava/lang/String;

    .line 1774921
    :cond_1
    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1774922
    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1774923
    iput-object v2, v0, LX/5M9;->E:Ljava/lang/String;

    .line 1774924
    :cond_2
    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1774925
    iput-object v2, v0, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1774926
    move-object v2, v0

    .line 1774927
    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    .line 1774928
    iput-object v1, v2, LX/5M9;->m:Ljava/lang/String;

    .line 1774929
    :cond_3
    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1774930
    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v1

    .line 1774931
    iput-object v1, v0, LX/5M9;->y:Ljava/lang/String;

    .line 1774932
    move-object v1, v0

    .line 1774933
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v2

    .line 1774934
    iput-object v2, v1, LX/5M9;->x:Ljava/lang/String;

    .line 1774935
    move-object v1, v1

    .line 1774936
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v2

    .line 1774937
    iput-object v2, v1, LX/5M9;->z:Ljava/lang/String;

    .line 1774938
    :cond_4
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    return-object v0

    .line 1774939
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_6
    const-wide/16 v4, 0x0

    goto/16 :goto_3

    :cond_7
    move-object v2, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1774940
    iget-object v0, p0, LX/BKy;->a:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->y()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BKy;->a:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1774941
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BKy;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1774942
    iget-object v1, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1774943
    iput-object v0, v2, LX/173;->f:Ljava/lang/String;

    .line 1774944
    move-object v2, v2

    .line 1774945
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v1, v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774946
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/BKy;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BKy;->s:Ljava/lang/String;

    .line 1774947
    :cond_0
    return-void
.end method

.method public static f(LX/BKy;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 50
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774948
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->q(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1774949
    invoke-static/range {p0 .. p0}, LX/BKy;->g(LX/BKy;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1774950
    :goto_0
    return-object v2

    .line 1774951
    :cond_0
    sget-object v24, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1774952
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1774953
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v24

    .line 1774954
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v25

    .line 1774955
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v11

    .line 1774956
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->t()LX/0Rf;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v18

    .line 1774957
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1774958
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1774959
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v9, v2, LX/BKm;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v10

    const/4 v12, 0x0

    invoke-virtual/range {v3 .. v12}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    .line 1774960
    :cond_2
    const/16 v25, 0x0

    goto :goto_1

    .line 1774961
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/BKy;->p:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v23, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v0, v2, LX/BKm;->a:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->q()Z

    move-result v26

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_5

    const/16 v27, 0x1

    :goto_3
    const/16 v29, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->z()Z

    move-result v30

    const/16 v31, 0x0

    move-object/from16 v28, v11

    invoke-virtual/range {v12 .. v31}, LX/8LV;->a(LX/0Px;LX/0Px;Ljava/lang/String;JLX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    const/16 v23, 0x0

    goto :goto_2

    :cond_5
    const/16 v27, 0x0

    goto :goto_3

    .line 1774962
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-object/from16 v0, p0

    iget-object v4, v0, LX/BKy;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    .line 1774963
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v23

    new-instance v26, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/BKy;->p:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v31

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v32, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v0, v2, LX/BKm;->a:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->j()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->k()J

    move-result-wide v35

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->q()Z

    move-result v38

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_8

    const/16 v39, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v41

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->z()Z

    move-result v42

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    move-object/from16 v27, v18

    move-object/from16 v40, v11

    invoke-virtual/range {v20 .. v47}, LX/8LV;->a(LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_7
    const/16 v32, 0x0

    goto :goto_4

    :cond_8
    const/16 v39, 0x0

    goto :goto_5

    .line 1774964
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_10

    .line 1774965
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1774966
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/BKy;->p:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v28

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v29, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v0, v2, LX/BKm;->a:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->m()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->m()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v33

    :goto_7
    const/16 v35, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_c

    const/16 v37, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v38

    const/16 v39, 0x0

    const/16 v40, 0x0

    move-object/from16 v36, v11

    invoke-virtual/range {v18 .. v40}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZZLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Ljava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_a
    const/16 v29, 0x0

    goto :goto_6

    :cond_b
    const-wide/16 v33, 0x0

    goto :goto_7

    :cond_c
    const/16 v37, 0x0

    goto :goto_8

    .line 1774967
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/BKy;->p:J

    move-wide/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_e

    const/16 v38, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v40

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->w(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->m:LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->b()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v41, 0x1

    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v0, v2, LX/BKm;->a:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->j()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->k()J

    move-result-wide v44

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->q:LX/0Px;

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v47

    const/16 v48, 0x0

    const/16 v49, 0x0

    move-object/from16 v33, v24

    move-object/from16 v34, v25

    move-object/from16 v35, v18

    move-object/from16 v46, v11

    invoke-virtual/range {v26 .. v49}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_e
    const/16 v38, 0x0

    goto :goto_9

    :cond_f
    const/16 v41, 0x0

    goto :goto_a

    .line 1774968
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->k:LX/03V;

    const-string v3, "photo upload error"

    const-string v4, "Unsupported upload type target=%d, user=%d, attach=%s"

    move-object/from16 v0, p0

    iget-object v5, v0, LX/BKy;->a:LX/BKm;

    iget-object v5, v5, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v6, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/BKy;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v6}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1774969
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private static g(LX/BKy;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 38

    .prologue
    .line 1774970
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1774971
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v0, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-wide/from16 v19, v0

    .line 1774972
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->h(LX/0Px;)Landroid/net/Uri;

    move-result-object v2

    .line 1774973
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->i(LX/0Px;)Landroid/os/Bundle;

    move-result-object v4

    .line 1774974
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->t()LX/0Rf;

    move-result-object v12

    .line 1774975
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v14, v3, LX/BKm;->a:Ljava/lang/String;

    .line 1774976
    const-wide/16 v10, -0x1

    .line 1774977
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1774978
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v10

    .line 1774979
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v29

    .line 1774980
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->m()Ljava/lang/Long;

    move-result-object v30

    .line 1774981
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->l:LX/74n;

    sget-object v5, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v3, v2, v5}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/media/VideoItem;

    .line 1774982
    if-nez v3, :cond_1

    .line 1774983
    move-object/from16 v0, p0

    iget-object v3, v0, LX/BKy;->k:LX/03V;

    const-string v4, "video upload error"

    const-string v5, "Null VideoItem for Uri %s"

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1774984
    const/4 v2, 0x0

    .line 1774985
    :goto_0
    return-object v2

    .line 1774986
    :cond_1
    sget-object v6, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1774987
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1774988
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->d()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v6

    .line 1774989
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v7

    .line 1774990
    :goto_1
    cmp-long v2, v8, v19

    if-eqz v2, :cond_3

    const-wide/16 v8, 0x0

    cmp-long v2, v19, v8

    if-gtz v2, :cond_6

    .line 1774991
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/BKy;->s:Ljava/lang/String;

    new-instance v8, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/BKy;->a:LX/BKm;

    iget-object v9, v9, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v9}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v12, v0, LX/BKy;->a:LX/BKm;

    iget-object v12, v12, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v12}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, LX/BKy;->a:LX/BKm;

    iget-object v15, v15, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v15}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->a:LX/BKm;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/platform/composer/model/PlatformComposition;->q()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->a:LX/BKm;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v17

    if-eqz v17, :cond_5

    const/16 v17, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->a:LX/BKm;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->a:LX/BKm;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/facebook/platform/composer/model/PlatformComposition;->z()Z

    move-result v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v2 .. v24}, LX/8LV;->a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    .line 1774992
    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1774993
    :cond_5
    const/16 v17, 0x0

    goto :goto_2

    .line 1774994
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1774995
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->s:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_7

    const/16 v26, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    move-object/from16 v22, v6

    move-object/from16 v23, v7

    move-wide/from16 v24, v10

    move-object/from16 v31, v14

    invoke-virtual/range {v16 .. v35}, LX/8LV;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZLjava/lang/String;ZLX/5Rn;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_7
    const/16 v26, 0x0

    goto :goto_3

    .line 1774996
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->j:LX/8LV;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, LX/BKy;->s:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static {v12}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_9

    const/16 v28, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->j()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->k()J

    move-result-wide v33

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v35

    const/16 v36, 0x0

    const/16 v37, 0x0

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    move-object/from16 v23, v6

    move-object/from16 v24, v7

    move-wide/from16 v26, v10

    move-object/from16 v31, v14

    invoke-virtual/range {v16 .. v37}, LX/8LV;->a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0

    :cond_9
    const/16 v28, 0x0

    goto :goto_4
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 15

    .prologue
    const/4 v3, 0x1

    .line 1774997
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1774998
    invoke-static {p0}, LX/BKy;->b(LX/BKy;)Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1774999
    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1775000
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1775001
    iget-object v5, p0, LX/BKy;->a:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v5

    const/4 v8, 0x1

    .line 1775002
    invoke-static {v5}, LX/7kq;->k(LX/0Px;)Z

    move-result v9

    if-eqz v9, :cond_14

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    if-le v9, v8, :cond_14

    :goto_0
    move v5, v8

    .line 1775003
    if-eqz v5, :cond_2

    .line 1775004
    iget-object v5, p0, LX/BKy;->q:LX/0Px;

    .line 1775005
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1775006
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v11

    const/4 v8, 0x0

    move v9, v8

    :goto_1
    if-ge v9, v11, :cond_1

    invoke-virtual {v5, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1775007
    invoke-static {v8}, LX/7kq;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 1775008
    invoke-virtual {v10, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775009
    :goto_2
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_1

    .line 1775010
    :cond_0
    invoke-virtual {v8}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v8

    check-cast v8, Lcom/facebook/photos/base/media/VideoItem;

    .line 1775011
    new-instance v12, LX/74k;

    invoke-direct {v12}, LX/74k;-><init>()V

    .line 1775012
    iget-object v13, v8, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v13, v13

    .line 1775013
    sget-object v14, LX/4gQ;->Photo:LX/4gQ;

    invoke-static {v13, v14}, LX/74c;->a(Lcom/facebook/ipc/media/data/LocalMediaData;LX/4gQ;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v13

    .line 1775014
    iput-object v13, v12, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1775015
    move-object v12, v12

    .line 1775016
    invoke-virtual {v12}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v12

    move-object v8, v12

    .line 1775017
    invoke-static {v8}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v8

    .line 1775018
    invoke-virtual {v8}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v8

    invoke-virtual {v10, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1775019
    :cond_1
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    move-object v5, v8

    .line 1775020
    iput-object v5, p0, LX/BKy;->q:LX/0Px;

    .line 1775021
    :cond_2
    iget-object v5, p0, LX/BKy;->a:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/7kq;->j(LX/0Px;)Z

    move-result v5

    if-eqz v5, :cond_a

    move v5, v7

    .line 1775022
    :goto_3
    move v4, v5

    .line 1775023
    if-nez v4, :cond_6

    .line 1775024
    const/4 v4, 0x0

    .line 1775025
    :goto_4
    move v2, v4

    .line 1775026
    if-eqz v2, :cond_5

    .line 1775027
    new-instance v2, LX/5M9;

    invoke-direct {v2, v0}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1775028
    iput-boolean v3, v2, LX/5M9;->r:Z

    .line 1775029
    move-object v0, v2

    .line 1775030
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1775031
    const-string v2, "is_uploading_media"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1775032
    :cond_3
    :goto_5
    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1775033
    iget-object v0, p0, LX/BKy;->n:LX/AR1;

    if-eqz v0, :cond_4

    .line 1775034
    iget-object v0, p0, LX/BKy;->n:LX/AR1;

    invoke-virtual {v0}, LX/AR1;->b()LX/9A3;

    move-result-object v0

    .line 1775035
    const-string v2, "extra_optimistic_feed_story"

    invoke-virtual {v0}, LX/9A3;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1775036
    :cond_4
    return-object v1

    .line 1775037
    :cond_5
    iget-object v2, p0, LX/BKy;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v2

    sget-object v3, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v2, v3, :cond_3

    .line 1775038
    const-string v2, "extra_actor_viewer_context"

    iget-object v3, p0, LX/BKy;->a:LX/BKm;

    iget-object v3, v3, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_5

    .line 1775039
    :cond_6
    invoke-static {p0}, LX/BKy;->f(LX/BKy;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    .line 1775040
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1775041
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1775042
    iget-object v5, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v8, v5

    .line 1775043
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_6
    if-ge v6, v9, :cond_8

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/media/MediaItem;

    .line 1775044
    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v10

    .line 1775045
    iget-object v11, v10, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v10, v11

    .line 1775046
    sget-object v11, LX/4gQ;->Photo:LX/4gQ;

    if-ne v10, v11, :cond_7

    .line 1775047
    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1775048
    :cond_7
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_6

    .line 1775049
    :cond_8
    iget-object v5, p0, LX/BKy;->e:LX/75Q;

    invoke-static {v5, v7}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a(LX/75Q;Ljava/util/List;)Lcom/facebook/photos/tagging/store/TagStoreCopy;

    move-result-object v5

    iget-object v6, p0, LX/BKy;->f:LX/75F;

    invoke-static {v6, v7}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a(LX/75F;Ljava/util/List;)Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/tagging/store/TagStoreCopy;Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;)V

    .line 1775050
    iget-object v5, p0, LX/BKy;->g:LX/1EZ;

    invoke-virtual {v5, v4}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1775051
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->l(LX/0Px;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1775052
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->d(LX/0Px;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1775053
    const v4, 0x7f082480

    .line 1775054
    :goto_7
    iget-object v5, p0, LX/BKy;->h:LX/0kL;

    new-instance v6, LX/27k;

    invoke-direct {v6, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v5, v6}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1775055
    :cond_9
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 1775056
    :cond_a
    iget-object v5, p0, LX/BKy;->a:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/7kq;->l(LX/0Px;)Z

    move-result v5

    if-nez v5, :cond_b

    move v5, v6

    .line 1775057
    goto/16 :goto_3

    .line 1775058
    :cond_b
    iget-object v5, p0, LX/BKy;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 1775059
    sget-object v8, LX/0pP;->v:LX/0Tn;

    iget-object v9, p0, LX/BKy;->d:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    invoke-interface {v5, v8, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1775060
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 1775061
    iget-object v5, p0, LX/BKy;->a:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 1775062
    iget-object v5, p0, LX/BKy;->q:LX/0Px;

    iget-object v6, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1775063
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_d

    move v8, v9

    :goto_8
    invoke-static {v8}, LX/0PB;->checkArgument(Z)V

    .line 1775064
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 1775065
    invoke-virtual {v5, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1775066
    invoke-virtual {v8}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    move v11, v9

    .line 1775067
    :goto_9
    if-eqz v11, :cond_c

    .line 1775068
    const-string v11, "\n"

    invoke-static {v11}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    invoke-static {v6, v11}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 1775069
    :cond_c
    invoke-virtual {v8}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-static {v6, v8}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    .line 1775070
    invoke-virtual {v5, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v8}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v8

    .line 1775071
    iput-object v11, v8, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1775072
    move-object v11, v8

    .line 1775073
    invoke-virtual {v5, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v8}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v8

    .line 1775074
    iput-object v8, v11, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1775075
    move-object v8, v11

    .line 1775076
    invoke-virtual {v8}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v8

    .line 1775077
    invoke-virtual {v12, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775078
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    invoke-virtual {v5, v9, v8}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    :goto_a
    if-ge v10, v11, :cond_f

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1775079
    invoke-virtual {v12, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775080
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    :cond_d
    move v8, v10

    .line 1775081
    goto :goto_8

    :cond_e
    move v11, v10

    .line 1775082
    goto :goto_9

    .line 1775083
    :cond_f
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    move-object v5, v8

    .line 1775084
    iput-object v5, p0, LX/BKy;->q:LX/0Px;

    .line 1775085
    sget-object v5, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v5, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    :cond_10
    :goto_b
    move v5, v7

    .line 1775086
    goto/16 :goto_3

    .line 1775087
    :cond_11
    iget-object v5, p0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ne v5, v7, :cond_10

    iget-object v5, p0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 1775088
    iget-object v5, p0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1775089
    iget-object v8, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_13

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_13

    move v8, v7

    .line 1775090
    :goto_c
    if-eqz v8, :cond_12

    .line 1775091
    iget-object v8, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const-string v9, "\n"

    invoke-static {v9}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-static {v8, v9}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1775092
    :cond_12
    iget-object v8, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v9, p0, LX/BKy;->q:LX/0Px;

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {v8, v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    iput-object v6, p0, LX/BKy;->r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1775093
    invoke-static {v5}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v5

    sget-object v6, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1775094
    iput-object v6, v5, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1775095
    move-object v5, v5

    .line 1775096
    invoke-virtual {v5}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    .line 1775097
    if-eqz v5, :cond_10

    .line 1775098
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/BKy;->q:LX/0Px;

    goto :goto_b

    :cond_13
    move v8, v6

    .line 1775099
    goto :goto_c

    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1775100
    :cond_15
    iget-object v4, p0, LX/BKy;->a:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_16

    .line 1775101
    const v4, 0x7f08247e

    goto/16 :goto_7

    .line 1775102
    :cond_16
    const v4, 0x7f08247f

    goto/16 :goto_7
.end method
