.class public final LX/Bin;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventCreationCategorySelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventCreationCategorySelectionFragment;)V
    .locals 0

    .prologue
    .line 1810825
    iput-object p1, p0, LX/Bin;->a:Lcom/facebook/events/create/EventCreationCategorySelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "onFetchedCategories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1810826
    if-eqz p2, :cond_3

    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 1810827
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v1, p0, LX/Bin;->a:Lcom/facebook/events/create/EventCreationCategorySelectionFragment;

    iget-object v1, v1, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->f:LX/Bik;

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1810828
    :goto_2
    iput-object v0, v1, LX/Bik;->b:Ljava/util/List;

    .line 1810829
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1810830
    :cond_0
    return-void

    .line 1810831
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1810832
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1810833
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1810834
    goto :goto_2
.end method
