.class public LX/CNk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/CNb;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 4

    .prologue
    .line 1882043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882044
    const-string v0, "children"

    invoke-virtual {p1, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882045
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CNk;->a:Ljava/util/List;

    .line 1882046
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1882047
    iget-object v3, p0, LX/CNk;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1882048
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1882049
    :cond_0
    iput-object p1, p0, LX/CNk;->b:LX/CNb;

    .line 1882050
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1882051
    iget-object v0, p0, LX/CNk;->b:LX/CNb;

    .line 1882052
    const-string v1, "action_controller"

    move-object v1, v1

    .line 1882053
    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNl;

    .line 1882054
    iget v1, v0, LX/CNl;->a:I

    move v1, v1

    .line 1882055
    iget-object v2, p0, LX/CNk;->b:LX/CNb;

    const-string v3, "limit"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1882056
    iget-object v1, p0, LX/CNk;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CNe;

    .line 1882057
    invoke-interface {v1}, LX/CNe;->a()V

    goto :goto_0

    .line 1882058
    :cond_0
    iget v1, v0, LX/CNl;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/CNl;->a:I

    .line 1882059
    :cond_1
    return-void
.end method
