.class public LX/CLt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final traceInfo:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1879917
    new-instance v0, LX/1sv;

    const-string v1, "MqttThriftHeader"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/CLt;->b:LX/1sv;

    .line 1879918
    new-instance v0, LX/1sw;

    const-string v1, "traceInfo"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLt;->c:LX/1sw;

    .line 1879919
    sput-boolean v3, LX/CLt;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1879914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879915
    iput-object p1, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    .line 1879916
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1879893
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1879894
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1879895
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1879896
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MqttThriftHeader"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1879897
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879898
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879899
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879900
    iget-object v4, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1879901
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879902
    const-string v4, "traceInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879903
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879904
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879905
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1879906
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879907
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879908
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879909
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1879910
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 1879911
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 1879912
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1879913
    :cond_4
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1879866
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1879867
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1879868
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1879869
    sget-object v0, LX/CLt;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879870
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1879871
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1879872
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1879873
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1879878
    if-nez p1, :cond_1

    .line 1879879
    :cond_0
    :goto_0
    return v0

    .line 1879880
    :cond_1
    instance-of v1, p1, LX/CLt;

    if-eqz v1, :cond_0

    .line 1879881
    check-cast p1, LX/CLt;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1879882
    if-nez p1, :cond_3

    .line 1879883
    :cond_2
    :goto_1
    move v0, v2

    .line 1879884
    goto :goto_0

    .line 1879885
    :cond_3
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1879886
    :goto_2
    iget-object v3, p1, LX/CLt;->traceInfo:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1879887
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1879888
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879889
    iget-object v0, p0, LX/CLt;->traceInfo:Ljava/lang/String;

    iget-object v3, p1, LX/CLt;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1879890
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1879891
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1879892
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1879877
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1879874
    sget-boolean v0, LX/CLt;->a:Z

    .line 1879875
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/CLt;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1879876
    return-object v0
.end method
