.class public LX/BF3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1765348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765349
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1765350
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031341

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765351
    new-instance v0, LX/71J;

    sget-object v1, LX/71I;->AVAILABLE_PAYMENT_PROVIDER:LX/71I;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c7f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/71J;-><init>(LX/71I;Ljava/lang/String;Landroid/content/Intent;I)V

    .line 1765352
    new-instance v1, LX/BEs;

    invoke-direct {v1, p0, v0}, LX/BEs;-><init>(Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;LX/71J;)V

    .line 1765353
    new-instance v0, LX/BEt;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/BEt;-><init>(Landroid/content/Context;)V

    .line 1765354
    iget-object v2, v0, LX/BEt;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, v1, LX/BEs;->b:LX/71J;

    iget-object v3, v3, LX/71J;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1765355
    return-object v0
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765356
    instance-of v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;

    if-eqz v0, :cond_0

    .line 1765357
    check-cast p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;

    invoke-static {p0, p1}, LX/BF3;->a(Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1765358
    :goto_0
    return-object v0

    .line 1765359
    :cond_0
    instance-of v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    if-eqz v0, :cond_1

    .line 1765360
    check-cast p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    invoke-static {p0, p1}, LX/BF3;->a(Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1765361
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1765362
    sget-object v1, LX/BF2;->a:[I

    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->a()LX/BEp;

    move-result-object v2

    invoke-virtual {v2}, LX/BEp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1765363
    :goto_0
    return-object v0

    .line 1765364
    :pswitch_0
    new-instance v1, LX/BEu;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02160f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081d81

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0, v0}, LX/BEu;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 1765365
    new-instance v0, LX/BEv;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/BEv;-><init>(Landroid/content/Context;)V

    .line 1765366
    iput-object v1, v0, LX/BEv;->b:LX/BEu;

    .line 1765367
    iget-object v2, v0, LX/BEv;->a:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iget-object v3, v0, LX/BEv;->b:LX/BEu;

    iget-object v3, v3, LX/BEu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1765368
    iget-object v2, v0, LX/BEv;->a:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iget-object v3, v1, LX/BEu;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setTitle(Ljava/lang/String;)V

    .line 1765369
    iget-object v2, v0, LX/BEv;->a:Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iget-object v3, v1, LX/BEu;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setSubtitle(Ljava/lang/String;)V

    .line 1765370
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
