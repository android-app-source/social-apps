.class public LX/BiF;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1810074
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1810075
    const v0, 0x7f030382

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1810076
    const v0, 0x7f0d0b58

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/BiF;->a:Landroid/widget/ImageView;

    .line 1810077
    const v0, 0x7f0d0b59

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/BiF;->b:Landroid/widget/ProgressBar;

    .line 1810078
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, LX/BiF;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0d1b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, LX/BiF;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1810079
    const v0, 0x7f0208f0

    invoke-virtual {p0, v0}, LX/BiF;->setBackgroundResource(I)V

    .line 1810080
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1810081
    if-eqz p1, :cond_0

    .line 1810082
    iget-object v0, p0, LX/BiF;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1810083
    iget-object v0, p0, LX/BiF;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1810084
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/BiF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810085
    invoke-virtual {p0}, LX/BiF;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/BiF;->setBackgroundColor(I)V

    .line 1810086
    :goto_0
    return-void

    .line 1810087
    :cond_0
    iget-object v0, p0, LX/BiF;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1810088
    iget-object v0, p0, LX/BiF;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
