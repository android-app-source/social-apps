.class public final LX/Arl;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;)V
    .locals 0

    .prologue
    .line 1719625
    iput-object p1, p0, LX/Arl;->a:LX/Arq;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1719626
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->b()V

    .line 1719627
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1719628
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    const-string v1, "pr_camera_start_preview"

    invoke-static {v0, v1, p1}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719629
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    invoke-virtual {v0, p1}, LX/Arh;->a(Ljava/lang/Throwable;)V

    .line 1719630
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 1719631
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    .line 1719632
    iget-boolean v1, v0, LX/Arq;->C:Z

    if-eqz v1, :cond_0

    .line 1719633
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Arq;->C:Z

    .line 1719634
    :goto_0
    invoke-static {v0}, LX/Arq;->s(LX/Arq;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, LX/Arq;->a$redex0(LX/Arq;Ljava/io/File;)V

    .line 1719635
    iget-object v0, p0, LX/Arl;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1719636
    return-void

    .line 1719637
    :cond_0
    iget-object v1, v0, LX/Arq;->e:LX/Arh;

    .line 1719638
    iget-object v2, v1, LX/Arh;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Yi;

    .line 1719639
    iget-object v3, v1, LX/Arh;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/192;

    .line 1719640
    const-string v4, "camera_lib_type"

    sget-object v5, LX/6KU;->CAMERA_CORE:LX/6KU;

    invoke-virtual {v5}, LX/6KU;->name()Ljava/lang/String;

    move-result-object v5

    const-string v6, "product_name"

    sget-object v7, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v7}, LX/6KV;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v4

    .line 1719641
    iget-object v5, v2, LX/0Yi;->j:LX/0Yl;

    const v6, 0xa00a6

    const-string v7, "NNFInspirationCameraColdTTI"

    invoke-virtual {v5, v6, v7, v4}, LX/0Yl;->a(ILjava/lang/String;LX/0P1;)LX/0Yl;

    .line 1719642
    iget-object v5, v2, LX/0Yi;->j:LX/0Yl;

    const v6, 0xa00a7

    const-string v7, "NNFInspirationCameraWarmTTI"

    invoke-virtual {v5, v6, v7, v4}, LX/0Yl;->a(ILjava/lang/String;LX/0P1;)LX/0Yl;

    .line 1719643
    iget-object v2, v1, LX/Arh;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0xa10007

    const/4 v5, 0x2

    invoke-interface {v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1719644
    const v2, 0xa00a9

    const/4 v4, 0x2

    invoke-static {v3, v2, v4}, LX/192;->a(LX/192;IS)V

    .line 1719645
    iget-object v2, v1, LX/Arh;->k:LX/195;

    invoke-virtual {v2}, LX/195;->a()V

    .line 1719646
    invoke-static {v1}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v2

    sget-object v3, LX/86q;->READY:LX/86q;

    invoke-static {v1, v2, v3}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v2

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 1719647
    goto :goto_0
.end method
