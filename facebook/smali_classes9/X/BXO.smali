.class public final LX/BXO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793066
    iput-object p1, p0, LX/BXO;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 1793067
    if-nez p2, :cond_0

    .line 1793068
    iget-object v0, p0, LX/BXO;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1793069
    invoke-static {v0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b$redex0(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V

    .line 1793070
    iget-object v0, p0, LX/BXO;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BXO;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793071
    iget-object v0, p0, LX/BXO;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    const/4 v1, 0x1

    .line 1793072
    invoke-static {v0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    .line 1793073
    :cond_0
    return-void
.end method
