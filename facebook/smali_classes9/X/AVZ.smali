.class public LX/AVZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[D


# instance fields
.field private final b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

.field private final c:I

.field private final d:LX/AVX;

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679984
    const/16 v0, 0xf

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, LX/AVZ;->a:[D

    return-void

    :array_0
    .array-data 8
        -0x3ff8000000000000L    # -3.0
        -0x4000000000000000L    # -2.0
        -0x4010000000000000L    # -1.0
        -0x4020000000000000L    # -0.5
        -0x4030000000000000L    # -0.25
        -0x4046666666666666L    # -0.1
        -0x4056666666666666L    # -0.05
        0x0
        0x3fa999999999999aL    # 0.05
        0x3fb999999999999aL    # 0.1
        0x3fd0000000000000L    # 0.25
        0x3fe0000000000000L    # 0.5
        0x3ff0000000000000L    # 1.0
        0x4000000000000000L    # 2.0
        0x4008000000000000L    # 3.0
    .end array-data
.end method

.method public constructor <init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;ILX/AVX;)V
    .locals 0

    .prologue
    .line 1679979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679980
    iput-object p1, p0, LX/AVZ;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    .line 1679981
    iput p2, p0, LX/AVZ;->c:I

    .line 1679982
    iput-object p3, p0, LX/AVZ;->d:LX/AVX;

    .line 1679983
    return-void
.end method

.method private static a(LX/AVZ;D)V
    .locals 7

    .prologue
    .line 1679955
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/AVZ;->e:D

    .line 1679956
    iput-wide p1, p0, LX/AVZ;->f:D

    .line 1679957
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    iput-wide v0, p0, LX/AVZ;->g:D

    .line 1679958
    iget-object v0, p0, LX/AVZ;->d:LX/AVX;

    iget v0, v0, LX/AVX;->b:I

    int-to-double v0, v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    iget-object v4, p0, LX/AVZ;->d:LX/AVX;

    iget v4, v4, LX/AVX;->a:I

    iget-object v5, p0, LX/AVZ;->d:LX/AVX;

    iget v5, v5, LX/AVX;->b:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/AVZ;->h:D

    .line 1679959
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    add-double/2addr v0, v2

    iget-object v2, p0, LX/AVZ;->d:LX/AVX;

    iget-wide v2, v2, LX/AVX;->c:D

    mul-double/2addr v0, v2

    iput-wide v0, p0, LX/AVZ;->i:D

    .line 1679960
    return-void
.end method

.method private static b(LX/AVZ;D)D
    .locals 9

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 1679974
    iget-wide v2, p0, LX/AVZ;->e:D

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    .line 1679975
    :goto_0
    return-wide v0

    .line 1679976
    :cond_0
    iget-wide v0, p0, LX/AVZ;->e:D

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    iget-wide v2, p0, LX/AVZ;->f:D

    mul-double/2addr v0, v2

    iget v2, p0, LX/AVZ;->c:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    .line 1679977
    iget-wide v2, p0, LX/AVZ;->i:D

    mul-double/2addr v2, p1

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v4

    div-double v2, v4, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 1679978
    mul-double/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(DJ)Ljava/util/ArrayList;
    .locals 19
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DJ)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1679961
    move-object/from16 v0, p0

    iget-object v2, v0, LX/AVZ;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    invoke-virtual {v2}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getWidth()I

    move-result v2

    int-to-double v4, v2

    .line 1679962
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_2

    .line 1679963
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/AVZ;->e:D

    move-wide/from16 v0, p3

    long-to-double v6, v0

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/AVZ;->h:D

    div-double/2addr v6, v8

    add-double/2addr v2, v6

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/AVZ;->e:D

    .line 1679964
    :goto_0
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/AVZ;->e:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v6

    if-gtz v2, :cond_0

    move-object/from16 v0, p0

    iget-wide v2, v0, LX/AVZ;->f:D

    move-object/from16 v0, p0

    iget-object v6, v0, LX/AVZ;->d:LX/AVX;

    iget-wide v6, v6, LX/AVX;->d:D

    cmpg-double v2, v2, v6

    if-gez v2, :cond_1

    .line 1679965
    :cond_0
    invoke-static/range {p0 .. p2}, LX/AVZ;->a(LX/AVZ;D)V

    .line 1679966
    :cond_1
    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double v2, v4, v2

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/AVZ;->g:D

    mul-double/2addr v6, v4

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    add-double/2addr v6, v2

    .line 1679967
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1679968
    sget-object v8, LX/AVZ;->a:[D

    array-length v9, v8

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v9, :cond_3

    aget-wide v10, v8, v2

    .line 1679969
    new-instance v12, Landroid/util/Pair;

    mul-double v14, v4, v10

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    div-double v14, v14, v16

    add-double/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, LX/AVZ;->c:I

    int-to-double v14, v14

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, LX/AVZ;->b(LX/AVZ;D)D

    move-result-wide v10

    sub-double v10, v14, v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-direct {v12, v13, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1679970
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1679971
    :cond_2
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/AVZ;->e:D

    .line 1679972
    invoke-static/range {p0 .. p2}, LX/AVZ;->a(LX/AVZ;D)V

    goto :goto_0

    .line 1679973
    :cond_3
    return-object v3
.end method
