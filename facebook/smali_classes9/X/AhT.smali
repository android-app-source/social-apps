.class public final LX/AhT;
.super LX/5OG;
.source ""


# instance fields
.field public final synthetic c:LX/AhQ;

.field public d:LX/3v0;

.field private e:I


# direct methods
.method public constructor <init>(LX/AhQ;Landroid/content/Context;LX/3v0;)V
    .locals 1

    .prologue
    .line 1702995
    iput-object p1, p0, LX/AhT;->c:LX/AhQ;

    .line 1702996
    invoke-direct {p0, p2}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 1702997
    const/4 v0, -0x1

    iput v0, p0, LX/AhT;->e:I

    .line 1702998
    iput-object p3, p0, LX/AhT;->d:LX/3v0;

    .line 1702999
    invoke-direct {p0}, LX/AhT;->b()V

    .line 1703000
    return-void
.end method

.method private b(I)LX/3v3;
    .locals 2

    .prologue
    .line 1703001
    iget-object v0, p0, LX/AhT;->c:LX/AhQ;

    iget-boolean v0, v0, LX/AhQ;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AhT;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 1703002
    :goto_0
    iget v1, p0, LX/AhT;->e:I

    if-ltz v1, :cond_0

    iget v1, p0, LX/AhT;->e:I

    if-lt p1, v1, :cond_0

    .line 1703003
    add-int/lit8 p1, p1, 0x1

    .line 1703004
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    return-object v0

    .line 1703005
    :cond_1
    iget-object v0, p0, LX/AhT;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1703006
    iget-object v0, p0, LX/AhT;->c:LX/AhQ;

    iget-object v0, v0, LX/AhQ;->c:LX/3v0;

    .line 1703007
    iget-object v1, v0, LX/3v0;->x:LX/3v3;

    move-object v2, v1

    .line 1703008
    if-eqz v2, :cond_1

    .line 1703009
    iget-object v0, p0, LX/AhT;->c:LX/AhQ;

    iget-object v0, v0, LX/AhQ;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v3

    .line 1703010
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1703011
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 1703012
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 1703013
    if-ne v0, v2, :cond_0

    .line 1703014
    iput v1, p0, LX/AhT;->e:I

    .line 1703015
    :goto_1
    return-void

    .line 1703016
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1703017
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, LX/AhT;->e:I

    goto :goto_1
.end method


# virtual methods
.method public final getCount()I
    .locals 2

    .prologue
    .line 1702989
    iget-object v0, p0, LX/AhT;->c:LX/AhQ;

    iget-boolean v0, v0, LX/AhQ;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AhT;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 1702990
    :goto_0
    iget v1, p0, LX/AhT;->e:I

    if-gez v1, :cond_1

    .line 1702991
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1702992
    :goto_1
    return v0

    .line 1702993
    :cond_0
    iget-object v0, p0, LX/AhT;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 1702994
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public final synthetic getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702982
    invoke-direct {p0, p1}, LX/AhT;->b(I)LX/3v3;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1702988
    invoke-direct {p0, p1}, LX/AhT;->b(I)LX/3v3;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1702987
    int-to-long v0, p1

    return-wide v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    .prologue
    .line 1702986
    iget-object v0, p0, LX/AhT;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 1702983
    invoke-direct {p0}, LX/AhT;->b()V

    .line 1702984
    invoke-super {p0}, LX/5OG;->notifyDataSetChanged()V

    .line 1702985
    return-void
.end method
