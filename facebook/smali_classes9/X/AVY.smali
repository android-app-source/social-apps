.class public LX/AVY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public b:[D

.field public c:I

.field public d:I

.field public e:D

.field public f:D

.field private g:[LX/AVZ;

.field private h:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

.field private i:J

.field public j:Ljava/lang/Runnable;

.field public k:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/0SG;LX/AVX;Landroid/os/Handler;Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;I)V
    .locals 4
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 1679929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679930
    iput-object p1, p0, LX/AVY;->a:LX/0SG;

    .line 1679931
    iput-object p3, p0, LX/AVY;->k:Landroid/os/Handler;

    .line 1679932
    new-array v0, v3, [LX/AVZ;

    iput-object v0, p0, LX/AVY;->g:[LX/AVZ;

    .line 1679933
    const/16 v0, 0x64

    new-array v0, v0, [D

    iput-object v0, p0, LX/AVY;->b:[D

    .line 1679934
    iput-object p4, p0, LX/AVY;->h:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    .line 1679935
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 1679936
    new-instance v1, LX/AVZ;

    invoke-direct {v1, p4, p5, p2}, LX/AVZ;-><init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;ILX/AVX;)V

    .line 1679937
    iget-object v2, p0, LX/AVY;->g:[LX/AVZ;

    aput-object v1, v2, v0

    .line 1679938
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1679939
    :cond_0
    return-void
.end method

.method public static c(LX/AVY;D)V
    .locals 9

    .prologue
    .line 1679940
    iget-object v0, p0, LX/AVY;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1679941
    iget-wide v0, p0, LX/AVY;->i:J

    sub-long v4, v2, v0

    .line 1679942
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1679943
    iget-object v6, p0, LX/AVY;->g:[LX/AVZ;

    array-length v7, v6

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 1679944
    invoke-virtual {v8, p1, p2, v4, v5}, LX/AVZ;->a(DJ)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1679945
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1679946
    :cond_0
    iput-wide v2, p0, LX/AVY;->i:J

    .line 1679947
    iget-object v0, p0, LX/AVY;->h:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    .line 1679948
    iput-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    .line 1679949
    iget-object v0, p0, LX/AVY;->h:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    invoke-virtual {v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a()V

    .line 1679950
    return-void
.end method

.method public static d(D)D
    .locals 4

    .prologue
    .line 1679951
    const-wide v0, 0x3fc999999999999aL    # 0.2

    sub-double v0, p0, v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 1679952
    const-wide v2, 0x3fe999999999999aL    # 0.8

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1679953
    iget-object v0, p0, LX/AVY;->k:Landroid/os/Handler;

    iget-object v1, p0, LX/AVY;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1679954
    return-void
.end method
