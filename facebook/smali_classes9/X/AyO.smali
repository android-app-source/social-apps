.class public LX/AyO;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/AyO;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729965
    const-string v0, "souvenirs_part_1"

    const/4 v1, 0x3

    new-instance v2, LX/AyJ;

    invoke-direct {v2}, LX/AyJ;-><init>()V

    new-instance v3, LX/AyN;

    invoke-direct {v3}, LX/AyN;-><init>()V

    new-instance v4, LX/AyL;

    invoke-direct {v4}, LX/AyL;-><init>()V

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1729966
    return-void
.end method

.method public static a(LX/0QB;)LX/AyO;
    .locals 3

    .prologue
    .line 1729946
    sget-object v0, LX/AyO;->a:LX/AyO;

    if-nez v0, :cond_1

    .line 1729947
    const-class v1, LX/AyO;

    monitor-enter v1

    .line 1729948
    :try_start_0
    sget-object v0, LX/AyO;->a:LX/AyO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729949
    if-eqz v2, :cond_0

    .line 1729950
    :try_start_1
    new-instance v0, LX/AyO;

    invoke-direct {v0}, LX/AyO;-><init>()V

    .line 1729951
    move-object v0, v0

    .line 1729952
    sput-object v0, LX/AyO;->a:LX/AyO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729953
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729954
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729955
    :cond_1
    sget-object v0, LX/AyO;->a:LX/AyO;

    return-object v0

    .line 1729956
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1729958
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    if-ne p2, v1, :cond_1

    :cond_0
    const/4 v0, 0x3

    if-ne p3, v0, :cond_1

    .line 1729959
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    move-object v0, v0

    .line 1729960
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 1729961
    instance-of v1, v0, LX/AyL;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1729962
    invoke-virtual {v0, p1, p2, p3}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 1729963
    :goto_0
    return-void

    .line 1729964
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0
.end method
