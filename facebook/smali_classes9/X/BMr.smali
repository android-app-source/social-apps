.class public LX/BMr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BMq;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/BMr;


# instance fields
.field public final b:LX/3fr;

.field private final c:LX/0TD;

.field public final d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field public final e:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778032
    new-instance v0, LX/BMo;

    invoke-direct {v0}, LX/BMo;-><init>()V

    sput-object v0, LX/BMr;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3fr;LX/0TD;LX/2RQ;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778034
    iput-object p1, p0, LX/BMr;->d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1778035
    iput-object p2, p0, LX/BMr;->b:LX/3fr;

    .line 1778036
    iput-object p3, p0, LX/BMr;->c:LX/0TD;

    .line 1778037
    iput-object p4, p0, LX/BMr;->e:LX/2RQ;

    .line 1778038
    return-void
.end method

.method public static a(LX/0QB;)LX/BMr;
    .locals 7

    .prologue
    .line 1778039
    sget-object v0, LX/BMr;->f:LX/BMr;

    if-nez v0, :cond_1

    .line 1778040
    const-class v1, LX/BMr;

    monitor-enter v1

    .line 1778041
    :try_start_0
    sget-object v0, LX/BMr;->f:LX/BMr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778042
    if-eqz v2, :cond_0

    .line 1778043
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778044
    new-instance p0, LX/BMr;

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v4

    check-cast v4, LX/3fr;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v6

    check-cast v6, LX/2RQ;

    invoke-direct {p0, v3, v4, v5, v6}, LX/BMr;-><init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3fr;LX/0TD;LX/2RQ;)V

    .line 1778045
    move-object v0, p0

    .line 1778046
    sput-object v0, LX/BMr;->f:LX/BMr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778047
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778048
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778049
    :cond_1
    sget-object v0, LX/BMr;->f:LX/BMr;

    return-object v0

    .line 1778050
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778051
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1778052
    iget-object v0, p0, LX/BMr;->c:LX/0TD;

    new-instance v1, LX/BMp;

    invoke-direct {v1, p0, p1}, LX/BMp;-><init>(LX/BMr;LX/0am;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1778053
    const/4 v0, 0x1

    return v0
.end method
