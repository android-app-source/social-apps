.class public LX/C0Y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pe;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/C0D;

.field public final b:LX/23t;


# direct methods
.method public constructor <init>(LX/23t;LX/C0D;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840984
    iput-object p1, p0, LX/C0Y;->b:LX/23t;

    .line 1840985
    iput-object p2, p0, LX/C0Y;->a:LX/C0D;

    .line 1840986
    return-void
.end method

.method public static a(LX/0QB;)LX/C0Y;
    .locals 5

    .prologue
    .line 1840987
    const-class v1, LX/C0Y;

    monitor-enter v1

    .line 1840988
    :try_start_0
    sget-object v0, LX/C0Y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840989
    sput-object v2, LX/C0Y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840990
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840991
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840992
    new-instance p0, LX/C0Y;

    invoke-static {v0}, LX/23t;->a(LX/0QB;)LX/23t;

    move-result-object v3

    check-cast v3, LX/23t;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v4

    check-cast v4, LX/C0D;

    invoke-direct {p0, v3, v4}, LX/C0Y;-><init>(LX/23t;LX/C0D;)V

    .line 1840993
    move-object v0, p0

    .line 1840994
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840995
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840996
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(I)[[I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 1840998
    packed-switch p0, :pswitch_data_0

    .line 1840999
    const/4 v0, 0x5

    new-array v0, v0, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v5

    new-array v1, v2, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v2, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v2

    :goto_0
    return-object v0

    .line 1841000
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1841001
    :pswitch_1
    new-array v0, v4, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v3

    goto :goto_0

    .line 1841002
    :pswitch_2
    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    goto :goto_0

    .line 1841003
    :pswitch_3
    new-array v0, v6, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_8

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_a

    aput-object v1, v0, v5

    goto :goto_0

    .line 1841004
    :pswitch_4
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_b

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_c

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_d

    aput-object v1, v0, v5

    new-array v1, v2, [I

    fill-array-data v1, :array_e

    aput-object v1, v0, v6

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3
        0x3
        0x3
    .end array-data

    :array_2
    .array-data 4
        0x3
        0x0
        0x3
        0x2
    .end array-data

    :array_3
    .array-data 4
        0x3
        0x2
        0x3
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x3
        0x4
        0x3
        0x2
    .end array-data

    .line 1841005
    :array_5
    .array-data 4
        0x0
        0x0
        0x6
        0x6
    .end array-data

    .line 1841006
    :array_6
    .array-data 4
        0x0
        0x0
        0x6
        0x3
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x3
        0x6
        0x3
    .end array-data

    .line 1841007
    :array_8
    .array-data 4
        0x0
        0x0
        0x6
        0x3
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x3
        0x3
        0x3
    .end array-data

    :array_a
    .array-data 4
        0x3
        0x3
        0x3
        0x3
    .end array-data

    .line 1841008
    :array_b
    .array-data 4
        0x0
        0x0
        0x6
        0x4
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x4
        0x2
        0x2
    .end array-data

    :array_d
    .array-data 4
        0x2
        0x4
        0x2
        0x2
    .end array-data

    :array_e
    .array-data 4
        0x4
        0x4
        0x2
        0x2
    .end array-data

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
