.class public final enum LX/BVy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVy;

.field public static final enum ABOVE:LX/BVy;

.field public static final enum ALIGN_PARENT_BOTTOM:LX/BVy;

.field public static final enum ALIGN_PARENT_LEFT:LX/BVy;

.field public static final enum ALIGN_PARENT_RIGHT:LX/BVy;

.field public static final enum ALIGN_PARENT_TOP:LX/BVy;

.field public static final enum BELOW:LX/BVy;

.field public static final enum CENTER_HORIZONTAL:LX/BVy;

.field public static final enum CENTER_IN_PARENT:LX/BVy;

.field public static final enum CENTER_VERTICAL:LX/BVy;

.field public static final enum TO_LEFT_OF:LX/BVy;

.field public static final enum TO_RIGHT_OF:LX/BVy;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791564
    new-instance v0, LX/BVy;

    const-string v1, "BELOW"

    const-string v2, "below"

    invoke-direct {v0, v1, v4, v2}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->BELOW:LX/BVy;

    .line 1791565
    new-instance v0, LX/BVy;

    const-string v1, "TO_LEFT_OF"

    const-string v2, "toLeftOf"

    invoke-direct {v0, v1, v5, v2}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->TO_LEFT_OF:LX/BVy;

    .line 1791566
    new-instance v0, LX/BVy;

    const-string v1, "ALIGN_PARENT_RIGHT"

    const-string v2, "alignParentRight"

    invoke-direct {v0, v1, v6, v2}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->ALIGN_PARENT_RIGHT:LX/BVy;

    .line 1791567
    new-instance v0, LX/BVy;

    const-string v1, "ALIGN_PARENT_TOP"

    const-string v2, "alignParentTop"

    invoke-direct {v0, v1, v7, v2}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->ALIGN_PARENT_TOP:LX/BVy;

    .line 1791568
    new-instance v0, LX/BVy;

    const-string v1, "ALIGN_PARENT_LEFT"

    const-string v2, "alignParentLeft"

    invoke-direct {v0, v1, v8, v2}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->ALIGN_PARENT_LEFT:LX/BVy;

    .line 1791569
    new-instance v0, LX/BVy;

    const-string v1, "ALIGN_PARENT_BOTTOM"

    const/4 v2, 0x5

    const-string v3, "alignParentBottom"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->ALIGN_PARENT_BOTTOM:LX/BVy;

    .line 1791570
    new-instance v0, LX/BVy;

    const-string v1, "TO_RIGHT_OF"

    const/4 v2, 0x6

    const-string v3, "toRightOf"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->TO_RIGHT_OF:LX/BVy;

    .line 1791571
    new-instance v0, LX/BVy;

    const-string v1, "CENTER_VERTICAL"

    const/4 v2, 0x7

    const-string v3, "centerVertical"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->CENTER_VERTICAL:LX/BVy;

    .line 1791572
    new-instance v0, LX/BVy;

    const-string v1, "CENTER_HORIZONTAL"

    const/16 v2, 0x8

    const-string v3, "centerHorizontal"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->CENTER_HORIZONTAL:LX/BVy;

    .line 1791573
    new-instance v0, LX/BVy;

    const-string v1, "ABOVE"

    const/16 v2, 0x9

    const-string v3, "above"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->ABOVE:LX/BVy;

    .line 1791574
    new-instance v0, LX/BVy;

    const-string v1, "CENTER_IN_PARENT"

    const/16 v2, 0xa

    const-string v3, "centerInParent"

    invoke-direct {v0, v1, v2, v3}, LX/BVy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVy;->CENTER_IN_PARENT:LX/BVy;

    .line 1791575
    const/16 v0, 0xb

    new-array v0, v0, [LX/BVy;

    sget-object v1, LX/BVy;->BELOW:LX/BVy;

    aput-object v1, v0, v4

    sget-object v1, LX/BVy;->TO_LEFT_OF:LX/BVy;

    aput-object v1, v0, v5

    sget-object v1, LX/BVy;->ALIGN_PARENT_RIGHT:LX/BVy;

    aput-object v1, v0, v6

    sget-object v1, LX/BVy;->ALIGN_PARENT_TOP:LX/BVy;

    aput-object v1, v0, v7

    sget-object v1, LX/BVy;->ALIGN_PARENT_LEFT:LX/BVy;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVy;->ALIGN_PARENT_BOTTOM:LX/BVy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVy;->TO_RIGHT_OF:LX/BVy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BVy;->CENTER_VERTICAL:LX/BVy;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BVy;->CENTER_HORIZONTAL:LX/BVy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BVy;->ABOVE:LX/BVy;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BVy;->CENTER_IN_PARENT:LX/BVy;

    aput-object v2, v0, v1

    sput-object v0, LX/BVy;->$VALUES:[LX/BVy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791555
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791556
    iput-object p3, p0, LX/BVy;->mValue:Ljava/lang/String;

    .line 1791557
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVy;
    .locals 3

    .prologue
    .line 1791558
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, LX/BVy;->values()[LX/BVy;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1791559
    invoke-static {}, LX/BVy;->values()[LX/BVy;

    move-result-object v1

    aget-object v1, v1, v0

    .line 1791560
    iget-object v2, v1, LX/BVy;->mValue:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1791561
    return-object v1

    .line 1791562
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791563
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown relative layout param = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVy;
    .locals 1

    .prologue
    .line 1791553
    const-class v0, LX/BVy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVy;

    return-object v0
.end method

.method public static values()[LX/BVy;
    .locals 1

    .prologue
    .line 1791554
    sget-object v0, LX/BVy;->$VALUES:[LX/BVy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVy;

    return-object v0
.end method
