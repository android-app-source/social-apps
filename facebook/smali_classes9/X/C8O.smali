.class public LX/C8O;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/view/ViewGroup;

.field public d:Landroid/view/ViewGroup;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/view/View;

.field public g:LX/C8D;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1852713
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1852714
    const v0, 0x7f030c03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1852715
    const v0, 0x7f0d110e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/C8O;->c:Landroid/view/ViewGroup;

    .line 1852716
    const v0, 0x7f0d1112

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/C8O;->a:Landroid/view/View;

    .line 1852717
    const v0, 0x7f0d1dc0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C8O;->b:Landroid/widget/TextView;

    .line 1852718
    const v0, 0x7f0d1dbf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    .line 1852719
    const v0, 0x7f0d1dbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/C8O;->f:Landroid/view/View;

    .line 1852720
    const v0, 0x7f0d1dc1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/C8O;->e:Landroid/view/ViewGroup;

    .line 1852721
    iget-object v0, p0, LX/C8O;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1852722
    new-instance v0, LX/C8D;

    invoke-direct {v0}, LX/C8D;-><init>()V

    iput-object v0, p0, LX/C8O;->g:LX/C8D;

    .line 1852723
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1852669
    iget-object v0, p0, LX/C8O;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1852670
    iget-object v0, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852671
    return-void
.end method

.method public final a(Landroid/view/animation/Animation$AnimationListener;)V
    .locals 2

    .prologue
    .line 1852711
    iget-object v0, p0, LX/C8O;->g:LX/C8D;

    iget-object v1, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p1}, LX/C8D;->a(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    .line 1852712
    return-void
.end method

.method public final b(II)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1852692
    iget-object v0, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852693
    iget-object v0, p0, LX/C8O;->a:Landroid/view/View;

    .line 1852694
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1852695
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iput p1, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1852696
    new-instance v5, LX/AhY;

    invoke-direct {v5, v0, p1, p2}, LX/AhY;-><init>(Landroid/view/View;II)V

    .line 1852697
    const-wide/16 v7, 0xc8

    invoke-virtual {v5, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1852698
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1852699
    move-object v0, v5

    .line 1852700
    invoke-virtual {p0, v0}, LX/C8O;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1852701
    const/4 v1, 0x0

    invoke-static {v1}, LX/C8D;->a(Landroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1852702
    iget-object v2, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1852703
    iget-object v2, p0, LX/C8O;->g:LX/C8D;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/animation/Animation;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v1, 0x0

    .line 1852704
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, v2, LX/C8D;->a:Landroid/view/animation/Animation;

    .line 1852705
    iget-object v0, v2, LX/C8D;->a:Landroid/view/animation/Animation;

    check-cast v0, Landroid/view/animation/AnimationSet;

    .line 1852706
    array-length v4, v3

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 1852707
    invoke-virtual {v0, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1852708
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1852709
    :cond_0
    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->start()V

    .line 1852710
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1852689
    iget-object v0, p0, LX/C8O;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1852690
    iget-object v0, p0, LX/C8O;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1852691
    return-void
.end method

.method public final g()I
    .locals 3

    .prologue
    .line 1852687
    iget-object v0, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->measure(II)V

    .line 1852688
    iget-object v0, p0, LX/C8O;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1852685
    iget-object v0, p0, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1852686
    return-void
.end method

.method public setContentVisibility(I)V
    .locals 1

    .prologue
    .line 1852683
    iget-object v0, p0, LX/C8O;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852684
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 1852676
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setEnabled(Z)V

    .line 1852677
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1852678
    iget-object v0, p0, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/C8C;

    .line 1852679
    invoke-virtual {v0, p1}, LX/C8C;->setEnabled(Z)V

    .line 1852680
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1852681
    :cond_0
    iget-object v0, p0, LX/C8O;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1852682
    return-void
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 1852672
    iget-object v0, p0, LX/C8O;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1852673
    iget-object v0, p0, LX/C8O;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1852674
    iget-object v0, p0, LX/C8O;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setMinimumHeight(I)V

    .line 1852675
    return-void
.end method
