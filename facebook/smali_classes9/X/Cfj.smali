.class public final LX/Cfj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Cfk;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;


# direct methods
.method public constructor <init>(LX/Cfk;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V
    .locals 0

    .prologue
    .line 1926713
    iput-object p1, p0, LX/Cfj;->a:LX/Cfk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926714
    iput-object p2, p0, LX/Cfj;->b:Ljava/lang/String;

    .line 1926715
    iput-object p3, p0, LX/Cfj;->c:Ljava/lang/String;

    .line 1926716
    iput-object p4, p0, LX/Cfj;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 1926717
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0xb1fa249

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1926718
    iget-object v1, p0, LX/Cfj;->a:LX/Cfk;

    iget-object v2, p0, LX/Cfj;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v1, v2, p1}, LX/Cfk;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;

    move-result-object v1

    .line 1926719
    if-eqz v1, :cond_0

    .line 1926720
    iget-object v2, p0, LX/Cfj;->a:LX/Cfk;

    iget-object v3, p0, LX/Cfj;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Cfj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 1926721
    iget-object v2, p0, LX/Cfj;->a:LX/Cfk;

    iget-object v3, p0, LX/Cfj;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, p1}, LX/Cfk;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 1926722
    :cond_0
    const v1, 0x43561a81

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
