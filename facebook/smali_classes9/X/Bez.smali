.class public LX/Bez;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/TimeZone;


# instance fields
.field private final b:Ljava/util/Locale;

.field private final c:LX/11S;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1805676
    const-string v0, "GMT-8"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, LX/Bez;->a:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805621
    iput-object p1, p0, LX/Bez;->b:Ljava/util/Locale;

    .line 1805622
    iput-object p2, p0, LX/Bez;->c:LX/11S;

    .line 1805623
    return-void
.end method

.method public static a(ILX/97c;)LX/Bex;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1805645
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1805646
    packed-switch p0, :pswitch_data_0

    .line 1805647
    :cond_0
    new-instance v0, LX/Bex;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Bex;-><init>(LX/0Px;)V

    return-object v0

    .line 1805648
    :pswitch_0
    invoke-interface {p1}, LX/97c;->gt_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805649
    invoke-interface {p1}, LX/97c;->gt_()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$MonModel;

    .line 1805650
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$MonModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$MonModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805651
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1805652
    :pswitch_1
    invoke-interface {p1}, LX/97c;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805653
    invoke-interface {p1}, LX/97c;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$TueModel;

    .line 1805654
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$TueModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$TueModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805655
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1805656
    :pswitch_2
    invoke-interface {p1}, LX/97c;->p()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805657
    invoke-interface {p1}, LX/97c;->p()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$WedModel;

    .line 1805658
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$WedModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$WedModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805659
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1805660
    :pswitch_3
    invoke-interface {p1}, LX/97c;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805661
    invoke-interface {p1}, LX/97c;->n()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$ThuModel;

    .line 1805662
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$ThuModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$ThuModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805663
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1805664
    :pswitch_4
    invoke-interface {p1}, LX/97c;->e()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805665
    invoke-interface {p1}, LX/97c;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$FriModel;

    .line 1805666
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$FriModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$FriModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805667
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1805668
    :pswitch_5
    invoke-interface {p1}, LX/97c;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805669
    invoke-interface {p1}, LX/97c;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_5
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;

    .line 1805670
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SatModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805671
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1805672
    :pswitch_6
    invoke-interface {p1}, LX/97c;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1805673
    invoke-interface {p1}, LX/97c;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;

    .line 1805674
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->b()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedHoursModel$SunModel;->a()J

    move-result-wide v8

    invoke-static {v2, v6, v7, v8, v9}, LX/Bez;->a(LX/0Pz;JJ)V

    .line 1805675
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/0Pz;JJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1805642
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    cmp-long v0, p3, v2

    if-lez v0, :cond_0

    .line 1805643
    new-instance v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;-><init>(JJ)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1805644
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/Bez;
    .locals 3

    .prologue
    .line 1805640
    new-instance v2, LX/Bez;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-direct {v2, v0, v1}, LX/Bez;-><init>(Ljava/util/Locale;LX/11S;)V

    .line 1805641
    return-object v2
.end method

.method private d(J)Ljava/util/Calendar;
    .locals 5

    .prologue
    .line 1805637
    sget-object v0, LX/Bez;->a:Ljava/util/TimeZone;

    iget-object v1, p0, LX/Bez;->b:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 1805638
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1805639
    return-object v0
.end method


# virtual methods
.method public final a(III)J
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1805631
    sget-object v0, LX/Bez;->a:Ljava/util/TimeZone;

    iget-object v1, p0, LX/Bez;->b:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 1805632
    const/4 v1, 0x5

    if-lt p1, v1, :cond_0

    add-int/lit8 v3, p1, -0x4

    .line 1805633
    :goto_0
    const/16 v1, 0x7b2

    move v4, p2

    move v5, p3

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 1805634
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1805635
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0

    .line 1805636
    :cond_0
    add-int/lit8 v3, p1, 0x3

    goto :goto_0
.end method

.method public final a(J)Ljava/lang/String;
    .locals 11

    .prologue
    .line 1805626
    iget-object v0, p0, LX/Bez;->c:LX/11S;

    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    const-wide/16 v9, 0x0

    .line 1805627
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v7, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v7

    .line 1805628
    sget-object v8, LX/Bez;->a:Ljava/util/TimeZone;

    invoke-virtual {v8, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v8

    .line 1805629
    sub-int v7, v8, v7

    move v4, v7

    .line 1805630
    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)I
    .locals 3

    .prologue
    .line 1805625
    invoke-direct {p0, p1, p2}, LX/Bez;->d(J)Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public final c(J)I
    .locals 3

    .prologue
    .line 1805624
    invoke-direct {p0, p1, p2}, LX/Bez;->d(J)Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method
