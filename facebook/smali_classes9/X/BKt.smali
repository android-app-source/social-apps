.class public final LX/BKt;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5L2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/93Q;

.field private b:Ljava/lang/String;

.field public c:LX/93t;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/BKE;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/privacy/PlatformComposerPrivacyController$ComposerPrivacyCallback;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/93S;

.field public final h:LX/93n;

.field public final i:LX/93y;

.field public final j:LX/93d;

.field public final k:LX/93j;

.field public final l:LX/941;

.field public final m:LX/93p;

.field public final n:LX/93Z;

.field public final o:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1774660
    sget-object v0, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/BKt;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/BKE;LX/BKG;LX/93S;LX/93n;LX/93y;LX/93d;LX/93j;LX/941;LX/93p;LX/93Z;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # LX/BKE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BKG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774662
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BKt;->e:Ljava/lang/ref/WeakReference;

    .line 1774663
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BKt;->f:Ljava/lang/ref/WeakReference;

    .line 1774664
    iput-object p3, p0, LX/BKt;->g:LX/93S;

    .line 1774665
    iput-object p4, p0, LX/BKt;->h:LX/93n;

    .line 1774666
    iput-object p5, p0, LX/BKt;->i:LX/93y;

    .line 1774667
    iput-object p6, p0, LX/BKt;->j:LX/93d;

    .line 1774668
    iput-object p7, p0, LX/BKt;->k:LX/93j;

    .line 1774669
    iput-object p8, p0, LX/BKt;->l:LX/941;

    .line 1774670
    iput-object p9, p0, LX/BKt;->m:LX/93p;

    .line 1774671
    iput-object p10, p0, LX/BKt;->n:LX/93Z;

    .line 1774672
    iput-object p11, p0, LX/BKt;->o:Landroid/content/res/Resources;

    .line 1774673
    invoke-direct {p0}, LX/BKt;->c()V

    .line 1774674
    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    .line 1774630
    iget-object v4, p0, LX/BKt;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BKE;

    .line 1774631
    iget-object v5, p0, LX/BKt;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BKG;

    .line 1774632
    invoke-virtual {v4}, LX/BKE;->h()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    .line 1774633
    invoke-virtual {v4}, LX/BKE;->f()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    .line 1774634
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1774635
    invoke-virtual {v4}, LX/BKE;->e()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v8, LX/2rw;->GROUP:LX/2rw;

    if-eq v6, v8, :cond_4

    .line 1774636
    iget-object v6, p0, LX/BKt;->g:LX/93S;

    invoke-virtual {v4}, LX/BKE;->e()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    invoke-virtual {v6, v5, v4}, LX/93S;->a(LX/93q;Lcom/facebook/graphql/model/GraphQLAlbum;)LX/93R;

    move-result-object v4

    .line 1774637
    :goto_0
    move-object v1, v4

    .line 1774638
    iget-object v0, p0, LX/BKt;->a:LX/93Q;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1774639
    :goto_1
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/BKt;->b:Ljava/lang/String;

    invoke-virtual {v1}, LX/93Q;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1774640
    :cond_0
    if-eqz v0, :cond_1

    .line 1774641
    iget-object v2, p0, LX/BKt;->a:LX/93Q;

    invoke-virtual {v2}, LX/93Q;->e()V

    .line 1774642
    :cond_1
    iput-object v1, p0, LX/BKt;->a:LX/93Q;

    .line 1774643
    iget-object v1, p0, LX/BKt;->a:LX/93Q;

    invoke-virtual {v1}, LX/93Q;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/BKt;->b:Ljava/lang/String;

    .line 1774644
    if-eqz v0, :cond_2

    .line 1774645
    iget-object v0, p0, LX/BKt;->a:LX/93Q;

    invoke-virtual {v0}, LX/93Q;->a()V

    .line 1774646
    :cond_2
    return-void

    .line 1774647
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1774648
    :cond_4
    invoke-virtual {v4}, LX/BKE;->i()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v6

    invoke-static {v6}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1774649
    iget-object v4, p0, LX/BKt;->h:LX/93n;

    iget-object v6, p0, LX/BKt;->o:Landroid/content/res/Resources;

    const v8, 0x7f08245b

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, LX/BKt;->o:Landroid/content/res/Resources;

    const v9, 0x7f082461

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v7, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v7, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/93n;->a(LX/93q;Ljava/lang/String;Ljava/lang/String;)LX/93m;

    move-result-object v4

    goto :goto_0

    .line 1774650
    :cond_5
    sget-object v6, LX/BKs;->a:[I

    iget-object v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v8}, LX/2rw;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 1774651
    iget-object v4, p0, LX/BKt;->n:LX/93Z;

    invoke-virtual {v4, v5}, LX/93Z;->a(LX/93q;)LX/93Y;

    move-result-object v4

    goto :goto_0

    .line 1774652
    :pswitch_0
    iget-object v6, p0, LX/BKt;->c:LX/93t;

    if-eqz v6, :cond_6

    .line 1774653
    iget-object v5, p0, LX/BKt;->c:LX/93t;

    invoke-virtual {v4}, LX/BKE;->g()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/93s;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 1774654
    iget-object v4, p0, LX/BKt;->c:LX/93t;

    goto :goto_0

    .line 1774655
    :cond_6
    iget-object v6, p0, LX/BKt;->i:LX/93y;

    invoke-virtual {v4}, LX/BKE;->g()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    invoke-virtual {v6, v5, v5, v7, v4}, LX/93y;->a(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/Object;)LX/93t;

    move-result-object v4

    iput-object v4, p0, LX/BKt;->c:LX/93t;

    goto/16 :goto_0

    .line 1774656
    :pswitch_1
    iget-object v4, p0, LX/BKt;->j:LX/93d;

    iget-wide v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v7, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v4, v5, v6, v8, v7}, LX/93d;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/93c;

    move-result-object v4

    goto/16 :goto_0

    .line 1774657
    :pswitch_2
    iget-object v4, p0, LX/BKt;->k:LX/93j;

    iget-wide v6, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/93j;->a(LX/93q;Ljava/lang/Long;)LX/93i;

    move-result-object v4

    goto/16 :goto_0

    .line 1774658
    :pswitch_3
    iget-object v4, p0, LX/BKt;->l:LX/941;

    iget-wide v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v8, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v7, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v4, v5, v6, v8, v7}, LX/941;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/940;

    move-result-object v4

    goto/16 :goto_0

    .line 1774659
    :pswitch_4
    iget-object v4, p0, LX/BKt;->m:LX/93p;

    iget-object v6, v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/93p;->a(LX/93q;Ljava/lang/String;)LX/93o;

    move-result-object v4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 1774627
    sget-object v0, LX/BKt;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1774628
    invoke-direct {p0}, LX/BKt;->c()V

    .line 1774629
    :cond_0
    return-void
.end method
