.class public final enum LX/AUl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AUl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AUl;

.field public static final enum BOOLEAN:LX/AUl;

.field public static final enum BYTES:LX/AUl;

.field public static final enum DOUBLE:LX/AUl;

.field public static final enum FLOAT:LX/AUl;

.field public static final enum INT:LX/AUl;

.field public static final enum LONG:LX/AUl;

.field public static final enum STRING:LX/AUl;


# instance fields
.field private final mCursorGetter:Ljava/lang/String;

.field private final mObjectTypeString:Ljava/lang/String;

.field private final mSqlType:LX/AUk;

.field private final mSuffixHack:Ljava/lang/String;

.field private final mTypeString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1678358
    new-instance v0, LX/AUl;

    const-string v1, "INT"

    sget-object v3, LX/AUk;->INTEGER:LX/AUk;

    const-string v4, "int"

    const-string v5, "Integer"

    const-string v6, "getInt"

    invoke-direct/range {v0 .. v6}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/AUl;->INT:LX/AUl;

    .line 1678359
    new-instance v3, LX/AUl;

    const-string v4, "LONG"

    sget-object v6, LX/AUk;->INTEGER:LX/AUk;

    const-string v7, "long"

    const-string v8, "Long"

    const-string v9, "getLong"

    move v5, v11

    invoke-direct/range {v3 .. v9}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->LONG:LX/AUl;

    .line 1678360
    new-instance v3, LX/AUl;

    const-string v4, "STRING"

    sget-object v6, LX/AUk;->TEXT:LX/AUk;

    const-string v7, "String"

    const-string v8, "String"

    const-string v9, "getString"

    move v5, v12

    invoke-direct/range {v3 .. v9}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->STRING:LX/AUl;

    .line 1678361
    new-instance v3, LX/AUl;

    const-string v4, "BYTES"

    sget-object v6, LX/AUk;->BLOB:LX/AUk;

    const-string v7, "byte[]"

    const-string v8, "byte[]"

    const-string v9, "getBlob"

    move v5, v13

    invoke-direct/range {v3 .. v9}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->BYTES:LX/AUl;

    .line 1678362
    new-instance v3, LX/AUl;

    const-string v4, "FLOAT"

    sget-object v6, LX/AUk;->REAL:LX/AUk;

    const-string v7, "float"

    const-string v8, "Float"

    const-string v9, "getFloat"

    move v5, v14

    invoke-direct/range {v3 .. v9}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->FLOAT:LX/AUl;

    .line 1678363
    new-instance v3, LX/AUl;

    const-string v4, "DOUBLE"

    const/4 v5, 0x5

    sget-object v6, LX/AUk;->REAL:LX/AUk;

    const-string v7, "double"

    const-string v8, "Double"

    const-string v9, "getDouble"

    invoke-direct/range {v3 .. v9}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->DOUBLE:LX/AUl;

    .line 1678364
    new-instance v3, LX/AUl;

    const-string v4, "BOOLEAN"

    const/4 v5, 0x6

    sget-object v6, LX/AUk;->INTEGER:LX/AUk;

    const-string v7, "boolean"

    const-string v8, "Boolean"

    const-string v9, "getInt"

    const-string v10, "!= 0"

    invoke-direct/range {v3 .. v10}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/AUl;->BOOLEAN:LX/AUl;

    .line 1678365
    const/4 v0, 0x7

    new-array v0, v0, [LX/AUl;

    sget-object v1, LX/AUl;->INT:LX/AUl;

    aput-object v1, v0, v2

    sget-object v1, LX/AUl;->LONG:LX/AUl;

    aput-object v1, v0, v11

    sget-object v1, LX/AUl;->STRING:LX/AUl;

    aput-object v1, v0, v12

    sget-object v1, LX/AUl;->BYTES:LX/AUl;

    aput-object v1, v0, v13

    sget-object v1, LX/AUl;->FLOAT:LX/AUl;

    aput-object v1, v0, v14

    const/4 v1, 0x5

    sget-object v2, LX/AUl;->DOUBLE:LX/AUl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AUl;->BOOLEAN:LX/AUl;

    aput-object v2, v0, v1

    sput-object v0, LX/AUl;->$VALUES:[LX/AUl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUk;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1678356
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/AUl;-><init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1678357
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/AUk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUk;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1678349
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1678350
    iput-object p3, p0, LX/AUl;->mSqlType:LX/AUk;

    .line 1678351
    iput-object p4, p0, LX/AUl;->mTypeString:Ljava/lang/String;

    .line 1678352
    iput-object p5, p0, LX/AUl;->mObjectTypeString:Ljava/lang/String;

    .line 1678353
    iput-object p6, p0, LX/AUl;->mCursorGetter:Ljava/lang/String;

    .line 1678354
    iput-object p7, p0, LX/AUl;->mSuffixHack:Ljava/lang/String;

    .line 1678355
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AUl;
    .locals 1

    .prologue
    .line 1678348
    const-class v0, LX/AUl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AUl;

    return-object v0
.end method

.method public static values()[LX/AUl;
    .locals 1

    .prologue
    .line 1678347
    sget-object v0, LX/AUl;->$VALUES:[LX/AUl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AUl;

    return-object v0
.end method


# virtual methods
.method public final getCursorGetter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1678346
    iget-object v0, p0, LX/AUl;->mCursorGetter:Ljava/lang/String;

    return-object v0
.end method

.method public final getObjectTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1678345
    iget-object v0, p0, LX/AUl;->mObjectTypeString:Ljava/lang/String;

    return-object v0
.end method

.method public final getSqlType()LX/AUk;
    .locals 1

    .prologue
    .line 1678342
    iget-object v0, p0, LX/AUl;->mSqlType:LX/AUk;

    return-object v0
.end method

.method public final getSuffixHack()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1678344
    iget-object v0, p0, LX/AUl;->mSuffixHack:Ljava/lang/String;

    return-object v0
.end method

.method public final getTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1678343
    iget-object v0, p0, LX/AUl;->mTypeString:Ljava/lang/String;

    return-object v0
.end method
