.class public LX/CLn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/CLn;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/CLn;
    .locals 3

    .prologue
    .line 1879499
    sget-object v0, LX/CLn;->a:LX/CLn;

    if-nez v0, :cond_1

    .line 1879500
    const-class v1, LX/CLn;

    monitor-enter v1

    .line 1879501
    :try_start_0
    sget-object v0, LX/CLn;->a:LX/CLn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1879502
    if-eqz v2, :cond_0

    .line 1879503
    :try_start_1
    new-instance v0, LX/CLn;

    invoke-direct {v0}, LX/CLn;-><init>()V

    .line 1879504
    move-object v0, v0

    .line 1879505
    sput-object v0, LX/CLn;->a:LX/CLn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1879506
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1879507
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1879508
    :cond_1
    sget-object v0, LX/CLn;->a:LX/CLn;

    return-object v0

    .line 1879509
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1879510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/List;LX/CLr;)LX/CLr;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/CLr;",
            ")",
            "LX/CLr;"
        }
    .end annotation

    .prologue
    .line 1879511
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v4

    .line 1879512
    invoke-virtual {v4, p0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 1879513
    iget-object v0, p1, LX/CLr;->messageIds:Ljava/util/List;

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 1879514
    new-instance v0, LX/CLr;

    iget-object v1, p1, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    iget-object v2, p1, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/CLr;->threadFbid:Ljava/lang/Long;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    iget-object v5, p1, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    iget-object v6, p1, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v6}, LX/CLr;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(LX/CLr;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4

    .prologue
    .line 1879515
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1879516
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 1879517
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method
