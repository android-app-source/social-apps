.class public final LX/By6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/By7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/By7;


# direct methods
.method public constructor <init>(LX/By7;)V
    .locals 1

    .prologue
    .line 1836741
    iput-object p1, p0, LX/By6;->e:LX/By7;

    .line 1836742
    move-object v0, p1

    .line 1836743
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1836744
    return-void
.end method

.method public synthetic constructor <init>(LX/By7;B)V
    .locals 0

    .prologue
    .line 1836765
    invoke-direct {p0, p1}, LX/By6;-><init>(LX/By7;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1836764
    const-string v0, "EventAttachmentFooterViewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1836745
    if-ne p0, p1, :cond_1

    .line 1836746
    :cond_0
    :goto_0
    return v0

    .line 1836747
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1836748
    goto :goto_0

    .line 1836749
    :cond_3
    check-cast p1, LX/By6;

    .line 1836750
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1836751
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1836752
    if-eq v2, v3, :cond_0

    .line 1836753
    iget-object v2, p0, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1836754
    goto :goto_0

    .line 1836755
    :cond_5
    iget-object v2, p1, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1836756
    :cond_6
    iget v2, p0, LX/By6;->b:I

    iget v3, p1, LX/By6;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1836757
    goto :goto_0

    .line 1836758
    :cond_7
    iget-object v2, p0, LX/By6;->c:LX/1Pq;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/By6;->c:LX/1Pq;

    iget-object v3, p1, LX/By6;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1836759
    goto :goto_0

    .line 1836760
    :cond_9
    iget-object v2, p1, LX/By6;->c:LX/1Pq;

    if-nez v2, :cond_8

    .line 1836761
    :cond_a
    iget-object v2, p0, LX/By6;->d:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/By6;->d:Ljava/lang/String;

    iget-object v3, p1, LX/By6;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1836762
    goto :goto_0

    .line 1836763
    :cond_b
    iget-object v2, p1, LX/By6;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
