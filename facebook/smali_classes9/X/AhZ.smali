.class public LX/AhZ;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 1703245
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1703246
    iput-object p1, p0, LX/AhZ;->a:Landroid/view/View;

    .line 1703247
    iput p2, p0, LX/AhZ;->b:I

    .line 1703248
    iput p3, p0, LX/AhZ;->c:I

    .line 1703249
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 1703250
    iget v0, p0, LX/AhZ;->b:I

    iget v1, p0, LX/AhZ;->c:I

    iget v2, p0, LX/AhZ;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 1703251
    iget-object v1, p0, LX/AhZ;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1703252
    iget-object v0, p0, LX/AhZ;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1703253
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1703254
    const/4 v0, 0x1

    return v0
.end method
