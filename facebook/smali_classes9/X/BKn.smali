.class public LX/BKn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0if;

.field public final b:LX/0Uh;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0if;LX/0Uh;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774223
    iput-object p1, p0, LX/BKn;->a:LX/0if;

    .line 1774224
    iput-object p2, p0, LX/BKn;->b:LX/0Uh;

    .line 1774225
    iput-object p3, p0, LX/BKn;->c:LX/0Or;

    .line 1774226
    return-void
.end method

.method public static a(LX/0QB;)LX/BKn;
    .locals 4

    .prologue
    .line 1774271
    new-instance v2, LX/BKn;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v3, 0x12cb

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/BKn;-><init>(LX/0if;LX/0Uh;LX/0Or;)V

    .line 1774272
    move-object v0, v2

    .line 1774273
    return-object v0
.end method

.method public static b(LX/BKn;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/platform/composer/model/PlatformComposition;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1774227
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1774228
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v7, v6

    :goto_0
    if-ge v7, v10, :cond_0

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1774229
    new-instance v1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1774230
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1774231
    :cond_0
    const/4 v0, 0x0

    .line 1774232
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1774233
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    .line 1774234
    :cond_1
    if-nez v0, :cond_3

    .line 1774235
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1774236
    :goto_1
    new-instance v2, LX/BKp;

    iget-object v0, p0, LX/BKn;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1774237
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1774238
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/BKp;-><init>(J)V

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/BKp;->b(LX/0Px;)LX/BKp;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/BKp;->a(LX/0Px;)LX/BKp;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    .line 1774239
    iput-object v2, v0, LX/BKp;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774240
    move-object v0, v0

    .line 1774241
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    .line 1774242
    iput-object v2, v0, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774243
    move-object v0, v0

    .line 1774244
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    .line 1774245
    iput-object v2, v0, LX/BKp;->u:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1774246
    move-object v0, v0

    .line 1774247
    const/4 v4, 0x0

    .line 1774248
    iget-object v2, p0, LX/BKn;->b:LX/0Uh;

    const/16 v3, 0x424

    invoke-virtual {v2, v3}, LX/0Uh;->a(I)LX/03R;

    move-result-object v2

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v2, v3, :cond_2

    .line 1774249
    if-eqz v1, :cond_2

    .line 1774250
    new-instance v2, LX/88v;

    invoke-direct {v2, v1}, LX/88v;-><init>(Ljava/lang/CharSequence;)V

    .line 1774251
    invoke-virtual {v2}, LX/88v;->b()Ljava/util/List;

    move-result-object v2

    .line 1774252
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1774253
    :cond_2
    :goto_2
    move-object v1, v4

    .line 1774254
    iput-object v1, v0, LX/BKp;->x:Ljava/lang/String;

    .line 1774255
    move-object v0, v0

    .line 1774256
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1774257
    iput-object v1, v0, LX/BKp;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774258
    move-object v0, v0

    .line 1774259
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    .line 1774260
    iput-object v1, v0, LX/BKp;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1774261
    move-object v0, v0

    .line 1774262
    invoke-virtual {v0}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 1774263
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v4

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1774264
    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    .line 1774265
    const/16 v7, 0x21

    if-le v6, v7, :cond_6

    move-object v3, v2

    .line 1774266
    goto :goto_3

    :cond_5
    move-object v2, v4

    .line 1774267
    :cond_6
    if-nez v2, :cond_7

    .line 1774268
    iget-object v4, p0, LX/BKn;->a:LX/0if;

    sget-object v5, LX/0ig;->k:LX/0ih;

    const-string v6, "hashtag.disallowed_too_long"

    invoke-virtual {v4, v5, v6, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1774269
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "hashtag."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_8

    move-object v3, v2

    :cond_8
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1774270
    iget-object v4, p0, LX/BKn;->a:LX/0if;

    sget-object v5, LX/0ig;->k:LX/0ih;

    invoke-virtual {v4, v5, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    move-object v4, v2

    goto :goto_2
.end method
