.class public final LX/ATr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ATX;


# instance fields
.field public final synthetic a:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;)V
    .locals 0

    .prologue
    .line 1677046
    iput-object p1, p0, LX/ATr;->a:LX/ATs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1677047
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    invoke-virtual {v0}, LX/ATL;->b()V

    .line 1677048
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1677042
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    .line 1677043
    iput p1, v0, LX/ATs;->A:I

    .line 1677044
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    invoke-static {v0}, LX/ATs;->p(LX/ATs;)V

    .line 1677045
    return-void
.end method

.method public final a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1677025
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1677026
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    .line 1677027
    iput p2, v0, LX/ATs;->A:I

    .line 1677028
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    iget-object v1, p0, LX/ATr;->a:LX/ATs;

    iget-object v1, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATr;->a:LX/ATs;

    iget-object v2, v2, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1, p3}, LX/ATL;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;Ljava/lang/String;)V

    .line 1677029
    iget-object v0, p0, LX/ATr;->a:LX/ATs;

    .line 1677030
    iget-object v1, v0, LX/ATs;->z:LX/Azc;

    if-eqz v1, :cond_0

    .line 1677031
    iget-object v1, v0, LX/ATs;->z:LX/Azc;

    invoke-virtual {v1}, LX/Azc;->a()V

    .line 1677032
    :cond_0
    iget-object v1, v0, LX/ATs;->t:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_2

    .line 1677033
    iget-object v1, v0, LX/ATs;->t:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1677034
    iget-object v1, v0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677035
    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    if-eqz v2, :cond_1

    .line 1677036
    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    .line 1677037
    iget-object v1, v2, LX/7OL;->r:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 1677038
    iget-object v1, v2, LX/7OL;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1677039
    iget-object v1, v2, LX/7OL;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1677040
    :cond_1
    iget-object v1, v0, LX/ATs;->q:Ljava/util/concurrent/ExecutorService;

    iget-object v2, v0, LX/ATs;->u:Ljava/lang/Runnable;

    const p0, -0x4e2c95df

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, v0, LX/ATs;->t:Ljava/util/concurrent/Future;

    .line 1677041
    :cond_2
    return-void
.end method
