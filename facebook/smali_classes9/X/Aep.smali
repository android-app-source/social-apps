.class public LX/Aep;
.super LX/AeK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeK",
        "<",
        "LX/Aen;",
        ">;"
    }
.end annotation


# static fields
.field private static final l:I

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final n:LX/AaZ;

.field private final o:LX/0wM;

.field private final p:LX/Aem;

.field private final q:Lcom/facebook/fbui/glyph/GlyphView;

.field private final r:Lcom/facebook/resources/ui/FbTextView;

.field private final s:Lcom/facebook/fig/button/FigButton;

.field private final t:Landroid/graphics/drawable/GradientDrawable;

.field public u:LX/Aen;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/AeU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/Aef;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    .line 1698159
    const v0, 0x7f020999

    sput v0, LX/Aep;->l:I

    .line 1698160
    const-string v0, "bulb"

    const v1, 0x7f0207a7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "checkmark"

    const v3, 0x7f0207d6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "comment"

    const v5, 0x7f020809

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "eye"

    const v7, 0x7f020862

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "internet"

    const v9, 0x7f0208f1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "like"

    const v11, 0x7f0208fa

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static/range {v0 .. v11}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LX/Aep;->m:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/AaZ;LX/0wM;LX/Aem;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698161
    invoke-direct {p0, p1}, LX/AeK;-><init>(Landroid/view/View;)V

    .line 1698162
    iput-object p2, p0, LX/Aep;->n:LX/AaZ;

    .line 1698163
    iput-object p3, p0, LX/Aep;->o:LX/0wM;

    .line 1698164
    iput-object p4, p0, LX/Aep;->p:LX/Aem;

    .line 1698165
    const v0, 0x7f0d1938

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Aep;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1698166
    const v0, 0x7f0d1939

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Aep;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1698167
    const v0, 0x7f0d193a

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    .line 1698168
    iget-object v0, p0, LX/Aep;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, LX/Aep;->t:Landroid/graphics/drawable/GradientDrawable;

    .line 1698169
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Aeo;

    invoke-direct {v1, p0}, LX/Aeo;-><init>(LX/Aep;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698170
    return-void
.end method

.method public static x(LX/Aep;)V
    .locals 3

    .prologue
    .line 1698171
    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    if-nez v0, :cond_0

    .line 1698172
    :goto_0
    return-void

    .line 1698173
    :cond_0
    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    iget-boolean v0, v0, LX/Aen;->j:Z

    if-eqz v0, :cond_1

    .line 1698174
    iget-object v1, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    sget-object v0, LX/Aep;->m:Ljava/util/Map;

    iget-object v2, p0, LX/Aep;->u:LX/Aen;

    iget-object v2, v2, LX/Aen;->i:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 1698175
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, LX/Aep;->u:LX/Aen;

    iget-object v1, v1, LX/Aen;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1698176
    :cond_1
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1698177
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, LX/Aep;->u:LX/Aen;

    iget-object v1, v1, LX/Aen;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/AeO;LX/AeU;)V
    .locals 5

    .prologue
    .line 1698178
    check-cast p1, LX/Aen;

    .line 1698179
    iput-object p1, p0, LX/Aep;->u:LX/Aen;

    .line 1698180
    iput-object p2, p0, LX/Aep;->v:LX/AeU;

    .line 1698181
    sget-object v0, LX/Aep;->m:Ljava/util/Map;

    iget-object v1, p1, LX/Aen;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/Aep;->m:Ljava/util/Map;

    iget-object v1, p1, LX/Aen;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1698182
    :goto_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/Aen;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1698183
    :goto_1
    iget-object v2, p0, LX/Aep;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1698184
    iget-object v1, p0, LX/Aep;->t:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 1698185
    iget-object v1, p0, LX/Aep;->q:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/Aep;->o:LX/0wM;

    const/4 v3, -0x1

    invoke-virtual {v2, v0, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1698186
    iget-object v0, p0, LX/Aep;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p1, LX/Aen;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1698187
    iget-object v0, p1, LX/Aen;->a:Ljava/lang/String;

    const-string v1, "translate_comments_cta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698188
    iget-object v0, p0, LX/Aep;->n:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    iput-boolean v0, p1, LX/Aen;->j:Z

    .line 1698189
    :cond_0
    iget-object v0, p0, LX/Aep;->p:LX/Aem;

    const/4 v1, 0x0

    .line 1698190
    iget-object v2, p1, LX/Aen;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1698191
    :cond_1
    :goto_2
    move-object v0, v1

    .line 1698192
    iput-object v0, p0, LX/Aep;->w:LX/Aef;

    .line 1698193
    iget-object v0, p0, LX/Aep;->w:LX/Aef;

    if-eqz v0, :cond_4

    .line 1698194
    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    iget-object v0, v0, LX/Aen;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    iget-object v0, v0, LX/Aen;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Aep;->u:LX/Aen;

    iget-object v0, v0, LX/Aen;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Aep;->v:LX/AeU;

    if-nez v0, :cond_b

    .line 1698195
    :cond_2
    const/4 v0, 0x0

    .line 1698196
    :goto_3
    move v0, v0

    .line 1698197
    if-eqz v0, :cond_4

    .line 1698198
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1698199
    invoke-static {p0}, LX/Aep;->x(LX/Aep;)V

    .line 1698200
    :goto_4
    return-void

    .line 1698201
    :cond_3
    sget v0, LX/Aep;->l:I

    goto :goto_0

    .line 1698202
    :catch_0
    const v1, -0x6e685d

    goto :goto_1

    .line 1698203
    :cond_4
    iget-object v0, p0, LX/Aep;->s:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto :goto_4

    .line 1698204
    :cond_5
    iget-object v3, p1, LX/Aen;->f:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_6
    :goto_5
    packed-switch v2, :pswitch_data_0

    move-object v2, v1

    .line 1698205
    :goto_6
    if-eqz v2, :cond_1

    invoke-interface {v2, p2}, LX/Aef;->a(LX/AeU;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 1698206
    goto :goto_2

    .line 1698207
    :sswitch_0
    const-string v4, "page_like"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x0

    goto :goto_5

    :sswitch_1
    const-string v4, "follow"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x1

    goto :goto_5

    :sswitch_2
    const-string v4, "live_subscribe"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x2

    goto :goto_5

    :sswitch_3
    const-string v4, "translate"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x3

    goto :goto_5

    .line 1698208
    :pswitch_0
    iget-object v2, v0, LX/Aem;->h:LX/Aef;

    if-nez v2, :cond_7

    .line 1698209
    new-instance v2, LX/Aeg;

    invoke-direct {v2, v0}, LX/Aeg;-><init>(LX/Aem;)V

    iput-object v2, v0, LX/Aem;->h:LX/Aef;

    .line 1698210
    :cond_7
    iget-object v2, v0, LX/Aem;->h:LX/Aef;

    move-object v2, v2

    .line 1698211
    goto :goto_6

    .line 1698212
    :pswitch_1
    iget-object v2, v0, LX/Aem;->i:LX/Aef;

    if-nez v2, :cond_8

    .line 1698213
    new-instance v2, LX/Aeh;

    invoke-direct {v2, v0}, LX/Aeh;-><init>(LX/Aem;)V

    iput-object v2, v0, LX/Aem;->i:LX/Aef;

    .line 1698214
    :cond_8
    iget-object v2, v0, LX/Aem;->i:LX/Aef;

    move-object v2, v2

    .line 1698215
    goto :goto_6

    .line 1698216
    :pswitch_2
    iget-object v2, v0, LX/Aem;->j:LX/Aef;

    if-nez v2, :cond_9

    .line 1698217
    new-instance v2, LX/Aei;

    invoke-direct {v2, v0}, LX/Aei;-><init>(LX/Aem;)V

    iput-object v2, v0, LX/Aem;->j:LX/Aef;

    .line 1698218
    :cond_9
    iget-object v2, v0, LX/Aem;->j:LX/Aef;

    move-object v2, v2

    .line 1698219
    goto :goto_6

    .line 1698220
    :pswitch_3
    iget-object v2, v0, LX/Aem;->k:LX/Aef;

    if-nez v2, :cond_a

    .line 1698221
    new-instance v2, LX/Aej;

    invoke-direct {v2, v0}, LX/Aej;-><init>(LX/Aem;)V

    iput-object v2, v0, LX/Aem;->k:LX/Aef;

    .line 1698222
    :cond_a
    iget-object v2, v0, LX/Aem;->k:LX/Aef;

    move-object v2, v2

    .line 1698223
    goto :goto_6

    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x4ba2c44f -> :sswitch_1
        0x2d5c51b7 -> :sswitch_2
        0x34ab4747 -> :sswitch_0
        0x3ec0f14e -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
