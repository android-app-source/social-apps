.class public final LX/BXf;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 0

    .prologue
    .line 1793355
    iput-object p1, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;B)V
    .locals 0

    .prologue
    .line 1793354
    invoke-direct {p0, p1}, LX/BXf;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 14

    .prologue
    const/4 v12, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 1793336
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793337
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    .line 1793338
    iget-object v6, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v6, v6, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v7, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v6, v7}, LX/BXl;->b(LX/BXk;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v6

    div-double v4, v6, v4

    sub-double v4, v10, v4

    double-to-float v4, v4

    .line 1793339
    :goto_1
    iget-object v5, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v5, v5, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->m:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1793340
    iget-object v4, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v4, v4, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/mediareorderview/MovableImageView;

    move-wide v6, v0

    move-wide v8, v2

    .line 1793341
    invoke-static/range {v5 .. v11}, Lcom/facebook/widget/mediareorderview/MovableImageView;->b(Lcom/facebook/widget/mediareorderview/MovableImageView;DDD)V

    .line 1793342
    goto :goto_2

    :cond_1
    move v0, v12

    .line 1793343
    goto :goto_0

    .line 1793344
    :cond_2
    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v6

    div-double v4, v6, v4

    double-to-float v4, v4

    goto :goto_1

    .line 1793345
    :cond_3
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getCurrentWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1793346
    iget-object v1, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v1, v1, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->l:Landroid/view/View;

    iget-object v2, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v2, v2, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v2, v2, LX/BXg;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    const v2, 0x3dcccccd    # 0.1f

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1793347
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1793348
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793349
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1793350
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-static {v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    .line 1793351
    :goto_1
    return-void

    .line 1793352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1793353
    :cond_2
    iget-object v0, p0, LX/BXf;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-static {v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->h(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    goto :goto_1
.end method
