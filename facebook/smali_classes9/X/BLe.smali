.class public final LX/BLe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V
    .locals 0

    .prologue
    .line 1776206
    iput-object p1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x67b90ff

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776207
    :cond_0
    iget-object v1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1776208
    iget-object v1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->n:LX/BJV;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->o:LX/BJW;

    if-eqz v1, :cond_1

    .line 1776209
    iget-object v1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1776210
    if-eqz v1, :cond_1

    .line 1776211
    iget-object v2, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->n:LX/BJV;

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, LX/BJV;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1776212
    iget-object v2, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->o:LX/BJW;

    invoke-virtual {v2, v1}, LX/BJW;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1776213
    :cond_1
    iget-object v1, p0, LX/BLe;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "target_privacy_picker_donebutton"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776214
    const v1, 0x76405022

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void
.end method
