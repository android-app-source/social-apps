.class public final LX/AVb;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;)V
    .locals 0

    .prologue
    .line 1680105
    iput-object p1, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1680094
    if-nez p1, :cond_1

    .line 1680095
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AVb;->f(LX/1ca;)V

    .line 1680096
    :cond_0
    :goto_0
    return-void

    .line 1680097
    :cond_1
    iget-object v0, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v1, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v1, v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->e:LX/1FZ;

    invoke-virtual {v1, p1}, LX/1FZ;->a(Landroid/graphics/Bitmap;)LX/1FJ;

    move-result-object v1

    .line 1680098
    iput-object v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->k:LX/1FJ;

    .line 1680099
    iget-object v0, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->m:LX/1bA;

    if-eqz v0, :cond_0

    .line 1680100
    iget-object v0, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->m:LX/1bA;

    invoke-interface {v0}, LX/1bA;->o()V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1680101
    iget-object v0, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    const/4 v1, 0x1

    .line 1680102
    iput-boolean v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->n:Z

    .line 1680103
    iget-object v0, p0, LX/AVb;->a:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->f:LX/AVT;

    const-string v1, "bitmap download fail"

    invoke-virtual {v0, v1}, LX/AVT;->g(Ljava/lang/String;)V

    .line 1680104
    return-void
.end method
