.class public final LX/BzW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BzX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:LX/33B;

.field public final synthetic f:LX/BzX;


# direct methods
.method public constructor <init>(LX/BzX;)V
    .locals 1

    .prologue
    .line 1838840
    iput-object p1, p0, LX/BzW;->f:LX/BzX;

    .line 1838841
    move-object v0, p1

    .line 1838842
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1838843
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1838818
    const-string v0, "CoverPhotoShareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1838819
    if-ne p0, p1, :cond_1

    .line 1838820
    :cond_0
    :goto_0
    return v0

    .line 1838821
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1838822
    goto :goto_0

    .line 1838823
    :cond_3
    check-cast p1, LX/BzW;

    .line 1838824
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1838825
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1838826
    if-eq v2, v3, :cond_0

    .line 1838827
    iget-object v2, p0, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1838828
    goto :goto_0

    .line 1838829
    :cond_5
    iget-object v2, p1, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1838830
    :cond_6
    iget-object v2, p0, LX/BzW;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BzW;->b:LX/1Po;

    iget-object v3, p1, LX/BzW;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1838831
    goto :goto_0

    .line 1838832
    :cond_8
    iget-object v2, p1, LX/BzW;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 1838833
    :cond_9
    iget-boolean v2, p0, LX/BzW;->c:Z

    iget-boolean v3, p1, LX/BzW;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1838834
    goto :goto_0

    .line 1838835
    :cond_a
    iget-boolean v2, p0, LX/BzW;->d:Z

    iget-boolean v3, p1, LX/BzW;->d:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1838836
    goto :goto_0

    .line 1838837
    :cond_b
    iget-object v2, p0, LX/BzW;->e:LX/33B;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/BzW;->e:LX/33B;

    iget-object v3, p1, LX/BzW;->e:LX/33B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1838838
    goto :goto_0

    .line 1838839
    :cond_c
    iget-object v2, p1, LX/BzW;->e:LX/33B;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
