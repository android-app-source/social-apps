.class public LX/BYq;
.super LX/BYp;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796621
    invoke-direct {p0}, LX/BYp;-><init>()V

    return-void
.end method


# virtual methods
.method public getBackgroundInfluence()F
    .locals 1

    .prologue
    .line 1796620
    iget v0, p0, LX/BYq;->b:F

    return v0
.end method

.method public getDiffuseTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796619
    iget-object v0, p0, LX/BYq;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPaintBrightness()F
    .locals 1

    .prologue
    .line 1796618
    iget v0, p0, LX/BYq;->a:F

    return v0
.end method

.method public setBackgroundInfluence(F)V
    .locals 0

    .prologue
    .line 1796616
    iput p1, p0, LX/BYq;->b:F

    .line 1796617
    return-void
.end method

.method public setDiffuseTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796612
    iput-object p1, p0, LX/BYq;->c:Ljava/lang/String;

    .line 1796613
    return-void
.end method

.method public setPaintBrightness(F)V
    .locals 0

    .prologue
    .line 1796614
    iput p1, p0, LX/BYq;->a:F

    .line 1796615
    return-void
.end method
