.class public final LX/BMb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BMc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/BMc;


# direct methods
.method public constructor <init>(LX/BMc;)V
    .locals 1

    .prologue
    .line 1777609
    iput-object p1, p0, LX/BMb;->e:LX/BMc;

    .line 1777610
    move-object v0, p1

    .line 1777611
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1777612
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1777608
    const-string v0, "LowConfidencePromptBannerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1777588
    if-ne p0, p1, :cond_1

    .line 1777589
    :cond_0
    :goto_0
    return v0

    .line 1777590
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1777591
    goto :goto_0

    .line 1777592
    :cond_3
    check-cast p1, LX/BMb;

    .line 1777593
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1777594
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1777595
    if-eq v2, v3, :cond_0

    .line 1777596
    iget-object v2, p0, LX/BMb;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BMb;->a:Ljava/lang/String;

    iget-object v3, p1, LX/BMb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1777597
    goto :goto_0

    .line 1777598
    :cond_5
    iget-object v2, p1, LX/BMb;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1777599
    :cond_6
    iget-object v2, p0, LX/BMb;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BMb;->b:Ljava/lang/String;

    iget-object v3, p1, LX/BMb;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1777600
    goto :goto_0

    .line 1777601
    :cond_8
    iget-object v2, p1, LX/BMb;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1777602
    :cond_9
    iget-object v2, p0, LX/BMb;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/BMb;->c:Ljava/lang/String;

    iget-object v3, p1, LX/BMb;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1777603
    goto :goto_0

    .line 1777604
    :cond_b
    iget-object v2, p1, LX/BMb;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1777605
    :cond_c
    iget-object v2, p0, LX/BMb;->d:Ljava/util/List;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/BMb;->d:Ljava/util/List;

    iget-object v3, p1, LX/BMb;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1777606
    goto :goto_0

    .line 1777607
    :cond_d
    iget-object v2, p1, LX/BMb;->d:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
