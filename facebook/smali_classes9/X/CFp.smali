.class public LX/CFp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/greetingcards/verve/model/VMDeck;

.field public final b:LX/CEl;

.field public final c:LX/CEO;

.field public final d:LX/CEU;

.field public final e:LX/CGR;

.field public final f:F

.field private final g:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;>;"
        }
    .end annotation
.end field

.field public final h:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;F)V
    .locals 1

    .prologue
    .line 1864412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864413
    iput-object p1, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    .line 1864414
    iput-object p2, p0, LX/CFp;->b:LX/CEl;

    .line 1864415
    iput-object p3, p0, LX/CFp;->c:LX/CEO;

    .line 1864416
    iput-object p4, p0, LX/CFp;->d:LX/CEU;

    .line 1864417
    iput p5, p0, LX/CFp;->f:F

    .line 1864418
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CFp;->g:LX/0YU;

    .line 1864419
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CFp;->h:LX/0YU;

    .line 1864420
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CFp;->i:LX/0YU;

    .line 1864421
    new-instance v0, LX/CGR;

    invoke-direct {v0, p2}, LX/CGR;-><init>(LX/CEl;)V

    iput-object v0, p0, LX/CFp;->e:LX/CGR;

    .line 1864422
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864423
    if-nez p0, :cond_0

    .line 1864424
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1864425
    :goto_0
    return-object v0

    .line 1864426
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1864427
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864428
    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v4

    sget-object v5, LX/CFo;->GROUP:LX/CFo;

    if-ne v4, v5, :cond_1

    .line 1864429
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-static {v0}, LX/CFp;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1864430
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1864431
    :cond_1
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1864432
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/CFp;LX/0Px;LX/0Px;F)LX/0Px;
    .locals 11
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;F)",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864433
    if-nez p1, :cond_0

    .line 1864434
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1864435
    :goto_0
    return-object v0

    .line 1864436
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1864437
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_f

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864438
    invoke-static {}, Lcom/facebook/greetingcards/verve/model/VMView;->p()LX/CFn;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/CFn;->a(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;

    move-result-object v4

    .line 1864439
    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v5

    sget-object v6, LX/CFo;->GROUP:LX/CFo;

    if-eq v5, v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v5

    sget-object v6, LX/CFo;->SEQUENCE:LX/CFo;

    if-ne v5, v6, :cond_2

    .line 1864440
    :cond_1
    iget-object v5, v0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-static {p0, v5, p2, p3}, LX/CFp;->a(LX/CFp;LX/0Px;LX/0Px;F)LX/0Px;

    move-result-object v5

    .line 1864441
    iput-object v5, v4, LX/CFn;->m:LX/0Px;

    .line 1864442
    :cond_2
    invoke-virtual {v4, p3}, LX/CFn;->a(F)LX/CFn;

    .line 1864443
    iget-object v5, v0, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    const/4 v7, 0x0

    .line 1864444
    if-nez p2, :cond_10

    move-object v6, v7

    .line 1864445
    :cond_3
    :goto_2
    move-object v5, v6

    .line 1864446
    if-eqz v5, :cond_a

    .line 1864447
    iget-object v6, v4, LX/CFn;->f:Ljava/lang/String;

    sget-object v7, LX/CFo;->PLACEHOLDER:LX/CFo;

    invoke-virtual {v7}, LX/CFo;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a()LX/CFo;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 1864448
    invoke-virtual {v5}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a()LX/CFo;

    move-result-object v6

    invoke-virtual {v6}, LX/CFo;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1864449
    iput-object v6, v4, LX/CFn;->f:Ljava/lang/String;

    .line 1864450
    :cond_4
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->src:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 1864451
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->src:Ljava/lang/String;

    .line 1864452
    iput-object v6, v4, LX/CFn;->i:Ljava/lang/String;

    .line 1864453
    :cond_5
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->text:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 1864454
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->text:Ljava/lang/String;

    .line 1864455
    iput-object v6, v4, LX/CFn;->o:Ljava/lang/String;

    .line 1864456
    :cond_6
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->title:Ljava/lang/String;

    if-eqz v6, :cond_7

    .line 1864457
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->title:Ljava/lang/String;

    .line 1864458
    iput-object v6, v4, LX/CFn;->y:Ljava/lang/String;

    .line 1864459
    :cond_7
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->duration:Ljava/lang/Float;

    if-eqz v6, :cond_8

    .line 1864460
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->duration:Ljava/lang/Float;

    .line 1864461
    iput-object v6, v4, LX/CFn;->C:Ljava/lang/Float;

    .line 1864462
    :cond_8
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->subviews:LX/0Px;

    if-eqz v6, :cond_9

    .line 1864463
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->subviews:LX/0Px;

    .line 1864464
    iput-object v6, v4, LX/CFn;->m:LX/0Px;

    .line 1864465
    :cond_9
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a:Landroid/graphics/PointF;

    if-eqz v6, :cond_a

    .line 1864466
    iget-object v6, v5, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a:Landroid/graphics/PointF;

    .line 1864467
    iput-object v6, v4, LX/CFn;->D:Landroid/graphics/PointF;

    .line 1864468
    :cond_a
    iget-object v5, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMDeck;->styles:LX/0Px;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->styleName:Ljava/lang/String;

    const/4 v7, 0x0

    .line 1864469
    if-nez v0, :cond_12

    move-object v6, v7

    .line 1864470
    :cond_b
    :goto_3
    move-object v0, v6

    .line 1864471
    iget-object v5, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMDeck;->theme:Ljava/lang/String;

    .line 1864472
    if-eqz v0, :cond_c

    iget-object v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    if-nez v6, :cond_14

    .line 1864473
    :cond_c
    const/4 v6, 0x0

    .line 1864474
    :goto_4
    move-object v5, v6

    .line 1864475
    if-eqz v5, :cond_d

    .line 1864476
    invoke-virtual {v4, v5}, LX/CFn;->b(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;

    .line 1864477
    :cond_d
    if-eqz v0, :cond_e

    .line 1864478
    invoke-virtual {v4, v0}, LX/CFn;->b(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;

    .line 1864479
    :cond_e
    invoke-virtual {v4}, LX/CFn;->a()Lcom/facebook/greetingcards/verve/model/VMView;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1864480
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 1864481
    :cond_f
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1864482
    :cond_10
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v8, v6

    :goto_5
    if-ge v8, v9, :cond_11

    invoke-virtual {p2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    .line 1864483
    iget-object v10, v6, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->name:Ljava/lang/String;

    invoke-static {v5, v10}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 1864484
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_5

    :cond_11
    move-object v6, v7

    .line 1864485
    goto/16 :goto_2

    .line 1864486
    :cond_12
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v8, v6

    :goto_6
    if-ge v8, v9, :cond_13

    invoke-virtual {v5, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864487
    iget-object v10, v6, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 1864488
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_6

    :cond_13
    move-object v6, v7

    .line 1864489
    goto :goto_3

    :cond_14
    iget-object v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    invoke-virtual {v6, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/greetingcards/verve/model/VMView;

    goto :goto_4
.end method

.method private b(Lcom/facebook/greetingcards/verve/model/VMSlide;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1864490
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    if-nez v0, :cond_3

    .line 1864491
    invoke-static {p0, p1}, LX/CFp;->c(LX/CFp;Lcom/facebook/greetingcards/verve/model/VMSlide;)I

    move-result v1

    .line 1864492
    iget-object v0, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864493
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMSlide;->b()F

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMSlide;->b()F

    move-result v3

    div-float/2addr v2, v3

    .line 1864494
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMSlide;->c()F

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMSlide;->c()F

    move-result v5

    div-float/2addr v3, v5

    .line 1864495
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    move v2, v2

    .line 1864496
    iget-object v0, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    iget v3, p0, LX/CFp;->f:F

    mul-float/2addr v2, v3

    invoke-static {p0, v0, v1, v2}, LX/CFp;->a(LX/CFp;LX/0Px;LX/0Px;F)LX/0Px;

    move-result-object v0

    .line 1864497
    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    iget v2, p0, LX/CFp;->f:F

    invoke-static {p0, v1, v4, v2}, LX/CFp;->a(LX/CFp;LX/0Px;LX/0Px;F)LX/0Px;

    move-result-object v1

    .line 1864498
    const/4 v3, 0x0

    .line 1864499
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1864500
    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1864501
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 1864502
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    move v4, v3

    :goto_0
    if-ge v4, p1, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864503
    iget-object v2, v2, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1864504
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 1864505
    :cond_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v3, v4, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864506
    iget-object p1, v2, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1864507
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1864508
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1864509
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 1864510
    :goto_2
    return-object v0

    :cond_3
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    iget v1, p0, LX/CFp;->f:F

    invoke-static {p0, v0, v4, v1}, LX/CFp;->a(LX/CFp;LX/0Px;LX/0Px;F)LX/0Px;

    move-result-object v0

    goto :goto_2
.end method

.method public static c(LX/CFp;Lcom/facebook/greetingcards/verve/model/VMSlide;)I
    .locals 18

    .prologue
    .line 1864511
    const/4 v3, -0x1

    .line 1864512
    const v2, 0x7fffffff

    .line 1864513
    const/4 v1, 0x0

    move v4, v2

    move v5, v3

    move v3, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1864514
    move-object/from16 v0, p0

    iget-object v1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864515
    iget-boolean v2, v1, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1864516
    const/4 v6, 0x0

    .line 1864517
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/greetingcards/verve/model/VMSlide;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    new-array v11, v1, [Z

    .line 1864518
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/CFp;->b(I)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/CFp;->a(LX/0Px;)LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v1, 0x0

    move v10, v1

    :goto_1
    if-ge v10, v13, :cond_1

    invoke-virtual {v12, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864519
    iget-object v2, v1, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 1864520
    const/4 v7, 0x0

    .line 1864521
    const/4 v8, 0x0

    .line 1864522
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/greetingcards/verve/model/VMSlide;->a()LX/0Px;

    move-result-object v14

    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v15

    const/4 v2, 0x0

    move v9, v8

    move v8, v2

    :goto_2
    if-ge v8, v15, :cond_8

    invoke-virtual {v14, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    .line 1864523
    iget-object v0, v1, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v2, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v16

    invoke-virtual {v2}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a()LX/CFo;

    move-result-object v2

    move-object/from16 v0, v16

    if-ne v0, v2, :cond_0

    .line 1864524
    const/4 v1, 0x1

    .line 1864525
    const/4 v2, 0x1

    aput-boolean v2, v11, v9

    .line 1864526
    :goto_3
    if-nez v1, :cond_7

    .line 1864527
    add-int/lit16 v1, v6, 0x3e8

    .line 1864528
    :goto_4
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v6, v1

    goto :goto_1

    .line 1864529
    :cond_0
    add-int/lit8 v9, v9, 0x1

    .line 1864530
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_2

    .line 1864531
    :cond_1
    const/4 v1, 0x0

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/greetingcards/verve/model/VMSlide;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1864532
    aget-boolean v2, v11, v1

    if-nez v2, :cond_2

    .line 1864533
    add-int/lit8 v6, v6, 0x1

    .line 1864534
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1864535
    :cond_3
    if-ge v6, v4, :cond_6

    move v2, v6

    move v4, v3

    .line 1864536
    :goto_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v5, v4

    move v4, v2

    goto/16 :goto_0

    .line 1864537
    :cond_4
    const/4 v1, -0x1

    if-ne v5, v1, :cond_5

    .line 1864538
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No master slide with className "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1864539
    :cond_5
    return v5

    :cond_6
    move v2, v4

    move v4, v5

    goto :goto_6

    :cond_7
    move v1, v6

    goto :goto_4

    :cond_8
    move v1, v7

    goto :goto_3
.end method


# virtual methods
.method public final b(I)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864540
    iget-object v0, p0, LX/CFp;->g:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1864541
    if-eqz v0, :cond_0

    .line 1864542
    :goto_0
    return-object v0

    .line 1864543
    :cond_0
    iget-object v0, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    invoke-direct {p0, v0}, LX/CFp;->b(Lcom/facebook/greetingcards/verve/model/VMSlide;)LX/0Px;

    move-result-object v1

    .line 1864544
    iget-object v0, p0, LX/CFp;->g:LX/0YU;

    invoke-virtual {v0, p1, v1}, LX/0YU;->b(ILjava/lang/Object;)V

    .line 1864545
    iget-object v0, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864546
    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-eqz v2, :cond_2

    .line 1864547
    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864548
    :goto_1
    move-object v0, v2

    .line 1864549
    iget-object v2, p0, LX/CFp;->h:LX/0YU;

    invoke-virtual {v2, p1, v0}, LX/0YU;->b(ILjava/lang/Object;)V

    .line 1864550
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864551
    iget-object v4, p0, LX/CFp;->i:LX/0YU;

    iget v5, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v4, v5, v0}, LX/0YU;->b(ILjava/lang/Object;)V

    .line 1864552
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    .line 1864553
    goto :goto_0

    .line 1864554
    :cond_2
    iget-boolean v2, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1864555
    invoke-static {p0, v0}, LX/CFp;->c(LX/CFp;Lcom/facebook/greetingcards/verve/model/VMSlide;)I

    move-result v2

    .line 1864556
    iget-object v3, p0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object v3, v3, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMSlide;

    iget-object v2, v2, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    goto :goto_1

    .line 1864557
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
