.class public LX/Btm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Btk;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1829494
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Btm;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1829491
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1829492
    iput-object p1, p0, LX/Btm;->b:LX/0Ot;

    .line 1829493
    return-void
.end method

.method public static a(LX/0QB;)LX/Btm;
    .locals 4

    .prologue
    .line 1829480
    const-class v1, LX/Btm;

    monitor-enter v1

    .line 1829481
    :try_start_0
    sget-object v0, LX/Btm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1829482
    sput-object v2, LX/Btm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1829483
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829484
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1829485
    new-instance v3, LX/Btm;

    const/16 p0, 0x1d65

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Btm;-><init>(LX/0Ot;)V

    .line 1829486
    move-object v0, v3

    .line 1829487
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1829488
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Btm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1829489
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1829490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 1829475
    check-cast p2, LX/Btl;

    .line 1829476
    iget-object v0, p0, LX/Btm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    iget-object v1, p2, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    iget-object v3, p2, LX/Btl;->d:LX/1Pm;

    .line 1829477
    iget-object v4, v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->e:LX/3Bi;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object v5, p1

    move-object v6, v1

    move-object v7, v2

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, LX/3Bi;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;Landroid/content/Context;LX/1Pm;)Z

    .line 1829478
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-interface {v3, v4}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1829479
    return-void
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829474
    const v0, 0x69571ea

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829457
    const v0, -0x22a04801

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1829471
    check-cast p2, LX/Btl;

    .line 1829472
    iget-object v0, p0, LX/Btm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    iget-object v2, p2, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    iget-boolean v4, p2, LX/Btl;->c:Z

    iget-object v5, p2, LX/Btl;->d:LX/1Pm;

    iget-object v6, p2, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;ZLX/1Pm;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1Dg;

    move-result-object v0

    .line 1829473
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1829458
    invoke-static {}, LX/1dS;->b()V

    .line 1829459
    iget v0, p1, LX/1dQ;->b:I

    .line 1829460
    sparse-switch v0, :sswitch_data_0

    .line 1829461
    :goto_0
    return-object v2

    .line 1829462
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1829463
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 1829464
    iget-object p1, p0, LX/Btm;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    .line 1829465
    const p2, 0x7f0d0068

    invoke-virtual {v0, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1y5;

    .line 1829466
    const p0, 0x7f0d0069

    invoke-virtual {v0, p0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/Bundle;

    .line 1829467
    iget-object v1, p1, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nA;

    invoke-virtual {v1, v0, p2, p0}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 1829468
    goto :goto_0

    .line 1829469
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1829470
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Btm;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x22a04801 -> :sswitch_1
        0x69571ea -> :sswitch_0
    .end sparse-switch
.end method
