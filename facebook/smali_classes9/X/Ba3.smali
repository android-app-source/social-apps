.class public final LX/Ba3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ba5;


# direct methods
.method public constructor <init>(LX/Ba5;)V
    .locals 0

    .prologue
    .line 1798449
    iput-object p1, p0, LX/Ba3;->a:LX/Ba5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1798450
    iget-object v0, p0, LX/Ba3;->a:LX/Ba5;

    .line 1798451
    iget-object p1, v0, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v0, p1

    .line 1798452
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1798453
    iget-object v0, p0, LX/Ba3;->a:LX/Ba5;

    .line 1798454
    iget-object v1, v0, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v1, v1

    .line 1798455
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1798456
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1798457
    iget-object p0, v0, LX/Ba5;->a:LX/17T;

    invoke-virtual {p0}, LX/17T;->b()Z

    move-result p0

    .line 1798458
    iget-object p1, v0, LX/Ba5;->c:Landroid/content/pm/PackageManager;

    const-string p2, "com.amazon.venezia"

    invoke-static {p1, p2}, LX/32v;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result p1

    .line 1798459
    if-eqz p1, :cond_0

    if-nez p0, :cond_0

    .line 1798460
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "amzn://apps/android?p="

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1798461
    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1798462
    invoke-virtual {p0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1798463
    iget-object v2, v0, LX/Ba5;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1798464
    :goto_0
    return-void

    .line 1798465
    :cond_0
    iget-object p0, v0, LX/Ba5;->a:LX/17T;

    invoke-virtual {p0, v1, v2}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
