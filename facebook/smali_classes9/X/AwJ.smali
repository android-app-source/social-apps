.class public final LX/AwJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AvS;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

.field public final synthetic b:LX/AwK;


# direct methods
.method public constructor <init>(LX/AwK;Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)V
    .locals 0

    .prologue
    .line 1726355
    iput-object p1, p0, LX/AwJ;->b:LX/AwK;

    iput-object p2, p0, LX/AwJ;->a:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1726356
    iget-object v0, p0, LX/AwJ;->b:LX/AwK;

    const/4 v1, 0x0

    iput-object v1, v0, LX/AwK;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1726357
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 1726358
    iget-object v0, p0, LX/AwJ;->a:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    invoke-static {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->setAssetPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    .line 1726359
    iget-object v1, p0, LX/AwJ;->b:LX/AwK;

    iput-object v0, v1, LX/AwK;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1726360
    iget-object v0, p0, LX/AwJ;->b:LX/AwK;

    iget-boolean v0, v0, LX/AwK;->h:Z

    if-eqz v0, :cond_0

    .line 1726361
    iget-object v0, p0, LX/AwJ;->b:LX/AwK;

    invoke-static {v0}, LX/AwK;->i(LX/AwK;)V

    .line 1726362
    :cond_0
    return-void
.end method
