.class public final LX/BSI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BSK;


# direct methods
.method public constructor <init>(LX/BSK;)V
    .locals 0

    .prologue
    .line 1785327
    iput-object p1, p0, LX/BSI;->a:LX/BSK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x410d2df

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785328
    iget-object v1, p0, LX/BSI;->a:LX/BSK;

    .line 1785329
    const v3, 0x7f030169

    invoke-virtual {v1, v3}, LX/BSK;->setContentView(I)V

    .line 1785330
    const v3, 0x1020002

    invoke-virtual {v1, v3}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785331
    const v3, 0x7f0d067d

    invoke-virtual {v1, v3}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785332
    const v3, 0x7f0d0677

    invoke-virtual {v1, v3}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance p0, LX/BSJ;

    invoke-direct {p0, v1}, LX/BSJ;-><init>(LX/BSK;)V

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785333
    iget-object v3, v1, LX/BSK;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p0, LX/BSN;->b:LX/0Tn;

    sget-object p1, LX/BSN;->c:LX/0Tn;

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1785334
    const v3, 0x7f0d067a

    .line 1785335
    sget-object p0, LX/BSN;->c:LX/0Tn;

    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1785336
    const v3, 0x7f0d067a

    move p0, v3

    .line 1785337
    :goto_0
    const v3, 0x7f0d0679

    invoke-virtual {v1, v3}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    .line 1785338
    invoke-virtual {v3, p0}, Landroid/widget/RadioGroup;->check(I)V

    .line 1785339
    const-string v3, "impression"

    invoke-static {v1, v3, p1}, LX/BSK;->a(LX/BSK;Ljava/lang/String;Ljava/lang/String;)V

    .line 1785340
    const v1, -0x6c7222da

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1785341
    :cond_0
    sget-object p0, LX/BSN;->d:LX/0Tn;

    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1785342
    const v3, 0x7f0d067b

    move p0, v3

    goto :goto_0

    .line 1785343
    :cond_1
    sget-object p0, LX/BSN;->e:LX/0Tn;

    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1785344
    const v3, 0x7f0d067c

    move p0, v3

    goto :goto_0

    :cond_2
    move p0, v3

    goto :goto_0
.end method
