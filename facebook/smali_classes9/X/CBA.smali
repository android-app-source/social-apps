.class public final LX/CBA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1856185
    iput-object p1, p0, LX/CBA;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    iput-object p2, p0, LX/CBA;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    iput-object p3, p0, LX/CBA;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x5f1b5faf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1856186
    iget-object v0, p0, LX/CBA;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->j:LX/2mJ;

    iget-object v2, p0, LX/CBA;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856187
    iget-object v0, p0, LX/CBA;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13P;

    sget-object v2, LX/77m;->SECONDARY_ACTION:LX/77m;

    iget-object v3, p0, LX/CBA;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    .line 1856188
    const v0, -0x5925acbf

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
