.class public final LX/BoW;
.super LX/BoF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BoF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 1

    .prologue
    .line 1821781
    iput-object p1, p0, LX/BoW;->b:LX/1dt;

    invoke-direct {p0, p1}, LX/BoF;-><init>(LX/1dt;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 5

    .prologue
    .line 1821782
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 1821783
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;

    move-result-object v0

    const-string v1, "pyma_dont_want_to_see_menu_item"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, LX/4Zk;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, LX/4Zk;->b(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v4

    .line 1821784
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1821785
    const/4 p0, 0x0

    .line 1821786
    :goto_0
    move-object v0, p0

    .line 1821787
    return-object v0

    .line 1821788
    :cond_0
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "pyma_xout"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "pyma_category"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "option_name"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "page_id"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "pyma_additional_info"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "native_newsfeed"

    .line 1821789
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1821790
    move-object p0, p0

    .line 1821791
    goto :goto_0
.end method
