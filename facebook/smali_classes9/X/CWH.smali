.class public final enum LX/CWH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CWH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CWH;

.field public static final enum BACKWARD:LX/CWH;

.field public static final enum FORWARD:LX/CWH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1907811
    new-instance v0, LX/CWH;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v2}, LX/CWH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CWH;->FORWARD:LX/CWH;

    .line 1907812
    new-instance v0, LX/CWH;

    const-string v1, "BACKWARD"

    invoke-direct {v0, v1, v3}, LX/CWH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CWH;->BACKWARD:LX/CWH;

    .line 1907813
    const/4 v0, 0x2

    new-array v0, v0, [LX/CWH;

    sget-object v1, LX/CWH;->FORWARD:LX/CWH;

    aput-object v1, v0, v2

    sget-object v1, LX/CWH;->BACKWARD:LX/CWH;

    aput-object v1, v0, v3

    sput-object v0, LX/CWH;->$VALUES:[LX/CWH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1907815
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CWH;
    .locals 1

    .prologue
    .line 1907816
    const-class v0, LX/CWH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CWH;

    return-object v0
.end method

.method public static values()[LX/CWH;
    .locals 1

    .prologue
    .line 1907814
    sget-object v0, LX/CWH;->$VALUES:[LX/CWH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CWH;

    return-object v0
.end method
