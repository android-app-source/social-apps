.class public LX/BnA;
.super LX/3x6;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 1819930
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1819931
    iput p1, p0, LX/BnA;->a:I

    .line 1819932
    iput p2, p0, LX/BnA;->b:I

    .line 1819933
    iput p3, p0, LX/BnA;->c:I

    .line 1819934
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 3

    .prologue
    .line 1819918
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 1819919
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)I

    move-result v1

    .line 1819920
    instance-of v2, v0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    if-eqz v2, :cond_2

    .line 1819921
    iget v0, p0, LX/BnA;->b:I

    rem-int v0, v1, v0

    if-eqz v0, :cond_0

    .line 1819922
    iget v0, p0, LX/BnA;->a:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1819923
    :cond_0
    iget v0, p0, LX/BnA;->a:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 1819924
    :cond_1
    :goto_0
    return-void

    .line 1819925
    :cond_2
    instance-of v0, v0, LX/BnF;

    if-eqz v0, :cond_1

    .line 1819926
    if-nez v1, :cond_3

    .line 1819927
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    invoke-static {p2}, LX/0vv;->u(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1819928
    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 1819929
    :cond_3
    iget v0, p0, LX/BnA;->c:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method
