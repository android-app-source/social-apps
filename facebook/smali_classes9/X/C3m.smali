.class public LX/C3m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/C3b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1846225
    const v0, 0x7f0b1d6e

    sput v0, LX/C3m;->a:I

    return-void
.end method

.method public constructor <init>(LX/C3b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846227
    iput-object p1, p0, LX/C3m;->b:LX/C3b;

    .line 1846228
    return-void
.end method

.method public static a(LX/0QB;)LX/C3m;
    .locals 4

    .prologue
    .line 1846229
    const-class v1, LX/C3m;

    monitor-enter v1

    .line 1846230
    :try_start_0
    sget-object v0, LX/C3m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846231
    sput-object v2, LX/C3m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846232
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846233
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846234
    new-instance p0, LX/C3m;

    invoke-static {v0}, LX/C3b;->b(LX/0QB;)LX/C3b;

    move-result-object v3

    check-cast v3, LX/C3b;

    invoke-direct {p0, v3}, LX/C3m;-><init>(LX/C3b;)V

    .line 1846235
    move-object v0, p0

    .line 1846236
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846237
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846238
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
