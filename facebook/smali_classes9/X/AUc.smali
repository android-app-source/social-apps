.class public final LX/AUc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AU9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AU9",
        "<",
        "Lcom/facebook/crudolib/dbschema/direct/model/SchemaTable_Queries$QueryByTableDAO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1678251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678252
    iput-object p1, p0, LX/AUc;->a:Ljava/lang/String;

    .line 1678253
    return-void
.end method

.method public static b(Landroid/database/Cursor;)LX/AUb;
    .locals 2

    .prologue
    .line 1678254
    new-instance v0, LX/AUb;

    invoke-direct {v0, p0}, LX/AUb;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/database/Cursor;)LX/AU0;
    .locals 1

    .prologue
    .line 1678250
    invoke-static {p1}, LX/AUc;->b(Landroid/database/Cursor;)LX/AUb;

    move-result-object v0

    return-object v0
.end method

.method public final a()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1678249
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, LX/AUa;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final b()[Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1678248
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "sqliteproc_schema "

    aput-object v1, v0, v4

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    const-string v2, "name"

    aput-object v2, v1, v5

    const-string v2, "type_name"

    aput-object v2, v1, v6

    const-string v2, "default_value"

    aput-object v2, v1, v7

    const-string v2, "is_nullable"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "is_primary"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "is_autoincrement"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "is_deleted"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "does_affect_indices"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "auto_upgrade_policy"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "foreign_table"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "foreign_column"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    const-string v1, "table_name = ?"

    aput-object v1, v0, v6

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p0, LX/AUc;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v1, v0, v7

    const/4 v1, 0x0

    aput-object v1, v0, v8

    return-object v0
.end method
