.class public final LX/C3q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic b:LX/C3r;


# direct methods
.method public constructor <init>(LX/C3r;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 1846392
    iput-object p1, p0, LX/C3q;->b:LX/C3r;

    iput-object p2, p0, LX/C3q;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1846393
    iget-object v0, p0, LX/C3q;->b:LX/C3r;

    iget-object v0, v0, LX/C3r;->b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0813d0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1846394
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1846395
    invoke-direct {p0}, LX/C3q;->a()V

    .line 1846396
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1846397
    check-cast p1, LX/0Px;

    const/4 v3, 0x0

    .line 1846398
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1846399
    :cond_0
    invoke-direct {p0}, LX/C3q;->a()V

    .line 1846400
    :goto_0
    return-void

    .line 1846401
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    move v1, v3

    :goto_1
    if-ge v4, v5, :cond_3

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1846402
    if-nez v1, :cond_3

    .line 1846403
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v3

    :goto_2
    if-ge v2, v7, :cond_5

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1846404
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, LX/C3q;->b:LX/C3r;

    iget-object v8, v8, LX/C3r;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1846405
    const/4 v0, 0x1

    .line 1846406
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_1

    .line 1846407
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1846408
    :cond_3
    if-eqz v1, :cond_4

    .line 1846409
    iget-object v0, p0, LX/C3q;->b:LX/C3r;

    iget-object v0, v0, LX/C3r;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->N()Ljava/lang/String;

    move-result-object v0

    .line 1846410
    :goto_4
    iget-object v1, p0, LX/C3q;->b:LX/C3r;

    iget-object v1, v1, LX/C3r;->b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    iget-object v2, p0, LX/C3q;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->a$redex0(Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Landroid/app/Activity;Ljava/lang/String;LX/0Px;)V

    goto :goto_0

    .line 1846411
    :cond_4
    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_3
.end method
