.class public final LX/CU3;
.super LX/CTv;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1896221
    invoke-direct {p0, p1}, LX/CTv;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;)V

    .line 1896222
    iget-object v0, p0, LX/CTv;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x5e546ab9

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896223
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896224
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    move-result-object v0

    iput-object v0, p0, LX/CU3;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    .line 1896225
    sget-object v0, LX/CTt;->a:[I

    iget-object v2, p0, LX/CU3;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1896226
    const-string v0, "Unhandled handler type"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1896227
    :pswitch_0
    return-void

    :cond_0
    move v0, v1

    .line 1896228
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
