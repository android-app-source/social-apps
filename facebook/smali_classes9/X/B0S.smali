.class public LX/B0S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/B0L;

.field public final b:LX/B0P;

.field public c:LX/B0R;


# direct methods
.method public constructor <init>(LX/B0L;LX/B0P;)V
    .locals 1
    .param p1    # LX/B0L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733930
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0S;->c:LX/B0R;

    .line 1733931
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0L;

    iput-object v0, p0, LX/B0S;->a:LX/B0L;

    .line 1733932
    iput-object p2, p0, LX/B0S;->b:LX/B0P;

    .line 1733933
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1733934
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B0S;->c:LX/B0R;

    if-eqz v0, :cond_0

    .line 1733935
    iget-object v0, p0, LX/B0S;->c:LX/B0R;

    invoke-virtual {v0}, LX/B0R;->b()V

    .line 1733936
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0S;->c:LX/B0R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1733937
    :cond_0
    monitor-exit p0

    return-void

    .line 1733938
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/B0U;)V
    .locals 2

    .prologue
    .line 1733939
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B0S;->c:LX/B0R;

    .line 1733940
    new-instance v1, LX/B0R;

    invoke-direct {v1, p0, p1, p2}, LX/B0R;-><init>(LX/B0S;Ljava/lang/String;LX/B0U;)V

    iput-object v1, p0, LX/B0S;->c:LX/B0R;

    .line 1733941
    iget-object v1, p0, LX/B0S;->c:LX/B0R;

    .line 1733942
    iget-object p1, v1, LX/B0R;->a:LX/B0O;

    invoke-virtual {p1}, LX/B0O;->a()V

    .line 1733943
    if-eqz v0, :cond_0

    .line 1733944
    invoke-virtual {v0}, LX/B0R;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1733945
    :cond_0
    monitor-exit p0

    return-void

    .line 1733946
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
