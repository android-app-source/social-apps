.class public LX/BZC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private effect_asset_folder:Ljava/lang/String;

.field private effect_file:Ljava/lang/String;

.field private effect_version:I

.field private existingVersion:I

.field private filter_id:Ljava/lang/String;

.field private image_url:Ljava/lang/String;

.field private info_message_type:Ljava/lang/String;

.field private isFavorite:Z

.field private isNew:Z

.field private max_faces_support:I

.field private min_faces_support:I

.field private orderInFavorites:I

.field private thumbnail_uri:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796975
    return-void
.end method


# virtual methods
.method public getEffect_asset_folder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796976
    iget-object v0, p0, LX/BZC;->effect_asset_folder:Ljava/lang/String;

    return-object v0
.end method

.method public getEffect_file()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796977
    iget-object v0, p0, LX/BZC;->effect_file:Ljava/lang/String;

    return-object v0
.end method

.method public getEffect_version()I
    .locals 1

    .prologue
    .line 1796978
    iget v0, p0, LX/BZC;->effect_version:I

    return v0
.end method

.method public getExistingVersion()I
    .locals 1

    .prologue
    .line 1796962
    iget v0, p0, LX/BZC;->existingVersion:I

    return v0
.end method

.method public getFilter_id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796979
    iget-object v0, p0, LX/BZC;->filter_id:Ljava/lang/String;

    return-object v0
.end method

.method public getImage_url()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796980
    iget-object v0, p0, LX/BZC;->image_url:Ljava/lang/String;

    return-object v0
.end method

.method public getInfo_message_type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796986
    iget-object v0, p0, LX/BZC;->info_message_type:Ljava/lang/String;

    return-object v0
.end method

.method public getMax_faces_support()I
    .locals 1

    .prologue
    .line 1796981
    iget v0, p0, LX/BZC;->max_faces_support:I

    return v0
.end method

.method public getMin_faces_support()I
    .locals 1

    .prologue
    .line 1796982
    iget v0, p0, LX/BZC;->min_faces_support:I

    return v0
.end method

.method public getOrderInFavorites()I
    .locals 1

    .prologue
    .line 1796983
    iget v0, p0, LX/BZC;->orderInFavorites:I

    return v0
.end method

.method public getThumbnail_uri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796984
    iget-object v0, p0, LX/BZC;->thumbnail_uri:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796985
    iget-object v0, p0, LX/BZC;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setEffect_asset_folder(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796970
    iput-object p1, p0, LX/BZC;->effect_asset_folder:Ljava/lang/String;

    .line 1796971
    return-void
.end method

.method public setEffect_file(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796972
    iput-object p1, p0, LX/BZC;->effect_file:Ljava/lang/String;

    .line 1796973
    return-void
.end method

.method public setEffect_version(I)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonSetter;
    .end annotation

    .prologue
    .line 1796942
    iput p1, p0, LX/BZC;->effect_version:I

    .line 1796943
    iget v0, p0, LX/BZC;->existingVersion:I

    if-gtz v0, :cond_0

    .line 1796944
    iget v0, p0, LX/BZC;->effect_version:I

    iput v0, p0, LX/BZC;->existingVersion:I

    .line 1796945
    :cond_0
    return-void
.end method

.method public setExistingVersion(I)V
    .locals 0

    .prologue
    .line 1796946
    iput p1, p0, LX/BZC;->existingVersion:I

    .line 1796947
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0

    .prologue
    .line 1796948
    iput-boolean p1, p0, LX/BZC;->isFavorite:Z

    .line 1796949
    return-void
.end method

.method public setFilter_id(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796950
    iput-object p1, p0, LX/BZC;->filter_id:Ljava/lang/String;

    .line 1796951
    return-void
.end method

.method public setImage_url(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796952
    iput-object p1, p0, LX/BZC;->image_url:Ljava/lang/String;

    .line 1796953
    return-void
.end method

.method public setInfo_message_type(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796954
    iput-object p1, p0, LX/BZC;->info_message_type:Ljava/lang/String;

    .line 1796955
    return-void
.end method

.method public setMax_faces_support(I)V
    .locals 0

    .prologue
    .line 1796956
    iput p1, p0, LX/BZC;->max_faces_support:I

    .line 1796957
    return-void
.end method

.method public setMin_faces_support(I)V
    .locals 0

    .prologue
    .line 1796958
    iput p1, p0, LX/BZC;->min_faces_support:I

    .line 1796959
    return-void
.end method

.method public setNew(Z)V
    .locals 0

    .prologue
    .line 1796960
    iput-boolean p1, p0, LX/BZC;->isNew:Z

    .line 1796961
    return-void
.end method

.method public setOrderInFavorites(I)V
    .locals 0

    .prologue
    .line 1796963
    iput p1, p0, LX/BZC;->orderInFavorites:I

    .line 1796964
    return-void
.end method

.method public setThumbnail_uri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796965
    iput-object p1, p0, LX/BZC;->thumbnail_uri:Ljava/lang/String;

    .line 1796966
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796967
    iput-object p1, p0, LX/BZC;->title:Ljava/lang/String;

    .line 1796968
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 1796969
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Content{filter_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BZC;->filter_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZC;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", image_url=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZC;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", effect_file=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZC;->effect_file:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", effect_asset_folder=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZC;->effect_asset_folder:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", effect_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZC;->effect_version:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", min_faces_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZC;->min_faces_support:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max_faces_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZC;->max_faces_support:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info_message_type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZC;->info_message_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
