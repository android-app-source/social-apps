.class public final LX/BbH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800531
    iput-object p1, p0, LX/BbH;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iput-object p2, p0, LX/BbH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x1d6fb9d7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1800532
    iget-object v0, p0, LX/BbH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1800533
    const v0, -0x127d4df5

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800534
    :goto_0
    return-void

    .line 1800535
    :cond_0
    iget-object v0, p0, LX/BbH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800536
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1800537
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1800538
    iget-object v2, p0, LX/BbH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1800539
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/BbH;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iget-object v4, v4, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->b:Landroid/content/Context;

    const-class v5, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1800540
    const-string v4, "post_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1800541
    const-string v2, "placelist_id"

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1800542
    iget-object v0, p0, LX/BbH;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iget-object v2, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    const v4, 0xc365

    iget-object v0, p0, LX/BbH;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1800543
    const v0, -0x77f856c4

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
