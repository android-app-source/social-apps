.class public final LX/BKd;
.super LX/4i0;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BKd;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/platform/perflogging/PlatformPerformanceLoggingCommonTags;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774056
    const v0, 0x7b0004

    const-string v1, "PlatformComposerTTI"

    const-class v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.facebook.composer.shareintent.ImplicitShareIntentHandler"

    invoke-static {v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p1}, LX/4i0;-><init>(ILjava/lang/String;LX/0Rf;Ljava/lang/String;)V

    .line 1774057
    return-void
.end method

.method public static a(LX/0QB;)LX/BKd;
    .locals 4

    .prologue
    .line 1774058
    sget-object v0, LX/BKd;->a:LX/BKd;

    if-nez v0, :cond_1

    .line 1774059
    const-class v1, LX/BKd;

    monitor-enter v1

    .line 1774060
    :try_start_0
    sget-object v0, LX/BKd;->a:LX/BKd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1774061
    if-eqz v2, :cond_0

    .line 1774062
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1774063
    new-instance p0, LX/BKd;

    invoke-static {v0}, LX/4hn;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, LX/BKd;-><init>(Ljava/lang/String;)V

    .line 1774064
    move-object v0, p0

    .line 1774065
    sput-object v0, LX/BKd;->a:LX/BKd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1774066
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1774067
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1774068
    :cond_1
    sget-object v0, LX/BKd;->a:LX/BKd;

    return-object v0

    .line 1774069
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1774070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
