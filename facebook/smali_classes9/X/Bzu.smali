.class public final LX/Bzu;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/8uh;

.field public c:Landroid/graphics/Typeface;

.field public d:I

.field public e:I

.field public f:F

.field public g:I

.field public h:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1839613
    invoke-static {}, LX/Bzv;->a()LX/Bzv;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 1839614
    sget-object v0, LX/Bzw;->a:LX/8uh;

    iput-object v0, p0, LX/Bzu;->b:LX/8uh;

    .line 1839615
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1839640
    const-string v0, "MessengerRoomsInitialReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1839616
    if-ne p0, p1, :cond_1

    .line 1839617
    :cond_0
    :goto_0
    return v0

    .line 1839618
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1839619
    goto :goto_0

    .line 1839620
    :cond_3
    check-cast p1, LX/Bzu;

    .line 1839621
    iget-object v2, p0, LX/Bzu;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bzu;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Bzu;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1839622
    goto :goto_0

    .line 1839623
    :cond_5
    iget-object v2, p1, LX/Bzu;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1839624
    :cond_6
    iget-object v2, p0, LX/Bzu;->b:LX/8uh;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bzu;->b:LX/8uh;

    iget-object v3, p1, LX/Bzu;->b:LX/8uh;

    invoke-virtual {v2, v3}, LX/8uh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1839625
    goto :goto_0

    .line 1839626
    :cond_8
    iget-object v2, p1, LX/Bzu;->b:LX/8uh;

    if-nez v2, :cond_7

    .line 1839627
    :cond_9
    iget-object v2, p0, LX/Bzu;->c:Landroid/graphics/Typeface;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Bzu;->c:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/Bzu;->c:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1839628
    goto :goto_0

    .line 1839629
    :cond_b
    iget-object v2, p1, LX/Bzu;->c:Landroid/graphics/Typeface;

    if-nez v2, :cond_a

    .line 1839630
    :cond_c
    iget v2, p0, LX/Bzu;->d:I

    iget v3, p1, LX/Bzu;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1839631
    goto :goto_0

    .line 1839632
    :cond_d
    iget v2, p0, LX/Bzu;->e:I

    iget v3, p1, LX/Bzu;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1839633
    goto :goto_0

    .line 1839634
    :cond_e
    iget v2, p0, LX/Bzu;->f:F

    iget v3, p1, LX/Bzu;->f:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_f

    move v0, v1

    .line 1839635
    goto :goto_0

    .line 1839636
    :cond_f
    iget v2, p0, LX/Bzu;->g:I

    iget v3, p1, LX/Bzu;->g:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1839637
    goto :goto_0

    .line 1839638
    :cond_10
    iget v2, p0, LX/Bzu;->h:F

    iget v3, p1, LX/Bzu;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1839639
    goto :goto_0
.end method
