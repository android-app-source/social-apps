.class public LX/AxF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AxD;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1727732
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AxF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727744
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1727745
    iput-object p1, p0, LX/AxF;->b:LX/0Ot;

    .line 1727746
    return-void
.end method

.method public static a(LX/0QB;)LX/AxF;
    .locals 4

    .prologue
    .line 1727747
    const-class v1, LX/AxF;

    monitor-enter v1

    .line 1727748
    :try_start_0
    sget-object v0, LX/AxF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1727749
    sput-object v2, LX/AxF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1727750
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727751
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1727752
    new-instance v3, LX/AxF;

    const/16 p0, 0x22b5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/AxF;-><init>(LX/0Ot;)V

    .line 1727753
    move-object v0, v3

    .line 1727754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1727755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AxF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1727756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1727757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1727735
    check-cast p2, LX/AxE;

    .line 1727736
    iget-object v0, p0, LX/AxF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;

    iget v1, p2, LX/AxE;->a:I

    iget-object v2, p2, LX/AxE;->b:Ljava/lang/String;

    .line 1727737
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v3, v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    .line 1727738
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object p2

    new-instance v0, LX/1o9;

    invoke-direct {v0, v1, v1}, LX/1o9;-><init>(II)V

    .line 1727739
    iput-object v0, p2, LX/1bX;->c:LX/1o9;

    .line 1727740
    move-object p2, p2

    .line 1727741
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/1bX;->a(Z)LX/1bX;

    move-result-object p2

    invoke-virtual {p2}, LX/1bX;->n()LX/1bf;

    move-result-object p2

    move-object p2, p2

    .line 1727742
    invoke-virtual {v3, p2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    sget-object p2, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    const/4 p2, 0x1

    invoke-virtual {v3, p2}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    sget-object p0, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, p0}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f0b1a15

    invoke-interface {v3, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const p0, 0x7f0b1a15

    invoke-interface {v3, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/16 p0, 0x8

    const p2, 0x7f0b1a17

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1727743
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1727733
    invoke-static {}, LX/1dS;->b()V

    .line 1727734
    const/4 v0, 0x0

    return-object v0
.end method
