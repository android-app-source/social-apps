.class public final LX/ATY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ATX;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676173
    iput-object p1, p0, LX/ATY;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1676171
    iget-object v0, p0, LX/ATY;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    invoke-virtual {v0}, LX/ATL;->b()V

    .line 1676172
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1676167
    return-void
.end method

.method public final a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1676168
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676169
    iget-object v0, p0, LX/ATY;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v1, p0, LX/ATY;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATY;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1, p3}, LX/ATL;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;Ljava/lang/String;)V

    .line 1676170
    return-void
.end method
