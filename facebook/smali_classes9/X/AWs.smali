.class public final LX/AWs;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AWy;


# direct methods
.method public constructor <init>(LX/AWy;)V
    .locals 0

    .prologue
    .line 1683440
    iput-object p1, p0, LX/AWs;->a:LX/AWy;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1683457
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    if-eqz v0, :cond_0

    .line 1683458
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/AWx;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1683459
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1683441
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1683442
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v1, p0, LX/AWs;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v1}, Lcom/facebook/facecast/model/FacecastPrivacyData;->d()LX/AWS;

    move-result-object v1

    new-instance v2, LX/8QV;

    invoke-direct {v2}, LX/8QV;-><init>()V

    .line 1683443
    iput-object p1, v2, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1683444
    move-object v2, v2

    .line 1683445
    iget-object v3, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2, v3}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v2

    invoke-virtual {v2}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v2

    .line 1683446
    iput-object v2, v1, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1683447
    move-object v1, v1

    .line 1683448
    invoke-virtual {v1}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v1

    .line 1683449
    iput-object v1, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683450
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 1683451
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    invoke-static {v0}, LX/AWy;->k(LX/AWy;)V

    .line 1683452
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    if-eqz v0, :cond_0

    .line 1683453
    iget-object v0, p0, LX/AWs;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    iget-object v1, p0, LX/AWs;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683454
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v2

    .line 1683455
    invoke-interface {v0, v1}, LX/AWx;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1683456
    :cond_0
    return-void
.end method
