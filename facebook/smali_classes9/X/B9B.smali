.class public final LX/B9B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;)V
    .locals 0

    .prologue
    .line 1751236
    iput-object p1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x6d1c8617

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1751237
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->f:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->j()V

    .line 1751238
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->d:LX/B7i;

    invoke-interface {v1}, LX/B7i;->b()V

    .line 1751239
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->f:LX/B6u;

    .line 1751240
    iget v2, v1, LX/B6u;->h:I

    move v1, v2

    .line 1751241
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->f:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1751242
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a:LX/B6l;

    const-string v3, "lead_gen_close_context_card_click"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751243
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "lead_gen_close_context_card_click"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1751244
    :goto_0
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1751245
    const v1, 0x7a46dc38

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1751246
    :cond_0
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->f:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1751247
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a:LX/B6l;

    const-string v3, "cta_lead_gen_xout_on_top"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751248
    iget-object v1, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "click_xout_button_on_disclaimer"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1751249
    :cond_1
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a:LX/B6l;

    const-string v3, "cta_lead_gen_xout_on_top"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751250
    iget-object v2, p0, LX/B9B;->a:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "cta_lead_gen_xout_on_top"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
