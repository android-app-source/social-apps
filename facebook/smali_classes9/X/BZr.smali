.class public final enum LX/BZr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZr;

.field public static final enum STARS_BACKGROUND:LX/BZr;

.field public static final enum STARS_DISMISS:LX/BZr;

.field public static final enum STARS_HIGHRATING_GOTOSTORE:LX/BZr;

.field public static final enum STARS_HIGHRATING_NOTHANKS:LX/BZr;

.field public static final enum STARS_LOWRATING_CANCEL:LX/BZr;

.field public static final enum STARS_LOWRATING_SUBMIT:LX/BZr;

.field public static final enum STARS_STARCHOSEN:LX/BZr;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1798034
    new-instance v0, LX/BZr;

    const-string v1, "STARS_BACKGROUND"

    invoke-direct {v0, v1, v3}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_BACKGROUND:LX/BZr;

    .line 1798035
    new-instance v0, LX/BZr;

    const-string v1, "STARS_DISMISS"

    invoke-direct {v0, v1, v4}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_DISMISS:LX/BZr;

    .line 1798036
    new-instance v0, LX/BZr;

    const-string v1, "STARS_STARCHOSEN"

    invoke-direct {v0, v1, v5}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_STARCHOSEN:LX/BZr;

    .line 1798037
    new-instance v0, LX/BZr;

    const-string v1, "STARS_HIGHRATING_NOTHANKS"

    invoke-direct {v0, v1, v6}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_HIGHRATING_NOTHANKS:LX/BZr;

    .line 1798038
    new-instance v0, LX/BZr;

    const-string v1, "STARS_HIGHRATING_GOTOSTORE"

    invoke-direct {v0, v1, v7}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_HIGHRATING_GOTOSTORE:LX/BZr;

    .line 1798039
    new-instance v0, LX/BZr;

    const-string v1, "STARS_LOWRATING_CANCEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_LOWRATING_CANCEL:LX/BZr;

    .line 1798040
    new-instance v0, LX/BZr;

    const-string v1, "STARS_LOWRATING_SUBMIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BZr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZr;->STARS_LOWRATING_SUBMIT:LX/BZr;

    .line 1798041
    const/4 v0, 0x7

    new-array v0, v0, [LX/BZr;

    sget-object v1, LX/BZr;->STARS_BACKGROUND:LX/BZr;

    aput-object v1, v0, v3

    sget-object v1, LX/BZr;->STARS_DISMISS:LX/BZr;

    aput-object v1, v0, v4

    sget-object v1, LX/BZr;->STARS_STARCHOSEN:LX/BZr;

    aput-object v1, v0, v5

    sget-object v1, LX/BZr;->STARS_HIGHRATING_NOTHANKS:LX/BZr;

    aput-object v1, v0, v6

    sget-object v1, LX/BZr;->STARS_HIGHRATING_GOTOSTORE:LX/BZr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BZr;->STARS_LOWRATING_CANCEL:LX/BZr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BZr;->STARS_LOWRATING_SUBMIT:LX/BZr;

    aput-object v2, v0, v1

    sput-object v0, LX/BZr;->$VALUES:[LX/BZr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1798042
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZr;
    .locals 1

    .prologue
    .line 1798043
    const-class v0, LX/BZr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZr;

    return-object v0
.end method

.method public static values()[LX/BZr;
    .locals 1

    .prologue
    .line 1798044
    sget-object v0, LX/BZr;->$VALUES:[LX/BZr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZr;

    return-object v0
.end method
