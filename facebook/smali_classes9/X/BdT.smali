.class public final LX/BdT;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/BdW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/BdU;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1803800
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "sectionBuilder"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "markerId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "logger"

    aput-object v2, v0, v1

    sput-object v0, LX/BdT;->b:[Ljava/lang/String;

    .line 1803801
    sput v3, LX/BdT;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1803784
    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1803785
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/BdT;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/BdT;->d:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1803786
    invoke-super {p0}, LX/BcN;->a()V

    .line 1803787
    const/4 v0, 0x0

    iput-object v0, p0, LX/BdT;->a:LX/BdU;

    .line 1803788
    sget-object v0, LX/BdW;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1803789
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/BdW;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1803790
    iget-object v1, p0, LX/BdT;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/BdT;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/BdT;->c:I

    if-ge v1, v2, :cond_2

    .line 1803791
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1803792
    :goto_0
    sget v2, LX/BdT;->c:I

    if-ge v0, v2, :cond_1

    .line 1803793
    iget-object v2, p0, LX/BdT;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1803794
    sget-object v2, LX/BdT;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803795
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1803796
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1803797
    :cond_2
    iget-object v0, p0, LX/BdT;->a:LX/BdU;

    .line 1803798
    invoke-virtual {p0}, LX/BdT;->a()V

    .line 1803799
    return-object v0
.end method
