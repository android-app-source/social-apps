.class public final LX/Avz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Aw4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V
    .locals 0

    .prologue
    .line 1724850
    iput-object p1, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1724851
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    .line 1724852
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->j:Ljava/util/Set;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 1724853
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    sget-object v1, LX/0i7;->FETCH_FAILURE:LX/0i7;

    .line 1724854
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    .line 1724855
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->o:LX/03V;

    const-string v1, "InspirationNuxStore#fetchNuxAssets()"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1724856
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1724857
    check-cast p1, LX/Aw4;

    const/4 v3, 0x0

    .line 1724858
    if-nez p1, :cond_0

    .line 1724859
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->o:LX/03V;

    const-string v1, "InspirationNuxStore#fetchNuxAssets()"

    const-string v2, "Device not enough space for assets"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1724860
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    .line 1724861
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->j:Ljava/util/Set;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 1724862
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    sget-object v1, LX/0i7;->FETCH_FAILURE:LX/0i7;

    .line 1724863
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    .line 1724864
    :goto_0
    return-void

    .line 1724865
    :cond_0
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    move v2, v3

    .line 1724866
    :goto_1
    iget-object v0, p1, LX/Aw4;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1724867
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->d:LX/0Tn;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1724868
    iget-object v1, p1, LX/Aw4;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1724869
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1724870
    :cond_1
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->c:LX/0Tn;

    iget-object v1, p1, LX/Aw4;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 1724871
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->e:LX/0Tn;

    iget-object v1, p1, LX/Aw4;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1724872
    iget-object v0, p1, LX/Aw4;->c:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 1724873
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->f:LX/0Tn;

    iget-object v1, p1, LX/Aw4;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1724874
    :cond_2
    iget-object v0, p1, LX/Aw4;->d:Ljava/io/File;

    if-eqz v0, :cond_3

    .line 1724875
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->g:LX/0Tn;

    iget-object v1, p1, LX/Aw4;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1724876
    :cond_3
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->b:LX/0Tn;

    iget-object v1, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->m:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v4, v0, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1724877
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->t:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1724878
    sget-object v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->h:LX/0Tn;

    iget-object v1, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->t:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1724879
    :cond_4
    invoke-interface {v4}, LX/0hN;->commit()V

    .line 1724880
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    sget-object v1, LX/0i7;->FETCHED:LX/0i7;

    .line 1724881
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    .line 1724882
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->s:LX/ArM;

    iget-object v1, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 1724883
    iget-object v2, v0, LX/ArM;->a:LX/0Zb;

    sget-object v4, LX/ArH;->NUX_VIDEO_DOWNLOAD:LX/ArH;

    invoke-static {v4}, LX/ArM;->a(LX/ArH;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/ArI;->INDEX:LX/ArI;

    invoke-virtual {v5}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1724884
    iget-object v0, p0, LX/Avz;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v1, 0x1

    new-array v1, v1, [LX/0Tn;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    aput-object v2, v1, v3

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    goto/16 :goto_0
.end method
