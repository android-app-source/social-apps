.class public LX/Acy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:LX/03V;

.field public final e:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1693342
    const-class v0, LX/Acy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Acy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/03V;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1693336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1693337
    iput-object p1, p0, LX/Acy;->b:LX/0tX;

    .line 1693338
    iput-object p2, p0, LX/Acy;->d:LX/03V;

    .line 1693339
    iput-object p3, p0, LX/Acy;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1693340
    iput-object p4, p0, LX/Acy;->e:Landroid/os/Handler;

    .line 1693341
    return-void
.end method

.method public static a(LX/0QB;)LX/Acy;
    .locals 1

    .prologue
    .line 1693312
    invoke-static {p0}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Acy;
    .locals 5

    .prologue
    .line 1693343
    new-instance v4, LX/Acy;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Acy;-><init>(LX/0tX;LX/03V;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;)V

    .line 1693344
    return-object v4
.end method


# virtual methods
.method public final a(LX/Acx;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1693328
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1693329
    invoke-static {}, LX/AdH;->a()LX/AdG;

    move-result-object v0

    .line 1693330
    const-string v1, "videoID"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1693331
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1693332
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1693333
    iget-object v1, p0, LX/Acy;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1693334
    new-instance v1, LX/Acs;

    invoke-direct {v1, p0, p1}, LX/Acs;-><init>(LX/Acy;LX/Acx;)V

    iget-object v2, p0, LX/Acy;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1693335
    return-void
.end method

.method public final a(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V
    .locals 3

    .prologue
    .line 1693320
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1693321
    new-instance v0, LX/AdD;

    invoke-direct {v0}, LX/AdD;-><init>()V

    move-object v0, v0

    .line 1693322
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1693323
    const-string v1, "limit"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1693324
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1693325
    iget-object v1, p0, LX/Acy;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1693326
    new-instance v1, LX/Acu;

    invoke-direct {v1, p0, p1}, LX/Acu;-><init>(LX/Acy;Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    iget-object v2, p0, LX/Acy;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1693327
    return-void
.end method

.method public final a(Ljava/lang/String;LX/AXX;)V
    .locals 3

    .prologue
    .line 1693313
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1693314
    new-instance v0, LX/AdE;

    invoke-direct {v0}, LX/AdE;-><init>()V

    move-object v0, v0

    .line 1693315
    const-string v1, "videoID"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1693316
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1693317
    iget-object v1, p0, LX/Acy;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1693318
    new-instance v1, LX/Acv;

    invoke-direct {v1, p0, p2}, LX/Acv;-><init>(LX/Acy;LX/AXX;)V

    iget-object v2, p0, LX/Acy;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1693319
    return-void
.end method
