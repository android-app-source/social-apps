.class public final LX/BBM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1757072
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1757073
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757074
    :goto_0
    return v1

    .line 1757075
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757076
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1757077
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1757078
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1757079
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1757080
    const-string v5, "deltas"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1757081
    invoke-static {p0, p1}, LX/BBP;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1757082
    :cond_2
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1757083
    invoke-static {p0, p1}, LX/BBa;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1757084
    :cond_3
    const-string v5, "page_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1757085
    invoke-static {p0, p1}, LX/BBL;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1757086
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1757087
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1757088
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1757089
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1757090
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1757091
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1757092
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757093
    if-eqz v0, :cond_0

    .line 1757094
    const-string v1, "deltas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757095
    invoke-static {p0, v0, p2, p3}, LX/BBP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757096
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757097
    if-eqz v0, :cond_1

    .line 1757098
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757099
    invoke-static {p0, v0, p2, p3}, LX/BBa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757100
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757101
    if-eqz v0, :cond_2

    .line 1757102
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757103
    invoke-static {p0, v0, p2}, LX/BBL;->a(LX/15i;ILX/0nX;)V

    .line 1757104
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757105
    return-void
.end method
