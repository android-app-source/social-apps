.class public LX/CCa;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oV",
        "<",
        "LX/3Yv;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/2oL;

.field private b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/2oL;)V
    .locals 1

    .prologue
    .line 1858028
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1858029
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CCa;->b:Z

    .line 1858030
    iput-object p2, p0, LX/CCa;->a:LX/2oL;

    .line 1858031
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CCa;->c:Ljava/util/Set;

    .line 1858032
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1858033
    check-cast p1, LX/3Yv;

    .line 1858034
    iget-boolean v0, p0, LX/CCa;->b:Z

    if-nez v0, :cond_0

    .line 1858035
    :goto_0
    return-void

    .line 1858036
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CCa;->b:Z

    .line 1858037
    iget-object v0, p1, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-nez v0, :cond_1

    .line 1858038
    const/4 v0, 0x0

    .line 1858039
    :goto_1
    move v0, v0

    .line 1858040
    iget-object v1, p0, LX/CCa;->a:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->c()LX/04g;

    move-result-object v1

    .line 1858041
    iget-object v2, p1, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-nez v2, :cond_2

    .line 1858042
    :goto_2
    iget-object v1, p0, LX/CCa;->a:LX/2oL;

    invoke-virtual {v1, v0}, LX/2oL;->a(I)V

    .line 1858043
    goto :goto_0

    :cond_1
    iget-object v0, p1, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    goto :goto_1

    .line 1858044
    :cond_2
    iget-object v2, p1, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1858045
    check-cast p1, LX/3Yv;

    .line 1858046
    iget-boolean v0, p0, LX/CCa;->b:Z

    if-eqz v0, :cond_1

    .line 1858047
    :cond_0
    :goto_0
    return-void

    .line 1858048
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CCa;->b:Z

    .line 1858049
    iget-object v0, p0, LX/CCa;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    iget-object v1, p0, LX/CCa;->c:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/2oO;->a(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858050
    iget-object v0, p0, LX/CCa;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->c()LX/04g;

    move-result-object v0

    iget-object v1, p0, LX/CCa;->a:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->a()I

    move-result v1

    .line 1858051
    invoke-static {p1}, LX/3Yv;->e(LX/3Yv;)V

    .line 1858052
    iget-object p0, p1, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 1858053
    goto :goto_0
.end method
