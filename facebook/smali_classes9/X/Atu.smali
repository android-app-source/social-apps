.class public final LX/Atu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V
    .locals 0

    .prologue
    .line 1721926
    iput-object p1, p0, LX/Atu;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1721924
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1721927
    iget-object v0, p0, LX/Atu;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1721928
    iget-object v0, p0, LX/Atu;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1721929
    iget-object v0, p0, LX/Atu;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1721930
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1721925
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1721923
    return-void
.end method
