.class public final LX/BNT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;",
        ">;",
        "Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsInterfaces$PageOverallStarRating;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BNU;


# direct methods
.method public constructor <init>(LX/BNU;)V
    .locals 0

    .prologue
    .line 1778718
    iput-object p1, p0, LX/BNT;->a:LX/BNU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1778719
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1778720
    if-eqz p1, :cond_0

    .line 1778721
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1778722
    if-eqz v0, :cond_0

    .line 1778723
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1778724
    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
