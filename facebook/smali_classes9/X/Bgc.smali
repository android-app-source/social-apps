.class public LX/Bgc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BeE;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BeD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1807991
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "337046403064701"

    sget-object v2, LX/BeE;->LOCATION:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "333522400104087"

    sget-object v2, LX/BeE;->MULTI_VALUE_FIELD:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "463427363734722"

    sget-object v2, LX/BeE;->TEXT_FIELD:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "114481832091120"

    sget-object v2, LX/BeE;->HOURS_FIELD:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "137075966484179"

    sget-object v2, LX/BeE;->TEXT_FIELD:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "376081639179091"

    sget-object v2, LX/BeE;->MULTI_TEXT_FIELD:LX/BeE;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Bgc;->a:Ljava/util/Map;

    .line 1807992
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "337046403064701"

    sget-object v2, LX/BeD;->LOCATION:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "333522400104087"

    sget-object v2, LX/BeD;->CATEGORY_PICKER:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "463427363734722"

    sget-object v2, LX/BeD;->TEXT:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "114481832091120"

    sget-object v2, LX/BeD;->HOURS_PICKER:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "137075966484179"

    sget-object v2, LX/BeD;->TEXT:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "376081639179091"

    sget-object v2, LX/BeD;->TEXT:LX/BeD;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Bgc;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1807993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/97f;)LX/BeE;
    .locals 2

    .prologue
    .line 1807994
    sget-object v0, LX/Bgc;->a:Ljava/util/Map;

    invoke-interface {p0}, LX/97e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1807995
    sget-object v0, LX/BeE;->UNSUPPORTED:LX/BeE;

    .line 1807996
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Bgc;->a:Ljava/util/Map;

    invoke-interface {p0}, LX/97e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BeE;

    goto :goto_0
.end method

.method public static b(LX/97f;)Z
    .locals 2

    .prologue
    .line 1807997
    invoke-static {p0}, LX/Bgc;->a(LX/97f;)LX/BeE;

    move-result-object v0

    sget-object v1, LX/BeE;->UNSUPPORTED:LX/BeE;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/97f;)LX/BeD;
    .locals 2

    .prologue
    .line 1807998
    sget-object v0, LX/Bgc;->b:Ljava/util/Map;

    invoke-interface {p0}, LX/97e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1807999
    sget-object v0, LX/BeD;->TEXT:LX/BeD;

    .line 1808000
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Bgc;->b:Ljava/util/Map;

    invoke-interface {p0}, LX/97e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BeD;

    goto :goto_0
.end method
