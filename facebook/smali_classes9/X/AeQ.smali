.class public abstract LX/AeQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public b:LX/AeV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Z


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0

    .prologue
    .line 1697372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697373
    iput-object p1, p0, LX/AeQ;->a:LX/0SG;

    .line 1697374
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 1697368
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/AeQ;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/AeV;)V
    .locals 1

    .prologue
    .line 1697369
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/AeQ;->b:LX/AeV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697370
    monitor-exit p0

    return-void

    .line 1697371
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1697362
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/AeQ;->e()V

    .line 1697363
    iput-object p1, p0, LX/AeQ;->c:Ljava/lang/String;

    .line 1697364
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AeQ;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697365
    monitor-exit p0

    return-void

    .line 1697366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 1697367
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/AeQ;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 1697357
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/AeQ;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 1697358
    iget-object v0, p0, LX/AeQ;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/AeQ;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697359
    monitor-exit p0

    return-void

    .line 1697360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1697361
    const/4 v0, -0x1

    return v0
.end method

.method public abstract e()V
.end method

.method public abstract f()Z
.end method

.method public abstract g()LX/AeN;
.end method
