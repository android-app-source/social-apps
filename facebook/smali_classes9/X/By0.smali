.class public LX/By0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bxy;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/By1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1836593
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/By0;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/By1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836590
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1836591
    iput-object p1, p0, LX/By0;->b:LX/0Ot;

    .line 1836592
    return-void
.end method

.method public static a(LX/0QB;)LX/By0;
    .locals 4

    .prologue
    .line 1836579
    const-class v1, LX/By0;

    monitor-enter v1

    .line 1836580
    :try_start_0
    sget-object v0, LX/By0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836581
    sput-object v2, LX/By0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836582
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836583
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836584
    new-instance v3, LX/By0;

    const/16 p0, 0x1e1c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/By0;-><init>(LX/0Ot;)V

    .line 1836585
    move-object v0, v3

    .line 1836586
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836587
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/By0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836588
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836589
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;LX/BnW;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/BnW;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836578
    const v0, -0x69d6c5e6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1836562
    check-cast p2, LX/Bxz;

    .line 1836563
    iget-object v0, p0, LX/By0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Bxz;->a:LX/BnW;

    .line 1836564
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v1

    iget-object v2, v0, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1836565
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const p0, 0x7f0b0b5a

    invoke-interface {v2, p0}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const p0, 0x7f0b0b5a

    invoke-interface {v2, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    iget-object p0, v0, LX/BnW;->b:Ljava/lang/String;

    invoke-interface {v2, p0}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 1836566
    const v2, -0x69d6c5e6

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v0, p0, p2

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1836567
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1836568
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1836569
    invoke-static {}, LX/1dS;->b()V

    .line 1836570
    iget v0, p1, LX/1dQ;->b:I

    .line 1836571
    packed-switch v0, :pswitch_data_0

    .line 1836572
    :goto_0
    return-object v3

    .line 1836573
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1836574
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/BnW;

    .line 1836575
    iget-object p1, p0, LX/By0;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/By1;

    .line 1836576
    iget-object p0, v0, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-interface {p0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1836577
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x69d6c5e6
        :pswitch_0
    .end packed-switch
.end method
