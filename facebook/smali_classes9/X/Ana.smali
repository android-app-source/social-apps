.class public LX/Ana;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Anc;

.field public d:LX/Anb;

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/Anc;LX/Anb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/Anc;",
            "LX/Anb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712674
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ana;->e:Z

    .line 1712675
    iput-object p1, p0, LX/Ana;->a:Landroid/content/Context;

    .line 1712676
    iput-object p2, p0, LX/Ana;->b:LX/0Or;

    .line 1712677
    iput-object p3, p0, LX/Ana;->c:LX/Anc;

    .line 1712678
    iput-object p4, p0, LX/Ana;->d:LX/Anb;

    .line 1712679
    return-void
.end method

.method public static a(LX/0QB;)LX/Ana;
    .locals 7

    .prologue
    .line 1712680
    const-class v1, LX/Ana;

    monitor-enter v1

    .line 1712681
    :try_start_0
    sget-object v0, LX/Ana;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712682
    sput-object v2, LX/Ana;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712683
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712684
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712685
    new-instance v6, LX/Ana;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x1399

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/Anc;->a(LX/0QB;)LX/Anc;

    move-result-object v4

    check-cast v4, LX/Anc;

    invoke-static {v0}, LX/Anb;->a(LX/0QB;)LX/Anb;

    move-result-object v5

    check-cast v5, LX/Anb;

    invoke-direct {v6, v3, p0, v4, v5}, LX/Ana;-><init>(Landroid/content/Context;LX/0Or;LX/Anc;LX/Anb;)V

    .line 1712686
    move-object v0, v6

    .line 1712687
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712688
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ana;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712689
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712690
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
