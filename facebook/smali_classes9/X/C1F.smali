.class public final LX/C1F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final d:Z

.field public final e:Lcom/facebook/graphql/model/GraphQLNode;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNode;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1842179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842180
    iput p1, p0, LX/C1F;->a:I

    .line 1842181
    iput-object p2, p0, LX/C1F;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842182
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1842183
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/C1F;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842184
    iput-boolean p4, p0, LX/C1F;->d:Z

    .line 1842185
    iput-object p3, p0, LX/C1F;->e:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1842186
    iput-object p5, p0, LX/C1F;->g:Ljava/lang/String;

    .line 1842187
    iput-object p6, p0, LX/C1F;->f:Ljava/lang/String;

    .line 1842188
    return-void
.end method
