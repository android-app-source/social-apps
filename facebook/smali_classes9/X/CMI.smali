.class public LX/CMI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1880428
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "emojis/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1880429
    sput-object v0, LX/CMI;->a:LX/0Tn;

    const-string v1, "popup/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1880430
    sput-object v0, LX/CMI;->b:LX/0Tn;

    const-string v1, "tab_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->c:LX/0Tn;

    .line 1880431
    sget-object v0, LX/CMI;->b:LX/0Tn;

    const-string v1, "page"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->d:LX/0Tn;

    .line 1880432
    sget-object v0, LX/CMI;->a:LX/0Tn;

    const-string v1, "has_seen_emoji_color_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->e:LX/0Tn;

    .line 1880433
    sget-object v0, LX/CMI;->a:LX/0Tn;

    const-string v1, "force_emoji_color_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->f:LX/0Tn;

    .line 1880434
    sget-object v0, LX/CMI;->a:LX/0Tn;

    const-string v1, "has_seen_full_screen_emoji_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->g:LX/0Tn;

    .line 1880435
    sget-object v0, LX/CMI;->a:LX/0Tn;

    const-string v1, "force_full_screen_emoji_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CMI;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1880436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
