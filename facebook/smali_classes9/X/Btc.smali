.class public final LX/Btc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1829370
    iput-object p1, p0, LX/Btc;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1829371
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 1829372
    iget-object v0, p0, LX/Btc;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static {v0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1829373
    if-nez v0, :cond_1

    .line 1829374
    :cond_0
    :goto_0
    return-object v2

    .line 1829375
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1829376
    iget-object v1, p0, LX/Btc;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Btc;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829377
    :cond_2
    iget-object v0, p0, LX/Btc;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method
