.class public LX/Cad;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Cad;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918220
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1918221
    const-string v0, "photo/{#%s}/?set={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "photo_fbid"

    const-string v2, "photoset_token"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1918222
    const-string v0, "photo/{#%s}/"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "photo_fbid"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1918223
    return-void
.end method

.method public static a(LX/0QB;)LX/Cad;
    .locals 3

    .prologue
    .line 1918224
    sget-object v0, LX/Cad;->a:LX/Cad;

    if-nez v0, :cond_1

    .line 1918225
    const-class v1, LX/Cad;

    monitor-enter v1

    .line 1918226
    :try_start_0
    sget-object v0, LX/Cad;->a:LX/Cad;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1918227
    if-eqz v2, :cond_0

    .line 1918228
    :try_start_1
    new-instance v0, LX/Cad;

    invoke-direct {v0}, LX/Cad;-><init>()V

    .line 1918229
    move-object v0, v0

    .line 1918230
    sput-object v0, LX/Cad;->a:LX/Cad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1918231
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1918232
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1918233
    :cond_1
    sget-object v0, LX/Cad;->a:LX/Cad;

    return-object v0

    .line 1918234
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1918235
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
