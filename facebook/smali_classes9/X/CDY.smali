.class public final LX/CDY;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/CDa;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CDZ;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x9

    .line 1859635
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1859636
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "richVideoPlayerParams"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "richVideoPlayerPluginSelector"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "richVideoPlayerState"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "playerOrigin"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "channelEligibility"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "richVideoPlayerCallbackListener"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "inlineVideoPersistentState"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "videoDisplayedInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "feedStoryInfo"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CDY;->b:[Ljava/lang/String;

    .line 1859637
    iput v3, p0, LX/CDY;->c:I

    .line 1859638
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CDY;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CDY;LX/1De;IILX/CDZ;)V
    .locals 1

    .prologue
    .line 1859639
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1859640
    iput-object p4, p0, LX/CDY;->a:LX/CDZ;

    .line 1859641
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1859642
    return-void
.end method


# virtual methods
.method public final a(LX/04D;)LX/CDY;
    .locals 2

    .prologue
    .line 1859643
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->g:LX/04D;

    .line 1859644
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859645
    return-object p0
.end method

.method public final a(LX/04H;)LX/CDY;
    .locals 2

    .prologue
    .line 1859646
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->h:LX/04H;

    .line 1859647
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859648
    return-object p0
.end method

.method public final a(LX/093;)LX/CDY;
    .locals 2

    .prologue
    .line 1859649
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->k:LX/093;

    .line 1859650
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859651
    return-object p0
.end method

.method public final a(LX/0Px;)LX/CDY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/CDY;"
        }
    .end annotation

    .prologue
    .line 1859652
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->d:LX/0Px;

    .line 1859653
    return-object p0
.end method

.method public final a(LX/1Pe;)LX/CDY;
    .locals 1

    .prologue
    .line 1859654
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->n:LX/1Pe;

    .line 1859655
    return-object p0
.end method

.method public final a(LX/2oL;)LX/CDY;
    .locals 2

    .prologue
    .line 1859656
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->j:LX/2oL;

    .line 1859657
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859658
    return-object p0
.end method

.method public final a(LX/2pa;)LX/CDY;
    .locals 2

    .prologue
    .line 1859659
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->a:LX/2pa;

    .line 1859660
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859661
    return-object p0
.end method

.method public final a(LX/3Ge;)LX/CDY;
    .locals 2

    .prologue
    .line 1859597
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->b:LX/3Ge;

    .line 1859598
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859599
    return-object p0
.end method

.method public final a(LX/3It;)LX/CDY;
    .locals 2
    .param p1    # LX/3It;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1859632
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->i:LX/3It;

    .line 1859633
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859634
    return-object p0
.end method

.method public final a(LX/3Iv;)LX/CDY;
    .locals 1

    .prologue
    .line 1859630
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->r:LX/3Iv;

    .line 1859631
    return-object p0
.end method

.method public final a(LX/CDg;)LX/CDY;
    .locals 2

    .prologue
    .line 1859627
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->f:LX/CDg;

    .line 1859628
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859629
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/CDY;
    .locals 1

    .prologue
    .line 1859625
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1859626
    return-object p0
.end method

.method public final a(Lcom/facebook/video/analytics/VideoFeedStoryInfo;)LX/CDY;
    .locals 2

    .prologue
    .line 1859622
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-object p1, v0, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859623
    iget-object v0, p0, LX/CDY;->d:Ljava/util/BitSet;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859624
    return-object p0
.end method

.method public final a(Z)LX/CDY;
    .locals 1

    .prologue
    .line 1859620
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-boolean p1, v0, LX/CDZ;->c:Z

    .line 1859621
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1859616
    invoke-super {p0}, LX/1X5;->a()V

    .line 1859617
    const/4 v0, 0x0

    iput-object v0, p0, LX/CDY;->a:LX/CDZ;

    .line 1859618
    sget-object v0, LX/CDa;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1859619
    return-void
.end method

.method public final b(Z)LX/CDY;
    .locals 1

    .prologue
    .line 1859614
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-boolean p1, v0, LX/CDZ;->o:Z

    .line 1859615
    return-object p0
.end method

.method public final c(Z)LX/CDY;
    .locals 1

    .prologue
    .line 1859612
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput-boolean p1, v0, LX/CDZ;->p:Z

    .line 1859613
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/CDa;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1859602
    iget-object v1, p0, LX/CDY;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CDY;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CDY;->c:I

    if-ge v1, v2, :cond_2

    .line 1859603
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1859604
    :goto_0
    iget v2, p0, LX/CDY;->c:I

    if-ge v0, v2, :cond_1

    .line 1859605
    iget-object v2, p0, LX/CDY;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1859606
    iget-object v2, p0, LX/CDY;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1859607
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1859608
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1859609
    :cond_2
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    .line 1859610
    invoke-virtual {p0}, LX/CDY;->a()V

    .line 1859611
    return-object v0
.end method

.method public final h(I)LX/CDY;
    .locals 1

    .prologue
    .line 1859600
    iget-object v0, p0, LX/CDY;->a:LX/CDZ;

    iput p1, v0, LX/CDZ;->q:I

    .line 1859601
    return-object p0
.end method
