.class public LX/BYc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile k:LX/BYc;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0yI;

.field private final d:LX/12x;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/7WS;

.field private final g:LX/0Xl;

.field private final h:LX/0Zb;

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1796382
    const-class v0, LX/BYc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BYc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0yI;LX/12x;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7WS;LX/0Xl;LX/0Zb;)V
    .locals 0
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1796373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796374
    iput-object p1, p0, LX/BYc;->b:Landroid/content/Context;

    .line 1796375
    iput-object p2, p0, LX/BYc;->c:LX/0yI;

    .line 1796376
    iput-object p3, p0, LX/BYc;->d:LX/12x;

    .line 1796377
    iput-object p4, p0, LX/BYc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1796378
    iput-object p5, p0, LX/BYc;->f:LX/7WS;

    .line 1796379
    iput-object p6, p0, LX/BYc;->g:LX/0Xl;

    .line 1796380
    iput-object p7, p0, LX/BYc;->h:LX/0Zb;

    .line 1796381
    return-void
.end method

.method public static a(LX/0QB;)LX/BYc;
    .locals 11

    .prologue
    .line 1796383
    sget-object v0, LX/BYc;->k:LX/BYc;

    if-nez v0, :cond_1

    .line 1796384
    const-class v1, LX/BYc;

    monitor-enter v1

    .line 1796385
    :try_start_0
    sget-object v0, LX/BYc;->k:LX/BYc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1796386
    if-eqz v2, :cond_0

    .line 1796387
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1796388
    new-instance v3, LX/BYc;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v5

    check-cast v5, LX/0yI;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v6

    check-cast v6, LX/12x;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/7WS;->b(LX/0QB;)LX/7WS;

    move-result-object v8

    check-cast v8, LX/7WS;

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-direct/range {v3 .. v10}, LX/BYc;-><init>(Landroid/content/Context;LX/0yI;LX/12x;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7WS;LX/0Xl;LX/0Zb;)V

    .line 1796389
    move-object v0, v3

    .line 1796390
    sput-object v0, LX/BYc;->k:LX/BYc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1796391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1796392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1796393
    :cond_1
    sget-object v0, LX/BYc;->k:LX/BYc;

    return-object v0

    .line 1796394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1796395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ZZJ)V
    .locals 5

    .prologue
    .line 1796362
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "offpeak_download_job_scheduled"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_module"

    .line 1796363
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796364
    move-object v1, v0

    .line 1796365
    const-string v0, "carrier_id"

    iget-object v2, p0, LX/BYc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796366
    const-string v2, "service"

    if-eqz p1, :cond_0

    const-string v0, "gcm"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796367
    const-string v2, "action"

    if-eqz p2, :cond_1

    const-string v0, "start"

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796368
    const-string v0, "latency"

    invoke-virtual {v1, v0, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796369
    iget-object v0, p0, LX/BYc;->h:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1796370
    return-void

    .line 1796371
    :cond_0
    const-string v0, "alarm"

    goto :goto_0

    .line 1796372
    :cond_1
    const-string v0, "stop"

    goto :goto_1
.end method

.method public static a$redex0(LX/BYc;ZJ)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1796335
    iget-object v0, p0, LX/BYc;->b:Landroid/content/Context;

    invoke-static {v0}, LX/45m;->a(Landroid/content/Context;)LX/45m;

    move-result-object v3

    .line 1796336
    if-eqz v3, :cond_1

    .line 1796337
    if-eqz p1, :cond_0

    const v0, 0x7f0d00f9

    .line 1796338
    :goto_0
    invoke-virtual {v3, v0}, LX/45m;->a(I)V

    .line 1796339
    new-instance v4, LX/45t;

    invoke-direct {v4, v0}, LX/45t;-><init>(I)V

    .line 1796340
    iput-wide p2, v4, LX/45t;->d:J

    .line 1796341
    move-object v0, v4

    .line 1796342
    const-wide/32 v4, 0x927c0

    add-long/2addr v4, p2

    .line 1796343
    iput-wide v4, v0, LX/45t;->e:J

    .line 1796344
    move-object v0, v0

    .line 1796345
    iput v1, v0, LX/45t;->b:I

    .line 1796346
    move-object v0, v0

    .line 1796347
    invoke-virtual {v0}, LX/45t;->a()LX/45u;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/45m;->a(LX/45u;)V

    .line 1796348
    invoke-direct {p0, v2, p1, p2, p3}, LX/BYc;->a(ZZJ)V

    .line 1796349
    :goto_1
    if-eqz p1, :cond_3

    .line 1796350
    iput-boolean v2, p0, LX/BYc;->i:Z

    .line 1796351
    :goto_2
    return-void

    .line 1796352
    :cond_0
    const v0, 0x7f0d00fa

    goto :goto_0

    .line 1796353
    :cond_1
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, LX/BYc;->b:Landroid/content/Context;

    const-class v4, Lcom/facebook/zero/offpeakdownload/ZeroOffpeakAlarmBroadcastReceiver;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1796354
    const-string v0, "is_start_download_intent"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1796355
    const-string v0, "com.facebook.zero.offpeakdownload.ZERO_OFFPEAK_DOWNLOAD_ACTION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1796356
    if-eqz p1, :cond_2

    move v0, v1

    .line 1796357
    :goto_3
    iget-object v4, p0, LX/BYc;->b:Landroid/content/Context;

    invoke-static {v4, v0, v3, v1}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1796358
    iget-object v3, p0, LX/BYc;->d:LX/12x;

    const/4 v4, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v6, p2

    invoke-virtual {v3, v4, v6, v7, v0}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 1796359
    invoke-direct {p0, v1, p1, p2, p3}, LX/BYc;->a(ZZJ)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1796360
    goto :goto_3

    .line 1796361
    :cond_3
    iput-boolean v2, p0, LX/BYc;->j:Z

    goto :goto_2
.end method


# virtual methods
.method public final c()V
    .locals 4

    .prologue
    .line 1796333
    iget-object v0, p0, LX/BYc;->g:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->OFFPEAK_DOWNLOAD_REFRESH:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1796334
    return-void
.end method
