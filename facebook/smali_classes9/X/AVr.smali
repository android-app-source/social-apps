.class public LX/AVr;
.super LX/AVk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVk",
        "<",
        "Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoReactionEventModel;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideReactionsEventModel;",
        ">;"
    }
.end annotation


# instance fields
.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680488
    invoke-direct {p0}, LX/AVk;-><init>()V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 1680465
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-nez v0, :cond_0

    .line 1680466
    :goto_0
    return-void

    .line 1680467
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AVr;->f:Z

    .line 1680468
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680469
    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1680470
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AVr;->f:Z

    .line 1680471
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680472
    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->clearDisappearingChildren()V

    .line 1680473
    return-void
.end method

.method public final g()V
    .locals 9

    .prologue
    .line 1680474
    invoke-super {p0}, LX/AVk;->g()V

    .line 1680475
    iget-boolean v0, p0, LX/AVr;->f:Z

    if-eqz v0, :cond_0

    .line 1680476
    const/4 v2, 0x1

    .line 1680477
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    mul-double/2addr v3, v5

    double-to-int v1, v3

    if-eqz v1, :cond_1

    .line 1680478
    :cond_0
    :goto_0
    return-void

    .line 1680479
    :cond_1
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 1680480
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    const-wide/high16 v7, 0x4008000000000000L    # 3.0

    mul-double/2addr v5, v7

    double-to-int v1, v5

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v1, v2

    .line 1680481
    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1680482
    iget-object v1, p0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1680483
    check-cast v1, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1, v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Landroid/util/SparseArray;)V

    goto :goto_0

    .line 1680484
    :pswitch_1
    const/4 v1, 0x2

    .line 1680485
    goto :goto_1

    .line 1680486
    :pswitch_2
    const/4 v1, 0x4

    .line 1680487
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
