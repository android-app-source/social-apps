.class public LX/BUA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-com.google.common.util.concurrent.Futures.addCallback",
        "ConstructorMayLeakThis"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Ljava/lang/String;

.field private static volatile y:LX/BUA;


# instance fields
.field private final a:LX/BU8;

.field private final b:LX/BU9;

.field public final c:LX/BTy;

.field public final e:LX/19w;

.field private final f:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/BUM;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/16U;

.field public final i:LX/1fW;

.field private final j:LX/BTn;

.field public final k:LX/19v;

.field public final l:LX/16I;

.field private final m:LX/1Sc;

.field public final n:LX/0tQ;

.field public final o:LX/15V;

.field public final p:LX/BUi;

.field public final q:LX/20h;

.field private final r:LX/0bH;

.field private final s:LX/0SG;

.field public t:LX/BUF;

.field public final u:LX/BYc;

.field private final v:LX/0Xp;

.field private final w:LX/0kb;

.field public x:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1788381
    const-class v0, LX/BUA;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BUA;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/15V;LX/BUi;LX/16I;LX/19w;Lcom/facebook/video/downloadmanager/VideoDownloadHandler;LX/16U;LX/1fW;LX/BTn;LX/1Sc;LX/19v;LX/0tQ;Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;LX/20h;LX/0bH;LX/0SG;LX/BUF;LX/BYc;LX/0Xp;LX/0kb;)V
    .locals 4
    .param p7    # LX/1fW;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1788382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788383
    new-instance v2, LX/BU8;

    invoke-direct {v2, p0}, LX/BU8;-><init>(LX/BUA;)V

    iput-object v2, p0, LX/BUA;->a:LX/BU8;

    .line 1788384
    new-instance v2, LX/BU9;

    invoke-direct {v2, p0}, LX/BU9;-><init>(LX/BUA;)V

    iput-object v2, p0, LX/BUA;->b:LX/BU9;

    .line 1788385
    new-instance v2, LX/BTy;

    invoke-direct {v2, p0}, LX/BTy;-><init>(LX/BUA;)V

    iput-object v2, p0, LX/BUA;->c:LX/BTy;

    .line 1788386
    iput-object p1, p0, LX/BUA;->o:LX/15V;

    .line 1788387
    iput-object p2, p0, LX/BUA;->p:LX/BUi;

    .line 1788388
    iput-object p7, p0, LX/BUA;->i:LX/1fW;

    .line 1788389
    iput-object p3, p0, LX/BUA;->l:LX/16I;

    .line 1788390
    iput-object p6, p0, LX/BUA;->h:LX/16U;

    .line 1788391
    iput-object p4, p0, LX/BUA;->e:LX/19w;

    .line 1788392
    iput-object p5, p0, LX/BUA;->f:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    .line 1788393
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/BUA;->g:Ljava/util/HashMap;

    .line 1788394
    iput-object p8, p0, LX/BUA;->j:LX/BTn;

    .line 1788395
    iput-object p10, p0, LX/BUA;->k:LX/19v;

    .line 1788396
    iput-object p11, p0, LX/BUA;->n:LX/0tQ;

    .line 1788397
    iput-object p9, p0, LX/BUA;->m:LX/1Sc;

    .line 1788398
    move-object/from16 v0, p15

    iput-object v0, p0, LX/BUA;->s:LX/0SG;

    .line 1788399
    move-object/from16 v0, p16

    iput-object v0, p0, LX/BUA;->t:LX/BUF;

    .line 1788400
    move-object/from16 v0, p17

    iput-object v0, p0, LX/BUA;->u:LX/BYc;

    .line 1788401
    move-object/from16 v0, p18

    iput-object v0, p0, LX/BUA;->v:LX/0Xp;

    .line 1788402
    move-object/from16 v0, p19

    iput-object v0, p0, LX/BUA;->w:LX/0kb;

    .line 1788403
    iget-object v2, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->h()J

    move-result-wide v2

    iput-wide v2, p0, LX/BUA;->x:J

    .line 1788404
    iget-object v2, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1788405
    invoke-direct {p0}, LX/BUA;->e()V

    .line 1788406
    invoke-direct {p0}, LX/BUA;->f()V

    .line 1788407
    invoke-virtual {p0}, LX/BUA;->a()V

    .line 1788408
    move-object/from16 v0, p12

    invoke-direct {p0, v0}, LX/BUA;->a(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V

    .line 1788409
    invoke-virtual {p0}, LX/BUA;->b()V

    .line 1788410
    :goto_0
    move-object/from16 v0, p13

    iput-object v0, p0, LX/BUA;->q:LX/20h;

    .line 1788411
    move-object/from16 v0, p14

    iput-object v0, p0, LX/BUA;->r:LX/0bH;

    .line 1788412
    iget-object v2, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->B()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1788413
    iget-object v2, p0, LX/BUA;->r:LX/0bH;

    iget-object v3, p0, LX/BUA;->a:LX/BU8;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 1788414
    iget-object v2, p0, LX/BUA;->r:LX/0bH;

    iget-object v3, p0, LX/BUA;->b:LX/BU9;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 1788415
    :cond_0
    invoke-direct {p0}, LX/BUA;->d()V

    .line 1788416
    return-void

    .line 1788417
    :cond_1
    iget-object v2, p0, LX/BUA;->i:LX/1fW;

    new-instance v3, Lcom/facebook/video/downloadmanager/DownloadManager$2;

    invoke-direct {v3, p0}, Lcom/facebook/video/downloadmanager/DownloadManager$2;-><init>(LX/BUA;)V

    invoke-virtual {v2, v3}, LX/0TT;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static synthetic a(LX/BUA;J)J
    .locals 1

    .prologue
    .line 1788418
    iput-wide p1, p0, LX/BUA;->x:J

    return-wide p1
.end method

.method public static a(LX/0QB;)LX/BUA;
    .locals 3

    .prologue
    .line 1788419
    sget-object v0, LX/BUA;->y:LX/BUA;

    if-nez v0, :cond_1

    .line 1788420
    const-class v1, LX/BUA;

    monitor-enter v1

    .line 1788421
    :try_start_0
    sget-object v0, LX/BUA;->y:LX/BUA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1788422
    if-eqz v2, :cond_0

    .line 1788423
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/BUA;->b(LX/0QB;)LX/BUA;

    move-result-object v0

    sput-object v0, LX/BUA;->y:LX/BUA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788424
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1788425
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1788426
    :cond_1
    sget-object v0, LX/BUA;->y:LX/BUA;

    return-object v0

    .line 1788427
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1788428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/BUA;LX/7Jg;LX/7Jb;)V
    .locals 6

    .prologue
    .line 1788429
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/7Jg;->l:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v0, v1, :cond_4

    iget-object v0, p1, LX/7Jg;->l:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v0, v1, :cond_4

    iget-object v0, p1, LX/7Jg;->l:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v0, v1, :cond_4

    .line 1788430
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    .line 1788431
    const/4 v2, 0x1

    invoke-static {v0, v1, p2, v2}, LX/19v;->a(LX/19v;Ljava/lang/String;LX/7Jb;Z)V

    .line 1788432
    :goto_0
    iget-object v0, p0, LX/BUA;->p:LX/BUi;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BUi;->b(Ljava/lang/String;)V

    .line 1788433
    new-instance v1, Ljava/io/File;

    iget-object v0, p1, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1788434
    const/4 v0, 0x0

    .line 1788435
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1788436
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Failed to delete the video file %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, LX/7Jg;->h:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788437
    :cond_0
    iget-object v1, p1, LX/7Jg;->c:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1788438
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, LX/7Jg;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1788439
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1788440
    sget-object v0, LX/BUA;->d:Ljava/lang/String;

    const-string v1, "Failed to delete the audio file %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/7Jg;->k:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788441
    :cond_2
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/19w;->j(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1788442
    sget-object v0, LX/BUA;->d:Ljava/lang/String;

    const-string v1, "Failed to delete the video record %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/7Jg;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788443
    :cond_3
    iget-object v0, p0, LX/BUA;->m:LX/1Sc;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Sc;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788444
    monitor-exit p0

    return-void

    .line 1788445
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    .line 1788446
    const/4 v2, 0x0

    invoke-static {v0, v1, p2, v2}, LX/19v;->a(LX/19v;Ljava/lang/String;LX/7Jb;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788447
    goto :goto_0

    .line 1788448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V
    .locals 2

    .prologue
    .line 1788376
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, Lcom/facebook/video/downloadmanager/DownloadManager$10;

    invoke-direct {v1, p0, p1, p0}, Lcom/facebook/video/downloadmanager/DownloadManager$10;-><init>(LX/BUA;Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;LX/BUA;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788377
    return-void
.end method

.method public static a$redex0(LX/BUA;JLjava/lang/String;)V
    .locals 11

    .prologue
    .line 1788449
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->g()J

    move-result-wide v0

    .line 1788450
    iget-object v2, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v2}, LX/19w;->p()J

    move-result-wide v2

    .line 1788451
    iget-object v4, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v4}, LX/19w;->q()J

    move-result-wide v4

    .line 1788452
    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    sub-long v6, v0, v4

    cmp-long v6, v6, p1

    if-ltz v6, :cond_0

    cmp-long v6, v2, p1

    if-gez v6, :cond_3

    .line 1788453
    :cond_0
    sget-object v6, LX/BUA;->d:Ljava/lang/String;

    const-string v7, "Insufficient space. Free Space: %d. Quota: %d. Video Size: %d. Committed: %d"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v9

    const/4 v0, 0x2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v8, v0

    const/4 v0, 0x3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v8, v0

    invoke-static {v6, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788454
    add-long v0, v4, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_2

    .line 1788455
    new-instance v1, LX/BTp;

    const-string v2, "Insufficient space. Delete one or more videos"

    const-string v0, "saved_dashboard"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD:LX/BTo;

    :goto_0
    invoke-direct {v1, v2, v0}, LX/BTp;-><init>(Ljava/lang/String;LX/BTo;)V

    throw v1

    :cond_1
    sget-object v0, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL:LX/BTo;

    goto :goto_0

    .line 1788456
    :cond_2
    new-instance v0, LX/BTp;

    const-string v1, "Insufficient space. Delete one or more file from device"

    sget-object v2, LX/BTo;->INSUFFICIENT_SPACE_DEVICE:LX/BTo;

    invoke-direct {v0, v1, v2}, LX/BTp;-><init>(Ljava/lang/String;LX/BTo;)V

    throw v0

    .line 1788457
    :cond_3
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/BUA;LX/7Jg;)V
    .locals 4

    .prologue
    .line 1788458
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/7Jg;->n:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    if-ne v0, v1, :cond_1

    invoke-static {p0}, LX/BUA;->h(LX/BUA;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1788459
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1788460
    :cond_1
    :try_start_1
    iget-object v0, p1, LX/7Jg;->a:Ljava/lang/String;

    sget-object v1, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    invoke-static {p0, v0, v1}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788461
    invoke-static {p0, p1}, LX/BUA;->b$redex0(LX/BUA;LX/7Jg;)V

    .line 1788462
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    .line 1788463
    iget-object v1, v0, LX/0tQ;->a:LX/0ad;

    sget-short v2, LX/0wh;->B:S

    invoke-static {v0}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1788464
    if-eqz v0, :cond_0

    iget-boolean v0, p1, LX/7Jg;->o:Z

    if-nez v0, :cond_0

    .line 1788465
    iget-object v0, p0, LX/BUA;->p:LX/BUi;

    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BUi;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1788466
    :catch_0
    move-exception v0

    .line 1788467
    :try_start_2
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Failed to schedule download"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1788468
    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-static {p0, v1, v0}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1788469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/BUA;LX/BUL;LX/7Jg;)V
    .locals 10

    .prologue
    .line 1788470
    iget-object v0, p1, LX/BUL;->o:Ljava/lang/String;

    .line 1788471
    iget-object v1, p0, LX/BUA;->n:LX/0tQ;

    const/4 v2, 0x0

    .line 1788472
    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/0tQ;->a:LX/0ad;

    sget-short v4, LX/0wh;->f:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 1788473
    if-nez v1, :cond_2

    .line 1788474
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1788475
    :cond_1
    :goto_0
    return-void

    .line 1788476
    :cond_2
    const/4 v3, 0x0

    .line 1788477
    const/4 v2, 0x0

    .line 1788478
    iget-object v1, p1, LX/BUL;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/BUL;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1788479
    if-nez v0, :cond_b

    .line 1788480
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    .line 1788481
    iget-object v1, v0, LX/0tQ;->a:LX/0ad;

    sget-char v4, LX/0wh;->g:C

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1788482
    move-object v1, v0

    .line 1788483
    :goto_1
    new-instance v0, LX/0AZ;

    invoke-direct {v0}, LX/0AZ;-><init>()V

    .line 1788484
    const/4 v4, 0x0

    .line 1788485
    :try_start_0
    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v6, p1, LX/BUL;->c:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1788486
    const-string v6, ""

    invoke-virtual {v0, v6, v5}, LX/0AZ;->a(Ljava/lang/String;Ljava/io/InputStream;)LX/0AY;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1788487
    :try_start_1
    invoke-virtual {v4}, LX/0AY;->b()I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_a

    .line 1788488
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, LX/0AY;->a(I)LX/0Am;

    move-result-object v0

    .line 1788489
    iget-object v5, v0, LX/0Am;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1788490
    iget-object v0, v0, LX/0Am;->c:Ljava/util/List;

    .line 1788491
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ak;

    .line 1788492
    iget-object v6, v0, LX/0Ak;->c:Ljava/util/List;

    .line 1788493
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    const-string v7, "video/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1788494
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 1788495
    if-eqz v1, :cond_4

    .line 1788496
    iget-object v7, v0, LX/0Ah;->c:LX/0AR;

    iget-object v7, v7, LX/0AR;->d:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    move-object v3, v0

    .line 1788497
    goto :goto_3

    .line 1788498
    :cond_4
    if-nez v3, :cond_5

    move-object v3, v0

    .line 1788499
    goto :goto_3

    .line 1788500
    :cond_5
    iget-object v7, v3, LX/0Ah;->c:LX/0AR;

    iget v7, v7, LX/0AR;->c:I

    iget-object v8, v0, LX/0Ah;->c:LX/0AR;

    iget v8, v8, LX/0AR;->c:I

    if-le v7, v8, :cond_9

    :goto_4
    move-object v3, v0

    .line 1788501
    goto :goto_3

    .line 1788502
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    iget-object v0, v0, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->b:Ljava/lang/String;

    const-string v7, "audio/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1788503
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    move-object v2, v0

    .line 1788504
    goto :goto_2

    :cond_7
    move-object v0, v3

    :goto_6
    move-object v1, v2

    move-object v2, v0

    .line 1788505
    :goto_7
    if-eqz v1, :cond_1

    instance-of v0, v1, LX/0Aw;

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    instance-of v0, v2, LX/0Aw;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1788506
    check-cast v0, LX/0Aw;

    iget-object v0, v0, LX/0Aw;->e:Landroid/net/Uri;

    iput-object v0, p2, LX/7Jg;->c:Landroid/net/Uri;

    move-object v0, v2

    .line 1788507
    check-cast v0, LX/0Aw;

    iget-object v0, v0, LX/0Aw;->e:Landroid/net/Uri;

    iput-object v0, p2, LX/7Jg;->b:Landroid/net/Uri;

    .line 1788508
    iget-object v0, v2, LX/0Ah;->c:LX/0AR;

    iget v0, v0, LX/0AR;->c:I

    int-to-long v6, v0

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, LX/0AY;->b(I)J

    move-result-wide v8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x1f40

    div-long/2addr v6, v8

    iput-wide v6, p2, LX/7Jg;->d:J

    .line 1788509
    iget-object v0, v1, LX/0Ah;->c:LX/0AR;

    iget v0, v0, LX/0AR;->c:I

    int-to-long v6, v0

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, LX/0AY;->b(I)J

    move-result-wide v8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x1f40

    div-long/2addr v6, v8

    iput-wide v6, p2, LX/7Jg;->e:J

    .line 1788510
    iget-object v0, v2, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->a:Ljava/lang/String;

    iput-object v0, p2, LX/7Jg;->i:Ljava/lang/String;

    .line 1788511
    iget-object v0, v1, LX/0Ah;->c:LX/0AR;

    iget-object v0, v0, LX/0AR;->a:Ljava/lang/String;

    iput-object v0, p2, LX/7Jg;->j:Ljava/lang/String;

    .line 1788512
    iget-object v0, p1, LX/BUL;->c:Ljava/lang/String;

    iput-object v0, p2, LX/7Jg;->q:Ljava/lang/String;

    .line 1788513
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0AY;->b(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x1

    iget-wide v4, p2, LX/7Jg;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x2

    iget-object v4, v2, LX/0Ah;->c:LX/0AR;

    iget v4, v4, LX/0AR;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, v1, LX/0Ah;->c:LX/0AR;

    iget v4, v4, LX/0AR;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x4

    iget-wide v4, p2, LX/7Jg;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x5

    iget-object v2, v2, LX/0Ah;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    aput-object v2, v0, v3

    const/4 v2, 0x6

    iget-object v1, v1, LX/0Ah;->c:LX/0AR;

    iget-object v1, v1, LX/0AR;->a:Ljava/lang/String;

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 1788514
    :catch_0
    move-exception v0

    move-object v1, v4

    .line 1788515
    :goto_8
    sget-object v4, LX/BUA;->d:Ljava/lang/String;

    const-string v5, "Exception is thrown"

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v1

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_7

    .line 1788516
    :catch_1
    move-exception v0

    move-object v1, v4

    goto :goto_8

    :cond_8
    move-object v0, v2

    goto/16 :goto_5

    :cond_9
    move-object v0, v3

    goto/16 :goto_4

    :cond_a
    move-object v0, v3

    goto/16 :goto_6

    :cond_b
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V
    .locals 3

    .prologue
    .line 1788533
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    .line 1788534
    iget-object v1, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v1, p1, p2}, LX/19w;->a(Ljava/lang/String;LX/1A0;)LX/7Jg;

    .line 1788535
    sget-object v1, LX/BU2;->a:[I

    invoke-virtual {p2}, LX/1A0;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1788536
    :goto_0
    :pswitch_0
    return-void

    .line 1788537
    :pswitch_1
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    sget-object v1, LX/7Jd;->DOWNLOAD_COMPLETED:LX/7Jd;

    invoke-virtual {v0, p1, v1}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    goto :goto_0

    .line 1788538
    :pswitch_2
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    sget-object v1, LX/7Jd;->DOWNLOAD_PAUSED:LX/7Jd;

    invoke-virtual {v0, p1, v1}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    goto :goto_0

    .line 1788539
    :pswitch_3
    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v0, v1, :cond_0

    .line 1788540
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    sget-object v1, LX/7Jd;->DOWNLOAD_STOPPED:LX/7Jd;

    invoke-virtual {v0, p1, v1}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    goto :goto_0

    .line 1788541
    :cond_0
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    sget-object v1, LX/7Jd;->DOWNLOAD_QUEUED:LX/7Jd;

    invoke-virtual {v0, p1, v1}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    goto :goto_0

    .line 1788542
    :pswitch_4
    iget-object v0, p0, LX/BUA;->k:LX/19v;

    sget-object v1, LX/7Jd;->DOWNLOAD_STARTED:LX/7Jd;

    invoke-virtual {v0, p1, v1}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a$redex0(LX/BUA;Ljava/lang/String;LX/1zt;)V
    .locals 3

    .prologue
    .line 1788589
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, Lcom/facebook/video/downloadmanager/DownloadManager$24;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/downloadmanager/DownloadManager$24;-><init>(LX/BUA;Ljava/lang/String;LX/1zt;)V

    const v2, -0x17686c2e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1788590
    return-void
.end method

.method public static a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1788581
    iget-object v0, p0, LX/BUA;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788582
    iget-object v0, p0, LX/BUA;->m:LX/1Sc;

    invoke-virtual {v0, p1}, LX/1Sc;->d(Ljava/lang/String;)V

    .line 1788583
    iget-object v1, p0, LX/BUA;->k:LX/19v;

    instance-of v0, p2, LX/BTp;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, LX/BTp;

    iget-object v0, v0, LX/BTp;->mExceptionCode:LX/BTo;

    invoke-virtual {v0}, LX/BTo;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, LX/19v;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1788584
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v0, v1, :cond_0

    .line 1788585
    iget-object v0, p0, LX/BUA;->o:LX/15V;

    invoke-virtual {v0, p2}, LX/15V;->b(Ljava/lang/Throwable;)V

    .line 1788586
    sget-object v0, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    invoke-static {p0, p1, v0}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788587
    :cond_0
    return-void

    .line 1788588
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/BUA;
    .locals 20

    .prologue
    .line 1788579
    new-instance v0, LX/BUA;

    invoke-static/range {p0 .. p0}, LX/15V;->a(LX/0QB;)LX/15V;

    move-result-object v1

    check-cast v1, LX/15V;

    invoke-static/range {p0 .. p0}, LX/BUi;->a(LX/0QB;)LX/BUi;

    move-result-object v2

    check-cast v2, LX/BUi;

    invoke-static/range {p0 .. p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v3

    check-cast v3, LX/16I;

    invoke-static/range {p0 .. p0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v4

    check-cast v4, LX/19w;

    invoke-static/range {p0 .. p0}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(LX/0QB;)Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-static/range {p0 .. p0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v6

    check-cast v6, LX/16U;

    invoke-static/range {p0 .. p0}, LX/1fV;->a(LX/0QB;)LX/1fW;

    move-result-object v7

    check-cast v7, LX/1fW;

    invoke-static/range {p0 .. p0}, LX/BTn;->a(LX/0QB;)LX/BTn;

    move-result-object v8

    check-cast v8, LX/BTn;

    invoke-static/range {p0 .. p0}, LX/1Sc;->a(LX/0QB;)LX/1Sc;

    move-result-object v9

    check-cast v9, LX/1Sc;

    invoke-static/range {p0 .. p0}, LX/19v;->a(LX/0QB;)LX/19v;

    move-result-object v10

    check-cast v10, LX/19v;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v11

    check-cast v11, LX/0tQ;

    invoke-static/range {p0 .. p0}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->a(LX/0QB;)Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    move-result-object v12

    check-cast v12, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-static/range {p0 .. p0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v13

    check-cast v13, LX/20h;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v14

    check-cast v14, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v15

    check-cast v15, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/BUF;->a(LX/0QB;)LX/BUF;

    move-result-object v16

    check-cast v16, LX/BUF;

    invoke-static/range {p0 .. p0}, LX/BYc;->a(LX/0QB;)LX/BYc;

    move-result-object v17

    check-cast v17, LX/BYc;

    invoke-static/range {p0 .. p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v18

    check-cast v18, LX/0Xp;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v19

    check-cast v19, LX/0kb;

    invoke-direct/range {v0 .. v19}, LX/BUA;-><init>(LX/15V;LX/BUi;LX/16I;LX/19w;Lcom/facebook/video/downloadmanager/VideoDownloadHandler;LX/16U;LX/1fW;LX/BTn;LX/1Sc;LX/19v;LX/0tQ;Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;LX/20h;LX/0bH;LX/0SG;LX/BUF;LX/BYc;LX/0Xp;LX/0kb;)V

    .line 1788580
    return-object v0
.end method

.method public static b(LX/BUA;LX/BUL;)V
    .locals 12

    .prologue
    .line 1788567
    :try_start_0
    iget-object v0, p1, LX/BUL;->a:Landroid/net/Uri;

    const-string v1, "remote-uri"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1788568
    if-nez v5, :cond_0

    .line 1788569
    iget-object v0, p1, LX/BUL;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1788570
    :cond_0
    iget-object v0, p0, LX/BUA;->m:LX/1Sc;

    iget-object v1, p1, LX/BUL;->b:Ljava/lang/String;

    iget-object v2, p1, LX/BUL;->g:Ljava/lang/String;

    iget-object v3, p1, LX/BUL;->h:Ljava/lang/String;

    iget-object v4, p1, LX/BUL;->i:Ljava/lang/String;

    iget-wide v6, p1, LX/BUL;->e:J

    iget-object v8, p1, LX/BUL;->j:Ljava/lang/String;

    iget-object v9, p1, LX/BUL;->k:Ljava/lang/String;

    iget-object v10, p1, LX/BUL;->l:Ljava/lang/String;

    iget-object v11, p1, LX/BUL;->m:Ljava/lang/String;

    invoke-virtual/range {v0 .. v11}, LX/1Sc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788571
    return-void

    .line 1788572
    :catch_0
    move-exception v0

    .line 1788573
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Optimistic update to saved2db failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788574
    iget-object v1, p0, LX/BUA;->k:LX/19v;

    iget-object v2, p1, LX/BUL;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/19v;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1788575
    iget-object v1, p0, LX/BUA;->o:LX/15V;

    invoke-virtual {v1, v0}, LX/15V;->b(Ljava/lang/Throwable;)V

    .line 1788576
    iget-object v1, p1, LX/BUL;->b:Ljava/lang/String;

    sget-object v2, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    invoke-static {p0, v1, v2}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788577
    iget-object v1, p1, LX/BUL;->b:Ljava/lang/String;

    invoke-static {p0, v1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    .line 1788578
    throw v0
.end method

.method public static b(LX/BUA;Ljava/lang/String;LX/7Jb;)V
    .locals 1

    .prologue
    .line 1788561
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 1788562
    if-nez v0, :cond_0

    .line 1788563
    :goto_0
    return-void

    .line 1788564
    :cond_0
    invoke-direct {p0, p1}, LX/BUA;->f(Ljava/lang/String;)V

    .line 1788565
    invoke-static {p0, v0, p2}, LX/BUA;->a(LX/BUA;LX/7Jg;LX/7Jb;)V

    .line 1788566
    invoke-static {p0, p1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized b(LX/BUA;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1788555
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/BUA;->f(Ljava/lang/String;)V

    .line 1788556
    if-eqz p2, :cond_0

    sget-object v0, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    :goto_0
    invoke-static {p0, p1, v0}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788557
    invoke-static {p0, p1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788558
    monitor-exit p0

    return-void

    .line 1788559
    :cond_0
    :try_start_1
    sget-object v0, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1788560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/BUA;LX/7Jg;)V
    .locals 8

    .prologue
    .line 1788543
    monitor-enter p0

    :try_start_0
    iget-wide v0, p1, LX/7Jg;->d:J

    iget-wide v2, p1, LX/7Jg;->f:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1788544
    new-instance v4, Ljava/io/File;

    iget-object v0, p1, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1788545
    iget-object v1, p0, LX/BUA;->f:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    iget-object v2, p1, LX/7Jg;->b:Landroid/net/Uri;

    iget-object v3, p1, LX/7Jg;->a:Ljava/lang/String;

    iget-object v5, p0, LX/BUA;->c:LX/BTy;

    iget-wide v6, p1, LX/7Jg;->d:J

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/io/File;LX/BTy;J)LX/BUJ;

    move-result-object v0

    move-object v1, v0

    .line 1788546
    :goto_0
    new-instance v2, LX/BUM;

    iget-wide v4, p1, LX/7Jg;->f:J

    invoke-direct {v2, v4, v5, v1}, LX/BUM;-><init>(JLX/BUJ;)V

    .line 1788547
    iget-object v0, p0, LX/BUA;->g:Ljava/util/HashMap;

    iget-object v3, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1788548
    iget-object v0, p0, LX/BUA;->g:Ljava/util/HashMap;

    iget-object v3, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788549
    invoke-virtual {v1}, LX/BUJ;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/BTu;

    invoke-direct {v2, p0, p1, v1}, LX/BTu;-><init>(LX/BUA;LX/7Jg;LX/BUJ;)V

    invoke-static {v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788550
    monitor-exit p0

    return-void

    .line 1788551
    :cond_0
    :try_start_1
    new-instance v4, Ljava/io/File;

    iget-object v0, p1, LX/7Jg;->k:Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1788552
    iget-object v1, p0, LX/BUA;->f:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    iget-object v2, p1, LX/7Jg;->c:Landroid/net/Uri;

    iget-object v3, p1, LX/7Jg;->a:Ljava/lang/String;

    iget-object v5, p0, LX/BUA;->c:LX/BTy;

    iget-wide v6, p1, LX/7Jg;->d:J

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/io/File;LX/BTy;J)LX/BUJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1788553
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1788554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b$redex0(LX/BUA;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1788517
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 1788518
    :try_start_0
    iget-boolean v1, v0, LX/7Jg;->o:Z

    if-eqz v1, :cond_1

    .line 1788519
    iget-object v1, p0, LX/BUA;->e:LX/19w;

    iget-object v3, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/19w;->n(Ljava/lang/String;)LX/7Jh;

    move-result-object v1

    .line 1788520
    if-nez v1, :cond_0

    .line 1788521
    iget-object v1, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/BUA;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1788522
    :catch_0
    move-exception v1

    .line 1788523
    sget-object v3, LX/BUA;->d:Ljava/lang/String;

    const-string v4, "Rescheduling download exception for %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, LX/7Jg;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788524
    iget-object v0, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    goto :goto_0

    .line 1788525
    :cond_0
    :try_start_1
    invoke-static {v1}, LX/15V;->a(LX/7Jh;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1788526
    iget-object v4, p0, LX/BUA;->p:LX/BUi;

    iget-object v5, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v1, v3}, LX/BUi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1788527
    :cond_1
    invoke-static {p0, v0}, LX/BUA;->a$redex0(LX/BUA;LX/7Jg;)V

    .line 1788528
    iget-object v1, p0, LX/BUA;->o:LX/15V;

    iget-object v3, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/15V;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1788529
    :cond_2
    return-void
.end method

.method public static c(LX/BUA;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1788530
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1788531
    :goto_0
    return-void

    .line 1788532
    :cond_0
    iget-object v0, p0, LX/BUA;->t:LX/BUF;

    new-instance v1, LX/BTq;

    invoke-direct {v1, p0}, LX/BTq;-><init>(LX/BUA;)V

    invoke-virtual {v0, p1, v1}, LX/BUF;->a(Ljava/util/List;LX/BTq;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1788378
    iget-object v0, p0, LX/BUA;->v:LX/0Xp;

    new-instance v1, LX/0Yd;

    const-string v2, "com.facebook.zero.offpeakdownload.START_OFFPEAK_DOWNLOAD_ACTION"

    new-instance v3, LX/BU3;

    invoke-direct {v3, p0}, LX/BU3;-><init>(LX/BUA;)V

    invoke-direct {v1, v2, v3}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.zero.offpeakdownload.START_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1788379
    iget-object v0, p0, LX/BUA;->v:LX/0Xp;

    new-instance v1, LX/0Yd;

    const-string v2, "com.facebook.zero.offpeakdownload.STOP_OFFPEAK_DOWNLOAD_ACTION"

    new-instance v3, LX/BU4;

    invoke-direct {v3, p0}, LX/BU4;-><init>(LX/BUA;)V

    invoke-direct {v1, v2, v3}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.zero.offpeakdownload.STOP_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1788380
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1788292
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, LX/BU5;

    invoke-direct {v1, p0}, LX/BU5;-><init>(LX/BUA;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788293
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1788296
    iget-object v0, p0, LX/BUA;->l:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/video/downloadmanager/DownloadManager$7;

    invoke-direct {v2, p0}, Lcom/facebook/video/downloadmanager/DownloadManager$7;-><init>(LX/BUA;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 1788297
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1788298
    iget-object v0, p0, LX/BUA;->l:LX/16I;

    sget-object v1, LX/1Ed;->NO_INTERNET:LX/1Ed;

    new-instance v2, Lcom/facebook/video/downloadmanager/DownloadManager$8;

    invoke-direct {v2, p0}, Lcom/facebook/video/downloadmanager/DownloadManager$8;-><init>(LX/BUA;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 1788299
    :cond_0
    return-void
.end method

.method private declared-synchronized f(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1788300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUA;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUM;

    .line 1788301
    if-eqz v0, :cond_1

    iget-object v0, v0, LX/BUM;->b:LX/BUJ;

    .line 1788302
    :goto_0
    if-eqz v0, :cond_0

    .line 1788303
    iget-object v1, v0, LX/BUJ;->a:LX/1j2;

    invoke-virtual {v1}, LX/1j2;->b()V

    .line 1788304
    iget-object v1, v0, LX/BUJ;->a:LX/1j2;

    .line 1788305
    iget-object v2, v1, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v2

    .line 1788306
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788307
    :try_start_1
    invoke-virtual {v0}, LX/BUJ;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, 0x17081004

    invoke-static {v0, v2, v3, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788308
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 1788309
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1788310
    :catch_0
    move-exception v0

    .line 1788311
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1788312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1788313
    :catch_1
    move-exception v0

    .line 1788314
    :try_start_3
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Download already completed with an exception"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1788315
    :catch_2
    goto :goto_1
.end method

.method public static g(LX/BUA;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1788316
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 1788317
    iget-object v1, p0, LX/BUA;->h:LX/16U;

    new-instance v2, LX/1ub;

    iget-object v3, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v3, p1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v3

    invoke-direct {v2, p1, v3}, LX/1ub;-><init>(Ljava/lang/String;LX/2fs;)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 1788318
    if-eqz v0, :cond_0

    iget-boolean v1, v0, LX/7Jg;->o:Z

    if-nez v1, :cond_0

    iget-object v0, v0, LX/7Jg;->l:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v0, v1, :cond_0

    .line 1788319
    iget-object v0, p0, LX/BUA;->j:LX/BTn;

    .line 1788320
    new-instance v1, LX/4JI;

    invoke-direct {v1}, LX/4JI;-><init>()V

    .line 1788321
    const-string v2, "video_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1788322
    move-object v1, v1

    .line 1788323
    new-instance v2, LX/BUX;

    invoke-direct {v2}, LX/BUX;-><init>()V

    move-object v2, v2

    .line 1788324
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1788325
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1788326
    iget-object v2, v0, LX/BTn;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/BTm;

    invoke-direct {v2, v0, p1}, LX/BTm;-><init>(LX/BTn;Ljava/lang/String;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1788327
    :cond_0
    return-void
.end method

.method public static h(LX/BUA;)Z
    .locals 1

    .prologue
    .line 1788328
    iget-object v0, p0, LX/BUA;->w:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BUA;->w:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUA;->l:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2ft;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1788329
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, LX/BU6;

    invoke-direct {v1, p0, p1}, LX/BU6;-><init>(LX/BUA;LX/2ft;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1788330
    iget-wide v0, p1, LX/BUL;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1788331
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video size must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1788332
    :cond_0
    iget-object v0, p1, LX/BUL;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788333
    iget-object v0, p1, LX/BUL;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788334
    iget-object v0, p1, LX/BUL;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788335
    new-instance v0, LX/BTx;

    invoke-direct {v0, p0, p1}, LX/BTx;-><init>(LX/BUA;LX/BUL;)V

    .line 1788336
    iget-object v1, p0, LX/BUA;->i:LX/1fW;

    invoke-virtual {v1, v0}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1788337
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/BTz;

    invoke-direct {v0, p0, p1, p2}, LX/BTz;-><init>(LX/BUA;Ljava/lang/String;LX/7Jb;)V

    .line 1788338
    iget-object v1, p0, LX/BUA;->i:LX/1fW;

    invoke-virtual {v1, v0}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1788339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1788294
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, LX/BU7;

    invoke-direct {v1, p0}, LX/BU7;-><init>(LX/BUA;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788295
    return-void
.end method

.method public final a(LX/7Jb;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1788340
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, v1, v1}, LX/19w;->a(ZZ)LX/0Px;

    move-result-object v2

    .line 1788341
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1788342
    invoke-static {p0, v0, p1}, LX/BUA;->b(LX/BUA;Ljava/lang/String;LX/7Jb;)V

    .line 1788343
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1788344
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1788345
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->B()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1788346
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1788347
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1788348
    invoke-static {p0, v0}, LX/BUA;->c(LX/BUA;Ljava/util/List;)V

    .line 1788349
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 1788350
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->B()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1788351
    :goto_0
    return-void

    .line 1788352
    :cond_0
    new-instance v0, LX/7Jh;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-static {p2, v2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/7Jh;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 1788353
    iget-object v1, p0, LX/BUA;->i:LX/1fW;

    new-instance v2, Lcom/facebook/video/downloadmanager/DownloadManager$20;

    invoke-direct {v2, p0, v0}, Lcom/facebook/video/downloadmanager/DownloadManager$20;-><init>(LX/BUA;LX/7Jh;)V

    const v0, 0x48531908    # 216164.12f

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1788354
    new-instance v0, LX/BTv;

    invoke-direct {v0, p0, p1, p2}, LX/BTv;-><init>(LX/BUA;Ljava/lang/String;Z)V

    .line 1788355
    iget-object v1, p0, LX/BUA;->i:LX/1fW;

    invoke-virtual {v1, v0}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788356
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Jh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1788357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1788358
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jh;

    .line 1788359
    iget-object v3, p0, LX/BUA;->s:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, v0, LX/7Jh;->d:J

    sub-long/2addr v4, v6

    iget-object v3, p0, LX/BUA;->n:LX/0tQ;

    .line 1788360
    iget-object v8, v3, LX/0tQ;->a:LX/0ad;

    sget-wide v10, LX/0wh;->F:J

    const-wide/32 v12, 0x5265c00

    invoke-interface {v8, v10, v11, v12, v13}, LX/0ad;->a(JJ)J

    move-result-wide v8

    move-wide v6, v8

    .line 1788361
    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 1788362
    iget-object v0, v0, LX/7Jh;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1788363
    :cond_1
    invoke-static {p0, v1}, LX/BUA;->c(LX/BUA;Ljava/util/List;)V

    .line 1788364
    return-void
.end method

.method public final b(LX/2ft;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1788365
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, LX/BU0;

    invoke-direct {v1, p0, p1}, LX/BU0;-><init>(LX/BUA;LX/2ft;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1788366
    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->B()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1788367
    :cond_0
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, Lcom/facebook/video/downloadmanager/DownloadManager$11;

    invoke-direct {v1, p0}, Lcom/facebook/video/downloadmanager/DownloadManager$11;-><init>(LX/BUA;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788368
    :cond_1
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1788369
    iget-object v0, p0, LX/BUA;->i:LX/1fW;

    new-instance v1, LX/BTt;

    invoke-direct {v1, p0, p1}, LX/BTt;-><init>(LX/BUA;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1788370
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/BU1;

    invoke-direct {v0, p0, p1}, LX/BU1;-><init>(LX/BUA;Ljava/lang/String;)V

    .line 1788371
    iget-object v1, p0, LX/BUA;->i:LX/1fW;

    invoke-virtual {v1, v0}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1788372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1788373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->k(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788374
    monitor-exit p0

    return-void

    .line 1788375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
