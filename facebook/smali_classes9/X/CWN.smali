.class public LX/CWN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CWI;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/23P;

.field private final c:Ljava/util/Calendar;

.field public final d:Ljava/text/SimpleDateFormat;

.field private final e:Ljava/text/SimpleDateFormat;

.field private final f:Ljava/text/SimpleDateFormat;

.field private final g:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(LX/0Or;LX/23P;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/23P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1908002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1908003
    iput-object p1, p0, LX/CWN;->a:LX/0Or;

    .line 1908004
    iput-object p2, p0, LX/CWN;->b:LX/23P;

    .line 1908005
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    .line 1908006
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd"

    iget-object v0, p0, LX/CWN;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWN;->d:Ljava/text/SimpleDateFormat;

    .line 1908007
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "d"

    iget-object v0, p0, LX/CWN;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWN;->f:Ljava/text/SimpleDateFormat;

    .line 1908008
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM"

    iget-object v0, p0, LX/CWN;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWN;->g:Ljava/text/SimpleDateFormat;

    .line 1908009
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE"

    iget-object v0, p0, LX/CWN;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWN;->e:Ljava/text/SimpleDateFormat;

    .line 1908010
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1908001
    iget-object v0, p0, LX/CWN;->b:LX/23P;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/CWF;ILjava/util/Date;)LX/CWF;
    .locals 6

    .prologue
    .line 1907972
    const/4 v0, 0x7

    new-array v1, v0, [LX/CWE;

    .line 1907973
    const/4 v0, 0x0

    :goto_0
    int-to-long v2, v0

    const-wide/16 v4, 0x7

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 1907974
    if-ne v0, p2, :cond_0

    .line 1907975
    sget-object v2, LX/CWE;->SELECTED:LX/CWE;

    aput-object v2, v1, v0

    .line 1907976
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1907977
    :cond_0
    iget-object v2, p1, LX/CWF;->d:[LX/CWE;

    aget-object v2, v2, v0

    sget-object v3, LX/CWE;->UNAVAILABLE:LX/CWE;

    invoke-virtual {v2, v3}, LX/CWE;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1907978
    sget-object v2, LX/CWE;->UNAVAILABLE:LX/CWE;

    aput-object v2, v1, v0

    goto :goto_1

    .line 1907979
    :cond_1
    sget-object v2, LX/CWE;->AVAILABLE:LX/CWE;

    aput-object v2, v1, v0

    goto :goto_1

    .line 1907980
    :cond_2
    new-instance v0, LX/CWG;

    invoke-direct {v0}, LX/CWG;-><init>()V

    iget-object v2, p1, LX/CWF;->e:Ljava/lang/String;

    .line 1907981
    iput-object v2, v0, LX/CWG;->e:Ljava/lang/String;

    .line 1907982
    move-object v0, v0

    .line 1907983
    iget-boolean v2, p1, LX/CWF;->g:Z

    .line 1907984
    iput-boolean v2, v0, LX/CWG;->g:Z

    .line 1907985
    move-object v0, v0

    .line 1907986
    iget-boolean v2, p1, LX/CWF;->f:Z

    .line 1907987
    iput-boolean v2, v0, LX/CWG;->f:Z

    .line 1907988
    move-object v0, v0

    .line 1907989
    iget-object v2, p1, LX/CWF;->a:[Ljava/lang/String;

    .line 1907990
    iput-object v2, v0, LX/CWG;->a:[Ljava/lang/String;

    .line 1907991
    move-object v0, v0

    .line 1907992
    iget-object v2, p1, LX/CWF;->b:[Ljava/lang/String;

    .line 1907993
    iput-object v2, v0, LX/CWG;->b:[Ljava/lang/String;

    .line 1907994
    move-object v0, v0

    .line 1907995
    iget-object v2, p1, LX/CWF;->c:[Ljava/lang/String;

    .line 1907996
    iput-object v2, v0, LX/CWG;->c:[Ljava/lang/String;

    .line 1907997
    move-object v0, v0

    .line 1907998
    iput-object v1, v0, LX/CWG;->d:[LX/CWE;

    .line 1907999
    move-object v0, v0

    .line 1908000
    invoke-virtual {v0}, LX/CWG;->a()LX/CWF;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Date;Ljava/util/TreeMap;Ljava/util/Date;Ljava/util/Date;)LX/CWF;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/CU5;",
            ">;>;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "LX/CWF;"
        }
    .end annotation

    .prologue
    .line 1908011
    new-instance v1, LX/CWG;

    invoke-direct {v1}, LX/CWG;-><init>()V

    .line 1908012
    iget-object v0, p0, LX/CWN;->g:Ljava/text/SimpleDateFormat;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CWN;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1908013
    iput-object v0, v1, LX/CWG;->e:Ljava/lang/String;

    .line 1908014
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    .line 1908015
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908016
    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    const-wide/16 v6, 0x7

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 1908017
    iget-object v3, p0, LX/CWN;->e:Ljava/text/SimpleDateFormat;

    iget-object v4, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/CWN;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1908018
    iget-object v3, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 1908019
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1908020
    :cond_0
    iput-object v2, v1, LX/CWG;->a:[Ljava/lang/String;

    .line 1908021
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    .line 1908022
    const/4 v0, 0x7

    new-array v3, v0, [Ljava/lang/String;

    .line 1908023
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908024
    const/4 v0, 0x0

    :goto_1
    int-to-long v4, v0

    const-wide/16 v6, 0x7

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 1908025
    iget-object v4, p0, LX/CWN;->f:Ljava/text/SimpleDateFormat;

    iget-object v5, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 1908026
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 1908027
    iget-object v4, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v5, 0x5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 1908028
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1908029
    :cond_1
    const/4 v0, 0x7

    new-array v4, v0, [LX/CWE;

    .line 1908030
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908031
    const/4 v0, 0x0

    :goto_2
    int-to-long v6, v0

    const-wide/16 v8, 0x7

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    .line 1908032
    iget-object v5, p0, LX/CWN;->d:Ljava/text/SimpleDateFormat;

    iget-object v6, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1908033
    sget-object v5, LX/CWE;->AVAILABLE:LX/CWE;

    aput-object v5, v4, v0

    .line 1908034
    :goto_3
    iget-object v5, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v6, 0x5

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 1908035
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1908036
    :cond_2
    sget-object v5, LX/CWE;->UNAVAILABLE:LX/CWE;

    aput-object v5, v4, v0

    goto :goto_3

    .line 1908037
    :cond_3
    iput-object v2, v1, LX/CWG;->b:[Ljava/lang/String;

    .line 1908038
    move-object v0, v1

    .line 1908039
    iput-object v3, v0, LX/CWG;->c:[Ljava/lang/String;

    .line 1908040
    move-object v0, v0

    .line 1908041
    iput-object v4, v0, LX/CWG;->d:[LX/CWE;

    .line 1908042
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908043
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, -0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1908044
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const/4 v0, 0x1

    .line 1908045
    :goto_4
    iput-boolean v0, v1, LX/CWG;->f:Z

    .line 1908046
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908047
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1908048
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    const/4 v0, 0x1

    .line 1908049
    :goto_5
    iput-boolean v0, v1, LX/CWG;->g:Z

    .line 1908050
    invoke-virtual {v1}, LX/CWG;->a()LX/CWF;

    move-result-object v0

    return-object v0

    .line 1908051
    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    .line 1908052
    :cond_7
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 1907964
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907965
    sget-object v0, LX/CWM;->a:[I

    invoke-virtual {p2}, LX/CWH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1907966
    const/4 v0, 0x0

    const-string v1, "Unsupported calendar shift direction"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1907967
    const/4 v0, 0x0

    .line 1907968
    :goto_0
    return-object v0

    .line 1907969
    :pswitch_0
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v1, -0x7

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 1907970
    :goto_1
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1907971
    :pswitch_1
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;
    .locals 3

    .prologue
    .line 1907958
    if-eqz p2, :cond_1

    .line 1907959
    :goto_0
    sget-object v0, LX/CWH;->FORWARD:LX/CWH;

    invoke-virtual {p0, p1, v0}, LX/CWN;->a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object v0

    .line 1907960
    invoke-virtual {p2, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/CWN;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CWN;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1907961
    if-eqz v0, :cond_1

    .line 1907962
    sget-object v0, LX/CWH;->FORWARD:LX/CWH;

    invoke-virtual {p0, p1, v0}, LX/CWN;->a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object p1

    goto :goto_0

    .line 1907963
    :cond_1
    return-object p1

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/util/Date;Ljava/util/Date;)I
    .locals 6

    .prologue
    .line 1907957
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final b(Ljava/util/Date;LX/CWH;)Ljava/util/Date;
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 1907949
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907950
    sget-object v0, LX/CWM;->a:[I

    invoke-virtual {p2}, LX/CWH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1907951
    const/4 v0, 0x0

    const-string v1, "Unsupported calendar shift direction"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1907952
    const/4 v0, 0x0

    .line 1907953
    :goto_0
    return-object v0

    .line 1907954
    :pswitch_0
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v1, -0x7

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 1907955
    :goto_1
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1907956
    :pswitch_1
    iget-object v0, p0, LX/CWN;->c:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
