.class public final LX/Agh;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 0

    .prologue
    .line 1701475
    iput-object p1, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 1

    .prologue
    .line 1701476
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1701477
    iget-object v0, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1701478
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1701479
    iget-object v0, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1701480
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 2

    .prologue
    .line 1701481
    iget-object v1, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    if-eqz p2, :cond_0

    sget-object v0, LX/Agq;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/Agq;

    :goto_0
    invoke-static {v1, v0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a$redex0(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/Agq;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1701482
    return-void

    .line 1701483
    :cond_0
    sget-object v0, LX/Agq;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/Agq;

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1701484
    iget-object v0, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Agh;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1701485
    return-void
.end method
