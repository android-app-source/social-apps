.class public LX/B4H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/B4H;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/8GV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/5vm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/B4D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741434
    return-void
.end method

.method public static a(LX/0QB;)LX/B4H;
    .locals 10

    .prologue
    .line 1741435
    sget-object v0, LX/B4H;->h:LX/B4H;

    if-nez v0, :cond_1

    .line 1741436
    const-class v1, LX/B4H;

    monitor-enter v1

    .line 1741437
    :try_start_0
    sget-object v0, LX/B4H;->h:LX/B4H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1741438
    if-eqz v2, :cond_0

    .line 1741439
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1741440
    new-instance v3, LX/B4H;

    invoke-direct {v3}, LX/B4H;-><init>()V

    .line 1741441
    const/16 v4, 0x1032

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x36f4

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/8GV;->b(LX/0QB;)LX/8GV;

    move-result-object v7

    check-cast v7, LX/8GV;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {v0}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v9

    check-cast v9, LX/5vm;

    invoke-static {v0}, LX/B4D;->a(LX/0QB;)LX/B4D;

    move-result-object p0

    check-cast p0, LX/B4D;

    .line 1741442
    iput-object v4, v3, LX/B4H;->a:LX/0Or;

    iput-object v5, v3, LX/B4H;->b:LX/0Or;

    iput-object v6, v3, LX/B4H;->c:Landroid/content/Context;

    iput-object v7, v3, LX/B4H;->d:LX/8GV;

    iput-object v8, v3, LX/B4H;->e:LX/0TD;

    iput-object v9, v3, LX/B4H;->f:LX/5vm;

    iput-object p0, v3, LX/B4H;->g:LX/B4D;

    .line 1741443
    move-object v0, v3

    .line 1741444
    sput-object v0, LX/B4H;->h:LX/B4H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1741445
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1741446
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1741447
    :cond_1
    sget-object v0, LX/B4H;->h:LX/B4H;

    return-object v0

    .line 1741448
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1741449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Landroid/content/Intent;
    .locals 9
    .param p3    # Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741450
    iget-object v0, p0, LX/B4H;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SF;

    .line 1741451
    new-instance v1, LX/5Rw;

    invoke-direct {v1}, LX/5Rw;-><init>()V

    sget-object v2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v1, v2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v1

    invoke-virtual {v1}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v8

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p5

    move/from16 v6, p7

    move/from16 v7, p8

    .line 1741452
    invoke-virtual/range {v1 .. v7}, LX/B4H;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    .line 1741453
    new-instance v2, LX/5SJ;

    invoke-direct {v2, v1}, LX/5SJ;-><init>(Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    invoke-virtual {v2, p2, p1}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v1

    invoke-virtual {v1}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    .line 1741454
    iget-object v2, p0, LX/B4H;->c:Landroid/content/Context;

    invoke-interface {v0, v2, v1, v8}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
    .locals 6
    .param p1    # Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1741455
    iget-object v0, p0, LX/B4H;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 1741456
    sget-short v2, LX/B3X;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    const/4 v0, 0x2

    .line 1741457
    :goto_0
    new-instance v1, LX/5SJ;

    invoke-direct {v1}, LX/5SJ;-><init>()V

    .line 1741458
    iput-object p2, v1, LX/5SJ;->l:LX/5QV;

    .line 1741459
    move-object v1, v1

    .line 1741460
    iput-object p1, v1, LX/5SJ;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1741461
    move-object v1, v1

    .line 1741462
    sget-object v2, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v2}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v2

    .line 1741463
    iput-object v2, v1, LX/5SJ;->c:Ljava/lang/String;

    .line 1741464
    move-object v1, v1

    .line 1741465
    iget-object v2, p0, LX/B4H;->g:LX/B4D;

    invoke-virtual {v2, p3, p4}, LX/B4D;->a(J)J

    move-result-wide v2

    .line 1741466
    iput-wide v2, v1, LX/5SJ;->e:J

    .line 1741467
    move-object v1, v1

    .line 1741468
    const/4 v2, 0x1

    .line 1741469
    iput-boolean v2, v1, LX/5SJ;->o:Z

    .line 1741470
    move-object v1, v1

    .line 1741471
    const-string v2, "profile_picture_overlay"

    .line 1741472
    iput-object v2, v1, LX/5SJ;->j:Ljava/lang/String;

    .line 1741473
    move-object v1, v1

    .line 1741474
    iput v0, v1, LX/5SJ;->q:I

    .line 1741475
    move-object v0, v1

    .line 1741476
    iput-boolean p6, v0, LX/5SJ;->r:Z

    .line 1741477
    move-object v0, v0

    .line 1741478
    invoke-virtual {v0}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 1741479
    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1741480
    iget-object v0, p0, LX/B4H;->e:LX/0TD;

    new-instance v1, LX/B4E;

    invoke-direct {v1, p0, p1}, LX/B4E;-><init>(LX/B4H;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1741481
    new-instance v1, LX/B4G;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/B4G;-><init>(LX/B4H;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
