.class public LX/CXD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CU0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CTS;)V
    .locals 2

    .prologue
    .line 1909620
    iget-object v0, p1, LX/CTS;->k:Ljava/util/ArrayList;

    iget-object v1, p1, LX/CTS;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, LX/CXD;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1909621
    return-void
.end method

.method public constructor <init>(LX/CTT;)V
    .locals 2

    .prologue
    .line 1909622
    iget-object v0, p1, LX/CTT;->l:Ljava/util/ArrayList;

    iget-object v1, p1, LX/CTT;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, LX/CXD;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1909623
    return-void
.end method

.method private constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "LX/CU0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1909624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1909625
    iput-object p2, p0, LX/CXD;->b:Ljava/util/ArrayList;

    .line 1909626
    iput-object p1, p0, LX/CXD;->a:Ljava/util/ArrayList;

    .line 1909627
    return-void
.end method

.method public static a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V
    .locals 1

    .prologue
    .line 1909628
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909629
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909630
    invoke-virtual {p2, p0, p1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1909631
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909632
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909633
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909634
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909635
    invoke-virtual {p0, p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909636
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909637
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1909638
    iget-object v0, p0, LX/CXD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1909639
    iget-object v0, p0, LX/CXD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    iget-object v0, v0, LX/CU0;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;
    .locals 4
    .param p2    # LX/CSY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/CSY;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1909640
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1909641
    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 1909642
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1909643
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/CXD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1909644
    iget-object v0, p0, LX/CXD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    iget-object v0, v0, LX/CU0;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1909645
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1909646
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1909647
    :cond_1
    iget-object v0, p0, LX/CXD;->a:Ljava/util/ArrayList;

    move-object v1, v0

    goto :goto_0

    .line 1909648
    :cond_2
    return-object v3
.end method
