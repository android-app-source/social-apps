.class public final LX/B4k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1742667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1742668
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1742669
    iget-object v1, p0, LX/B4k;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1742670
    iget-object v3, p0, LX/B4k;->b:LX/0Px;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1742671
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1742672
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1742673
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1742674
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1742675
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1742676
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1742677
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1742678
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1742679
    new-instance v1, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;-><init>(LX/15i;)V

    .line 1742680
    return-object v1
.end method
