.class public final LX/BBH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BBB;

.field private final b:LX/BB6;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756826
    new-instance v0, LX/BBB;

    invoke-direct {v0}, LX/BBB;-><init>()V

    invoke-direct {p0, v0}, LX/BBH;-><init>(LX/BBB;)V

    .line 1756827
    return-void
.end method

.method public constructor <init>(LX/BB6;)V
    .locals 1

    .prologue
    .line 1756828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756829
    const/4 v0, 0x0

    iput-object v0, p0, LX/BBH;->a:LX/BBB;

    .line 1756830
    iput-object p1, p0, LX/BBH;->b:LX/BB6;

    .line 1756831
    return-void
.end method

.method public constructor <init>(LX/BBB;)V
    .locals 1

    .prologue
    .line 1756832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756833
    iput-object p1, p0, LX/BBH;->a:LX/BBB;

    .line 1756834
    const/4 v0, 0x0

    iput-object v0, p0, LX/BBH;->b:LX/BB6;

    .line 1756835
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 1756836
    iget-object v0, p0, LX/BBH;->b:LX/BB6;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)LX/BBH;
    .locals 1
    .param p1    # Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1756837
    invoke-direct {p0}, LX/BBH;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756838
    iget-object v0, p0, LX/BBH;->b:LX/BB6;

    .line 1756839
    iput-object p1, v0, LX/BB6;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1756840
    :goto_0
    return-object p0

    .line 1756841
    :cond_0
    iget-object v0, p0, LX/BBH;->a:LX/BBB;

    .line 1756842
    iput-object p1, v0, LX/BBB;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1756843
    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;
    .locals 8

    .prologue
    .line 1756844
    invoke-direct {p0}, LX/BBH;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    iget-object v1, p0, LX/BBH;->b:LX/BB6;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1756845
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756846
    iget-object v3, v1, LX/BB6;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1756847
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1756848
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1756849
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756850
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756851
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756852
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756853
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756854
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;-><init>(LX/15i;)V

    .line 1756855
    move-object v1, v3

    .line 1756856
    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    iget-object v1, p0, LX/BBH;->a:LX/BBB;

    invoke-virtual {v1}, LX/BBB;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)V

    goto :goto_0
.end method
