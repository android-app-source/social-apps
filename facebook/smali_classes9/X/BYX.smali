.class public final LX/BYX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/BYY;

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:Z

.field public final synthetic d:LX/BYZ;


# direct methods
.method public constructor <init>(LX/BYZ;LX/BYY;Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 1796206
    iput-object p1, p0, LX/BYX;->d:LX/BYZ;

    iput-object p2, p0, LX/BYX;->a:LX/BYY;

    iput-object p3, p0, LX/BYX;->b:Landroid/app/Activity;

    iput-boolean p4, p0, LX/BYX;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1796207
    iget-object v0, p0, LX/BYX;->a:LX/BYY;

    iget-object v1, p0, LX/BYX;->d:LX/BYZ;

    iget-boolean v1, v1, LX/BYZ;->a:Z

    invoke-interface {v0, v1}, LX/BYY;->a(Z)V

    .line 1796208
    iget-object v0, p0, LX/BYX;->d:LX/BYZ;

    iget-object v1, p0, LX/BYX;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BYX;->d:LX/BYZ;

    iget-boolean v2, v2, LX/BYZ;->a:Z

    .line 1796209
    iget-object v3, v0, LX/BYZ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {p1}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object p1

    const-string p2, ""

    invoke-interface {v3, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1796210
    if-eqz v2, :cond_1

    .line 1796211
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "offpeak_download_dialog_download_offpeak"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "zero_module"

    .line 1796212
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796213
    move-object v3, v3

    .line 1796214
    move-object p1, v3

    .line 1796215
    :goto_0
    const-string v3, "location"

    invoke-virtual {p1, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796216
    const-string v3, "carrier_id"

    invoke-virtual {p1, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796217
    iget-object v3, v0, LX/BYZ;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-interface {v3, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1796218
    iget-boolean v0, p0, LX/BYX;->c:Z

    iget-object v1, p0, LX/BYX;->d:LX/BYZ;

    iget-boolean v1, v1, LX/BYZ;->a:Z

    if-eq v0, v1, :cond_0

    .line 1796219
    iget-object v0, p0, LX/BYX;->d:LX/BYZ;

    iget-object v0, v0, LX/BYZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0df;->M:LX/0Tn;

    iget-object v2, p0, LX/BYX;->d:LX/BYZ;

    iget-boolean v2, v2, LX/BYZ;->a:Z

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1796220
    :cond_0
    return-void

    .line 1796221
    :cond_1
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "offpeak_download_dialog_download_now"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "zero_module"

    .line 1796222
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796223
    move-object v3, v3

    .line 1796224
    move-object p1, v3

    goto :goto_0
.end method
