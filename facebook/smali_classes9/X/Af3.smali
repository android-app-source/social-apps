.class public final LX/Af3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Af4;


# direct methods
.method public constructor <init>(LX/Af4;)V
    .locals 0

    .prologue
    .line 1698628
    iput-object p1, p0, LX/Af3;->a:LX/Af4;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1698629
    iget-object v1, p0, LX/Af3;->a:LX/Af4;

    monitor-enter v1

    .line 1698630
    :try_start_0
    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v0, v0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v0, v0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698631
    :cond_0
    monitor-exit v1

    .line 1698632
    :goto_0
    return-void

    .line 1698633
    :cond_1
    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v2, v0, LX/Af4;->i:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/Af4;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_graphFailure"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get live comments for "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/Af3;->a:LX/Af4;

    iget-object v4, v4, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v0, v0, LX/AeQ;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v3, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1698634
    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    invoke-static {v0, p1}, LX/Af4;->a$redex0(LX/Af4;Ljava/lang/Throwable;)V

    .line 1698635
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1698636
    :cond_2
    :try_start_1
    const-string v0, "no story id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1698591
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1698592
    iget-object v1, p0, LX/Af3;->a:LX/Af4;

    monitor-enter v1

    .line 1698593
    :try_start_0
    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v0, v0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    iget-object v0, v0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698594
    :cond_0
    monitor-exit v1

    .line 1698595
    :goto_0
    return-void

    .line 1698596
    :cond_1
    if-eqz p1, :cond_2

    .line 1698597
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698598
    if-eqz v0, :cond_2

    .line 1698599
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698600
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel$FeedbackModel;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1698601
    :cond_2
    iget-object v0, p0, LX/Af3;->a:LX/Af4;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/Af4;->a$redex0(LX/Af4;Ljava/lang/Throwable;)V

    .line 1698602
    monitor-exit v1

    goto :goto_0

    .line 1698603
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1698604
    :cond_3
    :try_start_1
    iget-object v2, p0, LX/Af3;->a:LX/Af4;

    .line 1698605
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698606
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel$FeedbackModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsModel;

    move-result-object v0

    const/4 p1, 0x0

    const/4 v4, 0x1

    .line 1698607
    if-nez v0, :cond_5

    .line 1698608
    iget-object v3, v2, LX/Af4;->i:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p0, LX/Af4;->f:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, "_parseComments"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string p0, "Top level comments were null."

    invoke-virtual {v3, v4, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698609
    invoke-static {v2, p1}, LX/Af4;->a$redex0(LX/Af4;Ljava/lang/Throwable;)V

    .line 1698610
    :cond_4
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1698611
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsModel;->j()LX/0Px;

    move-result-object v3

    .line 1698612
    if-nez v3, :cond_6

    .line 1698613
    iget-object v3, v2, LX/Af4;->i:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p0, LX/Af4;->f:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, "_parseComments"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string p0, "Comments were null."

    invoke-virtual {v3, v4, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698614
    invoke-static {v2, p1}, LX/Af4;->a$redex0(LX/Af4;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1698615
    :cond_6
    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 1698616
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsModel$EdgesModel;

    .line 1698617
    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    .line 1698618
    if-nez v0, :cond_a

    .line 1698619
    const/4 v0, 0x0

    .line 1698620
    :goto_3
    move-object v3, v0

    .line 1698621
    if-eqz v3, :cond_7

    .line 1698622
    invoke-virtual {p0, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_2

    .line 1698623
    :cond_8
    iget-boolean v3, v2, LX/AeQ;->e:Z

    if-nez v3, :cond_9

    move v3, v4

    .line 1698624
    :goto_4
    iput-boolean v4, v2, LX/Af4;->e:Z

    .line 1698625
    iget-object v4, v2, LX/AeQ;->b:LX/AeV;

    if-eqz v4, :cond_4

    .line 1698626
    iget-object v4, v2, LX/AeQ;->b:LX/AeV;

    invoke-virtual {v2}, LX/Af4;->g()LX/AeN;

    move-result-object p1

    invoke-virtual {v4, p1, p0, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    goto :goto_1

    .line 1698627
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    :cond_a
    invoke-static {v0}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;

    move-result-object v0

    goto :goto_3
.end method
