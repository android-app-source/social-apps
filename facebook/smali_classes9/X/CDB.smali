.class public final LX/CDB;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CDD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/CDD;


# direct methods
.method public constructor <init>(LX/CDD;)V
    .locals 1

    .prologue
    .line 1858802
    iput-object p1, p0, LX/CDB;->c:LX/CDD;

    .line 1858803
    move-object v0, p1

    .line 1858804
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1858805
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1858806
    const-string v0, "DeferredWatchComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1858807
    if-ne p0, p1, :cond_1

    .line 1858808
    :cond_0
    :goto_0
    return v0

    .line 1858809
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1858810
    goto :goto_0

    .line 1858811
    :cond_3
    check-cast p1, LX/CDB;

    .line 1858812
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1858813
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1858814
    if-eq v2, v3, :cond_0

    .line 1858815
    iget-object v2, p0, LX/CDB;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDB;->a:Ljava/lang/Boolean;

    iget-object v3, p1, LX/CDB;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1858816
    goto :goto_0

    .line 1858817
    :cond_5
    iget-object v2, p1, LX/CDB;->a:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 1858818
    :cond_6
    iget-object v2, p0, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1858819
    goto :goto_0

    .line 1858820
    :cond_7
    iget-object v2, p1, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1858821
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDB;

    .line 1858822
    const/4 v1, 0x0

    iput-object v1, v0, LX/CDB;->a:Ljava/lang/Boolean;

    .line 1858823
    return-object v0
.end method
