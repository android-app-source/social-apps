.class public LX/Boy;
.super LX/99a;
.source ""

# interfaces
.implements Landroid/widget/ListAdapter;
.implements LX/0fn;
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/99a;",
        "LX/0fn;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "Lcom/facebook/permalink/PermalinkAdapter;"
    }
.end annotation


# instance fields
.field private final a:LX/1Cq;

.field private final b:LX/0Sh;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(LX/1Qq;LX/1Cq;LX/0Sh;LX/03V;)V
    .locals 0
    .param p1    # LX/1Qq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Cq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Qq;",
            "LX/1Cq",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822541
    invoke-direct {p0, p1}, LX/99a;-><init>(LX/1Qq;)V

    .line 1822542
    iput-object p2, p0, LX/Boy;->a:LX/1Cq;

    .line 1822543
    iput-object p3, p0, LX/Boy;->b:LX/0Sh;

    .line 1822544
    iput-object p4, p0, LX/Boy;->c:LX/03V;

    .line 1822545
    return-void
.end method


# virtual methods
.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 1822537
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v0, v0

    .line 1822538
    iget-object v1, p0, LX/99a;->a:LX/1Qq;

    move-object v1, v1

    .line 1822539
    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 1822540
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1822546
    invoke-super {p0, p1}, LX/99a;->a(Landroid/content/res/Configuration;)V

    .line 1822547
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1822530
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1822531
    iget-object v0, p0, LX/Boy;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1822532
    iget-object v0, p0, LX/Boy;->c:LX/03V;

    const-string v1, "StoryPermalinkAdapter"

    const-string v2, "bind called on non-UI thread"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1822533
    :cond_0
    iget-object v0, p0, LX/Boy;->a:LX/1Cq;

    .line 1822534
    iput-object p1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 1822535
    invoke-virtual {p0}, LX/99a;->notifyDataSetChanged()V

    .line 1822536
    return-void
.end method
