.class public final LX/Bct;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Bcw;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Bcu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.GraphQ",
            "LConnectionConfigurationChangeSetImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Bcw;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Bcw;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1802891
    iput-object p1, p0, LX/Bct;->b:LX/Bcw;

    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1802892
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "connectionConfiguration"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sessionId"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Bct;->c:[Ljava/lang/String;

    .line 1802893
    iput v3, p0, LX/Bct;->d:I

    .line 1802894
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Bct;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bct;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a(I)LX/Bct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802889
    iget-object v0, p0, LX/Bct;->a:LX/Bcu;

    iput p1, v0, LX/Bcu;->b:I

    .line 1802890
    return-object p0
.end method

.method public final a(LX/1rs;)LX/Bct;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTResponseModel;>;)",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802865
    iget-object v0, p0, LX/Bct;->a:LX/Bcu;

    iput-object p1, v0, LX/Bcu;->d:LX/1rs;

    .line 1802866
    iget-object v0, p0, LX/Bct;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1802867
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1802875
    invoke-super {p0}, LX/BcN;->a()V

    .line 1802876
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bct;->a:LX/Bcu;

    .line 1802877
    iget-object v0, p0, LX/Bct;->b:LX/Bcw;

    iget-object v0, v0, LX/Bcw;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1802878
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Bcw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1802879
    iget-object v1, p0, LX/Bct;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bct;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Bct;->d:I

    if-ge v1, v2, :cond_2

    .line 1802880
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1802881
    :goto_0
    iget v2, p0, LX/Bct;->d:I

    if-ge v0, v2, :cond_1

    .line 1802882
    iget-object v2, p0, LX/Bct;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1802883
    iget-object v2, p0, LX/Bct;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802884
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802885
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802886
    :cond_2
    iget-object v0, p0, LX/Bct;->a:LX/Bcu;

    .line 1802887
    invoke-virtual {p0}, LX/Bct;->a()V

    .line 1802888
    return-object v0
.end method

.method public final b(LX/BcQ;)LX/Bct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ;",
            ")",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802873
    iget-object v0, p0, LX/Bct;->a:LX/Bcu;

    iput-object p1, v0, LX/Bcu;->k:LX/BcQ;

    .line 1802874
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/Bct;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802870
    iget-object v0, p0, LX/Bct;->a:LX/Bcu;

    iput-object p1, v0, LX/Bcu;->e:Ljava/lang/String;

    .line 1802871
    iget-object v0, p0, LX/Bct;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1802872
    return-object p0
.end method

.method public final c(LX/BcQ;)LX/Bct;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802868
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 1802869
    return-object p0
.end method
