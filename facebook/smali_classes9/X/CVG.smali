.class public final LX/CVG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1902844
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1902845
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1902846
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1902847
    const-wide/16 v7, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1902848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_5

    .line 1902849
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902850
    :goto_1
    move v1, v4

    .line 1902851
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1902852
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1902853
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_3

    .line 1902854
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1902855
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902856
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1902857
    const-string v11, "time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1902858
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v5

    move v3, v9

    goto :goto_2

    .line 1902859
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1902860
    :cond_3
    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1902861
    if-eqz v3, :cond_4

    move-object v3, p1

    .line 1902862
    invoke-virtual/range {v3 .. v8}, LX/186;->a(IJJ)V

    .line 1902863
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_1

    :cond_5
    move v3, v4

    move-wide v5, v7

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1902864
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1902865
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1902866
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const-wide/16 v4, 0x0

    .line 1902867
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1902868
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1902869
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1902870
    const-string v4, "time"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902871
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 1902872
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1902873
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1902874
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1902875
    return-void
.end method
