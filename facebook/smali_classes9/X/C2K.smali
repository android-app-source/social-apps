.class public LX/C2K;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C2J;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C2L;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1844191
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C2K;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C2L;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844188
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1844189
    iput-object p1, p0, LX/C2K;->b:LX/0Ot;

    .line 1844190
    return-void
.end method

.method public static a(LX/0QB;)LX/C2K;
    .locals 4

    .prologue
    .line 1844177
    const-class v1, LX/C2K;

    monitor-enter v1

    .line 1844178
    :try_start_0
    sget-object v0, LX/C2K;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844179
    sput-object v2, LX/C2K;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844182
    new-instance v3, LX/C2K;

    const/16 p0, 0x1eaa

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C2K;-><init>(LX/0Ot;)V

    .line 1844183
    move-object v0, v3

    .line 1844184
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844185
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C2K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844186
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844176
    const v0, -0x5baac17a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1844159
    check-cast p2, LX/C2I;

    .line 1844160
    iget-object v0, p0, LX/C2K;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2L;

    iget-object v1, p2, LX/C2I;->a:Ljava/lang/CharSequence;

    const/4 p2, 0x1

    .line 1844161
    iget-object v2, v0, LX/C2L;->a:LX/23P;

    const/4 p0, 0x0

    invoke-virtual {v2, v1, p0}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1844162
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b004b

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const p0, 0x1010038

    invoke-virtual {v2, p0}, LX/1ne;->o(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const p2, 0x7f0b0062

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b0060

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/16 p0, 0x8

    const p2, 0x7f0b0064

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const p0, 0x7f02079d

    invoke-interface {v2, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    .line 1844163
    const p0, -0x5baac17a

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1844164
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1844165
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1844166
    invoke-static {}, LX/1dS;->b()V

    .line 1844167
    iget v0, p1, LX/1dQ;->b:I

    .line 1844168
    packed-switch v0, :pswitch_data_0

    .line 1844169
    :goto_0
    return-object v2

    .line 1844170
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1844171
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1844172
    check-cast v1, LX/C2I;

    .line 1844173
    iget-object p1, p0, LX/C2K;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C2L;

    iget-object p2, v1, LX/C2I;->b:Landroid/view/View$OnClickListener;

    .line 1844174
    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1844175
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5baac17a
        :pswitch_0
    .end packed-switch
.end method
