.class public final enum LX/BOS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BOS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BOS;

.field public static final enum CHARITY_FROM_CATEGORY:LX/BOS;

.field public static final enum CHARITY_FROM_CURATED_PICKER:LX/BOS;

.field public static final enum CHARITY_FROM_NO_FILTER:LX/BOS;

.field public static final enum CHARITY_FROM_SEARCH:LX/BOS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1779821
    new-instance v0, LX/BOS;

    const-string v1, "CHARITY_FROM_SEARCH"

    invoke-direct {v0, v1, v2}, LX/BOS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOS;->CHARITY_FROM_SEARCH:LX/BOS;

    .line 1779822
    new-instance v0, LX/BOS;

    const-string v1, "CHARITY_FROM_CATEGORY"

    invoke-direct {v0, v1, v3}, LX/BOS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOS;->CHARITY_FROM_CATEGORY:LX/BOS;

    .line 1779823
    new-instance v0, LX/BOS;

    const-string v1, "CHARITY_FROM_CURATED_PICKER"

    invoke-direct {v0, v1, v4}, LX/BOS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOS;->CHARITY_FROM_CURATED_PICKER:LX/BOS;

    .line 1779824
    new-instance v0, LX/BOS;

    const-string v1, "CHARITY_FROM_NO_FILTER"

    invoke-direct {v0, v1, v5}, LX/BOS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOS;->CHARITY_FROM_NO_FILTER:LX/BOS;

    .line 1779825
    const/4 v0, 0x4

    new-array v0, v0, [LX/BOS;

    sget-object v1, LX/BOS;->CHARITY_FROM_SEARCH:LX/BOS;

    aput-object v1, v0, v2

    sget-object v1, LX/BOS;->CHARITY_FROM_CATEGORY:LX/BOS;

    aput-object v1, v0, v3

    sget-object v1, LX/BOS;->CHARITY_FROM_CURATED_PICKER:LX/BOS;

    aput-object v1, v0, v4

    sget-object v1, LX/BOS;->CHARITY_FROM_NO_FILTER:LX/BOS;

    aput-object v1, v0, v5

    sput-object v0, LX/BOS;->$VALUES:[LX/BOS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1779826
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BOS;
    .locals 1

    .prologue
    .line 1779827
    const-class v0, LX/BOS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BOS;

    return-object v0
.end method

.method public static values()[LX/BOS;
    .locals 1

    .prologue
    .line 1779828
    sget-object v0, LX/BOS;->$VALUES:[LX/BOS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BOS;

    return-object v0
.end method
