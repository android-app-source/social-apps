.class public final LX/B0g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95b;


# instance fields
.field private final a:LX/95b;

.field private final b:LX/B0d;


# direct methods
.method public constructor <init>(LX/95b;LX/B0d;)V
    .locals 0

    .prologue
    .line 1734120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734121
    iput-object p1, p0, LX/B0g;->a:LX/95b;

    .line 1734122
    iput-object p2, p0, LX/B0g;->b:LX/B0d;

    .line 1734123
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1734119
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0}, LX/95b;->a()I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 1734118
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0, p1}, LX/95b;->a(I)I

    move-result v0

    return v0
.end method

.method public final b(I)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1734117
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0, p1}, LX/95b;->b(I)LX/0w5;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1734116
    iget-object v0, p0, LX/B0g;->b:LX/B0d;

    iget-object v0, v0, LX/B0d;->a:Ljava/lang/String;

    invoke-static {v0, p1}, LX/2nU;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1734115
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0, p1}, LX/95b;->d(I)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734113
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0, p1}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)I
    .locals 1

    .prologue
    .line 1734114
    iget-object v0, p0, LX/B0g;->a:LX/95b;

    invoke-interface {v0, p1}, LX/95b;->f(I)I

    move-result v0

    return v0
.end method
