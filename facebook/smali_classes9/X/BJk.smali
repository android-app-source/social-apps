.class public final LX/BJk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/ViewTreeObserver;

.field public final synthetic b:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;Landroid/view/ViewTreeObserver;)V
    .locals 0

    .prologue
    .line 1772337
    iput-object p1, p0, LX/BJk;->b:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iput-object p2, p0, LX/BJk;->a:Landroid/view/ViewTreeObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 1772338
    iget-object v0, p0, LX/BJk;->b:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1772339
    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-nez v1, :cond_0

    .line 1772340
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 1772341
    :cond_0
    instance-of v0, v0, Landroid/widget/ScrollView;

    const-string v1, "The wrapper of ComposerTextEdit must be put in a ScrollView"

    invoke-static {v0, v1}, LX/45Y;->a(ZLjava/lang/String;)V

    .line 1772342
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 1772343
    iget-object v0, p0, LX/BJk;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1772344
    :goto_1
    return-void

    .line 1772345
    :cond_1
    iget-object v0, p0, LX/BJk;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method
