.class public final LX/B3H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 0

    .prologue
    .line 1739893
    iput-object p1, p0, LX/B3H;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1739881
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739882
    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1739883
    invoke-static {v0}, LX/5Qm;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1739884
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid swipeable overlays list from server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1739885
    :cond_0
    iget-object v1, p0, LX/B3H;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v2, p0, LX/B3H;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v2

    .line 1739886
    iput-object v0, v2, LX/B3N;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1739887
    move-object v2, v2

    .line 1739888
    invoke-virtual {v2}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739889
    iget-object v1, p0, LX/B3H;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    .line 1739890
    iget-object v2, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v1, v2

    .line 1739891
    invoke-static {v0, v1}, LX/B5P;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1739892
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/B3H;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
