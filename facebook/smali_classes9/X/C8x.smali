.class public LX/C8x;
.super Landroid/graphics/drawable/Drawable;
.source ""


# static fields
.field public static final a:Landroid/graphics/RectF;

.field public static final b:[Landroid/graphics/RectF;

.field public static final c:[Landroid/graphics/RectF;


# instance fields
.field private final d:F

.field public e:Landroid/graphics/Paint;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v5, 0x42480000    # 50.0f

    const/high16 v1, 0x42200000    # 40.0f

    const/4 v6, 0x0

    .line 1853443
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v6, v6, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/C8x;->a:Landroid/graphics/RectF;

    .line 1853444
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v3, 0x430c0000    # 140.0f

    const/high16 v4, 0x41700000    # 15.0f

    invoke-direct {v1, v5, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/RectF;

    const/high16 v2, 0x41d00000    # 26.0f

    const/high16 v3, 0x42ea0000    # 117.0f

    const/high16 v4, 0x41f80000    # 31.0f

    invoke-direct {v1, v5, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v1, v0, v8

    sput-object v0, LX/C8x;->b:[Landroid/graphics/RectF;

    .line 1853445
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v3, 0x43890000    # 274.0f

    const/high16 v4, 0x42820000    # 65.0f

    invoke-direct {v1, v6, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/graphics/RectF;

    const/high16 v2, 0x429c0000    # 78.0f

    const/high16 v3, 0x43960000    # 300.0f

    const/high16 v4, 0x42a60000    # 83.0f

    invoke-direct {v1, v6, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v1, v0, v8

    const/4 v1, 0x2

    new-instance v2, Landroid/graphics/RectF;

    const/high16 v3, 0x42c00000    # 96.0f

    const/high16 v4, 0x43160000    # 150.0f

    const/high16 v5, 0x42ca0000    # 101.0f

    invoke-direct {v2, v6, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v2, v0, v1

    sput-object v0, LX/C8x;->c:[Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 1853446
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1853447
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/C8x;->e:Landroid/graphics/Paint;

    .line 1853448
    iget-object v0, p0, LX/C8x;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1853449
    iget-object v0, p0, LX/C8x;->e:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1853450
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, LX/C8x;->d:F

    .line 1853451
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1853452
    invoke-virtual {p0}, LX/C8x;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1853453
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    .line 1853454
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v2, v0

    .line 1853455
    const/high16 v0, 0x41200000    # 10.0f

    iget v3, p0, LX/C8x;->d:F

    mul-float/2addr v0, v3

    sub-float v0, v1, v0

    const/high16 v1, 0x43960000    # 300.0f

    div-float/2addr v0, v1

    .line 1853456
    const/high16 v1, 0x42ca0000    # 101.0f

    const/high16 v3, 0x42200000    # 40.0f

    iget v4, p0, LX/C8x;->d:F

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    div-float v1, v2, v1

    .line 1853457
    cmpg-float v2, v0, v1

    if-gez v2, :cond_2

    .line 1853458
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1853459
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1853460
    const/4 v0, 0x0

    .line 1853461
    sget-object v1, LX/C8x;->a:Landroid/graphics/RectF;

    iget-object v2, p0, LX/C8x;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1853462
    sget-object v2, LX/C8x;->b:[Landroid/graphics/RectF;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1853463
    iget-object v5, p0, LX/C8x;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1853464
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1853465
    :cond_0
    sget-object v1, LX/C8x;->c:[Landroid/graphics/RectF;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1853466
    iget-object v4, p0, LX/C8x;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1853467
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1853468
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1853469
    return-void

    :cond_2
    move v0, v1

    .line 1853470
    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1853471
    const/4 v0, -0x3

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1853472
    iget-object v0, p0, LX/C8x;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1853473
    invoke-virtual {p0}, LX/C8x;->invalidateSelf()V

    .line 1853474
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1853475
    iget-object v0, p0, LX/C8x;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1853476
    invoke-virtual {p0}, LX/C8x;->invalidateSelf()V

    .line 1853477
    return-void
.end method
