.class public final LX/AnU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AnV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public b:LX/162;

.field public c:Z

.field public final synthetic d:LX/AnV;


# direct methods
.method public constructor <init>(LX/AnV;)V
    .locals 1

    .prologue
    .line 1712515
    iput-object p1, p0, LX/AnU;->d:LX/AnV;

    .line 1712516
    move-object v0, p1

    .line 1712517
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1712518
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1712519
    const-string v0, "MisinformationBannerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1712520
    if-ne p0, p1, :cond_1

    .line 1712521
    :cond_0
    :goto_0
    return v0

    .line 1712522
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1712523
    goto :goto_0

    .line 1712524
    :cond_3
    check-cast p1, LX/AnU;

    .line 1712525
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1712526
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1712527
    if-eq v2, v3, :cond_0

    .line 1712528
    iget-object v2, p0, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v3, p1, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1712529
    goto :goto_0

    .line 1712530
    :cond_5
    iget-object v2, p1, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v2, :cond_4

    .line 1712531
    :cond_6
    iget-object v2, p0, LX/AnU;->b:LX/162;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/AnU;->b:LX/162;

    iget-object v3, p1, LX/AnU;->b:LX/162;

    invoke-virtual {v2, v3}, LX/162;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1712532
    goto :goto_0

    .line 1712533
    :cond_8
    iget-object v2, p1, LX/AnU;->b:LX/162;

    if-nez v2, :cond_7

    .line 1712534
    :cond_9
    iget-boolean v2, p0, LX/AnU;->c:Z

    iget-boolean v3, p1, LX/AnU;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1712535
    goto :goto_0
.end method
