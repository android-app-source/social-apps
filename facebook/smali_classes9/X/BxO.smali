.class public final LX/BxO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:LX/BxV;

.field public final synthetic c:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxV;)V
    .locals 0

    .prologue
    .line 1835326
    iput-object p1, p0, LX/BxO;->c:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    iput-object p2, p0, LX/BxO;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/BxO;->b:LX/BxV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x38cd9695

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1835327
    iget-object v1, p0, LX/BxO;->c:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    iget-object v2, p0, LX/BxO;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p0, LX/BxO;->b:LX/BxV;

    .line 1835328
    iget-object v5, v3, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v5, v5

    .line 1835329
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1835330
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/BxV;->a(Z)V

    .line 1835331
    iget-object v5, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->i:LX/BxX;

    invoke-virtual {v5}, LX/BxX;->b()V

    .line 1835332
    iget-object v5, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->l:LX/5up;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    const-string v7, "native_feed_chaining_box"

    const-string p0, "toggle_button"

    new-instance p1, LX/BxP;

    invoke-direct {p1, v1}, LX/BxP;-><init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;)V

    invoke-virtual {v5, v6, v7, p0, p1}, LX/5up;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 1835333
    :goto_0
    iget-object v5, v1, LX/AnF;->d:LX/1PW;

    if-eqz v5, :cond_0

    .line 1835334
    iget-object v5, v1, LX/AnF;->d:LX/1PW;

    check-cast v5, LX/1Pq;

    invoke-interface {v5}, LX/1Pq;->iN_()V

    .line 1835335
    :cond_0
    const v1, 0x75c56d1c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1835336
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/BxV;->a(Z)V

    .line 1835337
    iget-object v5, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->i:LX/BxX;

    invoke-virtual {v5}, LX/BxX;->a()V

    .line 1835338
    iget-object v5, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->l:LX/5up;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    const-string v7, "native_feed_chaining_box"

    const-string p0, "toggle_button"

    new-instance p1, LX/BxQ;

    invoke-direct {p1, v1}, LX/BxQ;-><init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;)V

    invoke-virtual {v5, v6, v7, p0, p1}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_0
.end method
