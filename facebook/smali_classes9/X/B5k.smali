.class public final LX/B5k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/AQ7;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/AQ7;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1745868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745869
    iput-object p1, p0, LX/B5k;->a:LX/0QB;

    .line 1745870
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1745871
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/B5k;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1745872
    packed-switch p2, :pswitch_data_0

    .line 1745873
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1745874
    :pswitch_0
    invoke-static {p1}, LX/Baz;->a(LX/0QB;)LX/Baz;

    move-result-object v0

    .line 1745875
    :goto_0
    return-object v0

    .line 1745876
    :pswitch_1
    invoke-static {p1}, LX/GVs;->a(LX/0QB;)LX/GVs;

    move-result-object v0

    goto :goto_0

    .line 1745877
    :pswitch_2
    invoke-static {p1}, LX/AQ8;->a(LX/0QB;)LX/AQ8;

    move-result-object v0

    goto :goto_0

    .line 1745878
    :pswitch_3
    new-instance v1, LX/ARR;

    const-class v0, LX/ART;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/ART;

    invoke-direct {v1, v0}, LX/ARR;-><init>(LX/ART;)V

    .line 1745879
    move-object v0, v1

    .line 1745880
    goto :goto_0

    .line 1745881
    :pswitch_4
    new-instance p0, LX/Ajh;

    const-class v0, LX/Ajj;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Ajj;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-direct {p0, v0, v1}, LX/Ajh;-><init>(LX/Ajj;LX/1Nq;)V

    .line 1745882
    move-object v0, p0

    .line 1745883
    goto :goto_0

    .line 1745884
    :pswitch_5
    new-instance p0, LX/Als;

    const-class v0, LX/Alu;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Alu;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-direct {p0, v0, v1}, LX/Als;-><init>(LX/Alu;LX/1Nq;)V

    .line 1745885
    move-object v0, p0

    .line 1745886
    goto :goto_0

    .line 1745887
    :pswitch_6
    invoke-static {p1}, LX/CEI;->a(LX/0QB;)LX/CEI;

    move-result-object v0

    goto :goto_0

    .line 1745888
    :pswitch_7
    new-instance p0, LX/AyY;

    const-class v0, LX/Aya;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Aya;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-direct {p0, v0, v1}, LX/AyY;-><init>(LX/Aya;LX/1Nq;)V

    .line 1745889
    move-object v0, p0

    .line 1745890
    goto :goto_0

    .line 1745891
    :pswitch_8
    invoke-static {p1}, LX/CF5;->a(LX/0QB;)LX/CF5;

    move-result-object v0

    goto :goto_0

    .line 1745892
    :pswitch_9
    invoke-static {p1}, LX/CFG;->a(LX/0QB;)LX/CFG;

    move-result-object v0

    goto :goto_0

    .line 1745893
    :pswitch_a
    invoke-static {p1}, LX/CFK;->a(LX/0QB;)LX/CFK;

    move-result-object v0

    goto :goto_0

    .line 1745894
    :pswitch_b
    invoke-static {p1}, LX/Giu;->a(LX/0QB;)LX/Giu;

    move-result-object v0

    goto :goto_0

    .line 1745895
    :pswitch_c
    invoke-static {p1}, LX/DJb;->a(LX/0QB;)LX/DJb;

    move-result-object v0

    goto :goto_0

    .line 1745896
    :pswitch_d
    new-instance v1, LX/CGc;

    const-class v0, LX/CGe;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/CGe;

    invoke-direct {v1, v0}, LX/CGc;-><init>(LX/CGe;)V

    .line 1745897
    move-object v0, v1

    .line 1745898
    goto/16 :goto_0

    .line 1745899
    :pswitch_e
    invoke-static {p1}, LX/BMX;->a(LX/0QB;)LX/BMX;

    move-result-object v0

    goto/16 :goto_0

    .line 1745900
    :pswitch_f
    invoke-static {p1}, LX/Cgy;->a(LX/0QB;)LX/Cgy;

    move-result-object v0

    goto/16 :goto_0

    .line 1745901
    :pswitch_10
    new-instance v1, LX/JDt;

    const-class v0, LX/JDv;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/JDv;

    invoke-direct {v1, v0}, LX/JDt;-><init>(LX/JDv;)V

    .line 1745902
    move-object v0, v1

    .line 1745903
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1745904
    const/16 v0, 0x11

    return v0
.end method
