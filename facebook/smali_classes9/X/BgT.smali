.class public LX/BgT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BgS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BgS",
        "<",
        "LX/97f;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/BhM;

.field private final b:Landroid/support/v4/app/Fragment;

.field private final c:LX/BgK;

.field private final d:LX/BeD;

.field public final e:LX/97f;

.field public final f:Landroid/view/View;

.field public final g:LX/BeE;

.field private final h:Ljava/lang/String;

.field public i:LX/97f;


# direct methods
.method public constructor <init>(LX/BhM;Landroid/support/v4/app/Fragment;LX/BgK;LX/97f;LX/BeD;Landroid/view/View;LX/BeE;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BgK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/97f;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BeD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/BeE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1807511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807512
    iput-object p1, p0, LX/BgT;->a:LX/BhM;

    .line 1807513
    iput-object p2, p0, LX/BgT;->b:Landroid/support/v4/app/Fragment;

    .line 1807514
    iput-object p3, p0, LX/BgT;->c:LX/BgK;

    .line 1807515
    iput-object p4, p0, LX/BgT;->e:LX/97f;

    .line 1807516
    iput-object p4, p0, LX/BgT;->i:LX/97f;

    .line 1807517
    iput-object p5, p0, LX/BgT;->d:LX/BeD;

    .line 1807518
    iput-object p6, p0, LX/BgT;->f:Landroid/view/View;

    .line 1807519
    iput-object p7, p0, LX/BgT;->g:LX/BeE;

    .line 1807520
    iput-object p8, p0, LX/BgT;->h:Ljava/lang/String;

    .line 1807521
    return-void
.end method

.method public static c(LX/BgT;LX/97f;)V
    .locals 10

    .prologue
    .line 1807503
    iput-object p1, p0, LX/BgT;->i:LX/97f;

    .line 1807504
    iget-object v0, p0, LX/BgT;->a:LX/BhM;

    iget-object v1, p0, LX/BgT;->f:Landroid/view/View;

    iget-object v2, p0, LX/BgT;->i:LX/97f;

    iget-object v3, p0, LX/BgT;->e:LX/97f;

    iget-object v4, p0, LX/BgT;->g:LX/BeE;

    iget-object v6, p0, LX/BgT;->c:LX/BgK;

    iget-object v7, p0, LX/BgT;->d:LX/BeD;

    iget-object v8, p0, LX/BgT;->b:Landroid/support/v4/app/Fragment;

    iget-object v9, p0, LX/BgT;->h:Ljava/lang/String;

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1807505
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1807510
    iget-object v0, p0, LX/BgT;->i:LX/97f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BgT;->i:LX/97f;

    invoke-interface {v0}, LX/97e;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1807506
    check-cast p1, LX/97f;

    .line 1807507
    invoke-static {p0, p1}, LX/BgT;->c(LX/BgT;LX/97f;)V

    .line 1807508
    iget-object v0, p0, LX/BgT;->c:LX/BgK;

    invoke-virtual {v0}, LX/BgK;->a()V

    .line 1807509
    return-void
.end method
