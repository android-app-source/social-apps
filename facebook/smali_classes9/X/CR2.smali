.class public final LX/CR2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:J

.field public d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1889904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 1889905
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1889906
    iget-object v1, p0, LX/CR2;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1889907
    iget-object v2, p0, LX/CR2;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1889908
    iget-object v2, p0, LX/CR2;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1889909
    iget-object v2, p0, LX/CR2;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1889910
    iget-object v2, p0, LX/CR2;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1889911
    iget-object v2, p0, LX/CR2;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1889912
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1889913
    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 1889914
    iget-boolean v1, p0, LX/CR2;->b:Z

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1889915
    const/4 v1, 0x2

    iget-wide v2, p0, LX/CR2;->c:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1889916
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1889917
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1889918
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1889919
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1889920
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/CR2;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1889921
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1889922
    const/16 v1, 0x9

    iget-wide v2, p0, LX/CR2;->j:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1889923
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1889924
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1889925
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1889926
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1889927
    new-instance v0, LX/15i;

    move-object v2, v11

    move-object v3, v11

    move v4, v13

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1889928
    new-instance v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;-><init>(LX/15i;)V

    .line 1889929
    return-object v1
.end method
