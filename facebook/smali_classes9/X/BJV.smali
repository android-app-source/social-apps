.class public final LX/BJV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V
    .locals 0

    .prologue
    .line 1772104
    iput-object p1, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 3

    .prologue
    .line 1772105
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1772106
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v1

    .line 1772107
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1772108
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1772109
    :goto_0
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-nez v0, :cond_0

    .line 1772110
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 1772111
    :cond_0
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-eqz v0, :cond_1

    .line 1772112
    const v2, 0x7f0d25fd

    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1772113
    :cond_1
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1772114
    iget-object v0, p0, LX/BJV;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1772115
    return-void
.end method
