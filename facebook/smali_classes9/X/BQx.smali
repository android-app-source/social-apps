.class public LX/BQx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQq;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3iT;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/BRP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/BRC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/BRH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/63c;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B3r;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/BRg;

.field public final m:LX/BQo;

.field public final n:LX/B37;

.field public final o:LX/0gc;

.field public p:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

.field public q:LX/BR0;

.field public r:Lcom/facebook/fig/button/FigButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/BR1;


# direct methods
.method public constructor <init>(LX/BRh;JLX/BQo;LX/B37;LX/0gc;)V
    .locals 2
    .param p2    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BQo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/B37;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1782873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782874
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1782875
    iput-object v0, p0, LX/BQx;->j:LX/0Ot;

    .line 1782876
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1782877
    iput-object v0, p0, LX/BQx;->k:LX/0Ot;

    .line 1782878
    invoke-virtual {p1, p2, p3}, LX/BRh;->a(J)LX/BRg;

    move-result-object v0

    iput-object v0, p0, LX/BQx;->l:LX/BRg;

    .line 1782879
    iput-object p4, p0, LX/BQx;->m:LX/BQo;

    .line 1782880
    iput-object p5, p0, LX/BQx;->n:LX/B37;

    .line 1782881
    iput-object p6, p0, LX/BQx;->o:LX/0gc;

    .line 1782882
    return-void
.end method

.method public static a(LX/BQx;LX/0Or;LX/0Or;LX/0Or;LX/BRP;LX/BRC;LX/BRH;LX/63c;Landroid/content/Context;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BQx;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQq;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3iT;",
            ">;",
            "LX/BRP;",
            "LX/BRC;",
            "LX/BRH;",
            "LX/63c;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/B3r;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1782872
    iput-object p1, p0, LX/BQx;->a:LX/0Or;

    iput-object p2, p0, LX/BQx;->b:LX/0Or;

    iput-object p3, p0, LX/BQx;->c:LX/0Or;

    iput-object p4, p0, LX/BQx;->d:LX/BRP;

    iput-object p5, p0, LX/BQx;->e:LX/BRC;

    iput-object p6, p0, LX/BQx;->f:LX/BRH;

    iput-object p7, p0, LX/BQx;->g:LX/63c;

    iput-object p8, p0, LX/BQx;->h:Landroid/content/Context;

    iput-object p9, p0, LX/BQx;->i:LX/0Or;

    iput-object p10, p0, LX/BQx;->j:LX/0Ot;

    iput-object p11, p0, LX/BQx;->k:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V
    .locals 10

    .prologue
    .line 1782883
    iput-object p3, p0, LX/BQx;->p:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    .line 1782884
    iget-object v0, p0, LX/BQx;->d:LX/BRP;

    iget-object v1, p0, LX/BQx;->p:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    .line 1782885
    if-eqz v1, :cond_0

    .line 1782886
    iget-object v2, v1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    move-object v2, v2

    .line 1782887
    if-eqz v2, :cond_0

    .line 1782888
    iget-object v2, v1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->l:LX/5QV;

    move-object v2, v2

    .line 1782889
    iget-object v3, v1, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->k:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-object v3, v3

    .line 1782890
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, LX/BRP;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    .line 1782891
    :cond_0
    new-instance v0, LX/BR1;

    invoke-direct {v0, p2, p3}, LX/BR1;-><init>(Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    iput-object v0, p0, LX/BQx;->s:LX/BR1;

    .line 1782892
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1782893
    const-string v3, "video_model"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1782894
    iget-object v2, p0, LX/BQx;->f:LX/BRH;

    iget-object v3, p0, LX/BQx;->o:LX/0gc;

    iget-object v5, p0, LX/BQx;->s:LX/BR1;

    iget-object v7, p0, LX/BQx;->p:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v4, p1

    move-object v6, p2

    invoke-virtual/range {v2 .. v7}, LX/BRH;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)LX/BRG;

    move-result-object v2

    .line 1782895
    :goto_0
    move-object v0, v2

    .line 1782896
    iput-object v0, p0, LX/BQx;->q:LX/BR0;

    .line 1782897
    invoke-static {p4}, LX/63Z;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1782898
    :goto_1
    const v0, 0x7f0d2d61

    invoke-virtual {p4, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;

    .line 1782899
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 1782900
    if-eqz v1, :cond_6

    .line 1782901
    const v1, 0x7f0813d5

    .line 1782902
    iput v1, v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->c:I

    .line 1782903
    const v1, 0x7f0813d5

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->setPrivacyText(I)V

    .line 1782904
    const v1, 0x7f0214f9

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->setPrivacyIcon(I)V

    .line 1782905
    :goto_2
    iget-object v0, p0, LX/BQx;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iT;

    .line 1782906
    iget-object v1, p0, LX/BQx;->s:LX/BR1;

    const/4 v2, 0x0

    .line 1782907
    iget-object v3, v1, LX/BR1;->h:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1782908
    iget-object v3, v1, LX/BR1;->h:Ljava/lang/String;

    invoke-static {v3, v0, v2}, LX/8of;->a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;)LX/8of;

    move-result-object v2

    .line 1782909
    :cond_1
    move-object v1, v2

    .line 1782910
    const v0, 0x7f0d2d6b

    invoke-virtual {p4, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1782911
    iget-boolean v2, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->o:Z

    move v2, v2

    .line 1782912
    if-nez v2, :cond_9

    .line 1782913
    :goto_3
    iget-object v0, p0, LX/BQx;->q:LX/BR0;

    invoke-interface {v0, p4}, LX/BR0;->a(Landroid/view/View;)V

    .line 1782914
    iget-object v0, p0, LX/BQx;->q:LX/BR0;

    invoke-interface {v0}, LX/BR0;->b()V

    .line 1782915
    iget-object v0, p0, LX/BQx;->q:LX/BR0;

    invoke-interface {v0, p4}, LX/BR0;->a(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V

    .line 1782916
    iget-boolean v0, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->r:Z

    move v0, v0

    .line 1782917
    if-eqz v0, :cond_2

    .line 1782918
    iget-object v0, p0, LX/BQx;->q:LX/BR0;

    invoke-interface {v0, p4}, LX/BR0;->b(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V

    .line 1782919
    :cond_2
    const/4 v6, 0x2

    .line 1782920
    new-instance v4, LX/0zw;

    const v0, 0x7f0d2d6a

    invoke-virtual {p4, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v4, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 1782921
    iget-object v0, p0, LX/BQx;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQq;

    .line 1782922
    new-instance v3, LX/BQt;

    invoke-direct {v3, p0}, LX/BQt;-><init>(LX/BQx;)V

    .line 1782923
    iget-boolean v1, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->s:Z

    move v1, v1

    .line 1782924
    if-eqz v1, :cond_3

    .line 1782925
    iget-object v1, p0, LX/BQx;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iget-object v5, p0, LX/BQx;->l:LX/BRg;

    invoke-virtual {v4}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1782926
    const v7, 0x7f0313ac

    const/4 p4, 0x0

    invoke-virtual {v1, v7, v2, p4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/fig/button/FigButton;

    .line 1782927
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1782928
    const p4, -0x6e685d

    .line 1782929
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    sput-object v1, LX/BRi;->b:Ljava/util/Calendar;

    .line 1782930
    iget-object v1, v5, LX/BRg;->q:LX/BRi;

    invoke-virtual {v1}, LX/BRi;->h()V

    .line 1782931
    new-instance v1, LX/6WS;

    iget-object v2, v5, LX/BRg;->p:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v1, v5, LX/BRg;->m:LX/5OM;

    .line 1782932
    invoke-static {v5}, LX/BRg;->b(LX/BRg;)V

    .line 1782933
    iget-object v1, v5, LX/BRg;->u:LX/0wM;

    const v2, 0x7f0207fd

    invoke-virtual {v1, v2, p4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1782934
    iget-object v1, v5, LX/BRg;->q:LX/BRi;

    invoke-static {v5, v7, v1}, LX/BRg;->a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V

    .line 1782935
    new-instance v1, LX/BRf;

    invoke-direct {v1, v5, v7}, LX/BRf;-><init>(LX/BRg;Lcom/facebook/fig/button/FigButton;)V

    invoke-virtual {v7, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1782936
    iput-object v7, v5, LX/BRg;->n:Lcom/facebook/fig/button/FigButton;

    .line 1782937
    iput-object v3, v5, LX/BRg;->o:LX/BQt;

    .line 1782938
    iget-object p4, v5, LX/BRg;->p:Landroid/content/Context;

    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p4

    const v1, 0x7f030598

    const/4 v2, 0x0

    invoke-virtual {p4, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1782939
    new-instance p4, LX/0ju;

    iget-object v2, v5, LX/BRg;->p:Landroid/content/Context;

    invoke-direct {p4, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p4, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object p4

    invoke-virtual {p4}, LX/0ju;->a()LX/2EJ;

    move-result-object p4

    iput-object p4, v5, LX/BRg;->h:LX/2EJ;

    .line 1782940
    const p4, 0x7f0d0f5c

    invoke-virtual {v1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Landroid/widget/NumberPicker;

    iput-object p4, v5, LX/BRg;->i:Landroid/widget/NumberPicker;

    .line 1782941
    const p4, 0x7f0d0f5d

    invoke-virtual {v1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Landroid/widget/NumberPicker;

    iput-object p4, v5, LX/BRg;->j:Landroid/widget/NumberPicker;

    .line 1782942
    const p4, 0x7f0d0f5e

    invoke-virtual {v1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Landroid/widget/NumberPicker;

    iput-object p4, v5, LX/BRg;->k:Landroid/widget/NumberPicker;

    .line 1782943
    const p4, 0x7f0d0f5f

    invoke-virtual {v1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Landroid/widget/NumberPicker;

    iput-object p4, v5, LX/BRg;->l:Landroid/widget/NumberPicker;

    .line 1782944
    :cond_3
    iget v1, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->q:I

    move v1, v1

    .line 1782945
    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    if-ne v1, v6, :cond_5

    .line 1782946
    :cond_4
    if-ne v1, v6, :cond_d

    .line 1782947
    new-instance v1, LX/BQu;

    invoke-direct {v1, p0, p1}, LX/BQu;-><init>(LX/BQx;Lcom/facebook/base/fragment/FbFragment;)V

    move-object v3, v1

    .line 1782948
    :goto_4
    iget-object v1, p0, LX/BQx;->d:LX/BRP;

    .line 1782949
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1782950
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v5

    iget-object v1, p0, LX/BQx;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-virtual {v4}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1782951
    iget-object v4, v0, LX/BQq;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0wM;

    .line 1782952
    const v6, 0x7f0313ac

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/fig/button/FigButton;

    .line 1782953
    const v7, 0x7f02098f

    const p1, -0x6e685d

    invoke-virtual {v4, v7, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1782954
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1782955
    invoke-virtual {v0, v5, v6}, LX/BQq;->a(ZLcom/facebook/fig/button/FigButton;)V

    .line 1782956
    invoke-virtual {v6, v3}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1782957
    move-object v0, v6

    .line 1782958
    iput-object v0, p0, LX/BQx;->r:Lcom/facebook/fig/button/FigButton;

    .line 1782959
    :cond_5
    return-void

    .line 1782960
    :cond_6
    iget v1, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->h:I

    move v1, v1

    .line 1782961
    iput v1, v0, Lcom/facebook/timeline/ui/common/ProfilePrivacyView;->c:I

    .line 1782962
    goto/16 :goto_2

    :cond_7
    iget-object v2, p0, LX/BQx;->e:LX/BRC;

    iget-object v3, p0, LX/BQx;->s:LX/BR1;

    iget-object v3, v3, LX/BR1;->g:Ljava/lang/String;

    iget-object v4, p0, LX/BQx;->m:LX/BQo;

    iget-object v5, p0, LX/BQx;->n:LX/B37;

    iget-object v7, p0, LX/BQx;->s:LX/BR1;

    iget-object v9, p0, LX/BQx;->p:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v6, p1

    move-object v8, p2

    invoke-virtual/range {v2 .. v9}, LX/BRC;->a(Ljava/lang/String;LX/BQo;LX/B37;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    move-result-object v2

    goto/16 :goto_0

    .line 1782963
    :cond_8
    const v0, 0x7f0d00bc

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1782964
    iget v1, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->f:I

    move v1, v1

    .line 1782965
    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1782966
    iget-object v1, p0, LX/BQx;->g:LX/63c;

    .line 1782967
    iget v2, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->g:I

    move v2, v2

    .line 1782968
    invoke-virtual {v1, v2}, LX/63c;->a(I)LX/63b;

    move-result-object v1

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1782969
    iget-object v1, p0, LX/BQx;->q:LX/BR0;

    iget-object v2, p0, LX/BQx;->l:LX/BRg;

    invoke-interface {v1, p1, v2}, LX/BR0;->a(Lcom/facebook/base/fragment/FbFragment;LX/BRg;)LX/63W;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1782970
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1782971
    new-instance v1, LX/BQr;

    invoke-direct {v1, p0, p1}, LX/BQr;-><init>(LX/BQx;Lcom/facebook/base/fragment/FbFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 1782972
    :cond_9
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1782973
    iget v3, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    move v3, v3

    .line 1782974
    if-nez v3, :cond_c

    iget-object v3, p0, LX/BQx;->q:LX/BR0;

    invoke-interface {v3}, LX/BR0;->g()I

    move-result v3

    :goto_5
    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setHint(I)V

    .line 1782975
    if-eqz v1, :cond_a

    .line 1782976
    invoke-virtual {v2, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1782977
    iget-object v3, p0, LX/BQx;->s:LX/BR1;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/BR1;->h:Ljava/lang/String;

    .line 1782978
    invoke-virtual {v1}, LX/8of;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(I)V

    .line 1782979
    :cond_a
    iget v3, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    move v3, v3

    .line 1782980
    if-lez v3, :cond_b

    .line 1782981
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    .line 1782982
    iget v6, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->p:I

    move v6, v6

    .line 1782983
    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 1782984
    :cond_b
    new-instance v3, LX/BQs;

    invoke-direct {v3, p0, v2}, LX/BQs;-><init>(LX/BQx;Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto/16 :goto_3

    .line 1782985
    :cond_c
    iget v3, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->i:I

    move v3, v3

    .line 1782986
    goto :goto_5

    .line 1782987
    :cond_d
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1782988
    const-string v2, "heisman_camera_intent_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1782989
    new-instance v2, LX/B4N;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->f()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, LX/B4N;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, LX/BQx;->d:LX/BRP;

    .line 1782990
    iget-object v5, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, v5

    .line 1782991
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v3

    invoke-interface {v3}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/B4N;->b(Ljava/lang/String;)LX/B4N;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->h()I

    move-result v3

    invoke-virtual {v2, v3}, LX/B4J;->a(I)LX/B4J;

    move-result-object v2

    check-cast v2, LX/B4N;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->g()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/B4J;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/B4J;

    move-result-object v2

    check-cast v2, LX/B4N;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v2

    check-cast v2, LX/B4N;

    invoke-virtual {v2}, LX/B4N;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    move-result-object v3

    .line 1782992
    iget-object v2, p0, LX/BQx;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    .line 1782993
    new-instance v5, LX/BQv;

    invoke-direct {v5, p0, p1, v3, v2}, LX/BQv;-><init>(LX/BQx;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;Lcom/facebook/content/SecureContextHelper;)V

    move-object v1, v5

    .line 1782994
    move-object v3, v1

    goto/16 :goto_4
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1782871
    iget-object v0, p0, LX/BQx;->s:LX/BR1;

    iget-object v0, v0, LX/BR1;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1782870
    iget-object v0, p0, LX/BQx;->s:LX/BR1;

    iget-object v0, v0, LX/BR1;->f:Ljava/lang/String;

    return-object v0
.end method
