.class public final LX/CAf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1855576
    iput-object p1, p0, LX/CAf;->b:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    iput-object p2, p0, LX/CAf;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, 0x68feb21c

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1855577
    iget-object v0, p0, LX/CAf;->b:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/CAf;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "should_open_new_timeline_activity_on_save_success"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 1855578
    if-eqz v2, :cond_0

    .line 1855579
    iget-object v0, p0, LX/CAf;->b:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1855580
    :cond_0
    const v0, -0xdb6bad4

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
