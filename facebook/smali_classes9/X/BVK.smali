.class public LX/BVK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/BVK;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Lcom/facebook/facedetection/detector/MacerFaceDetector;

.field private final e:LX/BVE;

.field public final f:Ljava/util/concurrent/locks/Lock;

.field private final g:Ljava/lang/Runnable;

.field private volatile h:Z

.field private i:Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

.field public j:LX/BET;

.field public k:Ljava/nio/ByteBuffer;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFaceDetectorLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/facebook/facedetection/detector/MacerFaceDetector;LX/BVE;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1790621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790622
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    .line 1790623
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BVK;->h:Z

    .line 1790624
    iput-object p1, p0, LX/BVK;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1790625
    iput-object p2, p0, LX/BVK;->b:Ljava/util/concurrent/ExecutorService;

    .line 1790626
    iput-object p3, p0, LX/BVK;->c:Ljava/util/concurrent/ExecutorService;

    .line 1790627
    iput-object p4, p0, LX/BVK;->d:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    .line 1790628
    iput-object p5, p0, LX/BVK;->e:LX/BVE;

    .line 1790629
    new-instance v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$1;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$1;-><init>(LX/BVK;)V

    iput-object v0, p0, LX/BVK;->g:Ljava/lang/Runnable;

    .line 1790630
    return-void
.end method

.method public static a(LX/0QB;)LX/BVK;
    .locals 9

    .prologue
    .line 1790608
    sget-object v0, LX/BVK;->l:LX/BVK;

    if-nez v0, :cond_1

    .line 1790609
    const-class v1, LX/BVK;

    monitor-enter v1

    .line 1790610
    :try_start_0
    sget-object v0, LX/BVK;->l:LX/BVK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1790611
    if-eqz v2, :cond_0

    .line 1790612
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1790613
    new-instance v3, LX/BVK;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(LX/0QB;)Lcom/facebook/facedetection/detector/MacerFaceDetector;

    move-result-object v7

    check-cast v7, Lcom/facebook/facedetection/detector/MacerFaceDetector;

    invoke-static {v0}, LX/BVE;->a(LX/0QB;)LX/BVE;

    move-result-object v8

    check-cast v8, LX/BVE;

    invoke-direct/range {v3 .. v8}, LX/BVK;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/facebook/facedetection/detector/MacerFaceDetector;LX/BVE;)V

    .line 1790614
    move-object v0, v3

    .line 1790615
    sput-object v0, LX/BVK;->l:LX/BVK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790616
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1790617
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1790618
    :cond_1
    sget-object v0, LX/BVK;->l:LX/BVK;

    return-object v0

    .line 1790619
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1790620
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/BVK;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;
    .locals 1

    .prologue
    .line 1790604
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BVK;->i:Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    if-nez v0, :cond_0

    .line 1790605
    new-instance v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;-><init>()V

    iput-object v0, p0, LX/BVK;->i:Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    .line 1790606
    :cond_0
    iget-object v0, p0, LX/BVK;->i:Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1790607
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/BVK;Ljava/nio/ByteBuffer;[BI)V
    .locals 1

    .prologue
    .line 1790596
    iget-boolean v0, p0, LX/BVK;->h:Z

    if-nez v0, :cond_1

    .line 1790597
    :cond_0
    :goto_0
    return-void

    .line 1790598
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    if-nez p1, :cond_5

    if-eqz p2, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790599
    and-int/lit8 v0, p3, 0x1

    if-lez v0, :cond_4

    .line 1790600
    invoke-direct {p0, p1, p2}, LX/BVK;->a(Ljava/nio/ByteBuffer;[B)V

    .line 1790601
    :cond_4
    and-int/lit8 v0, p3, 0x2

    if-lez v0, :cond_0

    .line 1790602
    invoke-direct {p0, p1, p2}, LX/BVK;->b(Ljava/nio/ByteBuffer;[B)V

    goto :goto_0

    .line 1790603
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/nio/ByteBuffer;[B)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const v3, 0xaf0005

    .line 1790631
    :try_start_0
    iget-object v0, p0, LX/BVK;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xaf0005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1790632
    if-eqz p1, :cond_0

    .line 1790633
    invoke-static {p0}, LX/BVK;->a(LX/BVK;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    move-result-object v0

    invoke-static {p0}, LX/BVK;->b(LX/BVK;)F

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->queueByteBuffer(Ljava/nio/ByteBuffer;F)V

    .line 1790634
    :goto_0
    iget-object v0, p0, LX/BVK;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/BVK;->g:Ljava/lang/Runnable;

    const v2, 0x1de2b29d    # 6.0006384E-21f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790635
    iget-object v0, p0, LX/BVK;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1790636
    return-void

    .line 1790637
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/BVK;->a(LX/BVK;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    move-result-object v0

    invoke-static {p0}, LX/BVK;->b(LX/BVK;)F

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->queueFrame([BF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1790638
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVK;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method private static b(LX/BVK;)F
    .locals 4

    .prologue
    .line 1790593
    iget-object v0, p0, LX/BVK;->e:LX/BVE;

    .line 1790594
    const v1, 0x3f59999a    # 0.85f

    iget-object v2, v0, LX/BVE;->d:LX/BVD;

    invoke-virtual {v2}, LX/BVD;->a()F

    move-result v2

    const v3, 0x3ca3d70a    # 0.02f

    mul-float/2addr v2, v3

    iget-object v3, v0, LX/BVE;->e:LX/BVD;

    invoke-virtual {v3}, LX/BVD;->a()F

    move-result v3

    const p0, 0x3e4ccccd    # 0.2f

    mul-float/2addr v3, p0

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    move v0, v1

    .line 1790595
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private b(Ljava/nio/ByteBuffer;[B)V
    .locals 4

    .prologue
    .line 1790581
    iget-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1790582
    :goto_0
    return-void

    .line 1790583
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 1790584
    iget-object v0, p0, LX/BVK;->j:LX/BET;

    invoke-virtual {v0}, LX/BET;->a()I

    move-result v0

    iget-object v1, p0, LX/BVK;->j:LX/BET;

    invoke-virtual {v1}, LX/BET;->b()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    .line 1790585
    :cond_1
    iget-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1790586
    if-eqz p1, :cond_2

    .line 1790587
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1790588
    iget-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1790589
    :goto_1
    iget-object v0, p0, LX/BVK;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;

    invoke-direct {v1, p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;-><init>(LX/BVK;)V

    const v2, -0x59c710bd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790590
    iget-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1790591
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    iget-object v2, p0, LX/BVK;->j:LX/BET;

    invoke-virtual {v2}, LX/BET;->a()I

    move-result v2

    iget-object v3, p0, LX/BVK;->j:LX/BET;

    invoke-virtual {v3}, LX/BET;->b()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {v0, p2, v1, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1790592
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1790579
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, LX/BVK;->a(LX/BVK;Ljava/nio/ByteBuffer;[BI)V

    .line 1790580
    return-void
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 1790563
    iget-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790564
    if-nez p1, :cond_0

    .line 1790565
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/BVK;->k:Ljava/nio/ByteBuffer;

    .line 1790566
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BVK;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790567
    iget-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790568
    :goto_0
    return-void

    .line 1790569
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/BVK;->a(LX/BVK;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->configure([B)V

    .line 1790570
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1790571
    new-instance v1, LX/BET;

    invoke-direct {v1}, LX/BET;-><init>()V

    .line 1790572
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result p1

    add-int/2addr v2, p1

    .line 1790573
    iput v2, v1, LX/BET;->a:I

    iput-object v0, v1, LX/BET;->b:Ljava/nio/ByteBuffer;

    move-object v2, v1

    .line 1790574
    move-object v1, v2

    .line 1790575
    move-object v0, v1

    .line 1790576
    iput-object v0, p0, LX/BVK;->j:LX/BET;

    .line 1790577
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BVK;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790578
    iget-object v0, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
