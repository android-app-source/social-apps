.class public abstract LX/BoF;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/HideableUnit;",
        ">",
        "LX/1wH",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic c:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 1821614
    iput-object p1, p0, LX/BoF;->c:LX/1dt;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/view/View;",
            ")",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation
.end method

.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821615
    invoke-super {p0, p1, p2, p3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821616
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821617
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1821618
    instance-of v1, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v1, :cond_2

    .line 1821619
    iget-object v1, p0, LX/BoF;->c:LX/1dt;

    invoke-virtual {v1, p1, p2, p3}, LX/1dt;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821620
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_0

    .line 1821621
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1821622
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v1}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1821623
    invoke-virtual {p0, v1}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1821624
    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1821625
    :cond_0
    :goto_0
    iget-object v1, p0, LX/BoF;->c:LX/1dt;

    invoke-static {v1, v0}, LX/1dt;->d(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821626
    iget-object v0, p0, LX/BoF;->c:LX/1dt;

    invoke-static {v0, p1, p2, p3}, LX/1dt;->d(LX/1dt;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821627
    :cond_1
    return-void

    .line 1821628
    :cond_2
    const v1, 0x7f081044

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1821629
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v2}, LX/Al4;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result v2

    .line 1821630
    iget-object v3, p0, LX/BoF;->c:LX/1dt;

    .line 1821631
    invoke-virtual {v3, v1, v2, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1821632
    new-instance v2, LX/BoN;

    invoke-direct {v2, p0, v0, p3, p2}, LX/BoN;-><init>(LX/BoF;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1821633
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1821634
    :goto_0
    return v0

    .line 1821635
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821636
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1821637
    instance-of v2, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v2, :cond_3

    .line 1821638
    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LX/BoF;->c:LX/1dt;

    invoke-static {v2, v0}, LX/1dt;->d(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1821639
    goto :goto_0
.end method
