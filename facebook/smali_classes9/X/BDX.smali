.class public LX/BDX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1763383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2nq;)LX/2nq;
    .locals 1

    .prologue
    .line 1763526
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, v0}, LX/BDX;->a(LX/2nq;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/2nq;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2nq;I)LX/2nq;
    .locals 1

    .prologue
    .line 1763525
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BBE;->a(I)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2nq;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/2nq;
    .locals 2

    .prologue
    .line 1763519
    invoke-interface {p0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 1763520
    iput-object p1, v0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1763521
    move-object v0, v0

    .line 1763522
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1763523
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/BBE;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    .line 1763524
    return-object v0
.end method

.method public static a(LX/2nq;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/2nq;
    .locals 3

    .prologue
    .line 1763527
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v0

    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    .line 1763528
    new-instance v2, LX/BDG;

    invoke-direct {v2}, LX/BDG;-><init>()V

    .line 1763529
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object p0

    iput-object p0, v2, LX/BDG;->a:LX/0Px;

    .line 1763530
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object p0

    iput-object p0, v2, LX/BDG;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1763531
    move-object v1, v2

    .line 1763532
    invoke-static {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    .line 1763533
    iput-object v2, v1, LX/BDG;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1763534
    move-object v1, v1

    .line 1763535
    invoke-virtual {v1}, LX/BDG;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBE;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2nq;Ljava/lang/String;)LX/2nq;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v13, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1763396
    invoke-interface {p0}, LX/2nq;->n()LX/BAy;

    move-result-object v8

    .line 1763397
    invoke-interface {v8}, LX/BAy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v13, :cond_1

    .line 1763398
    :cond_0
    :goto_0
    return-object v6

    .line 1763399
    :cond_1
    invoke-interface {v8}, LX/BAy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAx;

    .line 1763400
    invoke-interface {v0}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v9

    .line 1763401
    if-eqz v9, :cond_0

    .line 1763402
    invoke-virtual {v9}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v10

    .line 1763403
    if-eqz v10, :cond_0

    move v4, v5

    move-object v3, v6

    .line 1763404
    :goto_1
    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    .line 1763405
    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    .line 1763406
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v13, :cond_7

    .line 1763407
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BCR;

    .line 1763408
    invoke-interface {v2}, LX/BCR;->c()LX/BCP;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 1763409
    invoke-interface {v2}, LX/BCR;->c()LX/BCP;

    move-result-object v2

    invoke-interface {v2}, LX/BCP;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v7, v5

    :goto_2
    if-ge v7, v12, :cond_7

    invoke-virtual {v11, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BCO;

    .line 1763410
    invoke-interface {v2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1763411
    :goto_3
    if-nez v1, :cond_3

    .line 1763412
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    goto :goto_1

    .line 1763413
    :cond_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2

    :cond_3
    move-object v3, v1

    .line 1763414
    :cond_4
    if-eqz v3, :cond_0

    .line 1763415
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    .line 1763416
    if-eqz v2, :cond_0

    .line 1763417
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v7

    .line 1763418
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v13, :cond_0

    .line 1763419
    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BCR;

    .line 1763420
    invoke-interface {v1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v5

    .line 1763421
    invoke-static {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v5

    .line 1763422
    new-instance v6, LX/BCa;

    invoke-direct {v6}, LX/BCa;-><init>()V

    .line 1763423
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1763424
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->b:Ljava/lang/String;

    .line 1763425
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->c:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 1763426
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->d:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1763427
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1763428
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v11

    iput-object v11, v6, LX/BCa;->f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    .line 1763429
    move-object v5, v6

    .line 1763430
    iput-object p1, v5, LX/BCa;->b:Ljava/lang/String;

    .line 1763431
    move-object v5, v5

    .line 1763432
    invoke-virtual {v5}, LX/BCa;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v5

    .line 1763433
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->a(LX/BCR;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;

    move-result-object v1

    .line 1763434
    new-instance v6, LX/BCW;

    invoke-direct {v6}, LX/BCW;-><init>()V

    .line 1763435
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->b()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, LX/BCW;->a:Ljava/lang/String;

    .line 1763436
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$NotifOptionsModel;

    move-result-object v11

    iput-object v11, v6, LX/BCW;->b:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$NotifOptionsModel;

    .line 1763437
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v11

    iput-object v11, v6, LX/BCW;->c:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1763438
    move-object v1, v6

    .line 1763439
    invoke-static {v5}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v5

    .line 1763440
    iput-object v5, v1, LX/BCW;->c:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1763441
    move-object v1, v1

    .line 1763442
    invoke-virtual {v1}, LX/BCW;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;

    move-result-object v1

    .line 1763443
    invoke-static {v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v5

    .line 1763444
    new-instance v6, LX/BD4;

    invoke-direct {v6}, LX/BD4;-><init>()V

    .line 1763445
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    iput-object v7, v6, LX/BD4;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1763446
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v7

    iput-object v7, v6, LX/BD4;->b:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 1763447
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v7

    iput-object v7, v6, LX/BD4;->c:LX/0Px;

    .line 1763448
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/BD4;->d:Ljava/lang/String;

    .line 1763449
    move-object v5, v6

    .line 1763450
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->a(LX/BCR;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1763451
    iput-object v1, v5, LX/BD4;->c:LX/0Px;

    .line 1763452
    move-object v1, v5

    .line 1763453
    invoke-virtual {v1}, LX/BD4;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v1

    .line 1763454
    invoke-static {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    .line 1763455
    new-instance v5, LX/BD3;

    invoke-direct {v5}, LX/BD3;-><init>()V

    .line 1763456
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v6

    iput-object v6, v5, LX/BD3;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    .line 1763457
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v6

    iput-object v6, v5, LX/BD3;->b:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    .line 1763458
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/BD3;->c:Ljava/lang/String;

    .line 1763459
    move-object v2, v5

    .line 1763460
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v1

    .line 1763461
    iput-object v1, v2, LX/BD3;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    .line 1763462
    move-object v1, v2

    .line 1763463
    invoke-virtual {v1}, LX/BD3;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    .line 1763464
    invoke-static {v3}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object v2

    .line 1763465
    new-instance v3, LX/BD2;

    invoke-direct {v3}, LX/BD2;-><init>()V

    .line 1763466
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v5

    iput-object v5, v3, LX/BD2;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1763467
    move-object v2, v3

    .line 1763468
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    .line 1763469
    iput-object v1, v2, LX/BD2;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1763470
    move-object v1, v2

    .line 1763471
    invoke-virtual {v1}, LX/BD2;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object v1

    .line 1763472
    invoke-static {v10}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v2

    .line 1763473
    new-instance v3, LX/BD1;

    invoke-direct {v3}, LX/BD1;-><init>()V

    .line 1763474
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v5

    iput-object v5, v3, LX/BD1;->a:LX/0Px;

    .line 1763475
    move-object v2, v3

    .line 1763476
    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v6

    .line 1763477
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1763478
    const/4 v3, 0x0

    move v5, v3

    :goto_5
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_6

    .line 1763479
    if-ne v4, v5, :cond_5

    .line 1763480
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1763481
    :goto_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5

    .line 1763482
    :cond_5
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    invoke-static {v3}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_6

    .line 1763483
    :cond_6
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v1, v3

    .line 1763484
    iput-object v1, v2, LX/BD1;->a:LX/0Px;

    .line 1763485
    move-object v1, v2

    .line 1763486
    invoke-virtual {v1}, LX/BD1;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v1

    .line 1763487
    invoke-static {v9}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v2

    .line 1763488
    new-instance v3, LX/BD0;

    invoke-direct {v3}, LX/BD0;-><init>()V

    .line 1763489
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v4

    iput-object v4, v3, LX/BD0;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1763490
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v4

    iput-object v4, v3, LX/BD0;->b:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 1763491
    move-object v2, v3

    .line 1763492
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v1

    .line 1763493
    iput-object v1, v2, LX/BD0;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1763494
    move-object v1, v2

    .line 1763495
    invoke-virtual {v1}, LX/BD0;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v1

    .line 1763496
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a(LX/BAx;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v0

    .line 1763497
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v2, LX/BBH;

    iget-object v3, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    .line 1763498
    new-instance v4, LX/BB6;

    invoke-direct {v4}, LX/BB6;-><init>()V

    .line 1763499
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    iput-object v0, v4, LX/BB6;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1763500
    move-object v3, v4

    .line 1763501
    invoke-direct {v2, v3}, LX/BBH;-><init>(LX/BB6;)V

    :goto_7
    move-object v0, v2

    .line 1763502
    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBH;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)LX/BBH;

    move-result-object v0

    invoke-virtual {v0}, LX/BBH;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v0

    .line 1763503
    invoke-static {v8}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v1

    .line 1763504
    invoke-static {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, LX/BBG;

    iget-object v3, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1763505
    new-instance v4, LX/BB5;

    invoke-direct {v4}, LX/BB5;-><init>()V

    .line 1763506
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v4, LX/BB5;->a:LX/0Px;

    .line 1763507
    move-object v3, v4

    .line 1763508
    invoke-direct {v2, v3}, LX/BBG;-><init>(LX/BB5;)V

    :goto_8
    move-object v1, v2

    .line 1763509
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a(LX/BAx;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/BBG;->a(LX/0Px;)LX/BBG;

    move-result-object v0

    invoke-virtual {v0}, LX/BBG;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    .line 1763510
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v6

    goto/16 :goto_0

    :cond_7
    move-object v1, v3

    goto/16 :goto_3

    :cond_8
    move-object v1, v3

    goto/16 :goto_4

    :cond_9
    new-instance v2, LX/BBH;

    iget-object v3, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1763511
    new-instance v4, LX/BBB;

    invoke-direct {v4}, LX/BBB;-><init>()V

    .line 1763512
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    iput-object v0, v4, LX/BBB;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1763513
    move-object v3, v4

    .line 1763514
    invoke-direct {v2, v3}, LX/BBH;-><init>(LX/BBB;)V

    goto :goto_7

    :cond_a
    new-instance v2, LX/BBG;

    iget-object v3, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 1763515
    new-instance v4, LX/BBA;

    invoke-direct {v4}, LX/BBA;-><init>()V

    .line 1763516
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v4, LX/BBA;->a:LX/0Px;

    .line 1763517
    move-object v3, v4

    .line 1763518
    invoke-direct {v2, v3}, LX/BBG;-><init>(LX/BBA;)V

    goto :goto_8
.end method

.method public static a(LX/2nq;Z)LX/2nq;
    .locals 1

    .prologue
    .line 1763395
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BBE;->a(Z)LX/BBE;

    move-result-object v0

    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;ZLX/BAy;ILX/0Px;)LX/2nq;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/BAy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/String;",
            "Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLInterfaces$RichNotification;",
            "Z",
            "LX/BAy;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;)",
            "LX/2nq;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1763390
    if-nez p0, :cond_0

    .line 1763391
    const/4 v0, 0x0

    .line 1763392
    :goto_0
    return-object v0

    .line 1763393
    :cond_0
    new-instance v0, LX/BBE;

    invoke-direct {v0}, LX/BBE;-><init>()V

    invoke-virtual {v0, p0}, LX/BBE;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BBE;->b(Ljava/lang/String;)LX/BBE;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBE;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/BBE;->a(Z)LX/BBE;

    move-result-object v0

    invoke-static {p4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/BBE;->a(I)LX/BBE;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/BBE;->a(LX/0Px;)LX/BBE;

    move-result-object v0

    .line 1763394
    invoke-virtual {v0}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLStorySeenState;J)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3

    .prologue
    .line 1763384
    invoke-static {p0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 1763385
    iput-object p1, v0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1763386
    move-object v0, v0

    .line 1763387
    iput-wide p2, v0, LX/23u;->v:J

    .line 1763388
    move-object v0, v0

    .line 1763389
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method
