.class public final LX/BmU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bm9;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V
    .locals 0

    .prologue
    .line 1818394
    iput-object p1, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 1818395
    if-nez p1, :cond_1

    .line 1818396
    iget-object v0, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    .line 1818397
    :cond_0
    :goto_0
    iget-object v0, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818398
    iget-object v0, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818399
    return-void

    .line 1818400
    :cond_1
    iget-object v0, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818401
    iget-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818402
    if-nez v0, :cond_0

    .line 1818403
    iget-object v0, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818404
    iget-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818405
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1818406
    iget-object v1, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818407
    iget-object v1, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1818408
    const/16 v1, 0xb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1818409
    iget-object v1, p0, LX/BmU;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    goto :goto_0
.end method
