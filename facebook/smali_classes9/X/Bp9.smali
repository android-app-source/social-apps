.class public LX/Bp9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Bpf;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/Bpf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;",
            ">;",
            "LX/Bpf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822637
    iput-object p1, p0, LX/Bp9;->a:LX/0Ot;

    .line 1822638
    iput-object p2, p0, LX/Bp9;->b:LX/0Ot;

    .line 1822639
    iput-object p3, p0, LX/Bp9;->c:LX/Bpf;

    .line 1822640
    return-void
.end method

.method public static a(LX/0QB;)LX/Bp9;
    .locals 6

    .prologue
    .line 1822641
    const-class v1, LX/Bp9;

    monitor-enter v1

    .line 1822642
    :try_start_0
    sget-object v0, LX/Bp9;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822643
    sput-object v2, LX/Bp9;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822644
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822645
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822646
    new-instance v4, LX/Bp9;

    const/16 v3, 0x1cf4

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1cef

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, LX/Bpf;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Bpf;

    invoke-direct {v4, v5, p0, v3}, LX/Bp9;-><init>(LX/0Ot;LX/0Ot;LX/Bpf;)V

    .line 1822647
    move-object v0, v4

    .line 1822648
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822649
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bp9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822650
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
