.class public final LX/Bv5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:LX/BvE;


# direct methods
.method public constructor <init>(LX/BvE;)V
    .locals 0

    .prologue
    .line 1831969
    iput-object p1, p0, LX/Bv5;->a:LX/BvE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1831970
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, Lcom/facebook/fig/button/FigButton;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1831971
    iget-object v1, p0, LX/Bv5;->a:LX/BvE;

    iget-object v1, v1, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1}, Lcom/facebook/fig/button/FigButton;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1831972
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {v1, v3}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 1831973
    :cond_0
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-nez v0, :cond_1

    .line 1831974
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->t:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1831975
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/Bv5;->a:LX/BvE;

    iget-object v1, v1, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1831976
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/Bv5;->a:LX/BvE;

    iget-object v1, v1, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1831977
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    invoke-static {v0, v2}, LX/BvE;->b(Landroid/view/View;I)V

    .line 1831978
    iget-object v0, p0, LX/Bv5;->a:LX/BvE;

    iget-object v0, v0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    invoke-static {v0, v2}, LX/BvE;->b(Landroid/view/View;I)V

    .line 1831979
    :cond_1
    return-void
.end method
