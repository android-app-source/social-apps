.class public LX/B5Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B5Q;


# instance fields
.field public a:LX/0sa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1744340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1744341
    return-void
.end method

.method public static a(LX/0QB;)LX/B5Q;
    .locals 4

    .prologue
    .line 1744342
    sget-object v0, LX/B5Q;->b:LX/B5Q;

    if-nez v0, :cond_1

    .line 1744343
    const-class v1, LX/B5Q;

    monitor-enter v1

    .line 1744344
    :try_start_0
    sget-object v0, LX/B5Q;->b:LX/B5Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1744345
    if-eqz v2, :cond_0

    .line 1744346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1744347
    new-instance p0, LX/B5Q;

    invoke-direct {p0}, LX/B5Q;-><init>()V

    .line 1744348
    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    .line 1744349
    iput-object v3, p0, LX/B5Q;->a:LX/0sa;

    .line 1744350
    move-object v0, p0

    .line 1744351
    sput-object v0, LX/B5Q;->b:LX/B5Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1744352
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1744353
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1744354
    :cond_1
    sget-object v0, LX/B5Q;->b:LX/B5Q;

    return-object v0

    .line 1744355
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1744356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
