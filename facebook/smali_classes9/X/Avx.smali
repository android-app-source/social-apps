.class public LX/Avx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1724844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;
    .locals 4

    .prologue
    .line 1724845
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->newBuilder()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    const-string v1, "inspiration_nux_disk_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setName(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setVersion(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setCapacity(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    const-wide v2, 0x7ffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setMaxSize(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    const-wide/16 v2, 0x7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setStaleDays(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->a()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    move-result-object v0

    return-object v0
.end method
