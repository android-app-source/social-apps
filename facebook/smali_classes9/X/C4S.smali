.class public LX/C4S;
.super LX/3lC;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/C4S;


# instance fields
.field private final a:LX/20l;

.field private final b:LX/0tH;

.field private final c:LX/3lD;


# direct methods
.method public constructor <init>(LX/20l;LX/0tH;LX/3lD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847292
    invoke-direct {p0}, LX/3lC;-><init>()V

    .line 1847293
    iput-object p1, p0, LX/C4S;->a:LX/20l;

    .line 1847294
    iput-object p2, p0, LX/C4S;->b:LX/0tH;

    .line 1847295
    iput-object p3, p0, LX/C4S;->c:LX/3lD;

    .line 1847296
    return-void
.end method

.method public static a(LX/0QB;)LX/C4S;
    .locals 6

    .prologue
    .line 1847279
    sget-object v0, LX/C4S;->d:LX/C4S;

    if-nez v0, :cond_1

    .line 1847280
    const-class v1, LX/C4S;

    monitor-enter v1

    .line 1847281
    :try_start_0
    sget-object v0, LX/C4S;->d:LX/C4S;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1847282
    if-eqz v2, :cond_0

    .line 1847283
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1847284
    new-instance p0, LX/C4S;

    invoke-static {v0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v3

    check-cast v3, LX/20l;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v4

    check-cast v4, LX/0tH;

    invoke-static {v0}, LX/3lD;->a(LX/0QB;)LX/3lD;

    move-result-object v5

    check-cast v5, LX/3lD;

    invoke-direct {p0, v3, v4, v5}, LX/C4S;-><init>(LX/20l;LX/0tH;LX/3lD;)V

    .line 1847285
    move-object v0, p0

    .line 1847286
    sput-object v0, LX/C4S;->d:LX/C4S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847287
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1847288
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1847289
    :cond_1
    sget-object v0, LX/C4S;->d:LX/C4S;

    return-object v0

    .line 1847290
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1847291
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 1847277
    const-wide/32 v2, 0x5265c00

    move-wide v0, v2

    .line 1847278
    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1847253
    iget-object v0, p0, LX/C4S;->c:LX/3lD;

    invoke-virtual {v0}, LX/3lD;->c()LX/10S;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 5

    .prologue
    .line 1847256
    iget-object v0, p0, LX/C4S;->c:LX/3lD;

    .line 1847257
    new-instance v2, LX/C4R;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, LX/C4R;-><init>(Landroid/content/Context;I)V

    .line 1847258
    const/4 p0, 0x0

    .line 1847259
    invoke-virtual {v2}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1847260
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1876

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1847261
    invoke-virtual {v2, v3, p0, v3, p0}, LX/0ht;->a(IIII)V

    .line 1847262
    const/4 v3, -0x1

    .line 1847263
    iput v3, v2, LX/0hs;->t:I

    .line 1847264
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08198d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1847265
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08198f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1847266
    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v2, v1}, LX/0ht;->b(F)V

    .line 1847267
    new-instance v1, LX/C4Q;

    invoke-direct {v1, v0}, LX/C4Q;-><init>(LX/3lD;)V

    .line 1847268
    iput-object v1, v2, LX/0ht;->H:LX/2dD;

    .line 1847269
    const-string v1, " "

    invoke-virtual {v2, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1847270
    iget-object v1, v0, LX/3lD;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Aw;

    .line 1847271
    invoke-virtual {v1, p2}, LX/9Aw;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1847272
    const/4 p0, 0x0

    .line 1847273
    iget-object v3, v2, LX/0ht;->g:LX/5OY;

    const v4, 0x7f0d0941

    invoke-virtual {v3, v4}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1847274
    invoke-virtual {v3, v1, p0, p0, p0}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1847275
    invoke-virtual {v2, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1847276
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1847255
    const-string v0, "4131"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1847254
    invoke-static {}, LX/3lD;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method
