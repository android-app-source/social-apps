.class public LX/Br8;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Br6;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/ThumbnailWithTextComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1825944
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Br8;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/ThumbnailWithTextComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825953
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1825954
    iput-object p1, p0, LX/Br8;->b:LX/0Ot;

    .line 1825955
    return-void
.end method

.method public static a(LX/0QB;)LX/Br8;
    .locals 4

    .prologue
    .line 1825956
    const-class v1, LX/Br8;

    monitor-enter v1

    .line 1825957
    :try_start_0
    sget-object v0, LX/Br8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825958
    sput-object v2, LX/Br8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825961
    new-instance v3, LX/Br8;

    const/16 p0, 0x1d11

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Br8;-><init>(LX/0Ot;)V

    .line 1825962
    move-object v0, v3

    .line 1825963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Br8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1825947
    check-cast p2, LX/Br7;

    .line 1825948
    iget-object v0, p0, LX/Br8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/ThumbnailWithTextComponentSpec;

    iget-object v1, p2, LX/Br7;->a:Landroid/net/Uri;

    iget-object v2, p2, LX/Br7;->b:Ljava/lang/String;

    iget-object v3, p2, LX/Br7;->c:Ljava/lang/String;

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 1825949
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 1825950
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v6

    iget-object v4, v0, Lcom/facebook/feed/rows/sections/ThumbnailWithTextComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    sget-object v7, Lcom/facebook/feed/rows/sections/ThumbnailWithTextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f010201

    invoke-interface {v4, v6}, LX/1Di;->h(I)LX/1Di;

    move-result-object v4

    const v6, 0x7f010201

    invoke-interface {v4, v6}, LX/1Di;->p(I)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b0060

    invoke-interface {v4, p2, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0e013f

    invoke-static {p1, p0, v7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0e013e

    invoke-static {p1, p0, v7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1825951
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1825952
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1825945
    invoke-static {}, LX/1dS;->b()V

    .line 1825946
    const/4 v0, 0x0

    return-object v0
.end method
