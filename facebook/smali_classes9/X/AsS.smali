.class public LX/AsS;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AsR;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720622
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1720623
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;LX/ArT;LX/ArL;Landroid/view/ViewStub;)LX/AsR;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            ":",
            "LX/0is;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "LX/ArT;",
            "LX/ArL;",
            "Landroid/view/ViewStub;",
            ")",
            "LX/AsR",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1720624
    new-instance v0, LX/AsR;

    move-object v2, p2

    check-cast v2, LX/0ik;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v1, LX/Asf;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Asf;

    const-class v1, LX/AsP;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/AsP;

    invoke-static {p0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v9

    check-cast v9, LX/0tS;

    const-class v1, LX/AuQ;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/AuQ;

    invoke-static {p0}, LX/87V;->a(LX/0QB;)LX/87V;

    move-result-object v11

    check-cast v11, LX/87V;

    move-object v1, p1

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v11}, LX/AsR;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0ik;LX/ArT;LX/ArL;Landroid/view/ViewStub;Landroid/content/Context;LX/Asf;LX/AsP;LX/0tS;LX/AuQ;LX/87V;)V

    .line 1720625
    return-object v0
.end method
