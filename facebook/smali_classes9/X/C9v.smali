.class public final LX/C9v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1854746
    iput-object p1, p0, LX/C9v;->b:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    iput-object p2, p0, LX/C9v;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x641ec96a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1854747
    iget-object v1, p0, LX/C9v;->b:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->b:LX/9A1;

    iget-object v2, p0, LX/C9v;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1854748
    new-instance v5, LX/0ju;

    invoke-direct {v5, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0810a8

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    const p0, 0x7f081065

    new-instance p1, LX/99z;

    invoke-direct {p1, v1}, LX/99z;-><init>(LX/9A1;)V

    invoke-virtual {v5, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const p0, 0x7f08104c

    new-instance p1, LX/99y;

    invoke-direct {p1, v1, v2}, LX/99y;-><init>(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v5, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    .line 1854749
    invoke-virtual {v5}, LX/0ju;->a()LX/2EJ;

    move-result-object v5

    move-object v1, v5

    .line 1854750
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1854751
    const v1, 0x45469272

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
