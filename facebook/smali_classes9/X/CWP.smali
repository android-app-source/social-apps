.class public LX/CWP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/CWJ;

.field public final d:Ljava/text/SimpleDateFormat;

.field public final e:Ljava/text/SimpleDateFormat;

.field public final f:Ljava/text/SimpleDateFormat;

.field public final g:Ljava/util/Calendar;

.field public final h:Ljava/util/Calendar;

.field public final i:Ljava/util/Calendar;

.field public final j:Ljava/util/Calendar;

.field public final k:Ljava/util/Calendar;

.field public final l:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/CU5;",
            ">;>;"
        }
    .end annotation
.end field

.field public final m:Landroid/content/Context;

.field public n:LX/CWI;

.field public o:LX/CT7;

.field public p:LX/CT6;

.field public q:LX/CTQ;

.field public r:Ljava/lang/String;

.field public s:LX/CSY;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/CWJ;Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/CWJ;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1908109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1908110
    iput-object p1, p0, LX/CWP;->a:LX/0Or;

    .line 1908111
    iput-object p2, p0, LX/CWP;->b:LX/0Ot;

    .line 1908112
    iput-object p3, p0, LX/CWP;->c:LX/CWJ;

    .line 1908113
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd"

    iget-object v0, p0, LX/CWP;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWP;->d:Ljava/text/SimpleDateFormat;

    .line 1908114
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "h:mma"

    iget-object v0, p0, LX/CWP;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWP;->e:Ljava/text/SimpleDateFormat;

    .line 1908115
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "cccc, MMMM dd"

    iget-object v0, p0, LX/CWP;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWP;->f:Ljava/text/SimpleDateFormat;

    .line 1908116
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWP;->j:Ljava/util/Calendar;

    .line 1908117
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWP;->g:Ljava/util/Calendar;

    .line 1908118
    const-string v0, "2100/01/01"

    iget-object v1, p0, LX/CWP;->g:Ljava/util/Calendar;

    invoke-direct {p0, v0, v1}, LX/CWP;->a(Ljava/lang/String;Ljava/util/Calendar;)V

    .line 1908119
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWP;->h:Ljava/util/Calendar;

    .line 1908120
    const-string v0, "1900/01/01"

    iget-object v1, p0, LX/CWP;->h:Ljava/util/Calendar;

    invoke-direct {p0, v0, v1}, LX/CWP;->a(Ljava/lang/String;Ljava/util/Calendar;)V

    .line 1908121
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWP;->i:Ljava/util/Calendar;

    .line 1908122
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/CWP;->l:Ljava/util/TreeMap;

    .line 1908123
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWP;->k:Ljava/util/Calendar;

    .line 1908124
    iput-object p4, p0, LX/CWP;->m:Landroid/content/Context;

    .line 1908125
    return-void
.end method

.method public static a(LX/CWP;Ljava/util/Date;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")",
            "LX/0Px",
            "<",
            "LX/CWQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1908106
    iget-object v0, p0, LX/CWP;->l:Ljava/util/TreeMap;

    iget-object v1, p0, LX/CWP;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1908107
    const/4 v0, 0x0

    .line 1908108
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/CWP;->l:Ljava/util/TreeMap;

    iget-object v1, p0, LX/CWP;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v1, LX/CWO;

    invoke-direct {v1, p0}, LX/CWO;-><init>(LX/CWP;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 4

    .prologue
    .line 1908103
    :try_start_0
    iget-object v0, p0, LX/CWP;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1908104
    :goto_0
    return-void

    .line 1908105
    :catch_0
    iget-object v0, p0, LX/CWP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pages_platform_date_parsing_failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CWW;LX/CWH;)LX/CWW;
    .locals 5

    .prologue
    .line 1908085
    iget-object v0, p0, LX/CWP;->j:Ljava/util/Calendar;

    iget-object v1, p0, LX/CWP;->n:LX/CWI;

    iget-object v2, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v1, v2, p2}, LX/CWI;->a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908086
    iget-object v0, p0, LX/CWP;->k:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1908087
    iget-object v0, p0, LX/CWP;->k:Ljava/util/Calendar;

    iget-object v1, p0, LX/CWP;->n:LX/CWI;

    iget-object v2, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v1, v2, p2}, LX/CWI;->b(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908088
    :cond_0
    invoke-static {p1}, LX/CWX;->a(LX/CWW;)LX/CWX;

    move-result-object v1

    .line 1908089
    iget-object v0, p0, LX/CWP;->n:LX/CWI;

    iget-object v2, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    iget-object v3, p0, LX/CWP;->l:Ljava/util/TreeMap;

    iget-object v4, p0, LX/CWP;->g:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iget-object p2, p0, LX/CWP;->h:Ljava/util/Calendar;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-interface {v0, v2, v3, v4, p2}, LX/CWI;->a(Ljava/util/Date;Ljava/util/TreeMap;Ljava/util/Date;Ljava/util/Date;)LX/CWF;

    move-result-object v0

    .line 1908090
    iget-object v2, p0, LX/CWP;->k:Ljava/util/Calendar;

    if-nez v2, :cond_1

    .line 1908091
    const/4 v2, 0x0

    .line 1908092
    iput-boolean v2, v1, LX/CWX;->b:Z

    .line 1908093
    :goto_0
    iput-object v0, v1, LX/CWX;->a:LX/CWF;

    .line 1908094
    iget-object v0, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {p0, v0}, LX/CWP;->a(LX/CWP;Ljava/util/Date;)LX/0Px;

    move-result-object v0

    .line 1908095
    iput-object v0, v1, LX/CWX;->d:LX/0Px;

    .line 1908096
    invoke-virtual {v1}, LX/CWX;->a()LX/CWW;

    move-result-object v0

    move-object v0, v0

    .line 1908097
    return-object v0

    .line 1908098
    :cond_1
    const/4 v2, 0x1

    .line 1908099
    iput-boolean v2, v1, LX/CWX;->b:Z

    .line 1908100
    iget-object v2, p0, LX/CWP;->f:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1908101
    iput-object v2, v1, LX/CWX;->c:Ljava/lang/String;

    .line 1908102
    iget-object v2, p0, LX/CWP;->n:LX/CWI;

    iget-object v3, p0, LX/CWP;->n:LX/CWI;

    iget-object v4, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iget-object p2, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-interface {v3, v4, p2}, LX/CWI;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v3

    iget-object v4, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-interface {v2, v0, v3, v4}, LX/CWI;->a(LX/CWF;ILjava/util/Date;)LX/CWF;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/CWW;Ljava/lang/String;)LX/CWW;
    .locals 5

    .prologue
    .line 1908072
    invoke-static {p1}, LX/CWX;->a(LX/CWW;)LX/CWX;

    move-result-object v0

    .line 1908073
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1908074
    iget-object v2, p0, LX/CWP;->k:Ljava/util/Calendar;

    iget-object v3, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908075
    iget-object v2, p0, LX/CWP;->k:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 1908076
    const/4 v2, 0x1

    .line 1908077
    iput-boolean v2, v0, LX/CWX;->b:Z

    .line 1908078
    iget-object v2, p0, LX/CWP;->f:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1908079
    iput-object v2, v0, LX/CWX;->c:Ljava/lang/String;

    .line 1908080
    iget-object v2, p0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-static {p0, v2}, LX/CWP;->a(LX/CWP;Ljava/util/Date;)LX/0Px;

    move-result-object v2

    .line 1908081
    iput-object v2, v0, LX/CWX;->d:LX/0Px;

    .line 1908082
    iget-object v2, p0, LX/CWP;->n:LX/CWI;

    iget-object v3, p1, LX/CWW;->a:LX/CWF;

    iget-object v4, p0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-interface {v2, v3, v1, v4}, LX/CWI;->a(LX/CWF;ILjava/util/Date;)LX/CWF;

    move-result-object v1

    .line 1908083
    iput-object v1, v0, LX/CWX;->a:LX/CWF;

    .line 1908084
    invoke-virtual {v0}, LX/CWX;->a()LX/CWW;

    move-result-object v0

    return-object v0
.end method
