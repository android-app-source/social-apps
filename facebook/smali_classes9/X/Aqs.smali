.class public final LX/Aqs;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V
    .locals 0

    .prologue
    .line 1718396
    iput-object p1, p0, LX/Aqs;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 2

    .prologue
    .line 1718388
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1718389
    iget-object v1, p0, LX/Aqs;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1718390
    iget-object v1, p0, LX/Aqs;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1718391
    iget-object v1, p0, LX/Aqs;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1718392
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1718393
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718394
    iget-object v0, p0, LX/Aqs;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1718395
    :cond_0
    return-void
.end method
