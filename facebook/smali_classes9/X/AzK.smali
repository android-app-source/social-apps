.class public final LX/AzK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1lZ;

.field public final synthetic b:LX/1FJ;

.field public final synthetic c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;LX/1lZ;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1732404
    iput-object p1, p0, LX/AzK;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iput-object p2, p0, LX/AzK;->a:LX/1lZ;

    iput-object p3, p0, LX/AzK;->b:LX/1FJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1732405
    :try_start_0
    iget-object v0, p0, LX/AzK;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->e:LX/9iU;

    iget-object v1, p0, LX/AzK;->a:LX/1lZ;

    invoke-virtual {v0, v1}, LX/9iU;->a(Ljava/io/InputStream;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1732406
    iget-object v1, p0, LX/AzK;->a:LX/1lZ;

    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1732407
    iget-object v1, p0, LX/AzK;->b:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1732408
    :catch_0
    iget-object v0, p0, LX/AzK;->a:LX/1lZ;

    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1732409
    iget-object v0, p0, LX/AzK;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    const/4 v0, 0x0

    goto :goto_0

    .line 1732410
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/AzK;->a:LX/1lZ;

    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1732411
    iget-object v1, p0, LX/AzK;->b:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
