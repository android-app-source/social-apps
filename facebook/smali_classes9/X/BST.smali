.class public final enum LX/BST;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BST;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BST;

.field public static final enum HIDE_AD:LX/BST;

.field public static final enum NONE:LX/BST;

.field public static final enum NO_VIDEO_AD:LX/BST;

.field public static final enum PLAYBACK_ERROR:LX/BST;

.field public static final enum SHORT_AD:LX/BST;

.field public static final enum TIME_SPENT_INSUFFICIENT:LX/BST;

.field public static final enum WAIT_FOR_FAILED:LX/BST;


# instance fields
.field private final mCommercialBreakNoAdReason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1785490
    new-instance v0, LX/BST;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v4, v2}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->NONE:LX/BST;

    .line 1785491
    new-instance v0, LX/BST;

    const-string v1, "NO_VIDEO_AD"

    const-string v2, "no_video_ad"

    invoke-direct {v0, v1, v5, v2}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->NO_VIDEO_AD:LX/BST;

    .line 1785492
    new-instance v0, LX/BST;

    const-string v1, "TIME_SPENT_INSUFFICIENT"

    const-string v2, "time_spent_insufficient"

    invoke-direct {v0, v1, v6, v2}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->TIME_SPENT_INSUFFICIENT:LX/BST;

    .line 1785493
    new-instance v0, LX/BST;

    const-string v1, "PLAYBACK_ERROR"

    const-string v2, "playback_error"

    invoke-direct {v0, v1, v7, v2}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->PLAYBACK_ERROR:LX/BST;

    .line 1785494
    new-instance v0, LX/BST;

    const-string v1, "HIDE_AD"

    const-string v2, "hide_ad"

    invoke-direct {v0, v1, v8, v2}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->HIDE_AD:LX/BST;

    .line 1785495
    new-instance v0, LX/BST;

    const-string v1, "WAIT_FOR_FAILED"

    const/4 v2, 0x5

    const-string v3, "wait_for_failed"

    invoke-direct {v0, v1, v2, v3}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->WAIT_FOR_FAILED:LX/BST;

    .line 1785496
    new-instance v0, LX/BST;

    const-string v1, "SHORT_AD"

    const/4 v2, 0x6

    const-string v3, "short_ad"

    invoke-direct {v0, v1, v2, v3}, LX/BST;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BST;->SHORT_AD:LX/BST;

    .line 1785497
    const/4 v0, 0x7

    new-array v0, v0, [LX/BST;

    sget-object v1, LX/BST;->NONE:LX/BST;

    aput-object v1, v0, v4

    sget-object v1, LX/BST;->NO_VIDEO_AD:LX/BST;

    aput-object v1, v0, v5

    sget-object v1, LX/BST;->TIME_SPENT_INSUFFICIENT:LX/BST;

    aput-object v1, v0, v6

    sget-object v1, LX/BST;->PLAYBACK_ERROR:LX/BST;

    aput-object v1, v0, v7

    sget-object v1, LX/BST;->HIDE_AD:LX/BST;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BST;->WAIT_FOR_FAILED:LX/BST;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BST;->SHORT_AD:LX/BST;

    aput-object v2, v0, v1

    sput-object v0, LX/BST;->$VALUES:[LX/BST;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1785498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1785499
    iput-object p3, p0, LX/BST;->mCommercialBreakNoAdReason:Ljava/lang/String;

    .line 1785500
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BST;
    .locals 1

    .prologue
    .line 1785501
    const-class v0, LX/BST;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BST;

    return-object v0
.end method

.method public static values()[LX/BST;
    .locals 1

    .prologue
    .line 1785502
    sget-object v0, LX/BST;->$VALUES:[LX/BST;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BST;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785503
    iget-object v0, p0, LX/BST;->mCommercialBreakNoAdReason:Ljava/lang/String;

    return-object v0
.end method
