.class public LX/B9c;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field public a:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field public final h:Landroid/widget/TextView;

.field private final i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/ImageView;

.field private l:Lcom/facebook/fbui/facepile/FacepileView;

.field private m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Z

.field public o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public p:LX/78A;

.field private q:Ljava/lang/Runnable;

.field private r:I

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1752004
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1752005
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9c;->s:Z

    .line 1752006
    const-class v0, LX/B9c;

    invoke-static {v0, p0}, LX/B9c;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1752007
    const v0, 0x7f0301da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1752008
    const v0, 0x7f0d0784

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9c;->e:Landroid/widget/TextView;

    .line 1752009
    const v0, 0x7f0d0785

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9c;->f:Landroid/widget/TextView;

    .line 1752010
    const v0, 0x7f0d078b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9c;->g:Landroid/widget/TextView;

    .line 1752011
    const v0, 0x7f0d078c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9c;->h:Landroid/widget/TextView;

    .line 1752012
    const v0, 0x7f0d0783

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/B9c;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1752013
    const v0, 0x7f0d0786

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/B9c;->k:Landroid/widget/ImageView;

    .line 1752014
    const v0, 0x7f0d078d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/B9c;->j:Landroid/view/View;

    .line 1752015
    const v0, 0x7f0d0788

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/B9c;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1752016
    const v0, 0x7f0d0787

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/B9c;->l:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1752017
    invoke-virtual {p0}, LX/B9c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/B9c;->r:I

    .line 1752018
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/B9c;->setBackgroundResource(I)V

    .line 1752019
    return-void
.end method

.method private a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 1751998
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1751999
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1752000
    :goto_0
    return-void

    .line 1752001
    :cond_1
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1752002
    iget-object v0, p0, LX/B9c;->d:LX/23P;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1752003
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/B9c;

    const-class v1, LX/13A;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/13A;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v2

    check-cast v2, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v3

    check-cast v3, LX/3Rb;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object v1, p1, LX/B9c;->a:LX/13A;

    iput-object v2, p1, LX/B9c;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v3, p1, LX/B9c;->c:LX/3Rb;

    iput-object p0, p1, LX/B9c;->d:LX/23P;

    return-void
.end method

.method public static b$redex0(LX/B9c;)V
    .locals 1

    .prologue
    .line 1751993
    iget-object v0, p0, LX/B9c;->q:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1751994
    iget-object v0, p0, LX/B9c;->q:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1751995
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9c;->n:Z

    .line 1751996
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9c;->setVisibility(I)V

    .line 1751997
    return-void
.end method

.method private setFacepileUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1751988
    iget-object v1, p0, LX/B9c;->l:Lcom/facebook/fbui/facepile/FacepileView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1751989
    iget-object v0, p0, LX/B9c;->l:Lcom/facebook/fbui/facepile/FacepileView;

    iget v1, p0, LX/B9c;->r:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceSize(I)V

    .line 1751990
    iget-object v0, p0, LX/B9c;->l:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 1751991
    return-void

    .line 1751992
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setSocialContext(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1751984
    iget-object v0, p0, LX/B9c;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751985
    iget-object v1, p0, LX/B9c;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1751986
    return-void

    .line 1751987
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1751907
    iget-object v0, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 1751908
    iget-boolean v0, p0, LX/B9c;->n:Z

    if-eqz v0, :cond_0

    .line 1751909
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9c;->setVisibility(I)V

    .line 1751910
    :cond_0
    :goto_0
    return-void

    .line 1751911
    :cond_1
    iput-object p1, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1751912
    iget-object v0, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    .line 1751913
    if-nez v0, :cond_2

    .line 1751914
    invoke-static {p0}, LX/B9c;->b$redex0(LX/B9c;)V

    goto :goto_0

    .line 1751915
    :cond_2
    iget-object v1, p0, LX/B9c;->a:LX/13A;

    iget-object v3, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v1, v3, p2, v0, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v1

    iput-object v1, p0, LX/B9c;->p:LX/78A;

    .line 1751916
    new-instance v1, LX/B9Z;

    invoke-direct {v1, p0}, LX/B9Z;-><init>(LX/B9c;)V

    .line 1751917
    new-instance v3, LX/B9a;

    invoke-direct {v3, p0}, LX/B9a;-><init>(LX/B9c;)V

    .line 1751918
    new-instance v4, LX/B9b;

    invoke-direct {v4, p0}, LX/B9b;-><init>(LX/B9c;)V

    .line 1751919
    iget-object v5, p0, LX/B9c;->h:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751920
    iget-object v1, p0, LX/B9c;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751921
    iget-object v1, p0, LX/B9c;->j:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751922
    iget-object v1, p0, LX/B9c;->e:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751923
    iget-object v1, p0, LX/B9c;->f:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751924
    sget-object v1, LX/76S;->ANY:LX/76S;

    invoke-static {v0, v1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v1

    .line 1751925
    if-eqz v1, :cond_4

    .line 1751926
    iget-object v3, p0, LX/B9c;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    .line 1751927
    const/4 v4, 0x0

    .line 1751928
    iget-object v5, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    if-eqz v5, :cond_9

    .line 1751929
    iget-object v4, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    const-string v5, "megaphone_image_resize_mode_option"

    invoke-virtual {v4, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v5, v4

    .line 1751930
    :goto_1
    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1751931
    if-eqz v5, :cond_8

    const-string p1, "cover"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1751932
    invoke-virtual {v4, p2, p2, p2, p2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1751933
    const/4 v5, -0x1

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1751934
    const/4 v5, -0x2

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1751935
    iget v4, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->width:I

    int-to-float v4, v4

    iget v5, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->height:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1751936
    :goto_2
    iget-object v4, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 1751937
    :goto_3
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1751938
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v3, p0, LX/B9c;->k:Landroid/widget/ImageView;

    .line 1751939
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1751940
    iget-object v5, p0, LX/B9c;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v5, v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1751941
    iget-object v5, p0, LX/B9c;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v5, v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1751942
    iget-object v4, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1751943
    :goto_4
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, p0, LX/B9c;->h:Landroid/widget/TextView;

    invoke-direct {p0, v1, v3}, LX/B9c;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 1751944
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, p0, LX/B9c;->g:Landroid/widget/TextView;

    invoke-direct {p0, v1, v3}, LX/B9c;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 1751945
    invoke-direct {p0, v6}, LX/B9c;->setFacepileUrls(Ljava/util/List;)V

    .line 1751946
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v1, :cond_6

    .line 1751947
    invoke-direct {p0, v6}, LX/B9c;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 1751948
    :cond_3
    :goto_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9c;->s:Z

    .line 1751949
    iput-boolean v2, p0, LX/B9c;->n:Z

    .line 1751950
    invoke-virtual {p0, v2}, LX/B9c;->setVisibility(I)V

    goto/16 :goto_0

    .line 1751951
    :cond_4
    iget-object v1, p0, LX/B9c;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_3

    .line 1751952
    :cond_5
    iget-object v1, p0, LX/B9c;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_4

    .line 1751953
    :cond_6
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-direct {p0, v1}, LX/B9c;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 1751954
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 1751955
    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1751956
    invoke-virtual {p0}, LX/B9c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1751957
    invoke-static {v7}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v5

    move v1, v2

    .line 1751958
    :goto_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    if-ge v1, v7, :cond_7

    .line 1751959
    iget-object v6, p0, LX/B9c;->c:LX/3Rb;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0, v4, v4}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1751960
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1751961
    :cond_7
    invoke-direct {p0, v5}, LX/B9c;->setFacepileUrls(Ljava/util/List;)V

    goto :goto_5

    .line 1751962
    :cond_8
    iget-object v5, p0, LX/B9c;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v5, v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1751963
    iget-object v5, p0, LX/B9c;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v5, v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1751964
    invoke-virtual {p0}, LX/B9c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0b19d4

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, p2, v5, p2, p2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_2

    :cond_9
    move-object v5, v4

    goto/16 :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1751971
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1751972
    iget-boolean v0, p0, LX/B9c;->s:Z

    if-eqz v0, :cond_2

    .line 1751973
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9c;->s:Z

    .line 1751974
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    .line 1751975
    iget-object p1, p0, LX/B9c;->h:Landroid/widget/TextView;

    invoke-static {p1}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1751976
    iget-object p1, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    .line 1751977
    iput-object p1, v0, LX/77n;->c:Ljava/lang/String;

    .line 1751978
    :cond_0
    iget-object p1, p0, LX/B9c;->g:Landroid/widget/TextView;

    invoke-static {p1}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1751979
    iget-object p1, p0, LX/B9c;->o:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    .line 1751980
    iput-object p1, v0, LX/77n;->d:Ljava/lang/String;

    .line 1751981
    :cond_1
    iget-object p1, p0, LX/B9c;->p:LX/78A;

    invoke-virtual {p1}, LX/78A;->a()V

    .line 1751982
    iget-object p1, p0, LX/B9c;->p:LX/78A;

    invoke-virtual {p1, v0}, LX/78A;->a(LX/77n;)V

    .line 1751983
    :cond_2
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1751967
    iget-boolean v0, p0, LX/B9c;->n:Z

    if-eqz v0, :cond_0

    .line 1751968
    invoke-virtual {p0, v1, v1}, LX/B9c;->setMeasuredDimension(II)V

    .line 1751969
    :goto_0
    return-void

    .line 1751970
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1751965
    iput-object p1, p0, LX/B9c;->q:Ljava/lang/Runnable;

    .line 1751966
    return-void
.end method
