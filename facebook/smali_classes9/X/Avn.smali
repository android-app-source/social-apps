.class public LX/Avn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/media/AudioManager;

.field private d:LX/7Cb;

.field public e:LX/7Cb;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Yd;

.field public i:Z

.field private j:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/0Or;Landroid/media/AudioManager;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;",
            "Landroid/media/AudioManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1724652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724653
    iput-object p1, p0, LX/Avn;->a:Landroid/content/Context;

    .line 1724654
    iput-object p2, p0, LX/Avn;->b:LX/0Or;

    .line 1724655
    iget-object v0, p0, LX/Avn;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cb;

    iput-object v0, p0, LX/Avn;->d:LX/7Cb;

    .line 1724656
    iget-object v0, p0, LX/Avn;->d:LX/7Cb;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1724657
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, v0, LX/7Cb;->f:Z

    .line 1724658
    iget-object v0, p0, LX/Avn;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cb;

    iput-object v0, p0, LX/Avn;->e:LX/7Cb;

    .line 1724659
    iput-object p3, p0, LX/Avn;->c:Landroid/media/AudioManager;

    .line 1724660
    return-void
.end method

.method public static b(LX/0QB;)LX/Avn;
    .locals 4

    .prologue
    .line 1724650
    new-instance v2, LX/Avn;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x3563

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-direct {v2, v0, v3, v1}, LX/Avn;-><init>(Landroid/content/Context;LX/0Or;Landroid/media/AudioManager;)V

    .line 1724651
    return-object v2
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1724638
    iget-boolean v0, p0, LX/Avn;->i:Z

    if-eqz v0, :cond_0

    .line 1724639
    iget-object v0, p0, LX/Avn;->a:Landroid/content/Context;

    iget-object v1, p0, LX/Avn;->h:LX/0Yd;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1724640
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Avn;->i:Z

    .line 1724641
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1724646
    iget-object v0, p0, LX/Avn;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/Avn;->j:Z

    if-nez v0, :cond_0

    .line 1724647
    iget-object v0, p0, LX/Avn;->d:LX/7Cb;

    iget-object v1, p0, LX/Avn;->f:Landroid/net/Uri;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/7Cb;->a(Landroid/net/Uri;I)V

    .line 1724648
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Avn;->j:Z

    .line 1724649
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1724642
    iget-boolean v0, p0, LX/Avn;->j:Z

    if-eqz v0, :cond_0

    .line 1724643
    iget-object v0, p0, LX/Avn;->d:LX/7Cb;

    invoke-virtual {v0}, LX/7Cb;->a()V

    .line 1724644
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Avn;->j:Z

    .line 1724645
    :cond_0
    return-void
.end method
