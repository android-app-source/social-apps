.class public LX/CSD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/CSD;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/notifications/subscription/NotificationTypeSubscriber;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/notifications/subscription/NotificationSubscriber;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private final f:Ljava/util/concurrent/locks/Lock;


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/notifications/subscription/NotificationTypeSubscriber;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/notifications/subscription/NotificationSubscriber;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1894399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894400
    iput-object p3, p0, LX/CSD;->c:Ljava/util/concurrent/ExecutorService;

    .line 1894401
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CSD;->a:Ljava/util/Map;

    .line 1894402
    iput-object p2, p0, LX/CSD;->b:Ljava/util/Set;

    .line 1894403
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, LX/CSD;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 1894404
    iget-object v0, p0, LX/CSD;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    iput-object v0, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    .line 1894405
    iget-object v0, p0, LX/CSD;->d:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    iput-object v0, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    .line 1894406
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2V;

    .line 1894407
    sget-object p1, LX/D2W;->a:[Ljava/lang/String;

    move-object p1, p1

    .line 1894408
    invoke-static {p0, v0, p1}, LX/CSD;->b(LX/CSD;LX/D2V;[Ljava/lang/String;)V

    .line 1894409
    goto :goto_0

    .line 1894410
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/CSD;
    .locals 7

    .prologue
    .line 1894383
    sget-object v0, LX/CSD;->g:LX/CSD;

    if-nez v0, :cond_1

    .line 1894384
    const-class v1, LX/CSD;

    monitor-enter v1

    .line 1894385
    :try_start_0
    sget-object v0, LX/CSD;->g:LX/CSD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1894386
    if-eqz v2, :cond_0

    .line 1894387
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1894388
    new-instance v4, LX/CSD;

    .line 1894389
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/CSF;

    invoke-direct {v6, v0}, LX/CSF;-><init>(LX/0QB;)V

    invoke-direct {v3, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v3

    .line 1894390
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance p0, LX/CSE;

    invoke-direct {p0, v0}, LX/CSE;-><init>(LX/0QB;)V

    invoke-direct {v3, v6, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v3

    .line 1894391
    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v4, v5, v6, v3}, LX/CSD;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/concurrent/ExecutorService;)V

    .line 1894392
    move-object v0, v4

    .line 1894393
    sput-object v0, LX/CSD;->g:LX/CSD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1894394
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1894395
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1894396
    :cond_1
    sget-object v0, LX/CSD;->g:LX/CSD;

    return-object v0

    .line 1894397
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1894398
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/CSD;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1894343
    iget-object v0, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1894344
    :try_start_0
    iget-object v0, p0, LX/CSD;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 1894345
    iget-object v1, p0, LX/CSD;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;-><init>(LX/CSD;LX/0Rf;Ljava/util/List;)V

    const v0, 0x18cf8b19

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1894346
    iget-object v0, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1894347
    return-void

    .line 1894348
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1894375
    iget-object v0, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1894376
    :try_start_0
    iget-object v0, p0, LX/CSD;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1894377
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1894378
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 1894379
    iget-object v1, p0, LX/CSD;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;-><init>(LX/CSD;LX/0Rf;Lcom/facebook/graphql/model/GraphQLStory;)V

    const v0, -0x1d7641d8

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1894380
    :cond_0
    iget-object v0, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1894381
    return-void

    .line 1894382
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/CSD;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static varargs b(LX/CSD;LX/D2V;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1894365
    iget-object v0, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1894366
    :try_start_0
    array-length v2, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p2, v1

    .line 1894367
    iget-object v0, p0, LX/CSD;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1894368
    if-nez v0, :cond_0

    .line 1894369
    iget-object v4, p0, LX/CSD;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894370
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1894371
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1894372
    :cond_1
    iget-object v0, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1894373
    return-void

    .line 1894374
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(LX/D2V;)V
    .locals 2

    .prologue
    .line 1894360
    iget-object v0, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1894361
    :try_start_0
    iget-object v0, p0, LX/CSD;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1894362
    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1894363
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    iget-object v0, p0, LX/CSD;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1894364
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1894349
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1894350
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 1894351
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1894352
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1894353
    const-string v4, "notif_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1894354
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1894355
    const-string v4, "notif_type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, LX/CSD;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1894356
    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "notif_type"

    aput-object v4, v0, v3

    goto :goto_0

    .line 1894357
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1894358
    invoke-static {p0, v1}, LX/CSD;->a(LX/CSD;Ljava/util/List;)V

    .line 1894359
    :cond_2
    return-void
.end method
