.class public final LX/Bfi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field private a:LX/69B;


# direct methods
.method public constructor <init>(LX/69B;)V
    .locals 0

    .prologue
    .line 1806420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1806421
    iput-object p1, p0, LX/Bfi;->a:LX/69B;

    .line 1806422
    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 10

    .prologue
    .line 1806423
    iget-object v0, p0, LX/Bfi;->a:LX/69B;

    const v1, -0xffff01

    .line 1806424
    iput v1, v0, LX/69B;->a:I

    .line 1806425
    iget-object v0, p0, LX/Bfi;->a:LX/69B;

    const/high16 v1, 0x41600000    # 14.0f

    .line 1806426
    iput v1, v0, LX/69B;->e:F

    .line 1806427
    iget-object v0, p0, LX/Bfi;->a:LX/69B;

    .line 1806428
    new-instance v1, LX/69A;

    invoke-direct {v1, p1, v0}, LX/69A;-><init>(LX/680;LX/69B;)V

    invoke-virtual {p1, v1}, LX/680;->a(LX/67m;)LX/67m;

    move-result-object v1

    check-cast v1, LX/69A;

    .line 1806429
    iget-object v0, p0, LX/Bfi;->a:LX/69B;

    .line 1806430
    iget-object v1, v0, LX/69B;->c:Ljava/util/List;

    move-object v1, v1

    .line 1806431
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v2

    .line 1806432
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 1806433
    invoke-virtual {v2, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    goto :goto_0

    .line 1806434
    :cond_0
    invoke-virtual {v2}, LX/696;->a()LX/697;

    move-result-object v2

    .line 1806435
    invoke-virtual {v2}, LX/697;->b()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v3

    .line 1806436
    iget-object v0, v2, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-object v0, v2, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double/2addr v4, v6

    .line 1806437
    iget-object v0, v2, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-object v0, v2, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v8, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v6, v8

    .line 1806438
    cmpl-double v0, v4, v6

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 1806439
    :goto_1
    new-instance v4, LX/Bfh;

    invoke-direct {v4, p0, v0, v3}, LX/Bfh;-><init>(LX/Bfi;ZLcom/facebook/android/maps/model/LatLng;)V

    invoke-static {v1, v4}, Ljava/util/Collections;->min(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 1806440
    new-instance v1, LX/67d;

    invoke-direct {v1}, LX/67d;-><init>()V

    .line 1806441
    iput-object v2, v1, LX/67d;->i:LX/697;

    .line 1806442
    iput-object v0, v1, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1806443
    move-object v0, v1

    .line 1806444
    invoke-virtual {p1, v0}, LX/680;->b(LX/67d;)V

    .line 1806445
    const/high16 v0, 0x41700000    # 15.0f

    invoke-static {v0}, LX/67e;->c(F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->b(LX/67d;)V

    .line 1806446
    return-void

    .line 1806447
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
