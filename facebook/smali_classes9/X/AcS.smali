.class public final LX/AcS;
.super LX/1OD;
.source ""


# instance fields
.field public final synthetic a:LX/AcX;


# direct methods
.method public constructor <init>(LX/AcX;)V
    .locals 0

    .prologue
    .line 1692593
    iput-object p1, p0, LX/AcS;->a:LX/AcX;

    invoke-direct {p0}, LX/1OD;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1692594
    invoke-super {p0, p1, p2}, LX/1OD;->a(II)V

    .line 1692595
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692596
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692597
    move-object v0, v1

    .line 1692598
    if-nez v0, :cond_1

    .line 1692599
    :cond_0
    :goto_0
    return-void

    .line 1692600
    :cond_1
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    iget-object v0, v0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int v1, p1, p2

    if-ne v0, v1, :cond_0

    .line 1692601
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692602
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692603
    move-object v0, v1

    .line 1692604
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l()V

    goto :goto_0
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 1692605
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692606
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692607
    move-object v0, v1

    .line 1692608
    if-nez v0, :cond_1

    .line 1692609
    :cond_0
    :goto_0
    return-void

    .line 1692610
    :cond_1
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692611
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692612
    move-object v0, v1

    .line 1692613
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1692614
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692615
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692616
    move-object v0, v1

    .line 1692617
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getLastCompletelyVisiblePosition()I

    move-result v0

    add-int/lit8 v1, p1, -0x1

    if-lt v0, v1, :cond_3

    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    iget-object v0, v0, LX/AcX;->k:LX/Aev;

    .line 1692618
    iget-boolean v1, v0, LX/Aev;->h:Z

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/Aev;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AfD;

    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 1692619
    iget-object v0, v1, LX/AfD;->g:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1692620
    iget-object v0, v1, LX/AfD;->i:LX/3Af;

    if-eqz v0, :cond_5

    iget-object v0, v1, LX/AfD;->i:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1692621
    :cond_2
    :goto_1
    move v1, p1

    .line 1692622
    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 1692623
    if-nez v0, :cond_3

    .line 1692624
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692625
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692626
    move-object v0, v1

    .line 1692627
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p0, LX/AcS;->a:LX/AcX;

    iget-object v1, v1, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    goto :goto_0

    .line 1692628
    :cond_3
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    iget-object v0, v0, LX/AcX;->n:LX/AeS;

    .line 1692629
    iget-object v1, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1692630
    const/4 v1, 0x0

    .line 1692631
    :goto_3
    move-object v1, v1

    .line 1692632
    if-eqz v1, :cond_0

    .line 1692633
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692634
    iget-object p0, v0, LX/AVi;->a:Landroid/view/View;

    move-object p0, p0

    .line 1692635
    move-object v0, p0

    .line 1692636
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    invoke-virtual {v0, v1}, LX/AcH;->a(LX/AeO;)V

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    move p1, p2

    .line 1692637
    goto :goto_1

    .line 1692638
    :cond_6
    iget-object v0, v1, LX/AfD;->h:LX/5OM;

    if-eqz v0, :cond_7

    iget-object v0, v1, LX/AfD;->h:LX/5OM;

    .line 1692639
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1692640
    if-nez v0, :cond_2

    :cond_7
    move p1, p2

    goto :goto_1

    :cond_8
    iget-object v1, v0, LX/AeS;->g:Ljava/util/List;

    iget-object p1, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AeO;

    goto :goto_3
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 1692641
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692642
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692643
    move-object v0, v1

    .line 1692644
    if-nez v0, :cond_1

    .line 1692645
    :cond_0
    :goto_0
    return-void

    .line 1692646
    :cond_1
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692647
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692648
    move-object v0, v1

    .line 1692649
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getLastCompletelyVisiblePosition()I

    move-result v0

    iget-object v1, p0, LX/AcS;->a:LX/AcX;

    iget-object v1, v1, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1692650
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692651
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692652
    move-object v0, v1

    .line 1692653
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1692654
    iget-object v0, p0, LX/AcS;->a:LX/AcX;

    .line 1692655
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1692656
    move-object v0, v1

    .line 1692657
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    .line 1692658
    invoke-virtual {v0}, LX/9Bh;->b()V

    .line 1692659
    goto :goto_0
.end method
