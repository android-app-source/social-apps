.class public LX/C6v;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C6w;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C6v",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C6w;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850693
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850694
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C6v;->b:LX/0Zi;

    .line 1850695
    iput-object p1, p0, LX/C6v;->a:LX/0Ot;

    .line 1850696
    return-void
.end method

.method public static a(LX/0QB;)LX/C6v;
    .locals 4

    .prologue
    .line 1850682
    const-class v1, LX/C6v;

    monitor-enter v1

    .line 1850683
    :try_start_0
    sget-object v0, LX/C6v;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850684
    sput-object v2, LX/C6v;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850687
    new-instance v3, LX/C6v;

    const/16 p0, 0x1f81

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C6v;-><init>(LX/0Ot;)V

    .line 1850688
    move-object v0, v3

    .line 1850689
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850690
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850691
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1850626
    check-cast p2, LX/C6u;

    .line 1850627
    iget-object v0, p0, LX/C6v;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C6w;

    iget-object v2, p2, LX/C6u;->a:Ljava/lang/String;

    iget-object v3, p2, LX/C6u;->b:LX/0Px;

    iget-object v4, p2, LX/C6u;->c:Ljava/lang/String;

    iget-boolean v5, p2, LX/C6u;->d:Z

    iget-object v6, p2, LX/C6u;->e:LX/C6a;

    iget-object v7, p2, LX/C6u;->f:LX/C6H;

    iget-object v8, p2, LX/C6u;->g:Ljava/lang/String;

    move-object v1, p1

    .line 1850628
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 1850629
    iget-boolean v9, v7, LX/C6H;->a:Z

    move v9, v9

    .line 1850630
    if-nez v9, :cond_9

    .line 1850631
    iget-boolean v9, v6, LX/C6a;->k:Z

    move v9, v9

    .line 1850632
    if-nez v9, :cond_9

    if-nez v5, :cond_0

    .line 1850633
    iget-boolean v9, v6, LX/C6a;->b:Z

    move v9, v9

    .line 1850634
    if-eqz v9, :cond_9

    :cond_0
    const/4 v9, 0x1

    :goto_0
    move v9, v9

    .line 1850635
    if-nez v9, :cond_1

    .line 1850636
    const/4 v9, 0x0

    .line 1850637
    :goto_1
    move-object v0, v9

    .line 1850638
    return-object v0

    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v9, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v10

    const v11, 0x7f0a011a

    invoke-virtual {v10, v11}, LX/25Q;->i(I)LX/25Q;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, LX/1Di;->r(I)LX/1Di;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {v0, v1, v8, v5}, LX/C6w;->a(LX/C6w;LX/1De;Ljava/lang/String;Z)LX/1Di;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    .line 1850639
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b10ad

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a00ab

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v10

    sget-object v11, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v10, v11}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const/4 v11, 0x1

    const v0, 0x7f0b10ab

    invoke-interface {v10, v11, v0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v10

    const/4 v11, 0x3

    const v0, 0x7f0b10ac

    invoke-interface {v10, v11, v0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v10

    const/4 v11, 0x6

    const v0, 0x7f0b10aa

    invoke-interface {v10, v11, v0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v10

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v0, 0x7f0a009a

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-interface {v10, v11}, LX/1Di;->y(I)LX/1Di;

    move-result-object v10

    move-object v10, v10

    .line 1850640
    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x2

    .line 1850641
    const-string v10, "only_num"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1850642
    const/4 v10, 0x0

    .line 1850643
    :goto_2
    move-object v10, v10

    .line 1850644
    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 1850645
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const/4 p0, 0x2

    invoke-interface {v10, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const/4 p0, 0x4

    invoke-interface {v10, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v10

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0a009a

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-interface {v10, p0}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object p1

    move p0, v12

    .line 1850646
    :goto_3
    const/4 v10, 0x5

    if-ge p0, v10, :cond_8

    .line 1850647
    if-eqz v5, :cond_5

    .line 1850648
    iget v10, v6, LX/C6a;->e:I

    move v10, v10

    .line 1850649
    if-ne v10, p0, :cond_4

    move v10, v11

    .line 1850650
    :goto_4
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v12}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p2

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a009a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-interface {p2, v0}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object p2

    const/4 v0, 0x3

    const v2, 0x7f0b10b1

    invoke-interface {p2, v0, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p2

    const/4 v0, 0x0

    .line 1850651
    new-instance v2, LX/Aq7;

    invoke-direct {v2}, LX/Aq7;-><init>()V

    .line 1850652
    sget-object v3, LX/Aq8;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Aq6;

    .line 1850653
    if-nez v3, :cond_2

    .line 1850654
    new-instance v3, LX/Aq6;

    invoke-direct {v3}, LX/Aq6;-><init>()V

    .line 1850655
    :cond_2
    invoke-static {v3, v1, v0, v0, v2}, LX/Aq6;->a$redex0(LX/Aq6;LX/1De;IILX/Aq7;)V

    .line 1850656
    move-object v2, v3

    .line 1850657
    move-object v0, v2

    .line 1850658
    move-object v0, v0

    .line 1850659
    iget-object v2, v0, LX/Aq6;->a:LX/Aq7;

    iput-boolean v10, v2, LX/Aq7;->a:Z

    .line 1850660
    move-object v10, v0

    .line 1850661
    const v0, 0xfd40530

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v3

    invoke-static {v1, v0, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 1850662
    iget-object v2, v10, LX/Aq6;->a:LX/Aq7;

    iput-object v0, v2, LX/Aq7;->b:LX/1dQ;

    .line 1850663
    move-object v10, v10

    .line 1850664
    invoke-interface {p2, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p2

    .line 1850665
    const-string v10, "only_num"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "label_with_num"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    :cond_3
    const/4 v10, 0x1

    :goto_5
    move v10, v10

    .line 1850666
    if-nez v10, :cond_7

    const/4 v10, 0x0

    :goto_6
    invoke-interface {p2, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v10

    invoke-interface {p1, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1850667
    add-int/lit8 v10, p0, 0x1

    move p0, v10

    goto/16 :goto_3

    :cond_4
    move v10, v12

    .line 1850668
    goto :goto_4

    .line 1850669
    :cond_5
    iget v10, v6, LX/C6a;->f:I

    move v10, v10

    .line 1850670
    if-ne v10, p0, :cond_6

    move v10, v11

    goto/16 :goto_4

    :cond_6
    move v10, v12

    goto/16 :goto_4

    .line 1850671
    :cond_7
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    add-int/lit8 v0, p0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v0, 0x7f0b004e

    invoke-virtual {v10, v0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    const v0, 0x7f0a00a4

    invoke-virtual {v10, v0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v10, v0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v10

    goto :goto_6

    .line 1850672
    :cond_8
    move-object v10, p1

    .line 1850673
    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    goto/16 :goto_1

    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 1850674
    :cond_a
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 1850675
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b10b5

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 1850676
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b10b6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 1850677
    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1850678
    invoke-virtual {v12, v10}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v11

    invoke-static {v11, p0, p1}, LX/C6w;->a(FII)I

    move-result p2

    .line 1850679
    const/4 v11, 0x4

    invoke-virtual {v3, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1850680
    invoke-virtual {v12, v11}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v12

    invoke-static {v12, p0, p1}, LX/C6w;->a(FII)I

    move-result v12

    .line 1850681
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p0

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0a009a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-interface {p0, p1}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object p0

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p1

    invoke-virtual {p1, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const p1, 0x7f0a00a4

    invoke-virtual {v10, p1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    const p1, 0x7f0b10b3

    invoke-virtual {v10, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v10

    sget-object p1, LX/1nd;->BOTTOM:LX/1nd;

    invoke-virtual {v10, p1}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const p1, 0x7f0b10b4

    invoke-interface {v10, p1}, LX/1Di;->m(I)LX/1Di;

    move-result-object v10

    invoke-interface {v10, v7, p2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v10

    invoke-interface {v10, v8}, LX/1Di;->c(I)LX/1Di;

    move-result-object v10

    invoke-interface {v10, v7, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v10

    invoke-interface {p0, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {p1, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const p1, 0x7f0a00a4

    invoke-virtual {v10, p1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    const p1, 0x7f0b10b3

    invoke-virtual {v10, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v10

    sget-object p1, LX/1nd;->BOTTOM:LX/1nd;

    invoke-virtual {v10, p1}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const p1, 0x7f0b10b4

    invoke-interface {v10, p1}, LX/1Di;->m(I)LX/1Di;

    move-result-object v10

    invoke-interface {p0, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v11}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v11

    const p0, 0x7f0a00a4

    invoke-virtual {v11, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v11

    const p0, 0x7f0b10b3

    invoke-virtual {v11, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v11

    invoke-virtual {v11, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v11

    sget-object p0, LX/1nd;->BOTTOM:LX/1nd;

    invoke-virtual {v11, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v11

    invoke-virtual {v11}, LX/1X5;->c()LX/1Di;

    move-result-object v11

    const p0, 0x7f0b10b4

    invoke-interface {v11, p0}, LX/1Di;->m(I)LX/1Di;

    move-result-object v11

    invoke-interface {v11, v2, v12}, LX/1Di;->e(II)LX/1Di;

    move-result-object v11

    invoke-interface {v11, v8}, LX/1Di;->c(I)LX/1Di;

    move-result-object v11

    invoke-interface {v11, v2, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v11

    invoke-interface {v10, v11}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    goto/16 :goto_2

    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1850591
    invoke-static {}, LX/1dS;->b()V

    .line 1850592
    iget v0, p1, LX/1dQ;->b:I

    .line 1850593
    sparse-switch v0, :sswitch_data_0

    .line 1850594
    :goto_0
    return-object v2

    .line 1850595
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1850596
    check-cast v0, LX/C6u;

    .line 1850597
    iget-object v1, p0, LX/C6v;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C6w;

    iget-object v3, v0, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v0, LX/C6u;->i:LX/1Pq;

    iget-object p2, v0, LX/C6u;->e:LX/C6a;

    .line 1850598
    invoke-virtual {p2}, LX/C6a;->p()V

    .line 1850599
    const/4 p0, 0x1

    new-array p0, p0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    aput-object v3, p0, v0

    invoke-interface {p1, p0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1850600
    invoke-static {v3}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p0

    .line 1850601
    iget-object v0, p2, LX/C6a;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1850602
    invoke-static {p0, v0}, LX/17Q;->h(LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1850603
    iget-object v0, v1, LX/C6w;->b:LX/0Zb;

    invoke-interface {v0, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1850604
    goto :goto_0

    .line 1850605
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1850606
    check-cast v1, LX/C6u;

    .line 1850607
    iget-object v3, p0, LX/C6v;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean v3, v1, LX/C6u;->d:Z

    iget-object v4, v1, LX/C6u;->e:LX/C6a;

    iget-object v5, v1, LX/C6u;->i:LX/1Pq;

    iget-object v6, v1, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 1850608
    if-eqz v3, :cond_3

    .line 1850609
    iget p1, v4, LX/C6a;->e:I

    move p1, p1

    .line 1850610
    if-eq p1, v0, :cond_1

    move p1, p2

    .line 1850611
    :goto_1
    invoke-virtual {v4}, LX/C6a;->f()V

    .line 1850612
    iput v0, v4, LX/C6a;->e:I

    .line 1850613
    iget-boolean v1, v4, LX/C6a;->a:Z

    move v1, v1

    .line 1850614
    if-eqz v1, :cond_2

    .line 1850615
    invoke-virtual {v4}, LX/C6a;->c()V

    .line 1850616
    :goto_2
    if-eqz p1, :cond_0

    .line 1850617
    new-array p1, p2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v6, p1, p0

    invoke-interface {v5, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1850618
    :cond_0
    goto :goto_0

    :cond_1
    move p1, p0

    .line 1850619
    goto :goto_1

    .line 1850620
    :cond_2
    invoke-virtual {v4}, LX/C6a;->h()V

    goto :goto_2

    .line 1850621
    :cond_3
    iget p1, v4, LX/C6a;->f:I

    move p1, p1

    .line 1850622
    if-eq p1, v0, :cond_4

    move p1, p2

    .line 1850623
    :goto_3
    iput v0, v4, LX/C6a;->f:I

    .line 1850624
    invoke-virtual {v4}, LX/C6a;->h()V

    goto :goto_2

    :cond_4
    move p1, p0

    .line 1850625
    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0xfd40530 -> :sswitch_1
        0x59104e1e -> :sswitch_0
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/C6t;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1850583
    new-instance v1, LX/C6u;

    invoke-direct {v1, p0}, LX/C6u;-><init>(LX/C6v;)V

    .line 1850584
    iget-object v2, p0, LX/C6v;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C6t;

    .line 1850585
    if-nez v2, :cond_0

    .line 1850586
    new-instance v2, LX/C6t;

    invoke-direct {v2, p0}, LX/C6t;-><init>(LX/C6v;)V

    .line 1850587
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C6t;->a$redex0(LX/C6t;LX/1De;IILX/C6u;)V

    .line 1850588
    move-object v1, v2

    .line 1850589
    move-object v0, v1

    .line 1850590
    return-object v0
.end method
