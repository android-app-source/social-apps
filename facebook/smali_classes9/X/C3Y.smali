.class public LX/C3Y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3Z;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3Y",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3Z;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845925
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845926
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3Y;->b:LX/0Zi;

    .line 1845927
    iput-object p1, p0, LX/C3Y;->a:LX/0Ot;

    .line 1845928
    return-void
.end method

.method public static a(LX/0QB;)LX/C3Y;
    .locals 4

    .prologue
    .line 1845888
    const-class v1, LX/C3Y;

    monitor-enter v1

    .line 1845889
    :try_start_0
    sget-object v0, LX/C3Y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845890
    sput-object v2, LX/C3Y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845891
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845892
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845893
    new-instance v3, LX/C3Y;

    const/16 p0, 0x1ec8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3Y;-><init>(LX/0Ot;)V

    .line 1845894
    move-object v0, v3

    .line 1845895
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845896
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845897
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845898
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845924
    const v0, -0x2de22655

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845923
    const v0, -0x2de22655

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1845917
    check-cast p2, LX/C3X;

    .line 1845918
    iget-object v0, p0, LX/C3Y;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3Z;

    iget-object v1, p2, LX/C3X;->a:LX/C33;

    iget-object v2, p2, LX/C3X;->b:LX/1Pb;

    iget v3, p2, LX/C3X;->c:I

    .line 1845919
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C3Z;->b:LX/C3I;

    invoke-virtual {v5, p1}, LX/C3I;->c(LX/1De;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/C3G;->a(LX/C33;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/C3G;->a(LX/1Pb;)LX/C3G;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x1

    .line 1845920
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/C3Z;->c:LX/C39;

    invoke-virtual {v6, p1}, LX/C39;->c(LX/1De;)LX/C37;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/C37;->a(LX/C33;)LX/C37;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/C37;->a(LX/1Pb;)LX/C37;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/C37;->h(I)LX/C37;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-static {p1}, LX/C3Y;->onClick(LX/1De;)LX/1dQ;

    move-result-object p0

    invoke-interface {v6, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v6, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/C3Z;->d:LX/C3p;

    invoke-virtual {v6, p1}, LX/C3p;->c(LX/1De;)LX/C3n;

    move-result-object v6

    iget-object p0, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v6, p0}, LX/C3n;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3n;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/C3n;->a(Z)LX/C3n;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 p0, 0x5

    const p2, 0x7f0b1d6d

    invoke-interface {v6, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    move-object v5, v5

    .line 1845921
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/C3Y;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1845922
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1845907
    invoke-static {}, LX/1dS;->b()V

    .line 1845908
    iget v0, p1, LX/1dQ;->b:I

    .line 1845909
    packed-switch v0, :pswitch_data_0

    .line 1845910
    :goto_0
    return-object v2

    .line 1845911
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1845912
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1845913
    check-cast v1, LX/C3X;

    .line 1845914
    iget-object p1, p0, LX/C3Y;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C3Z;

    iget-object p2, v1, LX/C3X;->a:LX/C33;

    .line 1845915
    iget-object p0, p1, LX/C3Z;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3B;

    invoke-virtual {p0, v0, p2}, LX/C3B;->onClick(Landroid/view/View;LX/C33;)V

    .line 1845916
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2de22655
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C3W;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3Y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845899
    new-instance v1, LX/C3X;

    invoke-direct {v1, p0}, LX/C3X;-><init>(LX/C3Y;)V

    .line 1845900
    iget-object v2, p0, LX/C3Y;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3W;

    .line 1845901
    if-nez v2, :cond_0

    .line 1845902
    new-instance v2, LX/C3W;

    invoke-direct {v2, p0}, LX/C3W;-><init>(LX/C3Y;)V

    .line 1845903
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3W;->a$redex0(LX/C3W;LX/1De;IILX/C3X;)V

    .line 1845904
    move-object v1, v2

    .line 1845905
    move-object v0, v1

    .line 1845906
    return-object v0
.end method
