.class public final LX/CCx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1858597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858598
    iput-object p1, p0, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1858599
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 2

    .prologue
    .line 1858595
    iget-object v0, p0, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 1858596
    instance-of v1, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1858591
    iget-object v0, p0, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 1858592
    instance-of v0, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1858593
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, p0

    .line 1858594
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
