.class public LX/Aui;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;ZI)V
    .locals 2

    .prologue
    .line 1723120
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1723121
    iput-object p1, p0, LX/Aui;->c:Landroid/view/View;

    .line 1723122
    iget-object v0, p0, LX/Aui;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    iput v0, p0, LX/Aui;->a:F

    .line 1723123
    if-eqz p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, LX/Aui;->b:F

    .line 1723124
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, LX/Aui;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1723125
    int-to-long v0, p3

    invoke-virtual {p0, v0, v1}, LX/Aui;->setDuration(J)V

    .line 1723126
    return-void

    .line 1723127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 1723117
    iget v0, p0, LX/Aui;->a:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, LX/Aui;->b:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 1723118
    iget-object v1, p0, LX/Aui;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1723119
    return-void
.end method
