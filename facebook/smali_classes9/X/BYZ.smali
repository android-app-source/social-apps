.class public LX/BYZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1796230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796231
    iput-object p1, p0, LX/BYZ;->b:LX/0Ot;

    .line 1796232
    iput-object p2, p0, LX/BYZ;->c:LX/0Ot;

    .line 1796233
    iput-object p3, p0, LX/BYZ;->d:LX/0Ot;

    .line 1796234
    iput-object p4, p0, LX/BYZ;->e:LX/0Ot;

    .line 1796235
    return-void
.end method

.method public static b(LX/0QB;)LX/BYZ;
    .locals 5

    .prologue
    .line 1796228
    new-instance v0, LX/BYZ;

    const/16 v1, 0xf9a

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x13f6

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x390f

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xbc

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/BYZ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1796229
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;LX/BYY;)Z
    .locals 9
    .param p1    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1796236
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1796237
    :goto_0
    return v0

    .line 1796238
    :cond_1
    iget-object v0, p0, LX/BYZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    sget-object v2, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BYZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    sget-object v2, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 1796239
    goto :goto_0

    .line 1796240
    :cond_3
    iget-object v0, p0, LX/BYZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1796241
    sget-object v2, LX/0df;->R:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1796242
    sget-object v3, LX/0df;->S:LX/0Tn;

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1796243
    sget-object v4, LX/0df;->T:LX/0Tn;

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1796244
    sget-object v5, LX/0df;->U:LX/0Tn;

    const-string v7, ""

    invoke-interface {v0, v5, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1796245
    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/CharSequence;

    aput-object v2, v7, v1

    aput-object v3, v7, v6

    const/4 v8, 0x2

    aput-object v4, v7, v8

    const/4 v8, 0x3

    aput-object v5, v7, v8

    invoke-static {v7}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1796246
    iget-object v0, p0, LX/BYZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7WS;

    invoke-virtual {v0}, LX/7WS;->a()V

    move v0, v1

    .line 1796247
    goto :goto_0

    .line 1796248
    :cond_4
    new-instance v7, LX/BYV;

    invoke-direct {v7, p1, v4, v5}, LX/BYV;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1796249
    sget-object v8, LX/0df;->M:LX/0Tn;

    invoke-interface {v0, v8, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1796250
    iput-boolean v0, p0, LX/BYZ;->a:Z

    .line 1796251
    iget-object v1, v7, LX/BYV;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1796252
    new-instance v1, LX/BYW;

    invoke-direct {v1, p0}, LX/BYW;-><init>(LX/BYZ;)V

    .line 1796253
    iget-object v8, v7, LX/BYV;->a:Landroid/widget/CheckBox;

    invoke-virtual {v8, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1796254
    new-instance v1, LX/6WI;

    invoke-direct {v1, p1}, LX/6WI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/6WI;->a(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/6WI;->b(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v1

    .line 1796255
    iget-object v8, v1, LX/6WI;->a:LX/31a;

    iput-object v7, v8, LX/31a;->y:Landroid/view/View;

    .line 1796256
    move-object v1, v1

    .line 1796257
    const v7, 0x7f081a03

    new-instance v8, LX/2r8;

    invoke-direct {v8, p0, p2}, LX/2r8;-><init>(LX/BYZ;LX/BYY;)V

    invoke-virtual {v1, v7, v8}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v1

    const v7, 0x7f081a04

    new-instance v8, LX/BYX;

    invoke-direct {v8, p0, p2, p1, v0}, LX/BYX;-><init>(LX/BYZ;LX/BYY;Landroid/app/Activity;Z)V

    invoke-virtual {v1, v7, v8}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    .line 1796258
    new-instance v1, Lcom/facebook/zero/offpeakdownload/OffpeakDownloadDialogHandler$4;

    invoke-direct {v1, p0, v0}, Lcom/facebook/zero/offpeakdownload/OffpeakDownloadDialogHandler$4;-><init>(LX/BYZ;LX/6WI;)V

    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1796259
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    .line 1796260
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "offpeak_download_dialog_impression"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "zero_module"

    .line 1796261
    iput-object v8, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796262
    move-object v8, v7

    .line 1796263
    const-string p0, "carrier_id"

    iget-object v7, v0, LX/BYZ;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {p1}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object p1

    const-string p2, ""

    invoke-interface {v7, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796264
    const-string v7, "location"

    invoke-virtual {v8, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796265
    const-string v7, "dialog_title"

    invoke-virtual {v8, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796266
    const-string v7, "dialog_desc"

    invoke-virtual {v8, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796267
    const-string v7, "checkbox_title"

    invoke-virtual {v8, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796268
    const-string v7, "checkbox_desc"

    invoke-virtual {v8, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796269
    iget-object v7, v0, LX/BYZ;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1796270
    move v0, v6

    .line 1796271
    goto/16 :goto_0
.end method
