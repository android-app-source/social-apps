.class public LX/Bcw;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        "TResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bcz;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bcz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1803074
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1803075
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bcw;->b:LX/0Zi;

    .line 1803076
    iput-object p1, p0, LX/Bcw;->a:LX/0Ot;

    .line 1803077
    return-void
.end method

.method public static a(LX/0QB;)LX/Bcw;
    .locals 4

    .prologue
    .line 1803063
    const-class v1, LX/Bcw;

    monitor-enter v1

    .line 1803064
    :try_start_0
    sget-object v0, LX/Bcw;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1803065
    sput-object v2, LX/Bcw;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1803066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1803067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1803068
    new-instance v3, LX/Bcw;

    const/16 p0, 0x1944

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bcw;-><init>(LX/0Ot;)V

    .line 1803069
    move-object v0, v3

    .line 1803070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1803071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bcw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1803072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1803073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private f(LX/BcP;LX/BcO;)LX/Bcp;
    .locals 12

    .prologue
    .line 1803051
    check-cast p2, LX/Bcu;

    .line 1803052
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bcz;

    iget-object v1, p2, LX/Bcu;->d:LX/1rs;

    iget-object v2, p2, LX/Bcu;->e:Ljava/lang/String;

    iget v3, p2, LX/Bcu;->b:I

    iget-wide v4, p2, LX/Bcu;->f:J

    iget v6, p2, LX/Bcu;->g:I

    .line 1803053
    new-instance v7, LX/Bcp;

    iget-object v8, v0, LX/Bcz;->a:LX/1rq;

    invoke-virtual {v8, v2, v1}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v8

    .line 1803054
    iput-wide v4, v8, LX/2jj;->j:J

    .line 1803055
    move-object v8, v8

    .line 1803056
    const/4 v9, 0x1

    .line 1803057
    iput-boolean v9, v8, LX/2jj;->k:Z

    .line 1803058
    move-object v8, v8

    .line 1803059
    iput v6, v8, LX/2jj;->o:I

    .line 1803060
    move-object v8, v8

    .line 1803061
    invoke-virtual {v8}, LX/2jj;->a()LX/2kW;

    move-result-object v8

    invoke-direct {v7, v8, v3, v3}, LX/Bcp;-><init>(LX/2kW;II)V

    move-object v0, v7

    .line 1803062
    return-object v0
.end method


# virtual methods
.method public final a(LX/BcO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1803050
    check-cast p1, LX/Bcu;

    iget-object v0, p1, LX/Bcu;->j:LX/Bcp;

    return-object v0
.end method

.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1803048
    invoke-static {}, LX/1dS;->b()V

    .line 1803049
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803043
    check-cast p1, LX/Bcu;

    .line 1803044
    check-cast p2, LX/Bcu;

    .line 1803045
    iget-object v0, p1, LX/Bcu;->c:Landroid/os/Handler;

    iput-object v0, p2, LX/Bcu;->c:Landroid/os/Handler;

    .line 1803046
    iget-object v0, p1, LX/Bcu;->h:LX/Bcm;

    iput-object v0, p2, LX/Bcu;->h:LX/Bcm;

    .line 1803047
    return-void
.end method

.method public final a(LX/BcP;IIILX/BcO;)V
    .locals 2

    .prologue
    .line 1803038
    check-cast p5, LX/Bcu;

    .line 1803039
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p5, LX/Bcu;->j:LX/Bcp;

    iget v1, p5, LX/Bcu;->i:I

    .line 1803040
    sub-int p0, p4, v1

    if-le p3, p0, :cond_0

    .line 1803041
    invoke-virtual {v0}, LX/Bcp;->b()V

    .line 1803042
    :cond_0
    return-void
.end method

.method public final a(LX/BcP;LX/BcJ;LX/BcO;LX/BcO;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1802952
    check-cast p3, LX/Bcu;

    .line 1802953
    check-cast p4, LX/Bcu;

    .line 1802954
    if-nez p3, :cond_1

    move-object v1, v0

    :goto_0
    if-nez p4, :cond_2

    :goto_1
    invoke-static {v1, v0}, LX/BcS;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 1802955
    iget-object v1, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1802956
    invoke-virtual {p1}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    if-nez v1, :cond_9

    .line 1802957
    const/4 v1, 0x0

    .line 1802958
    :goto_2
    move-object p0, v1

    .line 1802959
    iget-object v1, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 1802960
    check-cast v1, LX/Bcm;

    .line 1802961
    iget-object v2, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 1802962
    check-cast v2, LX/Bcm;

    .line 1802963
    if-nez v1, :cond_3

    move v3, v4

    .line 1802964
    :goto_3
    iget v5, v2, LX/Bcm;->b:I

    if-ge v5, v3, :cond_4

    .line 1802965
    :cond_0
    return-void

    .line 1802966
    :cond_1
    iget-object v1, p3, LX/Bcu;->h:LX/Bcm;

    goto :goto_0

    :cond_2
    iget-object v0, p4, LX/Bcu;->h:LX/Bcm;

    goto :goto_1

    .line 1802967
    :cond_3
    iget v3, v1, LX/Bcm;->b:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1802968
    :cond_4
    if-nez v1, :cond_7

    move v1, v4

    :goto_4
    move v3, v1

    .line 1802969
    :goto_5
    iget-object v1, v2, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 1802970
    iget-object v1, v2, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/Bcl;

    move v5, v4

    .line 1802971
    :goto_6
    array-length v6, v1

    if-ge v5, v6, :cond_8

    .line 1802972
    aget-object v6, v1, v5

    if-eqz v6, :cond_6

    .line 1802973
    const/4 v6, 0x0

    .line 1802974
    aget-object p3, v1, v5

    iget-object p3, p3, LX/Bcl;->a:LX/3Ca;

    sget-object p4, LX/3Ca;->DELETE:LX/3Ca;

    if-eq p3, p4, :cond_5

    .line 1802975
    aget-object v6, v1, v5

    iget v6, v6, LX/Bcl;->b:I

    aget-object p3, v1, v5

    iget-object p3, p3, LX/Bcl;->c:Ljava/lang/Object;

    .line 1802976
    new-instance p4, LX/BdG;

    invoke-direct {p4}, LX/BdG;-><init>()V

    .line 1802977
    iput v6, p4, LX/BdG;->a:I

    .line 1802978
    iput-object p3, p4, LX/BdG;->b:Ljava/lang/Object;

    .line 1802979
    iget-object p1, p0, LX/BcQ;->a:LX/BcO;

    .line 1802980
    iget-object v6, p1, LX/BcO;->i:LX/BcS;

    move-object p1, v6

    .line 1802981
    invoke-virtual {p1, p0, p4}, LX/BcS;->a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, LX/1X1;

    move-object v6, p4

    .line 1802982
    :cond_5
    sget-object p3, LX/Bcy;->a:[I

    aget-object p4, v1, v5

    iget-object p4, p4, LX/Bcl;->a:LX/3Ca;

    invoke-virtual {p4}, LX/3Ca;->ordinal()I

    move-result p4

    aget p3, p3, p4

    packed-switch p3, :pswitch_data_0

    .line 1802983
    :cond_6
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1802984
    :cond_7
    iget-object v1, v1, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto :goto_4

    .line 1802985
    :pswitch_0
    aget-object p3, v1, v5

    iget p3, p3, LX/Bcl;->b:I

    invoke-virtual {p2, p3, v6}, LX/BcJ;->a(ILX/1X1;)V

    goto :goto_7

    .line 1802986
    :pswitch_1
    aget-object v6, v1, v5

    iget v6, v6, LX/Bcl;->b:I

    invoke-virtual {p2, v6}, LX/BcJ;->b(I)V

    goto :goto_7

    .line 1802987
    :pswitch_2
    aget-object p3, v1, v5

    iget p3, p3, LX/Bcl;->b:I

    invoke-virtual {p2, p3, v6}, LX/BcJ;->b(ILX/1X1;)V

    goto :goto_7

    .line 1802988
    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    check-cast v1, LX/Bcu;

    iget-object v1, v1, LX/Bcu;->k:LX/BcQ;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1803030
    check-cast p2, LX/Bcu;

    .line 1803031
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bcz;

    iget-object v1, p2, LX/Bcu;->j:LX/Bcp;

    iget-object v2, p2, LX/Bcu;->c:Landroid/os/Handler;

    .line 1803032
    new-instance p0, LX/Bcx;

    invoke-direct {p0, v0, p1}, LX/Bcx;-><init>(LX/Bcz;LX/BcP;)V

    .line 1803033
    invoke-virtual {v1, p0}, LX/Bcp;->a(LX/Bcn;)V

    .line 1803034
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    if-ne p0, p2, :cond_0

    .line 1803035
    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/Bcp;->a(Ljava/lang/Object;)V

    .line 1803036
    :goto_0
    return-void

    .line 1803037
    :cond_0
    new-instance p0, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionConfigurationChangeSetSpec$2;

    invoke-direct {p0, v0, v1}, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionConfigurationChangeSetSpec$2;-><init>(LX/Bcz;LX/Bcp;)V

    const p2, -0x470dbe1

    invoke-static {v2, p0, p2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(LX/BcP;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1803027
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/Bcp;

    .line 1803028
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/Bcp;->a(Z)V

    .line 1803029
    return-void
.end method

.method public final b(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803023
    check-cast p1, LX/Bcu;

    .line 1803024
    check-cast p2, LX/Bcu;

    .line 1803025
    iget-object v0, p1, LX/Bcu;->j:LX/Bcp;

    iput-object v0, p2, LX/Bcu;->j:LX/Bcp;

    .line 1803026
    return-void
.end method

.method public final b(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803019
    check-cast p2, LX/Bcu;

    .line 1803020
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Bcu;->j:LX/Bcp;

    .line 1803021
    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/Bcp;->a(LX/Bcn;)V

    .line 1803022
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1803018
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/BcP;)LX/Bct;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            ")",
            "LX/Bcw",
            "<TTEdge;TTUserInfo;TTResponseModel;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1803009
    new-instance v0, LX/Bcu;

    invoke-direct {v0, p0}, LX/Bcu;-><init>(LX/Bcw;)V

    .line 1803010
    iget-object v1, p0, LX/Bcw;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bct;

    .line 1803011
    if-nez v1, :cond_0

    .line 1803012
    new-instance v1, LX/Bct;

    invoke-direct {v1, p0}, LX/Bct;-><init>(LX/Bcw;)V

    .line 1803013
    :cond_0
    iput-object v0, v1, LX/BcN;->a:LX/BcO;

    .line 1803014
    iput-object v0, v1, LX/Bct;->a:LX/Bcu;

    .line 1803015
    iget-object p0, v1, LX/Bct;->e:Ljava/util/BitSet;

    invoke-virtual {p0}, Ljava/util/BitSet;->clear()V

    .line 1803016
    move-object v0, v1

    .line 1803017
    return-object v0
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1802996
    check-cast p2, LX/Bcu;

    .line 1802997
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1802998
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v1

    .line 1802999
    iget-object v2, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1803000
    sget-object v2, LX/Bcm;->a:LX/Bcm;

    .line 1803001
    iput-object v2, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1803002
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-direct {v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1803003
    iput-object v2, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1803004
    iget-object v2, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v2

    .line 1803005
    check-cast v0, LX/Bcm;

    iput-object v0, p2, LX/Bcu;->h:LX/Bcm;

    .line 1803006
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1803007
    check-cast v0, Landroid/os/Handler;

    iput-object v0, p2, LX/Bcu;->c:Landroid/os/Handler;

    .line 1803008
    return-void
.end method

.method public final d(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 1802993
    move-object v0, p2

    check-cast v0, LX/Bcu;

    .line 1802994
    invoke-direct {p0, p1, p2}, LX/Bcw;->f(LX/BcP;LX/BcO;)LX/Bcp;

    move-result-object v1

    iput-object v1, v0, LX/Bcu;->j:LX/Bcp;

    .line 1802995
    return-void
.end method

.method public final e(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1802989
    check-cast p2, LX/Bcu;

    .line 1802990
    iget-object v0, p0, LX/Bcw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Bcu;->j:LX/Bcp;

    .line 1802991
    invoke-virtual {v0}, LX/Bcp;->a()V

    .line 1802992
    return-void
.end method
