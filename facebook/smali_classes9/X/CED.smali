.class public final LX/CED;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CED;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CEB;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CEE;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1861013
    const/4 v0, 0x0

    sput-object v0, LX/CED;->a:LX/CED;

    .line 1861014
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CED;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1861010
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1861011
    new-instance v0, LX/CEE;

    invoke-direct {v0}, LX/CEE;-><init>()V

    iput-object v0, p0, LX/CED;->c:LX/CEE;

    .line 1861012
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1861015
    const v0, 0x3cf2858a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/CED;
    .locals 2

    .prologue
    .line 1861006
    const-class v1, LX/CED;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CED;->a:LX/CED;

    if-nez v0, :cond_0

    .line 1861007
    new-instance v0, LX/CED;

    invoke-direct {v0}, LX/CED;-><init>()V

    sput-object v0, LX/CED;->a:LX/CED;

    .line 1861008
    :cond_0
    sget-object v0, LX/CED;->a:LX/CED;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1861009
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1861000
    check-cast p2, LX/CEC;

    .line 1861001
    iget-object v0, p2, LX/CEC;->a:LX/1X1;

    .line 1861002
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    .line 1861003
    const p0, 0x3cf2858a

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1861004
    invoke-interface {v1, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1861005
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1860990
    invoke-static {}, LX/1dS;->b()V

    .line 1860991
    iget v0, p1, LX/1dQ;->b:I

    .line 1860992
    packed-switch v0, :pswitch_data_0

    .line 1860993
    :goto_0
    return-object v2

    .line 1860994
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1860995
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1860996
    check-cast v1, LX/CEC;

    .line 1860997
    iget-object p0, v1, LX/CEC;->b:Landroid/view/View$OnClickListener;

    .line 1860998
    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1860999
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3cf2858a
        :pswitch_0
    .end packed-switch
.end method
