.class public LX/BOb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1779902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779903
    return-void
.end method

.method public static a(LX/0QB;)LX/BOb;
    .locals 1

    .prologue
    .line 1779904
    invoke-static {p0}, LX/BOb;->b(LX/0QB;)LX/BOb;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1779905
    sget-object v0, LX/0ax;->gM:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BOb;
    .locals 2

    .prologue
    .line 1779906
    new-instance v1, LX/BOb;

    invoke-direct {v1}, LX/BOb;-><init>()V

    .line 1779907
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 1779908
    iput-object v0, v1, LX/BOb;->a:LX/0Uh;

    .line 1779909
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1779910
    iget-object v0, p0, LX/BOb;->a:LX/0Uh;

    const/16 v1, 0x8c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 1779911
    if-eqz v0, :cond_2

    .line 1779912
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1779913
    sget-object v0, LX/0ax;->gQ:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1779914
    :goto_0
    return-object v0

    .line 1779915
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1779916
    sget-object v0, LX/0ax;->gR:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1779917
    :cond_1
    sget-object v0, LX/0ax;->gS:Ljava/lang/String;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1779918
    :cond_2
    sget-object v0, LX/0ax;->gO:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
