.class public final LX/BmS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bm9;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V
    .locals 0

    .prologue
    .line 1818385
    iput-object p1, p0, LX/BmS;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 1818382
    iget-object v0, p0, LX/BmS;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818383
    iget-object v0, p0, LX/BmS;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, LX/BmS;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, LX/BmS;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818384
    return-void
.end method
