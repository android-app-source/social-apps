.class public final LX/C48;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/39e;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C49;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39e",
            "<TE;>.ReactionsFooterComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/39e;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/39e;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1846891
    iput-object p1, p0, LX/C48;->b:LX/39e;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1846892
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "shouldOpenFlyout"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ufiWidthPx"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C48;->c:[Ljava/lang/String;

    .line 1846893
    iput v3, p0, LX/C48;->d:I

    .line 1846894
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C48;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C48;LX/1De;IILX/C49;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/39e",
            "<TE;>.ReactionsFooterComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1846895
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1846896
    iput-object p4, p0, LX/C48;->a:LX/C49;

    .line 1846897
    iget-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1846898
    return-void
.end method


# virtual methods
.method public final a(LX/1Po;)LX/C48;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846885
    iget-object v0, p0, LX/C48;->a:LX/C49;

    iput-object p1, v0, LX/C49;->b:LX/1Po;

    .line 1846886
    iget-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1846887
    return-object p0
.end method

.method public final a(LX/1zt;)LX/C48;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zt;",
            ")",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846880
    iget-object v0, p0, LX/C48;->a:LX/C49;

    iput-object p1, v0, LX/C49;->h:LX/1zt;

    .line 1846881
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C48;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846888
    iget-object v0, p0, LX/C48;->a:LX/C49;

    iput-object p1, v0, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846889
    iget-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1846890
    return-object p0
.end method

.method public final a(Z)LX/C48;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846882
    iget-object v0, p0, LX/C48;->a:LX/C49;

    iput-boolean p1, v0, LX/C49;->c:Z

    .line 1846883
    iget-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1846884
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1846876
    invoke-super {p0}, LX/1X5;->a()V

    .line 1846877
    const/4 v0, 0x0

    iput-object v0, p0, LX/C48;->a:LX/C49;

    .line 1846878
    iget-object v0, p0, LX/C48;->b:LX/39e;

    iget-object v0, v0, LX/39e;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1846879
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/39e;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1846866
    iget-object v1, p0, LX/C48;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C48;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C48;->d:I

    if-ge v1, v2, :cond_2

    .line 1846867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1846868
    :goto_0
    iget v2, p0, LX/C48;->d:I

    if-ge v0, v2, :cond_1

    .line 1846869
    iget-object v2, p0, LX/C48;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1846870
    iget-object v2, p0, LX/C48;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1846871
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1846872
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1846873
    :cond_2
    iget-object v0, p0, LX/C48;->a:LX/C49;

    .line 1846874
    invoke-virtual {p0}, LX/C48;->a()V

    .line 1846875
    return-object v0
.end method

.method public final h(I)LX/C48;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/39e",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846863
    iget-object v0, p0, LX/C48;->a:LX/C49;

    iput p1, v0, LX/C49;->d:I

    .line 1846864
    iget-object v0, p0, LX/C48;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1846865
    return-object p0
.end method
