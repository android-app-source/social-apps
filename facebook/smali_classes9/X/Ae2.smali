.class public LX/Ae2;
.super LX/AVi;
.source ""

# interfaces
.implements LX/Ae1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;",
        ">;",
        "LX/Ae1;",
        "Lcom/facebook/facecastdisplay/tipjar/LiveVideoTipJarSettingHelper$LiveTipJarVideoSettingFetchedListener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;


# instance fields
.field public final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ae6;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation
.end field

.field public final C:LX/14w;

.field private final D:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Z

.field private I:Z

.field private J:Z

.field public K:F

.field public L:I

.field private M:Z

.field public N:Z

.field public O:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:LX/3Gt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/Adv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:LX/3Af;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:LX/0hs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/Adk;

.field public final g:LX/AgU;

.field private final h:LX/Ac6;

.field public final i:LX/1b4;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2sP;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0kL;

.field private final p:LX/AeB;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ah9;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/01T;

.field private final s:LX/0ad;

.field public final t:LX/03V;

.field private final u:LX/AVW;

.field private final v:Z

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:Landroid/view/View$OnClickListener;

.field private final y:LX/Adj;

.field private final z:LX/Adu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1695864
    const-class v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ae2;->a:Ljava/lang/String;

    .line 1695865
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "live_feedback_input_is_reactions_tap_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ae2;->b:LX/0Tn;

    .line 1695866
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "live_feedback_input_is_reactions_swipe_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ae2;->c:LX/0Tn;

    .line 1695867
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "live_feedback_input_is_share_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ae2;->d:LX/0Tn;

    .line 1695868
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "live_feedback_input_is_tip_jar_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ae2;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/Adk;LX/AgU;LX/Ac6;LX/1b4;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;Ljava/lang/String;LX/0Ot;LX/0Ot;LX/0kL;LX/AeB;LX/0Ot;LX/0Ot;LX/01T;LX/0ad;LX/03V;LX/AVW;Ljava/lang/Boolean;LX/0Ot;LX/14w;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p18    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Adk;",
            "LX/AgU;",
            "LX/Ac6;",
            "LX/1b4;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/os/Handler;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/2sP;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0kL;",
            "LX/AeB;",
            "LX/0Ot",
            "<",
            "LX/Ah9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ae6;",
            ">;",
            "LX/01T;",
            "LX/0ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/AVW;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/14w;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1695870
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1695871
    iput-object p1, p0, LX/Ae2;->f:LX/Adk;

    .line 1695872
    iput-object p2, p0, LX/Ae2;->g:LX/AgU;

    .line 1695873
    iput-object p3, p0, LX/Ae2;->h:LX/Ac6;

    .line 1695874
    iput-object p4, p0, LX/Ae2;->i:LX/1b4;

    .line 1695875
    iput-object p5, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1695876
    iput-object p6, p0, LX/Ae2;->k:Landroid/os/Handler;

    .line 1695877
    iput-object p7, p0, LX/Ae2;->l:Ljava/lang/String;

    .line 1695878
    iput-object p8, p0, LX/Ae2;->m:LX/0Ot;

    .line 1695879
    iput-object p9, p0, LX/Ae2;->n:LX/0Ot;

    .line 1695880
    iput-object p10, p0, LX/Ae2;->o:LX/0kL;

    .line 1695881
    iput-object p11, p0, LX/Ae2;->p:LX/AeB;

    .line 1695882
    iput-object p12, p0, LX/Ae2;->q:LX/0Ot;

    .line 1695883
    iput-object p13, p0, LX/Ae2;->A:LX/0Ot;

    .line 1695884
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Ae2;->r:LX/01T;

    .line 1695885
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Ae2;->s:LX/0ad;

    .line 1695886
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Ae2;->t:LX/03V;

    .line 1695887
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Ae2;->u:LX/AVW;

    .line 1695888
    invoke-virtual/range {p18 .. p18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/Ae2;->v:Z

    .line 1695889
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Ae2;->B:LX/0Ot;

    .line 1695890
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Ae2;->C:LX/14w;

    .line 1695891
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Ae2;->D:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1695892
    new-instance v1, LX/Adr;

    invoke-direct {v1, p0}, LX/Adr;-><init>(LX/Ae2;)V

    iput-object v1, p0, LX/Ae2;->w:Landroid/view/View$OnClickListener;

    .line 1695893
    new-instance v1, LX/Ads;

    invoke-direct {v1, p0}, LX/Ads;-><init>(LX/Ae2;)V

    iput-object v1, p0, LX/Ae2;->x:Landroid/view/View$OnClickListener;

    .line 1695894
    new-instance v1, LX/Adt;

    invoke-direct {v1, p0}, LX/Adt;-><init>(LX/Ae2;)V

    iput-object v1, p0, LX/Ae2;->y:LX/Adj;

    .line 1695895
    new-instance v1, LX/Adu;

    invoke-direct {v1, p0}, LX/Adu;-><init>(LX/Ae2;)V

    iput-object v1, p0, LX/Ae2;->z:LX/Adu;

    .line 1695896
    return-void
.end method

.method public static E(LX/Ae2;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1695897
    iget-boolean v0, p0, LX/Ae2;->I:Z

    if-nez v0, :cond_0

    .line 1695898
    iget-object v0, p0, LX/Ae2;->k:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695899
    iget-object v0, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Ae2;->b:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1695900
    iput-boolean v2, p0, LX/Ae2;->I:Z

    .line 1695901
    :cond_0
    iget-boolean v0, p0, LX/Ae2;->J:Z

    if-nez v0, :cond_1

    .line 1695902
    iget-object v0, p0, LX/Ae2;->k:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695903
    invoke-direct {p0}, LX/Ae2;->x()V

    .line 1695904
    :cond_1
    return-void
.end method

.method public static F(LX/Ae2;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1695905
    iget-object v0, p0, LX/Ae2;->k:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695906
    iget-boolean v0, p0, LX/Ae2;->J:Z

    if-nez v0, :cond_0

    .line 1695907
    iget-object v0, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Ae2;->c:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1695908
    iput-boolean v2, p0, LX/Ae2;->J:Z

    .line 1695909
    invoke-static {p0}, LX/Ae2;->E(LX/Ae2;)V

    .line 1695910
    :cond_0
    return-void
.end method

.method public static a(LX/Ae2;LX/7TY;)V
    .locals 4

    .prologue
    .line 1695802
    iget-boolean v0, p0, LX/Ae2;->E:Z

    if-eqz v0, :cond_0

    .line 1695803
    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x41e065f

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1695804
    if-eqz v0, :cond_1

    .line 1695805
    const v0, 0x7f080c21

    .line 1695806
    :goto_1
    invoke-virtual {p1, v0}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    .line 1695807
    const v0, 0x7f020886

    invoke-virtual {v1, v0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v2, LX/Adw;

    invoke-direct {v2, p0}, LX/Adw;-><init>(LX/Ae2;)V

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1695808
    iget-object v0, p0, LX/Ae2;->i:LX/1b4;

    .line 1695809
    iget-object v2, v0, LX/1b4;->a:LX/0ad;

    sget-short v3, LX/1v6;->z:S

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1695810
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Ae2;->v:Z

    if-nez v0, :cond_0

    .line 1695811
    iget-boolean v0, p0, LX/Ae2;->N:Z

    if-eqz v0, :cond_3

    const v0, 0x7f080c1f

    :goto_2
    invoke-virtual {v1, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 1695812
    :cond_0
    return-void

    .line 1695813
    :cond_1
    iget-boolean v0, p0, LX/Ae2;->v:Z

    if-eqz v0, :cond_2

    const v0, 0x7f080c1d

    goto :goto_1

    :cond_2
    const v0, 0x7f080c1c

    goto :goto_1

    .line 1695814
    :cond_3
    const v0, 0x7f080c1e

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Ae2;II)V
    .locals 1

    .prologue
    .line 1695911
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695912
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/HorizontalScrollView;

    if-eqz v0, :cond_0

    .line 1695913
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695914
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    .line 1695915
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/Ae2;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1695916
    iput-object p1, p0, LX/Ae2;->X:Ljava/lang/String;

    .line 1695917
    iget-object v0, p0, LX/Ae2;->u:LX/AVW;

    iget-object v1, p0, LX/Ae2;->W:Ljava/lang/String;

    .line 1695918
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "facecast_broadcaster_update"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "facecast"

    .line 1695919
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1695920
    move-object v2, v2

    .line 1695921
    const-string v3, "facecast_event_name"

    const-string p0, "facecast_share_action"

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "facecast_event_extra"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "video_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1695922
    iget-object v3, v0, LX/AVW;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1695923
    return-void
.end method

.method public static b(LX/0QB;)LX/Ae2;
    .locals 24

    .prologue
    .line 1695924
    new-instance v2, LX/Ae2;

    invoke-static/range {p0 .. p0}, LX/Adk;->a(LX/0QB;)LX/Adk;

    move-result-object v3

    check-cast v3, LX/Adk;

    invoke-static/range {p0 .. p0}, LX/AgU;->a(LX/0QB;)LX/AgU;

    move-result-object v4

    check-cast v4, LX/AgU;

    invoke-static/range {p0 .. p0}, LX/Ac6;->a(LX/0QB;)LX/Ac6;

    move-result-object v5

    check-cast v5, LX/Ac6;

    invoke-static/range {p0 .. p0}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v6

    check-cast v6, LX/1b4;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const/16 v10, 0xfc4

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x19c6

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/AeB;->a(LX/0QB;)LX/AeB;

    move-result-object v13

    check-cast v13, LX/AeB;

    const/16 v14, 0x1c23

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x1c00

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v16

    check-cast v16, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {p0 .. p0}, LX/AVW;->a(LX/0QB;)LX/AVW;

    move-result-object v19

    check-cast v19, LX/AVW;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    const/16 v21, 0x2978

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v22

    check-cast v22, LX/14w;

    invoke-static/range {p0 .. p0}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v23

    check-cast v23, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct/range {v2 .. v23}, LX/Ae2;-><init>(LX/Adk;LX/AgU;LX/Ac6;LX/1b4;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;Ljava/lang/String;LX/0Ot;LX/0Ot;LX/0kL;LX/AeB;LX/0Ot;LX/0Ot;LX/01T;LX/0ad;LX/03V;LX/AVW;Ljava/lang/Boolean;LX/0Ot;LX/14w;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1695925
    return-object v2
.end method

.method private b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V
    .locals 2

    .prologue
    .line 1695926
    iput-object p0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    .line 1695927
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Ae2;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695928
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/Ae2;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695929
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b04ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Ae2;->L:I

    .line 1695930
    return-void
.end method

.method private static c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1695931
    iput-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    .line 1695932
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695933
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695934
    return-void
.end method

.method public static p(LX/Ae2;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1695935
    iget-object v1, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x403827a

    if-ne v1, v2, :cond_1

    .line 1695936
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Ae2;->r:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static q(LX/Ae2;)Z
    .locals 2

    .prologue
    .line 1695869
    iget-object v0, p0, LX/Ae2;->r:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ae2;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2sP;

    invoke-virtual {v0}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(LX/Ae2;)Z
    .locals 2

    .prologue
    .line 1695863
    iget-object v0, p0, LX/Ae2;->r:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Ae2;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(LX/Ae2;)Z
    .locals 1

    .prologue
    .line 1695862
    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v$redex0(LX/Ae2;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1695838
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695839
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1695840
    :cond_0
    :goto_0
    return-void

    .line 1695841
    :cond_1
    iget-object v0, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Ae2;->b:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Ae2;->I:Z

    .line 1695842
    iget-object v0, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Ae2;->c:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Ae2;->J:Z

    .line 1695843
    iget-object v0, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Ae2;->d:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1695844
    iget-object v1, p0, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Ae2;->e:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 1695845
    iget-boolean v2, p0, LX/Ae2;->I:Z

    if-nez v2, :cond_2

    .line 1695846
    iget-object v4, p0, LX/Ae2;->k:Landroid/os/Handler;

    new-instance v5, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$12;

    invoke-direct {v5, p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$12;-><init>(LX/Ae2;)V

    const-wide/16 v6, 0x1388

    const v8, 0x5373a704

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1695847
    goto :goto_0

    .line 1695848
    :cond_2
    iget-boolean v2, p0, LX/Ae2;->J:Z

    if-nez v2, :cond_3

    .line 1695849
    invoke-direct {p0}, LX/Ae2;->x()V

    goto :goto_0

    .line 1695850
    :cond_3
    iget-object v2, p0, LX/Ae2;->i:LX/1b4;

    .line 1695851
    iget-object v3, v2, LX/1b4;->b:LX/0Uh;

    const/16 v4, 0x35b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 1695852
    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    .line 1695853
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695854
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 1695855
    iget-object v4, p0, LX/Ae2;->k:Landroid/os/Handler;

    new-instance v5, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$14;

    invoke-direct {v5, p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$14;-><init>(LX/Ae2;)V

    const-wide/16 v6, 0x1388

    const v8, -0x3ab2408b

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1695856
    goto :goto_0

    .line 1695857
    :cond_4
    if-nez v1, :cond_0

    .line 1695858
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695859
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1695860
    iget-object v4, p0, LX/Ae2;->k:Landroid/os/Handler;

    new-instance v5, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$15;

    invoke-direct {v5, p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$15;-><init>(LX/Ae2;)V

    const-wide/16 v6, 0x1388

    const v8, -0x430d8101

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1695861
    goto/16 :goto_0
.end method

.method private x()V
    .locals 5

    .prologue
    .line 1695836
    iget-object v0, p0, LX/Ae2;->k:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$13;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputContainerController$13;-><init>(LX/Ae2;)V

    const-wide/16 v2, 0x2710

    const v4, -0x2bd09f7

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1695837
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1695824
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    check-cast p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    .line 1695825
    invoke-static {p2}, LX/Ae2;->c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V

    .line 1695826
    invoke-direct {p0, p1}, LX/Ae2;->b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V

    .line 1695827
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->setEnabled(Z)V

    .line 1695828
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1695829
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/Ae2;->M:Z

    if-eqz v0, :cond_0

    .line 1695830
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1695831
    :cond_0
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->setVisibility(I)V

    .line 1695832
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setVisibility(I)V

    .line 1695833
    iget-object v0, p0, LX/Ae2;->f:LX/Adk;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695834
    iget-object v0, p0, LX/Ae2;->g:LX/AgU;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695835
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1695815
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    .line 1695816
    invoke-direct {p0, p1}, LX/Ae2;->b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V

    .line 1695817
    iget-object v0, p0, LX/Ae2;->f:LX/Adk;

    iget-object v1, p0, LX/Ae2;->y:LX/Adj;

    .line 1695818
    iput-object v1, v0, LX/Adk;->g:LX/Adj;

    .line 1695819
    iget-object v0, p0, LX/Ae2;->g:LX/AgU;

    iget-object v1, p0, LX/Ae2;->z:LX/Adu;

    .line 1695820
    iput-object v1, v0, LX/AgU;->h:LX/Adu;

    .line 1695821
    iget-object v0, p0, LX/Ae2;->f:LX/Adk;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695822
    iget-object v0, p0, LX/Ae2;->g:LX/AgU;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695823
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1695787
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695788
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-static {v0}, LX/Ae2;->c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;)V

    .line 1695789
    iget-object v0, p0, LX/Ae2;->f:LX/Adk;

    .line 1695790
    iput-object v1, v0, LX/Adk;->g:LX/Adj;

    .line 1695791
    iget-object v0, p0, LX/Ae2;->g:LX/AgU;

    .line 1695792
    iput-object v1, v0, LX/AgU;->h:LX/Adu;

    .line 1695793
    iget-object v0, p0, LX/Ae2;->V:LX/0hs;

    if-eqz v0, :cond_0

    .line 1695794
    iget-object v0, p0, LX/Ae2;->V:LX/0hs;

    .line 1695795
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 1695796
    :cond_0
    iget-object v0, p0, LX/Ae2;->f:LX/Adk;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1695797
    iget-object v0, p0, LX/Ae2;->g:LX/AgU;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1695798
    iget-object v0, p0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-eqz v0, :cond_1

    .line 1695799
    iget-object v0, p0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1695800
    iput-object v1, v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->p:LX/Adv;

    .line 1695801
    :cond_1
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1695771
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695772
    if-nez v0, :cond_1

    .line 1695773
    :cond_0
    :goto_0
    return-void

    .line 1695774
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/Ae2;->i:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Ae2;->D:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1695775
    iget-boolean p1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, p1

    .line 1695776
    if-eqz v0, :cond_3

    .line 1695777
    :cond_2
    iput-boolean v1, p0, LX/Ae2;->M:Z

    .line 1695778
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695779
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 1695780
    :cond_3
    iput-boolean v2, p0, LX/Ae2;->M:Z

    .line 1695781
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695782
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 1695783
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695784
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1695785
    iget-object v0, p0, LX/Ae2;->k:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695786
    invoke-static {p0}, LX/Ae2;->v$redex0(LX/Ae2;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1695770
    iget-boolean v0, p0, LX/Ae2;->F:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1695769
    iget-boolean v0, p0, LX/Ae2;->H:Z

    return v0
.end method
