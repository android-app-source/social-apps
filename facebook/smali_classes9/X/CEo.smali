.class public final LX/CEo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;",
        ">;",
        "LX/1c9",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CEp;


# direct methods
.method public constructor <init>(LX/CEp;)V
    .locals 0

    .prologue
    .line 1861917
    iput-object p1, p0, LX/CEo;->a:LX/CEp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1861918
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1861919
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v1

    .line 1861920
    if-eqz p1, :cond_0

    .line 1861921
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1861922
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1861923
    :goto_0
    return-object v0

    .line 1861924
    :cond_1
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v1, v0}, LX/1c9;->add(Ljava/lang/Object;)Z

    .line 1861925
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1861926
    check-cast v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;

    .line 1861927
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v3

    .line 1861928
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_8

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;

    .line 1861929
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v7

    .line 1861930
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "#"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1861931
    new-instance v9, Ljava/lang/StringBuilder;

    const-string p1, "#"

    invoke-direct {v9, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1861932
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {p1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1861933
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-static {v8}, LX/CEp;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1861934
    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861935
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->a()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {v9}, LX/CEp;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1861936
    invoke-virtual {v7, v9}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861937
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->o()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1861938
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/5RS;->getValue(Ljava/lang/String;)LX/5RS;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setTextAlign(LX/5RS;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861939
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 1861940
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/5RY;->getValue(Ljava/lang/String;)LX/5RY;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setFontWeight(LX/5RY;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861941
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-static {p1}, LX/CEp;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1861942
    invoke-virtual {v7, p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientEndColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861943
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 1861944
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientDirection(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861945
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setPresetId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861946
    invoke-virtual {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/1c9;->contains(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setCanDefault(Ljava/lang/Boolean;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 1861947
    invoke-virtual {v7}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v7

    move-object v7, v7

    .line 1861948
    move-object v0, v7

    .line 1861949
    invoke-virtual {v1, v0}, LX/1c9;->add(Ljava/lang/Object;)Z

    .line 1861950
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1861951
    :cond_8
    iget-object v0, p0, LX/CEo;->a:LX/CEp;

    iget-object v0, v0, LX/CEp;->c:LX/CEw;

    .line 1861952
    iput-object v1, v0, LX/CEw;->a:LX/1c9;

    .line 1861953
    move-object v0, v1

    .line 1861954
    goto/16 :goto_0
.end method
