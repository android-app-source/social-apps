.class public LX/C33;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/C34;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            "LX/C34;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1845049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845050
    iput-object p1, p0, LX/C33;->a:Landroid/view/View$OnClickListener;

    .line 1845051
    iput-object p2, p0, LX/C33;->b:LX/C34;

    .line 1845052
    iput-object p3, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845053
    iput-object p4, p0, LX/C33;->d:Ljava/lang/String;

    .line 1845054
    return-void
.end method


# virtual methods
.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1845055
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
