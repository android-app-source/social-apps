.class public final LX/BvZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V
    .locals 0

    .prologue
    .line 1832378
    iput-object p1, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 3

    .prologue
    .line 1832379
    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 1832380
    :cond_0
    :goto_0
    return-void

    .line 1832381
    :cond_1
    iget-object v0, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getFeedback(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1832382
    if-eqz v0, :cond_0

    .line 1832383
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    const-string v2, "video"

    .line 1832384
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 1832385
    move-object v1, v1

    .line 1832386
    const-string v2, "video_fullscreen_ufi"

    .line 1832387
    iput-object v2, v1, LX/21A;->b:Ljava/lang/String;

    .line 1832388
    move-object v1, v1

    .line 1832389
    iget-object v2, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    invoke-static {v2}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getTrackingCodes(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)LX/162;

    move-result-object v2

    .line 1832390
    iput-object v2, v1, LX/21A;->a:LX/162;

    .line 1832391
    move-object v1, v1

    .line 1832392
    sget-object v2, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    .line 1832393
    iput-object v2, v1, LX/21A;->i:LX/21D;

    .line 1832394
    move-object v1, v1

    .line 1832395
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 1832396
    iget-object v2, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->e:LX/20h;

    invoke-virtual {v2, v0, p2, v1, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    .line 1832397
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1832398
    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 1832399
    iget-object v0, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->n:LX/20r;

    invoke-virtual {v0, p1}, LX/20r;->a(Landroid/view/View;)V

    .line 1832400
    :cond_2
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1832401
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1832402
    iget-object v0, p0, LX/BvZ;->a:Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->o:LX/20s;

    invoke-virtual {v0, p1}, LX/20s;->a(Landroid/view/View;)V

    goto :goto_0
.end method
