.class public LX/BMg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/BMj;

.field public final b:LX/BMm;

.field public final c:LX/BMc;

.field public final d:LX/BMP;

.field public final e:LX/B5l;

.field public final f:LX/1E1;


# direct methods
.method public constructor <init>(LX/BMj;LX/BMm;LX/BMc;LX/BMP;LX/B5l;LX/1E1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777788
    iput-object p1, p0, LX/BMg;->a:LX/BMj;

    .line 1777789
    iput-object p2, p0, LX/BMg;->b:LX/BMm;

    .line 1777790
    iput-object p3, p0, LX/BMg;->c:LX/BMc;

    .line 1777791
    iput-object p4, p0, LX/BMg;->d:LX/BMP;

    .line 1777792
    iput-object p5, p0, LX/BMg;->e:LX/B5l;

    .line 1777793
    iput-object p6, p0, LX/BMg;->f:LX/1E1;

    .line 1777794
    return-void
.end method

.method public static a(LX/0QB;)LX/BMg;
    .locals 10

    .prologue
    .line 1777776
    const-class v1, LX/BMg;

    monitor-enter v1

    .line 1777777
    :try_start_0
    sget-object v0, LX/BMg;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777778
    sput-object v2, LX/BMg;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777779
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777780
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777781
    new-instance v3, LX/BMg;

    invoke-static {v0}, LX/BMj;->a(LX/0QB;)LX/BMj;

    move-result-object v4

    check-cast v4, LX/BMj;

    invoke-static {v0}, LX/BMm;->a(LX/0QB;)LX/BMm;

    move-result-object v5

    check-cast v5, LX/BMm;

    invoke-static {v0}, LX/BMc;->a(LX/0QB;)LX/BMc;

    move-result-object v6

    check-cast v6, LX/BMc;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v7

    check-cast v7, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v8

    check-cast v8, LX/B5l;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v9

    check-cast v9, LX/1E1;

    invoke-direct/range {v3 .. v9}, LX/BMg;-><init>(LX/BMj;LX/BMm;LX/BMc;LX/BMP;LX/B5l;LX/1E1;)V

    .line 1777782
    move-object v0, v3

    .line 1777783
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777784
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BMg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777785
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777786
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
