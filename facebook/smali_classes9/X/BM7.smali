.class public LX/BM7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AkL;


# instance fields
.field private final a:LX/1E1;

.field private final b:Lcom/facebook/productionprompts/model/ProductionPrompt;

.field private final c:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/1bQ;LX/1E1;)V
    .locals 1
    .param p1    # Lcom/facebook/productionprompts/model/ProductionPrompt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776883
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776884
    iput-object p1, p0, LX/BM7;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1776885
    invoke-virtual {p2}, LX/1bQ;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/BM7;->c:Landroid/graphics/drawable/Drawable;

    .line 1776886
    iput-object p3, p0, LX/BM7;->a:LX/1E1;

    .line 1776887
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776881
    iget-object v0, p0, LX/BM7;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776880
    iget-object v0, p0, LX/BM7;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1776879
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776874
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776878
    iget-object v0, p0, LX/BM7;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BM7;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1776877
    iget-object v0, p0, LX/BM7;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public g()LX/AkM;
    .locals 1

    .prologue
    .line 1776876
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776875
    iget-object v0, p0, LX/BM7;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->v()Lcom/facebook/productionprompts/model/PromptDisplayReason;

    move-result-object v0

    return-object v0
.end method
