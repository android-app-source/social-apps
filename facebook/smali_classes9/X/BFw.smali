.class public final LX/BFw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fb;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766440
    iput-object p1, p0, LX/BFw;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1766441
    iget-object v0, p0, LX/BFw;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1766442
    iput-boolean v1, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->s:Z

    .line 1766443
    iget-object v0, p0, LX/BFw;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    .line 1766444
    iget-object v2, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C:LX/BFq;

    if-nez v2, :cond_0

    .line 1766445
    :goto_0
    iget-object v0, p0, LX/BFw;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    .line 1766446
    iput-boolean v1, v0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    .line 1766447
    return-void

    .line 1766448
    :cond_0
    iget-object v2, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 1766449
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v3

    .line 1766450
    new-instance v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;

    invoke-direct {v3, v0}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment$11$1;-><init>(LX/Jvd;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1766451
    iget-object v0, p0, LX/BFw;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->o:LX/03V;

    sget-object v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v2, "failed to initialize camera"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766452
    return-void
.end method
