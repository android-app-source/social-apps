.class public final LX/CcE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;LX/5kD;)V
    .locals 0

    .prologue
    .line 1920717
    iput-object p1, p0, LX/CcE;->c:LX/CcO;

    iput-object p2, p0, LX/CcE;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CcE;->b:LX/5kD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1920718
    iget-object v0, p0, LX/CcE;->c:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    const/16 v0, 0x7d2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1920719
    iget-object v0, p0, LX/CcE;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1920720
    new-instance v3, LX/CbS;

    invoke-direct {v3, v0}, LX/CbS;-><init>(Landroid/content/Context;)V

    move-object v3, v3

    .line 1920721
    iget-object v0, p0, LX/CcE;->b:LX/5kD;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1920722
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    iput-object v4, v3, LX/CbS;->h:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1920723
    if-nez v0, :cond_0

    .line 1920724
    iget-object v4, v3, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1920725
    :goto_0
    iget-object v4, v3, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/CbS;->g:Ljava/lang/String;

    .line 1920726
    invoke-virtual {v3}, LX/CbS;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 1920727
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 1920728
    const/16 p1, 0x50

    iput p1, v5, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1920729
    const/4 p1, -0x1

    iput p1, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1920730
    invoke-virtual {v4, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1920731
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1920732
    invoke-virtual {v3}, LX/CbS;->show()V

    .line 1920733
    iget-object v4, v3, LX/CbS;->h:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v4

    .line 1920734
    iget-object v3, p0, LX/CcE;->c:LX/CcO;

    iget-object v4, p0, LX/CcE;->b:LX/5kD;

    .line 1920735
    new-instance v5, LX/Cc0;

    invoke-direct {v5, v3, v4}, LX/Cc0;-><init>(LX/CcO;LX/5kD;)V

    move-object v3, v5

    .line 1920736
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1920737
    const/4 v0, 0x1

    return v0

    .line 1920738
    :cond_0
    invoke-interface {v0}, LX/5kD;->X()LX/175;

    move-result-object v4

    .line 1920739
    iget-object v5, v3, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p1, v3, LX/CbS;->c:LX/3iT;

    invoke-static {v4, p1}, LX/8of;->a(LX/175;LX/3iT;)LX/8of;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
