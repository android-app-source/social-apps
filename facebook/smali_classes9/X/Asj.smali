.class public final LX/Asj;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Ass;


# direct methods
.method public constructor <init>(LX/Ass;I)V
    .locals 0

    .prologue
    .line 1720812
    iput-object p1, p0, LX/Asj;->b:LX/Ass;

    iput p2, p0, LX/Asj;->a:I

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 3

    .prologue
    .line 1720813
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1720814
    iget-object v1, p0, LX/Asj;->b:LX/Ass;

    .line 1720815
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 1720816
    iget-object v2, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(F)V

    .line 1720817
    iget-object v2, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1720818
    :goto_0
    iget-object v2, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleX(F)V

    .line 1720819
    iget-object v2, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleY(F)V

    .line 1720820
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e2b020c    # 0.167f

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 1720821
    iget-object v2, p0, LX/Asj;->b:LX/Ass;

    iget-object v2, v2, LX/Ass;->o:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1720822
    iget-object v2, p0, LX/Asj;->b:LX/Ass;

    iget-object v2, v2, LX/Ass;->o:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1720823
    iget-object v1, p0, LX/Asj;->b:LX/Ass;

    iget-object v1, v1, LX/Ass;->m:Landroid/view/View;

    iget v2, p0, LX/Asj;->a:I

    neg-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1720824
    return-void

    .line 1720825
    :cond_0
    iget-object v2, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 p1, 0x8

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 1720826
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1720827
    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 1720828
    iget-object v0, p0, LX/Asj;->b:LX/Ass;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ass;->a$redex0(LX/Ass;Z)V

    .line 1720829
    iget-object v0, p0, LX/Asj;->b:LX/Ass;

    invoke-static {v0}, LX/Ass;->c$redex0(LX/Ass;)V

    .line 1720830
    :cond_0
    :goto_0
    return-void

    .line 1720831
    :cond_1
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1720832
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1720833
    iget-object v0, p0, LX/Asj;->b:LX/Ass;

    const/4 v2, 0x0

    .line 1720834
    iget-object v1, v0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1720835
    iget-object v1, v0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1720836
    iget-object v1, v0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1720837
    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 6

    .prologue
    .line 1720838
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1720839
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1720840
    iget-object v0, p0, LX/Asj;->b:LX/Ass;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/Ass;->a$redex0(LX/Ass;Z)V

    .line 1720841
    :cond_0
    return-void
.end method
