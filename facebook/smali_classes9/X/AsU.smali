.class public final LX/AsU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsT;


# instance fields
.field public final synthetic a:LX/AsW;


# direct methods
.method public constructor <init>(LX/AsW;)V
    .locals 0

    .prologue
    .line 1720626
    iput-object p1, p0, LX/AsU;->a:LX/AsW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 5

    .prologue
    .line 1720627
    iget-object v0, p0, LX/AsU;->a:LX/AsW;

    iget-object v0, v0, LX/AsW;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720628
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    move v2, v1

    .line 1720629
    :goto_0
    iget-object v1, p0, LX/AsU;->a:LX/AsW;

    invoke-static {v1, v2}, LX/AsW;->a$redex0(LX/AsW;Z)V

    .line 1720630
    iget-object v3, p0, LX/AsU;->a:LX/AsW;

    if-eqz v2, :cond_1

    const v1, 0x7f082791

    .line 1720631
    :goto_1
    iget-object v4, v3, LX/AsW;->b:LX/0hs;

    iget-object p0, v3, LX/AsW;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, p0}, LX/0ht;->c(Landroid/view/View;)V

    .line 1720632
    iget-object v4, v3, LX/AsW;->b:LX/0hs;

    invoke-virtual {v4, v1}, LX/0hs;->b(I)V

    .line 1720633
    iget-object v4, v3, LX/AsW;->b:LX/0hs;

    invoke-virtual {v4}, LX/0ht;->d()V

    .line 1720634
    move-object v1, v0

    .line 1720635
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v3, LX/AuF;

    invoke-static {v3}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsMuted(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720636
    return-void

    .line 1720637
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    .line 1720638
    :cond_1
    const v1, 0x7f082790

    goto :goto_1
.end method
