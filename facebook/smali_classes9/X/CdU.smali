.class public LX/CdU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/CdW;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1922809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922810
    return-void
.end method

.method private static a(LX/Bex;)LX/162;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 1922800
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1922801
    iget-object v3, p0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    .line 1922802
    new-instance v5, LX/162;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/162;-><init>(LX/0mC;)V

    .line 1922803
    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    .line 1922804
    :cond_0
    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 1922805
    iget-wide v6, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 1922806
    invoke-virtual {v2, v5}, LX/162;->a(LX/0lF;)LX/162;

    .line 1922807
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1922808
    :cond_2
    return-object v2
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;LX/97c;)Ljava/lang/String;
    .locals 3
    .param p1    # LX/97c;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1922789
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1922790
    const-string v1, "hours_type"

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922791
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->OPEN_FOR_SELECTED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    invoke-virtual {v1, p0}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1922792
    const-string v1, "mon"

    const/4 v2, 0x2

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922793
    const-string v1, "tue"

    const/4 v2, 0x3

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922794
    const-string v1, "wed"

    const/4 v2, 0x4

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922795
    const-string v1, "thu"

    const/4 v2, 0x5

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922796
    const-string v1, "fri"

    const/4 v2, 0x6

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922797
    const-string v1, "sat"

    const/4 v2, 0x7

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922798
    const-string v1, "sun"

    const/4 v2, 0x1

    invoke-static {v2, p1}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object v2

    invoke-static {v2}, LX/CdU;->a(LX/Bex;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1922799
    :cond_0
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1922811
    if-eqz p2, :cond_0

    .line 1922812
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v0, p1, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922813
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 13

    .prologue
    .line 1922690
    check-cast p1, LX/CdW;

    .line 1922691
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "place-suggest-info"

    .line 1922692
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1922693
    move-object v0, v0

    .line 1922694
    const-string v1, "POST"

    .line 1922695
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1922696
    move-object v0, v0

    .line 1922697
    const-string v1, "%s/suggestions"

    iget-object v2, p1, LX/CdW;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1922698
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1922699
    move-object v0, v0

    .line 1922700
    const/4 v4, 0x0

    .line 1922701
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1922702
    const-string v3, "name"

    iget-object v5, p1, LX/CdW;->b:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922703
    const-string v3, "phone"

    iget-object v5, p1, LX/CdW;->i:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922704
    const-string v3, "email"

    iget-object v5, p1, LX/CdW;->f:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922705
    const-string v3, "website"

    iget-object v5, p1, LX/CdW;->j:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922706
    iget-object v3, p1, LX/CdW;->h:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    if-eqz v3, :cond_0

    .line 1922707
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "hours"

    iget-object v7, p1, LX/CdW;->h:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    iget-object v8, p1, LX/CdW;->g:LX/97c;

    invoke-static {v7, v8}, LX/CdU;->a(Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;LX/97c;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922708
    :cond_0
    iget-object v3, p1, LX/CdW;->c:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, p1, LX/CdW;->d:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, p1, LX/CdW;->e:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, p1, LX/CdW;->l:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 1922709
    :cond_1
    new-instance v3, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1922710
    iget-object v5, p1, LX/CdW;->c:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 1922711
    const-string v5, "address"

    iget-object v7, p1, LX/CdW;->c:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922712
    :cond_2
    iget-object v5, p1, LX/CdW;->d:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 1922713
    const-string v5, "city"

    iget-object v7, p1, LX/CdW;->d:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922714
    :cond_3
    iget-object v5, p1, LX/CdW;->e:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 1922715
    const-string v5, "city_id"

    iget-object v7, p1, LX/CdW;->e:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922716
    :cond_4
    iget-object v5, p1, LX/CdW;->l:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 1922717
    const-string v5, "zip_code"

    iget-object v7, p1, LX/CdW;->l:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922718
    :cond_5
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "location"

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922719
    :cond_6
    iget-object v3, p1, LX/CdW;->m:Lcom/facebook/location/ImmutableLocation;

    if-eqz v3, :cond_7

    .line 1922720
    new-instance v3, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1922721
    const-string v5, "latitude"

    iget-object v7, p1, LX/CdW;->m:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v7

    invoke-virtual {v3, v5, v7, v8}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1922722
    const-string v5, "longitude"

    iget-object v7, p1, LX/CdW;->m:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v7

    invoke-virtual {v3, v5, v7, v8}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1922723
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "coordinates"

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922724
    :cond_7
    iget-object v3, p1, LX/CdW;->n:LX/0Px;

    if-eqz v3, :cond_9

    .line 1922725
    new-instance v7, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1922726
    iget-object v8, p1, LX/CdW;->n:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    :goto_0
    if-ge v5, v9, :cond_8

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    .line 1922727
    invoke-virtual {v7, v11, v12}, LX/162;->b(J)LX/162;

    .line 1922728
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1922729
    :cond_8
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "categories"

    invoke-virtual {v7}, LX/162;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922730
    :cond_9
    iget-object v3, p1, LX/CdW;->k:LX/0Px;

    if-eqz v3, :cond_b

    .line 1922731
    new-instance v7, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1922732
    iget-object v8, p1, LX/CdW;->k:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    :goto_1
    if-ge v5, v9, :cond_a

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1922733
    invoke-virtual {v7, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1922734
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 1922735
    :cond_a
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "websites"

    invoke-virtual {v7}, LX/162;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922736
    :cond_b
    iget-object v3, p1, LX/CdW;->o:LX/0Px;

    if-eqz v3, :cond_d

    .line 1922737
    new-instance v7, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1922738
    iget-object v8, p1, LX/CdW;->o:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    :goto_2
    if-ge v5, v9, :cond_c

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    .line 1922739
    invoke-virtual {v7, v11, v12}, LX/162;->b(J)LX/162;

    .line 1922740
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 1922741
    :cond_c
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "duplicates"

    invoke-virtual {v7}, LX/162;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922742
    :cond_d
    const-string v3, "originalName"

    iget-object v5, p1, LX/CdW;->p:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922743
    const-string v3, "originalPhone"

    iget-object v5, p1, LX/CdW;->w:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922744
    const-string v3, "originalEmail"

    iget-object v5, p1, LX/CdW;->t:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922745
    const-string v3, "originalWebsite"

    iget-object v5, p1, LX/CdW;->x:Ljava/lang/String;

    invoke-static {v6, v3, v5}, LX/CdU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922746
    iget-object v3, p1, LX/CdW;->v:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    if-eqz v3, :cond_e

    .line 1922747
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "originalHours"

    iget-object v7, p1, LX/CdW;->v:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    iget-object v8, p1, LX/CdW;->u:LX/97c;

    invoke-static {v7, v8}, LX/CdU;->a(Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;LX/97c;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922748
    :cond_e
    iget-object v3, p1, LX/CdW;->q:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, p1, LX/CdW;->r:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, p1, LX/CdW;->s:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, p1, LX/CdW;->z:Ljava/lang/String;

    if-eqz v3, :cond_14

    .line 1922749
    :cond_f
    new-instance v3, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1922750
    iget-object v5, p1, LX/CdW;->q:Ljava/lang/String;

    if-eqz v5, :cond_10

    .line 1922751
    const-string v5, "address"

    iget-object v7, p1, LX/CdW;->q:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922752
    :cond_10
    iget-object v5, p1, LX/CdW;->r:Ljava/lang/String;

    if-eqz v5, :cond_11

    .line 1922753
    const-string v5, "city"

    iget-object v7, p1, LX/CdW;->r:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922754
    :cond_11
    iget-object v5, p1, LX/CdW;->s:Ljava/lang/String;

    if-eqz v5, :cond_12

    .line 1922755
    const-string v5, "city_id"

    iget-object v7, p1, LX/CdW;->s:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922756
    :cond_12
    iget-object v5, p1, LX/CdW;->z:Ljava/lang/String;

    if-eqz v5, :cond_13

    .line 1922757
    const-string v5, "zip_code"

    iget-object v7, p1, LX/CdW;->z:Ljava/lang/String;

    invoke-virtual {v3, v5, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1922758
    :cond_13
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "originalLocation"

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922759
    :cond_14
    iget-object v3, p1, LX/CdW;->B:Lcom/facebook/location/ImmutableLocation;

    if-eqz v3, :cond_15

    .line 1922760
    new-instance v3, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1922761
    const-string v5, "latitude"

    iget-object v7, p1, LX/CdW;->B:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v7

    invoke-virtual {v3, v5, v7, v8}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1922762
    const-string v5, "longitude"

    iget-object v7, p1, LX/CdW;->B:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v7

    invoke-virtual {v3, v5, v7, v8}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1922763
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "originalCoordinates"

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922764
    :cond_15
    iget-object v3, p1, LX/CdW;->C:LX/0Px;

    if-eqz v3, :cond_17

    .line 1922765
    new-instance v7, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1922766
    iget-object v8, p1, LX/CdW;->C:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    :goto_3
    if-ge v5, v9, :cond_16

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    .line 1922767
    invoke-virtual {v7, v11, v12}, LX/162;->b(J)LX/162;

    .line 1922768
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 1922769
    :cond_16
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "originalCategories"

    invoke-virtual {v7}, LX/162;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922770
    :cond_17
    iget-object v3, p1, LX/CdW;->y:LX/0Px;

    if-eqz v3, :cond_19

    .line 1922771
    new-instance v5, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v3}, LX/162;-><init>(LX/0mC;)V

    .line 1922772
    iget-object v7, p1, LX/CdW;->y:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    :goto_4
    if-ge v4, v8, :cond_18

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1922773
    invoke-virtual {v5, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1922774
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 1922775
    :cond_18
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "originalWebsites"

    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922776
    :cond_19
    iget-object v3, p1, LX/CdW;->A:LX/CdT;

    if-eqz v3, :cond_1a

    .line 1922777
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "source"

    iget-object v5, p1, LX/CdW;->A:LX/CdT;

    invoke-virtual {v5}, LX/CdT;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922778
    :cond_1a
    iget-object v3, p1, LX/CdW;->D:Ljava/lang/String;

    if-eqz v3, :cond_1b

    .line 1922779
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "entry_point"

    iget-object v5, p1, LX/CdW;->D:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922780
    :cond_1b
    iget-object v3, p1, LX/CdW;->E:Ljava/lang/String;

    if-eqz v3, :cond_1c

    .line 1922781
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "end_point"

    iget-object v5, p1, LX/CdW;->E:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1922782
    :cond_1c
    move-object v1, v6

    .line 1922783
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1922784
    move-object v0, v0

    .line 1922785
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1922786
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1922787
    move-object v0, v0

    .line 1922788
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1922688
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    .line 1922689
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
