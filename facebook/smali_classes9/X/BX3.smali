.class public final LX/BX3;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V
    .locals 1

    .prologue
    .line 1792572
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1792573
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BX3;->a:Ljava/lang/ref/WeakReference;

    .line 1792574
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 1792575
    iget-object v0, p0, LX/BX3;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    .line 1792576
    if-nez v0, :cond_0

    .line 1792577
    :goto_0
    return-void

    .line 1792578
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1792579
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792580
    invoke-static {v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    goto :goto_0

    .line 1792581
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792582
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v0, v1, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
