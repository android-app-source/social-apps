.class public LX/AYY;
.super LX/AWT;
.source ""

# interfaces
.implements LX/AXs;
.implements LX/AYX;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final s:LX/0Tn;

.field private static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;


# instance fields
.field public final A:Lcom/facebook/fbui/glyph/GlyphView;

.field public final B:Lcom/facebook/widget/text/BetterTextView;

.field public final C:Landroid/animation/ObjectAnimator;

.field public final D:Landroid/view/View;

.field public final E:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:I

.field public L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Z

.field public N:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$FBLiveVideoTipJarPageEligibilityQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:LX/AYL;

.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ah9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/AaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/AaY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final w:Landroid/widget/FrameLayout;

.field public final x:Lcom/facebook/widget/text/BetterTextView;

.field public final y:Lcom/facebook/fbui/glyph/GlyphView;

.field public final z:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1685679
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "commercial_break_onboarding_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AYY;->s:LX/0Tn;

    .line 1685680
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "commercial_break_onboarding_notification"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AYY;->t:LX/0Tn;

    .line 1685681
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "facecast_donation_nux_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AYY;->u:LX/0Tn;

    .line 1685682
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "facecast_tip_jar_nux_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AYY;->v:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685783
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685784
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685781
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685782
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1685742
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685743
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/AYY;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1685744
    const v0, 0x7f0305ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1685745
    const v0, 0x7f0d0fe1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/AYY;->w:Landroid/widget/FrameLayout;

    .line 1685746
    const v0, 0x7f0d104e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AYY;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 1685747
    const v0, 0x7f0d1049

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYY;->y:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685748
    const v0, 0x7f0d104b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYY;->z:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685749
    const v0, 0x7f0d104c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AYY;->D:Landroid/view/View;

    .line 1685750
    const v0, 0x7f0d104a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/AYY;->E:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1685751
    const v0, 0x7f0d1048

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    .line 1685752
    const v0, 0x7f0d104d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685753
    iget-object v0, p0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/AYR;

    invoke-direct {v1, p0}, LX/AYR;-><init>(LX/AYY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685754
    iget-object v0, p0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setAlpha(F)V

    .line 1685755
    iget-object v0, p0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v5, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    .line 1685756
    iget-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1685757
    iget-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1685758
    iget-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1685759
    iget-object v0, p0, LX/AYY;->b:LX/1b4;

    iget-object v1, p0, LX/AYY;->o:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685760
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 1685761
    invoke-virtual {v0, v1}, LX/1b4;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1685762
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->EditTitle:[I

    const v2, 0x7f010449

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1685763
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, LX/AYY;->K:I

    .line 1685764
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1685765
    :cond_0
    iget-object v0, p0, LX/AYY;->l:LX/3kp;

    sget-object v1, LX/AYY;->t:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1685766
    iget-object v0, p0, LX/AYY;->k:LX/3kp;

    sget-object v1, LX/AYY;->s:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1685767
    iput-boolean v4, p0, LX/AYY;->H:Z

    .line 1685768
    iget-object v0, p0, LX/AYY;->o:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685769
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1685770
    if-eqz v0, :cond_1

    .line 1685771
    iget-object v0, p0, LX/AYY;->o:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685772
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1685773
    new-instance v1, LX/6U3;

    invoke-direct {v1}, LX/6U3;-><init>()V

    move-object v1, v1

    .line 1685774
    const-string v2, "page_id"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1685775
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1685776
    iget-object v1, p0, LX/AYY;->q:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AYY;->N:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1685777
    iget-object v0, p0, LX/AYY;->N:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/AYW;

    invoke-direct {v1, p0}, LX/AYW;-><init>(LX/AYY;)V

    iget-object v2, p0, LX/AYY;->r:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1685778
    :goto_0
    return-void

    .line 1685779
    :cond_1
    iget-object v0, p0, LX/AYY;->i:LX/0ad;

    sget-short v1, LX/1v6;->m:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/AYY;->H:Z

    .line 1685780
    invoke-static {p0}, LX/AYY;->q(LX/AYY;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method private static a(LX/AYY;LX/0kL;LX/1b4;LX/0wM;Ljava/lang/String;LX/Ah9;LX/AaQ;LX/0ad;LX/Ac6;LX/3kp;LX/3kp;LX/3kp;LX/3kp;Lcom/facebook/auth/viewercontext/ViewerContext;LX/AaY;LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 1685741
    iput-object p1, p0, LX/AYY;->a:LX/0kL;

    iput-object p2, p0, LX/AYY;->b:LX/1b4;

    iput-object p3, p0, LX/AYY;->c:LX/0wM;

    iput-object p4, p0, LX/AYY;->f:Ljava/lang/String;

    iput-object p5, p0, LX/AYY;->g:LX/Ah9;

    iput-object p6, p0, LX/AYY;->h:LX/AaQ;

    iput-object p7, p0, LX/AYY;->i:LX/0ad;

    iput-object p8, p0, LX/AYY;->j:LX/Ac6;

    iput-object p9, p0, LX/AYY;->k:LX/3kp;

    iput-object p10, p0, LX/AYY;->l:LX/3kp;

    iput-object p11, p0, LX/AYY;->m:LX/3kp;

    iput-object p12, p0, LX/AYY;->n:LX/3kp;

    iput-object p13, p0, LX/AYY;->o:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p14, p0, LX/AYY;->p:LX/AaY;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/AYY;->q:LX/0tX;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/AYY;->r:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 17

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v16

    move-object/from16 v0, p0

    check-cast v0, LX/AYY;

    invoke-static/range {v16 .. v16}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    invoke-static/range {v16 .. v16}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v2

    check-cast v2, LX/1b4;

    invoke-static/range {v16 .. v16}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static/range {v16 .. v16}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static/range {v16 .. v16}, LX/Ah9;->a(LX/0QB;)LX/Ah9;

    move-result-object v5

    check-cast v5, LX/Ah9;

    invoke-static/range {v16 .. v16}, LX/AaQ;->a(LX/0QB;)LX/AaQ;

    move-result-object v6

    check-cast v6, LX/AaQ;

    invoke-static/range {v16 .. v16}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static/range {v16 .. v16}, LX/Ac6;->a(LX/0QB;)LX/Ac6;

    move-result-object v8

    check-cast v8, LX/Ac6;

    invoke-static/range {v16 .. v16}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v9

    check-cast v9, LX/3kp;

    invoke-static/range {v16 .. v16}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v10

    check-cast v10, LX/3kp;

    invoke-static/range {v16 .. v16}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v11

    check-cast v11, LX/3kp;

    invoke-static/range {v16 .. v16}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v12

    check-cast v12, LX/3kp;

    invoke-static/range {v16 .. v16}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v13

    check-cast v13, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v16 .. v16}, LX/AaY;->a(LX/0QB;)LX/AaY;

    move-result-object v14

    check-cast v14, LX/AaY;

    invoke-static/range {v16 .. v16}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v15

    check-cast v15, LX/0tX;

    invoke-static/range {v16 .. v16}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v16

    check-cast v16, Ljava/util/concurrent/Executor;

    invoke-static/range {v0 .. v16}, LX/AYY;->a(LX/AYY;LX/0kL;LX/1b4;LX/0wM;Ljava/lang/String;LX/Ah9;LX/AaQ;LX/0ad;LX/Ac6;LX/3kp;LX/3kp;LX/3kp;LX/3kp;Lcom/facebook/auth/viewercontext/ViewerContext;LX/AaY;LX/0tX;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static a$redex0(LX/AYY;LX/AXr;)V
    .locals 5

    .prologue
    .line 1685722
    invoke-virtual {p0}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1685723
    if-nez v0, :cond_0

    .line 1685724
    :goto_0
    return-void

    .line 1685725
    :cond_0
    const/4 v2, 0x0

    .line 1685726
    invoke-virtual {p0}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Landroid/app/Activity;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1685727
    if-eqz v1, :cond_1

    .line 1685728
    const v3, 0x7f0d0fc9

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1685729
    if-eqz v1, :cond_1

    .line 1685730
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1685731
    :goto_1
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;

    invoke-direct {v2}, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;-><init>()V

    .line 1685732
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1685733
    const-string v4, "conflicting_status"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1685734
    const-string v4, "square_view_height"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1685735
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1685736
    move-object v1, v2

    .line 1685737
    iput-object v1, p0, LX/AYY;->G:Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;

    .line 1685738
    iget-object v1, p0, LX/AYY;->G:Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;

    .line 1685739
    iput-object p0, v1, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->s:LX/AXs;

    .line 1685740
    iget-object v1, p0, LX/AYY;->G:Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "FACECAST_MONETIZATION_CONFLICTING_FRAGMENT_TAG"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static j(LX/AYY;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1685683
    iget-boolean v0, p0, LX/AYY;->M:Z

    if-nez v0, :cond_1

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1685684
    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v0, :cond_6

    .line 1685685
    :cond_0
    :goto_0
    move v0, v3

    .line 1685686
    if-nez v0, :cond_2

    .line 1685687
    :cond_1
    iget-object v0, p0, LX/AYY;->E:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1685688
    :goto_1
    return-void

    .line 1685689
    :cond_2
    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-eqz v0, :cond_3

    .line 1685690
    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 1685691
    :goto_2
    if-eqz v1, :cond_5

    sget-object v0, LX/AZC;->VIOLATION:LX/AZC;

    .line 1685692
    :goto_3
    iget-object v3, p0, LX/AYY;->D:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1685693
    instance-of v4, v3, Landroid/graphics/drawable/GradientDrawable;

    if-nez v4, :cond_a

    .line 1685694
    :goto_4
    if-nez v1, :cond_3

    .line 1685695
    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    sget-object v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;->UNKNOWN:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    if-ne v0, v1, :cond_d

    .line 1685696
    :cond_3
    :goto_5
    iget-object v0, p0, LX/AYY;->E:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 1685697
    goto :goto_2

    .line 1685698
    :cond_5
    sget-object v0, LX/AZC;->ELIGIBLE:LX/AZC;

    goto :goto_3

    .line 1685699
    :cond_6
    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 1685700
    :goto_6
    iget-object v4, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    if-eqz v4, :cond_9

    iget-object v4, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    move v4, v1

    .line 1685701
    :goto_7
    iget-object v5, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-boolean v5, v5, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    if-eqz v5, :cond_0

    if-nez v0, :cond_7

    if-eqz v4, :cond_0

    :cond_7
    move v3, v1

    goto :goto_0

    :cond_8
    move v0, v3

    .line 1685702
    goto :goto_6

    :cond_9
    move v4, v3

    .line 1685703
    goto :goto_7

    .line 1685704
    :cond_a
    sget-object v4, LX/AZC;->VIOLATION:LX/AZC;

    if-ne v0, v4, :cond_b

    .line 1685705
    check-cast v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_4

    .line 1685706
    :cond_b
    iget-object v4, p0, LX/AYY;->l:LX/3kp;

    invoke-virtual {v4}, LX/3kp;->c()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1685707
    check-cast v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_4

    .line 1685708
    :cond_c
    iget-object v3, p0, LX/AYY;->D:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 1685709
    :cond_d
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1685710
    iget-object v1, p0, LX/AYY;->E:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1685711
    iget-object v1, p0, LX/AYY;->k:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1685712
    iget-object v1, p0, LX/AYY;->c:LX/0wM;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02082c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1685713
    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1685714
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080cf7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1685715
    iget-object v1, p0, LX/AYY;->L:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v1, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViewerCountCategory:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    sget-object v3, Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;->SOMETIMES_MEETS_THRESHOLD:Lcom/facebook/video/videostreaming/protocol/CommercialBreakBroadcasterViewerCountCategory;

    if-ne v1, v3, :cond_e

    .line 1685716
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080cf8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1685717
    :goto_8
    const/16 v1, 0x2710

    .line 1685718
    iput v1, v0, LX/0hs;->t:I

    .line 1685719
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1685720
    iget-object v0, p0, LX/AYY;->k:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    goto/16 :goto_5

    .line 1685721
    :cond_e
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080cf9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    goto :goto_8
.end method

.method public static q(LX/AYY;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1685785
    iget-boolean v0, p0, LX/AYY;->H:Z

    if-nez v0, :cond_0

    .line 1685786
    iget-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685787
    iget-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685788
    iput-boolean v1, p0, LX/AYY;->I:Z

    .line 1685789
    :goto_0
    return-void

    .line 1685790
    :cond_0
    iget-object v0, p0, LX/AYY;->o:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685791
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 1685792
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/AYY;->I:Z

    .line 1685793
    iget-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685794
    iget-object v1, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v0, p0, LX/AYY;->I:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685795
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1685796
    iget-object v1, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1685797
    iget-object v1, p0, LX/AYY;->n:LX/3kp;

    const/4 v2, 0x3

    .line 1685798
    iput v2, v1, LX/3kp;->b:I

    .line 1685799
    iget-object v1, p0, LX/AYY;->n:LX/3kp;

    sget-object v2, LX/AYY;->v:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 1685800
    iget-object v1, p0, LX/AYY;->n:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1685801
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080d1c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1685802
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080d1d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1685803
    const/16 v1, 0x2710

    .line 1685804
    iput v1, v0, LX/0hs;->t:I

    .line 1685805
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1685806
    iget-object v0, p0, LX/AYY;->n:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1685807
    :cond_1
    goto :goto_0

    .line 1685808
    :cond_2
    iget-object v0, p0, LX/AYY;->h:LX/AaQ;

    invoke-virtual {v0}, LX/AaQ;->a()Z

    move-result v0

    goto :goto_1

    .line 1685809
    :cond_3
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1685612
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1685613
    iget-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1685614
    iget-object v0, p0, LX/AYY;->p:LX/AaY;

    invoke-virtual {v0}, LX/AaY;->e()V

    .line 1685615
    return-void
.end method

.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 6
    .param p1    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1685616
    iget-boolean v0, p0, LX/AYY;->H:Z

    if-eqz v0, :cond_1

    .line 1685617
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1685618
    if-eqz p1, :cond_0

    .line 1685619
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685620
    if-eqz v0, :cond_0

    .line 1685621
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685622
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v3

    .line 1685623
    :goto_0
    move v0, v0

    .line 1685624
    if-nez v0, :cond_2

    iget-object v0, p0, LX/AYY;->j:LX/Ac6;

    .line 1685625
    iget-object v2, v0, LX/Ac6;->a:LX/0Uh;

    const/16 v3, 0x43e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 1685626
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1685627
    :goto_1
    iget-object v3, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_3

    const/16 v2, 0x8

    :goto_2
    invoke-virtual {v3, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685628
    if-eqz v0, :cond_1

    .line 1685629
    iput-boolean v1, p0, LX/AYY;->I:Z

    .line 1685630
    iget-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685631
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1685632
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1685633
    goto :goto_2

    .line 1685634
    :cond_4
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685635
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v4

    .line 1685636
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v5, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "114000975315193"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    move v0, v3

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1685637
    iget-object v2, p0, LX/AYY;->y:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685638
    if-eqz p1, :cond_2

    .line 1685639
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1685640
    iget-object v1, p0, LX/AYY;->y:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1685641
    iget-object v1, p0, LX/AYY;->m:LX/3kp;

    const/4 v2, 0x3

    .line 1685642
    iput v2, v1, LX/3kp;->b:I

    .line 1685643
    iget-object v1, p0, LX/AYY;->m:LX/3kp;

    sget-object v2, LX/AYY;->u:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 1685644
    iget-object v1, p0, LX/AYY;->m:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1685645
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cfa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1685646
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cfb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1685647
    const/16 v1, 0x2710

    .line 1685648
    iput v1, v0, LX/0hs;->t:I

    .line 1685649
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1685650
    iget-object v0, p0, LX/AYY;->m:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1685651
    :cond_0
    :goto_1
    return-void

    .line 1685652
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 1685653
    :cond_2
    iput-boolean v1, p0, LX/AYY;->J:Z

    goto :goto_1
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    .line 1685654
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    .line 1685655
    :goto_0
    iget-object v1, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_0

    .line 1685656
    iget-object v2, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685657
    :cond_0
    iput-boolean v0, p0, LX/AYY;->I:Z

    .line 1685658
    iget-object v0, p0, LX/AYY;->h:LX/AaQ;

    iget-boolean v1, p0, LX/AYY;->I:Z

    invoke-virtual {v0, v1}, LX/AaQ;->a(Z)V

    .line 1685659
    return-void

    .line 1685660
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1685661
    :cond_2
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 1685662
    iput-boolean p1, p0, LX/AYY;->J:Z

    .line 1685663
    iget-object v1, p0, LX/AYY;->y:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a008d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685664
    return-void

    .line 1685665
    :cond_0
    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1685666
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AYY;->b(Z)V

    .line 1685667
    iget-object v0, p0, LX/AYY;->O:LX/AYL;

    if-eqz v0, :cond_0

    .line 1685668
    iget-object v0, p0, LX/AYY;->O:LX/AYL;

    .line 1685669
    iget-object p0, v0, LX/AYL;->a:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    iget-object p0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->i()V

    .line 1685670
    :cond_0
    return-void
.end method

.method public final iF_()V
    .locals 1

    .prologue
    .line 1685671
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1685672
    iget-object v0, p0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1685673
    iget-object v0, p0, LX/AYY;->C:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1685674
    :cond_0
    return-void
.end method

.method public final iG_()V
    .locals 3

    .prologue
    .line 1685675
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AYY;->I:Z

    .line 1685676
    iget-object v0, p0, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1685677
    iget-object v0, p0, LX/AYY;->h:LX/AaQ;

    iget-boolean v1, p0, LX/AYY;->I:Z

    invoke-virtual {v0, v1}, LX/AaQ;->a(Z)V

    .line 1685678
    return-void
.end method
