.class public final LX/Buq;
.super LX/Bup;
.source ""


# instance fields
.field public final synthetic b:LX/Bus;


# direct methods
.method public constructor <init>(LX/Bus;)V
    .locals 0

    .prologue
    .line 1831717
    iput-object p1, p0, LX/Buq;->b:LX/Bus;

    invoke-direct {p0, p1}, LX/Bup;-><init>(LX/Bur;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1831718
    invoke-super {p0, p1, p2, p3, p4}, LX/Bup;->a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1831719
    :goto_0
    return v0

    .line 1831720
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->SOME:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1831721
    if-eqz v1, :cond_2

    .line 1831722
    iget-object v1, p0, LX/Buq;->b:LX/Bus;

    .line 1831723
    const v2, 0x7f08113f

    invoke-virtual {p4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1831724
    const v3, 0x7f081140

    invoke-virtual {p4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 1831725
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    .line 1831726
    instance-of v2, v3, LX/3Ai;

    if-eqz v2, :cond_1

    move-object v2, v3

    .line 1831727
    check-cast v2, LX/3Ai;

    invoke-virtual {v2, p0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1831728
    :cond_1
    new-instance v2, LX/Buo;

    invoke-direct {v2, v1, p3}, LX/Buo;-><init>(LX/Bus;Lcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831729
    const v2, 0x7f0208b2

    move v2, v2

    .line 1831730
    invoke-virtual {v1, v3, v2, p2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831731
    goto :goto_0

    .line 1831732
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
