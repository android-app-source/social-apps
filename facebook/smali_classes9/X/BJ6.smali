.class public final enum LX/BJ6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BJ6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BJ6;

.field public static final enum DRAFT_COMPOSER_POST_CLICKED:LX/BJ6;

.field public static final enum DRAFT_DIALOG_BACK_CLICKED:LX/BJ6;

.field public static final enum DRAFT_DIALOG_DISCARD_CLICKED:LX/BJ6;

.field public static final enum DRAFT_DIALOG_SAVE_CLICKED:LX/BJ6;

.field public static final enum DRAFT_DIALOG_SEEN:LX/BJ6;

.field public static final enum DRAFT_ERROR_SAVED_MEDIA_NOT_FOUND:LX/BJ6;

.field public static final enum DRAFT_NOTIFICATION_CLICKED:LX/BJ6;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1771588
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_DIALOG_SEEN"

    const-string v2, "fb4a_draft_dialog_seen"

    invoke-direct {v0, v1, v4, v2}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_DIALOG_SEEN:LX/BJ6;

    .line 1771589
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_DIALOG_SAVE_CLICKED"

    const-string v2, "fb4a_draft_dialog_save_clicked"

    invoke-direct {v0, v1, v5, v2}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_DIALOG_SAVE_CLICKED:LX/BJ6;

    .line 1771590
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_DIALOG_DISCARD_CLICKED"

    const-string v2, "fb4a_draft_dialog_discard_clicked"

    invoke-direct {v0, v1, v6, v2}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_DIALOG_DISCARD_CLICKED:LX/BJ6;

    .line 1771591
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_DIALOG_BACK_CLICKED"

    const-string v2, "fb4a_draft_dialog_back_clicked"

    invoke-direct {v0, v1, v7, v2}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_DIALOG_BACK_CLICKED:LX/BJ6;

    .line 1771592
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_NOTIFICATION_CLICKED"

    const-string v2, "fb4a_draft_notification_clicked"

    invoke-direct {v0, v1, v8, v2}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_NOTIFICATION_CLICKED:LX/BJ6;

    .line 1771593
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_COMPOSER_POST_CLICKED"

    const/4 v2, 0x5

    const-string v3, "fb4a_draft_composer_post_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_COMPOSER_POST_CLICKED:LX/BJ6;

    .line 1771594
    new-instance v0, LX/BJ6;

    const-string v1, "DRAFT_ERROR_SAVED_MEDIA_NOT_FOUND"

    const/4 v2, 0x6

    const-string v3, "fb4a_draft_error_saved_media_not_found"

    invoke-direct {v0, v1, v2, v3}, LX/BJ6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BJ6;->DRAFT_ERROR_SAVED_MEDIA_NOT_FOUND:LX/BJ6;

    .line 1771595
    const/4 v0, 0x7

    new-array v0, v0, [LX/BJ6;

    sget-object v1, LX/BJ6;->DRAFT_DIALOG_SEEN:LX/BJ6;

    aput-object v1, v0, v4

    sget-object v1, LX/BJ6;->DRAFT_DIALOG_SAVE_CLICKED:LX/BJ6;

    aput-object v1, v0, v5

    sget-object v1, LX/BJ6;->DRAFT_DIALOG_DISCARD_CLICKED:LX/BJ6;

    aput-object v1, v0, v6

    sget-object v1, LX/BJ6;->DRAFT_DIALOG_BACK_CLICKED:LX/BJ6;

    aput-object v1, v0, v7

    sget-object v1, LX/BJ6;->DRAFT_NOTIFICATION_CLICKED:LX/BJ6;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BJ6;->DRAFT_COMPOSER_POST_CLICKED:LX/BJ6;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BJ6;->DRAFT_ERROR_SAVED_MEDIA_NOT_FOUND:LX/BJ6;

    aput-object v2, v0, v1

    sput-object v0, LX/BJ6;->$VALUES:[LX/BJ6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1771597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1771598
    iput-object p3, p0, LX/BJ6;->name:Ljava/lang/String;

    .line 1771599
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BJ6;
    .locals 1

    .prologue
    .line 1771600
    const-class v0, LX/BJ6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BJ6;

    return-object v0
.end method

.method public static values()[LX/BJ6;
    .locals 1

    .prologue
    .line 1771596
    sget-object v0, LX/BJ6;->$VALUES:[LX/BJ6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BJ6;

    return-object v0
.end method
