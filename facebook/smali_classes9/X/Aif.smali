.class public LX/Aif;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1706730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1706731
    invoke-virtual {p0, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarNameViewText(Ljava/lang/String;)V

    .line 1706732
    if-nez p2, :cond_0

    const v0, 0x41e065f

    if-ne p3, v0, :cond_0

    .line 1706733
    const v0, 0x7f0200f0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarImageViewDrawable(I)V

    .line 1706734
    :goto_0
    return-void

    .line 1706735
    :cond_0
    if-nez p2, :cond_1

    const v0, 0x25d6af

    if-ne p3, v0, :cond_1

    .line 1706736
    const v0, 0x7f0217e9

    invoke-virtual {p0, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarImageViewDrawable(I)V

    goto :goto_0

    .line 1706737
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
