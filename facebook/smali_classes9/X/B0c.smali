.class public LX/B0c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0U;


# instance fields
.field public final a:LX/B0V;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/B0V;J)V
    .locals 2

    .prologue
    .line 1734077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734078
    const-string v0, "SingleBatchConfiguration:%s"

    invoke-virtual {p1}, LX/B0V;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B0c;->c:Ljava/lang/String;

    .line 1734079
    iput-object p1, p0, LX/B0c;->a:LX/B0V;

    .line 1734080
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0xe10

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1734081
    iput-wide p2, p0, LX/B0c;->b:J

    .line 1734082
    return-void

    .line 1734083
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1734076
    iget-wide v0, p0, LX/B0c;->b:J

    return-wide v0
.end method

.method public final a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;)LX/0v6;
    .locals 3

    .prologue
    .line 1734067
    iget-object v0, p0, LX/B0c;->a:LX/B0V;

    invoke-static {p1, v0, p4}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;)LX/0zO;

    move-result-object v1

    .line 1734068
    iget-object v0, v1, LX/0zO;->a:LX/0zS;

    move-object v0, v0

    .line 1734069
    sget-object v2, LX/0zS;->c:LX/0zS;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1734070
    new-instance v0, LX/0v6;

    iget-object v2, p0, LX/B0c;->c:Ljava/lang/String;

    invoke-direct {v0, v2}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1734071
    invoke-virtual {v0, v1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0zX;->b(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v1

    new-instance v2, LX/B0b;

    invoke-direct {v2, p0, p4, p1, p2}, LX/B0b;-><init>(LX/B0c;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;)V

    invoke-virtual {v1, v2}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 1734072
    return-object v0

    .line 1734073
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1734075
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1734074
    const/4 v0, 0x0

    return v0
.end method
