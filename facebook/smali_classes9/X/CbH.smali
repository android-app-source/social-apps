.class public LX/CbH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:LX/03R;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0if;

.field public c:Z

.field public d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Cay;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1919342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919343
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/CbH;->a:LX/03R;

    .line 1919344
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/CbH;->e:Ljava/util/Set;

    .line 1919345
    iput-boolean v1, p0, LX/CbH;->f:Z

    .line 1919346
    iput-boolean v1, p0, LX/CbH;->g:Z

    .line 1919347
    iput-boolean v1, p0, LX/CbH;->c:Z

    .line 1919348
    iput-object p1, p0, LX/CbH;->b:LX/0if;

    .line 1919349
    return-void
.end method

.method public static a(LX/0QB;)LX/CbH;
    .locals 4

    .prologue
    .line 1919350
    const-class v1, LX/CbH;

    monitor-enter v1

    .line 1919351
    :try_start_0
    sget-object v0, LX/CbH;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1919352
    sput-object v2, LX/CbH;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1919353
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919354
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1919355
    new-instance p0, LX/CbH;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/CbH;-><init>(LX/0if;)V

    .line 1919356
    move-object v0, p0

    .line 1919357
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1919358
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CbH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1919359
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1919360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Cay;)V
    .locals 1

    .prologue
    .line 1919361
    iget-object v0, p0, LX/CbH;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1919362
    return-void
.end method

.method public final b(LX/Cay;)V
    .locals 1

    .prologue
    .line 1919363
    iget-object v0, p0, LX/CbH;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1919364
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1919365
    iget-object v0, p0, LX/CbH;->a:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1919366
    iget-boolean v0, p0, LX/CbH;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/CbH;->c:Z

    .line 1919367
    iget-object v0, p0, LX/CbH;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cay;

    .line 1919368
    iget-boolean v2, p0, LX/CbH;->c:Z

    invoke-interface {v0, v2}, LX/Cay;->a(Z)V

    goto :goto_1

    .line 1919369
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1919370
    :cond_1
    return-void
.end method
