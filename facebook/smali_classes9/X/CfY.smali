.class public LX/CfY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1926370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926371
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/Set;
    .locals 3
    .param p0    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1926372
    const-string v1, "ReactionUnitTagHelper.getTags"

    const v2, 0x50cfc021

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1926373
    if-nez p0, :cond_0

    .line 1926374
    const v1, -0x196225e6

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 1926375
    :cond_0
    :try_start_0
    new-instance v1, LX/CfX;

    invoke-direct {v1}, LX/CfX;-><init>()V

    .line 1926376
    check-cast p0, LX/0jT;

    invoke-virtual {v1, p0}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 1926377
    iget-object v2, v1, LX/CfX;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1926378
    :goto_1
    const v1, -0x4063498a

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1926379
    :cond_1
    :try_start_1
    iget-object v0, v1, LX/CfX;->a:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1926380
    :catchall_0
    move-exception v0

    const v1, 0x4936c48e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
