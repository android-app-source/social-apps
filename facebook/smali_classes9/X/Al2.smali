.class public LX/Al2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4V2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4V2",
        "<",
        "Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6BP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;

.field private final d:Lcom/facebook/graphql/model/GraphQLStory;

.field private final e:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/0SG;Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 0
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6BP;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709429
    iput-object p1, p0, LX/Al2;->a:LX/0Ot;

    .line 1709430
    iput-object p2, p0, LX/Al2;->b:LX/0Or;

    .line 1709431
    iput-object p3, p0, LX/Al2;->c:LX/0SG;

    .line 1709432
    iput-object p4, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1709433
    iput-boolean p5, p0, LX/Al2;->e:Z

    .line 1709434
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1709478
    const-string v0, "LegacySetNotifyMe"

    return-object v0
.end method

.method public final b()LX/0jT;
    .locals 9

    .prologue
    .line 1709456
    new-instance v0, LX/5Ak;

    invoke-direct {v0}, LX/5Ak;-><init>()V

    iget-object v1, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 1709457
    iput-object v1, v0, LX/5Ak;->c:Ljava/lang/String;

    .line 1709458
    move-object v0, v0

    .line 1709459
    iget-boolean v1, p0, LX/Al2;->e:Z

    .line 1709460
    iput-boolean v1, v0, LX/5Ak;->b:Z

    .line 1709461
    move-object v0, v0

    .line 1709462
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1709463
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1709464
    iget-object v3, v0, LX/5Ak;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1709465
    iget-object v5, v0, LX/5Ak;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1709466
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1709467
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1709468
    iget-boolean v3, v0, LX/5Ak;->b:Z

    invoke-virtual {v2, v6, v3}, LX/186;->a(IZ)V

    .line 1709469
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1709470
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1709471
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1709472
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1709473
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1709474
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1709475
    new-instance v3, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;-><init>(LX/15i;)V

    .line 1709476
    move-object v0, v3

    .line 1709477
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/feedback/FeedbackSubscriptionMutationsModels$FeedbackSubscribeMutationFragmentModel$FeedbackModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1709435
    invoke-static {}, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f()LX/5H5;

    move-result-object v0

    iget-object v1, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1709436
    iput-object v1, v0, LX/5H5;->e:Ljava/lang/String;

    .line 1709437
    move-object v0, v0

    .line 1709438
    iget-object v1, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1709439
    iput-object v1, v0, LX/5H5;->c:Ljava/lang/String;

    .line 1709440
    move-object v0, v0

    .line 1709441
    iget-object v1, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    .line 1709442
    iput-object v1, v0, LX/5H5;->b:Ljava/lang/String;

    .line 1709443
    move-object v0, v0

    .line 1709444
    iget-object v1, p0, LX/Al2;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 1709445
    iput-object v1, v0, LX/5H5;->a:Ljava/lang/String;

    .line 1709446
    move-object v0, v0

    .line 1709447
    iget-boolean v1, p0, LX/Al2;->e:Z

    .line 1709448
    iput-boolean v1, v0, LX/5H5;->d:Z

    .line 1709449
    move-object v0, v0

    .line 1709450
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 1709451
    iput-object v1, v0, LX/5H5;->f:Ljava/lang/String;

    .line 1709452
    move-object v0, v0

    .line 1709453
    invoke-virtual {v0}, LX/5H5;->g()Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    move-result-object v2

    .line 1709454
    iget-object v0, p0, LX/Al2;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/Al2;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709455
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {p0}, LX/Al2;->b()LX/0jT;

    move-result-object v1

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/Al2;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    return-object v0
.end method
