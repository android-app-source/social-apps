.class public final LX/BhC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/BgK;

.field public final synthetic b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

.field public final synthetic c:LX/BhD;


# direct methods
.method public constructor <init>(LX/BhD;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V
    .locals 0

    .prologue
    .line 1808862
    iput-object p1, p0, LX/BhC;->c:LX/BhD;

    iput-object p2, p0, LX/BhC;->a:LX/BgK;

    iput-object p3, p0, LX/BhC;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1808863
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1808864
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1808865
    iget-object v0, p0, LX/BhC;->a:LX/BgK;

    invoke-virtual {v0}, LX/BgK;->a()V

    .line 1808866
    iget-object v0, p0, LX/BhC;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a()V

    .line 1808867
    return-void
.end method
