.class public final enum LX/AvK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AvK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AvK;

.field public static final enum UNZIP_TO_CACHE:LX/AvK;

.field public static final enum WRITE_TO_CACHE:LX/AvK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1724004
    new-instance v0, LX/AvK;

    const-string v1, "WRITE_TO_CACHE"

    invoke-direct {v0, v1, v2}, LX/AvK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AvK;->WRITE_TO_CACHE:LX/AvK;

    .line 1724005
    new-instance v0, LX/AvK;

    const-string v1, "UNZIP_TO_CACHE"

    invoke-direct {v0, v1, v3}, LX/AvK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AvK;->UNZIP_TO_CACHE:LX/AvK;

    .line 1724006
    const/4 v0, 0x2

    new-array v0, v0, [LX/AvK;

    sget-object v1, LX/AvK;->WRITE_TO_CACHE:LX/AvK;

    aput-object v1, v0, v2

    sget-object v1, LX/AvK;->UNZIP_TO_CACHE:LX/AvK;

    aput-object v1, v0, v3

    sput-object v0, LX/AvK;->$VALUES:[LX/AvK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1724007
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AvK;
    .locals 1

    .prologue
    .line 1724008
    const-class v0, LX/AvK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AvK;

    return-object v0
.end method

.method public static values()[LX/AvK;
    .locals 1

    .prologue
    .line 1724009
    sget-object v0, LX/AvK;->$VALUES:[LX/AvK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AvK;

    return-object v0
.end method
