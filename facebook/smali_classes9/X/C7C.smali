.class public final LX/C7C;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C7E;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/8pR;

.field public final synthetic d:LX/C7E;


# direct methods
.method public constructor <init>(LX/C7E;)V
    .locals 1

    .prologue
    .line 1851026
    iput-object p1, p0, LX/C7C;->d:LX/C7E;

    .line 1851027
    move-object v0, p1

    .line 1851028
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1851029
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1851030
    const-string v0, "AutoTranslationComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/C7E;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1851023
    check-cast p1, LX/C7C;

    .line 1851024
    iget-object v0, p1, LX/C7C;->c:LX/8pR;

    iput-object v0, p0, LX/C7C;->c:LX/8pR;

    .line 1851025
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1851009
    if-ne p0, p1, :cond_1

    .line 1851010
    :cond_0
    :goto_0
    return v0

    .line 1851011
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1851012
    goto :goto_0

    .line 1851013
    :cond_3
    check-cast p1, LX/C7C;

    .line 1851014
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1851015
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1851016
    if-eq v2, v3, :cond_0

    .line 1851017
    iget-object v2, p0, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1851018
    goto :goto_0

    .line 1851019
    :cond_5
    iget-object v2, p1, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1851020
    :cond_6
    iget-object v2, p0, LX/C7C;->b:LX/1Pr;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C7C;->b:LX/1Pr;

    iget-object v3, p1, LX/C7C;->b:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1851021
    goto :goto_0

    .line 1851022
    :cond_7
    iget-object v2, p1, LX/C7C;->b:LX/1Pr;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1851006
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C7C;

    .line 1851007
    const/4 v1, 0x0

    iput-object v1, v0, LX/C7C;->c:LX/8pR;

    .line 1851008
    return-object v0
.end method
