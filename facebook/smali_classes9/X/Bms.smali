.class public final LX/Bms;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1819234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;
    .locals 2

    .prologue
    .line 1819235
    new-instance v0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    invoke-direct {v0, p0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1819236
    invoke-static {p1}, LX/Bms;->a(Landroid/os/Parcel;)Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1819237
    new-array v0, p1, [Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    move-object v0, v0

    .line 1819238
    return-object v0
.end method
