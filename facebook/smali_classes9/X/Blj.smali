.class public LX/Blj;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/events/planning/CalendarRange;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/events/planning/CalendarRange;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1816511
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1816512
    iput p3, p0, LX/Blj;->a:I

    .line 1816513
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1816514
    if-nez p2, :cond_0

    .line 1816515
    new-instance v2, LX/Bln;

    invoke-virtual {p0}, LX/Blj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/Bln;-><init>(Landroid/content/Context;)V

    .line 1816516
    :goto_0
    invoke-virtual {p0, p1}, LX/Blj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/planning/CalendarRange;

    move-object v1, v2

    .line 1816517
    check-cast v1, LX/Bln;

    iget v3, p0, LX/Blj;->a:I

    .line 1816518
    iput v3, v1, LX/Bln;->e:I

    .line 1816519
    iput p1, v1, LX/Bln;->f:I

    .line 1816520
    invoke-virtual {v1, v0}, LX/Bln;->setCalendarRange(Lcom/facebook/events/planning/CalendarRange;)V

    .line 1816521
    return-object v2

    :cond_0
    move-object v2, p2

    goto :goto_0
.end method
