.class public final LX/BOy;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1780623
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/BOy;
    .locals 3

    .prologue
    .line 1780620
    new-instance v0, LX/BOy;

    invoke-direct {v0}, LX/BOy;-><init>()V

    .line 1780621
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1, p0}, LX/BOy;->a(ILjava/nio/ByteBuffer;)LX/BOy;

    move-result-object v1

    move-object v0, v1

    .line 1780622
    return-object v0
.end method

.method public static a(LX/0eX;)V
    .locals 1

    .prologue
    .line 1780607
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eX;->b(I)V

    return-void
.end method

.method public static a(LX/0eX;B)V
    .locals 2

    .prologue
    .line 1780619
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IBI)V

    return-void
.end method

.method public static a(LX/0eX;D)V
    .locals 9

    .prologue
    .line 1780614
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    .line 1780615
    iget-boolean v7, v0, LX/0eX;->l:Z

    if-nez v7, :cond_0

    cmpl-double v7, v2, v4

    if-eqz v7, :cond_1

    .line 1780616
    :cond_0
    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, LX/0eX;->a(II)V

    invoke-virtual {v0, v2, v3}, LX/0eX;->a(D)V

    .line 1780617
    invoke-static {v0, v1}, LX/0eX;->i(LX/0eX;I)V

    .line 1780618
    :cond_1
    return-void
.end method

.method public static b(LX/0eX;)I
    .locals 1

    .prologue
    .line 1780612
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 1780613
    return v0
.end method

.method public static c(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1780611
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method


# virtual methods
.method public final a(LX/0eW;)LX/0eW;
    .locals 1

    .prologue
    .line 1780610
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0}, LX/0eW;->a(LX/0eW;I)LX/0eW;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/nio/ByteBuffer;)LX/BOy;
    .locals 0

    .prologue
    .line 1780609
    iput p1, p0, LX/BOy;->a:I

    iput-object p2, p0, LX/BOy;->b:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1780608
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()D
    .locals 3

    .prologue
    .line 1780606
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getDouble(I)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final d()B
    .locals 3

    .prologue
    .line 1780605
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
