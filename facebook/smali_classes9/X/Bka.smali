.class public LX/Bka;
.super LX/1a1;
.source ""


# instance fields
.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Landroid/content/Context;

.field public n:Landroid/text/style/MetricAffectingSpan;

.field public o:Landroid/text/style/MetricAffectingSpan;

.field public p:Landroid/view/View$OnClickListener;

.field public q:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

.field public r:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

.field public s:LX/Bim;

.field public t:LX/17Y;

.field public u:Lcom/facebook/content/SecureContextHelper;

.field public v:LX/3GL;


# direct methods
.method public constructor <init>(Lcom/facebook/resources/ui/FbTextView;Ljava/lang/Boolean;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/3GL;)V
    .locals 3
    .param p1    # Lcom/facebook/resources/ui/FbTextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814536
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1814537
    iput-object p1, p0, LX/Bka;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1814538
    iput-object p3, p0, LX/Bka;->t:LX/17Y;

    .line 1814539
    iput-object p4, p0, LX/Bka;->u:Lcom/facebook/content/SecureContextHelper;

    .line 1814540
    iput-object p5, p0, LX/Bka;->v:LX/3GL;

    .line 1814541
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Bka;->m:Landroid/content/Context;

    .line 1814542
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, LX/Bka;->m:Landroid/content/Context;

    const v2, 0x7f0e08b0

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/Bka;->n:Landroid/text/style/MetricAffectingSpan;

    .line 1814543
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, LX/Bka;->m:Landroid/content/Context;

    const v2, 0x7f0e08b1

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/Bka;->o:Landroid/text/style/MetricAffectingSpan;

    .line 1814544
    new-instance v0, LX/BkZ;

    invoke-direct {v0, p0, p2}, LX/BkZ;-><init>(LX/Bka;Ljava/lang/Boolean;)V

    iput-object v0, p0, LX/Bka;->p:Landroid/view/View$OnClickListener;

    .line 1814545
    return-void
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 1814546
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1814547
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1814548
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p2, v0, v1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1814549
    return-void
.end method
