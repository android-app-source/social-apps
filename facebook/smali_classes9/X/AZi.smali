.class public LX/AZi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AZU;


# instance fields
.field public final a:LX/AVP;

.field public final b:LX/AVT;

.field public final c:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View$OnTouchListener;

.field public h:LX/AZe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/AYB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/AVP;LX/AVT;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AVP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1687379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687380
    iput-object p2, p0, LX/AZi;->a:LX/AVP;

    .line 1687381
    iput-object p3, p0, LX/AZi;->b:LX/AVT;

    .line 1687382
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1687383
    const v0, 0x7f0305be

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iput-object v0, p0, LX/AZi;->c:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    .line 1687384
    const v0, 0x7f0305bd

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AZi;->d:Landroid/view/View;

    .line 1687385
    iget-object v0, p0, LX/AZi;->d:Landroid/view/View;

    const v1, 0x7f0d0fce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AZi;->e:Landroid/view/View;

    .line 1687386
    iget-object v0, p0, LX/AZi;->d:Landroid/view/View;

    const v1, 0x7f0d0fcf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AZi;->f:Landroid/view/View;

    .line 1687387
    iget-object v0, p0, LX/AZi;->e:Landroid/view/View;

    new-instance v1, LX/AZf;

    invoke-direct {v1, p0}, LX/AZf;-><init>(LX/AZi;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1687388
    iget-object v0, p0, LX/AZi;->f:Landroid/view/View;

    new-instance v1, LX/AZg;

    invoke-direct {v1, p0}, LX/AZg;-><init>(LX/AZi;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1687389
    new-instance v0, LX/AZh;

    invoke-direct {v0, p0}, LX/AZh;-><init>(LX/AZi;)V

    iput-object v0, p0, LX/AZi;->g:Landroid/view/View$OnTouchListener;

    .line 1687390
    return-void
.end method

.method public static a$redex0(LX/AZi;LX/7S9;)V
    .locals 2

    .prologue
    .line 1687375
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1687376
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1687377
    iget-object v1, p0, LX/AZi;->a:LX/AVP;

    invoke-virtual {v1, v0}, LX/AVP;->a(Ljava/util/List;)V

    .line 1687378
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 4

    .prologue
    .line 1687372
    iget-object v0, p0, LX/AZi;->g:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1687373
    new-instance v0, LX/7S9;

    sget-object v1, LX/7S8;->VIEW_INIT:LX/7S8;

    invoke-virtual {p1}, Lcom/facebook/facecast/FacecastPreviewView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Lcom/facebook/facecast/FacecastPreviewView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v1, v2, v3}, LX/7S9;-><init>(LX/7S8;FF)V

    invoke-static {p0, v0}, LX/AZi;->a$redex0(LX/AZi;LX/7S9;)V

    .line 1687374
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1687365
    iget-object v0, p0, LX/AZi;->c:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    return-object v0
.end method

.method public final b(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 1

    .prologue
    .line 1687367
    iget-object v0, p0, LX/AZi;->g:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->b(Landroid/view/View$OnTouchListener;)V

    .line 1687368
    iget-object v0, p0, LX/AZi;->g:Landroid/view/View$OnTouchListener;

    check-cast v0, LX/AZh;

    .line 1687369
    const/4 p0, -0x1

    iput p0, v0, LX/AZh;->b:I

    .line 1687370
    const/4 p0, 0x0

    iput-object p0, v0, LX/AZh;->c:LX/7S9;

    .line 1687371
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1

    .prologue
    .line 1687366
    iget-object v0, p0, LX/AZi;->d:Landroid/view/View;

    return-object v0
.end method
