.class public LX/At6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# instance fields
.field private final a:LX/86f;

.field private final b:LX/AsL;

.field private c:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;


# direct methods
.method public constructor <init>(LX/AsL;LX/86f;)V
    .locals 0
    .param p1    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1721171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721172
    iput-object p1, p0, LX/At6;->b:LX/AsL;

    .line 1721173
    iput-object p2, p0, LX/At6;->a:LX/86f;

    .line 1721174
    return-void
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1721184
    iget-object v0, p0, LX/At6;->a:LX/86f;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 0

    .prologue
    .line 1721182
    iput-object p1, p0, LX/At6;->c:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1721183
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1721181
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1721180
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1721185
    iget-object v0, p0, LX/At6;->b:LX/AsL;

    iget-object v1, p0, LX/At6;->c:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    .line 1721186
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1721179
    iget-object v0, p0, LX/At6;->c:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721178
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721177
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721176
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721175
    const/4 v0, 0x0

    return-object v0
.end method
