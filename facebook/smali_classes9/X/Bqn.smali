.class public final LX/Bqn;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bqo;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/Bqo;


# direct methods
.method public constructor <init>(LX/Bqo;)V
    .locals 1

    .prologue
    .line 1825245
    iput-object p1, p0, LX/Bqn;->d:LX/Bqo;

    .line 1825246
    move-object v0, p1

    .line 1825247
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1825248
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1825244
    const-string v0, "StoryPromotionComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/Bqo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1825224
    check-cast p1, LX/Bqn;

    .line 1825225
    iget-object v0, p1, LX/Bqn;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Bqn;->c:Ljava/lang/String;

    .line 1825226
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1825230
    if-ne p0, p1, :cond_1

    .line 1825231
    :cond_0
    :goto_0
    return v0

    .line 1825232
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1825233
    goto :goto_0

    .line 1825234
    :cond_3
    check-cast p1, LX/Bqn;

    .line 1825235
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1825236
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1825237
    if-eq v2, v3, :cond_0

    .line 1825238
    iget-object v2, p0, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1825239
    goto :goto_0

    .line 1825240
    :cond_5
    iget-object v2, p1, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1825241
    :cond_6
    iget-object v2, p0, LX/Bqn;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Bqn;->b:LX/1Po;

    iget-object v3, p1, LX/Bqn;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1825242
    goto :goto_0

    .line 1825243
    :cond_7
    iget-object v2, p1, LX/Bqn;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1825227
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Bqn;

    .line 1825228
    const/4 v1, 0x0

    iput-object v1, v0, LX/Bqn;->c:Ljava/lang/String;

    .line 1825229
    return-object v0
.end method
