.class public final LX/Adx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Ae2;


# direct methods
.method public constructor <init>(LX/Ae2;)V
    .locals 0

    .prologue
    .line 1695722
    iput-object p1, p0, LX/Adx;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 1695723
    iget-object v0, p0, LX/Adx;->a:LX/Ae2;

    .line 1695724
    iget-object v1, v0, LX/Ae2;->l:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 1695725
    iget-object v1, v0, LX/Ae2;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2sP;

    invoke-virtual {v1}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    .line 1695726
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1695727
    new-instance v1, LX/5M9;

    invoke-direct {v1}, LX/5M9;-><init>()V

    .line 1695728
    iput-wide v3, v1, LX/5M9;->b:J

    .line 1695729
    move-object v1, v1

    .line 1695730
    iput-wide v3, v1, LX/5M9;->k:J

    .line 1695731
    move-object v3, v1

    .line 1695732
    iget-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 1695733
    iput-object v1, v3, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1695734
    move-object v1, v3

    .line 1695735
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v2

    .line 1695736
    iput-object v2, v1, LX/5M9;->h:Ljava/lang/String;

    .line 1695737
    move-object v1, v1

    .line 1695738
    iput-object v5, v1, LX/5M9;->G:Ljava/lang/String;

    .line 1695739
    move-object v1, v1

    .line 1695740
    sget-object v2, LX/2rt;->SHARE:LX/2rt;

    .line 1695741
    iput-object v2, v1, LX/5M9;->q:LX/2rt;

    .line 1695742
    move-object v1, v1

    .line 1695743
    invoke-virtual {v1}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v1

    .line 1695744
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1695745
    const-string v3, "publishPostParams"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1695746
    const-string v1, "quick_share_tapped"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695747
    iget-object v1, v0, LX/Ae2;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    new-instance v3, LX/Adm;

    invoke-direct {v3, v0}, LX/Adm;-><init>(LX/Ae2;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1695748
    const/4 v0, 0x1

    return v0
.end method
