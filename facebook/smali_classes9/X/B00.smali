.class public final LX/B00;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OZ;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;)V
    .locals 0

    .prologue
    .line 1733187
    iput-object p1, p0, LX/B00;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 1733188
    iget-object v0, p0, LX/B00;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    invoke-virtual {v0, p3}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733189
    iget-object v0, p0, LX/B00;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a()V

    .line 1733190
    :goto_0
    return-void

    .line 1733191
    :cond_0
    iget-object v0, p0, LX/B00;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    .line 1733192
    iget-object p0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    invoke-virtual {p0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/media/util/model/MediaModel;

    .line 1733193
    const p1, 0x7f0d1490

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 1733194
    iget-object p4, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {p4, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_2

    .line 1733195
    iget-object p4, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {p4, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1733196
    const p0, 0x7f020157

    invoke-virtual {p1, p0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1733197
    :goto_1
    iget-object p0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->d:LX/B01;

    if-eqz p0, :cond_1

    .line 1733198
    iget-object p0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->d:LX/B01;

    iget-object p1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, LX/B01;->a(I)V

    .line 1733199
    :cond_1
    goto :goto_0

    .line 1733200
    :cond_2
    iget-object p4, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {p4, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1733201
    const p0, 0x7f02003f

    invoke-virtual {p1, p0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
