.class public final enum LX/Btp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Btp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Btp;

.field public static final enum ENDSCREEN:LX/Btp;

.field public static final enum PAUSESCREEN:LX/Btp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1829539
    new-instance v0, LX/Btp;

    const-string v1, "ENDSCREEN"

    invoke-direct {v0, v1, v2}, LX/Btp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Btp;->ENDSCREEN:LX/Btp;

    .line 1829540
    new-instance v0, LX/Btp;

    const-string v1, "PAUSESCREEN"

    invoke-direct {v0, v1, v3}, LX/Btp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Btp;->PAUSESCREEN:LX/Btp;

    .line 1829541
    const/4 v0, 0x2

    new-array v0, v0, [LX/Btp;

    sget-object v1, LX/Btp;->ENDSCREEN:LX/Btp;

    aput-object v1, v0, v2

    sget-object v1, LX/Btp;->PAUSESCREEN:LX/Btp;

    aput-object v1, v0, v3

    sput-object v0, LX/Btp;->$VALUES:[LX/Btp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1829542
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Btp;
    .locals 1

    .prologue
    .line 1829543
    const-class v0, LX/Btp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Btp;

    return-object v0
.end method

.method public static values()[LX/Btp;
    .locals 1

    .prologue
    .line 1829538
    sget-object v0, LX/Btp;->$VALUES:[LX/Btp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Btp;

    return-object v0
.end method
