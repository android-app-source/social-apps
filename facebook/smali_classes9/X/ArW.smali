.class public final LX/ArW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9cu;


# instance fields
.field public final synthetic a:LX/ArZ;


# direct methods
.method public constructor <init>(LX/ArZ;)V
    .locals 0

    .prologue
    .line 1719294
    iput-object p1, p0, LX/ArW;->a:LX/ArZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1719271
    return-void
.end method

.method public final a(LX/5jK;LX/5jJ;)V
    .locals 2

    .prologue
    .line 1719273
    sget-object v0, LX/ArY;->a:[I

    invoke-virtual {p2}, LX/5jJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1719274
    :cond_0
    :goto_0
    return-void

    .line 1719275
    :pswitch_0
    invoke-virtual {p1}, LX/5jK;->h()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/5jK;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1719276
    :cond_1
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1719277
    iget-object v0, p0, LX/ArW;->a:LX/ArZ;

    .line 1719278
    iget-object v1, v0, LX/ArZ;->e:LX/0Px;

    iget-object p0, v0, LX/ArZ;->h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    .line 1719279
    iget-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object p0, v0, LX/ArZ;->f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719280
    iget-object p0, v0, LX/ArZ;->h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719281
    iput-object v1, v0, LX/ArZ;->h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719282
    invoke-static {v0}, LX/ArZ;->e(LX/ArZ;)V

    .line 1719283
    goto :goto_0

    .line 1719284
    :cond_2
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1719285
    iget-object v0, p0, LX/ArW;->a:LX/ArZ;

    .line 1719286
    iget-object v1, v0, LX/ArZ;->e:LX/0Px;

    iget-object p0, v0, LX/ArZ;->f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    .line 1719287
    iget-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object p0, v0, LX/ArZ;->h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719288
    iget-object p0, v0, LX/ArZ;->f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719289
    iput-object v1, v0, LX/ArZ;->f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719290
    invoke-static {v0}, LX/ArZ;->e(LX/ArZ;)V

    .line 1719291
    goto :goto_0

    .line 1719292
    :cond_3
    invoke-virtual {p1}, LX/5jK;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1719293
    iget-object v0, p0, LX/ArW;->a:LX/ArZ;

    invoke-static {v0}, LX/ArZ;->e(LX/ArZ;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1719272
    return-void
.end method
