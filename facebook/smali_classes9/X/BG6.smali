.class public final LX/BG6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766571
    iput-object p1, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1766572
    iget-object v0, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->v:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 1766573
    iget-object v0, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    .line 1766574
    :cond_0
    iget-object v0, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Jvd;->a(Z)V

    .line 1766575
    sget-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v1, "Error stopping the recording"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766576
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1766577
    iget-object v0, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v1, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->v:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1766578
    iput-object v1, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->y:Landroid/net/Uri;

    .line 1766579
    iget-object v0, p0, LX/BG6;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-static {v0}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->m(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    .line 1766580
    return-void
.end method
