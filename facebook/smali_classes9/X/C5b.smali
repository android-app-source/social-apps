.class public final enum LX/C5b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C5b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C5b;

.field public static final enum HIDE_PIVOT:LX/C5b;

.field public static final enum OPEN_POLITICIAN_PAGE:LX/C5b;

.field public static final enum RENDER:LX/C5b;

.field public static final enum SEE_POSITION_CLICK:LX/C5b;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1849128
    new-instance v0, LX/C5b;

    const-string v1, "RENDER"

    invoke-direct {v0, v1, v2}, LX/C5b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5b;->RENDER:LX/C5b;

    .line 1849129
    new-instance v0, LX/C5b;

    const-string v1, "SEE_POSITION_CLICK"

    invoke-direct {v0, v1, v3}, LX/C5b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5b;->SEE_POSITION_CLICK:LX/C5b;

    .line 1849130
    new-instance v0, LX/C5b;

    const-string v1, "HIDE_PIVOT"

    invoke-direct {v0, v1, v4}, LX/C5b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5b;->HIDE_PIVOT:LX/C5b;

    .line 1849131
    new-instance v0, LX/C5b;

    const-string v1, "OPEN_POLITICIAN_PAGE"

    invoke-direct {v0, v1, v5}, LX/C5b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5b;->OPEN_POLITICIAN_PAGE:LX/C5b;

    .line 1849132
    const/4 v0, 0x4

    new-array v0, v0, [LX/C5b;

    sget-object v1, LX/C5b;->RENDER:LX/C5b;

    aput-object v1, v0, v2

    sget-object v1, LX/C5b;->SEE_POSITION_CLICK:LX/C5b;

    aput-object v1, v0, v3

    sget-object v1, LX/C5b;->HIDE_PIVOT:LX/C5b;

    aput-object v1, v0, v4

    sget-object v1, LX/C5b;->OPEN_POLITICIAN_PAGE:LX/C5b;

    aput-object v1, v0, v5

    sput-object v0, LX/C5b;->$VALUES:[LX/C5b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1849133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C5b;
    .locals 1

    .prologue
    .line 1849134
    const-class v0, LX/C5b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C5b;

    return-object v0
.end method

.method public static values()[LX/C5b;
    .locals 1

    .prologue
    .line 1849135
    sget-object v0, LX/C5b;->$VALUES:[LX/C5b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C5b;

    return-object v0
.end method
