.class public LX/Bh5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;",
        "LX/97f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/Bg8;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1808781
    const-class v0, LX/Bh5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bh5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/Bg8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808778
    iput-object p1, p0, LX/Bh5;->c:LX/03V;

    .line 1808779
    iput-object p2, p0, LX/Bh5;->b:LX/Bg8;

    .line 1808780
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)LX/97f;
    .locals 6

    .prologue
    .line 1808756
    invoke-static {p1}, LX/Bgb;->j(LX/97f;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getValues()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1808757
    :goto_0
    return-object p1

    .line 1808758
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getValues()LX/0Px;

    move-result-object v0

    .line 1808759
    invoke-static {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v3

    .line 1808760
    if-nez v3, :cond_1

    .line 1808761
    :goto_1
    move-object p1, p1

    .line 1808762
    invoke-interface {p2, p1}, LX/BgS;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1808763
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1808764
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1808765
    new-instance p0, LX/97m;

    invoke-direct {p0}, LX/97m;-><init>()V

    .line 1808766
    iput-object v1, p0, LX/97m;->m:Ljava/lang/String;

    .line 1808767
    move-object v1, p0

    .line 1808768
    invoke-virtual {v1}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1808769
    new-instance p0, LX/97l;

    invoke-direct {p0}, LX/97l;-><init>()V

    .line 1808770
    iput-object v1, p0, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1808771
    move-object v1, p0

    .line 1808772
    invoke-virtual {v1}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v1

    .line 1808773
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1808774
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1808775
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1808776
    invoke-static {v3, v1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object p1

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/Bh5;
    .locals 3

    .prologue
    .line 1808754
    new-instance v2, LX/Bh5;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/Bg8;->b(LX/0QB;)LX/Bg8;

    move-result-object v1

    check-cast v1, LX/Bg8;

    invoke-direct {v2, v0, v1}, LX/Bh5;-><init>(LX/03V;LX/Bg8;)V

    .line 1808755
    return-object v2
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808753
    sget-object v0, LX/BeE;->MULTI_TEXT_FIELD:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808751
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808752
    const v1, 0x7f031421

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808714
    check-cast p1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    check-cast p2, LX/97f;

    invoke-static {p1, p2, p3}, LX/Bh5;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)LX/97f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808715
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    move-object v2, p2

    check-cast v2, LX/97f;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1808716
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a()V

    .line 1808717
    iget-object v6, v0, LX/Bh5;->b:LX/Bg8;

    .line 1808718
    iget-object p0, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object p0, p0

    .line 1808719
    iget-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    move-object p0, p1

    .line 1808720
    new-instance p1, LX/Bh0;

    invoke-direct {p1, v0, v1, v2, v3}, LX/Bh0;-><init>(LX/Bh5;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)V

    invoke-virtual {v6, p0, v2, v3, p1}, LX/Bg8;->a(Landroid/view/View;LX/97f;LX/BgS;LX/Bh0;)Landroid/view/View$OnClickListener;

    move-result-object p4

    .line 1808721
    sget-object v6, LX/Bh4;->a:[I

    invoke-interface {v2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ordinal()I

    move-result p0

    aget v6, v6, p0

    packed-switch v6, :pswitch_data_0

    .line 1808722
    iget-object v6, v0, LX/Bh5;->c:LX/03V;

    sget-object p0, LX/Bh5;->a:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "Trying to bind view with unsupported option selected: "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808723
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v6, v0

    move-object p0, v1

    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    .line 1808724
    invoke-static {p1}, LX/Bgb;->j(LX/97f;)LX/0Px;

    move-result-object p7

    .line 1808725
    iget-object p5, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object p6, p5

    .line 1808726
    invoke-virtual {p7}, LX/0Px;->isEmpty()Z

    move-result p5

    if-nez p5, :cond_2

    const/4 p5, 0x0

    invoke-virtual {p7, p5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/String;

    :goto_1
    invoke-virtual {p6, p5}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808727
    invoke-static {p1}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p6, p5}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1808728
    invoke-interface {p1}, LX/97e;->d()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p6, p5}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808729
    new-instance p5, LX/Bh1;

    invoke-direct {p5, v6, p3}, LX/Bh1;-><init>(LX/Bh5;LX/BgK;)V

    invoke-virtual {p6, p5}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808730
    invoke-virtual {p6, p4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808731
    const/4 p5, 0x1

    move p6, p5

    :goto_2
    invoke-virtual {p7}, LX/0Px;->size()I

    move-result p5

    if-ge p6, p5, :cond_3

    .line 1808732
    invoke-virtual {p7, p6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/String;

    .line 1808733
    new-instance p8, LX/Bh2;

    invoke-direct {p8, v6, p2, p1, p6}, LX/Bh2;-><init>(LX/Bh5;LX/BgS;LX/97f;I)V

    invoke-static {p1}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p5, p8, p3, v4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;LX/BgK;Ljava/lang/String;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object p8

    .line 1808734
    invoke-static {p5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p5

    if-eqz p5, :cond_1

    .line 1808735
    iget-object p5, p8, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    move-object p5, p5

    .line 1808736
    invoke-virtual {p5}, Landroid/widget/EditText;->requestFocus()Z

    .line 1808737
    :cond_1
    add-int/lit8 p5, p6, 0x1

    move p6, p5

    goto :goto_2

    .line 1808738
    :cond_2
    const-string p5, ""

    goto :goto_1

    .line 1808739
    :cond_3
    :goto_3
    if-eqz v5, :cond_0

    .line 1808740
    new-instance v6, LX/Bh3;

    invoke-direct {v6, v0, v1, v2, v3}, LX/Bh3;-><init>(LX/Bh5;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)V

    invoke-virtual {v1, v6}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->setOnFocusChangeListener(LX/Bgk;)V

    .line 1808741
    goto :goto_0

    .line 1808742
    :pswitch_1
    invoke-static {v2}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v6

    .line 1808743
    iget-object p0, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object p0, p0

    .line 1808744
    if-nez v6, :cond_4

    const-string v6, ""

    :goto_4
    invoke-virtual {p0, v6}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808745
    iget-object v6, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object v6, v6

    .line 1808746
    invoke-static {v2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1808747
    iget-object v6, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object v6, v6

    .line 1808748
    invoke-virtual {v6, p4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808749
    goto :goto_3

    .line 1808750
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->a()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
