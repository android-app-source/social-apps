.class public LX/BtK;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final c:Landroid/content/Context;

.field public d:LX/5OM;

.field public e:Landroid/view/View;

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1828976
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 1828977
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BtK;->f:Z

    .line 1828978
    iput-object p1, p0, LX/BtK;->a:LX/0Ot;

    .line 1828979
    iput-object p2, p0, LX/BtK;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1828980
    iput-object p3, p0, LX/BtK;->c:Landroid/content/Context;

    .line 1828981
    return-void
.end method


# virtual methods
.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1828982
    iget-object v0, p0, LX/BtK;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1828983
    iget-object v0, p0, LX/BtK;->d:LX/5OM;

    if-eqz v0, :cond_1

    .line 1828984
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BtK;->f:Z

    .line 1828985
    iget-object v0, p0, LX/BtK;->d:LX/5OM;

    iget-object v1, p0, LX/BtK;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1828986
    :cond_0
    return-void

    .line 1828987
    :cond_1
    new-instance v0, LX/6WS;

    iget-object v1, p0, LX/BtK;->c:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {v0, v1, p1}, LX/6WS;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/BtK;->d:LX/5OM;

    .line 1828988
    new-instance v0, LX/5OG;

    iget-object v1, p0, LX/BtK;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 1828989
    iget-object v1, p0, LX/BtK;->d:LX/5OM;

    invoke-virtual {v1, v0}, LX/5OM;->a(LX/5OG;)V

    .line 1828990
    const v1, 0x7f081081

    invoke-virtual {v0, v1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1828991
    new-instance v1, LX/BtJ;

    invoke-direct {v1, p0}, LX/BtJ;-><init>(LX/BtK;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method
