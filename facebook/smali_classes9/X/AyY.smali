.class public final LX/AyY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/Aya;

.field private final b:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Aya;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1730125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730126
    iput-object p1, p0, LX/AyY;->a:LX/Aya;

    .line 1730127
    iput-object p2, p0, LX/AyY;->b:LX/1Nq;

    .line 1730128
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 3
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1730129
    iget-object v1, p0, LX/AyY;->a:LX/Aya;

    iget-object v0, p0, LX/AyY;->b:LX/1Nq;

    const-class v2, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

    invoke-virtual {v0, p1, v2}, LX/1Nq;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

    .line 1730130
    new-instance p1, LX/AyZ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/BMQ;->b(LX/0QB;)LX/BMQ;

    move-result-object p0

    check-cast p0, LX/BMQ;

    invoke-direct {p1, p2, v0, v2, p0}, LX/AyZ;-><init>(LX/B5j;Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;Landroid/content/Context;LX/BMQ;)V

    .line 1730131
    move-object v0, p1

    .line 1730132
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1730133
    const-string v0, "SOUVENIR_PROMPT_PERSISTENCE_KEY"

    return-object v0
.end method
