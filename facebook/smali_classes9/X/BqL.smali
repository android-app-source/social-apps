.class public LX/BqL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1824571
    iput-object p1, p0, LX/BqL;->a:LX/03V;

    .line 1824572
    iput-object p2, p0, LX/BqL;->b:LX/0tX;

    .line 1824573
    return-void
.end method

.method public static a(LX/0QB;)LX/BqL;
    .locals 5

    .prologue
    .line 1824574
    const-class v1, LX/BqL;

    monitor-enter v1

    .line 1824575
    :try_start_0
    sget-object v0, LX/BqL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824576
    sput-object v2, LX/BqL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824577
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824578
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824579
    new-instance p0, LX/BqL;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/BqL;-><init>(LX/03V;LX/0tX;)V

    .line 1824580
    move-object v0, p0

    .line 1824581
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824582
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BqL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824583
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BqL;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AYMTTipEventType;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1824588
    invoke-static {p1}, LX/BqL;->g(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1824589
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1824590
    const-string v0, "AYMT Channel id is null or empty"

    invoke-virtual {p0, p2, v0}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824591
    const/4 v0, 0x0

    .line 1824592
    :goto_0
    move v0, v0

    .line 1824593
    if-eqz v0, :cond_0

    .line 1824594
    invoke-static {p1}, LX/BqL;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1824595
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1824596
    const-string v0, "AYMT Tip id is null or empty"

    invoke-virtual {p0, p2, v0}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824597
    const/4 v0, 0x0

    .line 1824598
    :goto_1
    move v0, v0

    .line 1824599
    if-nez v0, :cond_1

    .line 1824600
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cannot log impression: channel id = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/BqL;->g(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tip id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, LX/BqL;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824601
    :goto_2
    return-void

    .line 1824602
    :cond_1
    new-instance v0, LX/40m;

    invoke-direct {v0}, LX/40m;-><init>()V

    move-object v0, v0

    .line 1824603
    new-instance v1, LX/4D5;

    invoke-direct {v1}, LX/4D5;-><init>()V

    invoke-static {p1}, LX/BqL;->g(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4D5;->a(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/4D5;->b(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    invoke-static {p1}, LX/BqL;->f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4D5;->c(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/4D5;->d(Ljava/lang/String;)LX/4D5;

    move-result-object v1

    .line 1824604
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1824605
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1824606
    iget-object v1, p0, LX/BqL;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1824607
    new-instance v1, LX/BqK;

    invoke-direct {v1, p0, p2, p3}, LX/BqK;-><init>(LX/BqL;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1824585
    invoke-static {p0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824535
    invoke-static {p0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v0

    .line 1824536
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824586
    invoke-static {p0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v0

    .line 1824587
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->q()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824566
    invoke-static {p0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v0

    .line 1824567
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824568
    invoke-static {p0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v0

    .line 1824569
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824564
    invoke-static {p0}, LX/BqL;->i(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    .line 1824565
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1824558
    invoke-static {p0}, LX/BqL;->i(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v1

    .line 1824559
    if-nez v1, :cond_1

    .line 1824560
    :cond_0
    :goto_0
    return-object v0

    .line 1824561
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v1

    .line 1824562
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1824563
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    goto :goto_0
.end method

.method private static i(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1824556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v0

    .line 1824557
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->a()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1824554
    iget-object v0, p0, LX/BqL;->a:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824555
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1824549
    invoke-static {p1}, LX/BqL;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1824550
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824551
    const-string v0, "AYMT Tip\'s titleText is empty or null"

    invoke-virtual {p0, p2, v0}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824552
    const/4 v0, 0x0

    .line 1824553
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1824544
    invoke-static {p1}, LX/BqL;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1824545
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824546
    const-string v0, "AYMT Tip\'s subtitleText is empty or null"

    invoke-virtual {p0, p2, v0}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824547
    const/4 v0, 0x0

    .line 1824548
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1824537
    invoke-static {p1}, LX/BqL;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1824538
    if-nez v1, :cond_0

    .line 1824539
    const-string v1, "image is null"

    invoke-virtual {p0, p2, v1}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824540
    :goto_0
    return v0

    .line 1824541
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1824542
    const-string v1, "AYMT Tip\'s image uri is empty or null"

    invoke-virtual {p0, p2, v1}, LX/BqL;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1824543
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
