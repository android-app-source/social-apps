.class public LX/Bmi;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bmf;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bmk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1818895
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bmi;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bmk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1818892
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1818893
    iput-object p1, p0, LX/Bmi;->b:LX/0Ot;

    .line 1818894
    return-void
.end method

.method public static a(LX/0QB;)LX/Bmi;
    .locals 4

    .prologue
    .line 1818881
    const-class v1, LX/Bmi;

    monitor-enter v1

    .line 1818882
    :try_start_0
    sget-object v0, LX/Bmi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1818883
    sput-object v2, LX/Bmi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1818884
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1818885
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1818886
    new-instance v3, LX/Bmi;

    const/16 p0, 0x1bb2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bmi;-><init>(LX/0Ot;)V

    .line 1818887
    move-object v0, v3

    .line 1818888
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1818889
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bmi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1818890
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1818891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;Z)V
    .locals 3

    .prologue
    .line 1818896
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1818897
    if-nez v0, :cond_0

    .line 1818898
    :goto_0
    return-void

    .line 1818899
    :cond_0
    check-cast v0, LX/Bmg;

    .line 1818900
    new-instance v1, LX/Bmh;

    iget-object v2, v0, LX/Bmg;->g:LX/Bmi;

    invoke-direct {v1, v2, p1}, LX/Bmh;-><init>(LX/Bmi;Z)V

    move-object v0, v1

    .line 1818901
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1818846
    invoke-static {}, LX/1dS;->b()V

    .line 1818847
    iget v0, p1, LX/BcQ;->b:I

    .line 1818848
    sparse-switch v0, :sswitch_data_0

    move-object v0, v6

    .line 1818849
    :goto_0
    return-object v0

    .line 1818850
    :sswitch_0
    check-cast p2, LX/BcM;

    .line 1818851
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    move-object v0, p0

    .line 1818852
    iget-object p0, v0, LX/Bmi;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1, v2, v3, v4}, LX/Bmk;->a(ZLX/BcL;Ljava/lang/Throwable;LX/BcP;)V

    .line 1818853
    move-object v0, v6

    .line 1818854
    goto :goto_0

    .line 1818855
    :sswitch_1
    check-cast p2, LX/BdG;

    .line 1818856
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v4

    check-cast v0, LX/BcP;

    iget-object v3, p1, LX/BcQ;->a:LX/BcO;

    .line 1818857
    check-cast v3, LX/Bmg;

    .line 1818858
    iget-object v4, p0, LX/Bmi;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bmk;

    iget v5, v3, LX/Bmg;->d:I

    .line 1818859
    iget-object v6, v4, LX/Bmk;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bme;

    const/4 p0, 0x0

    .line 1818860
    new-instance v3, LX/Bmd;

    invoke-direct {v3, v6}, LX/Bmd;-><init>(LX/Bme;)V

    .line 1818861
    sget-object v4, LX/Bme;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bmc;

    .line 1818862
    if-nez v4, :cond_0

    .line 1818863
    new-instance v4, LX/Bmc;

    invoke-direct {v4}, LX/Bmc;-><init>()V

    .line 1818864
    :cond_0
    invoke-static {v4, v0, p0, p0, v3}, LX/Bmc;->a$redex0(LX/Bmc;LX/1De;IILX/Bmd;)V

    .line 1818865
    move-object v3, v4

    .line 1818866
    move-object p0, v3

    .line 1818867
    move-object v6, p0

    .line 1818868
    check-cast v2, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1818869
    iget-object p0, v6, LX/Bmc;->a:LX/Bmd;

    iput-object v2, p0, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1818870
    iget-object p0, v6, LX/Bmc;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 1818871
    move-object v6, v6

    .line 1818872
    iget-object p0, v6, LX/Bmc;->a:LX/Bmd;

    iput v1, p0, LX/Bmd;->b:I

    .line 1818873
    iget-object p0, v6, LX/Bmc;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 1818874
    move-object v6, v6

    .line 1818875
    iget-object p0, v6, LX/Bmc;->a:LX/Bmd;

    iput v5, p0, LX/Bmd;->c:I

    .line 1818876
    iget-object p0, v6, LX/Bmc;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 1818877
    move-object v6, v6

    .line 1818878
    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    move-object v4, v6

    .line 1818879
    move-object v0, v4

    .line 1818880
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x74deba50 -> :sswitch_0
        0x612025 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1818842
    check-cast p1, LX/Bmg;

    .line 1818843
    check-cast p2, LX/Bmg;

    .line 1818844
    iget-boolean v0, p1, LX/Bmg;->b:Z

    iput-boolean v0, p2, LX/Bmg;->b:Z

    .line 1818845
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 7

    .prologue
    .line 1818832
    check-cast p3, LX/Bmg;

    .line 1818833
    iget-object v0, p0, LX/Bmi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bmk;

    iget-boolean v3, p3, LX/Bmg;->b:Z

    iget-object v4, p3, LX/Bmg;->c:LX/1rs;

    iget v5, p3, LX/Bmg;->e:I

    iget-object v6, p3, LX/Bmg;->f:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    .line 1818834
    iget-object p0, v0, LX/Bmk;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bcw;

    invoke-virtual {p0, v1}, LX/Bcw;->c(LX/BcP;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/Bct;->b(Ljava/lang/String;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/Bct;->a(LX/1rs;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/Bct;->a(I)LX/Bct;

    move-result-object p0

    .line 1818835
    const p1, -0x74deba50

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    invoke-static {v1, p1, v0}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 1818836
    invoke-virtual {p0, p1}, LX/Bct;->c(LX/BcQ;)LX/Bct;

    move-result-object p0

    .line 1818837
    const p1, 0x612025

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v0, v4

    invoke-static {v1, p1, v0}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 1818838
    invoke-virtual {p0, p1}, LX/Bct;->b(LX/BcQ;)LX/Bct;

    move-result-object p0

    invoke-virtual {p0}, LX/Bct;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1818839
    if-eqz v3, :cond_0

    .line 1818840
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    invoke-static {v1}, LX/Bdj;->c(LX/1De;)LX/Bdh;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1818841
    :cond_0
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 1818824
    check-cast p2, LX/Bmg;

    .line 1818825
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1818826
    iget-object v1, p0, LX/Bmi;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1818827
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1818828
    iput-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1818829
    iget-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 1818830
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bmg;->b:Z

    .line 1818831
    return-void
.end method
