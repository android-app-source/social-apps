.class public LX/Bij;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bii;


# instance fields
.field public final a:Landroid/view/inputmethod/InputMethodManager;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810748
    const/4 v0, 0x0

    iput v0, p0, LX/Bij;->b:I

    .line 1810749
    iput-object p1, p0, LX/Bij;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 1810750
    return-void
.end method

.method public static a(LX/0QB;)LX/Bij;
    .locals 2

    .prologue
    .line 1810751
    new-instance v1, LX/Bij;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v1, v0}, LX/Bij;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    .line 1810752
    move-object v0, v1

    .line 1810753
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/create/ui/EventNameEditText;)V
    .locals 1

    .prologue
    .line 1810754
    iget v0, p0, LX/Bij;->b:I

    packed-switch v0, :pswitch_data_0

    .line 1810755
    :goto_0
    return-void

    .line 1810756
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, LX/Bij;->b:I

    goto :goto_0

    .line 1810757
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, LX/Bij;->b:I

    goto :goto_0

    .line 1810758
    :pswitch_2
    invoke-virtual {p0, p1}, LX/Bij;->b(Lcom/facebook/events/create/ui/EventNameEditText;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/facebook/events/create/ui/EventNameEditText;)V
    .locals 1

    .prologue
    .line 1810759
    const/4 v0, 0x0

    .line 1810760
    iput-object v0, p1, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    .line 1810761
    new-instance v0, Lcom/facebook/events/create/EventCreationAnimationHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/events/create/EventCreationAnimationHelper$1;-><init>(LX/Bij;Lcom/facebook/events/create/ui/EventNameEditText;)V

    invoke-virtual {p1, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->post(Ljava/lang/Runnable;)Z

    .line 1810762
    return-void
.end method
