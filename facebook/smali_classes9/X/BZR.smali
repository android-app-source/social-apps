.class public LX/BZR;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/BZS;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public final h:Landroid/content/Context;

.field public final i:Ljava/util/concurrent/Executor;

.field public j:LX/BZP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1797262
    const-class v0, LX/BZR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BZR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/BZS;Ljava/util/concurrent/Executor;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1797263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1797264
    iput-object p1, p0, LX/BZR;->b:LX/BZS;

    .line 1797265
    iput-object p2, p0, LX/BZR;->i:Ljava/util/concurrent/Executor;

    .line 1797266
    iput-object p3, p0, LX/BZR;->h:Landroid/content/Context;

    .line 1797267
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 1797268
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BZR;->g:Z

    .line 1797269
    iget-object v0, p0, LX/BZR;->b:LX/BZS;

    .line 1797270
    invoke-static {v0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->releaseModels(J)V

    .line 1797271
    return-void
.end method
