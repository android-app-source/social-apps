.class public final LX/CVz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1906454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static C(LX/186;LX/15i;I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1906377
    if-nez p2, :cond_0

    .line 1906378
    :goto_0
    return v2

    .line 1906379
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v4

    .line 1906380
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v7, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v5

    .line 1906381
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;

    invoke-virtual {p1, p2, v8, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 1906382
    :goto_1
    if-eqz v3, :cond_3

    .line 1906383
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [I

    move v1, v2

    .line 1906384
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1906385
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;)I

    move-result v0

    aput v0, v6, v1

    .line 1906386
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1906387
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1906388
    move-object v3, v0

    goto :goto_1

    .line 1906389
    :cond_2
    invoke-virtual {p0, v6, v7}, LX/186;->a([IZ)I

    move-result v0

    .line 1906390
    :goto_3
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 1906391
    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1906392
    invoke-virtual {p0, v7, v5}, LX/186;->b(II)V

    .line 1906393
    invoke-virtual {p0, v8, v0}, LX/186;->b(II)V

    .line 1906394
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906395
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method private static D(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1906396
    if-nez p2, :cond_0

    .line 1906397
    :goto_0
    return v1

    .line 1906398
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906399
    invoke-virtual {p1, p2, v1}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1906400
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906401
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static E(LX/186;LX/15i;I)I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1906402
    if-nez p2, :cond_0

    move v0, v1

    .line 1906403
    :goto_0
    return v0

    .line 1906404
    :cond_0
    invoke-virtual {p1, p2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906405
    invoke-virtual {p1, p2, v7}, LX/15i;->n(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, LX/186;->a(Ljava/util/List;)I

    move-result v0

    .line 1906406
    const/4 v3, 0x5

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906407
    invoke-virtual {p1, p2, v1}, LX/15i;->h(II)Z

    move-result v3

    invoke-virtual {p0, v1, v3}, LX/186;->a(IZ)V

    .line 1906408
    invoke-virtual {p1, p2, v4}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {p0, v4, v3, v1}, LX/186;->a(III)V

    .line 1906409
    invoke-virtual {p1, p2, v5}, LX/15i;->h(II)Z

    move-result v1

    invoke-virtual {p0, v5, v1}, LX/186;->a(IZ)V

    .line 1906410
    invoke-virtual {p0, v6, v2}, LX/186;->b(II)V

    .line 1906411
    invoke-virtual {p0, v7, v0}, LX/186;->b(II)V

    .line 1906412
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906413
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906414
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1906415
    goto :goto_1
.end method

.method private static F(LX/186;LX/15i;I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1906416
    if-nez p2, :cond_0

    move v0, v1

    .line 1906417
    :goto_0
    return v0

    .line 1906418
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)I

    move-result v0

    .line 1906419
    invoke-virtual {p1, p2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906420
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906421
    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 1906422
    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1906423
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906424
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;LX/15i;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1906425
    if-nez p2, :cond_0

    .line 1906426
    :goto_0
    return v0

    .line 1906427
    :cond_0
    invoke-virtual {p1, p2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906428
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906429
    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1906430
    invoke-virtual {p0, v3, v1}, LX/186;->b(II)V

    .line 1906431
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906432
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;LX/CUx;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1906433
    if-nez p1, :cond_0

    .line 1906434
    :goto_0
    return v0

    .line 1906435
    :cond_0
    invoke-interface {p1}, LX/CUw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906436
    invoke-interface {p1}, LX/CUw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906437
    invoke-interface {p1}, LX/CUw;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906438
    invoke-interface {p1}, LX/CUx;->jR_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-static {p0, v5, v4}, LX/CVz;->t(LX/186;LX/15i;I)I

    move-result v4

    .line 1906439
    invoke-interface {p1}, LX/CUx;->j()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-static {p0, v6, v5}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v5

    .line 1906440
    invoke-interface {p1}, LX/CUw;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1906441
    invoke-interface {p1}, LX/CUw;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1906442
    invoke-interface {p1}, LX/CUw;->jS_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1906443
    const/16 v9, 0x8

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1906444
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906445
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1906446
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1906447
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1906448
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1906449
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1906450
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1906451
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1906452
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906453
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;LX/CUy;)I
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1906299
    if-nez p1, :cond_0

    .line 1906300
    :goto_0
    return v2

    .line 1906301
    :cond_0
    invoke-interface {p1}, LX/CUy;->a()LX/0Px;

    move-result-object v3

    .line 1906302
    if-eqz v3, :cond_4

    .line 1906303
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1906304
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1906305
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    const/4 v5, 0x0

    .line 1906306
    if-nez v0, :cond_5

    .line 1906307
    :goto_2
    move v0, v5

    .line 1906308
    aput v0, v4, v1

    .line 1906309
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1906310
    :cond_1
    invoke-virtual {p0, v4, v7}, LX/186;->a([IZ)I

    move-result v0

    move v1, v0

    .line 1906311
    :goto_3
    invoke-interface {p1}, LX/CUy;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1906312
    invoke-interface {p1}, LX/CUy;->c()LX/0Px;

    move-result-object v5

    .line 1906313
    if-eqz v5, :cond_3

    .line 1906314
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [I

    move v3, v2

    .line 1906315
    :goto_4
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1906316
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    const/4 v8, 0x0

    .line 1906317
    if-nez v0, :cond_6

    .line 1906318
    :goto_5
    move v0, v8

    .line 1906319
    aput v0, v6, v3

    .line 1906320
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1906321
    :cond_2
    invoke-virtual {p0, v6, v7}, LX/186;->a([IZ)I

    move-result v0

    .line 1906322
    :goto_6
    invoke-interface {p1}, LX/CUy;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906323
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906324
    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1906325
    invoke-virtual {p0, v7, v4}, LX/186;->b(II)V

    .line 1906326
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 1906327
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1906328
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906329
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_6

    :cond_4
    move v1, v2

    goto :goto_3

    .line 1906330
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1906331
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1906332
    const/4 v9, 0x4

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1906333
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1906334
    const/4 v6, 0x1

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->b()I

    move-result v9

    invoke-virtual {p0, v6, v9, v5}, LX/186;->a(III)V

    .line 1906335
    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->c()I

    move-result v9

    invoke-virtual {p0, v6, v9, v5}, LX/186;->a(III)V

    .line 1906336
    const/4 v5, 0x3

    invoke-virtual {p0, v5, v8}, LX/186;->b(II)V

    .line 1906337
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1906338
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1906339
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1906340
    const/4 v10, 0x3

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1906341
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->a()I

    move-result v10

    invoke-virtual {p0, v8, v10, v8}, LX/186;->a(III)V

    .line 1906342
    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->b()I

    move-result v11

    invoke-virtual {p0, v10, v11, v8}, LX/186;->a(III)V

    .line 1906343
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1906344
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1906345
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto/16 :goto_5
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;)I
    .locals 48

    .prologue
    .line 1906455
    if-nez p1, :cond_0

    .line 1906456
    const/4 v3, 0x0

    .line 1906457
    :goto_0
    return v3

    .line 1906458
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 1906459
    const/4 v3, 0x0

    .line 1906460
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->C()LX/0Px;

    move-result-object v5

    .line 1906461
    if-eqz v5, :cond_e

    .line 1906462
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    new-array v6, v3, [I

    .line 1906463
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 1906464
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)I

    move-result v3

    aput v3, v6, v4

    .line 1906465
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1906466
    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v4, v3

    .line 1906467
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->D()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->b(LX/186;LX/15i;I)I

    move-result v11

    .line 1906468
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->E()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->d(LX/186;LX/15i;I)I

    move-result v12

    .line 1906469
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->F()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->e(LX/186;LX/15i;I)I

    move-result v13

    .line 1906470
    const/4 v3, 0x0

    .line 1906471
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->G()LX/2uF;

    move-result-object v5

    .line 1906472
    if-eqz v5, :cond_d

    .line 1906473
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v3

    new-array v6, v3, [I

    .line 1906474
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 1906475
    invoke-virtual {v5, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v9, v7, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, LX/CVz;->g(LX/186;LX/15i;I)I

    move-result v7

    aput v7, v6, v3

    .line 1906476
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1906477
    :cond_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v5, v3

    .line 1906478
    :goto_4
    const/4 v3, 0x0

    .line 1906479
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->H()LX/2uF;

    move-result-object v6

    .line 1906480
    if-eqz v6, :cond_c

    .line 1906481
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v3

    new-array v7, v3, [I

    .line 1906482
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 1906483
    invoke-virtual {v6, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v14, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v14}, LX/CVz;->h(LX/186;LX/15i;I)I

    move-result v8

    aput v8, v7, v3

    .line 1906484
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1906485
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, LX/186;->a([IZ)I

    move-result v3

    move v6, v3

    .line 1906486
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1906487
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->I()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->k(LX/186;LX/15i;I)I

    move-result v15

    .line 1906488
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->J()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->l(LX/186;LX/15i;I)I

    move-result v16

    .line 1906489
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->e()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;)I

    move-result v17

    .line 1906490
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->jO_()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;)I

    move-result v18

    .line 1906491
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1906492
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->K()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->m(LX/186;LX/15i;I)I

    move-result v20

    .line 1906493
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->L()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->p(LX/186;LX/15i;I)I

    move-result v21

    .line 1906494
    const/4 v3, 0x0

    .line 1906495
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->M()LX/2uF;

    move-result-object v7

    .line 1906496
    if-eqz v7, :cond_b

    .line 1906497
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v3

    new-array v8, v3, [I

    .line 1906498
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v9

    if-ge v3, v9, :cond_4

    .line 1906499
    invoke-virtual {v7, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v9

    iget-object v0, v9, LX/1vs;->a:LX/15i;

    move-object/from16 v22, v0

    iget v0, v9, LX/1vs;->b:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/CVz;->q(LX/186;LX/15i;I)I

    move-result v9

    aput v9, v8, v3

    .line 1906500
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1906501
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v3}, LX/186;->a([IZ)I

    move-result v3

    move v7, v3

    .line 1906502
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->N()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v22

    .line 1906503
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->k()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 1906504
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1906505
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1906506
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 1906507
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->O()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->s(LX/186;LX/15i;I)I

    move-result v27

    .line 1906508
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1906509
    const/4 v3, 0x0

    .line 1906510
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->P()LX/0Px;

    move-result-object v9

    .line 1906511
    if-eqz v9, :cond_6

    .line 1906512
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    new-array v0, v3, [I

    move-object/from16 v29, v0

    .line 1906513
    const/4 v3, 0x0

    move v8, v3

    :goto_9
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    if-ge v8, v3, :cond_5

    .line 1906514
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)I

    move-result v3

    aput v3, v29, v8

    .line 1906515
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_9

    .line 1906516
    :cond_5
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3}, LX/186;->a([IZ)I

    move-result v3

    .line 1906517
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->t()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 1906518
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->Q()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->F(LX/186;LX/15i;I)I

    move-result v30

    .line 1906519
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->u()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 1906520
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->R()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->f(LX/186;LX/15i;I)I

    move-result v32

    .line 1906521
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->S()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->c(LX/186;LX/15i;I)I

    move-result v33

    .line 1906522
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->v()LX/CUy;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v34

    .line 1906523
    const/4 v8, 0x0

    .line 1906524
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->T()LX/2uF;

    move-result-object v9

    .line 1906525
    if-eqz v9, :cond_8

    .line 1906526
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v8

    new-array v0, v8, [I

    move-object/from16 v35, v0

    .line 1906527
    const/4 v8, 0x0

    :goto_a
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v36

    move/from16 v0, v36

    if-ge v8, v0, :cond_7

    .line 1906528
    invoke-virtual {v9, v8}, LX/2uF;->a(I)LX/1vs;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v37, v0

    move-object/from16 v0, v36

    iget v0, v0, LX/1vs;->b:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move/from16 v2, v38

    invoke-static {v0, v1, v2}, LX/CVz;->u(LX/186;LX/15i;I)I

    move-result v36

    aput v36, v35, v8

    .line 1906529
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 1906530
    :cond_7
    const/4 v8, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v8}, LX/186;->a([IZ)I

    move-result v8

    .line 1906531
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->U()LX/CUx;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/CVz;->a(LX/186;LX/CUx;)I

    move-result v35

    .line 1906532
    const/4 v9, 0x0

    .line 1906533
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->V()LX/2uF;

    move-result-object v36

    .line 1906534
    if-eqz v36, :cond_a

    .line 1906535
    invoke-virtual/range {v36 .. v36}, LX/39O;->c()I

    move-result v9

    new-array v0, v9, [I

    move-object/from16 v37, v0

    .line 1906536
    const/4 v9, 0x0

    :goto_b
    invoke-virtual/range {v36 .. v36}, LX/39O;->c()I

    move-result v38

    move/from16 v0, v38

    if-ge v9, v0, :cond_9

    .line 1906537
    move-object/from16 v0, v36

    invoke-virtual {v0, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v38

    move-object/from16 v0, v38

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v39, v0

    move-object/from16 v0, v38

    iget v0, v0, LX/1vs;->b:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-static {v0, v1, v2}, LX/CVz;->v(LX/186;LX/15i;I)I

    move-result v38

    aput v38, v37, v9

    .line 1906538
    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    .line 1906539
    :cond_9
    const/4 v9, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1, v9}, LX/186;->a([IZ)I

    move-result v9

    .line 1906540
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->w()LX/CUy;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v36

    .line 1906541
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v37

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v37

    .line 1906542
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->W()LX/1vs;

    move-result-object v38

    move-object/from16 v0, v38

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v39, v0

    move-object/from16 v0, v38

    iget v0, v0, LX/1vs;->b:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move/from16 v2, v38

    invoke-static {v0, v1, v2}, LX/CVz;->w(LX/186;LX/15i;I)I

    move-result v38

    .line 1906543
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v39

    .line 1906544
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->X()LX/1vs;

    move-result-object v40

    move-object/from16 v0, v40

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v41, v0

    move-object/from16 v0, v40

    iget v0, v0, LX/1vs;->b:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move/from16 v2, v40

    invoke-static {v0, v1, v2}, LX/CVz;->y(LX/186;LX/15i;I)I

    move-result v40

    .line 1906545
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->Y()LX/1vs;

    move-result-object v41

    move-object/from16 v0, v41

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v42, v0

    move-object/from16 v0, v41

    iget v0, v0, LX/1vs;->b:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, LX/CVz;->z(LX/186;LX/15i;I)I

    move-result v41

    .line 1906546
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->A()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v42

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;)I

    move-result v42

    .line 1906547
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->Z()LX/1vs;

    move-result-object v43

    move-object/from16 v0, v43

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    iget v0, v0, LX/1vs;->b:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move/from16 v2, v43

    invoke-static {v0, v1, v2}, LX/CVz;->C(LX/186;LX/15i;I)I

    move-result v43

    .line 1906548
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->aa()LX/1vs;

    move-result-object v44

    move-object/from16 v0, v44

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v45, v0

    move-object/from16 v0, v44

    iget v0, v0, LX/1vs;->b:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, LX/CVz;->D(LX/186;LX/15i;I)I

    move-result v44

    .line 1906549
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->ab()LX/1vs;

    move-result-object v45

    move-object/from16 v0, v45

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v46, v0

    move-object/from16 v0, v45

    iget v0, v0, LX/1vs;->b:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, LX/CVz;->E(LX/186;LX/15i;I)I

    move-result v45

    .line 1906550
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->B()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    .line 1906551
    const/16 v47, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1906552
    const/16 v47, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1906553
    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v4}, LX/186;->b(II)V

    .line 1906554
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1906555
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1906556
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1906557
    const/4 v4, 0x5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->c()Z

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 1906558
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1906559
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1906560
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1906561
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1906562
    const/16 v4, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906563
    const/16 v4, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906564
    const/16 v4, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906565
    const/16 v4, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->jN_()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906566
    const/16 v4, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906567
    const/16 v4, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906568
    const/16 v4, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906569
    const/16 v4, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1906570
    const/16 v4, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906571
    const/16 v4, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906572
    const/16 v4, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906573
    const/16 v4, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906574
    const/16 v4, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906575
    const/16 v4, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906576
    const/16 v4, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906577
    const/16 v4, 0x1a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->p()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906578
    const/16 v4, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->q()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906579
    const/16 v4, 0x1c

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->r()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906580
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1906581
    const/16 v3, 0x1e

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->s()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1906582
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906583
    const/16 v3, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906584
    const/16 v3, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906585
    const/16 v3, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906586
    const/16 v3, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906587
    const/16 v3, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906588
    const/16 v3, 0x25

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1906589
    const/16 v3, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906590
    const/16 v3, 0x27

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1906591
    const/16 v3, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906592
    const/16 v3, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906593
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906594
    const/16 v3, 0x2b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;->y()Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1906595
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906596
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906597
    const/16 v3, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906598
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906599
    const/16 v3, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906600
    const/16 v3, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906601
    const/16 v3, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906602
    const/16 v3, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906603
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v3

    .line 1906604
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_b
    move v7, v3

    goto/16 :goto_8

    :cond_c
    move v6, v3

    goto/16 :goto_6

    :cond_d
    move v5, v3

    goto/16 :goto_4

    :cond_e
    move v4, v3

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1906605
    if-nez p1, :cond_0

    .line 1906606
    :goto_0
    return v1

    .line 1906607
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906608
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1906609
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906610
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)I
    .locals 47

    .prologue
    .line 1906641
    if-nez p1, :cond_0

    .line 1906642
    const/4 v3, 0x0

    .line 1906643
    :goto_0
    return v3

    .line 1906644
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 1906645
    const/4 v3, 0x0

    .line 1906646
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C()LX/0Px;

    move-result-object v5

    .line 1906647
    if-eqz v5, :cond_e

    .line 1906648
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    new-array v6, v3, [I

    .line 1906649
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 1906650
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)I

    move-result v3

    aput v3, v6, v4

    .line 1906651
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1906652
    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v4, v3

    .line 1906653
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->b(LX/186;LX/15i;I)I

    move-result v11

    .line 1906654
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->d(LX/186;LX/15i;I)I

    move-result v12

    .line 1906655
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->e(LX/186;LX/15i;I)I

    move-result v13

    .line 1906656
    const/4 v3, 0x0

    .line 1906657
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G()LX/2uF;

    move-result-object v5

    .line 1906658
    if-eqz v5, :cond_d

    .line 1906659
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v3

    new-array v6, v3, [I

    .line 1906660
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 1906661
    invoke-virtual {v5, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v9, v7, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, LX/CVz;->g(LX/186;LX/15i;I)I

    move-result v7

    aput v7, v6, v3

    .line 1906662
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1906663
    :cond_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v5, v3

    .line 1906664
    :goto_4
    const/4 v3, 0x0

    .line 1906665
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H()LX/2uF;

    move-result-object v6

    .line 1906666
    if-eqz v6, :cond_c

    .line 1906667
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v3

    new-array v7, v3, [I

    .line 1906668
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 1906669
    invoke-virtual {v6, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v14, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v14}, LX/CVz;->h(LX/186;LX/15i;I)I

    move-result v8

    aput v8, v7, v3

    .line 1906670
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1906671
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, LX/186;->a([IZ)I

    move-result v3

    move v6, v3

    .line 1906672
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1906673
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->k(LX/186;LX/15i;I)I

    move-result v15

    .line 1906674
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->l(LX/186;LX/15i;I)I

    move-result v16

    .line 1906675
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->e()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;)I

    move-result v17

    .line 1906676
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->jQ_()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;)I

    move-result v18

    .line 1906677
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1906678
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->m(LX/186;LX/15i;I)I

    move-result v20

    .line 1906679
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->p(LX/186;LX/15i;I)I

    move-result v21

    .line 1906680
    const/4 v3, 0x0

    .line 1906681
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M()LX/2uF;

    move-result-object v7

    .line 1906682
    if-eqz v7, :cond_b

    .line 1906683
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v3

    new-array v8, v3, [I

    .line 1906684
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v9

    if-ge v3, v9, :cond_4

    .line 1906685
    invoke-virtual {v7, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v9

    iget-object v0, v9, LX/1vs;->a:LX/15i;

    move-object/from16 v22, v0

    iget v0, v9, LX/1vs;->b:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/CVz;->q(LX/186;LX/15i;I)I

    move-result v9

    aput v9, v8, v3

    .line 1906686
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1906687
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v3}, LX/186;->a([IZ)I

    move-result v3

    move v7, v3

    .line 1906688
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v22

    .line 1906689
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 1906690
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1906691
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1906692
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 1906693
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->s(LX/186;LX/15i;I)I

    move-result v27

    .line 1906694
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1906695
    const/4 v3, 0x0

    .line 1906696
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P()LX/0Px;

    move-result-object v9

    .line 1906697
    if-eqz v9, :cond_6

    .line 1906698
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    new-array v0, v3, [I

    move-object/from16 v29, v0

    .line 1906699
    const/4 v3, 0x0

    move v8, v3

    :goto_9
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    if-ge v8, v3, :cond_5

    .line 1906700
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)I

    move-result v3

    aput v3, v29, v8

    .line 1906701
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_9

    .line 1906702
    :cond_5
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3}, LX/186;->a([IZ)I

    move-result v3

    .line 1906703
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 1906704
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 1906705
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->f(LX/186;LX/15i;I)I

    move-result v31

    .line 1906706
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->c(LX/186;LX/15i;I)I

    move-result v32

    .line 1906707
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v()LX/CUy;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v33

    .line 1906708
    const/4 v8, 0x0

    .line 1906709
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S()LX/2uF;

    move-result-object v9

    .line 1906710
    if-eqz v9, :cond_8

    .line 1906711
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v8

    new-array v0, v8, [I

    move-object/from16 v34, v0

    .line 1906712
    const/4 v8, 0x0

    :goto_a
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v35

    move/from16 v0, v35

    if-ge v8, v0, :cond_7

    .line 1906713
    invoke-virtual {v9, v8}, LX/2uF;->a(I)LX/1vs;

    move-result-object v35

    move-object/from16 v0, v35

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    iget v0, v0, LX/1vs;->b:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-static {v0, v1, v2}, LX/CVz;->u(LX/186;LX/15i;I)I

    move-result v35

    aput v35, v34, v8

    .line 1906714
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 1906715
    :cond_7
    const/4 v8, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v8}, LX/186;->a([IZ)I

    move-result v8

    .line 1906716
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->T()LX/CUx;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/CVz;->a(LX/186;LX/CUx;)I

    move-result v34

    .line 1906717
    const/4 v9, 0x0

    .line 1906718
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U()LX/2uF;

    move-result-object v35

    .line 1906719
    if-eqz v35, :cond_a

    .line 1906720
    invoke-virtual/range {v35 .. v35}, LX/39O;->c()I

    move-result v9

    new-array v0, v9, [I

    move-object/from16 v36, v0

    .line 1906721
    const/4 v9, 0x0

    :goto_b
    invoke-virtual/range {v35 .. v35}, LX/39O;->c()I

    move-result v37

    move/from16 v0, v37

    if-ge v9, v0, :cond_9

    .line 1906722
    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v38, v0

    move-object/from16 v0, v37

    iget v0, v0, LX/1vs;->b:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, LX/CVz;->v(LX/186;LX/15i;I)I

    move-result v37

    aput v37, v36, v9

    .line 1906723
    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    .line 1906724
    :cond_9
    const/4 v9, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1, v9}, LX/186;->a([IZ)I

    move-result v9

    .line 1906725
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->w()LX/CUy;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v35

    .line 1906726
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1906727
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v38, v0

    move-object/from16 v0, v37

    iget v0, v0, LX/1vs;->b:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v37

    invoke-static {v0, v1, v2}, LX/CVz;->w(LX/186;LX/15i;I)I

    move-result v37

    .line 1906728
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v38

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1906729
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v39

    move-object/from16 v0, v39

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v40, v0

    move-object/from16 v0, v39

    iget v0, v0, LX/1vs;->b:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, LX/CVz;->y(LX/186;LX/15i;I)I

    move-result v39

    .line 1906730
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v40

    move-object/from16 v0, v40

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v41, v0

    move-object/from16 v0, v40

    iget v0, v0, LX/1vs;->b:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move/from16 v2, v40

    invoke-static {v0, v1, v2}, LX/CVz;->z(LX/186;LX/15i;I)I

    move-result v40

    .line 1906731
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->A()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;)I

    move-result v41

    .line 1906732
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v42

    move-object/from16 v0, v42

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    iget v0, v0, LX/1vs;->b:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move/from16 v2, v42

    invoke-static {v0, v1, v2}, LX/CVz;->C(LX/186;LX/15i;I)I

    move-result v42

    .line 1906733
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v43

    move-object/from16 v0, v43

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    iget v0, v0, LX/1vs;->b:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move/from16 v2, v43

    invoke-static {v0, v1, v2}, LX/CVz;->D(LX/186;LX/15i;I)I

    move-result v43

    .line 1906734
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v44

    move-object/from16 v0, v44

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v45, v0

    move-object/from16 v0, v44

    iget v0, v0, LX/1vs;->b:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, LX/CVz;->E(LX/186;LX/15i;I)I

    move-result v44

    .line 1906735
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 1906736
    const/16 v46, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1906737
    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1906738
    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v4}, LX/186;->b(II)V

    .line 1906739
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1906740
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1906741
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1906742
    const/4 v4, 0x5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->c()Z

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 1906743
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1906744
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1906745
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1906746
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1906747
    const/16 v4, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906748
    const/16 v4, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906749
    const/16 v4, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906750
    const/16 v4, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->jP_()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906751
    const/16 v4, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906752
    const/16 v4, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906753
    const/16 v4, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906754
    const/16 v4, 0x11

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1906755
    const/16 v4, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906756
    const/16 v4, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906757
    const/16 v4, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906758
    const/16 v4, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906759
    const/16 v4, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906760
    const/16 v4, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906761
    const/16 v4, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906762
    const/16 v4, 0x19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906763
    const/16 v4, 0x1a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906764
    const/16 v4, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->r()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906765
    const/16 v4, 0x1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1906766
    const/16 v3, 0x1d

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->s()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1906767
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906768
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906769
    const/16 v3, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906770
    const/16 v3, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906771
    const/16 v3, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906772
    const/16 v3, 0x23

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1906773
    const/16 v3, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906774
    const/16 v3, 0x25

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1906775
    const/16 v3, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906776
    const/16 v3, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906777
    const/16 v3, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906778
    const/16 v3, 0x29

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->y()Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1906779
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906780
    const/16 v3, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906781
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906782
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906783
    const/16 v3, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906784
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906785
    const/16 v3, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906786
    const/16 v3, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906787
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v3

    .line 1906788
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_b
    move v7, v3

    goto/16 :goto_8

    :cond_c
    move v6, v3

    goto/16 :goto_6

    :cond_d
    move v5, v3

    goto/16 :goto_4

    :cond_e
    move v4, v3

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;)I
    .locals 13

    .prologue
    .line 1906611
    if-nez p1, :cond_0

    .line 1906612
    const/4 v0, 0x0

    .line 1906613
    :goto_0
    return v0

    .line 1906614
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1906615
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906616
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906617
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906618
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->jT_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906619
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->jU_()LX/0Px;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 1906620
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1906621
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1906622
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1906623
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1906624
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1906625
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1906626
    const/16 v12, 0xd

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1906627
    const/4 v12, 0x1

    invoke-virtual {p0, v12, v0}, LX/186;->b(II)V

    .line 1906628
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906629
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1906630
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1906631
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1906632
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1906633
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1906634
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1906635
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1906636
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1906637
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1906638
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 1906639
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906640
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1906939
    if-nez p1, :cond_0

    .line 1906940
    :goto_0
    return v0

    .line 1906941
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906942
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906943
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906944
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906945
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)I
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v0, 0x0

    .line 1906906
    if-nez p1, :cond_0

    .line 1906907
    :goto_0
    return v0

    .line 1906908
    :cond_0
    invoke-interface {p1}, LX/CUw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906909
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906910
    invoke-interface {p1}, LX/CUw;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906911
    invoke-interface {p1}, LX/CUw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906912
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->k()LX/0Px;

    move-result-object v6

    .line 1906913
    if-eqz v6, :cond_2

    .line 1906914
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v1, v0

    .line 1906915
    :goto_1
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1906916
    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CUx;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUx;)I

    move-result v0

    aput v0, v7, v1

    .line 1906917
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1906918
    :cond_1
    invoke-virtual {p0, v7, v12}, LX/186;->a([IZ)I

    move-result v0

    .line 1906919
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->jR_()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {p0, v6, v1}, LX/CVz;->t(LX/186;LX/15i;I)I

    move-result v1

    .line 1906920
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->j()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-static {p0, v7, v6}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v6

    .line 1906921
    invoke-interface {p1}, LX/CUw;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1906922
    invoke-interface {p1}, LX/CUw;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1906923
    invoke-interface {p1}, LX/CUw;->jS_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1906924
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1906925
    const/16 v11, 0xc

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1906926
    invoke-virtual {p0, v12, v2}, LX/186;->b(II)V

    .line 1906927
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906928
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1906929
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1906930
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1906931
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906932
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1906933
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1906934
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1906935
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1906936
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1906937
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906938
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1906873
    if-nez p1, :cond_0

    .line 1906874
    :goto_0
    return v1

    .line 1906875
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;->b()LX/2uF;

    move-result-object v2

    .line 1906876
    if-eqz v2, :cond_2

    .line 1906877
    invoke-virtual {v2}, LX/39O;->c()I

    move-result v0

    new-array v3, v0, [I

    move v0, v1

    .line 1906878
    :goto_1
    invoke-virtual {v2}, LX/39O;->c()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 1906879
    invoke-virtual {v2, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v6, v4, LX/1vs;->b:I

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 1906880
    if-nez v6, :cond_3

    .line 1906881
    :goto_2
    move v4, v4

    .line 1906882
    aput v4, v3, v0

    .line 1906883
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1906884
    :cond_1
    invoke-virtual {p0, v3, v7}, LX/186;->a([IZ)I

    move-result v0

    .line 1906885
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;->a()LX/CUy;

    move-result-object v2

    invoke-static {p0, v2}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v2

    .line 1906886
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906887
    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 1906888
    invoke-virtual {p0, v7, v2}, LX/186;->b(II)V

    .line 1906889
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906890
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_3

    .line 1906891
    :cond_3
    invoke-virtual {v5, v6, v11}, LX/15i;->g(II)I

    move-result v8

    const/4 v9, 0x0

    .line 1906892
    if-nez v8, :cond_4

    .line 1906893
    :goto_4
    move v8, v9

    .line 1906894
    invoke-virtual {v5, v6, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1906895
    const/4 v10, 0x3

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1906896
    invoke-virtual {v5, v6, v4}, LX/15i;->h(II)Z

    move-result v10

    invoke-virtual {p0, v4, v10}, LX/186;->a(IZ)V

    .line 1906897
    invoke-virtual {p0, v11, v8}, LX/186;->b(II)V

    .line 1906898
    invoke-virtual {p0, v12, v9}, LX/186;->b(II)V

    .line 1906899
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1906900
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_2

    .line 1906901
    :cond_4
    invoke-virtual {v5, v8, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1906902
    const/4 v13, 0x1

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1906903
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 1906904
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1906905
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1906946
    if-nez p1, :cond_0

    .line 1906947
    :goto_0
    return v0

    .line 1906948
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {p0, v2, v1}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v1

    .line 1906949
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906950
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906951
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906952
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1906953
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906954
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;)LX/CUx;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPagesPlatformProductFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1906861
    if-nez p0, :cond_1

    .line 1906862
    :cond_0
    :goto_0
    return-object v2

    .line 1906863
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1906864
    invoke-static {v0, p0}, LX/CVz;->b(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;)I

    move-result v1

    .line 1906865
    if-eqz v1, :cond_0

    .line 1906866
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1906867
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1906868
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1906869
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1906870
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1906871
    const-string v1, "PagesPlatformModelConversionHelper.getPagesPlatformProductFragment"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1906872
    :cond_2
    new-instance v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)LX/CUx;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPagesPlatformProductFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1906828
    if-nez p0, :cond_1

    .line 1906829
    :cond_0
    :goto_0
    return-object v2

    .line 1906830
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1906831
    const/4 v1, 0x0

    .line 1906832
    if-nez p0, :cond_3

    .line 1906833
    :goto_1
    move v1, v1

    .line 1906834
    if-eqz v1, :cond_0

    .line 1906835
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1906836
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1906837
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1906838
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1906839
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1906840
    const-string v1, "PagesPlatformModelConversionHelper.getPagesPlatformProductFragment"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1906841
    :cond_2
    new-instance v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1906842
    :cond_3
    invoke-interface {p0}, LX/CUw;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906843
    invoke-interface {p0}, LX/CUw;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906844
    invoke-interface {p0}, LX/CUw;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906845
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->jR_()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-static {v0, v7, v6}, LX/CVz;->t(LX/186;LX/15i;I)I

    move-result v6

    .line 1906846
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;->j()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    invoke-static {v0, v8, v7}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v7

    .line 1906847
    invoke-interface {p0}, LX/CUw;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1906848
    invoke-interface {p0}, LX/CUw;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1906849
    invoke-interface {p0}, LX/CUw;->jS_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1906850
    const/16 v11, 0x8

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1906851
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1906852
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1906853
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1906854
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1906855
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1906856
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1906857
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1906858
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1906859
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1906860
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1906801
    if-nez p0, :cond_1

    .line 1906802
    :cond_0
    :goto_0
    return-object v2

    .line 1906803
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1906804
    const/4 v1, 0x0

    .line 1906805
    if-nez p0, :cond_3

    .line 1906806
    :goto_1
    move v1, v1

    .line 1906807
    if-eqz v1, :cond_0

    .line 1906808
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1906809
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1906810
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1906811
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1906812
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1906813
    const-string v1, "PagesPlatformModelConversionHelper.getPagesPlatformEventListenersFragmentEventHandler"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1906814
    :cond_2
    new-instance v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1906815
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1906816
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906817
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1906818
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1906819
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;->e()LX/0Px;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 1906820
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1906821
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1906822
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1906823
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1906824
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1906825
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1906826
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1906827
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getScreenElementFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1906789
    if-nez p0, :cond_1

    .line 1906790
    :cond_0
    :goto_0
    return-object v2

    .line 1906791
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1906792
    invoke-static {v0, p0}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;)I

    move-result v1

    .line 1906793
    if-eqz v1, :cond_0

    .line 1906794
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1906795
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1906796
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1906797
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1906798
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1906799
    const-string v1, "PagesPlatformModelConversionHelper.getScreenElementFragment"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1906800
    :cond_2
    new-instance v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getScreenElementFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1906346
    if-nez p0, :cond_1

    .line 1906347
    :cond_0
    :goto_0
    return-object v2

    .line 1906348
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1906349
    invoke-static {v0, p0}, LX/CVz;->b(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)I

    move-result v1

    .line 1906350
    if-eqz v1, :cond_0

    .line 1906351
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1906352
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1906353
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1906354
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1906355
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1906356
    const-string v1, "PagesPlatformModelConversionHelper.getScreenElementFragment"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1906357
    :cond_2
    new-instance v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-direct {v2, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method private static b(LX/186;LX/15i;I)I
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 1906358
    if-nez p2, :cond_0

    .line 1906359
    :goto_0
    return v0

    .line 1906360
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906361
    invoke-virtual {p1, p2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906362
    invoke-virtual {p1, p2, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906363
    invoke-virtual {p1, p2, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906364
    invoke-virtual {p1, p2, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906365
    const/4 v6, 0x5

    invoke-virtual {p1, p2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1906366
    const/4 v7, 0x6

    invoke-virtual {p1, p2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1906367
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1906368
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906369
    invoke-virtual {p0, v9, v2}, LX/186;->b(II)V

    .line 1906370
    invoke-virtual {p0, v10, v3}, LX/186;->b(II)V

    .line 1906371
    invoke-virtual {p0, v11, v4}, LX/186;->b(II)V

    .line 1906372
    invoke-virtual {p0, v12, v5}, LX/186;->b(II)V

    .line 1906373
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1906374
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1906375
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906376
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;)I
    .locals 47

    .prologue
    .line 1905882
    if-nez p1, :cond_0

    .line 1905883
    const/4 v3, 0x0

    .line 1905884
    :goto_0
    return v3

    .line 1905885
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 1905886
    const/4 v3, 0x0

    .line 1905887
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C()LX/0Px;

    move-result-object v5

    .line 1905888
    if-eqz v5, :cond_e

    .line 1905889
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    new-array v6, v3, [I

    .line 1905890
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 1905891
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)I

    move-result v3

    aput v3, v6, v4

    .line 1905892
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1905893
    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v4, v3

    .line 1905894
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->b(LX/186;LX/15i;I)I

    move-result v11

    .line 1905895
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->d(LX/186;LX/15i;I)I

    move-result v12

    .line 1905896
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v5, v3}, LX/CVz;->e(LX/186;LX/15i;I)I

    move-result v13

    .line 1905897
    const/4 v3, 0x0

    .line 1905898
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G()LX/2uF;

    move-result-object v5

    .line 1905899
    if-eqz v5, :cond_d

    .line 1905900
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v3

    new-array v6, v3, [I

    .line 1905901
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v5}, LX/39O;->c()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 1905902
    invoke-virtual {v5, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v9, v7, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, LX/CVz;->g(LX/186;LX/15i;I)I

    move-result v7

    aput v7, v6, v3

    .line 1905903
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1905904
    :cond_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v3}, LX/186;->a([IZ)I

    move-result v3

    move v5, v3

    .line 1905905
    :goto_4
    const/4 v3, 0x0

    .line 1905906
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H()LX/2uF;

    move-result-object v6

    .line 1905907
    if-eqz v6, :cond_c

    .line 1905908
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v3

    new-array v7, v3, [I

    .line 1905909
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v6}, LX/39O;->c()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 1905910
    invoke-virtual {v6, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v14, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v14}, LX/CVz;->h(LX/186;LX/15i;I)I

    move-result v8

    aput v8, v7, v3

    .line 1905911
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1905912
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, LX/186;->a([IZ)I

    move-result v3

    move v6, v3

    .line 1905913
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1905914
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->k(LX/186;LX/15i;I)I

    move-result v15

    .line 1905915
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->l(LX/186;LX/15i;I)I

    move-result v16

    .line 1905916
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->e()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;)I

    move-result v17

    .line 1905917
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->jQ_()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;)I

    move-result v18

    .line 1905918
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1905919
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->m(LX/186;LX/15i;I)I

    move-result v20

    .line 1905920
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v7, v3}, LX/CVz;->p(LX/186;LX/15i;I)I

    move-result v21

    .line 1905921
    const/4 v3, 0x0

    .line 1905922
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M()LX/2uF;

    move-result-object v7

    .line 1905923
    if-eqz v7, :cond_b

    .line 1905924
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v3

    new-array v8, v3, [I

    .line 1905925
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v7}, LX/39O;->c()I

    move-result v9

    if-ge v3, v9, :cond_4

    .line 1905926
    invoke-virtual {v7, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v9

    iget-object v0, v9, LX/1vs;->a:LX/15i;

    move-object/from16 v22, v0

    iget v0, v9, LX/1vs;->b:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/CVz;->q(LX/186;LX/15i;I)I

    move-result v9

    aput v9, v8, v3

    .line 1905927
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1905928
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v3}, LX/186;->a([IZ)I

    move-result v3

    move v7, v3

    .line 1905929
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v22

    .line 1905930
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v23

    .line 1905931
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1905932
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    .line 1905933
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 1905934
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v3}, LX/CVz;->s(LX/186;LX/15i;I)I

    move-result v27

    .line 1905935
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1905936
    const/4 v3, 0x0

    .line 1905937
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P()LX/0Px;

    move-result-object v9

    .line 1905938
    if-eqz v9, :cond_6

    .line 1905939
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    new-array v0, v3, [I

    move-object/from16 v29, v0

    .line 1905940
    const/4 v3, 0x0

    move v8, v3

    :goto_9
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v3

    if-ge v8, v3, :cond_5

    .line 1905941
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)I

    move-result v3

    aput v3, v29, v8

    .line 1905942
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_9

    .line 1905943
    :cond_5
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v3}, LX/186;->a([IZ)I

    move-result v3

    .line 1905944
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 1905945
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 1905946
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->f(LX/186;LX/15i;I)I

    move-result v31

    .line 1905947
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    move-object/from16 v0, p0

    invoke-static {v0, v9, v8}, LX/CVz;->c(LX/186;LX/15i;I)I

    move-result v32

    .line 1905948
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v()LX/CUy;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v33

    .line 1905949
    const/4 v8, 0x0

    .line 1905950
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S()LX/2uF;

    move-result-object v9

    .line 1905951
    if-eqz v9, :cond_8

    .line 1905952
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v8

    new-array v0, v8, [I

    move-object/from16 v34, v0

    .line 1905953
    const/4 v8, 0x0

    :goto_a
    invoke-virtual {v9}, LX/39O;->c()I

    move-result v35

    move/from16 v0, v35

    if-ge v8, v0, :cond_7

    .line 1905954
    invoke-virtual {v9, v8}, LX/2uF;->a(I)LX/1vs;

    move-result-object v35

    move-object/from16 v0, v35

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    iget v0, v0, LX/1vs;->b:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-static {v0, v1, v2}, LX/CVz;->u(LX/186;LX/15i;I)I

    move-result v35

    aput v35, v34, v8

    .line 1905955
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 1905956
    :cond_7
    const/4 v8, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v8}, LX/186;->a([IZ)I

    move-result v8

    .line 1905957
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->T()LX/CUx;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/CVz;->a(LX/186;LX/CUx;)I

    move-result v34

    .line 1905958
    const/4 v9, 0x0

    .line 1905959
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U()LX/2uF;

    move-result-object v35

    .line 1905960
    if-eqz v35, :cond_a

    .line 1905961
    invoke-virtual/range {v35 .. v35}, LX/39O;->c()I

    move-result v9

    new-array v0, v9, [I

    move-object/from16 v36, v0

    .line 1905962
    const/4 v9, 0x0

    :goto_b
    invoke-virtual/range {v35 .. v35}, LX/39O;->c()I

    move-result v37

    move/from16 v0, v37

    if-ge v9, v0, :cond_9

    .line 1905963
    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v38, v0

    move-object/from16 v0, v37

    iget v0, v0, LX/1vs;->b:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, LX/CVz;->v(LX/186;LX/15i;I)I

    move-result v37

    aput v37, v36, v9

    .line 1905964
    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    .line 1905965
    :cond_9
    const/4 v9, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1, v9}, LX/186;->a([IZ)I

    move-result v9

    .line 1905966
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->w()LX/CUy;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v35

    .line 1905967
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1905968
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v38, v0

    move-object/from16 v0, v37

    iget v0, v0, LX/1vs;->b:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v37

    invoke-static {v0, v1, v2}, LX/CVz;->w(LX/186;LX/15i;I)I

    move-result v37

    .line 1905969
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v38

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1905970
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v39

    move-object/from16 v0, v39

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v40, v0

    move-object/from16 v0, v39

    iget v0, v0, LX/1vs;->b:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, LX/CVz;->y(LX/186;LX/15i;I)I

    move-result v39

    .line 1905971
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v40

    move-object/from16 v0, v40

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v41, v0

    move-object/from16 v0, v40

    iget v0, v0, LX/1vs;->b:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    move/from16 v2, v40

    invoke-static {v0, v1, v2}, LX/CVz;->z(LX/186;LX/15i;I)I

    move-result v40

    .line 1905972
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->A()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/CVz;->a(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;)I

    move-result v41

    .line 1905973
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v42

    move-object/from16 v0, v42

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    iget v0, v0, LX/1vs;->b:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move/from16 v2, v42

    invoke-static {v0, v1, v2}, LX/CVz;->C(LX/186;LX/15i;I)I

    move-result v42

    .line 1905974
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v43

    move-object/from16 v0, v43

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    iget v0, v0, LX/1vs;->b:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move/from16 v2, v43

    invoke-static {v0, v1, v2}, LX/CVz;->D(LX/186;LX/15i;I)I

    move-result v43

    .line 1905975
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v44

    move-object/from16 v0, v44

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v45, v0

    move-object/from16 v0, v44

    iget v0, v0, LX/1vs;->b:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, LX/CVz;->E(LX/186;LX/15i;I)I

    move-result v44

    .line 1905976
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 1905977
    const/16 v46, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1905978
    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1905979
    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v4}, LX/186;->b(II)V

    .line 1905980
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1905981
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1905982
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1905983
    const/4 v4, 0x5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->c()Z

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 1905984
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1905985
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1905986
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1905987
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1905988
    const/16 v4, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905989
    const/16 v4, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905990
    const/16 v4, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905991
    const/16 v4, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->jP_()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1905992
    const/16 v4, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905993
    const/16 v4, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905994
    const/16 v4, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905995
    const/16 v4, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1905996
    const/16 v4, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905997
    const/16 v4, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905998
    const/16 v4, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1905999
    const/16 v4, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906000
    const/16 v4, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906001
    const/16 v4, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906002
    const/16 v4, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1906003
    const/16 v4, 0x1a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906004
    const/16 v4, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906005
    const/16 v4, 0x1c

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->r()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1906006
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1906007
    const/16 v3, 0x1e

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->s()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1906008
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906009
    const/16 v3, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906010
    const/16 v3, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906011
    const/16 v3, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906012
    const/16 v3, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906013
    const/16 v3, 0x25

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1906014
    const/16 v3, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906015
    const/16 v3, 0x27

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1906016
    const/16 v3, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906017
    const/16 v3, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906018
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906019
    const/16 v3, 0x2b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->y()Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1906020
    const/16 v3, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906021
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906022
    const/16 v3, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906023
    const/16 v3, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906024
    const/16 v3, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906025
    const/16 v3, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906026
    const/16 v3, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906027
    const/16 v3, 0x33

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1906028
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v3

    .line 1906029
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_b
    move v7, v3

    goto/16 :goto_8

    :cond_c
    move v6, v3

    goto/16 :goto_6

    :cond_d
    move v5, v3

    goto/16 :goto_4

    :cond_e
    move v4, v3

    goto/16 :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;)I
    .locals 4

    .prologue
    .line 1906030
    if-nez p1, :cond_0

    .line 1906031
    const/4 v0, 0x0

    .line 1906032
    :goto_0
    return v0

    .line 1906033
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1906034
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;->b()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel$ProviderModel;

    move-result-object v1

    const/4 v2, 0x0

    .line 1906035
    if-nez v1, :cond_1

    .line 1906036
    :goto_1
    move v1, v2

    .line 1906037
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906038
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1906039
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906040
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906041
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906042
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel$ProviderModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906043
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1906044
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906045
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906046
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static c(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1906047
    if-nez p2, :cond_0

    .line 1906048
    :goto_0
    return v0

    .line 1906049
    :cond_0
    invoke-virtual {p1, p2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906050
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906051
    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1906052
    invoke-virtual {p1, p2, v3}, LX/15i;->h(II)Z

    move-result v2

    invoke-virtual {p0, v3, v2}, LX/186;->a(IZ)V

    .line 1906053
    invoke-virtual {p0, v4, v1}, LX/186;->b(II)V

    .line 1906054
    invoke-virtual {p1, p2, v5}, LX/15i;->j(II)I

    move-result v1

    invoke-virtual {p0, v5, v1, v0}, LX/186;->a(III)V

    .line 1906055
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906056
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static d(LX/186;LX/15i;I)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1906057
    if-nez p2, :cond_0

    move v0, v1

    .line 1906058
    :goto_0
    return v0

    .line 1906059
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v2

    .line 1906060
    invoke-virtual {p1, p2, v5}, LX/15i;->g(II)I

    move-result v0

    invoke-static {p0, p1, v0}, LX/CVz;->c(LX/186;LX/15i;I)I

    move-result v3

    .line 1906061
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v0

    .line 1906062
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1906063
    invoke-virtual {p0, v1, v2}, LX/186;->b(II)V

    .line 1906064
    invoke-virtual {p0, v5, v3}, LX/186;->b(II)V

    .line 1906065
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 1906066
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906067
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static e(LX/186;LX/15i;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1906068
    if-nez p2, :cond_0

    .line 1906069
    :goto_0
    return v0

    .line 1906070
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906071
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906072
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906073
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906074
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static f(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1906075
    if-nez p2, :cond_0

    .line 1906076
    :goto_0
    return v1

    .line 1906077
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906078
    invoke-virtual {p1, p2, v1}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1906079
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906080
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static g(LX/186;LX/15i;I)I
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1906081
    if-nez p2, :cond_0

    move v0, v1

    .line 1906082
    :goto_0
    return v0

    .line 1906083
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-virtual {p1, p2, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    const/4 v2, 0x0

    .line 1906084
    if-nez v0, :cond_1

    .line 1906085
    :goto_1
    move v0, v2

    .line 1906086
    invoke-virtual {p1, p2, v6}, LX/15i;->g(II)I

    move-result v2

    invoke-static {p0, p1, v2}, LX/CVz;->f(LX/186;LX/15i;I)I

    move-result v2

    .line 1906087
    invoke-virtual {p1, p2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906088
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1906089
    invoke-virtual {p1, p2, v1}, LX/15i;->j(II)I

    move-result v4

    invoke-virtual {p0, v1, v4, v1}, LX/186;->a(III)V

    .line 1906090
    invoke-virtual {p0, v5, v0}, LX/186;->b(II)V

    .line 1906091
    invoke-virtual {p0, v6, v2}, LX/186;->b(II)V

    .line 1906092
    invoke-virtual {p0, v7, v3}, LX/186;->b(II)V

    .line 1906093
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906094
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906095
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906096
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;->b()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel$ProviderModel;

    move-result-object v4

    const/4 v8, 0x0

    .line 1906097
    if-nez v4, :cond_2

    .line 1906098
    :goto_2
    move v4, v8

    .line 1906099
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1906100
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906101
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1906102
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906103
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1906104
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel$ProviderModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1906105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906106
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1906107
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1906108
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static h(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1906109
    if-nez p2, :cond_0

    .line 1906110
    :goto_0
    return v1

    .line 1906111
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906112
    invoke-virtual {p1, p2, v1}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1906113
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906114
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static k(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1906115
    if-nez p2, :cond_0

    .line 1906116
    :goto_0
    return v0

    .line 1906117
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v1

    const/4 v2, 0x0

    .line 1906118
    if-nez v1, :cond_1

    .line 1906119
    :goto_1
    move v1, v2

    .line 1906120
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v2

    const/4 v3, 0x0

    .line 1906121
    if-nez v2, :cond_2

    .line 1906122
    :goto_2
    move v2, v3

    .line 1906123
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906124
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906125
    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1906126
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906127
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906128
    :cond_1
    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906129
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906130
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906131
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906132
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1906133
    :cond_2
    invoke-virtual {p1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906134
    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/186;->c(I)V

    .line 1906135
    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 1906136
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1906137
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static l(LX/186;LX/15i;I)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1906138
    if-nez p2, :cond_0

    .line 1906139
    :goto_0
    return v0

    .line 1906140
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906141
    invoke-virtual {p1, p2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906142
    invoke-virtual {p1, p2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906143
    invoke-virtual {p1, p2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1906144
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906145
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906146
    invoke-virtual {p0, v6, v2}, LX/186;->b(II)V

    .line 1906147
    invoke-virtual {p0, v7, v3}, LX/186;->b(II)V

    .line 1906148
    invoke-virtual {p0, v8, v4}, LX/186;->b(II)V

    .line 1906149
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906150
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static m(LX/186;LX/15i;I)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1906151
    if-nez p2, :cond_0

    .line 1906152
    :goto_0
    return v0

    .line 1906153
    :cond_0
    invoke-virtual {p1, p2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906154
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906155
    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1906156
    invoke-virtual {p0, v3, v1}, LX/186;->b(II)V

    .line 1906157
    invoke-virtual {p1, p2, v4}, LX/15i;->j(II)I

    move-result v1

    invoke-virtual {p0, v4, v1, v0}, LX/186;->a(III)V

    .line 1906158
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906159
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static p(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1906160
    if-nez p2, :cond_0

    .line 1906161
    :goto_0
    return v0

    .line 1906162
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v1

    const/4 v2, 0x0

    .line 1906163
    if-nez v1, :cond_1

    .line 1906164
    :goto_1
    move v1, v2

    .line 1906165
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v2

    const/4 v3, 0x0

    .line 1906166
    if-nez v2, :cond_2

    .line 1906167
    :goto_2
    move v2, v3

    .line 1906168
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906169
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906170
    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1906171
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906172
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906173
    :cond_1
    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906174
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906175
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906176
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906177
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1906178
    :cond_2
    invoke-virtual {p1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906179
    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/186;->c(I)V

    .line 1906180
    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 1906181
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1906182
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static q(LX/186;LX/15i;I)I
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1906183
    if-nez p2, :cond_0

    move v0, v1

    .line 1906184
    :goto_0
    return v0

    .line 1906185
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    const/4 v2, 0x0

    .line 1906186
    if-nez v0, :cond_1

    .line 1906187
    :goto_1
    move v0, v2

    .line 1906188
    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v4, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1906189
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906190
    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 1906191
    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1906192
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906193
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906194
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1906195
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906196
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1906197
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1906198
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;->e()LX/0Px;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 1906199
    const/4 v9, 0x5

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1906200
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906201
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1906202
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1906203
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1906204
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1906205
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906206
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static s(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1906207
    if-nez p2, :cond_0

    .line 1906208
    :goto_0
    return v0

    .line 1906209
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v1

    const/4 v2, 0x0

    .line 1906210
    if-nez v1, :cond_1

    .line 1906211
    :goto_1
    move v1, v2

    .line 1906212
    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v4, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1906213
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906214
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906215
    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 1906216
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906217
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906218
    :cond_1
    invoke-virtual {p1, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906219
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906220
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1906221
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906222
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static t(LX/186;LX/15i;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1906223
    if-nez p2, :cond_0

    .line 1906224
    :goto_0
    return v0

    .line 1906225
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906226
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906227
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906228
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906229
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static u(LX/186;LX/15i;I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1906230
    if-nez p2, :cond_0

    move v0, v1

    .line 1906231
    :goto_0
    return v0

    .line 1906232
    :cond_0
    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1906233
    invoke-virtual {p1, p2, v4}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1906234
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1906235
    invoke-virtual {p0, v1, v2}, LX/186;->b(II)V

    .line 1906236
    invoke-virtual {p0, v4, v0}, LX/186;->b(II)V

    .line 1906237
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906238
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1906239
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1906240
    goto :goto_1
.end method

.method private static v(LX/186;LX/15i;I)I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1906241
    if-nez p2, :cond_0

    move v0, v1

    .line 1906242
    :goto_0
    return v0

    .line 1906243
    :cond_0
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 1906244
    invoke-virtual {p1, p2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1906245
    invoke-virtual {p1, p2, v8}, LX/15i;->g(II)I

    move-result v0

    invoke-static {p0, p1, v0}, LX/CVz;->a(LX/186;LX/15i;I)I

    move-result v4

    .line 1906246
    const/4 v0, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {p1, p2, v0, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUx;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUx;)I

    move-result v0

    .line 1906247
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906248
    invoke-virtual {p0, v1, v2}, LX/186;->b(II)V

    .line 1906249
    invoke-virtual {p1, p2, v6}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, v6, v2, v1}, LX/186;->a(III)V

    .line 1906250
    invoke-virtual {p0, v7, v3}, LX/186;->b(II)V

    .line 1906251
    invoke-virtual {p0, v8, v4}, LX/186;->b(II)V

    .line 1906252
    invoke-virtual {p1, p2, v9}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, v9, v2, v1}, LX/186;->a(III)V

    .line 1906253
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 1906254
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906255
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static w(LX/186;LX/15i;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1906256
    if-nez p2, :cond_0

    .line 1906257
    :goto_0
    return v0

    .line 1906258
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1906259
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1906260
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1906261
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1906262
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static y(LX/186;LX/15i;I)I
    .locals 12

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1906263
    if-nez p2, :cond_0

    .line 1906264
    :goto_0
    return v2

    .line 1906265
    :cond_0
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v4

    .line 1906266
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v6, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 1906267
    :goto_1
    if-eqz v3, :cond_3

    .line 1906268
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 1906269
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1906270
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v0

    aput v0, v5, v1

    .line 1906271
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1906272
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1906273
    move-object v3, v0

    goto :goto_1

    .line 1906274
    :cond_2
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    move v1, v0

    .line 1906275
    :goto_3
    invoke-virtual {p1, p2, v7}, LX/15i;->g(II)I

    move-result v0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 1906276
    if-nez v0, :cond_4

    .line 1906277
    :goto_4
    move v3, v3

    .line 1906278
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v8, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/CUy;

    invoke-static {p0, v0}, LX/CVz;->a(LX/186;LX/CUy;)I

    move-result v0

    .line 1906279
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1906280
    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1906281
    invoke-virtual {p0, v6, v1}, LX/186;->b(II)V

    .line 1906282
    invoke-virtual {p0, v7, v3}, LX/186;->b(II)V

    .line 1906283
    invoke-virtual {p0, v8, v0}, LX/186;->b(II)V

    .line 1906284
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1906285
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_3

    .line 1906286
    :cond_4
    invoke-virtual {p1, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1906287
    const/4 v9, 0x3

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1906288
    invoke-virtual {p1, v0, v3}, LX/15i;->j(II)I

    move-result v9

    invoke-virtual {p0, v3, v9, v3}, LX/186;->a(III)V

    .line 1906289
    invoke-virtual {p0, v10, v5}, LX/186;->b(II)V

    .line 1906290
    invoke-virtual {p1, v0, v11}, LX/15i;->j(II)I

    move-result v5

    invoke-virtual {p0, v11, v5, v3}, LX/186;->a(III)V

    .line 1906291
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1906292
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static z(LX/186;LX/15i;I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1906293
    if-nez p2, :cond_0

    .line 1906294
    :goto_0
    return v1

    .line 1906295
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1906296
    invoke-virtual {p1, p2, v1}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1906297
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1906298
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method
