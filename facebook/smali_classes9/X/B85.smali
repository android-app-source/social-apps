.class public final LX/B85;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/B8A;


# direct methods
.method public constructor <init>(LX/B8A;)V
    .locals 0

    .prologue
    .line 1748426
    iput-object p1, p0, LX/B85;->a:LX/B8A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 1748427
    if-eqz p2, :cond_0

    .line 1748428
    iget-object v0, p0, LX/B85;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B85;->a:LX/B8A;

    iget-object v1, v1, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 1748429
    iget-object v0, p0, LX/B85;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->d:LX/B7W;

    new-instance v1, LX/B7Z;

    const/4 v2, 0x0

    iget-object v3, p0, LX/B85;->a:LX/B8A;

    iget-object v3, v3, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-direct {v1, v2, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1748430
    iget-object v0, p0, LX/B85;->a:LX/B8A;

    invoke-static {v0}, LX/B8A;->f(LX/B8A;)V

    .line 1748431
    iget-object v0, p0, LX/B85;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748432
    :cond_0
    return-void
.end method
