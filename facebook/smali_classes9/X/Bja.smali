.class public final LX/Bja;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventPublishAndScheduleActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventPublishAndScheduleActivity;)V
    .locals 0

    .prologue
    .line 1812634
    iput-object p1, p0, LX/Bja;->a:Lcom/facebook/events/create/EventPublishAndScheduleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x32522284

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1812635
    iget-object v1, p0, LX/Bja;->a:Lcom/facebook/events/create/EventPublishAndScheduleActivity;

    iget-wide v2, v1, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->t:J

    iget-object v1, p0, LX/Bja;->a:Lcom/facebook/events/create/EventPublishAndScheduleActivity;

    iget-wide v4, v1, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->u:J

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->a(JJ)Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    move-result-object v1

    .line 1812636
    new-instance v2, LX/BjZ;

    invoke-direct {v2, p0}, LX/BjZ;-><init>(LX/Bja;)V

    .line 1812637
    iput-object v2, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->r:LX/BjY;

    .line 1812638
    iget-object v2, p0, LX/Bja;->a:Lcom/facebook/events/create/EventPublishAndScheduleActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "schedule_publish_time"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1812639
    const v1, 0x33ef4e54

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
