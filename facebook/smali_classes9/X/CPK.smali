.class public final LX/CPK;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPK;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884372
    const/4 v0, 0x0

    sput-object v0, LX/CPK;->a:LX/CPK;

    .line 1884373
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPK;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884369
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884370
    new-instance v0, LX/CPL;

    invoke-direct {v0}, LX/CPL;-><init>()V

    iput-object v0, p0, LX/CPK;->c:LX/CPL;

    .line 1884371
    return-void
.end method

.method public static onClick(LX/1X1;LX/CNe;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/CNe;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1884308
    const v0, -0x4d05de8d

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/CPK;
    .locals 2

    .prologue
    .line 1884365
    const-class v1, LX/CPK;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPK;->a:LX/CPK;

    if-nez v0, :cond_0

    .line 1884366
    new-instance v0, LX/CPK;

    invoke-direct {v0}, LX/CPK;-><init>()V

    sput-object v0, LX/CPK;->a:LX/CPK;

    .line 1884367
    :cond_0
    sget-object v0, LX/CPK;->a:LX/CPK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884368
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1884336
    check-cast p2, LX/CPJ;

    .line 1884337
    iget-object v0, p2, LX/CPJ;->a:LX/CNb;

    iget-object v1, p2, LX/CPJ;->b:LX/CNc;

    iget-object v2, p2, LX/CPJ;->c:Ljava/util/List;

    const/4 p2, 0x2

    const/high16 v8, -0x1000000

    const/4 v9, 0x0

    const/4 p0, 0x1

    .line 1884338
    const-string v3, "action"

    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1884339
    if-eqz v3, :cond_3

    invoke-static {v3, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v3

    .line 1884340
    :goto_0
    const-string v4, "font-size"

    const/high16 v5, 0x41500000    # 13.0f

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v7

    .line 1884341
    const-string v4, "enabled"

    invoke-virtual {v0, v4, p0}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1884342
    const-string v4, "pressed"

    invoke-virtual {v0, v4, v9}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1884343
    const-string v4, "pressed-text"

    const-string v5, "text"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1884344
    const-string v4, "pressed-text-color"

    invoke-virtual {v0, v4, v8}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    .line 1884345
    new-array v4, p2, [Ljava/lang/String;

    const-string v8, "pressed-background-image"

    aput-object v8, v4, v9

    const-string v8, "background-image"

    aput-object v8, v4, p0

    invoke-static {v0, v1, p1, v4}, LX/CPL;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    .line 1884346
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    .line 1884347
    const-string v9, "enabled"

    invoke-virtual {v0, v9, p0}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1884348
    const v9, -0x74bdeb80

    const/4 p0, 0x0

    invoke-static {p1, v9, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 1884349
    invoke-interface {v8, v9}, LX/1Dh;->f(LX/1dQ;)LX/1Dh;

    move-result-object v9

    .line 1884350
    const p0, -0x4d05de8d

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, p2, v1

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object v3, p0

    .line 1884351
    invoke-interface {v9, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 1884352
    :cond_0
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1884353
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const-string v5, "font-weight"

    invoke-static {v0, v5}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1884354
    :cond_1
    if-eqz v4, :cond_2

    .line 1884355
    invoke-static {p1}, LX/CNY;->a(LX/1De;)LX/CNW;

    move-result-object v5

    iget-object v3, v4, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/CO8;

    invoke-virtual {v5, v3}, LX/CNW;->a(LX/CO8;)LX/CNW;

    move-result-object v5

    iget-object v3, v4, LX/3rL;->b:Ljava/lang/Object;

    check-cast v3, LX/CNb;

    invoke-virtual {v5, v3}, LX/CNW;->a(LX/CNb;)LX/CNW;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    .line 1884356
    :cond_2
    invoke-static {v8, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1884357
    return-object v0

    .line 1884358
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1884359
    :cond_4
    const-string v4, "text"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1884360
    const-string v4, "text-color"

    invoke-virtual {v0, v4, v8}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    .line 1884361
    new-array v4, p0, [Ljava/lang/String;

    const-string v8, "background-image"

    aput-object v8, v4, v9

    invoke-static {v0, v1, p1, v4}, LX/CPL;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    goto/16 :goto_1

    .line 1884362
    :cond_5
    const-string v4, "disabled-text"

    const-string v5, "text"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1884363
    const-string v4, "disabled-text-color"

    invoke-virtual {v0, v4, v8}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    .line 1884364
    new-array v4, p2, [Ljava/lang/String;

    const-string v8, "disabled-background-image"

    aput-object v8, v4, v9

    const-string v8, "background-image"

    aput-object v8, v4, p0

    invoke-static {v0, v1, p1, v4}, LX/CPL;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1884309
    invoke-static {}, LX/1dS;->b()V

    .line 1884310
    iget v0, p1, LX/1dQ;->b:I

    .line 1884311
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884312
    :goto_0
    return-object v0

    .line 1884313
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1884314
    check-cast v2, LX/CPJ;

    .line 1884315
    iget-object v3, v2, LX/CPJ;->b:LX/CNc;

    iget-object p0, v2, LX/CPJ;->a:LX/CNb;

    .line 1884316
    iget-object p1, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {p1}, LX/CNS;->a()V

    .line 1884317
    iget-object p1, v3, LX/CNc;->b:LX/CNS;

    const-string p2, "0"

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    const-string v2, "pressed"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {p1, p2, v2, p0}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    .line 1884318
    if-eqz v0, :cond_0

    .line 1884319
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884320
    :cond_0
    iget-object p1, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {p1}, LX/CNS;->b()V

    .line 1884321
    move-object v0, v1

    .line 1884322
    goto :goto_0

    .line 1884323
    :sswitch_1
    check-cast p2, LX/48J;

    .line 1884324
    iget-object v0, p2, LX/48J;->b:Landroid/view/MotionEvent;

    iget-object v1, p2, LX/48J;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1884325
    check-cast v2, LX/CPJ;

    .line 1884326
    iget-object v3, v2, LX/CPJ;->b:LX/CNc;

    iget-object v4, v2, LX/CPJ;->a:LX/CNb;

    const/4 v2, 0x1

    .line 1884327
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p0

    if-nez p0, :cond_2

    .line 1884328
    iget-object p0, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->a()V

    .line 1884329
    iget-object p0, v3, LX/CNc;->b:LX/CNS;

    const-string p1, "1"

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    const-string p2, "pressed"

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v4}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    .line 1884330
    iget-object p0, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->b()V

    .line 1884331
    :cond_1
    :goto_1
    move v3, v2

    .line 1884332
    move v0, v3

    .line 1884333
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1884334
    :cond_2
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p0

    if-eq p0, v2, :cond_3

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p0

    const/4 p1, 0x3

    if-ne p0, p1, :cond_1

    .line 1884335
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x74bdeb80 -> :sswitch_1
        -0x4d05de8d -> :sswitch_0
    .end sparse-switch
.end method
