.class public final LX/Aro;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JU;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1719707
    iput-object p1, p0, LX/Aro;->b:LX/Arq;

    iput-object p2, p0, LX/Aro;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1719708
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    iget-boolean v0, v0, LX/Arq;->H:Z

    if-eqz v0, :cond_0

    .line 1719709
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->r:LX/6Jt;

    iget-object v1, p0, LX/Aro;->a:Ljava/io/File;

    iget-object v2, p0, LX/Aro;->b:LX/Arq;

    iget-object v3, p0, LX/Aro;->a:Ljava/io/File;

    invoke-static {v2, v3}, LX/Arq;->d(LX/Arq;Ljava/io/File;)LX/6JG;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Jt;->a(Ljava/io/File;LX/6JG;)V

    .line 1719710
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    const/4 v1, 0x0

    .line 1719711
    iput-boolean v1, v0, LX/Arq;->H:Z

    .line 1719712
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1719713
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    iget-boolean v0, v0, LX/Arq;->H:Z

    if-eqz v0, :cond_0

    .line 1719714
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    const-string v1, "pr_camera_take_video"

    invoke-static {v0, v1, p1}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719715
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    invoke-virtual {v0}, LX/Arh;->h()V

    .line 1719716
    :goto_0
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    const/4 v1, 0x0

    .line 1719717
    iput-boolean v1, v0, LX/Arq;->H:Z

    .line 1719718
    return-void

    .line 1719719
    :cond_0
    iget-object v0, p0, LX/Aro;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->h:LX/03V;

    const-string v1, "pr_camera_video_prepare"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
