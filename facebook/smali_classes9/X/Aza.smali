.class public LX/Aza;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/1aS;


# instance fields
.field private final j:Lcom/facebook/widget/text/BetterTextView;

.field private final k:Lcom/facebook/widget/text/BetterTextView;

.field public final l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1732697
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Aza;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732698
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1732695
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Aza;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732696
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1732685
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732686
    const v0, 0x7f0303a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1732687
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Aza;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 1732688
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Aza;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1732689
    const v0, 0x7f0d0b8f

    invoke-virtual {p0, v0}, LX/Aza;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    iput-object v0, p0, LX/Aza;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    .line 1732690
    return-void
.end method


# virtual methods
.method public getPhotoTray()Landroid/view/View;
    .locals 1

    .prologue
    .line 1732693
    iget-object v0, p0, LX/Aza;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    move-object v0, v0

    .line 1732694
    invoke-virtual {v0}, LX/Ale;->getPhotoTray()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1732692
    iget-object v0, p0, LX/Aza;->k:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1732691
    iget-object v0, p0, LX/Aza;->j:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
