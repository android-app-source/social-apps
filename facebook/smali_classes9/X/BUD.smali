.class public final LX/BUD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BTq;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/BUF;


# direct methods
.method public constructor <init>(LX/BUF;LX/BTq;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1788667
    iput-object p1, p0, LX/BUD;->c:LX/BUF;

    iput-object p2, p0, LX/BUD;->a:LX/BTq;

    iput-object p3, p0, LX/BUD;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1788668
    sget-object v0, LX/BUF;->a:Ljava/lang/String;

    const-string v1, "Fetch missing offline video stories failed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788669
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    .line 1788670
    iput-boolean v3, v0, LX/BUF;->i:Z

    .line 1788671
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    iget v0, v0, LX/BUF;->g:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1788672
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1788673
    new-instance v1, Lcom/facebook/video/downloadmanager/OfflineVideoInfoFetcher$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/downloadmanager/OfflineVideoInfoFetcher$1$1;-><init>(LX/BUD;)V

    iget-object v2, p0, LX/BUD;->c:LX/BUF;

    iget v2, v2, LX/BUF;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1788674
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    const/4 v1, 0x1

    .line 1788675
    iget v2, v0, LX/BUF;->g:I

    add-int/2addr v2, v1

    iput v2, v0, LX/BUF;->g:I

    .line 1788676
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/BUF;->d(LX/BUF;I)I

    .line 1788677
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1788678
    check-cast p1, Ljava/util/Map;

    const/4 v2, 0x0

    .line 1788679
    iget-object v0, p0, LX/BUD;->a:LX/BTq;

    .line 1788680
    if-eqz p1, :cond_0

    .line 1788681
    iget-object v1, v0, LX/BTq;->a:LX/BUA;

    .line 1788682
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 1788683
    if-nez p1, :cond_1

    .line 1788684
    :cond_0
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    .line 1788685
    iput v2, v0, LX/BUF;->g:I

    .line 1788686
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    const/16 v1, 0x1f4

    .line 1788687
    iput v1, v0, LX/BUF;->f:I

    .line 1788688
    iget-object v0, p0, LX/BUD;->c:LX/BUF;

    .line 1788689
    iput-boolean v2, v0, LX/BUF;->i:Z

    .line 1788690
    return-void

    .line 1788691
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1788692
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;

    invoke-virtual {v3}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, LX/BUA;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
