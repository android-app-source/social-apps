.class public final LX/CIV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 1874296
    const-wide/16 v12, 0x0

    .line 1874297
    const-wide/16 v10, 0x0

    .line 1874298
    const-wide/16 v8, 0x0

    .line 1874299
    const-wide/16 v6, 0x0

    .line 1874300
    const/4 v5, 0x0

    .line 1874301
    const/4 v4, 0x0

    .line 1874302
    const/4 v3, 0x0

    .line 1874303
    const/4 v2, 0x0

    .line 1874304
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 1874305
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1874306
    const/4 v2, 0x0

    .line 1874307
    :goto_0
    return v2

    .line 1874308
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 1874309
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1874310
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1874311
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1874312
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1874313
    const/4 v2, 0x1

    .line 1874314
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1874315
    :cond_1
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1874316
    const/4 v2, 0x1

    .line 1874317
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 1874318
    :cond_2
    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1874319
    const/4 v2, 0x1

    .line 1874320
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 1874321
    :cond_3
    const-string v6, "y"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1874322
    const/4 v2, 0x1

    .line 1874323
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 1874324
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1874325
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1874326
    if-eqz v3, :cond_6

    .line 1874327
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1874328
    :cond_6
    if-eqz v10, :cond_7

    .line 1874329
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1874330
    :cond_7
    if-eqz v9, :cond_8

    .line 1874331
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1874332
    :cond_8
    if-eqz v8, :cond_9

    .line 1874333
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1874334
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method
