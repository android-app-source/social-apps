.class public final LX/Am5;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/Am6;

.field public b:LX/Am4;

.field public c:LX/Am0;


# direct methods
.method public constructor <init>(LX/Am6;)V
    .locals 0

    .prologue
    .line 1710448
    iput-object p1, p0, LX/Am5;->a:LX/Am6;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1710449
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget-object v0, v0, LX/Am6;->j:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1710450
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    iget-object v6, p0, LX/Am5;->a:LX/Am6;

    iget v6, v6, LX/Am6;->g:I

    int-to-double v6, v6

    iget-object v8, p0, LX/Am5;->a:LX/Am6;

    iget v8, v8, LX/Am6;->h:I

    int-to-double v8, v8

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1710451
    iget-object v1, p0, LX/Am5;->a:LX/Am6;

    iget-object v1, v1, LX/Am6;->d:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/Am5;->a:LX/Am6;

    iget v2, v2, LX/Am6;->h:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setScaleX(F)V

    .line 1710452
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget-object v0, v0, LX/Am6;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1710453
    :goto_0
    return-void

    .line 1710454
    :cond_0
    iget-object v0, p0, LX/Am5;->b:LX/Am4;

    sget-object v1, LX/Am4;->SPINNER:LX/Am4;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget v0, v0, LX/Am6;->g:I

    int-to-float v0, v0

    move v6, v0

    .line 1710455
    :goto_1
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    float-to-double v6, v6

    iget-object v8, p0, LX/Am5;->a:LX/Am6;

    iget v8, v8, LX/Am6;->i:I

    int-to-double v8, v8

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1710456
    iget-object v1, p0, LX/Am5;->a:LX/Am6;

    iget-object v1, v1, LX/Am6;->e:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Am5;->a:LX/Am6;

    iget v2, v2, LX/Am6;->i:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setScaleX(F)V

    goto :goto_0

    .line 1710457
    :cond_1
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget v0, v0, LX/Am6;->h:I

    int-to-float v0, v0

    move v6, v0

    goto :goto_1
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1710458
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget-object v0, v0, LX/Am6;->j:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1710459
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1710460
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    invoke-virtual {v0}, LX/Am6;->a()V

    .line 1710461
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Am5;->c:LX/Am0;

    if-eqz v0, :cond_1

    .line 1710462
    iget-object v0, p0, LX/Am5;->c:LX/Am0;

    invoke-interface {v0}, LX/Am0;->a()V

    .line 1710463
    :cond_1
    return-void

    .line 1710464
    :cond_2
    iget-object v0, p0, LX/Am5;->a:LX/Am6;

    iget-object v0, v0, LX/Am6;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
