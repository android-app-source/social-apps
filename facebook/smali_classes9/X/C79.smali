.class public LX/C79;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C79",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850942
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850943
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C79;->b:LX/0Zi;

    .line 1850944
    iput-object p1, p0, LX/C79;->a:LX/0Ot;

    .line 1850945
    return-void
.end method

.method public static a(LX/0QB;)LX/C79;
    .locals 4

    .prologue
    .line 1850946
    const-class v1, LX/C79;

    monitor-enter v1

    .line 1850947
    :try_start_0
    sget-object v0, LX/C79;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850948
    sput-object v2, LX/C79;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850949
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850950
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850951
    new-instance v3, LX/C79;

    const/16 p0, 0x1f87

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C79;-><init>(LX/0Ot;)V

    .line 1850952
    move-object v0, v3

    .line 1850953
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850954
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C79;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850955
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850956
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1850957
    check-cast p2, LX/C78;

    .line 1850958
    iget-object v0, p0, LX/C79;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;

    iget-object v1, p2, LX/C78;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C78;->b:LX/1Pp;

    .line 1850959
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1850960
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1850961
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object p0

    .line 1850962
    if-nez p0, :cond_2

    .line 1850963
    invoke-static {v3}, LX/0x1;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 1850964
    invoke-static {v3}, LX/0x1;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 1850965
    :goto_0
    move-object v3, p0

    .line 1850966
    if-nez v3, :cond_0

    .line 1850967
    const/4 v3, 0x0

    .line 1850968
    :goto_1
    move-object v0, v3

    .line 1850969
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->b:LX/1nu;

    invoke-virtual {p2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object p2, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->c:LX/1Ai;

    .line 1850970
    iget-object v0, v3, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object p2, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    .line 1850971
    move-object v3, v3

    .line 1850972
    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    sget-object p2, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    sget-object p2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v3, p2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    const p2, 0x7f021529

    invoke-virtual {v3, p2}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p2, 0x7f0b0a3b

    invoke-interface {v3, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const p2, 0x7f0b0a3b

    invoke-interface {v3, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_1

    .line 1850973
    :cond_1
    const/4 p0, 0x0

    goto :goto_0

    .line 1850974
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSticker;->l()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1850975
    invoke-static {}, LX/1dS;->b()V

    .line 1850976
    const/4 v0, 0x0

    return-object v0
.end method
