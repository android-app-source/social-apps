.class public final LX/Aof;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;)V
    .locals 0

    .prologue
    .line 1714700
    iput-object p1, p0, LX/Aof;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x601ec400

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1714701
    iget-object v0, p0, LX/Aof;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->c:LX/1Vg;

    new-instance v2, LX/1Wx;

    invoke-direct {v2}, LX/1Wx;-><init>()V

    invoke-virtual {v0, v2}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Vr;->a(Landroid/content/Context;)V

    .line 1714702
    iget-object v0, p0, LX/Aof;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ww;

    .line 1714703
    iget-object v2, v0, LX/1Ww;->b:LX/1Wv;

    invoke-virtual {v2}, LX/1Wv;->d()I

    move-result v2

    .line 1714704
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "moments_in_feed_upsell_clicked"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1714705
    const-string p1, "total_impressions"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1714706
    iget-object v2, v0, LX/1Ww;->a:LX/0Zb;

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1714707
    const v0, 0x49a6b814    # 1365762.5f

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
