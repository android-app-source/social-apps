.class public final enum LX/Aqq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Aqq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Aqq;

.field public static final enum ERROR:LX/Aqq;

.field public static final enum FETCH:LX/Aqq;

.field public static final enum IDLE:LX/Aqq;

.field public static final enum NO_RESULTS:LX/Aqq;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1718269
    new-instance v0, LX/Aqq;

    const-string v1, "FETCH"

    invoke-direct {v0, v1, v2}, LX/Aqq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aqq;->FETCH:LX/Aqq;

    .line 1718270
    new-instance v0, LX/Aqq;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LX/Aqq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aqq;->IDLE:LX/Aqq;

    .line 1718271
    new-instance v0, LX/Aqq;

    const-string v1, "NO_RESULTS"

    invoke-direct {v0, v1, v4}, LX/Aqq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aqq;->NO_RESULTS:LX/Aqq;

    .line 1718272
    new-instance v0, LX/Aqq;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, LX/Aqq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aqq;->ERROR:LX/Aqq;

    .line 1718273
    const/4 v0, 0x4

    new-array v0, v0, [LX/Aqq;

    sget-object v1, LX/Aqq;->FETCH:LX/Aqq;

    aput-object v1, v0, v2

    sget-object v1, LX/Aqq;->IDLE:LX/Aqq;

    aput-object v1, v0, v3

    sget-object v1, LX/Aqq;->NO_RESULTS:LX/Aqq;

    aput-object v1, v0, v4

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    aput-object v1, v0, v5

    sput-object v0, LX/Aqq;->$VALUES:[LX/Aqq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1718268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Aqq;
    .locals 1

    .prologue
    .line 1718266
    const-class v0, LX/Aqq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Aqq;

    return-object v0
.end method

.method public static values()[LX/Aqq;
    .locals 1

    .prologue
    .line 1718267
    sget-object v0, LX/Aqq;->$VALUES:[LX/Aqq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Aqq;

    return-object v0
.end method
