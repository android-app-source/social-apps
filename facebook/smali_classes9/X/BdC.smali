.class public final enum LX/BdC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BdC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BdC;

.field public static final enum DATA_AVAILABLE:LX/BdC;

.field public static final enum DOWNLOADING_DATA:LX/BdC;

.field public static final enum DOWNLOAD_ERROR:LX/BdC;

.field public static final enum INITIAL_STATE:LX/BdC;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1803467
    new-instance v0, LX/BdC;

    const-string v1, "INITIAL_STATE"

    invoke-direct {v0, v1, v2}, LX/BdC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BdC;->INITIAL_STATE:LX/BdC;

    .line 1803468
    new-instance v0, LX/BdC;

    const-string v1, "DOWNLOADING_DATA"

    invoke-direct {v0, v1, v3}, LX/BdC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BdC;->DOWNLOADING_DATA:LX/BdC;

    .line 1803469
    new-instance v0, LX/BdC;

    const-string v1, "DATA_AVAILABLE"

    invoke-direct {v0, v1, v4}, LX/BdC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BdC;->DATA_AVAILABLE:LX/BdC;

    .line 1803470
    new-instance v0, LX/BdC;

    const-string v1, "DOWNLOAD_ERROR"

    invoke-direct {v0, v1, v5}, LX/BdC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BdC;->DOWNLOAD_ERROR:LX/BdC;

    .line 1803471
    const/4 v0, 0x4

    new-array v0, v0, [LX/BdC;

    sget-object v1, LX/BdC;->INITIAL_STATE:LX/BdC;

    aput-object v1, v0, v2

    sget-object v1, LX/BdC;->DOWNLOADING_DATA:LX/BdC;

    aput-object v1, v0, v3

    sget-object v1, LX/BdC;->DATA_AVAILABLE:LX/BdC;

    aput-object v1, v0, v4

    sget-object v1, LX/BdC;->DOWNLOAD_ERROR:LX/BdC;

    aput-object v1, v0, v5

    sput-object v0, LX/BdC;->$VALUES:[LX/BdC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1803472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BdC;
    .locals 1

    .prologue
    .line 1803473
    const-class v0, LX/BdC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BdC;

    return-object v0
.end method

.method public static values()[LX/BdC;
    .locals 1

    .prologue
    .line 1803474
    sget-object v0, LX/BdC;->$VALUES:[LX/BdC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BdC;

    return-object v0
.end method
