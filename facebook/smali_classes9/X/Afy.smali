.class public final LX/Afy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Afz;


# direct methods
.method public constructor <init>(LX/Afz;Z)V
    .locals 0

    .prologue
    .line 1699996
    iput-object p1, p0, LX/Afy;->b:LX/Afz;

    iput-boolean p2, p0, LX/Afy;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699997
    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1699998
    :cond_0
    :goto_0
    return-void

    .line 1699999
    :cond_1
    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Afz;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Afz;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get tip jar enabled states from video"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1700000
    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    if-eqz v0, :cond_0

    .line 1700001
    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    iget-boolean v1, p0, LX/Afy;->a:Z

    invoke-interface {v0, v1}, LX/Aeb;->c(Z)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700002
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1700003
    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Afy;->b:LX/Afz;

    iget-object v0, v0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1700004
    :cond_0
    :goto_0
    return-void

    .line 1700005
    :cond_1
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1700006
    if-nez p1, :cond_2

    move-object v0, v1

    .line 1700007
    :goto_1
    move-object v0, v0

    .line 1700008
    iget-object v1, p0, LX/Afy;->b:LX/Afz;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    if-eqz v1, :cond_0

    .line 1700009
    iget-object v1, p0, LX/Afy;->b:LX/Afz;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    iget-boolean v2, p0, LX/Afy;->a:Z

    invoke-interface {v1, v0, v2}, LX/Aeb;->a(Ljava/util/List;Z)V

    goto :goto_0

    .line 1700010
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700011
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel;

    .line 1700012
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1700013
    goto :goto_1

    .line 1700014
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel;->a()LX/0Px;

    move-result-object v5

    .line 1700015
    if-nez v5, :cond_4

    move-object v0, v1

    .line 1700016
    goto :goto_1

    .line 1700017
    :cond_4
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1700018
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_7

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel;

    .line 1700019
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel;->a()LX/0Px;

    move-result-object v7

    .line 1700020
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v3

    :goto_3
    if-ge v2, v8, :cond_6

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;

    .line 1700021
    invoke-static {v0}, LX/Afr;->a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;)LX/Afr;

    move-result-object v0

    .line 1700022
    if-eqz v0, :cond_5

    .line 1700023
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1700024
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1700025
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    .line 1700026
    goto :goto_1
.end method
