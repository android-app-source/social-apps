.class public final LX/Bce;
.super LX/BcS;
.source ""


# static fields
.field private static a:LX/Bce;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bcc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Bcf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1802415
    const/4 v0, 0x0

    sput-object v0, LX/Bce;->a:LX/Bce;

    .line 1802416
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bce;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1802455
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1802456
    new-instance v0, LX/Bcf;

    invoke-direct {v0}, LX/Bcf;-><init>()V

    iput-object v0, p0, LX/Bce;->c:LX/Bcf;

    .line 1802457
    return-void
.end method

.method public static b(LX/BcP;)LX/Bcc;
    .locals 3

    .prologue
    .line 1802446
    new-instance v0, LX/Bcd;

    invoke-direct {v0}, LX/Bcd;-><init>()V

    .line 1802447
    sget-object v1, LX/Bce;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bcc;

    .line 1802448
    if-nez v1, :cond_0

    .line 1802449
    new-instance v1, LX/Bcc;

    invoke-direct {v1}, LX/Bcc;-><init>()V

    .line 1802450
    :cond_0
    iput-object v0, v1, LX/BcN;->a:LX/BcO;

    .line 1802451
    iput-object v0, v1, LX/Bcc;->a:LX/Bcd;

    .line 1802452
    iget-object v2, v1, LX/Bcc;->d:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    .line 1802453
    move-object v0, v1

    .line 1802454
    return-object v0
.end method

.method public static declared-synchronized d()LX/Bce;
    .locals 2

    .prologue
    .line 1802442
    const-class v1, LX/Bce;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bce;->a:LX/Bce;

    if-nez v0, :cond_0

    .line 1802443
    new-instance v0, LX/Bce;

    invoke-direct {v0}, LX/Bce;-><init>()V

    sput-object v0, LX/Bce;->a:LX/Bce;

    .line 1802444
    :cond_0
    sget-object v0, LX/Bce;->a:LX/Bce;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1802445
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1802440
    invoke-static {}, LX/1dS;->b()V

    .line 1802441
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;LX/BcJ;LX/BcO;LX/BcO;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1802418
    check-cast p3, LX/Bcd;

    .line 1802419
    check-cast p4, LX/Bcd;

    .line 1802420
    if-nez p3, :cond_0

    move-object v2, v1

    :goto_0
    if-nez p4, :cond_1

    move-object v0, v1

    :goto_1
    invoke-static {v2, v0}, LX/BcS;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v2

    .line 1802421
    if-nez p3, :cond_2

    move-object v0, v1

    :goto_2
    if-nez p4, :cond_3

    :goto_3
    invoke-static {v0, v1}, LX/BcS;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 1802422
    const/4 p1, 0x0

    .line 1802423
    if-eqz v0, :cond_6

    .line 1802424
    iget-object v1, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1802425
    if-eqz v1, :cond_6

    .line 1802426
    iget-object v1, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1802427
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move p0, v1

    .line 1802428
    :goto_4
    iget-object v1, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1802429
    if-nez v1, :cond_4

    .line 1802430
    invoke-virtual {p2, p1}, LX/BcJ;->b(I)V

    .line 1802431
    :goto_5
    return-void

    .line 1802432
    :cond_0
    iget-object v0, p3, LX/Bcd;->b:LX/1X1;

    move-object v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p4, LX/Bcd;->b:LX/1X1;

    goto :goto_1

    .line 1802433
    :cond_2
    iget-object v0, p3, LX/Bcd;->c:Ljava/lang/Boolean;

    goto :goto_2

    :cond_3
    iget-object v1, p4, LX/Bcd;->c:Ljava/lang/Boolean;

    goto :goto_3

    .line 1802434
    :cond_4
    iget-object v1, v2, LX/3lz;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 1802435
    if-nez v1, :cond_5

    .line 1802436
    iget-object v1, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1802437
    check-cast v1, LX/1X1;

    invoke-virtual {p2, p1, v1, p0}, LX/BcJ;->a(ILX/1X1;Z)V

    goto :goto_5

    .line 1802438
    :cond_5
    iget-object v1, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 1802439
    check-cast v1, LX/1X1;

    invoke-virtual {p2, p1, v1, p0}, LX/BcJ;->b(ILX/1X1;Z)V

    goto :goto_5

    :cond_6
    move p0, p1

    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1802417
    const/4 v0, 0x1

    return v0
.end method
