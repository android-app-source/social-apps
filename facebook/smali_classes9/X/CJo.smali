.class public final LX/CJo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/CJm;

.field private final b:LX/0gc;


# direct methods
.method public constructor <init>(LX/CJi;LX/CJm;LX/0gc;)V
    .locals 0
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875841
    iput-object p2, p0, LX/CJo;->a:LX/CJm;

    .line 1875842
    iput-object p3, p0, LX/CJo;->b:LX/0gc;

    .line 1875843
    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p0    # Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875823
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1875824
    if-nez p0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1875825
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1875826
    const-string v0, "target_id"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875827
    if-eqz p0, :cond_4

    .line 1875828
    const-string v0, "cta"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1875829
    const-string v2, "fallback_url"

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875830
    const-string v2, "native_link"

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875831
    :goto_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1875832
    const-string v2, "extra_cta_clicked"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1875833
    return-object v0

    .line 1875834
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1875835
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1875836
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1875837
    :cond_4
    if-eqz p1, :cond_5

    .line 1875838
    const-string v0, "native_link"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875839
    :cond_5
    const-string v0, "fallback_url"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
