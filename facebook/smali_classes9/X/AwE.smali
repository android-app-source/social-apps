.class public LX/AwE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1725196
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "inspiration_prefs/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1725197
    sput-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "cam_facing_front"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->b:LX/0Tn;

    .line 1725198
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "inspiration_nux_video_pref_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->c:LX/0Tn;

    .line 1725199
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "inspiration_nux_flow_finished_pref_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->d:LX/0Tn;

    .line 1725200
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "inspiration_tooltip_capture_pref_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->e:LX/0Tn;

    .line 1725201
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "inspiration_tooltip_share_pref_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->f:LX/0Tn;

    .line 1725202
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "inspiration_tooltip_camera_pref_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->g:LX/0Tn;

    .line 1725203
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "time_last_inspiration_network_fetch_seconds"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->h:LX/0Tn;

    .line 1725204
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "last_inspiration_network_fetch_location_latitude"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->i:LX/0Tn;

    .line 1725205
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "last_inspiration_network_fetch_location_longitude"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->j:LX/0Tn;

    .line 1725206
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "last_inspiration_network_fetch_location_accuracy"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->k:LX/0Tn;

    .line 1725207
    sget-object v0, LX/AwE;->a:LX/0Tn;

    const-string v1, "last_inspiration_network_fetch_location_staletime"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AwE;->l:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1725208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1725209
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1725210
    sget-object v0, LX/AwE;->b:LX/0Tn;

    sget-object v1, LX/AwE;->c:LX/0Tn;

    sget-object v2, LX/AwE;->d:LX/0Tn;

    sget-object v3, LX/AwE;->e:LX/0Tn;

    sget-object v4, LX/AwE;->f:LX/0Tn;

    sget-object v5, LX/AwE;->i:LX/0Tn;

    const/4 v6, 0x3

    new-array v6, v6, [LX/0Tn;

    const/4 v7, 0x0

    sget-object v8, LX/AwE;->j:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/AwE;->k:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/AwE;->l:LX/0Tn;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
