.class public LX/AzH;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732364
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1732365
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            ")",
            "Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;"
        }
    .end annotation

    .prologue
    .line 1732366
    new-instance v0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    const-class v1, LX/AzL;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AzL;

    invoke-static {p0}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v8

    check-cast v8, LX/AzQ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;-><init>(LX/0Px;Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1Ad;LX/AzL;LX/AzQ;)V

    .line 1732367
    return-object v0
.end method
