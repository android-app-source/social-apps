.class public LX/Ajs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ajk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ajs;


# instance fields
.field private final a:LX/1HI;


# direct methods
.method public constructor <init>(LX/1HI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708473
    iput-object p1, p0, LX/Ajs;->a:LX/1HI;

    .line 1708474
    return-void
.end method

.method public static a(LX/0QB;)LX/Ajs;
    .locals 4

    .prologue
    .line 1708475
    sget-object v0, LX/Ajs;->b:LX/Ajs;

    if-nez v0, :cond_1

    .line 1708476
    const-class v1, LX/Ajs;

    monitor-enter v1

    .line 1708477
    :try_start_0
    sget-object v0, LX/Ajs;->b:LX/Ajs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1708478
    if-eqz v2, :cond_0

    .line 1708479
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1708480
    new-instance p0, LX/Ajs;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-direct {p0, v3}, LX/Ajs;-><init>(LX/1HI;)V

    .line 1708481
    move-object v0, p0

    .line 1708482
    sput-object v0, LX/Ajs;->b:LX/Ajs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1708484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1708485
    :cond_1
    sget-object v0, LX/Ajs;->b:LX/Ajs;

    return-object v0

    .line 1708486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1708487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1708488
    iget-object v0, p0, LX/Ajs;->a:LX/1HI;

    invoke-virtual {v0, p1, p2}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    return-object v0
.end method
