.class public final LX/CfX;
.super LX/1jx;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1926356
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 1926357
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CfX;->a:Ljava/util/Set;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1926358
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1926359
    iget-object v0, p0, LX/CfX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1926360
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 1

    .prologue
    .line 1926361
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1926362
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CfX;->a(Ljava/lang/String;)V

    .line 1926363
    :cond_0
    instance-of v0, p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1926364
    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CfX;->a(Ljava/lang/String;)V

    move-object v0, p1

    .line 1926365
    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionFeedbackFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CfX;->a(Ljava/lang/String;)V

    .line 1926366
    :cond_1
    instance-of v0, p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1926367
    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CfX;->a(Ljava/lang/String;)V

    .line 1926368
    check-cast p1, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CfX;->a(Ljava/lang/String;)V

    .line 1926369
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
