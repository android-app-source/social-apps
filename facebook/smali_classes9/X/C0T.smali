.class public LX/C0T;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0R;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1840710
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C0T;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0U;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840696
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1840697
    iput-object p1, p0, LX/C0T;->b:LX/0Ot;

    .line 1840698
    return-void
.end method

.method public static a(LX/0QB;)LX/C0T;
    .locals 4

    .prologue
    .line 1840699
    const-class v1, LX/C0T;

    monitor-enter v1

    .line 1840700
    :try_start_0
    sget-object v0, LX/C0T;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840701
    sput-object v2, LX/C0T;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840704
    new-instance v3, LX/C0T;

    const/16 p0, 0x1e6c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0T;-><init>(LX/0Ot;)V

    .line 1840705
    move-object v0, v3

    .line 1840706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1840671
    check-cast p2, LX/C0S;

    .line 1840672
    iget-object v0, p0, LX/C0T;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0U;

    iget-object v1, p2, LX/C0S;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C0S;->b:LX/1Pm;

    .line 1840673
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1840674
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840675
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C0U;->a:LX/Ap5;

    invoke-virtual {v5, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object v5

    const/4 p0, 0x4

    invoke-virtual {v5, p0}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object v5

    .line 1840676
    iget-object p0, v0, LX/C0U;->d:LX/ApL;

    invoke-virtual {p0, p1}, LX/ApL;->c(LX/1De;)LX/ApK;

    move-result-object p0

    const/4 p2, 0x4

    invoke-virtual {p0, p2}, LX/ApK;->h(I)LX/ApK;

    move-result-object p0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-static {p2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/ApK;->a(Landroid/net/Uri;)LX/ApK;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object p0, p0

    .line 1840677
    invoke-virtual {v5, p0}, LX/Ap4;->a(LX/1X1;)LX/Ap4;

    move-result-object v5

    .line 1840678
    iget-object p0, v0, LX/C0U;->e:LX/2sO;

    invoke-virtual {p0, v1}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object p0

    .line 1840679
    if-nez p0, :cond_0

    .line 1840680
    const/4 p0, 0x0

    .line 1840681
    :goto_0
    move-object p0, p0

    .line 1840682
    invoke-virtual {v5, p0}, LX/Ap4;->b(LX/1X1;)LX/Ap4;

    move-result-object v5

    iget-object p0, v0, LX/C0U;->b:LX/1qb;

    invoke-virtual {p0, v3}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v5

    invoke-static {v3}, LX/1qb;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    .line 1840683
    const v5, -0x7c53f970

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1840684
    invoke-virtual {v3, v5}, LX/Ap4;->a(LX/1dQ;)LX/Ap4;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1840685
    return-object v0

    :cond_0
    const/4 p2, 0x1

    invoke-virtual {p0, p1, v2, v1, p2}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1840686
    invoke-static {}, LX/1dS;->b()V

    .line 1840687
    iget v0, p1, LX/1dQ;->b:I

    .line 1840688
    packed-switch v0, :pswitch_data_0

    .line 1840689
    :goto_0
    return-object v2

    .line 1840690
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1840691
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1840692
    check-cast v1, LX/C0S;

    .line 1840693
    iget-object v3, p0, LX/C0T;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C0U;

    iget-object p1, v1, LX/C0S;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/C0S;->b:LX/1Pm;

    .line 1840694
    iget-object p0, v3, LX/C0U;->c:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1840695
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x7c53f970
        :pswitch_0
    .end packed-switch
.end method
