.class public LX/CEJ;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/B5j;)V
    .locals 0
    .param p2    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1861065
    invoke-direct {p0, p1, p2}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1861066
    return-void
.end method


# virtual methods
.method public final aB()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1861067
    new-instance v0, LX/CEG;

    invoke-direct {v0, p0}, LX/CEG;-><init>(LX/CEJ;)V

    return-object v0
.end method

.method public final aL()Ljava/lang/String;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1861068
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1861069
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1861070
    iget-object v0, p0, LX/AQ9;->b:Landroid/content/Context;

    move-object v0, v0

    .line 1861071
    const v1, 0x7f082973

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1861072
    :goto_1
    return-object v0

    .line 1861073
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1861074
    :cond_1
    iget-object v1, p0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1861075
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082974

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final ah()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1861076
    new-instance v0, LX/CEH;

    invoke-direct {v0, p0}, LX/CEH;-><init>(LX/CEJ;)V

    return-object v0
.end method
