.class public final LX/BFD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/BFI;


# direct methods
.method public constructor <init>(LX/BFI;LX/0TF;)V
    .locals 0

    .prologue
    .line 1765467
    iput-object p1, p0, LX/BFD;->b:LX/BFI;

    iput-object p2, p0, LX/BFD;->a:LX/0TF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1765468
    iget-object v0, p0, LX/BFD;->b:LX/BFI;

    iget-object v0, v0, LX/BFI;->b:LX/03V;

    sget-object v1, LX/BFI;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1765469
    iget-object v0, p0, LX/BFD;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1765470
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1765471
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1765472
    if-eqz p1, :cond_0

    .line 1765473
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1765474
    if-nez v0, :cond_1

    .line 1765475
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched story was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/BFD;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1765476
    :goto_0
    return-void

    .line 1765477
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1765478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765479
    iget-object v1, p0, LX/BFD;->b:LX/BFI;

    iget-object v1, v1, LX/BFI;->d:LX/189;

    invoke-virtual {v1, v0}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1765480
    iget-object v1, p0, LX/BFD;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
