.class public LX/B9j;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field public a:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/view/View;

.field private i:Z

.field private j:Z

.field private k:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public l:LX/78A;

.field private m:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1752206
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1752207
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9j;->j:Z

    .line 1752208
    const-class v0, LX/B9j;

    invoke-static {v0, p0}, LX/B9j;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1752209
    const v0, 0x7f031445

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1752210
    const v0, 0x7f0d2e50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9j;->c:Landroid/widget/TextView;

    .line 1752211
    const v0, 0x7f0d2e51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9j;->d:Landroid/widget/TextView;

    .line 1752212
    const v0, 0x7f0d2e52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9j;->e:Landroid/widget/TextView;

    .line 1752213
    const v0, 0x7f0d2e4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/B9j;->f:Landroid/widget/ImageView;

    .line 1752214
    const v0, 0x7f0d2e53

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/B9j;->g:Landroid/widget/ImageView;

    .line 1752215
    const v0, 0x7f0d2e54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/B9j;->h:Landroid/view/View;

    .line 1752216
    const v0, 0x7f0a00ea

    invoke-virtual {p0, v0}, LX/B9j;->setBackgroundResource(I)V

    .line 1752217
    return-void
.end method

.method private a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Landroid/widget/ImageView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)V
    .locals 2

    .prologue
    .line 1752201
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1752202
    iget-object v1, p0, LX/B9j;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v1, p1, p3}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1752203
    iget-object v1, p0, LX/B9j;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {v1, p1, p3}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1752204
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1752205
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/B9j;

    const-class v1, LX/13A;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/13A;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object p0

    check-cast p0, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v1, p1, LX/B9j;->a:LX/13A;

    iput-object p0, p1, LX/B9j;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    return-void
.end method

.method public static b$redex0(LX/B9j;)V
    .locals 1

    .prologue
    .line 1752153
    iget-object v0, p0, LX/B9j;->m:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1752154
    iget-object v0, p0, LX/B9j;->m:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1752155
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9j;->i:Z

    .line 1752156
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9j;->setVisibility(I)V

    .line 1752157
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1752171
    iget-object v0, p0, LX/B9j;->k:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 1752172
    iget-boolean v0, p0, LX/B9j;->i:Z

    if-eqz v0, :cond_0

    .line 1752173
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9j;->setVisibility(I)V

    .line 1752174
    :cond_0
    :goto_0
    return-void

    .line 1752175
    :cond_1
    iput-object p1, p0, LX/B9j;->k:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1752176
    iget-object v0, p0, LX/B9j;->k:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    .line 1752177
    if-nez v0, :cond_2

    .line 1752178
    invoke-static {p0}, LX/B9j;->b$redex0(LX/B9j;)V

    goto :goto_0

    .line 1752179
    :cond_2
    iget-object v1, p0, LX/B9j;->a:LX/13A;

    iget-object v2, p0, LX/B9j;->k:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v1, v2, p2, v0, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v1

    iput-object v1, p0, LX/B9j;->l:LX/78A;

    .line 1752180
    new-instance v1, LX/B9h;

    invoke-direct {v1, p0}, LX/B9h;-><init>(LX/B9j;)V

    .line 1752181
    new-instance v2, LX/B9i;

    invoke-direct {v2, p0}, LX/B9i;-><init>(LX/B9j;)V

    .line 1752182
    iget-object v3, p0, LX/B9j;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1752183
    iget-object v1, p0, LX/B9j;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1752184
    iget-object v1, p0, LX/B9j;->c:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1752185
    iget-object v1, p0, LX/B9j;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1752186
    sget-object v1, LX/76S;->ANY:LX/76S;

    invoke-static {v0, v1}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v1

    .line 1752187
    if-eqz v1, :cond_4

    .line 1752188
    iget-object v2, p0, LX/B9j;->f:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2, v0}, LX/B9j;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Landroid/widget/ImageView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)V

    .line 1752189
    :goto_1
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1752190
    iget-object v1, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v2, p0, LX/B9j;->g:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2, v0}, LX/B9j;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Landroid/widget/ImageView;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)V

    .line 1752191
    :goto_2
    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v1, p0, LX/B9j;->e:Landroid/widget/TextView;

    .line 1752192
    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1752193
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1752194
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9j;->j:Z

    .line 1752195
    iput-boolean v4, p0, LX/B9j;->i:Z

    .line 1752196
    invoke-virtual {p0, v4}, LX/B9j;->setVisibility(I)V

    goto :goto_0

    .line 1752197
    :cond_4
    iget-object v1, p0, LX/B9j;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_1

    .line 1752198
    :cond_5
    iget-object v1, p0, LX/B9j;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_2

    .line 1752199
    :cond_6
    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1752200
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1752164
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1752165
    iget-boolean v0, p0, LX/B9j;->j:Z

    if-eqz v0, :cond_0

    .line 1752166
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9j;->j:Z

    .line 1752167
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    .line 1752168
    iget-object p1, p0, LX/B9j;->l:LX/78A;

    invoke-virtual {p1}, LX/78A;->a()V

    .line 1752169
    iget-object p1, p0, LX/B9j;->l:LX/78A;

    invoke-virtual {p1, v0}, LX/78A;->a(LX/77n;)V

    .line 1752170
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1752160
    iget-boolean v0, p0, LX/B9j;->i:Z

    if-eqz v0, :cond_0

    .line 1752161
    invoke-virtual {p0, v1, v1}, LX/B9j;->setMeasuredDimension(II)V

    .line 1752162
    :goto_0
    return-void

    .line 1752163
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1752158
    iput-object p1, p0, LX/B9j;->m:Ljava/lang/Runnable;

    .line 1752159
    return-void
.end method
