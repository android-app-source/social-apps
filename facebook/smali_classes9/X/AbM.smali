.class public LX/AbM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690613
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1690593
    check-cast p1, Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;

    .line 1690594
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1690595
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "seal_video_broadcast"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690596
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "video_broadcast_update"

    .line 1690597
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1690598
    move-object v1, v1

    .line 1690599
    const-string v2, "POST"

    .line 1690600
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1690601
    move-object v1, v1

    .line 1690602
    iget-object v2, p1, Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;->a:Ljava/lang/String;

    .line 1690603
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1690604
    move-object v1, v1

    .line 1690605
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1690606
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1690607
    move-object v1, v1

    .line 1690608
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1690609
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1690610
    move-object v0, v1

    .line 1690611
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1690591
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1690592
    const/4 v0, 0x0

    return-object v0
.end method
