.class public final LX/CM1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/ui/emoji/model/Emoji;

.field public final synthetic b:Z

.field public final synthetic c:LX/CM3;


# direct methods
.method public constructor <init>(LX/CM3;Lcom/facebook/ui/emoji/model/Emoji;Z)V
    .locals 0

    .prologue
    .line 1880030
    iput-object p1, p0, LX/CM1;->c:LX/CM3;

    iput-object p2, p0, LX/CM1;->a:Lcom/facebook/ui/emoji/model/Emoji;

    iput-boolean p3, p0, LX/CM1;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x22bb933a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1880031
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->j:LX/CM6;

    if-eqz v0, :cond_0

    .line 1880032
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->j:LX/CM6;

    iget-object v2, p0, LX/CM1;->a:Lcom/facebook/ui/emoji/model/Emoji;

    invoke-virtual {v0, v2}, LX/CM6;->a(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880033
    :cond_0
    iget-boolean v0, p0, LX/CM1;->b:Z

    if-eqz v0, :cond_1

    .line 1880034
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CML;

    iget-object v2, p0, LX/CM1;->a:Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880035
    iget p1, v2, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v2, p1

    .line 1880036
    invoke-virtual {v0, v2}, LX/CML;->a(I)V

    .line 1880037
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->j:LX/CM6;

    if-eqz v0, :cond_1

    .line 1880038
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->j:LX/CM6;

    invoke-virtual {v0}, LX/CM6;->a()V

    .line 1880039
    :cond_1
    iget-object v0, p0, LX/CM1;->c:LX/CM3;

    iget-object v0, v0, LX/CM3;->k:LX/CMK;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1880040
    const v0, 0x2fa5fed9

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
