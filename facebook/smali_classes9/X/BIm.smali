.class public final LX/BIm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 0

    .prologue
    .line 1770937
    iput-object p1, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 2

    .prologue
    .line 1770938
    if-nez p1, :cond_0

    .line 1770939
    :goto_0
    return-void

    .line 1770940
    :cond_0
    iget-object v0, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    const/4 v1, 0x1

    .line 1770941
    iput-boolean v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    .line 1770942
    iget-object v0, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1770943
    sget-object v1, LX/74F;->REMOVE_TAG:LX/74F;

    invoke-static {v1}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1770944
    iget-object v0, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v1, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1770945
    iget-object v0, p0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9ip;->n()V

    goto :goto_0
.end method
