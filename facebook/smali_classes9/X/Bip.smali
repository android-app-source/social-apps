.class public LX/Bip;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;

.field public static final c:Ljava/lang/Object;

.field public static final d:Ljava/lang/Object;


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:LX/Bir;

.field public h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1810958
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Bip;->a:Ljava/lang/Object;

    .line 1810959
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Bip;->b:Ljava/lang/Object;

    .line 1810960
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Bip;->c:Ljava/lang/Object;

    .line 1810961
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Bip;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Or;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810953
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1810954
    iput-object p1, p0, LX/Bip;->f:Ljava/lang/String;

    .line 1810955
    iput-object p2, p0, LX/Bip;->h:LX/0Or;

    .line 1810956
    iget-object v0, p0, LX/Bip;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/Bip;->a:Ljava/lang/Object;

    sget-object v2, LX/Bip;->b:Ljava/lang/Object;

    sget-object v3, LX/Bip;->d:Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Bip;->e:Ljava/util/List;

    .line 1810957
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1810935
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1810936
    const v1, 0x7f0d019d

    if-ne p2, v1, :cond_0

    .line 1810937
    const v1, 0x7f0304be

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1810938
    new-instance v0, LX/Bke;

    invoke-direct {v0, v1}, LX/Bke;-><init>(Landroid/view/View;)V

    .line 1810939
    :goto_0
    return-object v0

    .line 1810940
    :cond_0
    const v1, 0x7f0d019e

    if-ne p2, v1, :cond_1

    .line 1810941
    const v1, 0x7f0304bd

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1810942
    new-instance v0, LX/Bkg;

    invoke-direct {v0, v1}, LX/Bkg;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1810943
    :cond_1
    const v1, 0x7f0d019f

    if-ne p2, v1, :cond_2

    .line 1810944
    const v1, 0x7f0304bf

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1810945
    new-instance v0, LX/Bkf;

    invoke-direct {v0, v1}, LX/Bkf;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1810946
    :cond_2
    const v1, 0x7f0d01a0

    if-ne p2, v1, :cond_3

    .line 1810947
    const v1, 0x7f0304c0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1810948
    new-instance v0, LX/Bkg;

    invoke-direct {v0, v1}, LX/Bkg;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1810949
    :cond_3
    const v1, 0x7f0d01a1

    if-ne p2, v1, :cond_4

    .line 1810950
    const v1, 0x7f0304bb

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1810951
    new-instance v0, LX/Bkd;

    invoke-direct {v0, v1}, LX/Bkd;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1810952
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 1810904
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1810905
    const v1, 0x7f0d019d

    if-ne v0, v1, :cond_0

    move-object v0, p1

    .line 1810906
    check-cast v0, LX/Bke;

    .line 1810907
    iget-object v1, p0, LX/Bip;->e:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 1810908
    iget-object v1, p0, LX/Bip;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 1810909
    check-cast v1, Lcom/facebook/user/model/User;

    .line 1810910
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v3

    .line 1810911
    move-object v1, v2

    .line 1810912
    check-cast v1, Lcom/facebook/user/model/User;

    .line 1810913
    iget-object v3, v1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v3

    .line 1810914
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v5

    move-object v1, v2

    .line 1810915
    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    .line 1810916
    iget-object v1, p0, LX/Bip;->f:Ljava/lang/String;

    check-cast v2, Lcom/facebook/user/model/User;

    .line 1810917
    iget-object v4, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v4

    .line 1810918
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    move-object v2, v5

    move-object v1, v6

    .line 1810919
    :goto_0
    iget-object v5, p0, LX/Bip;->g:LX/Bir;

    .line 1810920
    iput-object v1, v0, LX/Bke;->l:Ljava/lang/String;

    .line 1810921
    iput-object v5, v0, LX/Bke;->m:LX/Bir;

    .line 1810922
    if-nez v2, :cond_2

    .line 1810923
    iget-object v1, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 1810924
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v1, v2

    .line 1810925
    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v6

    move-object v1, v2

    .line 1810926
    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    move-object v1, v2

    .line 1810927
    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1810928
    iget-object v1, p0, LX/Bip;->f:Ljava/lang/String;

    check-cast v2, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    move-object v2, v5

    move-object v1, v6

    goto :goto_0

    .line 1810929
    :cond_2
    iget-object v1, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 1810930
    iget-object v1, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1810931
    iget-object v5, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    if-nez v3, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v5, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1810932
    iget-object v1, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 1810933
    iget-object v1, v0, LX/Bke;->n:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1810934
    :cond_3
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_2
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1810890
    iget-object v0, p0, LX/Bip;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1810891
    iget-object v1, p0, LX/Bip;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1810892
    const v0, 0x7f0d019d

    .line 1810893
    :goto_0
    return v0

    .line 1810894
    :cond_0
    sget-object v1, LX/Bip;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1810895
    const v0, 0x7f0d019e

    goto :goto_0

    .line 1810896
    :cond_1
    sget-object v1, LX/Bip;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1810897
    const v0, 0x7f0d019f

    goto :goto_0

    .line 1810898
    :cond_2
    sget-object v1, LX/Bip;->c:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1810899
    const v0, 0x7f0d01a0

    goto :goto_0

    .line 1810900
    :cond_3
    sget-object v1, LX/Bip;->d:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1810901
    const v0, 0x7f0d01a1

    goto :goto_0

    .line 1810902
    :cond_4
    const v0, 0x7f0d019d

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1810903
    iget-object v0, p0, LX/Bip;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
