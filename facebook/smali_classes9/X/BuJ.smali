.class public abstract LX/BuJ;
.super LX/2oy;
.source ""


# instance fields
.field public A:Landroid/view/View;

.field public B:Landroid/view/View;

.field public C:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

.field public D:LX/6Vi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6Vi",
            "<",
            "LX/82h;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/2pa;

.field public F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

.field public G:Z

.field public final H:Landroid/view/View$OnClickListener;

.field public final I:LX/6Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6Ve",
            "<",
            "LX/82h;",
            ">;"
        }
    .end annotation
.end field

.field public final J:LX/8qA;

.field public final K:LX/19J;

.field public final L:LX/1Kd;

.field public final M:LX/19M;

.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/ViewStub;

.field public c:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1K9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7I8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private x:Landroid/view/View;

.field private y:Lcom/facebook/resources/ui/FbTextView;

.field private z:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1830795
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BuJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830796
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830797
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BuJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    .line 1830664
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830665
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BuJ;->G:Z

    .line 1830666
    new-instance v0, LX/BuB;

    invoke-direct {v0, p0}, LX/BuB;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->H:Landroid/view/View$OnClickListener;

    .line 1830667
    new-instance v0, LX/BuE;

    invoke-direct {v0, p0}, LX/BuE;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->I:LX/6Ve;

    .line 1830668
    new-instance v0, LX/BuF;

    invoke-direct {v0, p0}, LX/BuF;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->J:LX/8qA;

    .line 1830669
    new-instance v0, LX/BuG;

    invoke-direct {v0, p0}, LX/BuG;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->K:LX/19J;

    .line 1830670
    new-instance v0, LX/BuH;

    invoke-direct {v0, p0}, LX/BuH;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->L:LX/1Kd;

    .line 1830671
    new-instance v0, LX/BuI;

    invoke-direct {v0, p0}, LX/BuI;-><init>(LX/BuJ;)V

    iput-object v0, p0, LX/BuJ;->M:LX/19M;

    .line 1830672
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, LX/BuJ;

    invoke-static {p3}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-static {p3}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v4

    check-cast v4, LX/1AM;

    invoke-static {p3}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v5

    check-cast v5, LX/1K9;

    const/16 v6, 0x1d6c

    invoke-static {p3, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p3}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v7

    check-cast v7, LX/154;

    const/16 v8, 0x13a4

    invoke-static {p3, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const-class p1, LX/3iG;

    invoke-interface {p3, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/3iG;

    invoke-static {p3}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object p2

    check-cast p2, Landroid/view/WindowManager;

    const/16 v0, 0x37b0

    invoke-static {p3, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v3, v2, LX/BuJ;->c:LX/0bH;

    iput-object v4, v2, LX/BuJ;->d:LX/1AM;

    iput-object v5, v2, LX/BuJ;->e:LX/1K9;

    iput-object v6, v2, LX/BuJ;->f:LX/0Ot;

    iput-object v7, v2, LX/BuJ;->n:LX/154;

    iput-object v8, v2, LX/BuJ;->o:LX/0Or;

    iput-object p1, v2, LX/BuJ;->p:LX/3iG;

    iput-object p2, v2, LX/BuJ;->q:Landroid/view/WindowManager;

    iput-object p3, v2, LX/BuJ;->r:LX/0Ot;

    .line 1830673
    invoke-virtual {p0}, LX/BuJ;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1830674
    invoke-virtual {p0}, LX/BuJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081107

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BuJ;->s:Ljava/lang/String;

    .line 1830675
    invoke-virtual {p0}, LX/BuJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BuJ;->t:Ljava/lang/String;

    .line 1830676
    const v0, 0x7f0d119b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830677
    const v0, 0x7f0d119d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830678
    const v0, 0x7f0d119c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/BuJ;->v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830679
    iget-object v1, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/BuJ;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1830680
    iget-object v1, p0, LX/BuJ;->v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/BuJ;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1830681
    iget-object v1, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/BuJ;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1830682
    const v0, 0x7f0d119e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/BuJ;->x:Landroid/view/View;

    .line 1830683
    const v0, 0x7f0d11a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 1830684
    const v0, 0x7f0d1188

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    .line 1830685
    const v0, 0x7f0d0395

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iput-object v0, p0, LX/BuJ;->C:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    .line 1830686
    iget-object v0, p0, LX/BuJ;->C:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iget-object v1, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830687
    iput-object v1, v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->B:Landroid/view/View;

    .line 1830688
    const v0, 0x7f0d0c42

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BuJ;->A:Landroid/view/View;

    .line 1830689
    const v0, 0x7f0d0c43

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BuJ;->B:Landroid/view/View;

    .line 1830690
    iget-object v0, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v1, LX/BuC;

    invoke-direct {v1, p0}, LX/BuC;-><init>(LX/BuJ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830691
    iget-object v0, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830692
    new-instance v1, LX/BuD;

    invoke-direct {v1, p0}, LX/BuD;-><init>(LX/BuJ;)V

    move-object v1, v1

    .line 1830693
    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830694
    iget-object v0, p0, LX/BuJ;->v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1830695
    iget-object v1, p0, LX/BuJ;->H:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1830696
    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830697
    iget-object v0, p0, LX/BuJ;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1830698
    iget-object v0, p0, LX/BuJ;->x:Landroid/view/View;

    .line 1830699
    iget-object v1, p0, LX/BuJ;->H:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1830700
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830701
    :cond_0
    return-void
.end method

.method public static getFullScreenVideoPlayer(LX/BuJ;)Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;
    .locals 2

    .prologue
    .line 1830791
    invoke-virtual {p0}, LX/BuJ;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 1830792
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830793
    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1830794
    return-object v0
.end method

.method public static j(LX/BuJ;)V
    .locals 2

    .prologue
    .line 1830783
    iget-object v0, p0, LX/BuJ;->D:LX/6Vi;

    if-eqz v0, :cond_0

    .line 1830784
    iget-object v0, p0, LX/BuJ;->e:LX/1K9;

    iget-object v1, p0, LX/BuJ;->D:LX/6Vi;

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/6Vi;)V

    .line 1830785
    const/4 v0, 0x0

    iput-object v0, p0, LX/BuJ;->D:LX/6Vi;

    .line 1830786
    :cond_0
    iget-object v0, p0, LX/BuJ;->c:LX/0bH;

    iget-object v1, p0, LX/BuJ;->J:LX/8qA;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830787
    iget-object v0, p0, LX/BuJ;->d:LX/1AM;

    iget-object v1, p0, LX/BuJ;->K:LX/19J;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830788
    iget-object v0, p0, LX/BuJ;->d:LX/1AM;

    iget-object v1, p0, LX/BuJ;->M:LX/19M;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830789
    iget-object v0, p0, LX/BuJ;->c:LX/0bH;

    iget-object v1, p0, LX/BuJ;->L:LX/1Kd;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830790
    return-void
.end method

.method public static k(LX/BuJ;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1830799
    iget-object v0, p0, LX/BuJ;->q:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1830800
    iget-object v3, p0, LX/BuJ;->E:LX/2pa;

    iget-wide v4, v3, LX/2pa;->d:D

    const-wide v6, 0x3fe251eb85000000L    # 0.5724999997764826

    cmpl-double v3, v4, v6

    if-gtz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-ne v0, v1, :cond_2

    :cond_0
    move v0, v2

    .line 1830801
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BuJ;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7I8;

    iget-boolean v0, v0, LX/7I8;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 1830802
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 1830803
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/16n;)V
    .locals 10

    .prologue
    const/4 v6, 0x4

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1830748
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830749
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830750
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830751
    iget-object v4, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830752
    iget-object v4, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f021a49

    :goto_1
    invoke-virtual {v4, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDrawable(I)V

    .line 1830753
    iget-object v4, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p0}, LX/BuJ;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f0a0458

    :goto_2
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 1830754
    iget-object v4, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/BuJ;->s:Ljava/lang/String;

    :goto_3
    invoke-virtual {v4, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1830755
    iget-object v4, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830756
    iget-object v1, p0, LX/BuJ;->v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v2

    :cond_0
    invoke-virtual {v1, v3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830757
    const/4 v9, 0x1

    .line 1830758
    invoke-interface {p1}, LX/16n;->k()I

    move-result v0

    .line 1830759
    if-gtz v0, :cond_a

    .line 1830760
    const/4 v0, 0x0

    .line 1830761
    :goto_5
    move-object v0, v0

    .line 1830762
    iget-object v1, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    .line 1830763
    iget-object v1, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1830764
    iget-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1830765
    :cond_1
    :goto_6
    const/4 v9, 0x1

    .line 1830766
    invoke-interface {p1}, LX/16n;->l()I

    move-result v0

    .line 1830767
    if-gtz v0, :cond_b

    .line 1830768
    const/4 v0, 0x0

    .line 1830769
    :goto_7
    move-object v0, v0

    .line 1830770
    iget-object v1, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_9

    if-eqz v0, :cond_9

    .line 1830771
    iget-object v1, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1830772
    iget-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1830773
    :cond_2
    :goto_8
    return-void

    :cond_3
    move v1, v3

    .line 1830774
    goto :goto_0

    .line 1830775
    :cond_4
    const v1, 0x7f021a48

    goto :goto_1

    .line 1830776
    :cond_5
    const v1, 0x7f0a0048

    goto :goto_2

    .line 1830777
    :cond_6
    iget-object v1, p0, LX/BuJ;->t:Ljava/lang/String;

    goto :goto_3

    :cond_7
    move v1, v3

    .line 1830778
    goto :goto_4

    .line 1830779
    :cond_8
    iget-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 1830780
    iget-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_6

    .line 1830781
    :cond_9
    iget-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_2

    .line 1830782
    iget-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_8

    :cond_a
    invoke-virtual {p0}, LX/BuJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080d75

    const v4, 0x7f080d76

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, LX/BuJ;->n:LX/154;

    invoke-virtual {v8, v0, v9}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v1, v3, v4, v0, v5}, LX/1z0;->a(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_b
    invoke-virtual {p0}, LX/BuJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080d77

    const v4, 0x7f080d78

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, LX/BuJ;->n:LX/154;

    invoke-virtual {v8, v0, v9}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v1, v3, v4, v0, v5}, LX/1z0;->a(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method public a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1830712
    iput-object p1, p0, LX/BuJ;->E:LX/2pa;

    .line 1830713
    if-eqz p2, :cond_0

    .line 1830714
    invoke-virtual {p0}, LX/BuJ;->g()V

    .line 1830715
    :cond_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1830716
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1830717
    if-eqz v0, :cond_2

    .line 1830718
    :cond_1
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1830719
    :goto_1
    return-void

    .line 1830720
    :cond_2
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1830721
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move-object v0, v1

    .line 1830722
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830723
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1830724
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1830725
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v1, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830726
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830727
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830728
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1830729
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830730
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830731
    check-cast v0, LX/16n;

    invoke-virtual {p0, v0}, LX/BuJ;->a(LX/16n;)V

    .line 1830732
    iget-object v0, p0, LX/BuJ;->C:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iget-object v1, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object p1, LX/1EO;->FULLSCREEN_VIDEO_PLAYER:LX/1EO;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;)V

    .line 1830733
    invoke-static {p0}, LX/BuJ;->j(LX/BuJ;)V

    .line 1830734
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830735
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830736
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830737
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830738
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1830739
    iget-object v1, p0, LX/BuJ;->e:LX/1K9;

    const-class p1, LX/82h;

    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830740
    iget-object p2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p2

    .line 1830741
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    iget-object p2, p0, LX/BuJ;->I:LX/6Ve;

    invoke-virtual {v1, p1, v0, p2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v0

    iput-object v0, p0, LX/BuJ;->D:LX/6Vi;

    .line 1830742
    iget-object v0, p0, LX/BuJ;->c:LX/0bH;

    iget-object v1, p0, LX/BuJ;->J:LX/8qA;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1830743
    iget-object v0, p0, LX/BuJ;->d:LX/1AM;

    iget-object v1, p0, LX/BuJ;->K:LX/19J;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1830744
    iget-object v0, p0, LX/BuJ;->d:LX/1AM;

    iget-object v1, p0, LX/BuJ;->M:LX/19M;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1830745
    iget-object v0, p0, LX/BuJ;->c:LX/0bH;

    iget-object v1, p0, LX/BuJ;->L:LX/1Kd;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1830746
    :cond_3
    goto/16 :goto_1

    .line 1830747
    :cond_4
    invoke-virtual {p0}, LX/2oy;->n()V

    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1830710
    invoke-static {p0}, LX/BuJ;->j(LX/BuJ;)V

    .line 1830711
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1830702
    iget-object v0, p0, LX/BuJ;->u:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830703
    iget-object v0, p0, LX/BuJ;->w:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830704
    iget-object v0, p0, LX/BuJ;->v:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1830705
    iget-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1830706
    iget-object v0, p0, LX/BuJ;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1830707
    :cond_0
    iget-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 1830708
    iget-object v0, p0, LX/BuJ;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1830709
    :cond_1
    return-void
.end method

.method public abstract getContentView()I
.end method
