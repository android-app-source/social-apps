.class public final LX/BHM;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:LX/BIG;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/controller/RecognitionManager;LX/BIG;J)V
    .locals 1

    .prologue
    .line 1768941
    iput-object p1, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iput-object p2, p0, LX/BHM;->a:LX/BIG;

    iput-wide p3, p0, LX/BHM;->b:J

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 1768942
    iget-object v0, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1768943
    iget-object v1, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->f:LX/0wL;

    iget-object v2, p0, LX/BHM;->a:LX/BIG;

    const v3, 0x7f081389

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1768944
    invoke-static {v1, v2, v0}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1768945
    if-eqz p1, :cond_0

    .line 1768946
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1768947
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x3c

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1768948
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 1768949
    :goto_0
    move-object v0, v0

    .line 1768950
    new-instance v1, LX/4cq;

    const-string v2, "image/jpeg"

    const-string v3, "image"

    invoke-direct {v1, v0, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1768951
    :try_start_0
    iget-object v0, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->b:Lcom/facebook/objectrec/manager/ObjectRecManager;

    invoke-virtual {v0, v1}, Lcom/facebook/objectrec/manager/ObjectRecManager;->a(LX/4cq;)Ljava/lang/String;

    move-result-object v0

    .line 1768952
    iget-object v1, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->f:LX/0wL;

    iget-object v2, p0, LX/BHM;->a:LX/BIG;

    .line 1768953
    invoke-static {v1, v2, v0}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1768954
    sget-object v1, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->a:Ljava/util/Map;

    iget-wide v2, p0, LX/BHM;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768955
    iget-object v1, p0, LX/BHM;->a:LX/BIG;

    invoke-virtual {v1, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1768956
    :goto_1
    return-void

    .line 1768957
    :catch_0
    move-exception v0

    .line 1768958
    iget-object v1, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08138a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1768959
    iget-object v2, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v2, v2, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->f:LX/0wL;

    iget-object v3, p0, LX/BHM;->a:LX/BIG;

    .line 1768960
    invoke-static {v2, v3, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1768961
    iget-object v1, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->e:LX/03V;

    const-string v2, "RecognitionManager"

    const-string v3, "RecognitionManager threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 3

    .prologue
    .line 1768962
    iget-object v0, p0, LX/BHM;->c:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->e:LX/03V;

    const-string v1, "RecognitionManager"

    const-string v2, "Failed to generate image from mediaItem"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768963
    return-void
.end method
