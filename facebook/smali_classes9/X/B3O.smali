.class public LX/B3O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740258
    return-void
.end method

.method public static a(LX/0QB;)LX/B3O;
    .locals 3

    .prologue
    .line 1740259
    const-class v1, LX/B3O;

    monitor-enter v1

    .line 1740260
    :try_start_0
    sget-object v0, LX/B3O;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1740261
    sput-object v2, LX/B3O;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1740262
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1740263
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1740264
    new-instance v0, LX/B3O;

    invoke-direct {v0}, LX/B3O;-><init>()V

    .line 1740265
    move-object v0, v0

    .line 1740266
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1740267
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B3O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740268
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1740269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;
    .locals 2

    .prologue
    .line 1740270
    iget-object v0, p0, LX/B3O;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    const-string v1, "Called getCameraModel() before initializing"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740271
    iget-object v0, p0, LX/B3O;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    return-object v0
.end method

.method public final a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V
    .locals 1

    .prologue
    .line 1740272
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    iput-object v0, p0, LX/B3O;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    .line 1740273
    return-void
.end method
