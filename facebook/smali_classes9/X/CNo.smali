.class public LX/CNo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNc;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 0

    .prologue
    .line 1882149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882150
    iput-object p1, p0, LX/CNo;->a:LX/CNb;

    .line 1882151
    iput-object p2, p0, LX/CNo;->b:LX/CNc;

    .line 1882152
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x4

    .line 1882134
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1882135
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1882136
    :goto_0
    iget-object v4, p0, LX/CNo;->a:LX/CNb;

    invoke-virtual {v4}, LX/CNb;->b()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 1882137
    iget-object v4, p0, LX/CNo;->a:LX/CNb;

    invoke-virtual {v4, v0}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 1882138
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_0

    invoke-virtual {v4, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "set-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1882139
    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1882140
    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1882141
    iget-object v5, p0, LX/CNo;->a:LX/CNb;

    invoke-virtual {v5, v4}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1882142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1882143
    :cond_1
    iget-object v0, p0, LX/CNo;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->a()V

    .line 1882144
    iget-object v0, p0, LX/CNo;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/CNo;->a:LX/CNb;

    const-string v4, "target-id"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1882145
    iget-object v4, v0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v4, v3}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v4

    .line 1882146
    invoke-virtual {v0, v1, v2, v4}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    .line 1882147
    iget-object v0, p0, LX/CNo;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->b()V

    .line 1882148
    return-void
.end method
