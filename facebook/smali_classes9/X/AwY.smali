.class public final LX/AwY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AwX;


# instance fields
.field public final synthetic a:LX/Awa;

.field public final synthetic b:LX/AwZ;


# direct methods
.method public constructor <init>(LX/AwZ;LX/Awa;)V
    .locals 0

    .prologue
    .line 1726708
    iput-object p1, p0, LX/AwY;->b:LX/AwZ;

    iput-object p2, p0, LX/AwY;->a:LX/Awa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 1726709
    iget-object v0, p0, LX/AwY;->a:LX/Awa;

    const/4 p0, 0x0

    .line 1726710
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726711
    iput-object p1, v1, LX/Awc;->l:Ljava/io/File;

    .line 1726712
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726713
    iput-object p2, v1, LX/Awc;->m:Ljava/io/File;

    .line 1726714
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726715
    iput-boolean p0, v1, LX/Awc;->s:Z

    .line 1726716
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    iget-boolean v1, v1, LX/Awc;->t:Z

    if-eqz v1, :cond_0

    .line 1726717
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726718
    iput-boolean p0, v1, LX/Awc;->t:Z

    .line 1726719
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    iget-object v1, v1, LX/Awc;->l:Ljava/io/File;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    iget-object v1, v1, LX/Awc;->m:Ljava/io/File;

    if-eqz v1, :cond_1

    .line 1726720
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    invoke-static {v1}, LX/Awc;->i(LX/Awc;)V

    .line 1726721
    :cond_0
    :goto_0
    return-void

    .line 1726722
    :cond_1
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    const-string p0, "Failed to load models"

    .line 1726723
    invoke-static {v1, p0}, LX/Awc;->b$redex0(LX/Awc;Ljava/lang/String;)V

    .line 1726724
    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1726725
    iget-object v0, p0, LX/AwY;->a:LX/Awa;

    const/4 p0, 0x0

    const/4 v2, 0x0

    .line 1726726
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726727
    iput-object p0, v1, LX/Awc;->l:Ljava/io/File;

    .line 1726728
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726729
    iput-object p0, v1, LX/Awc;->m:Ljava/io/File;

    .line 1726730
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726731
    iput-boolean v2, v1, LX/Awc;->s:Z

    .line 1726732
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    iget-boolean v1, v1, LX/Awc;->t:Z

    if-eqz v1, :cond_0

    .line 1726733
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    .line 1726734
    iput-boolean v2, v1, LX/Awc;->t:Z

    .line 1726735
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 1726736
    invoke-static {v1, v2}, LX/Awc;->b$redex0(LX/Awc;Ljava/lang/String;)V

    .line 1726737
    :goto_0
    return-void

    .line 1726738
    :cond_0
    iget-object v1, v0, LX/Awa;->a:LX/Awc;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 1726739
    iget-object p0, v1, LX/Awc;->f:LX/03V;

    sget-object v0, LX/Awc;->a:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/03V;->a(LX/0VG;)V

    .line 1726740
    goto :goto_0
.end method
