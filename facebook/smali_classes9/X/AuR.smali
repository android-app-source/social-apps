.class public final LX/AuR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AuS;


# direct methods
.method public constructor <init>(LX/AuS;)V
    .locals 0

    .prologue
    .line 1722581
    iput-object p1, p0, LX/AuR;->a:LX/AuS;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1722582
    iget-object v0, p0, LX/AuR;->a:LX/AuS;

    iget-object v0, v0, LX/AuS;->a:LX/AuT;

    iget-object v0, v0, LX/AuT;->a:Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a$redex0(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;Z)V

    .line 1722583
    iget-object v0, p0, LX/AuR;->a:LX/AuS;

    iget-object v0, v0, LX/AuS;->a:LX/AuT;

    iget-object v0, v0, LX/AuT;->a:Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V

    .line 1722584
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1722585
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 1722586
    iget-object v0, p0, LX/AuR;->a:LX/AuS;

    iget-object v0, v0, LX/AuS;->a:LX/AuT;

    iget-object v0, v0, LX/AuT;->a:Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    .line 1722587
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0is;

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Landroid/location/Location;)Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setLocation(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 1722588
    iget-object v0, p0, LX/AuR;->a:LX/AuS;

    iget-object v0, v0, LX/AuS;->a:LX/AuT;

    iget-object v0, v0, LX/AuT;->a:Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V

    .line 1722589
    return-void
.end method
