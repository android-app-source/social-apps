.class public LX/Ces;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/Ces;


# instance fields
.field public final b:LX/0aG;

.field public final c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field public final d:LX/0dC;

.field public final e:LX/2Gs;

.field public final f:LX/2Rl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925199
    const-class v0, LX/Ces;

    sput-object v0, LX/Ces;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0dC;LX/2Gs;LX/2Rl;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925201
    iput-object p1, p0, LX/Ces;->b:LX/0aG;

    .line 1925202
    iput-object p2, p0, LX/Ces;->c:Ljava/util/concurrent/ExecutorService;

    .line 1925203
    iput-object p3, p0, LX/Ces;->d:LX/0dC;

    .line 1925204
    iput-object p4, p0, LX/Ces;->e:LX/2Gs;

    .line 1925205
    iput-object p5, p0, LX/Ces;->f:LX/2Rl;

    .line 1925206
    return-void
.end method

.method public static a(LX/0QB;)LX/Ces;
    .locals 9

    .prologue
    .line 1925207
    sget-object v0, LX/Ces;->g:LX/Ces;

    if-nez v0, :cond_1

    .line 1925208
    const-class v1, LX/Ces;

    monitor-enter v1

    .line 1925209
    :try_start_0
    sget-object v0, LX/Ces;->g:LX/Ces;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925210
    if-eqz v2, :cond_0

    .line 1925211
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1925212
    new-instance v3, LX/Ces;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v6

    check-cast v6, LX/0dC;

    invoke-static {v0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v7

    check-cast v7, LX/2Gs;

    invoke-static {v0}, LX/2Rl;->a(LX/0QB;)LX/2Rl;

    move-result-object v8

    check-cast v8, LX/2Rl;

    invoke-direct/range {v3 .. v8}, LX/Ces;-><init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0dC;LX/2Gs;LX/2Rl;)V

    .line 1925213
    move-object v0, v3

    .line 1925214
    sput-object v0, LX/Ces;->g:LX/Ces;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925215
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925216
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925217
    :cond_1
    sget-object v0, LX/Ces;->g:LX/Ces;

    return-object v0

    .line 1925218
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
