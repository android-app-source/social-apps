.class public LX/CCs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9jH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1858419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858420
    iput-object p1, p0, LX/CCs;->a:LX/1vg;

    .line 1858421
    iput-object p2, p0, LX/CCs;->b:LX/0Ot;

    .line 1858422
    iput-object p3, p0, LX/CCs;->c:LX/0Ot;

    .line 1858423
    iput-object p4, p0, LX/CCs;->d:LX/0Ot;

    .line 1858424
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)I
    .locals 3
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1858436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1858437
    :goto_0
    return v0

    .line 1858438
    :cond_0
    sget-object v1, LX/CCq;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1858439
    :pswitch_0
    const v0, 0x7f020967

    goto :goto_0

    .line 1858440
    :pswitch_1
    const v0, 0x7f02091a

    goto :goto_0

    .line 1858441
    :pswitch_2
    const v0, 0x7f020972

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/CCs;
    .locals 7

    .prologue
    .line 1858425
    const-class v1, LX/CCs;

    monitor-enter v1

    .line 1858426
    :try_start_0
    sget-object v0, LX/CCs;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858427
    sput-object v2, LX/CCs;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858428
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858429
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858430
    new-instance v4, LX/CCs;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 v5, 0x3be

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xbc6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2f25

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/CCs;-><init>(LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1858431
    move-object v0, v4

    .line 1858432
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858433
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858434
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858435
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
