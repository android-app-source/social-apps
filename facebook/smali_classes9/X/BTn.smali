.class public LX/BTn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-com.google.common.util.concurrent.Futures.addCallback"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/BTn;


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/15V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1788001
    const-class v0, LX/BTn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BTn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/15V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787985
    iput-object p1, p0, LX/BTn;->b:LX/0tX;

    .line 1787986
    iput-object p2, p0, LX/BTn;->c:LX/15V;

    .line 1787987
    return-void
.end method

.method public static a(LX/0QB;)LX/BTn;
    .locals 5

    .prologue
    .line 1787988
    sget-object v0, LX/BTn;->d:LX/BTn;

    if-nez v0, :cond_1

    .line 1787989
    const-class v1, LX/BTn;

    monitor-enter v1

    .line 1787990
    :try_start_0
    sget-object v0, LX/BTn;->d:LX/BTn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1787991
    if-eqz v2, :cond_0

    .line 1787992
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1787993
    new-instance p0, LX/BTn;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/15V;->a(LX/0QB;)LX/15V;

    move-result-object v4

    check-cast v4, LX/15V;

    invoke-direct {p0, v3, v4}, LX/BTn;-><init>(LX/0tX;LX/15V;)V

    .line 1787994
    move-object v0, p0

    .line 1787995
    sput-object v0, LX/BTn;->d:LX/BTn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1787996
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1787997
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1787998
    :cond_1
    sget-object v0, LX/BTn;->d:LX/BTn;

    return-object v0

    .line 1787999
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1788000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
