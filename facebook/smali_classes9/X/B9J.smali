.class public LX/B9J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1751486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1751487
    iput-object p1, p0, LX/B9J;->a:LX/0Or;

    .line 1751488
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMegaphone;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1751489
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1751490
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1751491
    const-string v1, "tracking"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1751492
    :cond_0
    new-instance v1, Lcom/facebook/megaphone/api/LogMegaphoneParams;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2, v0}, Lcom/facebook/megaphone/api/LogMegaphoneParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1751493
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1751494
    const-string v0, "logMegaphoneParams"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1751495
    iget-object v0, p0, LX/B9J;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "log_megaphone"

    const v3, 0x5c31821f

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 1751496
    return-void
.end method
