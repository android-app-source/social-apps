.class public final enum LX/Acd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Acd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Acd;

.field public static final enum BROADCAST_INTERRUPTED:LX/Acd;

.field public static final enum BROADCAST_PAUSED:LX/Acd;

.field public static final enum PAUSED:LX/Acd;

.field public static final enum PLAYBACK_STALLED:LX/Acd;

.field public static final enum WEAK:LX/Acd;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1693006
    new-instance v0, LX/Acd;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v2}, LX/Acd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Acd;->WEAK:LX/Acd;

    .line 1693007
    new-instance v0, LX/Acd;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/Acd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Acd;->PAUSED:LX/Acd;

    .line 1693008
    new-instance v0, LX/Acd;

    const-string v1, "PLAYBACK_STALLED"

    invoke-direct {v0, v1, v4}, LX/Acd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Acd;->PLAYBACK_STALLED:LX/Acd;

    .line 1693009
    new-instance v0, LX/Acd;

    const-string v1, "BROADCAST_PAUSED"

    invoke-direct {v0, v1, v5}, LX/Acd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Acd;->BROADCAST_PAUSED:LX/Acd;

    .line 1693010
    new-instance v0, LX/Acd;

    const-string v1, "BROADCAST_INTERRUPTED"

    invoke-direct {v0, v1, v6}, LX/Acd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Acd;->BROADCAST_INTERRUPTED:LX/Acd;

    .line 1693011
    const/4 v0, 0x5

    new-array v0, v0, [LX/Acd;

    sget-object v1, LX/Acd;->WEAK:LX/Acd;

    aput-object v1, v0, v2

    sget-object v1, LX/Acd;->PAUSED:LX/Acd;

    aput-object v1, v0, v3

    sget-object v1, LX/Acd;->PLAYBACK_STALLED:LX/Acd;

    aput-object v1, v0, v4

    sget-object v1, LX/Acd;->BROADCAST_PAUSED:LX/Acd;

    aput-object v1, v0, v5

    sget-object v1, LX/Acd;->BROADCAST_INTERRUPTED:LX/Acd;

    aput-object v1, v0, v6

    sput-object v0, LX/Acd;->$VALUES:[LX/Acd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1693012
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Acd;
    .locals 1

    .prologue
    .line 1693013
    const-class v0, LX/Acd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Acd;

    return-object v0
.end method

.method public static values()[LX/Acd;
    .locals 1

    .prologue
    .line 1693014
    sget-object v0, LX/Acd;->$VALUES:[LX/Acd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Acd;

    return-object v0
.end method
