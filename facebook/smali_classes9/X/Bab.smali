.class public LX/Bab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799318
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1799319
    check-cast p1, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    .line 1799320
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "background-location-update-settings"

    .line 1799321
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1799322
    move-object v0, v0

    .line 1799323
    const-string v1, "POST"

    .line 1799324
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1799325
    move-object v0, v0

    .line 1799326
    const-string v1, "/me/location_privacy"

    .line 1799327
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1799328
    move-object v0, v0

    .line 1799329
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1799330
    iget-object v1, p1, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1799331
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "tracking"

    iget-object v1, p1, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, p0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1799332
    :cond_0
    iget-object v1, p1, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1799333
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "privacy"

    iget-object v1, p1, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, p0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1799334
    :cond_1
    move-object v1, v2

    .line 1799335
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1799336
    move-object v0, v0

    .line 1799337
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1799338
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1799339
    move-object v0, v0

    .line 1799340
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1799341
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
