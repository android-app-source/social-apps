.class public LX/BM6;
.super Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;
.source ""


# instance fields
.field private final a:LX/BM9;


# direct methods
.method public constructor <init>(LX/0Ot;LX/B5l;LX/BMC;LX/BM9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BMP;",
            ">;",
            "LX/B5l;",
            "LX/BMC;",
            "LX/BM9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776864
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;-><init>(LX/0Ot;LX/B5l;LX/BMC;)V

    .line 1776865
    iput-object p4, p0, LX/BM6;->a:LX/BM9;

    .line 1776866
    return-void
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 3

    .prologue
    .line 1776867
    iget-object v1, p0, LX/BM6;->a:LX/BM9;

    iget-object v0, p1, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1776868
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p0

    .line 1776869
    new-instance p2, LX/BM8;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object p0

    check-cast p0, LX/1bQ;

    invoke-static {v1}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object p1

    check-cast p1, LX/1E1;

    invoke-direct {p2, v0, v2, p0, p1}, LX/BM8;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;Landroid/content/Context;LX/1bQ;LX/1E1;)V

    .line 1776870
    move-object v0, p2

    .line 1776871
    invoke-static {v0}, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->a(LX/BM7;)LX/AlW;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1776872
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1776873
    instance-of v0, v0, LX/1kV;

    return v0
.end method
