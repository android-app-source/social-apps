.class public LX/AuG;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AuF;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722419
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1722420
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/Aqw;Landroid/view/ViewStub;)LX/AuF;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationTopBarController$Delegate;",
            "Landroid/view/ViewStub;",
            ")",
            "LX/AuF",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1722417
    new-instance v0, LX/AuF;

    move-object/from16 v1, p1

    check-cast v1, LX/0il;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v5

    check-cast v5, LX/23P;

    const-class v2, LX/AuJ;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AuJ;

    const-class v2, LX/At1;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/At1;

    const-class v2, LX/Au3;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Au3;

    const-class v2, LX/AsX;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/AsX;

    const-class v2, LX/AtV;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/AtV;

    const-class v2, LX/Atk;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Atk;

    const-class v2, LX/AtX;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AtX;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v13

    check-cast v13, Lcom/facebook/user/model/User;

    invoke-static {p0}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v14

    check-cast v14, LX/FJv;

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v14}, LX/AuF;-><init>(LX/0il;LX/Aqw;Landroid/view/ViewStub;Landroid/content/Context;LX/23P;LX/AuJ;LX/At1;LX/Au3;LX/AsX;LX/AtV;LX/Atk;LX/AtX;Lcom/facebook/user/model/User;LX/FJv;)V

    .line 1722418
    return-object v0
.end method
