.class public LX/AWe;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/AVc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683165
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683166
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683167
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683168
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683169
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683170
    const-class v0, LX/AWe;

    invoke-static {v0, p0}, LX/AWe;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683171
    const v0, 0x7f0305a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683172
    const v0, 0x7f0d0f7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AWe;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1683173
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AWe;

    invoke-static {p0}, LX/AVc;->b(LX/0QB;)LX/AVc;

    move-result-object p0

    check-cast p0, LX/AVc;

    iput-object p0, p1, LX/AWe;->a:LX/AVc;

    return-void
.end method


# virtual methods
.method public final iF_()V
    .locals 4

    .prologue
    .line 1683174
    iget-object v0, p0, LX/AWe;->a:LX/AVc;

    iget-object v1, p0, LX/AWe;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1683175
    iget-object v2, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, v0, LX/AVc;->b:LX/0Tn;

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1683176
    :goto_0
    return-void

    .line 1683177
    :cond_0
    iget-object v2, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, v0, LX/AVc;->b:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1683178
    new-instance v3, LX/0hs;

    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/4 p0, 0x2

    invoke-direct {v3, v2, p0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1683179
    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f080cfc

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1683180
    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f080cfd

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1683181
    invoke-virtual {v3, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1683182
    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v2}, LX/0ht;->a(LX/3AV;)V

    .line 1683183
    const/4 v2, -0x1

    .line 1683184
    iput v2, v3, LX/0hs;->t:I

    .line 1683185
    invoke-virtual {v3}, LX/0ht;->d()V

    goto :goto_0
.end method
