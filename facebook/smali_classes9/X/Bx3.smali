.class public final LX/Bx3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bx5;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public c:LX/1Pm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Bx5;


# direct methods
.method public constructor <init>(LX/Bx5;)V
    .locals 1

    .prologue
    .line 1834814
    iput-object p1, p0, LX/Bx3;->d:LX/Bx5;

    .line 1834815
    move-object v0, p1

    .line 1834816
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1834817
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1834818
    const-string v0, "ArticleChainingItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1834819
    if-ne p0, p1, :cond_1

    .line 1834820
    :cond_0
    :goto_0
    return v0

    .line 1834821
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1834822
    goto :goto_0

    .line 1834823
    :cond_3
    check-cast p1, LX/Bx3;

    .line 1834824
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1834825
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1834826
    if-eq v2, v3, :cond_0

    .line 1834827
    iget-object v2, p0, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1834828
    goto :goto_0

    .line 1834829
    :cond_5
    iget-object v2, p1, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1834830
    :cond_6
    iget-object v2, p0, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1834831
    goto :goto_0

    .line 1834832
    :cond_8
    iget-object v2, p1, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_7

    .line 1834833
    :cond_9
    iget-object v2, p0, LX/Bx3;->c:LX/1Pm;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Bx3;->c:LX/1Pm;

    iget-object v3, p1, LX/Bx3;->c:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1834834
    goto :goto_0

    .line 1834835
    :cond_a
    iget-object v2, p1, LX/Bx3;->c:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
