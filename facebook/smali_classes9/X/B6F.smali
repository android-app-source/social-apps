.class public final LX/B6F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenConfirmationFragment;)V
    .locals 0

    .prologue
    .line 1746390
    iput-object p1, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1746391
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->h:LX/B6p;

    new-instance v2, LX/B6E;

    invoke-direct {v2, p0}, LX/B6E;-><init>(LX/B6F;)V

    iget-object v3, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/B6p;->a(LX/B6D;Landroid/content/Context;)LX/B6o;

    move-result-object v1

    .line 1746392
    iput-object v1, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    .line 1746393
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    iget-object v1, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->u()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/B6o;->a(Z)V

    .line 1746394
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    invoke-virtual {v0}, LX/B6o;->a()V

    .line 1746395
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1746396
    iput-object v1, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->s:Landroid/view/ViewTreeObserver;

    .line 1746397
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->s:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1746398
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1746399
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->s:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1746400
    :goto_0
    return-void

    .line 1746401
    :cond_0
    iget-object v0, p0, LX/B6F;->a:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->s:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
