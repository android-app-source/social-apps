.class public LX/BA4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private b:LX/7ex;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752683
    iput-object p1, p0, LX/BA4;->a:LX/0ad;

    .line 1752684
    return-void
.end method

.method public static a(LX/0QB;)LX/BA4;
    .locals 1

    .prologue
    .line 1752681
    invoke-static {p0}, LX/BA4;->b(LX/0QB;)LX/BA4;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BA4;
    .locals 2

    .prologue
    .line 1752671
    new-instance v1, LX/BA4;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/BA4;-><init>(LX/0ad;)V

    .line 1752672
    return-object v1
.end method


# virtual methods
.method public final a()LX/7ex;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1752673
    iget-object v0, p0, LX/BA4;->b:LX/7ex;

    if-nez v0, :cond_0

    .line 1752674
    iget-object v0, p0, LX/BA4;->a:LX/0ad;

    sget-short v2, LX/BA3;->f:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1752675
    iget-object v2, p0, LX/BA4;->a:LX/0ad;

    sget v3, LX/BA3;->a:I

    const/16 v4, 0x7530

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 1752676
    iget-object v3, p0, LX/BA4;->a:LX/0ad;

    sget v4, LX/BA3;->c:I

    const v5, 0x11170

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    .line 1752677
    iget-object v4, p0, LX/BA4;->a:LX/0ad;

    sget v5, LX/BA3;->b:I

    const/16 v6, 0x3a98

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    .line 1752678
    iget-object v5, p0, LX/BA4;->a:LX/0ad;

    sget-short v6, LX/BA3;->e:S

    invoke-interface {v5, v6, v1}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 1752679
    invoke-static/range {v0 .. v5}, LX/7ex;->a(ZZIIIZ)LX/7ex;

    move-result-object v0

    iput-object v0, p0, LX/BA4;->b:LX/7ex;

    .line 1752680
    :cond_0
    iget-object v0, p0, LX/BA4;->b:LX/7ex;

    return-object v0
.end method
