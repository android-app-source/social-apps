.class public final LX/Axr;
.super Landroid/database/ContentObserver;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Axr;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/0Ot;LX/0Sh;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728921
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1728922
    iput-object p2, p0, LX/Axr;->a:LX/0Ot;

    .line 1728923
    iput-object p3, p0, LX/Axr;->b:LX/0Sh;

    .line 1728924
    return-void
.end method

.method public static a(LX/0QB;)LX/Axr;
    .locals 6

    .prologue
    .line 1728925
    sget-object v0, LX/Axr;->c:LX/Axr;

    if-nez v0, :cond_1

    .line 1728926
    const-class v1, LX/Axr;

    monitor-enter v1

    .line 1728927
    :try_start_0
    sget-object v0, LX/Axr;->c:LX/Axr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1728928
    if-eqz v2, :cond_0

    .line 1728929
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1728930
    new-instance v5, LX/Axr;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    const/16 v4, 0x22bd

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {v5, v3, p0, v4}, LX/Axr;-><init>(Landroid/os/Handler;LX/0Ot;LX/0Sh;)V

    .line 1728931
    move-object v0, v5

    .line 1728932
    sput-object v0, LX/Axr;->c:LX/Axr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728933
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1728934
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1728935
    :cond_1
    sget-object v0, LX/Axr;->c:LX/Axr;

    return-object v0

    .line 1728936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1728937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 2

    .prologue
    .line 1728938
    iget-object v0, p0, LX/Axr;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1728939
    iget-object v0, p0, LX/Axr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axq;

    const/4 v1, 0x1

    .line 1728940
    iput-boolean v1, v0, LX/Axq;->a:Z

    .line 1728941
    return-void
.end method
