.class public final LX/CPD;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/CPF;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/CPE;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1884128
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "template"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "templateContext"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "wrappers"

    aput-object v2, v0, v1

    sput-object v0, LX/CPD;->b:[Ljava/lang/String;

    .line 1884129
    sput v3, LX/CPD;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1884130
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1884131
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/CPD;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CPD;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CPD;LX/1De;IILX/CPE;)V
    .locals 1

    .prologue
    .line 1884132
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1884133
    iput-object p4, p0, LX/CPD;->a:LX/CPE;

    .line 1884134
    iget-object v0, p0, LX/CPD;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1884135
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1884136
    invoke-super {p0}, LX/1X5;->a()V

    .line 1884137
    const/4 v0, 0x0

    iput-object v0, p0, LX/CPD;->a:LX/CPE;

    .line 1884138
    sget-object v0, LX/CPF;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1884139
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/CPF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1884140
    iget-object v1, p0, LX/CPD;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CPD;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/CPD;->c:I

    if-ge v1, v2, :cond_2

    .line 1884141
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1884142
    :goto_0
    sget v2, LX/CPD;->c:I

    if-ge v0, v2, :cond_1

    .line 1884143
    iget-object v2, p0, LX/CPD;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1884144
    sget-object v2, LX/CPD;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1884145
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1884146
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1884147
    :cond_2
    iget-object v0, p0, LX/CPD;->a:LX/CPE;

    .line 1884148
    invoke-virtual {p0}, LX/CPD;->a()V

    .line 1884149
    return-object v0
.end method
