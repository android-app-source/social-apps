.class public LX/BEF;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BEG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AEn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1764250
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BEF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1764251
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1764252
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1764253
    const-class v0, LX/BEF;

    invoke-static {v0, p0}, LX/BEF;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1764254
    const v0, 0x7f030c65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1764255
    const v0, 0x7f0a00f7

    invoke-virtual {p0, v0}, LX/BEF;->setBackgroundResource(I)V

    .line 1764256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/BEF;->setOrientation(I)V

    .line 1764257
    const v0, 0x7f0d1e7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/BED;

    invoke-direct {v1, p0}, LX/BED;-><init>(LX/BEF;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1764258
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/BEF;

    invoke-static {v2}, LX/BEG;->a(LX/0QB;)LX/BEG;

    move-result-object v1

    check-cast v1, LX/BEG;

    const/16 p0, 0x1399

    invoke-static {v2, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object v1, p1, LX/BEF;->a:LX/BEG;

    iput-object v2, p1, LX/BEF;->b:LX/0Or;

    return-void
.end method
