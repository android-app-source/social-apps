.class public final LX/CE6;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CE6;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CE4;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CE7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1860596
    const/4 v0, 0x0

    sput-object v0, LX/CE6;->a:LX/CE6;

    .line 1860597
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CE6;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1860598
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1860599
    new-instance v0, LX/CE7;

    invoke-direct {v0}, LX/CE7;-><init>()V

    iput-object v0, p0, LX/CE6;->c:LX/CE7;

    .line 1860600
    return-void
.end method

.method public static c(LX/1De;)LX/CE4;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1860588
    new-instance v1, LX/CE5;

    invoke-direct {v1}, LX/CE5;-><init>()V

    .line 1860589
    sget-object v2, LX/CE6;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CE4;

    .line 1860590
    if-nez v2, :cond_0

    .line 1860591
    new-instance v2, LX/CE4;

    invoke-direct {v2}, LX/CE4;-><init>()V

    .line 1860592
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/CE4;->a$redex0(LX/CE4;LX/1De;IILX/CE5;)V

    .line 1860593
    move-object v1, v2

    .line 1860594
    move-object v0, v1

    .line 1860595
    return-object v0
.end method

.method public static declared-synchronized q()LX/CE6;
    .locals 2

    .prologue
    .line 1860601
    const-class v1, LX/CE6;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CE6;->a:LX/CE6;

    if-nez v0, :cond_0

    .line 1860602
    new-instance v0, LX/CE6;

    invoke-direct {v0}, LX/CE6;-><init>()V

    sput-object v0, LX/CE6;->a:LX/CE6;

    .line 1860603
    :cond_0
    sget-object v0, LX/CE6;->a:LX/CE6;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1860604
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1860586
    invoke-static {}, LX/1dS;->b()V

    .line 1860587
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1860573
    check-cast p4, LX/CE5;

    .line 1860574
    iget-object v0, p4, LX/CE5;->a:Ljava/lang/CharSequence;

    iget-object v1, p4, LX/CE5;->b:LX/0Or;

    .line 1860575
    invoke-static {p1, v0}, LX/CE7;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v2

    .line 1860576
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result p0

    if-eqz p0, :cond_0

    if-nez v1, :cond_1

    .line 1860577
    :cond_0
    invoke-static {p1, v2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 1860578
    :goto_0
    move-object v0, v2

    .line 1860579
    return-object v0

    .line 1860580
    :cond_1
    new-instance p0, LX/1no;

    invoke-direct {p0}, LX/1no;-><init>()V

    .line 1860581
    invoke-virtual {v2, p1, p2, p3, p0}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1860582
    iget p0, p0, LX/1no;->a:I

    invoke-static {p2}, LX/1mh;->b(I)I

    move-result p4

    if-lt p0, p4, :cond_2

    .line 1860583
    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {p1, v2}, LX/CE7;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v2

    invoke-static {p1, v2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0

    .line 1860584
    :cond_2
    invoke-static {p1, v2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1860585
    const/4 v0, 0x1

    return v0
.end method
