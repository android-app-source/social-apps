.class public final LX/BnU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1820790
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1820791
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820792
    :goto_0
    return v1

    .line 1820793
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820794
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1820795
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1820796
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1820797
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1820798
    const-string v6, "coverPhotoImage"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1820799
    const/4 v5, 0x0

    .line 1820800
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_a

    .line 1820801
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820802
    :goto_2
    move v4, v5

    .line 1820803
    goto :goto_1

    .line 1820804
    :cond_2
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1820805
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1820806
    :cond_3
    const-string v6, "themeListImage"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1820807
    const/4 v5, 0x0

    .line 1820808
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 1820809
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820810
    :goto_3
    move v2, v5

    .line 1820811
    goto :goto_1

    .line 1820812
    :cond_4
    const-string v6, "theme_tags"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1820813
    const/4 v5, 0x0

    .line 1820814
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_13

    .line 1820815
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820816
    :goto_4
    move v0, v5

    .line 1820817
    goto :goto_1

    .line 1820818
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1820819
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1820820
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1820821
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1820822
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1820823
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 1820824
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820825
    :cond_8
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1820826
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1820827
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1820828
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 1820829
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1820830
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_5

    .line 1820831
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1820832
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1820833
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_a
    move v4, v5

    goto :goto_5

    .line 1820834
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820835
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_d

    .line 1820836
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1820837
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1820838
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_c

    if-eqz v6, :cond_c

    .line 1820839
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1820840
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_6

    .line 1820841
    :cond_d
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1820842
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 1820843
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_e
    move v2, v5

    goto :goto_6

    .line 1820844
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1820845
    :cond_10
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_12

    .line 1820846
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1820847
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1820848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_10

    if-eqz v6, :cond_10

    .line 1820849
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1820850
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1820851
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_11

    .line 1820852
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_11

    .line 1820853
    invoke-static {p0, p1}, LX/BnT;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1820854
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1820855
    :cond_11
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1820856
    goto :goto_7

    .line 1820857
    :cond_12
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1820858
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1820859
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_13
    move v0, v5

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1820860
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1820861
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1820862
    if-eqz v0, :cond_1

    .line 1820863
    const-string v1, "coverPhotoImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820864
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1820865
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1820866
    if-eqz v1, :cond_0

    .line 1820867
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820868
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1820869
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1820870
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1820871
    if-eqz v0, :cond_2

    .line 1820872
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820873
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1820874
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1820875
    if-eqz v0, :cond_4

    .line 1820876
    const-string v1, "themeListImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820877
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1820878
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1820879
    if-eqz v1, :cond_3

    .line 1820880
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820881
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1820882
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1820883
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1820884
    if-eqz v0, :cond_7

    .line 1820885
    const-string v1, "theme_tags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820886
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1820887
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1820888
    if-eqz v1, :cond_6

    .line 1820889
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820890
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1820891
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1820892
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2}, LX/BnT;->a(LX/15i;ILX/0nX;)V

    .line 1820893
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1820894
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1820895
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1820896
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1820897
    return-void
.end method
