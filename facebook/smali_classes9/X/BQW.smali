.class public final LX/BQW;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BQX;


# direct methods
.method public constructor <init>(LX/BQX;)V
    .locals 0

    .prologue
    .line 1782308
    iput-object p1, p0, LX/BQW;->a:LX/BQX;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1782352
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1782309
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 1782310
    sget-object v0, LX/BQU;->a:[I

    .line 1782311
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782312
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v2

    .line 1782313
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1782314
    :goto_0
    return-void

    .line 1782315
    :pswitch_0
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->i:Ljava/lang/String;

    .line 1782316
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782317
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782318
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1782319
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    .line 1782320
    const/4 v3, 0x0

    .line 1782321
    iput-object v3, v0, LX/BQX;->i:Ljava/lang/String;

    .line 1782322
    iget-object v1, v0, LX/BQX;->k:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1782323
    iget-object v1, v0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/BQX;->k:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782324
    iput-object v3, v0, LX/BQX;->k:Ljava/lang/Runnable;

    .line 1782325
    :cond_0
    invoke-static {v0}, LX/BQX;->c(LX/BQX;)V

    .line 1782326
    :cond_1
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->f:LX/BQQ;

    .line 1782327
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782328
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782329
    invoke-virtual {v0, v1}, LX/BQQ;->a(Ljava/lang/String;)V

    .line 1782330
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->d:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->b()V

    goto :goto_0

    .line 1782331
    :pswitch_1
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-boolean v0, v0, LX/BQX;->m:Z

    if-eqz v0, :cond_2

    .line 1782332
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->g:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/BQW;->a:LX/BQX;

    iget-object v2, v2, LX/BQX;->h:LX/0ad;

    sget-char v3, LX/AzO;->a:C

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1782333
    :cond_2
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->j:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->j:Ljava/lang/String;

    .line 1782334
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782335
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782336
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const-string v0, "unsupportedSessionId"

    .line 1782337
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782338
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782339
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1782340
    :cond_4
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    .line 1782341
    const/4 v3, 0x0

    .line 1782342
    iput-object v3, v0, LX/BQX;->j:Ljava/lang/String;

    .line 1782343
    iget-object v1, v0, LX/BQX;->l:Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    .line 1782344
    iget-object v1, v0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/BQX;->l:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782345
    iput-object v3, v0, LX/BQX;->l:Ljava/lang/Runnable;

    .line 1782346
    :cond_5
    invoke-static {v0}, LX/BQX;->c(LX/BQX;)V

    .line 1782347
    :cond_6
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->f:LX/BQQ;

    .line 1782348
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782349
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782350
    invoke-virtual {v0, v1}, LX/BQQ;->b(Ljava/lang/String;)V

    .line 1782351
    iget-object v0, p0, LX/BQW;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->d:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->d()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
