.class public final LX/CJk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;)V
    .locals 0

    .prologue
    .line 1875719
    iput-object p1, p0, LX/CJk;->a:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x48e9cb67

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1875713
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1875714
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1875715
    iget-object v1, v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    .line 1875716
    :goto_0
    new-instance v3, LX/6lk;

    const-string v4, "xma_action_cta_clicked"

    const/4 v5, 0x0

    iget-object v6, p0, LX/CJk;->a:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    iget-object v6, v6, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->f:Ljava/lang/String;

    invoke-static {v0, v5, v1, v6}, LX/CJo;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    new-instance v5, LX/CJj;

    invoke-direct {v5, p0, v0, p1}, LX/CJj;-><init>(LX/CJk;Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;Landroid/view/View;)V

    invoke-direct {v3, v4, v1, v5}, LX/6lk;-><init>(Ljava/lang/String;Landroid/os/Bundle;LX/CJj;)V

    .line 1875717
    const v0, -0x403950c

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1875718
    :cond_0
    iget-object v1, p0, LX/CJk;->a:Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;

    iget-object v1, v1, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->e:Landroid/net/Uri;

    goto :goto_0
.end method
