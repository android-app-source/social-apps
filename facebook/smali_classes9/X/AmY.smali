.class public LX/AmY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AmY;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710926
    iput-object p1, p0, LX/AmY;->a:Landroid/content/res/Resources;

    .line 1710927
    return-void
.end method

.method public static a(LX/0QB;)LX/AmY;
    .locals 4

    .prologue
    .line 1710907
    sget-object v0, LX/AmY;->b:LX/AmY;

    if-nez v0, :cond_1

    .line 1710908
    const-class v1, LX/AmY;

    monitor-enter v1

    .line 1710909
    :try_start_0
    sget-object v0, LX/AmY;->b:LX/AmY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1710910
    if-eqz v2, :cond_0

    .line 1710911
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1710912
    new-instance p0, LX/AmY;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/AmY;-><init>(Landroid/content/res/Resources;)V

    .line 1710913
    move-object v0, p0

    .line 1710914
    sput-object v0, LX/AmY;->b:LX/AmY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1710915
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1710916
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1710917
    :cond_1
    sget-object v0, LX/AmY;->b:LX/AmY;

    return-object v0

    .line 1710918
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1710919
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 1710920
    const-wide/high16 v0, 0x3fd0000000000000L    # 0.25

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 1710921
    iget-object v1, p0, LX/AmY;->a:Landroid/content/res/Resources;

    const v2, 0x7f0214b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1710922
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    div-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1710923
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1710924
    return-void
.end method
