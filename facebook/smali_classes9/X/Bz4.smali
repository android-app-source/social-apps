.class public final LX/Bz4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9hN;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

.field public final synthetic b:LX/1bf;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;)V
    .locals 0

    .prologue
    .line 1838057
    iput-object p1, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iput-object p2, p0, LX/Bz4;->b:LX/1bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hO;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1838058
    iget-object v0, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1838059
    iget-object v1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    move-object v0, v1

    .line 1838060
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 1838061
    :goto_0
    return-object v0

    .line 1838062
    :cond_1
    iget-object v0, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachments()LX/0Px;

    move-result-object v3

    .line 1838063
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1838064
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    invoke-virtual {v0}, LX/26M;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1838065
    iget-object v0, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->d(I)LX/1af;

    move-result-object v0

    iget-object v2, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v3, p0, LX/Bz4;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v3, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {v0, v2, v1}, LX/9hP;->a(LX/1af;Landroid/view/View;Landroid/graphics/Rect;)LX/9hP;

    move-result-object v1

    .line 1838066
    new-instance v0, LX/9hO;

    iget-object v2, p0, LX/Bz4;->b:LX/1bf;

    invoke-direct {v0, v1, v2}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    goto :goto_0

    .line 1838067
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 1838068
    goto :goto_0
.end method
