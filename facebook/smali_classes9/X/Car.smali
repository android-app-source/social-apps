.class public final LX/Car;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JH;


# instance fields
.field public final synthetic a:LX/Caz;


# direct methods
.method public constructor <init>(LX/Caz;)V
    .locals 0

    .prologue
    .line 1918651
    iput-object p1, p0, LX/Car;->a:LX/Caz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)V
    .locals 11

    .prologue
    .line 1918652
    iget-object v0, p0, LX/Car;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->r:LX/Cau;

    .line 1918653
    iget-object v1, v0, LX/Cau;->a:LX/Caz;

    iget-object v1, v1, LX/Caz;->c:LX/Cb1;

    .line 1918654
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918655
    iget-object v2, v1, LX/Cb1;->d:LX/0Ri;

    invoke-interface {v2, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918656
    iget-object v2, v1, LX/Cb1;->d:LX/0Ri;

    invoke-interface {v2, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    move-object v1, v2

    .line 1918657
    iget-object v2, v0, LX/Cau;->a:LX/Caz;

    invoke-virtual {v2}, LX/Caz;->a()LX/CbL;

    move-result-object v2

    .line 1918658
    iget-object v3, v0, LX/Cau;->a:LX/Caz;

    iget-object v3, v3, LX/Caz;->c:LX/Cb1;

    invoke-virtual {v3}, LX/Cb1;->a()LX/0Rh;

    move-result-object v3

    invoke-virtual {v3}, LX/0Rh;->f()LX/0Rf;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/CbL;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;Ljava/util/Collection;)V

    .line 1918659
    iget-object v1, v0, LX/Cau;->a:LX/Caz;

    invoke-static {v1}, LX/Caz;->o(LX/Caz;)V

    .line 1918660
    iget-object v10, v0, LX/Cau;->a:LX/Caz;

    iget-object v1, v0, LX/Cau;->a:LX/Caz;

    iget-object v1, v1, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, v0, LX/Cau;->a:LX/Caz;

    iget-object v3, v3, LX/Caz;->h:LX/0Ot;

    iget-object v4, v0, LX/Cau;->a:LX/Caz;

    iget-object v4, v4, LX/Caz;->d:LX/9hh;

    iget-object v5, v0, LX/Cau;->a:LX/Caz;

    iget-object v5, v5, LX/Caz;->q:LX/5kD;

    invoke-interface {v5}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/Cau;->a:LX/Caz;

    invoke-static {v6}, LX/Caz;->l(LX/Caz;)LX/Caw;

    move-result-object v6

    iget-object v7, v0, LX/Cau;->a:LX/Caz;

    invoke-static {v7}, LX/Caz;->p(LX/Caz;)LX/8JW;

    move-result-object v7

    iget-object v8, v0, LX/Cau;->a:LX/Caz;

    iget-object v8, v8, LX/Caz;->w:LX/8Jo;

    const/4 v9, 0x1

    invoke-static/range {v1 .. v9}, LX/Cb5;->a(Landroid/content/Context;LX/CbL;LX/0Ot;LX/9hh;Ljava/lang/String;LX/Caw;LX/8JW;LX/8Jo;Z)LX/Cb5;

    move-result-object v1

    .line 1918661
    iput-object v1, v10, LX/Caz;->s:LX/Cb5;

    .line 1918662
    return-void
.end method
