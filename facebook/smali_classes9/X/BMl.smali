.class public final LX/BMl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BMm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Ri;

.field public final synthetic b:LX/BMm;


# direct methods
.method public constructor <init>(LX/BMm;)V
    .locals 1

    .prologue
    .line 1777909
    iput-object p1, p0, LX/BMl;->b:LX/BMm;

    .line 1777910
    move-object v0, p1

    .line 1777911
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1777912
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1777908
    const-string v0, "PromptChevronMenuComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1777897
    if-ne p0, p1, :cond_1

    .line 1777898
    :cond_0
    :goto_0
    return v0

    .line 1777899
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1777900
    goto :goto_0

    .line 1777901
    :cond_3
    check-cast p1, LX/BMl;

    .line 1777902
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1777903
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1777904
    if-eq v2, v3, :cond_0

    .line 1777905
    iget-object v2, p0, LX/BMl;->a:LX/1Ri;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/BMl;->a:LX/1Ri;

    iget-object v3, p1, LX/BMl;->a:LX/1Ri;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1777906
    goto :goto_0

    .line 1777907
    :cond_4
    iget-object v2, p1, LX/BMl;->a:LX/1Ri;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
