.class public final LX/BFx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fY;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766453
    iput-object p1, p0, LX/BFx;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5fd;Landroid/graphics/Point;)V
    .locals 3

    .prologue
    .line 1766454
    sget-object v0, LX/BFv;->a:[I

    invoke-virtual {p1}, LX/5fd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1766455
    :goto_0
    return-void

    .line 1766456
    :pswitch_0
    iget-object v0, p0, LX/BFx;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->q:Lcom/facebook/photos/creativecam/ui/FocusView;

    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/creativecam/ui/FocusView;->a(II)V

    goto :goto_0

    .line 1766457
    :pswitch_1
    iget-object v0, p0, LX/BFx;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->q:Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->a()V

    goto :goto_0

    .line 1766458
    :pswitch_2
    iget-object v0, p0, LX/BFx;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->q:Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->b()V

    goto :goto_0

    .line 1766459
    :pswitch_3
    iget-object v0, p0, LX/BFx;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->q:Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
