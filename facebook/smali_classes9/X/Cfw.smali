.class public LX/Cfw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1vj;

.field public final b:LX/1vo;


# direct methods
.method public constructor <init>(LX/1vj;LX/1vo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1927038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1927039
    iput-object p1, p0, LX/Cfw;->a:LX/1vj;

    .line 1927040
    iput-object p2, p0, LX/Cfw;->b:LX/1vo;

    .line 1927041
    return-void
.end method

.method public static a(LX/0QB;)LX/Cfw;
    .locals 1

    .prologue
    .line 1927037
    invoke-static {p0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Cfw;
    .locals 3

    .prologue
    .line 1927035
    new-instance v2, LX/Cfw;

    invoke-static {p0}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v0

    check-cast v0, LX/1vj;

    invoke-static {p0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v1

    check-cast v1, LX/1vo;

    invoke-direct {v2, v0, v1}, LX/Cfw;-><init>(LX/1vj;LX/1vo;)V

    .line 1927036
    return-object v2
.end method

.method public static b(LX/9qX;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1927016
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1927017
    :cond_1
    :goto_0
    return v0

    .line 1927018
    :cond_2
    sget-object v2, LX/Cfv;->b:[I

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 1927019
    goto :goto_0

    .line 1927020
    :pswitch_0
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 1927021
    :pswitch_1
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto/16 :goto_0

    .line 1927022
    :pswitch_2
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_5
    move v0, v1

    goto/16 :goto_0

    .line 1927023
    :pswitch_3
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_6
    move v0, v1

    goto/16 :goto_0

    .line 1927024
    :pswitch_4
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 1927025
    :pswitch_5
    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {p0}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object v2

    const/4 v5, 0x0

    .line 1927026
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1927027
    :cond_8
    :goto_1
    move v2, v5

    .line 1927028
    if-nez v2, :cond_1

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v4, v5

    .line 1927029
    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_b

    .line 1927030
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5sX;

    .line 1927031
    invoke-interface {v3}, LX/5sX;->d()LX/5sY;

    move-result-object p0

    .line 1927032
    if-eqz p0, :cond_8

    invoke-interface {p0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_8

    invoke-interface {v3}, LX/5sX;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1927033
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1927034
    :cond_b
    const/4 v5, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static c(LX/Cfw;LX/9qX;)LX/Cfx;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1927001
    invoke-interface {p1}, LX/9qX;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v3

    .line 1927002
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    .line 1927003
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    if-ne v4, v5, :cond_2

    :cond_1
    move v0, v1

    .line 1927004
    :cond_2
    if-nez v2, :cond_4

    if-eqz v0, :cond_4

    .line 1927005
    new-instance v0, LX/Cfx;

    const-string v1, "NO_STYLES_PROVIDED"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    .line 1927006
    :goto_1
    return-object v0

    :cond_3
    move v2, v0

    .line 1927007
    goto :goto_0

    .line 1927008
    :cond_4
    if-eqz v2, :cond_5

    if-nez v0, :cond_5

    .line 1927009
    new-instance v0, LX/Cfx;

    const-string v1, "EMPTY_ATTACHMENTS"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1927010
    :cond_5
    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    .line 1927011
    invoke-static {p1}, LX/Cfw;->b(LX/9qX;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, LX/Cfx;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    new-instance v0, LX/Cfx;

    const-string v1, "NO_CONTENT"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1927012
    :cond_7
    iget-object v0, p0, LX/Cfw;->a:LX/1vj;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1vj;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfk;

    move-result-object v1

    .line 1927013
    if-eqz v1, :cond_8

    invoke-virtual {v1, v3}, LX/Cfk;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1927014
    new-instance v0, LX/Cfx;

    const-string v2, "SUCCESS"

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, LX/Cfx;-><init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    goto :goto_1

    .line 1927015
    :cond_8
    new-instance v0, LX/Cfx;

    const-string v1, "NO_SUPPORTED_STYLE"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/9qX;)LX/Cfx;
    .locals 7

    .prologue
    .line 1926958
    invoke-interface {p1}, LX/9qX;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 1926959
    if-eqz v0, :cond_4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v2

    if-eq v1, v2, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v0

    if-ne v1, v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1926960
    if-eqz v0, :cond_1

    .line 1926961
    new-instance v0, LX/Cfx;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    .line 1926962
    :goto_1
    return-object v0

    .line 1926963
    :cond_1
    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v0

    .line 1926964
    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object v1

    .line 1926965
    invoke-interface {p1}, LX/9qX;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v2

    .line 1926966
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-nez v2, :cond_3

    .line 1926967
    :cond_2
    new-instance v0, LX/Cfx;

    const-string v1, "ERROR_INVALID_UNIT"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1926968
    :cond_3
    sget-object v0, LX/Cfv;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1926969
    new-instance v0, LX/Cfx;

    const-string v1, "NO_SUPPORTED_STYLE"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1926970
    :pswitch_0
    invoke-static {p0, p1}, LX/Cfw;->c(LX/Cfw;LX/9qX;)LX/Cfx;

    move-result-object v0

    goto :goto_1

    .line 1926971
    :pswitch_1
    instance-of v0, p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v0, :cond_5

    .line 1926972
    new-instance v0, LX/Cfx;

    const-string v1, "ERROR_INVALID_UNIT"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    .line 1926973
    :goto_2
    move-object v0, v0

    .line 1926974
    goto :goto_1

    .line 1926975
    :pswitch_2
    new-instance v0, LX/Cfx;

    .line 1926976
    instance-of v1, p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v1, :cond_a

    .line 1926977
    const-string v1, "ERROR_INVALID_UNIT"

    .line 1926978
    :goto_3
    move-object v1, v1

    .line 1926979
    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1926980
    :pswitch_3
    new-instance v0, LX/Cfx;

    invoke-virtual {p0, p1}, LX/Cfw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1926981
    :cond_5
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1926982
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1926983
    :cond_6
    new-instance v0, LX/Cfx;

    const-string v1, "NO_AGGREGATED_UNITS"

    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1926984
    :cond_7
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1926985
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1926986
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_9

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qX;

    .line 1926987
    invoke-static {p0, v0}, LX/Cfw;->c(LX/Cfw;LX/9qX;)LX/Cfx;

    move-result-object v5

    .line 1926988
    const-string v0, "SUCCESS"

    .line 1926989
    iget-object v6, v5, LX/Cfx;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1926990
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1926991
    new-instance v0, LX/Cfx;

    .line 1926992
    iget-object v1, v5, LX/Cfx;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1926993
    invoke-direct {v0, v1}, LX/Cfx;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1926994
    :cond_8
    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1926995
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1926996
    :cond_9
    new-instance v0, LX/Cfx;

    const-string v1, "SUCCESS"

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Cfx;-><init>(Ljava/lang/String;LX/0Px;)V

    goto :goto_2

    .line 1926997
    :cond_a
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1926998
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1926999
    :cond_b
    const-string v1, "ERROR_INVALID_UNIT"

    goto/16 :goto_3

    .line 1927000
    :cond_c
    const-string v1, "SUCCESS"

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1926933
    invoke-static {p1}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v2

    .line 1926934
    if-nez v2, :cond_0

    .line 1926935
    const-string v0, "ERROR_INVALID_COMPONENT"

    .line 1926936
    :goto_0
    return-object v0

    .line 1926937
    :cond_0
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1926938
    const-string v0, "EMPTY_SUB_COMPONENTS"

    goto :goto_0

    .line 1926939
    :cond_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 1926940
    iget-object v4, p0, LX/Cfw;->b:LX/1vo;

    invoke-virtual {v4, v0}, LX/1vo;->a(LX/9oK;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1926941
    const-string v0, "UNSUPPORTED_COMPONENT_STYLE"

    goto :goto_0

    .line 1926942
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1926943
    :cond_3
    const-string v0, "SUCCESS"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1926944
    instance-of v0, p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v0, :cond_0

    .line 1926945
    const-string v0, "ERROR_INVALID_UNIT"

    .line 1926946
    :goto_0
    return-object v0

    .line 1926947
    :cond_0
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1926948
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v2

    .line 1926949
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1926950
    :cond_1
    const-string v0, "ERROR_INVALID_UNIT"

    goto :goto_0

    .line 1926951
    :cond_2
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1926952
    const-string v0, "EMPTY_COMPONENTS"

    goto :goto_0

    .line 1926953
    :cond_3
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 1926954
    iget-object v4, p0, LX/Cfw;->b:LX/1vo;

    invoke-virtual {v4, v0}, LX/1vo;->a(LX/9oK;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1926955
    const-string v0, "UNSUPPORTED_COMPONENT_STYLE"

    goto :goto_0

    .line 1926956
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1926957
    :cond_5
    const-string v0, "SUCCESS"

    goto :goto_0
.end method
