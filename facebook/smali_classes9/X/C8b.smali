.class public final enum LX/C8b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C8b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C8b;

.field public static final enum ATTACHMENT:LX/C8b;

.field public static final enum CTA_BUTTON:LX/C8b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1853119
    new-instance v0, LX/C8b;

    const-string v1, "CTA_BUTTON"

    invoke-direct {v0, v1, v2}, LX/C8b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C8b;->CTA_BUTTON:LX/C8b;

    .line 1853120
    new-instance v0, LX/C8b;

    const-string v1, "ATTACHMENT"

    invoke-direct {v0, v1, v3}, LX/C8b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C8b;->ATTACHMENT:LX/C8b;

    .line 1853121
    const/4 v0, 0x2

    new-array v0, v0, [LX/C8b;

    sget-object v1, LX/C8b;->CTA_BUTTON:LX/C8b;

    aput-object v1, v0, v2

    sget-object v1, LX/C8b;->ATTACHMENT:LX/C8b;

    aput-object v1, v0, v3

    sput-object v0, LX/C8b;->$VALUES:[LX/C8b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1853122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C8b;
    .locals 1

    .prologue
    .line 1853123
    const-class v0, LX/C8b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C8b;

    return-object v0
.end method

.method public static values()[LX/C8b;
    .locals 1

    .prologue
    .line 1853124
    sget-object v0, LX/C8b;->$VALUES:[LX/C8b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C8b;

    return-object v0
.end method
