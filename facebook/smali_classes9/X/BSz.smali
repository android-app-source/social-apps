.class public final LX/BSz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public a:Z

.field public final synthetic b:LX/BT9;


# direct methods
.method public constructor <init>(LX/BT9;)V
    .locals 1

    .prologue
    .line 1786582
    iput-object p1, p0, LX/BSz;->b:LX/BT9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786583
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BSz;->a:Z

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1786584
    iget-boolean v0, p0, LX/BSz;->a:Z

    if-eqz v0, :cond_1

    .line 1786585
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setVisibility(I)V

    .line 1786586
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1786587
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-boolean v0, v0, LX/BT9;->ac:Z

    if-eqz v0, :cond_0

    .line 1786588
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->O:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1786589
    :cond_0
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    const/4 v6, 0x0

    .line 1786590
    iget-object v2, v0, LX/BT9;->T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    if-eqz v2, :cond_6

    iget-object v2, v0, LX/BT9;->T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iget-boolean v2, v2, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-eqz v2, :cond_6

    iget-wide v2, v0, LX/BT9;->U:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 1786591
    iget-object v2, v0, LX/BT9;->o:LX/BTX;

    iget-object v3, v0, LX/BT9;->T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iget v3, v3, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    invoke-virtual {v2, v3}, LX/BTX;->a(I)I

    move-result v2

    invoke-static {v0, v2, v6}, LX/BT9;->c(LX/BT9;IZ)V

    .line 1786592
    iget-object v2, v0, LX/BT9;->o:LX/BTX;

    iget-object v3, v0, LX/BT9;->T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    iget v3, v3, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    invoke-virtual {v2, v3}, LX/BTX;->a(I)I

    move-result v2

    invoke-static {v0, v2, v6}, LX/BT9;->d(LX/BT9;IZ)V

    .line 1786593
    :goto_0
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    invoke-static {v0}, LX/BT9;->w(LX/BT9;)V

    .line 1786594
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    invoke-static {v0}, LX/BT9;->u$redex0(LX/BT9;)V

    .line 1786595
    :goto_1
    return v1

    .line 1786596
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BSz;->a:Z

    .line 1786597
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-boolean v0, v0, LX/BT9;->ac:Z

    if-eqz v0, :cond_2

    .line 1786598
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->O:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 1786599
    :cond_2
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->invalidate()V

    .line 1786600
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    .line 1786601
    iget-object v2, v0, LX/BT9;->u:LX/BTS;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    iget v4, v0, LX/BT9;->m:I

    iget v5, v0, LX/BT9;->m:I

    invoke-virtual {v2, v3, v4, v5}, LX/BTS;->a(Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;II)V

    .line 1786602
    iget-object v2, v0, LX/BT9;->n:LX/BTl;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedInStripContentWidth()I

    move-result v3

    iget-object v4, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutStripContentWidth()I

    move-result v4

    iget v5, v0, LX/BT9;->m:I

    new-instance v6, LX/BT0;

    invoke-direct {v6, v0}, LX/BT0;-><init>(LX/BT9;)V

    .line 1786603
    iput v3, v2, LX/BTl;->a:I

    .line 1786604
    iput v4, v2, LX/BTl;->b:I

    .line 1786605
    iput v5, v2, LX/BTl;->c:I

    .line 1786606
    iput-object v6, v2, LX/BTl;->d:LX/BT0;

    .line 1786607
    iget-object v2, v0, LX/BT9;->o:LX/BTX;

    iget-wide v4, v0, LX/BT9;->U:J

    long-to-int v3, v4

    iget-object v4, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutStripContentWidth()I

    move-result v4

    iget v5, v0, LX/BT9;->m:I

    .line 1786608
    iput v3, v2, LX/BTX;->b:I

    .line 1786609
    iput v4, v2, LX/BTX;->c:I

    .line 1786610
    iput v5, v2, LX/BTX;->d:I

    .line 1786611
    iget-object v2, v0, LX/BT9;->q:LX/BTJ;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    iget-object v4, v0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786612
    iput-object v3, v2, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786613
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->b:Landroid/view/View;

    move-object v5, v5

    .line 1786614
    iput-object v5, v2, LX/BTJ;->d:Landroid/view/View;

    .line 1786615
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->c:Landroid/view/View;

    move-object v5, v5

    .line 1786616
    iput-object v5, v2, LX/BTJ;->e:Landroid/view/View;

    .line 1786617
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->d:Landroid/view/View;

    move-object v5, v5

    .line 1786618
    iput-object v5, v2, LX/BTJ;->f:Landroid/view/View;

    .line 1786619
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->e:Landroid/view/View;

    move-object v5, v5

    .line 1786620
    iput-object v5, v2, LX/BTJ;->g:Landroid/view/View;

    .line 1786621
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->f:Landroid/view/View;

    move-object v5, v5

    .line 1786622
    iput-object v5, v2, LX/BTJ;->h:Landroid/view/View;

    .line 1786623
    iget-object v2, v0, LX/BT9;->r:LX/BTL;

    iget-object v3, v0, LX/BT9;->K:Landroid/view/View;

    .line 1786624
    iput-object v3, v2, LX/BTL;->c:Landroid/view/View;

    .line 1786625
    iget-object v2, v0, LX/BT9;->s:LX/BTN;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786626
    iput-object v3, v2, LX/BTN;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786627
    iget-object v2, v0, LX/BT9;->t:LX/BTP;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786628
    iput-object v3, v2, LX/BTP;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786629
    iget-object v2, v0, LX/BT9;->H:Landroid/view/View;

    sget-object v3, LX/BTI;->LEFT:LX/BTI;

    invoke-static {v0, v3}, LX/BT9;->a(LX/BT9;LX/BTI;)Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1786630
    iget-object v2, v0, LX/BT9;->I:Landroid/view/View;

    sget-object v3, LX/BTI;->RIGHT:LX/BTI;

    invoke-static {v0, v3}, LX/BT9;->a(LX/BT9;LX/BTI;)Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1786631
    iget-object v2, v0, LX/BT9;->J:Landroid/view/View;

    iget-boolean v3, v0, LX/BT9;->h:Z

    .line 1786632
    new-instance v4, LX/BT3;

    invoke-direct {v4, v0, v3}, LX/BT3;-><init>(LX/BT9;Z)V

    move-object v3, v4

    .line 1786633
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1786634
    invoke-static {v0}, LX/BT9;->z(LX/BT9;)V

    .line 1786635
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/BT9;->V:Z

    .line 1786636
    iget-object v0, p0, LX/BSz;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->b:LX/BSr;

    .line 1786637
    const/4 p0, -0x1

    .line 1786638
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786639
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v3, v3

    .line 1786640
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    iget-object v4, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    const v5, 0x7f0813d7

    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1786641
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 1786642
    move-object v2, v2

    .line 1786643
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1786644
    new-instance v4, LX/BSo;

    invoke-direct {v4, v0}, LX/BSo;-><init>(LX/BSr;)V

    invoke-virtual {v3, v4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1786645
    iget-object v4, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786646
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-object v4, v5

    .line 1786647
    iget v5, v4, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->n:I

    move v4, v5

    .line 1786648
    if-eq v4, p0, :cond_3

    .line 1786649
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    iget-object v5, v0, LX/BSr;->i:LX/23P;

    iget-object v6, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v6, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1786650
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 1786651
    move-object v2, v2

    .line 1786652
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1786653
    :cond_3
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1786654
    new-instance v2, LX/BSp;

    invoke-direct {v2, v0}, LX/BSp;-><init>(LX/BSr;)V

    invoke-virtual {v3, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1786655
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786656
    iget-object v4, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-object v2, v4

    .line 1786657
    iget v4, v2, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->o:I

    move v2, v4

    .line 1786658
    if-eq v2, p0, :cond_4

    .line 1786659
    invoke-virtual {v3, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1786660
    :cond_4
    iget-object v2, v0, LX/BSr;->h:LX/BSu;

    .line 1786661
    iget-object v3, v2, LX/BSu;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x8c0001

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1786662
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786663
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-object v2, v3

    .line 1786664
    iget v3, v2, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m:I

    move v2, v3

    .line 1786665
    if-eqz v2, :cond_5

    .line 1786666
    iget-object v3, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(I)V

    .line 1786667
    :cond_5
    goto/16 :goto_1

    .line 1786668
    :cond_6
    iget-object v2, v0, LX/BT9;->J:Landroid/view/View;

    iget-object v3, v0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getLeft()I

    move-result v3

    iget v4, v0, LX/BT9;->l:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/view/View;->setLeft(I)V

    .line 1786669
    iget-wide v2, v0, LX/BT9;->U:J

    long-to-int v2, v2

    .line 1786670
    iget v3, v0, LX/BT9;->k:I

    if-lez v3, :cond_7

    iget v3, v0, LX/BT9;->k:I

    if-le v2, v3, :cond_7

    .line 1786671
    iget v2, v0, LX/BT9;->k:I

    .line 1786672
    :cond_7
    iget-object v3, v0, LX/BT9;->o:LX/BTX;

    invoke-virtual {v3, v2}, LX/BTX;->a(I)I

    move-result v2

    invoke-static {v0, v2, v6}, LX/BT9;->d(LX/BT9;IZ)V

    goto/16 :goto_0
.end method
