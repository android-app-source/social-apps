.class public LX/Aj3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/26L",
        "<",
        "LX/26M;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/26M;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(LX/1VL;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 12
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1VL;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707551
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Aj3;->b:Ljava/util/Map;

    .line 1707552
    invoke-virtual {p1, p2}, LX/1VL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v5

    .line 1707553
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 1707554
    if-nez v6, :cond_0

    .line 1707555
    const/16 v0, 0xc

    iput v0, p0, LX/Aj3;->c:I

    .line 1707556
    const/4 v0, 0x4

    iput v0, p0, LX/Aj3;->d:I

    .line 1707557
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1707558
    iput-object v0, p0, LX/Aj3;->a:LX/0Px;

    .line 1707559
    const/4 v0, 0x0

    iput v0, p0, LX/Aj3;->e:I

    .line 1707560
    :goto_0
    return-void

    .line 1707561
    :cond_0
    const-wide/16 v2, 0x0

    .line 1707562
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1707563
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_1

    .line 1707564
    new-instance v1, LX/26M;

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v1, v0}, LX/26M;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1707565
    iget-object v0, p0, LX/Aj3;->b:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1707566
    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1707567
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1707568
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1707569
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1707570
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1707571
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    int-to-double v8, v1

    .line 1707572
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    int-to-double v0, v0

    .line 1707573
    const-wide/16 v10, 0x0

    cmpl-double v10, v0, v10

    if-lez v10, :cond_4

    .line 1707574
    div-double v0, v8, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 1707575
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_1

    .line 1707576
    :cond_1
    const/4 v0, 0x3

    if-ge v6, v0, :cond_3

    .line 1707577
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v2, v0

    if-gez v0, :cond_2

    .line 1707578
    const/4 v0, 0x2

    iput v0, p0, LX/Aj3;->e:I

    .line 1707579
    const/16 v0, 0x8

    div-int/2addr v0, v6

    iput v0, p0, LX/Aj3;->c:I

    .line 1707580
    :goto_3
    const/4 v0, 0x4

    iget v1, p0, LX/Aj3;->c:I

    mul-int/lit8 v1, v1, 0x2

    iget v4, p0, LX/Aj3;->c:I

    int-to-double v4, v4

    div-double v2, v4, v2

    double-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/Aj3;->d:I

    .line 1707581
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Aj3;->a:LX/0Px;

    goto/16 :goto_0

    .line 1707582
    :cond_2
    const/16 v0, 0xc

    div-int/2addr v0, v6

    iput v0, p0, LX/Aj3;->c:I

    .line 1707583
    const/4 v0, 0x0

    iput v0, p0, LX/Aj3;->e:I

    goto :goto_3

    .line 1707584
    :cond_3
    const/4 v0, 0x4

    iput v0, p0, LX/Aj3;->c:I

    .line 1707585
    const/4 v0, 0x0

    iput v0, p0, LX/Aj3;->e:I

    goto :goto_3

    :cond_4
    move-wide v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/26N;)I
    .locals 2

    .prologue
    .line 1707591
    check-cast p1, LX/26M;

    .line 1707592
    iget-object v0, p0, LX/Aj3;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, LX/Aj3;->c:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/Aj3;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1707590
    iget-object v0, p0, LX/Aj3;->a:LX/0Px;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1707589
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/26N;)I
    .locals 1

    .prologue
    .line 1707593
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1707588
    const/16 v0, 0xc

    return v0
.end method

.method public final c(LX/26N;)I
    .locals 1

    .prologue
    .line 1707587
    iget v0, p0, LX/Aj3;->c:I

    return v0
.end method

.method public final d(LX/26N;)I
    .locals 1

    .prologue
    .line 1707586
    iget v0, p0, LX/Aj3;->d:I

    return v0
.end method
