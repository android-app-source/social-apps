.class public final LX/AjB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public final synthetic c:LX/0TF;

.field public final synthetic d:LX/0TF;

.field public final synthetic e:LX/1du;


# direct methods
.method public constructor <init>(LX/1du;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;LX/0TF;)V
    .locals 0

    .prologue
    .line 1707641
    iput-object p1, p0, LX/AjB;->e:LX/1du;

    iput-object p2, p0, LX/AjB;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AjB;->b:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object p4, p0, LX/AjB;->c:LX/0TF;

    iput-object p5, p0, LX/AjB;->d:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1707642
    iget-object v0, p0, LX/AjB;->e:LX/1du;

    iget-object v1, p0, LX/AjB;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1du;->d(LX/1du;Ljava/lang/String;)V

    .line 1707643
    iget-object v0, p0, LX/AjB;->d:LX/0TF;

    if-eqz v0, :cond_0

    .line 1707644
    iget-object v0, p0, LX/AjB;->d:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1707645
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1707646
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1707647
    iget-object v0, p0, LX/AjB;->e:LX/1du;

    iget-object v1, p0, LX/AjB;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AjB;->b:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iget-object v3, p0, LX/AjB;->c:LX/0TF;

    invoke-virtual {v0, v1, v2, v3}, LX/1du;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;)V

    .line 1707648
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 1707649
    iget-object v1, p0, LX/AjB;->e:LX/1du;

    iget-object v2, p0, LX/AjB;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/1du;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707650
    return-void
.end method
