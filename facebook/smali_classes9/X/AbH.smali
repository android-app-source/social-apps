.class public LX/AbH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690526
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1690506
    check-cast p1, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;

    .line 1690507
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1690508
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "action"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->b:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    invoke-virtual {v3}, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690509
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "instream_ads_creator_actions"

    .line 1690510
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1690511
    move-object v1, v1

    .line 1690512
    const-string v2, "POST"

    .line 1690513
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1690514
    move-object v1, v1

    .line 1690515
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/instream_ads_creator_actions"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1690516
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1690517
    move-object v1, v1

    .line 1690518
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1690519
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1690520
    move-object v1, v1

    .line 1690521
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1690522
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1690523
    move-object v0, v1

    .line 1690524
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1690504
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1690505
    const/4 v0, 0x0

    return-object v0
.end method
