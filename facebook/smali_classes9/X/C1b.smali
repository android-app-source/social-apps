.class public final LX/C1b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/C1d;


# direct methods
.method public constructor <init>(LX/C1d;)V
    .locals 0

    .prologue
    .line 1842571
    iput-object p1, p0, LX/C1b;->a:LX/C1d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1842569
    sget-object v0, LX/C1d;->b:Ljava/lang/String;

    const-string v1, "ScheduledLiveStartTimeSubscription query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1842570
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1842561
    check-cast p1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel;

    .line 1842562
    if-nez p1, :cond_1

    .line 1842563
    :cond_0
    :goto_0
    return-void

    .line 1842564
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel;->a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;

    move-result-object v9

    .line 1842565
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1842566
    new-instance v0, LX/6Pm;

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->p()J

    move-result-wide v2

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->l()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, LX/6Pm;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1842567
    iget-object v1, p0, LX/C1b;->a:LX/C1d;

    iget-object v1, v1, LX/C1d;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ti;

    invoke-virtual {v1, v0}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1842568
    iget-object v0, p0, LX/C1b;->a:LX/C1d;

    invoke-static {v0, v9}, LX/C1d;->a$redex0(LX/C1d;Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;)V

    goto :goto_0
.end method
