.class public final LX/AUS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field private final a:[LX/AUP;

.field private final b:[LX/AUP;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AUP;",
            ">;"
        }
    .end annotation
.end field

.field private f:[Z

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/AUP;",
            "LX/AUP;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([LX/AUP;[LX/AUP;)V
    .locals 0

    .prologue
    .line 1678052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678053
    iput-object p1, p0, LX/AUS;->a:[LX/AUP;

    .line 1678054
    iput-object p2, p0, LX/AUS;->b:[LX/AUP;

    .line 1678055
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1678056
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/AUS;->a:[LX/AUP;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/AUS;->c:Ljava/util/ArrayList;

    .line 1678057
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/AUS;->d:Ljava/util/ArrayList;

    .line 1678058
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/AUS;->b:[LX/AUP;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/AUS;->e:Ljava/util/ArrayList;

    .line 1678059
    invoke-static {}, LX/AUj;->values()[LX/AUj;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, p0, LX/AUS;->f:[Z

    .line 1678060
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/AUS;->b:[LX/AUP;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/AUS;->g:Ljava/util/ArrayList;

    .line 1678061
    iget-object v1, p0, LX/AUS;->b:[LX/AUP;

    .line 1678062
    new-instance v3, Ljava/util/HashMap;

    array-length v2, v1

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 1678063
    array-length v4, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v1, v2

    .line 1678064
    iget-object v6, v5, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v3, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678065
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1678066
    :cond_0
    move-object v2, v3

    .line 1678067
    iget-object v3, p0, LX/AUS;->a:[LX/AUP;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 1678068
    iget-object v0, v5, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUP;

    .line 1678069
    if-eqz v0, :cond_3

    .line 1678070
    invoke-virtual {v0, v5}, LX/AUP;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1678071
    iget-boolean v6, v5, LX/AUP;->g:Z

    if-nez v6, :cond_2

    iget-boolean v6, v0, LX/AUP;->g:Z

    if-eqz v6, :cond_2

    .line 1678072
    iget-object v0, p0, LX/AUS;->c:Ljava/util/ArrayList;

    iget-object v5, v5, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1678073
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1678074
    :cond_2
    iget-object v6, p0, LX/AUS;->g:Ljava/util/ArrayList;

    invoke-static {v5, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1678075
    :cond_3
    iget-object v0, p0, LX/AUS;->d:Ljava/util/ArrayList;

    iget-object v5, v5, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1678076
    :cond_4
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUP;

    .line 1678077
    iget-boolean v2, v0, LX/AUP;->g:Z

    if-nez v2, :cond_5

    .line 1678078
    iget-object v2, p0, LX/AUS;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1678079
    iget-object v2, p0, LX/AUS;->f:[Z

    iget-object v0, v0, LX/AUP;->i:LX/AUj;

    invoke-virtual {v0}, LX/AUj;->ordinal()I

    move-result v0

    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    goto :goto_3

    .line 1678080
    :cond_6
    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1678081
    iget-object v0, p0, LX/AUS;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1678082
    iget-object v0, p0, LX/AUS;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/AUP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1678083
    iget-object v0, p0, LX/AUS;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final e()[Z
    .locals 1

    .prologue
    .line 1678084
    iget-object v0, p0, LX/AUS;->f:[Z

    return-object v0
.end method

.method public final f()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/AUP;",
            "LX/AUP;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1678085
    iget-object v0, p0, LX/AUS;->g:Ljava/util/ArrayList;

    return-object v0
.end method
