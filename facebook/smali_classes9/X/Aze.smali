.class public final LX/Aze;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/camera/CameraActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V
    .locals 0

    .prologue
    .line 1732807
    iput-object p1, p0, LX/Aze;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7gj;)V
    .locals 4

    .prologue
    .line 1732808
    iget-object v0, p1, LX/7gj;->b:LX/7gi;

    move-object v0, v0

    .line 1732809
    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    if-ne v0, v1, :cond_0

    const-string v0, "photo"

    .line 1732810
    :goto_0
    iget-object v1, p0, LX/Aze;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v1, v1, Lcom/facebook/goodfriends/camera/CameraActivity;->t:LX/9J2;

    .line 1732811
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1732812
    const-string v3, "media_type"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732813
    sget-object v3, LX/9J1;->CAMERA_CAPTURE_BUTTON_TAPPED:LX/9J1;

    invoke-static {v1, v3, v2}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1732814
    iget-object v0, p0, LX/Aze;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    .line 1732815
    iput-object p1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    .line 1732816
    iget-object v0, p0, LX/Aze;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, LX/Aze;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v2, v2, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    const-string v3, "preview_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1732817
    return-void

    .line 1732818
    :cond_0
    const-string v0, "video"

    goto :goto_0
.end method
