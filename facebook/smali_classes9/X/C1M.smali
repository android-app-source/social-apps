.class public LX/C1M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C1K;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C1N;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1842310
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C1M;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C1N;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842311
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1842312
    iput-object p1, p0, LX/C1M;->b:LX/0Ot;

    .line 1842313
    return-void
.end method

.method public static a(LX/0QB;)LX/C1M;
    .locals 4

    .prologue
    .line 1842314
    const-class v1, LX/C1M;

    monitor-enter v1

    .line 1842315
    :try_start_0
    sget-object v0, LX/C1M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842316
    sput-object v2, LX/C1M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842317
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842318
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842319
    new-instance v3, LX/C1M;

    const/16 p0, 0x1e8e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C1M;-><init>(LX/0Ot;)V

    .line 1842320
    move-object v0, v3

    .line 1842321
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842322
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842323
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842324
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842325
    const v0, 0x516379b6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1842326
    check-cast p2, LX/C1L;

    .line 1842327
    iget-object v0, p0, LX/C1M;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/C1L;->a:LX/C1O;

    .line 1842328
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    iget-object p2, v0, LX/C1O;->a:Ljava/lang/String;

    invoke-virtual {p0, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a010e

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 1842329
    const p2, 0x516379b6

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1842330
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1842331
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1842332
    invoke-static {}, LX/1dS;->b()V

    .line 1842333
    iget v0, p1, LX/1dQ;->b:I

    .line 1842334
    packed-switch v0, :pswitch_data_0

    .line 1842335
    :goto_0
    return-object v2

    .line 1842336
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1842337
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1842338
    check-cast v1, LX/C1L;

    .line 1842339
    iget-object v3, p0, LX/C1M;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C1N;

    iget-object p1, v1, LX/C1L;->a:LX/C1O;

    .line 1842340
    iget-object p2, v3, LX/C1N;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object p0, p1, LX/C1O;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    invoke-interface {p2, p0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object p2

    .line 1842341
    iget-object p0, v3, LX/C1N;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1842342
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x516379b6
        :pswitch_0
    .end packed-switch
.end method
