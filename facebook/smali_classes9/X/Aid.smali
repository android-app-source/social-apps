.class public final LX/Aid;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/Aie;


# direct methods
.method public constructor <init>(LX/Aie;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1706543
    iput-object p1, p0, LX/Aid;->f:LX/Aie;

    iput-object p2, p0, LX/Aid;->a:Ljava/lang/String;

    iput p3, p0, LX/Aid;->b:I

    iput p4, p0, LX/Aid;->c:I

    iput p5, p0, LX/Aid;->d:I

    iput-object p6, p0, LX/Aid;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1706544
    new-instance v0, LX/Ahc;

    invoke-direct {v0}, LX/Ahc;-><init>()V

    move-object v0, v0

    .line 1706545
    const-string v1, "topic_id"

    iget-object v2, p0, LX/Aid;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1706546
    const-string v1, "num_profiles"

    iget v2, p0, LX/Aid;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706547
    const-string v1, "num_connected_friends"

    iget v2, p0, LX/Aid;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706548
    const-string v1, "connected_friends_profile_picture_size"

    iget v2, p0, LX/Aid;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706549
    iget-object v1, p0, LX/Aid;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1706550
    const-string v1, "after_cursor"

    iget-object v2, p0, LX/Aid;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1706551
    :cond_0
    invoke-static {v0}, LX/Aie;->b(LX/0gW;)V

    .line 1706552
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1706553
    iget-object v1, p0, LX/Aid;->f:LX/Aie;

    iget-object v1, v1, LX/Aie;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
