.class public LX/Bnj;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/Bnk;",
        "LX/Bnm;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Bnj;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1821274
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1821275
    return-void
.end method

.method public static a(LX/0QB;)LX/Bnj;
    .locals 3

    .prologue
    .line 1821276
    sget-object v0, LX/Bnj;->a:LX/Bnj;

    if-nez v0, :cond_1

    .line 1821277
    const-class v1, LX/Bnj;

    monitor-enter v1

    .line 1821278
    :try_start_0
    sget-object v0, LX/Bnj;->a:LX/Bnj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1821279
    if-eqz v2, :cond_0

    .line 1821280
    :try_start_1
    new-instance v0, LX/Bnj;

    invoke-direct {v0}, LX/Bnj;-><init>()V

    .line 1821281
    move-object v0, v0

    .line 1821282
    sput-object v0, LX/Bnj;->a:LX/Bnj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1821283
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1821284
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1821285
    :cond_1
    sget-object v0, LX/Bnj;->a:LX/Bnj;

    return-object v0

    .line 1821286
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1821287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
