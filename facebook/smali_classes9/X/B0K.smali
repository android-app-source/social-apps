.class public LX/B0K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/ARD;

.field private final b:LX/AR2;

.field private final c:LX/AR4;

.field public d:Lcom/facebook/user/model/User;

.field public e:Ljava/lang/String;

.field public f:LX/74n;


# direct methods
.method public constructor <init>(LX/0Or;LX/74n;LX/ARD;LX/AR4;LX/AR2;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/74n;",
            "LX/ARD;",
            "LX/AR4;",
            "LX/AR2;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733707
    iput-object p2, p0, LX/B0K;->f:LX/74n;

    .line 1733708
    iput-object p5, p0, LX/B0K;->b:LX/AR2;

    .line 1733709
    iput-object p4, p0, LX/B0K;->c:LX/AR4;

    .line 1733710
    iput-object p3, p0, LX/B0K;->a:LX/ARD;

    .line 1733711
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/B0K;->d:Lcom/facebook/user/model/User;

    .line 1733712
    return-void
.end method

.method public static a(LX/B0K;LX/B0J;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1733713
    iget-object v0, p0, LX/B0K;->c:LX/AR4;

    invoke-virtual {v0, p1}, LX/AR4;->a(Ljava/lang/Object;)LX/AR3;

    move-result-object v0

    .line 1733714
    iget-object v1, p0, LX/B0K;->b:LX/AR2;

    sget-object v2, LX/ARN;->a:LX/ARN;

    invoke-virtual {v1, p1, v2}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v1

    .line 1733715
    iget-object v2, p0, LX/B0K;->a:LX/ARD;

    invoke-virtual {v2, p1, v0, v1}, LX/ARD;->a(Ljava/lang/Object;LX/AR3;LX/AR1;)LX/ARC;

    move-result-object v0

    .line 1733716
    invoke-virtual {v0}, LX/ARC;->b()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
