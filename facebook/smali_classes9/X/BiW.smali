.class public LX/BiW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BiW;


# instance fields
.field public final a:LX/0Yj;

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1810366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810367
    iput-object p1, p0, LX/BiW;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1810368
    new-instance v0, LX/0Yj;

    const v1, 0x60010

    const-string v2, "CreateEventTTI"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "event_composer"

    aput-object v3, v1, v2

    const-string v2, "event_permalink"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 1810369
    iput-boolean v4, v0, LX/0Yj;->n:Z

    .line 1810370
    move-object v0, v0

    .line 1810371
    iput-object v0, p0, LX/BiW;->a:LX/0Yj;

    .line 1810372
    return-void
.end method

.method public static a(LX/0QB;)LX/BiW;
    .locals 4

    .prologue
    .line 1810373
    sget-object v0, LX/BiW;->c:LX/BiW;

    if-nez v0, :cond_1

    .line 1810374
    const-class v1, LX/BiW;

    monitor-enter v1

    .line 1810375
    :try_start_0
    sget-object v0, LX/BiW;->c:LX/BiW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1810376
    if-eqz v2, :cond_0

    .line 1810377
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1810378
    new-instance p0, LX/BiW;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/BiW;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1810379
    move-object v0, p0

    .line 1810380
    sput-object v0, LX/BiW;->c:LX/BiW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1810381
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1810382
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1810383
    :cond_1
    sget-object v0, LX/BiW;->c:LX/BiW;

    return-object v0

    .line 1810384
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1810385
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
