.class public final LX/Alw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alv;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1aX;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;LX/1aX;)V
    .locals 1

    .prologue
    .line 1710396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710397
    iput-object p1, p0, LX/Alw;->b:Landroid/net/Uri;

    .line 1710398
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Alw;->a:LX/0Px;

    .line 1710399
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1aX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1710400
    iget-object v0, p0, LX/Alw;->a:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/graphics/Canvas;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1710401
    iget-object v0, p0, LX/Alw;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v1, v1, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1710402
    iget-object v0, p0, LX/Alw;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1710403
    return-void
.end method
