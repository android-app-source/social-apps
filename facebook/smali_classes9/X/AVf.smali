.class public final LX/AVf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AVe;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V
    .locals 0

    .prologue
    .line 1680169
    iput-object p1, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;B)V
    .locals 0

    .prologue
    .line 1680168
    invoke-direct {p0, p1}, LX/AVf;-><init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1680163
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 1680164
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v0, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->r:LX/03V;

    const-string v1, "FacecastBroadcastNuxQuery_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1680165
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    .line 1680166
    invoke-static {v0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->b$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 1680167
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;)V
    .locals 4

    .prologue
    .line 1680150
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->j()I

    move-result v0

    if-nez v0, :cond_1

    .line 1680151
    :cond_0
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    .line 1680152
    invoke-static {v0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->b$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 1680153
    :goto_0
    return-void

    .line 1680154
    :cond_1
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    const/4 v1, 0x1

    .line 1680155
    iput-boolean v1, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->E:Z

    .line 1680156
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v0, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    invoke-virtual {v0, p1}, LX/AVn;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;)V

    .line 1680157
    iget-object v0, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v0, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->s:LX/AVn;

    .line 1680158
    invoke-static {v0}, LX/AVn;->d(LX/AVn;)V

    .line 1680159
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->m()I

    move-result v0

    int-to-double v0, v0

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->j()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1680160
    iget-object v2, p0, LX/AVf;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 1680161
    invoke-static {v2, v3, v0, v1}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->a$redex0(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;Ljava/lang/String;D)V

    .line 1680162
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1680148
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/AVf;->b(Ljava/lang/Throwable;)V

    .line 1680149
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1680146
    invoke-direct {p0, p1}, LX/AVf;->b(Ljava/lang/Throwable;)V

    .line 1680147
    return-void
.end method
