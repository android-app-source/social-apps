.class public final LX/Ar7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ar6;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 0

    .prologue
    .line 1718633
    iput-object p1, p0, LX/Ar7;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1718634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The Inspiration framework should not have any functionality that involves plugins"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 1718635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The Inspiration framework should not have any functionality that involves plugins"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1718636
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The Inspiration framework should not have any functionality that involves plugins"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1718637
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The Inspiration framework should not have any functionality that involves plugins"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
