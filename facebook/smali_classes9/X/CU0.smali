.class public final LX/CU0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CUx;)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1896151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896152
    invoke-interface {p1}, LX/CUw;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->a:Ljava/lang/String;

    .line 1896153
    invoke-interface {p1}, LX/CUw;->jS_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->b:Ljava/lang/String;

    .line 1896154
    invoke-interface {p1}, LX/CUw;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->g:Ljava/lang/String;

    .line 1896155
    invoke-interface {p1}, LX/CUw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->h:Ljava/lang/String;

    .line 1896156
    invoke-interface {p1}, LX/CUw;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->c:Ljava/lang/String;

    .line 1896157
    invoke-interface {p1}, LX/CUx;->jR_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1896158
    invoke-interface {p1}, LX/CUx;->jR_()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896159
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/CU0;->e:Ljava/lang/String;

    .line 1896160
    invoke-interface {p1}, LX/CUw;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU0;->f:Ljava/lang/String;

    .line 1896161
    invoke-interface {p1}, LX/CUx;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1896162
    invoke-interface {p1}, LX/CUx;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896163
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1896164
    invoke-interface {p1}, LX/CUx;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896165
    invoke-interface {p1}, LX/CUx;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1896166
    new-instance v6, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v4, v2}, LX/15i;->j(II)I

    move-result v1

    int-to-long v2, v1

    invoke-direct {v6, v0, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    iput-object v6, p0, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1896167
    :goto_2
    return-void

    .line 1896168
    :cond_0
    sget-object v0, LX/CU6;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1896169
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 1896170
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/5fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1896145
    iget-object v0, p0, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v0, :cond_0

    .line 1896146
    iget-object v0, p0, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    .line 1896147
    :goto_0
    return-object v0

    .line 1896148
    :cond_0
    iget-object v0, p0, LX/CU0;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1896149
    iget-object v0, p0, LX/CU0;->c:Ljava/lang/String;

    goto :goto_0

    .line 1896150
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
