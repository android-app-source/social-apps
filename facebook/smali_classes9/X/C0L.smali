.class public final LX/C0L;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C0M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public final synthetic e:LX/C0M;


# direct methods
.method public constructor <init>(LX/C0M;)V
    .locals 1

    .prologue
    .line 1840337
    iput-object p1, p0, LX/C0L;->e:LX/C0M;

    .line 1840338
    move-object v0, p1

    .line 1840339
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1840340
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1840341
    const-string v0, "SmallPhotoShareAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1840342
    if-ne p0, p1, :cond_1

    .line 1840343
    :cond_0
    :goto_0
    return v0

    .line 1840344
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1840345
    goto :goto_0

    .line 1840346
    :cond_3
    check-cast p1, LX/C0L;

    .line 1840347
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1840348
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1840349
    if-eq v2, v3, :cond_0

    .line 1840350
    iget-object v2, p0, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1840351
    goto :goto_0

    .line 1840352
    :cond_5
    iget-object v2, p1, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1840353
    :cond_6
    iget-object v2, p0, LX/C0L;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C0L;->b:LX/1Po;

    iget-object v3, p1, LX/C0L;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1840354
    goto :goto_0

    .line 1840355
    :cond_8
    iget-object v2, p1, LX/C0L;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 1840356
    :cond_9
    iget-boolean v2, p0, LX/C0L;->c:Z

    iget-boolean v3, p1, LX/C0L;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1840357
    goto :goto_0

    .line 1840358
    :cond_a
    iget-boolean v2, p0, LX/C0L;->d:Z

    iget-boolean v3, p1, LX/C0L;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1840359
    goto :goto_0
.end method
