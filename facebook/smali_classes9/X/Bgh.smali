.class public LX/Bgh;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Bgh;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808046
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1808047
    sget-object v0, LX/0ax;->aL:Ljava/lang/String;

    const-string v1, "{com.facebook.katana.profile.id}"

    const-string v2, "{entry_point}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGE_SUGGEST_EDITS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1808048
    return-void
.end method

.method public static a(LX/0QB;)LX/Bgh;
    .locals 3

    .prologue
    .line 1808049
    sget-object v0, LX/Bgh;->a:LX/Bgh;

    if-nez v0, :cond_1

    .line 1808050
    const-class v1, LX/Bgh;

    monitor-enter v1

    .line 1808051
    :try_start_0
    sget-object v0, LX/Bgh;->a:LX/Bgh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1808052
    if-eqz v2, :cond_0

    .line 1808053
    :try_start_1
    new-instance v0, LX/Bgh;

    invoke-direct {v0}, LX/Bgh;-><init>()V

    .line 1808054
    move-object v0, v0

    .line 1808055
    sput-object v0, LX/Bgh;->a:LX/Bgh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1808056
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1808057
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1808058
    :cond_1
    sget-object v0, LX/Bgh;->a:LX/Bgh;

    return-object v0

    .line 1808059
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1808060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
