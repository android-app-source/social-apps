.class public LX/Aoa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1xh;

.field private final b:LX/1xi;

.field private final c:Landroid/content/res/Resources;

.field private d:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(LX/1xh;LX/1xi;Landroid/content/res/Resources;)V
    .locals 0
    .param p3    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/resources/BaseResources;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1714592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1714593
    iput-object p1, p0, LX/Aoa;->a:LX/1xh;

    .line 1714594
    iput-object p2, p0, LX/Aoa;->b:LX/1xi;

    .line 1714595
    iput-object p3, p0, LX/Aoa;->c:Landroid/content/res/Resources;

    .line 1714596
    return-void
.end method

.method private static a(LX/Aoa;Ljava/lang/String;Z)LX/AoZ;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1714600
    if-eqz p2, :cond_2

    move v8, v9

    .line 1714601
    :goto_0
    invoke-static {p0, p1}, LX/Aoa;->b(LX/Aoa;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1714602
    new-instance v0, LX/AoZ;

    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    invoke-virtual {v1}, LX/1xi;->a()Z

    move-result v2

    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    .line 1714603
    iget-object v3, v1, LX/1xi;->a:LX/0ad;

    sget-short v4, LX/0ob;->b:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 1714604
    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    .line 1714605
    iget-object v4, v1, LX/1xi;->a:LX/0ad;

    sget-short v5, LX/0ob;->d:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    move v4, v4

    .line 1714606
    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    .line 1714607
    iget-object v5, v1, LX/1xi;->a:LX/0ad;

    sget-short v6, LX/0ob;->c:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 1714608
    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    .line 1714609
    iget-object v6, v1, LX/1xi;->b:Ljava/lang/Integer;

    if-nez v6, :cond_0

    .line 1714610
    iget-object v6, v1, LX/1xi;->a:LX/0ad;

    sget-char v7, LX/0ob;->f:C

    const-string p1, ""

    invoke-interface {v6, v7, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const v7, -0x6e685d

    invoke-static {v6, v7}, LX/1xh;->a(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v1, LX/1xi;->b:Ljava/lang/Integer;

    .line 1714611
    :cond_0
    iget-object v6, v1, LX/1xi;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move v6, v6

    .line 1714612
    iget-object v1, p0, LX/Aoa;->b:LX/1xi;

    .line 1714613
    iget-object v7, v1, LX/1xi;->c:Ljava/lang/Integer;

    if-nez v7, :cond_1

    .line 1714614
    iget-object v7, v1, LX/1xi;->a:LX/0ad;

    sget-char p1, LX/0ob;->e:C

    const-string p2, ""

    invoke-interface {v7, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const p1, -0x161513

    invoke-static {v7, p1}, LX/1xh;->a(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v1, LX/1xi;->c:Ljava/lang/Integer;

    .line 1714615
    :cond_1
    iget-object v7, v1, LX/1xi;->c:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move v7, v7

    .line 1714616
    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/AoZ;-><init>(LX/Aoa;ZZZZIIZB)V

    .line 1714617
    :goto_1
    return-object v0

    .line 1714618
    :cond_2
    iget-object v0, p0, LX/Aoa;->a:LX/1xh;

    .line 1714619
    iget-object v1, v0, LX/1xh;->b:LX/0ad;

    sget-short v2, LX/0ob;->k:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v8, v1

    .line 1714620
    goto/16 :goto_0

    .line 1714621
    :cond_3
    if-nez p1, :cond_7

    .line 1714622
    const/4 v0, 0x0

    .line 1714623
    :goto_2
    move v0, v0

    .line 1714624
    if-eqz v0, :cond_6

    .line 1714625
    new-instance v0, LX/AoZ;

    iget-object v1, p0, LX/Aoa;->a:LX/1xh;

    invoke-virtual {v1}, LX/1xh;->a()Z

    move-result v2

    iget-object v1, p0, LX/Aoa;->a:LX/1xh;

    .line 1714626
    iget-object v3, v1, LX/1xh;->b:LX/0ad;

    sget-short v4, LX/0ob;->h:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 1714627
    const/4 v4, 0x1

    iget-object v1, p0, LX/Aoa;->a:LX/1xh;

    .line 1714628
    iget-object v5, v1, LX/1xh;->b:LX/0ad;

    sget-short v6, LX/0ob;->l:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 1714629
    iget-object v1, p0, LX/Aoa;->a:LX/1xh;

    .line 1714630
    iget-object v6, v1, LX/1xh;->c:Ljava/lang/Integer;

    if-nez v6, :cond_4

    .line 1714631
    iget-object v6, v1, LX/1xh;->b:LX/0ad;

    sget-char v7, LX/0ob;->m:C

    const-string p1, ""

    invoke-interface {v6, v7, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const v7, -0x6e685d

    invoke-static {v6, v7}, LX/1xh;->a(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v1, LX/1xh;->c:Ljava/lang/Integer;

    .line 1714632
    :cond_4
    iget-object v6, v1, LX/1xh;->c:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move v6, v6

    .line 1714633
    iget-object v1, p0, LX/Aoa;->a:LX/1xh;

    .line 1714634
    iget-object v7, v1, LX/1xh;->d:Ljava/lang/Integer;

    if-nez v7, :cond_5

    .line 1714635
    iget-object v7, v1, LX/1xh;->b:LX/0ad;

    sget-char p1, LX/0ob;->i:C

    const-string p2, ""

    invoke-interface {v7, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const p1, -0x161513

    invoke-static {v7, p1}, LX/1xh;->a(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v1, LX/1xh;->d:Ljava/lang/Integer;

    .line 1714636
    :cond_5
    iget-object v7, v1, LX/1xh;->d:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move v7, v7

    .line 1714637
    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/AoZ;-><init>(LX/Aoa;ZZZZIIZB)V

    goto/16 :goto_1

    .line 1714638
    :cond_6
    new-instance v0, LX/AoZ;

    move-object v1, p0

    move v2, v9

    move v3, v9

    move v4, v9

    move v5, v9

    move v6, v9

    move v7, v9

    invoke-direct/range {v0 .. v9}, LX/AoZ;-><init>(LX/Aoa;ZZZZIIZB)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, LX/Aoa;->a:LX/1xh;

    invoke-virtual {v0}, LX/1xh;->g()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_2
.end method

.method private static a(LX/AoZ;LX/0Ot;LX/0Ot;Landroid/text/SpannableStringBuilder;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AoZ;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 1714597
    new-instance v1, LX/63E;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v3, p0, LX/AoZ;->f:I

    iget v4, p0, LX/AoZ;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v1, v2, v0, v3, v4}, LX/63E;-><init>(FFILjava/lang/Integer;)V

    .line 1714598
    const/16 v0, 0x11

    invoke-virtual {p3, v1, p4, p5, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1714599
    return-void
.end method

.method private a(LX/AoZ;Landroid/text/SpannableStringBuilder;II)V
    .locals 3

    .prologue
    .line 1714581
    new-instance v0, LX/63H;

    iget-object v1, p0, LX/Aoa;->d:Landroid/text/TextPaint;

    iget v2, p1, LX/AoZ;->e:I

    invoke-direct {v0, v1, v2}, LX/63H;-><init>(Landroid/text/TextPaint;I)V

    .line 1714582
    const/16 v1, 0x11

    invoke-virtual {p2, v0, p3, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1714583
    return-void
.end method

.method private static a(LX/AoZ;Landroid/text/SpannableStringBuilder;LX/1z2;)V
    .locals 3

    .prologue
    .line 1714584
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1714585
    iget-boolean v1, p0, LX/AoZ;->b:Z

    if-eqz v1, :cond_0

    .line 1714586
    iget v1, p0, LX/AoZ;->e:I

    .line 1714587
    iget-object v2, p2, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    sget-object p0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, p0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1714588
    const/16 v1, 0x61

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1714589
    add-int/lit8 v1, v0, 0x1

    const/16 v2, 0x11

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1714590
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1714591
    :cond_0
    return-void
.end method

.method private static a(LX/Aoa;Ljava/lang/String;LX/AoZ;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;LX/1z3;Landroid/text/TextPaint;LX/0Ot;LX/0Ot;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/AoZ;",
            "Landroid/text/SpannableStringBuilder;",
            "Landroid/text/SpannableStringBuilder;",
            "LX/1z3;",
            "Landroid/text/TextPaint;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1714567
    invoke-virtual {p4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 1714568
    invoke-virtual {p5}, LX/1z3;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1714569
    invoke-virtual {p4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 1714570
    iget-object v0, p0, LX/Aoa;->d:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 1714571
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, p6}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, LX/Aoa;->d:Landroid/text/TextPaint;

    .line 1714572
    :cond_0
    iget-object v0, p0, LX/Aoa;->d:Landroid/text/TextPaint;

    iget v1, p2, LX/AoZ;->e:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1714573
    invoke-virtual {p5}, LX/1z3;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/Aoa;->b(LX/Aoa;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1714574
    iget-boolean v0, p2, LX/AoZ;->g:Z

    if-eqz v0, :cond_2

    new-instance v0, LX/AoX;

    invoke-direct {v0, p0, p5, p6, p1}, LX/AoX;-><init>(LX/Aoa;LX/1z3;Landroid/text/TextPaint;Ljava/lang/String;)V

    .line 1714575
    :goto_0
    add-int/lit8 v1, v6, -0x1

    const/16 v2, 0x11

    invoke-virtual {p4, v0, v1, v7, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1714576
    :cond_1
    iget-boolean v0, p2, LX/AoZ;->d:Z

    if-nez v0, :cond_3

    .line 1714577
    invoke-direct {p0, p2, p4, v6, v7}, LX/Aoa;->a(LX/AoZ;Landroid/text/SpannableStringBuilder;II)V

    .line 1714578
    :goto_1
    return-void

    .line 1714579
    :cond_2
    new-instance v0, LX/AoY;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/AoY;-><init>(LX/Aoa;LX/1z3;Landroid/text/TextPaint;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, p2

    move-object v1, p7

    move-object/from16 v2, p8

    move-object v3, p4

    move v4, v6

    move v5, v7

    .line 1714580
    invoke-static/range {v0 .. v5}, LX/Aoa;->a(LX/AoZ;LX/0Ot;LX/0Ot;Landroid/text/SpannableStringBuilder;II)V

    goto :goto_1
.end method

.method public static a$redex0(LX/Aoa;Landroid/view/View;ILjava/lang/String;)LX/0hs;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1714559
    new-instance v0, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 1714560
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Aoa;->c:Landroid/content/res/Resources;

    const v2, 0x7f081a27

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1714561
    const/4 v1, -0x1

    .line 1714562
    iput v1, v0, LX/0hs;->t:I

    .line 1714563
    move-object v1, p1

    move v2, p2

    move v4, v3

    move v5, v3

    .line 1714564
    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 1714565
    return-object v0

    .line 1714566
    :cond_0
    iget-object v1, p0, LX/Aoa;->c:Landroid/content/res/Resources;

    const v2, 0x7f081a28

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p3, v4, v3

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static b(IFLjava/lang/String;Landroid/text/TextPaint;)I
    .locals 3

    .prologue
    .line 1714556
    div-int/lit8 v0, p0, 0x2

    int-to-float v0, v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    .line 1714557
    invoke-static {p2, p3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 1714558
    int-to-float v0, v0

    sub-float v0, p1, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static b(LX/Aoa;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1714555
    const-string v0, "good_friends"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aoa;->b:LX/1xi;

    invoke-virtual {v0}, LX/1xi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1xg;LX/1z2;LX/1z3;Landroid/text/SpannableStringBuilder;Landroid/text/TextPaint;LX/0Ot;LX/0Ot;Z)Landroid/text/SpannableStringBuilder;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1xg;",
            "LX/1z2;",
            "LX/1z3;",
            "Landroid/text/SpannableStringBuilder;",
            "Landroid/text/TextPaint;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;Z)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 1714541
    invoke-virtual {p4}, LX/1z3;->b()Ljava/lang/String;

    move-result-object v1

    move/from16 v0, p9

    invoke-static {p0, v1, v0}, LX/Aoa;->a(LX/Aoa;Ljava/lang/String;Z)LX/AoZ;

    move-result-object v3

    .line 1714542
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1714543
    iget-boolean v1, v3, LX/AoZ;->g:Z

    if-eqz v1, :cond_1

    .line 1714544
    invoke-static {v3, v5, p3}, LX/Aoa;->a(LX/AoZ;Landroid/text/SpannableStringBuilder;LX/1z2;)V

    .line 1714545
    iget-boolean v1, v3, LX/AoZ;->c:Z

    if-eqz v1, :cond_0

    move-object v1, p0

    move-object v2, p1

    move-object v4, p5

    move-object v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 1714546
    invoke-static/range {v1 .. v9}, LX/Aoa;->a(LX/Aoa;Ljava/lang/String;LX/AoZ;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;LX/1z3;Landroid/text/TextPaint;LX/0Ot;LX/0Ot;)V

    .line 1714547
    :cond_0
    invoke-virtual {p2, v5}, LX/1xg;->a(Landroid/text/SpannableStringBuilder;)V

    .line 1714548
    invoke-virtual {v5, p5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p5

    .line 1714549
    :goto_0
    return-object p5

    .line 1714550
    :cond_1
    invoke-virtual {p2, v5}, LX/1xg;->a(Landroid/text/SpannableStringBuilder;)V

    .line 1714551
    invoke-static {v3, v5, p3}, LX/Aoa;->a(LX/AoZ;Landroid/text/SpannableStringBuilder;LX/1z2;)V

    .line 1714552
    iget-boolean v1, v3, LX/AoZ;->c:Z

    if-eqz v1, :cond_2

    move-object v1, p0

    move-object v2, p1

    move-object v4, p5

    move-object v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 1714553
    invoke-static/range {v1 .. v9}, LX/Aoa;->a(LX/Aoa;Ljava/lang/String;LX/AoZ;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;LX/1z3;Landroid/text/TextPaint;LX/0Ot;LX/0Ot;)V

    .line 1714554
    :cond_2
    invoke-virtual {p5, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method
