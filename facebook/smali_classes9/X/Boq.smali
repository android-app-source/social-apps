.class public LX/Boq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/21s;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;

.field public d:LX/Bsz;


# direct methods
.method public constructor <init>(LX/21s;LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822034
    iput-object p1, p0, LX/Boq;->a:LX/21s;

    .line 1822035
    iput-object p2, p0, LX/Boq;->c:LX/0Sh;

    .line 1822036
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Boq;->b:Ljava/util/Set;

    .line 1822037
    return-void
.end method

.method public static a(LX/0QB;)LX/Boq;
    .locals 5

    .prologue
    .line 1822022
    const-class v1, LX/Boq;

    monitor-enter v1

    .line 1822023
    :try_start_0
    sget-object v0, LX/Boq;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822024
    sput-object v2, LX/Boq;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822025
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822026
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822027
    new-instance p0, LX/Boq;

    invoke-static {v0}, LX/21s;->a(LX/0QB;)LX/21s;

    move-result-object v3

    check-cast v3, LX/21s;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/Boq;-><init>(LX/21s;LX/0Sh;)V

    .line 1822028
    move-object v0, p0

    .line 1822029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Boq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/C96;)V
    .locals 2

    .prologue
    .line 1822018
    iget-object v0, p0, LX/Boq;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;-><init>(LX/Boq;LX/C96;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1822019
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1822020
    iget-object v0, p0, LX/Boq;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1822021
    return-void
.end method
