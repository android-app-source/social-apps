.class public LX/AVR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/graphics/Bitmap$CompressFormat;


# instance fields
.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679714
    const-class v0, LX/AVR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AVR;->a:Ljava/lang/String;

    .line 1679715
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    sput-object v0, LX/AVR;->b:Landroid/graphics/Bitmap$CompressFormat;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679717
    iput-object p1, p0, LX/AVR;->c:Ljava/util/concurrent/ExecutorService;

    .line 1679718
    return-void
.end method

.method public static b(Ljava/io/BufferedOutputStream;)V
    .locals 3

    .prologue
    .line 1679719
    if-nez p0, :cond_0

    .line 1679720
    :goto_0
    return-void

    .line 1679721
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1679722
    :catch_0
    move-exception v0

    .line 1679723
    sget-object v1, LX/AVR;->a:Ljava/lang/String;

    const-string v2, "error while closing BufferedOutputStream."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/nio/ByteBuffer;[FIILjava/util/concurrent/atomic/AtomicBoolean;LX/AVQ;)V
    .locals 10
    .param p7    # LX/AVQ;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1679724
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1679725
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v2, p4

    move v3, p5

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1679726
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1679727
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1679728
    invoke-virtual {v3, p2}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 1679729
    iget-object v9, p0, LX/AVR;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/facecast/ScreenCaptureUtil$1;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/facecast/ScreenCaptureUtil$1;-><init>(LX/AVR;Ljava/lang/String;Landroid/graphics/Bitmap;[FIILjava/util/concurrent/atomic/AtomicBoolean;LX/AVQ;)V

    const v1, 0x69a9cd6

    invoke-static {v9, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679730
    monitor-exit p0

    return-void

    .line 1679731
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
