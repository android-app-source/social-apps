.class public LX/C7o;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mX",
        "<",
        "LX/C7n;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/C7g;

.field private final d:LX/C7j;

.field private final e:LX/C7u;

.field private final f:LX/C82;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/C7g;LX/C7j;LX/C7u;LX/C82;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/C7n;",
            ">;TE;",
            "LX/25M;",
            "LX/C7g;",
            "LX/C7j;",
            "LX/C7u;",
            "LX/C82;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851826
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1851827
    iput-object p5, p0, LX/C7o;->c:LX/C7g;

    .line 1851828
    iput-object p6, p0, LX/C7o;->d:LX/C7j;

    .line 1851829
    iput-object p7, p0, LX/C7o;->e:LX/C7u;

    .line 1851830
    iput-object p8, p0, LX/C7o;->f:LX/C82;

    .line 1851831
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1851832
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 5

    .prologue
    .line 1851833
    check-cast p2, LX/C7n;

    const/4 v1, 0x0

    .line 1851834
    iget-object v0, p2, LX/C7n;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851835
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1851836
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 1851837
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1851838
    :goto_0
    return-object v0

    .line 1851839
    :cond_0
    iget v2, p2, LX/C7n;->a:I

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 1851840
    goto :goto_0

    .line 1851841
    :pswitch_0
    iget-object v1, p0, LX/C7o;->c:LX/C7g;

    const/4 v2, 0x0

    .line 1851842
    new-instance p0, LX/C7f;

    invoke-direct {p0, v1}, LX/C7f;-><init>(LX/C7g;)V

    .line 1851843
    sget-object p2, LX/C7g;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C7e;

    .line 1851844
    if-nez p2, :cond_1

    .line 1851845
    new-instance p2, LX/C7e;

    invoke-direct {p2}, LX/C7e;-><init>()V

    .line 1851846
    :cond_1
    invoke-static {p2, p1, v2, v2, p0}, LX/C7e;->a$redex0(LX/C7e;LX/1De;IILX/C7f;)V

    .line 1851847
    move-object p0, p2

    .line 1851848
    move-object v2, p0

    .line 1851849
    move-object v1, v2

    .line 1851850
    iget-object v2, v1, LX/C7e;->a:LX/C7f;

    iput-object v0, v2, LX/C7f;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1851851
    iget-object v2, v1, LX/C7e;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1851852
    move-object v0, v1

    .line 1851853
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1851854
    :pswitch_1
    iget-object v1, p0, LX/C7o;->d:LX/C7j;

    const/4 v2, 0x0

    .line 1851855
    new-instance p0, LX/C7i;

    invoke-direct {p0, v1}, LX/C7i;-><init>(LX/C7j;)V

    .line 1851856
    sget-object p2, LX/C7j;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C7h;

    .line 1851857
    if-nez p2, :cond_2

    .line 1851858
    new-instance p2, LX/C7h;

    invoke-direct {p2}, LX/C7h;-><init>()V

    .line 1851859
    :cond_2
    invoke-static {p2, p1, v2, v2, p0}, LX/C7h;->a$redex0(LX/C7h;LX/1De;IILX/C7i;)V

    .line 1851860
    move-object p0, p2

    .line 1851861
    move-object v2, p0

    .line 1851862
    move-object v1, v2

    .line 1851863
    iget-object v2, v1, LX/C7h;->a:LX/C7i;

    iput-object v0, v2, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1851864
    iget-object v2, v1, LX/C7h;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1851865
    move-object v0, v1

    .line 1851866
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1851867
    :pswitch_2
    iget-object v1, p0, LX/C7o;->e:LX/C7u;

    const/4 v2, 0x0

    .line 1851868
    new-instance p0, LX/C7t;

    invoke-direct {p0, v1}, LX/C7t;-><init>(LX/C7u;)V

    .line 1851869
    sget-object p2, LX/C7u;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C7s;

    .line 1851870
    if-nez p2, :cond_3

    .line 1851871
    new-instance p2, LX/C7s;

    invoke-direct {p2}, LX/C7s;-><init>()V

    .line 1851872
    :cond_3
    invoke-static {p2, p1, v2, v2, p0}, LX/C7s;->a$redex0(LX/C7s;LX/1De;IILX/C7t;)V

    .line 1851873
    move-object p0, p2

    .line 1851874
    move-object v2, p0

    .line 1851875
    move-object v1, v2

    .line 1851876
    iget-object v2, v1, LX/C7s;->a:LX/C7t;

    iput-object v0, v2, LX/C7t;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1851877
    iget-object v2, v1, LX/C7s;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1851878
    move-object v0, v1

    .line 1851879
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 1851880
    :pswitch_3
    iget-object v1, p0, LX/C7o;->f:LX/C82;

    const/4 v2, 0x0

    .line 1851881
    new-instance v3, LX/C81;

    invoke-direct {v3, v1}, LX/C81;-><init>(LX/C82;)V

    .line 1851882
    iget-object v4, v1, LX/C82;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C80;

    .line 1851883
    if-nez v4, :cond_4

    .line 1851884
    new-instance v4, LX/C80;

    invoke-direct {v4, v1}, LX/C80;-><init>(LX/C82;)V

    .line 1851885
    :cond_4
    invoke-static {v4, p1, v2, v2, v3}, LX/C80;->a$redex0(LX/C80;LX/1De;IILX/C81;)V

    .line 1851886
    move-object v3, v4

    .line 1851887
    move-object v2, v3

    .line 1851888
    move-object v1, v2

    .line 1851889
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    .line 1851890
    iget-object v2, v1, LX/C80;->a:LX/C81;

    iput-object v0, v2, LX/C81;->b:Ljava/lang/String;

    .line 1851891
    iget-object v2, v1, LX/C80;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1851892
    move-object v1, v1

    .line 1851893
    iget-object v0, p2, LX/C7n;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851894
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1851895
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/C84;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1851896
    iget-object v2, v1, LX/C80;->a:LX/C81;

    iput-object v0, v2, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851897
    iget-object v2, v1, LX/C80;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1851898
    move-object v1, v1

    .line 1851899
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pn;

    .line 1851900
    iget-object v2, v1, LX/C80;->a:LX/C81;

    iput-object v0, v2, LX/C81;->c:LX/1Pn;

    .line 1851901
    iget-object v2, v1, LX/C80;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1851902
    move-object v0, v1

    .line 1851903
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1851904
    const/4 v0, 0x0

    return v0
.end method
