.class public LX/CDJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pv;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/7zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7zi",
            "<",
            "LX/5JS;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1Z4;

.field public final c:LX/361;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1858993
    new-instance v0, LX/CDI;

    const-class v1, LX/5JS;

    invoke-direct {v0, v1}, LX/CDI;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/CDJ;->a:LX/7zi;

    return-void
.end method

.method public constructor <init>(LX/1Z4;LX/361;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858990
    iput-object p1, p0, LX/CDJ;->b:LX/1Z4;

    .line 1858991
    iput-object p2, p0, LX/CDJ;->c:LX/361;

    .line 1858992
    return-void
.end method

.method public static a(LX/0QB;)LX/CDJ;
    .locals 5

    .prologue
    .line 1858978
    const-class v1, LX/CDJ;

    monitor-enter v1

    .line 1858979
    :try_start_0
    sget-object v0, LX/CDJ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858980
    sput-object v2, LX/CDJ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858983
    new-instance p0, LX/CDJ;

    invoke-static {v0}, LX/1Z4;->a(LX/0QB;)LX/1Z4;

    move-result-object v3

    check-cast v3, LX/1Z4;

    invoke-static {v0}, LX/361;->a(LX/0QB;)LX/361;

    move-result-object v4

    check-cast v4, LX/361;

    invoke-direct {p0, v3, v4}, LX/CDJ;-><init>(LX/1Z4;LX/361;)V

    .line 1858984
    move-object v0, p0

    .line 1858985
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858986
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858987
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
