.class public final LX/Ag9;
.super LX/Ag8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ag8",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/3Hc;


# direct methods
.method public constructor <init>(LX/3Hc;)V
    .locals 1

    .prologue
    .line 1700337
    iput-object p1, p0, LX/Ag9;->b:LX/3Hc;

    invoke-direct {p0, p1}, LX/Ag8;-><init>(LX/3Hc;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1700338
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;

    .line 1700339
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    .line 1700340
    iget-object v1, p0, LX/Ag9;->b:LX/3Hc;

    .line 1700341
    iput-object v0, v1, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1700342
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 1700343
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->o()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->p()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1700344
    if-eqz v0, :cond_3

    .line 1700345
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_0

    .line 1700346
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-boolean v0, v0, LX/3Hc;->w:Z

    if-eqz v0, :cond_2

    .line 1700347
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 1700348
    :cond_0
    :goto_1
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    invoke-static {v0, v1}, LX/3Hc;->a$redex0(LX/3Hc;I)V

    .line 1700349
    return-void

    .line 1700350
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1700351
    :cond_2
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    goto :goto_1

    .line 1700352
    :cond_3
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-boolean v0, v0, LX/3Hc;->w:Z

    if-nez v0, :cond_4

    .line 1700353
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1700354
    :cond_4
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_0

    .line 1700355
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-boolean v0, v0, LX/3Hc;->w:Z

    if-eqz v0, :cond_5

    .line 1700356
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    goto :goto_1

    .line 1700357
    :cond_5
    iget-object v0, p0, LX/Ag9;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    goto :goto_1
.end method
