.class public final LX/AZr;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AZw;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AZt;


# direct methods
.method public constructor <init>(LX/AZt;)V
    .locals 0

    .prologue
    .line 1687497
    iput-object p1, p0, LX/AZr;->a:LX/AZt;

    invoke-direct {p0}, LX/1OM;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1687456
    new-instance v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;-><init>(Landroid/content/Context;)V

    .line 1687457
    iget-object v1, p0, LX/AZr;->a:LX/AZt;

    .line 1687458
    iput-object v1, v0, LX/AZX;->e:LX/AZT;

    .line 1687459
    iget-object v0, v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1687461
    check-cast p1, LX/AZw;

    .line 1687462
    iget-object v1, p1, LX/AZw;->l:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    iget-object v0, p0, LX/AZr;->a:LX/AZt;

    iget-object v0, v0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0, p2}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZl;

    const p2, 0x3e4ccccd    # 0.2f

    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 1687463
    iput-object v0, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->d:LX/AZl;

    .line 1687464
    iget-boolean v2, v0, LX/AZl;->f:Z

    if-eqz v2, :cond_1

    .line 1687465
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1687466
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->e:LX/AZx;

    if-nez v2, :cond_0

    .line 1687467
    new-instance v2, LX/AZx;

    invoke-direct {v2}, LX/AZx;-><init>()V

    iput-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->e:LX/AZx;

    .line 1687468
    :cond_0
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v5, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->e:LX/AZx;

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1687469
    invoke-virtual {v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b05fc

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1687470
    iget-object v5, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v5, v5, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1687471
    :goto_0
    sget-object v2, LX/AZv;->a:[I

    .line 1687472
    iget-object v5, v0, LX/AZl;->g:LX/AZk;

    move-object v5, v5

    .line 1687473
    invoke-virtual {v5}, LX/AZk;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 1687474
    :goto_1
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v5, v2, LX/AZw;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1687475
    iget-boolean v2, v0, LX/AZl;->h:Z

    move v2, v2

    .line 1687476
    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1687477
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->l:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    .line 1687478
    iget-boolean v3, v0, LX/AZl;->i:Z

    move v3, v3

    .line 1687479
    invoke-virtual {v2, v3}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->setSelected(Z)V

    .line 1687480
    return-void

    .line 1687481
    :cond_1
    iget-object v2, v0, LX/AZl;->b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1687482
    iget-object v5, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v5, v5, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->c:LX/1Ad;

    sget-object p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1687483
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    goto :goto_0

    .line 1687484
    :pswitch_0
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1687485
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1687486
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->q:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1687487
    :pswitch_1
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1687488
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1687489
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->q:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1687490
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->o:Landroid/widget/ProgressBar;

    .line 1687491
    iget-object v5, v0, LX/AZl;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    move v5, v5

    .line 1687492
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    .line 1687493
    :pswitch_2
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1687494
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1687495
    iget-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v2, v2, LX/AZw;->q:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_2
    move v2, v4

    .line 1687496
    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1687460
    iget-object v0, p0, LX/AZr;->a:LX/AZt;

    iget-object v0, v0, LX/AZt;->m:LX/AZm;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/AZr;->a:LX/AZt;

    iget-object v0, v0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0}, LX/AZZ;->e()I

    move-result v0

    goto :goto_0
.end method
