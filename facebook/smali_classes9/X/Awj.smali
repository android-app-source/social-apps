.class public final LX/Awj;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V
    .locals 0

    .prologue
    .line 1727018
    iput-object p1, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;B)V
    .locals 0

    .prologue
    .line 1726995
    invoke-direct {p0, p1}, LX/Awj;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V

    return-void
.end method

.method private a(FF)F
    .locals 3

    .prologue
    .line 1727017
    sub-float v1, p1, p2

    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getHeight()I

    move-result v0

    iget-object v2, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getWidth()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getHeight()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    div-float v0, v1, v0

    return v0

    :cond_0
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getWidth()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    .line 1727009
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727010
    const/4 v0, 0x0

    .line 1727011
    :goto_0
    return v0

    .line 1727012
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpan()F

    move-result v1

    invoke-direct {p0, v0, v1}, LX/Awj;->a(FF)F

    move-result v1

    .line 1727013
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Arf;

    .line 1727014
    iget-object v3, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, v3, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1727015
    invoke-interface {v0, v1}, LX/Arf;->a(F)V

    goto :goto_1

    .line 1727016
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1726999
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727000
    const/4 v0, 0x0

    .line 1727001
    :goto_0
    return v0

    .line 1727002
    :cond_0
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Arf;

    .line 1727003
    iget-object v3, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, v3, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1727004
    invoke-interface {v0}, LX/Arf;->a()V

    goto :goto_1

    .line 1727005
    :cond_2
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1727006
    iput-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    .line 1727007
    move v0, v1

    .line 1727008
    goto :goto_0
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 1726996
    iget-object v0, p0, LX/Awj;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    const/4 v1, 0x0

    .line 1726997
    iput-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    .line 1726998
    return-void
.end method
