.class public final LX/BJM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;",
        ">;",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BJN;


# direct methods
.method public constructor <init>(LX/BJN;)V
    .locals 0

    .prologue
    .line 1771802
    iput-object p1, p0, LX/BJM;->a:LX/BJN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1771803
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1771804
    if-eqz p1, :cond_0

    .line 1771805
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771806
    if-eqz v0, :cond_0

    .line 1771807
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771808
    check-cast v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1771809
    :cond_0
    const/4 v0, 0x0

    .line 1771810
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/5m9;

    invoke-direct {v1}, LX/5m9;-><init>()V

    .line 1771811
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771812
    check-cast v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1771813
    iput-object v0, v1, LX/5m9;->f:Ljava/lang/String;

    .line 1771814
    move-object v1, v1

    .line 1771815
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771816
    check-cast v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1771817
    iput-object v0, v1, LX/5m9;->h:Ljava/lang/String;

    .line 1771818
    move-object v1, v1

    .line 1771819
    new-instance v2, LX/5mE;

    invoke-direct {v2}, LX/5mE;-><init>()V

    .line 1771820
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771821
    check-cast v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a()D

    move-result-wide v4

    .line 1771822
    iput-wide v4, v2, LX/5mE;->a:D

    .line 1771823
    move-object v2, v2

    .line 1771824
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1771825
    check-cast v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->b()D

    move-result-wide v4

    .line 1771826
    iput-wide v4, v2, LX/5mE;->b:D

    .line 1771827
    move-object v0, v2

    .line 1771828
    invoke-virtual {v0}, LX/5mE;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    .line 1771829
    iput-object v0, v1, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1771830
    move-object v0, v1

    .line 1771831
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    goto :goto_0
.end method
