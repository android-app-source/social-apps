.class public LX/Cao;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8HH;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8nP;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/8nX;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Can;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1918625
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Cao;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/8nX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8HH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8nP;",
            ">;",
            "LX/8nX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1918590
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Cao;->d:Ljava/util/List;

    .line 1918591
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cao;->e:LX/0Px;

    .line 1918592
    iput-object p1, p0, LX/Cao;->a:LX/0Ot;

    .line 1918593
    iput-object p2, p0, LX/Cao;->b:LX/0Ot;

    .line 1918594
    iput-object p3, p0, LX/Cao;->c:LX/8nX;

    .line 1918595
    return-void
.end method

.method public static a(LX/0QB;)LX/Cao;
    .locals 9

    .prologue
    .line 1918596
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1918597
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1918598
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1918599
    if-nez v1, :cond_0

    .line 1918600
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1918601
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1918602
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1918603
    sget-object v1, LX/Cao;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1918604
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1918605
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1918606
    :cond_1
    if-nez v1, :cond_4

    .line 1918607
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1918608
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1918609
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1918610
    new-instance v7, LX/Cao;

    const/16 v1, 0x2e50

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v1, 0x35db

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v1, LX/8nX;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8nX;

    invoke-direct {v7, v8, p0, v1}, LX/Cao;-><init>(LX/0Ot;LX/0Ot;LX/8nX;)V

    .line 1918611
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1918612
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1918613
    if-nez v1, :cond_2

    .line 1918614
    sget-object v0, LX/Cao;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cao;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1918615
    :goto_1
    if-eqz v0, :cond_3

    .line 1918616
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1918617
    :goto_3
    check-cast v0, LX/Cao;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1918618
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1918619
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1918620
    :catchall_1
    move-exception v0

    .line 1918621
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1918622
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1918623
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1918624
    :cond_2
    :try_start_8
    sget-object v0, LX/Cao;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cao;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/Can;)V
    .locals 1

    .prologue
    .line 1918576
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918577
    iget-object v0, p0, LX/Cao;->e:LX/0Px;

    if-nez v0, :cond_0

    .line 1918578
    iget-object v0, p0, LX/Cao;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1918579
    invoke-virtual {p0}, LX/Cao;->b()V

    .line 1918580
    :goto_0
    return-void

    .line 1918581
    :cond_0
    iget-object v0, p0, LX/Cao;->e:LX/0Px;

    invoke-interface {p1, v0}, LX/Can;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1918582
    new-instance v1, LX/Cal;

    invoke-direct {v1, p0}, LX/Cal;-><init>(LX/Cao;)V

    .line 1918583
    iget-object v0, p0, LX/Cao;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cao;->f:Ljava/lang/String;

    const-string v2, "good_friends"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1918584
    iget-object v0, p0, LX/Cao;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nP;

    invoke-virtual {v0, v1}, LX/8nB;->a(LX/8JX;)V

    .line 1918585
    :goto_0
    return-void

    .line 1918586
    :cond_0
    iget-object v0, p0, LX/Cao;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cao;->f:Ljava/lang/String;

    const-string v2, "group"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cao;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918587
    iget-object v0, p0, LX/Cao;->c:LX/8nX;

    iget-object v2, p0, LX/Cao;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8nX;->a(Ljava/lang/Long;)LX/8nW;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/8nB;->a(LX/8JX;)V

    goto :goto_0

    .line 1918588
    :cond_1
    iget-object v0, p0, LX/Cao;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8HH;

    new-instance v1, LX/Cam;

    invoke-direct {v1, p0}, LX/Cam;-><init>(LX/Cao;)V

    invoke-virtual {v0, v1}, LX/8HH;->a(LX/7yX;)V

    goto :goto_0
.end method
