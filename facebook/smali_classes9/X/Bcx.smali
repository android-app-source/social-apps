.class public final LX/Bcx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bcn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bcn",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BcP;

.field public final synthetic b:LX/Bcz;


# direct methods
.method public constructor <init>(LX/Bcz;LX/BcP;)V
    .locals 0

    .prologue
    .line 1803095
    iput-object p1, p0, LX/Bcx;->b:LX/Bcz;

    iput-object p2, p0, LX/Bcx;->a:LX/BcP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1803093
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    const/4 v1, 0x0

    sget-object v2, LX/BcL;->INITIAL_LOAD:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803094
    return-void
.end method

.method public final a(LX/Bcm;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bcm",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1803085
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    .line 1803086
    invoke-virtual {v0}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    .line 1803087
    if-nez v1, :cond_0

    .line 1803088
    :goto_0
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    const/4 v1, 0x0

    sget-object v2, LX/BcL;->SUCCEEDED:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803089
    return-void

    .line 1803090
    :cond_0
    check-cast v1, LX/Bcu;

    .line 1803091
    new-instance v2, LX/Bcv;

    iget-object v3, v1, LX/Bcu;->l:LX/Bcw;

    invoke-direct {v2, v3, p1}, LX/Bcv;-><init>(LX/Bcw;LX/Bcm;)V

    move-object v1, v2

    .line 1803092
    invoke-virtual {v0, v1}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1803078
    if-eqz p1, :cond_0

    .line 1803079
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    const/4 v1, 0x1

    sget-object v2, LX/BcL;->SUCCEEDED:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803080
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1803083
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    sget-object v1, LX/BcL;->FAILED:LX/BcL;

    invoke-static {v0, p1, v1, p2}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803084
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1803081
    iget-object v0, p0, LX/Bcx;->a:LX/BcP;

    const/4 v1, 0x0

    sget-object v2, LX/BcL;->LOADING:LX/BcL;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803082
    return-void
.end method
