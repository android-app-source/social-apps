.class public final LX/BL8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V
    .locals 0

    .prologue
    .line 1775307
    iput-object p1, p0, LX/BL8;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1775308
    iget-object v0, p0, LX/BL8;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    .line 1775309
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {v1, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1775310
    new-instance v2, LX/89I;

    iget-wide v3, v1, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    sget-object v5, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v2, v3, v4, v5}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v1, v1, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1775311
    iput-object v1, v2, LX/89I;->c:Ljava/lang/String;

    .line 1775312
    move-object v1, v2

    .line 1775313
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 1775314
    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->m:LX/BJU;

    if-eqz v2, :cond_0

    .line 1775315
    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->m:LX/BJU;

    invoke-virtual {v2, v1}, LX/BJU;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1775316
    :cond_0
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->f:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "target_privacy_picker_group_selected"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1775317
    return-void
.end method
