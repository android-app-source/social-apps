.class public LX/BZD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:LX/BZG;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZM;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZI;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZE;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZK;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/BZJ;

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1797012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1797013
    return-void
.end method


# virtual methods
.method public getFormat()I
    .locals 1

    .prologue
    .line 1797014
    iget v0, p0, LX/BZD;->a:I

    return v0
.end method

.method public getImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797015
    iget-object v0, p0, LX/BZD;->f:Ljava/util/List;

    return-object v0
.end method

.method public getInfo_message_type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797010
    iget-object v0, p0, LX/BZD;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getMax_faces_support()I
    .locals 1

    .prologue
    .line 1797016
    iget v0, p0, LX/BZD;->j:I

    return v0
.end method

.method public getMetadata()LX/BZG;
    .locals 1

    .prologue
    .line 1797017
    iget-object v0, p0, LX/BZD;->c:LX/BZG;

    return-object v0
.end method

.method public getMin_faces_support()I
    .locals 1

    .prologue
    .line 1797018
    iget v0, p0, LX/BZD;->i:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797019
    iget-object v0, p0, LX/BZD;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getOffsets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797020
    iget-object v0, p0, LX/BZD;->e:Ljava/util/List;

    return-object v0
.end method

.method public getScene()LX/BZJ;
    .locals 1

    .prologue
    .line 1797021
    iget-object v0, p0, LX/BZD;->h:LX/BZJ;

    return-object v0
.end method

.method public getScripts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797022
    iget-object v0, p0, LX/BZD;->g:Ljava/util/List;

    return-object v0
.end method

.method public getUvs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797011
    iget-object v0, p0, LX/BZD;->d:Ljava/util/List;

    return-object v0
.end method

.method public setFormat(I)V
    .locals 0

    .prologue
    .line 1796987
    iput p1, p0, LX/BZD;->a:I

    .line 1796988
    return-void
.end method

.method public setImages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1796989
    iput-object p1, p0, LX/BZD;->f:Ljava/util/List;

    .line 1796990
    return-void
.end method

.method public setInfo_message_type(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796991
    iput-object p1, p0, LX/BZD;->k:Ljava/lang/String;

    .line 1796992
    return-void
.end method

.method public setMax_faces_support(I)V
    .locals 0

    .prologue
    .line 1796993
    iput p1, p0, LX/BZD;->j:I

    .line 1796994
    return-void
.end method

.method public setMetadata(LX/BZG;)V
    .locals 0

    .prologue
    .line 1796995
    iput-object p1, p0, LX/BZD;->c:LX/BZG;

    .line 1796996
    return-void
.end method

.method public setMin_faces_support(I)V
    .locals 0

    .prologue
    .line 1796997
    iput p1, p0, LX/BZD;->i:I

    .line 1796998
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796999
    iput-object p1, p0, LX/BZD;->b:Ljava/lang/String;

    .line 1797000
    return-void
.end method

.method public setOffsets(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1797001
    iput-object p1, p0, LX/BZD;->e:Ljava/util/List;

    .line 1797002
    return-void
.end method

.method public setScene(LX/BZJ;)V
    .locals 0

    .prologue
    .line 1797003
    iput-object p1, p0, LX/BZD;->h:LX/BZJ;

    .line 1797004
    return-void
.end method

.method public setScripts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1797005
    iput-object p1, p0, LX/BZD;->g:Ljava/util/List;

    .line 1797006
    return-void
.end method

.method public setUvs(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1797007
    iput-object p1, p0, LX/BZD;->d:Ljava/util/List;

    .line 1797008
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1797009
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Effect{mFormat="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/BZD;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMetadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->c:LX/BZG;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uvs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", offsets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mImages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScripts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScene="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZD;->h:LX/BZJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMin_faces_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZD;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMax_faces_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZD;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
