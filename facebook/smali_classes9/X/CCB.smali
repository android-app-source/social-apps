.class public LX/CCB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/3mL;

.field private final b:LX/CC8;

.field private final c:LX/87Y;

.field private final d:LX/CC5;

.field private final e:LX/CEw;

.field private final f:LX/CEp;

.field private final g:LX/0hB;

.field private final h:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/3mL;LX/CC8;LX/87Y;LX/CC5;LX/CEw;LX/CEp;LX/0hB;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1857264
    iput-object p1, p0, LX/CCB;->a:LX/3mL;

    .line 1857265
    iput-object p2, p0, LX/CCB;->b:LX/CC8;

    .line 1857266
    iput-object p3, p0, LX/CCB;->c:LX/87Y;

    .line 1857267
    iput-object p4, p0, LX/CCB;->d:LX/CC5;

    .line 1857268
    iput-object p5, p0, LX/CCB;->e:LX/CEw;

    .line 1857269
    iput-object p6, p0, LX/CCB;->f:LX/CEp;

    .line 1857270
    iput-object p7, p0, LX/CCB;->g:LX/0hB;

    .line 1857271
    iput-object p8, p0, LX/CCB;->h:Ljava/util/concurrent/Executor;

    .line 1857272
    return-void
.end method

.method public static a(LX/0QB;)LX/CCB;
    .locals 12

    .prologue
    .line 1857273
    const-class v1, LX/CCB;

    monitor-enter v1

    .line 1857274
    :try_start_0
    sget-object v0, LX/CCB;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1857275
    sput-object v2, LX/CCB;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1857276
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857277
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1857278
    new-instance v3, LX/CCB;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    const-class v5, LX/CC8;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/CC8;

    invoke-static {v0}, LX/87Y;->b(LX/0QB;)LX/87Y;

    move-result-object v6

    check-cast v6, LX/87Y;

    invoke-static {v0}, LX/CC5;->a(LX/0QB;)LX/CC5;

    move-result-object v7

    check-cast v7, LX/CC5;

    invoke-static {v0}, LX/CEw;->a(LX/0QB;)LX/CEw;

    move-result-object v8

    check-cast v8, LX/CEw;

    invoke-static {v0}, LX/CEp;->b(LX/0QB;)LX/CEp;

    move-result-object v9

    check-cast v9, LX/CEp;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-direct/range {v3 .. v11}, LX/CCB;-><init>(LX/3mL;LX/CC8;LX/87Y;LX/CC5;LX/CEw;LX/CEp;LX/0hB;Ljava/util/concurrent/Executor;)V

    .line 1857279
    move-object v0, v3

    .line 1857280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1857281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1857282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1857283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pf;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 12
    .param p2    # LX/1Pf;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Pf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x6

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1857284
    iget-object v0, p0, LX/CCB;->e:LX/CEw;

    .line 1857285
    iget-object v1, v0, LX/CEw;->a:LX/1c9;

    move-object v0, v1

    .line 1857286
    if-nez v0, :cond_0

    .line 1857287
    iget-object v0, p0, LX/CCB;->f:LX/CEp;

    iget-object v1, p0, LX/CCB;->g:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->b()F

    move-result v1

    invoke-virtual {v0, v1}, LX/CEp;->a(F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1857288
    new-instance v1, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerHScrollComponentSpec$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerHScrollComponentSpec$1;-><init>(LX/CCB;LX/1Pf;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object v2, p0, LX/CCB;->h:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1857289
    const/4 v0, 0x0

    .line 1857290
    :goto_0
    return-object v0

    .line 1857291
    :cond_0
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1857292
    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1857293
    iget-object v0, p0, LX/CCB;->b:LX/CC8;

    iget-object v2, p0, LX/CCB;->e:LX/CEw;

    .line 1857294
    iget-object v3, v2, LX/CEw;->a:LX/1c9;

    move-object v2, v3

    .line 1857295
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1857296
    invoke-virtual {v2}, LX/1c9;->size()I

    move-result v10

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v10, :cond_2

    invoke-virtual {v2, v4}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1857297
    sget-object v11, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v11, v3}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 1857298
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1857299
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1857300
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1857301
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v3

    .line 1857302
    iput v9, v3, LX/3mP;->b:I

    .line 1857303
    move-object v3, v3

    .line 1857304
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1857305
    iput-object v4, v3, LX/3mP;->c:Ljava/lang/Integer;

    .line 1857306
    move-object v3, v3

    .line 1857307
    iput-object p3, v3, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1857308
    move-object v3, v3

    .line 1857309
    iput-object v1, v3, LX/3mP;->e:LX/0jW;

    .line 1857310
    move-object v3, v3

    .line 1857311
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v1

    .line 1857312
    iput-object v1, v3, LX/3mP;->d:LX/25L;

    .line 1857313
    move-object v1, v3

    .line 1857314
    invoke-virtual {v1}, LX/3mP;->a()LX/25M;

    move-result-object v4

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/CC8;->a(Landroid/content/Context;LX/0Px;LX/1Pf;LX/25M;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CC7;

    move-result-object v1

    .line 1857315
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1857316
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v2, -0x74569f26

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1857317
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 1857318
    if-nez v0, :cond_3

    .line 1857319
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082a3e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1857320
    :cond_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0xc

    invoke-interface {v2, v9, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v4, 0x7f0b1daa

    invoke-virtual {v0, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v4, 0x3

    invoke-interface {v0, v4, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v3, p0, LX/CCB;->a:LX/3mL;

    invoke-virtual {v3, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/CCB;->d:LX/CC5;

    const/4 v2, 0x0

    .line 1857321
    new-instance v3, LX/CC4;

    invoke-direct {v3, v1}, LX/CC4;-><init>(LX/CC5;)V

    .line 1857322
    sget-object v4, LX/CC5;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CC3;

    .line 1857323
    if-nez v4, :cond_4

    .line 1857324
    new-instance v4, LX/CC3;

    invoke-direct {v4}, LX/CC3;-><init>()V

    .line 1857325
    :cond_4
    invoke-static {v4, p1, v2, v2, v3}, LX/CC3;->a$redex0(LX/CC3;LX/1De;IILX/CC4;)V

    .line 1857326
    move-object v3, v4

    .line 1857327
    move-object v2, v3

    .line 1857328
    move-object v1, v2

    .line 1857329
    iget-object v2, v1, LX/CC3;->a:LX/CC4;

    iput-object p3, v2, LX/CC4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1857330
    iget-object v2, v1, LX/CC3;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1857331
    move-object v1, v1

    .line 1857332
    iget-object v2, v1, LX/CC3;->a:LX/CC4;

    iput-object p2, v2, LX/CC4;->b:LX/1Pr;

    .line 1857333
    iget-object v2, v1, LX/CC3;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1857334
    move-object v1, v1

    .line 1857335
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v6, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a009a

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2
.end method
