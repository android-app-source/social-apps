.class public LX/BM1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/BM1;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17T;

.field private final d:LX/0Zb;

.field private final e:LX/0s6;

.field public f:Landroid/content/BroadcastReceiver;

.field public g:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17T;LX/0Zb;LX/0s6;Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776740
    iput-object p1, p0, LX/BM1;->a:Landroid/content/Context;

    .line 1776741
    iput-object p2, p0, LX/BM1;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1776742
    iput-object p3, p0, LX/BM1;->c:LX/17T;

    .line 1776743
    iput-object p4, p0, LX/BM1;->d:LX/0Zb;

    .line 1776744
    iput-object p5, p0, LX/BM1;->e:LX/0s6;

    .line 1776745
    iput-object p6, p0, LX/BM1;->g:Landroid/content/pm/PackageManager;

    .line 1776746
    return-void
.end method

.method public static a(LX/0QB;)LX/BM1;
    .locals 10

    .prologue
    .line 1776726
    sget-object v0, LX/BM1;->h:LX/BM1;

    if-nez v0, :cond_1

    .line 1776727
    const-class v1, LX/BM1;

    monitor-enter v1

    .line 1776728
    :try_start_0
    sget-object v0, LX/BM1;->h:LX/BM1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1776729
    if-eqz v2, :cond_0

    .line 1776730
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1776731
    new-instance v3, LX/BM1;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v6

    check-cast v6, LX/17T;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v8

    check-cast v8, LX/0s6;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v9

    check-cast v9, Landroid/content/pm/PackageManager;

    invoke-direct/range {v3 .. v9}, LX/BM1;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17T;LX/0Zb;LX/0s6;Landroid/content/pm/PackageManager;)V

    .line 1776732
    move-object v0, v3

    .line 1776733
    sput-object v0, LX/BM1;->h:LX/BM1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1776734
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1776735
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1776736
    :cond_1
    sget-object v0, LX/BM1;->h:LX/BM1;

    return-object v0

    .line 1776737
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1776738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/BM1;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 1776711
    iget-object v0, p0, LX/BM1;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1776712
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1776713
    const-string v2, "al_applink_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1776714
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1776715
    const-string v3, "extras"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1776716
    move-object v0, v0

    .line 1776717
    const-string v1, "al_applink_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "extras"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1776718
    const-string v2, "com.facebook.platform.extra.IS_COMPOSE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1776719
    iget-object v1, p0, LX/BM1;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x3ec

    invoke-interface {v1, v0, v2, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1776720
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1776747
    iget-object v7, p0, LX/BM1;->d:LX/0Zb;

    new-instance v0, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;

    const-string v2, "platform_attribution_impression"

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;-><init>(LX/BM1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776748
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1776725
    iget-object v0, p0, LX/BM1;->e:LX/0s6;

    invoke-virtual {v0, p1}, LX/01H;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1776723
    iget-object v7, p0, LX/BM1;->d:LX/0Zb;

    new-instance v0, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;

    const-string v2, "platform_attribution_conversion"

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;-><init>(LX/BM1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776724
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1776721
    iget-object v7, p0, LX/BM1;->d:LX/0Zb;

    new-instance v0, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;

    const-string v2, "platform_attribution_conversion"

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;-><init>(LX/BM1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1776722
    return-void
.end method
