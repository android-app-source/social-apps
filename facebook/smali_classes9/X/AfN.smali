.class public final LX/AfN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AfO;


# direct methods
.method public constructor <init>(LX/AfO;)V
    .locals 0

    .prologue
    .line 1699219
    iput-object p1, p0, LX/AfN;->a:LX/AfO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1699220
    sget-object v0, LX/AfO;->a:Ljava/lang/String;

    const-string v1, "Failed to get subscription."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699221
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699208
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;

    .line 1699209
    iget-object v0, p0, LX/AfN;->a:LX/AfO;

    iget-object v0, v0, LX/AfO;->i:LX/AfJ;

    if-nez v0, :cond_1

    .line 1699210
    :cond_0
    :goto_0
    return-void

    .line 1699211
    :cond_1
    if-nez p1, :cond_2

    .line 1699212
    goto :goto_0

    .line 1699213
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;

    move-result-object v0

    .line 1699214
    if-eqz v0, :cond_0

    .line 1699215
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateSubscriptionModel$PinnedCommentEventModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    .line 1699216
    if-nez v0, :cond_3

    .line 1699217
    iget-object v0, p0, LX/AfN;->a:LX/AfO;

    iget-object v0, v0, LX/AfO;->i:LX/AfJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AfJ;->c(LX/Aeu;)V

    goto :goto_0

    .line 1699218
    :cond_3
    iget-object v1, p0, LX/AfN;->a:LX/AfO;

    iget-object v1, v1, LX/AfO;->i:LX/AfJ;

    invoke-static {v0}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AfJ;->c(LX/Aeu;)V

    goto :goto_0
.end method
