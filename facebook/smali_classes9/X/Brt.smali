.class public LX/Brt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:Landroid/text/Layout$Alignment;

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1826777
    const v0, 0x7f0b004e

    sput v0, LX/Brt;->a:I

    .line 1826778
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/Brt;->b:Landroid/text/Layout$Alignment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826780
    return-void
.end method

.method public static a(LX/0QB;)LX/Brt;
    .locals 3

    .prologue
    .line 1826781
    const-class v1, LX/Brt;

    monitor-enter v1

    .line 1826782
    :try_start_0
    sget-object v0, LX/Brt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826783
    sput-object v2, LX/Brt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826784
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826785
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1826786
    new-instance v0, LX/Brt;

    invoke-direct {v0}, LX/Brt;-><init>()V

    .line 1826787
    move-object v0, v0

    .line 1826788
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826789
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Brt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826790
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826791
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
