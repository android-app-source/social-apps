.class public final LX/C7S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3AH;

.field public final synthetic b:LX/3AC;


# direct methods
.method public constructor <init>(LX/3AC;LX/3AH;)V
    .locals 0

    .prologue
    .line 1851342
    iput-object p1, p0, LX/C7S;->b:LX/3AC;

    iput-object p2, p0, LX/C7S;->a:LX/3AH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1851343
    sget-object v0, LX/3AC;->a:Ljava/lang/Class;

    const-string v1, "Cannot fetch translation for story."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1851344
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1851345
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 1851346
    if-eqz p1, :cond_0

    .line 1851347
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1851348
    if-eqz v0, :cond_0

    .line 1851349
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1851350
    check-cast v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1851351
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1851352
    check-cast v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1851353
    :cond_0
    sget-object v0, LX/3AC;->a:Ljava/lang/Class;

    const-string v1, "Cannot fetch translation for story."

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null Story Translation"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1851354
    iget-object v0, p0, LX/C7S;->a:LX/3AH;

    const/4 v1, 0x0

    .line 1851355
    iput-object v1, v0, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    .line 1851356
    iget-object v0, p0, LX/C7S;->a:LX/3AH;

    .line 1851357
    iput-boolean v4, v0, LX/3AH;->b:Z

    .line 1851358
    iget-object v0, p0, LX/C7S;->b:LX/3AC;

    iget-object v0, v0, LX/3AC;->d:LX/C7Q;

    invoke-virtual {v0}, LX/C7Q;->a()V

    .line 1851359
    :goto_0
    return-void

    .line 1851360
    :cond_1
    iget-object v0, p0, LX/C7S;->b:LX/3AC;

    iget-object v0, v0, LX/3AC;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1851361
    iget-object v1, p0, LX/C7S;->a:LX/3AH;

    .line 1851362
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1851363
    check-cast v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v0

    .line 1851364
    iput-object v0, v1, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    .line 1851365
    iget-object v0, p0, LX/C7S;->a:LX/3AH;

    .line 1851366
    iput-boolean v4, v0, LX/3AH;->b:Z

    .line 1851367
    iget-object v0, p0, LX/C7S;->b:LX/3AC;

    iget-object v0, v0, LX/3AC;->d:LX/C7Q;

    invoke-virtual {v0}, LX/C7Q;->a()V

    goto :goto_0
.end method
