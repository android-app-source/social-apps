.class public final LX/BmQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bm9;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V
    .locals 0

    .prologue
    .line 1818226
    iput-object p1, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 6

    .prologue
    .line 1818227
    if-nez p1, :cond_1

    .line 1818228
    iget-object v0, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    .line 1818229
    :cond_0
    :goto_0
    iget-object v0, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818230
    iget-object v0, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818231
    return-void

    .line 1818232
    :cond_1
    iget-object v0, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818233
    iget-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818234
    if-nez v0, :cond_0

    .line 1818235
    iget-object v0, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818236
    iget-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818237
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1818238
    iget-object v1, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818239
    iget-object v1, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->getStartTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J

    move-result-wide v2

    iget-object v1, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->getEndTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1818240
    const/16 v1, 0xb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1818241
    iget-object v1, p0, LX/BmQ;->a:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    goto :goto_0
.end method
