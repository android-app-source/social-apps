.class public final LX/AfM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AfO;


# direct methods
.method public constructor <init>(LX/AfO;)V
    .locals 0

    .prologue
    .line 1699191
    iput-object p1, p0, LX/AfM;->a:LX/AfO;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699192
    iget-object v0, p0, LX/AfM;->a:LX/AfO;

    iget-object v0, v0, LX/AfO;->e:LX/03V;

    sget-object v1, LX/AfO;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch pinned comment in the beginning."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699193
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699194
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1699195
    iget-object v0, p0, LX/AfM;->a:LX/AfO;

    iget-object v0, v0, LX/AfO;->i:LX/AfJ;

    if-nez v0, :cond_0

    .line 1699196
    :goto_0
    return-void

    .line 1699197
    :cond_0
    if-eqz p1, :cond_1

    .line 1699198
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699199
    if-eqz v0, :cond_1

    .line 1699200
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699201
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1699202
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699203
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel$LatestPinnedCommentEventModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1699204
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699205
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel$LatestPinnedCommentEventModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveLatestPinnedCommentEventQueryModel$FeedbackModel$LatestPinnedCommentEventModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1699206
    :cond_1
    goto :goto_0

    .line 1699207
    :cond_2
    iget-object v1, p0, LX/AfM;->a:LX/AfO;

    iget-object v1, v1, LX/AfO;->i:LX/AfJ;

    invoke-static {v0}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AfJ;->c(LX/Aeu;)V

    goto :goto_0
.end method
