.class public LX/BTl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:LX/BT0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787964
    return-void
.end method

.method public static a(LX/0QB;)LX/BTl;
    .locals 1

    .prologue
    .line 1787965
    new-instance v0, LX/BTl;

    invoke-direct {v0}, LX/BTl;-><init>()V

    .line 1787966
    move-object v0, v0

    .line 1787967
    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 1787968
    iget v0, p0, LX/BTl;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1787969
    iget-object v0, p0, LX/BTl;->d:LX/BT0;

    invoke-virtual {v0}, LX/BT0;->a()I

    move-result v0

    sub-int v0, p1, v0

    iget v1, p0, LX/BTl;->c:I

    sub-int/2addr v0, v1

    .line 1787970
    iget v1, p0, LX/BTl;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/BTl;->a:I

    div-int/2addr v0, v1

    iget v1, p0, LX/BTl;->c:I

    add-int/2addr v0, v1

    return v0

    .line 1787971
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 1787972
    iget v0, p0, LX/BTl;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1787973
    iget v0, p0, LX/BTl;->c:I

    sub-int v0, p1, v0

    .line 1787974
    iget v1, p0, LX/BTl;->a:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/BTl;->b:I

    div-int/2addr v0, v1

    .line 1787975
    iget-object v1, p0, LX/BTl;->d:LX/BT0;

    invoke-virtual {v1}, LX/BT0;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, LX/BTl;->c:I

    add-int/2addr v0, v1

    return v0

    .line 1787976
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
