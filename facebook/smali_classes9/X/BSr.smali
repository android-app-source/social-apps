.class public LX/BSr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final A:Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;

.field public B:LX/BTG;

.field public C:Ljava/util/concurrent/Future;

.field public D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

.field public E:LX/ATX;

.field public F:Z

.field public final G:LX/9f2;

.field public final H:LX/BSm;

.field public final b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

.field public final c:LX/0Sh;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/2MV;

.field public final f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

.field public final g:LX/03V;

.field public final h:LX/BSu;

.field public final i:LX/23P;

.field public final j:Landroid/net/Uri;

.field private final k:LX/BTg;

.field private final l:LX/BTh;

.field private m:LX/BQj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/60x;

.field public final o:LX/BT9;

.field private final p:LX/BSk;

.field private q:LX/BSj;

.field public final r:LX/BTj;

.field public s:LX/BTi;

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9fk;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0hB;

.field private final v:LX/0ad;

.field public final w:LX/9fh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9fi;",
            ">;"
        }
    .end annotation
.end field

.field private y:Landroid/content/Context;

.field private z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1786104
    const-class v0, LX/BSr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BSr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2MV;LX/03V;LX/BSu;LX/0Sh;LX/BTA;Landroid/content/Context;LX/0Ot;LX/BTG;LX/BSk;LX/BTj;Ljava/util/Set;Ljava/util/concurrent/ExecutorService;Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;Landroid/net/Uri;Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;LX/ATX;LX/9fh;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;LX/0hB;LX/23P;LX/BTg;LX/0ad;LX/BTh;)V
    .locals 15
    .param p12    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # LX/ATX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/9fh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p18    # Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2MV;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/BSu;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/BTA;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "LX/BTG;",
            "LX/BSk;",
            "LX/BTj;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/video/creativeediting/logging/VideoEditGalleryFunnelLogger;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;",
            "Landroid/net/Uri;",
            "Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;",
            "LX/ATX;",
            "LX/9fh;",
            "Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;",
            "LX/0hB;",
            "LX/23P;",
            "LX/BTg;",
            "LX/0ad;",
            "LX/BTh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1786147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786148
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/BSr;->t:Ljava/util/List;

    .line 1786149
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/BSr;->x:LX/0am;

    .line 1786150
    new-instance v1, Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;

    invoke-direct {v1}, Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;-><init>()V

    iput-object v1, p0, LX/BSr;->A:Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;

    .line 1786151
    new-instance v1, LX/BSl;

    invoke-direct {v1, p0}, LX/BSl;-><init>(LX/BSr;)V

    iput-object v1, p0, LX/BSr;->G:LX/9f2;

    .line 1786152
    new-instance v1, LX/BSn;

    invoke-direct {v1, p0}, LX/BSn;-><init>(LX/BSr;)V

    iput-object v1, p0, LX/BSr;->H:LX/BSm;

    .line 1786153
    invoke-static/range {p13 .. p13}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1786154
    move-object/from16 v0, p1

    iput-object v0, p0, LX/BSr;->e:LX/2MV;

    .line 1786155
    move-object/from16 v0, p2

    iput-object v0, p0, LX/BSr;->g:LX/03V;

    .line 1786156
    move-object/from16 v0, p3

    iput-object v0, p0, LX/BSr;->h:LX/BSu;

    .line 1786157
    move-object/from16 v0, p4

    iput-object v0, p0, LX/BSr;->c:LX/0Sh;

    .line 1786158
    move-object/from16 v0, p18

    iput-object v0, p0, LX/BSr;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1786159
    move-object/from16 v0, p22

    iput-object v0, p0, LX/BSr;->v:LX/0ad;

    .line 1786160
    move-object/from16 v0, p23

    iput-object v0, p0, LX/BSr;->l:LX/BTh;

    .line 1786161
    sget-object v4, LX/7Sv;->NONE:LX/7Sv;

    .line 1786162
    invoke-virtual/range {p15 .. p15}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p15 .. p15}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1786163
    sget-object v4, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    .line 1786164
    :cond_0
    invoke-virtual/range {p15 .. p15}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->n()Landroid/view/ViewStub;

    move-result-object v6

    invoke-virtual/range {p15 .. p15}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v7

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v8

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->l()Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    move-result-object v9

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->m()Landroid/view/ViewStub;

    move-result-object v11

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v12

    move-object/from16 v1, p5

    move-object/from16 v2, p14

    move-object v13, p0

    move-object v14, p0

    invoke-virtual/range {v1 .. v14}, LX/BTA;->a(Landroid/net/Uri;Landroid/net/Uri;LX/7Sv;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/model/VideoTrimParams;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/BSr;LX/BSr;)LX/BT9;

    move-result-object v1

    iput-object v1, p0, LX/BSr;->o:LX/BT9;

    .line 1786165
    move-object/from16 v0, p9

    iput-object v0, p0, LX/BSr;->p:LX/BSk;

    .line 1786166
    move-object/from16 v0, p10

    iput-object v0, p0, LX/BSr;->r:LX/BTj;

    .line 1786167
    move-object/from16 v0, p12

    iput-object v0, p0, LX/BSr;->d:Ljava/util/concurrent/ExecutorService;

    .line 1786168
    move-object/from16 v0, p13

    iput-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786169
    move-object/from16 v0, p14

    iput-object v0, p0, LX/BSr;->j:Landroid/net/Uri;

    .line 1786170
    move-object/from16 v0, p15

    iput-object v0, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1786171
    move-object/from16 v0, p16

    iput-object v0, p0, LX/BSr;->E:LX/ATX;

    .line 1786172
    move-object/from16 v0, p17

    iput-object v0, p0, LX/BSr;->w:LX/9fh;

    .line 1786173
    move-object/from16 v0, p19

    iput-object v0, p0, LX/BSr;->u:LX/0hB;

    .line 1786174
    move-object/from16 v0, p6

    iput-object v0, p0, LX/BSr;->y:Landroid/content/Context;

    .line 1786175
    move-object/from16 v0, p7

    iput-object v0, p0, LX/BSr;->z:LX/0Ot;

    .line 1786176
    move-object/from16 v0, p20

    iput-object v0, p0, LX/BSr;->i:LX/23P;

    .line 1786177
    move-object/from16 v0, p8

    iput-object v0, p0, LX/BSr;->B:LX/BTG;

    .line 1786178
    move-object/from16 v0, p21

    iput-object v0, p0, LX/BSr;->k:LX/BTg;

    .line 1786179
    iget-object v1, p0, LX/BSr;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-virtual {v1}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q()Ljava/lang/String;

    move-result-object v2

    .line 1786180
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1786181
    invoke-interface/range {p11 .. p11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BQj;

    .line 1786182
    invoke-virtual {v1}, LX/BQj;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1786183
    iput-object v1, p0, LX/BSr;->m:LX/BQj;

    .line 1786184
    :cond_2
    return-void
.end method

.method public static a(LX/BSr;Landroid/view/View;)LX/9fk;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1786126
    iget-object v0, p0, LX/BSr;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1786127
    iget-boolean v1, v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->d:Z

    move v0, v1

    .line 1786128
    if-nez v0, :cond_0

    .line 1786129
    const/4 v0, 0x0

    .line 1786130
    :goto_0
    return-object v0

    .line 1786131
    :cond_0
    const v0, 0x7f0d3096

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 1786132
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    const v1, 0x7f0d30a0

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;

    .line 1786133
    iget-object v1, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v1

    const v3, 0x7f0d30a2

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1786134
    iget-object v3, p0, LX/BSr;->p:LX/BSk;

    iget-object v4, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v4

    .line 1786135
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v4, v5

    .line 1786136
    new-instance v6, LX/BSj;

    const-class v5, Landroid/content/Context;

    invoke-interface {v3, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v6, v4, v0, v1, v5}, LX/BSj;-><init>(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;Landroid/view/View;Landroid/content/Context;)V

    .line 1786137
    move-object v0, v6

    .line 1786138
    iput-object v0, p0, LX/BSr;->q:LX/BSj;

    .line 1786139
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    .line 1786140
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v0, v1

    .line 1786141
    iget-object v1, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1786142
    iget-object v3, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v3

    .line 1786143
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setCropRect(Landroid/graphics/RectF;)V

    .line 1786144
    new-instance v0, LX/9fk;

    iget-object v1, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/BSr;->G:LX/9f2;

    iget-object v4, p0, LX/BSr;->q:LX/BSj;

    iget-object v5, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786145
    iget-object v6, v5, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v5, v6

    .line 1786146
    const v6, 0x7f0d0d05

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    const v7, 0x7f0d0d06

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v0 .. v7}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/BSr;LX/9fk;)V
    .locals 2

    .prologue
    .line 1786105
    iget-object v0, p0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1786106
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1786107
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1786108
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786109
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 1786110
    iget-object v1, p1, LX/9fk;->b:LX/9ea;

    move-object v1, v1

    .line 1786111
    invoke-interface {v1}, LX/9ea;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1786112
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1786113
    invoke-interface {v0}, LX/9ea;->h()V

    .line 1786114
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/9fk;->a(Z)V

    .line 1786115
    iget-object v0, p0, LX/BSr;->m:LX/BQj;

    if-eqz v0, :cond_0

    .line 1786116
    iget-object v0, p1, LX/9fk;->b:LX/9ea;

    move-object v0, v0

    .line 1786117
    invoke-interface {v0}, LX/9ea;->j()Ljava/lang/Object;

    move-result-object v0

    .line 1786118
    instance-of v1, v0, LX/5SK;

    if-eqz v1, :cond_0

    .line 1786119
    iget-object v1, p0, LX/BSr;->m:LX/BQj;

    check-cast v0, LX/5SK;

    .line 1786120
    sget-object p0, LX/BQi;->a:[I

    invoke-virtual {v0}, LX/5SK;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1786121
    :cond_0
    :goto_0
    return-void

    .line 1786122
    :pswitch_0
    const-string p0, "android_profile_video_crop"

    const-string p1, "addRemoveSound"

    invoke-static {v1, p0, p1}, LX/BQj;->a(LX/BQj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1786123
    goto :goto_0

    .line 1786124
    :pswitch_1
    const-string p0, "android_profile_video_trim"

    const-string p1, "addRemoveSound"

    invoke-static {v1, p0, p1}, LX/BQj;->a(LX/BQj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1786125
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/BSr;I)V
    .locals 4

    .prologue
    .line 1786030
    iget-object v0, p0, LX/BSr;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1786031
    iget-boolean v1, v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    move v0, v1

    .line 1786032
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BSr;->s:LX/BTi;

    if-nez v0, :cond_1

    .line 1786033
    :cond_0
    :goto_0
    return-void

    .line 1786034
    :cond_1
    iget-object v0, p0, LX/BSr;->s:LX/BTi;

    .line 1786035
    iget v1, v0, LX/BTi;->d:I

    div-int v1, p1, v1

    iget v2, v0, LX/BTi;->d:I

    mul-int/2addr v1, v2

    .line 1786036
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "video_editing_frame_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/BTi;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1786037
    iget-object v2, v0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1786038
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1786039
    const/4 v1, 0x0

    .line 1786040
    :goto_1
    move-object v0, v1

    .line 1786041
    if-eqz v0, :cond_2

    .line 1786042
    iget-object v1, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 1786043
    :cond_2
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->d()V

    goto :goto_0

    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method public static l(LX/BSr;)V
    .locals 5

    .prologue
    .line 1786096
    iget-object v0, p0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fk;

    .line 1786097
    iget-object v2, v0, LX/9fk;->b:LX/9ea;

    move-object v2, v2

    .line 1786098
    invoke-interface {v2}, LX/9ea;->j()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1786099
    iget-object v4, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a:LX/5SK;

    move-object v3, v4

    .line 1786100
    if-ne v2, v3, :cond_0

    .line 1786101
    iget-object v1, v0, LX/9fk;->b:LX/9ea;

    move-object v0, v1

    .line 1786102
    invoke-interface {v0}, LX/9ea;->i()V

    .line 1786103
    :cond_1
    return-void
.end method

.method public static o(LX/BSr;)V
    .locals 10

    .prologue
    .line 1786185
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BSr;->F:Z

    .line 1786186
    invoke-static {p0}, LX/BSr;->q(LX/BSr;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v8

    .line 1786187
    iget-object v0, p0, LX/BSr;->B:LX/BTG;

    iget-object v1, p0, LX/BSr;->j:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786188
    iget-object v4, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    move-object v3, v4

    .line 1786189
    iget-object v4, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getVideoDurationMs()I

    move-result v4

    iget-object v5, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v5}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v5

    .line 1786190
    iget-boolean v6, v5, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    move v5, v6

    .line 1786191
    invoke-static {v8}, LX/63w;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)I

    move-result v6

    invoke-virtual {v8}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v7

    invoke-virtual {v8}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v8

    .line 1786192
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "video_editing_cancel"

    invoke-direct {v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "video_editing_module"

    .line 1786193
    iput-object p0, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1786194
    move-object v9, v9

    .line 1786195
    sget-object p0, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {p0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {p0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {p0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTE;->ORIGINAL_LENGTH:LX/BTE;

    invoke-virtual {p0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTE;->TRIMMED_LENGTH:LX/BTE;

    invoke-virtual {p0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTE;->IS_MUTED:LX/BTE;

    invoke-virtual {p0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTE;->ROTATION:LX/BTE;

    invoke-virtual {p0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object p0, LX/BTE;->HD_UPLOAD_STATE:LX/BTE;

    invoke-virtual {p0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 1786196
    iget-object p0, v0, LX/BTG;->a:LX/0Zb;

    invoke-interface {p0, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1786197
    return-void
.end method

.method public static p(LX/BSr;)V
    .locals 8

    .prologue
    .line 1786073
    iget-object v0, p0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fk;

    .line 1786074
    iget-object v2, v0, LX/9fk;->b:LX/9ea;

    move-object v0, v2

    .line 1786075
    check-cast v0, LX/BSi;

    invoke-interface {v0}, LX/BSi;->k()V

    goto :goto_0

    .line 1786076
    :cond_0
    iget-object v0, p0, LX/BSr;->B:LX/BTG;

    iget-object v1, p0, LX/BSr;->j:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786077
    iget-object v4, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    move-object v3, v4

    .line 1786078
    iget-object v4, p0, LX/BSr;->A:Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;

    .line 1786079
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "video_editing_audio_interactions"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "video_editing_module"

    .line 1786080
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1786081
    move-object v5, v5

    .line 1786082
    sget-object v6, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->AUDIO_BUTTON_CLICKS:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    .line 1786083
    iget v7, v4, Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;->a:I

    move v7, v7

    .line 1786084
    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1786085
    iget-object v6, v0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1786086
    iget-object v0, p0, LX/BSr;->s:LX/BTi;

    if-eqz v0, :cond_1

    .line 1786087
    iget-object v0, p0, LX/BSr;->C:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1786088
    iget-object v0, p0, LX/BSr;->s:LX/BTi;

    .line 1786089
    iget-object v1, v0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1786090
    :cond_1
    return-void

    .line 1786091
    :cond_2
    iget-object v1, v0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1786092
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "video_editing_frame_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1786093
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1786094
    sget-object v4, LX/BTi;->a:Ljava/lang/String;

    const-string v5, "Couldn\'t delete video frame file"

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1786095
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static q(LX/BSr;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 7

    .prologue
    .line 1786053
    iget-object v0, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1786054
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1786055
    invoke-static {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v6

    .line 1786056
    iget-object v0, p0, LX/BSr;->o:LX/BT9;

    .line 1786057
    iget-boolean v1, v0, LX/BT9;->V:Z

    move v0, v1

    .line 1786058
    if-eqz v0, :cond_0

    .line 1786059
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->newBuilder()LX/5jP;

    move-result-object v0

    iget-object v1, p0, LX/BSr;->o:LX/BT9;

    invoke-virtual {v1}, LX/BT9;->m()I

    move-result v1

    iget-object v2, p0, LX/BSr;->o:LX/BT9;

    invoke-virtual {v2}, LX/BT9;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/5jP;->a(II)LX/5jP;

    move-result-object v0

    invoke-virtual {v0}, LX/5jP;->a()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setVideoTrimParams(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    .line 1786060
    :cond_0
    iget-object v0, p0, LX/BSr;->q:LX/BSj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BSr;->q:LX/BSj;

    .line 1786061
    iget-boolean v1, v0, LX/BSj;->e:Z

    move v0, v1

    .line 1786062
    if-eqz v0, :cond_1

    .line 1786063
    iget-object v0, p0, LX/BSr;->q:LX/BSj;

    .line 1786064
    iget-object v1, v0, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCropRect()Landroid/graphics/RectF;

    move-result-object v1

    move-object v0, v1

    .line 1786065
    invoke-static {v0}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setCropRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    .line 1786066
    :cond_1
    iget-object v0, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->c:Z

    if-eqz v0, :cond_2

    .line 1786067
    iget-object v0, p0, LX/BSr;->l:LX/BTh;

    iget-object v1, p0, LX/BSr;->j:Landroid/net/Uri;

    iget-object v2, p0, LX/BSr;->o:LX/BT9;

    invoke-virtual {v2}, LX/BT9;->m()I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1786068
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v4, v5

    .line 1786069
    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v4

    iget-object v5, p0, LX/BSr;->y:Landroid/content/Context;

    invoke-virtual/range {v0 .. v5}, LX/BTh;->a(Landroid/net/Uri;JILandroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 1786070
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v6, v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    .line 1786071
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    return-object v0

    .line 1786072
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1786047
    iget-object v0, p0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    const/4 v1, 0x1

    .line 1786048
    iput-boolean v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->c:Z

    .line 1786049
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e()V

    .line 1786050
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(I)V

    .line 1786051
    invoke-static {p0, p1}, LX/BSr;->c(LX/BSr;I)V

    .line 1786052
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1786045
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e()V

    .line 1786046
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1786044
    iget-object v0, p0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->f()Z

    move-result v0

    return v0
.end method
