.class public final LX/CRz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1894058
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1894059
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1894060
    :goto_0
    return v1

    .line 1894061
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_3

    .line 1894062
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1894063
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1894064
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1894065
    const-string v11, "latitude"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1894066
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1894067
    :cond_1
    const-string v11, "longitude"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1894068
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v8

    move v6, v7

    goto :goto_1

    .line 1894069
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1894070
    :cond_3
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1894071
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1894072
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1894073
    :cond_4
    if-eqz v6, :cond_5

    move-object v0, p1

    move v1, v7

    move-wide v2, v8

    .line 1894074
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1894075
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move-wide v8, v4

    move-wide v2, v4

    goto :goto_1
.end method
