.class public final LX/Bgm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V
    .locals 0

    .prologue
    .line 1808335
    iput-object p1, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1808319
    if-eqz p2, :cond_0

    .line 1808320
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808321
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->e(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808322
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1808323
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    iget-object v1, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1808324
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    .line 1808325
    invoke-static {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d$redex0(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808326
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 1808327
    :goto_0
    return-void

    .line 1808328
    :cond_0
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808329
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->e(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808330
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1808331
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 1808332
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    .line 1808333
    invoke-static {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d$redex0(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808334
    iget-object v0, p0, LX/Bgm;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method
