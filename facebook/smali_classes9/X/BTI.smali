.class public final enum LX/BTI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTI;

.field public static final enum LEFT:LX/BTI;

.field public static final enum RIGHT:LX/BTI;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1787075
    new-instance v0, LX/BTI;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/BTI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTI;->LEFT:LX/BTI;

    .line 1787076
    new-instance v0, LX/BTI;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LX/BTI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTI;->RIGHT:LX/BTI;

    .line 1787077
    const/4 v0, 0x2

    new-array v0, v0, [LX/BTI;

    sget-object v1, LX/BTI;->LEFT:LX/BTI;

    aput-object v1, v0, v2

    sget-object v1, LX/BTI;->RIGHT:LX/BTI;

    aput-object v1, v0, v3

    sput-object v0, LX/BTI;->$VALUES:[LX/BTI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1787078
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTI;
    .locals 1

    .prologue
    .line 1787079
    const-class v0, LX/BTI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTI;

    return-object v0
.end method

.method public static values()[LX/BTI;
    .locals 1

    .prologue
    .line 1787080
    sget-object v0, LX/BTI;->$VALUES:[LX/BTI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTI;

    return-object v0
.end method
