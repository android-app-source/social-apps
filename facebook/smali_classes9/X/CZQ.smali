.class public final LX/CZQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1915397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1915398
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1915399
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1915400
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1915401
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1915402
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915403
    :goto_1
    move v1, v2

    .line 1915404
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1915405
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1915406
    :cond_1
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1915407
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v4

    move v4, v3

    .line 1915408
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 1915409
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1915410
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915411
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 1915412
    const-string v9, "entity_with_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1915413
    const/4 v8, 0x0

    .line 1915414
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_e

    .line 1915415
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915416
    :goto_3
    move v7, v8

    .line 1915417
    goto :goto_2

    .line 1915418
    :cond_3
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1915419
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_2

    .line 1915420
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1915421
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1915422
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1915423
    if-eqz v4, :cond_6

    .line 1915424
    invoke-virtual {p1, v3, v6, v2}, LX/186;->a(III)V

    .line 1915425
    :cond_6
    if-eqz v1, :cond_7

    .line 1915426
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 1915427
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2

    .line 1915428
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915429
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_d

    .line 1915430
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1915431
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915432
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_a

    if-eqz v10, :cond_a

    .line 1915433
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_b

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1915434
    :cond_b
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_4

    .line 1915435
    :cond_c
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1915436
    const/4 v10, 0x0

    .line 1915437
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v11, :cond_12

    .line 1915438
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915439
    :goto_5
    move v7, v10

    .line 1915440
    goto :goto_4

    .line 1915441
    :cond_d
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1915442
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1915443
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1915444
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_e
    move v7, v8

    move v9, v8

    goto :goto_4

    .line 1915445
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915446
    :cond_10
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_11

    .line 1915447
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1915448
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915449
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_10

    if-eqz v11, :cond_10

    .line 1915450
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 1915451
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 1915452
    :cond_11
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1915453
    invoke-virtual {p1, v10, v7}, LX/186;->b(II)V

    .line 1915454
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_5

    :cond_12
    move v7, v10

    goto :goto_6
.end method
