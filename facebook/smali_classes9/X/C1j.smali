.class public final LX/C1j;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C1k;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/C1k;


# direct methods
.method public constructor <init>(LX/C1k;)V
    .locals 1

    .prologue
    .line 1843132
    iput-object p1, p0, LX/C1j;->d:LX/C1k;

    .line 1843133
    move-object v0, p1

    .line 1843134
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1843135
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1843136
    const-string v0, "ScheduledLiveAttachmentTickerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1843137
    if-ne p0, p1, :cond_1

    .line 1843138
    :cond_0
    :goto_0
    return v0

    .line 1843139
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1843140
    goto :goto_0

    .line 1843141
    :cond_3
    check-cast p1, LX/C1j;

    .line 1843142
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1843143
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1843144
    if-eq v2, v3, :cond_0

    .line 1843145
    iget-object v2, p0, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iget-object v3, p1, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1843146
    goto :goto_0

    .line 1843147
    :cond_5
    iget-object v2, p1, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-nez v2, :cond_4

    .line 1843148
    :cond_6
    iget-object v2, p0, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1843149
    goto :goto_0

    .line 1843150
    :cond_8
    iget-object v2, p1, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1843151
    :cond_9
    iget-object v2, p0, LX/C1j;->c:LX/1Pq;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/C1j;->c:LX/1Pq;

    iget-object v3, p1, LX/C1j;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1843152
    goto :goto_0

    .line 1843153
    :cond_a
    iget-object v2, p1, LX/C1j;->c:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
