.class public LX/Bas;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799972
    iput-object p1, p0, LX/Bas;->a:Landroid/content/res/Resources;

    .line 1799973
    iput-object p2, p0, LX/Bas;->b:LX/0Or;

    .line 1799974
    return-void
.end method

.method private static a(ZZZ)I
    .locals 1

    .prologue
    .line 1799975
    if-eqz p1, :cond_1

    if-eqz p0, :cond_1

    .line 1799976
    if-eqz p2, :cond_0

    .line 1799977
    const/4 v0, 0x2

    .line 1799978
    :goto_0
    return v0

    .line 1799979
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1799980
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/common/callercontext/CallerContext;LX/1cC;LX/1bf;LX/1bf;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)LX/1aZ;
    .locals 2
    .param p3    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1799981
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    .line 1799982
    :cond_0
    iget-object v0, p0, LX/Bas;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p5}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p3}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1799983
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Bas;
    .locals 3

    .prologue
    .line 1799984
    new-instance v1, LX/Bas;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const/16 v2, 0x509

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Bas;-><init>(Landroid/content/res/Resources;LX/0Or;)V

    .line 1799985
    return-object v1
.end method


# virtual methods
.method public final a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V
    .locals 7
    .param p1    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799986
    invoke-static {p3, p5, p6}, LX/Bas;->a(ZZZ)I

    move-result v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEditAffordance(I)V

    .line 1799987
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move-object v1, p0

    move-object v2, p7

    move-object/from16 v3, p9

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p10

    .line 1799988
    invoke-direct/range {v1 .. v6}, LX/Bas;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1cC;LX/1bf;LX/1bf;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)LX/1aZ;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1799989
    :cond_1
    if-nez p4, :cond_2

    if-eqz p5, :cond_3

    :cond_2
    const/4 v2, 0x1

    .line 1799990
    :goto_0
    if-nez p8, :cond_4

    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEnabled(Z)V

    .line 1799991
    if-eqz v2, :cond_5

    :goto_2
    move-object/from16 v0, p10

    invoke-virtual {v0, p8}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799992
    if-eqz p3, :cond_6

    iget-object v1, p0, LX/Bas;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f6a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1799993
    :goto_3
    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1799994
    return-void

    .line 1799995
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    move v1, v2

    .line 1799996
    goto :goto_1

    .line 1799997
    :cond_5
    const/4 p8, 0x0

    goto :goto_2

    .line 1799998
    :cond_6
    iget-object v1, p0, LX/Bas;->a:Landroid/content/res/Resources;

    const v2, 0x7f080f63

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
