.class public final LX/CLv;
.super LX/CLu;
.source ""


# static fields
.field public static final o:Landroid/util/SparseIntArray;


# instance fields
.field public final l:Lcom/facebook/drawee/view/DraweeView;

.field private final m:Lcom/facebook/common/callercontext/CallerContext;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const v5, 0x1f623

    const v4, 0x1f614

    const v3, 0x1f601

    .line 1879933
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 1879934
    sput-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f61d

    const v2, 0x1f445

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879935
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f60c

    const v2, 0x1f606

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879936
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f629

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879937
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f62b

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879938
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f624

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879939
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f635

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879940
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f63a

    const v2, 0x1f603

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879941
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f638

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879942
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f63b

    const v2, 0x1f60d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879943
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f63d

    const v2, 0x1f618

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879944
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f63c

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879945
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f640

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879946
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f63f

    const v2, 0x1f622

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879947
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f639

    const v2, 0x1f602

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879948
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const/16 v1, 0x270b

    const v2, 0x1f64b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879949
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f64d

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879950
    sget-object v0, LX/CLv;->o:Landroid/util/SparseIntArray;

    const v1, 0x1f3bc

    const v2, 0x1f3b6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1879951
    return-void
.end method

.method public constructor <init>(Lcom/facebook/drawee/view/DraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/view/DraweeView;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1879952
    invoke-direct {p0, p1}, LX/CLu;-><init>(Landroid/view/View;)V

    .line 1879953
    iput-object p1, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    .line 1879954
    iput-object p2, p0, LX/CLv;->m:Lcom/facebook/common/callercontext/CallerContext;

    .line 1879955
    iput-object p3, p0, LX/CLv;->n:LX/0Or;

    .line 1879956
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 5

    .prologue
    .line 1879957
    iget-object v0, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1879958
    iget-object v1, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1879959
    iget v3, p1, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    move v3, v3

    .line 1879960
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1879961
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1879962
    move-object v0, v2

    .line 1879963
    sget-object v2, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1879964
    iget-object v1, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    iget-object v0, p0, LX/CLv;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, LX/CLv;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1879965
    iget-object v2, p0, LX/CLv;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1879966
    iget-object v3, p0, LX/CLu;->m:Lcom/facebook/ui/emoji/model/Emoji;

    move-object v3, v3

    .line 1879967
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 p1, 0x64

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1879968
    const-string p1, "http://www.%s/images/emoji/unicode/"

    invoke-static {v2, p1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879969
    const-string v2, "emoji_"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879970
    iget v2, v3, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v2, v2

    .line 1879971
    sget-object p1, LX/CLv;->o:Landroid/util/SparseIntArray;

    invoke-virtual {p1, v2, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    .line 1879972
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    sget-object p1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879973
    iget v2, v3, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v2, v2

    .line 1879974
    if-eqz v2, :cond_0

    .line 1879975
    const/16 v2, 0x5f

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1879976
    iget v2, v3, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v2, v2

    .line 1879977
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879978
    :cond_0
    const-string v2, "_64.png"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879979
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v2, v2

    .line 1879980
    invoke-virtual {v0, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1879981
    return-void
.end method
