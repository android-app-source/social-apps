.class public final LX/Agy;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "[",
        "LX/3K3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

.field private b:Landroid/content/res/Resources;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;)V
    .locals 0

    .prologue
    .line 1701880
    iput-object p1, p0, LX/Agy;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;B)V
    .locals 0

    .prologue
    .line 1701881
    invoke-direct {p0, p1}, LX/Agy;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;)V

    return-void
.end method

.method private a([LX/3K3;)V
    .locals 3

    .prologue
    .line 1701882
    iget-object v0, p0, LX/Agy;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    iget v1, p0, LX/Agy;->c:I

    const/4 v2, 0x0

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .line 1701883
    iget-object v0, p0, LX/Agy;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    iget v1, p0, LX/Agy;->c:I

    const/4 v2, 0x1

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .line 1701884
    iget-object v0, p0, LX/Agy;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget v1, p0, LX/Agy;->c:I

    invoke-static {v0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->setKFImage(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;I)V

    .line 1701885
    return-void
.end method

.method private varargs a([Ljava/lang/Integer;)[LX/3K3;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1701886
    aget-object v0, p1, v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Agy;->c:I

    .line 1701887
    const/4 v0, 0x2

    new-array v1, v0, [LX/3K3;

    .line 1701888
    :try_start_0
    iget-object v0, p0, LX/Agy;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->b:[Ljava/lang/String;

    iget v3, p0, LX/Agy;->c:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 1701889
    iget-object v0, p0, LX/Agy;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    sget-object v3, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->c:[Ljava/lang/String;

    iget v4, p0, LX/Agy;->c:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1701890
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v2}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;

    move-result-object v4

    aput-object v4, v1, v0

    .line 1701891
    const/4 v0, 0x1

    invoke-static {v3}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;

    move-result-object v4

    aput-object v4, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1701892
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 1701893
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 1701894
    :goto_0
    return-object v1

    .line 1701895
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 1701896
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1701897
    :catch_0
    move-exception v0

    .line 1701898
    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->e:Ljava/lang/String;

    const-string v3, "Failed to load the tip jar animation from json file"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1701899
    check-cast p1, [Ljava/lang/Integer;

    invoke-direct {p0, p1}, LX/Agy;->a([Ljava/lang/Integer;)[LX/3K3;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1701900
    check-cast p1, [LX/3K3;

    invoke-direct {p0, p1}, LX/Agy;->a([LX/3K3;)V

    return-void
.end method

.method public final onPreExecute()V
    .locals 1

    .prologue
    .line 1701901
    iget-object v0, p0, LX/Agy;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->k:Landroid/content/res/Resources;

    iput-object v0, p0, LX/Agy;->b:Landroid/content/res/Resources;

    .line 1701902
    return-void
.end method
