.class public final LX/Afs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Afu;


# direct methods
.method public constructor <init>(LX/Afu;)V
    .locals 0

    .prologue
    .line 1699800
    iput-object p1, p0, LX/Afs;->a:LX/Afu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699801
    iget-object v0, p0, LX/Afs;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Afu;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get tip jar subscription."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699802
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699803
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;

    .line 1699804
    iget-object v0, p0, LX/Afs;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->g:LX/AeV;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1699805
    :cond_0
    :goto_0
    return-void

    .line 1699806
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;

    move-result-object v0

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1699807
    if-nez v0, :cond_3

    .line 1699808
    :cond_2
    :goto_1
    move-object v0, v4

    .line 1699809
    if-eqz v0, :cond_0

    .line 1699810
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1699811
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1699812
    iget-object v0, p0, LX/Afs;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->g:LX/AeV;

    sget-object v2, LX/AeN;->LIVE_TIP_JAR_EVENT:LX/AeN;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    goto :goto_0

    .line 1699813
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->c()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;

    move-result-object v6

    .line 1699814
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v6}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->c()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1699815
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel;->b()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;

    move-result-object v8

    .line 1699816
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1699817
    new-instance v4, LX/Afr;

    invoke-virtual {v6}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$TipGiverModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveTipEventSubscriptionModel$TipJarTransactionModel$AmountReceivedModel;->b()Ljava/lang/String;

    move-result-object v8

    move v10, v9

    invoke-direct/range {v4 .. v10}, LX/Afr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_1
.end method
