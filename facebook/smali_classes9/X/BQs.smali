.class public final LX/BQs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field public final synthetic b:LX/BQx;


# direct methods
.method public constructor <init>(LX/BQx;Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 1782849
    iput-object p1, p0, LX/BQs;->b:LX/BQx;

    iput-object p2, p0, LX/BQs;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1782848
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1782847
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1782845
    iget-object v0, p0, LX/BQs;->b:LX/BQx;

    iget-object v0, v0, LX/BQx;->s:LX/BR1;

    iget-object v1, p0, LX/BQs;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/BR1;->h:Ljava/lang/String;

    .line 1782846
    return-void
.end method
