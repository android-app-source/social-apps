.class public final LX/As9;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/media/util/model/MediaModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AsB;


# direct methods
.method public constructor <init>(LX/AsB;)V
    .locals 0

    .prologue
    .line 1720107
    iput-object p1, p0, LX/As9;->a:LX/AsB;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1720108
    return-void
.end method

.method private b()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1720109
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1720110
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1720111
    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "fb"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v0

    .line 1720112
    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    const-string v3, "Facebook_edited"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v1

    .line 1720113
    new-instance v2, Ljava/io/File;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    const-string v6, "fb"

    invoke-direct {v2, v3, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v2, v2

    .line 1720114
    invoke-static {}, LX/As0;->b()Ljava/io/File;

    move-result-object v3

    .line 1720115
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1720116
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1720117
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1720118
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1720119
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1720120
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1720121
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1720122
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1720123
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_8

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1720124
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1720125
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".jpeg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1720126
    :cond_4
    new-instance v1, Lcom/facebook/media/util/model/MediaModel;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/1po;->PHOTO:LX/1po;

    invoke-direct {v1, v0, v2}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;)V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1720127
    :cond_5
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1720128
    :cond_6
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mp4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1720129
    const/16 v2, 0x7d0

    .line 1720130
    :try_start_0
    iget-object v1, p0, LX/As9;->a:LX/AsB;

    iget-object v1, v1, LX/AsB;->r:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1720131
    if-nez v1, :cond_7

    .line 1720132
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1kH;->a(Landroid/net/Uri;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1720133
    :try_start_1
    iget-object v2, p0, LX/As9;->a:LX/AsB;

    iget-object v2, v2, LX/AsB;->r:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1720134
    :goto_2
    int-to-long v8, v1

    const-wide/16 v10, 0x3e8

    cmp-long v2, v8, v10

    if-ltz v2, :cond_5

    .line 1720135
    new-instance v2, Lcom/facebook/media/util/model/MediaModel;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v7, LX/1po;->VIDEO:LX/1po;

    invoke-direct {v2, v0, v7, v1}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;I)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1720136
    :cond_7
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    goto :goto_2

    .line 1720137
    :cond_8
    return-object v4

    :catch_0
    move v1, v2

    goto :goto_2

    :catch_1
    goto :goto_2
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1720138
    iget-object v0, p0, LX/As9;->a:LX/AsB;

    iget-object v0, v0, LX/AsB;->f:LX/1kH;

    const/4 v1, 0x1

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, LX/1kH;->a(ZI)Ljava/util/List;

    move-result-object v0

    .line 1720139
    if-nez v0, :cond_0

    .line 1720140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1720141
    :cond_0
    invoke-direct {p0}, LX/As9;->b()Ljava/util/List;

    move-result-object v1

    .line 1720142
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1720143
    new-instance v1, LX/6bQ;

    sget-object v2, LX/6bP;->NEWEST_FIRST:LX/6bP;

    invoke-direct {v1, v2}, LX/6bQ;-><init>(LX/6bP;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1720144
    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1720145
    check-cast p1, Ljava/util/List;

    .line 1720146
    iget-object v0, p0, LX/As9;->a:LX/AsB;

    iget-object v0, v0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a(Ljava/util/List;)V

    .line 1720147
    iget-object v0, p0, LX/As9;->a:LX/AsB;

    iget-object v0, v0, LX/AsB;->q:LX/As8;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1720148
    iget-object v0, p0, LX/As9;->a:LX/AsB;

    iget-object v1, v0, LX/AsB;->q:LX/As8;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    invoke-interface {v1, v0}, LX/As8;->a(Lcom/facebook/media/util/model/MediaModel;)V

    .line 1720149
    :cond_0
    return-void
.end method

.method public final onPreExecute()V
    .locals 0

    .prologue
    .line 1720150
    return-void
.end method
