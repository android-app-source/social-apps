.class public LX/BKm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public final c:Lcom/facebook/platform/composer/model/PlatformComposition;

.field public final d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final f:Lcom/facebook/ipc/composer/model/ComposerLocation;

.field public final g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

.field public final i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

.field public final j:I

.field public final k:Z

.field public final l:I

.field public final m:Z

.field public final n:Z

.field public final o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;


# direct methods
.method public constructor <init>(LX/BKl;)V
    .locals 1

    .prologue
    .line 1774204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774205
    iget-object v0, p1, LX/BKl;->a:Ljava/lang/String;

    iput-object v0, p0, LX/BKm;->a:Ljava/lang/String;

    .line 1774206
    iget-object v0, p1, LX/BKl;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1774207
    iget-object v0, p1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    iput-object v0, p0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1774208
    iget-object v0, p1, LX/BKl;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1774209
    iget-object v0, p1, LX/BKl;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1774210
    iget-object v0, p1, LX/BKl;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, LX/BKm;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1774211
    iget-object v0, p1, LX/BKl;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, LX/BKm;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1774212
    iget-object v0, p1, LX/BKl;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, LX/BKm;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 1774213
    iget-object v0, p1, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1774214
    iget v0, p1, LX/BKl;->j:I

    iput v0, p0, LX/BKm;->j:I

    .line 1774215
    iget-boolean v0, p1, LX/BKl;->l:Z

    iput-boolean v0, p0, LX/BKm;->k:Z

    .line 1774216
    iget v0, p1, LX/BKl;->k:I

    iput v0, p0, LX/BKm;->l:I

    .line 1774217
    iget-boolean v0, p1, LX/BKl;->m:Z

    iput-boolean v0, p0, LX/BKm;->m:Z

    .line 1774218
    iget-boolean v0, p1, LX/BKl;->n:Z

    iput-boolean v0, p0, LX/BKm;->n:Z

    .line 1774219
    iget-object v0, p1, LX/BKl;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, LX/BKm;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1774220
    return-void
.end method


# virtual methods
.method public final a()LX/BKl;
    .locals 1

    .prologue
    .line 1774221
    new-instance v0, LX/BKl;

    invoke-direct {v0, p0}, LX/BKl;-><init>(LX/BKm;)V

    return-object v0
.end method
