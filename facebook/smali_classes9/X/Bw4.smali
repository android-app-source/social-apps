.class public final LX/Bw4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Bur;


# direct methods
.method public constructor <init>(LX/Bur;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1833244
    iput-object p1, p0, LX/Bw4;->b:LX/Bur;

    iput-object p2, p0, LX/Bw4;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1833245
    iget-object v0, p0, LX/Bw4;->b:LX/Bur;

    iget-object v1, p0, LX/Bw4;->a:Landroid/content/Context;

    .line 1833246
    iget-object v2, v0, LX/Bur;->s:LX/AaZ;

    invoke-virtual {v2}, LX/AaZ;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 1833247
    :goto_0
    iget-object v2, v0, LX/Bur;->q:LX/AVU;

    const-string p0, "video_menu_item"

    invoke-virtual {v2, v3, p0}, LX/AVU;->a(ZLjava/lang/String;)V

    .line 1833248
    iget-object v2, v0, LX/Bur;->s:LX/AaZ;

    invoke-virtual {v2, v3}, LX/AaZ;->a(Z)V

    .line 1833249
    invoke-static {v1, v3}, LX/Bur;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1833250
    invoke-static {p1, v1, v3}, LX/Bur;->a(Landroid/view/MenuItem;Landroid/content/Context;Z)V

    .line 1833251
    iget-object v2, v0, LX/Bur;->t:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kL;

    new-instance p0, LX/27k;

    if-eqz v3, :cond_1

    const v3, 0x7f081143

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {p0, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, p0}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1833252
    iget-object v2, v0, LX/Bur;->p:LX/3HT;

    new-instance v3, LX/Ada;

    invoke-direct {v3}, LX/Ada;-><init>()V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1833253
    const/4 v0, 0x1

    return v0

    .line 1833254
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    .line 1833255
    :cond_1
    const v3, 0x7f081144

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
