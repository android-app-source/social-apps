.class public final LX/Bf6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$SuggestEditsSections$;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BgK;

.field public final synthetic b:LX/Bf7;


# direct methods
.method public constructor <init>(LX/Bf7;LX/BgK;)V
    .locals 0

    .prologue
    .line 1805727
    iput-object p1, p0, LX/Bf6;->b:LX/Bf7;

    iput-object p2, p0, LX/Bf6;->a:LX/BgK;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1805725
    iget-object v0, p0, LX/Bf6;->a:LX/BgK;

    invoke-virtual {v0, p1}, LX/BgK;->b(Ljava/lang/Throwable;)V

    .line 1805726
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1805713
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    .line 1805714
    if-nez p1, :cond_0

    .line 1805715
    iget-object v0, p0, LX/Bf6;->a:LX/BgK;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty result returned for Suggest Edits section GraphQL query"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/BgK;->b(Ljava/lang/Throwable;)V

    .line 1805716
    :goto_0
    return-void

    .line 1805717
    :cond_0
    iget-object v0, p0, LX/Bf6;->a:LX/BgK;

    .line 1805718
    iput-object p1, v0, LX/BgK;->w:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    .line 1805719
    iget-object v3, v0, LX/BgK;->v:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1805720
    iget-object v3, v0, LX/BgK;->l:LX/BhM;

    iget-object v4, v0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    iget-object v6, v0, LX/BgK;->u:Landroid/widget/LinearLayout;

    iget-object v8, v0, LX/BgK;->r:Ljava/lang/String;

    move-object v5, v0

    move-object v7, p1

    invoke-virtual/range {v3 .. v8}, LX/BhM;->a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    iput-object v3, v0, LX/BgK;->y:LX/0Px;

    .line 1805721
    iget-object v3, v0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1805722
    iget-object v3, v0, LX/BgK;->z:LX/Bg9;

    sget-object v4, LX/BgJ;->SECTIONS:LX/BgJ;

    invoke-virtual {v3, v4}, LX/Bg9;->a(LX/BgJ;)V

    .line 1805723
    :cond_1
    iget-object v3, v0, LX/BgK;->A:LX/Bg9;

    sget-object v4, LX/BgJ;->SECTIONS:LX/BgJ;

    invoke-virtual {v3, v4}, LX/Bg9;->a(LX/BgJ;)V

    .line 1805724
    goto :goto_0
.end method
