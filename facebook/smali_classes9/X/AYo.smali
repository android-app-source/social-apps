.class public LX/AYo;
.super LX/AWT;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public a:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AVc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/resources/ui/FbFrameLayout;

.field public final f:Landroid/view/ViewGroup;

.field public final g:Lcom/facebook/fbui/glyph/GlyphView;

.field public final h:Lcom/facebook/fbui/glyph/GlyphView;

.field public final i:Landroid/view/View;

.field public final j:I

.field public k:LX/AYM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Landroid/animation/Animator$AnimatorListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Landroid/animation/Animator$AnimatorListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686092
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686093
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686094
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686095
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686096
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686097
    const-class v0, LX/AYo;

    invoke-static {v0, p0}, LX/AYo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686098
    const v0, 0x7f0305a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686099
    const v0, 0x7f0d0f7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, LX/AYo;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 1686100
    const v0, 0x7f0d0f7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/AYo;->f:Landroid/view/ViewGroup;

    .line 1686101
    const v0, 0x7f0d0f80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYo;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686102
    const v0, 0x7f0d0f81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686103
    const v0, 0x7f0d0f7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AYo;->i:Landroid/view/View;

    .line 1686104
    invoke-virtual {p0}, LX/AYo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0620

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AYo;->j:I

    .line 1686105
    const p1, 0x3f666666    # 0.9f

    .line 1686106
    iget-object v0, p0, LX/AYo;->f:Landroid/view/ViewGroup;

    iget v1, p0, LX/AYo;->j:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 1686107
    iget-object v0, p0, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(F)V

    .line 1686108
    iget-object v0, p0, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleX(F)V

    .line 1686109
    iget-object v0, p0, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleY(F)V

    .line 1686110
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AYo;->n:Z

    .line 1686111
    iget-object v0, p0, LX/AYo;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    new-instance v1, LX/AYl;

    invoke-direct {v1, p0}, LX/AYl;-><init>(LX/AYo;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686112
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AYo;

    invoke-static {p0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v1

    check-cast v1, LX/AVT;

    invoke-static {p0}, LX/AVc;->b(LX/0QB;)LX/AVc;

    move-result-object p0

    check-cast p0, LX/AVc;

    iput-object v1, p1, LX/AYo;->a:LX/AVT;

    iput-object p0, p1, LX/AYo;->b:LX/AVc;

    return-void
.end method


# virtual methods
.method public final iF_()V
    .locals 4

    .prologue
    .line 1686113
    iget-object v0, p0, LX/AYo;->b:LX/AVc;

    iget-object v1, p0, LX/AYo;->i:Landroid/view/View;

    .line 1686114
    iget-object v2, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, v0, LX/AVc;->c:LX/0Tn;

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1686115
    :goto_0
    return-void

    .line 1686116
    :cond_0
    iget-object v2, v0, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, v0, LX/AVc;->c:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1686117
    new-instance v3, LX/0hs;

    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/4 p0, 0x2

    invoke-direct {v3, v2, p0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v3, v0, LX/AVc;->g:LX/0hs;

    .line 1686118
    iget-object v3, v0, LX/AVc;->g:LX/0hs;

    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f080d01

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1686119
    iget-object v3, v0, LX/AVc;->g:LX/0hs;

    iget-object v2, v0, LX/AVc;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f080d02

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1686120
    iget-object v2, v0, LX/AVc;->g:LX/0hs;

    invoke-virtual {v2, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1686121
    iget-object v2, v0, LX/AVc;->g:LX/0hs;

    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 1686122
    iget-object v2, v0, LX/AVc;->g:LX/0hs;

    const/4 v3, -0x1

    .line 1686123
    iput v3, v2, LX/0hs;->t:I

    .line 1686124
    iget-object v2, v0, LX/AVc;->g:LX/0hs;

    invoke-virtual {v2}, LX/0ht;->d()V

    goto :goto_0
.end method
