.class public final LX/Bd0;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Bd3;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/Bd1;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1803142
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pageSize"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "renderEventHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "connectionController"

    aput-object v2, v0, v1

    sput-object v0, LX/Bd0;->b:[Ljava/lang/String;

    .line 1803143
    sput v3, LX/Bd0;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1803140
    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1803141
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/Bd0;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bd0;->d:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a(LX/2kW;)LX/Bd0;
    .locals 2

    .prologue
    .line 1803137
    iget-object v0, p0, LX/Bd0;->a:LX/Bd1;

    iput-object p1, v0, LX/Bd1;->g:LX/2kW;

    .line 1803138
    iget-object v0, p0, LX/Bd0;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1803139
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1803133
    invoke-super {p0}, LX/BcN;->a()V

    .line 1803134
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bd0;->a:LX/Bd1;

    .line 1803135
    sget-object v0, LX/Bd3;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1803136
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Bd3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1803123
    iget-object v1, p0, LX/Bd0;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bd0;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/Bd0;->c:I

    if-ge v1, v2, :cond_2

    .line 1803124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1803125
    :goto_0
    sget v2, LX/Bd0;->c:I

    if-ge v0, v2, :cond_1

    .line 1803126
    iget-object v2, p0, LX/Bd0;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1803127
    sget-object v2, LX/Bd0;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803128
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1803129
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1803130
    :cond_2
    iget-object v0, p0, LX/Bd0;->a:LX/Bd1;

    .line 1803131
    invoke-virtual {p0}, LX/Bd0;->a()V

    .line 1803132
    return-object v0
.end method

.method public final b(I)LX/Bd0;
    .locals 2

    .prologue
    .line 1803115
    iget-object v0, p0, LX/Bd0;->a:LX/Bd1;

    iput p1, v0, LX/Bd1;->d:I

    .line 1803116
    iget-object v0, p0, LX/Bd0;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1803117
    return-object p0
.end method

.method public final b(LX/BcQ;)LX/Bd0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BdG;",
            ">;)",
            "LX/Bd0;"
        }
    .end annotation

    .prologue
    .line 1803120
    iget-object v0, p0, LX/Bd0;->a:LX/Bd1;

    iput-object p1, v0, LX/Bd1;->f:LX/BcQ;

    .line 1803121
    iget-object v0, p0, LX/Bd0;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1803122
    return-object p0
.end method

.method public final c(LX/BcQ;)LX/Bd0;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Bd0;"
        }
    .end annotation

    .prologue
    .line 1803118
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 1803119
    return-object p0
.end method
