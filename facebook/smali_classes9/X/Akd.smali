.class public final LX/Akd;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

.field public final synthetic e:LX/1SX;


# direct methods
.method public constructor <init>(LX/1SX;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V
    .locals 0

    .prologue
    .line 1709235
    iput-object p1, p0, LX/Akd;->e:LX/1SX;

    iput-object p2, p0, LX/Akd;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    iput-object p3, p0, LX/Akd;->b:Landroid/view/View;

    iput-object p4, p0, LX/Akd;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object p5, p0, LX/Akd;->d:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 10

    .prologue
    .line 1709236
    iget-object v0, p0, LX/Akd;->e:LX/1SX;

    iget-object v0, v0, LX/1SX;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081046

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1709237
    iget-object v0, p0, LX/Akd;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 1709238
    iget-object v1, p0, LX/Akd;->e:LX/1SX;

    iget-object v2, p0, LX/Akd;->b:Landroid/view/View;

    const/4 v5, 0x0

    .line 1709239
    iget-object v9, v1, LX/1SX;->l:LX/0bH;

    new-instance v3, LX/1Nd;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move-object v6, v5

    invoke-direct/range {v3 .. v8}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v9, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1709240
    iget-object v3, v1, LX/1SX;->l:LX/0bH;

    new-instance v4, LX/1YM;

    invoke-direct {v4}, LX/1YM;-><init>()V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 1709241
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1709242
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1709243
    iget-object v0, p0, LX/Akd;->e:LX/1SX;

    iget-object v0, v0, LX/1SX;->t:LX/0Zb;

    iget-object v1, p0, LX/Akd;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1709244
    iget-object v0, p0, LX/Akd;->e:LX/1SX;

    iget-object v1, p0, LX/Akd;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    iget-object v2, p0, LX/Akd;->d:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-virtual {v0, v1, v2, p1}, LX/1SX;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1709245
    return-void
.end method
