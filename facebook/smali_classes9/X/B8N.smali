.class public final LX/B8N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:LX/B8R;


# direct methods
.method public constructor <init>(LX/B8R;)V
    .locals 0

    .prologue
    .line 1748754
    iput-object p1, p0, LX/B8N;->a:LX/B8R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1748755
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 1748756
    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1748757
    iget-object v0, p0, LX/B8N;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/B8N;->a:LX/B8R;

    invoke-virtual {v1}, LX/B8R;->getCountry()Lcom/facebook/common/locale/Country;

    move-result-object v1

    const/4 p1, 0x0

    .line 1748758
    if-eqz v1, :cond_0

    .line 1748759
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 1748760
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 1748761
    :cond_0
    :goto_0
    move v0, p1

    .line 1748762
    if-nez v0, :cond_1

    .line 1748763
    iget-object v0, p0, LX/B8N;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->k:LX/B7F;

    iget-object v1, p0, LX/B8N;->a:LX/B8R;

    iget-object v1, v1, LX/B8R;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-static {v1}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v0

    .line 1748764
    iget-object v1, p0, LX/B8N;->a:LX/B8R;

    invoke-virtual {v1, v0}, LX/B8R;->a(Ljava/lang/String;)V

    .line 1748765
    iget-object v0, p0, LX/B8N;->a:LX/B8R;

    invoke-virtual {v0}, LX/B8R;->b()V

    .line 1748766
    const/4 v0, 0x1

    .line 1748767
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1748768
    :cond_2
    invoke-static {p2, v1}, LX/46H;->a(Ljava/lang/CharSequence;Lcom/facebook/common/locale/Country;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1748769
    const/4 p1, 0x1

    goto :goto_0
.end method
