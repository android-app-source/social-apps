.class public final LX/C8P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V
    .locals 0

    .prologue
    .line 1852731
    iput-object p1, p0, LX/C8P;->c:Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8P;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C8P;->b:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x3f90a851

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1852724
    iget-object v1, p0, LX/C8P;->c:Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/C8P;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/C8P;->c:Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;

    iget-object v5, p0, LX/C8P;->b:LX/1Po;

    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;LX/1PT;)LX/0wD;

    move-result-object v4

    .line 1852725
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1852726
    check-cast v5, Lcom/facebook/graphql/model/HideableUnit;

    .line 1852727
    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a(Lcom/facebook/graphql/model/HideableUnit;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-interface {v5}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    instance-of p0, v5, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz p0, :cond_0

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    sget-object v5, LX/0wD;->TIMELINE_SELF:LX/0wD;

    if-ne v4, v5, :cond_2

    .line 1852728
    :cond_1
    :goto_0
    const v1, -0x408e3736

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1852729
    :cond_2
    sget-object v5, LX/0wD;->NEWSFEED:LX/0wD;

    if-eq v4, v5, :cond_3

    sget-object v5, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    if-eq v4, v5, :cond_3

    sget-object v5, LX/0wD;->PERMALINK:LX/0wD;

    if-ne v4, v5, :cond_1

    .line 1852730
    :cond_3
    iget-object v5, v1, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->j:LX/9yI;

    invoke-virtual {v4}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {v5, v2, v3, p0, p1}, LX/9yI;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/9lZ;)V

    goto :goto_0
.end method
