.class public LX/BsF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1nq;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hL;LX/1W9;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1827336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827337
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1827338
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010212

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1827339
    new-instance v1, LX/1nq;

    invoke-direct {v1}, LX/1nq;-><init>()V

    iput-object v1, p0, LX/BsF;->a:LX/1nq;

    .line 1827340
    iget-object v1, p0, LX/BsF;->a:LX/1nq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0904

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1nq;->b(I)LX/1nq;

    .line 1827341
    iget-object v1, p0, LX/BsF;->a:LX/1nq;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v1, v0}, LX/1nq;->c(I)LX/1nq;

    .line 1827342
    iget-object v0, p0, LX/BsF;->a:LX/1nq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1nq;->a(Z)LX/1nq;

    .line 1827343
    iget-object v0, p0, LX/BsF;->a:LX/1nq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b04f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/1nq;->a(F)LX/1nq;

    .line 1827344
    iget-object v1, p0, LX/BsF;->a:LX/1nq;

    invoke-virtual {p2}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_0
    invoke-virtual {v1, v0}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 1827345
    iget-object v0, p0, LX/BsF;->a:LX/1nq;

    .line 1827346
    iput-boolean v4, v0, LX/1nq;->f:Z

    .line 1827347
    iget-object v0, p0, LX/BsF;->a:LX/1nq;

    .line 1827348
    iput-object p3, v0, LX/1nq;->d:LX/1WA;

    .line 1827349
    return-void

    .line 1827350
    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_0
.end method
