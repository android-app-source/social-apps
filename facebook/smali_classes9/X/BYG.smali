.class public final LX/BYG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1795367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1795368
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/BYG;->a:Ljava/lang/String;

    .line 1795369
    iput-object p2, p0, LX/BYG;->b:Ljava/lang/String;

    .line 1795370
    iput-object p3, p0, LX/BYG;->c:Landroid/net/Uri;

    .line 1795371
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1795358
    instance-of v0, p1, LX/BYG;

    if-nez v0, :cond_0

    .line 1795359
    const/4 v0, 0x0

    .line 1795360
    :goto_0
    return v0

    .line 1795361
    :cond_0
    if-ne p1, p0, :cond_1

    .line 1795362
    const/4 v0, 0x1

    goto :goto_0

    .line 1795363
    :cond_1
    check-cast p1, LX/BYG;

    .line 1795364
    iget-object v0, p1, LX/BYG;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1795365
    iget-object v1, p0, LX/BYG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1795366
    iget-object v0, p0, LX/BYG;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
