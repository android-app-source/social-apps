.class public final LX/CGU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/CGV;


# direct methods
.method public constructor <init>(LX/CGV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1865547
    iput-object p1, p0, LX/CGU;->d:LX/CGV;

    iput-object p2, p0, LX/CGU;->a:Ljava/lang/String;

    iput-object p3, p0, LX/CGU;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CGU;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const v0, 0x46713d22

    invoke-static {v7, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1865548
    iget-object v0, p0, LX/CGU;->d:LX/CGV;

    iget-object v2, v0, LX/CGV;->d:LX/1g8;

    iget-object v3, p0, LX/CGU;->a:Ljava/lang/String;

    iget-object v0, p0, LX/CGU;->d:LX/CGV;

    iget-object v0, v0, LX/CGV;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/CGU;->b:Ljava/lang/String;

    .line 1865549
    iget-object v5, v2, LX/1g8;->a:LX/0Zb;

    const-string v8, "group_member_info_composer_launched"

    const/4 v9, 0x0

    invoke-interface {v5, v8, v9}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1865550
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1865551
    const-string v8, "group_id"

    invoke-virtual {v5, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1865552
    const-string v8, "member_id"

    invoke-virtual {v5, v8, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1865553
    const-string v8, "source"

    invoke-virtual {v5, v8, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1865554
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 1865555
    :cond_0
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    iget-object v2, p0, LX/CGU;->b:Ljava/lang/String;

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v2, LX/89I;

    iget-object v3, p0, LX/CGU;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v3, p0, LX/CGU;->c:Ljava/lang/String;

    .line 1865556
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 1865557
    move-object v2, v2

    .line 1865558
    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "group_composer"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "ANDROID_GROUP_COMPOSER"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsGroupMemberBioPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 1865559
    iget-object v0, p0, LX/CGU;->d:LX/CGV;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1865560
    iput-object v3, v0, LX/CGV;->b:Ljava/lang/String;

    .line 1865561
    iget-object v0, p0, LX/CGU;->d:LX/CGV;

    iget-object v3, v0, LX/CGV;->a:LX/1Kf;

    iget-object v0, p0, LX/CGU;->d:LX/CGV;

    .line 1865562
    iget-object v4, v0, LX/CGV;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1865563
    const/16 v5, 0x6dc

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Landroid/app/Activity;

    invoke-static {v0, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v4, v2, v5, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1865564
    const v0, -0x693c1043

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
