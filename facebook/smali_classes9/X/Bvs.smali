.class public LX/Bvs;
.super Landroid/graphics/drawable/ShapeDrawable;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1832975
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1832976
    invoke-direct {p0, p1}, LX/Bvs;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Bvs;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1832977
    return-void
.end method

.method private a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/high16 v3, -0x1000000

    .line 1832978
    new-array v0, v4, [I

    aput v1, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0a03d8

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v3, v0, v1

    const/4 v1, 0x3

    aput v3, v0, v1

    .line 1832979
    new-array v1, v4, [F

    fill-array-data v1, :array_0

    .line 1832980
    new-instance v2, LX/Bvr;

    invoke-direct {v2, p0, v0, v1}, LX/Bvr;-><init>(LX/Bvs;[I[F)V

    return-object v2

    nop

    :array_0
    .array-data 4
        0x0
        0x3e99999a    # 0.3f
        0x3f051eb8    # 0.52f
        0x3f800000    # 1.0f
    .end array-data
.end method
