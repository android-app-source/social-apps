.class public LX/CdW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final A:LX/CdT;

.field public final B:Lcom/facebook/location/ImmutableLocation;

.field public final C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final D:Ljava/lang/String;

.field public final E:Ljava/lang/String;

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:LX/97c;

.field public final h:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;

.field public final m:Lcom/facebook/location/ImmutableLocation;

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:Ljava/lang/String;

.field public final s:Ljava/lang/String;

.field public final t:Ljava/lang/String;

.field public final u:LX/97c;

.field public final v:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public final w:Ljava/lang/String;

.field public final x:Ljava/lang/String;

.field public final y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/CdT;Lcom/facebook/location/ImmutableLocation;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/97c;",
            "Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/location/ImmutableLocation;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/97c;",
            "Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/CdT;",
            "Lcom/facebook/location/ImmutableLocation;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1922818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922819
    iput-object p1, p0, LX/CdW;->a:Ljava/lang/String;

    .line 1922820
    iput-object p2, p0, LX/CdW;->b:Ljava/lang/String;

    .line 1922821
    iput-object p3, p0, LX/CdW;->c:Ljava/lang/String;

    .line 1922822
    iput-object p4, p0, LX/CdW;->d:Ljava/lang/String;

    .line 1922823
    iput-object p5, p0, LX/CdW;->e:Ljava/lang/String;

    .line 1922824
    iput-object p6, p0, LX/CdW;->f:Ljava/lang/String;

    .line 1922825
    iput-object p7, p0, LX/CdW;->g:LX/97c;

    .line 1922826
    iput-object p8, p0, LX/CdW;->h:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 1922827
    iput-object p9, p0, LX/CdW;->i:Ljava/lang/String;

    .line 1922828
    iput-object p10, p0, LX/CdW;->j:Ljava/lang/String;

    .line 1922829
    iput-object p11, p0, LX/CdW;->k:LX/0Px;

    .line 1922830
    iput-object p12, p0, LX/CdW;->l:Ljava/lang/String;

    .line 1922831
    iput-object p13, p0, LX/CdW;->m:Lcom/facebook/location/ImmutableLocation;

    .line 1922832
    iput-object p14, p0, LX/CdW;->n:LX/0Px;

    .line 1922833
    move-object/from16 v0, p15

    iput-object v0, p0, LX/CdW;->o:LX/0Px;

    .line 1922834
    move-object/from16 v0, p16

    iput-object v0, p0, LX/CdW;->p:Ljava/lang/String;

    .line 1922835
    move-object/from16 v0, p17

    iput-object v0, p0, LX/CdW;->q:Ljava/lang/String;

    .line 1922836
    move-object/from16 v0, p18

    iput-object v0, p0, LX/CdW;->r:Ljava/lang/String;

    .line 1922837
    move-object/from16 v0, p19

    iput-object v0, p0, LX/CdW;->s:Ljava/lang/String;

    .line 1922838
    move-object/from16 v0, p20

    iput-object v0, p0, LX/CdW;->t:Ljava/lang/String;

    .line 1922839
    move-object/from16 v0, p21

    iput-object v0, p0, LX/CdW;->u:LX/97c;

    .line 1922840
    move-object/from16 v0, p22

    iput-object v0, p0, LX/CdW;->v:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 1922841
    move-object/from16 v0, p23

    iput-object v0, p0, LX/CdW;->w:Ljava/lang/String;

    .line 1922842
    move-object/from16 v0, p24

    iput-object v0, p0, LX/CdW;->x:Ljava/lang/String;

    .line 1922843
    move-object/from16 v0, p25

    iput-object v0, p0, LX/CdW;->y:LX/0Px;

    .line 1922844
    move-object/from16 v0, p26

    iput-object v0, p0, LX/CdW;->z:Ljava/lang/String;

    .line 1922845
    move-object/from16 v0, p28

    iput-object v0, p0, LX/CdW;->B:Lcom/facebook/location/ImmutableLocation;

    .line 1922846
    move-object/from16 v0, p27

    iput-object v0, p0, LX/CdW;->A:LX/CdT;

    .line 1922847
    move-object/from16 v0, p29

    iput-object v0, p0, LX/CdW;->C:LX/0Px;

    .line 1922848
    move-object/from16 v0, p30

    iput-object v0, p0, LX/CdW;->D:Ljava/lang/String;

    .line 1922849
    move-object/from16 v0, p31

    iput-object v0, p0, LX/CdW;->E:Ljava/lang/String;

    .line 1922850
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/CdT;Lcom/facebook/location/ImmutableLocation;LX/0Px;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1922851
    invoke-direct/range {p0 .. p31}, LX/CdW;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/97c;Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/CdT;Lcom/facebook/location/ImmutableLocation;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/CdV;
    .locals 2

    .prologue
    .line 1922852
    new-instance v0, LX/CdV;

    invoke-direct {v0, p0}, LX/CdV;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
