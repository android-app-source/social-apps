.class public final LX/Bsg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1yn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/1yn;


# direct methods
.method public constructor <init>(LX/1yn;)V
    .locals 1

    .prologue
    .line 1828104
    iput-object p1, p0, LX/Bsg;->e:LX/1yn;

    .line 1828105
    move-object v0, p1

    .line 1828106
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1828107
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1828108
    const-string v0, "HeaderSaveButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1828109
    if-ne p0, p1, :cond_1

    .line 1828110
    :cond_0
    :goto_0
    return v0

    .line 1828111
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1828112
    goto :goto_0

    .line 1828113
    :cond_3
    check-cast p1, LX/Bsg;

    .line 1828114
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1828115
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1828116
    if-eq v2, v3, :cond_0

    .line 1828117
    iget-object v2, p0, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1828118
    goto :goto_0

    .line 1828119
    :cond_5
    iget-object v2, p1, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1828120
    :cond_6
    iget-object v2, p0, LX/Bsg;->b:LX/1dc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bsg;->b:LX/1dc;

    iget-object v3, p1, LX/Bsg;->b:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1828121
    goto :goto_0

    .line 1828122
    :cond_8
    iget-object v2, p1, LX/Bsg;->b:LX/1dc;

    if-nez v2, :cond_7

    .line 1828123
    :cond_9
    iget-object v2, p0, LX/Bsg;->c:LX/1dc;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Bsg;->c:LX/1dc;

    iget-object v3, p1, LX/Bsg;->c:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1828124
    goto :goto_0

    .line 1828125
    :cond_b
    iget-object v2, p1, LX/Bsg;->c:LX/1dc;

    if-nez v2, :cond_a

    .line 1828126
    :cond_c
    iget-object v2, p0, LX/Bsg;->d:LX/1Pq;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Bsg;->d:LX/1Pq;

    iget-object v3, p1, LX/Bsg;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1828127
    goto :goto_0

    .line 1828128
    :cond_d
    iget-object v2, p1, LX/Bsg;->d:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
