.class public final LX/Cc7;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/io/InputStream;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1lZ;

.field public final synthetic b:LX/1FJ;

.field public final synthetic c:LX/Cc8;


# direct methods
.method public constructor <init>(LX/Cc8;LX/1lZ;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1920634
    iput-object p1, p0, LX/Cc7;->c:LX/Cc8;

    iput-object p2, p0, LX/Cc7;->a:LX/1lZ;

    iput-object p3, p0, LX/Cc7;->b:LX/1FJ;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1920642
    check-cast p1, [Ljava/io/InputStream;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1920643
    aget-object v1, p1, v1

    .line 1920644
    :try_start_0
    iget-object v2, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v2, v2, LX/Cc8;->a:LX/CcO;

    iget-object v2, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1920645
    if-nez v2, :cond_0

    .line 1920646
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/Cc7;->cancel(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1920647
    iget-object v1, p0, LX/Cc7;->a:LX/1lZ;

    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920648
    iget-object v1, p0, LX/Cc7;->b:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1920649
    :cond_0
    :try_start_1
    new-instance v3, Ljava/io/File;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const-string v5, "Facebook"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1920650
    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V

    .line 1920651
    const-string v4, "%s_%d.jpg"

    const-string v5, "FB_IMG"

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1920652
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1920653
    iget-object v3, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v3, v3, LX/Cc8;->a:LX/CcO;

    iget-object v3, v3, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    invoke-virtual {v3, v1, v5}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1920654
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "image/jpeg"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1920655
    iget-object v0, p0, LX/Cc7;->a:LX/1lZ;

    invoke-static {v0}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920656
    iget-object v0, p0, LX/Cc7;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    .line 1920657
    :catch_0
    move-exception v1

    .line 1920658
    :try_start_2
    iget-object v2, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v2, v2, LX/Cc8;->a:LX/CcO;

    iget-object v2, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not save file (w/ Fresco + jpeg) "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1920659
    iget-object v1, p0, LX/Cc7;->a:LX/1lZ;

    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920660
    iget-object v1, p0, LX/Cc7;->b:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 1920661
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/Cc7;->a:LX/1lZ;

    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920662
    iget-object v1, p0, LX/Cc7;->b:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1920635
    check-cast p1, Landroid/net/Uri;

    .line 1920636
    invoke-virtual {p0}, LX/Cc7;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920637
    :goto_0
    return-void

    .line 1920638
    :cond_0
    if-nez p1, :cond_1

    .line 1920639
    iget-object v0, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v0, v0, LX/Cc8;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not save file (w/ Fresco + jpeg) No temp uri"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920640
    iget-object v0, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v0, v0, LX/Cc8;->a:LX/CcO;

    invoke-static {v0}, LX/CcO;->m(LX/CcO;)V

    goto :goto_0

    .line 1920641
    :cond_1
    iget-object v0, p0, LX/Cc7;->c:LX/Cc8;

    iget-object v0, v0, LX/Cc8;->a:LX/CcO;

    invoke-static {v0}, LX/CcO;->l(LX/CcO;)V

    goto :goto_0
.end method
