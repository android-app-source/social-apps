.class public LX/AlT;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AlS;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709725
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1709726
    return-void
.end method


# virtual methods
.method public final a(LX/0QK;LX/9DG;)LX/AlS;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9DG;",
            ")",
            "LX/AlS;"
        }
    .end annotation

    .prologue
    .line 1709727
    new-instance v0, LX/AlS;

    invoke-static {p0}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v3

    check-cast v3, LX/3iR;

    const-class v1, LX/3iG;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/3iG;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    invoke-static {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/BFI;->b(LX/0QB;)LX/BFI;

    move-result-object v7

    check-cast v7, LX/BFI;

    invoke-static {p0}, LX/1Kx;->a(LX/0QB;)LX/1Kx;

    move-result-object v8

    check-cast v8, LX/1Kx;

    const/16 v1, 0x123

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v10

    check-cast v10, LX/20h;

    invoke-static {p0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v11

    check-cast v11, LX/1rp;

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v12

    check-cast v12, LX/1rU;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v12}, LX/AlS;-><init>(LX/0QK;LX/9DG;LX/3iR;LX/3iG;LX/189;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/BFI;LX/1Kx;LX/0Or;LX/20h;LX/1rp;LX/1rU;)V

    .line 1709728
    return-object v0
.end method
