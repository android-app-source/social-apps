.class public final LX/Cbg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

.field public final synthetic b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V
    .locals 0

    .prologue
    .line 1919742
    iput-object p1, p0, LX/Cbg;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iput-object p2, p0, LX/Cbg;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x6f270eb9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919732
    iget-object v1, p0, LX/Cbg;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v1

    .line 1919733
    iget-object v2, p0, LX/Cbg;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->s:LX/9hh;

    iget-object v3, p0, LX/Cbg;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v3}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, LX/Cbg;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->b()Ljava/lang/String;

    move-result-object v5

    const/4 p1, 0x0

    .line 1919734
    new-instance v7, LX/9hf;

    invoke-direct {v7, v2, v3, v4, v5}, LX/9hf;-><init>(LX/9hh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1919735
    iget-object v8, v2, LX/9hh;->f:LX/0Uh;

    const/16 v9, 0x405

    invoke-virtual {v8, v9, p1}, LX/0Uh;->a(IZ)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1919736
    const/4 v8, 0x2

    new-array v8, v8, [LX/4VT;

    new-instance v9, LX/9hR;

    invoke-static {v4, v1}, LX/9hi;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v10

    invoke-direct {v9, v3, v10}, LX/9hR;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlace;)V

    aput-object v9, v8, p1

    const/4 v9, 0x1

    new-instance v10, LX/9hQ;

    invoke-static {v4, v1}, LX/9hi;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object p1

    invoke-direct {v10, v3, p1}, LX/9hQ;-><init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V

    aput-object v10, v8, v9

    invoke-static {v2, v7, v8}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1919737
    :goto_0
    iget-object v1, p0, LX/Cbg;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t:LX/9iE;

    iget-object v2, p0, LX/Cbg;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    iget-object v3, p0, LX/Cbg;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    .line 1919738
    const-string v4, "photo_location_suggestion_accept"

    invoke-static {v1, v4, v2, v3}, LX/9iE;->a(LX/9iE;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;LX/5kD;)V

    .line 1919739
    const v1, 0x7aaf3e8d

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    iget-object v8, v2, LX/9hh;->d:LX/9hs;

    .line 1919740
    new-instance v9, LX/9hk;

    new-instance v10, LX/9hq;

    invoke-direct {v10, v8, v3, v4, v1}, LX/9hq;-><init>(LX/9hs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v3, v10}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    move-object v8, v9

    .line 1919741
    invoke-static {v2, v7, v8}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
