.class public LX/Ae5;
.super LX/AVi;
.source ""

# interfaces
.implements LX/Ae4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;",
        ">;",
        "LX/Ae4;"
    }
.end annotation


# instance fields
.field public final a:LX/Ae2;

.field private final b:LX/3HT;

.field private final c:LX/Ae3;

.field public d:LX/Ac3;

.field public e:F

.field private f:I

.field public g:I


# direct methods
.method public constructor <init>(LX/Ae2;LX/3HT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1696026
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1696027
    iput-object p1, p0, LX/Ae5;->a:LX/Ae2;

    .line 1696028
    iput-object p2, p0, LX/Ae5;->b:LX/3HT;

    .line 1696029
    new-instance v0, LX/Ae3;

    invoke-direct {v0, p0}, LX/Ae3;-><init>(LX/Ae5;)V

    iput-object v0, p0, LX/Ae5;->c:LX/Ae3;

    .line 1696030
    return-void
.end method

.method private b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V
    .locals 2

    .prologue
    .line 1696018
    iput-object p0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->f:LX/Ae4;

    .line 1696019
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->c:LX/Ac3;

    move-object v0, v0

    .line 1696020
    iput-object v0, p0, LX/Ae5;->d:LX/Ac3;

    .line 1696021
    iget-object v0, p0, LX/Ae5;->d:LX/Ac3;

    iget v1, p0, LX/Ae5;->e:F

    invoke-virtual {v0, v1}, LX/Ac3;->a(F)V

    .line 1696022
    iget v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->d:I

    move v0, v0

    .line 1696023
    iput v0, p0, LX/Ae5;->f:I

    .line 1696024
    iget-object v0, p0, LX/Ae5;->d:LX/Ac3;

    invoke-virtual {v0}, LX/Ac3;->getColor()I

    move-result v0

    iput v0, p0, LX/Ae5;->g:I

    .line 1696025
    return-void
.end method

.method private static c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V
    .locals 1

    .prologue
    .line 1696015
    const/4 v0, 0x0

    .line 1696016
    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->f:LX/Ae4;

    .line 1696017
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1696012
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1696013
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setVisibility(I)V

    .line 1696014
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1695955
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    check-cast p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    .line 1695956
    invoke-static {p2}, LX/Ae5;->c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V

    .line 1695957
    invoke-direct {p0, p1}, LX/Ae5;->b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V

    .line 1695958
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setVisibility(I)V

    .line 1695959
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getAlpha()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setAlpha(F)V

    .line 1695960
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setEnabled(Z)V

    .line 1695961
    iget-boolean v0, p2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->e:Z

    move v0, v0

    .line 1695962
    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setSwipeToReactions(Z)V

    .line 1695963
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    .line 1695964
    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    move-object v1, v1

    .line 1695965
    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695966
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696031
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1696032
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1696033
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1696034
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1696035
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1696036
    iput-object p1, v0, LX/Ae2;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1696037
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1696038
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1696039
    iget-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/Ae2;->F:Z

    .line 1696040
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Ae2;->G:Z

    .line 1696041
    const/4 v1, 0x0

    iput-object v1, v0, LX/Ae2;->W:Ljava/lang/String;

    .line 1696042
    iget-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1696043
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v2

    iput-boolean v2, v0, LX/Ae2;->G:Z

    .line 1696044
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Ae2;->W:Ljava/lang/String;

    .line 1696045
    :cond_0
    iget-boolean v1, v0, LX/Ae2;->F:Z

    if-eqz v1, :cond_8

    .line 1696046
    iget-object v1, v0, LX/Ae2;->f:LX/Adk;

    invoke-virtual {v1, p1}, LX/Adk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1696047
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1696048
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v1, v3}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->setVisibility(I)V

    .line 1696049
    :goto_0
    iget-boolean v1, v0, LX/Ae2;->G:Z

    const/4 v5, 0x0

    .line 1696050
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1696051
    check-cast v2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1696052
    invoke-static {v0}, LX/Ae2;->p(LX/Ae2;)Z

    move-result v2

    if-eqz v2, :cond_a

    if-eqz v1, :cond_a

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v0, LX/Ae2;->E:Z

    .line 1696053
    invoke-static {p1}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/Ae2;->U:Ljava/lang/String;

    .line 1696054
    invoke-static {v0}, LX/Ae2;->p(LX/Ae2;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/Ae2;->q(LX/Ae2;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/Ae2;->r(LX/Ae2;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/Ae2;->s(LX/Ae2;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_1
    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1696055
    if-eqz v2, :cond_2

    .line 1696056
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1696057
    check-cast v2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1696058
    :cond_2
    iget-object v2, v0, LX/Ae2;->f:LX/Adk;

    .line 1696059
    iput-boolean v1, v2, LX/Adk;->i:Z

    .line 1696060
    iget-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/Ac6;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    iput-boolean v1, v0, LX/Ae2;->H:Z

    .line 1696061
    invoke-virtual {v0}, LX/Ae2;->f()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1696062
    iget-object v1, v0, LX/Ae2;->g:LX/AgU;

    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v8, 0x0

    .line 1696063
    invoke-static {v2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v4, v1, LX/AgU;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1696064
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-static {v4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1696065
    iget-object v4, v1, LX/AVi;->a:Landroid/view/View;

    move-object v4, v4

    .line 1696066
    check-cast v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    .line 1696067
    invoke-static {v1, v4}, LX/AgU;->a(LX/AgU;Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;)V

    .line 1696068
    iget-object v5, v1, LX/AgU;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 1696069
    iget-object v5, v1, LX/AVi;->a:Landroid/view/View;

    move-object v5, v5

    .line 1696070
    check-cast v5, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setSelectedReaction(Landroid/view/View;)V

    .line 1696071
    iget-object v5, v1, LX/AgU;->a:LX/1zf;

    iget-object v6, v1, LX/AgU;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    .line 1696072
    if-nez v5, :cond_c

    .line 1696073
    iget-object v5, v1, LX/AgU;->a:LX/1zf;

    invoke-virtual {v5}, LX/1zf;->d()LX/0Px;

    move-result-object v5

    move-object v6, v5

    :goto_3
    move v7, v8

    .line 1696074
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    if-ge v7, v5, :cond_6

    .line 1696075
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1zt;

    .line 1696076
    iget v9, v5, LX/1zt;->e:I

    move v10, v9

    .line 1696077
    iget-object v5, v1, LX/AgU;->a:LX/1zf;

    invoke-virtual {v5, v10}, LX/1zf;->a(I)LX/1zt;

    move-result-object v11

    .line 1696078
    new-instance v12, LX/AgD;

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v12, v5}, LX/AgD;-><init>(Landroid/content/Context;)V

    .line 1696079
    invoke-virtual {v12, v11}, LX/AgD;->setReaction(LX/1zt;)V

    .line 1696080
    const/4 v5, 0x1

    invoke-virtual {v12, v5}, LX/AgD;->setClickable(Z)V

    .line 1696081
    new-instance v5, LX/AgT;

    invoke-direct {v5, v1}, LX/AgT;-><init>(LX/AgU;)V

    invoke-virtual {v12, v5}, LX/AgD;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1696082
    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v13

    .line 1696083
    iget v5, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->a:I

    iput v5, v13, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1696084
    iget v5, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->a:I

    iput v5, v13, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1696085
    if-nez v7, :cond_4

    move v5, v8

    :goto_5
    iget p0, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v7, v9, :cond_5

    move v9, v8

    :goto_6
    iget p1, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    invoke-virtual {v13, v5, p0, v9, p1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1696086
    invoke-virtual {v4, v12, v13}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1696087
    iget-object v5, v1, LX/AgU;->d:Ljava/util/List;

    invoke-interface {v5, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1696088
    iget-object v5, v11, LX/1zt;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1696089
    invoke-virtual {v12, v5}, LX/AgD;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1696090
    iget-object v5, v1, LX/AgU;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v5

    if-ne v5, v10, :cond_3

    .line 1696091
    iget-object v5, v1, LX/AVi;->a:Landroid/view/View;

    move-object v5, v5

    .line 1696092
    check-cast v5, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v5, v12}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setSelectedReaction(Landroid/view/View;)V

    .line 1696093
    :cond_3
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_4

    .line 1696094
    :cond_4
    iget v5, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    div-int/lit8 v5, v5, 0x2

    goto :goto_5

    :cond_5
    iget v9, v4, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    div-int/lit8 v9, v9, 0x2

    goto :goto_6

    .line 1696095
    :cond_6
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1696096
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v1, v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setVisibility(I)V

    .line 1696097
    invoke-static {v0}, LX/Ae2;->v$redex0(LX/Ae2;)V

    .line 1696098
    :goto_7
    iget-boolean v1, v0, LX/Ae2;->G:Z

    if-eqz v1, :cond_7

    iget-object v1, v0, LX/Ae2;->W:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1696099
    iget-object v1, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1696100
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1696101
    iget-object v1, v0, LX/Ae2;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ah9;

    iget-object v2, v0, LX/Ae2;->W:Ljava/lang/String;

    .line 1696102
    new-instance v3, LX/6Td;

    invoke-direct {v3}, LX/6Td;-><init>()V

    move-object v3, v3

    .line 1696103
    const-string v4, "videoID"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1696104
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1696105
    iget-object v4, v1, LX/Ah9;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1696106
    new-instance v4, LX/Ah8;

    invoke-direct {v4, v1, v0}, LX/Ah8;-><init>(LX/Ah9;LX/Ae2;)V

    iget-object v5, v1, LX/Ah9;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1696107
    :cond_7
    return-void

    .line 1696108
    :cond_8
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1696109
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v1, v4}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->setVisibility(I)V

    goto/16 :goto_0

    .line 1696110
    :cond_9
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1696111
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v1, v4}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setVisibility(I)V

    goto :goto_7

    :cond_a
    move v2, v5

    .line 1696112
    goto/16 :goto_1

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_c
    move-object v6, v5

    goto/16 :goto_3
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1695998
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695999
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setEnabled(Z)V

    .line 1696000
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    const/4 v2, 0x0

    .line 1696001
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1696002
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v1, p1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->setEnabled(Z)V

    .line 1696003
    if-nez p1, :cond_1

    .line 1696004
    iget-object v1, v0, LX/Ae2;->k:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1696005
    iget-object v1, v0, LX/Ae2;->V:LX/0hs;

    if-eqz v1, :cond_0

    .line 1696006
    iget-object v1, v0, LX/Ae2;->V:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 1696007
    iput-object v2, v0, LX/Ae2;->V:LX/0hs;

    .line 1696008
    :cond_0
    iget-object v1, v0, LX/Ae2;->f:LX/Adk;

    invoke-virtual {v1}, LX/Adk;->e()V

    .line 1696009
    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/Ae5;->a(I)V

    .line 1696010
    return-void

    .line 1696011
    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1695987
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    const/4 v1, 0x0

    .line 1695988
    const/4 v0, 0x0

    iput v0, p0, LX/Ae5;->e:F

    .line 1695989
    invoke-direct {p0, p1}, LX/Ae5;->b(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V

    .line 1695990
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setAlpha(F)V

    .line 1695991
    invoke-virtual {p1, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setVisibility(I)V

    .line 1695992
    invoke-virtual {p1, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setSwipeToReactions(Z)V

    .line 1695993
    iget-object v0, p0, LX/Ae5;->b:LX/3HT;

    iget-object v1, p0, LX/Ae5;->c:LX/Ae3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1695994
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    .line 1695995
    iget-object v1, p1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    move-object v1, v1

    .line 1695996
    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1695997
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1695971
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695972
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    invoke-static {v0}, LX/Ae5;->c(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;)V

    .line 1695973
    iget-object v0, p0, LX/Ae5;->b:LX/3HT;

    iget-object v1, p0, LX/Ae5;->c:LX/Ae3;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1695974
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    .line 1695975
    iget-object v1, v0, LX/Ae2;->k:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695976
    iget-object v1, v0, LX/Ae2;->f:LX/Adk;

    invoke-virtual {v1}, LX/Adk;->e()V

    .line 1695977
    iget-object v1, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1695978
    iget-object v2, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v2

    .line 1695979
    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1695980
    iget-object v2, v1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v2

    .line 1695981
    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1695982
    iget-object v1, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1695983
    :cond_0
    iget-object v1, v0, LX/Ae2;->T:LX/3Af;

    if-eqz v1, :cond_1

    .line 1695984
    iget-object v1, v0, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->dismiss()V

    .line 1695985
    :cond_1
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1695986
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1695967
    iget-object v0, p0, LX/Ae5;->a:LX/Ae2;

    .line 1695968
    invoke-virtual {v0}, LX/Ae2;->f()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1695969
    invoke-static {v0}, LX/Ae2;->F(LX/Ae2;)V

    .line 1695970
    :cond_0
    return-void
.end method
