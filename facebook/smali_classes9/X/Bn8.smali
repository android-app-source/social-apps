.class public LX/Bn8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Landroid/view/View;

.field public f:LX/BnG;

.field public g:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;

.field public final i:LX/0hB;

.field public final j:LX/1rq;

.field public k:Z

.field public final l:LX/1De;

.field public final m:LX/BcP;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bmi;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0W3;

.field public final p:I

.field public final q:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Landroid/content/Context;LX/0ad;LX/0hB;LX/1rq;LX/0Ot;LX/0W3;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0hB;",
            "LX/1rq;",
            "LX/0Ot",
            "<",
            "LX/Bmi;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1819832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1819833
    iput-object p1, p0, LX/Bn8;->b:Ljava/lang/String;

    .line 1819834
    iput-object p2, p0, LX/Bn8;->c:Ljava/lang/String;

    .line 1819835
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Bn8;->p:I

    .line 1819836
    iput-object p4, p0, LX/Bn8;->d:Ljava/lang/String;

    .line 1819837
    iput-object p5, p0, LX/Bn8;->a:Landroid/content/Context;

    .line 1819838
    new-instance v0, LX/1De;

    invoke-direct {v0, p5}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Bn8;->l:LX/1De;

    .line 1819839
    new-instance v0, LX/BcP;

    invoke-direct {v0, p5}, LX/BcP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Bn8;->m:LX/BcP;

    .line 1819840
    iget v0, p0, LX/Bn8;->p:I

    mul-int/lit8 v0, v0, 0x3

    iput v0, p0, LX/Bn8;->q:I

    .line 1819841
    iput-object p6, p0, LX/Bn8;->h:LX/0ad;

    .line 1819842
    iput-object p7, p0, LX/Bn8;->i:LX/0hB;

    .line 1819843
    iput-object p8, p0, LX/Bn8;->j:LX/1rq;

    .line 1819844
    iput-object p9, p0, LX/Bn8;->n:LX/0Ot;

    .line 1819845
    iput-object p10, p0, LX/Bn8;->o:LX/0W3;

    .line 1819846
    return-void
.end method

.method public static a(LX/Bn8;)Landroid/view/View;
    .locals 7

    .prologue
    .line 1819847
    iget-object v1, p0, LX/Bn8;->o:LX/0W3;

    sget-wide v3, LX/0X5;->cM:J

    const/4 v2, 0x0

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JZ)Z

    move-result v1

    move v0, v1

    .line 1819848
    if-eqz v0, :cond_0

    .line 1819849
    const/4 v2, -0x1

    .line 1819850
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1819851
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    .line 1819852
    :goto_0
    move-object v0, v0

    .line 1819853
    :goto_1
    return-object v0

    .line 1819854
    :cond_0
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 1819855
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    .line 1819856
    :goto_2
    move-object v0, v0

    .line 1819857
    goto :goto_1

    .line 1819858
    :cond_1
    new-instance v0, Lcom/facebook/components/ComponentView;

    iget-object v1, p0, LX/Bn8;->l:LX/1De;

    invoke-direct {v0, v1}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    iput-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    .line 1819859
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1819860
    iget-object v0, p0, LX/Bn8;->m:LX/BcP;

    invoke-static {v0}, LX/Bdq;->c(LX/1De;)LX/Bdl;

    move-result-object v0

    .line 1819861
    iget-object v1, p0, LX/Bn8;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bmi;

    .line 1819862
    new-instance v3, LX/Bmg;

    invoke-direct {v3, v1}, LX/Bmg;-><init>(LX/Bmi;)V

    .line 1819863
    sget-object v4, LX/Bmi;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bmf;

    .line 1819864
    if-nez v4, :cond_2

    .line 1819865
    new-instance v4, LX/Bmf;

    invoke-direct {v4}, LX/Bmf;-><init>()V

    .line 1819866
    :cond_2
    iput-object v3, v4, LX/BcN;->a:LX/BcO;

    .line 1819867
    iput-object v3, v4, LX/Bmf;->a:LX/Bmg;

    .line 1819868
    iget-object v1, v4, LX/Bmf;->d:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 1819869
    move-object v3, v4

    .line 1819870
    move-object v1, v3

    .line 1819871
    new-instance v2, LX/Bn7;

    invoke-direct {v2, p0}, LX/Bn7;-><init>(LX/Bn8;)V

    .line 1819872
    iget-object v3, v1, LX/Bmf;->a:LX/Bmg;

    iput-object v2, v3, LX/Bmg;->c:LX/1rs;

    .line 1819873
    iget-object v3, v1, LX/Bmf;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1819874
    move-object v1, v1

    .line 1819875
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/event/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Bn8;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/theme/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/Bn8;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1819876
    iget-object v3, v1, LX/Bmf;->a:LX/Bmg;

    iput-object v2, v3, LX/Bmg;->f:Ljava/lang/String;

    .line 1819877
    iget-object v3, v1, LX/Bmf;->d:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1819878
    move-object v1, v1

    .line 1819879
    iget v2, p0, LX/Bn8;->p:I

    .line 1819880
    iget-object v3, v1, LX/Bmf;->a:LX/Bmg;

    iput v2, v3, LX/Bmg;->d:I

    .line 1819881
    iget-object v3, v1, LX/Bmf;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1819882
    move-object v1, v1

    .line 1819883
    const/16 v2, 0xc

    .line 1819884
    iget-object v3, v1, LX/Bmf;->a:LX/Bmg;

    iput v2, v3, LX/Bmg;->e:I

    .line 1819885
    iget-object v3, v1, LX/Bmf;->d:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1819886
    move-object v1, v1

    .line 1819887
    invoke-virtual {v1}, LX/Bmf;->b()LX/BcO;

    move-result-object v1

    move-object v1, v1

    .line 1819888
    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/BcO;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, LX/Bn8;->l:LX/1De;

    invoke-static {v1}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bdl;->b(LX/1X1;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, LX/Bn8;->l:LX/1De;

    invoke-static {v1}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/1X1;)LX/Bdl;

    move-result-object v0

    new-instance v1, LX/Bdc;

    iget v2, p0, LX/Bn8;->p:I

    invoke-direct {v1, v2}, LX/Bdc;-><init>(I)V

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/Bdb;)LX/Bdl;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1819889
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    check-cast v0, Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/Bn8;->m:LX/BcP;

    invoke-static {v2, v1}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v1

    invoke-virtual {v1}, LX/1me;->b()LX/1dV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1819890
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    goto/16 :goto_0

    .line 1819891
    :cond_3
    new-instance v0, LX/BnG;

    invoke-direct {v0}, LX/BnG;-><init>()V

    iput-object v0, p0, LX/Bn8;->f:LX/BnG;

    .line 1819892
    iget-object v0, p0, LX/Bn8;->f:LX/BnG;

    iget v1, p0, LX/Bn8;->p:I

    .line 1819893
    iput v1, v0, LX/BnG;->b:I

    .line 1819894
    new-instance v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/Bn8;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    .line 1819895
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1819896
    new-instance v1, LX/3wu;

    iget-object v2, p0, LX/Bn8;->a:Landroid/content/Context;

    iget v3, p0, LX/Bn8;->p:I

    invoke-direct {v1, v2, v3}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 1819897
    new-instance v2, LX/Bn5;

    invoke-direct {v2, p0}, LX/Bn5;-><init>(LX/Bn8;)V

    .line 1819898
    iput-object v2, v1, LX/3wu;->h:LX/3wr;

    .line 1819899
    move-object v1, v1

    .line 1819900
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1819901
    iget-object v1, p0, LX/Bn8;->f:LX/BnG;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1819902
    new-instance v1, LX/Bn4;

    invoke-direct {v1, p0}, LX/Bn4;-><init>(LX/Bn8;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1819903
    new-instance v1, LX/BnA;

    iget-object v2, p0, LX/Bn8;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1e1d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget v3, p0, LX/Bn8;->p:I

    iget-object v4, p0, LX/Bn8;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1e1e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, LX/BnA;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1819904
    iget-object v0, p0, LX/Bn8;->j:LX/1rq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/event/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Bn8;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/theme/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/Bn8;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Bn7;

    invoke-direct {v2, p0}, LX/Bn7;-><init>(LX/Bn8;)V

    invoke-virtual {v0, v1, v2}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, LX/Bn8;->g:LX/2kW;

    .line 1819905
    iget-object v0, p0, LX/Bn8;->g:LX/2kW;

    new-instance v1, LX/Bn6;

    invoke-direct {v1, p0}, LX/Bn6;-><init>(LX/Bn8;)V

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 1819906
    iget-object v0, p0, LX/Bn8;->g:LX/2kW;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->a(ILjava/lang/Object;)V

    .line 1819907
    iget-object v0, p0, LX/Bn8;->e:Landroid/view/View;

    goto/16 :goto_2
.end method


# virtual methods
.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1819908
    iget-boolean v0, p0, LX/Bn8;->k:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1819909
    invoke-static {p0}, LX/Bn8;->a(LX/Bn8;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1819910
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Bn8;->k:Z

    .line 1819911
    :cond_0
    iget-object v0, p0, LX/Bn8;->g:LX/2kW;

    if-eqz v0, :cond_1

    .line 1819912
    iget-object v0, p0, LX/Bn8;->g:LX/2kW;

    invoke-virtual {v0}, LX/2kW;->c()V

    .line 1819913
    :cond_1
    return-void
.end method
