.class public LX/CcX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/CaP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryVideoUfiListener;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryVideoPagerListener;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921289
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/CcX;->b:Ljava/util/WeakHashMap;

    .line 1921290
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/CcX;->c:Ljava/util/WeakHashMap;

    .line 1921291
    return-void
.end method

.method public static a(LX/0QB;)LX/CcX;
    .locals 3

    .prologue
    .line 1921292
    const-class v1, LX/CcX;

    monitor-enter v1

    .line 1921293
    :try_start_0
    sget-object v0, LX/CcX;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921294
    sput-object v2, LX/CcX;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1921297
    new-instance v0, LX/CcX;

    invoke-direct {v0}, LX/CcX;-><init>()V

    .line 1921298
    move-object v0, v0

    .line 1921299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CcX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1921303
    iget-object v0, p0, LX/CcX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cae;

    .line 1921304
    if-eqz v0, :cond_0

    .line 1921305
    invoke-virtual {v0, p1, p2}, LX/Cae;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1921306
    :cond_1
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1921307
    iget-object v0, p0, LX/CcX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cae;

    .line 1921308
    if-eqz v0, :cond_0

    .line 1921309
    iget-object v2, v0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v2, :cond_3

    .line 1921310
    :cond_1
    :goto_1
    goto :goto_0

    .line 1921311
    :cond_2
    return-void

    .line 1921312
    :cond_3
    iget-object v2, v0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v2, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1921313
    iget-object v2, v0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1921314
    iget-object v2, v0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object p0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_1
.end method
