.class public final LX/BN5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/profilelist/ProfilesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profilelist/ProfilesListFragment;)V
    .locals 0

    .prologue
    .line 1778262
    iput-object p1, p0, LX/BN5;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1778247
    iget-object v0, p0, LX/BN5;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778248
    const/4 v3, 0x0

    .line 1778249
    iget-object v2, v0, LX/BN0;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1778250
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1778251
    iget-object v4, v0, LX/BN0;->a:LX/2Rd;

    invoke-static {v2}, LX/BN0;->b(Lcom/facebook/ipc/model/FacebookProfile;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1778252
    iget-boolean v6, v0, LX/BN0;->f:Z

    if-eqz v6, :cond_1

    if-eqz v3, :cond_0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1778253
    :cond_0
    iget-object v3, v0, LX/BN0;->c:Ljava/util/List;

    new-instance v6, LX/BMy;

    invoke-static {v2}, LX/BN0;->b(Lcom/facebook/ipc/model/FacebookProfile;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v0, v7}, LX/BMy;-><init>(LX/BN0;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    .line 1778254
    :cond_1
    iget-object v4, v0, LX/BN0;->c:Ljava/util/List;

    new-instance v6, LX/BMz;

    invoke-direct {v6, v0, v2}, LX/BMz;-><init>(LX/BN0;Lcom/facebook/ipc/model/FacebookProfile;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1778255
    :cond_2
    const v2, -0x2de73b7

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1778256
    iget-object v2, p0, LX/BN5;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, p0, LX/BN5;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    invoke-virtual {v0}, LX/BN0;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v1, v0}, Lcom/facebook/profilelist/ProfilesListFragment;->a(ZZ)V

    .line 1778257
    return-void

    :cond_3
    move v0, v1

    .line 1778258
    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1778260
    sget-object v0, Lcom/facebook/profilelist/ProfilesListFragment;->a:Ljava/lang/Class;

    const-string v1, "Failure fetching contacts"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1778261
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1778259
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, LX/BN5;->a(Ljava/util/List;)V

    return-void
.end method
