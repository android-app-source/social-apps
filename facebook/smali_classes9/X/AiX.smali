.class public LX/AiX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1706346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1706347
    iput-object p1, p0, LX/AiX;->a:LX/0Or;

    .line 1706348
    iput-object p2, p0, LX/AiX;->b:LX/0ad;

    .line 1706349
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 1706350
    iget-object v0, p0, LX/AiX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AiX;->b:LX/0ad;

    sget-short v1, LX/Aha;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1706351
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "/feed_awesomizer"

    .line 1706352
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1706353
    move-object v0, v0

    .line 1706354
    const-string v1, "AwesomizerRoute"

    .line 1706355
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 1706356
    move-object v0, v0

    .line 1706357
    const v1, 0x7f082388

    .line 1706358
    iput v1, v0, LX/98r;->d:I

    .line 1706359
    move-object v0, v0

    .line 1706360
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1706361
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 1706362
    :goto_0
    return-object v0

    .line 1706363
    :cond_0
    new-instance v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    invoke-direct {v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;-><init>()V

    .line 1706364
    move-object v0, v0

    .line 1706365
    goto :goto_0
.end method
