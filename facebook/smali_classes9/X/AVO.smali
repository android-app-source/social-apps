.class public final LX/AVO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field public final synthetic a:LX/AVP;

.field private final b:Ljava/lang/Object;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/SurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/61B;

.field private volatile e:Landroid/view/Surface;

.field public f:LX/7Rn;

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(LX/AVP;Landroid/view/SurfaceView;LX/61B;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1679395
    iput-object p1, p0, LX/AVO;->a:LX/AVP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679396
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/AVO;->b:Ljava/lang/Object;

    .line 1679397
    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Invalid view submitted"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1679398
    if-eqz p3, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "Invalid GLRenderer submitted"

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1679399
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AVO;->c:Ljava/lang/ref/WeakReference;

    .line 1679400
    iput-object p3, p0, LX/AVO;->d:LX/61B;

    .line 1679401
    invoke-virtual {p2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1679402
    iput-boolean v1, p0, LX/AVO;->g:Z

    .line 1679403
    return-void

    :cond_1
    move v0, v2

    .line 1679404
    goto :goto_0
.end method

.method public static b$redex0(LX/AVO;)V
    .locals 1

    .prologue
    .line 1679391
    iget-object v0, p0, LX/AVO;->f:LX/7Rn;

    if-eqz v0, :cond_0

    .line 1679392
    iget-object v0, p0, LX/AVO;->f:LX/7Rn;

    invoke-virtual {v0}, LX/7Rn;->d()V

    .line 1679393
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVO;->f:LX/7Rn;

    .line 1679394
    :cond_0
    return-void
.end method

.method public static i$redex0(LX/AVO;)V
    .locals 4

    .prologue
    .line 1679384
    iget-object v0, p0, LX/AVO;->f:LX/7Rn;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AVO;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1679385
    new-instance v0, LX/7Rn;

    iget-object v1, p0, LX/AVO;->a:LX/AVP;

    iget-object v1, v1, LX/AVP;->t:LX/7Rl;

    iget-object v2, p0, LX/AVO;->e:Landroid/view/Surface;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/7Rn;-><init>(LX/7Rl;Landroid/view/Surface;Z)V

    iput-object v0, p0, LX/AVO;->f:LX/7Rn;

    .line 1679386
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AVO;->h:Z

    .line 1679387
    iget-object v0, p0, LX/AVO;->d:LX/61B;

    iget-object v1, p0, LX/AVO;->a:LX/AVP;

    iget-object v1, v1, LX/AVP;->w:LX/AWK;

    .line 1679388
    iget-object v2, v1, LX/AWK;->b:LX/5Pc;

    move-object v1, v2

    .line 1679389
    invoke-interface {v0, v1}, LX/61B;->a(LX/5Pc;)V

    .line 1679390
    :cond_0
    return-void
.end method


# virtual methods
.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    .prologue
    .line 1679378
    iget-object v1, p0, LX/AVO;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1679379
    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, LX/AVO;->e:Landroid/view/Surface;

    .line 1679380
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679381
    iget-object v0, p0, LX/AVO;->a:LX/AVP;

    iget-object v0, v0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVO;->a:LX/AVP;

    iget-object v1, v1, LX/AVP;->b:LX/AVN;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, p3, p4, p0}, LX/AVN;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679382
    return-void

    .line 1679383
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 1679367
    iget-object v1, p0, LX/AVO;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1679368
    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, LX/AVO;->e:Landroid/view/Surface;

    .line 1679369
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679370
    iget-object v0, p0, LX/AVO;->a:LX/AVP;

    iget-object v0, v0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVO;->a:LX/AVP;

    iget-object v1, v1, LX/AVP;->b:LX/AVN;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, p0}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679371
    return-void

    .line 1679372
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 1679373
    iget-object v1, p0, LX/AVO;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1679374
    :try_start_0
    iget-object v0, p0, LX/AVO;->e:Landroid/view/Surface;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 1679375
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVO;->e:Landroid/view/Surface;

    .line 1679376
    iget-object v0, p0, LX/AVO;->a:LX/AVP;

    iget-object v0, v0, LX/AVP;->b:LX/AVN;

    iget-object v2, p0, LX/AVO;->a:LX/AVP;

    iget-object v2, v2, LX/AVP;->b:LX/AVN;

    const/16 v3, 0xd

    invoke-virtual {v2, v3, p0}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679377
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
