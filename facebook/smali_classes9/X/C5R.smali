.class public LX/C5R;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5S;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5R",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C5S;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848843
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1848844
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C5R;->b:LX/0Zi;

    .line 1848845
    iput-object p1, p0, LX/C5R;->a:LX/0Ot;

    .line 1848846
    return-void
.end method

.method public static a(LX/0QB;)LX/C5R;
    .locals 4

    .prologue
    .line 1848847
    const-class v1, LX/C5R;

    monitor-enter v1

    .line 1848848
    :try_start_0
    sget-object v0, LX/C5R;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848849
    sput-object v2, LX/C5R;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848850
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848851
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848852
    new-instance v3, LX/C5R;

    const/16 p0, 0x1f6a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C5R;-><init>(LX/0Ot;)V

    .line 1848853
    move-object v0, v3

    .line 1848854
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848855
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848856
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1848858
    check-cast p2, LX/C5Q;

    .line 1848859
    iget-object v0, p0, LX/C5R;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5S;

    iget-object v1, p2, LX/C5Q;->a:LX/1Pq;

    iget-object v2, p2, LX/C5Q;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848860
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848861
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1848862
    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848863
    iget-object v4, v0, LX/C5S;->c:LX/AnA;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/AnA;->a(Ljava/lang/Class;)LX/An9;

    move-result-object v3

    .line 1848864
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C5S;->a:LX/C5C;

    const/4 v6, 0x0

    .line 1848865
    new-instance p0, LX/C5B;

    invoke-direct {p0, v5}, LX/C5B;-><init>(LX/C5C;)V

    .line 1848866
    iget-object p2, v5, LX/C5C;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C5A;

    .line 1848867
    if-nez p2, :cond_0

    .line 1848868
    new-instance p2, LX/C5A;

    invoke-direct {p2, v5}, LX/C5A;-><init>(LX/C5C;)V

    .line 1848869
    :cond_0
    invoke-static {p2, p1, v6, v6, p0}, LX/C5A;->a$redex0(LX/C5A;LX/1De;IILX/C5B;)V

    .line 1848870
    move-object p0, p2

    .line 1848871
    move-object v6, p0

    .line 1848872
    move-object v5, v6

    .line 1848873
    iget-object v6, v5, LX/C5A;->a:LX/C5B;

    iput-object v2, v6, LX/C5B;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848874
    iget-object v6, v5, LX/C5A;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1848875
    move-object v5, v5

    .line 1848876
    iget-object v6, v5, LX/C5A;->a:LX/C5B;

    iput-object v1, v6, LX/C5B;->b:LX/1Pq;

    .line 1848877
    iget-object v6, v5, LX/C5A;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1848878
    move-object v5, v5

    .line 1848879
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C5S;->b:LX/C5K;

    const/4 v6, 0x0

    .line 1848880
    new-instance p0, LX/C5J;

    invoke-direct {p0, v5}, LX/C5J;-><init>(LX/C5K;)V

    .line 1848881
    iget-object p2, v5, LX/C5K;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C5I;

    .line 1848882
    if-nez p2, :cond_1

    .line 1848883
    new-instance p2, LX/C5I;

    invoke-direct {p2, v5}, LX/C5I;-><init>(LX/C5K;)V

    .line 1848884
    :cond_1
    invoke-static {p2, p1, v6, v6, p0}, LX/C5I;->a$redex0(LX/C5I;LX/1De;IILX/C5J;)V

    .line 1848885
    move-object p0, p2

    .line 1848886
    move-object v6, p0

    .line 1848887
    move-object v5, v6

    .line 1848888
    iget-object v6, v5, LX/C5I;->a:LX/C5J;

    iput-object v2, v6, LX/C5J;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848889
    iget-object v6, v5, LX/C5I;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1848890
    move-object v5, v5

    .line 1848891
    iget-object v6, v5, LX/C5I;->a:LX/C5J;

    iput-object v1, v6, LX/C5J;->a:LX/1Pq;

    .line 1848892
    iget-object v6, v5, LX/C5I;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1848893
    move-object v5, v5

    .line 1848894
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-virtual {v3, p1, v2}, LX/An9;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b09af

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1848895
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1848896
    invoke-static {}, LX/1dS;->b()V

    .line 1848897
    const/4 v0, 0x0

    return-object v0
.end method
