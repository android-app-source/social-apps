.class public final LX/CcI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:LX/CcJ;


# direct methods
.method public constructor <init>(LX/CcJ;)V
    .locals 0

    .prologue
    .line 1920788
    iput-object p1, p0, LX/CcI;->a:LX/CcJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1920789
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    iget-object v2, p0, LX/CcI;->a:LX/CcJ;

    iget-object v2, v2, LX/CcJ;->a:LX/5kD;

    iget-object v3, p0, LX/CcI;->a:LX/CcJ;

    iget-object v3, v3, LX/CcJ;->b:LX/CcO;

    invoke-virtual {v3}, LX/CcO;->b()LX/1cj;

    move-result-object v3

    .line 1920790
    invoke-static {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;LX/5kD;)LX/1ca;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4, v3, v5}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1920791
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1920792
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 1920793
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1920794
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1920795
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1920796
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1920797
    :cond_1
    invoke-virtual {v3}, Ljava/util/LinkedList;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1920798
    :goto_1
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1920799
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1920800
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v8, v0

    .line 1920801
    :goto_3
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1920802
    :goto_4
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->B()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 1920803
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1920804
    :goto_5
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 1920805
    :goto_6
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v9, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    const/16 v0, 0x7d5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v1, p0, LX/CcI;->a:LX/CcJ;

    iget-object v1, v1, LX/CcJ;->a:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v0, v12, v13}, LX/CcO;->a$redex0(LX/CcO;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    new-instance v0, LX/CcH;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, LX/CcH;-><init>(LX/CcI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v11, v0}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1920806
    return-void

    :cond_2
    move-object v5, v1

    .line 1920807
    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    .line 1920808
    goto/16 :goto_2

    .line 1920809
    :cond_4
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1920810
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 1920811
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->V()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyLegacyContainerStoryModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_7
    move-object v8, v0

    goto/16 :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_7

    :cond_6
    move-object v8, v1

    move-object v7, v1

    .line 1920812
    goto/16 :goto_3

    :cond_7
    move-object v2, v1

    .line 1920813
    goto/16 :goto_4

    :cond_8
    move-object v4, v1

    .line 1920814
    goto/16 :goto_5

    :cond_9
    move-object v6, v1

    .line 1920815
    goto :goto_6
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1920816
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0819de

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1920817
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1920818
    iget-object v0, p0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0819de

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1920819
    return-void
.end method
