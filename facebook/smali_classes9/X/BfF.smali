.class public LX/BfF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BfC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BfC",
        "<",
        "LX/97f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/Bez;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1805811
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/BfF;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/Bez;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/Bez;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805808
    iput-object p1, p0, LX/BfF;->b:LX/0Or;

    .line 1805809
    iput-object p2, p0, LX/BfF;->c:LX/Bez;

    .line 1805810
    return-void
.end method


# virtual methods
.method public final a()LX/BeD;
    .locals 1

    .prologue
    .line 1805782
    sget-object v0, LX/BeD;->HOURS_PICKER:LX/BeD;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1805789
    check-cast p1, LX/97f;

    .line 1805790
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/BfF;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1805791
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->SUGGEST_EDITS_HOURS_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1805792
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1805793
    sget-object v1, LX/BfF;->a:Ljava/util/Map;

    invoke-interface {p1}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1805794
    const-string v3, "extra_hours_selected_option"

    sget-object v1, LX/BfF;->a:Ljava/util/Map;

    invoke-interface {p1}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1805795
    :cond_0
    invoke-interface {p1}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v1, v3, :cond_3

    .line 1805796
    invoke-static {p1}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v3

    .line 1805797
    if-nez v3, :cond_1

    move-object v1, v2

    .line 1805798
    :goto_0
    move-object v1, v1

    .line 1805799
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1805800
    return-object v0

    .line 1805801
    :cond_1
    new-instance p0, Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-direct {p0}, Lcom/facebook/crowdsourcing/helper/HoursData;-><init>()V

    .line 1805802
    const/4 v1, 0x1

    :goto_1
    const/4 p2, 0x7

    if-gt v1, p2, :cond_2

    .line 1805803
    invoke-static {v1, v3}, LX/Bez;->a(ILX/97c;)LX/Bex;

    move-result-object p2

    invoke-virtual {p0, v1, p2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(ILX/Bex;)V

    .line 1805804
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1805805
    :cond_2
    const-string v1, "extra_hours_data"

    invoke-virtual {v2, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    move-object v1, v2

    .line 1805806
    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1805783
    check-cast p2, LX/97f;

    const/4 v2, -0x1

    .line 1805784
    const-string v0, "extra_hours_selected_option"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1805785
    if-ne v1, v2, :cond_0

    .line 1805786
    :goto_0
    return-object p2

    .line 1805787
    :cond_0
    const-string v0, "extra_hours_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData;

    .line 1805788
    invoke-static {p2, v1, v0}, LX/BgZ;->a(LX/97f;ILcom/facebook/crowdsourcing/helper/HoursData;)LX/97f;

    move-result-object p2

    goto :goto_0
.end method
