.class public LX/CUN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ds;


# instance fields
.field private final a:LX/6tK;


# direct methods
.method public constructor <init>(LX/6tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1896497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896498
    iput-object p1, p0, LX/CUN;->a:LX/6tK;

    .line 1896499
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1896500
    iget-object v0, p0, LX/CUN;->a:LX/6tK;

    invoke-virtual {v0, p1}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1896501
    iget-object v0, p0, LX/CUN;->a:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1896502
    sget-object v0, LX/CUM;->a:[I

    invoke-virtual {p1}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1896503
    iget-object v0, p0, LX/CUN;->a:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1896504
    :pswitch_0
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;

    .line 1896505
    iget-object v1, v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1896506
    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e()LX/3Ab;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1896507
    const/4 v0, 0x0

    .line 1896508
    :goto_1
    move-object v0, v0

    .line 1896509
    goto :goto_0

    :cond_0
    new-instance v0, LX/CWB;

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e()LX/3Ab;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/CWB;-><init>(Ljava/lang/String;LX/3Ab;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
