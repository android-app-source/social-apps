.class public LX/AmS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final b:LX/1nG;

.field private final c:LX/0Zb;

.field private final d:LX/0gh;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/0Zb;LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710834
    iput-object p1, p0, LX/AmS;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1710835
    iput-object p2, p0, LX/AmS;->b:LX/1nG;

    .line 1710836
    iput-object p3, p0, LX/AmS;->c:LX/0Zb;

    .line 1710837
    iput-object p4, p0, LX/AmS;->d:LX/0gh;

    .line 1710838
    return-void
.end method

.method public static b(LX/0QB;)LX/AmS;
    .locals 5

    .prologue
    .line 1710839
    new-instance v4, LX/AmS;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v1

    check-cast v1, LX/1nG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v3

    check-cast v3, LX/0gh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/AmS;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1nG;LX/0Zb;LX/0gh;)V

    .line 1710840
    return-object v4
.end method


# virtual methods
.method public final a(LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1710841
    iget-object v0, p0, LX/AmS;->b:LX/1nG;

    invoke-interface {p1}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-interface {p1}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1710842
    if-nez v2, :cond_0

    .line 1710843
    :goto_0
    return-void

    .line 1710844
    :cond_0
    invoke-static {p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1710845
    invoke-static {p2, p3}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1710846
    :cond_1
    iget-object v0, p0, LX/AmS;->c:LX/0Zb;

    invoke-interface {v0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1710847
    iget-object v3, p0, LX/AmS;->d:LX/0gh;

    if-eqz p2, :cond_2

    .line 1710848
    iget-boolean v0, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v0

    .line 1710849
    if-eqz v0, :cond_2

    const-string v0, "tap_profile_pic_sponsored"

    :goto_1
    invoke-virtual {v3, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1710850
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1710851
    invoke-interface {p1}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-interface {p1}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    invoke-interface {v0}, LX/3Bf;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {p1}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v0, v6}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1710852
    iget-object v0, p0, LX/AmS;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0, v4, v2, v3, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto :goto_0

    .line 1710853
    :cond_2
    const-string v0, "tap_profile_pic"

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1710854
    goto :goto_2
.end method
