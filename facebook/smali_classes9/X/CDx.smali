.class public final LX/CDx;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CDx;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lcom/facebook/fig/components/newsfeed/FigProfileVideoComponentSpec;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1860334
    const/4 v0, 0x0

    sput-object v0, LX/CDx;->a:LX/CDx;

    .line 1860335
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CDx;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1860344
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1860345
    new-instance v0, Lcom/facebook/fig/components/newsfeed/FigProfileVideoComponentSpec;

    invoke-direct {v0}, Lcom/facebook/fig/components/newsfeed/FigProfileVideoComponentSpec;-><init>()V

    iput-object v0, p0, LX/CDx;->c:Lcom/facebook/fig/components/newsfeed/FigProfileVideoComponentSpec;

    .line 1860346
    return-void
.end method

.method public static declared-synchronized q()LX/CDx;
    .locals 2

    .prologue
    .line 1860340
    const-class v1, LX/CDx;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CDx;->a:LX/CDx;

    if-nez v0, :cond_0

    .line 1860341
    new-instance v0, LX/CDx;

    invoke-direct {v0}, LX/CDx;-><init>()V

    sput-object v0, LX/CDx;->a:LX/CDx;

    .line 1860342
    :cond_0
    sget-object v0, LX/CDx;->a:LX/CDx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1860343
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1860338
    invoke-static {}, LX/1dS;->b()V

    .line 1860339
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x47c2adcc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1860336
    invoke-static {p3, p4, p5}, LX/1oC;->a(IILX/1no;)V

    .line 1860337
    const/16 v1, 0x1f

    const v2, -0x595be5dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1860323
    const/4 v3, 0x0

    .line 1860324
    new-instance v0, LX/7LN;

    invoke-direct {v0}, LX/7LN;-><init>()V

    sget-object v1, LX/04D;->ACTOR_PROFILE_VIDEO:LX/04D;

    .line 1860325
    iput-object v1, v0, LX/7LN;->a:LX/04D;

    .line 1860326
    move-object v0, v0

    .line 1860327
    sget-object v1, LX/04G;->OTHERS:LX/04G;

    .line 1860328
    iput-object v1, v0, LX/7LN;->b:LX/04G;

    .line 1860329
    move-object v0, v0

    .line 1860330
    iput-boolean v3, v0, LX/7LN;->d:Z

    .line 1860331
    move-object v0, v0

    .line 1860332
    const/4 v1, 0x2

    new-array v1, v1, [LX/2oy;

    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v3

    const/4 v2, 0x1

    new-instance v3, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object p0, Lcom/facebook/fig/components/newsfeed/FigProfileVideoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, p1, p0}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/7LN;->a([LX/2oy;)LX/7LN;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/7LN;->a(Landroid/content/Context;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    move-object v0, v0

    .line 1860333
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 5

    .prologue
    .line 1860347
    check-cast p2, LX/CDw;

    .line 1860348
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1860349
    iget-object v0, p2, LX/CDw;->a:Ljava/lang/String;

    iget-object v2, p2, LX/CDw;->b:Ljava/lang/String;

    .line 1860350
    new-instance v3, LX/2oE;

    invoke-direct {v3}, LX/2oE;-><init>()V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1860351
    iput-object v4, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 1860352
    move-object v3, v3

    .line 1860353
    sget-object v4, LX/097;->FROM_STREAM:LX/097;

    .line 1860354
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 1860355
    move-object v3, v3

    .line 1860356
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    .line 1860357
    new-instance v4, LX/2oH;

    invoke-direct {v4}, LX/2oH;-><init>()V

    invoke-virtual {v4, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    const/4 v4, 0x1

    .line 1860358
    iput-boolean v4, v3, LX/2oH;->g:Z

    .line 1860359
    move-object v3, v3

    .line 1860360
    invoke-virtual {v3}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 1860361
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    .line 1860362
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1860363
    const-string p0, "CoverImageParamsKey"

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object p1

    invoke-virtual {v4, p0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1860364
    :cond_0
    new-instance p0, LX/2pZ;

    invoke-direct {p0}, LX/2pZ;-><init>()V

    .line 1860365
    iput-object v3, p0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1860366
    move-object v3, p0

    .line 1860367
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v3

    invoke-virtual {v3}, LX/2pZ;->b()LX/2pa;

    move-result-object v3

    .line 1860368
    iput-object v3, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1860369
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1860370
    check-cast v0, LX/2pa;

    iput-object v0, p2, LX/CDw;->c:LX/2pa;

    .line 1860371
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1860372
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1860322
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1860318
    check-cast p3, LX/CDw;

    .line 1860319
    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, p3, LX/CDw;->c:LX/2pa;

    .line 1860320
    invoke-virtual {p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1860321
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1860307
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1860315
    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1860316
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1860317
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1860312
    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1860313
    sget-object p0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1860314
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1860309
    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1860310
    sget-object p0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1860311
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1860308
    const/16 v0, 0xf

    return v0
.end method
