.class public LX/BRC;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1783591
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1783592
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/BQo;LX/B37;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;
    .locals 18

    .prologue
    .line 1783593
    new-instance v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-static/range {p0 .. p0}, LX/BRP;->a(LX/0QB;)LX/BRP;

    move-result-object v2

    check-cast v2, LX/BRP;

    const-class v3, LX/9fo;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/9fo;

    const-class v4, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const-class v4, LX/BRM;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BRM;

    invoke-static/range {p0 .. p0}, LX/8GV;->a(LX/0QB;)LX/8GV;

    move-result-object v8

    check-cast v8, LX/8GV;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->a(LX/0QB;)Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    move-result-object v10

    check-cast v10, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    move-object/from16 v4, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move-object/from16 v16, p6

    move-object/from16 v17, p7

    invoke-direct/range {v1 .. v17}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;-><init>(LX/BRP;LX/9fo;Ljava/lang/String;Landroid/content/Context;LX/03V;LX/BRM;LX/8GV;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;Ljava/util/concurrent/Executor;LX/BQo;LX/B37;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    .line 1783594
    const/16 v2, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x5cb

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2e5c

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/0Or;LX/0Or;LX/0Or;)V

    .line 1783595
    return-object v1
.end method
