.class public final LX/Bbm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 0

    .prologue
    .line 1801079
    iput-object p1, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1801058
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801059
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object v2, LX/0ig;->aA:LX/0ih;

    const-string v3, "friends_fetched"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1801060
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1801061
    if-eqz p1, :cond_0

    .line 1801062
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801063
    if-eqz v0, :cond_0

    .line 1801064
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801065
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1801066
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801067
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1801068
    :cond_0
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    invoke-virtual {v0}, LX/8yG;->d()V

    .line 1801069
    :goto_0
    return-void

    .line 1801070
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801071
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;

    .line 1801072
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1801073
    :cond_2
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    invoke-virtual {v0}, LX/8yG;->d()V

    goto :goto_0

    .line 1801074
    :cond_3
    iget-object v1, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;

    move-result-object v0

    .line 1801075
    invoke-static {v1, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a$redex0(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;)V

    .line 1801076
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801077
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object v2, LX/0ig;->aA:LX/0ih;

    const-string v3, "friend_list_loaded"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1801078
    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1801055
    iget-object v0, p0, LX/Bbm;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801056
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object p0, LX/0ig;->aA:LX/0ih;

    const-string p1, "friend_fetch_failed"

    invoke-virtual {v1, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1801057
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801054
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bbm;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
