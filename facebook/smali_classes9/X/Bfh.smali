.class public final LX/Bfh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/android/maps/model/LatLng;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic c:LX/Bfi;


# direct methods
.method public constructor <init>(LX/Bfi;ZLcom/facebook/android/maps/model/LatLng;)V
    .locals 0

    .prologue
    .line 1806419
    iput-object p1, p0, LX/Bfh;->c:LX/Bfi;

    iput-boolean p2, p0, LX/Bfh;->a:Z

    iput-object p3, p0, LX/Bfh;->b:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 1806414
    check-cast p1, Lcom/facebook/android/maps/model/LatLng;

    check-cast p2, Lcom/facebook/android/maps/model/LatLng;

    const-wide/16 v6, 0x0

    .line 1806415
    iget-boolean v0, p0, LX/Bfh;->a:Z

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-object v2, p0, LX/Bfh;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v2, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p2, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-object v4, p0, LX/Bfh;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 1806416
    :goto_0
    cmpl-double v2, v0, v6

    if-lez v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1806417
    :cond_0
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-object v2, p0, LX/Bfh;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, v2, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p2, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-object v4, p0, LX/Bfh;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    goto :goto_0

    .line 1806418
    :cond_1
    cmpg-double v0, v0, v6

    if-gez v0, :cond_2

    const/4 v0, -0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
