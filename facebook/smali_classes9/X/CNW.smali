.class public final LX/CNW;
.super LX/1n6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n6",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/CNX;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1881845
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "imageProvider"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "imageTemplate"

    aput-object v2, v0, v1

    sput-object v0, LX/CNW;->b:[Ljava/lang/String;

    .line 1881846
    sput v3, LX/CNW;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1881847
    invoke-direct {p0}, LX/1n6;-><init>()V

    .line 1881848
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/CNW;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CNW;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CNW;LX/1De;LX/CNX;)V
    .locals 1

    .prologue
    .line 1881849
    invoke-super {p0, p1}, LX/1n6;->a(LX/1De;)V

    .line 1881850
    iput-object p2, p0, LX/CNW;->a:LX/CNX;

    .line 1881851
    iget-object v0, p0, LX/CNW;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1881852
    return-void
.end method


# virtual methods
.method public final a(LX/CNb;)LX/CNW;
    .locals 2

    .prologue
    .line 1881853
    iget-object v0, p0, LX/CNW;->a:LX/CNX;

    iput-object p1, v0, LX/CNX;->b:LX/CNb;

    .line 1881854
    iget-object v0, p0, LX/CNW;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1881855
    return-object p0
.end method

.method public final a(LX/CO8;)LX/CNW;
    .locals 2

    .prologue
    .line 1881856
    iget-object v0, p0, LX/CNW;->a:LX/CNX;

    iput-object p1, v0, LX/CNX;->a:LX/CO8;

    .line 1881857
    iget-object v0, p0, LX/CNW;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1881858
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1881859
    invoke-super {p0}, LX/1n6;->a()V

    .line 1881860
    const/4 v0, 0x0

    iput-object v0, p0, LX/CNW;->a:LX/CNX;

    .line 1881861
    sget-object v0, LX/CNY;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1881862
    return-void
.end method

.method public final b()LX/1dc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1881863
    iget-object v1, p0, LX/CNW;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CNW;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/CNW;->c:I

    if-ge v1, v2, :cond_2

    .line 1881864
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1881865
    :goto_0
    sget v2, LX/CNW;->c:I

    if-ge v0, v2, :cond_1

    .line 1881866
    iget-object v2, p0, LX/CNW;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1881867
    sget-object v2, LX/CNW;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1881868
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1881869
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881870
    :cond_2
    iget-object v0, p0, LX/CNW;->a:LX/CNX;

    .line 1881871
    invoke-virtual {p0}, LX/CNW;->a()V

    .line 1881872
    return-object v0
.end method
