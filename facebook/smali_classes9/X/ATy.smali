.class public LX/ATy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/1RW;

.field private final c:LX/7mW;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/concurrent/Executor;

.field public final f:LX/0ad;

.field private final g:Lcom/facebook/content/SecureContextHelper;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/75Q;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/8Ms;

.field public final j:LX/8Ln;


# direct methods
.method public constructor <init>(LX/0SG;LX/1RW;LX/7mW;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/8Ms;LX/8Ln;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/1RW;",
            "LX/7mW;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "LX/0ad;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/75Q;",
            ">;",
            "LX/8Ms;",
            "LX/8Ln;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1677462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677463
    iput-object p1, p0, LX/ATy;->a:LX/0SG;

    .line 1677464
    iput-object p2, p0, LX/ATy;->b:LX/1RW;

    .line 1677465
    iput-object p3, p0, LX/ATy;->c:LX/7mW;

    .line 1677466
    iput-object p4, p0, LX/ATy;->d:Landroid/content/Context;

    .line 1677467
    iput-object p5, p0, LX/ATy;->e:Ljava/util/concurrent/Executor;

    .line 1677468
    iput-object p6, p0, LX/ATy;->f:LX/0ad;

    .line 1677469
    iput-object p7, p0, LX/ATy;->g:Lcom/facebook/content/SecureContextHelper;

    .line 1677470
    iput-object p8, p0, LX/ATy;->h:LX/0Ot;

    .line 1677471
    iput-object p9, p0, LX/ATy;->i:LX/8Ms;

    .line 1677472
    iput-object p10, p0, LX/ATy;->j:LX/8Ln;

    .line 1677473
    return-void
.end method

.method private static a(LX/ATw;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1677454
    iget-object v0, p0, LX/ATw;->b:LX/0Px;

    if-eqz v0, :cond_0

    .line 1677455
    iget-object v3, p0, LX/ATw;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677456
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1677457
    iget-object v5, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v5

    .line 1677458
    sget-object v5, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v5, :cond_1

    .line 1677459
    add-int/lit8 v0, v1, 0x1

    .line 1677460
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1677461
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static b(LX/ATw;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1677446
    iget-object v0, p0, LX/ATw;->b:LX/0Px;

    if-eqz v0, :cond_0

    .line 1677447
    iget-object v3, p0, LX/ATw;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677448
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1677449
    iget-object v5, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v5

    .line 1677450
    sget-object v5, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v5, :cond_1

    .line 1677451
    add-int/lit8 v0, v1, 0x1

    .line 1677452
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1677453
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/ATy;
    .locals 14

    .prologue
    .line 1677380
    new-instance v0, LX/ATy;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v2

    check-cast v2, LX/1RW;

    invoke-static {p0}, LX/7mW;->a(LX/0QB;)LX/7mW;

    move-result-object v3

    check-cast v3, LX/7mW;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const/16 v8, 0x2ecc

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/8Ms;->a(LX/0QB;)LX/8Ms;

    move-result-object v9

    check-cast v9, LX/8Ms;

    .line 1677381
    new-instance v13, LX/8Ln;

    const-class v10, Landroid/content/Context;

    invoke-interface {p0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v13, v10, v11, v12}, LX/8Ln;-><init>(Landroid/content/Context;LX/0ad;Lcom/facebook/content/SecureContextHelper;)V

    .line 1677382
    move-object v10, v13

    .line 1677383
    check-cast v10, LX/8Ln;

    invoke-direct/range {v0 .. v10}, LX/ATy;-><init>(LX/0SG;LX/1RW;LX/7mW;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/8Ms;LX/8Ln;)V

    .line 1677384
    return-object v0
.end method


# virtual methods
.method public final a(LX/8K5;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/ATw;)V
    .locals 14
    .param p5    # Lcom/facebook/composer/minutiae/model/MinutiaeObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/ipc/composer/model/ComposerStickerData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8K5;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "LX/ATw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1677426
    move-object/from16 v0, p10

    iget-object v2, v0, LX/ATw;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    if-eqz v2, :cond_1

    .line 1677427
    move-object/from16 v0, p10

    iget-object v2, v0, LX/ATw;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-static {v2}, LX/ATz;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v4, v2

    .line 1677428
    :goto_0
    invoke-static/range {p9 .. p9}, LX/ATz;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v12

    .line 1677429
    invoke-static/range {p8 .. p8}, LX/ATz;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v11

    .line 1677430
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, LX/23u;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;

    move-result-object v2

    invoke-static/range {p4 .. p4}, LX/ATz;->b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)LX/23u;

    move-result-object v3

    if-eqz p5, :cond_3

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->g()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)LX/23u;

    move-result-object v3

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static/range {p6 .. p6}, LX/ATz;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)LX/23u;

    move-result-object v3

    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->USER:LX/2rw;

    if-ne v2, v5, :cond_5

    invoke-static/range {p7 .. p7}, LX/ATz;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    :goto_3
    invoke-virtual {v3, v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/23u;->f(LX/0Px;)LX/23u;

    move-result-object v2

    iget-object v3, p0, LX/ATy;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, LX/23u;->a(J)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/23u;

    move-result-object v2

    invoke-virtual {v2, v12}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLSticker;)LX/23u;

    move-result-object v5

    .line 1677431
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/ATy;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    move v3, v2

    .line 1677432
    :goto_4
    invoke-virtual {v5}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v0, v2}, LX/7mj;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/7mj;

    move-result-object v5

    .line 1677433
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, LX/ATy;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1677434
    iget-object v2, p0, LX/ATy;->c:LX/7mW;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/7mV;->c(Ljava/lang/String;)LX/7mi;

    move-result-object v2

    check-cast v2, LX/7mj;

    .line 1677435
    invoke-virtual {v2}, LX/7mj;->d()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, LX/7mj;->a(J)Z

    .line 1677436
    :cond_0
    iget-object v2, p0, LX/ATy;->c:LX/7mW;

    invoke-virtual {v2, v5}, LX/7mW;->a(LX/7mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1677437
    new-instance v6, LX/ATx;

    move-object/from16 v0, p2

    invoke-direct {v6, p0, v0, v3, v5}, LX/ATx;-><init>(LX/ATy;Ljava/lang/String;ZLX/7mj;)V

    iget-object v3, p0, LX/ATy;->e:Ljava/util/concurrent/Executor;

    invoke-static {v2, v6, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1677438
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    iget-object v3, p1, LX/8K5;->analyticsName:Ljava/lang/String;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    invoke-static/range {p10 .. p10}, LX/ATy;->a(LX/ATw;)I

    move-result v7

    invoke-static/range {p10 .. p10}, LX/ATy;->b(LX/ATw;)I

    move-result v8

    invoke-virtual/range {p4 .. p4}, LX/0Px;->size()I

    move-result v9

    if-eqz p5, :cond_7

    const/4 v10, 0x1

    :goto_5
    if-eqz v11, :cond_8

    const/4 v11, 0x1

    :goto_6
    if-eqz v12, :cond_9

    const/4 v12, 0x1

    :goto_7
    move-object/from16 v0, p10

    iget-object v4, v0, LX/ATw;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    if-eqz v4, :cond_a

    const/4 v13, 0x1

    :goto_8
    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v13}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;IIIIIZZZZ)V

    .line 1677439
    return-void

    .line 1677440
    :cond_1
    move-object/from16 v0, p10

    iget-object v2, v0, LX/ATw;->b:LX/0Px;

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1677441
    move-object/from16 v0, p10

    iget-object v2, v0, LX/ATw;->b:LX/0Px;

    invoke-static {v2}, LX/ATz;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_0

    .line 1677442
    :cond_2
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_0

    .line 1677443
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1677444
    :cond_6
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_4

    .line 1677445
    :cond_7
    const/4 v10, 0x0

    goto :goto_5

    :cond_8
    const/4 v11, 0x0

    goto :goto_6

    :cond_9
    const/4 v12, 0x0

    goto :goto_7

    :cond_a
    const/4 v13, 0x0

    goto :goto_8
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1677425
    iget-object v0, p0, LX/ATy;->c:LX/7mW;

    invoke-virtual {v0, p1}, LX/7mV;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZLcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/share/model/ComposerAppAttribution;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0Px;)Z
    .locals 10
    .param p4    # Lcom/facebook/ipc/composer/intent/ComposerShareParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/ipc/composer/model/ComposerStickerData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Z",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1677385
    iget-object v2, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v2, v3, :cond_0

    iget-object v2, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->USER:LX/2rw;

    if-eq v2, v3, :cond_0

    .line 1677386
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->NON_STATUS:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v4}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1677387
    const/4 v2, 0x0

    .line 1677388
    :goto_0
    return v2

    .line 1677389
    :cond_0
    if-eqz p3, :cond_1

    .line 1677390
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->FEED_ONLY:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677391
    const/4 v2, 0x0

    goto :goto_0

    .line 1677392
    :cond_1
    if-eqz p4, :cond_4

    iget-object v2, p4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_2

    invoke-static {p4}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v2, :cond_4

    iget-object v2, p4, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1677393
    :cond_3
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->RESHARE:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677394
    const/4 v2, 0x0

    goto :goto_0

    .line 1677395
    :cond_4
    if-eqz p5, :cond_5

    .line 1677396
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->ALBUM:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677397
    const/4 v2, 0x0

    goto :goto_0

    .line 1677398
    :cond_5
    if-eqz p6, :cond_6

    .line 1677399
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->APP_ATTRIBUTION:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677400
    const/4 v2, 0x0

    goto :goto_0

    .line 1677401
    :cond_6
    invoke-virtual/range {p9 .. p9}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_b

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677402
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/data/MediaData;->b()LX/4gQ;

    move-result-object v3

    sget-object v6, LX/4gQ;->Video:LX/4gQ;

    if-ne v3, v6, :cond_7

    invoke-virtual/range {p9 .. p9}, LX/0Px;->size()I

    move-result v3

    const/4 v6, 0x1

    if-le v3, v6, :cond_7

    iget-object v3, p0, LX/ATy;->f:LX/0ad;

    sget-short v6, LX/1aO;->A:S

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1677403
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->MULTI_MEDIA:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677404
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677405
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-static {v3}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1677406
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->CREATIVE_PHOTO_EDITING:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677407
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677408
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 1677409
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->CREATIVE_VIDEO_EDITING:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677410
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677411
    :cond_9
    iget-object v3, p0, LX/ATy;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/75Q;

    new-instance v6, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/LocalMediaData;->d()J

    move-result-wide v8

    invoke-direct {v6, v7, v8, v9}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v3, v6}, LX/75Q;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, LX/ATy;->f:LX/0ad;

    sget-short v3, LX/1aO;->B:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1677412
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->X_Y_TAGS:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677413
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677414
    :cond_a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1

    .line 1677415
    :cond_b
    invoke-virtual/range {p8 .. p8}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v2

    if-nez v2, :cond_c

    invoke-virtual/range {p8 .. p8}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_d

    :cond_c
    iget-object v2, p0, LX/ATy;->f:LX/0ad;

    sget-short v3, LX/1aO;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1677416
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->CHECK_IN:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677417
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677418
    :cond_d
    if-eqz p7, :cond_e

    iget-object v2, p0, LX/ATy;->f:LX/0ad;

    sget-short v3, LX/1aO;->b:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1677419
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->STICKER_POST:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677420
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677421
    :cond_e
    invoke-virtual/range {p9 .. p9}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_f

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/MediaData;->b()LX/4gQ;

    move-result-object v2

    sget-object v3, LX/4gQ;->Video:LX/4gQ;

    if-ne v2, v3, :cond_f

    iget-object v2, p0, LX/ATy;->f:LX/0ad;

    sget-short v3, LX/1aO;->c:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_f

    .line 1677422
    iget-object v2, p0, LX/ATy;->b:LX/1RW;

    sget-object v3, LX/8K4;->VIDEO_POST:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677423
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1677424
    :cond_f
    const/4 v2, 0x1

    goto/16 :goto_0
.end method
