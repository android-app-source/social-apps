.class public LX/C1g;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C1g",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842808
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1842809
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C1g;->b:LX/0Zi;

    .line 1842810
    iput-object p1, p0, LX/C1g;->a:LX/0Ot;

    .line 1842811
    return-void
.end method

.method public static a(LX/0QB;)LX/C1g;
    .locals 4

    .prologue
    .line 1842847
    const-class v1, LX/C1g;

    monitor-enter v1

    .line 1842848
    :try_start_0
    sget-object v0, LX/C1g;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842849
    sput-object v2, LX/C1g;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842850
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842851
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842852
    new-instance v3, LX/C1g;

    const/16 p0, 0x1e94

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C1g;-><init>(LX/0Ot;)V

    .line 1842853
    move-object v0, v3

    .line 1842854
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842855
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842856
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 11

    .prologue
    .line 1842832
    check-cast p2, LX/C1f;

    .line 1842833
    iget-object v0, p0, LX/C1g;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v1, p2, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C1f;->b:LX/1Pq;

    .line 1842834
    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    .line 1842835
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1842836
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842837
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v7

    .line 1842838
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, LX/0f8;

    invoke-static {v3, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0f8;

    .line 1842839
    invoke-interface {v3}, LX/0f8;->i()LX/0hE;

    move-result-object v10

    .line 1842840
    new-instance v3, LX/0AV;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v4

    .line 1842841
    new-instance v3, LX/0AW;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    invoke-direct {v3, v5}, LX/0AW;-><init>(LX/162;)V

    invoke-virtual {v3}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v5

    .line 1842842
    new-instance v3, LX/395;

    const/4 v6, 0x0

    move-object v8, v1

    invoke-direct/range {v3 .. v8}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1842843
    new-instance v4, LX/C1h;

    invoke-direct {v4, v0, v9, v2}, LX/C1h;-><init>(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;)V

    move-object v4, v4

    .line 1842844
    invoke-interface {v10, v4}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 1842845
    invoke-interface {v10, v3}, LX/0hE;->a(LX/395;)V

    .line 1842846
    return-void
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842831
    const v0, -0x7edcfdd3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842830
    const v0, -0x4bdba87c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1842823
    check-cast p2, LX/C1f;

    .line 1842824
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1842825
    iget-object v0, p0, LX/C1g;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v2, p2, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C1f;->b:LX/1Pq;

    invoke-virtual {v0, p1, v2, v3, v1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;LX/1np;)LX/1Dg;

    move-result-object v2

    .line 1842826
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1842827
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p2, LX/C1f;->c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1842828
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1842829
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1842812
    invoke-static {}, LX/1dS;->b()V

    .line 1842813
    iget v0, p1, LX/1dQ;->b:I

    .line 1842814
    sparse-switch v0, :sswitch_data_0

    .line 1842815
    :goto_0
    return-object v2

    .line 1842816
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1842817
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1842818
    check-cast v1, LX/C1f;

    .line 1842819
    iget-object p1, p0, LX/C1g;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object p2, v1, LX/C1f;->c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {p1, p2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1842820
    goto :goto_0

    .line 1842821
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1842822
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/C1g;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7edcfdd3 -> :sswitch_0
        -0x4bdba87c -> :sswitch_1
    .end sparse-switch
.end method
