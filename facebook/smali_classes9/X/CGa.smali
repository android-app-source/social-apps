.class public final LX/CGa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CGd;


# direct methods
.method public constructor <init>(LX/CGd;)V
    .locals 0

    .prologue
    .line 1865613
    iput-object p1, p0, LX/CGa;->a:LX/CGd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1865614
    const/4 v6, 0x0

    .line 1865615
    iget-object v0, p0, LX/CGa;->a:LX/CGd;

    iget-object v0, v0, LX/CGd;->c:LX/AR2;

    iget-object v1, p0, LX/CGa;->a:LX/CGd;

    .line 1865616
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1865617
    iget-object v2, p0, LX/CGa;->a:LX/CGd;

    .line 1865618
    iget-object v3, v2, LX/AQ9;->q:LX/ARN;

    move-object v2, v3

    .line 1865619
    invoke-virtual {v0, v1, v2}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v0

    .line 1865620
    iget-object v1, p0, LX/CGa;->a:LX/CGd;

    iget-object v1, v1, LX/CGd;->d:LX/AR4;

    iget-object v2, p0, LX/CGa;->a:LX/CGd;

    .line 1865621
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1865622
    invoke-virtual {v1, v2}, LX/AR4;->a(Ljava/lang/Object;)LX/AR3;

    move-result-object v1

    .line 1865623
    iget-object v2, p0, LX/CGa;->a:LX/CGd;

    iget-object v2, v2, LX/CGd;->e:LX/ARD;

    iget-object v3, p0, LX/CGa;->a:LX/CGd;

    .line 1865624
    invoke-virtual {v3}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    move-object v3, v4

    .line 1865625
    invoke-virtual {v2, v3, v1, v0}, LX/ARD;->a(Ljava/lang/Object;LX/AR3;LX/AR1;)LX/ARC;

    move-result-object v7

    .line 1865626
    invoke-virtual {v7}, LX/ARC;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    .line 1865627
    new-instance v0, Lcom/facebook/composer/publish/common/PollUploadParams;

    iget-wide v4, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    iget-object v3, p0, LX/CGa;->a:LX/CGd;

    iget-object v3, v3, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v3}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->b()LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/CGa;->a:LX/CGd;

    iget-object v4, v4, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v4}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->c()Z

    move-result v4

    iget-object v5, p0, LX/CGa;->a:LX/CGd;

    iget-object v5, v5, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v5}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->d()Z

    move-result v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/composer/publish/common/PollUploadParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZ)V

    .line 1865628
    new-instance v1, LX/5M9;

    invoke-virtual {v7}, LX/ARC;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v2

    invoke-direct {v1, v2}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1865629
    iput-object v0, v1, LX/5M9;->ae:Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 1865630
    move-object v0, v1

    .line 1865631
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1865632
    invoke-virtual {v7, v0, v6}, LX/ARC;->a(Lcom/facebook/composer/publish/common/PublishPostParams;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
