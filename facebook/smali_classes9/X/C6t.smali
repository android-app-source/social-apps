.class public final LX/C6t;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C6v;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C6u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C6v",
            "<TE;>.InlineSurveyQuestionComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C6v;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C6v;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1850537
    iput-object p1, p0, LX/C6t;->b:LX/C6v;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1850538
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "surveyQuestion"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "answerOptions"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "style"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "isFirstQuestion"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "questionState"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "acknowledgementState"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C6t;->c:[Ljava/lang/String;

    .line 1850539
    iput v3, p0, LX/C6t;->d:I

    .line 1850540
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C6t;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C6t;LX/1De;IILX/C6u;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C6v",
            "<TE;>.InlineSurveyQuestionComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1850533
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1850534
    iput-object p4, p0, LX/C6t;->a:LX/C6u;

    .line 1850535
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1850536
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850530
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->b:LX/0Px;

    .line 1850531
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850532
    return-object p0
.end method

.method public final a(LX/1Pq;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850527
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->i:LX/1Pq;

    .line 1850528
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850529
    return-object p0
.end method

.method public final a(LX/C6H;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C6H;",
            ")",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850524
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->f:LX/C6H;

    .line 1850525
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850526
    return-object p0
.end method

.method public final a(LX/C6a;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C6a;",
            ")",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850541
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->e:LX/C6a;

    .line 1850542
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850543
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850521
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850522
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850523
    return-object p0
.end method

.method public final a(Z)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850518
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-boolean p1, v0, LX/C6u;->d:Z

    .line 1850519
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850520
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1850514
    invoke-super {p0}, LX/1X5;->a()V

    .line 1850515
    const/4 v0, 0x0

    iput-object v0, p0, LX/C6t;->a:LX/C6u;

    .line 1850516
    iget-object v0, p0, LX/C6t;->b:LX/C6v;

    iget-object v0, v0, LX/C6v;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1850517
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850511
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->a:Ljava/lang/String;

    .line 1850512
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850513
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/C6t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/C6v",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1850498
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    iput-object p1, v0, LX/C6u;->c:Ljava/lang/String;

    .line 1850499
    iget-object v0, p0, LX/C6t;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1850500
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C6v;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1850501
    iget-object v1, p0, LX/C6t;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C6t;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C6t;->d:I

    if-ge v1, v2, :cond_2

    .line 1850502
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1850503
    :goto_0
    iget v2, p0, LX/C6t;->d:I

    if-ge v0, v2, :cond_1

    .line 1850504
    iget-object v2, p0, LX/C6t;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1850505
    iget-object v2, p0, LX/C6t;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1850506
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1850507
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1850508
    :cond_2
    iget-object v0, p0, LX/C6t;->a:LX/C6u;

    .line 1850509
    invoke-virtual {p0}, LX/C6t;->a()V

    .line 1850510
    return-object v0
.end method
