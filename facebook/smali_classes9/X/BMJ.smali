.class public LX/BMJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasComposerSessionId;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# static fields
.field private static final a:LX/1Qh;


# instance fields
.field private final b:LX/1Ns;

.field public final c:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1776993
    new-instance v0, LX/1Qg;

    invoke-direct {v0}, LX/1Qg;-><init>()V

    sput-object v0, LX/BMJ;->a:LX/1Qh;

    return-void
.end method

.method public constructor <init>(LX/1Ns;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776995
    iput-object p1, p0, LX/BMJ;->b:LX/1Ns;

    .line 1776996
    iput-object p2, p0, LX/BMJ;->c:LX/1Nq;

    .line 1776997
    return-void
.end method

.method private a(Landroid/view/View;LX/1RN;Lcom/facebook/productionprompts/model/ProductionPrompt;LX/B5o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "Lcom/facebook/productionprompts/model/ProductionPrompt;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1776998
    new-instance v0, LX/BMI;

    invoke-direct {v0, p0, p3}, LX/BMI;-><init>(LX/BMJ;Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    .line 1776999
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v1, p0, LX/BMJ;->b:LX/1Ns;

    sget-object v2, LX/BMJ;->a:LX/1Qh;

    invoke-virtual {v1, v2, v0}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v7

    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, p4

    check-cast v2, LX/B5p;

    .line 1777000
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1777001
    check-cast p4, LX/B5p;

    .line 1777002
    iget-object v3, p4, LX/B5p;->j:LX/0am;

    move-object v3, v3

    .line 1777003
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, v6

    move-object v2, v7

    move-object v4, p3

    .line 1777004
    const-class v6, Landroid/app/Activity;

    invoke-static {v1, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    .line 1777005
    iget-object v7, v3, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    const-string p0, "editorialPrompt"

    iget-object p1, v0, LX/BMJ;->c:LX/1Nq;

    invoke-static {v4, v3, v5}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object p1

    invoke-virtual {v2, v7, p0, p1, v6}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    .line 1777006
    return-void
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 0
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777007
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777008
    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1777009
    iget-object v1, v0, LX/B5p;->j:LX/0am;

    move-object v0, v1

    .line 1777010
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1777011
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1777012
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    check-cast v0, LX/1kW;

    .line 1777013
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1777014
    invoke-direct {p0, p1, p2, v0, p3}, LX/BMJ;->a(Landroid/view/View;LX/1RN;Lcom/facebook/productionprompts/model/ProductionPrompt;LX/B5o;)V

    .line 1777015
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1777016
    const/4 v0, 0x1

    return v0
.end method
