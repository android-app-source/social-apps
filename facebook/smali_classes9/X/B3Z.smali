.class public final LX/B3Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B3T;


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/category/CategoryBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserActivity;)V
    .locals 0

    .prologue
    .line 1740472
    iput-object p1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserActivity;B)V
    .locals 0

    .prologue
    .line 1740471
    invoke-direct {p0, p1}, LX/B3Z;-><init>(Lcom/facebook/heisman/category/CategoryBrowserActivity;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1740466
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1740467
    const-string v1, "clear_frame"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1740468
    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 1740469
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-virtual {v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->finish()V

    .line 1740470
    return-void
.end method

.method public final a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;I)V
    .locals 10

    .prologue
    .line 1740456
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1740457
    const-string v1, "heisman_profile_overlay_item"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1740458
    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v1, v1, Lcom/facebook/heisman/category/CategoryBrowserActivity;->v:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v1, v1, Lcom/facebook/heisman/category/CategoryBrowserActivity;->w:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1740459
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->p:LX/B4H;

    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v2, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->v:Ljava/lang/String;

    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    .line 1740460
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v5, v0

    .line 1740461
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v1 .. v9}, LX/B4H;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Landroid/content/Intent;

    move-result-object v1

    .line 1740462
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc35

    iget-object v3, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1740463
    :goto_0
    return-void

    .line 1740464
    :cond_0
    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 1740465
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-virtual {v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1740452
    new-instance v0, LX/B4N;

    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-static {v1}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->l(Lcom/facebook/heisman/category/CategoryBrowserActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "category_browser"

    invoke-direct {v0, v1, v2}, LX/B4N;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, LX/B4N;->c(Ljava/lang/String;)LX/B4N;

    move-result-object v0

    invoke-virtual {v0}, LX/B4N;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    move-result-object v0

    .line 1740453
    invoke-static {v0}, Lcom/facebook/heisman/category/SingleCategoryFragment;->a(Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)Lcom/facebook/heisman/category/SingleCategoryFragment;

    move-result-object v0

    .line 1740454
    iget-object v1, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2766

    const-string v3, "single_category_fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const-string v1, "single_category_transaction"

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1740455
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1740449
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->t:LX/0h5;

    if-eqz v0, :cond_0

    .line 1740450
    iget-object v0, p0, LX/B3Z;->a:Lcom/facebook/heisman/category/CategoryBrowserActivity;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->t:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1740451
    :cond_0
    return-void
.end method
