.class public interface abstract LX/CQM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CQJ;
.implements LX/CQK;
.implements LX/CQL;


# virtual methods
.method public abstract A()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()Z
.end method

.method public abstract C()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$PageTWEFragment$RedirectionInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract F()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$Photo320Fragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract G()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract H()Z
.end method

.method public abstract I()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract K()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract L()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract j()Z
.end method

.method public abstract jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract jI_()Z
.end method

.method public abstract k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLInterfaces$NearbyPagePlaceInfoFragment$Hours;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract m()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()Z
.end method

.method public abstract o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract w()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract z()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
