.class public LX/BYo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BYo;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/BYu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1796586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796587
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BYo;->c:Ljava/util/List;

    .line 1796588
    return-void
.end method


# virtual methods
.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BYo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1796598
    iget-object v0, p0, LX/BYo;->c:Ljava/util/List;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796597
    iget-object v0, p0, LX/BYo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796596
    iget-object v0, p0, LX/BYo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getRoot()LX/BYu;
    .locals 1

    .prologue
    .line 1796595
    iget-object v0, p0, LX/BYo;->d:LX/BYu;

    return-object v0
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796593
    iput-object p1, p0, LX/BYo;->b:Ljava/lang/String;

    .line 1796594
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796591
    iput-object p1, p0, LX/BYo;->a:Ljava/lang/String;

    .line 1796592
    return-void
.end method

.method public setRoot(LX/BYu;)V
    .locals 0

    .prologue
    .line 1796589
    iput-object p1, p0, LX/BYo;->d:LX/BYu;

    .line 1796590
    return-void
.end method
