.class public LX/BJH;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BJG;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1771753
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1771754
    return-void
.end method


# virtual methods
.method public final a(LX/BK9;LX/BK5;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)LX/BJG;
    .locals 13

    .prologue
    .line 1771755
    new-instance v0, LX/BJG;

    invoke-static {p0}, LX/9bY;->a(LX/0QB;)LX/9bY;

    move-result-object v6

    check-cast v6, LX/9bY;

    invoke-static {p0}, LX/9at;->b(LX/0QB;)LX/9at;

    move-result-object v7

    check-cast v7, LX/9at;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static {p0}, LX/9bq;->a(LX/0QB;)LX/9bq;

    move-result-object v11

    check-cast v11, LX/9bq;

    const-class v1, LX/9b2;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/9b2;

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v12}, LX/BJG;-><init>(LX/BK9;LX/BK5;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;LX/9bY;LX/9at;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0Sh;LX/9bq;LX/9b2;)V

    .line 1771756
    return-object v0
.end method
