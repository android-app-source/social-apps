.class public LX/CA6;
.super LX/CA5;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/widget/ProgressBar;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:LX/5ON;

.field public j:Lcom/facebook/fbui/glyph/GlyphView;

.field public k:Lcom/facebook/composer/publish/common/PendingStory;

.field public l:LX/C98;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    .line 1854929
    invoke-direct {p0, p1}, LX/CA5;-><init>(Landroid/content/Context;)V

    .line 1854930
    const-class v0, LX/CA6;

    invoke-static {v0, p0}, LX/CA6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1854931
    const v0, 0x7f031551

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1854932
    const v0, 0x7f0d0008

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    .line 1854933
    iget-object v0, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1854934
    const v0, 0x7f0d2ff3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1854935
    const v0, 0x7f0d2ff4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CA6;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1854936
    iget-object v0, p0, LX/CA6;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1854937
    const v0, 0x7f0d2ff5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1854938
    iget-object v0, p0, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1854939
    const/4 p1, 0x0

    .line 1854940
    iget-object v0, p0, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/CA6;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081a38

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1854941
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CA6;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v2

    check-cast v2, LX/0qn;

    invoke-static {p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iput-object v1, p1, LX/CA6;->a:LX/0SG;

    iput-object v2, p1, LX/CA6;->b:LX/0qn;

    iput-object p0, p1, LX/CA6;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1854925
    iget-object v0, p0, LX/CA6;->i:LX/5ON;

    if-eqz v0, :cond_0

    .line 1854926
    iget-object v0, p0, LX/CA6;->i:LX/5ON;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1854927
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/CA6;->setVisibility(I)V

    .line 1854928
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1854911
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    if-nez v0, :cond_0

    .line 1854912
    iget-object v0, p0, LX/CA6;->c:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    iput-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1854913
    :cond_0
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    if-nez v0, :cond_2

    .line 1854914
    :cond_1
    :goto_0
    return-void

    .line 1854915
    :cond_2
    iget-object v0, p0, LX/CA6;->b:LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_3

    .line 1854916
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    iget-object v1, p0, LX/CA6;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->b(J)V

    .line 1854917
    :cond_3
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    iget-object v1, p0, LX/CA6;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v0

    .line 1854918
    invoke-virtual {p0, v0}, LX/CA5;->setProgress(I)V

    .line 1854919
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/CA6;->d:LX/0TF;

    if-eqz v0, :cond_4

    .line 1854920
    iget-object v0, p0, LX/CA6;->d:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1854921
    iput-object v4, p0, LX/CA6;->d:LX/0TF;

    goto :goto_0

    .line 1854922
    :cond_4
    iget-object v0, p0, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CA6;->e:LX/0TF;

    if-eqz v0, :cond_1

    .line 1854923
    iget-object v0, p0, LX/CA6;->e:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1854924
    iput-object v4, p0, LX/CA6;->e:LX/0TF;

    goto :goto_0
.end method

.method public setCallbackOnProgressComplete(LX/0TF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1854909
    iput-object p1, p0, LX/CA6;->d:LX/0TF;

    .line 1854910
    return-void
.end method

.method public setCallbackOnProgressStarted(LX/0TF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1854907
    iput-object p1, p0, LX/CA6;->e:LX/0TF;

    .line 1854908
    return-void
.end method

.method public setProgress(I)V
    .locals 5

    .prologue
    .line 1854894
    iget-object v0, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1854895
    const/high16 v0, 0x42c80000    # 100.0f

    int-to-float v1, p1

    mul-float/2addr v0, v1

    iget-object v1, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1854896
    invoke-virtual {p0}, LX/CA6;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081a39

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1854897
    iget-object v1, p0, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1854898
    return-void
.end method

.method public setStoryIsWaitingForWifi(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1854899
    if-eqz p1, :cond_0

    .line 1854900
    iget-object v0, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1854901
    iget-object v0, p0, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1854902
    iget-object v0, p0, LX/CA6;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1854903
    :goto_0
    return-void

    .line 1854904
    :cond_0
    iget-object v0, p0, LX/CA6;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1854905
    iget-object v0, p0, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1854906
    iget-object v0, p0, LX/CA6;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
