.class public LX/AlY;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/1po;

.field public final synthetic o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1709784
    iput-object p1, p0, LX/AlY;->o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1709785
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1709786
    const v0, 0x7f0d0883

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AlY;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1709787
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/1po;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1709788
    iput-object p1, p0, LX/AlY;->m:Landroid/net/Uri;

    .line 1709789
    iput-object p2, p0, LX/AlY;->n:LX/1po;

    .line 1709790
    iget-object v0, p0, LX/AlY;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AlY;->o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1709791
    if-eqz p1, :cond_0

    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    if-nez v2, :cond_1

    .line 1709792
    :cond_0
    const/4 v2, 0x0

    .line 1709793
    :goto_0
    move-object v1, v2

    .line 1709794
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1709795
    iget-object v0, p0, LX/AlY;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1709796
    return-void

    .line 1709797
    :cond_1
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1709798
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1aZ;

    goto :goto_0

    .line 1709799
    :cond_2
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    iget-object p2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->j:LX/1o9;

    invoke-static {v1, p1, p2}, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->a(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/net/Uri;LX/1o9;)LX/1aZ;

    move-result-object p2

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709800
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1aZ;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2c0b2454

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1709801
    iget-object v0, p0, LX/AlY;->m:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1709802
    iget-object v0, p0, LX/AlY;->o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->n:Ljava/lang/String;

    invoke-static {v0}, LX/B5l;->a(Ljava/lang/String;)LX/B5p;

    move-result-object v0

    .line 1709803
    :goto_0
    iget-object v2, p0, LX/AlY;->o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1709804
    const/4 v3, 0x1

    move v3, v3

    .line 1709805
    if-eqz v3, :cond_1

    .line 1709806
    iget-object v3, v2, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BMP;

    iget-object v4, v2, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->s:LX/1RN;

    invoke-virtual {v3, p1, v4, v0}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1709807
    :goto_1
    const v0, 0x123cc3ed

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1709808
    :cond_0
    iget-object v0, p0, LX/AlY;->o:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->n:Ljava/lang/String;

    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v2

    iget-object v3, p0, LX/AlY;->n:LX/1po;

    iget-object v4, p0, LX/AlY;->m:Landroid/net/Uri;

    .line 1709809
    new-instance v5, LX/B5n;

    invoke-direct {v5}, LX/B5n;-><init>()V

    .line 1709810
    iput-object v0, v5, LX/B5n;->a:Ljava/lang/String;

    .line 1709811
    move-object v5, v5

    .line 1709812
    invoke-virtual {v5, v0}, LX/B5n;->b(Ljava/lang/String;)LX/B5n;

    move-result-object v5

    .line 1709813
    iput v2, v5, LX/B5n;->c:I

    .line 1709814
    move-object v5, v5

    .line 1709815
    iput-object v3, v5, LX/B5n;->d:LX/1po;

    .line 1709816
    move-object v5, v5

    .line 1709817
    iput-object v4, v5, LX/B5n;->e:Landroid/net/Uri;

    .line 1709818
    move-object v5, v5

    .line 1709819
    sget-object v6, LX/B5m;->PHOTO_REMINDER_TAP_ON_MEDIA:LX/B5m;

    .line 1709820
    iput-object v6, v5, LX/B5n;->b:LX/B5m;

    .line 1709821
    move-object v5, v5

    .line 1709822
    invoke-virtual {v5}, LX/B5n;->a()LX/B5p;

    move-result-object v5

    move-object v0, v5

    .line 1709823
    goto :goto_0

    .line 1709824
    :cond_1
    iget-object v3, v2, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->d:LX/Alh;

    new-instance v4, LX/1kJ;

    new-instance v5, LX/1lR;

    iget-object p0, v2, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-direct {v5, p0}, LX/1lR;-><init>(Ljava/util/List;)V

    invoke-direct {v4, v5}, LX/1kJ;-><init>(LX/1lR;)V

    .line 1709825
    new-instance v5, LX/32d;

    invoke-direct {v5}, LX/32d;-><init>()V

    invoke-virtual {v5, v4}, LX/32d;->a(LX/1kK;)LX/32d;

    move-result-object v5

    sget-object p0, LX/1lP;->a:LX/1lP;

    invoke-virtual {v5, p0}, LX/32d;->a(LX/1lP;)LX/32d;

    move-result-object v5

    invoke-virtual {v5}, LX/32d;->a()LX/1RN;

    move-result-object v5

    move-object v4, v5

    .line 1709826
    invoke-virtual {v3, p1, v4, v0}, LX/Alh;->a(Landroid/view/View;LX/1RN;LX/B5p;)V

    goto :goto_1
.end method
