.class public final LX/CYo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1912757
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1912758
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912759
    :goto_0
    return v1

    .line 1912760
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912761
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1912762
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1912763
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1912765
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1912766
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1912767
    :cond_2
    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1912768
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1912769
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 1912770
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912771
    :goto_2
    move v2, v4

    .line 1912772
    goto :goto_1

    .line 1912773
    :cond_3
    const-string v5, "url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1912774
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1912775
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1912776
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1912777
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1912778
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1912779
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1912780
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1912781
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1912782
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912783
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_6

    if-eqz v10, :cond_6

    .line 1912784
    const-string v11, "height"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1912785
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_3

    .line 1912786
    :cond_7
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1912787
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 1912788
    :cond_8
    const-string v11, "width"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1912789
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v7, v2

    move v2, v5

    goto :goto_3

    .line 1912790
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1912791
    :cond_a
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1912792
    if-eqz v6, :cond_b

    .line 1912793
    invoke-virtual {p1, v4, v9, v4}, LX/186;->a(III)V

    .line 1912794
    :cond_b
    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 1912795
    if-eqz v2, :cond_c

    .line 1912796
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7, v4}, LX/186;->a(III)V

    .line 1912797
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_d
    move v2, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1912798
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912799
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912800
    if-eqz v0, :cond_0

    .line 1912801
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912802
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912803
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912804
    if-eqz v0, :cond_4

    .line 1912805
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912806
    const/4 p3, 0x0

    .line 1912807
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912808
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 1912809
    if-eqz v1, :cond_1

    .line 1912810
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912811
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1912812
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1912813
    if-eqz v1, :cond_2

    .line 1912814
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912815
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912816
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 1912817
    if-eqz v1, :cond_3

    .line 1912818
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912819
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1912820
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912821
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912822
    if-eqz v0, :cond_5

    .line 1912823
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912825
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912826
    return-void
.end method
