.class public final LX/C9U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/C9Y;


# direct methods
.method public constructor <init>(LX/C9Y;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1854143
    iput-object p1, p0, LX/C9U;->b:LX/C9Y;

    iput-object p2, p0, LX/C9U;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x104f75e4

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1854144
    new-instance v1, LX/5OM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1854145
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1854146
    iget-object v3, p0, LX/C9U;->b:LX/C9Y;

    iget-object v4, p0, LX/C9U;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1854147
    iget-object v6, v3, LX/C9Y;->h:LX/0ad;

    sget-short v7, LX/1aO;->L:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1854148
    :goto_0
    iget-object v3, p0, LX/C9U;->b:LX/C9Y;

    iget-object v4, p0, LX/C9U;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    .line 1854149
    iget-object v6, v3, LX/C9Y;->h:LX/0ad;

    sget-short v7, LX/1aO;->N:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1854150
    :goto_1
    iget-object v3, p0, LX/C9U;->b:LX/C9Y;

    iget-object v4, p0, LX/C9U;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1854151
    const v6, 0x7f081a5e

    invoke-virtual {v2, v6}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v6

    new-instance v7, LX/C9X;

    invoke-direct {v7, v3, v4}, LX/C9X;-><init>(LX/C9Y;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v6, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1854152
    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1854153
    const/4 v2, 0x0

    .line 1854154
    iput-boolean v2, v1, LX/0ht;->e:Z

    .line 1854155
    invoke-virtual {v1}, LX/0ht;->d()V

    .line 1854156
    const v1, 0x3f76d6c2

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1854157
    :cond_0
    const v6, 0x7f081a5d

    invoke-virtual {v2, v6}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v6

    new-instance v7, LX/C9V;

    invoke-direct {v7, v3, v4}, LX/C9V;-><init>(LX/C9Y;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v6, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1854158
    :cond_1
    const v6, 0x7f081a5c

    invoke-virtual {v2, v6}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v6

    new-instance v7, LX/C9W;

    invoke-direct {v7, v3, v4}, LX/C9W;-><init>(LX/C9Y;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1
.end method
