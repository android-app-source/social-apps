.class public LX/C6n;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C6o;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C6n",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C6o;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850267
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850268
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C6n;->b:LX/0Zi;

    .line 1850269
    iput-object p1, p0, LX/C6n;->a:LX/0Ot;

    .line 1850270
    return-void
.end method

.method public static a(LX/0QB;)LX/C6n;
    .locals 4

    .prologue
    .line 1850271
    const-class v1, LX/C6n;

    monitor-enter v1

    .line 1850272
    :try_start_0
    sget-object v0, LX/C6n;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850273
    sput-object v2, LX/C6n;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850274
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850275
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850276
    new-instance v3, LX/C6n;

    const/16 p0, 0x1f7c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C6n;-><init>(LX/0Ot;)V

    .line 1850277
    move-object v0, v3

    .line 1850278
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850279
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850280
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850281
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1850282
    check-cast p2, LX/C6m;

    .line 1850283
    iget-object v0, p0, LX/C6n;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/C6m;->a:LX/C6a;

    iget-object v1, p2, LX/C6m;->b:LX/C6H;

    const/4 v3, 0x0

    .line 1850284
    iget-boolean v2, v0, LX/C6a;->k:Z

    move v2, v2

    .line 1850285
    if-nez v2, :cond_2

    .line 1850286
    iget-boolean v2, v0, LX/C6a;->c:Z

    move v2, v2

    .line 1850287
    if-eqz v2, :cond_2

    .line 1850288
    iget-boolean v2, v1, LX/C6H;->a:Z

    move v2, v2

    .line 1850289
    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1850290
    if-nez v2, :cond_0

    move-object v2, v3

    .line 1850291
    :goto_1
    move-object v0, v2

    .line 1850292
    return-object v0

    .line 1850293
    :cond_0
    iget-boolean v2, v0, LX/C6a;->d:Z

    move v2, v2

    .line 1850294
    if-eqz v2, :cond_1

    const v2, 0x7f0a008b

    .line 1850295
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x0

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object p0

    const p2, 0x7f0a011a

    invoke-virtual {p0, p2}, LX/25Q;->i(I)LX/25Q;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p2, 0x1

    invoke-interface {p0, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f081a54

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b10ad

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    sget-object p0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p2, LX/0xr;->BOLD:LX/0xr;

    invoke-static {p1, p0, p2, v3}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b10bb

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p0, 0x7f0a009a

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Di;->y(I)LX/1Di;

    move-result-object v2

    .line 1850296
    const v3, -0x345cf50a    # -2.1370348E7f

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1850297
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_1

    .line 1850298
    :cond_1
    const v2, 0x7f0a00e6

    goto/16 :goto_2

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1850299
    invoke-static {}, LX/1dS;->b()V

    .line 1850300
    iget v0, p1, LX/1dQ;->b:I

    .line 1850301
    packed-switch v0, :pswitch_data_0

    .line 1850302
    :goto_0
    return-object v1

    .line 1850303
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1850304
    check-cast v0, LX/C6m;

    .line 1850305
    iget-object v2, p0, LX/C6n;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C6o;

    iget-object v3, v0, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, v0, LX/C6m;->d:LX/1Pq;

    iget-object v5, v0, LX/C6m;->a:LX/C6a;

    iget-object p1, v0, LX/C6m;->b:LX/C6H;

    .line 1850306
    iget-boolean p2, v5, LX/C6a;->d:Z

    move p2, p2

    .line 1850307
    if-nez p2, :cond_0

    .line 1850308
    :goto_1
    goto :goto_0

    .line 1850309
    :cond_0
    invoke-virtual {p1}, LX/C6H;->b()V

    .line 1850310
    const/4 p2, 0x1

    new-array p2, p2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    aput-object v3, p2, p0

    invoke-interface {v4, p2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1850311
    iget-object p2, v2, LX/C6o;->b:LX/C6g;

    iget-object p0, v2, LX/C6o;->d:LX/0SG;

    iget-object v0, v2, LX/C6o;->c:LX/03V;

    invoke-static {v3, p2, p0, v0, v5}, LX/C6T;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C6g;LX/0SG;LX/03V;LX/C6a;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x345cf50a
        :pswitch_0
    .end packed-switch
.end method
