.class public abstract LX/B4J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/B4J;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1741494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741495
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/B4J;->c:J

    .line 1741496
    const/4 v0, 0x0

    iput v0, p0, LX/B4J;->f:I

    .line 1741497
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1741498
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1741499
    iput-object p1, p0, LX/B4J;->a:Ljava/lang/String;

    .line 1741500
    iput-object p2, p0, LX/B4J;->b:Ljava/lang/String;

    .line 1741501
    return-void
.end method


# virtual methods
.method public final a(I)LX/B4J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 1741492
    iput p1, p0, LX/B4J;->f:I

    .line 1741493
    invoke-virtual {p0}, LX/B4J;->b()LX/B4J;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)LX/B4J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .prologue
    .line 1741485
    iput-wide p1, p0, LX/B4J;->c:J

    .line 1741486
    invoke-virtual {p0}, LX/B4J;->b()LX/B4J;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/B4J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1741490
    iput-object p1, p0, LX/B4J;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1741491
    invoke-virtual {p0}, LX/B4J;->b()LX/B4J;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/B4J;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1741488
    iput-object p1, p0, LX/B4J;->d:Ljava/lang/String;

    .line 1741489
    invoke-virtual {p0}, LX/B4J;->b()LX/B4J;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()LX/B4J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final c()Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;
    .locals 9

    .prologue
    .line 1741487
    new-instance v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v2, p0, LX/B4J;->a:Ljava/lang/String;

    iget-object v3, p0, LX/B4J;->b:Ljava/lang/String;

    iget-wide v4, p0, LX/B4J;->c:J

    iget-object v6, p0, LX/B4J;->d:Ljava/lang/String;

    iget-object v7, p0, LX/B4J;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget v8, p0, LX/B4J;->f:I

    invoke-direct/range {v1 .. v8}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;I)V

    return-object v1
.end method
