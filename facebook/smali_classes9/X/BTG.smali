.class public LX/BTG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787048
    iput-object p1, p0, LX/BTG;->a:LX/0Zb;

    .line 1787049
    return-void
.end method

.method public static a(LX/0QB;)LX/BTG;
    .locals 1

    .prologue
    .line 1787050
    invoke-static {p0}, LX/BTG;->b(LX/0QB;)LX/BTG;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BTG;
    .locals 3

    .prologue
    .line 1787051
    new-instance v2, LX/BTG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/BTG;-><init>(LX/0Zb;LX/0ad;)V

    .line 1787052
    return-object v2
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1787053
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "video_editing_entry"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "video_editing_module"

    .line 1787054
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1787055
    move-object v0, v0

    .line 1787056
    sget-object v1, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v1}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v1}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {v1}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v0, LX/BTE;->HD_UPLOAD_STATE:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v2

    const-string v0, "high"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1787057
    return-object v0

    .line 1787058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/BTC;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1787059
    const-string v0, "composer"

    invoke-static {p1, v0, p3, p4}, LX/BTG;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1787060
    sget-object v1, LX/BTF;->ENTRY_ACTION:LX/BTF;

    invoke-virtual {v1}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LX/BTC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1787061
    iget-object v1, p0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1787062
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1787063
    invoke-static {p1, p2, p3, p4}, LX/BTG;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1787064
    iget-object v1, p0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1787065
    return-void
.end method
