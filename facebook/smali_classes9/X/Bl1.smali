.class public LX/Bl1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:I

.field public T:I

.field public U:I

.field public V:I

.field public W:I

.field public X:I

.field public Y:I

.field public Z:I

.field public a:I

.field public aa:I

.field public ab:I

.field public ac:I

.field public ad:I

.field public ae:I

.field public af:I

.field public ag:I

.field public ah:I

.field public ai:I

.field public aj:I

.field public ak:Landroid/database/Cursor;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1815663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1815664
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1815660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1815661
    invoke-virtual {p0, p1}, LX/Bl1;->a(Landroid/database/Cursor;)V

    .line 1815662
    return-void
.end method

.method public static a(LX/0QB;)LX/Bl1;
    .locals 1

    .prologue
    .line 1815657
    new-instance v0, LX/Bl1;

    invoke-direct {v0}, LX/Bl1;-><init>()V

    .line 1815658
    move-object v0, v0

    .line 1815659
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1815305
    iput-object p1, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    .line 1815306
    if-nez p1, :cond_0

    .line 1815307
    :goto_0
    return-void

    .line 1815308
    :cond_0
    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 1815309
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815310
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->a:I

    .line 1815311
    sget-object v0, LX/Bkw;->c:LX/0U1;

    .line 1815312
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815313
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->b:I

    .line 1815314
    sget-object v0, LX/Bkw;->d:LX/0U1;

    .line 1815315
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815316
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->c:I

    .line 1815317
    sget-object v0, LX/Bkw;->e:LX/0U1;

    .line 1815318
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815319
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->d:I

    .line 1815320
    sget-object v0, LX/Bkw;->f:LX/0U1;

    .line 1815321
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815322
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->e:I

    .line 1815323
    sget-object v0, LX/Bkw;->g:LX/0U1;

    .line 1815324
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815325
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->f:I

    .line 1815326
    sget-object v0, LX/Bkw;->h:LX/0U1;

    .line 1815327
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815328
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->g:I

    .line 1815329
    sget-object v0, LX/Bkw;->i:LX/0U1;

    .line 1815330
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815331
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->h:I

    .line 1815332
    sget-object v0, LX/Bkw;->p:LX/0U1;

    .line 1815333
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815334
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->i:I

    .line 1815335
    sget-object v0, LX/Bkw;->j:LX/0U1;

    .line 1815336
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815337
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->j:I

    .line 1815338
    sget-object v0, LX/Bkw;->k:LX/0U1;

    .line 1815339
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815340
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->k:I

    .line 1815341
    sget-object v0, LX/Bkw;->l:LX/0U1;

    .line 1815342
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815343
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->l:I

    .line 1815344
    sget-object v0, LX/Bkw;->m:LX/0U1;

    .line 1815345
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815346
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->m:I

    .line 1815347
    sget-object v0, LX/Bkw;->n:LX/0U1;

    .line 1815348
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815349
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->n:I

    .line 1815350
    sget-object v0, LX/Bkw;->o:LX/0U1;

    .line 1815351
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815352
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->o:I

    .line 1815353
    sget-object v0, LX/Bkw;->q:LX/0U1;

    .line 1815354
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815355
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->p:I

    .line 1815356
    sget-object v0, LX/Bkw;->r:LX/0U1;

    .line 1815357
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815358
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->q:I

    .line 1815359
    sget-object v0, LX/Bkw;->s:LX/0U1;

    .line 1815360
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815361
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->r:I

    .line 1815362
    sget-object v0, LX/Bkw;->t:LX/0U1;

    .line 1815363
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815364
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->s:I

    .line 1815365
    sget-object v0, LX/Bkw;->u:LX/0U1;

    .line 1815366
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815367
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->t:I

    .line 1815368
    sget-object v0, LX/Bkw;->v:LX/0U1;

    .line 1815369
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815370
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->u:I

    .line 1815371
    sget-object v0, LX/Bkw;->w:LX/0U1;

    .line 1815372
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815373
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->v:I

    .line 1815374
    sget-object v0, LX/Bkw;->x:LX/0U1;

    .line 1815375
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815376
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->y:I

    .line 1815377
    sget-object v0, LX/Bkw;->y:LX/0U1;

    .line 1815378
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815379
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->w:I

    .line 1815380
    sget-object v0, LX/Bkw;->z:LX/0U1;

    .line 1815381
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815382
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->x:I

    .line 1815383
    sget-object v0, LX/Bkw;->A:LX/0U1;

    .line 1815384
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815385
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->z:I

    .line 1815386
    sget-object v0, LX/Bkw;->B:LX/0U1;

    .line 1815387
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815388
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->A:I

    .line 1815389
    sget-object v0, LX/Bkw;->C:LX/0U1;

    .line 1815390
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815391
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->B:I

    .line 1815392
    sget-object v0, LX/Bkw;->D:LX/0U1;

    .line 1815393
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815394
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->C:I

    .line 1815395
    sget-object v0, LX/Bkw;->E:LX/0U1;

    .line 1815396
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815397
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->D:I

    .line 1815398
    sget-object v0, LX/Bkw;->F:LX/0U1;

    .line 1815399
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815400
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->E:I

    .line 1815401
    sget-object v0, LX/Bkw;->G:LX/0U1;

    .line 1815402
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815403
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->F:I

    .line 1815404
    sget-object v0, LX/Bkw;->I:LX/0U1;

    .line 1815405
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815406
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->G:I

    .line 1815407
    sget-object v0, LX/Bkw;->J:LX/0U1;

    .line 1815408
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815409
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->H:I

    .line 1815410
    sget-object v0, LX/Bkw;->K:LX/0U1;

    .line 1815411
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815412
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->I:I

    .line 1815413
    sget-object v0, LX/Bkw;->L:LX/0U1;

    .line 1815414
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815415
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->J:I

    .line 1815416
    sget-object v0, LX/Bkw;->M:LX/0U1;

    .line 1815417
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815418
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->K:I

    .line 1815419
    sget-object v0, LX/Bkw;->N:LX/0U1;

    .line 1815420
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815421
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->L:I

    .line 1815422
    sget-object v0, LX/Bkw;->O:LX/0U1;

    .line 1815423
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815424
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->M:I

    .line 1815425
    sget-object v0, LX/Bkw;->P:LX/0U1;

    .line 1815426
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815427
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->N:I

    .line 1815428
    sget-object v0, LX/Bkw;->Q:LX/0U1;

    .line 1815429
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815430
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->O:I

    .line 1815431
    sget-object v0, LX/Bkw;->S:LX/0U1;

    .line 1815432
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815433
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->P:I

    .line 1815434
    sget-object v0, LX/Bkw;->R:LX/0U1;

    .line 1815435
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815436
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->Q:I

    .line 1815437
    sget-object v0, LX/Bkw;->T:LX/0U1;

    .line 1815438
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815439
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->R:I

    .line 1815440
    sget-object v0, LX/Bkw;->U:LX/0U1;

    .line 1815441
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815442
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->S:I

    .line 1815443
    sget-object v0, LX/Bkw;->V:LX/0U1;

    .line 1815444
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815445
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->T:I

    .line 1815446
    sget-object v0, LX/Bkw;->W:LX/0U1;

    .line 1815447
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815448
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->U:I

    .line 1815449
    sget-object v0, LX/Bkw;->X:LX/0U1;

    .line 1815450
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815451
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->V:I

    .line 1815452
    sget-object v0, LX/Bkw;->Y:LX/0U1;

    .line 1815453
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815454
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->W:I

    .line 1815455
    sget-object v0, LX/Bkw;->Z:LX/0U1;

    .line 1815456
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815457
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->X:I

    .line 1815458
    sget-object v0, LX/Bkw;->aa:LX/0U1;

    .line 1815459
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815460
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->Y:I

    .line 1815461
    sget-object v0, LX/Bkw;->ab:LX/0U1;

    .line 1815462
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815463
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->Z:I

    .line 1815464
    sget-object v0, LX/Bkw;->ac:LX/0U1;

    .line 1815465
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815466
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->aa:I

    .line 1815467
    sget-object v0, LX/Bkw;->ad:LX/0U1;

    .line 1815468
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815469
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ab:I

    .line 1815470
    sget-object v0, LX/Bkw;->ae:LX/0U1;

    .line 1815471
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815472
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ac:I

    .line 1815473
    sget-object v0, LX/Bkw;->af:LX/0U1;

    .line 1815474
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815475
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ad:I

    .line 1815476
    sget-object v0, LX/Bkw;->ag:LX/0U1;

    .line 1815477
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815478
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ae:I

    .line 1815479
    sget-object v0, LX/Bkw;->ah:LX/0U1;

    .line 1815480
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815481
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->af:I

    .line 1815482
    sget-object v0, LX/Bkw;->ak:LX/0U1;

    .line 1815483
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815484
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ag:I

    .line 1815485
    sget-object v0, LX/Bkw;->ai:LX/0U1;

    .line 1815486
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815487
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ah:I

    .line 1815488
    sget-object v0, LX/Bkw;->aj:LX/0U1;

    .line 1815489
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815490
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->ai:I

    .line 1815491
    sget-object v0, LX/Bkw;->al:LX/0U1;

    .line 1815492
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1815493
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Bl1;->aj:I

    goto/16 :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1815656
    iget-object v0, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1815655
    iget-object v0, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 1815654
    iget-object v0, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    iget v1, p0, LX/Bl1;->G:I

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, LX/Bl4;->a(Landroid/database/Cursor;IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()Lcom/facebook/events/model/Event;
    .locals 12

    .prologue
    .line 1815494
    iget-object v0, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    const-wide/16 v9, -0x1

    const/4 v1, 0x1

    const-wide/high16 v7, 0x7ff8000000000000L    # NaN

    const/4 v2, 0x0

    .line 1815495
    new-instance v3, LX/7vC;

    invoke-direct {v3}, LX/7vC;-><init>()V

    .line 1815496
    iget v4, p0, LX/Bl1;->a:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815497
    iput-object v4, v3, LX/7vC;->a:Ljava/lang/String;

    .line 1815498
    iget v4, p0, LX/Bl1;->b:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815499
    iput-object v4, v3, LX/7vC;->b:Ljava/lang/String;

    .line 1815500
    iget v4, p0, LX/Bl1;->c:I

    iget v5, p0, LX/Bl1;->d:I

    invoke-static {v0, v4, v5}, LX/Bl4;->a(Landroid/database/Cursor;II)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v4

    .line 1815501
    iput-object v4, v3, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1815502
    iget v4, p0, LX/Bl1;->e:I

    .line 1815503
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815504
    if-eqz v5, :cond_3

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v5

    :goto_0
    move-object v4, v5

    .line 1815505
    iput-object v4, v3, LX/7vC;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1815506
    iget v4, p0, LX/Bl1;->f:I

    .line 1815507
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815508
    if-eqz v5, :cond_4

    invoke-static {v5}, Lcom/facebook/events/model/EventType;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/EventType;

    move-result-object v5

    :goto_1
    move-object v4, v5

    .line 1815509
    iput-object v4, v3, LX/7vC;->e:Lcom/facebook/events/model/EventType;

    .line 1815510
    iget v4, p0, LX/Bl1;->g:I

    .line 1815511
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815512
    if-eqz v5, :cond_5

    invoke-static {v5}, Lcom/facebook/events/model/PrivacyKind;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyKind;

    move-result-object v5

    :goto_2
    move-object v4, v5

    .line 1815513
    iput-object v4, v3, LX/7vC;->f:Lcom/facebook/events/model/PrivacyKind;

    .line 1815514
    iget v4, p0, LX/Bl1;->h:I

    .line 1815515
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815516
    if-eqz v5, :cond_6

    invoke-static {v5}, Lcom/facebook/events/model/PrivacyType;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyType;

    move-result-object v5

    :goto_3
    move-object v4, v5

    .line 1815517
    iput-object v4, v3, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    .line 1815518
    iget v4, p0, LX/Bl1;->j:I

    .line 1815519
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815520
    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v5

    move-object v4, v5

    .line 1815521
    iput-object v4, v3, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1815522
    iget v4, p0, LX/Bl1;->k:I

    .line 1815523
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815524
    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v5

    move-object v4, v5

    .line 1815525
    iput-object v4, v3, LX/7vC;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1815526
    iget v4, p0, LX/Bl1;->l:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815527
    iput-boolean v4, v3, LX/7vC;->h:Z

    .line 1815528
    iget v4, p0, LX/Bl1;->m:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815529
    iput-boolean v4, v3, LX/7vC;->i:Z

    .line 1815530
    iget v4, p0, LX/Bl1;->n:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815531
    iput-boolean v4, v3, LX/7vC;->j:Z

    .line 1815532
    iget v4, p0, LX/Bl1;->o:I

    .line 1815533
    if-gez v4, :cond_7

    sget-object v5, LX/03R;->UNSET:LX/03R;

    :goto_4
    move-object v4, v5

    .line 1815534
    invoke-virtual {v3, v4}, LX/7vC;->a(LX/03R;)LX/7vC;

    .line 1815535
    iget v4, p0, LX/Bl1;->i:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815536
    iput-boolean v4, v3, LX/7vC;->l:Z

    .line 1815537
    iget v4, p0, LX/Bl1;->p:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815538
    iput-object v4, v3, LX/7vC;->r:Ljava/lang/String;

    .line 1815539
    iget v4, p0, LX/Bl1;->q:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815540
    iput-object v4, v3, LX/7vC;->s:Ljava/lang/String;

    .line 1815541
    iget v4, p0, LX/Bl1;->r:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815542
    iput-object v4, v3, LX/7vC;->t:Ljava/lang/String;

    .line 1815543
    iget v4, p0, LX/Bl1;->s:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815544
    iput-object v4, v3, LX/7vC;->u:Ljava/lang/String;

    .line 1815545
    iget v4, p0, LX/Bl1;->t:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815546
    iput-object v4, v3, LX/7vC;->v:Ljava/lang/String;

    .line 1815547
    iget v4, p0, LX/Bl1;->u:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815548
    iput-object v4, v3, LX/7vC;->w:Ljava/lang/String;

    .line 1815549
    iget v4, p0, LX/Bl1;->v:I

    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815550
    iput-object v4, v3, LX/7vC;->x:Ljava/lang/String;

    .line 1815551
    iget v4, p0, LX/Bl1;->y:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815552
    iput-boolean v4, v3, LX/7vC;->y:Z

    .line 1815553
    iget v4, p0, LX/Bl1;->w:I

    invoke-static {v0, v4, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v4

    .line 1815554
    iput-boolean v4, v3, LX/7vC;->z:Z

    .line 1815555
    iget v4, p0, LX/Bl1;->x:I

    invoke-static {v0, v4, v9, v10}, LX/Bl4;->a(Landroid/database/Cursor;IJ)J

    move-result-wide v5

    .line 1815556
    iput-wide v5, v3, LX/7vC;->A:J

    .line 1815557
    iget v4, p0, LX/Bl1;->A:I

    .line 1815558
    invoke-static {v0, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815559
    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    move-object v4, v5

    .line 1815560
    iget v5, p0, LX/Bl1;->z:I

    invoke-static {v0, v5, v1}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v5

    .line 1815561
    iput-boolean v5, v3, LX/7vC;->B:Z

    .line 1815562
    iput-object v4, v3, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1815563
    iget v5, p0, LX/Bl1;->B:I

    .line 1815564
    invoke-static {v0, v5}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v6

    move-object v5, v6

    .line 1815565
    iput-object v5, v3, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1815566
    iget v5, p0, LX/Bl1;->C:I

    invoke-static {v0, v5, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v5

    .line 1815567
    iput-boolean v5, v3, LX/7vC;->E:Z

    .line 1815568
    iget v5, p0, LX/Bl1;->D:I

    invoke-static {v0, v5}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815569
    iput-object v5, v3, LX/7vC;->F:Ljava/lang/String;

    .line 1815570
    iget v5, p0, LX/Bl1;->E:I

    invoke-static {v0, v5}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 1815571
    iput-object v5, v3, LX/7vC;->G:Ljava/lang/String;

    .line 1815572
    iget v5, p0, LX/Bl1;->F:I

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v6, :cond_2

    :goto_5
    invoke-static {v0, v5, v1}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v1

    .line 1815573
    iput-boolean v1, v3, LX/7vC;->H:Z

    .line 1815574
    iget v1, p0, LX/Bl1;->G:I

    invoke-static {v0, v1}, LX/Bl4;->k(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v1

    .line 1815575
    iput-object v1, v3, LX/7vC;->I:Ljava/util/Date;

    .line 1815576
    iget v1, p0, LX/Bl1;->H:I

    invoke-static {v0, v1}, LX/Bl4;->k(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v1

    .line 1815577
    iput-object v1, v3, LX/7vC;->J:Ljava/util/Date;

    .line 1815578
    iget v1, p0, LX/Bl1;->I:I

    invoke-static {v0, v1}, LX/Bl4;->l(Landroid/database/Cursor;I)Ljava/util/TimeZone;

    move-result-object v1

    .line 1815579
    iput-object v1, v3, LX/7vC;->K:Ljava/util/TimeZone;

    .line 1815580
    iget v1, p0, LX/Bl1;->J:I

    invoke-static {v0, v1, v2}, LX/Bl4;->a(Landroid/database/Cursor;IZ)Z

    move-result v1

    .line 1815581
    iput-boolean v1, v3, LX/7vC;->L:Z

    .line 1815582
    iget v1, p0, LX/Bl1;->K:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815583
    iput-object v1, v3, LX/7vC;->P:Ljava/lang/String;

    .line 1815584
    iget v1, p0, LX/Bl1;->L:I

    invoke-static {v0, v1, v7, v8}, LX/Bl4;->a(Landroid/database/Cursor;ID)D

    move-result-wide v5

    .line 1815585
    iget v1, p0, LX/Bl1;->M:I

    invoke-static {v0, v1, v7, v8}, LX/Bl4;->a(Landroid/database/Cursor;ID)D

    move-result-wide v7

    .line 1815586
    iget v1, p0, LX/Bl1;->N:I

    invoke-static {v0, v1, v9, v10}, LX/Bl4;->a(Landroid/database/Cursor;IJ)J

    move-result-wide v9

    .line 1815587
    iput-wide v9, v3, LX/7vC;->N:J

    .line 1815588
    iget v1, p0, LX/Bl1;->O:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815589
    iput-object v1, v3, LX/7vC;->O:Ljava/lang/String;

    .line 1815590
    iget v1, p0, LX/Bl1;->P:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815591
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1815592
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v4, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 1815593
    iput v1, v3, LX/7vC;->R:I

    .line 1815594
    :cond_0
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1815595
    invoke-virtual {v3, v5, v6, v7, v8}, LX/7vC;->a(DD)LX/7vC;

    .line 1815596
    :cond_1
    iget v1, p0, LX/Bl1;->Q:I

    invoke-static {v0, v1}, LX/Bl4;->l(Landroid/database/Cursor;I)Ljava/util/TimeZone;

    move-result-object v1

    .line 1815597
    iput-object v1, v3, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1815598
    iget v1, p0, LX/Bl1;->R:I

    invoke-static {v0, v1}, LX/Bl4;->m(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v1

    .line 1815599
    iput-object v1, v3, LX/7vC;->T:Landroid/net/Uri;

    .line 1815600
    iget v1, p0, LX/Bl1;->S:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815601
    iput-object v1, v3, LX/7vC;->U:Ljava/lang/String;

    .line 1815602
    iget v1, p0, LX/Bl1;->T:I

    invoke-static {v0, v1}, LX/Bl4;->m(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v1

    .line 1815603
    iput-object v1, v3, LX/7vC;->V:Landroid/net/Uri;

    .line 1815604
    iget v1, p0, LX/Bl1;->U:I

    invoke-static {v0, v1}, LX/Bl4;->m(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v1

    .line 1815605
    iput-object v1, v3, LX/7vC;->W:Landroid/net/Uri;

    .line 1815606
    iget v1, p0, LX/Bl1;->V:I

    invoke-static {v0, v1}, LX/Bl4;->m(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v1

    .line 1815607
    iput-object v1, v3, LX/7vC;->X:Landroid/net/Uri;

    .line 1815608
    iget v1, p0, LX/Bl1;->W:I

    invoke-static {v0, v1}, LX/Bl4;->o(Landroid/database/Cursor;I)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/7vC;->a(Ljava/util/EnumSet;)LX/7vC;

    .line 1815609
    iget v1, p0, LX/Bl1;->X:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815610
    iput-object v1, v3, LX/7vC;->o:Ljava/lang/String;

    .line 1815611
    iget v1, p0, LX/Bl1;->Y:I

    invoke-static {v0, v1, v2}, LX/Bl4;->b(Landroid/database/Cursor;II)I

    move-result v1

    .line 1815612
    iput v1, v3, LX/7vC;->p:I

    .line 1815613
    iget v1, p0, LX/Bl1;->Z:I

    .line 1815614
    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815615
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1815616
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1815617
    :goto_6
    move-object v1, v4

    .line 1815618
    iput-object v1, v3, LX/7vC;->q:LX/0Px;

    .line 1815619
    iget v1, p0, LX/Bl1;->aa:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815620
    iput-object v1, v3, LX/7vC;->ab:Ljava/lang/String;

    .line 1815621
    iget v1, p0, LX/Bl1;->ab:I

    invoke-static {v0, v1, v2}, LX/Bl4;->b(Landroid/database/Cursor;II)I

    move-result v1

    .line 1815622
    iput v1, v3, LX/7vC;->ac:I

    .line 1815623
    iget v1, p0, LX/Bl1;->ac:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815624
    iput-object v1, v3, LX/7vC;->ad:Ljava/lang/String;

    .line 1815625
    iget v1, p0, LX/Bl1;->ad:I

    invoke-static {v0, v1, v2}, LX/Bl4;->b(Landroid/database/Cursor;II)I

    move-result v1

    .line 1815626
    iput v1, v3, LX/7vC;->ae:I

    .line 1815627
    iget v1, p0, LX/Bl1;->ae:I

    invoke-static {v0, v1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815628
    iput-object v1, v3, LX/7vC;->af:Ljava/lang/String;

    .line 1815629
    iget v1, p0, LX/Bl1;->af:I

    invoke-static {v0, v1, v2}, LX/Bl4;->b(Landroid/database/Cursor;II)I

    move-result v1

    .line 1815630
    iput v1, v3, LX/7vC;->ag:I

    .line 1815631
    iget-object v1, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    iget v4, p0, LX/Bl1;->ag:I

    invoke-static {v1, v4}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815632
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1815633
    const/4 v1, 0x0

    .line 1815634
    :goto_7
    move-object v1, v1

    .line 1815635
    iput-object v1, v3, LX/7vC;->ah:Lcom/facebook/events/model/EventUser;

    .line 1815636
    iget v1, p0, LX/Bl1;->aj:I

    invoke-static {v0, v1, v2}, LX/Bl4;->b(Landroid/database/Cursor;II)I

    move-result v1

    .line 1815637
    iput v1, v3, LX/7vC;->ao:I

    .line 1815638
    invoke-virtual {v3}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v1

    move-object v0, v1

    .line 1815639
    return-object v0

    :cond_2
    move v1, v2

    .line 1815640
    goto/16 :goto_5

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_7
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v5

    goto/16 :goto_4

    :cond_8
    const/16 v5, 0x2c

    invoke-static {v4, v5}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    goto :goto_6

    .line 1815641
    :cond_9
    iget-object v4, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    iget v5, p0, LX/Bl1;->ah:I

    invoke-static {v4, v5}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v4

    .line 1815642
    iget-object v5, p0, LX/Bl1;->ak:Landroid/database/Cursor;

    iget v6, p0, LX/Bl1;->ai:I

    invoke-static {v5, v6}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    .line 1815643
    new-instance v6, LX/7vI;

    invoke-direct {v6}, LX/7vI;-><init>()V

    .line 1815644
    iput-object v1, v6, LX/7vI;->c:Ljava/lang/String;

    .line 1815645
    move-object v1, v6

    .line 1815646
    iput-object v4, v1, LX/7vI;->b:Ljava/lang/String;

    .line 1815647
    move-object v1, v1

    .line 1815648
    iput-object v5, v1, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1815649
    move-object v1, v1

    .line 1815650
    sget-object v4, LX/7vJ;->USER:LX/7vJ;

    .line 1815651
    iput-object v4, v1, LX/7vI;->a:LX/7vJ;

    .line 1815652
    move-object v1, v1

    .line 1815653
    invoke-virtual {v1}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v1

    goto :goto_7
.end method
