.class public LX/ApD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public static final b:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1715530
    const v0, 0x7f0e0129

    sput v0, LX/ApD;->a:I

    .line 1715531
    const v0, 0x7f0e012b

    sput v0, LX/ApD;->b:I

    .line 1715532
    const v0, 0x7f0e012d

    sput v0, LX/ApD;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1715533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1715534
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    goto :goto_0
.end method
