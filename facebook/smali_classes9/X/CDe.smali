.class public LX/CDe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/392;",
        ":",
        "LX/3FP;",
        ":",
        "LX/3FT;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field public final a:LX/CDg;

.field private final b:LX/198;

.field private final c:LX/094;

.field private final d:LX/03V;

.field private final e:LX/2mr;

.field public final f:Lcom/facebook/graphql/model/GraphQLVideo;

.field private final g:LX/CDX;


# direct methods
.method public constructor <init>(LX/CDg;LX/198;LX/094;LX/03V;LX/2mr;Lcom/facebook/graphql/model/GraphQLVideo;LX/CDX;)V
    .locals 0

    .prologue
    .line 1859903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859904
    iput-object p1, p0, LX/CDe;->a:LX/CDg;

    .line 1859905
    iput-object p2, p0, LX/CDe;->b:LX/198;

    .line 1859906
    iput-object p3, p0, LX/CDe;->c:LX/094;

    .line 1859907
    iput-object p4, p0, LX/CDe;->d:LX/03V;

    .line 1859908
    iput-object p5, p0, LX/CDe;->e:LX/2mr;

    .line 1859909
    iput-object p6, p0, LX/CDe;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1859910
    iput-object p7, p0, LX/CDe;->g:LX/CDX;

    .line 1859911
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    const/4 v3, 0x1

    const v0, -0xc3f2c54

    invoke-static {v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 1859912
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-boolean v0, v0, LX/CDg;->i:Z

    if-eqz v0, :cond_0

    .line 1859913
    const v0, 0x165c608f

    invoke-static {v1, v1, v0, v5}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1859914
    :goto_0
    return-void

    .line 1859915
    :cond_0
    instance-of v0, p1, LX/392;

    if-eqz v0, :cond_1

    instance-of v0, p1, LX/3FP;

    if-nez v0, :cond_2

    .line 1859916
    :cond_1
    iget-object v0, p0, LX/CDe;->d:LX/03V;

    const-class v1, LX/CDe;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Wrong view type: view should implement VideoAttachmentView and CanShowLiveCommentDialogFragment"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859917
    const v0, -0x5be9c563

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1859918
    check-cast v0, LX/3FT;

    invoke-interface {v0}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v6

    .line 1859919
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iput-boolean v3, v0, LX/CDg;->i:Z

    .line 1859920
    iget-object v0, p0, LX/CDe;->g:LX/CDX;

    .line 1859921
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/CDX;->n:Ljava/lang/ref/WeakReference;

    .line 1859922
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->d:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->a()I

    move-result v0

    .line 1859923
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1859924
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 1859925
    :cond_3
    iget-object v1, p0, LX/CDe;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 1859926
    const/4 v0, 0x0

    .line 1859927
    :cond_4
    move v2, v0

    .line 1859928
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v7

    .line 1859929
    iget-object v0, p0, LX/CDe;->c:LX/094;

    .line 1859930
    iget-object v1, v6, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 1859931
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/094;->a(Ljava/lang/String;)V

    .line 1859932
    iget-object v0, p0, LX/CDe;->c:LX/094;

    .line 1859933
    iget-object v1, v6, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 1859934
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/094;->a(Ljava/lang/String;Z)V

    .line 1859935
    if-eqz v7, :cond_9

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v0

    move v1, v0

    .line 1859936
    :goto_1
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v8, p0, LX/CDe;->b:LX/198;

    sget-object v9, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1859937
    iget-boolean v10, v0, LX/CDg;->e:Z

    if-eqz v10, :cond_5

    .line 1859938
    const/4 v10, 0x0

    iput-boolean v10, v0, LX/CDg;->e:Z

    .line 1859939
    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/198;->b(LX/1YD;)V

    .line 1859940
    :cond_5
    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v10

    if-lez v10, :cond_6

    .line 1859941
    iget-object v10, v0, LX/CDg;->d:LX/2oL;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v11

    invoke-virtual {v10, v11}, LX/2oL;->a(I)V

    .line 1859942
    :cond_6
    invoke-virtual {v6, v9}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1859943
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v8, LX/0f8;

    invoke-static {v0, v8}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 1859944
    if-nez v0, :cond_7

    .line 1859945
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v8, Landroid/app/Activity;

    invoke-static {v0, v8}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1859946
    iget-object v8, p0, LX/CDe;->e:LX/2mr;

    .line 1859947
    iput-object v0, v8, LX/2mr;->b:Landroid/app/Activity;

    .line 1859948
    iget-object v0, p0, LX/CDe;->e:LX/2mr;

    .line 1859949
    :cond_7
    iget-object v8, p0, LX/CDe;->a:LX/CDg;

    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v0

    iput-object v0, v8, LX/CDg;->b:LX/0hE;

    .line 1859950
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    iget-object v8, p0, LX/CDe;->g:LX/CDX;

    invoke-interface {v0, v8}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 1859951
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    invoke-interface {v0, v3}, LX/0hE;->setAllowLooping(Z)V

    .line 1859952
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v8, v0, LX/CDg;->b:LX/0hE;

    if-nez v7, :cond_a

    move v0, v3

    :goto_2
    invoke-interface {v8, v0}, LX/0hE;->setLogEnteringStartEvent(Z)V

    .line 1859953
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    invoke-interface {v0, v4}, LX/0hE;->setLogExitingPauseEvent(Z)V

    .line 1859954
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v6, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1859955
    if-eqz v0, :cond_8

    .line 1859956
    iget-object v3, p0, LX/CDe;->a:LX/CDg;

    iget-object v3, v3, LX/CDg;->a:LX/395;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->getViewportState()LX/7DJ;

    move-result-object v0

    .line 1859957
    iput-object v0, v3, LX/395;->p:LX/7DJ;

    .line 1859958
    :cond_8
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->c:LX/2oO;

    invoke-virtual {v0}, LX/2oO;->a()V

    .line 1859959
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->a:LX/395;

    invoke-virtual {v0, v2}, LX/395;->a(I)LX/395;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/395;->b(I)LX/395;

    move-result-object v0

    check-cast p1, LX/3FP;

    invoke-interface {p1}, LX/3FP;->getAndClearShowLiveCommentDialogFragment()Z

    move-result v1

    .line 1859960
    iput-boolean v1, v0, LX/395;->k:Z

    .line 1859961
    if-eqz v7, :cond_b

    .line 1859962
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->a:LX/395;

    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1859963
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 1859964
    :goto_3
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->a:LX/395;

    const/4 v1, 0x0

    .line 1859965
    iput-object v1, v0, LX/395;->g:LX/0P1;

    .line 1859966
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    iget-object v1, p0, LX/CDe;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->a:LX/395;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/395;)V

    .line 1859967
    const v0, 0x756e22f0

    invoke-static {v0, v5}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    .line 1859968
    goto/16 :goto_1

    :cond_a
    move v0, v4

    .line 1859969
    goto :goto_2

    .line 1859970
    :cond_b
    iget-object v0, p0, LX/CDe;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->a:LX/395;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 1859971
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 1859972
    goto :goto_3
.end method
