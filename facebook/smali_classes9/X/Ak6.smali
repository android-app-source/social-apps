.class public final LX/Ak6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/productionprompts/model/PromptDisplayReason;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/Ak7;


# direct methods
.method public constructor <init>(LX/Ak7;Lcom/facebook/productionprompts/model/PromptDisplayReason;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1708721
    iput-object p1, p0, LX/Ak6;->c:LX/Ak7;

    iput-object p2, p0, LX/Ak6;->a:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    iput-object p3, p0, LX/Ak6;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1708722
    iget-object v0, p0, LX/Ak6;->a:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    .line 1708723
    iget-object v1, v0, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v0, v1

    .line 1708724
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1708725
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v0

    invoke-interface {v0}, LX/171;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1708726
    iget-object v0, p0, LX/Ak6;->c:LX/Ak7;

    iget-object v0, v0, LX/Ak7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Ak6;->b:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1708727
    const/4 v0, 0x1

    return v0
.end method
