.class public LX/And;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17Q;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712738
    iput-object p1, p0, LX/And;->a:LX/0Zb;

    .line 1712739
    iput-object p2, p0, LX/And;->b:LX/17Q;

    .line 1712740
    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/Flattenable;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712741
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-eqz v0, :cond_0

    .line 1712742
    const-string v0, "gsym_click"

    .line 1712743
    :goto_0
    return-object v0

    .line 1712744
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    if-eqz v0, :cond_1

    .line 1712745
    const-string v0, "psym_click"

    goto :goto_0

    .line 1712746
    :cond_1
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;

    if-eqz v0, :cond_2

    .line 1712747
    const-string v0, "ssfy_click"

    goto :goto_0

    .line 1712748
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
