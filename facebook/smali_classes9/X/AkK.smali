.class public LX/AkK;
.super LX/3x6;
.source ""


# instance fields
.field private final a:LX/62Z;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708972
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1708973
    const v0, 0x7f0b11cb

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AkK;->b:I

    .line 1708974
    new-instance v0, LX/62Z;

    const v1, 0x106000d

    iget v2, p0, LX/AkK;->b:I

    invoke-direct {v0, v1, v2}, LX/62Z;-><init>(II)V

    iput-object v0, p0, LX/AkK;->a:LX/62Z;

    .line 1708975
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1708976
    iget-object v0, p0, LX/AkK;->a:LX/62Z;

    invoke-virtual {v0, p1, p2, p3}, LX/3x6;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1708977
    return-void
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1708978
    iget v0, p0, LX/AkK;->b:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1708979
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    .line 1708980
    iget-object p4, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object p4, p4

    .line 1708981
    invoke-virtual {p4}, LX/1OM;->ij_()I

    move-result p4

    add-int/lit8 p4, p4, -0x1

    if-ne v0, p4, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1708982
    if-eqz v0, :cond_0

    iget v0, p0, LX/AkK;->b:I

    :goto_1
    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1708983
    return-void

    .line 1708984
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
