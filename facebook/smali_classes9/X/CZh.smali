.class public LX/CZh;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:I

.field public final b:I

.field public c:Landroid/view/View;

.field public d:Landroid/widget/LinearLayout;

.field public e:I

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/9Iy;",
            ">;"
        }
    .end annotation
.end field

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1916484
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1916485
    invoke-virtual {p0}, LX/CZh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b011c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/CZh;->a:I

    .line 1916486
    invoke-virtual {p0}, LX/CZh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/CZh;->b:I

    .line 1916487
    const/4 v0, 0x1

    iput v0, p0, LX/CZh;->e:I

    .line 1916488
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1916489
    iput-object v0, p0, LX/CZh;->f:LX/0Px;

    .line 1916490
    const v0, 0x7f030f2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1916491
    const v0, 0x7f0d07fd

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CZh;->c:Landroid/view/View;

    .line 1916492
    const v0, 0x7f0d24c8

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/CZh;->d:Landroid/widget/LinearLayout;

    .line 1916493
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1916503
    iget-object v0, p0, LX/CZh;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1916504
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1916496
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1916497
    iget v0, p0, LX/CZh;->a:I

    iget v1, p0, LX/CZh;->b:I

    add-int/2addr v0, v1

    .line 1916498
    invoke-virtual {p0}, LX/CZh;->getPaddingRight()I

    move-result v1

    .line 1916499
    invoke-virtual {p0}, LX/CZh;->getPaddingLeft()I

    move-result v2

    .line 1916500
    invoke-virtual {p0}, LX/CZh;->getMeasuredWidth()I

    move-result v3

    sub-int v2, v3, v2

    sub-int v1, v2, v1

    .line 1916501
    iget v2, p0, LX/CZh;->b:I

    add-int/2addr v1, v2

    div-int v0, v1, v0

    iput v0, p0, LX/CZh;->g:I

    .line 1916502
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1916494
    iget-object v0, p0, LX/CZh;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1916495
    return-void
.end method
