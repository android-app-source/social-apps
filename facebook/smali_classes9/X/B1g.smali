.class public final LX/B1g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B1b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/B1b",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V
    .locals 0

    .prologue
    .line 1736226
    iput-object p1, p0, LX/B1g;->a:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGroupVisibility;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1736233
    iget-object v0, p0, LX/B1g;->a:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    .line 1736234
    invoke-static {v0, p1}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a$redex0(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;LX/0Px;)V

    .line 1736235
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1736227
    iget-object v0, p0, LX/B1g;->a:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    .line 1736228
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1736229
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    .line 1736230
    :goto_0
    iget-object p0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1736231
    :cond_0
    return-void

    .line 1736232
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
