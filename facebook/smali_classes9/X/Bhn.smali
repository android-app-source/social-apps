.class public final LX/Bhn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/4pL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1809669
    iput-object p1, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    iput-object p2, p0, LX/Bhn;->a:Landroid/view/View;

    iput-object p3, p0, LX/Bhn;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/4pL;)V
    .locals 4
    .param p1    # LX/4pL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1809674
    if-nez p1, :cond_0

    .line 1809675
    iget-object v0, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    iget-object v1, p0, LX/Bhn;->a:Landroid/view/View;

    iget-object v2, p0, LX/Bhn;->b:Landroid/view/View;

    .line 1809676
    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->a$redex0(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V

    .line 1809677
    :goto_0
    return-void

    .line 1809678
    :cond_0
    iget-object v0, p1, LX/4pL;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1809679
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1809680
    iget-object v1, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    iget-object v1, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dQ;->u:LX/0Tn;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1809681
    if-eqz v0, :cond_1

    .line 1809682
    iget-object v1, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    .line 1809683
    invoke-static {v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->l(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;)V

    .line 1809684
    :cond_1
    iget-object v0, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    iget-object v1, p0, LX/Bhn;->a:Landroid/view/View;

    iget-object v2, p0, LX/Bhn;->b:Landroid/view/View;

    .line 1809685
    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->a$redex0(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V

    .line 1809686
    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1809671
    iget-object v0, p0, LX/Bhn;->c:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    iget-object v1, p0, LX/Bhn;->a:Landroid/view/View;

    iget-object v2, p0, LX/Bhn;->b:Landroid/view/View;

    .line 1809672
    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;->a$redex0(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;Landroid/view/View;Landroid/view/View;)V

    .line 1809673
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1809670
    check-cast p1, LX/4pL;

    invoke-direct {p0, p1}, LX/Bhn;->a(LX/4pL;)V

    return-void
.end method
