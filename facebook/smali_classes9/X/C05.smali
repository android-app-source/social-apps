.class public LX/C05;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C06;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C05",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C06;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839872
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839873
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C05;->b:LX/0Zi;

    .line 1839874
    iput-object p1, p0, LX/C05;->a:LX/0Ot;

    .line 1839875
    return-void
.end method

.method public static a(LX/0QB;)LX/C05;
    .locals 4

    .prologue
    .line 1839876
    const-class v1, LX/C05;

    monitor-enter v1

    .line 1839877
    :try_start_0
    sget-object v0, LX/C05;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839878
    sput-object v2, LX/C05;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839879
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839880
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839881
    new-instance v3, LX/C05;

    const/16 p0, 0x1e57

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C05;-><init>(LX/0Ot;)V

    .line 1839882
    move-object v0, v3

    .line 1839883
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839884
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C05;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839885
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1839887
    check-cast p2, LX/C04;

    .line 1839888
    iget-object v0, p0, LX/C05;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C06;

    iget-object v1, p2, LX/C04;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C04;->b:LX/1Po;

    const/4 p0, 0x1

    .line 1839889
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1839890
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1839891
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 1839892
    invoke-static {v3}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    .line 1839893
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/C06;->a:LX/BzX;

    invoke-virtual {v6, p1}, LX/BzX;->c(LX/1De;)LX/BzV;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/BzV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzV;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/BzV;->a(LX/1Po;)LX/BzV;

    move-result-object v6

    .line 1839894
    iget-object v7, v6, LX/BzV;->a:LX/BzW;

    iput-boolean p0, v7, LX/BzW;->c:Z

    .line 1839895
    move-object v6, v6

    .line 1839896
    iget-object v7, v6, LX/BzV;->a:LX/BzW;

    iput-boolean p0, v7, LX/BzW;->d:Z

    .line 1839897
    move-object v6, v6

    .line 1839898
    iget-object v7, v0, LX/C06;->b:LX/1WM;

    invoke-virtual {v7}, LX/1WM;->a()LX/33B;

    move-result-object v7

    .line 1839899
    iget-object p2, v6, LX/BzV;->a:LX/BzW;

    iput-object v7, p2, LX/BzW;->e:LX/33B;

    .line 1839900
    move-object v6, v6

    .line 1839901
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/C06;->c:LX/Cch;

    invoke-virtual {v6, p1}, LX/Cch;->c(LX/1De;)LX/Ccf;

    move-result-object v6

    check-cast v2, LX/1Pq;

    invoke-virtual {v6, v2}, LX/Ccf;->a(LX/1Pq;)LX/Ccf;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/Ccf;->b(Ljava/lang/String;)LX/Ccf;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Ccf;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Ccf;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v4

    invoke-virtual {v3, v4}, LX/Ccf;->a(Z)LX/Ccf;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    const/4 v6, 0x0

    invoke-interface {v3, v4, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const v6, 0x7f0b00d3

    invoke-interface {v3, v4, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1839902
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1839903
    invoke-static {}, LX/1dS;->b()V

    .line 1839904
    const/4 v0, 0x0

    return-object v0
.end method
