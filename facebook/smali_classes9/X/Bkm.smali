.class public LX/Bkm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static h:I

.field public static i:I

.field public static j:I


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/facebook/events/create/ui/CoverPhotoSelector;

.field public c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/FrameLayout;

.field private e:Landroid/animation/ValueAnimator;

.field private f:Landroid/animation/ObjectAnimator;

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/events/create/ui/CoverPhotoSelector;Landroid/widget/LinearLayout;Landroid/widget/FrameLayout;I)V
    .locals 6

    .prologue
    .line 1814900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814901
    iput-object p1, p0, LX/Bkm;->a:Landroid/content/Context;

    .line 1814902
    iput-object p2, p0, LX/Bkm;->b:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    .line 1814903
    iput-object p3, p0, LX/Bkm;->c:Landroid/widget/LinearLayout;

    .line 1814904
    iput-object p4, p0, LX/Bkm;->d:Landroid/widget/FrameLayout;

    .line 1814905
    iput p5, p0, LX/Bkm;->g:I

    .line 1814906
    iget-object v0, p0, LX/Bkm;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1814907
    const v1, 0x7f0b1547

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, LX/Bkm;->i:I

    .line 1814908
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v2, v1

    const-wide v4, 0x3ffc7ae147ae147bL    # 1.78

    div-double/2addr v2, v4

    double-to-int v1, v2

    const v2, 0x7f0b154f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 1814909
    sput v0, LX/Bkm;->j:I

    sget v1, LX/Bkm;->i:I

    sub-int/2addr v0, v1

    sput v0, LX/Bkm;->h:I

    .line 1814910
    return-void
.end method

.method public static d(LX/Bkm;)Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    .line 1814881
    iget-object v0, p0, LX/Bkm;->f:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 1814882
    iget-object v0, p0, LX/Bkm;->d:Landroid/widget/FrameLayout;

    const-string v1, "y"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    sget v4, LX/Bkm;->h:I

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Bkm;->f:Landroid/animation/ObjectAnimator;

    .line 1814883
    iget-object v0, p0, LX/Bkm;->f:Landroid/animation/ObjectAnimator;

    iget v1, p0, LX/Bkm;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1814884
    :cond_0
    iget-object v0, p0, LX/Bkm;->f:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public static e(LX/Bkm;)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 1814894
    iget-object v0, p0, LX/Bkm;->e:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 1814895
    iget-object v0, p0, LX/Bkm;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1814896
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, LX/Bkm;->i:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, LX/Bkm;->j:I

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, LX/Bkm;->e:Landroid/animation/ValueAnimator;

    .line 1814897
    iget-object v1, p0, LX/Bkm;->e:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Bkl;

    invoke-direct {v2, p0, v0}, LX/Bkl;-><init>(LX/Bkm;Landroid/widget/FrameLayout$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1814898
    iget-object v0, p0, LX/Bkm;->e:Landroid/animation/ValueAnimator;

    iget v1, p0, LX/Bkm;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1814899
    :cond_0
    iget-object v0, p0, LX/Bkm;->e:Landroid/animation/ValueAnimator;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1814890
    iget-object v0, p0, LX/Bkm;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1814891
    if-eqz p1, :cond_0

    sget v1, LX/Bkm;->j:I

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1814892
    return-void

    .line 1814893
    :cond_0
    sget v1, LX/Bkm;->i:I

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1814885
    iget-object v0, p0, LX/Bkm;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1814886
    if-eqz p1, :cond_0

    .line 1814887
    sget v1, LX/Bkm;->h:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1814888
    :goto_0
    return-void

    .line 1814889
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_0
.end method
