.class public final LX/BTR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:I

.field private e:I


# direct methods
.method public constructor <init>(LX/0Px;III)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 1787399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787400
    iput-object p1, p0, LX/BTR;->a:LX/0Px;

    .line 1787401
    iput p2, p0, LX/BTR;->d:I

    .line 1787402
    iput p3, p0, LX/BTR;->e:I

    .line 1787403
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    move v2, p2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    .line 1787404
    iget-object v4, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v2

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 1787405
    iget-object v4, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    iput v2, v4, Landroid/graphics/Rect;->right:I

    .line 1787406
    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    .line 1787407
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1787408
    :cond_0
    if-lez p4, :cond_2

    .line 1787409
    iput p4, p0, LX/BTR;->b:I

    .line 1787410
    iget v0, p0, LX/BTR;->b:I

    sub-int v1, v0, p3

    .line 1787411
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    .line 1787412
    iget v2, v0, Landroid/graphics/Rect;->left:I

    if-le v1, v2, :cond_1

    .line 1787413
    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1787414
    :cond_1
    :goto_1
    iget v0, p0, LX/BTR;->b:I

    iget v1, p0, LX/BTR;->d:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/BTR;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/BTR;->c:I

    .line 1787415
    return-void

    .line 1787416
    :cond_2
    add-int v0, v2, p2

    iput v0, p0, LX/BTR;->b:I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/1aZ;
    .locals 1

    .prologue
    .line 1787417
    iget-object v0, p0, LX/BTR;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->d:LX/1aZ;

    return-object v0
.end method

.method public final a(ILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1787418
    iget-object v0, p0, LX/BTR;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1787419
    return-void
.end method
