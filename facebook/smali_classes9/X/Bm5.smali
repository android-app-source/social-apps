.class public final LX/Bm5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:LX/Bm7;


# direct methods
.method public constructor <init>(LX/Bm7;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1817827
    iput-object p1, p0, LX/Bm5;->c:LX/Bm7;

    iput-object p2, p0, LX/Bm5;->a:Ljava/lang/String;

    iput p3, p0, LX/Bm5;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1817828
    invoke-static {}, LX/7oV;->e()LX/7oI;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1817829
    new-instance v1, LX/7oI;

    invoke-direct {v1}, LX/7oI;-><init>()V

    const-string v2, "event_id"

    iget-object v3, p0, LX/Bm5;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget v3, p0, LX/Bm5;->b:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_portrait_size"

    iget-object v3, p0, LX/Bm5;->c:LX/Bm7;

    iget-object v3, v3, LX/Bm7;->e:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_landscape_size"

    iget-object v3, p0, LX/Bm5;->c:LX/Bm7;

    iget-object v3, v3, LX/Bm7;->e:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    .line 1817830
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 1817831
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 1817832
    iget-object v1, p0, LX/Bm5;->c:LX/Bm7;

    iget-object v1, v1, LX/Bm7;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
