.class public final enum LX/BVw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVw;

.field public static final enum GRAVITY:LX/BVw;

.field public static final enum WEIGHT:LX/BVw;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1791521
    new-instance v0, LX/BVw;

    const-string v1, "GRAVITY"

    const-string v2, "gravity"

    invoke-direct {v0, v1, v3, v2}, LX/BVw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVw;->GRAVITY:LX/BVw;

    .line 1791522
    new-instance v0, LX/BVw;

    const-string v1, "WEIGHT"

    const-string v2, "weight"

    invoke-direct {v0, v1, v4, v2}, LX/BVw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVw;->WEIGHT:LX/BVw;

    .line 1791523
    const/4 v0, 0x2

    new-array v0, v0, [LX/BVw;

    sget-object v1, LX/BVw;->GRAVITY:LX/BVw;

    aput-object v1, v0, v3

    sget-object v1, LX/BVw;->WEIGHT:LX/BVw;

    aput-object v1, v0, v4

    sput-object v0, LX/BVw;->$VALUES:[LX/BVw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791533
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791534
    iput-object p3, p0, LX/BVw;->mValue:Ljava/lang/String;

    .line 1791535
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVw;
    .locals 4

    .prologue
    .line 1791526
    invoke-static {}, LX/BVw;->values()[LX/BVw;

    move-result-object v2

    .line 1791527
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 1791528
    aget-object v1, v2, v0

    .line 1791529
    iget-object v3, v1, LX/BVw;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1791530
    :goto_1
    return-object v0

    .line 1791531
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791532
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVw;
    .locals 1

    .prologue
    .line 1791525
    const-class v0, LX/BVw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVw;

    return-object v0
.end method

.method public static values()[LX/BVw;
    .locals 1

    .prologue
    .line 1791524
    sget-object v0, LX/BVw;->$VALUES:[LX/BVw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVw;

    return-object v0
.end method
