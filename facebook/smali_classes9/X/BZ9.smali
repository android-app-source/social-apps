.class public LX/BZ9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private A:Z

.field private B:F

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:[B

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:F

.field private K:F

.field private L:F

.field private M:F

.field private N:F

.field private O:F

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:[LX/BZ9;

.field private k:F

.field private l:F

.field private m:Ljava/lang/String;

.field private mX:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "X"
    .end annotation
.end field

.field private mY:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "Y"
    .end annotation
.end field

.field private mZ:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "Z"
    .end annotation
.end field

.field private n:I

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:F

.field private u:F

.field private v:I

.field private w:I

.field private x:F

.field private y:F

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBackgroundInfluence()F
    .locals 1

    .prologue
    .line 1796886
    iget v0, p0, LX/BZ9;->l:F

    return v0
.end method

.method public getChildren()[LX/BZ9;
    .locals 1

    .prologue
    .line 1796885
    iget-object v0, p0, LX/BZ9;->j:[LX/BZ9;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 1796884
    iget v0, p0, LX/BZ9;->n:I

    return v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 1796883
    iget-object v0, p0, LX/BZ9;->F:[B

    return-object v0
.end method

.method public getDiffuseTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796882
    iget-object v0, p0, LX/BZ9;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getDoubleSided()Z
    .locals 1

    .prologue
    .line 1796881
    iget-boolean v0, p0, LX/BZ9;->E:Z

    return v0
.end method

.method public getEqualScreenHeight()Z
    .locals 1

    .prologue
    .line 1796880
    iget-boolean v0, p0, LX/BZ9;->z:Z

    return v0
.end method

.method public getEqualScreenWidth()Z
    .locals 1

    .prologue
    .line 1796879
    iget-boolean v0, p0, LX/BZ9;->A:Z

    return v0
.end method

.method public getFaceId()I
    .locals 1

    .prologue
    .line 1796866
    iget v0, p0, LX/BZ9;->s:I

    return v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796877
    iget-object v0, p0, LX/BZ9;->I:Ljava/lang/String;

    return-object v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796876
    iget-object v0, p0, LX/BZ9;->H:Ljava/lang/String;

    return-object v0
.end method

.method public getHidesSceneWhenNoFaces()Z
    .locals 1

    .prologue
    .line 1796875
    iget-boolean v0, p0, LX/BZ9;->h:Z

    return v0
.end method

.method public getHorizontalAlignment()I
    .locals 1

    .prologue
    .line 1796874
    iget v0, p0, LX/BZ9;->v:I

    return v0
.end method

.method public getHorizontalSpacing()F
    .locals 1

    .prologue
    .line 1796873
    iget v0, p0, LX/BZ9;->x:F

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796872
    iget-object v0, p0, LX/BZ9;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796871
    iget-object v0, p0, LX/BZ9;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicesType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796870
    iget-object v0, p0, LX/BZ9;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getMaterialIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796869
    iget-object v0, p0, LX/BZ9;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getMaximumSupportedNumberOfFaces()I
    .locals 1

    .prologue
    .line 1796868
    iget v0, p0, LX/BZ9;->f:I

    return v0
.end method

.method public getMinimumSupportedNumberOfFaces()I
    .locals 1

    .prologue
    .line 1796867
    iget v0, p0, LX/BZ9;->g:I

    return v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796890
    iget-object v0, p0, LX/BZ9;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796889
    iget-object v0, p0, LX/BZ9;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getNormalTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796909
    iget-object v0, p0, LX/BZ9;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getPaintBrightness()F
    .locals 1

    .prologue
    .line 1796908
    iget v0, p0, LX/BZ9;->k:F

    return v0
.end method

.method public getReadDepth()Z
    .locals 1

    .prologue
    .line 1796907
    iget-boolean v0, p0, LX/BZ9;->C:Z

    return v0
.end method

.method public getRotationX()F
    .locals 1

    .prologue
    .line 1796906
    iget v0, p0, LX/BZ9;->J:F

    return v0
.end method

.method public getRotationY()F
    .locals 1

    .prologue
    .line 1796905
    iget v0, p0, LX/BZ9;->K:F

    return v0
.end method

.method public getRotationZ()F
    .locals 1

    .prologue
    .line 1796904
    iget v0, p0, LX/BZ9;->L:F

    return v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 1796903
    iget v0, p0, LX/BZ9;->M:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 1796902
    iget v0, p0, LX/BZ9;->N:F

    return v0
.end method

.method public getScaleZ()F
    .locals 1

    .prologue
    .line 1796901
    iget v0, p0, LX/BZ9;->O:F

    return v0
.end method

.method public getSourcePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796910
    iget-object v0, p0, LX/BZ9;->G:Ljava/lang/String;

    return-object v0
.end method

.method public getSpecularTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796900
    iget-object v0, p0, LX/BZ9;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getTextureHeight()F
    .locals 1

    .prologue
    .line 1796899
    iget v0, p0, LX/BZ9;->u:F

    return v0
.end method

.method public getTextureWidth()F
    .locals 1

    .prologue
    .line 1796898
    iget v0, p0, LX/BZ9;->t:F

    return v0
.end method

.method public getTrackerType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796897
    iget-object v0, p0, LX/BZ9;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTransparency()F
    .locals 1

    .prologue
    .line 1796896
    iget v0, p0, LX/BZ9;->B:F

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796895
    iget-object v0, p0, LX/BZ9;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getVerticalAlignment()I
    .locals 1

    .prologue
    .line 1796894
    iget v0, p0, LX/BZ9;->w:I

    return v0
.end method

.method public getVerticalSpacing()F
    .locals 1

    .prologue
    .line 1796893
    iget v0, p0, LX/BZ9;->y:F

    return v0
.end method

.method public getWriteDepth()Z
    .locals 1

    .prologue
    .line 1796892
    iget-boolean v0, p0, LX/BZ9;->D:Z

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 1796891
    iget v0, p0, LX/BZ9;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 1796878
    iget v0, p0, LX/BZ9;->mY:F

    return v0
.end method

.method public getZ()F
    .locals 1

    .prologue
    .line 1796888
    iget v0, p0, LX/BZ9;->mZ:F

    return v0
.end method

.method public setBackgroundInfluence(F)V
    .locals 0

    .prologue
    .line 1796780
    iput p1, p0, LX/BZ9;->l:F

    .line 1796781
    return-void
.end method

.method public setChildren([LX/BZ9;)V
    .locals 0

    .prologue
    .line 1796818
    iput-object p1, p0, LX/BZ9;->j:[LX/BZ9;

    .line 1796819
    return-void
.end method

.method public setColor(I)V
    .locals 0

    .prologue
    .line 1796816
    iput p1, p0, LX/BZ9;->n:I

    .line 1796817
    return-void
.end method

.method public setData([B)V
    .locals 0

    .prologue
    .line 1796814
    iput-object p1, p0, LX/BZ9;->F:[B

    .line 1796815
    return-void
.end method

.method public setDiffuseTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796812
    iput-object p1, p0, LX/BZ9;->m:Ljava/lang/String;

    .line 1796813
    return-void
.end method

.method public setDoubleSided(Z)V
    .locals 0

    .prologue
    .line 1796810
    iput-boolean p1, p0, LX/BZ9;->E:Z

    .line 1796811
    return-void
.end method

.method public setEqualScreenHeight(Z)V
    .locals 0

    .prologue
    .line 1796808
    iput-boolean p1, p0, LX/BZ9;->z:Z

    .line 1796809
    return-void
.end method

.method public setEqualScreenWidth(Z)V
    .locals 0

    .prologue
    .line 1796806
    iput-boolean p1, p0, LX/BZ9;->A:Z

    .line 1796807
    return-void
.end method

.method public setFaceId(I)V
    .locals 0

    .prologue
    .line 1796804
    iput p1, p0, LX/BZ9;->s:I

    .line 1796805
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796802
    iput-object p1, p0, LX/BZ9;->I:Ljava/lang/String;

    .line 1796803
    return-void
.end method

.method public setHash(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796800
    iput-object p1, p0, LX/BZ9;->H:Ljava/lang/String;

    .line 1796801
    return-void
.end method

.method public setHidesSceneWhenNoFaces(Z)V
    .locals 0

    .prologue
    .line 1796798
    iput-boolean p1, p0, LX/BZ9;->h:Z

    .line 1796799
    return-void
.end method

.method public setHorizontalAlignment(I)V
    .locals 0

    .prologue
    .line 1796796
    iput p1, p0, LX/BZ9;->v:I

    .line 1796797
    return-void
.end method

.method public setHorizontalSpacing(F)V
    .locals 0

    .prologue
    .line 1796794
    iput p1, p0, LX/BZ9;->x:F

    .line 1796795
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796792
    iput-object p1, p0, LX/BZ9;->a:Ljava/lang/String;

    .line 1796793
    return-void
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796790
    iput-object p1, p0, LX/BZ9;->e:Ljava/lang/String;

    .line 1796791
    return-void
.end method

.method public setIndicesType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796788
    iput-object p1, p0, LX/BZ9;->q:Ljava/lang/String;

    .line 1796789
    return-void
.end method

.method public setMaterialIdentifier(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796786
    iput-object p1, p0, LX/BZ9;->r:Ljava/lang/String;

    .line 1796787
    return-void
.end method

.method public setMaximumSupportedNumberOfFaces(I)V
    .locals 0

    .prologue
    .line 1796784
    iput p1, p0, LX/BZ9;->f:I

    .line 1796785
    return-void
.end method

.method public setMinimumSupportedNumberOfFaces(I)V
    .locals 0

    .prologue
    .line 1796782
    iput p1, p0, LX/BZ9;->g:I

    .line 1796783
    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796778
    iput-object p1, p0, LX/BZ9;->b:Ljava/lang/String;

    .line 1796779
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796844
    iput-object p1, p0, LX/BZ9;->d:Ljava/lang/String;

    .line 1796845
    return-void
.end method

.method public setNormalTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796864
    iput-object p1, p0, LX/BZ9;->o:Ljava/lang/String;

    .line 1796865
    return-void
.end method

.method public setPaintBrightness(F)V
    .locals 0

    .prologue
    .line 1796862
    iput p1, p0, LX/BZ9;->k:F

    .line 1796863
    return-void
.end method

.method public setReadDepth(Z)V
    .locals 0

    .prologue
    .line 1796860
    iput-boolean p1, p0, LX/BZ9;->C:Z

    .line 1796861
    return-void
.end method

.method public setRotationX(F)V
    .locals 0

    .prologue
    .line 1796858
    iput p1, p0, LX/BZ9;->J:F

    .line 1796859
    return-void
.end method

.method public setRotationY(F)V
    .locals 0

    .prologue
    .line 1796856
    iput p1, p0, LX/BZ9;->K:F

    .line 1796857
    return-void
.end method

.method public setRotationZ(F)V
    .locals 0

    .prologue
    .line 1796854
    iput p1, p0, LX/BZ9;->L:F

    .line 1796855
    return-void
.end method

.method public setScaleX(F)V
    .locals 0

    .prologue
    .line 1796852
    iput p1, p0, LX/BZ9;->M:F

    .line 1796853
    return-void
.end method

.method public setScaleY(F)V
    .locals 0

    .prologue
    .line 1796850
    iput p1, p0, LX/BZ9;->N:F

    .line 1796851
    return-void
.end method

.method public setScaleZ(F)V
    .locals 0

    .prologue
    .line 1796848
    iput p1, p0, LX/BZ9;->O:F

    .line 1796849
    return-void
.end method

.method public setSourcePath(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796846
    iput-object p1, p0, LX/BZ9;->G:Ljava/lang/String;

    .line 1796847
    return-void
.end method

.method public setSpecularTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796822
    iput-object p1, p0, LX/BZ9;->p:Ljava/lang/String;

    .line 1796823
    return-void
.end method

.method public setTextureHeight(F)V
    .locals 0

    .prologue
    .line 1796842
    iput p1, p0, LX/BZ9;->u:F

    .line 1796843
    return-void
.end method

.method public setTextureWidth(F)V
    .locals 0

    .prologue
    .line 1796840
    iput p1, p0, LX/BZ9;->t:F

    .line 1796841
    return-void
.end method

.method public setTrackerType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796838
    iput-object p1, p0, LX/BZ9;->c:Ljava/lang/String;

    .line 1796839
    return-void
.end method

.method public setTransparency(F)V
    .locals 0

    .prologue
    .line 1796836
    iput p1, p0, LX/BZ9;->B:F

    .line 1796837
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796834
    iput-object p1, p0, LX/BZ9;->i:Ljava/lang/String;

    .line 1796835
    return-void
.end method

.method public setVerticalAlignment(I)V
    .locals 0

    .prologue
    .line 1796832
    iput p1, p0, LX/BZ9;->w:I

    .line 1796833
    return-void
.end method

.method public setVerticalSpacing(F)V
    .locals 0

    .prologue
    .line 1796830
    iput p1, p0, LX/BZ9;->y:F

    .line 1796831
    return-void
.end method

.method public setWriteDepth(Z)V
    .locals 0

    .prologue
    .line 1796828
    iput-boolean p1, p0, LX/BZ9;->D:Z

    .line 1796829
    return-void
.end method

.method public setX(F)V
    .locals 0

    .prologue
    .line 1796826
    iput p1, p0, LX/BZ9;->mX:F

    .line 1796827
    return-void
.end method

.method public setY(F)V
    .locals 0

    .prologue
    .line 1796824
    iput p1, p0, LX/BZ9;->mY:F

    .line 1796825
    return-void
.end method

.method public setZ(F)V
    .locals 0

    .prologue
    .line 1796820
    iput p1, p0, LX/BZ9;->mZ:F

    .line 1796821
    return-void
.end method
