.class public final LX/Bpw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)V
    .locals 0

    .prologue
    .line 1823668
    iput-object p1, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iput p2, p0, LX/Bpw;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 1823669
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1823670
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    .line 1823671
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 1823672
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    iget v1, p0, LX/Bpw;->a:I

    if-le v0, v1, :cond_0

    .line 1823673
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    .line 1823674
    :goto_0
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1823675
    return-void

    .line 1823676
    :cond_0
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget v2, p0, LX/Bpw;->a:I

    .line 1823677
    new-instance v3, LX/BqC;

    iget-object v4, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    new-instance v5, LX/Bpx;

    invoke-direct {v5, v1, v2}, LX/Bpx;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)V

    invoke-direct {v3, v4, v2, v5}, LX/BqC;-><init>(LX/2kW;ILX/Bpx;)V

    move-object v1, v3

    .line 1823678
    iput-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Z:LX/BqC;

    .line 1823679
    iget-object v0, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    iget-object v1, p0, LX/Bpw;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Z:LX/BqC;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    goto :goto_0
.end method
