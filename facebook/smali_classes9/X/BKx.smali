.class public LX/BKx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private a:Landroid/text/TextPaint;

.field private b:Landroid/content/res/Resources;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:LX/8Rp;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/8Rp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774786
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, LX/BKx;->a:Landroid/text/TextPaint;

    .line 1774787
    const v0, 0x7f02197d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/BKx;->c:Landroid/graphics/drawable/Drawable;

    .line 1774788
    iput-object p1, p0, LX/BKx;->b:Landroid/content/res/Resources;

    .line 1774789
    iput-object p2, p0, LX/BKx;->d:LX/8Rp;

    .line 1774790
    return-void
.end method

.method public static a(LX/0QB;)LX/BKx;
    .locals 5

    .prologue
    .line 1774791
    const-class v1, LX/BKx;

    monitor-enter v1

    .line 1774792
    :try_start_0
    sget-object v0, LX/BKx;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1774793
    sput-object v2, LX/BKx;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1774794
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1774795
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1774796
    new-instance p0, LX/BKx;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v4

    check-cast v4, LX/8Rp;

    invoke-direct {p0, v3, v4}, LX/BKx;-><init>(Landroid/content/res/Resources;LX/8Rp;)V

    .line 1774797
    move-object v0, p0

    .line 1774798
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1774799
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BKx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1774800
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1774801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/0Px;FIZ)Landroid/text/SpannableStringBuilder;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;FIZ)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 1774740
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1774741
    iget-object v0, p0, LX/BKx;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b0b86

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, LX/BKx;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    .line 1774742
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sub-int v0, p4, v0

    .line 1774743
    if-lez v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v3

    .line 1774744
    :goto_0
    return-object v0

    .line 1774745
    :cond_1
    iget-object v1, p0, LX/BKx;->a:Landroid/text/TextPaint;

    invoke-virtual {v1, p3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1774746
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_8

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QL;

    .line 1774747
    iget-object v5, v1, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v5, v6, :cond_7

    .line 1774748
    :goto_2
    move-object v1, v1

    .line 1774749
    if-eqz v1, :cond_6

    .line 1774750
    iget-object v2, p0, LX/BKx;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, LX/8QL;->d()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    move v1, v0

    .line 1774751
    :goto_3
    invoke-virtual {p2}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1774752
    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1774753
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1774754
    const/4 v2, 0x0

    .line 1774755
    if-eqz p5, :cond_2

    .line 1774756
    iget-object v2, p0, LX/BKx;->b:Landroid/content/res/Resources;

    const v6, 0x7f0a010e

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1774757
    iget-object v6, p0, LX/BKx;->a:Landroid/text/TextPaint;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 1774758
    :goto_5
    new-instance v6, LX/8uj;

    invoke-direct {v6}, LX/8uj;-><init>()V

    .line 1774759
    iput-object v0, v6, LX/8uj;->a:LX/8QL;

    .line 1774760
    move-object v0, v6

    .line 1774761
    invoke-virtual {v0, v1}, LX/8uj;->a(I)LX/8uj;

    move-result-object v0

    iget-object v6, p0, LX/BKx;->a:Landroid/text/TextPaint;

    .line 1774762
    iput-object v6, v0, LX/8uj;->b:Landroid/text/TextPaint;

    .line 1774763
    move-object v0, v0

    .line 1774764
    iget-object v6, p0, LX/BKx;->b:Landroid/content/res/Resources;

    .line 1774765
    iput-object v6, v0, LX/8uj;->c:Landroid/content/res/Resources;

    .line 1774766
    move-object v0, v0

    .line 1774767
    iget-object v6, p0, LX/BKx;->c:Landroid/graphics/drawable/Drawable;

    .line 1774768
    iput-object v6, v0, LX/8uj;->e:Landroid/graphics/drawable/Drawable;

    .line 1774769
    move-object v0, v0

    .line 1774770
    iput-object v2, v0, LX/8uj;->h:Ljava/lang/Integer;

    .line 1774771
    move-object v0, v0

    .line 1774772
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/8uj;->b(I)LX/8uj;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    invoke-virtual {v0, v2}, LX/8uj;->a(Z)LX/8uj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/8uj;->a(Landroid/content/Context;)LX/8ul;

    move-result-object v0

    .line 1774773
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1774774
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v3, v0, v2, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    .line 1774775
    :cond_2
    invoke-virtual {v0}, LX/8QK;->a()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1774776
    iget-object v6, p0, LX/BKx;->a:Landroid/text/TextPaint;

    iget-object v7, p0, LX/BKx;->b:Landroid/content/res/Resources;

    const v8, 0x7f0a00e7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    .line 1774777
    :cond_3
    iget-boolean v6, v0, LX/8QK;->c:Z

    move v6, v6

    .line 1774778
    if-eqz v6, :cond_4

    .line 1774779
    iget-object v6, p0, LX/BKx;->a:Landroid/text/TextPaint;

    iget-object v7, p0, LX/BKx;->b:Landroid/content/res/Resources;

    const v8, 0x7f0a00d5

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    .line 1774780
    :cond_4
    iget-object v2, p0, LX/BKx;->b:Landroid/content/res/Resources;

    const v6, 0x7f0a00d2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1774781
    iget-object v6, p0, LX/BKx;->a:Landroid/text/TextPaint;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_5

    :cond_5
    move-object v0, v3

    .line 1774782
    goto/16 :goto_0

    :cond_6
    move v1, v0

    goto/16 :goto_3

    .line 1774783
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 1774784
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2
.end method
