.class public final LX/BsA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/model/GraphQLPage;

.field private final c:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;LX/1Pd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1827167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827168
    iput-object p1, p0, LX/BsA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827169
    iput-object p2, p0, LX/BsA;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1827170
    iput-object p3, p0, LX/BsA;->c:LX/1Pd;

    .line 1827171
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x778706ae

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1827172
    iget-object v0, p0, LX/BsA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827173
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1827174
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827175
    iget-object v1, p0, LX/BsA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1827176
    iget-object v1, p0, LX/BsA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827177
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    .line 1827178
    if-eqz v4, :cond_3

    instance-of v5, v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v5, :cond_3

    .line 1827179
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1827180
    :goto_0
    move-object v4, v4

    .line 1827181
    iget-object v5, p0, LX/BsA;->c:LX/1Pd;

    iget-object v1, p0, LX/BsA;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "sponsored_story"

    :goto_2
    invoke-interface {v5, v4, v6, v1, v0}, LX/1Pd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1827182
    const v0, 0x2ab44e4e

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1827183
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "feed_story"

    goto :goto_2

    :cond_3
    invoke-static {v1}, LX/182;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    goto :goto_0
.end method
