.class public LX/BtN;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1829020
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BtN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829021
    invoke-direct {p0}, LX/BtN;->a()V

    .line 1829022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1829023
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829024
    invoke-direct {p0}, LX/BtN;->a()V

    .line 1829025
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1829026
    const v0, 0x7f03064a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1829027
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/BtN;->setOrientation(I)V

    .line 1829028
    const v0, 0x7f0d1162

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BtN;->a:Landroid/widget/TextView;

    .line 1829029
    const v0, 0x7f0d1163

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BtN;->b:Landroid/widget/TextView;

    .line 1829030
    const v0, 0x7f0d1164

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BtN;->c:Landroid/widget/TextView;

    .line 1829031
    return-void
.end method
