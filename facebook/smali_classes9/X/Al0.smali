.class public LX/Al0;
.super LX/1SX;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1709396
    new-instance v0, LX/Aky;

    invoke-direct {v0}, LX/Aky;-><init>()V

    sput-object v0, LX/Al0;->a:LX/0Or;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V
    .locals 41
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedPrivacyEditingEnabled;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p22    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709397
    sget-object v17, LX/Al0;->a:LX/0Or;

    const/16 v29, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    move-object/from16 v22, p20

    move-object/from16 v23, p21

    move-object/from16 v24, p22

    move-object/from16 v25, p23

    move-object/from16 v26, p24

    move-object/from16 v27, p25

    move-object/from16 v28, p26

    move-object/from16 v30, p27

    move-object/from16 v31, p28

    move-object/from16 v32, p29

    move-object/from16 v33, p30

    move-object/from16 v34, p31

    move-object/from16 v35, p32

    move-object/from16 v36, p33

    move-object/from16 v40, p34

    invoke-direct/range {v1 .. v40}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 1709398
    new-instance v1, LX/Akz;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, LX/Akz;-><init>(LX/Al0;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/Al0;->b:LX/1Sp;

    .line 1709399
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(Ljava/lang/String;)V

    .line 1709400
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Al0;->b:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(LX/1Sp;)V

    .line 1709401
    return-void
.end method

.method public static b(LX/0QB;)LX/Al0;
    .locals 37

    .prologue
    .line 1709402
    new-instance v2, LX/Al0;

    const/16 v3, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 v7, 0x236a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v9

    check-cast v9, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v10

    check-cast v10, LX/1Sj;

    const/16 v11, 0x327a

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v12

    check-cast v12, LX/16H;

    const/16 v13, 0xde

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v14

    check-cast v14, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v15

    check-cast v15, LX/0bH;

    const/16 v16, 0x14a1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x14a0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v18

    check-cast v18, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v19

    check-cast v19, LX/17Q;

    const/16 v20, 0x2fd

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x12c4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v22

    check-cast v22, LX/0SG;

    const/16 v23, 0x2fd7

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    const/16 v24, 0x14a2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    const/16 v25, 0x31d5

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v26

    check-cast v26, LX/14w;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v27

    check-cast v27, LX/0ad;

    const/16 v28, 0x1399

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v29

    check-cast v29, LX/0qn;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v30

    check-cast v30, LX/0W3;

    const/16 v31, 0x31aa

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x122d

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v32

    invoke-static/range {p0 .. p0}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v33

    check-cast v33, LX/1Sl;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v34

    check-cast v34, LX/0tX;

    const-class v35, LX/1Sm;

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v35

    check-cast v35, LX/1Sm;

    invoke-static/range {p0 .. p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v36

    check-cast v36, LX/0wL;

    invoke-direct/range {v2 .. v36}, LX/Al0;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V

    .line 1709403
    return-object v2
.end method
