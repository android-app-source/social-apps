.class public final LX/Aln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/Ale;


# direct methods
.method public constructor <init>(LX/Ale;)V
    .locals 0

    .prologue
    .line 1710268
    iput-object p1, p0, LX/Aln;->a:LX/Ale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1710257
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1710261
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    iget-object v0, v0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1710262
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    iget-object v0, v0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/Aln;->a:LX/Ale;

    iget v1, v1, LX/Ale;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setTranslationX(F)V

    .line 1710263
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    invoke-virtual {v0, v2}, LX/Ale;->setIsAnimationRunning(Z)V

    .line 1710264
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    invoke-virtual {v0, v2}, LX/Ale;->setHasBeenShown(Z)V

    .line 1710265
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    .line 1710266
    iput-boolean v2, v0, LX/Ale;->i:Z

    .line 1710267
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1710260
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1710258
    iget-object v0, p0, LX/Aln;->a:LX/Ale;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Ale;->setIsAnimationRunning(Z)V

    .line 1710259
    return-void
.end method
