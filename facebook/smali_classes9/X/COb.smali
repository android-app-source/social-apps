.class public final LX/COb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1883303
    invoke-static {}, LX/COc;->q()LX/COc;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883304
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883305
    const-string v0, "NTUFIActionBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1883306
    if-ne p0, p1, :cond_1

    .line 1883307
    :cond_0
    :goto_0
    return v0

    .line 1883308
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883309
    goto :goto_0

    .line 1883310
    :cond_3
    check-cast p1, LX/COb;

    .line 1883311
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883312
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883313
    if-eq v2, v3, :cond_0

    .line 1883314
    iget-object v2, p0, LX/COb;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/COb;->a:LX/CNb;

    iget-object v3, p1, LX/COb;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1883315
    goto :goto_0

    .line 1883316
    :cond_5
    iget-object v2, p1, LX/COb;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1883317
    :cond_6
    iget-object v2, p0, LX/COb;->b:LX/CNc;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/COb;->b:LX/CNc;

    iget-object v3, p1, LX/COb;->b:LX/CNc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1883318
    goto :goto_0

    .line 1883319
    :cond_7
    iget-object v2, p1, LX/COb;->b:LX/CNc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
