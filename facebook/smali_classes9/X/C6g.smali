.class public LX/C6g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;

.field public final b:LX/0tX;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1850139
    iput-object p1, p0, LX/C6g;->a:LX/1Ck;

    .line 1850140
    iput-object p2, p0, LX/C6g;->b:LX/0tX;

    .line 1850141
    iput-object p3, p0, LX/C6g;->c:Ljava/lang/String;

    .line 1850142
    return-void
.end method

.method public static b(LX/0QB;)LX/C6g;
    .locals 4

    .prologue
    .line 1850143
    new-instance v3, LX/C6g;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/C6g;-><init>(LX/1Ck;LX/0tX;Ljava/lang/String;)V

    .line 1850144
    return-object v3
.end method
