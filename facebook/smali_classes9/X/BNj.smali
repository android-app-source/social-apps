.class public final LX/BNj;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1778927
    const-class v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;

    const v0, 0x1b0a9922

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FetchUpdatedPageReviewQuery"

    const-string v6, "bec257ff30b0c720d61ddbe5e88a7524"

    const-string v7, "page"

    const-string v8, "10155069966096729"

    const/4 v9, 0x0

    .line 1778928
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1778929
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1778930
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1778931
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1778932
    sparse-switch v0, :sswitch_data_0

    .line 1778933
    :goto_0
    return-object p1

    .line 1778934
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1778935
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1778936
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2fe52f35 -> :sswitch_0
        0x20b244e5 -> :sswitch_1
        0x3cbbc973 -> :sswitch_2
    .end sparse-switch
.end method
