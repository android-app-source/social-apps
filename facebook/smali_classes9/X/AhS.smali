.class public final LX/AhS;
.super LX/3ut;
.source ""


# instance fields
.field private g:Landroid/content/Context;

.field public h:LX/AhM;

.field public i:I

.field public j:I

.field public k:Landroid/content/res/ColorStateList;

.field private l:LX/AhP;

.field public m:I

.field public n:Z

.field private o:I

.field public p:I

.field public q:I

.field public r:LX/AhR;

.field public s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3ux;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1702978
    invoke-direct {p0, p1, v0, v0}, LX/3ut;-><init>(Landroid/content/Context;II)V

    .line 1702979
    iput-object p1, p0, LX/AhS;->g:Landroid/content/Context;

    .line 1702980
    iput-object p2, p0, LX/AhS;->f:LX/3ux;

    .line 1702981
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)LX/3ux;
    .locals 1

    .prologue
    .line 1702977
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    return-object v0
.end method

.method public final a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1702967
    invoke-virtual {p1}, LX/3v3;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 1702968
    if-nez v0, :cond_1

    .line 1702969
    instance-of v0, p2, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    if-nez v0, :cond_0

    .line 1702970
    const/4 p2, 0x0

    .line 1702971
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1702972
    :cond_1
    check-cast p3, LX/AhO;

    .line 1702973
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1702974
    invoke-virtual {p3, v1}, LX/AhO;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1702975
    invoke-virtual {p3}, LX/AhO;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1702976
    :cond_2
    return-object v0
.end method

.method public final a(LX/3v0;Z)V
    .locals 0

    .prologue
    .line 1702964
    invoke-virtual {p0}, LX/AhS;->e()Z

    .line 1702965
    invoke-super {p0, p1, p2}, LX/3ut;->a(LX/3v0;Z)V

    .line 1702966
    return-void
.end method

.method public final a(LX/3v3;LX/3uq;)V
    .locals 2

    .prologue
    .line 1702955
    check-cast p2, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    .line 1702956
    invoke-virtual {p2, p1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->a(LX/3v3;)V

    .line 1702957
    iget-object v0, p0, LX/AhS;->g:Landroid/content/Context;

    iget v1, p0, LX/AhS;->i:I

    invoke-virtual {p2, v0, v1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 1702958
    iget-object v0, p0, LX/AhS;->k:Landroid/content/res/ColorStateList;

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1702959
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/AhO;

    .line 1702960
    iput-object v0, p2, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c:LX/3uw;

    .line 1702961
    invoke-virtual {p1}, LX/3v3;->getItemId()I

    move-result v0

    if-lez v0, :cond_0

    .line 1702962
    invoke-virtual {p1}, LX/3v3;->getItemId()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setId(I)V

    .line 1702963
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1702857
    invoke-super {p0, p1, p2}, LX/3ut;->a(Landroid/content/Context;LX/3v0;)V

    .line 1702858
    iget-boolean v0, p0, LX/AhS;->n:Z

    if-nez v0, :cond_0

    .line 1702859
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1702860
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, LX/AhS;->m:I

    .line 1702861
    const v1, 0x7f0b0d78

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AhS;->q:I

    .line 1702862
    :cond_0
    iget v0, p0, LX/AhS;->m:I

    .line 1702863
    iget-object v1, p0, LX/AhS;->l:LX/AhP;

    if-nez v1, :cond_1

    .line 1702864
    new-instance v1, LX/AhP;

    iget-object v2, p0, LX/3ut;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, LX/AhP;-><init>(LX/AhS;Landroid/content/Context;)V

    iput-object v1, p0, LX/AhS;->l:LX/AhP;

    .line 1702865
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1702866
    iget-object v2, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v2, v1, v1}, LX/AhP;->measure(II)V

    .line 1702867
    :cond_1
    iget-object v1, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v1}, LX/AhP;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1702868
    iput v0, p0, LX/AhS;->o:I

    .line 1702869
    return-void
.end method

.method public final a(LX/3v3;)Z
    .locals 1

    .prologue
    .line 1702954
    invoke-virtual {p1}, LX/3v3;->j()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 1702951
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/AhS;->l:LX/AhP;

    if-ne v0, v1, :cond_0

    .line 1702952
    const/4 v0, 0x0

    .line 1702953
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/3ut;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)LX/3uq;
    .locals 2

    .prologue
    .line 1702950
    new-instance v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    iget-object v1, p0, LX/AhS;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1702921
    invoke-super {p0, p1}, LX/3ut;->b(Z)V

    .line 1702922
    iget-object v2, p0, LX/3ut;->f:LX/3ux;

    if-nez v2, :cond_1

    .line 1702923
    :cond_0
    return-void

    .line 1702924
    :cond_1
    iget-object v2, p0, LX/3ut;->c:LX/3v0;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v2}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v2

    .line 1702925
    :goto_0
    if-eqz v2, :cond_9

    .line 1702926
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1702927
    if-ne v2, v0, :cond_7

    .line 1702928
    :cond_2
    :goto_1
    if-eqz v0, :cond_8

    .line 1702929
    iget-object v0, p0, LX/AhS;->l:LX/AhP;

    if-nez v0, :cond_3

    .line 1702930
    new-instance v0, LX/AhP;

    iget-object v2, p0, LX/3ut;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v2}, LX/AhP;-><init>(LX/AhS;Landroid/content/Context;)V

    iput-object v0, p0, LX/AhS;->l:LX/AhP;

    .line 1702931
    :cond_3
    iget-object v0, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v0}, LX/AhP;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1702932
    iget-object v2, p0, LX/3ut;->f:LX/3ux;

    if-eq v0, v2, :cond_5

    .line 1702933
    if-eqz v0, :cond_4

    .line 1702934
    iget-object v2, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1702935
    :cond_4
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/AhO;

    .line 1702936
    invoke-virtual {v0}, LX/AhO;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    .line 1702937
    iget-object v3, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v0, v3, v2}, LX/AhO;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1702938
    :cond_5
    :goto_2
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/AhO;

    .line 1702939
    invoke-virtual {v0}, LX/AhO;->getChildCount()I

    move-result v2

    .line 1702940
    if-lez v2, :cond_0

    iget v3, p0, LX/AhS;->j:I

    if-lez v3, :cond_0

    .line 1702941
    :goto_3
    if-ge v1, v2, :cond_0

    .line 1702942
    invoke-virtual {v0, v1}, LX/AhO;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1702943
    iget v4, p0, LX/AhS;->j:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1702944
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 1702945
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1702946
    :cond_6
    const/4 v2, 0x0

    goto :goto_0

    .line 1702947
    :cond_7
    if-gtz v2, :cond_2

    move v0, v1

    goto :goto_1

    .line 1702948
    :cond_8
    iget-object v0, p0, LX/AhS;->l:LX/AhP;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v0}, LX/AhP;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, LX/3ut;->f:LX/3ux;

    if-ne v0, v2, :cond_5

    .line 1702949
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, LX/AhS;->l:LX/AhP;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_1
.end method

.method public final b()Z
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1702886
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v10

    .line 1702887
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1702888
    iget v5, p0, LX/AhS;->p:I

    .line 1702889
    iget v7, p0, LX/AhS;->o:I

    move v8, v6

    move v1, v6

    move v3, v6

    move v4, v6

    .line 1702890
    :goto_0
    if-ge v8, v11, :cond_2

    .line 1702891
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 1702892
    invoke-virtual {v0}, LX/3v3;->l()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1702893
    add-int/lit8 v0, v4, 0x1

    move v12, v1

    move v1, v3

    move v3, v0

    move v0, v12

    .line 1702894
    :goto_1
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1702895
    :cond_0
    invoke-virtual {v0}, LX/3v3;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1702896
    add-int/lit8 v0, v3, 0x1

    move v3, v4

    move v12, v0

    move v0, v1

    move v1, v12

    goto :goto_1

    :cond_1
    move v0, v2

    move v1, v3

    move v3, v4

    .line 1702897
    goto :goto_1

    .line 1702898
    :cond_2
    if-nez v1, :cond_3

    add-int v0, v4, v3

    if-le v0, v5, :cond_c

    .line 1702899
    :cond_3
    add-int/lit8 v0, v5, -0x1

    .line 1702900
    :goto_2
    sub-int v4, v0, v4

    .line 1702901
    iget v0, p0, LX/AhS;->q:I

    div-int v0, v7, v0

    add-int/lit8 v3, v0, 0x1

    move v9, v6

    move v5, v7

    .line 1702902
    :goto_3
    if-ge v9, v11, :cond_8

    .line 1702903
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 1702904
    invoke-virtual {v0}, LX/3v3;->l()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1702905
    add-int/lit8 v3, v3, -0x1

    .line 1702906
    iget v7, p0, LX/AhS;->q:I

    sub-int/2addr v5, v7

    .line 1702907
    invoke-virtual {v0, v2}, LX/3v3;->d(Z)V

    move v0, v3

    move v3, v5

    .line 1702908
    :goto_4
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move v5, v3

    move v3, v0

    goto :goto_3

    .line 1702909
    :cond_4
    invoke-virtual {v0}, LX/3v3;->k()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1702910
    if-lez v4, :cond_5

    if-lez v5, :cond_5

    if-lez v3, :cond_5

    move v7, v2

    .line 1702911
    :goto_5
    if-eqz v7, :cond_a

    .line 1702912
    add-int/lit8 v7, v3, -0x1

    .line 1702913
    iget v3, p0, LX/AhS;->q:I

    sub-int/2addr v5, v3

    .line 1702914
    if-ltz v5, :cond_6

    move v3, v2

    :goto_6
    move v8, v3

    move v12, v7

    move v7, v5

    move v5, v12

    .line 1702915
    :goto_7
    if-eqz v8, :cond_9

    .line 1702916
    add-int/lit8 v3, v4, -0x1

    .line 1702917
    :goto_8
    invoke-virtual {v0, v8}, LX/3v3;->d(Z)V

    move v0, v5

    move v4, v3

    move v3, v7

    goto :goto_4

    :cond_5
    move v7, v6

    .line 1702918
    goto :goto_5

    .line 1702919
    :cond_6
    add-int/lit8 v3, v11, -0x1

    if-ne v9, v3, :cond_7

    if-nez v1, :cond_7

    iget v3, p0, LX/AhS;->m:I

    iget v8, p0, LX/AhS;->o:I

    sub-int/2addr v3, v8

    add-int/2addr v3, v5

    if-ltz v3, :cond_7

    move v3, v2

    goto :goto_6

    :cond_7
    move v3, v6

    goto :goto_6

    .line 1702920
    :cond_8
    return v2

    :cond_9
    move v3, v4

    goto :goto_8

    :cond_a
    move v8, v7

    move v7, v5

    move v5, v3

    goto :goto_7

    :cond_b
    move v0, v3

    move v3, v5

    goto :goto_4

    :cond_c
    move v0, v5

    goto :goto_2
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1702881
    invoke-virtual {p0}, LX/AhS;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    if-nez v0, :cond_0

    .line 1702882
    new-instance v0, LX/AhR;

    iget-object v2, p0, LX/AhS;->g:Landroid/content/Context;

    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    iget-object v4, p0, LX/AhS;->l:LX/AhP;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/AhR;-><init>(LX/AhS;Landroid/content/Context;LX/3v0;Landroid/view/View;Z)V

    .line 1702883
    new-instance v1, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    invoke-direct {v1, p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;-><init>(LX/AhS;LX/AhR;)V

    iput-object v1, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    .line 1702884
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1702885
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1702872
    iget-object v0, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-eqz v0, :cond_0

    .line 1702873
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1702874
    const/4 v0, 0x0

    iput-object v0, p0, LX/AhS;->s:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionMenuPresenter$OpenOverflowRunnable;

    move v0, v1

    .line 1702875
    :goto_0
    return v0

    .line 1702876
    :cond_0
    iget-object v0, p0, LX/AhS;->r:LX/AhR;

    .line 1702877
    if-eqz v0, :cond_1

    .line 1702878
    invoke-virtual {v0}, LX/AhQ;->c()V

    move v0, v1

    .line 1702879
    goto :goto_0

    .line 1702880
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1702871
    invoke-virtual {p0}, LX/AhS;->d()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1702870
    iget-object v0, p0, LX/AhS;->r:LX/AhR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AhS;->r:LX/AhR;

    invoke-virtual {v0}, LX/AhQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
