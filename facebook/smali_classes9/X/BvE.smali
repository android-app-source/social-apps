.class public LX/BvE;
.super LX/3Ga;
.source ""


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public b:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/view/View;

.field public o:Lcom/facebook/fbui/facepile/FacepileView;

.field private p:Lcom/facebook/widget/text/BetterTextView;

.field private q:Lcom/facebook/widget/text/BetterTextView;

.field public r:Lcom/facebook/privacy/ui/PrivacyOptionView;

.field public s:Lcom/facebook/widget/text/BetterTextView;

.field public t:Landroid/widget/LinearLayout;

.field public u:Lcom/facebook/fig/button/FigButton;

.field public v:Lcom/facebook/fig/button/FigButton;

.field public w:Lcom/facebook/graphql/model/GraphQLStory;

.field public x:Lcom/facebook/graphql/model/GraphQLActor;

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ae6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1832017
    const-class v0, LX/BvE;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BvE;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BvE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832106
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832103
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BvE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832104
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832096
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832097
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1832098
    iput-object v0, p0, LX/BvE;->y:LX/0Ot;

    .line 1832099
    const-class v0, LX/BvE;

    invoke-static {v0, p0}, LX/BvE;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832100
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvB;

    invoke-direct {v1, p0}, LX/BvB;-><init>(LX/BvE;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832101
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvD;

    invoke-direct {v1, p0}, LX/BvD;-><init>(LX/BvE;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832102
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/BvE;

    invoke-static {v3}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v1

    check-cast v1, LX/8Sa;

    const/16 v2, 0x12cc

    invoke-static {v3, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    const/16 p0, 0x1c00

    invoke-static {v3, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, LX/BvE;->b:LX/8Sa;

    iput-object v4, p1, LX/BvE;->c:LX/0Or;

    iput-object v2, p1, LX/BvE;->d:LX/03V;

    iput-object v3, p1, LX/BvE;->y:LX/0Ot;

    return-void
.end method

.method public static a$redex0(LX/BvE;LX/BvC;)V
    .locals 5

    .prologue
    .line 1832056
    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BvE;->x:Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v0, :cond_1

    .line 1832057
    :cond_0
    :goto_0
    return-void

    .line 1832058
    :cond_1
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1832059
    iget-object v0, p0, LX/BvE;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1832060
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1832061
    iget-object v0, p0, LX/BvE;->x:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 1832062
    :goto_1
    if-nez v0, :cond_4

    .line 1832063
    iget-object v0, p0, LX/BvE;->d:LX/03V;

    sget-object v2, LX/BvE;->e:Ljava/lang/String;

    const-string v3, "Broadcaster Pic Uri is null"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832064
    :goto_2
    iget-object v0, p0, LX/BvE;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    iget-object v2, p0, LX/BvE;->o:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1832065
    iget v3, v2, Lcom/facebook/fbui/facepile/FacepileView;->m:I

    move v2, v3

    .line 1832066
    invoke-virtual {v0, v2}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 1832067
    if-nez v0, :cond_5

    .line 1832068
    iget-object v0, p0, LX/BvE;->d:LX/03V;

    sget-object v2, LX/BvE;->e:Ljava/lang/String;

    const-string v3, "Viewer Pic Uri is null"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832069
    :goto_3
    iget-object v0, p0, LX/BvE;->o:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1832070
    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 1832071
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v1

    if-nez v1, :cond_6

    .line 1832072
    :cond_2
    :goto_4
    sget-object v0, LX/BvA;->a:[I

    invoke-virtual {p1}, LX/BvC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1832073
    :pswitch_0
    iget-object v0, p0, LX/BvE;->p:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080dec

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1832074
    iget-object v0, p0, LX/BvE;->q:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080ded

    invoke-direct {p0, v1}, LX/BvE;->c(I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832075
    iget-object v0, p0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080dee

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1832076
    iget-object v0, p0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Bv6;

    invoke-direct {v1, p0}, LX/Bv6;-><init>(LX/BvE;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1832077
    iget-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080def

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1832078
    iget-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Bv7;

    invoke-direct {v1, p0}, LX/Bv7;-><init>(LX/BvE;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1832079
    :pswitch_1
    iget-object v0, p0, LX/BvE;->p:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080df2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1832080
    iget-object v0, p0, LX/BvE;->q:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080df3

    invoke-direct {p0, v1}, LX/BvE;->c(I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832081
    iget-object v0, p0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080df0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1832082
    iget-object v0, p0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Bv8;

    invoke-direct {v1, p0}, LX/Bv8;-><init>(LX/BvE;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1832083
    iget-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080df1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1832084
    iget-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Bv9;

    invoke-direct {v1, p0}, LX/Bv9;-><init>(LX/BvE;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1832085
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1832086
    :cond_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1832087
    :cond_5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1832088
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLIcon;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    .line 1832089
    iget-object v2, p0, LX/BvE;->r:Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v0

    sget-object v4, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v1, v4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/String;I)V

    .line 1832090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1832091
    const v0, 0x7f080df4

    .line 1832092
    :goto_5
    iget-object v1, p0, LX/BvE;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto/16 :goto_4

    .line 1832093
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1832094
    const v0, 0x7f080df5

    goto :goto_5

    .line 1832095
    :cond_8
    const v0, 0x7f080df6

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1832052
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1832053
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1832054
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1832055
    return-void
.end method

.method private c(I)Landroid/text/SpannableString;
    .locals 6

    .prologue
    .line 1832047
    invoke-virtual {p0}, LX/BvE;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1832048
    new-instance v1, LX/47x;

    invoke-direct {v1, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1832049
    invoke-virtual {v1, p1}, LX/47x;->a(I)LX/47x;

    .line 1832050
    const/4 v2, 0x1

    iget-object v3, p0, LX/BvE;->x:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f0a00d5

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v2, v3, v0, v1}, LX/3Hk;->a(ILjava/lang/String;Ljava/util/List;LX/47x;)V

    .line 1832051
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/BvE;)V
    .locals 2

    .prologue
    .line 1832043
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1832044
    if-eqz v0, :cond_0

    .line 1832045
    iget-object v0, p0, LX/BvE;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1832046
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1832035
    invoke-super {p0, p1, p2}, LX/3Ga;->a(LX/2pa;Z)V

    .line 1832036
    if-nez p2, :cond_1

    .line 1832037
    :cond_0
    :goto_0
    return-void

    .line 1832038
    :cond_1
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1832039
    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1832040
    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BvE;->w:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    :goto_1
    iput-object v0, p0, LX/BvE;->x:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1832041
    iget-object v0, p0, LX/BvE;->x:Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1832042
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 1832034
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1832031
    invoke-super {p0}, LX/3Ga;->d()V

    .line 1832032
    invoke-static {p0}, LX/BvE;->i(LX/BvE;)V

    .line 1832033
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1832030
    const v0, 0x7f030a30

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 1832029
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1832018
    iput-object p1, p0, LX/BvE;->f:Landroid/view/View;

    .line 1832019
    const v0, 0x7f0d19c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/BvE;->o:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1832020
    const v0, 0x7f0d19c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BvE;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1832021
    const v0, 0x7f0d19c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BvE;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1832022
    const v0, 0x7f0d19c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    iput-object v0, p0, LX/BvE;->r:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 1832023
    const v0, 0x7f0d19c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BvE;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 1832024
    const v0, 0x7f0d19c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/BvE;->t:Landroid/widget/LinearLayout;

    .line 1832025
    const v0, 0x7f0d19cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/BvE;->u:Lcom/facebook/fig/button/FigButton;

    .line 1832026
    const v0, 0x7f0d19ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    .line 1832027
    iget-object v0, p0, LX/BvE;->v:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Bv5;

    invoke-direct {v1, p0}, LX/Bv5;-><init>(LX/BvE;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1832028
    return-void
.end method
