.class public final LX/BKC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772704
    iput-object p1, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1772705
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-nez v0, :cond_0

    .line 1772706
    :goto_0
    return-void

    .line 1772707
    :cond_0
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    if-eqz v0, :cond_1

    .line 1772708
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08249f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 1772709
    :cond_1
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v1}, LX/BKm;->a()LX/BKl;

    move-result-object v1

    iget-object v2, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v2

    const/4 v3, 0x0

    .line 1772710
    iput-object v3, v2, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1772711
    move-object v2, v2

    .line 1772712
    invoke-virtual {v2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1772713
    iput-object v2, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1772714
    move-object v1, v1

    .line 1772715
    invoke-virtual {v1}, LX/BKl;->a()LX/BKm;

    move-result-object v1

    .line 1772716
    iput-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772717
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772718
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1772719
    if-nez p1, :cond_1

    .line 1772720
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    if-eqz v0, :cond_0

    .line 1772721
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v1, 0x0

    iget-object v2, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08249f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1772722
    :cond_0
    :goto_0
    return-void

    .line 1772723
    :cond_1
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5RO;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    .line 1772724
    iget-object v1, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v2}, LX/BKm;->a()LX/BKl;

    move-result-object v2

    iget-object v3, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v3, v3, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v3

    .line 1772725
    iput-object v0, v3, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1772726
    move-object v0, v3

    .line 1772727
    invoke-virtual {v0}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v0

    .line 1772728
    iput-object v0, v2, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1772729
    move-object v0, v2

    .line 1772730
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    .line 1772731
    iput-object v0, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772732
    iget-object v0, p0, LX/BKC;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_0
.end method
