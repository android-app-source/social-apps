.class public LX/Ca0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ca0;


# instance fields
.field private final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Long;",
            "LX/CZz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1916918
    new-instance v0, LX/0aq;

    const/16 v1, 0x4b

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/Ca0;->a:LX/0aq;

    .line 1916919
    return-void
.end method

.method public static a(LX/0QB;)LX/Ca0;
    .locals 3

    .prologue
    .line 1916920
    sget-object v0, LX/Ca0;->b:LX/Ca0;

    if-nez v0, :cond_1

    .line 1916921
    const-class v1, LX/Ca0;

    monitor-enter v1

    .line 1916922
    :try_start_0
    sget-object v0, LX/Ca0;->b:LX/Ca0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1916923
    if-eqz v2, :cond_0

    .line 1916924
    :try_start_1
    new-instance v0, LX/Ca0;

    invoke-direct {v0}, LX/Ca0;-><init>()V

    .line 1916925
    move-object v0, v0

    .line 1916926
    sput-object v0, LX/Ca0;->b:LX/Ca0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1916927
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1916928
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1916929
    :cond_1
    sget-object v0, LX/Ca0;->b:LX/Ca0;

    return-object v0

    .line 1916930
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1916931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 1916932
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ca0;->a:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1916933
    monitor-exit p0

    return-void

    .line 1916934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(J)LX/CZz;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1916935
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ca0;->a:LX/0aq;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CZz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 1916936
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/Ca0;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1916937
    monitor-exit p0

    return-void

    .line 1916938
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
