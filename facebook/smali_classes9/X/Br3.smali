.class public LX/Br3;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825786
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1825787
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Float;)Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ">(",
            "Ljava/lang/Float;",
            ")",
            "Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1825788
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;-><init>(Ljava/lang/Float;Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/1Ad;LX/1qa;)V

    .line 1825789
    return-object v0
.end method
