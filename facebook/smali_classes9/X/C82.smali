.class public LX/C82;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C83;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C82",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C83;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852274
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1852275
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C82;->b:LX/0Zi;

    .line 1852276
    iput-object p1, p0, LX/C82;->a:LX/0Ot;

    .line 1852277
    return-void
.end method

.method public static a(LX/0QB;)LX/C82;
    .locals 4

    .prologue
    .line 1852235
    const-class v1, LX/C82;

    monitor-enter v1

    .line 1852236
    :try_start_0
    sget-object v0, LX/C82;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852237
    sput-object v2, LX/C82;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852238
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852239
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852240
    new-instance v3, LX/C82;

    const/16 p0, 0x1fa5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C82;-><init>(LX/0Ot;)V

    .line 1852241
    move-object v0, v3

    .line 1852242
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852243
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C82;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852244
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852245
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1852246
    check-cast p2, LX/C81;

    .line 1852247
    iget-object v0, p0, LX/C82;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C83;

    iget-object v1, p2, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C81;->c:LX/1Pn;

    .line 1852248
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1d56

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1d56

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020a43

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 1852249
    const v4, 0x108461e9

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1852250
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x1

    .line 1852251
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f082a07

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b1d4f

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const p0, 0x7f0b1d4e

    invoke-interface {v4, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const p0, 0x7f0b1d52

    invoke-interface {v4, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1852252
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1852253
    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v4

    const v5, 0x7f0b1d68

    invoke-virtual {v4, v5}, LX/3Ad;->j(I)LX/3Ad;

    move-result-object v4

    move-object v4, v4

    .line 1852254
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/C83;->a:LX/C87;

    invoke-virtual {v4, p1}, LX/C87;->c(LX/1De;)LX/C85;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/C85;->a(LX/1Pn;)LX/C85;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/C85;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C85;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1d61

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1852255
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1852256
    invoke-static {}, LX/1dS;->b()V

    .line 1852257
    iget v0, p1, LX/1dQ;->b:I

    .line 1852258
    packed-switch v0, :pswitch_data_0

    .line 1852259
    :goto_0
    return-object v2

    .line 1852260
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1852261
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1852262
    check-cast v1, LX/C81;

    .line 1852263
    iget-object v3, p0, LX/C82;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C83;

    iget-object v4, v1, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/C81;->b:Ljava/lang/String;

    .line 1852264
    iget-object v6, v3, LX/C83;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1g8;

    const-string p1, "gmw_unit_pinned_post_card_click"

    invoke-virtual {v6, v5, p1}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852265
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1852266
    new-instance p2, LX/0Pz;

    invoke-direct {p2}, LX/0Pz;-><init>()V

    .line 1852267
    iget-object v6, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1852268
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1852269
    new-instance v6, LX/5QQ;

    invoke-direct {v6}, LX/5QQ;-><init>()V

    invoke-virtual {v6, v5}, LX/5QQ;->a(Ljava/lang/String;)LX/5QQ;

    move-result-object v6

    invoke-virtual {p2}, LX/0Pz;->b()LX/0Px;

    move-result-object p2

    invoke-virtual {v6, p2}, LX/5QQ;->a(LX/0Px;)LX/5QQ;

    move-result-object v6

    .line 1852270
    iget-object p2, v6, LX/5QQ;->a:Landroid/os/Bundle;

    move-object v6, p2

    .line 1852271
    invoke-virtual {p1, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1852272
    iget-object p1, v3, LX/C83;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->C:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    invoke-interface {p1, p2, p0, v6, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1852273
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x108461e9
        :pswitch_0
    .end packed-switch
.end method
