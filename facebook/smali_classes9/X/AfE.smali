.class public final LX/AfE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/AfG;


# direct methods
.method public constructor <init>(LX/AfG;I)V
    .locals 0

    .prologue
    .line 1698803
    iput-object p1, p0, LX/AfE;->b:LX/AfG;

    iput p2, p0, LX/AfE;->a:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1698804
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698805
    :cond_0
    :goto_0
    return-void

    .line 1698806
    :cond_1
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v1, v0, LX/AfG;->f:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AfG;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_graphFailure"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get vod comment events for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AfE;->b:LX/AfG;

    iget-object v3, v3, LX/Aec;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/Aec;->b:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1698807
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    if-eqz v0, :cond_0

    .line 1698808
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/Aeb;->c(Z)V

    goto :goto_0

    .line 1698809
    :cond_2
    const-string v0, "no story id"

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1698810
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x1

    .line 1698811
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v0, v0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698812
    :cond_0
    :goto_0
    return-void

    .line 1698813
    :cond_1
    if-eqz p1, :cond_2

    .line 1698814
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698815
    if-eqz v0, :cond_2

    .line 1698816
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698817
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1698818
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698819
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1698820
    iget-object v0, p0, LX/AfE;->b:LX/AfG;

    iget-object v1, v0, LX/AfG;->h:LX/Aee;

    .line 1698821
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698822
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;->s()LX/0Px;

    move-result-object v0

    iget v2, p0, LX/AfE;->a:I

    invoke-virtual {v1, v0, v3, v2}, LX/Aee;->a(LX/0Px;ZI)V

    .line 1698823
    :cond_2
    const/4 v1, 0x0

    .line 1698824
    if-nez p1, :cond_3

    move-object v0, v1

    .line 1698825
    :goto_1
    move-object v0, v0

    .line 1698826
    iget-object v1, p0, LX/AfE;->b:LX/AfG;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    if-eqz v1, :cond_0

    .line 1698827
    iget-object v1, p0, LX/AfE;->b:LX/AfG;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    invoke-interface {v1, v0, v3}, LX/Aeb;->a(Ljava/util/List;Z)V

    goto :goto_0

    .line 1698828
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698829
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;

    .line 1698830
    if-nez v0, :cond_4

    move-object v0, v1

    .line 1698831
    goto :goto_1

    .line 1698832
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;

    move-result-object v0

    .line 1698833
    if-nez v0, :cond_5

    move-object v0, v1

    .line 1698834
    goto :goto_1

    .line 1698835
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODBackfillCommentsFragmentModel;->t()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;

    move-result-object v0

    invoke-static {v0}, LX/AfG;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;)Ljava/util/LinkedList;

    move-result-object v0

    goto :goto_1
.end method
