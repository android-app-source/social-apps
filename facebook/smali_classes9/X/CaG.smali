.class public LX/CaG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/23R;


# direct methods
.method public constructor <init>(LX/23R;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1917337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1917338
    iput-object p1, p0, LX/CaG;->a:LX/23R;

    .line 1917339
    return-void
.end method

.method public static b(LX/0QB;)LX/CaG;
    .locals 2

    .prologue
    .line 1917325
    new-instance v1, LX/CaG;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-direct {v1, v0}, LX/CaG;-><init>(LX/23R;)V

    .line 1917326
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/0Px;Ljava/lang/String;LX/9hN;LX/1bf;ZILX/9hM;LX/74S;)V
    .locals 14
    .param p4    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/9hM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/74S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;",
            "Ljava/lang/String;",
            "LX/9hN;",
            "LX/1bf;",
            "ZI",
            "LX/9hM;",
            "LX/74S;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1917335
    invoke-static/range {p2 .. p2}, LX/9hF;->d(LX/0Px;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-virtual/range {v0 .. v13}, LX/CaG;->a(Landroid/content/Context;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;Ljava/lang/String;LX/9hN;LX/1bf;ZILX/9hM;LX/74S;ILjava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1917336
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;Ljava/lang/String;LX/9hN;LX/1bf;ZILX/9hM;LX/74S;ILjava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .param p5    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/9hM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/74S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;",
            "Ljava/lang/String;",
            "LX/9hN;",
            "LX/1bf;",
            "ZI",
            "LX/9hM;",
            "LX/74S;",
            "I",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1917327
    const/4 v2, 0x0

    .line 1917328
    if-eqz p13, :cond_1

    .line 1917329
    invoke-virtual/range {p13 .. p13}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 1917330
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1917331
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v1

    .line 1917332
    :goto_0
    new-instance v2, LX/9hE;

    invoke-direct {v2, p2}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    invoke-virtual {v2, p3}, LX/9hD;->a(LX/0Px;)LX/9hD;

    move-result-object v2

    if-nez p10, :cond_0

    sget-object p10, LX/74S;->UNKNOWN:LX/74S;

    :cond_0
    invoke-virtual {v2, p10}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v2

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, LX/9hD;->b(I)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/9hD;->c(Z)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/9hD;->a(I)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/9hD;->a(LX/9hM;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p11}, LX/9hD;->c(I)LX/9hD;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/9hD;->c(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/9hD;->d(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 1917333
    iget-object v2, p0, LX/CaG;->a:LX/23R;

    invoke-interface {v2, p1, v1, p5}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 1917334
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
