.class public final LX/CGc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/CGe;


# direct methods
.method public constructor <init>(LX/CGe;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1865636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865637
    iput-object p1, p0, LX/CGc;->a:LX/CGe;

    .line 1865638
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 9
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1865640
    if-eqz p3, :cond_0

    .line 1865641
    iget-object v0, p3, LX/B5f;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1865642
    sput-object v0, LX/CGd;->h:Ljava/lang/String;

    .line 1865643
    :cond_0
    iget-object v0, p0, LX/CGc;->a:LX/CGe;

    .line 1865644
    new-instance v1, LX/CGd;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v2, LX/AR2;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AR2;

    const-class v2, LX/AR4;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AR4;

    const-class v2, LX/ARD;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ARD;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lB;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, LX/CGd;-><init>(LX/B5j;Landroid/content/Context;LX/AR2;LX/AR4;LX/ARD;LX/0lB;LX/03V;)V

    .line 1865645
    move-object v0, v1

    .line 1865646
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1865639
    const-string v0, "GroupsPollComposerPluginConfig"

    return-object v0
.end method
