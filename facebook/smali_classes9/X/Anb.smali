.class public LX/Anb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712692
    iput-object p1, p0, LX/Anb;->a:LX/0Zb;

    .line 1712693
    iput-object p2, p0, LX/Anb;->b:LX/0if;

    .line 1712694
    return-void
.end method

.method public static a(LX/0QB;)LX/Anb;
    .locals 5

    .prologue
    .line 1712699
    const-class v1, LX/Anb;

    monitor-enter v1

    .line 1712700
    :try_start_0
    sget-object v0, LX/Anb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712701
    sput-object v2, LX/Anb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712704
    new-instance p0, LX/Anb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/Anb;-><init>(LX/0Zb;LX/0if;)V

    .line 1712705
    move-object v0, p0

    .line 1712706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Anb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ih;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1712710
    iget-object v0, p0, LX/Anb;->b:LX/0if;

    invoke-virtual {v0, p1}, LX/0if;->a(LX/0ih;)V

    .line 1712711
    iget-object v0, p0, LX/Anb;->b:LX/0if;

    invoke-virtual {v0, p1, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1712712
    iget-object v0, p0, LX/Anb;->b:LX/0if;

    invoke-virtual {v0, p1}, LX/0if;->c(LX/0ih;)V

    .line 1712713
    return-void
.end method

.method public final a(LX/162;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1712695
    iget-object v0, p0, LX/Anb;->a:LX/0Zb;

    const-string v1, "misinformation_warning_tap"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1712696
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1712697
    invoke-virtual {v0, p2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "tracking"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1712698
    :cond_0
    return-void
.end method
