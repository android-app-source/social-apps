.class public LX/Bhd;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:LX/1Mw;

.field private b:Landroid/graphics/Paint;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Mw;)V
    .locals 3

    .prologue
    .line 1809528
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1809529
    const/16 v0, 0xa

    iput v0, p0, LX/Bhd;->c:I

    const/4 v0, 0x5

    iput v0, p0, LX/Bhd;->d:I

    const/16 v0, 0x14

    iput v0, p0, LX/Bhd;->e:I

    .line 1809530
    const/16 v0, 0x10

    iput v0, p0, LX/Bhd;->f:I

    const/16 v0, 0x13

    iput v0, p0, LX/Bhd;->g:I

    const/16 v0, 0xe

    iput v0, p0, LX/Bhd;->h:I

    .line 1809531
    const/16 v0, 0x1e

    iput v0, p0, LX/Bhd;->i:I

    .line 1809532
    iput-object p2, p0, LX/Bhd;->a:LX/1Mw;

    .line 1809533
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/Bhd;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Bhd;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1809534
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    .line 1809535
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v4, 0x41c80000    # 25.0f

    const/high16 v6, 0x41980000    # 19.0f

    const/high16 v2, 0x40a00000    # 5.0f

    .line 1809536
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1809537
    iget-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Bhd;->a:LX/1Mw;

    .line 1809538
    iget-object v3, v1, LX/1Mw;->f:LX/0p3;

    sget-object v5, LX/0p3;->UNKNOWN:LX/0p3;

    if-eq v3, v5, :cond_0

    .line 1809539
    iget-object v3, v1, LX/1Mw;->f:LX/0p3;

    invoke-static {v3}, LX/1Mw;->a(LX/0p3;)I

    move-result v3

    .line 1809540
    :goto_0
    move v1, v3

    .line 1809541
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1809542
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v3, 0x41f00000    # 30.0f

    iget-object v5, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1809543
    iget-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Bhd;->a:LX/1Mw;

    .line 1809544
    iget-object v3, v1, LX/1Mw;->h:LX/0p3;

    invoke-static {v3}, LX/1Mw;->a(LX/0p3;)I

    move-result v3

    move v1, v3

    .line 1809545
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1809546
    const/high16 v1, 0x42200000    # 40.0f

    const/high16 v3, 0x42700000    # 60.0f

    iget-object v5, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1809547
    iget-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Bhd;->a:LX/1Mw;

    .line 1809548
    iget-boolean v3, v1, LX/1Mw;->i:Z

    if-eqz v3, :cond_2

    .line 1809549
    const v3, -0xff0100

    .line 1809550
    :goto_1
    move v1, v3

    .line 1809551
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1809552
    const/high16 v1, 0x428c0000    # 70.0f

    const/high16 v3, 0x42b40000    # 90.0f

    iget-object v5, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1809553
    iget-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1809554
    iget-object v0, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1809555
    const-string v0, "B"

    const/high16 v1, 0x41800000    # 16.0f

    iget-object v2, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1809556
    const-string v0, "L"

    const/high16 v1, 0x42380000    # 46.0f

    iget-object v2, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1809557
    const-string v0, "C"

    const/high16 v1, 0x42980000    # 76.0f

    iget-object v2, p0, LX/Bhd;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1809558
    return-void

    .line 1809559
    :cond_0
    iget-object v3, v1, LX/1Mw;->g:LX/0p3;

    sget-object v5, LX/0p3;->UNKNOWN:LX/0p3;

    if-ne v3, v5, :cond_1

    .line 1809560
    iget-object v3, v1, LX/1Mw;->g:LX/0p3;

    invoke-static {v3}, LX/1Mw;->a(LX/0p3;)I

    move-result v3

    goto :goto_0

    .line 1809561
    :cond_1
    iget-object v3, v1, LX/1Mw;->g:LX/0p3;

    invoke-static {v3}, LX/1Mw;->a(LX/0p3;)I

    move-result v3

    const v5, 0xffffff

    and-int/2addr v3, v5

    const/high16 v5, 0x77000000

    or-int/2addr v3, v5

    goto/16 :goto_0

    :cond_2
    const/high16 v3, -0x10000

    goto :goto_1
.end method
