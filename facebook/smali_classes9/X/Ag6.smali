.class public final LX/Ag6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ag7;


# direct methods
.method public constructor <init>(LX/Ag7;)V
    .locals 0

    .prologue
    .line 1700193
    iput-object p1, p0, LX/Ag6;->a:LX/Ag7;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1700194
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    iget-object v0, v0, LX/Ag7;->d:LX/03V;

    sget-object v1, LX/Ag7;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "video broadcast poll failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Ag6;->a:LX/Ag7;

    iget-object v3, v3, LX/Ag7;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1700195
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    iget-object v0, v0, LX/Ag7;->l:LX/0Zc;

    const-string v1, ""

    sget-object v2, LX/7IM;->LIVE_POLLER_BATCH_FAIL:LX/7IM;

    const-string v3, "Batch poller failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700196
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    const-string v1, "error"

    .line 1700197
    iput-object v1, v0, LX/Ag7;->s:Ljava/lang/String;

    .line 1700198
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    invoke-static {v0}, LX/Ag7;->e(LX/Ag7;)V

    .line 1700199
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700200
    check-cast p1, Ljava/util/Map;

    .line 1700201
    if-eqz p1, :cond_4

    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    iget-object v0, v0, LX/Ag7;->m:LX/Bwd;

    if-eqz v0, :cond_4

    .line 1700202
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    iget-object v0, v0, LX/Ag7;->m:LX/Bwd;

    .line 1700203
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1700204
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;

    .line 1700205
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1700206
    iget-object v3, v0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bwc;

    .line 1700207
    if-eqz v3, :cond_0

    .line 1700208
    iget-object v4, v3, LX/Bwc;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3Ha;

    .line 1700209
    if-nez v2, :cond_2

    .line 1700210
    if-eqz v4, :cond_1

    .line 1700211
    invoke-interface {v4, v1}, LX/3Ha;->a(Ljava/lang/String;)V

    .line 1700212
    :cond_1
    invoke-virtual {v0, v1}, LX/Bwd;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1700213
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->k()I

    move-result v6

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->l()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 1700214
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    .line 1700215
    if-eqz v4, :cond_0

    .line 1700216
    iget v7, v3, LX/Bwc;->c:I

    if-eq v7, v6, :cond_3

    .line 1700217
    iput v6, v3, LX/Bwc;->c:I

    .line 1700218
    invoke-interface {v4, v6}, LX/3Ha;->s_(I)V

    .line 1700219
    :cond_3
    iget-object v6, v3, LX/Bwc;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v2, v6, :cond_0

    .line 1700220
    iput-object v2, v3, LX/Bwc;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1700221
    iget-object v2, v0, LX/Bwd;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Hc;

    .line 1700222
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/3Hc;->a(Z)V

    .line 1700223
    iput-object v4, v2, LX/3Hc;->l:LX/3Ha;

    .line 1700224
    invoke-virtual {v2, v1}, LX/3Hc;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1700225
    :cond_4
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    const-string v1, "success"

    .line 1700226
    iput-object v1, v0, LX/Ag7;->s:Ljava/lang/String;

    .line 1700227
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    .line 1700228
    iput-object p1, v0, LX/Ag7;->v:Ljava/util/Map;

    .line 1700229
    iget-object v0, p0, LX/Ag6;->a:LX/Ag7;

    invoke-static {v0}, LX/Ag7;->e(LX/Ag7;)V

    .line 1700230
    return-void
.end method
