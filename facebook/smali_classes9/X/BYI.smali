.class public LX/BYI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0tX;

.field private final b:Ljava/util/concurrent/Executor;

.field public c:LX/Ajt;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1795408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1795409
    iput-object p1, p0, LX/BYI;->a:LX/0tX;

    .line 1795410
    iput-object p2, p0, LX/BYI;->b:Ljava/util/concurrent/Executor;

    .line 1795411
    return-void
.end method

.method public static a(LX/0QB;)LX/BYI;
    .locals 5

    .prologue
    .line 1795412
    const-class v1, LX/BYI;

    monitor-enter v1

    .line 1795413
    :try_start_0
    sget-object v0, LX/BYI;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1795414
    sput-object v2, LX/BYI;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1795415
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1795416
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1795417
    new-instance p0, LX/BYI;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/BYI;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 1795418
    move-object v0, p0

    .line 1795419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1795420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BYI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1795421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1795422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BYI;LX/0zS;LX/Ajt;)V
    .locals 3

    .prologue
    .line 1795423
    new-instance v0, LX/BYJ;

    invoke-direct {v0}, LX/BYJ;-><init>()V

    move-object v0, v0

    .line 1795424
    const-string v1, "iconWidth"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1795425
    const-string v1, "scale"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1795426
    const-string v1, "limit"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1795427
    iget-object v1, p0, LX/BYI;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/BYH;

    invoke-direct {v1, p0, p2}, LX/BYH;-><init>(LX/BYI;LX/Ajt;)V

    iget-object v2, p0, LX/BYI;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1795428
    return-void
.end method

.method public static c(Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1795429
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->k()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->k()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->k()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel$EdgesModel;->a()Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1795430
    :goto_0
    return-object v0

    .line 1795431
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->k()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel$GroupPurposesModel$EdgesModel;->a()Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1795432
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3

    .line 1795433
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 1795434
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1795435
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
