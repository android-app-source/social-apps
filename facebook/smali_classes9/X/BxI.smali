.class public LX/BxI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BxJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BxI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BxJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835225
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1835226
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BxI;->b:LX/0Zi;

    .line 1835227
    iput-object p1, p0, LX/BxI;->a:LX/0Ot;

    .line 1835228
    return-void
.end method

.method public static a(LX/0QB;)LX/BxI;
    .locals 4

    .prologue
    .line 1835229
    const-class v1, LX/BxI;

    monitor-enter v1

    .line 1835230
    :try_start_0
    sget-object v0, LX/BxI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835231
    sput-object v2, LX/BxI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835232
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835233
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835234
    new-instance v3, LX/BxI;

    const/16 p0, 0x1e07

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BxI;-><init>(LX/0Ot;)V

    .line 1835235
    move-object v0, v3

    .line 1835236
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835237
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835238
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1835240
    iget-object v0, p0, LX/BxI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BxJ;

    const/4 p2, 0x2

    .line 1835241
    iget-object v1, v0, LX/BxJ;->a:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    const v2, 0x7f0219c9

    invoke-virtual {v1, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const v2, 0x7f0a00e4

    invoke-virtual {v1, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1835242
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 p0, 0x6

    const/4 p2, 0x4

    invoke-interface {v1, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const p0, 0x7f080fd9

    invoke-virtual {v2, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00e5

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const/high16 p0, 0x41600000    # 14.0f

    invoke-virtual {v2, p0}, LX/1ne;->g(F)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f081109

    invoke-interface {v1, v2}, LX/1Dh;->Y(I)LX/1Dh;

    move-result-object v1

    .line 1835243
    const v2, -0x38b2eb15

    const/4 p0, 0x0

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1835244
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1835245
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1835246
    invoke-static {}, LX/1dS;->b()V

    .line 1835247
    iget v0, p1, LX/1dQ;->b:I

    .line 1835248
    packed-switch v0, :pswitch_data_0

    .line 1835249
    :goto_0
    return-object v2

    .line 1835250
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1835251
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1835252
    check-cast v1, LX/BxG;

    .line 1835253
    iget-object v3, p0, LX/BxI;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BxJ;

    iget-object v4, v1, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v5, v1, LX/BxG;->b:LX/1Pm;

    .line 1835254
    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object p1

    invoke-static {p1}, LX/9Ir;->b(LX/1PT;)LX/21D;

    move-result-object p1

    .line 1835255
    const-string p2, "articleChaining"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object p0

    invoke-virtual {p0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p0

    invoke-static {p1, p2, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p1

    sget-object p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p2

    .line 1835256
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1835257
    iget-object p1, v3, LX/BxJ;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1Kf;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, p0, p2, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1835258
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x38b2eb15
        :pswitch_0
    .end packed-switch
.end method
