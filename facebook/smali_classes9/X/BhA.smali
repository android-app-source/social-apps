.class public final LX/BhA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/BhD;


# direct methods
.method public constructor <init>(LX/BhD;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1808852
    iput-object p1, p0, LX/BhA;->c:LX/BhD;

    iput-object p2, p0, LX/BhA;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    iput-object p3, p0, LX/BhA;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x2

    const v0, 0x5038fa73

    invoke-static {v7, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1808853
    iget-object v0, p0, LX/BhA;->c:LX/BhD;

    iget-object v0, v0, LX/BhD;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1808854
    iget-object v0, p0, LX/BhA;->c:LX/BhD;

    iget-object v0, v0, LX/BhD;->c:LX/17Y;

    iget-object v2, p0, LX/BhA;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aK:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/BhA;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1808855
    if-eqz v0, :cond_0

    .line 1808856
    iget-object v2, p0, LX/BhA;->c:LX/BhD;

    iget-object v2, v2, LX/BhD;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/BhA;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1808857
    :cond_0
    const v0, 0x7ff3124f

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
