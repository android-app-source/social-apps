.class public final LX/BS9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1785092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;
    .locals 2

    .prologue
    .line 1785093
    new-instance v0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;

    invoke-direct {v0, p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1785094
    invoke-static {p1}, LX/BS9;->a(Landroid/os/Parcel;)Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1785095
    new-array v0, p1, [Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;

    move-object v0, v0

    .line 1785096
    return-object v0
.end method
