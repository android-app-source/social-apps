.class public final LX/ApR;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ApS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/Apj;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1716019
    invoke-static {}, LX/ApS;->q()LX/ApS;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716020
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716021
    const-string v0, "FigHscrollFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716022
    if-ne p0, p1, :cond_1

    .line 1716023
    :cond_0
    :goto_0
    return v0

    .line 1716024
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716025
    goto :goto_0

    .line 1716026
    :cond_3
    check-cast p1, LX/ApR;

    .line 1716027
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716028
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716029
    if-eq v2, v3, :cond_0

    .line 1716030
    iget v2, p0, LX/ApR;->a:I

    iget v3, p1, LX/ApR;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716031
    goto :goto_0

    .line 1716032
    :cond_4
    iget-object v2, p0, LX/ApR;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/ApR;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApR;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1716033
    goto :goto_0

    .line 1716034
    :cond_6
    iget-object v2, p1, LX/ApR;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 1716035
    :cond_7
    iget-object v2, p0, LX/ApR;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/ApR;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApR;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1716036
    goto :goto_0

    .line 1716037
    :cond_9
    iget-object v2, p1, LX/ApR;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_8

    .line 1716038
    :cond_a
    iget-object v2, p0, LX/ApR;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/ApR;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApR;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1716039
    goto :goto_0

    .line 1716040
    :cond_c
    iget-object v2, p1, LX/ApR;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_b

    .line 1716041
    :cond_d
    iget-object v2, p0, LX/ApR;->e:LX/1X1;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/ApR;->e:LX/1X1;

    iget-object v3, p1, LX/ApR;->e:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1716042
    goto :goto_0

    .line 1716043
    :cond_f
    iget-object v2, p1, LX/ApR;->e:LX/1X1;

    if-nez v2, :cond_e

    .line 1716044
    :cond_10
    iget-object v2, p0, LX/ApR;->f:LX/1X1;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/ApR;->f:LX/1X1;

    iget-object v3, p1, LX/ApR;->f:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1716045
    goto :goto_0

    .line 1716046
    :cond_12
    iget-object v2, p1, LX/ApR;->f:LX/1X1;

    if-nez v2, :cond_11

    .line 1716047
    :cond_13
    iget-object v2, p0, LX/ApR;->g:LX/1dQ;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/ApR;->g:LX/1dQ;

    iget-object v3, p1, LX/ApR;->g:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 1716048
    goto/16 :goto_0

    .line 1716049
    :cond_15
    iget-object v2, p1, LX/ApR;->g:LX/1dQ;

    if-nez v2, :cond_14

    .line 1716050
    :cond_16
    iget-boolean v2, p0, LX/ApR;->h:Z

    iget-boolean v3, p1, LX/ApR;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1716051
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 1716052
    const/4 v2, 0x0

    .line 1716053
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/ApR;

    .line 1716054
    iget-object v1, v0, LX/ApR;->e:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/ApR;->e:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/ApR;->e:LX/1X1;

    .line 1716055
    iget-object v1, v0, LX/ApR;->f:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/ApR;->f:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, LX/ApR;->f:LX/1X1;

    .line 1716056
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1716057
    goto :goto_0
.end method
