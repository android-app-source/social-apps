.class public LX/AqB;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Aq9;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AqC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717335
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AqB;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AqC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1717336
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717337
    iput-object p1, p0, LX/AqB;->b:LX/0Ot;

    .line 1717338
    return-void
.end method

.method public static a(LX/0QB;)LX/AqB;
    .locals 4

    .prologue
    .line 1717339
    const-class v1, LX/AqB;

    monitor-enter v1

    .line 1717340
    :try_start_0
    sget-object v0, LX/AqB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1717341
    sput-object v2, LX/AqB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1717342
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1717343
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1717344
    new-instance v3, LX/AqB;

    const/16 p0, 0x2219

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/AqB;-><init>(LX/0Ot;)V

    .line 1717345
    move-object v0, v3

    .line 1717346
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1717347
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AqB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1717348
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1717349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1717350
    check-cast p2, LX/AqA;

    .line 1717351
    iget-object v0, p0, LX/AqB;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AqC;

    iget-boolean v1, p2, LX/AqA;->a:Z

    .line 1717352
    iget-object p0, v0, LX/AqC;->a:LX/Apv;

    invoke-virtual {p0, p1}, LX/Apv;->c(LX/1De;)LX/Aps;

    move-result-object p0

    const p2, 0x7f02000b

    invoke-virtual {p0, p2}, LX/Aps;->h(I)LX/Aps;

    move-result-object p0

    const p2, 0x7f02000a

    invoke-virtual {p0, p2}, LX/Aps;->i(I)LX/Aps;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/Aps;->a(Z)LX/Aps;

    move-result-object p0

    .line 1717353
    iget-object p2, p1, LX/1De;->g:LX/1X1;

    move-object p2, p2

    .line 1717354
    if-nez p2, :cond_0

    .line 1717355
    const/4 p2, 0x0

    .line 1717356
    :goto_0
    move-object p2, p2

    .line 1717357
    invoke-virtual {p0, p2}, LX/Aps;->a(LX/1dQ;)LX/Aps;

    move-result-object p0

    const/4 p2, 0x1

    .line 1717358
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput-boolean p2, v0, LX/Apt;->k:Z

    .line 1717359
    move-object p0, p0

    .line 1717360
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1717361
    return-object v0

    .line 1717362
    :cond_0
    iget-object p2, p1, LX/1De;->g:LX/1X1;

    move-object p2, p2

    .line 1717363
    check-cast p2, LX/AqA;

    iget-object p2, p2, LX/AqA;->b:LX/1dQ;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717364
    invoke-static {}, LX/1dS;->b()V

    .line 1717365
    const/4 v0, 0x0

    return-object v0
.end method
