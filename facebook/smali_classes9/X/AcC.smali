.class public LX/AcC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1692250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1692251
    iput-object p1, p0, LX/AcC;->a:Ljava/lang/String;

    .line 1692252
    iput-object p2, p0, LX/AcC;->b:Ljava/lang/String;

    .line 1692253
    iput-boolean p3, p0, LX/AcC;->d:Z

    .line 1692254
    iput-object p4, p0, LX/AcC;->c:Ljava/lang/String;

    .line 1692255
    return-void
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;
    .locals 5
    .param p0    # Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1692243
    if-nez p0, :cond_1

    .line 1692244
    :cond_0
    :goto_0
    return-object v0

    .line 1692245
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1692246
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 1692247
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 1692248
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1692249
    :cond_2
    new-instance v1, LX/AcC;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->k()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, v0}, LX/AcC;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)LX/AcC;
    .locals 5

    .prologue
    .line 1692242
    new-instance v0, LX/AcC;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LX/AcC;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method
