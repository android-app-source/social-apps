.class public final enum LX/AXr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AXr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AXr;

.field public static final enum FUNDRAISER_ALREADY_TURNED_ON:LX/AXr;

.field public static final enum TIP_JAR_ALREADY_TURNED_ON:LX/AXr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1684586
    new-instance v0, LX/AXr;

    const-string v1, "FUNDRAISER_ALREADY_TURNED_ON"

    invoke-direct {v0, v1, v2}, LX/AXr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AXr;->FUNDRAISER_ALREADY_TURNED_ON:LX/AXr;

    .line 1684587
    new-instance v0, LX/AXr;

    const-string v1, "TIP_JAR_ALREADY_TURNED_ON"

    invoke-direct {v0, v1, v3}, LX/AXr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AXr;->TIP_JAR_ALREADY_TURNED_ON:LX/AXr;

    .line 1684588
    const/4 v0, 0x2

    new-array v0, v0, [LX/AXr;

    sget-object v1, LX/AXr;->FUNDRAISER_ALREADY_TURNED_ON:LX/AXr;

    aput-object v1, v0, v2

    sget-object v1, LX/AXr;->TIP_JAR_ALREADY_TURNED_ON:LX/AXr;

    aput-object v1, v0, v3

    sput-object v0, LX/AXr;->$VALUES:[LX/AXr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1684583
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AXr;
    .locals 1

    .prologue
    .line 1684584
    const-class v0, LX/AXr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AXr;

    return-object v0
.end method

.method public static values()[LX/AXr;
    .locals 1

    .prologue
    .line 1684585
    sget-object v0, LX/AXr;->$VALUES:[LX/AXr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AXr;

    return-object v0
.end method
