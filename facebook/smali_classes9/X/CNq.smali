.class public LX/CNq;
.super LX/CNc;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final d:LX/CNs;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CNs;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1882154
    invoke-direct {p0, p1}, LX/CNc;-><init>(Landroid/content/Context;)V

    .line 1882155
    iput-object p2, p0, LX/CNq;->d:LX/CNs;

    .line 1882156
    return-void
.end method


# virtual methods
.method public final e()Lcom/facebook/content/SecureContextHelper;
    .locals 1

    .prologue
    .line 1882157
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    return-object v0
.end method

.method public final f()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1882158
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->a:LX/0Or;

    return-object v0
.end method

.method public final g()LX/0tX;
    .locals 1

    .prologue
    .line 1882159
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    return-object v0
.end method

.method public final j()LX/COk;
    .locals 1

    .prologue
    .line 1882160
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/COk;

    return-object v0
.end method

.method public final m()LX/0lC;
    .locals 1

    .prologue
    .line 1882161
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    return-object v0
.end method

.method public final n()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .prologue
    .line 1882162
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public final o()LX/03V;
    .locals 1

    .prologue
    .line 1882163
    iget-object v0, p0, LX/CNq;->d:LX/CNs;

    iget-object v0, v0, LX/CNs;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    return-object v0
.end method
