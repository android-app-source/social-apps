.class public LX/BtM;
.super Lcom/facebook/widget/ShimmerFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public c:Z

.field public d:I

.field public e:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1829001
    invoke-direct {p0, p1}, Lcom/facebook/widget/ShimmerFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1829002
    invoke-virtual {p0}, LX/BtM;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1829003
    const p1, 0x7f0308a9

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1829004
    const v0, 0x7f0d1669

    invoke-virtual {p0, v0}, LX/BtM;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/BtM;->e:Landroid/widget/RelativeLayout;

    .line 1829005
    invoke-virtual {p0}, LX/BtM;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b0917

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/BtM;->d:I

    .line 1829006
    invoke-virtual {p0}, LX/BtM;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1829007
    new-instance p1, LX/BtL;

    invoke-direct {p1, p0}, LX/BtL;-><init>(LX/BtM;)V

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1829008
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1829015
    iget-boolean v0, p0, LX/BtM;->c:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5768e1f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1829016
    invoke-super {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->onAttachedToWindow()V

    .line 1829017
    const/4 v1, 0x1

    .line 1829018
    iput-boolean v1, p0, LX/BtM;->c:Z

    .line 1829019
    const/16 v1, 0x2d

    const v2, -0x55bc3a00

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1f8ec158

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1829011
    invoke-super {p0}, Lcom/facebook/widget/ShimmerFrameLayout;->onDetachedFromWindow()V

    .line 1829012
    const/4 v1, 0x0

    .line 1829013
    iput-boolean v1, p0, LX/BtM;->c:Z

    .line 1829014
    const/16 v1, 0x2d

    const v2, -0x472b36ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1829009
    const/4 v0, 0x0

    invoke-super {p0, p1, v0}, Lcom/facebook/widget/ShimmerFrameLayout;->onMeasure(II)V

    .line 1829010
    return-void
.end method
