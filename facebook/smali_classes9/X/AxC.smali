.class public LX/AxC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/AxB;

.field private final b:LX/3mL;


# direct methods
.method public constructor <init>(LX/3mL;LX/AxB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727689
    iput-object p1, p0, LX/AxC;->b:LX/3mL;

    .line 1727690
    iput-object p2, p0, LX/AxC;->a:LX/AxB;

    .line 1727691
    return-void
.end method

.method public static a(LX/0QB;)LX/AxC;
    .locals 5

    .prologue
    .line 1727677
    const-class v1, LX/AxC;

    monitor-enter v1

    .line 1727678
    :try_start_0
    sget-object v0, LX/AxC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1727679
    sput-object v2, LX/AxC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1727680
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727681
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1727682
    new-instance p0, LX/AxC;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/AxB;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AxB;

    invoke-direct {p0, v3, v4}, LX/AxC;-><init>(LX/3mL;LX/AxB;)V

    .line 1727683
    move-object v0, p0

    .line 1727684
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1727685
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AxC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1727686
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1727687
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pq;LX/1Ri;)LX/1Dg;
    .locals 9
    .param p2    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Ri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/1Ri;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1727669
    iget-object v0, p0, LX/AxC;->a:LX/AxB;

    .line 1727670
    iget-object v1, p3, LX/1Ri;->a:LX/1RN;

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    check-cast v1, LX/1kW;

    .line 1727671
    iget-object p3, v1, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, p3

    .line 1727672
    invoke-virtual {v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->c()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1727673
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v2

    invoke-virtual {v2}, LX/3mP;->a()LX/25M;

    move-result-object v2

    .line 1727674
    new-instance v3, LX/AxA;

    move-object v7, p2

    check-cast v7, LX/1Pq;

    invoke-static {v0}, LX/AxF;->a(LX/0QB;)LX/AxF;

    move-result-object v8

    check-cast v8, LX/AxF;

    move-object v4, p1

    move-object v5, v1

    move-object v6, v2

    invoke-direct/range {v3 .. v8}, LX/AxA;-><init>(Landroid/content/Context;LX/0Px;LX/25M;LX/1Pq;LX/AxF;)V

    .line 1727675
    move-object v0, v3

    .line 1727676
    iget-object v1, p0, LX/AxC;->b:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
