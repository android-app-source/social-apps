.class public final enum LX/BKf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BKf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BKf;

.field public static final enum FULL:LX/BKf;

.field public static final enum MINI:LX/BKf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1774094
    new-instance v0, LX/BKf;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/BKf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKf;->FULL:LX/BKf;

    .line 1774095
    new-instance v0, LX/BKf;

    const-string v1, "MINI"

    invoke-direct {v0, v1, v3}, LX/BKf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKf;->MINI:LX/BKf;

    .line 1774096
    const/4 v0, 0x2

    new-array v0, v0, [LX/BKf;

    sget-object v1, LX/BKf;->FULL:LX/BKf;

    aput-object v1, v0, v2

    sget-object v1, LX/BKf;->MINI:LX/BKf;

    aput-object v1, v0, v3

    sput-object v0, LX/BKf;->$VALUES:[LX/BKf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1774098
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BKf;
    .locals 1

    .prologue
    .line 1774099
    const-class v0, LX/BKf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BKf;

    return-object v0
.end method

.method public static values()[LX/BKf;
    .locals 1

    .prologue
    .line 1774097
    sget-object v0, LX/BKf;->$VALUES:[LX/BKf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BKf;

    return-object v0
.end method
