.class public LX/AmB;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710527
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1710528
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    .line 1710529
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1710530
    invoke-virtual {p0}, LX/AmB;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1710531
    const-string v1, "Native Feed - internal"

    invoke-virtual {p0, v1}, LX/AmB;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710532
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1710533
    const-string v2, "Native Feed settings"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710534
    const-string v2, "View/Change Native feed settings for debugging"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710535
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1710536
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1710537
    invoke-virtual {p0, v1}, LX/AmB;->addPreference(Landroid/preference/Preference;)Z

    .line 1710538
    return-void
.end method
