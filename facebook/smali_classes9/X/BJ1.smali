.class public final enum LX/BJ1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BJ1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BJ1;

.field public static final enum FRIEND_TAGGING:LX/BJ1;

.field public static final enum PRODUCT_TAGGING:LX/BJ1;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1771153
    new-instance v0, LX/BJ1;

    const-string v1, "FRIEND_TAGGING"

    invoke-direct {v0, v1, v2}, LX/BJ1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ1;->FRIEND_TAGGING:LX/BJ1;

    .line 1771154
    new-instance v0, LX/BJ1;

    const-string v1, "PRODUCT_TAGGING"

    invoke-direct {v0, v1, v3}, LX/BJ1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ1;->PRODUCT_TAGGING:LX/BJ1;

    .line 1771155
    const/4 v0, 0x2

    new-array v0, v0, [LX/BJ1;

    sget-object v1, LX/BJ1;->FRIEND_TAGGING:LX/BJ1;

    aput-object v1, v0, v2

    sget-object v1, LX/BJ1;->PRODUCT_TAGGING:LX/BJ1;

    aput-object v1, v0, v3

    sput-object v0, LX/BJ1;->$VALUES:[LX/BJ1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1771150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BJ1;
    .locals 1

    .prologue
    .line 1771152
    const-class v0, LX/BJ1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BJ1;

    return-object v0
.end method

.method public static values()[LX/BJ1;
    .locals 1

    .prologue
    .line 1771151
    sget-object v0, LX/BJ1;->$VALUES:[LX/BJ1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BJ1;

    return-object v0
.end method
