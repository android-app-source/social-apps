.class public LX/BTV;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/BTR;

.field public b:I

.field public c:I

.field private d:LX/4Ac;

.field public e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/1aX;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Landroid/graphics/Rect;

.field public h:Landroid/graphics/Rect;

.field public i:LX/4Ab;

.field public j:LX/4Ab;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1787577
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1787578
    invoke-direct {p0, p1}, LX/BTV;->a(Landroid/content/Context;)V

    .line 1787579
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1787673
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787674
    invoke-direct {p0, p1}, LX/BTV;->a(Landroid/content/Context;)V

    .line 1787675
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1787670
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1787671
    invoke-direct {p0, p1}, LX/BTV;->a(Landroid/content/Context;)V

    .line 1787672
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1787654
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, LX/BTV;->d:LX/4Ac;

    .line 1787655
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/BTV;->f:Ljava/util/LinkedList;

    .line 1787656
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    .line 1787657
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c48

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1787658
    int-to-float v1, v0

    int-to-float v2, v0

    invoke-static {v1, v3, v3, v2}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v1

    iput-object v1, p0, LX/BTV;->i:LX/4Ab;

    .line 1787659
    int-to-float v1, v0

    int-to-float v0, v0

    invoke-static {v3, v1, v0, v3}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v0

    iput-object v0, p0, LX/BTV;->j:LX/4Ab;

    .line 1787660
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 1787661
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, LX/BTV;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 1787662
    invoke-static {v1, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    .line 1787663
    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1787664
    iget-object v2, p0, LX/BTV;->f:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1787665
    iget-object v2, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v2, v1}, LX/4Ac;->a(LX/1aX;)V

    .line 1787666
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1787667
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/BTV;->g:Landroid/graphics/Rect;

    .line 1787668
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/BTV;->h:Landroid/graphics/Rect;

    .line 1787669
    return-void
.end method


# virtual methods
.method public final a(LX/BTR;I)V
    .locals 4

    .prologue
    .line 1787640
    iget-object v0, p0, LX/BTV;->a:LX/BTR;

    if-eq v0, p1, :cond_1

    .line 1787641
    const/4 v2, 0x0

    .line 1787642
    move v1, v2

    :goto_0
    iget v0, p0, LX/BTV;->c:I

    if-ge v1, v0, :cond_0

    .line 1787643
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1787644
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/1aX;->a(LX/1aZ;)V

    .line 1787645
    iget-object v3, p0, LX/BTV;->f:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1787646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1787647
    :cond_0
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1787648
    iput v2, p0, LX/BTV;->c:I

    .line 1787649
    iput-object p1, p0, LX/BTV;->a:LX/BTR;

    .line 1787650
    iget-object v0, p1, LX/BTR;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v0, v0

    .line 1787651
    iput v0, p0, LX/BTV;->b:I

    .line 1787652
    invoke-virtual {p0, p2}, LX/BTV;->setScrollX(I)V

    .line 1787653
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1787639
    return-void
.end method

.method public getAnimatedAlpha$134621()I
    .locals 1

    .prologue
    .line 1787638
    const/16 v0, 0xff

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1787676
    invoke-virtual {p0}, LX/BTV;->invalidate()V

    .line 1787677
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x58b0901

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1787635
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1787636
    iget-object v1, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1787637
    const/16 v1, 0x2d

    const v2, 0x22d30113

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x574dcdc0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1787632
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1787633
    iget-object v1, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1787634
    const/16 v1, 0x2d

    const v2, 0x501caa1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1787617
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1787618
    const/4 v1, 0x0

    .line 1787619
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    iget v0, p0, LX/BTV;->c:I

    if-ge v1, v0, :cond_1

    .line 1787620
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1787621
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1787622
    iget-object v4, p0, LX/BTV;->a:LX/BTR;

    iget-object v5, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v5}, LX/BTR;->a(ILandroid/graphics/Rect;)V

    .line 1787623
    iget-object v0, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, LX/BTV;->a(Landroid/graphics/Rect;)V

    .line 1787624
    if-eqz v2, :cond_0

    .line 1787625
    iget-object v0, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v0, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1787626
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1787627
    :cond_0
    iget-object v0, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, LX/BTV;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1787628
    invoke-virtual {p0}, LX/BTV;->getAnimatedAlpha$134621()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1787629
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1787630
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v2, v3

    goto :goto_0

    .line 1787631
    :cond_1
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1787614
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1787615
    iget-object v0, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1787616
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1787611
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1787612
    iget-object v0, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1787613
    return-void
.end method

.method public setScrollX(I)V
    .locals 4

    .prologue
    .line 1787582
    invoke-super {p0, p1}, Landroid/view/View;->setScrollX(I)V

    .line 1787583
    const/4 p1, 0x0

    .line 1787584
    iget-object v0, p0, LX/BTV;->g:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, LX/BTV;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1787585
    iget v0, p0, LX/BTV;->c:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1787586
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1787587
    iget-object v2, p0, LX/BTV;->a:LX/BTR;

    iget-object v3, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v3}, LX/BTR;->a(ILandroid/graphics/Rect;)V

    .line 1787588
    iget-object v0, p0, LX/BTV;->g:Landroid/graphics/Rect;

    iget-object v2, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-static {v0, v2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1787589
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1787590
    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 1787591
    iget-object v2, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->removeAt(I)V

    .line 1787592
    iget-object v2, p0, LX/BTV;->f:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1787593
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1787594
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget v0, p0, LX/BTV;->b:I

    if-ge v1, v0, :cond_5

    .line 1787595
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1787596
    if-nez v0, :cond_2

    .line 1787597
    iget-object v0, p0, LX/BTV;->a:LX/BTR;

    iget-object v2, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2}, LX/BTR;->a(ILandroid/graphics/Rect;)V

    .line 1787598
    iget-object v0, p0, LX/BTV;->g:Landroid/graphics/Rect;

    iget-object v2, p0, LX/BTV;->h:Landroid/graphics/Rect;

    invoke-static {v0, v2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1787599
    iget-object v0, p0, LX/BTV;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1787600
    if-eqz v0, :cond_2

    .line 1787601
    iget-object v2, p0, LX/BTV;->a:LX/BTR;

    invoke-virtual {v2, v1}, LX/BTR;->a(I)LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1aX;->a(LX/1aZ;)V

    .line 1787602
    iget-object v2, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1787603
    if-nez v1, :cond_3

    .line 1787604
    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v2, p0, LX/BTV;->i:LX/4Ab;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/4Ab;)V

    .line 1787605
    :cond_2
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1787606
    :cond_3
    iget v2, p0, LX/BTV;->b:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_4

    .line 1787607
    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v2, p0, LX/BTV;->j:LX/4Ab;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/4Ab;)V

    goto :goto_3

    .line 1787608
    :cond_4
    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(LX/4Ab;)V

    goto :goto_3

    .line 1787609
    :cond_5
    iget-object v0, p0, LX/BTV;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    iput v0, p0, LX/BTV;->c:I

    .line 1787610
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1787580
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    .line 1787581
    iget-object v0, p0, LX/BTV;->d:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
