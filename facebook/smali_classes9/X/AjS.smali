.class public LX/AjS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0aG;

.field private c:LX/0kb;

.field private d:LX/1Ck;

.field private e:LX/00H;

.field private f:LX/17S;

.field private g:LX/3iX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1708001
    const-class v0, LX/AjS;

    sput-object v0, LX/AjS;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0kb;LX/1Ck;LX/00H;LX/17S;LX/3iX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708022
    iput-object p2, p0, LX/AjS;->c:LX/0kb;

    .line 1708023
    iput-object p1, p0, LX/AjS;->b:LX/0aG;

    .line 1708024
    iput-object p3, p0, LX/AjS;->d:LX/1Ck;

    .line 1708025
    iput-object p4, p0, LX/AjS;->e:LX/00H;

    .line 1708026
    iput-object p5, p0, LX/AjS;->f:LX/17S;

    .line 1708027
    iput-object p6, p0, LX/AjS;->g:LX/3iX;

    .line 1708028
    return-void
.end method

.method public static a(LX/AjS;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;",
            ")",
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1708013
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1708014
    invoke-static {p1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AjS;->f:LX/17S;

    invoke-virtual {v1}, LX/17S;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1708015
    iget-object v1, p0, LX/AjS;->f:LX/17S;

    invoke-virtual {v1}, LX/17S;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1708016
    iget-object v1, p0, LX/AjS;->g:LX/3iX;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    .line 1708017
    invoke-static {v1, v2}, LX/3iX;->c(LX/3iX;Ljava/lang/String;)LX/5Og;

    move-result-object p0

    .line 1708018
    iput-object p2, p0, LX/5Og;->b:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 1708019
    :cond_0
    :goto_0
    invoke-static {v0}, LX/55K;->getSupportedUnitTypes(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1708020
    :cond_1
    sget-object v1, LX/55K;->INSTAGRAM_PHOTO_CHAINING:LX/55K;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(LX/AjS;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;LX/0Px;ZLcom/facebook/common/callercontext/CallerContext;LX/2h0;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;",
            "LX/0Px",
            "<",
            "LX/55K;",
            ">;Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/2h0;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1708029
    invoke-static {p0, p1}, LX/AjS;->a(LX/AjS;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1708030
    :goto_0
    return-void

    .line 1708031
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1708032
    iget-object v1, p0, LX/AjS;->g:LX/3iX;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/3iX;->a(Ljava/lang/String;Z)V

    .line 1708033
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1708034
    invoke-static {p0, v8}, LX/AjS;->a(LX/AjS;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1708035
    :cond_1
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1708036
    const-string v10, "fetchFollowUpFeedUnitParamsKey"

    new-instance v1, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xa

    move-object v4, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;ILX/0Px;Z)V

    invoke-virtual {v9, v10, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1708037
    iget-object v1, p0, LX/AjS;->b:LX/0aG;

    const-string v2, "feed_fetch_followup_feed_unit"

    const v3, 0x5a64d9ff

    move-object/from16 v0, p5

    invoke-static {v1, v2, v9, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 1708038
    iget-object v2, p0, LX/AjS;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fetchFollowUpFeedUnit_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v1, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method private static a(LX/AjS;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 1708011
    iget-object v0, p0, LX/AjS;->g:LX/3iX;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/3iX;->a(Ljava/lang/String;Z)V

    .line 1708012
    return-void
.end method

.method private static a(LX/AjS;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1708002
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1708003
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1708004
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/AjS;->g:LX/3iX;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1708005
    invoke-static {v1, v0}, LX/3iX;->c(LX/3iX;Ljava/lang/String;)LX/5Og;

    move-result-object p1

    .line 1708006
    iget-boolean v1, p1, LX/5Og;->a:Z

    move p1, v1

    .line 1708007
    move v0, p1

    .line 1708008
    if-nez v0, :cond_0

    iget-object v0, p0, LX/AjS;->e:LX/00H;

    .line 1708009
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 1708010
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
