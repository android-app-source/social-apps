.class public final LX/Bxl;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:LX/Bxo;

.field public final synthetic b:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;)V
    .locals 0

    .prologue
    .line 1835990
    iput-object p1, p0, LX/Bxl;->b:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    iput-object p2, p0, LX/Bxl;->a:LX/Bxo;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1835986
    iget-object v0, p0, LX/Bxl;->a:LX/Bxo;

    iget-object v0, v0, LX/Bxo;->h:LX/ByP;

    sget-object v1, LX/6Wv;->DONE_LOADING:LX/6Wv;

    invoke-virtual {v0, v1}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1835987
    if-eqz p3, :cond_0

    .line 1835988
    invoke-interface {p3}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1835989
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1835981
    iget-object v0, p0, LX/Bxl;->a:LX/Bxo;

    sget-object v1, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    iput-object v1, v0, LX/Bxo;->g:LX/Bxn;

    .line 1835982
    iget-object v0, p0, LX/Bxl;->a:LX/Bxo;

    iget-object v0, v0, LX/Bxo;->h:LX/ByP;

    if-eqz v0, :cond_0

    .line 1835983
    iget-object v0, p0, LX/Bxl;->a:LX/Bxo;

    iget-object v0, v0, LX/Bxo;->h:LX/ByP;

    iget-object v1, p0, LX/Bxl;->a:LX/Bxo;

    iget-object v1, v1, LX/Bxo;->a:LX/1aZ;

    invoke-virtual {v0, v1}, LX/ByP;->setImageController(LX/1aZ;)V

    .line 1835984
    iget-object v0, p0, LX/Bxl;->a:LX/Bxo;

    iget-object v0, v0, LX/Bxo;->h:LX/ByP;

    sget-object v1, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v0, v1}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1835985
    :cond_0
    return-void
.end method
