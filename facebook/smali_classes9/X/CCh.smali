.class public LX/CCh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0wM;

.field public final d:LX/1aw;

.field public final e:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/CCk;

.field public final g:LX/D1j;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/res/Resources;LX/1Ns;LX/1Vm;LX/CCk;LX/0wM;LX/D1j;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1858192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858193
    iput-object p1, p0, LX/CCh;->a:Landroid/app/Activity;

    .line 1858194
    iput-object p2, p0, LX/CCh;->b:Landroid/content/res/Resources;

    .line 1858195
    new-instance v0, LX/1Qg;

    invoke-direct {v0}, LX/1Qg;-><init>()V

    sget-object v1, LX/1aw;->a:LX/1DQ;

    invoke-virtual {p3, v0, v1}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v0

    iput-object v0, p0, LX/CCh;->d:LX/1aw;

    .line 1858196
    iput-object p6, p0, LX/CCh;->c:LX/0wM;

    .line 1858197
    iput-object p4, p0, LX/CCh;->e:LX/1Vm;

    .line 1858198
    iput-object p5, p0, LX/CCh;->f:LX/CCk;

    .line 1858199
    iput-object p7, p0, LX/CCh;->g:LX/D1j;

    .line 1858200
    return-void
.end method

.method public static a(LX/0QB;)LX/CCh;
    .locals 11

    .prologue
    .line 1858201
    const-class v1, LX/CCh;

    monitor-enter v1

    .line 1858202
    :try_start_0
    sget-object v0, LX/CCh;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858203
    sput-object v2, LX/CCh;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858206
    new-instance v3, LX/CCh;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const-class v6, LX/1Ns;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1Ns;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v7

    check-cast v7, LX/1Vm;

    invoke-static {v0}, LX/CCk;->a(LX/0QB;)LX/CCk;

    move-result-object v8

    check-cast v8, LX/CCk;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-static {v0}, LX/D1j;->a(LX/0QB;)LX/D1j;

    move-result-object v10

    check-cast v10, LX/D1j;

    invoke-direct/range {v3 .. v10}, LX/CCh;-><init>(Landroid/app/Activity;Landroid/content/res/Resources;LX/1Ns;LX/1Vm;LX/CCk;LX/0wM;LX/D1j;)V

    .line 1858207
    move-object v0, v3

    .line 1858208
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858209
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858210
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
