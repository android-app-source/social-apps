.class public final LX/BYb;
.super LX/7WR;
.source ""


# instance fields
.field public final synthetic a:LX/BYc;


# direct methods
.method public constructor <init>(LX/BYc;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1796320
    iput-object p1, p0, LX/BYb;->a:LX/BYc;

    invoke-direct {p0, p2}, LX/7WR;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x3e8

    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1796321
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1796322
    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->r()I

    move-result v0

    int-to-long v0, v0

    mul-long v2, v0, v4

    .line 1796323
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1796324
    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->q()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    .line 1796325
    cmp-long v4, v2, v8

    if-ltz v4, :cond_0

    .line 1796326
    iget-object v4, p0, LX/BYb;->a:LX/BYc;

    invoke-static {v4, v7, v2, v3}, LX/BYc;->a$redex0(LX/BYc;ZJ)V

    .line 1796327
    iget-object v2, p0, LX/BYb;->a:LX/BYc;

    invoke-static {v2, v6, v0, v1}, LX/BYc;->a$redex0(LX/BYc;ZJ)V

    .line 1796328
    :goto_0
    return-void

    .line 1796329
    :cond_0
    cmp-long v2, v2, v8

    if-gez v2, :cond_1

    const-wide/32 v2, 0x927c0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1796330
    iget-object v2, p0, LX/BYb;->a:LX/BYc;

    const-wide/32 v4, 0xea60

    invoke-static {v2, v7, v4, v5}, LX/BYc;->a$redex0(LX/BYc;ZJ)V

    .line 1796331
    iget-object v2, p0, LX/BYb;->a:LX/BYc;

    invoke-static {v2, v6, v0, v1}, LX/BYc;->a$redex0(LX/BYc;ZJ)V

    .line 1796332
    :cond_1
    iget-object v0, p0, LX/BYb;->a:LX/BYc;

    invoke-virtual {v0}, LX/BYc;->c()V

    goto :goto_0
.end method
