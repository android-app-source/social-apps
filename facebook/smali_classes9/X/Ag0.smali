.class public final LX/Ag0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Wt;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/AcU;

.field public final synthetic c:LX/Ag1;


# direct methods
.method public constructor <init>(LX/Ag1;Ljava/util/List;LX/AcU;)V
    .locals 0

    .prologue
    .line 1700050
    iput-object p1, p0, LX/Ag0;->c:LX/Ag1;

    iput-object p2, p0, LX/Ag0;->a:Ljava/util/List;

    iput-object p3, p0, LX/Ag0;->b:LX/AcU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1700051
    iget-object v0, p0, LX/Ag0;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aeu;

    .line 1700052
    iget-object v1, v0, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1700053
    iget-object v1, v0, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1700054
    iput-object v1, v0, LX/Aeu;->j:Ljava/lang/String;

    .line 1700055
    invoke-static {v1}, LX/Aeu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, LX/Aeu;->k:Ljava/lang/String;

    .line 1700056
    if-eqz v1, :cond_3

    iget-object v3, v0, LX/Aeu;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    :goto_1
    iput-boolean v3, v0, LX/Aeu;->q:Z

    .line 1700057
    goto :goto_0

    .line 1700058
    :cond_1
    iget-object v0, p0, LX/Ag0;->b:LX/AcU;

    if-eqz v0, :cond_2

    .line 1700059
    iget-object v0, p0, LX/Ag0;->b:LX/AcU;

    .line 1700060
    iget-object v1, v0, LX/AcU;->a:LX/AcX;

    iget-object v1, v1, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1700061
    iget-object v1, v0, LX/AcU;->a:LX/AcX;

    iget-object v1, v1, LX/AcX;->l:LX/AfJ;

    invoke-virtual {v1}, LX/AfJ;->f()V

    .line 1700062
    :cond_2
    return-void

    .line 1700063
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final hv_()V
    .locals 0

    .prologue
    .line 1700064
    return-void
.end method
