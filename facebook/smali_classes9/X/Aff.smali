.class public final LX/Aff;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Afg;


# direct methods
.method public constructor <init>(LX/Afg;)V
    .locals 0

    .prologue
    .line 1699499
    iput-object p1, p0, LX/Aff;->a:LX/Afg;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1699500
    iget-object v2, p0, LX/Aff;->a:LX/Afg;

    monitor-enter v2

    .line 1699501
    :try_start_0
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v1, v1, LX/Afg;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v1, v1, LX/Afg;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1699502
    :cond_0
    monitor-exit v2

    .line 1699503
    :goto_0
    return-void

    .line 1699504
    :cond_1
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v3, v1, LX/Afg;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/Afg;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_graphFailure"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Failed to get like events for "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/Aff;->a:LX/Afg;

    iget-object v5, v5, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v1, v1, LX/AeQ;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v4, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699505
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-boolean v1, v1, LX/AeQ;->e:Z

    if-nez v1, :cond_4

    .line 1699506
    :goto_2
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    const/4 v3, 0x1

    .line 1699507
    iput-boolean v3, v1, LX/Afg;->e:Z

    .line 1699508
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    if-eqz v1, :cond_2

    .line 1699509
    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    iget-object v3, p0, LX/Aff;->a:LX/Afg;

    invoke-virtual {v3}, LX/Afg;->g()LX/AeN;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/AeV;->a(LX/AeN;Z)V

    .line 1699510
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1699511
    :cond_3
    :try_start_1
    const-string v1, "no story id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1699512
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699513
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1699514
    iget-object v2, p0, LX/Aff;->a:LX/Afg;

    monitor-enter v2

    .line 1699515
    :try_start_0
    iget-object v0, p0, LX/Aff;->a:LX/Afg;

    iget-object v0, v0, LX/Afg;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aff;->a:LX/Afg;

    iget-object v0, v0, LX/Afg;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1699516
    :cond_0
    monitor-exit v2

    .line 1699517
    :goto_0
    return-void

    .line 1699518
    :cond_1
    if-nez p1, :cond_2

    .line 1699519
    monitor-exit v2

    goto :goto_0

    .line 1699520
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1699521
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699522
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel;

    .line 1699523
    if-nez v0, :cond_3

    .line 1699524
    monitor-exit v2

    goto :goto_0

    .line 1699525
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel;

    move-result-object v0

    .line 1699526
    if-nez v0, :cond_4

    .line 1699527
    monitor-exit v2

    goto :goto_0

    .line 1699528
    :cond_4
    iget-object v3, p0, LX/Aff;->a:LX/Afg;

    const/4 v4, 0x1

    .line 1699529
    iput-boolean v4, v3, LX/Afg;->e:Z

    .line 1699530
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel$NotableLikedCommentsModel;

    move-result-object v0

    .line 1699531
    if-nez v0, :cond_5

    .line 1699532
    monitor-exit v2

    goto :goto_0

    .line 1699533
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel$NotableLikedCommentsModel;->a()LX/0Px;

    move-result-object v3

    .line 1699534
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1699535
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel$NotableLikedCommentsModel$EdgesModel;

    .line 1699536
    iget-object v6, p0, LX/Aff;->a:LX/Afg;

    iget-object v6, v6, LX/Afg;->j:Ljava/lang/String;

    const/4 p1, 0x0

    const/4 v8, 0x0

    .line 1699537
    if-eqz v0, :cond_6

    if-nez v6, :cond_9

    :cond_6
    move-object v7, v8

    .line 1699538
    :goto_2
    move-object v0, v7

    .line 1699539
    const/4 v6, 0x0

    invoke-interface {v4, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1699540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1699541
    :cond_7
    iget-object v0, p0, LX/Aff;->a:LX/Afg;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    if-eqz v0, :cond_8

    .line 1699542
    iget-object v0, p0, LX/Aff;->a:LX/Afg;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    iget-object v1, p0, LX/Aff;->a:LX/Afg;

    invoke-virtual {v1}, LX/Afg;->g()LX/AeN;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v4, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    .line 1699543
    :cond_8
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1699544
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel$FeedbackModel$NotableLikedCommentsModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v9

    .line 1699545
    if-nez v9, :cond_a

    move-object v7, v8

    .line 1699546
    goto :goto_2

    .line 1699547
    :cond_a
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v10

    .line 1699548
    if-nez v10, :cond_b

    move-object v7, v8

    .line 1699549
    goto :goto_2

    .line 1699550
    :cond_b
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->o()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$NotableLikersModel;

    move-result-object v7

    .line 1699551
    if-nez v7, :cond_c

    move-object v7, v8

    .line 1699552
    goto :goto_2

    .line 1699553
    :cond_c
    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$NotableLikersModel;->a()LX/0Px;

    move-result-object v7

    .line 1699554
    invoke-static {v7}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v11

    if-eqz v11, :cond_d

    move-object v7, v8

    .line 1699555
    goto :goto_2

    .line 1699556
    :cond_d
    invoke-virtual {v7, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$NotableLikersModel$EdgesModel;

    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$NotableLikersModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v7

    .line 1699557
    if-nez v7, :cond_e

    move-object v7, v8

    .line 1699558
    goto :goto_2

    .line 1699559
    :cond_e
    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 1699560
    if-eqz v11, :cond_f

    move-object v7, v8

    .line 1699561
    goto :goto_2

    .line 1699562
    :cond_f
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v9

    .line 1699563
    if-nez v9, :cond_10

    move-object v7, v8

    .line 1699564
    goto :goto_2

    .line 1699565
    :cond_10
    invoke-static {v10}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v8

    .line 1699566
    invoke-static {v7}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v10

    .line 1699567
    invoke-virtual {v9}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 1699568
    invoke-static {v8, v7, p1}, LX/Aeu;->a(LX/AcC;Ljava/lang/String;I)LX/Aeu;

    move-result-object v8

    .line 1699569
    new-instance v7, LX/Afd;

    invoke-direct {v7, v10, v8}, LX/Afd;-><init>(LX/AcC;LX/Aeu;)V

    goto :goto_2
.end method
