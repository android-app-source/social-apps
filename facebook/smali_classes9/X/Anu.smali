.class public final LX/Anu;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Anw;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:LX/1Wk;

.field public f:I

.field public g:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public h:I

.field public i:LX/20Z;

.field public final synthetic j:LX/Anw;


# direct methods
.method public constructor <init>(LX/Anw;)V
    .locals 1

    .prologue
    .line 1712987
    iput-object p1, p0, LX/Anu;->j:LX/Anw;

    .line 1712988
    move-object v0, p1

    .line 1712989
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1712990
    const/4 v0, 0x0

    iput v0, p0, LX/Anu;->h:I

    .line 1712991
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1712992
    const-string v0, "BasicFooterButtonsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1712993
    if-ne p0, p1, :cond_1

    .line 1712994
    :cond_0
    :goto_0
    return v0

    .line 1712995
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1712996
    goto :goto_0

    .line 1712997
    :cond_3
    check-cast p1, LX/Anu;

    .line 1712998
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1712999
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1713000
    if-eq v2, v3, :cond_0

    .line 1713001
    iget-object v2, p0, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1713002
    goto :goto_0

    .line 1713003
    :cond_5
    iget-object v2, p1, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1713004
    :cond_6
    iget-boolean v2, p0, LX/Anu;->b:Z

    iget-boolean v3, p1, LX/Anu;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1713005
    goto :goto_0

    .line 1713006
    :cond_7
    iget-boolean v2, p0, LX/Anu;->c:Z

    iget-boolean v3, p1, LX/Anu;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1713007
    goto :goto_0

    .line 1713008
    :cond_8
    iget-boolean v2, p0, LX/Anu;->d:Z

    iget-boolean v3, p1, LX/Anu;->d:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1713009
    goto :goto_0

    .line 1713010
    :cond_9
    iget-object v2, p0, LX/Anu;->e:LX/1Wk;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Anu;->e:LX/1Wk;

    iget-object v3, p1, LX/Anu;->e:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1713011
    goto :goto_0

    .line 1713012
    :cond_b
    iget-object v2, p1, LX/Anu;->e:LX/1Wk;

    if-nez v2, :cond_a

    .line 1713013
    :cond_c
    iget v2, p0, LX/Anu;->f:I

    iget v3, p1, LX/Anu;->f:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1713014
    goto :goto_0

    .line 1713015
    :cond_d
    iget-object v2, p0, LX/Anu;->g:LX/1Pr;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/Anu;->g:LX/1Pr;

    iget-object v3, p1, LX/Anu;->g:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1713016
    goto :goto_0

    .line 1713017
    :cond_f
    iget-object v2, p1, LX/Anu;->g:LX/1Pr;

    if-nez v2, :cond_e

    .line 1713018
    :cond_10
    iget v2, p0, LX/Anu;->h:I

    iget v3, p1, LX/Anu;->h:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1713019
    goto :goto_0

    .line 1713020
    :cond_11
    iget-object v2, p0, LX/Anu;->i:LX/20Z;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/Anu;->i:LX/20Z;

    iget-object v3, p1, LX/Anu;->i:LX/20Z;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1713021
    goto/16 :goto_0

    .line 1713022
    :cond_12
    iget-object v2, p1, LX/Anu;->i:LX/20Z;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
