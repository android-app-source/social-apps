.class public final LX/ATp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V
    .locals 0

    .prologue
    .line 1676903
    iput-object p1, p0, LX/ATp;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4d5c5f40    # 2.31076864E8f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1676904
    iget-object v1, p0, LX/ATp;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    if-eqz v1, :cond_2

    .line 1676905
    iget-object v1, p0, LX/ATp;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    .line 1676906
    const/4 v9, 0x1

    .line 1676907
    iget-object v4, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v4

    .line 1676908
    if-nez v4, :cond_0

    .line 1676909
    invoke-static {}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v4

    .line 1676910
    :cond_0
    iget-object v5, v1, LX/ATs;->x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    if-nez v5, :cond_3

    .line 1676911
    new-instance v5, LX/5SM;

    invoke-direct {v5}, LX/5SM;-><init>()V

    sget-object v6, LX/5SK;->TRIM:LX/5SK;

    invoke-virtual {v5, v6}, LX/5SM;->a(LX/5SK;)LX/5SM;

    move-result-object v5

    iget-object v6, v1, LX/ATs;->m:Ljava/lang/String;

    .line 1676912
    iput-object v6, v5, LX/5SM;->b:Ljava/lang/String;

    .line 1676913
    move-object v5, v5

    .line 1676914
    iput-boolean v9, v5, LX/5SM;->d:Z

    .line 1676915
    move-object v5, v5

    .line 1676916
    iput-boolean v9, v5, LX/5SM;->e:Z

    .line 1676917
    move-object v5, v5

    .line 1676918
    iget-object v6, v1, LX/ATs;->f:LX/0ad;

    sget-short v7, LX/8tG;->h:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    .line 1676919
    iput-boolean v6, v5, LX/5SM;->j:Z

    .line 1676920
    move-object v5, v5

    .line 1676921
    iput-boolean v9, v5, LX/5SM;->g:Z

    .line 1676922
    move-object v5, v5

    .line 1676923
    iget-object v6, v1, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v6}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getCurrentPositionMs()I

    move-result v6

    .line 1676924
    iput v6, v5, LX/5SM;->o:I

    .line 1676925
    move-object v5, v5

    .line 1676926
    invoke-virtual {v5}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v5

    iput-object v5, v1, LX/ATs;->x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1676927
    :goto_0
    new-instance v5, LX/5SM;

    iget-object v6, v1, LX/ATs;->x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v5, v6}, LX/5SM;-><init>(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V

    .line 1676928
    iput-object v4, v5, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1676929
    move-object v4, v5

    .line 1676930
    iget-object v5, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->h()Ljava/lang/String;

    move-result-object v5

    .line 1676931
    iput-object v5, v4, LX/5SM;->q:Ljava/lang/String;

    .line 1676932
    move-object v4, v4

    .line 1676933
    invoke-virtual {v4}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v5

    .line 1676934
    iget-object v4, v1, LX/ATs;->j:LX/BTG;

    iget-object v6, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/BTC;->TAP_PLAYER:LX/BTC;

    iget-object v8, v1, LX/ATs;->m:Ljava/lang/String;

    iget-object v9, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v9}, Lcom/facebook/composer/attachments/ComposerAttachment;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v6, v7, v8, v9}, LX/BTG;->a(Ljava/lang/String;LX/BTC;Ljava/lang/String;Ljava/lang/String;)V

    .line 1676935
    iget-object v4, v1, LX/ATs;->y:LX/BSt;

    if-nez v4, :cond_1

    .line 1676936
    new-instance v4, LX/BSt;

    iget-object v6, v1, LX/ATs;->l:LX/0gc;

    invoke-direct {v4, v6}, LX/BSt;-><init>(LX/0gc;)V

    iput-object v4, v1, LX/ATs;->y:LX/BSt;

    .line 1676937
    :cond_1
    iget-object v4, v1, LX/ATs;->k:LX/BSu;

    invoke-virtual {v4}, LX/BSu;->a()V

    .line 1676938
    iget-object v4, v1, LX/ATs;->y:LX/BSt;

    iget-object v6, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v1, LX/ATs;->b:LX/ATX;

    const-string v8, "composer"

    iget-object v9, v1, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-static {v9}, LX/9fh;->a(Landroid/view/View;)LX/9fh;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, LX/BSt;->a(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Landroid/net/Uri;LX/ATX;Ljava/lang/String;LX/9fh;)V

    .line 1676939
    iget-object v3, v1, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v3}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a()V

    .line 1676940
    :cond_2
    const v1, 0x7720b8bd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1676941
    :cond_3
    new-instance v5, LX/5SM;

    iget-object v6, v1, LX/ATs;->x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v5, v6}, LX/5SM;-><init>(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V

    iget-object v6, v1, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v6}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getCurrentPositionMs()I

    move-result v6

    .line 1676942
    iput v6, v5, LX/5SM;->o:I

    .line 1676943
    move-object v5, v5

    .line 1676944
    invoke-virtual {v5}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v5

    iput-object v5, v1, LX/ATs;->x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    goto :goto_0
.end method
