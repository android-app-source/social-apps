.class public final LX/Apd;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Apd;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Apb;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Ape;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716407
    const/4 v0, 0x0

    sput-object v0, LX/Apd;->a:LX/Apd;

    .line 1716408
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apd;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1716437
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716438
    new-instance v0, LX/Ape;

    invoke-direct {v0}, LX/Ape;-><init>()V

    iput-object v0, p0, LX/Apd;->c:LX/Ape;

    .line 1716439
    return-void
.end method

.method public static c(LX/1De;)LX/Apb;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716429
    new-instance v1, LX/Apc;

    invoke-direct {v1}, LX/Apc;-><init>()V

    .line 1716430
    sget-object v2, LX/Apd;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Apb;

    .line 1716431
    if-nez v2, :cond_0

    .line 1716432
    new-instance v2, LX/Apb;

    invoke-direct {v2}, LX/Apb;-><init>()V

    .line 1716433
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Apb;->a$redex0(LX/Apb;LX/1De;IILX/Apc;)V

    .line 1716434
    move-object v1, v2

    .line 1716435
    move-object v0, v1

    .line 1716436
    return-object v0
.end method

.method public static declared-synchronized q()LX/Apd;
    .locals 2

    .prologue
    .line 1716440
    const-class v1, LX/Apd;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Apd;->a:LX/Apd;

    if-nez v0, :cond_0

    .line 1716441
    new-instance v0, LX/Apd;

    invoke-direct {v0}, LX/Apd;-><init>()V

    sput-object v0, LX/Apd;->a:LX/Apd;

    .line 1716442
    :cond_0
    sget-object v0, LX/Apd;->a:LX/Apd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1716443
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716427
    invoke-static {}, LX/1dS;->b()V

    .line 1716428
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1716410
    check-cast p4, LX/Apc;

    .line 1716411
    iget-object v3, p4, LX/Apc;->a:Ljava/lang/CharSequence;

    iget-object v4, p4, LX/Apc;->b:Ljava/lang/CharSequence;

    iget-object v5, p4, LX/Apc;->c:Ljava/lang/CharSequence;

    move-object v0, p1

    move v1, p2

    move v2, p3

    const/4 v7, 0x0

    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 1716412
    sget v6, LX/Ape;->a:I

    const/4 v8, 0x2

    invoke-static {v0, v3, v6, v8}, LX/Ape;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v6

    .line 1716413
    if-nez v6, :cond_2

    move-object v6, v7

    .line 1716414
    :goto_0
    const/4 v8, 0x1

    const/4 p2, 0x0

    .line 1716415
    invoke-static {v1}, LX/1mh;->a(I)I

    move-result p3

    .line 1716416
    if-nez p3, :cond_5

    .line 1716417
    :cond_0
    :goto_1
    move p2, v8

    .line 1716418
    if-eqz p2, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v8, p0

    .line 1716419
    :goto_2
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p3

    invoke-interface {p3, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p3

    invoke-interface {p3, p1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p1

    const/4 p3, 0x4

    invoke-interface {p1, p3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p1

    const/4 p3, 0x7

    const p4, 0x7f0b1169

    invoke-interface {p1, p3, p4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object p1

    if-eqz p2, :cond_4

    sget v6, LX/Ape;->b:I

    invoke-static {v0, v5, v6, p0}, LX/Ape;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v6

    :goto_3
    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    if-eqz v8, :cond_1

    sget v7, LX/Ape;->c:I

    invoke-static {v0, v5, v7, p0}, LX/Ape;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v7

    :cond_1
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1716420
    return-object v0

    .line 1716421
    :cond_2
    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    goto :goto_0

    :cond_3
    move v8, p1

    .line 1716422
    goto :goto_2

    :cond_4
    move-object v6, v7

    .line 1716423
    goto :goto_3

    .line 1716424
    :cond_5
    new-instance p3, LX/1no;

    invoke-direct {p3}, LX/1no;-><init>()V

    .line 1716425
    invoke-static {p2, p2}, LX/1mh;->a(II)I

    move-result p4

    invoke-virtual {v6, v0, p4, v2, p3}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1716426
    iget p3, p3, LX/1no;->a:I

    invoke-static {v1}, LX/1mh;->b(I)I

    move-result p4

    if-le p3, p4, :cond_0

    move v8, p2

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1716409
    const/4 v0, 0x1

    return v0
.end method
