.class public LX/AyZ;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

.field public final b:LX/BMQ;


# direct methods
.method public constructor <init>(LX/B5j;Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;Landroid/content/Context;LX/BMQ;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;",
            "Landroid/content/Context;",
            "LX/BMQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1730139
    invoke-direct {p0, p3, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1730140
    iput-object p2, p0, LX/AyZ;->a:Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

    .line 1730141
    iput-object p4, p0, LX/AyZ;->b:LX/BMQ;

    .line 1730142
    return-void
.end method


# virtual methods
.method public final aE()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730137
    new-instance v0, LX/AyX;

    invoke-direct {v0, p0}, LX/AyX;-><init>(LX/AyZ;)V

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730138
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730136
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730135
    new-instance v0, LX/AyW;

    invoke-direct {v0, p0}, LX/AyW;-><init>(LX/AyZ;)V

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730134
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
