.class public final LX/COj;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COk;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLVideo;

.field public b:LX/2pa;

.field public c:Landroid/view/View$OnClickListener;

.field public final synthetic d:LX/COk;


# direct methods
.method public constructor <init>(LX/COk;)V
    .locals 1

    .prologue
    .line 1883459
    iput-object p1, p0, LX/COj;->d:LX/COk;

    .line 1883460
    move-object v0, p1

    .line 1883461
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883462
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883447
    const-string v0, "NTVideoPlayerComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/COk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1883463
    check-cast p1, LX/COj;

    .line 1883464
    iget-object v0, p1, LX/COj;->b:LX/2pa;

    iput-object v0, p0, LX/COj;->b:LX/2pa;

    .line 1883465
    iget-object v0, p1, LX/COj;->c:Landroid/view/View$OnClickListener;

    iput-object v0, p0, LX/COj;->c:Landroid/view/View$OnClickListener;

    .line 1883466
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1883448
    if-ne p0, p1, :cond_1

    .line 1883449
    :cond_0
    :goto_0
    return v0

    .line 1883450
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883451
    goto :goto_0

    .line 1883452
    :cond_3
    check-cast p1, LX/COj;

    .line 1883453
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883454
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883455
    if-eq v2, v3, :cond_0

    .line 1883456
    iget-object v2, p0, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v3, p1, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1883457
    goto :goto_0

    .line 1883458
    :cond_4
    iget-object v2, p1, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1883442
    const/4 v1, 0x0

    .line 1883443
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/COj;

    .line 1883444
    iput-object v1, v0, LX/COj;->b:LX/2pa;

    .line 1883445
    iput-object v1, v0, LX/COj;->c:Landroid/view/View$OnClickListener;

    .line 1883446
    return-object v0
.end method
