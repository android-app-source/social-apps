.class public LX/AVH;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/AVH;


# instance fields
.field private final a:LX/1E5;

.field public final b:LX/1b2;


# direct methods
.method public constructor <init>(LX/1E5;LX/1b2;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679122
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1679123
    iput-object p1, p0, LX/AVH;->a:LX/1E5;

    .line 1679124
    iput-object p2, p0, LX/AVH;->b:LX/1b2;

    .line 1679125
    const-string v0, "composer/?view={live}&text={text}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/AVG;

    invoke-direct {v1, p0}, LX/AVG;-><init>(LX/AVH;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1679126
    const-string v0, "composer/?view={live}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/AVG;

    invoke-direct {v1, p0}, LX/AVG;-><init>(LX/AVH;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1679127
    return-void
.end method

.method public static a(LX/0QB;)LX/AVH;
    .locals 5

    .prologue
    .line 1679129
    sget-object v0, LX/AVH;->c:LX/AVH;

    if-nez v0, :cond_1

    .line 1679130
    const-class v1, LX/AVH;

    monitor-enter v1

    .line 1679131
    :try_start_0
    sget-object v0, LX/AVH;->c:LX/AVH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679132
    if-eqz v2, :cond_0

    .line 1679133
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679134
    new-instance p0, LX/AVH;

    invoke-static {v0}, LX/1E5;->b(LX/0QB;)LX/1E5;

    move-result-object v3

    check-cast v3, LX/1E5;

    invoke-static {v0}, LX/1b2;->a(LX/0QB;)LX/1b2;

    move-result-object v4

    check-cast v4, LX/1b2;

    invoke-direct {p0, v3, v4}, LX/AVH;-><init>(LX/1E5;LX/1b2;)V

    .line 1679135
    move-object v0, p0

    .line 1679136
    sput-object v0, LX/AVH;->c:LX/AVH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679137
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679138
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679139
    :cond_1
    sget-object v0, LX/AVH;->c:LX/AVH;

    return-object v0

    .line 1679140
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1679128
    iget-object v0, p0, LX/AVH;->a:LX/1E5;

    invoke-virtual {v0}, LX/1E5;->a()Z

    move-result v0

    return v0
.end method
