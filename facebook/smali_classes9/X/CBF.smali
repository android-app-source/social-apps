.class public final LX/CBF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AmK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AmK",
        "<",
        "LX/CBz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

.field public final synthetic b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;)V
    .locals 0

    .prologue
    .line 1856217
    iput-object p1, p0, LX/CBF;->b:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;

    iput-object p2, p0, LX/CBF;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 1856218
    iget-object v0, p0, LX/CBF;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1bf;
    .locals 1

    .prologue
    .line 1856219
    invoke-direct {p0}, LX/CBF;->b()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1856220
    const/4 v0, 0x0

    .line 1856221
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, LX/CBF;->b()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/1aZ;)V
    .locals 0
    .param p2    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1856222
    check-cast p1, LX/CBz;

    .line 1856223
    iget-object p0, p1, LX/CBz;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1856224
    invoke-virtual {p1}, LX/CBz;->d()V

    .line 1856225
    if-eqz p2, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-virtual {p1, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1856226
    return-void

    .line 1856227
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
