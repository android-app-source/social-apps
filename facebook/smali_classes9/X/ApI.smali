.class public LX/ApI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApG;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ApJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715771
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApI;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ApJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715772
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715773
    iput-object p1, p0, LX/ApI;->b:LX/0Ot;

    .line 1715774
    return-void
.end method

.method public static a(LX/0QB;)LX/ApI;
    .locals 4

    .prologue
    .line 1715775
    const-class v1, LX/ApI;

    monitor-enter v1

    .line 1715776
    :try_start_0
    sget-object v0, LX/ApI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715777
    sput-object v2, LX/ApI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715778
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715779
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715780
    new-instance v3, LX/ApI;

    const/16 p0, 0x21fa

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ApI;-><init>(LX/0Ot;)V

    .line 1715781
    move-object v0, v3

    .line 1715782
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715783
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715784
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1715786
    check-cast p2, LX/ApH;

    .line 1715787
    iget-object v0, p0, LX/ApI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ApJ;

    iget v2, p2, LX/ApH;->a:I

    iget-object v3, p2, LX/ApH;->b:Ljava/lang/CharSequence;

    iget v4, p2, LX/ApH;->c:I

    iget-object v5, p2, LX/ApH;->d:Ljava/lang/CharSequence;

    iget-object v6, p2, LX/ApH;->e:Landroid/util/SparseArray;

    iget-object v7, p2, LX/ApH;->f:LX/1dQ;

    iget-object v8, p2, LX/ApH;->g:Landroid/view/View$OnClickListener;

    iget-object v9, p2, LX/ApH;->h:LX/1dQ;

    iget-object v10, p2, LX/ApH;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v11, p2, LX/ApH;->j:LX/1X1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, LX/ApJ;->a(LX/1De;ILjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/util/SparseArray;LX/1dQ;Landroid/view/View$OnClickListener;LX/1dQ;Landroid/widget/CompoundButton$OnCheckedChangeListener;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1715788
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715789
    invoke-static {}, LX/1dS;->b()V

    .line 1715790
    const/4 v0, 0x0

    return-object v0
.end method
