.class public LX/BZ4;
.super LX/BYo;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:I

.field private d:I

.field private e:F

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796761
    invoke-direct {p0}, LX/BYo;-><init>()V

    return-void
.end method


# virtual methods
.method public getEqualScreenHeight()Z
    .locals 1

    .prologue
    .line 1796760
    iget-boolean v0, p0, LX/BZ4;->g:Z

    return v0
.end method

.method public getEqualScreenWidth()Z
    .locals 1

    .prologue
    .line 1796759
    iget-boolean v0, p0, LX/BZ4;->h:Z

    return v0
.end method

.method public getHorizontalAlignment()I
    .locals 1

    .prologue
    .line 1796758
    iget v0, p0, LX/BZ4;->c:I

    return v0
.end method

.method public getHorizontalSpacing()F
    .locals 1

    .prologue
    .line 1796757
    iget v0, p0, LX/BZ4;->e:F

    return v0
.end method

.method public getMaterialIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796756
    iget-object v0, p0, LX/BZ4;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getTextureHeight()F
    .locals 1

    .prologue
    .line 1796755
    iget v0, p0, LX/BZ4;->b:F

    return v0
.end method

.method public getTextureWidth()F
    .locals 1

    .prologue
    .line 1796736
    iget v0, p0, LX/BZ4;->a:F

    return v0
.end method

.method public getVerticalAlignment()I
    .locals 1

    .prologue
    .line 1796754
    iget v0, p0, LX/BZ4;->d:I

    return v0
.end method

.method public getVerticalSpacing()F
    .locals 1

    .prologue
    .line 1796753
    iget v0, p0, LX/BZ4;->f:F

    return v0
.end method

.method public setEqualScreenHeight(Z)V
    .locals 0

    .prologue
    .line 1796734
    iput-boolean p1, p0, LX/BZ4;->g:Z

    .line 1796735
    return-void
.end method

.method public setEqualScreenWidth(Z)V
    .locals 0

    .prologue
    .line 1796737
    iput-boolean p1, p0, LX/BZ4;->h:Z

    .line 1796738
    return-void
.end method

.method public setHorizontalAlignment(I)V
    .locals 0

    .prologue
    .line 1796739
    iput p1, p0, LX/BZ4;->c:I

    .line 1796740
    return-void
.end method

.method public setHorizontalSpacing(F)V
    .locals 0

    .prologue
    .line 1796741
    iput p1, p0, LX/BZ4;->e:F

    .line 1796742
    return-void
.end method

.method public setMaterialIdentifier(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796743
    iput-object p1, p0, LX/BZ4;->i:Ljava/lang/String;

    .line 1796744
    return-void
.end method

.method public setTextureHeight(F)V
    .locals 0

    .prologue
    .line 1796745
    iput p1, p0, LX/BZ4;->b:F

    .line 1796746
    return-void
.end method

.method public setTextureWidth(F)V
    .locals 0

    .prologue
    .line 1796747
    iput p1, p0, LX/BZ4;->a:F

    .line 1796748
    return-void
.end method

.method public setVerticalAlignment(I)V
    .locals 0

    .prologue
    .line 1796749
    iput p1, p0, LX/BZ4;->d:I

    .line 1796750
    return-void
.end method

.method public setVerticalSpacing(F)V
    .locals 0

    .prologue
    .line 1796751
    iput p1, p0, LX/BZ4;->f:F

    .line 1796752
    return-void
.end method
