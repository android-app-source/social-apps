.class public LX/BQc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8LV;

.field private final b:LX/1EZ;

.field private final c:LX/74n;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7ma;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8LV;LX/1EZ;LX/0Ot;LX/74n;LX/0SG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/8LV;",
            "LX/1EZ;",
            "LX/0Ot",
            "<",
            "LX/7ma;",
            ">;",
            "LX/74n;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782446
    iput-object p1, p0, LX/BQc;->e:Landroid/content/Context;

    .line 1782447
    iput-object p2, p0, LX/BQc;->a:LX/8LV;

    .line 1782448
    iput-object p3, p0, LX/BQc;->b:LX/1EZ;

    .line 1782449
    iput-object p5, p0, LX/BQc;->c:LX/74n;

    .line 1782450
    iput-object p4, p0, LX/BQc;->d:LX/0Ot;

    .line 1782451
    iput-object p6, p0, LX/BQc;->f:LX/0SG;

    .line 1782452
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 17
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782453
    invoke-static/range {p2 .. p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1782454
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BQc;->e:Landroid/content/Context;

    const v3, 0x7f08273d

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1782455
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, LX/BQc;->c:LX/74n;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/ATz;->a(Landroid/net/Uri;LX/74n;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/23u;->f(LX/0Px;)LX/23u;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/23u;->b(Ljava/lang/String;)LX/23u;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/BQc;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/23u;->a(J)LX/23u;

    move-result-object v2

    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1782456
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BQc;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7ma;

    new-instance v4, LX/7ml;

    new-instance v5, LX/5M1;

    new-instance v6, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    new-instance v7, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    new-instance v8, LX/5M9;

    invoke-direct {v8}, LX/5M9;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, LX/5M9;->r(Ljava/lang/String;)LX/5M9;

    move-result-object v8

    invoke-virtual {v8}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    invoke-static {}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->newBuilder()Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v8

    invoke-direct {v6, v3, v7, v8}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    invoke-direct {v5, v6}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    invoke-virtual {v5}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v3

    sget-object v5, LX/7mm;->PROFILE_PIC:LX/7mm;

    invoke-direct {v4, v3, v5}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    invoke-virtual {v2, v4}, LX/7mV;->a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1782457
    move-object/from16 v0, p0

    iget-object v15, v0, LX/BQc;->b:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BQc;->a:LX/8LV;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const/4 v13, 0x0

    move-object/from16 v3, p11

    move-object/from16 v5, p2

    move-object/from16 v6, p8

    move/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v10, p9

    move-object/from16 v11, p3

    move-object/from16 v12, p10

    move-object/from16 v14, p7

    invoke-virtual/range {v2 .. v14}, LX/8LV;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    invoke-virtual {v15, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1782458
    return-void

    .line 1782459
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/BQc;
    .locals 7

    .prologue
    .line 1782460
    new-instance v0, LX/BQc;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v2

    check-cast v2, LX/8LV;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v3

    check-cast v3, LX/1EZ;

    const/16 v4, 0x19ee

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v5

    check-cast v5, LX/74n;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct/range {v0 .. v6}, LX/BQc;-><init>(Landroid/content/Context;LX/8LV;LX/1EZ;LX/0Ot;LX/74n;LX/0SG;)V

    .line 1782461
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 10
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 1782462
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    move-object/from16 v8, p6

    invoke-virtual/range {v0 .. v8}, LX/BQc;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1782463
    return-void
.end method

.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 14
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782464
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1782465
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1782466
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b()I

    move-result v5

    move-object v1, p0

    move-wide/from16 v6, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    invoke-direct/range {v1 .. v12}, LX/BQc;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1782467
    return-void

    .line 1782468
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a()Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method
