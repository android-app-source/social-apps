.class public final LX/BfV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

.field public final synthetic c:LX/BfX;


# direct methods
.method public constructor <init>(LX/BfX;ILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V
    .locals 0

    .prologue
    .line 1806087
    iput-object p1, p0, LX/BfV;->c:LX/BfX;

    iput p2, p0, LX/BfV;->a:I

    iput-object p3, p0, LX/BfV;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x720045df

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806088
    iget-object v1, p0, LX/BfV;->c:LX/BfX;

    iget-object v1, v1, LX/BfX;->c:LX/BfK;

    iget-object v2, p0, LX/BfV;->c:LX/BfX;

    iget-object v2, v2, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v3, p0, LX/BfV;->a:I

    invoke-virtual {v1, v2, v3}, LX/BfK;->a(Lcom/facebook/crowdsourcing/helper/HoursData;I)V

    .line 1806089
    iget-object v1, p0, LX/BfV;->c:LX/BfX;

    iget-object v2, p0, LX/BfV;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iget-object v4, p0, LX/BfV;->c:LX/BfX;

    iget-object v4, v4, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v5, p0, LX/BfV;->a:I

    invoke-virtual {v4, v5}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    .line 1806090
    invoke-static {v4}, LX/BfX;->b(LX/Bex;)Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    move-result-object v5

    move-object v3, v5

    .line 1806091
    invoke-static {v1, v2, v3}, LX/BfX;->b(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806092
    iget-object v1, p0, LX/BfV;->c:LX/BfX;

    iget-object v1, v1, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806093
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1806094
    const-string v3, "extra_hours_selected_option"

    iget-object v4, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806095
    iget v5, v4, LX/BfX;->f:I

    move v4, v5

    .line 1806096
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806097
    iget-object v3, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806098
    iget v4, v3, LX/BfX;->f:I

    move v3, v4

    .line 1806099
    if-nez v3, :cond_0

    .line 1806100
    const-string v3, "extra_hours_data"

    iget-object v4, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806101
    iget-object v5, v4, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object v4, v5

    .line 1806102
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806103
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806104
    const v1, 0x17a4d28f

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
