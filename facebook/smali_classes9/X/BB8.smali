.class public final LX/BB8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:I

.field public f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1756336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 1756337
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1756338
    iget-object v1, p0, LX/BB8;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1756339
    iget-object v3, p0, LX/BB8;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1756340
    iget-object v5, p0, LX/BB8;->c:LX/0Px;

    invoke-virtual {v0, v5}, LX/186;->c(Ljava/util/List;)I

    move-result v5

    .line 1756341
    iget-object v6, p0, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1756342
    iget-object v7, p0, LX/BB8;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1756343
    iget-object v8, p0, LX/BB8;->h:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1756344
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1756345
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1756346
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1756347
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1756348
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/BB8;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1756349
    const/4 v1, 0x4

    iget v3, p0, LX/BB8;->e:I

    invoke-virtual {v0, v1, v3, v10}, LX/186;->a(III)V

    .line 1756350
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1756351
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1756352
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1756353
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1756354
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1756355
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1756356
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756357
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756358
    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;-><init>(LX/15i;)V

    .line 1756359
    iget-object v0, p0, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1756360
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1756361
    :cond_0
    return-object v1
.end method
