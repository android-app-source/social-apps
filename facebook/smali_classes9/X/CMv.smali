.class public final enum LX/CMv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CMv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CMv;

.field public static final enum EXCELLENT:LX/CMv;

.field public static final enum FAILING:LX/CMv;

.field public static final enum GOOD:LX/CMv;

.field public static final enum MODERATE:LX/CMv;

.field public static final enum POOR:LX/CMv;

.field public static final enum UNKNOWN:LX/CMv;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1881069
    new-instance v0, LX/CMv;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->UNKNOWN:LX/CMv;

    new-instance v0, LX/CMv;

    const-string v1, "FAILING"

    invoke-direct {v0, v1, v4}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->FAILING:LX/CMv;

    new-instance v0, LX/CMv;

    const-string v1, "POOR"

    invoke-direct {v0, v1, v5}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->POOR:LX/CMv;

    new-instance v0, LX/CMv;

    const-string v1, "MODERATE"

    invoke-direct {v0, v1, v6}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->MODERATE:LX/CMv;

    new-instance v0, LX/CMv;

    const-string v1, "GOOD"

    invoke-direct {v0, v1, v7}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->GOOD:LX/CMv;

    new-instance v0, LX/CMv;

    const-string v1, "EXCELLENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CMv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMv;->EXCELLENT:LX/CMv;

    .line 1881070
    const/4 v0, 0x6

    new-array v0, v0, [LX/CMv;

    sget-object v1, LX/CMv;->UNKNOWN:LX/CMv;

    aput-object v1, v0, v3

    sget-object v1, LX/CMv;->FAILING:LX/CMv;

    aput-object v1, v0, v4

    sget-object v1, LX/CMv;->POOR:LX/CMv;

    aput-object v1, v0, v5

    sget-object v1, LX/CMv;->MODERATE:LX/CMv;

    aput-object v1, v0, v6

    sget-object v1, LX/CMv;->GOOD:LX/CMv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CMv;->EXCELLENT:LX/CMv;

    aput-object v2, v0, v1

    sput-object v0, LX/CMv;->$VALUES:[LX/CMv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1881071
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CMv;
    .locals 1

    .prologue
    .line 1881072
    const-class v0, LX/CMv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CMv;

    return-object v0
.end method

.method public static values()[LX/CMv;
    .locals 1

    .prologue
    .line 1881073
    sget-object v0, LX/CMv;->$VALUES:[LX/CMv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CMv;

    return-object v0
.end method
