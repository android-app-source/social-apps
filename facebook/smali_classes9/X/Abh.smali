.class public LX/Abh;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:J


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0Uh;

.field public final d:LX/AVT;

.field public final e:LX/0gt;

.field public f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1691186
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/Abh;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uh;LX/AVT;LX/0gt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1691187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1691188
    iput-object p1, p0, LX/Abh;->b:Landroid/content/Context;

    .line 1691189
    iput-object p2, p0, LX/Abh;->c:LX/0Uh;

    .line 1691190
    iput-object p3, p0, LX/Abh;->d:LX/AVT;

    .line 1691191
    iput-object p4, p0, LX/Abh;->e:LX/0gt;

    .line 1691192
    return-void
.end method

.method public static a(LX/0QB;)LX/Abh;
    .locals 5

    .prologue
    .line 1691193
    new-instance v4, LX/Abh;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v2

    check-cast v2, LX/AVT;

    invoke-static {p0}, LX/0gt;->b(LX/0QB;)LX/0gt;

    move-result-object v3

    check-cast v3, LX/0gt;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Abh;-><init>(Landroid/content/Context;LX/0Uh;LX/AVT;LX/0gt;)V

    .line 1691194
    move-object v0, v4

    .line 1691195
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1691196
    iget-object v0, p0, LX/Abh;->c:LX/0Uh;

    const/16 v1, 0x354

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1691197
    sget-wide v0, LX/Abh;->a:J

    cmp-long v0, p2, v0

    if-ltz v0, :cond_1

    const-string v0, "313309579005599"

    :goto_0
    iput-object v0, p0, LX/Abh;->f:Ljava/lang/String;

    .line 1691198
    iget-object v0, p0, LX/Abh;->e:LX/0gt;

    iget-object v1, p0, LX/Abh;->f:Ljava/lang/String;

    .line 1691199
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1691200
    move-object v0, v0

    .line 1691201
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    invoke-virtual {v0}, LX/0gt;->b()V

    .line 1691202
    :cond_0
    return-void

    .line 1691203
    :cond_1
    const-string v0, "1114878941893486"

    goto :goto_0
.end method
