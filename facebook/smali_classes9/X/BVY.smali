.class public LX/BVY;
.super LX/BVT;
.source ""


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791229
    invoke-direct {p0, p1, p2}, LX/BVT;-><init>(LX/BW4;LX/BW5;)V

    .line 1791230
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1791231
    new-instance v0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;

    invoke-direct {v0, p1}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1791232
    check-cast p1, Lcom/facebook/widget/ratingbar/FractionalRatingBar;

    .line 1791233
    sget-object v0, LX/BVW;->a:[I

    invoke-static {p2}, LX/BVX;->from(Ljava/lang/String;)LX/BVX;

    move-result-object v1

    invoke-virtual {v1}, LX/BVX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1791234
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unhandled rating bar attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791235
    :pswitch_0
    invoke-static {p3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->setRating(F)V

    .line 1791236
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
