.class public final enum LX/BZL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZL;

.field public static final enum facemask:LX/BZL;

.field public static final enum facemaskdisplace:LX/BZL;

.field public static final enum facemasklight:LX/BZL;

.field public static final enum facemasksmooth:LX/BZL;

.field public static final enum mesh:LX/BZL;

.field public static final enum square:LX/BZL;

.field public static final enum texture_square:LX/BZL;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1797197
    new-instance v0, LX/BZL;

    const-string v1, "facemask"

    invoke-direct {v0, v1, v3}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->facemask:LX/BZL;

    .line 1797198
    new-instance v0, LX/BZL;

    const-string v1, "facemasklight"

    invoke-direct {v0, v1, v4}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->facemasklight:LX/BZL;

    .line 1797199
    new-instance v0, LX/BZL;

    const-string v1, "facemaskdisplace"

    invoke-direct {v0, v1, v5}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->facemaskdisplace:LX/BZL;

    .line 1797200
    new-instance v0, LX/BZL;

    const-string v1, "facemasksmooth"

    invoke-direct {v0, v1, v6}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->facemasksmooth:LX/BZL;

    .line 1797201
    new-instance v0, LX/BZL;

    const-string v1, "square"

    invoke-direct {v0, v1, v7}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->square:LX/BZL;

    .line 1797202
    new-instance v0, LX/BZL;

    const-string v1, "texture_square"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->texture_square:LX/BZL;

    .line 1797203
    new-instance v0, LX/BZL;

    const-string v1, "mesh"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BZL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZL;->mesh:LX/BZL;

    .line 1797204
    const/4 v0, 0x7

    new-array v0, v0, [LX/BZL;

    sget-object v1, LX/BZL;->facemask:LX/BZL;

    aput-object v1, v0, v3

    sget-object v1, LX/BZL;->facemasklight:LX/BZL;

    aput-object v1, v0, v4

    sget-object v1, LX/BZL;->facemaskdisplace:LX/BZL;

    aput-object v1, v0, v5

    sget-object v1, LX/BZL;->facemasksmooth:LX/BZL;

    aput-object v1, v0, v6

    sget-object v1, LX/BZL;->square:LX/BZL;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BZL;->texture_square:LX/BZL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BZL;->mesh:LX/BZL;

    aput-object v2, v0, v1

    sput-object v0, LX/BZL;->$VALUES:[LX/BZL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1797194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZL;
    .locals 1

    .prologue
    .line 1797195
    const-class v0, LX/BZL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZL;

    return-object v0
.end method

.method public static values()[LX/BZL;
    .locals 1

    .prologue
    .line 1797196
    sget-object v0, LX/BZL;->$VALUES:[LX/BZL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZL;

    return-object v0
.end method
