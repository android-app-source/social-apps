.class public final LX/B02;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;)V
    .locals 0

    .prologue
    .line 1733206
    iput-object p1, p0, LX/B02;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x70c1d1e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733207
    iget-object v1, p0, LX/B02;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    iget-object v2, p0, LX/B02;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    iget-object v2, v2, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    .line 1733208
    iget-object p1, v2, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    move-object v2, p1

    .line 1733209
    invoke-virtual {v1, v2}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a(LX/0Px;)V

    .line 1733210
    iget-object v1, p0, LX/B02;->a:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    iget-object v1, v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    .line 1733211
    iget-object v2, v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1733212
    iget-object v2, v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->d:LX/B01;

    if-eqz v2, :cond_0

    .line 1733213
    iget-object v2, v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->d:LX/B01;

    iget-object p0, v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    invoke-virtual {v2, p0}, LX/B01;->a(I)V

    .line 1733214
    :cond_0
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1733215
    const v1, -0xaf1da30

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
