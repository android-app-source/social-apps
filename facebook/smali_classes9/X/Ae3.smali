.class public final LX/Ae3;
.super LX/3Gy;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Gy",
        "<",
        "LX/Add;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ae5;


# direct methods
.method public constructor <init>(LX/Ae5;)V
    .locals 0

    .prologue
    .line 1695937
    iput-object p1, p0, LX/Ae3;->a:LX/Ae5;

    invoke-direct {p0}, LX/3Gy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Add;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1695938
    const-class v0, LX/Add;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1695939
    check-cast p1, LX/Add;

    .line 1695940
    iget-object v0, p0, LX/Ae3;->a:LX/Ae5;

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p1, LX/Add;->a:F

    sub-float/2addr v1, v2

    .line 1695941
    iput v1, v0, LX/Ae5;->e:F

    .line 1695942
    iget-object v0, p0, LX/Ae3;->a:LX/Ae5;

    iget-object v0, v0, LX/Ae5;->d:LX/Ac3;

    iget-object v1, p0, LX/Ae3;->a:LX/Ae5;

    iget v1, v1, LX/Ae5;->e:F

    invoke-virtual {v0, v1}, LX/Ac3;->a(F)V

    .line 1695943
    iget-object v0, p0, LX/Ae3;->a:LX/Ae5;

    iget-object v1, p0, LX/Ae3;->a:LX/Ae5;

    iget-object v1, v1, LX/Ae5;->d:LX/Ac3;

    invoke-virtual {v1}, LX/Ac3;->getColor()I

    move-result v1

    .line 1695944
    iput v1, v0, LX/Ae5;->g:I

    .line 1695945
    iget-object v0, p0, LX/Ae3;->a:LX/Ae5;

    .line 1695946
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1695947
    move-object v0, v1

    .line 1695948
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    iget v1, p1, LX/Add;->a:F

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setAlpha(F)V

    .line 1695949
    iget-object v0, p0, LX/Ae3;->a:LX/Ae5;

    .line 1695950
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1695951
    move-object v0, v1

    .line 1695952
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    iget v1, p1, LX/Add;->a:F

    float-to-double v2, v1

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setVisibility(I)V

    .line 1695953
    return-void

    .line 1695954
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
