.class public LX/Cer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925175
    const-class v0, LX/Cer;

    sput-object v0, LX/Cer;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1925176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x2bf13c3e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925177
    if-nez p2, :cond_0

    .line 1925178
    const/16 v1, 0x27

    const v2, 0x46b6395c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925179
    :goto_0
    return-void

    .line 1925180
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1925181
    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_1
    move-object v1, v1

    .line 1925182
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 1925183
    invoke-static {v1}, LX/2RD;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1925184
    const v1, 0x452aedd3

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1925185
    :cond_1
    const-string v2, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1925186
    const-string v2, "receiver"

    invoke-static {p1, v1, v2}, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925187
    :cond_2
    const v1, -0x45c8b47a

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
