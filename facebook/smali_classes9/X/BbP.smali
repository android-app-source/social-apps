.class public final LX/BbP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800764
    iput-object p1, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iput-object p2, p0, LX/BbP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x21c39818

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1800765
    iget-object v0, p0, LX/BbP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1800766
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1800767
    :cond_0
    iget-object v0, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->s:LX/03V;

    sget-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b:Ljava/lang/String;

    const-string v3, "Null story on SocialSearchAttachment"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800768
    const v0, -0x2a14d5a8

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800769
    :goto_0
    return-void

    .line 1800770
    :cond_1
    iget-object v0, p0, LX/BbP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800771
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1800772
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1800773
    if-nez v0, :cond_2

    .line 1800774
    iget-object v0, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->s:LX/03V;

    sget-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b:Ljava/lang/String;

    const-string v3, "Null PlaceList on SocialSearchAttachment"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800775
    const v0, -0x178bdc21

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1800776
    :cond_2
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v4, v4, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->d:Landroid/content/Context;

    const-class v5, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1800777
    const-string v4, "social_search_story_id_extra"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1800778
    const-string v4, "social_search_can_viewer_edit_extra"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1800779
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->qc()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 1800780
    const-string v4, "social_search_place_location_extra"

    invoke-static {v3, v4, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1800781
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    .line 1800782
    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1800783
    const-string v2, "social_search_place_list_extra"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {v3, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 1800784
    :cond_3
    iget-object v0, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v2, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    const v4, 0xc364

    iget-object v0, p0, LX/BbP;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1800785
    const v0, 0x7e00781f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
