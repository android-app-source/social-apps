.class public final LX/COF;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/COH;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/COG;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1882803
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "template"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "templateContext"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "wrappers"

    aput-object v2, v0, v1

    sput-object v0, LX/COF;->b:[Ljava/lang/String;

    .line 1882804
    sput v3, LX/COF;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1882801
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1882802
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/COF;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/COF;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/COF;LX/1De;IILX/COG;)V
    .locals 1

    .prologue
    .line 1882797
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1882798
    iput-object p4, p0, LX/COF;->a:LX/COG;

    .line 1882799
    iget-object v0, p0, LX/COF;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1882800
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1882793
    invoke-super {p0}, LX/1X5;->a()V

    .line 1882794
    const/4 v0, 0x0

    iput-object v0, p0, LX/COF;->a:LX/COG;

    .line 1882795
    sget-object v0, LX/COH;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1882796
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/COH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1882783
    iget-object v1, p0, LX/COF;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/COF;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/COF;->c:I

    if-ge v1, v2, :cond_2

    .line 1882784
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1882785
    :goto_0
    sget v2, LX/COF;->c:I

    if-ge v0, v2, :cond_1

    .line 1882786
    iget-object v2, p0, LX/COF;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1882787
    sget-object v2, LX/COF;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1882788
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1882789
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1882790
    :cond_2
    iget-object v0, p0, LX/COF;->a:LX/COG;

    .line 1882791
    invoke-virtual {p0}, LX/COF;->a()V

    .line 1882792
    return-object v0
.end method
