.class public LX/C3y;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/24a;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLImage;

.field public b:Lcom/facebook/graphql/model/GraphQLImage;

.field public c:Lcom/facebook/graphql/model/GraphQLImage;

.field public final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private final g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final i:Landroid/widget/ImageView;

.field private j:Z

.field private final k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final l:Landroid/view/View;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1846645
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1846646
    const v0, 0x7f03059d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1846647
    const v0, 0x7f0d0f66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    .line 1846648
    const v0, 0x7f0d0626

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C3y;->e:Landroid/widget/TextView;

    .line 1846649
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/C3y;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1846650
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/C3y;->i:Landroid/widget/ImageView;

    .line 1846651
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1846652
    const v0, 0x7f0d0bdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/C3y;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1846653
    const v0, 0x7f0d0ac0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/C3y;->l:Landroid/view/View;

    .line 1846654
    const v0, 0x7f0d08f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/C3y;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1846655
    return-void
.end method

.method public static a(LX/C3y;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;I)V
    .locals 2

    .prologue
    .line 1846636
    if-eqz p1, :cond_0

    .line 1846637
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1846638
    invoke-virtual {p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1846639
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    add-int/2addr v1, p4

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846640
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1846641
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1846642
    invoke-virtual {p0}, LX/C3y;->requestLayout()V

    .line 1846643
    :goto_0
    return-void

    .line 1846644
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 1846635
    iget-object v0, p0, LX/C3y;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAccentImageMargin(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1846632
    iget-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1846633
    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1846634
    return-void
.end method

.method private setContentSummaryMargin(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1846627
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1846628
    iget-boolean v1, p0, LX/C3y;->m:Z

    if-eqz v1, :cond_0

    .line 1846629
    neg-int v1, p1

    div-int/lit8 v1, v1, 0x3

    .line 1846630
    :goto_0
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1846631
    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 1846625
    iget-object v0, p0, LX/C3y;->i:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1846626
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 3

    .prologue
    .line 1846656
    iput-object p1, p0, LX/C3y;->b:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1846657
    iget-object v0, p0, LX/C3y;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, LX/C3y;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {p0, p1, v0, p2, v1}, LX/C3y;->a(LX/C3y;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;I)V

    .line 1846658
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/common/callercontext/CallerContext;)V
    .locals 5

    .prologue
    .line 1846617
    iput-object p1, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1846618
    iput-boolean p2, p0, LX/C3y;->j:Z

    .line 1846619
    iget-object v1, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1846620
    if-nez p1, :cond_0

    invoke-direct {p0}, LX/C3y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846621
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, LX/C3y;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1cbb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1846622
    :cond_0
    invoke-virtual {p0}, LX/C3y;->requestLayout()V

    .line 1846623
    return-void

    .line 1846624
    :cond_1
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 1846604
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1846605
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1846606
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1846607
    iget-object v0, p0, LX/C3y;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1846608
    :goto_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1846609
    :cond_0
    iget-object v0, p0, LX/C3y;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1846610
    :goto_2
    return-void

    .line 1846611
    :cond_1
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1846612
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1846613
    :cond_2
    iget-object v0, p0, LX/C3y;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1846614
    iget-object v0, p0, LX/C3y;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1846615
    :cond_3
    iget-object v0, p0, LX/C3y;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/C3y;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, p3, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    .line 1846616
    iget-object v0, p0, LX/C3y;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1846603
    invoke-direct {p0}, LX/C3y;->a()Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1846577
    iget-object v0, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_2

    .line 1846578
    iget-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1846579
    iget-object v0, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    .line 1846580
    iget-boolean v3, p0, LX/C3y;->j:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    if-lez v3, :cond_1

    .line 1846581
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1846582
    int-to-double v4, v0

    iget-object v0, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    int-to-double v6, v0

    div-double/2addr v4, v6

    .line 1846583
    const/4 v0, -0x1

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1846584
    iget-object v0, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    int-to-double v6, v0

    mul-double/2addr v6, v4

    double-to-int v0, v6

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846585
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846586
    invoke-virtual {p0}, LX/C3y;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, LX/C3y;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v1, v2, v1, v3}, LX/C3y;->setPadding(IIII)V

    .line 1846587
    iget-object v2, p0, LX/C3y;->c:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v2, :cond_0

    .line 1846588
    iget-object v2, p0, LX/C3y;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1846589
    iget-object v3, p0, LX/C3y;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    int-to-double v6, v3

    mul-double/2addr v6, v4

    double-to-int v3, v6

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1846590
    iget-object v3, p0, LX/C3y;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    int-to-double v6, v3

    mul-double/2addr v4, v6

    double-to-int v3, v4

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846591
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, LX/C3y;->setContentSummaryMargin(I)V

    .line 1846592
    invoke-direct {p0, v1}, LX/C3y;->setAccentImageMargin(I)V

    .line 1846593
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 1846594
    return-void

    .line 1846595
    :cond_1
    iget-object v1, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1846596
    iget-object v1, p0, LX/C3y;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846597
    invoke-virtual {p0}, LX/C3y;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1cbc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 1846598
    :cond_2
    iget-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1846599
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1846600
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1846601
    invoke-direct {p0, v1}, LX/C3y;->setContentSummaryMargin(I)V

    .line 1846602
    invoke-direct {p0, v1}, LX/C3y;->setAccentImageMargin(I)V

    goto :goto_1
.end method

.method public setContentSummaryColor(I)V
    .locals 1

    .prologue
    .line 1846575
    iget-object v0, p0, LX/C3y;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1846576
    return-void
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 1846572
    iget-object v1, p0, LX/C3y;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1846573
    return-void

    .line 1846574
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOverlapMode(Z)V
    .locals 1

    .prologue
    .line 1846568
    iget-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 1846569
    iput-boolean p1, p0, LX/C3y;->m:Z

    .line 1846570
    iget-object v0, p0, LX/C3y;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v0

    invoke-direct {p0, v0}, LX/C3y;->setContentSummaryMargin(I)V

    .line 1846571
    :cond_0
    return-void
.end method
