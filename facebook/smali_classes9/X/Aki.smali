.class public final LX/Aki;
.super LX/AkW;
.source ""


# instance fields
.field public final synthetic a:LX/1SX;

.field private final b:Landroid/app/Activity;

.field private final c:LX/3Af;


# direct methods
.method public constructor <init>(LX/1SX;Landroid/app/Activity;LX/3Af;)V
    .locals 0

    .prologue
    .line 1709269
    iput-object p1, p0, LX/Aki;->a:LX/1SX;

    invoke-direct {p0}, LX/AkW;-><init>()V

    .line 1709270
    iput-object p2, p0, LX/Aki;->b:Landroid/app/Activity;

    .line 1709271
    iput-object p3, p0, LX/Aki;->c:LX/3Af;

    .line 1709272
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1709273
    iget-object v0, p0, LX/Aki;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1709274
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1709275
    iget-object v0, p0, LX/Aki;->b:Landroid/app/Activity;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1709276
    iget-object v0, p0, LX/Aki;->c:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->dismiss()V

    .line 1709277
    invoke-virtual {p0}, LX/Aki;->a()V

    .line 1709278
    :cond_0
    return-void
.end method
