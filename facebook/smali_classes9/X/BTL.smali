.class public LX/BTL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Runnable;

.field public b:Landroid/os/Handler;

.field public c:Landroid/view/View;

.field public final d:LX/BT5;

.field public final e:LX/BTX;

.field public final f:LX/BSr;

.field public final g:LX/BTJ;


# direct methods
.method public constructor <init>(LX/BT5;LX/BTX;LX/BSr;LX/BTJ;)V
    .locals 1

    .prologue
    .line 1787247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787248
    iput-object p1, p0, LX/BTL;->d:LX/BT5;

    .line 1787249
    iput-object p2, p0, LX/BTL;->e:LX/BTX;

    .line 1787250
    iput-object p3, p0, LX/BTL;->f:LX/BSr;

    .line 1787251
    iput-object p4, p0, LX/BTL;->g:LX/BTJ;

    .line 1787252
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/BTL;->b:Landroid/os/Handler;

    .line 1787253
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1787226
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1787227
    invoke-virtual {p0}, LX/BTL;->c()V

    .line 1787228
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1787229
    iget-object v0, p0, LX/BTL;->d:LX/BT5;

    if-eqz v0, :cond_0

    .line 1787230
    iget-object v0, p0, LX/BTL;->d:LX/BT5;

    iget-object v1, p0, LX/BTL;->e:LX/BTX;

    invoke-virtual {v1, p1, v2}, LX/BTX;->a(IZ)I

    move-result v1

    .line 1787231
    iget-object v2, v0, LX/BT5;->a:LX/BT9;

    iget-object v2, v2, LX/BT9;->b:LX/BSr;

    .line 1787232
    iget-object v0, v2, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e()V

    .line 1787233
    iget-object v0, v2, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(I)V

    .line 1787234
    invoke-static {v2, v1}, LX/BSr;->c(LX/BSr;I)V

    .line 1787235
    :cond_0
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/BTL;->e()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1787236
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1787245
    iget-object v0, p0, LX/BTL;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/BTL;->a:Ljava/lang/Runnable;

    const v2, -0x15502d1a

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1787246
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1787241
    iget-object v0, p0, LX/BTL;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/BTL;->a:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1787242
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1787243
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1787244
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1787238
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1787239
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1787240
    :cond_0
    return-void
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1787237
    iget-object v0, p0, LX/BTL;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, LX/BTL;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method
