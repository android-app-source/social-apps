.class public LX/BFI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0bH;

.field public final d:LX/189;

.field public final e:LX/3H7;

.field public final f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final g:LX/1rn;

.field private final h:LX/0lC;

.field public final i:LX/1Ck;

.field public final j:LX/967;

.field private final k:LX/0ad;

.field public final l:LX/19w;

.field public final m:LX/0TD;

.field private final n:LX/0tO;

.field public final o:LX/0xX;

.field public final p:LX/3Q4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765511
    const-class v0, LX/BFI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BFI;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0bH;LX/189;LX/3H7;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rn;LX/0lC;LX/1Ck;LX/967;LX/0ad;LX/19w;LX/0TD;LX/0tO;LX/0xX;LX/3Q4;)V
    .locals 0
    .param p12    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765513
    iput-object p1, p0, LX/BFI;->b:LX/03V;

    .line 1765514
    iput-object p2, p0, LX/BFI;->c:LX/0bH;

    .line 1765515
    iput-object p3, p0, LX/BFI;->d:LX/189;

    .line 1765516
    iput-object p4, p0, LX/BFI;->e:LX/3H7;

    .line 1765517
    iput-object p5, p0, LX/BFI;->f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 1765518
    iput-object p6, p0, LX/BFI;->g:LX/1rn;

    .line 1765519
    iput-object p7, p0, LX/BFI;->h:LX/0lC;

    .line 1765520
    iput-object p8, p0, LX/BFI;->i:LX/1Ck;

    .line 1765521
    iput-object p9, p0, LX/BFI;->j:LX/967;

    .line 1765522
    iput-object p10, p0, LX/BFI;->k:LX/0ad;

    .line 1765523
    iput-object p11, p0, LX/BFI;->l:LX/19w;

    .line 1765524
    iput-object p12, p0, LX/BFI;->m:LX/0TD;

    .line 1765525
    iput-object p13, p0, LX/BFI;->n:LX/0tO;

    .line 1765526
    iput-object p14, p0, LX/BFI;->o:LX/0xX;

    .line 1765527
    iput-object p15, p0, LX/BFI;->p:LX/3Q4;

    .line 1765528
    return-void
.end method

.method public static a(LX/0QB;)LX/BFI;
    .locals 1

    .prologue
    .line 1765529
    invoke-static {p0}, LX/BFI;->b(LX/0QB;)LX/BFI;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BFI;
    .locals 16

    .prologue
    .line 1765530
    new-instance v0, LX/BFI;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v2

    check-cast v2, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/189;->a(LX/0QB;)LX/189;

    move-result-object v3

    check-cast v3, LX/189;

    invoke-static/range {p0 .. p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v4

    check-cast v4, LX/3H7;

    invoke-static/range {p0 .. p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {p0 .. p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v6

    check-cast v6, LX/1rn;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/967;->a(LX/0QB;)LX/967;

    move-result-object v9

    check-cast v9, LX/967;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v11

    check-cast v11, LX/19w;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v13

    check-cast v13, LX/0tO;

    invoke-static/range {p0 .. p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v14

    check-cast v14, LX/0xX;

    invoke-static/range {p0 .. p0}, LX/3Q4;->a(LX/0QB;)LX/3Q4;

    move-result-object v15

    check-cast v15, LX/3Q4;

    invoke-direct/range {v0 .. v15}, LX/BFI;-><init>(LX/03V;LX/0bH;LX/189;LX/3H7;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rn;LX/0lC;LX/1Ck;LX/967;LX/0ad;LX/19w;LX/0TD;LX/0tO;LX/0xX;LX/3Q4;)V

    .line 1765531
    return-object v0
.end method

.method private static b(LX/BFI;Lcom/facebook/permalink/PermalinkParams;LX/0TF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/permalink/PermalinkParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765532
    iget-object v0, p1, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1765533
    :try_start_0
    iget-object v1, p0, LX/BFI;->h:LX/0lC;

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765534
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 1765535
    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1765536
    :goto_0
    return-void

    .line 1765537
    :catch_0
    move-exception v0

    .line 1765538
    invoke-interface {p2, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/permalink/PermalinkParams;LX/0TF;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/permalink/PermalinkParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765539
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->a()LX/89m;

    move-result-object v1

    invoke-virtual {v1}, LX/89m;->isJsonType()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1765540
    invoke-static/range {p0 .. p2}, LX/BFI;->b(LX/BFI;Lcom/facebook/permalink/PermalinkParams;LX/0TF;)V

    .line 1765541
    :goto_0
    return-void

    .line 1765542
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->a()LX/89m;

    move-result-object v1

    .line 1765543
    sget-object v2, LX/BFH;->a:[I

    invoke-virtual {v1}, LX/89m;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1765544
    sget-object v5, LX/5Go;->GRAPHQL_FEEDBACK_DETAILS:LX/5Go;

    .line 1765545
    :goto_1
    iget-object v12, p0, LX/BFI;->i:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_story_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    iget-object v1, p0, LX/BFI;->e:LX/3H7;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->g()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->h()LX/21y;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/permalink/PermalinkParams;->i()Z

    move-result v9

    iget-object v10, p0, LX/BFI;->k:LX/0ad;

    sget-short v11, LX/227;->d:S

    const/4 v14, 0x0

    invoke-interface {v10, v11, v14}, LX/0ad;->a(SZ)Z

    move-result v10

    iget-object v11, p0, LX/BFI;->n:LX/0tO;

    invoke-virtual {v11}, LX/0tO;->b()Z

    move-result v11

    invoke-virtual/range {v1 .. v11}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/BFD;

    move-object/from16 v0, p2

    invoke-direct {v2, p0, v0}, LX/BFD;-><init>(LX/BFI;LX/0TF;)V

    invoke-virtual {v12, v13, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 1765546
    :pswitch_0
    sget-object v5, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    goto :goto_1

    .line 1765547
    :pswitch_1
    sget-object v5, LX/5Go;->PLATFORM_FEEDBACK_DETAILS:LX/5Go;

    goto :goto_1

    .line 1765548
    :pswitch_2
    sget-object v5, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/permalink/PermalinkParams;LX/0Ve;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/permalink/PermalinkParams;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765549
    iget-object v0, p1, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    move-object v0, v0

    .line 1765550
    invoke-virtual {v0}, LX/89m;->isJsonType()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1765551
    :cond_0
    :goto_0
    return-void

    .line 1765552
    :cond_1
    invoke-virtual {v0}, LX/89m;->isNotificationType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1765553
    iget-object v0, p1, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    move-object v0, v0

    .line 1765554
    if-nez v0, :cond_3

    sget-object v0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->a:LX/89g;

    .line 1765555
    :goto_1
    sget-object v1, LX/BFH;->b:[I

    invoke-virtual {v0}, LX/89g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1765556
    :goto_2
    goto :goto_0

    .line 1765557
    :cond_2
    iget-object v0, p0, LX/BFI;->e:LX/3H7;

    .line 1765558
    iget-object v1, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1765559
    sget-object v2, LX/5Go;->GRAPHQL_FEEDBACK_DETAILS:LX/5Go;

    .line 1765560
    iget-object v3, p1, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1765561
    sget-object v4, LX/21y;->DEFAULT_ORDER:LX/21y;

    .line 1765562
    iget-boolean v5, p1, Lcom/facebook/permalink/PermalinkParams;->j:Z

    move v5, v5

    .line 1765563
    invoke-virtual/range {v0 .. v5}, LX/3H7;->a(Ljava/lang/String;LX/5Go;Ljava/lang/String;LX/21y;Z)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1765564
    if-eqz v0, :cond_0

    .line 1765565
    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 1765566
    :cond_3
    iget-object v0, p1, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    move-object v0, v0

    .line 1765567
    goto :goto_1

    .line 1765568
    :pswitch_0
    iget-object v0, p0, LX/BFI;->o:LX/0xX;

    sget-object v1, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1765569
    iget-object v0, p0, LX/BFI;->p:LX/3Q4;

    .line 1765570
    iget-object v1, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1765571
    invoke-virtual {v0, v1}, LX/3Q4;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1765572
    if-eqz v0, :cond_5

    .line 1765573
    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1765574
    :goto_3
    goto :goto_2

    .line 1765575
    :pswitch_1
    iget-object v0, p0, LX/BFI;->f:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 1765576
    iget-object v1, p1, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1765577
    invoke-virtual {v0, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1765578
    if-eqz v0, :cond_4

    .line 1765579
    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_2

    .line 1765580
    :cond_4
    iget-object v0, p0, LX/BFI;->i:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_single_notification_from_db "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1765581
    iget-object v2, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1765582
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BFI;->g:LX/1rn;

    .line 1765583
    iget-object v3, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1765584
    iget-object v4, p1, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1765585
    invoke-virtual {v2, v3, v4}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_2

    .line 1765586
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1765587
    iget-object v1, p0, LX/BFI;->m:LX/0TD;

    new-instance v2, LX/BFE;

    invoke-direct {v2, p0, v0}, LX/BFE;-><init>(LX/BFI;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1765588
    iget-object v1, p0, LX/BFI;->i:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetch_single_offline_video_story "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1765589
    iget-object v3, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1765590
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_2

    .line 1765591
    :cond_5
    iget-object v0, p0, LX/BFI;->i:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_cached_single_video_home_notification"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1765592
    iget-object v2, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1765593
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BFI;->p:LX/3Q4;

    .line 1765594
    iget-object v3, p1, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1765595
    iget-object v4, v2, LX/3Q4;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3H7;

    sget-object v5, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    sget-object p1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-virtual {v4, v3, v5, p1}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 1765596
    new-instance v3, LX/BFF;

    invoke-direct {v3, p0, p2}, LX/BFF;-><init>(LX/BFI;LX/0Ve;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
