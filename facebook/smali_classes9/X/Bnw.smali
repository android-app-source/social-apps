.class public final LX/Bnw;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Landroid/graphics/Picture;


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1821324
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1821325
    new-instance v0, Landroid/graphics/Picture;

    invoke-direct {v0}, Landroid/graphics/Picture;-><init>()V

    iput-object v0, p0, LX/Bnw;->a:Landroid/graphics/Picture;

    .line 1821326
    return-void
.end method


# virtual methods
.method public final finalize()V
    .locals 1

    .prologue
    .line 1821327
    invoke-virtual {p0}, LX/Bnw;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1821328
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1821329
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1821330
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/text/Layout;

    .line 1821331
    :try_start_0
    iget-object v1, p0, LX/Bnw;->a:Landroid/graphics/Picture;

    invoke-static {v0}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v2

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Picture;->beginRecording(II)Landroid/graphics/Canvas;

    move-result-object v1

    .line 1821332
    invoke-virtual {v0, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1821333
    iget-object v0, p0, LX/Bnw;->a:Landroid/graphics/Picture;

    invoke-virtual {v0}, Landroid/graphics/Picture;->endRecording()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1821334
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
