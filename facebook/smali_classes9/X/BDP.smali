.class public final LX/BDP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1763150
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1763151
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1763152
    if-eqz v0, :cond_0

    .line 1763153
    const-string v1, "num_impressions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763154
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1763155
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1763156
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1763157
    const-string v2, "timestamp_after"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763158
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1763159
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1763160
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 1763161
    const-string v2, "timestamp_before"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763162
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1763163
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1763164
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 1763165
    const/4 v3, 0x0

    .line 1763166
    const-wide/16 v6, 0x0

    .line 1763167
    const-wide/16 v4, 0x0

    .line 1763168
    const/4 v2, 0x0

    .line 1763169
    const/4 v1, 0x0

    .line 1763170
    const/4 v0, 0x0

    .line 1763171
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1763172
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763173
    const/4 v0, 0x0

    .line 1763174
    :goto_0
    return v0

    .line 1763175
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v7, :cond_4

    .line 1763176
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1763177
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1763178
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v10, :cond_0

    if-eqz v0, :cond_0

    .line 1763179
    const-string v7, "num_impressions"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1763180
    const/4 v0, 0x1

    .line 1763181
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v5, v4

    move v4, v0

    goto :goto_1

    .line 1763182
    :cond_1
    const-string v7, "timestamp_after"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1763183
    const/4 v0, 0x1

    .line 1763184
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1763185
    :cond_2
    const-string v7, "timestamp_before"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1763186
    const/4 v0, 0x1

    .line 1763187
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v6

    move-wide v8, v6

    move v6, v0

    goto :goto_1

    .line 1763188
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1763189
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1763190
    if-eqz v4, :cond_5

    .line 1763191
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v5, v4}, LX/186;->a(III)V

    .line 1763192
    :cond_5
    if-eqz v1, :cond_6

    .line 1763193
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1763194
    :cond_6
    if-eqz v6, :cond_7

    .line 1763195
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1763196
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v8, v4

    move v4, v2

    move v5, v3

    move-wide v2, v6

    move v6, v0

    goto :goto_1
.end method
