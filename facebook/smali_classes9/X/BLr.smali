.class public LX/BLr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetSelectorController$TargetSelectorClient;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetSelectorController$DataProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/BLt;

.field public final d:Landroid/content/res/Resources;

.field public final e:Ljava/lang/Boolean;

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/BLm;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0if;

.field public final h:LX/31w;


# direct methods
.method public constructor <init>(LX/BLt;LX/0if;Landroid/content/res/Resources;Ljava/lang/Boolean;LX/31w;Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/BKL;)V
    .locals 4
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/platform/composer/composer/PlatformComposerFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/BKL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776458
    iput-object p1, p0, LX/BLr;->c:LX/BLt;

    .line 1776459
    iput-object p5, p0, LX/BLr;->h:LX/31w;

    .line 1776460
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1776461
    invoke-static {}, LX/BLl;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BLm;

    .line 1776462
    iget-object v3, v0, LX/BLm;->a:LX/2rw;

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1776463
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/BLr;->f:LX/0P1;

    .line 1776464
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BLr;->a:Ljava/lang/ref/WeakReference;

    .line 1776465
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BLr;->b:Ljava/lang/ref/WeakReference;

    .line 1776466
    iput-object p3, p0, LX/BLr;->d:Landroid/content/res/Resources;

    .line 1776467
    iput-object p2, p0, LX/BLr;->g:LX/0if;

    .line 1776468
    iput-object p4, p0, LX/BLr;->e:Ljava/lang/Boolean;

    .line 1776469
    return-void
.end method

.method public static a(LX/0P1;)LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/BLq;",
            ">;)",
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1776470
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1776471
    invoke-virtual {p0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    .line 1776472
    invoke-virtual {p0, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BLq;

    .line 1776473
    iget-boolean v1, v1, LX/BLq;->a:Z

    if-nez v1, :cond_0

    .line 1776474
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 1776475
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/BLr;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/BLq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1776476
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1776477
    iget-object v0, p0, LX/BLr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "dataProvider was garbage collected"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BKL;

    .line 1776478
    iget-object v1, p0, LX/BLr;->c:LX/BLt;

    .line 1776479
    iget-boolean v3, v1, LX/BLt;->c:Z

    if-eqz v3, :cond_2

    sget-object v3, LX/BLt;->b:LX/0Rf;

    :goto_0
    const-class v4, LX/2rw;

    invoke-static {v3, v4}, LX/0RA;->a(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v3

    move-object v1, v3

    .line 1776480
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rw;

    .line 1776481
    new-instance v4, LX/BLp;

    invoke-direct {v4}, LX/BLp;-><init>()V

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1776482
    :cond_0
    new-instance v1, LX/BLo;

    invoke-direct {v1, p0, v0}, LX/BLo;-><init>(LX/BLr;LX/BKL;)V

    invoke-static {v2, v1}, LX/0PM;->a(Ljava/util/Map;LX/4zq;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    .line 1776483
    invoke-static {v1}, LX/BLr;->a(LX/0P1;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No active targets! Target states: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1776484
    return-object v1

    .line 1776485
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    sget-object v3, LX/BLt;->a:LX/0Rf;

    goto :goto_0
.end method
