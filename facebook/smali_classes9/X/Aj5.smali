.class public final LX/Aj5;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V
    .locals 0

    .prologue
    .line 1707599
    iput-object p1, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;B)V
    .locals 0

    .prologue
    .line 1707600
    invoke-direct {p0, p1}, LX/Aj5;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    return-void
.end method


# virtual methods
.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1707601
    iget-object v0, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    if-nez v0, :cond_1

    .line 1707602
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 1707603
    :goto_1
    iget-object v0, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1707604
    iget-object v0, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->j:LX/4Ac;

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    check-cast v0, LX/Aj6;

    .line 1707605
    iget-object v0, v0, LX/Aj6;->a:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1707606
    iget-object v0, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v2, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    iget-object v3, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v0, p0, LX/Aj5;->a:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v0, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    invoke-virtual {v0}, LX/26O;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26N;

    invoke-interface {v2, v3, v0, v1}, LX/Aj7;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V

    .line 1707607
    const/4 v2, 0x1

    goto :goto_0

    .line 1707608
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
