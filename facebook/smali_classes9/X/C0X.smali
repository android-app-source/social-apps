.class public LX/C0X;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pe;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0Y;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0X",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840979
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1840980
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C0X;->b:LX/0Zi;

    .line 1840981
    iput-object p1, p0, LX/C0X;->a:LX/0Ot;

    .line 1840982
    return-void
.end method

.method public static a(LX/0QB;)LX/C0X;
    .locals 4

    .prologue
    .line 1840904
    const-class v1, LX/C0X;

    monitor-enter v1

    .line 1840905
    :try_start_0
    sget-object v0, LX/C0X;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840906
    sput-object v2, LX/C0X;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840909
    new-instance v3, LX/C0X;

    const/16 p0, 0x1e72

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0X;-><init>(LX/0Ot;)V

    .line 1840910
    move-object v0, v3

    .line 1840911
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840912
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840913
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1840925
    check-cast p2, LX/C0W;

    .line 1840926
    iget-object v0, p0, LX/C0X;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0Y;

    iget-object v1, p2, LX/C0W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C0W;->b:LX/1Pq;

    const/16 v8, 0x8

    const/4 v6, 0x1

    .line 1840927
    const/4 v5, 0x0

    .line 1840928
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1840929
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v7

    .line 1840930
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1840931
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v3

    invoke-static {v3}, LX/C0Y;->a(I)[[I

    move-result-object v10

    move v4, v5

    .line 1840932
    :goto_0
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v3

    const/4 v11, 0x5

    invoke-static {v3, v11}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 1840933
    new-instance v3, LX/4Yt;

    invoke-direct {v3}, LX/4Yt;-><init>()V

    aget-object v11, v10, v4

    aget v11, v11, v5

    .line 1840934
    iput v11, v3, LX/4Yt;->E:I

    .line 1840935
    move-object v3, v3

    .line 1840936
    aget-object v11, v10, v4

    const/4 v12, 0x1

    aget v11, v11, v12

    .line 1840937
    iput v11, v3, LX/4Yt;->F:I

    .line 1840938
    move-object v3, v3

    .line 1840939
    aget-object v11, v10, v4

    const/4 v12, 0x2

    aget v11, v11, v12

    .line 1840940
    iput v11, v3, LX/4Yt;->D:I

    .line 1840941
    move-object v3, v3

    .line 1840942
    aget-object v11, v10, v4

    const/4 v12, 0x3

    aget v11, v11, v12

    .line 1840943
    iput v11, v3, LX/4Yt;->C:I

    .line 1840944
    move-object v3, v3

    .line 1840945
    invoke-virtual {v3}, LX/4Yt;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v11

    .line 1840946
    new-instance v12, LX/39x;

    invoke-direct {v12}, LX/39x;-><init>()V

    new-instance p0, LX/4XB;

    invoke-direct {p0}, LX/4XB;-><init>()V

    new-instance p2, LX/2dc;

    invoke-direct {p2}, LX/2dc;-><init>()V

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1840947
    iput-object v3, p2, LX/2dc;->h:Ljava/lang/String;

    .line 1840948
    move-object v3, p2

    .line 1840949
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1840950
    iput-object v3, p0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1840951
    move-object v3, p0

    .line 1840952
    invoke-virtual {v3}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1840953
    iput-object v3, v12, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1840954
    move-object v3, v12

    .line 1840955
    invoke-static {v11}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v11

    .line 1840956
    iput-object v11, v3, LX/39x;->o:LX/0Px;

    .line 1840957
    move-object v3, v3

    .line 1840958
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v9, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1840959
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1840960
    :cond_0
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 1840961
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 1840962
    iput-object v3, v4, LX/39x;->q:LX/0Px;

    .line 1840963
    move-object v3, v4

    .line 1840964
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1840965
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C0Y;->b:LX/23t;

    invoke-virtual {v5, p1}, LX/23t;->c(LX/1De;)LX/240;

    move-result-object v5

    invoke-virtual {v1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/240;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/240;

    move-result-object v3

    check-cast v2, LX/1Pe;

    invoke-virtual {v3, v2}, LX/240;->a(LX/1Pe;)LX/240;

    move-result-object v3

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1840966
    iget-object v7, v3, LX/240;->a:LX/23y;

    iput-object v5, v7, LX/23y;->c:Landroid/graphics/Rect;

    .line 1840967
    move-object v3, v3

    .line 1840968
    iget-object v5, v3, LX/240;->a:LX/23y;

    iput-boolean v6, v5, LX/23y;->d:Z

    .line 1840969
    move-object v3, v3

    .line 1840970
    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v5, 0x7f020e0c

    invoke-interface {v3, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v8, v5}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    .line 1840971
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1840972
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    const/high16 v7, 0x41800000    # 16.0f

    invoke-virtual {v3, v7}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    const v7, 0x7f0a0098

    invoke-virtual {v3, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    .line 1840973
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1840974
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/high16 v7, 0x41300000    # 11.0f

    invoke-virtual {v3, v7}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    const v7, 0x7f0a00a4

    invoke-virtual {v3, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v6, 0x7f0b010f

    invoke-interface {v3, v8, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1840975
    const v4, -0x4cee7ad2

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1840976
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    .line 1840977
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1840978
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1840915
    invoke-static {}, LX/1dS;->b()V

    .line 1840916
    iget v0, p1, LX/1dQ;->b:I

    .line 1840917
    packed-switch v0, :pswitch_data_0

    .line 1840918
    :goto_0
    return-object v2

    .line 1840919
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1840920
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1840921
    check-cast v1, LX/C0W;

    .line 1840922
    iget-object v3, p0, LX/C0X;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C0Y;

    iget-object p1, v1, LX/C0W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/C0W;->b:LX/1Pq;

    .line 1840923
    iget-object p0, v3, LX/C0Y;->a:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1840924
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4cee7ad2
        :pswitch_0
    .end packed-switch
.end method
