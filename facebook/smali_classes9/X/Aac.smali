.class public LX/Aac;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1688770
    check-cast p1, Ljava/lang/String;

    .line 1688771
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1688772
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1688773
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "delay_delete"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1688774
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "graphLiveVideoDelete"

    .line 1688775
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1688776
    move-object v1, v1

    .line 1688777
    const-string v2, "DELETE"

    .line 1688778
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1688779
    move-object v1, v1

    .line 1688780
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1688781
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1688782
    move-object v1, v1

    .line 1688783
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1688784
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1688785
    move-object v0, v1

    .line 1688786
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1688787
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1688788
    move-object v0, v0

    .line 1688789
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1688768
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1688769
    const/4 v0, 0x0

    return-object v0
.end method
