.class public LX/AvM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/compactdisk/DiskCache;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/AvK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/1Ck;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1724015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724016
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    const-string v1, "inspiration_asset_disk_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    new-instance v1, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v1}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    .line 1724017
    invoke-virtual {p1, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/AvM;->a:Lcom/facebook/compactdisk/DiskCache;

    .line 1724018
    iput-object p2, p0, LX/AvM;->b:LX/1Ck;

    .line 1724019
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    .prologue
    .line 1724020
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 1724021
    :try_start_1
    const/4 v3, 0x0

    .line 1724022
    const/16 v0, 0x400

    new-array v4, v0, [B

    .line 1724023
    new-instance v5, Ljava/util/zip/ZipInputStream;

    invoke-direct {v5, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1724024
    const-string v0, ""

    invoke-static {p1, v0}, LX/AvM;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 1724025
    :goto_0
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1724026
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1724027
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, LX/AvM;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1724028
    :cond_0
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object p0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1724029
    :goto_1
    invoke-virtual {v5, v4}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    const/4 v8, -0x1

    if-eq v6, v8, :cond_1

    .line 1724030
    invoke-virtual {v7, v4, v3, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 1724031
    add-int/2addr v0, v6

    goto :goto_1

    .line 1724032
    :cond_1
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1724033
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .line 1724034
    :cond_2
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1724035
    move v0, v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1724036
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1724037
    :goto_2
    return v0

    .line 1724038
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1724039
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :goto_3
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    .line 1724040
    const-string v1, "Decompress"

    const-string v2, "unzip"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1724041
    const/4 v0, 0x0

    goto :goto_2

    .line 1724042
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1724011
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1724012
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1724013
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1724014
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1724010
    iget-object v0, p0, LX/AvM;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
