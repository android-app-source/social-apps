.class public final LX/CDj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CDp;

.field public final synthetic b:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;LX/CDp;)V
    .locals 0

    .prologue
    .line 1860024
    iput-object p1, p0, LX/CDj;->b:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    iput-object p2, p0, LX/CDj;->a:LX/CDp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x66416519

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1860025
    iget-object v1, p0, LX/CDj;->b:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    iget-object v1, v1, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->c:LX/CDo;

    if-eqz v1, :cond_0

    .line 1860026
    iget-object v1, p0, LX/CDj;->b:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    iget-object v1, v1, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->c:LX/CDo;

    iget-object v2, p0, LX/CDj;->b:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    iget-object v3, p0, LX/CDj;->a:LX/CDp;

    .line 1860027
    iget-object p0, v2, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {p0, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    move v2, p0

    .line 1860028
    iget-object v3, v1, LX/CDo;->a:LX/3iF;

    iget-object v3, v3, LX/3iF;->d:LX/3Af;

    invoke-virtual {v3}, LX/3Af;->dismiss()V

    .line 1860029
    iget-object v3, v1, LX/CDo;->a:LX/3iF;

    iget-object v3, v3, LX/3iF;->l:LX/4At;

    invoke-virtual {v3}, LX/4At;->beginShowingProgress()V

    .line 1860030
    iget-object v3, v1, LX/CDo;->a:LX/3iF;

    iget-object v3, v3, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    .line 1860031
    iget-object v5, v3, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CDp;

    move-object v3, v5

    .line 1860032
    iget-object v3, v3, LX/CDp;->a:Ljava/lang/String;

    .line 1860033
    iget-object v5, v1, LX/CDo;->a:LX/3iF;

    iget-object v5, v5, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    invoke-virtual {v5, v3}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->a(Ljava/lang/String;)V

    .line 1860034
    iget-object v5, v1, LX/CDo;->a:LX/3iF;

    .line 1860035
    new-instance v6, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;

    invoke-direct {v6, v5}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;-><init>(LX/3iF;)V

    .line 1860036
    iget-object p0, v5, LX/3iF;->h:LX/1Ck;

    const-string p1, "voice_switcher_processor_refetch_feedback"

    .line 1860037
    iget-object v1, v5, LX/3iF;->e:Lcom/facebook/user/model/User;

    .line 1860038
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1860039
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1860040
    iget-object v1, v5, LX/3iF;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1860041
    :goto_0
    move-object v1, v1

    .line 1860042
    invoke-virtual {p0, p1, v1, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1860043
    :cond_0
    const v1, 0x3c4f70d2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    iget-object v1, v5, LX/3iF;->c:LX/3iH;

    invoke-virtual {v1, v3}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
