.class public final enum LX/BIf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BIf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BIf;

.field public static final enum COMPOSER:LX/BIf;

.field public static final enum SIMPLEPICKER:LX/BIf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1770863
    new-instance v0, LX/BIf;

    const-string v1, "SIMPLEPICKER"

    invoke-direct {v0, v1, v2}, LX/BIf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BIf;->SIMPLEPICKER:LX/BIf;

    .line 1770864
    new-instance v0, LX/BIf;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v3}, LX/BIf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BIf;->COMPOSER:LX/BIf;

    .line 1770865
    const/4 v0, 0x2

    new-array v0, v0, [LX/BIf;

    sget-object v1, LX/BIf;->SIMPLEPICKER:LX/BIf;

    aput-object v1, v0, v2

    sget-object v1, LX/BIf;->COMPOSER:LX/BIf;

    aput-object v1, v0, v3

    sput-object v0, LX/BIf;->$VALUES:[LX/BIf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1770862
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BIf;
    .locals 1

    .prologue
    .line 1770860
    const-class v0, LX/BIf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BIf;

    return-object v0
.end method

.method public static values()[LX/BIf;
    .locals 1

    .prologue
    .line 1770861
    sget-object v0, LX/BIf;->$VALUES:[LX/BIf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BIf;

    return-object v0
.end method
