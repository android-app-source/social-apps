.class public final enum LX/BVs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVs;

.field public static final enum PLACEHOLDER_SRC:LX/BVs;

.field public static final enum SCALE_TYPE:LX/BVs;

.field public static final enum URL:LX/BVs;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1791481
    new-instance v0, LX/BVs;

    const-string v1, "SCALE_TYPE"

    const-string v2, "scaleType"

    invoke-direct {v0, v1, v3, v2}, LX/BVs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVs;->SCALE_TYPE:LX/BVs;

    .line 1791482
    new-instance v0, LX/BVs;

    const-string v1, "URL"

    const-string v2, "url"

    invoke-direct {v0, v1, v4, v2}, LX/BVs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVs;->URL:LX/BVs;

    .line 1791483
    new-instance v0, LX/BVs;

    const-string v1, "PLACEHOLDER_SRC"

    const-string v2, "placeholderSrc"

    invoke-direct {v0, v1, v5, v2}, LX/BVs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVs;->PLACEHOLDER_SRC:LX/BVs;

    .line 1791484
    const/4 v0, 0x3

    new-array v0, v0, [LX/BVs;

    sget-object v1, LX/BVs;->SCALE_TYPE:LX/BVs;

    aput-object v1, v0, v3

    sget-object v1, LX/BVs;->URL:LX/BVs;

    aput-object v1, v0, v4

    sget-object v1, LX/BVs;->PLACEHOLDER_SRC:LX/BVs;

    aput-object v1, v0, v5

    sput-object v0, LX/BVs;->$VALUES:[LX/BVs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791485
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791486
    iput-object p3, p0, LX/BVs;->mValue:Ljava/lang/String;

    .line 1791487
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVs;
    .locals 4

    .prologue
    .line 1791474
    invoke-static {}, LX/BVs;->values()[LX/BVs;

    move-result-object v1

    .line 1791475
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791476
    aget-object v2, v1, v0

    .line 1791477
    iget-object v3, v2, LX/BVs;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791478
    return-object v2

    .line 1791479
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791480
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown url image attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVs;
    .locals 1

    .prologue
    .line 1791472
    const-class v0, LX/BVs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVs;

    return-object v0
.end method

.method public static values()[LX/BVs;
    .locals 1

    .prologue
    .line 1791473
    sget-object v0, LX/BVs;->$VALUES:[LX/BVs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVs;

    return-object v0
.end method
