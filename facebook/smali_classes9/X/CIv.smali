.class public final LX/CIv;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CIw;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public d:LX/1Pt;

.field public e:Z

.field public f:Z

.field public g:F

.field public h:F

.field public final synthetic i:LX/CIw;


# direct methods
.method public constructor <init>(LX/CIw;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 1874881
    iput-object p1, p0, LX/CIv;->i:LX/CIw;

    .line 1874882
    move-object v0, p1

    .line 1874883
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1874884
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CIv;->e:Z

    .line 1874885
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CIv;->f:Z

    .line 1874886
    iput v1, p0, LX/CIv;->g:F

    .line 1874887
    iput v1, p0, LX/CIv;->h:F

    .line 1874888
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1874889
    const-string v0, "FbStaticMapComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1874890
    if-ne p0, p1, :cond_1

    .line 1874891
    :cond_0
    :goto_0
    return v0

    .line 1874892
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1874893
    goto :goto_0

    .line 1874894
    :cond_3
    check-cast p1, LX/CIv;

    .line 1874895
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1874896
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1874897
    if-eq v2, v3, :cond_0

    .line 1874898
    iget v2, p0, LX/CIv;->a:I

    iget v3, p1, LX/CIv;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1874899
    goto :goto_0

    .line 1874900
    :cond_4
    iget v2, p0, LX/CIv;->b:I

    iget v3, p1, LX/CIv;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1874901
    goto :goto_0

    .line 1874902
    :cond_5
    iget-object v2, p0, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v3, p1, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    .line 1874903
    goto :goto_0

    .line 1874904
    :cond_7
    iget-object v2, p1, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-nez v2, :cond_6

    .line 1874905
    :cond_8
    iget-object v2, p0, LX/CIv;->d:LX/1Pt;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CIv;->d:LX/1Pt;

    iget-object v3, p1, LX/CIv;->d:LX/1Pt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1874906
    goto :goto_0

    .line 1874907
    :cond_a
    iget-object v2, p1, LX/CIv;->d:LX/1Pt;

    if-nez v2, :cond_9

    .line 1874908
    :cond_b
    iget-boolean v2, p0, LX/CIv;->e:Z

    iget-boolean v3, p1, LX/CIv;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1874909
    goto :goto_0

    .line 1874910
    :cond_c
    iget-boolean v2, p0, LX/CIv;->f:Z

    iget-boolean v3, p1, LX/CIv;->f:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1874911
    goto :goto_0

    .line 1874912
    :cond_d
    iget v2, p0, LX/CIv;->g:F

    iget v3, p1, LX/CIv;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_e

    move v0, v1

    .line 1874913
    goto :goto_0

    .line 1874914
    :cond_e
    iget v2, p0, LX/CIv;->h:F

    iget v3, p1, LX/CIv;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1874915
    goto :goto_0
.end method
