.class public final LX/BY7;
.super LX/0vn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;)V
    .locals 0

    .prologue
    .line 1794076
    iput-object p1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1794077
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 1794078
    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 1794079
    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 1

    .prologue
    .line 1794080
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 1794081
    const-class v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 1794082
    invoke-direct {p0}, LX/BY7;->b()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/3sp;->i(Z)V

    .line 1794083
    invoke-direct {p0}, LX/BY7;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1794084
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1794085
    :cond_0
    invoke-direct {p0}, LX/BY7;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1794086
    const/16 v0, 0x2000

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1794087
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1794088
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1794089
    :goto_0
    return v0

    .line 1794090
    :cond_0
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 1794091
    goto :goto_0

    .line 1794092
    :sswitch_0
    invoke-direct {p0}, LX/BY7;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1794093
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v2, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v2, v2, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setCurrentItem(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1794094
    goto :goto_0

    .line 1794095
    :sswitch_1
    invoke-direct {p0}, LX/BY7;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1794096
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v2, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v2, v2, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setCurrentItem(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1794097
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    .line 1794098
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1794099
    const-class v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1794100
    invoke-static {}, LX/2fO;->a()LX/2fO;

    move-result-object v0

    .line 1794101
    invoke-direct {p0}, LX/BY7;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/2fO;->a(Z)V

    .line 1794102
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v1, :cond_0

    .line 1794103
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget-object v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2fO;->a(I)V

    .line 1794104
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-virtual {v0, v1}, LX/2fO;->b(I)V

    .line 1794105
    iget-object v1, p0, LX/BY7;->b:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    iget v1, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-virtual {v0, v1}, LX/2fO;->c(I)V

    .line 1794106
    :cond_0
    return-void
.end method
