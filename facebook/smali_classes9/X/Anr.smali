.class public LX/Anr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/154;


# direct methods
.method public constructor <init>(LX/154;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712978
    iput-object p1, p0, LX/Anr;->a:LX/154;

    .line 1712979
    return-void
.end method

.method public static a(LX/Anr;LX/1De;II)LX/1Dg;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1712960
    if-gtz p2, :cond_0

    .line 1712961
    const/4 v0, 0x0

    .line 1712962
    :goto_0
    return-object v0

    .line 1712963
    :cond_0
    iget-object v0, p0, LX/Anr;->a:LX/154;

    invoke-virtual {v0, p2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1712964
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v1, p3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1712965
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b0976

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b0971

    invoke-interface {v0, v1, v2}, LX/1Di;->n(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Anr;
    .locals 4

    .prologue
    .line 1712966
    const-class v1, LX/Anr;

    monitor-enter v1

    .line 1712967
    :try_start_0
    sget-object v0, LX/Anr;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712968
    sput-object v2, LX/Anr;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712969
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712970
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712971
    new-instance p0, LX/Anr;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v3

    check-cast v3, LX/154;

    invoke-direct {p0, v3}, LX/Anr;-><init>(LX/154;)V

    .line 1712972
    move-object v0, p0

    .line 1712973
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712974
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Anr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712975
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712976
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
