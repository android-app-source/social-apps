.class public final LX/CJ9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/messaging/model/attribution/ContentAppAttribution;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attribution/InlineReplyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V
    .locals 0

    .prologue
    .line 1875091
    iput-object p1, p0, LX/CJ9;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1875092
    const-string v0, "InlineReplyFragment"

    const-string v1, "Unable to prefetch ContentAppAttribution"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1875093
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875094
    check-cast p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1875095
    iget-object v0, p0, LX/CJ9;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    .line 1875096
    iput-object p1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->N:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1875097
    return-void
.end method
