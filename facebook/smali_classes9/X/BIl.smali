.class public LX/BIl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BIi;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1770918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770919
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1770920
    invoke-static {v2}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/BIl;->a:Ljava/util/List;

    .line 1770921
    invoke-static {v2}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/BIl;->b:Ljava/util/Map;

    .line 1770922
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1770923
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770924
    invoke-static {v0}, LX/BIa;->a(Lcom/facebook/photos/base/media/PhotoItem;)LX/BIa;

    move-result-object v3

    .line 1770925
    iget-object v4, p0, LX/BIl;->a:Ljava/util/List;

    invoke-virtual {v3}, LX/BIa;->a()Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1770926
    iget-object v3, p0, LX/BIl;->b:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770927
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1770928
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1770899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770900
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770901
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770902
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1770903
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 1770904
    invoke-static {v3}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/BIl;->a:Ljava/util/List;

    .line 1770905
    invoke-static {v3}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/BIl;->b:Ljava/util/Map;

    move v2, v1

    .line 1770906
    :goto_1
    if-ge v2, v3, :cond_1

    .line 1770907
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770908
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1770909
    iget-object v4, p0, LX/BIl;->a:Ljava/util/List;

    invoke-static {v0}, LX/BIa;->a(Lcom/facebook/photos/base/media/PhotoItem;)LX/BIa;

    move-result-object v5

    .line 1770910
    iput-object v1, v5, LX/BIa;->b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1770911
    move-object v1, v5

    .line 1770912
    invoke-virtual {v1}, LX/BIa;->a()Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1770913
    iget-object v1, p0, LX/BIl;->b:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770914
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1770915
    goto :goto_0

    .line 1770916
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1770917
    iget-object v0, p0, LX/BIl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;
    .locals 1

    .prologue
    .line 1770897
    iget-object v0, p0, LX/BIl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1770898
    iget-object v0, p0, LX/BIl;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method
