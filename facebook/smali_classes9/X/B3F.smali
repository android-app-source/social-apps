.class public final LX/B3F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/B3G;


# direct methods
.method public constructor <init>(LX/B3G;)V
    .locals 0

    .prologue
    .line 1739841
    iput-object p1, p0, LX/B3F;->a:LX/B3G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1718f4c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1739842
    iget-object v0, p0, LX/B3F;->a:LX/B3G;

    iget-object v0, v0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l:LX/B4O;

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v2

    .line 1739843
    const-string v3, "heisman_open_pivot_from_camera"

    invoke-static {v0, v3, v2}, LX/B4O;->g(LX/B4O;Ljava/lang/String;Ljava/lang/String;)V

    .line 1739844
    new-instance v0, LX/B4N;

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/B3F;->a:LX/B3G;

    iget-object v3, v3, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v3, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, LX/B4N;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    .line 1739845
    iget-object v3, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v2, v3

    .line 1739846
    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B4N;->b(Ljava/lang/String;)LX/B4N;

    move-result-object v0

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/B4J;->a(J)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->h()I

    move-result v2

    invoke-virtual {v0, v2}, LX/B4J;->a(I)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    invoke-virtual {v0}, LX/B4N;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    move-result-object v0

    .line 1739847
    iget-object v2, p0, LX/B3F;->a:LX/B3G;

    iget-object v2, v2, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->h:LX/B35;

    iget-object v3, p0, LX/B3F;->a:LX/B3G;

    iget-object v3, v3, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v3, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-virtual {v2, v3, v0}, LX/B35;->a(Landroid/content/Context;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V

    .line 1739848
    iget-object v0, p0, LX/B3F;->a:LX/B3G;

    iget-object v0, v0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1739849
    const v0, -0x5c504895

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
