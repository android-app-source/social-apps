.class public LX/C1N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842381
    iput-object p1, p0, LX/C1N;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1842382
    iput-object p2, p0, LX/C1N;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1842383
    return-void
.end method

.method public static a(LX/0QB;)LX/C1N;
    .locals 5

    .prologue
    .line 1842384
    const-class v1, LX/C1N;

    monitor-enter v1

    .line 1842385
    :try_start_0
    sget-object v0, LX/C1N;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842386
    sput-object v2, LX/C1N;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842389
    new-instance p0, LX/C1N;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/C1N;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V

    .line 1842390
    move-object v0, p0

    .line 1842391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
