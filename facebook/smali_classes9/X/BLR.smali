.class public final LX/BLR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1775503
    iput-object p1, p0, LX/BLR;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1775504
    iget-object v0, p0, LX/BLR;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1775505
    iget-object v1, p0, LX/BLR;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    .line 1775506
    if-nez v0, :cond_1

    .line 1775507
    :cond_0
    :goto_0
    return-void

    .line 1775508
    :cond_1
    iget-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->j:LX/0if;

    sget-object p1, LX/0ig;->k:LX/0ih;

    const-string p3, " %s privacy option clicked"

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1775509
    iget-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0, p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1775510
    iget-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1775511
    instance-of p0, v0, LX/8QO;

    if-nez p0, :cond_0

    instance-of p0, v0, LX/8QX;

    if-nez p0, :cond_0

    instance-of p0, v0, LX/8QN;

    if-nez p0, :cond_0

    .line 1775512
    const/4 p0, -0x1

    .line 1775513
    instance-of p1, p2, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    if-eqz p1, :cond_3

    .line 1775514
    const p0, 0x7f0d0beb

    .line 1775515
    :cond_2
    :goto_1
    if-lez p0, :cond_0

    .line 1775516
    invoke-virtual {p2, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 1775517
    if-eqz p0, :cond_0

    .line 1775518
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object p1

    .line 1775519
    if-eqz p1, :cond_4

    .line 1775520
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p3

    invoke-static {v1, p1, p3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V

    .line 1775521
    :goto_2
    goto :goto_0

    .line 1775522
    :cond_3
    instance-of p1, p2, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    if-eqz p1, :cond_2

    .line 1775523
    const p0, 0x7f0d0e19

    goto :goto_1

    .line 1775524
    :cond_4
    invoke-virtual {p0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    .line 1775525
    new-instance p3, LX/BLO;

    invoke-direct {p3, v1, p0, p1}, LX/BLO;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Landroid/widget/TextView;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {p1, p3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_2
.end method
