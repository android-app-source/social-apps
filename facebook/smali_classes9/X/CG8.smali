.class public LX/CG8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/support/v7/widget/RecyclerView;

.field public b:LX/CEO;

.field private final c:Landroid/graphics/Rect;

.field public d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 1864918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864919
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/CG8;->c:Landroid/graphics/Rect;

    .line 1864920
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/CG8;->d:Landroid/util/SparseArray;

    .line 1864921
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/CG8;->e:Landroid/util/SparseArray;

    .line 1864922
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    .line 1864923
    iput-object p1, p0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1864924
    iget-object v0, p0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/CG6;

    invoke-direct {v1, p0}, LX/CG6;-><init>(LX/CG8;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1864925
    return-void
.end method

.method private a(Landroid/util/SparseArray;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864926
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1864927
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1864928
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1864929
    const v1, 0x7f0d01d4

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864930
    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    invoke-virtual {v3, p2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1864931
    iget-object v3, p0, LX/CG8;->b:LX/CEO;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    invoke-virtual {v1, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMAction;

    invoke-interface {v3, v1, v0}, LX/CEO;->a(Lcom/facebook/greetingcards/verve/model/VMAction;Landroid/view/View;)V

    .line 1864932
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1864933
    :cond_1
    return-void
.end method

.method public static a$redex0(LX/CG8;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1864934
    iget-object v0, p0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    move v2, v3

    .line 1864935
    :goto_0
    iget-object v0, p0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1864936
    iget-object v0, p0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move v4, v3

    .line 1864937
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v4, v1, :cond_1

    .line 1864938
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1864939
    iget-object v1, p0, LX/CG8;->c:Landroid/graphics/Rect;

    invoke-virtual {v5, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1864940
    const v1, 0x7f0d01d4

    invoke-virtual {v5, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864941
    iget-object v6, p0, LX/CG8;->d:Landroid/util/SparseArray;

    iget v1, v1, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v6, v1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1864942
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1864943
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1864944
    :cond_2
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    move v0, v3

    .line 1864945
    :goto_2
    iget-object v1, p0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1864946
    iget-object v1, p0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1864947
    iget-object v2, p0, LX/CG8;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_3

    .line 1864948
    iget-object v2, p0, LX/CG8;->f:Landroid/util/SparseArray;

    iget-object v4, p0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1864949
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1864950
    :cond_4
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1864951
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    const-string v1, "visible"

    invoke-direct {p0, v0, v1}, LX/CG8;->a(Landroid/util/SparseArray;Ljava/lang/String;)V

    .line 1864952
    :cond_5
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1864953
    :goto_3
    iget-object v0, p0, LX/CG8;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 1864954
    iget-object v0, p0, LX/CG8;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1864955
    iget-object v1, p0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_6

    .line 1864956
    iget-object v1, p0, LX/CG8;->f:Landroid/util/SparseArray;

    iget-object v2, p0, LX/CG8;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1864957
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1864958
    :cond_7
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 1864959
    iget-object v0, p0, LX/CG8;->f:Landroid/util/SparseArray;

    const-string v1, "hidden"

    invoke-direct {p0, v0, v1}, LX/CG8;->a(Landroid/util/SparseArray;Ljava/lang/String;)V

    .line 1864960
    :cond_8
    iget-object v0, p0, LX/CG8;->e:Landroid/util/SparseArray;

    .line 1864961
    iget-object v1, p0, LX/CG8;->d:Landroid/util/SparseArray;

    iput-object v1, p0, LX/CG8;->e:Landroid/util/SparseArray;

    .line 1864962
    iput-object v0, p0, LX/CG8;->d:Landroid/util/SparseArray;

    .line 1864963
    return-void
.end method
