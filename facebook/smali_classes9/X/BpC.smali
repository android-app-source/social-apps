.class public final LX/BpC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:LX/5kD;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;LX/5kD;LX/1Po;)V
    .locals 0

    .prologue
    .line 1822670
    iput-object p1, p0, LX/BpC;->c:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    iput-object p2, p0, LX/BpC;->a:LX/5kD;

    iput-object p3, p0, LX/BpC;->b:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 6

    .prologue
    .line 1822671
    iget-object v0, p0, LX/BpC;->c:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    iget-object v1, p0, LX/BpC;->a:LX/5kD;

    iget-object v2, p0, LX/BpC;->b:LX/1Po;

    .line 1822672
    sget-object v3, LX/BpD;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1822673
    :goto_0
    return-void

    .line 1822674
    :pswitch_0
    invoke-interface {v1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v3

    invoke-static {v3}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1822675
    check-cast v2, LX/Bp4;

    invoke-interface {v2, v3}, LX/Bp4;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1822676
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 1822677
    :pswitch_1
    invoke-interface {v1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v3

    invoke-static {v3}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1822678
    check-cast v2, LX/Bp2;

    sget-object v4, LX/An0;->PHOTOS_FEED_FOOTER:LX/An0;

    invoke-interface {v2, v3, p1, v4}, LX/Bp2;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V

    .line 1822679
    goto :goto_0

    .line 1822680
    :pswitch_2
    iget-object v4, v0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->e:LX/1Kf;

    const/4 v5, 0x0

    iget-object v3, v0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object p1

    invoke-static {p1}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object p1

    const-string p2, "mediaMetadataFooter"

    invoke-interface {v3, p0, p1, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Ljava/lang/String;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    iget-object p0, v0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->c:Landroid/content/Context;

    invoke-interface {v4, v5, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1822681
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
