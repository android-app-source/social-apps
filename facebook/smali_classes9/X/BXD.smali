.class public LX/BXD;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1792974
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1792975
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/BXD;->b:Ljava/util/Set;

    .line 1792976
    iput-object p1, p0, LX/BXD;->a:LX/0Px;

    .line 1792977
    return-void
.end method

.method private b(I)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 1

    .prologue
    .line 1792973
    iget-object v0, p0, LX/BXD;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    return-object v0
.end method


# virtual methods
.method public final a(LX/8QL;)I
    .locals 1

    .prologue
    .line 1792972
    iget-object v0, p0, LX/BXD;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1792971
    iget-object v0, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1792965
    invoke-direct {p0, p1}, LX/BXD;->b(I)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v0

    .line 1792966
    iget-object v1, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1792967
    iget-object v1, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1792968
    :goto_0
    const v0, 0x184944a4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1792969
    return-void

    .line 1792970
    :cond_0
    iget-object v1, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1792961
    iput-object p1, p0, LX/BXD;->a:LX/0Px;

    .line 1792962
    iget-object v0, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1792963
    const v0, 0x6777c44e

    invoke-static {p0, v0}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    .line 1792964
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1792960
    iget-object v0, p0, LX/BXD;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1792951
    iget-object v0, p0, LX/BXD;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1792959
    invoke-direct {p0, p1}, LX/BXD;->b(I)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1792958
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1792952
    if-nez p2, :cond_1

    .line 1792953
    new-instance v1, LX/BWj;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/BWj;-><init>(Landroid/content/Context;)V

    .line 1792954
    :goto_0
    invoke-direct {p0, p1}, LX/BXD;->b(I)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v3

    move-object v0, v1

    .line 1792955
    check-cast v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    iget-object v2, p0, LX/BXD;->b:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v0, v3, v2}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 1792956
    return-object v1

    .line 1792957
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method
