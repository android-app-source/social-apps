.class public final LX/B7C;
.super LX/B7B;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/B7K;

.field private e:LX/B7H;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;LX/B7K;)V
    .locals 2

    .prologue
    .line 1747766
    invoke-direct {p0}, LX/B7B;-><init>()V

    .line 1747767
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1747768
    :cond_0
    :goto_0
    return-void

    .line 1747769
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    .line 1747770
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7C;->a:Ljava/lang/String;

    .line 1747771
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->j()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/B7C;->c:LX/0Px;

    .line 1747772
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v1

    iput-object v1, p0, LX/B7C;->b:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    .line 1747773
    iput-object p2, p0, LX/B7C;->d:LX/B7K;

    .line 1747774
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1747775
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->l()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1747776
    new-instance v0, LX/B7H;

    invoke-direct {v0, v1}, LX/B7H;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/B7C;->e:LX/B7H;

    goto :goto_0
.end method
