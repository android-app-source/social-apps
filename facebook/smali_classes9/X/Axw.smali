.class public LX/Axw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Axw;


# instance fields
.field public final a:LX/0TD;

.field public final b:LX/AyG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0SG;


# direct methods
.method public constructor <init>(LX/0TD;LX/AyG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729100
    iput-object p1, p0, LX/Axw;->a:LX/0TD;

    .line 1729101
    iput-object p2, p0, LX/Axw;->b:LX/AyG;

    .line 1729102
    iput-object p3, p0, LX/Axw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1729103
    iput-object p4, p0, LX/Axw;->d:LX/0SG;

    .line 1729104
    return-void
.end method

.method public static a(LX/0QB;)LX/Axw;
    .locals 7

    .prologue
    .line 1729105
    sget-object v0, LX/Axw;->e:LX/Axw;

    if-nez v0, :cond_1

    .line 1729106
    const-class v1, LX/Axw;

    monitor-enter v1

    .line 1729107
    :try_start_0
    sget-object v0, LX/Axw;->e:LX/Axw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729108
    if-eqz v2, :cond_0

    .line 1729109
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729110
    new-instance p0, LX/Axw;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/AyG;->a(LX/0QB;)LX/AyG;

    move-result-object v4

    check-cast v4, LX/AyG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Axw;-><init>(LX/0TD;LX/AyG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 1729111
    move-object v0, p0

    .line 1729112
    sput-object v0, LX/Axw;->e:LX/Axw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729113
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729114
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729115
    :cond_1
    sget-object v0, LX/Axw;->e:LX/Axw;

    return-object v0

    .line 1729116
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729118
    iget-object v0, p0, LX/Axw;->a:LX/0TD;

    new-instance v1, LX/Axv;

    invoke-direct {v1, p0, p1}, LX/Axv;-><init>(LX/Axw;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 1729119
    iget-object v0, p0, LX/Axw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AyF;->b:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1729120
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method
