.class public final LX/C98;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final a:LX/9A1;

.field public final b:Landroid/content/Context;

.field public c:Lcom/facebook/composer/publish/common/PendingStory;

.field public d:LX/CA6;

.field public e:LX/2EJ;

.field public f:LX/2EJ;


# direct methods
.method public constructor <init>(LX/9A1;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1853665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853666
    iput-object p1, p0, LX/C98;->a:LX/9A1;

    .line 1853667
    iput-object p2, p0, LX/C98;->b:Landroid/content/Context;

    .line 1853668
    return-void
.end method

.method public static a(LX/C98;Lcom/facebook/graphql/model/GraphQLStory;Z)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 1853669
    new-instance v0, LX/C97;

    invoke-direct {v0, p0, p1, p2}, LX/C97;-><init>(LX/C98;Lcom/facebook/graphql/model/GraphQLStory;Z)V

    return-object v0
.end method

.method public static a(LX/2EJ;)V
    .locals 1

    .prologue
    .line 1853670
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/2EJ;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853671
    invoke-virtual {p0}, LX/2EJ;->dismiss()V

    .line 1853672
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1853673
    iget-object v0, p0, LX/C98;->c:Lcom/facebook/composer/publish/common/PendingStory;

    if-nez v0, :cond_1

    .line 1853674
    :cond_0
    :goto_0
    return v3

    .line 1853675
    :cond_1
    iget-object v0, p0, LX/C98;->c:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1853676
    const v1, 0x7f0d3215

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 1853677
    iget-object v1, p0, LX/C98;->a:LX/9A1;

    invoke-virtual {v1, v0}, LX/9A1;->c(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1853678
    :cond_2
    const v1, 0x7f0d3216

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 1853679
    iget-object v1, p0, LX/C98;->a:LX/9A1;

    .line 1853680
    iget-object v2, v1, LX/9A1;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1EZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1853681
    if-eqz v2, :cond_3

    .line 1853682
    new-instance p0, LX/8LQ;

    invoke-direct {p0, v2}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1853683
    const/4 v2, -0x1

    .line 1853684
    iput v2, p0, LX/8LQ;->W:I

    .line 1853685
    const/4 v2, 0x1

    .line 1853686
    iput-boolean v2, p0, LX/8LQ;->af:Z

    .line 1853687
    iget-object v2, v1, LX/9A1;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1EZ;

    invoke-virtual {p0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1853688
    :cond_3
    goto :goto_0

    .line 1853689
    :cond_4
    const v1, 0x7f0d3217

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 1853690
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f08298a

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f08298b

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    const/4 p1, 0x1

    invoke-static {p0, v0, p1}, LX/C98;->a(LX/C98;Lcom/facebook/graphql/model/GraphQLStory;Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    iput-object v1, p0, LX/C98;->e:LX/2EJ;

    .line 1853691
    iget-object v1, p0, LX/C98;->e:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1853692
    goto/16 :goto_0

    .line 1853693
    :cond_5
    const v1, 0x7f0d3218

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1853694
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f08298c

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/C98;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f08298d

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, LX/C98;->a(LX/C98;Lcom/facebook/graphql/model/GraphQLStory;Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    iput-object v1, p0, LX/C98;->f:LX/2EJ;

    .line 1853695
    iget-object v1, p0, LX/C98;->f:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1853696
    goto/16 :goto_0
.end method
