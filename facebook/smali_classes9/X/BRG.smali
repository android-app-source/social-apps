.class public LX/BRG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BR0;


# instance fields
.field public a:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

.field public b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public c:LX/BR1;

.field public d:Lcom/facebook/video/player/RichVideoPlayer;

.field private e:Landroid/view/View;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final h:LX/19P;

.field public final i:LX/BTG;

.field public final j:LX/0gc;

.field public k:LX/BSt;

.field private final l:Landroid/content/Context;

.field public final m:LX/BR2;

.field public final n:LX/BQj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/19P;LX/BTG;LX/BR2;LX/BQj;LX/0gc;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 0
    .param p8    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/BR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783737
    iput-object p1, p0, LX/BRG;->l:Landroid/content/Context;

    .line 1783738
    iput-object p10, p0, LX/BRG;->c:LX/BR1;

    .line 1783739
    iput-object p2, p0, LX/BRG;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1783740
    iput-object p3, p0, LX/BRG;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1783741
    iput-object p4, p0, LX/BRG;->h:LX/19P;

    .line 1783742
    iput-object p8, p0, LX/BRG;->j:LX/0gc;

    .line 1783743
    iput-object p5, p0, LX/BRG;->i:LX/BTG;

    .line 1783744
    iput-object p6, p0, LX/BRG;->m:LX/BR2;

    .line 1783745
    iput-object p7, p0, LX/BRG;->n:LX/BQj;

    .line 1783746
    iget-object p1, p9, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, p1

    .line 1783747
    const-string p2, "extra_video_edit_gallery_launch_settings"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    iput-object p1, p0, LX/BRG;->a:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1783748
    iget-object p1, p0, LX/BRG;->a:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    if-nez p1, :cond_0

    .line 1783749
    const-class p1, Lcom/facebook/timeline/stagingground/StagingGroundActivity;

    const-string p2, "mVideoEditGalleryLaunchConfiguration must be set"

    invoke-static {p1, p2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1783750
    invoke-virtual {p9}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 1783751
    :goto_0
    return-void

    .line 1783752
    :cond_0
    if-eqz p11, :cond_3

    const-string p1, "videoCreativeEditingDataKey"

    invoke-virtual {p11, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 1783753
    const-string p1, "videoCreativeEditingDataKey"

    invoke-virtual {p11, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object p2, p0

    .line 1783754
    :goto_1
    iput-object p1, p2, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1783755
    iget-object p1, p0, LX/BRG;->m:LX/BR2;

    const/4 p2, 0x0

    .line 1783756
    if-eqz p11, :cond_1

    const-string p9, "hasShownNuxKey"

    invoke-virtual {p11, p9, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p2

    :cond_1
    iput-boolean p2, p1, LX/BR2;->d:Z

    .line 1783757
    new-instance p1, LX/BSt;

    iget-object p2, p0, LX/BRG;->j:LX/0gc;

    invoke-direct {p1, p2}, LX/BSt;-><init>(LX/0gc;)V

    iput-object p1, p0, LX/BRG;->k:LX/BSt;

    .line 1783758
    iget-object p1, p0, LX/BRG;->n:LX/BQj;

    .line 1783759
    iget-boolean p2, p1, LX/BQj;->d:Z

    move p1, p2

    .line 1783760
    if-nez p1, :cond_2

    .line 1783761
    iget-object p1, p0, LX/BRG;->n:LX/BQj;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/BQj;->a(I)V

    .line 1783762
    :cond_2
    iget-object p1, p0, LX/BRG;->n:LX/BQj;

    invoke-virtual {p1}, LX/BQj;->g()V

    goto :goto_0

    .line 1783763
    :cond_3
    iget-object p1, p12, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object p1, p1

    .line 1783764
    if-eqz p1, :cond_4

    .line 1783765
    iget-object p1, p12, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object p1, p1

    .line 1783766
    move-object p2, p0

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object p1

    move-object p2, p0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/BRg;)LX/63W;
    .locals 1

    .prologue
    .line 1783631
    new-instance v0, LX/BRD;

    invoke-direct {v0, p0, p1, p2}, LX/BRD;-><init>(LX/BRG;Lcom/facebook/base/fragment/FbFragment;LX/BRg;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/base/activity/FbFragmentActivity;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 1783767
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1783768
    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1783769
    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 1783770
    :goto_0
    return-void

    .line 1783771
    :cond_0
    if-eq p3, v1, :cond_1

    .line 1783772
    iget-object v0, p0, LX/BRG;->n:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->g()V

    goto :goto_0

    .line 1783773
    :cond_1
    invoke-virtual {p1, v1}, Landroid/app/Activity;->setResult(I)V

    .line 1783774
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1783775
    const-string v0, "videoCreativeEditingDataKey"

    iget-object v1, p0, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783776
    iget-object v0, p0, LX/BRG;->m:LX/BR2;

    .line 1783777
    const-string v1, "hasShownNuxKey"

    iget-boolean p0, v0, LX/BR2;->d:Z

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1783778
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1783779
    const v0, 0x7f0d2d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783780
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1783781
    const v1, 0x7f0d2d66

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1783782
    return-void
.end method

.method public final a(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V
    .locals 7

    .prologue
    .line 1783701
    const v0, 0x7f0d2d67

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783702
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BRG;->e:Landroid/view/View;

    .line 1783703
    iget-object v0, p0, LX/BRG;->e:Landroid/view/View;

    .line 1783704
    new-instance v1, LX/BRF;

    invoke-direct {v1, p0}, LX/BRF;-><init>(LX/BRG;)V

    move-object v1, v1

    .line 1783705
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1783706
    iget-object v0, p0, LX/BRG;->m:LX/BR2;

    iget-object v1, p0, LX/BRG;->e:Landroid/view/View;

    .line 1783707
    iget-boolean v2, v0, LX/BR2;->d:Z

    if-eqz v2, :cond_1

    .line 1783708
    :cond_0
    :goto_0
    return-void

    .line 1783709
    :cond_1
    iget-object v2, v0, LX/BR2;->c:LX/0iA;

    const-string v3, "4314"

    const-class v4, LX/3lE;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3lE;

    .line 1783710
    if-nez v2, :cond_3

    .line 1783711
    const/4 v3, 0x0

    .line 1783712
    :goto_1
    move-object v2, v3

    .line 1783713
    if-eqz v2, :cond_0

    .line 1783714
    const v4, 0x7f08189b

    const v5, 0x7f08189c

    const v6, 0x7f020919

    invoke-virtual {v2}, LX/3lE;->b()Ljava/lang/String;

    move-result-object p0

    move-object v2, v0

    move-object v3, v1

    .line 1783715
    new-instance p1, LX/0hs;

    iget-object v0, v2, LX/BR2;->a:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-direct {p1, v0, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1783716
    sget-object v0, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {p1, v0}, LX/0ht;->a(LX/3AV;)V

    .line 1783717
    const/4 v0, -0x1

    .line 1783718
    iput v0, p1, LX/0hs;->t:I

    .line 1783719
    invoke-virtual {p1, v3}, LX/0ht;->a(Landroid/view/View;)V

    .line 1783720
    invoke-virtual {p1, v4}, LX/0hs;->a(I)V

    .line 1783721
    invoke-virtual {p1, v5}, LX/0hs;->b(I)V

    .line 1783722
    invoke-virtual {p1, v6}, LX/0hs;->c(I)V

    .line 1783723
    invoke-virtual {p1}, LX/0ht;->d()V

    .line 1783724
    const/4 p1, 0x1

    iput-boolean p1, v2, LX/BR2;->d:Z

    .line 1783725
    if-eqz p0, :cond_2

    .line 1783726
    iget-object p1, v2, LX/BR2;->c:LX/0iA;

    invoke-virtual {p1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1783727
    :cond_2
    goto :goto_0

    .line 1783728
    :cond_3
    const/4 v3, 0x1

    .line 1783729
    iput-boolean v3, v2, LX/3lE;->b:Z

    .line 1783730
    iget-object v3, v0, LX/BR2;->c:LX/0iA;

    sget-object v4, LX/3lE;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v5, LX/3lE;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3lE;

    .line 1783731
    const/4 v4, 0x0

    .line 1783732
    iput-boolean v4, v2, LX/3lE;->b:Z

    .line 1783733
    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1783734
    invoke-virtual {p0}, LX/BRG;->h()V

    .line 1783735
    return-void
.end method

.method public final b(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V
    .locals 0

    .prologue
    .line 1783700
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1783699
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1783697
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1783698
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1783695
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1783696
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1783689
    iget-object v0, p0, LX/BRG;->n:LX/BQj;

    .line 1783690
    iget v1, v0, LX/BQj;->e:I

    move v0, v1

    .line 1783691
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1783692
    iget-object v0, p0, LX/BRG;->n:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->k()V

    .line 1783693
    iget-object v0, p0, LX/BRG;->n:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->b()V

    .line 1783694
    :cond_0
    return-void
.end method

.method public final g()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1783688
    const v0, 0x7f081896

    return v0
.end method

.method public final h()V
    .locals 10

    .prologue
    .line 1783632
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->l()Z

    .line 1783633
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v2, p0, LX/BRG;->l:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1783634
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1783635
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/7Nb;

    iget-object v2, p0, LX/BRG;->l:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/7Nb;-><init>(Landroid/content/Context;)V

    .line 1783636
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1783637
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    iget-object v2, p0, LX/BRG;->l:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;-><init>(Landroid/content/Context;)V

    .line 1783638
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1783639
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04D;->PROFILE_VIDEO_PREVIEW:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1783640
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1783641
    iget-object v0, p0, LX/BRG;->h:LX/19P;

    invoke-virtual {v0}, LX/19P;->d()V

    .line 1783642
    iget-object v0, p0, LX/BRG;->c:LX/BR1;

    iget-object v0, v0, LX/BR1;->a:Landroid/net/Uri;

    iget-object v1, p0, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1783643
    new-instance v3, LX/2oE;

    invoke-direct {v3}, LX/2oE;-><init>()V

    .line 1783644
    iput-object v0, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 1783645
    move-object v3, v3

    .line 1783646
    sget-object v4, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1783647
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 1783648
    move-object v4, v3

    .line 1783649
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 1783650
    :goto_0
    iput-object v3, v4, LX/2oE;->g:LX/2oF;

    .line 1783651
    move-object v3, v4

    .line 1783652
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    .line 1783653
    new-instance v4, LX/2oH;

    invoke-direct {v4}, LX/2oH;-><init>()V

    invoke-virtual {v4, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    const/4 v4, 0x1

    .line 1783654
    iput-boolean v4, v3, LX/2oH;->g:Z

    .line 1783655
    move-object v3, v3

    .line 1783656
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "staging_ground_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1783657
    iput-object v4, v3, LX/2oH;->b:Ljava/lang/String;

    .line 1783658
    move-object v3, v3

    .line 1783659
    invoke-virtual {v3}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 1783660
    const/4 v4, 0x0

    .line 1783661
    const/4 v3, -0x1

    .line 1783662
    if-eqz v1, :cond_0

    .line 1783663
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v6

    .line 1783664
    if-eqz v6, :cond_0

    iget-boolean v7, v6, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-eqz v7, :cond_0

    .line 1783665
    iget v4, v6, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    .line 1783666
    iget v3, v6, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    .line 1783667
    :cond_0
    new-instance v6, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v6}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1783668
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1783669
    invoke-static {v6}, LX/BTk;->b(Landroid/media/MediaMetadataRetriever;)F

    move-result v7

    .line 1783670
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1783671
    new-instance v6, LX/2pZ;

    invoke-direct {v6}, LX/2pZ;-><init>()V

    .line 1783672
    iput-object v5, v6, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1783673
    move-object v5, v6

    .line 1783674
    const-string v8, "TrimStartPosition"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v8, v4}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v4

    const-string v5, "TrimEndPosition"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v3

    if-eqz v3, :cond_3

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    .line 1783675
    :goto_1
    iput-wide v3, v5, LX/2pZ;->e:D

    .line 1783676
    if-eqz v1, :cond_1

    .line 1783677
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1783678
    if-eqz v3, :cond_1

    .line 1783679
    const-string v4, "OverlayImageParamsKey"

    invoke-virtual {v6, v4, v3}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1783680
    :cond_1
    invoke-virtual {v6}, LX/2pZ;->b()LX/2pa;

    move-result-object v3

    move-object v0, v3

    .line 1783681
    iget-object v1, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1783682
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setCropRect(Landroid/graphics/RectF;)V

    .line 1783683
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v1

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1783684
    iget-object v0, p0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1783685
    return-void

    .line 1783686
    :cond_2
    sget-object v3, LX/2oF;->NONE:LX/2oF;

    goto/16 :goto_0

    .line 1783687
    :cond_3
    float-to-double v3, v7

    goto :goto_1
.end method
