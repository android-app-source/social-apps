.class public final LX/B0Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0d;

.field public final synthetic b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final synthetic c:LX/B0M;

.field public final synthetic d:LX/B0a;

.field private e:LX/B0d;


# direct methods
.method public constructor <init>(LX/B0a;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;)V
    .locals 1

    .prologue
    .line 1733986
    iput-object p1, p0, LX/B0Y;->d:LX/B0a;

    iput-object p2, p0, LX/B0Y;->a:LX/B0d;

    iput-object p3, p0, LX/B0Y;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p4, p0, LX/B0Y;->c:LX/B0M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733987
    iget-object v0, p0, LX/B0Y;->a:LX/B0d;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0Y;->e:LX/B0d;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1733988
    iget-object v0, p0, LX/B0Y;->c:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1733989
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1733990
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1733991
    if-eqz p1, :cond_0

    .line 1733992
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1733993
    if-nez v0, :cond_1

    .line 1733994
    :cond_0
    const-class v0, LX/B0a;

    const-string v1, "Null response from network"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1733995
    :goto_0
    return-void

    .line 1733996
    :cond_1
    iget-object v0, p0, LX/B0Y;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/B0Y;->d:LX/B0a;

    iget-object v1, v1, LX/B0a;->d:LX/B0V;

    iget-object v2, p0, LX/B0Y;->e:LX/B0d;

    invoke-static {v0, v1, v2, p1}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;

    move-result-object v1

    .line 1733997
    invoke-interface {v1}, LX/B0N;->d()LX/B0d;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0Y;->e:LX/B0d;

    .line 1733998
    iget-object v0, p0, LX/B0Y;->c:LX/B0M;

    invoke-virtual {v0, v1}, LX/B0M;->a(LX/B0N;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1733999
    iget-object v0, p0, LX/B0Y;->c:LX/B0M;

    invoke-virtual {v0, p1}, LX/B0M;->a(Ljava/lang/Throwable;)V

    .line 1734000
    invoke-virtual {p0}, LX/B0Y;->a()V

    .line 1734001
    return-void
.end method
