.class public final enum LX/CQC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CQC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CQC;

.field public static final enum nearby_places:LX/CQC;

.field public static final enum open:LX/CQC;

.field public static final enum tab_bar_tap:LX/CQC;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1885440
    new-instance v0, LX/CQC;

    const-string v1, "open"

    invoke-direct {v0, v1, v2}, LX/CQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CQC;->open:LX/CQC;

    .line 1885441
    new-instance v0, LX/CQC;

    const-string v1, "tab_bar_tap"

    invoke-direct {v0, v1, v3}, LX/CQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CQC;->tab_bar_tap:LX/CQC;

    .line 1885442
    new-instance v0, LX/CQC;

    const-string v1, "nearby_places"

    invoke-direct {v0, v1, v4}, LX/CQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CQC;->nearby_places:LX/CQC;

    .line 1885443
    const/4 v0, 0x3

    new-array v0, v0, [LX/CQC;

    sget-object v1, LX/CQC;->open:LX/CQC;

    aput-object v1, v0, v2

    sget-object v1, LX/CQC;->tab_bar_tap:LX/CQC;

    aput-object v1, v0, v3

    sget-object v1, LX/CQC;->nearby_places:LX/CQC;

    aput-object v1, v0, v4

    sput-object v0, LX/CQC;->$VALUES:[LX/CQC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1885444
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CQC;
    .locals 1

    .prologue
    .line 1885445
    const-class v0, LX/CQC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CQC;

    return-object v0
.end method

.method public static values()[LX/CQC;
    .locals 1

    .prologue
    .line 1885446
    sget-object v0, LX/CQC;->$VALUES:[LX/CQC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CQC;

    return-object v0
.end method
