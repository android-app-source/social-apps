.class public LX/Aab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AVT;


# direct methods
.method public constructor <init>(LX/AVT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688741
    iput-object p1, p0, LX/Aab;->a:LX/AVT;

    .line 1688742
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1688743
    check-cast p1, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;

    .line 1688744
    iget-boolean v0, p1, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "BROADCASTER_CLAIMED_RIGHTS"

    .line 1688745
    :goto_0
    iget-object v1, p0, LX/Aab;->a:LX/AVT;

    invoke-virtual {v1, v0}, LX/AVT;->c(Ljava/lang/String;)V

    .line 1688746
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "graphCopyrightViolation"

    .line 1688747
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1688748
    move-object v1, v1

    .line 1688749
    const-string v2, "POST"

    .line 1688750
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1688751
    move-object v1, v1

    .line 1688752
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1688753
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1688754
    move-object v1, v1

    .line 1688755
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "copyrights_violation_dialog_state"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1688756
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1688757
    move-object v0, v1

    .line 1688758
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1688759
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1688760
    move-object v0, v0

    .line 1688761
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1688762
    :cond_0
    const-string v0, "BROADCAST_WAS_STOPPED"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1688763
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->j()V
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1688764
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1688765
    :catch_0
    move-exception v0

    .line 1688766
    iget-object v1, p0, LX/Aab;->a:LX/AVT;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "answer_error\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/2Oo;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AVT;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
