.class public LX/BXg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I


# direct methods
.method public constructor <init>(IIIIIILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1793564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1793565
    add-int v0, p3, p4

    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p5

    add-int/2addr v0, v1

    .line 1793566
    iput p1, p0, LX/BXg;->a:I

    .line 1793567
    iput p2, p0, LX/BXg;->b:I

    .line 1793568
    iput p5, p0, LX/BXg;->e:I

    .line 1793569
    invoke-direct {p0, p7, v0}, LX/BXg;->a(Ljava/util/List;I)I

    move-result v1

    iput v1, p0, LX/BXg;->f:I

    .line 1793570
    invoke-direct {p0, p7, v0}, LX/BXg;->b(Ljava/util/List;I)I

    move-result v0

    .line 1793571
    iget v1, p0, LX/BXg;->b:I

    if-ge v0, v1, :cond_0

    .line 1793572
    iget v1, p0, LX/BXg;->b:I

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p3

    iput v1, p0, LX/BXg;->c:I

    .line 1793573
    iget v1, p0, LX/BXg;->b:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p4

    iput v0, p0, LX/BXg;->d:I

    .line 1793574
    iget v0, p0, LX/BXg;->b:I

    iput v0, p0, LX/BXg;->g:I

    .line 1793575
    :goto_0
    invoke-direct {p0}, LX/BXg;->a()I

    move-result v0

    iput v0, p0, LX/BXg;->h:I

    .line 1793576
    invoke-direct {p0, p7, p6}, LX/BXg;->c(Ljava/util/List;I)I

    move-result v0

    iput v0, p0, LX/BXg;->i:I

    .line 1793577
    return-void

    .line 1793578
    :cond_0
    iput p3, p0, LX/BXg;->c:I

    .line 1793579
    iput p4, p0, LX/BXg;->d:I

    .line 1793580
    iput v0, p0, LX/BXg;->g:I

    goto :goto_0
.end method

.method private a()I
    .locals 2

    .prologue
    .line 1793581
    iget v0, p0, LX/BXg;->g:I

    iget v1, p0, LX/BXg;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private a(Ljava/util/List;I)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 1793582
    iget v0, p0, LX/BXg;->a:I

    int-to-double v0, v0

    const-wide v2, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v0, v2

    double-to-int v4, v0

    .line 1793583
    iget v0, p0, LX/BXg;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v0, v2

    double-to-int v5, v0

    .line 1793584
    iget v0, p0, LX/BXg;->b:I

    if-lt p2, v0, :cond_0

    move v0, v4

    .line 1793585
    :goto_0
    return v0

    .line 1793586
    :cond_0
    const-wide/16 v2, 0x0

    .line 1793587
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1793588
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v6, v0

    float-to-double v6, v0

    add-double/2addr v2, v6

    .line 1793589
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1793590
    :cond_1
    iget v0, p0, LX/BXg;->b:I

    sub-int/2addr v0, p2

    .line 1793591
    int-to-double v0, v0

    div-double/2addr v0, v2

    int-to-double v2, v4

    int-to-double v4, v5

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method private b(Ljava/util/List;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 1793592
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1793593
    int-to-float v2, p2

    iget v0, p0, LX/BXg;->f:I

    int-to-float v3, v0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v3, v0

    add-float/2addr v0, v2

    float-to-int p2, v0

    .line 1793594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1793595
    :cond_0
    return p2
.end method

.method private c(Ljava/util/List;I)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1793596
    iget v0, p0, LX/BXg;->c:I

    move v1, v2

    move v3, v0

    .line 1793597
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1793598
    iget v0, p0, LX/BXg;->f:I

    int-to-float v4, v0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v4, v0

    float-to-int v0, v0

    iget v4, p0, LX/BXg;->e:I

    add-int/2addr v0, v4

    .line 1793599
    if-ne p2, v1, :cond_1

    .line 1793600
    iget v1, p0, LX/BXg;->b:I

    if-le v1, v0, :cond_0

    .line 1793601
    iget v1, p0, LX/BXg;->b:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    .line 1793602
    :cond_0
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, LX/BXg;->h:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    .line 1793603
    :cond_1
    add-int/2addr v3, v0

    .line 1793604
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
