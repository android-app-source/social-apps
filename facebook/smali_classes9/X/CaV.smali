.class public LX/CaV;
.super LX/8wv;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final A:Ljava/lang/Runnable;

.field public B:Z

.field public C:Z

.field public D:LX/7US;

.field private final E:I

.field private F:LX/Cab;

.field private G:LX/3IC;

.field private H:Landroid/view/GestureDetector;

.field private final y:I

.field public final z:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1917786
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CaV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1917784
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CaV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1917785
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1917775
    invoke-direct {p0, p1, p2, p3}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1917776
    const/16 v0, 0xa2

    iput v0, p0, LX/CaV;->y:I

    .line 1917777
    const/16 v0, 0x3e8

    iput v0, p0, LX/CaV;->E:I

    .line 1917778
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/CaV;->z:Landroid/os/Handler;

    .line 1917779
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360View$1;

    invoke-direct {v0, p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360View$1;-><init>(LX/CaV;)V

    iput-object v0, p0, LX/CaV;->A:Ljava/lang/Runnable;

    .line 1917780
    iput-boolean v1, p0, LX/CaV;->B:Z

    .line 1917781
    iput-boolean v1, p0, LX/CaV;->C:Z

    .line 1917782
    invoke-virtual {p0}, LX/CaV;->b()V

    .line 1917783
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1917770
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-eqz v0, :cond_0

    .line 1917771
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    if-eqz v0, :cond_0

    .line 1917772
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1917773
    iget-object v1, p0, LX/CaV;->D:LX/7US;

    invoke-interface {v1, v0, v0}, LX/7US;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 1917774
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1917764
    new-instance v0, LX/Cab;

    invoke-direct {v0, p0}, LX/Cab;-><init>(LX/CaV;)V

    iput-object v0, p0, LX/CaV;->F:LX/Cab;

    .line 1917765
    new-instance v0, LX/3IC;

    invoke-virtual {p0}, LX/CaV;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/CaV;->F:LX/Cab;

    iget-object v3, p0, LX/CaV;->F:LX/Cab;

    invoke-direct {v0, v1, v2, v3}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/CaV;->G:LX/3IC;

    .line 1917766
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/CaV;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Cac;

    invoke-direct {v2, p0}, LX/Cac;-><init>(LX/CaV;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/CaV;->H:Landroid/view/GestureDetector;

    .line 1917767
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1917768
    iput-boolean v4, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1917769
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1917755
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CaV;->B:Z

    if-nez v0, :cond_0

    .line 1917756
    invoke-virtual {p0}, LX/CaV;->t()V

    .line 1917757
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CaV;->w:Z

    .line 1917758
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1917759
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_1

    .line 1917760
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b()V

    .line 1917761
    :cond_1
    invoke-virtual {p0}, LX/8wv;->g()V

    .line 1917762
    invoke-virtual {p0}, LX/8wv;->p()V

    .line 1917763
    return-void
.end method

.method public getIndicatorBottomMargin()I
    .locals 1

    .prologue
    .line 1917754
    const/16 v0, 0xa2

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xb5205f6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1917744
    iget-boolean v2, p0, LX/CaV;->C:Z

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 1917745
    :cond_0
    invoke-virtual {p0}, LX/CaV;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1917746
    :cond_1
    iget-object v2, p0, LX/CaV;->H:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1917747
    const v2, 0x3cfe74a8

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1917748
    :goto_0
    return v0

    .line 1917749
    :cond_2
    iget-boolean v2, p0, LX/CaV;->C:Z

    if-eqz v2, :cond_3

    .line 1917750
    iget-object v2, p0, LX/CaV;->G:LX/3IC;

    invoke-virtual {v2, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    .line 1917751
    iget-object v2, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v2}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d()V

    .line 1917752
    const v2, -0x51d41761

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1917753
    :cond_3
    const/4 v0, 0x0

    const v2, 0x614f600f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public q()V
    .locals 1

    .prologue
    .line 1917719
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CaV;->w:Z

    .line 1917720
    invoke-virtual {p0}, LX/8wv;->n()V

    .line 1917721
    return-void
.end method

.method public r()V
    .locals 5

    .prologue
    const/16 v1, 0x8

    .line 1917736
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CaV;->B:Z

    .line 1917737
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-nez v0, :cond_0

    .line 1917738
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d()V

    .line 1917739
    iget-object v0, p0, LX/CaV;->z:Landroid/os/Handler;

    iget-object v1, p0, LX/CaV;->A:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0x5083df2c

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1917740
    :goto_0
    return-void

    .line 1917741
    :cond_0
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1917742
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setVisibility(I)V

    .line 1917743
    invoke-virtual {p0}, LX/8wv;->l()V

    goto :goto_0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 1917732
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CaV;->C:Z

    .line 1917733
    iget-object v0, p0, LX/CaV;->z:Landroid/os/Handler;

    iget-object v1, p0, LX/CaV;->A:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1917734
    invoke-virtual {p0}, LX/8wv;->m()V

    .line 1917735
    return-void
.end method

.method public setListener(LX/7US;)V
    .locals 0
    .param p1    # LX/7US;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1917730
    iput-object p1, p0, LX/CaV;->D:LX/7US;

    .line 1917731
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1917724
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g()V

    .line 1917725
    iput-boolean v1, p0, LX/CaV;->C:Z

    .line 1917726
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, v1}, LX/2qW;->setTiltInteractionEnabled(Z)V

    .line 1917727
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    if-eqz v0, :cond_0

    .line 1917728
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    invoke-interface {v0}, LX/7US;->f()V

    .line 1917729
    :cond_0
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 1917722
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CaV;->B:Z

    .line 1917723
    return-void
.end method
