.class public LX/C7c;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7d;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7c",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7d;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851492
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851493
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C7c;->b:LX/0Zi;

    .line 1851494
    iput-object p1, p0, LX/C7c;->a:LX/0Ot;

    .line 1851495
    return-void
.end method

.method public static a(LX/0QB;)LX/C7c;
    .locals 4

    .prologue
    .line 1851496
    const-class v1, LX/C7c;

    monitor-enter v1

    .line 1851497
    :try_start_0
    sget-object v0, LX/C7c;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851498
    sput-object v2, LX/C7c;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851499
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851500
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851501
    new-instance v3, LX/C7c;

    const/16 p0, 0x1f99

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7c;-><init>(LX/0Ot;)V

    .line 1851502
    move-object v0, v3

    .line 1851503
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851504
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851505
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1851507
    check-cast p2, LX/C7b;

    .line 1851508
    iget-object v0, p0, LX/C7c;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7d;

    iget-object v1, p2, LX/C7b;->a:LX/1Pn;

    iget-object v2, p2, LX/C7b;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    .line 1851509
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1851510
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851511
    invoke-static {v3}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    .line 1851512
    iget-object v5, v0, LX/C7d;->b:LX/1g8;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v4

    :goto_0
    const-string p2, "gmw_unit_impression"

    invoke-virtual {v5, v4, p2}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851513
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v3, v0, LX/C7d;->d:LX/1wB;

    invoke-virtual {v3, p1}, LX/1wB;->c(LX/1De;)LX/22A;

    move-result-object v5

    move-object v3, v1

    check-cast v3, LX/1Pq;

    invoke-virtual {v5, v3}, LX/22A;->a(LX/1Pq;)LX/22A;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/22A;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22A;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b1d52

    invoke-interface {v3, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v5, 0x3

    const/4 p0, 0x4

    invoke-interface {v3, v5, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/C7d;->c:LX/C7m;

    const/4 v5, 0x0

    .line 1851514
    new-instance p0, LX/C7l;

    invoke-direct {p0, v4}, LX/C7l;-><init>(LX/C7m;)V

    .line 1851515
    iget-object p2, v4, LX/C7m;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C7k;

    .line 1851516
    if-nez p2, :cond_0

    .line 1851517
    new-instance p2, LX/C7k;

    invoke-direct {p2, v4}, LX/C7k;-><init>(LX/C7m;)V

    .line 1851518
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/C7k;->a$redex0(LX/C7k;LX/1De;IILX/C7l;)V

    .line 1851519
    move-object p0, p2

    .line 1851520
    move-object v5, p0

    .line 1851521
    move-object v4, v5

    .line 1851522
    iget-object v5, v4, LX/C7k;->a:LX/C7l;

    iput-object v1, v5, LX/C7l;->a:LX/1Pn;

    .line 1851523
    iget-object v5, v4, LX/C7k;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1851524
    move-object v4, v4

    .line 1851525
    iget-object v5, v4, LX/C7k;->a:LX/C7l;

    iput-object v2, v5, LX/C7l;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851526
    iget-object v5, v4, LX/C7k;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1851527
    move-object v4, v4

    .line 1851528
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    const/4 p2, 0x1

    .line 1851529
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b1d6a

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    .line 1851530
    const v5, 0x5ee847a4

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1851531
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const p0, 0x7f082a05

    invoke-virtual {v5, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b0050

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a0443

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object p0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, p0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v4, v5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    move-object v4, v4

    .line 1851532
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1851533
    return-object v0

    .line 1851534
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1851535
    invoke-static {}, LX/1dS;->b()V

    .line 1851536
    iget v0, p1, LX/1dQ;->b:I

    .line 1851537
    packed-switch v0, :pswitch_data_0

    .line 1851538
    :goto_0
    return-object v2

    .line 1851539
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1851540
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1851541
    check-cast v1, LX/C7b;

    .line 1851542
    iget-object v3, p0, LX/C7c;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C7d;

    iget-object v4, v1, LX/C7b;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851543
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1851544
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object p2

    .line 1851545
    iget-object p1, v3, LX/C7d;->b:LX/1g8;

    const-string p0, "gmw_unit_footer_click"

    invoke-virtual {p1, p2, p0}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851546
    iget-object p1, v3, LX/C7d;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p0, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1851547
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5ee847a4
        :pswitch_0
    .end packed-switch
.end method
