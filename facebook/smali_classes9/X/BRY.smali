.class public final LX/BRY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/BRg;


# direct methods
.method public constructor <init>(LX/BRg;)V
    .locals 0

    .prologue
    .line 1784165
    iput-object p1, p0, LX/BRY;->a:LX/BRg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v2, 0x6

    const/4 v3, 0x1

    .line 1784166
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1784167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unimplemented Context Menu Entry!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784168
    :pswitch_0
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->q:LX/BRi;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, LX/BRi;->a(II)V

    .line 1784169
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->n:Lcom/facebook/fig/button/FigButton;

    iget-object v2, p0, LX/BRY;->a:LX/BRg;

    iget-object v2, v2, LX/BRg;->q:LX/BRi;

    invoke-static {v0, v1, v2}, LX/BRg;->a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V

    .line 1784170
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    if-eqz v0, :cond_0

    .line 1784171
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->q:LX/BRi;

    invoke-virtual {v1}, LX/BRi;->d()J

    invoke-virtual {v0}, LX/BQt;->a()V

    .line 1784172
    :cond_0
    :goto_0
    return v3

    .line 1784173
    :pswitch_1
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->q:LX/BRi;

    invoke-virtual {v0, v2, v3}, LX/BRi;->a(II)V

    .line 1784174
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->n:Lcom/facebook/fig/button/FigButton;

    iget-object v2, p0, LX/BRY;->a:LX/BRg;

    iget-object v2, v2, LX/BRg;->q:LX/BRi;

    invoke-static {v0, v1, v2}, LX/BRg;->a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V

    .line 1784175
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    if-eqz v0, :cond_0

    .line 1784176
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->q:LX/BRi;

    invoke-virtual {v1}, LX/BRi;->d()J

    invoke-virtual {v0}, LX/BQt;->a()V

    goto :goto_0

    .line 1784177
    :pswitch_2
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->q:LX/BRi;

    const/4 v1, 0x7

    invoke-virtual {v0, v2, v1}, LX/BRi;->a(II)V

    .line 1784178
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->n:Lcom/facebook/fig/button/FigButton;

    iget-object v2, p0, LX/BRY;->a:LX/BRg;

    iget-object v2, v2, LX/BRg;->q:LX/BRi;

    invoke-static {v0, v1, v2}, LX/BRg;->a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V

    .line 1784179
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    if-eqz v0, :cond_0

    .line 1784180
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->q:LX/BRi;

    invoke-virtual {v1}, LX/BRi;->d()J

    invoke-virtual {v0}, LX/BQt;->a()V

    goto :goto_0

    .line 1784181
    :pswitch_3
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    invoke-static {v0}, LX/BRg;->d$redex0(LX/BRg;)V

    .line 1784182
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    invoke-static {v0}, LX/BRg;->c(LX/BRg;)V

    .line 1784183
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->h:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0

    .line 1784184
    :pswitch_4
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->q:LX/BRi;

    invoke-virtual {v0}, LX/BRi;->g()V

    .line 1784185
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->n:Lcom/facebook/fig/button/FigButton;

    iget-object v2, p0, LX/BRY;->a:LX/BRg;

    iget-object v2, v2, LX/BRg;->q:LX/BRi;

    invoke-static {v0, v1, v2}, LX/BRg;->a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V

    .line 1784186
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    if-eqz v0, :cond_0

    .line 1784187
    iget-object v0, p0, LX/BRY;->a:LX/BRg;

    iget-object v0, v0, LX/BRg;->o:LX/BQt;

    iget-object v1, p0, LX/BRY;->a:LX/BRg;

    iget-object v1, v1, LX/BRg;->q:LX/BRi;

    invoke-virtual {v1}, LX/BRi;->d()J

    invoke-virtual {v0}, LX/BQt;->a()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
