.class public final LX/B3N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

.field public b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V
    .locals 1

    .prologue
    .line 1740204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740205
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, LX/B3N;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1740206
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    iput-object v0, p0, LX/B3N;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1740207
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    iput-object v0, p0, LX/B3N;->c:Ljava/lang/String;

    .line 1740208
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->d:Ljava/lang/String;

    iput-object v0, p0, LX/B3N;->d:Ljava/lang/String;

    .line 1740209
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, LX/B3N;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1740210
    iget-object v0, p1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    iput-object v0, p0, LX/B3N;->f:Ljava/lang/String;

    .line 1740211
    return-void
.end method

.method public constructor <init>(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V
    .locals 0

    .prologue
    .line 1740212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740213
    iput-object p1, p0, LX/B3N;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1740214
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;
    .locals 2

    .prologue
    .line 1740215
    new-instance v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    invoke-direct {v0, p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;-><init>(LX/B3N;)V

    return-object v0
.end method
