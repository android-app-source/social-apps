.class public final LX/BUL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:LX/2ft;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;)V
    .locals 11

    .prologue
    .line 1788942
    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;Z)V

    .line 1788943
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;Z)V
    .locals 1

    .prologue
    .line 1788944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788945
    iput-object p1, p0, LX/BUL;->a:Landroid/net/Uri;

    .line 1788946
    iput-object p2, p0, LX/BUL;->b:Ljava/lang/String;

    .line 1788947
    iput-object p3, p0, LX/BUL;->c:Ljava/lang/String;

    .line 1788948
    iput-object p4, p0, LX/BUL;->d:Ljava/lang/String;

    .line 1788949
    iput-wide p5, p0, LX/BUL;->e:J

    .line 1788950
    iput-object p7, p0, LX/BUL;->f:LX/2ft;

    .line 1788951
    iput-boolean p8, p0, LX/BUL;->n:Z

    .line 1788952
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1788953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788954
    iput-object p1, p0, LX/BUL;->a:Landroid/net/Uri;

    .line 1788955
    iput-object p2, p0, LX/BUL;->b:Ljava/lang/String;

    .line 1788956
    iput-object p3, p0, LX/BUL;->c:Ljava/lang/String;

    .line 1788957
    iput-object p4, p0, LX/BUL;->d:Ljava/lang/String;

    .line 1788958
    iput-wide p5, p0, LX/BUL;->e:J

    .line 1788959
    iput-object p7, p0, LX/BUL;->g:Ljava/lang/String;

    .line 1788960
    iput-object p8, p0, LX/BUL;->h:Ljava/lang/String;

    .line 1788961
    iput-object p9, p0, LX/BUL;->i:Ljava/lang/String;

    .line 1788962
    iput-object p10, p0, LX/BUL;->j:Ljava/lang/String;

    .line 1788963
    iput-object p11, p0, LX/BUL;->k:Ljava/lang/String;

    .line 1788964
    iput-object p12, p0, LX/BUL;->l:Ljava/lang/String;

    .line 1788965
    iput-object p13, p0, LX/BUL;->m:Ljava/lang/String;

    .line 1788966
    sget-object v0, LX/2ft;->NONE:LX/2ft;

    iput-object v0, p0, LX/BUL;->f:LX/2ft;

    .line 1788967
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ft;)V
    .locals 1

    .prologue
    .line 1788968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788969
    iput-object p1, p0, LX/BUL;->a:Landroid/net/Uri;

    .line 1788970
    iput-object p2, p0, LX/BUL;->b:Ljava/lang/String;

    .line 1788971
    iput-object p3, p0, LX/BUL;->c:Ljava/lang/String;

    .line 1788972
    iput-object p4, p0, LX/BUL;->d:Ljava/lang/String;

    .line 1788973
    iput-wide p5, p0, LX/BUL;->e:J

    .line 1788974
    iput-object p7, p0, LX/BUL;->g:Ljava/lang/String;

    .line 1788975
    iput-object p8, p0, LX/BUL;->h:Ljava/lang/String;

    .line 1788976
    iput-object p9, p0, LX/BUL;->i:Ljava/lang/String;

    .line 1788977
    iput-object p10, p0, LX/BUL;->j:Ljava/lang/String;

    .line 1788978
    iput-object p11, p0, LX/BUL;->k:Ljava/lang/String;

    .line 1788979
    iput-object p12, p0, LX/BUL;->l:Ljava/lang/String;

    .line 1788980
    iput-object p13, p0, LX/BUL;->m:Ljava/lang/String;

    .line 1788981
    iput-object p14, p0, LX/BUL;->f:LX/2ft;

    .line 1788982
    return-void
.end method
