.class public final LX/CIH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    .line 1873344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1873345
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1873346
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1873347
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1873348
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1873349
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1873350
    :goto_1
    move v1, v2

    .line 1873351
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1873352
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1873353
    :cond_1
    const-string v8, "is_enabled"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1873354
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 1873355
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1873356
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1873357
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1873358
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_2

    if-eqz v7, :cond_2

    .line 1873359
    const-string v8, "action"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1873360
    invoke-static {p0, p1}, LX/CIE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_2

    .line 1873361
    :cond_3
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1873362
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 1873363
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1873364
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1873365
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1873366
    if-eqz v1, :cond_6

    .line 1873367
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 1873368
    :cond_6
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1873369
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1873324
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1873325
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1873326
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 1873327
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1873328
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1873329
    if-eqz v2, :cond_0

    .line 1873330
    const-string v3, "action"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873331
    invoke-static {p0, v2, p2}, LX/CIE;->a(LX/15i;ILX/0nX;)V

    .line 1873332
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1873333
    if-eqz v2, :cond_1

    .line 1873334
    const-string v3, "is_enabled"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873335
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 1873336
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1873337
    if-eqz v2, :cond_2

    .line 1873338
    const-string v3, "name"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873339
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1873340
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1873341
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1873342
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1873343
    return-void
.end method
