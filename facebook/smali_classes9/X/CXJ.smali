.class public final enum LX/CXJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CXJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CXJ;

.field public static final enum OTHER_FEE:LX/CXJ;

.field public static final enum TOTAL_FEE:LX/CXJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1909854
    new-instance v0, LX/CXJ;

    const-string v1, "OTHER_FEE"

    invoke-direct {v0, v1, v2}, LX/CXJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CXJ;->OTHER_FEE:LX/CXJ;

    .line 1909855
    new-instance v0, LX/CXJ;

    const-string v1, "TOTAL_FEE"

    invoke-direct {v0, v1, v3}, LX/CXJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CXJ;->TOTAL_FEE:LX/CXJ;

    .line 1909856
    const/4 v0, 0x2

    new-array v0, v0, [LX/CXJ;

    sget-object v1, LX/CXJ;->OTHER_FEE:LX/CXJ;

    aput-object v1, v0, v2

    sget-object v1, LX/CXJ;->TOTAL_FEE:LX/CXJ;

    aput-object v1, v0, v3

    sput-object v0, LX/CXJ;->$VALUES:[LX/CXJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1909857
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CXJ;
    .locals 1

    .prologue
    .line 1909858
    const-class v0, LX/CXJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CXJ;

    return-object v0
.end method

.method public static values()[LX/CXJ;
    .locals 1

    .prologue
    .line 1909859
    sget-object v0, LX/CXJ;->$VALUES:[LX/CXJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CXJ;

    return-object v0
.end method
