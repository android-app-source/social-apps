.class public LX/BOg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field public a:LX/1Ns;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1779978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779979
    return-void
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 4
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1779985
    iget-object v0, p0, LX/BOg;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1kp;->j:LX/0Tn;

    iget-object v2, p0, LX/BOg;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1779986
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1779981
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1779982
    iget-object v1, p0, LX/BOg;->a:LX/1Ns;

    new-instance v2, LX/1Qg;

    invoke-direct {v2}, LX/1Qg;-><init>()V

    new-instance p1, LX/AnN;

    invoke-direct {p1}, LX/AnN;-><init>()V

    invoke-virtual {v1, v2, p1}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v1

    move-object v1, v1

    .line 1779983
    sget-object v2, LX/B68;->STORYLINE_PROMPT:LX/B68;

    invoke-virtual {v1, v0, v2}, LX/1aw;->a(Landroid/app/Activity;LX/B68;)V

    .line 1779984
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1779980
    invoke-static {p1}, LX/1RT;->a(LX/1RN;)Z

    move-result v0

    return v0
.end method
