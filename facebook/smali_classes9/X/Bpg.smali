.class public LX/Bpg;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Pf;
.implements LX/Boz;
.implements LX/Bp2;
.implements LX/Bp4;
.implements LX/Bp6;


# instance fields
.field private final n:LX/1PT;

.field private final o:LX/Bp0;

.field private final p:LX/Bp3;

.field private final q:LX/Bp5;

.field private final r:LX/1QW;

.field private final s:LX/Bp7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/Bp0;Ljava/lang/Runnable;Ljava/util/concurrent/Callable;LX/1QW;LX/3iG;LX/1EN;LX/03V;LX/20h;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Bp0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Callable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1QW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1PT;",
            "LX/Bp0;",
            "Ljava/lang/Runnable;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "LX/1QW;",
            "LX/3iG;",
            "LX/1EN;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/20h;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823471
    sget-object v0, LX/1PU;->b:LX/1PY;

    invoke-direct {p0, p1, p4, v0}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 1823472
    iput-object p2, p0, LX/Bpg;->n:LX/1PT;

    .line 1823473
    iput-object p3, p0, LX/Bpg;->o:LX/Bp0;

    .line 1823474
    new-instance v0, LX/Bp3;

    iget-object v1, p0, LX/Bpg;->n:LX/1PT;

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-direct {v0, p8, p5, p9, v1}, LX/Bp3;-><init>(LX/1EN;Ljava/util/concurrent/Callable;LX/03V;LX/1Qt;)V

    iput-object v0, p0, LX/Bpg;->p:LX/Bp3;

    .line 1823475
    new-instance v0, LX/Bp5;

    invoke-direct {v0, p7, p5, p9}, LX/Bp5;-><init>(LX/3iG;Ljava/util/concurrent/Callable;LX/03V;)V

    iput-object v0, p0, LX/Bpg;->q:LX/Bp5;

    .line 1823476
    iput-object p6, p0, LX/Bpg;->r:LX/1QW;

    .line 1823477
    new-instance v0, LX/Bp7;

    invoke-direct {v0, p10, p5, p9}, LX/Bp7;-><init>(LX/20h;Ljava/util/concurrent/Callable;LX/03V;)V

    iput-object v0, p0, LX/Bpg;->s:LX/Bp7;

    .line 1823478
    return-void
.end method


# virtual methods
.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 1823469
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0, p1}, LX/1QW;->a(LX/1Rb;)V

    .line 1823470
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1823467
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0, p1, p2}, LX/1QW;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1823468
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1823465
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0, p1, p2, p3}, LX/1QW;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1823466
    return-void
.end method

.method public final a(LX/5kD;LX/1bf;ZI)V
    .locals 1

    .prologue
    .line 1823463
    iget-object v0, p0, LX/Bpg;->o:LX/Bp0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/Bp0;->a(LX/5kD;LX/1bf;ZI)V

    .line 1823464
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 1823461
    iget-object v0, p0, LX/Bpg;->q:LX/Bp5;

    invoke-virtual {v0, p1}, LX/Bp5;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1823462
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;LX/0Ve;)V
    .locals 1

    .prologue
    .line 1823479
    iget-object v0, p0, LX/Bpg;->s:LX/Bp7;

    invoke-virtual {v0, p1, p2, p3}, LX/Bp7;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;LX/0Ve;)V

    .line 1823480
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V
    .locals 1

    .prologue
    .line 1823459
    iget-object v0, p0, LX/Bpg;->p:LX/Bp3;

    invoke-virtual {v0, p1, p2, p3}, LX/Bp3;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V

    .line 1823460
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 1823458
    iget-object v0, p0, LX/Bpg;->n:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 1823457
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 1823456
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 1823452
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1823454
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->p()V

    .line 1823455
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1823453
    iget-object v0, p0, LX/Bpg;->r:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->q()Z

    move-result v0

    return v0
.end method
