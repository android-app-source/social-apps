.class public LX/C2P;
.super LX/3WJ;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1844338
    invoke-direct {p0, p1}, LX/3WJ;-><init>(Landroid/content/Context;)V

    .line 1844339
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/C2P;->setBackgroundColor(I)V

    .line 1844340
    const v0, 0x7f0d0390

    invoke-virtual {p0, v0}, LX/C2P;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1844341
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1844342
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1844343
    invoke-virtual {p0, v0, v0, v0, v0}, LX/C2P;->setPadding(IIII)V

    .line 1844344
    return-void
.end method
