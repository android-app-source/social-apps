.class public LX/BHh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

.field private b:Landroid/view/View;

.field public c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

.field public d:Landroid/view/View;

.field private e:LX/BH0;

.field public f:Landroid/animation/AnimatorSet;

.field public g:Landroid/animation/AnimatorSet;

.field private h:Landroid/animation/AnimatorSet;

.field public i:Z


# direct methods
.method public constructor <init>(Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;Landroid/view/View;Landroid/view/View;LX/BH0;)V
    .locals 1

    .prologue
    .line 1769498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769499
    iput-object p1, p0, LX/BHh;->a:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1769500
    iput-object p2, p0, LX/BHh;->b:Landroid/view/View;

    .line 1769501
    iput-object p3, p0, LX/BHh;->d:Landroid/view/View;

    .line 1769502
    iput-object p4, p0, LX/BHh;->e:LX/BH0;

    .line 1769503
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BHh;->i:Z

    .line 1769504
    return-void
.end method

.method public static b(LX/BHh;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1769505
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769506
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1769507
    :cond_0
    iput-boolean v3, p0, LX/BHh;->i:Z

    .line 1769508
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    if-nez v0, :cond_4

    .line 1769509
    invoke-static {p0}, LX/BHh;->d(LX/BHh;)V

    .line 1769510
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    .line 1769511
    iget-object v1, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1769512
    :cond_2
    iget-object v0, p0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_3

    .line 1769513
    invoke-static {p0}, LX/BHh;->f(LX/BHh;)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    .line 1769514
    :cond_3
    iget-object v0, p0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    iput-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    .line 1769515
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1769516
    :goto_0
    return-void

    .line 1769517
    :cond_4
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0
.end method

.method public static d(LX/BHh;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    .line 1769518
    iget-object v0, p0, LX/BHh;->b:Landroid/view/View;

    const v1, 0x7f0d2c7b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1769519
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    iput-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    .line 1769520
    iget-object v0, p0, LX/BHh;->b:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769521
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    iget-object v1, p0, LX/BHh;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, LX/BHh;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->measure(II)V

    .line 1769522
    :goto_0
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    iget-object v1, p0, LX/BHh;->a:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1769523
    iget-boolean v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    move v1, v2

    .line 1769524
    iget-object v2, p0, LX/BHh;->e:LX/BH0;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(ZLX/BH0;)V

    .line 1769525
    return-void

    .line 1769526
    :cond_0
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v0, v2, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->measure(II)V

    goto :goto_0
.end method

.method public static e(LX/BHh;)V
    .locals 11

    .prologue
    .line 1769527
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769528
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1769529
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BHh;->i:Z

    .line 1769530
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 1769531
    :cond_1
    :goto_0
    return-void

    .line 1769532
    :cond_2
    iget-object v0, p0, LX/BHh;->h:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_3

    .line 1769533
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1769534
    iget-object v2, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getMeasuredHeight()I

    move-result v2

    .line 1769535
    iget-object v3, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const-string v4, "translationY"

    new-array v5, v9, [F

    aput v10, v5, v7

    neg-int v6, v2

    int-to-float v6, v6

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1769536
    iget-object v4, p0, LX/BHh;->d:Landroid/view/View;

    const-string v5, "translationY"

    new-array v6, v9, [F

    int-to-float v2, v2

    aput v2, v6, v7

    aput v10, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1769537
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1769538
    new-array v5, v9, [Landroid/animation/Animator;

    aput-object v3, v5, v7

    aput-object v2, v5, v8

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1769539
    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1769540
    const-wide/16 v2, 0x190

    invoke-virtual {v4, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1769541
    new-instance v2, LX/BHg;

    invoke-direct {v2, p0}, LX/BHg;-><init>(LX/BHh;)V

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1769542
    move-object v0, v4

    .line 1769543
    iput-object v0, p0, LX/BHh;->h:Landroid/animation/AnimatorSet;

    .line 1769544
    :cond_3
    iget-object v0, p0, LX/BHh;->h:Landroid/animation/AnimatorSet;

    iput-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    .line 1769545
    iget-object v0, p0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public static f(LX/BHh;)Landroid/animation/AnimatorSet;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1769546
    iget-object v0, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getMeasuredHeight()I

    move-result v0

    .line 1769547
    iget-object v1, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->setTranslationY(F)V

    .line 1769548
    iget-object v1, p0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const-string v2, "translationY"

    new-array v3, v7, [F

    neg-int v4, v0

    int-to-float v4, v4

    aput v4, v3, v5

    aput v8, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1769549
    iget-object v2, p0, LX/BHh;->d:Landroid/view/View;

    const-string v3, "translationY"

    new-array v4, v7, [F

    aput v8, v4, v5

    int-to-float v0, v0

    aput v0, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1769550
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1769551
    new-array v3, v7, [Landroid/animation/Animator;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1769552
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1769553
    const-wide/16 v0, 0x190

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1769554
    new-instance v0, LX/BHf;

    invoke-direct {v0, p0}, LX/BHf;-><init>(LX/BHh;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1769555
    return-object v2
.end method
