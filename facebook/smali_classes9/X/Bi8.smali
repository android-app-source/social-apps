.class public LX/Bi8;
.super LX/1Cv;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:LX/BiM;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryInterfaces$ContextItemsConnectionFragment$Edges;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/Bi3;

.field private g:LX/Bi6;

.field public h:Z

.field public i:Ljava/lang/String;

.field private j:[LX/Bi7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1809965
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Bi8;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/BiM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1809953
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1809954
    iput-object p1, p0, LX/Bi8;->b:Landroid/content/Context;

    .line 1809955
    iput-object p2, p0, LX/Bi8;->c:Landroid/view/LayoutInflater;

    .line 1809956
    iput-object p3, p0, LX/Bi8;->d:LX/BiM;

    .line 1809957
    return-void
.end method

.method public static b(LX/0QB;)LX/Bi8;
    .locals 4

    .prologue
    .line 1809958
    new-instance v3, LX/Bi8;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/BiM;->b(LX/0QB;)LX/BiM;

    move-result-object v2

    check-cast v2, LX/BiM;

    invoke-direct {v3, v0, v1, v2}, LX/Bi8;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/BiM;)V

    .line 1809959
    return-object v3
.end method

.method private d()[LX/Bi7;
    .locals 1

    .prologue
    .line 1809918
    iget-object v0, p0, LX/Bi8;->j:[LX/Bi7;

    if-nez v0, :cond_0

    .line 1809919
    invoke-static {}, LX/Bi7;->values()[LX/Bi7;

    move-result-object v0

    iput-object v0, p0, LX/Bi8;->j:[LX/Bi7;

    .line 1809920
    :cond_0
    iget-object v0, p0, LX/Bi8;->j:[LX/Bi7;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1809960
    invoke-direct {p0}, LX/Bi8;->d()[LX/Bi7;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1809961
    sget-object v1, LX/Bi5;->a:[I

    invoke-virtual {v0}, LX/Bi7;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1809962
    iget-object v0, p0, LX/Bi8;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030383

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1809963
    :pswitch_0
    new-instance v0, LX/BiF;

    iget-object v1, p0, LX/Bi8;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/BiF;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1809964
    :pswitch_1
    iget-object v0, p0, LX/Bi8;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f031144

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1809936
    sget-object v0, LX/Bi8;->a:Ljava/lang/Object;

    if-ne p2, v0, :cond_0

    instance-of v0, p3, LX/BiF;

    if-eqz v0, :cond_0

    .line 1809937
    check-cast p3, LX/BiF;

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/BiF;->a(Z)V

    .line 1809938
    :goto_0
    return-void

    .line 1809939
    :cond_0
    check-cast p2, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 1809940
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v0

    .line 1809941
    check-cast p3, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    .line 1809942
    iget-object v1, p0, LX/Bi8;->d:LX/BiM;

    iget-object v2, p0, LX/Bi8;->g:LX/Bi6;

    invoke-virtual {v1, p3, v0, v2}, LX/BiM;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/Bi3;LX/Bi6;)V
    .locals 2
    .param p1    # Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1809943
    if-nez p1, :cond_1

    .line 1809944
    :cond_0
    :goto_0
    return-void

    .line 1809945
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object v0

    new-instance v1, LX/Bi4;

    invoke-direct {v1, p0}, LX/Bi4;-><init>(LX/Bi8;)V

    invoke-static {v0, v1}, LX/0PN;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Bi8;->e:LX/0Px;

    .line 1809946
    iget-object v0, p0, LX/Bi8;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1809947
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1809948
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/Bi8;->h:Z

    .line 1809949
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bi8;->i:Ljava/lang/String;

    .line 1809950
    :cond_2
    iput-object p2, p0, LX/Bi8;->f:LX/Bi3;

    .line 1809951
    iput-object p3, p0, LX/Bi8;->g:LX/Bi6;

    .line 1809952
    const v0, 0xff39417

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1809935
    iget-object v0, p0, LX/Bi8;->g:LX/Bi6;

    iget-object v0, v0, LX/Bi6;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1809933
    iget-object v1, p0, LX/Bi8;->e:LX/0Px;

    if-nez v1, :cond_0

    .line 1809934
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/Bi8;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-boolean v2, p0, LX/Bi8;->h:Z

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1809930
    iget-object v0, p0, LX/Bi8;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1809931
    sget-object v0, LX/Bi8;->a:Ljava/lang/Object;

    .line 1809932
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Bi8;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1809929
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 1809922
    invoke-virtual {p0, p1}, LX/Bi8;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1809923
    iget-object v1, p0, LX/Bi8;->f:LX/Bi3;

    sget-object v2, LX/Bi3;->PLACE_TIPS:LX/Bi3;

    invoke-virtual {v1, v2}, LX/Bi3;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1809924
    sget-object v0, LX/Bi7;->REACTION_CONTEXT_ITEM_VIEW:LX/Bi7;

    invoke-virtual {v0}, LX/Bi7;->ordinal()I

    move-result v0

    .line 1809925
    :goto_0
    return v0

    .line 1809926
    :cond_0
    instance-of v0, v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1809927
    sget-object v0, LX/Bi7;->CONTEXT_ITEM_VIEW:LX/Bi7;

    invoke-virtual {v0}, LX/Bi7;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1809928
    :cond_1
    sget-object v0, LX/Bi7;->LOAD_MORE_VIEW:LX/Bi7;

    invoke-virtual {v0}, LX/Bi7;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1809921
    invoke-direct {p0}, LX/Bi8;->d()[LX/Bi7;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
