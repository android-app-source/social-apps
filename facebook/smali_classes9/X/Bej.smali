.class public final LX/Bej;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorPagerFragment;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorPagerFragment;LX/0gc;)V
    .locals 7

    .prologue
    .line 1805234
    iput-object p1, p0, LX/Bej;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorPagerFragment;

    .line 1805235
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 1805236
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1805237
    invoke-static {}, LX/Bek;->values()[LX/Bek;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1805238
    sget-object v5, LX/Bei;->a:[I

    invoke-virtual {v4}, LX/Bek;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1805239
    const/4 v5, 0x0

    :goto_1
    move-object v4, v5

    .line 1805240
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1805241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1805242
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Bej;->b:LX/0Px;

    .line 1805243
    return-void

    .line 1805244
    :pswitch_0
    new-instance v5, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    invoke-direct {v5}, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;-><init>()V

    goto :goto_1

    .line 1805245
    :pswitch_1
    sget-object v5, LX/Bek;->MY_EDITS:LX/Bek;

    .line 1805246
    new-instance p1, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;

    invoke-direct {p1}, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;-><init>()V

    .line 1805247
    sget-object v6, LX/Bei;->a:[I

    invoke-virtual {v5}, LX/Bek;->ordinal()I

    move-result p2

    aget v6, v6, p2

    packed-switch v6, :pswitch_data_1

    .line 1805248
    const/4 v6, 0x0

    .line 1805249
    :goto_2
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 1805250
    const-string v4, "url"

    invoke-virtual {p2, v4, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805251
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1805252
    move-object v5, p1

    .line 1805253
    goto :goto_1

    .line 1805254
    :pswitch_2
    new-instance v5, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorVoteFragment;

    invoke-direct {v5}, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorVoteFragment;-><init>()V

    goto :goto_1

    .line 1805255
    :pswitch_3
    const-string v6, "http://m.facebook.com/editor/isolated/tab/profile"

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 1805256
    sget-object v0, LX/Bei;->a:[I

    .line 1805257
    sget-object v1, LX/Bek;->SCOREBOARD:LX/Bek;

    invoke-virtual {v1}, LX/Bek;->toPosition()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 1805258
    sget-object v1, LX/Bek;->SCOREBOARD:LX/Bek;

    .line 1805259
    :goto_0
    move-object v1, v1

    .line 1805260
    invoke-virtual {v1}, LX/Bek;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1805261
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1805262
    :pswitch_0
    const-string v0, "Scoreboard"

    goto :goto_1

    .line 1805263
    :pswitch_1
    const-string v0, "My Edits"

    goto :goto_1

    .line 1805264
    :pswitch_2
    const-string v0, "Vote"

    goto :goto_1

    .line 1805265
    :cond_0
    sget-object v1, LX/Bek;->MY_EDITS:LX/Bek;

    invoke-virtual {v1}, LX/Bek;->toPosition()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 1805266
    sget-object v1, LX/Bek;->MY_EDITS:LX/Bek;

    goto :goto_0

    .line 1805267
    :cond_1
    sget-object v1, LX/Bek;->VOTE:LX/Bek;

    invoke-virtual {v1}, LX/Bek;->toPosition()I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 1805268
    sget-object v1, LX/Bek;->VOTE:LX/Bek;

    goto :goto_0

    .line 1805269
    :cond_2
    iget-object v1, p0, LX/Bej;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorPagerFragment;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorPagerFragment;->a:LX/03V;

    const-string v2, "graph_editor_pager_fragment"

    const-string v3, "GraphEditorPage not defined for all positions"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805270
    sget-object v1, LX/Bek;->VOTE:LX/Bek;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1805271
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/Bej;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1805272
    iget-object v0, p0, LX/Bej;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0

    .line 1805273
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1805233
    iget-object v0, p0, LX/Bej;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
