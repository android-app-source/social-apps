.class public final LX/CZe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1916359
    iput-object p1, p0, LX/CZe;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    iput-object p2, p0, LX/CZe;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x2

    const v0, 0xfa1b1a7

    invoke-static {v2, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1916360
    iget-object v0, p0, LX/CZe;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1916361
    const v0, -0x4a717021

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1916362
    :goto_0
    return-void

    .line 1916363
    :cond_0
    iget-object v0, p0, LX/CZe;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v2, "tap_like_list"

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1916364
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1916365
    iget-object v2, p0, LX/CZe;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    iget-object v2, v2, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/CZe;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    const-string v4, "native_permalink"

    sget-object v5, LX/8s1;->ACTIVITY_RESULT:LX/8s1;

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;

    move-result-object v2

    .line 1916366
    const-string v3, "fragment_title"

    const v4, 0x7f080fe7

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1916367
    const-string v3, "open_fragment_as_modal"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1916368
    iget-object v3, p0, LX/CZe;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    iget-object v3, v3, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    const v4, 0xb256

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1916369
    const v0, -0x74bac702

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
