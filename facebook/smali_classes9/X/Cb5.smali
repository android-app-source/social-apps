.class public LX/Cb5;
.super LX/3Ag;
.source ""


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:LX/Caw;

.field public final e:LX/CbL;

.field public final f:LX/9hh;

.field public final g:Ljava/lang/String;

.field public final h:LX/8JW;

.field public final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/8Jd;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/8Jo;

.field public l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

.field public m:Z

.field private final n:Ljava/lang/Runnable;

.field public final o:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/0Ot;LX/9hh;Ljava/lang/String;LX/Caw;LX/CbL;LX/8JW;LX/8Jo;LX/8Jd;)V
    .locals 2
    .param p9    # LX/8Jd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;",
            "LX/9hh;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$DialogDismissListener;",
            "LX/CbL;",
            "LX/8JW;",
            "LX/8Jo;",
            "LX/8Jd;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1919127
    const v0, 0x1030010

    invoke-direct {p0, p1, v0}, LX/3Ag;-><init>(Landroid/content/Context;I)V

    .line 1919128
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;

    invoke-direct {v0, p0}, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;-><init>(LX/Cb5;)V

    iput-object v0, p0, LX/Cb5;->n:Ljava/lang/Runnable;

    .line 1919129
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$5;

    invoke-direct {v0, p0}, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$5;-><init>(LX/Cb5;)V

    iput-object v0, p0, LX/Cb5;->o:Ljava/lang/Runnable;

    .line 1919130
    invoke-virtual {p0, v1}, LX/Cb5;->requestWindowFeature(I)Z

    .line 1919131
    invoke-virtual {p0, v1}, LX/Cb5;->setCanceledOnTouchOutside(Z)V

    .line 1919132
    iput-object p1, p0, LX/Cb5;->c:Landroid/content/Context;

    .line 1919133
    iput-object p2, p0, LX/Cb5;->j:LX/0Ot;

    .line 1919134
    iput-object p3, p0, LX/Cb5;->f:LX/9hh;

    .line 1919135
    iput-object p4, p0, LX/Cb5;->g:Ljava/lang/String;

    .line 1919136
    iput-object p5, p0, LX/Cb5;->d:LX/Caw;

    .line 1919137
    iput-object p6, p0, LX/Cb5;->e:LX/CbL;

    .line 1919138
    iput-object p7, p0, LX/Cb5;->h:LX/8JW;

    .line 1919139
    invoke-static {p9}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Cb5;->i:LX/0am;

    .line 1919140
    iput-object p8, p0, LX/Cb5;->k:LX/8Jo;

    .line 1919141
    iput-boolean v1, p0, LX/Cb5;->m:Z

    .line 1919142
    new-instance v0, LX/Cb2;

    invoke-direct {v0, p0}, LX/Cb2;-><init>(LX/Cb5;)V

    move-object v0, v0

    .line 1919143
    invoke-virtual {p0, v0}, LX/Cb5;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1919144
    return-void
.end method

.method public static a(Landroid/content/Context;LX/CbL;LX/0Ot;LX/9hh;Ljava/lang/String;LX/Caw;LX/8JW;LX/8Jo;Z)LX/Cb5;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/CbL;",
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;",
            "LX/9hh;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$DialogDismissListener;",
            "LX/8JW;",
            "LX/8Jo;",
            "Z)",
            "LX/Cb5;"
        }
    .end annotation

    .prologue
    .line 1919113
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919114
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919115
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919116
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919117
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919118
    invoke-static/range {p6 .. p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919119
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1919120
    if-eqz p8, :cond_1

    new-instance v9, LX/8Jd;

    invoke-direct {v9, p0}, LX/8Jd;-><init>(Landroid/content/Context;)V

    .line 1919121
    :goto_1
    new-instance v0, LX/Cb5;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v9}, LX/Cb5;-><init>(Landroid/content/Context;LX/0Ot;LX/9hh;Ljava/lang/String;LX/Caw;LX/CbL;LX/8JW;LX/8Jo;LX/8Jd;)V

    .line 1919122
    invoke-virtual {v0}, LX/Cb5;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1}, LX/8Hi;->a(Landroid/view/Window;)V

    .line 1919123
    invoke-virtual {v0}, LX/Cb5;->show()V

    .line 1919124
    return-object v0

    .line 1919125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1919126
    :cond_1
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public static a(LX/Cb5;LX/0am;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1919092
    iget-object v0, p0, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v0}, LX/CbL;->c()LX/CbN;

    move-result-object v0

    .line 1919093
    iget-object v1, v0, LX/CbN;->e:LX/0Px;

    move-object v0, v1

    .line 1919094
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1919095
    :cond_0
    iget-object v0, p0, LX/Cb5;->e:LX/CbL;

    iget-object v1, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1919096
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919097
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1919098
    :goto_0
    if-eqz v2, :cond_1

    .line 1919099
    iget-object v3, v0, LX/CbL;->l:LX/CbF;

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v2

    .line 1919100
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1919101
    :cond_1
    :goto_1
    return-void

    .line 1919102
    :cond_2
    iget-object v1, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setTagSuggestions(Ljava/util/List;)V

    goto :goto_1

    .line 1919103
    :cond_3
    iget-object v2, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    goto :goto_0

    .line 1919104
    :cond_4
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p0

    .line 1919105
    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, p1, :cond_5

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel$EdgesModel;

    .line 1919106
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxUserModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxUserModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1919107
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 1919108
    :cond_5
    iget-object v4, v3, LX/CbF;->a:LX/8Gm;

    .line 1919109
    iget-object v5, v4, LX/8Gm;->a:LX/0TD;

    new-instance v0, LX/8Gl;

    invoke-direct {v0, v4, p0}, LX/8Gl;-><init>(LX/8Gm;Ljava/util/Collection;)V

    invoke-interface {v5, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1919110
    move-object v4, v5

    .line 1919111
    new-instance v5, LX/CbE;

    invoke-direct {v5, v3, p0, v1}, LX/CbE;-><init>(LX/CbF;Ljava/util/List;LX/7Gn;)V

    move-object v5, v5

    .line 1919112
    iget-object p0, v3, LX/CbF;->d:LX/0Sh;

    invoke-virtual {p0, v4, v5}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/Cb5;Lcom/facebook/tagging/model/TaggingProfile;I)V
    .locals 6

    .prologue
    .line 1919074
    iget-wide v4, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v4

    .line 1919075
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1919076
    :goto_0
    iget-object v0, p0, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v0}, LX/CbL;->c()LX/CbN;

    move-result-object v0

    .line 1919077
    iget-boolean v2, v0, LX/CbN;->a:Z

    move v0, v2

    .line 1919078
    if-eqz v0, :cond_1

    sget-object v0, LX/74U;->FACEBOX:LX/74U;

    move-object v2, v0

    .line 1919079
    :goto_1
    sget-object v3, LX/74T;->CONSUMPTION:LX/74T;

    .line 1919080
    iget-object v0, p0, LX/Cb5;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/745;

    .line 1919081
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919082
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919083
    invoke-static {v0}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v4

    .line 1919084
    const-string v5, "is_text"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919085
    const-string v5, "ex_tag_screen"

    invoke-virtual {v3}, LX/74T;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919086
    const-string v5, "tag_src"

    invoke-virtual {v2}, LX/74U;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919087
    const-string v5, "ex_tag_index"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919088
    sget-object v5, LX/74R;->TAG_CREATED:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v5, v4, p0}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1919089
    return-void

    .line 1919090
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1919091
    :cond_1
    sget-object v0, LX/74U;->TAP_ANYWHERE:LX/74U;

    move-object v2, v0

    goto :goto_1
.end method

.method public static c(LX/Cb5;)V
    .locals 2

    .prologue
    .line 1919145
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, p0, LX/Cb5;->n:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1919146
    return-void
.end method

.method public static e(LX/Cb5;)V
    .locals 8

    .prologue
    .line 1919066
    iget-object v0, p0, LX/Cb5;->e:LX/CbL;

    .line 1919067
    iget-object v3, v0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v3}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v3

    .line 1919068
    iget-object v4, v0, LX/CbL;->c:Landroid/graphics/Matrix;

    const-wide/16 v5, 0x12c

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, LX/5ub;->a(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V

    .line 1919069
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(ZLandroid/graphics/PointF;)V

    .line 1919070
    iget-object v0, p0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919071
    iget-object v0, p0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Jd;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/8Jd;->setVisibility(I)V

    .line 1919072
    :cond_0
    invoke-virtual {p0}, LX/Cb5;->dismiss()V

    .line 1919073
    return-void
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 1919064
    invoke-static {p0}, LX/Cb5;->e(LX/Cb5;)V

    .line 1919065
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1919048
    invoke-super {p0, p1}, LX/3Ag;->onCreate(Landroid/os/Bundle;)V

    .line 1919049
    iget-object v0, p0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919050
    iget-object v0, p0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, LX/Cb5;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1919051
    :cond_0
    new-instance v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, p0, LX/Cb5;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1919052
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1919053
    new-instance v1, LX/Cb4;

    invoke-direct {v1, p0}, LX/Cb4;-><init>(LX/Cb5;)V

    move-object v1, v1

    .line 1919054
    iput-object v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->r:LX/8JT;

    .line 1919055
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, p0, LX/Cb5;->e:LX/CbL;

    .line 1919056
    iget-object p1, v1, LX/CbL;->m:LX/8nB;

    move-object v1, p1

    .line 1919057
    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;)V

    .line 1919058
    iget-object v0, p0, LX/Cb5;->e:LX/CbL;

    .line 1919059
    new-instance v1, LX/Cb3;

    invoke-direct {v1, p0}, LX/Cb3;-><init>(LX/Cb5;)V

    move-object v1, v1

    .line 1919060
    invoke-virtual {v0, v1}, LX/CbL;->a(LX/Can;)V

    .line 1919061
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, LX/Cb5;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1919062
    iget-object v0, p0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$2;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$2;-><init>(LX/Cb5;)V

    invoke-static {v0, v1}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1919063
    return-void
.end method
