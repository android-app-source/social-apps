.class public LX/BNY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1vn;


# direct methods
.method public constructor <init>(LX/0tX;LX/1vn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778804
    iput-object p1, p0, LX/BNY;->a:LX/0tX;

    .line 1778805
    iput-object p2, p0, LX/BNY;->b:LX/1vn;

    .line 1778806
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1778807
    new-instance v0, LX/BNh;

    invoke-direct {v0}, LX/BNh;-><init>()V

    move-object v0, v0

    .line 1778808
    const-string v1, "user_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_cursor"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "review_profile_pic_size"

    iget-object v3, p0, LX/BNY;->b:LX/1vn;

    invoke-virtual {v3}, LX/1vn;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1778809
    iget-object v1, p0, LX/BNY;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
