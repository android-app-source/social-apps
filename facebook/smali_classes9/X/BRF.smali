.class public final LX/BRF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BRG;


# direct methods
.method public constructor <init>(LX/BRG;)V
    .locals 0

    .prologue
    .line 1783617
    iput-object p1, p0, LX/BRF;->a:LX/BRG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x2f6fc307

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1783618
    iget-object v0, p0, LX/BRF;->a:LX/BRG;

    iget-object v0, v0, LX/BRG;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1783619
    iget-object v0, p0, LX/BRF;->a:LX/BRG;

    iget-object v0, v0, LX/BRG;->i:LX/BTG;

    iget-object v1, p0, LX/BRF;->a:LX/BRG;

    iget-object v1, v1, LX/BRG;->c:LX/BR1;

    iget-object v1, v1, LX/BR1;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile_video"

    iget-object v3, p0, LX/BRF;->a:LX/BRG;

    iget-object v3, v3, LX/BRG;->c:LX/BR1;

    iget-object v3, v3, LX/BR1;->g:Ljava/lang/String;

    const-string v4, "standard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BTG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783620
    iget-object v0, p0, LX/BRF;->a:LX/BRG;

    iget-object v0, v0, LX/BRG;->k:LX/BSt;

    new-instance v1, LX/5SM;

    iget-object v2, p0, LX/BRF;->a:LX/BRG;

    iget-object v2, v2, LX/BRG;->a:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v1, v2}, LX/5SM;-><init>(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V

    sget-object v2, LX/BQj;->a:Ljava/lang/String;

    .line 1783621
    iput-object v2, v1, LX/5SM;->p:Ljava/lang/String;

    .line 1783622
    move-object v1, v1

    .line 1783623
    iget-object v2, p0, LX/BRF;->a:LX/BRG;

    iget-object v2, v2, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1783624
    iput-object v2, v1, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1783625
    move-object v1, v1

    .line 1783626
    const-string v2, "standard"

    .line 1783627
    iput-object v2, v1, LX/5SM;->q:Ljava/lang/String;

    .line 1783628
    move-object v1, v1

    .line 1783629
    invoke-virtual {v1}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v1

    iget-object v2, p0, LX/BRF;->a:LX/BRG;

    iget-object v2, v2, LX/BRG;->c:LX/BR1;

    iget-object v2, v2, LX/BR1;->a:Landroid/net/Uri;

    new-instance v3, LX/BRE;

    invoke-direct {v3, p0}, LX/BRE;-><init>(LX/BRF;)V

    const-string v4, "profile_video"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/BSt;->a(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Landroid/net/Uri;LX/ATX;Ljava/lang/String;LX/9fh;)V

    .line 1783630
    const v0, -0x35167b2d    # -7651945.5f

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
