.class public final LX/Bpv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V
    .locals 0

    .prologue
    .line 1823656
    iput-object p1, p0, LX/Bpv;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1823657
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1823658
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1823659
    if-eqz p1, :cond_0

    .line 1823660
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1823661
    if-nez v0, :cond_1

    .line 1823662
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched story was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/Bpv;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1823663
    :goto_0
    return-void

    .line 1823664
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1823665
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1823666
    iget-object v1, p0, LX/Bpv;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->e:LX/189;

    invoke-virtual {v1, v0}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1823667
    iget-object v1, p0, LX/Bpv;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
