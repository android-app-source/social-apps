.class public LX/C0D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2yJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2mt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/2yI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/39V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1840123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840124
    return-void
.end method

.method public static b(LX/0QB;)LX/C0D;
    .locals 5

    .prologue
    .line 1840125
    new-instance v4, LX/C0D;

    invoke-direct {v4}, LX/C0D;-><init>()V

    .line 1840126
    invoke-static {p0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v0

    check-cast v0, LX/2yJ;

    invoke-static {p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v1

    check-cast v1, LX/2mt;

    const-class v2, LX/2yI;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2yI;

    invoke-static {p0}, LX/39V;->a(LX/0QB;)LX/39V;

    move-result-object v3

    check-cast v3, LX/39V;

    .line 1840127
    iput-object v0, v4, LX/C0D;->a:LX/2yJ;

    iput-object v1, v4, LX/C0D;->b:LX/2mt;

    iput-object v2, v4, LX/C0D;->c:LX/2yI;

    iput-object v3, v4, LX/C0D;->d:LX/39V;

    .line 1840128
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ":",
            "LX/1Pr;",
            ">(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 1840129
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840130
    invoke-static {v2}, LX/2yb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1840131
    iget-object v2, p0, LX/C0D;->c:LX/2yI;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v2, v0, v3, v1}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/B6B;->onClick(Landroid/view/View;)V

    .line 1840132
    :cond_0
    :goto_0
    return-void

    .line 1840133
    :cond_1
    iget-object v3, p0, LX/C0D;->b:LX/2mt;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v5

    .line 1840134
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1840135
    invoke-static {v5}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1840136
    iget-object v2, p0, LX/C0D;->d:LX/39V;

    const/4 v6, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, LX/39V;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/util/Map;LX/1Pq;)V

    goto :goto_0

    .line 1840137
    :cond_2
    invoke-static {v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 1840138
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    move v4, v3

    .line 1840139
    :goto_1
    if-eqz v4, :cond_4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v3

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v3, v7, :cond_4

    const/4 v3, 0x1

    .line 1840140
    :goto_2
    if-eqz v4, :cond_5

    if-nez v3, :cond_5

    .line 1840141
    iget-object v2, p0, LX/C0D;->d:LX/39V;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, LX/39V;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/util/Map;LX/1Pq;)V

    goto :goto_0

    .line 1840142
    :cond_3
    const/4 v3, 0x0

    move v4, v3

    goto :goto_1

    .line 1840143
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 1840144
    :cond_5
    invoke-static/range {p2 .. p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object/from16 v3, p3

    .line 1840145
    check-cast v3, LX/1Pr;

    new-instance v6, LX/2yU;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v3, v6, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/2yV;

    move-object/from16 v3, p3

    .line 1840146
    check-cast v3, LX/1Pr;

    new-instance v6, LX/2yh;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, LX/2yh;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v3, v6, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/2yi;

    .line 1840147
    iget-object v6, p0, LX/C0D;->a:LX/2yJ;

    iget-object v3, p0, LX/C0D;->a:LX/2yJ;

    invoke-virtual {v3}, LX/2yJ;->a()LX/2yN;

    move-result-object v9

    invoke-static {v2}, LX/2yg;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v13

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object v10, v5

    move-object/from16 v14, p3

    invoke-virtual/range {v6 .. v14}, LX/2yJ;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;Ljava/lang/String;LX/2yV;LX/2yi;ZLX/1Pq;)V

    goto/16 :goto_0
.end method
