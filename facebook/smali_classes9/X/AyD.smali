.class public LX/AyD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/AyD;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0se;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AyB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0se;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AyB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729787
    iput-object p1, p0, LX/AyD;->a:LX/0Ot;

    .line 1729788
    iput-object p2, p0, LX/AyD;->b:LX/0Ot;

    .line 1729789
    iput-object p3, p0, LX/AyD;->c:LX/0Ot;

    .line 1729790
    return-void
.end method

.method public static a(LX/0QB;)LX/AyD;
    .locals 6

    .prologue
    .line 1729791
    sget-object v0, LX/AyD;->d:LX/AyD;

    if-nez v0, :cond_1

    .line 1729792
    const-class v1, LX/AyD;

    monitor-enter v1

    .line 1729793
    :try_start_0
    sget-object v0, LX/AyD;->d:LX/AyD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729794
    if-eqz v2, :cond_0

    .line 1729795
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729796
    new-instance v3, LX/AyD;

    const/16 v4, 0xafd

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf27

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x22c3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/AyD;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1729797
    move-object v0, v3

    .line 1729798
    sput-object v0, LX/AyD;->d:LX/AyD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729799
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729800
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729801
    :cond_1
    sget-object v0, LX/AyD;->d:LX/AyD;

    return-object v0

    .line 1729802
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
