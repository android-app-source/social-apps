.class public final LX/BTy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;)V
    .locals 0

    .prologue
    .line 1788208
    iput-object p1, p0, LX/BTy;->a:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;J)Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 1788198
    :try_start_0
    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, v1, LX/BUA;->n:LX/0tQ;

    .line 1788199
    iget-object v6, v1, LX/0tQ;->a:LX/0ad;

    sget-wide v8, LX/0wh;->x:J

    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-interface {v6, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v6

    move-wide v2, v6

    .line 1788200
    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, v1, LX/BUA;->e:LX/19w;

    invoke-virtual {v1, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v1

    .line 1788201
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    if-nez v1, :cond_1

    .line 1788202
    :cond_0
    :goto_0
    return v0

    .line 1788203
    :cond_1
    iget-wide v4, v1, LX/7Jg;->f:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788204
    sub-long/2addr v4, p2

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1788205
    :catch_0
    move-exception v0

    .line 1788206
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Exception"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1788207
    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;JJ)V
    .locals 9

    .prologue
    .line 1788138
    iget-object v8, p0, LX/BTy;->a:LX/BUA;

    monitor-enter v8

    .line 1788139
    cmp-long v0, p3, p5

    if-ltz v0, :cond_0

    .line 1788140
    :try_start_0
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, v0, LX/BUA;->e:LX/19w;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p5

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, LX/19w;->a(Ljava/lang/String;Landroid/net/Uri;JJ)V

    .line 1788141
    :cond_0
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 1788142
    iget-object v1, v0, LX/7Jg;->c:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1788143
    iget-wide v2, v0, LX/7Jg;->e:J

    iget-wide v4, v0, LX/7Jg;->d:J

    add-long p3, v2, v4

    .line 1788144
    iget-wide v2, v0, LX/7Jg;->g:J

    iget-wide v4, v0, LX/7Jg;->f:J

    add-long p5, v2, v4

    .line 1788145
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-lez v1, :cond_3

    cmp-long v1, p3, p5

    if-gtz v1, :cond_3

    .line 1788146
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    invoke-static {v0, p1, v1}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788147
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    iget-object v0, v0, LX/BUA;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788148
    :cond_2
    :goto_0
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v0, p1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    .line 1788149
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, v1, LX/BUA;->n:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->h()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/BUA;->a(LX/BUA;J)J

    .line 1788150
    monitor-exit v8

    return-void

    .line 1788151
    :cond_3
    iget-object v1, v0, LX/7Jg;->c:Landroid/net/Uri;

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-wide v2, v0, LX/7Jg;->f:J

    iget-wide v4, v0, LX/7Jg;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 1788152
    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    iget-object v1, v1, LX/BUA;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1788153
    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    .line 1788154
    invoke-static {v1, v0}, LX/BUA;->b$redex0(LX/BUA;LX/7Jg;)V

    .line 1788155
    goto :goto_0

    .line 1788156
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;I)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1788157
    iget-object v5, p0, LX/BTy;->a:LX/BUA;

    monitor-enter v5

    .line 1788158
    :try_start_0
    sget-object v2, LX/BUA;->d:Ljava/lang/String;

    const-string v6, "Error downloading video %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v2, p2, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788159
    :try_start_1
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-object v2, v2, LX/BUA;->g:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BUM;

    .line 1788160
    iget-object v6, p0, LX/BTy;->a:LX/BUA;

    iget-object v6, v6, LX/BUA;->e:LX/19w;

    invoke-virtual {v6, p1}, LX/19w;->m(Ljava/lang/String;)LX/7Jf;

    move-result-object v6

    .line 1788161
    iget-object v7, p0, LX/BTy;->a:LX/BUA;

    iget-object v7, v7, LX/BUA;->e:LX/19w;

    invoke-virtual {v7, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v7

    .line 1788162
    const/16 v8, 0x191

    if-eq p3, v8, :cond_0

    const/16 v8, 0x193

    if-eq p3, v8, :cond_0

    iget-object v7, v7, LX/7Jg;->b:Landroid/net/Uri;

    invoke-static {v7}, LX/1H1;->i(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1788163
    :cond_0
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    sget-object v4, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    invoke-static {v2, p1, v4}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788164
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-object v4, v2, LX/BUA;->k:LX/19v;

    instance-of v2, p2, LX/BTp;

    if-eqz v2, :cond_1

    move-object v0, p2

    check-cast v0, LX/BTp;

    move-object v2, v0

    iget-object v2, v2, LX/BTp;->mExceptionCode:LX/BTo;

    invoke-virtual {v2}, LX/BTo;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {v4, p1, p2, v3}, LX/19v;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1788165
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    .line 1788166
    iget-object v0, v2, LX/BUA;->t:LX/BUF;

    new-instance v1, LX/BTr;

    invoke-direct {v1, v2}, LX/BTr;-><init>(LX/BUA;)V

    invoke-virtual {v0, p1, v1}, LX/BUF;->a(Ljava/lang/String;LX/BTr;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788167
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1788168
    :goto_0
    return-void

    .line 1788169
    :cond_2
    if-eqz v2, :cond_8

    .line 1788170
    :try_start_3
    iget-wide v8, v2, LX/BUM;->a:J

    .line 1788171
    invoke-direct {p0, p1, v8, v9}, LX/BTy;->a(Ljava/lang/String;J)Z

    move-result v2

    .line 1788172
    :goto_1
    if-nez v2, :cond_6

    iget-object v4, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v4}, LX/BUA;->h(LX/BUA;)Z

    move-result v4

    if-nez v4, :cond_3

    if-eqz v6, :cond_6

    iget v4, v6, LX/7Jf;->b:I

    const/4 v6, 0x5

    if-ge v4, v6, :cond_6

    .line 1788173
    :cond_3
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    sget-object v4, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    invoke-static {v2, p1, v4}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788174
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-object v4, v2, LX/BUA;->k:LX/19v;

    instance-of v2, p2, LX/BTp;

    if-eqz v2, :cond_5

    move-object v0, p2

    check-cast v0, LX/BTp;

    move-object v2, v0

    iget-object v2, v2, LX/BTp;->mExceptionCode:LX/BTo;

    invoke-virtual {v2}, LX/BTo;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v4, p1, p2, v2}, LX/19v;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1788175
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-object v2, v2, LX/BUA;->l:LX/16I;

    invoke-virtual {v2}, LX/16I;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1788176
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    .line 1788177
    new-instance v3, Lcom/facebook/video/downloadmanager/DownloadManager$1$1;

    invoke-direct {v3, p0, p1}, Lcom/facebook/video/downloadmanager/DownloadManager$1$1;-><init>(LX/BTy;Ljava/lang/String;)V

    iget-object v4, p0, LX/BTy;->a:LX/BUA;

    iget-wide v6, v4, LX/BUA;->x:J

    invoke-virtual {v2, v3, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1788178
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-wide v2, v2, LX/BUA;->x:J

    const-wide/32 v6, 0xdbba0

    cmp-long v2, v2, v6

    if-gez v2, :cond_4

    .line 1788179
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    iget-object v3, p0, LX/BTy;->a:LX/BUA;

    iget-wide v6, v3, LX/BUA;->x:J

    const-wide/16 v8, 0x2

    mul-long/2addr v6, v8

    .line 1788180
    iput-wide v6, v2, LX/BUA;->x:J

    .line 1788181
    :cond_4
    :goto_3
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v2, p1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1788182
    :goto_4
    :try_start_4
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    :cond_5
    move-object v2, v3

    .line 1788183
    goto :goto_2

    .line 1788184
    :cond_6
    if-eqz v2, :cond_7

    .line 1788185
    :try_start_5
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 1788186
    if-eqz v0, :cond_9

    iget-object v1, v0, LX/7Jg;->l:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v1, v2, :cond_9

    .line 1788187
    iget-object v1, p0, LX/BTy;->a:LX/BUA;

    .line 1788188
    invoke-static {v1, v0}, LX/BUA;->b$redex0(LX/BUA;LX/7Jg;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1788189
    :goto_5
    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1788190
    :cond_7
    :try_start_7
    sget-object v2, LX/BUA;->d:Ljava/lang/String;

    const-string v3, "ABORTING DOWNLOAD"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, p2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788191
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v2, p1, p2}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 1788192
    :catch_0
    move-exception v2

    .line 1788193
    :try_start_8
    sget-object v3, LX/BUA;->d:Ljava/lang/String;

    const-string v4, "failing retry download"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788194
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v2, p1, p2}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1788195
    iget-object v2, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v2, p1}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    :cond_8
    move v2, v4

    goto/16 :goto_1

    .line 1788196
    :cond_9
    sget-object v0, LX/BUA;->d:Ljava/lang/String;

    const-string v1, "Cannot retry. No record found in DB. ABORTING DOWNLOAD"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p2, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788197
    iget-object v0, p0, LX/BTy;->a:LX/BUA;

    invoke-static {v0, p1, p2}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
