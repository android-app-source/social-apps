.class public LX/CME;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/CMB;

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# instance fields
.field public final e:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

.field private final f:LX/1zC;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/content/res/Resources;

.field public i:I

.field public j:I

.field private k:LX/CMB;

.field public l:Lcom/facebook/ui/emoji/model/Emoji;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/CMR;

.field public n:LX/CMD;

.field private o:LX/A7L;

.field private p:I

.field private q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1880341
    new-instance v0, LX/CMB;

    invoke-direct {v0, v2, v2}, LX/CMB;-><init>(ZZ)V

    sput-object v0, LX/CME;->a:LX/CMB;

    .line 1880342
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/CME;->b:[I

    .line 1880343
    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, LX/CME;->c:[I

    .line 1880344
    new-array v0, v3, [I

    const v1, 0x10100a1

    aput v1, v0, v2

    sput-object v0, LX/CME;->d:[I

    return-void

    .line 1880345
    :array_0
    .array-data 4
        0x101009e
        -0x10100a7
        -0x10100a1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;LX/1zC;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880329
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1880330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CME;->g:Ljava/util/List;

    .line 1880331
    sget-object v0, LX/CME;->a:LX/CMB;

    iput-object v0, p0, LX/CME;->k:LX/CMB;

    .line 1880332
    iput-object p2, p0, LX/CME;->e:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    .line 1880333
    iput-object p3, p0, LX/CME;->f:LX/1zC;

    .line 1880334
    iput-object p1, p0, LX/CME;->h:Landroid/content/res/Resources;

    .line 1880335
    const v0, 0x7f0b025c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/CME;->i:I

    .line 1880336
    iget v0, p0, LX/CME;->i:I

    iput v0, p0, LX/CME;->j:I

    .line 1880337
    iget-object v0, p0, LX/CME;->h:Landroid/content/res/Resources;

    const v1, 0x7f0a019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1880338
    const/4 v1, 0x0

    const v2, 0x3e99999a    # 0.3f

    invoke-static {v0, v2}, LX/47Z;->b(IF)I

    move-result v2

    invoke-static {p0, v1, v0, v2}, LX/CME;->a(LX/CME;III)V

    .line 1880339
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 1880340
    return-void
.end method

.method public static a(LX/CME;III)V
    .locals 3

    .prologue
    .line 1880322
    iput p2, p0, LX/CME;->p:I

    .line 1880323
    new-instance v0, LX/A7L;

    invoke-direct {v0}, LX/A7L;-><init>()V

    iput-object v0, p0, LX/CME;->o:LX/A7L;

    .line 1880324
    iget-object v0, p0, LX/CME;->o:LX/A7L;

    sget-object v1, LX/CME;->b:[I

    const v2, 0x7f020f53

    invoke-direct {p0, v2, p1}, LX/CME;->e(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/A7L;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1880325
    iget-object v0, p0, LX/CME;->o:LX/A7L;

    sget-object v1, LX/CME;->c:[I

    const v2, 0x7f020f54

    invoke-direct {p0, v2, p2}, LX/CME;->e(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/A7L;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1880326
    iget-object v0, p0, LX/CME;->o:LX/A7L;

    sget-object v1, LX/CME;->d:[I

    const v2, 0x7f020f54

    invoke-direct {p0, v2, p3}, LX/CME;->e(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/A7L;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1880327
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1880328
    return-void
.end method

.method private e()I
    .locals 1

    .prologue
    .line 1880321
    iget-object v0, p0, LX/CME;->k:LX/CMB;

    iget-boolean v0, v0, LX/CMB;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(II)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1880268
    iget-object v0, p0, LX/CME;->h:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 1880269
    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1880270
    return-object v0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1880320
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1880296
    packed-switch p2, :pswitch_data_0

    .line 1880297
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EmojilikePickerView onCreateViewHolder with unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1880298
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1880299
    const v1, 0x7f030b48

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1880300
    new-instance v1, LX/CM8;

    invoke-direct {v1, p0}, LX/CM8;-><init>(LX/CME;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1880301
    new-instance v1, LX/CMC;

    invoke-direct {v1, p0, v0}, LX/CMC;-><init>(LX/CME;Lcom/facebook/fbui/glyph/GlyphButton;)V

    move-object v0, v1

    .line 1880302
    :goto_0
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 1880303
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object p2

    .line 1880304
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1a3;

    .line 1880305
    if-nez v1, :cond_0

    .line 1880306
    invoke-virtual {p2}, LX/1OR;->b()LX/1a3;

    move-result-object v1

    .line 1880307
    iget-object p2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1880308
    :cond_0
    iget p2, p0, LX/CME;->i:I

    iput p2, v1, LX/1a3;->width:I

    .line 1880309
    iget p2, p0, LX/CME;->j:I

    iput p2, v1, LX/1a3;->height:I

    .line 1880310
    return-object v0

    .line 1880311
    :pswitch_1
    iget-object v0, p0, LX/CME;->e:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a(Landroid/view/ViewGroup;)LX/CLu;

    move-result-object v0

    .line 1880312
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    new-instance p2, LX/CM9;

    invoke-direct {p2, p0, v0}, LX/CM9;-><init>(LX/CME;LX/CLu;)V

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1880313
    move-object v0, v0

    .line 1880314
    goto :goto_0

    .line 1880315
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1880316
    const v1, 0x7f030b49

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1880317
    new-instance v1, LX/CMA;

    invoke-direct {v1, p0}, LX/CMA;-><init>(LX/CME;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1880318
    new-instance v1, LX/CMC;

    invoke-direct {v1, p0, v0}, LX/CMC;-><init>(LX/CME;Lcom/facebook/fbui/glyph/GlyphButton;)V

    move-object v0, v1

    .line 1880319
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1880281
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1880282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EmojilikePickerView onBindViewHolder with unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v0, p1

    .line 1880283
    check-cast v0, LX/CMC;

    .line 1880284
    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/CME;->l:Lcom/facebook/ui/emoji/model/Emoji;

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1880285
    iget v1, p0, LX/CME;->p:I

    invoke-virtual {v0, v1}, LX/CMC;->c(I)V

    .line 1880286
    :goto_1
    iget-object v0, p0, LX/CME;->o:LX/A7L;

    if-eqz v0, :cond_0

    .line 1880287
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/CME;->o:LX/A7L;

    invoke-virtual {v1}, LX/A7L;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1880288
    :cond_0
    return-void

    .line 1880289
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1880290
    :pswitch_1
    iget-object v0, p0, LX/CME;->g:Ljava/util/List;

    invoke-direct {p0}, LX/CME;->e()I

    move-result v1

    sub-int v1, p2, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/emoji/model/Emoji;

    move-object v1, p1

    .line 1880291
    check-cast v1, LX/CLu;

    .line 1880292
    invoke-virtual {v1, v0}, LX/CLu;->b(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880293
    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    iget-object v2, p0, LX/CME;->l:Lcom/facebook/ui/emoji/model/Emoji;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/emoji/model/Emoji;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1

    :pswitch_2
    move-object v0, p1

    .line 1880294
    check-cast v0, LX/CMC;

    .line 1880295
    iget v1, p0, LX/CME;->q:I

    invoke-virtual {v0, v1}, LX/CMC;->c(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 0
    .param p1    # Lcom/facebook/ui/emoji/model/Emoji;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880278
    iput-object p1, p0, LX/CME;->l:Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880279
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1880280
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1880272
    if-nez p1, :cond_0

    iget-object v0, p0, LX/CME;->k:LX/CMB;

    iget-boolean v0, v0, LX/CMB;->a:Z

    if-eqz v0, :cond_0

    .line 1880273
    const/4 v0, 0x0

    .line 1880274
    :goto_0
    return v0

    .line 1880275
    :cond_0
    iget-object v0, p0, LX/CME;->k:LX/CMB;

    iget-boolean v0, v0, LX/CMB;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1880276
    const/4 v0, 0x2

    goto :goto_0

    .line 1880277
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1880271
    invoke-direct {p0}, LX/CME;->e()I

    move-result v0

    iget-object v1, p0, LX/CME;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LX/CME;->k:LX/CMB;

    iget-boolean v0, v0, LX/CMB;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
