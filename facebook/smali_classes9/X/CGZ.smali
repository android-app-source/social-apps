.class public final LX/CGZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARN;


# instance fields
.field public final synthetic c:LX/CGd;


# direct methods
.method public constructor <init>(LX/CGd;)V
    .locals 0

    .prologue
    .line 1865590
    iput-object p1, p0, LX/CGZ;->c:LX/CGd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1865591
    iget-object v0, p0, LX/CGZ;->c:LX/CGd;

    const/4 v1, 0x0

    .line 1865592
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 1865593
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 1865594
    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_3

    .line 1865595
    iget-object v1, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1865596
    const v2, 0x7f082967

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1865597
    :goto_1
    new-instance v2, LX/31Y;

    .line 1865598
    iget-object v3, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v3, v3

    .line 1865599
    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1865600
    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v3, 0x104000a

    new-instance p0, LX/CGb;

    invoke-direct {p0, v0}, LX/CGb;-><init>(LX/CGd;)V

    invoke-virtual {v1, v3, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1865601
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1865602
    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1865603
    if-eqz v0, :cond_1

    .line 1865604
    const/4 v0, 0x1

    .line 1865605
    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v2, v1

    .line 1865606
    goto :goto_0

    .line 1865607
    :cond_3
    const/16 v3, 0x1f4

    if-le v2, v3, :cond_4

    .line 1865608
    iget-object v1, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1865609
    const v2, 0x7f082968    # 1.8099E38f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1865610
    :cond_4
    iget-object v2, v0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v2}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v2}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 1865611
    iget-object v1, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1865612
    const v2, 0x7f082969

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
