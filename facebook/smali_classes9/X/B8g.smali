.class public final LX/B8g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1750216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1750217
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1750218
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1750219
    invoke-static {p0, p1}, LX/B8g;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1750220
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1750221
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1750222
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1750223
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1750224
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/B8g;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1750225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1750226
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1750227
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1750228
    const/4 v15, 0x0

    .line 1750229
    const/4 v14, 0x0

    .line 1750230
    const/4 v13, 0x0

    .line 1750231
    const/4 v12, 0x0

    .line 1750232
    const/4 v11, 0x0

    .line 1750233
    const/4 v10, 0x0

    .line 1750234
    const/4 v9, 0x0

    .line 1750235
    const/4 v8, 0x0

    .line 1750236
    const/4 v7, 0x0

    .line 1750237
    const/4 v6, 0x0

    .line 1750238
    const/4 v5, 0x0

    .line 1750239
    const/4 v4, 0x0

    .line 1750240
    const/4 v3, 0x0

    .line 1750241
    const/4 v2, 0x0

    .line 1750242
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1750243
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1750244
    const/4 v2, 0x0

    .line 1750245
    :goto_0
    return v2

    .line 1750246
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1750247
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_c

    .line 1750248
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1750249
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1750250
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1750251
    const-string v17, "customized_tokens"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1750252
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1750253
    :cond_2
    const-string v17, "field_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1750254
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 1750255
    :cond_3
    const-string v17, "input_domain"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1750256
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto :goto_1

    .line 1750257
    :cond_4
    const-string v17, "input_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1750258
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto :goto_1

    .line 1750259
    :cond_5
    const-string v17, "is_custom_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1750260
    const/4 v4, 0x1

    .line 1750261
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1750262
    :cond_6
    const-string v17, "is_editable"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1750263
    const/4 v3, 0x1

    .line 1750264
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 1750265
    :cond_7
    const-string v17, "is_required"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1750266
    const/4 v2, 0x1

    .line 1750267
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 1750268
    :cond_8
    const-string v17, "name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1750269
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1750270
    :cond_9
    const-string v17, "place_holder"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1750271
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1750272
    :cond_a
    const-string v17, "validation_spec"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1750273
    invoke-static/range {p0 .. p1}, LX/B8f;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1750274
    :cond_b
    const-string v17, "values"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1750275
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1750276
    :cond_c
    const/16 v16, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1750277
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1750278
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1750279
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1750280
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1750281
    if-eqz v4, :cond_d

    .line 1750282
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 1750283
    :cond_d
    if-eqz v3, :cond_e

    .line 1750284
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 1750285
    :cond_e
    if-eqz v2, :cond_f

    .line 1750286
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1750287
    :cond_f
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1750288
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1750289
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1750290
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1750291
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1750292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1750293
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1750294
    if-eqz v0, :cond_0

    .line 1750295
    const-string v0, "customized_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750296
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1750297
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750298
    if-eqz v0, :cond_1

    .line 1750299
    const-string v1, "field_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750300
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750301
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1750302
    if-eqz v0, :cond_2

    .line 1750303
    const-string v0, "input_domain"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750304
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750305
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1750306
    if-eqz v0, :cond_3

    .line 1750307
    const-string v0, "input_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750308
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750309
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750310
    if-eqz v0, :cond_4

    .line 1750311
    const-string v1, "is_custom_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750312
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750313
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750314
    if-eqz v0, :cond_5

    .line 1750315
    const-string v1, "is_editable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750316
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750317
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750318
    if-eqz v0, :cond_6

    .line 1750319
    const-string v1, "is_required"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750320
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750321
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750322
    if-eqz v0, :cond_7

    .line 1750323
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750324
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750325
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750326
    if-eqz v0, :cond_8

    .line 1750327
    const-string v1, "place_holder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750328
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750329
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1750330
    if-eqz v0, :cond_e

    .line 1750331
    const-string v1, "validation_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750332
    const/4 p3, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x1

    .line 1750333
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1750334
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1750335
    if-eqz v1, :cond_9

    .line 1750336
    const-string v2, "custom_error_message"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750337
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750338
    :cond_9
    invoke-virtual {p0, v0, v3}, LX/15i;->g(II)I

    move-result v1

    .line 1750339
    if-eqz v1, :cond_a

    .line 1750340
    const-string v1, "range"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750341
    invoke-virtual {p0, v0, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1750342
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1750343
    if-eqz v1, :cond_b

    .line 1750344
    const-string v2, "single_value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750345
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750346
    :cond_b
    invoke-virtual {p0, v0, v5}, LX/15i;->g(II)I

    move-result v1

    .line 1750347
    if-eqz v1, :cond_c

    .line 1750348
    const-string v1, "validation_condition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750349
    invoke-virtual {p0, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750350
    :cond_c
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 1750351
    if-eqz v1, :cond_d

    .line 1750352
    const-string v1, "validation_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750353
    invoke-virtual {p0, v0, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750354
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1750355
    :cond_e
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1750356
    if-eqz v0, :cond_f

    .line 1750357
    const-string v0, "values"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750358
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1750359
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1750360
    return-void
.end method
