.class public final LX/C53;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/99r;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1848288
    iput-object p1, p0, LX/C53;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iput-object p2, p0, LX/C53;->a:LX/99r;

    iput-object p3, p0, LX/C53;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x239606cc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1848289
    iget-object v1, p0, LX/C53;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v1}, LX/17S;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1848290
    iget-object v1, p0, LX/C53;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/C53;->a:LX/99r;

    invoke-virtual {v1, v2, v3}, LX/17S;->a(Landroid/content/Context;LX/99r;)V

    .line 1848291
    :goto_0
    const v1, 0x37a3ceb9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1848292
    :cond_0
    iget-object v1, p0, LX/C53;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/C53;->a:LX/99r;

    iget-object v4, p0, LX/C53;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2, v3, v4}, LX/17S;->a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
