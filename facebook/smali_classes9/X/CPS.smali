.class public final LX/CPS;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPS;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPQ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPT;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884515
    const/4 v0, 0x0

    sput-object v0, LX/CPS;->a:LX/CPS;

    .line 1884516
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPS;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884517
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884518
    new-instance v0, LX/CPT;

    invoke-direct {v0}, LX/CPT;-><init>()V

    iput-object v0, p0, LX/CPS;->c:LX/CPT;

    .line 1884519
    return-void
.end method

.method public static declared-synchronized q()LX/CPS;
    .locals 2

    .prologue
    .line 1884520
    const-class v1, LX/CPS;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPS;->a:LX/CPS;

    if-nez v0, :cond_0

    .line 1884521
    new-instance v0, LX/CPS;

    invoke-direct {v0}, LX/CPS;-><init>()V

    sput-object v0, LX/CPS;->a:LX/CPS;

    .line 1884522
    :cond_0
    sget-object v0, LX/CPS;->a:LX/CPS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884523
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1884524
    check-cast p2, LX/CPR;

    .line 1884525
    iget-object v0, p2, LX/CPR;->a:LX/CNb;

    iget-object v1, p2, LX/CPR;->b:LX/CNc;

    iget-object v2, p2, LX/CPR;->c:Ljava/util/List;

    .line 1884526
    new-instance v3, LX/CPV;

    invoke-direct {v3}, LX/CPV;-><init>()V

    .line 1884527
    sget-object v4, LX/CPW;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CPU;

    .line 1884528
    if-nez v4, :cond_0

    .line 1884529
    new-instance v4, LX/CPU;

    invoke-direct {v4}, LX/CPU;-><init>()V

    .line 1884530
    :cond_0
    invoke-static {v4, p1, v3}, LX/CPU;->a$redex0(LX/CPU;LX/1De;LX/CPV;)V

    .line 1884531
    move-object v3, v4

    .line 1884532
    move-object v3, v3

    .line 1884533
    :try_start_0
    const-string v4, "orientation"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/drawable/GradientDrawable$Orientation;->valueOf(Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-result-object v4

    .line 1884534
    if-nez v4, :cond_1

    sget-object v4, LX/CPT;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1884535
    :cond_1
    :goto_0
    move-object v4, v4

    .line 1884536
    iget-object v5, v3, LX/CPU;->a:LX/CPV;

    iput-object v4, v5, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 1884537
    iget-object v5, v3, LX/CPU;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1884538
    move-object v3, v3

    .line 1884539
    const/4 v8, 0x0

    .line 1884540
    sget-object v4, LX/CPT;->b:[Ljava/lang/String;

    array-length v4, v4

    new-array v6, v4, [I

    .line 1884541
    sget-object v9, LX/CPT;->b:[Ljava/lang/String;

    array-length p0, v9

    move v7, v8

    move v5, v8

    :goto_1
    if-ge v7, p0, :cond_2

    aget-object p2, v9, v7

    .line 1884542
    invoke-virtual {v0, p2}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1884543
    add-int/lit8 v4, v5, 0x1

    invoke-virtual {v0, p2, v8}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result p2

    aput p2, v6, v5

    .line 1884544
    :goto_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v4

    goto :goto_1

    .line 1884545
    :cond_2
    sget-object v4, LX/CPT;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v5, v4, :cond_5

    invoke-static {v6, v5}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v4

    :goto_3
    move-object v4, v4

    .line 1884546
    iget-object v5, v3, LX/CPU;->a:LX/CPV;

    iput-object v4, v5, LX/CPV;->b:[I

    .line 1884547
    iget-object v5, v3, LX/CPU;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1884548
    move-object v3, v3

    .line 1884549
    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 1884550
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1884551
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884552
    if-eqz v4, :cond_3

    .line 1884553
    const v5, 0x3e5e8898

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884554
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884555
    :cond_3
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884556
    if-eqz v4, :cond_4

    .line 1884557
    const v5, 0x23661335

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884558
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884559
    :cond_4
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1884560
    return-object v0

    .line 1884561
    :catch_0
    sget-object v4, LX/CPT;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto/16 :goto_0

    .line 1884562
    :catch_1
    sget-object v4, LX/CPT;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    goto/16 :goto_0

    :cond_5
    move-object v4, v6

    goto :goto_3

    :cond_6
    move v4, v5

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884563
    invoke-static {}, LX/1dS;->b()V

    .line 1884564
    iget v0, p1, LX/1dQ;->b:I

    .line 1884565
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884566
    :goto_0
    return-object v0

    .line 1884567
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884568
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884569
    move-object v0, v1

    .line 1884570
    goto :goto_0

    .line 1884571
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884572
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884573
    const/4 v2, 0x1

    move v2, v2

    .line 1884574
    move v0, v2

    .line 1884575
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x23661335 -> :sswitch_1
        0x3e5e8898 -> :sswitch_0
    .end sparse-switch
.end method
