.class public LX/BRr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BRn;

.field public final b:Landroid/content/res/Resources;

.field private final c:LX/0SG;

.field public final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Z

.field public i:LX/Fw2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BRn;Landroid/content/res/Resources;LX/0SG;LX/0zw;)V
    .locals 1
    .param p4    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BRn;",
            "Landroid/content/res/Resources;",
            "LX/0SG;",
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784529
    const/4 v0, 0x0

    iput v0, p0, LX/BRr;->g:I

    .line 1784530
    iput-object p1, p0, LX/BRr;->a:LX/BRn;

    .line 1784531
    iput-object p2, p0, LX/BRr;->b:Landroid/content/res/Resources;

    .line 1784532
    iput-object p3, p0, LX/BRr;->c:LX/0SG;

    .line 1784533
    iput-object p4, p0, LX/BRr;->d:LX/0zw;

    .line 1784534
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1784535
    iget-object v0, p0, LX/BRr;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1784536
    return-void
.end method

.method public final a(LX/BQ1;)V
    .locals 6

    .prologue
    .line 1784537
    iget-object v0, p0, LX/BRr;->a:LX/BRn;

    iget-object v1, p0, LX/BRr;->d:LX/0zw;

    .line 1784538
    iget-object v2, v0, LX/BRn;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11R;

    sget-object v3, LX/1lB;->DAY_HOUR_FUTURE_STYLE:LX/1lB;

    invoke-virtual {p1}, LX/BQ1;->y()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    .line 1784539
    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    invoke-virtual {v2, v3}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->setTimerText(Ljava/lang/CharSequence;)V

    .line 1784540
    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    iget-object v3, v0, LX/BRn;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0wM;

    const v4, 0x7f02190e

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->setTimerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1784541
    iget-object v0, p0, LX/BRr;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    .line 1784542
    iget-object v1, p0, LX/BRr;->e:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    .line 1784543
    new-instance v1, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;-><init>(LX/BRr;)V

    iput-object v1, p0, LX/BRr;->e:Ljava/lang/Runnable;

    .line 1784544
    :cond_0
    iget-object v1, p0, LX/BRr;->e:Ljava/lang/Runnable;

    move-object v1, v1

    .line 1784545
    invoke-virtual {v0, v1}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->post(Ljava/lang/Runnable;)Z

    .line 1784546
    const/4 v0, 0x1

    iput v0, p0, LX/BRr;->g:I

    .line 1784547
    return-void
.end method

.method public final a(LX/Fw2;)V
    .locals 0

    .prologue
    .line 1784548
    iput-object p1, p0, LX/BRr;->i:LX/Fw2;

    .line 1784549
    return-void
.end method

.method public final a(LX/5SB;LX/BQ1;ZZ)Z
    .locals 4

    .prologue
    .line 1784550
    if-eqz p1, :cond_0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    invoke-virtual {p1}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/BQ1;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/BQ1;->y()J

    move-result-wide v0

    iget-object v2, p0, LX/BRr;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1784551
    iget v0, p0, LX/BRr;->g:I

    return v0
.end method
