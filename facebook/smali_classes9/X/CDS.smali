.class public final LX/CDS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Iv;


# instance fields
.field public final synthetic a:LX/2oL;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:LX/CDU;


# direct methods
.method public constructor <init>(LX/CDU;LX/2oL;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1859273
    iput-object p1, p0, LX/CDS;->d:LX/CDU;

    iput-object p2, p0, LX/CDS;->a:LX/2oL;

    iput-object p3, p0, LX/CDS;->b:LX/1Pq;

    iput-object p4, p0, LX/CDS;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2oN;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1859274
    iget-object v0, p0, LX/CDS;->d:LX/CDU;

    iget-object v0, v0, LX/CDU;->f:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_1

    .line 1859275
    :cond_0
    :goto_0
    return-void

    .line 1859276
    :cond_1
    iget-object v0, p0, LX/CDS;->a:LX/2oL;

    .line 1859277
    iget-object v3, v0, LX/2oL;->h:LX/2oN;

    move-object v3, v3

    .line 1859278
    if-eq v3, p1, :cond_0

    .line 1859279
    iget-object v0, p0, LX/CDS;->a:LX/2oL;

    .line 1859280
    iput-object p1, v0, LX/2oL;->h:LX/2oN;

    .line 1859281
    sget-object v0, LX/2oN;->TRANSITION:LX/2oN;

    if-ne p1, v0, :cond_3

    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-ne v3, v0, :cond_3

    move v0, v1

    .line 1859282
    :goto_1
    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-ne p1, v4, :cond_4

    sget-object v4, LX/2oN;->NONE:LX/2oN;

    if-eq v3, v4, :cond_4

    move v3, v1

    .line 1859283
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    .line 1859284
    :cond_2
    iget-object v0, p0, LX/CDS;->b:LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, LX/CDS;->c:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1859285
    goto :goto_1

    :cond_4
    move v3, v2

    .line 1859286
    goto :goto_2
.end method
