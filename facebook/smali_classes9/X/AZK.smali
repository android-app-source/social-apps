.class public LX/AZK;
.super LX/AWT;
.source ""

# interfaces
.implements LX/AZD;


# instance fields
.field public A:Landroid/widget/LinearLayout;

.field public B:Lcom/facebook/widget/CustomRelativeLayout;

.field public C:Lcom/facebook/fbui/glyph/GlyphView;

.field public D:Lcom/facebook/fbui/glyph/GlyphView;

.field public E:LX/AYN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;",
            ">;"
        }
    .end annotation
.end field

.field public H:I

.field public I:Ljava/lang/String;

.field public J:Z

.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;

.field public k:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;

.field public l:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

.field public m:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;

.field public n:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingViolation;

.field public o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

.field public p:Lcom/facebook/resources/ui/FbButton;

.field public q:Lcom/facebook/widget/text/BetterTextView;

.field public r:Lcom/facebook/widget/text/BetterTextView;

.field public s:Lcom/facebook/widget/text/BetterTextView;

.field public t:Lcom/facebook/widget/text/BetterTextView;

.field public u:Lcom/facebook/widget/text/BetterTextView;

.field public v:Lcom/facebook/widget/text/BetterTextView;

.field public w:Lcom/facebook/resources/ui/FbButton;

.field public x:Lcom/facebook/resources/ui/FbButton;

.field public y:Landroid/widget/ImageView;

.field public z:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686878
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AZK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686879
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686910
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AZK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686911
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686880
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686881
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AZK;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v4

    check-cast v4, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v5

    check-cast v5, LX/48V;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object p1

    check-cast p1, LX/0WJ;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object p2

    check-cast p2, LX/0WJ;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object p3

    check-cast p3, LX/0lC;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    iput-object v3, v2, LX/AZK;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, LX/AZK;->b:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iput-object v5, v2, LX/AZK;->c:LX/48V;

    iput-object p1, v2, LX/AZK;->f:LX/0WJ;

    iput-object p2, v2, LX/AZK;->g:LX/0WJ;

    iput-object p3, v2, LX/AZK;->h:LX/0lC;

    iput-object v0, v2, LX/AZK;->i:LX/17Y;

    .line 1686882
    const v0, 0x7f0305b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686883
    const v0, 0x7f0d0fa2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AZK;->A:Landroid/widget/LinearLayout;

    .line 1686884
    const v0, 0x7f0d0fa5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;

    iput-object v0, p0, LX/AZK;->j:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;

    .line 1686885
    const v0, 0x7f0d0f9e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 1686886
    const v0, 0x7f0d0fbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 1686887
    const v0, 0x7f0d0fc0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->v:Lcom/facebook/widget/text/BetterTextView;

    .line 1686888
    const v0, 0x7f0d0f9f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 1686889
    const v0, 0x7f0d0fa0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->u:Lcom/facebook/widget/text/BetterTextView;

    .line 1686890
    const v0, 0x7f0d0fa1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AZK;->p:Lcom/facebook/resources/ui/FbButton;

    .line 1686891
    const v0, 0x7f0d0fae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1686892
    const v0, 0x7f0d0fa6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;

    iput-object v0, p0, LX/AZK;->k:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;

    .line 1686893
    const v0, 0x7f0d0fb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AZK;->w:Lcom/facebook/resources/ui/FbButton;

    .line 1686894
    const v0, 0x7f0d0fa7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

    iput-object v0, p0, LX/AZK;->l:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

    .line 1686895
    const v0, 0x7f0d0fa8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;

    iput-object v0, p0, LX/AZK;->m:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;

    .line 1686896
    const v0, 0x7f0d0fbc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZK;->z:Lcom/facebook/widget/text/BetterTextView;

    .line 1686897
    const v0, 0x7f0d0fbb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    iput-object v0, p0, LX/AZK;->o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    .line 1686898
    iget-object v0, p0, LX/AZK;->o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    invoke-virtual {v0, p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->a(LX/AZD;)V

    .line 1686899
    const v0, 0x7f0d0fa4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingViolation;

    iput-object v0, p0, LX/AZK;->n:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingViolation;

    .line 1686900
    const v0, 0x7f0d0fa3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/AZK;->y:Landroid/widget/ImageView;

    .line 1686901
    const v0, 0x7f0d0fb8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AZK;->x:Lcom/facebook/resources/ui/FbButton;

    .line 1686902
    const v0, 0x7f0d0fb2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomRelativeLayout;

    iput-object v0, p0, LX/AZK;->B:Lcom/facebook/widget/CustomRelativeLayout;

    .line 1686903
    const v0, 0x7f0d0f9c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AZK;->C:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686904
    const v0, 0x7f0d0f9d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AZK;->D:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686905
    iget-object v0, p0, LX/AZK;->p:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AZE;

    invoke-direct {v1, p0}, LX/AZE;-><init>(LX/AZK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686906
    iget-object v0, p0, LX/AZK;->w:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AZF;

    invoke-direct {v1, p0}, LX/AZF;-><init>(LX/AZK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686907
    iget-object v0, p0, LX/AZK;->y:Landroid/widget/ImageView;

    new-instance v1, LX/AZG;

    invoke-direct {v1, p0}, LX/AZG;-><init>(LX/AZK;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686908
    iget-object v0, p0, LX/AZK;->x:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AZH;

    invoke-direct {v1, p0}, LX/AZH;-><init>(LX/AZK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686909
    return-void
.end method

.method public static synthetic b(LX/AZK;)I
    .locals 2

    .prologue
    .line 1686839
    iget v0, p0, LX/AZK;->H:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/AZK;->H:I

    return v0
.end method

.method public static l(LX/AZK;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1686840
    iget-object v0, p0, LX/AZK;->G:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AZK;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/AZK;->H:I

    if-gt v0, v1, :cond_2

    .line 1686841
    :cond_0
    iget-object v0, p0, LX/AZK;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1686842
    :cond_1
    :goto_0
    return-void

    .line 1686843
    :cond_2
    const/16 v1, 0x8

    .line 1686844
    iget-object v0, p0, LX/AZK;->j:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;->setVisibility(I)V

    .line 1686845
    iget-object v0, p0, LX/AZK;->k:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;->setVisibility(I)V

    .line 1686846
    iget-object v0, p0, LX/AZK;->l:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->setVisibility(I)V

    .line 1686847
    iget-object v0, p0, LX/AZK;->m:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;->setVisibility(I)V

    .line 1686848
    iget-object v0, p0, LX/AZK;->C:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->clearAnimation()V

    .line 1686849
    iget-object v0, p0, LX/AZK;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->clearAnimation()V

    .line 1686850
    iget-object v0, p0, LX/AZK;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1686851
    iget-object v0, p0, LX/AZK;->G:Ljava/util/List;

    iget v1, p0, LX/AZK;->H:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    .line 1686852
    sget-object v1, LX/AZJ;->a:[I

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1686853
    :pswitch_0
    const/high16 v6, 0x41a00000    # 20.0f

    const/high16 v5, 0x41000000    # 8.0f

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v3, 0x41700000    # 15.0f

    .line 1686854
    invoke-virtual {p0}, LX/AZK;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1686855
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x50

    if-lt v1, v2, :cond_3

    .line 1686856
    iget-object v0, p0, LX/AZK;->r:Lcom/facebook/widget/text/BetterTextView;

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686857
    iget-object v0, p0, LX/AZK;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686858
    iget-object v0, p0, LX/AZK;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686859
    iget-object v0, p0, LX/AZK;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbButton;->setTextSize(F)V

    .line 1686860
    :goto_1
    iget-object v0, p0, LX/AZK;->j:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingEntryView;->setVisibility(I)V

    .line 1686861
    iget-object v0, p0, LX/AZK;->C:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AZK;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040061

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1686862
    iget-object v0, p0, LX/AZK;->D:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AZK;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040062

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 1686863
    :pswitch_1
    iget-object v0, p0, LX/AZK;->k:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;

    invoke-virtual {v0, v2}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingHowItWorks;->setVisibility(I)V

    .line 1686864
    iget-object v0, p0, LX/AZK;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_1

    .line 1686865
    iget-object v0, p0, LX/AZK;->w:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1686866
    iget-object v0, p0, LX/AZK;->B:Lcom/facebook/widget/CustomRelativeLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomRelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1686867
    :pswitch_2
    iget-object v0, p0, LX/AZK;->l:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

    invoke-virtual {v0, v2}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->setVisibility(I)V

    goto/16 :goto_0

    .line 1686868
    :pswitch_3
    iget-object v0, p0, LX/AZK;->m:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;

    invoke-virtual {v0, v2}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingPayments;->setVisibility(I)V

    goto/16 :goto_0

    .line 1686869
    :cond_3
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_4

    .line 1686870
    iget-object v0, p0, LX/AZK;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686871
    iget-object v0, p0, LX/AZK;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686872
    iget-object v0, p0, LX/AZK;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686873
    iget-object v0, p0, LX/AZK;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setTextSize(F)V

    goto :goto_1

    .line 1686874
    :cond_4
    iget-object v0, p0, LX/AZK;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686875
    iget-object v0, p0, LX/AZK;->s:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686876
    iget-object v0, p0, LX/AZK;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1686877
    iget-object v0, p0, LX/AZK;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbButton;->setTextSize(F)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setCookies(LX/AZK;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1686822
    iget-object v0, p0, LX/AZK;->g:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1686823
    :cond_0
    :goto_0
    return-void

    .line 1686824
    :cond_1
    iget-object v0, p0, LX/AZK;->g:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1686825
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1686826
    if-eqz v0, :cond_0

    .line 1686827
    iget-object v1, p0, LX/AZK;->h:LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 1686828
    if-eqz v3, :cond_0

    .line 1686829
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 1686830
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 1686831
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flush()V

    move-object v1, v0

    .line 1686832
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 1686833
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 1686834
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1686835
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1686836
    :cond_2
    invoke-virtual {p0}, LX/AZK;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1686837
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1686838
    :cond_3
    iget-object v0, p0, LX/AZK;->f:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->i()V

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1686819
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/AZK;->setVisibility(I)V

    .line 1686820
    iget-object v0, p0, LX/AZK;->E:LX/AYN;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/AYN;->c(Z)V

    .line 1686821
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1686818
    iget-object v0, p0, LX/AZK;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iH_()V
    .locals 2

    .prologue
    .line 1686816
    iget-object v0, p0, LX/AZK;->z:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingFlowPlugin$6;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingFlowPlugin$6;-><init>(LX/AZK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686817
    return-void
.end method
