.class public final LX/Auo;
.super LX/9dh;
.source ""


# instance fields
.field public final synthetic a:LX/Aur;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(LX/Aur;)V
    .locals 1

    .prologue
    .line 1723249
    iput-object p1, p0, LX/Auo;->a:LX/Aur;

    invoke-direct {p0}, LX/9dh;-><init>()V

    .line 1723250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Auo;->b:Ljava/util/List;

    .line 1723251
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Auo;->c:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1723232
    iget-object v0, p0, LX/Auo;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getRotation()F

    move-result v0

    .line 1723233
    const/high16 v9, 0x40a00000    # 5.0f

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 1723234
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1723235
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v6, :cond_3

    move v2, v4

    move v5, v4

    .line 1723236
    :goto_0
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_1

    .line 1723237
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    add-int/lit8 v8, v2, 0x1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v7, v1

    if-lez v1, :cond_6

    .line 1723238
    add-int/lit8 v5, v5, 0x1

    .line 1723239
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v9

    if-ltz v1, :cond_0

    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_6

    :cond_0
    move v4, v6

    .line 1723240
    :cond_1
    iget-object v1, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v5, v1, :cond_2

    if-ne v4, v6, :cond_3

    .line 1723241
    :cond_2
    iput-boolean v6, p0, LX/Auo;->c:Z

    .line 1723242
    :cond_3
    iget-boolean v1, p0, LX/Auo;->c:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1723243
    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 1723244
    iget-object v0, p0, LX/Auo;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1723245
    iget-object v0, p0, LX/Auo;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1723246
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Auo;->c:Z

    .line 1723247
    :cond_5
    return-void

    .line 1723248
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final a(LX/9e2;)Z
    .locals 1

    .prologue
    .line 1723255
    const/4 v0, 0x1

    return v0
.end method

.method public final b(LX/9e2;)Z
    .locals 3

    .prologue
    .line 1723252
    invoke-virtual {p1}, LX/9e2;->c()F

    move-result v0

    .line 1723253
    iget-object v1, p0, LX/Auo;->a:LX/Aur;

    iget-object v1, v1, LX/Aur;->b:Landroid/widget/ImageView;

    iget-object v2, p0, LX/Auo;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getRotation()F

    move-result v2

    sub-float v0, v2, v0

    const/high16 v2, 0x43b40000    # 360.0f

    rem-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1723254
    const/4 v0, 0x1

    return v0
.end method
