.class public LX/AZ4;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/AYr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field public final k:Landroid/widget/LinearLayout;

.field public final l:LX/AZ2;

.field public final m:Landroid/widget/ProgressBar;

.field public final n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public final o:Lcom/facebook/widget/text/BetterTextView;

.field public final p:LX/AZ3;

.field public q:LX/AY9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:I

.field public u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686447
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AZ4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686448
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686449
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AZ4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686450
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686451
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686452
    new-instance v0, LX/AZ2;

    invoke-direct {v0, p0}, LX/AZ2;-><init>(LX/AZ4;)V

    iput-object v0, p0, LX/AZ4;->l:LX/AZ2;

    .line 1686453
    new-instance v0, LX/AZ3;

    invoke-direct {v0, p0}, LX/AZ3;-><init>(LX/AZ4;)V

    iput-object v0, p0, LX/AZ4;->p:LX/AZ3;

    .line 1686454
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AZ4;

    invoke-static {v0}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object v3

    check-cast v3, LX/AYq;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object p1

    check-cast p1, LX/3HT;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p2

    check-cast p2, LX/0Uh;

    invoke-static {v0}, LX/AYr;->a(LX/0QB;)LX/AYr;

    move-result-object p3

    check-cast p3, LX/AYr;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    check-cast v0, LX/1b4;

    iput-object v3, v2, LX/AZ4;->a:LX/AYq;

    iput-object v4, v2, LX/AZ4;->b:LX/0hB;

    iput-object p1, v2, LX/AZ4;->c:LX/3HT;

    iput-object p2, v2, LX/AZ4;->f:LX/0Uh;

    iput-object p3, v2, LX/AZ4;->g:LX/AYr;

    iput-object v0, v2, LX/AZ4;->h:LX/1b4;

    .line 1686455
    const v0, 0x7f0305b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686456
    const v0, 0x7f0d0fc1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    .line 1686457
    const v0, 0x7f0d0fc4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/AZ4;->i:Landroid/widget/TextView;

    .line 1686458
    const v0, 0x7f0d0fc3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/AZ4;->j:Landroid/widget/TextView;

    .line 1686459
    const v0, 0x7f0d0fc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1686460
    const v1, 0x7f0d0fc5

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v1, p0, LX/AZ4;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1686461
    const v1, 0x7f0d0fc7

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, LX/AZ4;->m:Landroid/widget/ProgressBar;

    .line 1686462
    const v1, 0x7f0d0fc2

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/AZ4;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1686463
    new-instance v1, LX/AZ0;

    invoke-direct {v1, p0}, LX/AZ0;-><init>(LX/AZ4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686464
    iget-object v0, p0, LX/AZ4;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v1, LX/AZ1;

    invoke-direct {v1, p0}, LX/AZ1;-><init>(LX/AZ4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686465
    return-void
.end method

.method public static h(LX/AZ4;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1686466
    iget-object v0, p0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v0, :cond_0

    .line 1686467
    :goto_0
    return-void

    .line 1686468
    :cond_0
    iget-object v0, p0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    int-to-long v4, v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v0, v4

    .line 1686469
    int-to-float v0, v0

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v3, v0, 0x5

    .line 1686470
    iget-object v0, p0, LX/AZ4;->h:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v0}, LX/AZN;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1686471
    :goto_1
    iget-object v4, p0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-static {v4}, LX/AZN;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1686472
    new-instance v4, LX/AZN;

    iget v5, p0, LX/AZ4;->s:I

    iget-object v6, p0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-direct {v4, v5, v6}, LX/AZN;-><init>(ILcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)V

    .line 1686473
    iget v5, v4, LX/AZN;->a:I

    move v5, v5

    .line 1686474
    iput v5, p0, LX/AZ4;->t:I

    .line 1686475
    iget v5, v4, LX/AZN;->b:I

    move v4, v5

    .line 1686476
    iput v4, p0, LX/AZ4;->u:I

    .line 1686477
    :cond_1
    if-eqz v0, :cond_3

    .line 1686478
    iget-object v0, p0, LX/AZ4;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/AZ4;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ccc

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "$"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, LX/AZ4;->u:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "$"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LX/AZ4;->t:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686479
    iget-object v0, p0, LX/AZ4;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/AZ4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080ce1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 1686480
    goto :goto_1

    .line 1686481
    :cond_3
    iget-object v0, p0, LX/AZ4;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/AZ4;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080cca

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686482
    iget-object v0, p0, LX/AZ4;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/AZ4;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080ce0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public static j(LX/AZ4;)V
    .locals 2

    .prologue
    .line 1686483
    iget-object v0, p0, LX/AZ4;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1686484
    iget-object v0, p0, LX/AZ4;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setEnabled(Z)V

    .line 1686485
    iget-object v0, p0, LX/AZ4;->o:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686486
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1686487
    iget-object v0, p0, LX/AZ4;->a:LX/AYq;

    .line 1686488
    iget-object v1, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v1

    .line 1686489
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_PROMPT:LX/AYp;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/AZ4;->q:LX/AY9;

    if-eqz v0, :cond_0

    .line 1686490
    iget-object v0, p0, LX/AZ4;->q:LX/AY9;

    invoke-virtual {v0}, LX/AY9;->c()V

    .line 1686491
    iget-object v0, p0, LX/AZ4;->p:LX/AZ3;

    invoke-virtual {v0}, LX/AZ3;->cancel()V

    .line 1686492
    const/4 v0, 0x1

    .line 1686493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
