.class public LX/C5D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/C5G;


# direct methods
.method public constructor <init>(LX/C5G;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1848535
    iput-object p1, p0, LX/C5D;->a:LX/C5G;

    .line 1848536
    return-void
.end method

.method public static a(LX/0QB;)LX/C5D;
    .locals 4

    .prologue
    .line 1848537
    const-class v1, LX/C5D;

    monitor-enter v1

    .line 1848538
    :try_start_0
    sget-object v0, LX/C5D;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848539
    sput-object v2, LX/C5D;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848540
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848541
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848542
    new-instance p0, LX/C5D;

    invoke-static {v0}, LX/C5G;->a(LX/0QB;)LX/C5G;

    move-result-object v3

    check-cast v3, LX/C5G;

    invoke-direct {p0, v3}, LX/C5D;-><init>(LX/C5G;)V

    .line 1848543
    move-object v0, p0

    .line 1848544
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848545
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848546
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
