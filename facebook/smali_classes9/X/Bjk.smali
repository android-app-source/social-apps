.class public LX/Bjk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Bjk;


# instance fields
.field public final a:LX/0Yj;

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1812775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1812776
    iput-object p1, p0, LX/Bjk;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1812777
    new-instance v0, LX/0Yj;

    const v1, 0x60011

    const-string v2, "EventComposerTTI"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "event_composer"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 1812778
    iput-boolean v4, v0, LX/0Yj;->n:Z

    .line 1812779
    move-object v0, v0

    .line 1812780
    iput-object v0, p0, LX/Bjk;->a:LX/0Yj;

    .line 1812781
    return-void
.end method

.method public static a(LX/0QB;)LX/Bjk;
    .locals 4

    .prologue
    .line 1812782
    sget-object v0, LX/Bjk;->c:LX/Bjk;

    if-nez v0, :cond_1

    .line 1812783
    const-class v1, LX/Bjk;

    monitor-enter v1

    .line 1812784
    :try_start_0
    sget-object v0, LX/Bjk;->c:LX/Bjk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1812785
    if-eqz v2, :cond_0

    .line 1812786
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1812787
    new-instance p0, LX/Bjk;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/Bjk;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1812788
    move-object v0, p0

    .line 1812789
    sput-object v0, LX/Bjk;->c:LX/Bjk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1812790
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1812791
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1812792
    :cond_1
    sget-object v0, LX/Bjk;->c:LX/Bjk;

    return-object v0

    .line 1812793
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1812794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
