.class public LX/C5K;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5O;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5K",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C5O;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848714
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1848715
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C5K;->b:LX/0Zi;

    .line 1848716
    iput-object p1, p0, LX/C5K;->a:LX/0Ot;

    .line 1848717
    return-void
.end method

.method public static a(LX/0QB;)LX/C5K;
    .locals 4

    .prologue
    .line 1848718
    const-class v1, LX/C5K;

    monitor-enter v1

    .line 1848719
    :try_start_0
    sget-object v0, LX/C5K;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848720
    sput-object v2, LX/C5K;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848721
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848722
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848723
    new-instance v3, LX/C5K;

    const/16 p0, 0x1f68

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C5K;-><init>(LX/0Ot;)V

    .line 1848724
    move-object v0, v3

    .line 1848725
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848726
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848727
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1848729
    check-cast p2, LX/C5J;

    .line 1848730
    iget-object v0, p0, LX/C5K;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5O;

    iget-object v1, p2, LX/C5J;->a:LX/1Pq;

    iget-object v2, p2, LX/C5J;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848731
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848732
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1848733
    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848734
    iget-object v3, v0, LX/C5O;->c:LX/AnA;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/AnA;->a(Ljava/lang/Class;)LX/An9;

    move-result-object v4

    .line 1848735
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v3

    new-instance v5, LX/C5N;

    invoke-direct {v5, v0, v6}, LX/C5N;-><init>(LX/C5O;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 1848736
    iput-object v5, v3, LX/3mP;->g:LX/25K;

    .line 1848737
    move-object v5, v3

    .line 1848738
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848739
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 1848740
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 1848741
    move-object v5, v5

    .line 1848742
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848743
    check-cast v3, LX/0jW;

    .line 1848744
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 1848745
    move-object v3, v5

    .line 1848746
    invoke-virtual {v4}, LX/An9;->b()I

    move-result v4

    .line 1848747
    iput v4, v3, LX/3mP;->b:I

    .line 1848748
    move-object v3, v3

    .line 1848749
    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1848750
    iput-object v4, v3, LX/3mP;->c:Ljava/lang/Integer;

    .line 1848751
    move-object v3, v3

    .line 1848752
    iput-object v2, v3, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848753
    move-object v3, v3

    .line 1848754
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v9

    .line 1848755
    iget-object v3, v0, LX/C5O;->b:LX/C5M;

    invoke-interface {v6}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v4, p1

    move-object v5, v2

    move-object v8, v1

    invoke-virtual/range {v3 .. v9}, LX/C5M;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/0Px;Ljava/lang/Object;LX/25M;)LX/C5L;

    move-result-object v3

    .line 1848756
    iget-object v4, v0, LX/C5O;->a:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    const v5, 0x7f0b09ac

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1848757
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1848758
    invoke-static {}, LX/1dS;->b()V

    .line 1848759
    const/4 v0, 0x0

    return-object v0
.end method
