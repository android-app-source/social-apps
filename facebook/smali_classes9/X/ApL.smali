.class public LX/ApL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApK;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ApM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715863
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApL;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ApM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715864
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715865
    iput-object p1, p0, LX/ApL;->b:LX/0Ot;

    .line 1715866
    return-void
.end method

.method public static a(LX/0QB;)LX/ApL;
    .locals 4

    .prologue
    .line 1715867
    const-class v1, LX/ApL;

    monitor-enter v1

    .line 1715868
    :try_start_0
    sget-object v0, LX/ApL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715869
    sput-object v2, LX/ApL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715870
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715871
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715872
    new-instance v3, LX/ApL;

    const/16 p0, 0x21fc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ApL;-><init>(LX/0Ot;)V

    .line 1715873
    move-object v0, v3

    .line 1715874
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715875
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715876
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1715878
    check-cast p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715879
    iget-object v0, p0, LX/ApL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ApM;

    iget v2, p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->a:I

    iget-object v3, p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    iget-object v4, p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/ApM;->a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1715880
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715881
    invoke-static {}, LX/1dS;->b()V

    .line 1715882
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/ApK;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1715883
    new-instance v1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;-><init>(LX/ApL;)V

    .line 1715884
    sget-object v2, LX/ApL;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ApK;

    .line 1715885
    if-nez v2, :cond_0

    .line 1715886
    new-instance v2, LX/ApK;

    invoke-direct {v2}, LX/ApK;-><init>()V

    .line 1715887
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/ApK;->a$redex0(LX/ApK;LX/1De;IILcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;)V

    .line 1715888
    move-object v1, v2

    .line 1715889
    move-object v0, v1

    .line 1715890
    return-object v0
.end method
