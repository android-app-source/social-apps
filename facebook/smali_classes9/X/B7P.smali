.class public final enum LX/B7P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B7P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B7P;

.field public static final enum CALL:LX/B7P;

.field public static final enum MESSENGER:LX/B7P;

.field public static final enum NONE:LX/B7P;

.field public static final enum WEBSITE:LX/B7P;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1748006
    new-instance v0, LX/B7P;

    const-string v1, "WEBSITE"

    invoke-direct {v0, v1, v2}, LX/B7P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B7P;->WEBSITE:LX/B7P;

    .line 1748007
    new-instance v0, LX/B7P;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v3}, LX/B7P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B7P;->MESSENGER:LX/B7P;

    .line 1748008
    new-instance v0, LX/B7P;

    const-string v1, "CALL"

    invoke-direct {v0, v1, v4}, LX/B7P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B7P;->CALL:LX/B7P;

    .line 1748009
    new-instance v0, LX/B7P;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/B7P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B7P;->NONE:LX/B7P;

    .line 1748010
    const/4 v0, 0x4

    new-array v0, v0, [LX/B7P;

    sget-object v1, LX/B7P;->WEBSITE:LX/B7P;

    aput-object v1, v0, v2

    sget-object v1, LX/B7P;->MESSENGER:LX/B7P;

    aput-object v1, v0, v3

    sget-object v1, LX/B7P;->CALL:LX/B7P;

    aput-object v1, v0, v4

    sget-object v1, LX/B7P;->NONE:LX/B7P;

    aput-object v1, v0, v5

    sput-object v0, LX/B7P;->$VALUES:[LX/B7P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1748003
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B7P;
    .locals 1

    .prologue
    .line 1748004
    const-class v0, LX/B7P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B7P;

    return-object v0
.end method

.method public static values()[LX/B7P;
    .locals 1

    .prologue
    .line 1748005
    sget-object v0, LX/B7P;->$VALUES:[LX/B7P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B7P;

    return-object v0
.end method
