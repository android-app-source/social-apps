.class public LX/CMM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/lang/Runnable;

.field public c:LX/CM5;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880540
    new-instance v0, Lcom/facebook/messaging/emoji/KeyRepeaterTouchListener$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/emoji/KeyRepeaterTouchListener$1;-><init>(LX/CMM;)V

    iput-object v0, p0, LX/CMM;->b:Ljava/lang/Runnable;

    .line 1880541
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/CMM;->a:Landroid/os/Handler;

    .line 1880542
    return-void
.end method

.method public static b(LX/CMM;)V
    .locals 2

    .prologue
    .line 1880543
    iget-object v0, p0, LX/CMM;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/CMM;->b:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1880544
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1880545
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1880546
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 1880547
    :pswitch_1
    invoke-static {p0}, LX/CMM;->b(LX/CMM;)V

    .line 1880548
    iget-object v1, p0, LX/CMM;->a:Landroid/os/Handler;

    iget-object v2, p0, LX/CMM;->b:Ljava/lang/Runnable;

    const-wide/16 v3, 0x190

    const v5, -0xa0a397f

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1880549
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0

    .line 1880550
    :pswitch_2
    invoke-static {p0}, LX/CMM;->b(LX/CMM;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
