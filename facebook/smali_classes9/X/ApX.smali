.class public LX/ApX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# static fields
.field private static final a:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static final b:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static final c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1716176
    const v0, 0x7f0e0129

    sput v0, LX/ApX;->a:I

    .line 1716177
    const v0, 0x7f0e012b

    sput v0, LX/ApX;->b:I

    .line 1716178
    const v0, 0x7f0e012d

    sput v0, LX/ApX;->c:I

    .line 1716179
    const/4 v0, 0x0

    sput v0, LX/ApX;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1716180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(II)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1716181
    if-ne p0, v0, :cond_0

    .line 1716182
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 1716183
    :goto_0
    :pswitch_0
    return v0

    .line 1716184
    :cond_0
    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    .line 1716185
    packed-switch p1, :pswitch_data_1

    move v0, v1

    .line 1716186
    goto :goto_0

    .line 1716187
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid number of title lines = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/1De;III)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1716188
    invoke-static {p0, v2, p3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    const-string v1, "\n\n\n"

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->i(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1716189
    new-instance v1, LX/1no;

    invoke-direct {v1}, LX/1no;-><init>()V

    .line 1716190
    invoke-static {v2, v2}, LX/1mh;->a(II)I

    move-result v2

    invoke-virtual {v0, p0, p1, v2, v1}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1716191
    iget v0, v1, LX/1no;->b:I

    return v0
.end method

.method public static a(LX/1De;IILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/1Dg;
    .locals 7
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1716192
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "title can not be empty"

    invoke-static {v1}, LX/0zj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1716194
    :cond_0
    sget v0, LX/ApX;->a:I

    const/4 v1, 0x2

    invoke-static {p0, p3, v0, v1}, LX/ApX;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v0

    .line 1716195
    if-nez v0, :cond_5

    const/4 v0, 0x0

    move-object v3, v0

    .line 1716196
    :goto_0
    if-eqz v3, :cond_1

    invoke-static {p0, v3, p1, p2}, LX/ApX;->a(LX/1De;LX/1X1;II)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    move v1, v0

    .line 1716197
    :goto_1
    if-eqz v1, :cond_7

    const/4 v0, 0x1

    move v2, v0

    .line 1716198
    :goto_2
    if-eqz v1, :cond_8

    const/4 v0, 0x2

    .line 1716199
    :goto_3
    sget v1, LX/ApX;->b:I

    invoke-static {p0, p4, v1, v0}, LX/ApX;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v4

    .line 1716200
    if-nez v4, :cond_9

    const/4 v0, 0x0

    move-object v1, v0

    .line 1716201
    :goto_4
    if-eqz v4, :cond_2

    invoke-static {p0, v1, p1, p2}, LX/ApX;->a(LX/1De;LX/1X1;II)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_2
    const/4 v0, 0x1

    .line 1716202
    :goto_5
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v0, 0x0

    .line 1716203
    :goto_6
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b1169

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    const/4 v0, 0x0

    :goto_7
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    .line 1716204
    if-eqz p6, :cond_4

    .line 1716205
    sget v1, LX/ApX;->d:I

    if-nez v1, :cond_3

    .line 1716206
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1716207
    const v2, 0x7f0b1169

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    .line 1716208
    const/4 v2, 0x2

    sget v3, LX/ApX;->a:I

    invoke-static {p0, p1, v2, v3}, LX/ApX;->a(LX/1De;III)I

    move-result v2

    .line 1716209
    const/4 v3, 0x1

    sget v4, LX/ApX;->c:I

    invoke-static {p0, p1, v3, v4}, LX/ApX;->a(LX/1De;III)I

    move-result v3

    .line 1716210
    add-int/2addr v1, v2

    add-int/2addr v1, v3

    sput v1, LX/ApX;->d:I

    .line 1716211
    :cond_3
    sget v1, LX/ApX;->d:I

    invoke-interface {v0, v1}, LX/1Dh;->I(I)LX/1Dh;

    .line 1716212
    :cond_4
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1716213
    :cond_5
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_0

    .line 1716214
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    .line 1716215
    :cond_7
    const/4 v0, 0x2

    move v2, v0

    goto/16 :goto_2

    .line 1716216
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 1716217
    :cond_9
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_4

    .line 1716218
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 1716219
    :cond_b
    if-eqz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_6

    :cond_c
    const/4 v0, 0x2

    goto :goto_6

    .line 1716220
    :cond_d
    sget v3, LX/ApX;->c:I

    invoke-static {v2, v0}, LX/ApX;->a(II)I

    move-result v0

    invoke-static {p0, p5, v3, v0}, LX/ApX;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v0

    goto :goto_7
.end method

.method private static a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1716221
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/1De;LX/1X1;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716222
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v2

    .line 1716223
    if-nez v2, :cond_1

    .line 1716224
    :cond_0
    :goto_0
    return v0

    .line 1716225
    :cond_1
    new-instance v2, LX/1no;

    invoke-direct {v2}, LX/1no;-><init>()V

    .line 1716226
    invoke-static {v1, v1}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {p1, p0, v3, p3, v2}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1716227
    iget v2, v2, LX/1no;->a:I

    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v3

    if-le v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
