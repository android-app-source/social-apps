.class public final enum LX/CH3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CH3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CH3;

.field public static final enum FINAL_IMAGE:LX/CH3;

.field public static final enum INTERMEDIATE_IMAGE:LX/CH3;

.field public static final enum NONE:LX/CH3;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1866149
    new-instance v0, LX/CH3;

    const-string v1, "FINAL_IMAGE"

    invoke-direct {v0, v1, v2}, LX/CH3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH3;->FINAL_IMAGE:LX/CH3;

    .line 1866150
    new-instance v0, LX/CH3;

    const-string v1, "INTERMEDIATE_IMAGE"

    invoke-direct {v0, v1, v3}, LX/CH3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH3;->INTERMEDIATE_IMAGE:LX/CH3;

    .line 1866151
    new-instance v0, LX/CH3;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/CH3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH3;->NONE:LX/CH3;

    .line 1866152
    const/4 v0, 0x3

    new-array v0, v0, [LX/CH3;

    sget-object v1, LX/CH3;->FINAL_IMAGE:LX/CH3;

    aput-object v1, v0, v2

    sget-object v1, LX/CH3;->INTERMEDIATE_IMAGE:LX/CH3;

    aput-object v1, v0, v3

    sget-object v1, LX/CH3;->NONE:LX/CH3;

    aput-object v1, v0, v4

    sput-object v0, LX/CH3;->$VALUES:[LX/CH3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1866153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CH3;
    .locals 1

    .prologue
    .line 1866154
    const-class v0, LX/CH3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CH3;

    return-object v0
.end method

.method public static values()[LX/CH3;
    .locals 1

    .prologue
    .line 1866155
    sget-object v0, LX/CH3;->$VALUES:[LX/CH3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CH3;

    return-object v0
.end method
