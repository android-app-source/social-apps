.class public final LX/BpQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:LX/1Po;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;LX/1Po;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1823047
    iput-object p1, p0, LX/BpQ;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iput-object p2, p0, LX/BpQ;->a:LX/1Po;

    iput-object p3, p0, LX/BpQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 2

    .prologue
    .line 1823048
    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 1823049
    :cond_0
    :goto_0
    return-void

    .line 1823050
    :cond_1
    iget-object v0, p0, LX/BpQ;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->j:LX/20l;

    invoke-virtual {v0}, LX/20l;->b()V

    .line 1823051
    iget-object v0, p0, LX/BpQ;->a:LX/1Po;

    check-cast v0, LX/Bp6;

    iget-object v1, p0, LX/BpQ;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-interface {v0, v1, p2, p3}, LX/Bp6;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;LX/0Ve;)V

    .line 1823052
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1823053
    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 1823054
    iget-object v0, p0, LX/BpQ;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->o:LX/20r;

    invoke-virtual {v0, p1}, LX/20r;->a(Landroid/view/View;)V

    .line 1823055
    :cond_2
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1823056
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1823057
    iget-object v0, p0, LX/BpQ;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->p:LX/20s;

    invoke-virtual {v0, p1}, LX/20s;->a(Landroid/view/View;)V

    goto :goto_0
.end method
