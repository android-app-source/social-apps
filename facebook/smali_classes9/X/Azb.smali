.class public LX/Azb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732700
    iput-object p1, p0, LX/Azb;->a:LX/0ad;

    .line 1732701
    return-void
.end method

.method public static a(LX/0QB;)LX/Azb;
    .locals 2

    .prologue
    .line 1732702
    new-instance v1, LX/Azb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/Azb;-><init>(LX/0ad;)V

    .line 1732703
    move-object v0, v1

    .line 1732704
    return-object v0
.end method

.method private static a(LX/Azb;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1732705
    iget-object v1, p0, LX/Azb;->a:LX/0ad;

    sget-short v2, LX/1EB;->U:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Azb;->a:LX/0ad;

    sget-short v2, LX/1EB;->V:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Azb;->a:LX/0ad;

    sget-short v2, LX/1EB;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public final a(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;"
        }
    .end annotation

    .prologue
    .line 1732706
    invoke-static/range {p1 .. p1}, LX/7kq;->j(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static/range {p0 .. p0}, LX/Azb;->a(LX/Azb;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1732707
    :cond_0
    const/4 v2, 0x0

    .line 1732708
    :goto_0
    return-object v2

    .line 1732709
    :cond_1
    const/4 v7, 0x0

    .line 1732710
    const/4 v3, 0x1

    .line 1732711
    const/4 v6, 0x0

    .line 1732712
    const-wide v4, 0x7fffffffffffffffL

    .line 1732713
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v9, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1732714
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v10

    .line 1732715
    if-eqz v10, :cond_8

    .line 1732716
    const/4 v6, 0x1

    .line 1732717
    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceboxes()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1732718
    const/4 v7, 0x1

    .line 1732719
    :cond_2
    if-eqz v3, :cond_7

    .line 1732720
    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceDetectionFinished()Z

    move-result v2

    .line 1732721
    :goto_2
    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v12

    cmp-long v3, v12, v4

    if-gez v3, :cond_6

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v12

    const-wide/16 v14, -0x1

    cmp-long v3, v12, v14

    if-eqz v3, :cond_6

    .line 1732722
    invoke-virtual {v10}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getTimeToFindFirstFaceMs()J

    move-result-wide v4

    move-wide/from16 v16, v4

    move v4, v6

    move v5, v2

    move-wide/from16 v2, v16

    move v6, v7

    .line 1732723
    :goto_3
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v6

    move v6, v4

    move/from16 v16, v5

    move-wide v4, v2

    move/from16 v3, v16

    goto :goto_1

    .line 1732724
    :cond_3
    if-eqz v6, :cond_5

    .line 1732725
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceboxes(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v2

    if-eqz v7, :cond_4

    :goto_4
    invoke-virtual {v2, v4, v5}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setTimeToFindFirstFaceMs(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceDetectionFinished(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v2

    goto :goto_0

    :cond_4
    const-wide/16 v4, -0x1

    goto :goto_4

    .line 1732726
    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    move-wide/from16 v16, v4

    move v4, v6

    move v5, v2

    move-wide/from16 v2, v16

    move v6, v7

    goto :goto_3

    :cond_7
    move v2, v3

    goto :goto_2

    :cond_8
    move-wide/from16 v16, v4

    move v4, v6

    move v5, v3

    move-wide/from16 v2, v16

    move v6, v7

    goto :goto_3
.end method
