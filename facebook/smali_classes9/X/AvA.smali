.class public final enum LX/AvA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AvA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AvA;

.field public static final enum CACHE:LX/AvA;

.field public static final enum NETWORK:LX/AvA;

.field public static final enum PREFETCH:LX/AvA;

.field public static final enum REFRESH:LX/AvA;


# instance fields
.field private final mAge:J

.field private final mKey:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1723809
    new-instance v0, LX/AvA;

    const-string v1, "REFRESH"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x384

    invoke-direct/range {v0 .. v5}, LX/AvA;-><init>(Ljava/lang/String;IIJ)V

    sput-object v0, LX/AvA;->REFRESH:LX/AvA;

    .line 1723810
    new-instance v0, LX/AvA;

    const-string v1, "PREFETCH"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-wide/32 v4, 0x15180

    invoke-direct/range {v0 .. v5}, LX/AvA;-><init>(Ljava/lang/String;IIJ)V

    sput-object v0, LX/AvA;->PREFETCH:LX/AvA;

    .line 1723811
    new-instance v0, LX/AvA;

    const-string v1, "CACHE"

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-wide/16 v4, 0x384

    invoke-direct/range {v0 .. v5}, LX/AvA;-><init>(Ljava/lang/String;IIJ)V

    sput-object v0, LX/AvA;->CACHE:LX/AvA;

    .line 1723812
    new-instance v0, LX/AvA;

    const-string v1, "NETWORK"

    const/4 v2, 0x3

    const/4 v3, 0x3

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, LX/AvA;-><init>(Ljava/lang/String;IIJ)V

    sput-object v0, LX/AvA;->NETWORK:LX/AvA;

    .line 1723813
    const/4 v0, 0x4

    new-array v0, v0, [LX/AvA;

    const/4 v1, 0x0

    sget-object v2, LX/AvA;->REFRESH:LX/AvA;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/AvA;->PREFETCH:LX/AvA;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/AvA;->CACHE:LX/AvA;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/AvA;->NETWORK:LX/AvA;

    aput-object v2, v0, v1

    sput-object v0, LX/AvA;->$VALUES:[LX/AvA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)V"
        }
    .end annotation

    .prologue
    .line 1723814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1723815
    iput p3, p0, LX/AvA;->mKey:I

    .line 1723816
    iput-wide p4, p0, LX/AvA;->mAge:J

    .line 1723817
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AvA;
    .locals 1

    .prologue
    .line 1723818
    const-class v0, LX/AvA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AvA;

    return-object v0
.end method

.method public static values()[LX/AvA;
    .locals 1

    .prologue
    .line 1723819
    sget-object v0, LX/AvA;->$VALUES:[LX/AvA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AvA;

    return-object v0
.end method


# virtual methods
.method public final getKey()I
    .locals 1

    .prologue
    .line 1723820
    iget v0, p0, LX/AvA;->mKey:I

    return v0
.end method

.method public final getMaxCacheAge()J
    .locals 2

    .prologue
    .line 1723821
    iget-wide v0, p0, LX/AvA;->mAge:J

    return-wide v0
.end method
