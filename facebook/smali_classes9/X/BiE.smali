.class public final enum LX/BiE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BiE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BiE;

.field public static final enum LOAD_MORE_CONTEXT_ITEMS:LX/BiE;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1810017
    new-instance v0, LX/BiE;

    const-string v1, "LOAD_MORE_CONTEXT_ITEMS"

    invoke-direct {v0, v1, v2}, LX/BiE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiE;->LOAD_MORE_CONTEXT_ITEMS:LX/BiE;

    .line 1810018
    const/4 v0, 0x1

    new-array v0, v0, [LX/BiE;

    sget-object v1, LX/BiE;->LOAD_MORE_CONTEXT_ITEMS:LX/BiE;

    aput-object v1, v0, v2

    sput-object v0, LX/BiE;->$VALUES:[LX/BiE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1810019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BiE;
    .locals 1

    .prologue
    .line 1810020
    const-class v0, LX/BiE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BiE;

    return-object v0
.end method

.method public static values()[LX/BiE;
    .locals 1

    .prologue
    .line 1810021
    sget-object v0, LX/BiE;->$VALUES:[LX/BiE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BiE;

    return-object v0
.end method
