.class public final LX/BDN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1763075
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1763076
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763077
    :goto_0
    return v1

    .line 1763078
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763079
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1763080
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1763081
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1763082
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1763083
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1763084
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1763085
    :cond_3
    const-string v6, "action_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1763086
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1763087
    :cond_4
    const-string v6, "submenu_option_sets"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1763088
    invoke-static {p0, p1}, LX/BCl;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1763089
    :cond_5
    const-string v6, "undo_server_action"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1763090
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1763091
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1763092
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1763093
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1763094
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1763095
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1763096
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1763097
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1763098
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1763099
    if-eqz v0, :cond_0

    .line 1763100
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763101
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1763102
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1763103
    if-eqz v0, :cond_1

    .line 1763104
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763105
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1763106
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763107
    if-eqz v0, :cond_2

    .line 1763108
    const-string v1, "submenu_option_sets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763109
    invoke-static {p0, v0, p2, p3}, LX/BCl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1763110
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1763111
    if-eqz v0, :cond_3

    .line 1763112
    const-string v1, "undo_server_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763113
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1763114
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1763115
    return-void
.end method
