.class public LX/AZl;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/AZl;


# instance fields
.field public final b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/AMT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z

.field public g:LX/AZk;

.field public h:Z

.field public i:Z

.field public final j:Ljava/util/concurrent/atomic/AtomicInteger;

.field public k:LX/AN1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/AN2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1687400
    const/4 v2, 0x0

    .line 1687401
    new-instance v0, LX/AZl;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v2, v1}, LX/AZl;-><init>(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;LX/AMT;Ljava/util/List;Z)V

    move-object v0, v0

    .line 1687402
    sput-object v0, LX/AZl;->a:LX/AZl;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;LX/AMT;Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;",
            "LX/AMT;",
            "Ljava/util/List",
            "<",
            "LX/AMT;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1687403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687404
    iput-object p1, p0, LX/AZl;->b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    .line 1687405
    iput-object p2, p0, LX/AZl;->c:LX/AMT;

    .line 1687406
    iput-object p3, p0, LX/AZl;->d:Ljava/util/List;

    .line 1687407
    iput-boolean p4, p0, LX/AZl;->f:Z

    .line 1687408
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/AZl;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1687409
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1687410
    if-eqz p2, :cond_0

    .line 1687411
    invoke-virtual {v0, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1687412
    :cond_0
    if-eqz p3, :cond_1

    .line 1687413
    invoke-virtual {v0, p3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1687414
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AZl;->e:LX/0Px;

    .line 1687415
    return-void
.end method
