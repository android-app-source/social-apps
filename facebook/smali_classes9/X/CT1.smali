.class public LX/CT1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/CT6;


# direct methods
.method public constructor <init>(LX/CT6;)V
    .locals 0

    .prologue
    .line 1895198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895199
    iput-object p1, p0, LX/CT1;->a:LX/CT6;

    .line 1895200
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;LX/CTv;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1895201
    iget-object v0, p2, LX/CTv;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1895202
    const/4 v0, 0x0

    const-string v1, "Unknown event handler type"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1895203
    :goto_0
    return-void

    .line 1895204
    :sswitch_0
    check-cast p2, LX/CTz;

    iget-object v0, p0, LX/CT1;->a:LX/CT6;

    .line 1895205
    iget-object v1, p2, LX/CTz;->b:LX/0Px;

    iget-object v2, p2, LX/CTz;->c:Ljava/lang/String;

    .line 1895206
    iget-object v3, v0, LX/CT6;->a:LX/CTC;

    iget-object v3, v3, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    .line 1895207
    iget-object v4, v3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v4

    .line 1895208
    check-cast v3, LX/CSW;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1895209
    iget-object v4, v3, LX/CSW;->j:LX/CUD;

    sget-object v5, LX/CU9;->SHIMMER:LX/CU9;

    invoke-virtual {v4, v5, v1}, LX/CUD;->a(LX/CU9;LX/0Px;)Ljava/util/ArrayList;

    move-result-object v9

    .line 1895210
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result p0

    move v8, v7

    :goto_1
    if-ge v8, p0, :cond_2

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 1895211
    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ltz v5, :cond_0

    move v5, v6

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 1895212
    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result p2

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt p2, v5, :cond_1

    move v5, v6

    :goto_3
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 1895213
    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result p2

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v4, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v4, v5, v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, p2, v4}, LX/1OM;->a(II)V

    .line 1895214
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    :cond_0
    move v5, v7

    .line 1895215
    goto :goto_2

    :cond_1
    move v5, v7

    .line 1895216
    goto :goto_3

    .line 1895217
    :cond_2
    iget-object v3, v0, LX/CT6;->a:LX/CTC;

    iget-object v4, v3, LX/CTC;->b:LX/CSz;

    new-instance v5, LX/4Hw;

    invoke-direct {v5}, LX/4Hw;-><init>()V

    iget-object v3, v0, LX/CT6;->a:LX/CTC;

    iget-object v3, v3, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3rL;

    iget-object v3, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/CUD;

    iget-object v3, v3, LX/CUD;->a:LX/CUA;

    iget-object v3, v3, LX/CUA;->b:Ljava/lang/String;

    .line 1895218
    const-string v6, "component_flow_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895219
    move-object v3, v5

    .line 1895220
    iget-object v5, v0, LX/CT6;->a:LX/CTC;

    iget-object v5, v5, LX/CTC;->e:LX/CSc;

    invoke-virtual {v5}, LX/CSc;->a()Ljava/lang/String;

    move-result-object v5

    .line 1895221
    const-string v6, "raw_form_fields"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895222
    move-object v3, v3

    .line 1895223
    const-string v5, "raw_event_data"

    invoke-virtual {v3, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895224
    move-object v3, v3

    .line 1895225
    const-string v5, "screen_element_id"

    invoke-virtual {v3, v5, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895226
    move-object v3, v3

    .line 1895227
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->name()Ljava/lang/String;

    move-result-object v5

    .line 1895228
    const-string v6, "screen_event"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895229
    move-object v5, v3

    .line 1895230
    iget-object v3, v0, LX/CT6;->a:LX/CTC;

    iget-object v3, v3, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3rL;

    iget-object v3, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/CUD;

    iget-object v3, v3, LX/CUD;->a:LX/CUA;

    iget-object v3, v3, LX/CUA;->a:Ljava/lang/String;

    .line 1895231
    const-string v6, "screen_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895232
    move-object v3, v5

    .line 1895233
    const-string v5, "target_screen_element_ids"

    invoke-virtual {v3, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1895234
    move-object v3, v3

    .line 1895235
    iget-object v5, v0, LX/CT6;->a:LX/CTC;

    iget-object v5, v5, LX/CTC;->h:LX/CT9;

    .line 1895236
    iget-object v6, v4, LX/CSz;->b:LX/1Ck;

    sget-object v7, LX/CSy;->FETCH_PARTIAL_SCREEN:LX/CSy;

    invoke-virtual {v7}, LX/CSy;->name()Ljava/lang/String;

    move-result-object v7

    .line 1895237
    iget-object v9, v4, LX/CSz;->a:LX/0tX;

    .line 1895238
    new-instance v8, LX/CUg;

    invoke-direct {v8}, LX/CUg;-><init>()V

    move-object v8, v8

    .line 1895239
    const-string p0, "param"

    invoke-virtual {v8, p0, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v8

    check-cast v8, LX/CUg;

    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    sget-object p0, LX/0zS;->c:LX/0zS;

    invoke-virtual {v8, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    invoke-static {v8}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v8, v8

    .line 1895240
    new-instance v9, LX/CSx;

    invoke-direct {v9, v4, v5, v1, v3}, LX/CSx;-><init>(LX/CSz;LX/CT9;LX/0Px;LX/4Hw;)V

    move-object v9, v9

    .line 1895241
    invoke-virtual {v6, v7, v8, v9}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1895242
    goto/16 :goto_0

    .line 1895243
    :sswitch_1
    iget-object v0, p0, LX/CT1;->a:LX/CT6;

    .line 1895244
    invoke-virtual {v0, p3}, LX/CT6;->b(Ljava/lang/String;)V

    .line 1895245
    goto/16 :goto_0

    .line 1895246
    :sswitch_2
    check-cast p2, LX/CU3;

    iget-object v0, p0, LX/CT1;->a:LX/CT6;

    .line 1895247
    sget-object v1, LX/CT0;->a:[I

    iget-object v2, p2, LX/CU3;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSimpleEventHandlerType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1895248
    const/4 v1, 0x0

    const-string v2, "Unknown handler type for simple event."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1895249
    :goto_4
    goto/16 :goto_0

    .line 1895250
    :pswitch_0
    invoke-virtual {v0}, LX/CT6;->b()V

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x1af8cba5 -> :sswitch_1
        0x2fc8e8c6 -> :sswitch_0
        0x5e546ab9 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
