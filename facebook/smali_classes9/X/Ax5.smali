.class public final LX/Ax5;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

.field public final m:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1727513
    iput-object p1, p0, LX/Ax5;->l:Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

    .line 1727514
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1727515
    iput-object p2, p0, LX/Ax5;->m:Landroid/view/View;

    .line 1727516
    return-void
.end method

.method public static a(LX/Ax5;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;)LX/1aZ;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1727517
    invoke-virtual {p1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    iget-object v2, p0, LX/Ax5;->l:Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

    iget v2, v2, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;->b:I

    iget-object v3, p0, LX/Ax5;->l:Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

    iget v3, v3, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;->b:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1727518
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1727519
    move-object v0, v0

    .line 1727520
    invoke-virtual {v0, v4}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1727521
    iget-object v1, p0, LX/Ax5;->l:Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;->c:LX/1Ad;

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1727522
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)LX/1af;
    .locals 2

    .prologue
    .line 1727523
    new-instance v0, LX/1Uo;

    invoke-direct {v0, p0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/Ax5;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1727524
    new-instance v0, LX/Ax4;

    invoke-direct {v0, p0, p1}, LX/Ax4;-><init>(LX/Ax5;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;)V

    return-object v0
.end method
