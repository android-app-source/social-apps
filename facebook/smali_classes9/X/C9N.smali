.class public LX/C9N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/C99;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1854089
    const v0, 0x7f0b1cc0

    sput v0, LX/C9N;->a:I

    return-void
.end method

.method public constructor <init>(LX/C99;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854091
    iput-object p1, p0, LX/C9N;->b:LX/C99;

    .line 1854092
    return-void
.end method

.method public static a(LX/0QB;)LX/C9N;
    .locals 4

    .prologue
    .line 1854093
    const-class v1, LX/C9N;

    monitor-enter v1

    .line 1854094
    :try_start_0
    sget-object v0, LX/C9N;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854095
    sput-object v2, LX/C9N;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854096
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854097
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854098
    new-instance p0, LX/C9N;

    invoke-static {v0}, LX/C99;->a(LX/0QB;)LX/C99;

    move-result-object v3

    check-cast v3, LX/C99;

    invoke-direct {p0, v3}, LX/C9N;-><init>(LX/C99;)V

    .line 1854099
    move-object v0, p0

    .line 1854100
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854101
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854102
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854103
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<",
            "LX/1o2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1854104
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f02116b

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method
