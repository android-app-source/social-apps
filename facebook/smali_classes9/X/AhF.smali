.class public final LX/AhF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AhH;


# direct methods
.method public constructor <init>(LX/AhH;)V
    .locals 0

    .prologue
    .line 1702365
    iput-object p1, p0, LX/AhF;->a:LX/AhH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1702362
    iget-object v0, p0, LX/AhF;->a:LX/AhH;

    iget-object v0, v0, LX/AhH;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AhF;->a:LX/AhH;

    iget-object v1, v1, LX/AhH;->g:Ljava/lang/Runnable;

    iget-object v2, p0, LX/AhF;->a:LX/AhH;

    iget v2, v2, LX/AhH;->l:I

    int-to-long v2, v2

    const v4, -0x6c421bf2

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1702363
    iget-object v0, p0, LX/AhF;->a:LX/AhH;

    iget-object v0, v0, LX/AhH;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AhH;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch current viewers for live video id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AhF;->a:LX/AhH;

    iget-object v3, v3, LX/AhH;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1702364
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1702346
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1702347
    iget-object v0, p0, LX/AhF;->a:LX/AhH;

    iget-object v0, v0, LX/AhH;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AhF;->a:LX/AhH;

    iget-object v1, v1, LX/AhH;->g:Ljava/lang/Runnable;

    iget-object v2, p0, LX/AhF;->a:LX/AhH;

    iget v2, v2, LX/AhH;->l:I

    int-to-long v2, v2

    const v4, -0x122aa416

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1702348
    if-nez p1, :cond_1

    .line 1702349
    :cond_0
    return-void

    .line 1702350
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1702351
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel;

    .line 1702352
    if-eqz v0, :cond_0

    .line 1702353
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel;

    move-result-object v0

    .line 1702354
    if-eqz v0, :cond_0

    .line 1702355
    iget-object v1, p0, LX/AhF;->a:LX/AhH;

    iget-object v1, v1, LX/AhH;->k:LX/AhG;

    if-eqz v1, :cond_0

    .line 1702356
    iget-object v1, p0, LX/AhF;->a:LX/AhH;

    iget-object v1, v1, LX/AhH;->k:LX/AhG;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel;->a()LX/0Px;

    move-result-object v2

    invoke-interface {v1, v2}, LX/AhG;->a(LX/0Px;)V

    .line 1702357
    iget-object v1, p0, LX/AhF;->a:LX/AhH;

    iget-object v1, v1, LX/AhH;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1702358
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;

    .line 1702359
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1702360
    iget-object v4, p0, LX/AhF;->a:LX/AhH;

    iget-object v4, v4, LX/AhH;->h:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1702361
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
