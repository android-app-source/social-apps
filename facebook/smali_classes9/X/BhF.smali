.class public final LX/BhF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/BgK;

.field public final synthetic b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

.field public final synthetic c:LX/BhJ;


# direct methods
.method public constructor <init>(LX/BhJ;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;)V
    .locals 0

    .prologue
    .line 1808949
    iput-object p1, p0, LX/BhF;->c:LX/BhJ;

    iput-object p2, p0, LX/BhF;->a:LX/BgK;

    iput-object p3, p0, LX/BhF;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1808948
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1808947
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1808944
    iget-object v0, p0, LX/BhF;->a:LX/BgK;

    invoke-virtual {v0}, LX/BgK;->a()V

    .line 1808945
    iget-object v0, p0, LX/BhF;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808946
    return-void
.end method
