.class public final LX/CWl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/CXC;

.field public final synthetic c:LX/CSY;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/CT7;

.field public final synthetic f:LX/CTT;

.field public final synthetic g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;Ljava/util/ArrayList;LX/CXC;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTT;)V
    .locals 0

    .prologue
    .line 1908808
    iput-object p1, p0, LX/CWl;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    iput-object p2, p0, LX/CWl;->a:Ljava/util/ArrayList;

    iput-object p3, p0, LX/CWl;->b:LX/CXC;

    iput-object p4, p0, LX/CWl;->c:LX/CSY;

    iput-object p5, p0, LX/CWl;->d:Ljava/lang/String;

    iput-object p6, p0, LX/CWl;->e:LX/CT7;

    iput-object p7, p0, LX/CWl;->f:LX/CTT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 1908809
    iget-object v0, p0, LX/CWl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1908810
    if-eqz p1, :cond_0

    .line 1908811
    iget-object v0, p0, LX/CWl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, LX/CWl;->b:LX/CXC;

    .line 1908812
    iget-object v2, v1, LX/CXC;->g:Ljava/util/ArrayList;

    iget v3, v1, LX/CXC;->i:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CU0;

    iget-object v2, v2, LX/CU0;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1908813
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1908814
    :cond_0
    iget-object v0, p0, LX/CWl;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->f:LX/CXC;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CWl;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->f:LX/CXC;

    iget-object v1, p0, LX/CWl;->b:LX/CXC;

    if-eq v0, v1, :cond_1

    .line 1908815
    iget-object v0, p0, LX/CWl;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->f:LX/CXC;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CXC;->setChecked(Z)V

    .line 1908816
    :cond_1
    iget-object v0, p0, LX/CWl;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    iget-object v1, p0, LX/CWl;->b:LX/CXC;

    .line 1908817
    iput-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->f:LX/CXC;

    .line 1908818
    iget-object v0, p0, LX/CWl;->c:LX/CSY;

    iget-object v1, p0, LX/CWl;->d:Ljava/lang/String;

    iget-object v2, p0, LX/CWl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908819
    iget-object v0, p0, LX/CWl;->e:LX/CT7;

    iget-object v1, p0, LX/CWl;->f:LX/CTT;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, LX/CWl;->f:LX/CTT;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, LX/CWl;->c:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908820
    return-void
.end method
