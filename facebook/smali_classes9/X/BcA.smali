.class public final LX/BcA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field public b:Landroid/view/ViewGroup$OnHierarchyChangeListener;


# direct methods
.method public constructor <init>(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;)V
    .locals 0

    .prologue
    .line 1801518
    iput-object p1, p0, LX/BcA;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;B)V
    .locals 0

    .prologue
    .line 1801519
    invoke-direct {p0, p1}, LX/BcA;-><init>(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;)V

    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1801520
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    .line 1801521
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1801522
    invoke-static {}, LX/473;->a()I

    move-result v0

    .line 1801523
    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 1801524
    :cond_0
    return-void
.end method


# virtual methods
.method public final onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1801525
    iget-object v0, p0, LX/BcA;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    if-ne p1, v0, :cond_0

    instance-of v0, p2, LX/BcB;

    if-eqz v0, :cond_0

    .line 1801526
    invoke-static {p2}, LX/BcA;->a(Landroid/view/View;)V

    move-object v0, p2

    .line 1801527
    check-cast v0, LX/BcB;

    iget-object v1, p0, LX/BcA;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, v1, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->b:LX/Bc8;

    invoke-interface {v0, v1}, LX/BcB;->setOnCheckedChangeWidgetListener(LX/Bc8;)V

    .line 1801528
    :cond_0
    iget-object v0, p0, LX/BcA;->b:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_1

    .line 1801529
    iget-object v0, p0, LX/BcA;->b:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    .line 1801530
    :cond_1
    return-void
.end method

.method public final onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1801531
    iget-object v0, p0, LX/BcA;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    if-ne p1, v0, :cond_0

    instance-of v0, p2, LX/BcB;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 1801532
    check-cast v0, LX/BcB;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/BcB;->setOnCheckedChangeWidgetListener(LX/Bc8;)V

    .line 1801533
    :cond_0
    iget-object v0, p0, LX/BcA;->b:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_1

    .line 1801534
    iget-object v0, p0, LX/BcA;->b:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    .line 1801535
    :cond_1
    return-void
.end method
