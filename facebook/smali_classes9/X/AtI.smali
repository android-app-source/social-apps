.class public final LX/AtI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsT;


# instance fields
.field public final synthetic a:LX/AtM;


# direct methods
.method public constructor <init>(LX/AtM;)V
    .locals 0

    .prologue
    .line 1721246
    iput-object p1, p0, LX/AtI;->a:LX/AtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 8

    .prologue
    .line 1721247
    iget-object v0, p0, LX/AtI;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->y:LX/Ar3;

    .line 1721248
    iget-object v1, v0, LX/Ar3;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Aud;

    .line 1721249
    iget-object v2, v1, LX/Aud;->g:LX/Auh;

    .line 1721250
    invoke-virtual {v2}, LX/Auh;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721251
    sget-object v3, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    invoke-virtual {v2, v3}, LX/Auh;->setDrawingMode(LX/86w;)V

    .line 1721252
    :goto_0
    iget-object v2, v1, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    sget-object v3, LX/86t;->DOODLE:LX/86t;

    sget-object v4, LX/Aud;->a:LX/0jK;

    invoke-static {v2, v3, v4}, LX/87R;->a(LX/0il;LX/86t;LX/0jK;)V

    .line 1721253
    iget-object v1, v0, LX/Ar3;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    sget-object v3, LX/86o;->COLOR_PICKER:LX/86o;

    invoke-static {v1, v2, v3}, LX/87N;->a(LX/0il;LX/0jK;LX/86o;)V

    .line 1721254
    iget-object v1, v0, LX/Ar3;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v2, LX/ArJ;->TAP_DOODLE_BUTTON:LX/ArJ;

    .line 1721255
    iget-object v4, v1, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setDoodleSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    iget-object v5, v1, LX/ArT;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setDoodleSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v1, v4}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1721256
    iget-object v4, v1, LX/ArT;->b:LX/ArL;

    .line 1721257
    sget-object v5, LX/ArH;->START_DOODLE_SESSION:LX/ArH;

    invoke-static {v4, v5, v2}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-static {v4, v5}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721258
    return-void

    .line 1721259
    :cond_0
    sget-object v3, LX/86w;->ACTIVE_EMPTY:LX/86w;

    invoke-virtual {v2, v3}, LX/Auh;->setDrawingMode(LX/86w;)V

    goto :goto_0
.end method
