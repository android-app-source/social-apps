.class public final LX/B3U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B3T;


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;)V
    .locals 0

    .prologue
    .line 1740350
    iput-object p1, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;I)V
    .locals 3

    .prologue
    .line 1740351
    iget-object v1, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    iget-object v0, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B35;

    iget-object v2, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->p:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-static {v1, v0, p1, p2, v2}, LX/B3V;->a(Landroid/app/Activity;LX/B35;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;ILcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V

    .line 1740352
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1740353
    iget-object v0, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->q:LX/0h5;

    if-eqz v0, :cond_0

    .line 1740354
    iget-object v0, p0, LX/B3U;->a:Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->q:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1740355
    :cond_0
    return-void
.end method
