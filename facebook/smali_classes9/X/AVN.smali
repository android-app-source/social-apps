.class public final LX/AVN;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/AVP;

.field private b:Z


# direct methods
.method public constructor <init>(LX/AVP;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1679248
    iput-object p1, p0, LX/AVN;->a:LX/AVP;

    .line 1679249
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1679250
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AVN;->b:Z

    .line 1679251
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    .line 1679252
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1679253
    packed-switch v0, :pswitch_data_0

    .line 1679254
    :try_start_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled msg what="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1679255
    :catch_0
    move-exception v0

    .line 1679256
    sget-object v1, LX/AVP;->a:Ljava/lang/String;

    const-string v2, "Exception thrown while handling preview renderer event"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1679257
    iget-boolean v1, p0, LX/AVN;->b:Z

    if-nez v1, :cond_0

    .line 1679258
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/AVN;->b:Z

    .line 1679259
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    invoke-static {v1}, LX/AVP;->s(LX/AVP;)LX/1bI;

    move-result-object v1

    .line 1679260
    if-eqz v1, :cond_0

    .line 1679261
    iget-object v2, p0, LX/AVN;->a:LX/AVP;

    iget-object v2, v2, LX/AVP;->e:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/facecast/PreviewRenderer$PreviewRendererHandler$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/facecast/PreviewRenderer$PreviewRendererHandler$1;-><init>(LX/AVN;LX/1bI;Ljava/lang/Exception;)V

    const v0, -0x43cbe033

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1679262
    :cond_0
    :goto_0
    return-void

    .line 1679263
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    invoke-static {v0}, LX/AVP;->m(LX/AVP;)V

    goto :goto_0

    .line 1679264
    :pswitch_1
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2}, LX/AVP;->e(LX/AVP;II)V

    goto :goto_0

    .line 1679265
    :pswitch_2
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    .line 1679266
    iget-object v1, v0, LX/AVP;->w:LX/AWK;

    if-eqz v1, :cond_1

    .line 1679267
    iget-object v1, v0, LX/AVP;->w:LX/AWK;

    invoke-virtual {v1}, LX/AWK;->c()V

    .line 1679268
    :cond_1
    goto :goto_0

    .line 1679269
    :pswitch_3
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 1679270
    iget v3, v0, LX/AVP;->i:I

    if-ne v3, v1, :cond_b

    iget v3, v0, LX/AVP;->j:I

    if-ne v3, v2, :cond_b

    .line 1679271
    :cond_2
    :goto_1
    goto :goto_0

    .line 1679272
    :pswitch_4
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    const/4 v3, 0x1

    .line 1679273
    iput v1, v0, LX/AVP;->o:I

    .line 1679274
    if-ne v2, v3, :cond_c

    :goto_2
    iput-boolean v3, v0, LX/AVP;->p:Z

    .line 1679275
    iget-boolean v3, v0, LX/AVP;->u:Z

    if-eqz v3, :cond_3

    .line 1679276
    iget-object v3, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getWidth()I

    move-result p1

    iget-object v3, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHeight()I

    move-result v3

    invoke-static {v0, p1, v3}, LX/AVP;->e(LX/AVP;II)V

    .line 1679277
    :cond_3
    goto :goto_0

    .line 1679278
    :pswitch_5
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2}, LX/AVP;->f(LX/AVP;II)V

    goto :goto_0

    .line 1679279
    :pswitch_6
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    invoke-static {v0}, LX/AVP;->p(LX/AVP;)V

    goto :goto_0

    .line 1679280
    :pswitch_7
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    .line 1679281
    iget-object v1, v0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AVL;

    .line 1679282
    const/4 v3, 0x1

    .line 1679283
    iput-boolean v3, v1, LX/AVL;->d:Z

    .line 1679284
    goto :goto_3

    .line 1679285
    :cond_4
    goto/16 :goto_0

    .line 1679286
    :pswitch_8
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    invoke-static {v0}, LX/AVP;->v(LX/AVP;)V

    goto/16 :goto_0

    .line 1679287
    :pswitch_9
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    invoke-static {v0}, LX/AVP;->r(LX/AVP;)V

    goto/16 :goto_0

    .line 1679288
    :pswitch_a
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    .line 1679289
    iget-object v1, v0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AVL;

    .line 1679290
    invoke-virtual {v1}, LX/AVL;->d()V

    goto :goto_4

    .line 1679291
    :cond_5
    iget-object v1, v0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1679292
    invoke-static {v0}, LX/AVP;->v(LX/AVP;)V

    .line 1679293
    invoke-static {v0}, LX/AVP;->t(LX/AVP;)LX/1bM;

    move-result-object v1

    .line 1679294
    if-eqz v1, :cond_6

    .line 1679295
    invoke-interface {v1}, LX/1bM;->l()V

    .line 1679296
    :cond_6
    goto/16 :goto_0

    .line 1679297
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AVO;

    invoke-static {v0}, LX/AVO;->i$redex0(LX/AVO;)V

    goto/16 :goto_0

    .line 1679298
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AVO;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 1679299
    iget-object v3, v0, LX/AVO;->d:LX/61B;

    invoke-interface {v3, v1, v2}, LX/61B;->a(II)V

    .line 1679300
    goto/16 :goto_0

    .line 1679301
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AVO;

    .line 1679302
    invoke-static {v0}, LX/AVO;->b$redex0(LX/AVO;)V

    .line 1679303
    iget-object v1, v0, LX/AVO;->d:LX/61B;

    invoke-interface {v1}, LX/61B;->b()V

    .line 1679304
    goto/16 :goto_0

    .line 1679305
    :pswitch_e
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AVO;

    invoke-static {v1, v0}, LX/AVP;->b(LX/AVP;LX/AVO;)V

    goto/16 :goto_0

    .line 1679306
    :pswitch_f
    iget-object v0, p0, LX/AVN;->a:LX/AVP;

    .line 1679307
    invoke-static {v0}, LX/AVP;->s(LX/AVP;)LX/1bI;

    move-result-object v4

    .line 1679308
    if-nez v4, :cond_d

    .line 1679309
    sget-object v4, LX/AVP;->a:Ljava/lang/String;

    const-string v5, "Delegate is null. Not doing any orientation change."

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679310
    :goto_5
    goto/16 :goto_0

    .line 1679311
    :pswitch_10
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1679312
    iput v0, v1, LX/AVP;->l:F

    .line 1679313
    iget-object v4, v1, LX/AVP;->w:LX/AWK;

    iget v5, v1, LX/AVP;->i:I

    iget v6, v1, LX/AVP;->j:I

    iget v7, v1, LX/AVP;->k:I

    iget-object v8, v1, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/SurfaceView;

    invoke-virtual {v8}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    iget-object v9, v1, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/SurfaceView;

    invoke-virtual {v9}, Landroid/view/SurfaceView;->getHeight()I

    move-result v9

    iget v10, v1, LX/AVP;->l:F

    invoke-virtual/range {v4 .. v10}, LX/AWK;->a(IIIIIF)V

    .line 1679314
    goto/16 :goto_0

    .line 1679315
    :pswitch_11
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1679316
    invoke-static {v1}, LX/AVP;->l(LX/AVP;)LX/AWK;

    move-result-object v2

    .line 1679317
    if-eqz v2, :cond_7

    .line 1679318
    iget-object v1, v2, LX/AWK;->g:LX/7SB;

    if-eqz v1, :cond_7

    .line 1679319
    iget-object v1, v2, LX/AWK;->g:LX/7SB;

    invoke-virtual {v1, v0}, LX/7SB;->a(Ljava/util/List;)V

    .line 1679320
    :cond_7
    goto/16 :goto_0

    .line 1679321
    :pswitch_12
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    .line 1679322
    invoke-static {v1}, LX/AVP;->l(LX/AVP;)LX/AWK;

    move-result-object v2

    .line 1679323
    if-eqz v2, :cond_8

    .line 1679324
    invoke-virtual {v2, v0}, LX/AWK;->a(Landroid/net/Uri;)V

    .line 1679325
    :cond_8
    goto/16 :goto_0

    .line 1679326
    :pswitch_13
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/BA6;

    .line 1679327
    invoke-static {v1}, LX/AVP;->l(LX/AVP;)LX/AWK;

    move-result-object v2

    .line 1679328
    if-eqz v2, :cond_9

    .line 1679329
    iget-object v1, v2, LX/AWK;->j:LX/BAC;

    if-eqz v1, :cond_9

    .line 1679330
    iget-object v1, v2, LX/AWK;->j:LX/BAC;

    invoke-virtual {v1, v0}, LX/BAC;->a(LX/BA6;)V

    .line 1679331
    :cond_9
    goto/16 :goto_0

    .line 1679332
    :pswitch_14
    iget-object v1, p0, LX/AVN;->a:LX/AVP;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/BVA;

    .line 1679333
    invoke-static {v1}, LX/AVP;->l(LX/AVP;)LX/AWK;

    move-result-object v2

    .line 1679334
    if-eqz v2, :cond_a

    .line 1679335
    iget-object v3, v2, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    if-nez v3, :cond_e
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1679336
    :cond_a
    :goto_6
    goto/16 :goto_0

    .line 1679337
    :cond_b
    :try_start_2
    iput v1, v0, LX/AVP;->i:I

    .line 1679338
    iput v2, v0, LX/AVP;->j:I

    .line 1679339
    iget-boolean v3, v0, LX/AVP;->u:Z

    if-eqz v3, :cond_2

    .line 1679340
    iget-object v3, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getWidth()I

    move-result p1

    iget-object v3, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHeight()I

    move-result v3

    invoke-static {v0, p1, v3}, LX/AVP;->e(LX/AVP;II)V

    goto/16 :goto_1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1679341
    :cond_c
    :try_start_3
    const/4 v3, 0x0

    goto/16 :goto_2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 1679342
    :cond_d
    :try_start_4
    invoke-interface {v4}, LX/1bI;->iv_()I

    move-result v4

    iput v4, v0, LX/AVP;->k:I

    .line 1679343
    iget-object v4, v0, LX/AVP;->w:LX/AWK;

    iget v5, v0, LX/AVP;->i:I

    iget v6, v0, LX/AVP;->j:I

    iget v7, v0, LX/AVP;->k:I

    iget-object v8, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/SurfaceView;

    invoke-virtual {v8}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    iget-object v9, v0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/SurfaceView;

    invoke-virtual {v9}, Landroid/view/SurfaceView;->getHeight()I

    move-result v9

    iget v10, v0, LX/AVP;->l:F

    invoke-virtual/range {v4 .. v10}, LX/AWK;->a(IIIIIF)V

    goto/16 :goto_5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1679344
    :cond_e
    iget-object v3, v2, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-virtual {v3, v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a(LX/BVA;)V

    .line 1679345
    iget-object v3, v0, LX/BVA;->b:Ljava/util/List;

    move-object v3, v3

    .line 1679346
    if-eqz v3, :cond_f

    .line 1679347
    iget-object v3, v0, LX/BVA;->b:Ljava/util/List;

    move-object v3, v3

    .line 1679348
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1679349
    :cond_f
    iget-object v3, v2, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    .line 1679350
    iget-object v2, v3, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->g:LX/BVE;

    .line 1679351
    iget-object v3, v2, LX/BVE;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v3, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1679352
    goto :goto_6

    .line 1679353
    :cond_10
    iget-object v3, v2, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    .line 1679354
    iget-object v1, v3, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->g:LX/BVE;

    const/4 v3, 0x0

    .line 1679355
    iget-object v2, v1, LX/BVE;->b:Landroid/hardware/Sensor;

    if-eqz v2, :cond_11

    iget-object v2, v1, LX/BVE;->c:Landroid/hardware/Sensor;

    if-nez v2, :cond_13

    .line 1679356
    :cond_11
    iget-object v2, v1, LX/BVE;->a:Landroid/hardware/SensorManager;

    const/16 v0, 0xa

    invoke-virtual {v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, v1, LX/BVE;->b:Landroid/hardware/Sensor;

    .line 1679357
    iget-object v2, v1, LX/BVE;->b:Landroid/hardware/Sensor;

    if-nez v2, :cond_12

    .line 1679358
    const-class v2, LX/BVE;

    const-string v0, "Acceleration sensor not found"

    invoke-static {v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1679359
    :cond_12
    iget-object v2, v1, LX/BVE;->a:Landroid/hardware/SensorManager;

    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, v1, LX/BVE;->c:Landroid/hardware/Sensor;

    .line 1679360
    iget-object v2, v1, LX/BVE;->c:Landroid/hardware/Sensor;

    if-nez v2, :cond_13

    .line 1679361
    const-class v2, LX/BVE;

    const-string v0, "Gyro sensor not found"

    invoke-static {v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1679362
    :cond_13
    iget-object v2, v1, LX/BVE;->b:Landroid/hardware/Sensor;

    if-eqz v2, :cond_14

    .line 1679363
    iget-object v2, v1, LX/BVE;->a:Landroid/hardware/SensorManager;

    iget-object v0, v1, LX/BVE;->b:Landroid/hardware/Sensor;

    invoke-virtual {v2, v1, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1679364
    :cond_14
    iget-object v2, v1, LX/BVE;->c:Landroid/hardware/Sensor;

    if-eqz v2, :cond_15

    .line 1679365
    iget-object v2, v1, LX/BVE;->a:Landroid/hardware/SensorManager;

    iget-object v0, v1, LX/BVE;->c:Landroid/hardware/Sensor;

    invoke-virtual {v2, v1, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1679366
    :cond_15
    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_4
    .end packed-switch
.end method
