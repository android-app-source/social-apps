.class public LX/C2F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private A:LX/0yc;

.field public B:LX/04H;

.field private final a:LX/1qa;

.field private final b:LX/2mn;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final e:LX/0bH;

.field private final f:LX/1Ad;

.field private final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/C2E;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/23s;

.field private final i:Lcom/facebook/common/callercontext/CallerContext;

.field public j:LX/1bf;

.field public k:LX/04D;

.field public l:I

.field public m:I

.field private n:I

.field private o:I

.field private p:I

.field public q:Z

.field private r:F

.field public s:LX/395;

.field public t:LX/0hE;

.field public u:LX/C2D;

.field public v:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public w:LX/3J0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/1Yd;

.field public y:LX/1C2;

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0am;Lcom/facebook/common/callercontext/CallerContext;LX/1qa;LX/1Yd;LX/2mn;LX/0bH;LX/1C2;LX/1Ad;LX/0yc;LX/23s;)V
    .locals 2
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0am",
            "<",
            "LX/C2E;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/1qa;",
            "LX/1Yd;",
            "LX/2mn;",
            "LX/0bH;",
            "LX/1C2;",
            "LX/1Ad;",
            "LX/0yc;",
            "LX/23s;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1843995
    sget-object v0, LX/04H;->UNSUPPORTED_LOCATION:LX/04H;

    iput-object v0, p0, LX/C2F;->B:LX/04H;

    .line 1843996
    const-string v0, "VideoAttachmentDelegate.init"

    const v1, -0x73d374bd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1843997
    :try_start_0
    iput-object p2, p0, LX/C2F;->g:LX/0am;

    .line 1843998
    iput-object p4, p0, LX/C2F;->a:LX/1qa;

    .line 1843999
    iput-object p6, p0, LX/C2F;->b:LX/2mn;

    .line 1844000
    iput-object p1, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844001
    iget-object v0, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844002
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1844003
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1844004
    iput-object p5, p0, LX/C2F;->x:LX/1Yd;

    .line 1844005
    iput-object p7, p0, LX/C2F;->e:LX/0bH;

    .line 1844006
    iput-object p8, p0, LX/C2F;->y:LX/1C2;

    .line 1844007
    iput-object p9, p0, LX/C2F;->f:LX/1Ad;

    .line 1844008
    new-instance v0, LX/C2D;

    invoke-direct {v0, p0}, LX/C2D;-><init>(LX/C2F;)V

    iput-object v0, p0, LX/C2F;->u:LX/C2D;

    .line 1844009
    const/4 v0, 0x0

    iput v0, p0, LX/C2F;->r:F

    .line 1844010
    iget-object v0, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844011
    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1844012
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1844013
    iput-boolean v0, p0, LX/C2F;->q:Z

    .line 1844014
    iput-object p10, p0, LX/C2F;->A:LX/0yc;

    .line 1844015
    iput-object p11, p0, LX/C2F;->h:LX/23s;

    .line 1844016
    iput-object p3, p0, LX/C2F;->i:Lcom/facebook/common/callercontext/CallerContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1844017
    const v0, -0x164939ec

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1844018
    return-void

    .line 1844019
    :catchall_0
    move-exception v0

    const v1, 0x5811f347

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/C2F;LX/04D;LX/C2B;)V
    .locals 9

    .prologue
    .line 1844084
    iget-boolean v0, p0, LX/C2F;->z:Z

    if-nez v0, :cond_0

    .line 1844085
    const-string v0, "VideoAttachmentDelegate.FeedImageLoader.createImageRequest"

    const v1, -0x3ac796e5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1844086
    :try_start_0
    iget-object v0, p0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-static {v0}, LX/6X7;->a(Lcom/facebook/graphql/model/GraphQLVideo;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1844087
    iget-object v1, p0, LX/C2F;->a:LX/1qa;

    sget-object v2, LX/26P;->Video:LX/26P;

    invoke-virtual {v1, v0, v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v0

    iput-object v0, p0, LX/C2F;->j:LX/1bf;

    .line 1844088
    iget-object v0, p0, LX/C2F;->j:LX/1bf;

    iget-object v1, p0, LX/C2F;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, v0, v1}, LX/C2B;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1844089
    const v0, -0x4f3bbb0e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1844090
    invoke-static {p0}, LX/C2F;->g(LX/C2F;)V

    .line 1844091
    iput-object p1, p0, LX/C2F;->k:LX/04D;

    .line 1844092
    iget-object v3, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 1844093
    new-instance v4, LX/0AW;

    invoke-direct {v4, v3}, LX/0AW;-><init>(LX/162;)V

    iget-boolean v3, p0, LX/C2F;->q:Z

    .line 1844094
    iput-boolean v3, v4, LX/0AW;->d:Z

    .line 1844095
    move-object v3, v4

    .line 1844096
    iget-object v4, p0, LX/C2F;->B:LX/04H;

    .line 1844097
    iput-object v4, v3, LX/0AW;->c:LX/04H;

    .line 1844098
    move-object v3, v3

    .line 1844099
    invoke-virtual {v3}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v3

    iput-object v3, p0, LX/C2F;->v:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1844100
    new-instance v3, LX/395;

    new-instance v4, LX/0AV;

    iget-object v5, p0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v4

    iget-object v5, p0, LX/C2F;->v:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v6, p0, LX/C2F;->j:LX/1bf;

    iget-object v7, p0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v8, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v3 .. v8}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v3, p0, LX/C2F;->s:LX/395;

    .line 1844101
    iget-object v3, p0, LX/C2F;->s:LX/395;

    iget-object v4, p0, LX/C2F;->k:LX/04D;

    invoke-virtual {v3, v4}, LX/395;->a(LX/04D;)LX/395;

    .line 1844102
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/C2F;->z:Z

    .line 1844103
    :cond_0
    return-void

    .line 1844104
    :catchall_0
    move-exception v0

    const v1, 0x39945567

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static g(LX/C2F;)V
    .locals 3

    .prologue
    .line 1844072
    iget-object v0, p0, LX/C2F;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844073
    iget-object v0, p0, LX/C2F;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2E;

    iget v0, v0, LX/C2E;->a:I

    iput v0, p0, LX/C2F;->n:I

    .line 1844074
    iget-object v0, p0, LX/C2F;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2E;

    iget v0, v0, LX/C2E;->a:I

    iput v0, p0, LX/C2F;->p:I

    .line 1844075
    iget-object v0, p0, LX/C2F;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2E;

    iget v0, v0, LX/C2E;->b:I

    iput v0, p0, LX/C2F;->m:I

    .line 1844076
    iget-object v0, p0, LX/C2F;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2E;

    iget v0, v0, LX/C2E;->b:I

    iput v0, p0, LX/C2F;->o:I

    .line 1844077
    :goto_0
    return-void

    .line 1844078
    :cond_0
    iget-object v0, p0, LX/C2F;->b:LX/2mn;

    iget-object v1, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p0, LX/C2F;->r:F

    invoke-virtual {v0, v1, v2}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 1844079
    iget v1, v0, LX/3FO;->a:I

    iput v1, p0, LX/C2F;->n:I

    .line 1844080
    iget v1, v0, LX/3FO;->b:I

    iput v1, p0, LX/C2F;->m:I

    .line 1844081
    iget v1, v0, LX/3FO;->c:I

    iput v1, p0, LX/C2F;->p:I

    .line 1844082
    iget v1, v0, LX/3FO;->d:I

    iput v1, p0, LX/C2F;->o:I

    .line 1844083
    iget v0, v0, LX/3FO;->e:I

    iput v0, p0, LX/C2F;->l:I

    goto :goto_0
.end method


# virtual methods
.method public final a(IIZLX/0P1;)V
    .locals 2
    .param p4    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1844060
    iget-object v0, p0, LX/C2F;->s:LX/395;

    if-nez v0, :cond_0

    .line 1844061
    :goto_0
    return-void

    .line 1844062
    :cond_0
    iget-object v0, p0, LX/C2F;->s:LX/395;

    invoke-virtual {v0, p1}, LX/395;->a(I)LX/395;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/395;->b(I)LX/395;

    .line 1844063
    if-eqz p3, :cond_1

    .line 1844064
    iget-object v0, p0, LX/C2F;->s:LX/395;

    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1844065
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 1844066
    :goto_1
    iget-object v0, p0, LX/C2F;->s:LX/395;

    .line 1844067
    iput-object p4, v0, LX/395;->g:LX/0P1;

    .line 1844068
    iget-object v0, p0, LX/C2F;->t:LX/0hE;

    iget-object v1, p0, LX/C2F;->s:LX/395;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/395;)V

    goto :goto_0

    .line 1844069
    :cond_1
    iget-object v0, p0, LX/C2F;->s:LX/395;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 1844070
    iput-object v1, v0, LX/395;->j:LX/04g;

    .line 1844071
    goto :goto_1
.end method

.method public final a(LX/04D;LX/1Pt;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pt;",
            ":",
            "LX/1Po;",
            ">(",
            "LX/04D;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1844057
    iget-object v1, p0, LX/C2F;->h:LX/23s;

    iget-object v2, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, p2

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v0

    iput-object v0, p0, LX/C2F;->B:LX/04H;

    .line 1844058
    new-instance v0, LX/C2C;

    invoke-direct {v0, p2}, LX/C2C;-><init>(LX/1Pt;)V

    invoke-static {p0, p1, v0}, LX/C2F;->a(LX/C2F;LX/04D;LX/C2B;)V

    .line 1844059
    return-void
.end method

.method public final a(LX/3J0;)V
    .locals 1

    .prologue
    .line 1844054
    const-string v0, "listener already set"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1844055
    iput-object p1, p0, LX/C2F;->w:LX/3J0;

    .line 1844056
    return-void
.end method

.method public final a(LX/3iB;)V
    .locals 5

    .prologue
    .line 1844029
    const-string v1, "VideoAttachmentDelegate.bindVideoPlayerAttachment"

    const v2, -0x6b3bd241

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1844030
    :try_start_0
    invoke-static {p0}, LX/C2F;->g(LX/C2F;)V

    .line 1844031
    iget-object v1, p0, LX/C2F;->A:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/C2F;->A:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->d()I

    move-result v1

    .line 1844032
    :goto_0
    iget v2, p0, LX/C2F;->n:I

    invoke-interface {p1, v2, v1}, LX/3iB;->a(II)V

    .line 1844033
    iget-object v1, p0, LX/C2F;->f:LX/1Ad;

    iget-object v2, p0, LX/C2F;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-interface {p1}, LX/3iB;->getCoverController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v2, p0, LX/C2F;->j:LX/1bf;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 1844034
    instance-of v1, v2, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v1, :cond_0

    .line 1844035
    move-object v0, v2

    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    move-object v1, v0

    const/4 v3, 0x0

    sget-object v4, LX/397;->VIDEO:LX/397;

    .line 1844036
    invoke-static {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 1844037
    :cond_0
    invoke-interface {p1, v2}, LX/3iB;->setCoverController(LX/1aZ;)V

    .line 1844038
    iget-object v2, p0, LX/C2F;->g:LX/0am;

    iget-object v1, p0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844039
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v0

    .line 1844040
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v3, 0x0

    .line 1844041
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1844042
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2E;

    iget v0, v0, LX/C2E;->c:I

    .line 1844043
    :goto_1
    move v1, v0

    .line 1844044
    invoke-interface {p1, v1}, LX/3iB;->setBackgroundResource(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1844045
    const v1, 0x63398e79

    invoke-static {v1}, LX/02m;->a(I)V

    .line 1844046
    return-void

    .line 1844047
    :cond_1
    :try_start_1
    iget v1, p0, LX/C2F;->m:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1844048
    :catchall_0
    move-exception v1

    const v2, -0x7ff2d7cd

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1

    .line 1844049
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    .line 1844050
    :goto_2
    invoke-static {v1}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v1}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR_WITH_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v4, :cond_5

    .line 1844051
    :cond_3
    const v0, 0x7f020ad4

    goto :goto_1

    .line 1844052
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_2

    :cond_5
    move v0, v3

    .line 1844053
    goto :goto_1
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1844021
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1844022
    const-class p1, LX/0f8;

    invoke-static {v0, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0f8;

    .line 1844023
    invoke-interface {p1}, LX/0f8;->i()LX/0hE;

    move-result-object v0

    iput-object v0, p0, LX/C2F;->t:LX/0hE;

    .line 1844024
    const/4 p1, 0x1

    .line 1844025
    iget-object v0, p0, LX/C2F;->t:LX/0hE;

    invoke-interface {v0, p1}, LX/0hE;->setLogExitingPauseEvent(Z)V

    .line 1844026
    iget-object v0, p0, LX/C2F;->t:LX/0hE;

    invoke-interface {v0, p1}, LX/0hE;->setAllowLooping(Z)V

    .line 1844027
    iget-object v0, p0, LX/C2F;->t:LX/0hE;

    iget-object p1, p0, LX/C2F;->u:LX/C2D;

    invoke-interface {v0, p1}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 1844028
    return-void
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLVideo;
    .locals 1

    .prologue
    .line 1844020
    iget-object v0, p0, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    return-object v0
.end method
