.class public final LX/Aak;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Aal;


# direct methods
.method public constructor <init>(LX/Aal;)V
    .locals 0

    .prologue
    .line 1688961
    iput-object p1, p0, LX/Aak;->a:LX/Aal;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 1689031
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    iget-object v0, v0, LX/Aal;->c:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Aal;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch video feedback "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Aak;->a:LX/Aal;

    iget-object v3, v3, LX/Aal;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1689032
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    iget v0, v0, LX/Aal;->h:I

    if-ge v0, v4, :cond_0

    .line 1689033
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    .line 1689034
    iget v1, v0, LX/Aal;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/Aal;->h:I

    .line 1689035
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    invoke-static {v0, v4}, LX/Aal;->a$redex0(LX/Aal;I)V

    .line 1689036
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1688962
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1688963
    if-eqz p1, :cond_0

    .line 1688964
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688965
    if-eqz v0, :cond_0

    .line 1688966
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688967
    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1688968
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688969
    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1688970
    :cond_0
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    iget-object v0, v0, LX/Aal;->g:LX/1Zp;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    iget-object v0, v0, LX/Aal;->g:LX/1Zp;

    invoke-virtual {v0}, LX/1Zp;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1688971
    iget-object v0, p0, LX/Aak;->a:LX/Aal;

    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/Aal;->a$redex0(LX/Aal;I)V

    .line 1688972
    :cond_1
    :goto_0
    return-void

    .line 1688973
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688974
    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v0

    .line 1688975
    if-nez v0, :cond_3

    .line 1688976
    const/4 v1, 0x0

    .line 1688977
    :goto_1
    move-object v0, v1

    .line 1688978
    iget-object v1, p0, LX/Aak;->a:LX/Aal;

    .line 1688979
    iget-object v2, v1, LX/Aal;->d:LX/AYD;

    invoke-interface {v2, v0}, LX/AYD;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1688980
    goto :goto_0

    .line 1688981
    :cond_3
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 1688982
    invoke-interface {v0}, LX/220;->b()Z

    move-result v2

    .line 1688983
    iput-boolean v2, v1, LX/3dM;->d:Z

    .line 1688984
    invoke-interface {v0}, LX/220;->c()Z

    move-result v2

    .line 1688985
    iput-boolean v2, v1, LX/3dM;->e:Z

    .line 1688986
    invoke-interface {v0}, LX/220;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->c(Z)LX/3dM;

    .line 1688987
    invoke-interface {v0}, LX/220;->e()Z

    move-result v2

    .line 1688988
    iput-boolean v2, v1, LX/3dM;->g:Z

    .line 1688989
    invoke-interface {v0}, LX/220;->ac_()Z

    move-result v2

    .line 1688990
    iput-boolean v2, v1, LX/3dM;->h:Z

    .line 1688991
    invoke-interface {v0}, LX/220;->ad_()Z

    move-result v2

    .line 1688992
    iput-boolean v2, v1, LX/3dM;->i:Z

    .line 1688993
    invoke-interface {v0}, LX/220;->j()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->g(Z)LX/3dM;

    .line 1688994
    invoke-interface {v0}, LX/220;->k()Z

    move-result v2

    .line 1688995
    iput-boolean v2, v1, LX/3dM;->l:Z

    .line 1688996
    invoke-interface {v0}, LX/220;->l()Ljava/lang/String;

    move-result-object v2

    .line 1688997
    iput-object v2, v1, LX/3dM;->p:Ljava/lang/String;

    .line 1688998
    invoke-interface {v0}, LX/220;->m()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->j(Z)LX/3dM;

    .line 1688999
    invoke-interface {v0}, LX/220;->n()Ljava/lang/String;

    move-result-object v2

    .line 1689000
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1689001
    invoke-interface {v0}, LX/220;->o()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->l(Z)LX/3dM;

    .line 1689002
    invoke-interface {v0}, LX/220;->p()Ljava/lang/String;

    move-result-object v2

    .line 1689003
    iput-object v2, v1, LX/3dM;->D:Ljava/lang/String;

    .line 1689004
    invoke-interface {v0}, LX/220;->q()Ljava/lang/String;

    move-result-object v2

    .line 1689005
    iput-object v2, v1, LX/3dM;->J:Ljava/lang/String;

    .line 1689006
    invoke-interface {v0}, LX/220;->r()LX/59N;

    move-result-object v2

    .line 1689007
    if-nez v2, :cond_4

    .line 1689008
    const/4 v3, 0x0

    .line 1689009
    :goto_2
    move-object v2, v3

    .line 1689010
    iput-object v2, v1, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1689011
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto :goto_1

    .line 1689012
    :cond_4
    new-instance v3, LX/4XY;

    invoke-direct {v3}, LX/4XY;-><init>()V

    .line 1689013
    invoke-interface {v2}, LX/59N;->b()Ljava/lang/String;

    move-result-object p1

    .line 1689014
    iput-object p1, v3, LX/4XY;->ag:Ljava/lang/String;

    .line 1689015
    invoke-interface {v2}, LX/59N;->c()Ljava/lang/String;

    move-result-object p1

    .line 1689016
    iput-object p1, v3, LX/4XY;->aT:Ljava/lang/String;

    .line 1689017
    invoke-interface {v2}, LX/59N;->d()LX/1Fb;

    move-result-object p1

    .line 1689018
    if-nez p1, :cond_5

    .line 1689019
    const/4 v0, 0x0

    .line 1689020
    :goto_3
    move-object p1, v0

    .line 1689021
    iput-object p1, v3, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1689022
    invoke-virtual {v3}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    goto :goto_2

    .line 1689023
    :cond_5
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1689024
    invoke-interface {p1}, LX/1Fb;->a()I

    move-result v2

    .line 1689025
    iput v2, v0, LX/2dc;->c:I

    .line 1689026
    invoke-interface {p1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1689027
    iput-object v2, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1689028
    invoke-interface {p1}, LX/1Fb;->c()I

    move-result v2

    .line 1689029
    iput v2, v0, LX/2dc;->i:I

    .line 1689030
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_3
.end method
