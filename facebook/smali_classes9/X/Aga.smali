.class public final LX/Aga;
.super LX/AgL;
.source ""

# interfaces
.implements LX/AgS;


# instance fields
.field public final a:Landroid/animation/AnimatorSet;

.field public final b:Landroid/animation/ObjectAnimator;

.field public final c:Landroid/animation/ObjectAnimator;

.field public final d:Landroid/animation/ObjectAnimator;

.field public final e:Landroid/animation/AnimatorSet;

.field public final f:Landroid/animation/ObjectAnimator;

.field public final g:Landroid/animation/AnimatorSet;

.field public final h:Landroid/animation/ObjectAnimator;

.field public i:Z

.field public j:Landroid/graphics/drawable/Drawable;

.field public final synthetic k:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/AnimatorSet;Landroid/animation/ObjectAnimator;Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 1701009
    iput-object p1, p0, LX/Aga;->k:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-direct {p0}, LX/AgL;-><init>()V

    .line 1701010
    iput-object p7, p0, LX/Aga;->a:Landroid/animation/AnimatorSet;

    .line 1701011
    iput-object p2, p0, LX/Aga;->b:Landroid/animation/ObjectAnimator;

    .line 1701012
    iput-object p3, p0, LX/Aga;->c:Landroid/animation/ObjectAnimator;

    .line 1701013
    iput-object p4, p0, LX/Aga;->d:Landroid/animation/ObjectAnimator;

    .line 1701014
    iput-object p5, p0, LX/Aga;->e:Landroid/animation/AnimatorSet;

    .line 1701015
    iput-object p6, p0, LX/Aga;->f:Landroid/animation/ObjectAnimator;

    .line 1701016
    iput-object p8, p0, LX/Aga;->g:Landroid/animation/AnimatorSet;

    .line 1701017
    iput-object p9, p0, LX/Aga;->h:Landroid/animation/ObjectAnimator;

    .line 1701018
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1701019
    iget-object v0, p0, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(F)Z
    .locals 1

    .prologue
    .line 1701020
    iget-object v0, p0, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1701021
    iget-object v0, p0, LX/Aga;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1701022
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Aga;->i:Z

    .line 1701023
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1701024
    iget-object v0, p0, LX/Aga;->k:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->invalidate()V

    .line 1701025
    return-void
.end method
