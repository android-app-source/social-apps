.class public LX/B8t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B8t;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/B76;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1750702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1750703
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B8t;->a:Ljava/util/Map;

    .line 1750704
    return-void
.end method

.method public static a(LX/0QB;)LX/B8t;
    .locals 3

    .prologue
    .line 1750711
    sget-object v0, LX/B8t;->b:LX/B8t;

    if-nez v0, :cond_1

    .line 1750712
    const-class v1, LX/B8t;

    monitor-enter v1

    .line 1750713
    :try_start_0
    sget-object v0, LX/B8t;->b:LX/B8t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1750714
    if-eqz v2, :cond_0

    .line 1750715
    :try_start_1
    new-instance v0, LX/B8t;

    invoke-direct {v0}, LX/B8t;-><init>()V

    .line 1750716
    move-object v0, v0

    .line 1750717
    sput-object v0, LX/B8t;->b:LX/B8t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1750718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1750719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1750720
    :cond_1
    sget-object v0, LX/B8t;->b:LX/B8t;

    return-object v0

    .line 1750721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1750722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1750705
    if-nez p1, :cond_0

    move v0, v1

    .line 1750706
    :goto_0
    return v0

    .line 1750707
    :cond_0
    iget-object v0, p0, LX/B8t;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B76;

    .line 1750708
    if-eqz v0, :cond_1

    sget-object v2, LX/B76;->SUCCESS:LX/B76;

    if-ne v0, v2, :cond_1

    .line 1750709
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1750710
    goto :goto_0
.end method
