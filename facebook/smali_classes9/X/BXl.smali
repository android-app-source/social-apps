.class public LX/BXl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/BXk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1793673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1793674
    sget-object v0, LX/BXk;->INVISIBLE:LX/BXk;

    iput-object v0, p0, LX/BXl;->a:LX/BXk;

    .line 1793675
    return-void
.end method


# virtual methods
.method public final a(LX/BXk;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1793676
    sget-object v2, LX/BXj;->a:[I

    iget-object v3, p0, LX/BXl;->a:LX/BXk;

    invoke-virtual {v3}, LX/BXk;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1793677
    :goto_0
    iput-object p1, p0, LX/BXl;->a:LX/BXk;

    .line 1793678
    return-void

    .line 1793679
    :pswitch_0
    sget-object v2, LX/BXk;->SHRINK:LX/BXk;

    if-ne p1, v2, :cond_0

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 1793680
    :pswitch_1
    sget-object v2, LX/BXk;->REORDER:LX/BXk;

    if-eq p1, v2, :cond_1

    sget-object v2, LX/BXk;->EXPAND:LX/BXk;

    if-ne p1, v2, :cond_2

    :cond_1
    move v1, v0

    :cond_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    .line 1793681
    :pswitch_2
    sget-object v2, LX/BXk;->EXPAND:LX/BXk;

    if-ne p1, v2, :cond_3

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    .line 1793682
    :pswitch_3
    sget-object v2, LX/BXk;->INVISIBLE:LX/BXk;

    if-ne p1, v2, :cond_4

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(LX/BXk;)Z
    .locals 1

    .prologue
    .line 1793683
    iget-object v0, p0, LX/BXl;->a:LX/BXk;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/BXk;)Z
    .locals 1

    .prologue
    .line 1793684
    iget-object v0, p0, LX/BXl;->a:LX/BXk;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
