.class public final LX/CFK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/CFM;

.field private final b:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1862987
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/CFK;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/CFM;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862984
    iput-object p1, p0, LX/CFK;->a:LX/CFM;

    .line 1862985
    iput-object p2, p0, LX/CFK;->b:LX/1Nq;

    .line 1862986
    return-void
.end method

.method public static a(LX/0QB;)LX/CFK;
    .locals 8

    .prologue
    .line 1862949
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1862950
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1862951
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1862952
    if-nez v1, :cond_0

    .line 1862953
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1862954
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1862955
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1862956
    sget-object v1, LX/CFK;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1862957
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1862958
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1862959
    :cond_1
    if-nez v1, :cond_4

    .line 1862960
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1862961
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1862962
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1862963
    new-instance p0, LX/CFK;

    const-class v1, LX/CFM;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/CFM;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v7

    check-cast v7, LX/1Nq;

    invoke-direct {p0, v1, v7}, LX/CFK;-><init>(LX/CFM;LX/1Nq;)V

    .line 1862964
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1862965
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1862966
    if-nez v1, :cond_2

    .line 1862967
    sget-object v0, LX/CFK;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1862968
    :goto_1
    if-eqz v0, :cond_3

    .line 1862969
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1862970
    :goto_3
    check-cast v0, LX/CFK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1862971
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1862972
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1862973
    :catchall_1
    move-exception v0

    .line 1862974
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1862975
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1862976
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1862977
    :cond_2
    :try_start_8
    sget-object v0, LX/CFK;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 11
    .param p3    # LX/B5f;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1862979
    iget-object v1, p0, LX/CFK;->a:LX/CFM;

    iget-object v0, p0, LX/CFK;->b:LX/1Nq;

    const-class v2, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    invoke-virtual {v0, p1, v2}, LX/1Nq;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    .line 1862980
    new-instance v3, LX/CFL;

    const-class v4, Landroid/content/Context;

    invoke-interface {v1, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v4, LX/AR2;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AR2;

    const-class v4, LX/AR4;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/AR4;

    const-class v4, LX/ARD;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/ARD;

    invoke-static {v1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v10

    check-cast v10, LX/0lC;

    move-object v4, p2

    move-object v5, v0

    invoke-direct/range {v3 .. v10}, LX/CFL;-><init>(LX/B5j;Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;Landroid/content/Context;LX/AR2;LX/AR4;LX/ARD;LX/0lC;)V

    .line 1862981
    move-object v0, v3

    .line 1862982
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862978
    const-string v0, "GoodwillFriendversaryCardComposerPluginConfig"

    return-object v0
.end method
