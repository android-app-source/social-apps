.class public LX/Blx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/events/protocol/EditEventParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1816851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816852
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1816853
    check-cast p1, Lcom/facebook/events/protocol/EditEventParams;

    .line 1816854
    new-instance v0, LX/14N;

    const-string v1, "graphEventEdit"

    const-string v2, "POST"

    .line 1816855
    iget-object v3, p1, Lcom/facebook/events/protocol/EditEventParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1816856
    invoke-virtual {p1}, Lcom/facebook/events/protocol/ContextParams;->a()Ljava/util/List;

    move-result-object v4

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1816857
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1816858
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
