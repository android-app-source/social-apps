.class public final LX/AdX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1695261
    const/16 v22, 0x0

    .line 1695262
    const/16 v21, 0x0

    .line 1695263
    const/16 v20, 0x0

    .line 1695264
    const/16 v19, 0x0

    .line 1695265
    const/16 v18, 0x0

    .line 1695266
    const/16 v17, 0x0

    .line 1695267
    const/16 v16, 0x0

    .line 1695268
    const/4 v15, 0x0

    .line 1695269
    const/4 v14, 0x0

    .line 1695270
    const/4 v13, 0x0

    .line 1695271
    const/4 v12, 0x0

    .line 1695272
    const/4 v11, 0x0

    .line 1695273
    const/4 v10, 0x0

    .line 1695274
    const/4 v9, 0x0

    .line 1695275
    const/4 v8, 0x0

    .line 1695276
    const-wide/16 v6, 0x0

    .line 1695277
    const/4 v5, 0x0

    .line 1695278
    const/4 v4, 0x0

    .line 1695279
    const/4 v3, 0x0

    .line 1695280
    const/4 v2, 0x0

    .line 1695281
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_17

    .line 1695282
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1695283
    const/4 v2, 0x0

    .line 1695284
    :goto_0
    return v2

    .line 1695285
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    if-eq v7, v0, :cond_13

    .line 1695286
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1695287
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1695288
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_0

    if-eqz v7, :cond_0

    .line 1695289
    const-string v24, "__type__"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_1

    const-string v24, "__typename"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 1695290
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    move/from16 v23, v7

    goto :goto_1

    .line 1695291
    :cond_2
    const-string v24, "all_donations_summary_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 1695292
    invoke-static/range {p0 .. p1}, LX/AdR;->a(LX/15w;LX/186;)I

    move-result v7

    move/from16 v22, v7

    goto :goto_1

    .line 1695293
    :cond_3
    const-string v24, "amount_raised_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 1695294
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move/from16 v21, v7

    goto :goto_1

    .line 1695295
    :cond_4
    const-string v24, "campaign_goal_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 1695296
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move/from16 v20, v7

    goto :goto_1

    .line 1695297
    :cond_5
    const-string v24, "campaign_title"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 1695298
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move/from16 v19, v7

    goto/16 :goto_1

    .line 1695299
    :cond_6
    const-string v24, "can_donate"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 1695300
    const/4 v6, 0x1

    .line 1695301
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v18, v7

    goto/16 :goto_1

    .line 1695302
    :cond_7
    const-string v24, "charity_interface"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 1695303
    invoke-static/range {p0 .. p1}, LX/AdT;->a(LX/15w;LX/186;)I

    move-result v7

    move/from16 v17, v7

    goto/16 :goto_1

    .line 1695304
    :cond_8
    const-string v24, "days_left_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 1695305
    invoke-static/range {p0 .. p1}, LX/AdU;->a(LX/15w;LX/186;)I

    move-result v7

    move/from16 v16, v7

    goto/16 :goto_1

    .line 1695306
    :cond_9
    const-string v24, "description"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 1695307
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v15, v7

    goto/16 :goto_1

    .line 1695308
    :cond_a
    const-string v24, "detailed_amount_raised_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 1695309
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v14, v7

    goto/16 :goto_1

    .line 1695310
    :cond_b
    const-string v24, "fundraiser_by_owner_text"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 1695311
    invoke-static/range {p0 .. p1}, LX/AdV;->a(LX/15w;LX/186;)I

    move-result v7

    move v13, v7

    goto/16 :goto_1

    .line 1695312
    :cond_c
    const-string v24, "has_viewer_donated"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 1695313
    const/4 v3, 0x1

    .line 1695314
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v12, v7

    goto/16 :goto_1

    .line 1695315
    :cond_d
    const-string v24, "id"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 1695316
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v11, v7

    goto/16 :goto_1

    .line 1695317
    :cond_e
    const-string v24, "logo_image"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 1695318
    invoke-static/range {p0 .. p1}, LX/AdW;->a(LX/15w;LX/186;)I

    move-result v7

    move v10, v7

    goto/16 :goto_1

    .line 1695319
    :cond_f
    const-string v24, "mobile_donate_url"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 1695320
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v9, v7

    goto/16 :goto_1

    .line 1695321
    :cond_10
    const-string v24, "percent_of_goal_reached"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 1695322
    const/4 v2, 0x1

    .line 1695323
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 1695324
    :cond_11
    const-string v24, "total_donated_amount_by_viewer"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 1695325
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v8, v7

    goto/16 :goto_1

    .line 1695326
    :cond_12
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1695327
    :cond_13
    const/16 v7, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1695328
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1695329
    const/4 v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1695330
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1695331
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1695332
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1695333
    if-eqz v6, :cond_14

    .line 1695334
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1695335
    :cond_14
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1695336
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1695337
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 1695338
    const/16 v6, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 1695339
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 1695340
    if-eqz v3, :cond_15

    .line 1695341
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 1695342
    :cond_15
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1695343
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1695344
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1695345
    if-eqz v2, :cond_16

    .line 1695346
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1695347
    :cond_16
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1695348
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_17
    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v5

    move-wide/from16 v26, v6

    move v6, v4

    move-wide/from16 v4, v26

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1695349
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1695350
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1695351
    if-eqz v0, :cond_0

    .line 1695352
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695353
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1695354
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1695355
    if-eqz v0, :cond_1

    .line 1695356
    const-string v1, "all_donations_summary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695357
    invoke-static {p0, v0, p2}, LX/AdR;->a(LX/15i;ILX/0nX;)V

    .line 1695358
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695359
    if-eqz v0, :cond_2

    .line 1695360
    const-string v1, "amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695361
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695362
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695363
    if-eqz v0, :cond_3

    .line 1695364
    const-string v1, "campaign_goal_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695365
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695366
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695367
    if-eqz v0, :cond_4

    .line 1695368
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695369
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695370
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1695371
    if-eqz v0, :cond_5

    .line 1695372
    const-string v1, "can_donate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695373
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1695374
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1695375
    if-eqz v0, :cond_6

    .line 1695376
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695377
    invoke-static {p0, v0, p2, p3}, LX/AdT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1695378
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1695379
    if-eqz v0, :cond_8

    .line 1695380
    const-string v1, "days_left_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695381
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1695382
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1695383
    if-eqz v1, :cond_7

    .line 1695384
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695385
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695386
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1695387
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695388
    if-eqz v0, :cond_9

    .line 1695389
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695390
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695391
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695392
    if-eqz v0, :cond_a

    .line 1695393
    const-string v1, "detailed_amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695394
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695395
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1695396
    if-eqz v0, :cond_c

    .line 1695397
    const-string v1, "fundraiser_by_owner_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695398
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1695399
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1695400
    if-eqz v1, :cond_b

    .line 1695401
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695402
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695403
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1695404
    :cond_c
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1695405
    if-eqz v0, :cond_d

    .line 1695406
    const-string v1, "has_viewer_donated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695407
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1695408
    :cond_d
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695409
    if-eqz v0, :cond_e

    .line 1695410
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695411
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695412
    :cond_e
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1695413
    if-eqz v0, :cond_10

    .line 1695414
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695415
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1695416
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1695417
    if-eqz v1, :cond_f

    .line 1695418
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695419
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695420
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1695421
    :cond_10
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695422
    if-eqz v0, :cond_11

    .line 1695423
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695424
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695425
    :cond_11
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1695426
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_12

    .line 1695427
    const-string v2, "percent_of_goal_reached"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695428
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1695429
    :cond_12
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1695430
    if-eqz v0, :cond_13

    .line 1695431
    const-string v1, "total_donated_amount_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1695432
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1695433
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1695434
    return-void
.end method
