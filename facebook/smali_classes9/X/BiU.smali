.class public final enum LX/BiU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BiU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BiU;

.field public static final enum GOING:LX/BiU;

.field public static final enum NOT_INTERESTED_OR_NOT_GOING_OR_IGNORE:LX/BiU;

.field public static final enum PRIVATE_GOING:LX/BiU;

.field public static final enum PRIVATE_MAYBE:LX/BiU;

.field public static final enum PRIVATE_NOT_GOING:LX/BiU;

.field public static final enum WATCH_INTERESTED:LX/BiU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1810359
    new-instance v0, LX/BiU;

    const-string v1, "PRIVATE_GOING"

    invoke-direct {v0, v1, v3}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->PRIVATE_GOING:LX/BiU;

    .line 1810360
    new-instance v0, LX/BiU;

    const-string v1, "PRIVATE_MAYBE"

    invoke-direct {v0, v1, v4}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->PRIVATE_MAYBE:LX/BiU;

    .line 1810361
    new-instance v0, LX/BiU;

    const-string v1, "PRIVATE_NOT_GOING"

    invoke-direct {v0, v1, v5}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->PRIVATE_NOT_GOING:LX/BiU;

    .line 1810362
    new-instance v0, LX/BiU;

    const-string v1, "WATCH_INTERESTED"

    invoke-direct {v0, v1, v6}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->WATCH_INTERESTED:LX/BiU;

    .line 1810363
    new-instance v0, LX/BiU;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v7}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->GOING:LX/BiU;

    .line 1810364
    new-instance v0, LX/BiU;

    const-string v1, "NOT_INTERESTED_OR_NOT_GOING_OR_IGNORE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BiU;->NOT_INTERESTED_OR_NOT_GOING_OR_IGNORE:LX/BiU;

    .line 1810365
    const/4 v0, 0x6

    new-array v0, v0, [LX/BiU;

    sget-object v1, LX/BiU;->PRIVATE_GOING:LX/BiU;

    aput-object v1, v0, v3

    sget-object v1, LX/BiU;->PRIVATE_MAYBE:LX/BiU;

    aput-object v1, v0, v4

    sget-object v1, LX/BiU;->PRIVATE_NOT_GOING:LX/BiU;

    aput-object v1, v0, v5

    sget-object v1, LX/BiU;->WATCH_INTERESTED:LX/BiU;

    aput-object v1, v0, v6

    sget-object v1, LX/BiU;->GOING:LX/BiU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BiU;->NOT_INTERESTED_OR_NOT_GOING_OR_IGNORE:LX/BiU;

    aput-object v2, v0, v1

    sput-object v0, LX/BiU;->$VALUES:[LX/BiU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1810358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BiU;
    .locals 1

    .prologue
    .line 1810357
    const-class v0, LX/BiU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BiU;

    return-object v0
.end method

.method public static values()[LX/BiU;
    .locals 1

    .prologue
    .line 1810356
    sget-object v0, LX/BiU;->$VALUES:[LX/BiU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BiU;

    return-object v0
.end method
