.class public final LX/Cai;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V
    .locals 0

    .prologue
    .line 1918291
    iput-object p1, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1918292
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1918293
    check-cast p1, LX/2ou;

    .line 1918294
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PREPARED:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 1918295
    iget-object v0, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->c:LX/23g;

    iget-object v1, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/23g;->a(Ljava/lang/String;Z)V

    .line 1918296
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918297
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918298
    if-eqz v0, :cond_1

    .line 1918299
    iget-object v0, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918300
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918301
    iget-object v1, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    iget-object v2, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v2}, LX/2qV;->isPlayingState()Z

    move-result v2

    .line 1918302
    iget-object p0, v0, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 1918303
    :cond_1
    :goto_1
    return-void

    .line 1918304
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ERROR:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1918305
    iget-object v0, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->c:LX/23g;

    iget-object v1, p0, LX/Cai;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23g;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1918306
    :cond_3
    iget-object p0, v0, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {p0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c(Z)V

    goto :goto_1
.end method
