.class public LX/BA0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BA0;


# instance fields
.field public final a:LX/BA2;

.field public final b:LX/0hB;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0hB;LX/BA2;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752615
    iput-object p1, p0, LX/BA0;->b:LX/0hB;

    .line 1752616
    iput-object p2, p0, LX/BA0;->a:LX/BA2;

    .line 1752617
    iput-object p3, p0, LX/BA0;->c:Ljava/util/concurrent/Executor;

    .line 1752618
    return-void
.end method

.method public static a(LX/0QB;)LX/BA0;
    .locals 6

    .prologue
    .line 1752556
    sget-object v0, LX/BA0;->d:LX/BA0;

    if-nez v0, :cond_1

    .line 1752557
    const-class v1, LX/BA0;

    monitor-enter v1

    .line 1752558
    :try_start_0
    sget-object v0, LX/BA0;->d:LX/BA0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1752559
    if-eqz v2, :cond_0

    .line 1752560
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1752561
    new-instance p0, LX/BA0;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v0}, LX/BA2;->a(LX/0QB;)LX/BA2;

    move-result-object v4

    check-cast v4, LX/BA2;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4, v5}, LX/BA0;-><init>(LX/0hB;LX/BA2;Ljava/util/concurrent/Executor;)V

    .line 1752562
    move-object v0, p0

    .line 1752563
    sput-object v0, LX/BA0;->d:LX/BA0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1752564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1752565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1752566
    :cond_1
    sget-object v0, LX/BA0;->d:LX/BA0;

    return-object v0

    .line 1752567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1752568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;IFFFF)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    .line 1752571
    const-string v0, "blur"

    const v1, -0x78efb3d2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1752572
    const/4 v0, 0x2

    invoke-static {p1, v0, p2}, Lcom/facebook/imagepipeline/nativecode/NativeBlurFilter;->a(Landroid/graphics/Bitmap;II)V

    .line 1752573
    const v0, 0x60bb4b1d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1752574
    const-string v0, "scale_saturate"

    const v1, 0x5c83e7f3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1752575
    const/high16 p4, 0x3f000000    # 0.5f

    const/4 p5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 1752576
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p2

    .line 1752577
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iget-object p0, v0, LX/BA0;->b:LX/0hB;

    invoke-virtual {p0}, LX/0hB;->c()I

    move-result p0

    div-int/lit8 p0, p0, 0xa

    invoke-static {v7, p0}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1752578
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p0

    int-to-float p3, v7

    div-float/2addr p3, v5

    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result p3

    invoke-static {p0, p3}, Ljava/lang/Math;->max(II)I

    move-result p0

    .line 1752579
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    if-gt v7, p3, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    if-le p0, p3, :cond_2

    .line 1752580
    :cond_0
    int-to-float v7, v7

    mul-float/2addr v7, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    int-to-float p3, p3

    div-float/2addr v7, p3

    .line 1752581
    int-to-float p0, p0

    mul-float/2addr v6, p0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p0

    int-to-float p0, p0

    div-float/2addr v6, p0

    .line 1752582
    invoke-static {v7, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 1752583
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v6

    add-float/2addr v7, p4

    float-to-int p0, v7

    .line 1752584
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v6

    add-float/2addr v7, p4

    float-to-int v7, v7

    .line 1752585
    :goto_0
    const-string p3, "createBitmap"

    const p4, 0x5fa30522

    invoke-static {p3, p4}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1752586
    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, v7, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1752587
    const p0, -0x74865f13

    invoke-static {p0}, LX/02m;->a(I)V

    .line 1752588
    new-instance p0, Landroid/graphics/Canvas;

    invoke-direct {p0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1752589
    invoke-virtual {p0, v6, v6}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1752590
    new-instance v6, Landroid/graphics/Paint;

    const/4 p3, 0x3

    invoke-direct {v6, p3}, Landroid/graphics/Paint;-><init>(I)V

    .line 1752591
    const/4 p3, 0x0

    .line 1752592
    new-instance p4, Landroid/graphics/ColorMatrix;

    invoke-direct {p4}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 1752593
    invoke-virtual {p4, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1752594
    invoke-virtual {p4}, Landroid/graphics/ColorMatrix;->getArray()[F

    move-result-object p6

    .line 1752595
    const/16 v0, 0x14

    new-array v0, v0, [F

    .line 1752596
    array-length v5, p6

    invoke-static {p6, p3, v0, p3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1752597
    :goto_1
    const/16 p6, 0xf

    if-ge p3, p6, :cond_1

    .line 1752598
    aget p6, v0, p3

    mul-float/2addr p6, v3

    aput p6, v0, p3

    .line 1752599
    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 1752600
    :cond_1
    const/4 p3, 0x4

    aget p6, v0, p3

    add-float/2addr p6, v4

    aput p6, v0, p3

    .line 1752601
    const/16 p3, 0x9

    aget p6, v0, p3

    add-float/2addr p6, v4

    aput p6, v0, p3

    .line 1752602
    const/16 p3, 0xe

    aget p6, v0, p3

    add-float/2addr p6, v4

    aput p6, v0, p3

    .line 1752603
    invoke-virtual {p4, v0}, Landroid/graphics/ColorMatrix;->set([F)V

    .line 1752604
    new-instance p3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {p3, p4}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    move-object p3, p3

    .line 1752605
    invoke-virtual {v6, p3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1752606
    move-object v6, v6

    .line 1752607
    invoke-virtual {p0, v1, p5, p5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1752608
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6, p2, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v6

    .line 1752609
    const v1, -0x59b7ca9e

    invoke-static {v1}, LX/02m;->a(I)V

    .line 1752610
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1752611
    return-object v0

    .line 1752612
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p0

    .line 1752613
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const v3, 0x3f8ccccd    # 1.1f

    .line 1752570
    const/4 v2, 0x5

    const v5, 0x3dcccccd    # 0.1f

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v6, p2

    invoke-virtual/range {v0 .. v6}, LX/BA0;->a(Ljava/lang/String;IFFFF)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;IFFFF)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IFFFF)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1752569
    iget-object v0, p0, LX/BA0;->a:LX/BA2;

    invoke-virtual {v0, p1}, LX/BA2;->b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v0, LX/B9z;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, LX/B9z;-><init>(LX/BA0;IFFFF)V

    iget-object v1, p0, LX/BA0;->c:Ljava/util/concurrent/Executor;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
