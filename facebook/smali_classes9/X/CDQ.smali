.class public LX/CDQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDU;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDQ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859204
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1859205
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/CDQ;->b:LX/0Zi;

    .line 1859206
    iput-object p1, p0, LX/CDQ;->a:LX/0Ot;

    .line 1859207
    return-void
.end method

.method public static a(LX/0QB;)LX/CDQ;
    .locals 4

    .prologue
    .line 1859246
    const-class v1, LX/CDQ;

    monitor-enter v1

    .line 1859247
    :try_start_0
    sget-object v0, LX/CDQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1859248
    sput-object v2, LX/CDQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1859249
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859250
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1859251
    new-instance v3, LX/CDQ;

    const/16 p0, 0x21ed

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CDQ;-><init>(LX/0Ot;)V

    .line 1859252
    move-object v0, v3

    .line 1859253
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1859254
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859255
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1859256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1859245
    const v0, -0x659709b6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1859244
    const v0, -0x659709b6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 15

    .prologue
    .line 1859232
    check-cast p2, LX/CDP;

    .line 1859233
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v12

    .line 1859234
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v13

    .line 1859235
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v14

    .line 1859236
    iget-object v1, p0, LX/CDQ;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CDU;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/CDP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/CDP;->b:LX/04D;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/CDP;->c:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    iget-boolean v6, v0, LX/CDP;->d:Z

    move-object/from16 v0, p2

    iget-boolean v7, v0, LX/CDP;->e:Z

    move-object/from16 v0, p2

    iget-boolean v8, v0, LX/CDP;->f:Z

    move-object/from16 v0, p2

    iget-object v9, v0, LX/CDP;->g:LX/CDf;

    move-object/from16 v0, p2

    iget-object v10, v0, LX/CDP;->h:LX/3FV;

    move-object/from16 v0, p2

    iget-object v11, v0, LX/CDP;->i:LX/1Pe;

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v14}, LX/CDU;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;Landroid/view/View$OnClickListener;ZZZLX/CDf;LX/3FV;LX/1Pe;LX/1np;LX/1np;LX/1np;)LX/1Dg;

    move-result-object v2

    .line 1859237
    invoke-virtual {v12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CDT;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/CDP;->j:LX/CDT;

    .line 1859238
    invoke-static {v12}, LX/1cy;->a(LX/1np;)V

    .line 1859239
    invoke-virtual {v13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/CDP;->k:LX/121;

    .line 1859240
    invoke-static {v13}, LX/1cy;->a(LX/1np;)V

    .line 1859241
    invoke-virtual {v14}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    iput-object v1, v0, LX/CDP;->l:Landroid/view/View$OnClickListener;

    .line 1859242
    invoke-static {v14}, LX/1cy;->a(LX/1np;)V

    .line 1859243
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1859216
    invoke-static {}, LX/1dS;->b()V

    .line 1859217
    iget v0, p1, LX/1dQ;->b:I

    .line 1859218
    packed-switch v0, :pswitch_data_0

    .line 1859219
    :goto_0
    return-object v2

    .line 1859220
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1859221
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1859222
    check-cast v1, LX/CDP;

    .line 1859223
    iget-object v3, p0, LX/CDQ;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CDU;

    iget-object v4, v1, LX/CDP;->k:LX/121;

    iget-object p1, v1, LX/CDP;->j:LX/CDT;

    iget-object p2, v1, LX/CDP;->l:Landroid/view/View$OnClickListener;

    .line 1859224
    if-nez p2, :cond_0

    .line 1859225
    :goto_1
    goto :goto_0

    .line 1859226
    :cond_0
    new-instance p0, LX/6Z1;

    invoke-direct {p0, v0}, LX/6Z1;-><init>(Ljava/lang/Object;)V

    iput-object p0, p1, LX/CDT;->a:LX/6Z1;

    .line 1859227
    iput-object p2, p1, LX/CDT;->b:Landroid/view/View$OnClickListener;

    .line 1859228
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class v1, LX/0ew;

    invoke-static {p0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0ew;

    .line 1859229
    if-eqz p0, :cond_1

    .line 1859230
    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object p0

    invoke-virtual {v4, v1, p0}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_1

    .line 1859231
    :cond_1
    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x659709b6
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/CDO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/CDQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1859208
    new-instance v1, LX/CDP;

    invoke-direct {v1, p0}, LX/CDP;-><init>(LX/CDQ;)V

    .line 1859209
    iget-object v2, p0, LX/CDQ;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDO;

    .line 1859210
    if-nez v2, :cond_0

    .line 1859211
    new-instance v2, LX/CDO;

    invoke-direct {v2, p0}, LX/CDO;-><init>(LX/CDQ;)V

    .line 1859212
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CDO;->a$redex0(LX/CDO;LX/1De;IILX/CDP;)V

    .line 1859213
    move-object v1, v2

    .line 1859214
    move-object v0, v1

    .line 1859215
    return-object v0
.end method
