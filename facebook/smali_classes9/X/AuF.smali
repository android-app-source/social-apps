.class public LX/AuF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public A:LX/Au2;

.field public B:LX/AsW;

.field public C:LX/AtU;

.field public D:LX/Atj;

.field public E:LX/AtW;

.field private F:LX/870;

.field private G:LX/4gQ;

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field private final c:LX/AuJ;

.field private final d:LX/At1;

.field private final e:LX/Au3;

.field public final f:LX/AsX;

.field private final g:LX/AtV;

.field private final h:LX/Atk;

.field private final i:LX/AtX;

.field public final j:Lcom/facebook/user/model/User;

.field public final k:LX/FJv;

.field public final l:LX/23P;

.field public m:Landroid/view/View;

.field public n:Landroid/view/View;

.field public o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public p:Lcom/facebook/widget/tiles/ThreadTileView;

.field public q:Lcom/facebook/widget/text/BetterTextView;

.field public r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public s:Lcom/facebook/widget/text/BetterTextView;

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public w:Landroid/view/View;

.field public x:LX/Aqw;

.field public y:LX/AuI;

.field public z:LX/At0;


# direct methods
.method public constructor <init>(LX/0il;LX/Aqw;Landroid/view/ViewStub;Landroid/content/Context;LX/23P;LX/AuJ;LX/At1;LX/Au3;LX/AsX;LX/AtV;LX/Atk;LX/AtX;Lcom/facebook/user/model/User;LX/FJv;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Aqw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationTopBarController$Delegate;",
            "Landroid/view/ViewStub;",
            "Landroid/content/Context;",
            "LX/23P;",
            "LX/AuJ;",
            "LX/At1;",
            "LX/Au3;",
            "LX/AsX;",
            "LX/AtV;",
            "LX/Atk;",
            "LX/AtX;",
            "Lcom/facebook/user/model/User;",
            "Lcom/facebook/messaging/ui/tiles/MessagingThreadTileViewDataFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1722275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722276
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AuF;->a:Ljava/lang/ref/WeakReference;

    .line 1722277
    iput-object p4, p0, LX/AuF;->b:Landroid/content/Context;

    .line 1722278
    iput-object p5, p0, LX/AuF;->l:LX/23P;

    .line 1722279
    iput-object p6, p0, LX/AuF;->c:LX/AuJ;

    .line 1722280
    iput-object p7, p0, LX/AuF;->d:LX/At1;

    .line 1722281
    iput-object p8, p0, LX/AuF;->e:LX/Au3;

    .line 1722282
    iput-object p10, p0, LX/AuF;->g:LX/AtV;

    .line 1722283
    iput-object p9, p0, LX/AuF;->f:LX/AsX;

    .line 1722284
    iput-object p11, p0, LX/AuF;->h:LX/Atk;

    .line 1722285
    iput-object p12, p0, LX/AuF;->i:LX/AtX;

    .line 1722286
    iput-object p13, p0, LX/AuF;->j:Lcom/facebook/user/model/User;

    .line 1722287
    iput-object p14, p0, LX/AuF;->k:LX/FJv;

    .line 1722288
    const v0, 0x7f030939

    invoke-virtual {p3, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1722289
    invoke-virtual {p3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AuF;->m:Landroid/view/View;

    .line 1722290
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a2

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/AuF;->p:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1722291
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a3

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AuF;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1722292
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a5

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722293
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a6

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 1722294
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a8

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AuF;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722295
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a9

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AuF;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722296
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17aa

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AuF;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722297
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17a7

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AuF;->w:Landroid/view/View;

    .line 1722298
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d179f

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AuF;->n:Landroid/view/View;

    .line 1722299
    iget-object v0, p0, LX/AuF;->m:Landroid/view/View;

    const p4, 0x7f0d17ab

    invoke-static {v0, p4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AuF;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722300
    iget-object v0, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    iget-object p4, p0, LX/AuF;->l:LX/23P;

    iget-object p5, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p5

    const p6, 0x7f08278c

    invoke-virtual {p5, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p5

    const/4 p6, 0x0

    invoke-virtual {p4, p5, p6}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {v0, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722301
    iput-object p2, p0, LX/AuF;->x:LX/Aqw;

    .line 1722302
    iget-object v0, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 p4, 0x10e0000

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LX/AuF;->H:I

    .line 1722303
    iget-object v0, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p4, 0x7f0b1ab9

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AuF;->I:I

    .line 1722304
    iget-object v0, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p4, 0x7f0b1aba

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AuF;->J:I

    .line 1722305
    iget-object v0, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p4, 0x7f0a0786

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/AuF;->K:I

    .line 1722306
    iget-object v0, p0, LX/AuF;->k:LX/FJv;

    iget-object p4, p0, LX/AuF;->j:Lcom/facebook/user/model/User;

    invoke-virtual {v0, p4}, LX/FJv;->a(Lcom/facebook/user/model/User;)LX/8Vc;

    move-result-object v0

    .line 1722307
    iget-object p4, p0, LX/AuF;->p:Lcom/facebook/widget/tiles/ThreadTileView;

    new-instance p5, LX/AuE;

    invoke-direct {p5, v0}, LX/AuE;-><init>(LX/8Vc;)V

    invoke-virtual {p4, p5}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1722308
    iget-object v0, p0, LX/AuF;->p:Lcom/facebook/widget/tiles/ThreadTileView;

    const/4 p4, 0x0

    invoke-virtual {v0, p4}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 1722309
    iget-object v0, p0, LX/AuF;->q:Lcom/facebook/widget/text/BetterTextView;

    iget-object p4, p0, LX/AuF;->j:Lcom/facebook/user/model/User;

    .line 1722310
    iget-object p5, p4, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object p4, p5

    .line 1722311
    invoke-virtual {p4}, Lcom/facebook/user/model/Name;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722312
    iget-object v0, p0, LX/AuF;->q:Lcom/facebook/widget/text/BetterTextView;

    iget p4, p0, LX/AuF;->J:I

    int-to-float p4, p4

    const/4 p5, 0x0

    iget p6, p0, LX/AuF;->I:I

    int-to-float p6, p6

    iget p3, p0, LX/AuF;->K:I

    invoke-virtual {v0, p4, p5, p6, p3}, Lcom/facebook/widget/text/BetterTextView;->setShadowLayer(FFFI)V

    .line 1722313
    iget-object v0, p0, LX/AuF;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1722314
    new-instance p4, LX/Au5;

    invoke-direct {p4, p0}, LX/Au5;-><init>(LX/AuF;)V

    .line 1722315
    new-instance p5, LX/AuI;

    invoke-direct {p5, p4}, LX/AuI;-><init>(LX/AsT;)V

    .line 1722316
    move-object p4, p5

    .line 1722317
    iput-object p4, p0, LX/AuF;->y:LX/AuI;

    .line 1722318
    new-instance p4, LX/Au6;

    invoke-direct {p4, p0}, LX/Au6;-><init>(LX/AuF;)V

    .line 1722319
    new-instance p5, LX/At0;

    invoke-direct {p5, p4}, LX/At0;-><init>(LX/AsT;)V

    .line 1722320
    move-object p4, p5

    .line 1722321
    iput-object p4, p0, LX/AuF;->z:LX/At0;

    .line 1722322
    new-instance p4, LX/Au7;

    invoke-direct {p4, p0}, LX/Au7;-><init>(LX/AuF;)V

    .line 1722323
    new-instance p5, LX/Au2;

    invoke-direct {p5, p4}, LX/Au2;-><init>(LX/AsT;)V

    .line 1722324
    move-object p4, p5

    .line 1722325
    iput-object p4, p0, LX/AuF;->A:LX/Au2;

    .line 1722326
    iget-object p4, p0, LX/AuF;->f:LX/AsX;

    iget-object p5, p0, LX/AuF;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p5

    .line 1722327
    new-instance p3, LX/AsW;

    check-cast p5, LX/0il;

    const-class p6, Landroid/content/Context;

    invoke-interface {p4, p6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Landroid/content/Context;

    invoke-direct {p3, p5, p6}, LX/AsW;-><init>(LX/0il;Landroid/content/Context;)V

    .line 1722328
    move-object p4, p3

    .line 1722329
    iput-object p4, p0, LX/AuF;->B:LX/AsW;

    .line 1722330
    new-instance p4, LX/Au8;

    invoke-direct {p4, p0}, LX/Au8;-><init>(LX/AuF;)V

    .line 1722331
    new-instance p5, LX/Atj;

    invoke-direct {p5, p4}, LX/Atj;-><init>(LX/AsT;)V

    .line 1722332
    move-object p4, p5

    .line 1722333
    iput-object p4, p0, LX/AuF;->D:LX/Atj;

    .line 1722334
    new-instance p4, LX/Au9;

    invoke-direct {p4, p0, v0}, LX/Au9;-><init>(LX/AuF;LX/0il;)V

    .line 1722335
    new-instance v0, LX/AtU;

    invoke-direct {v0, p4}, LX/AtU;-><init>(LX/AsT;)V

    .line 1722336
    move-object v0, v0

    .line 1722337
    iput-object v0, p0, LX/AuF;->C:LX/AtU;

    .line 1722338
    new-instance v0, LX/AuA;

    invoke-direct {v0, p0}, LX/AuA;-><init>(LX/AuF;)V

    .line 1722339
    new-instance p4, LX/AtW;

    invoke-direct {p4, v0}, LX/AtW;-><init>(LX/AsT;)V

    .line 1722340
    move-object v0, p4

    .line 1722341
    iput-object v0, p0, LX/AuF;->E:LX/AtW;

    .line 1722342
    iget-object v0, p0, LX/AuF;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p4, p0, LX/AuF;->E:LX/AtW;

    invoke-static {p0, v0, p4}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722343
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1722344
    invoke-direct {p0}, LX/AuF;->d()V

    .line 1722345
    :cond_0
    return-void
.end method

.method public static a(LX/AuF;Landroid/view/View;LX/AsV;)V
    .locals 1

    .prologue
    .line 1722346
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1722347
    instance-of v0, p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1722348
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {p2, v0}, LX/AsV;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 1722349
    :cond_0
    new-instance v0, LX/AuB;

    invoke-direct {v0, p0, p2}, LX/AuB;-><init>(LX/AuF;LX/AsV;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1722350
    return-void
.end method

.method private varargs a(Z[Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1722351
    array-length v2, p2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 1722352
    if-eqz p1, :cond_0

    .line 1722353
    new-instance v4, LX/Aui;

    iget v5, p0, LX/AuF;->H:I

    invoke-direct {v4, v3, v1, v5}, LX/Aui;-><init>(Landroid/view/View;ZI)V

    .line 1722354
    new-instance v5, LX/AuC;

    invoke-direct {v5, p0, v3}, LX/AuC;-><init>(LX/AuF;Landroid/view/View;)V

    invoke-virtual {v4, v5}, LX/Aui;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1722355
    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1722356
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1722357
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1722358
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1722359
    :cond_1
    return-void
.end method

.method private varargs a([Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1722360
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 1722361
    new-instance v4, LX/Aui;

    const/4 v5, 0x1

    iget v6, p0, LX/AuF;->H:I

    invoke-direct {v4, v3, v5, v6}, LX/Aui;-><init>(Landroid/view/View;ZI)V

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1722362
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1722363
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1722364
    :cond_0
    return-void
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1722365
    iget-object v0, p0, LX/AuF;->n:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1722366
    iget-object v0, p0, LX/AuF;->n:Landroid/view/View;

    new-instance v1, LX/AuH;

    iget-object v2, p0, LX/AuF;->n:Landroid/view/View;

    const/4 v3, 0x1

    iget-object v4, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x10e0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-direct {v1, v2, v3, v6, v4}, LX/AuH;-><init>(Landroid/view/View;ZZI)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1722367
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1722368
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1722369
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    const/16 v3, 0x8

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 1722370
    iget-object v0, p0, LX/AuF;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1722371
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    .line 1722372
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v6

    move-object v1, p1

    .line 1722373
    check-cast v1, LX/0io;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    invoke-static {v1, v2}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1722374
    const/4 p2, 0x0

    .line 1722375
    new-instance v1, LX/AuH;

    iget-object v2, p0, LX/AuF;->n:Landroid/view/View;

    iget-object v10, p0, LX/AuF;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/high16 p1, 0x10e0000

    invoke-virtual {v10, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-direct {v1, v2, p2, p2, v10}, LX/AuH;-><init>(Landroid/view/View;ZZI)V

    .line 1722376
    new-instance v2, LX/AuD;

    invoke-direct {v2, p0}, LX/AuD;-><init>(LX/AuF;)V

    invoke-virtual {v1, v2}, LX/AuH;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1722377
    iget-object v2, p0, LX/AuF;->n:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1722378
    :cond_0
    :goto_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {v1}, LX/87R;->a(LX/0io;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, LX/87N;->a(LX/0il;)LX/86o;

    move-result-object v1

    invoke-virtual {v1}, LX/86o;->shouldBlockOtherUIComponents()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v1, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v0, v1, :cond_5

    .line 1722379
    :cond_1
    iget-object v0, p0, LX/AuF;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1722380
    :goto_1
    if-eqz v5, :cond_2

    if-nez v6, :cond_6

    .line 1722381
    :cond_2
    new-array v0, v9, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v7

    iget-object v1, p0, LX/AuF;->w:Landroid/view/View;

    aput-object v1, v0, v8

    invoke-direct {p0, v4, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    .line 1722382
    const/4 v0, 0x0

    iput-object v0, p0, LX/AuF;->G:LX/4gQ;

    .line 1722383
    :cond_3
    :goto_2
    return-void

    .line 1722384
    :cond_4
    check-cast p1, LX/0io;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {p1, v1}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1722385
    invoke-direct {p0}, LX/AuF;->d()V

    goto :goto_0

    .line 1722386
    :cond_5
    iget-object v0, p0, LX/AuF;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1722387
    goto :goto_1

    .line 1722388
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1722389
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1722390
    iget-object v1, p0, LX/AuF;->F:LX/870;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/AuF;->F:LX/870;

    if-ne v1, v6, :cond_7

    iget-object v1, p0, LX/AuF;->G:LX/4gQ;

    if-eq v1, v0, :cond_3

    .line 1722391
    :cond_7
    iput-object v6, p0, LX/AuF;->F:LX/870;

    .line 1722392
    iput-object v0, p0, LX/AuF;->G:LX/4gQ;

    .line 1722393
    sget-object v0, LX/Au4;->a:[I

    iget-object v1, p0, LX/AuF;->F:LX/870;

    invoke-virtual {v1}, LX/870;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1722394
    :pswitch_0
    iget-object v0, p0, LX/AuF;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AuF;->D:LX/Atj;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722395
    iget-object v0, p0, LX/AuF;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AuF;->B:LX/AsW;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722396
    iget-object v0, p0, LX/AuF;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AuF;->C:LX/AtU;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722397
    iget-object v1, p0, LX/AuF;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/AuF;->G:LX/4gQ;

    sget-object v2, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v2, :cond_9

    move v0, v3

    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1722398
    iget-object v0, p0, LX/AuF;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1722399
    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1722400
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1722401
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-eq v0, v1, :cond_8

    .line 1722402
    iget-object v0, p0, LX/AuF;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1722403
    :cond_8
    new-array v0, v7, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->w:Landroid/view/View;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, LX/AuF;->a([Landroid/view/View;)V

    .line 1722404
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v7

    invoke-direct {p0, v7, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    goto/16 :goto_2

    :cond_9
    move v0, v4

    .line 1722405
    goto :goto_3

    .line 1722406
    :pswitch_1
    iget-object v0, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/AuF;->z:LX/At0;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722407
    new-array v0, v7, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, LX/AuF;->a([Landroid/view/View;)V

    .line 1722408
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->w:Landroid/view/View;

    aput-object v1, v0, v7

    invoke-direct {p0, v7, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    goto/16 :goto_2

    .line 1722409
    :pswitch_2
    iget-object v0, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AuF;->y:LX/AuI;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722410
    iget-object v0, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/AuF;->z:LX/At0;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722411
    new-array v0, v9, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->m:Landroid/view/View;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v7

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v8

    invoke-direct {p0, v0}, LX/AuF;->a([Landroid/view/View;)V

    .line 1722412
    new-array v0, v7, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->w:Landroid/view/View;

    aput-object v1, v0, v4

    invoke-direct {p0, v7, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    goto/16 :goto_2

    .line 1722413
    :pswitch_3
    new-array v0, v9, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->m:Landroid/view/View;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v7

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v8

    invoke-direct {p0, v7, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    goto/16 :goto_2

    .line 1722414
    :pswitch_4
    iget-object v0, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/AuF;->A:LX/Au2;

    invoke-static {p0, v0, v1}, LX/AuF;->a(LX/AuF;Landroid/view/View;LX/AsV;)V

    .line 1722415
    new-array v0, v7, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->s:Lcom/facebook/widget/text/BetterTextView;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, LX/AuF;->a([Landroid/view/View;)V

    .line 1722416
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, LX/AuF;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v1, v0, v4

    iget-object v1, p0, LX/AuF;->w:Landroid/view/View;

    aput-object v1, v0, v7

    invoke-direct {p0, v7, v0}, LX/AuF;->a(Z[Landroid/view/View;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
