.class public final LX/Bkh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/ui/EventCreationPublishOverlay;)V
    .locals 0

    .prologue
    .line 1814669
    iput-object p1, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x2a89b5c5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1814670
    iget-object v0, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1814671
    if-nez v0, :cond_0

    .line 1814672
    const v0, 0x63a7cfa5

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1814673
    :goto_0
    return-void

    .line 1814674
    :cond_0
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/facebook/events/create/EventPublishAndScheduleActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1814675
    iget-object v3, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v3, v3, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814676
    iget-wide v8, v3, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v4, v8

    .line 1814677
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 1814678
    const-string v3, "extra_scheduled_publish_time"

    iget-object v4, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v4, v4, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814679
    iget-wide v8, v4, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v4, v8

    .line 1814680
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1814681
    :cond_1
    const-string v3, "extra_save_as_draft"

    iget-object v4, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v4, v4, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814682
    iget-boolean v5, v4, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    move v4, v5

    .line 1814683
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1814684
    const-string v3, "extra_event_start_time"

    iget-object v4, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v4, v4, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814685
    iget-object v5, v4, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v4, v5

    .line 1814686
    iget-wide v8, v4, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    move-wide v4, v8

    .line 1814687
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1814688
    iget-object v3, p0, LX/Bkh;->a:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v3, v3, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x6f

    invoke-interface {v3, v2, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814689
    const v0, 0x7bbcb746

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
