.class public final LX/C3r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1846412
    iput-object p1, p0, LX/C3r;->b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    iput-object p2, p0, LX/C3r;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const v0, 0x3d7f45c5

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1846413
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1846414
    const v2, 0x7f0813cf

    invoke-static {v2, v3, v3, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    .line 1846415
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1846416
    iget-object v2, p0, LX/C3r;->b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->c:LX/1Ck;

    const-string v3, "any"

    iget-object v4, p0, LX/C3r;->b:Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/creativeediting/FrameCallToActionPartDefinition;->d:LX/36e;

    invoke-virtual {v4}, LX/36e;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/C3q;

    invoke-direct {v5, p0, v0}, LX/C3q;-><init>(LX/C3r;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1846417
    const v0, 0x34d8488d

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
