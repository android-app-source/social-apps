.class public final LX/AX6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AX5;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V
    .locals 0

    .prologue
    .line 1683663
    iput-object p1, p0, LX/AX6;->a:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AcC;",
            ">;I",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1683664
    iget-object v0, p0, LX/AX6;->a:Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    const/16 v2, 0x8

    const/4 p3, 0x0

    .line 1683665
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1683666
    :cond_0
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->n:Landroid/view/ViewStub;

    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1683667
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->m:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1683668
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1683669
    :goto_0
    invoke-static {v0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->i(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V

    .line 1683670
    return-void

    .line 1683671
    :cond_1
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->o:Lcom/facebook/facecast/FacecastFacepileView;

    if-nez v1, :cond_2

    .line 1683672
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->n:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/FacecastFacepileView;

    iput-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->o:Lcom/facebook/facecast/FacecastFacepileView;

    .line 1683673
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1683674
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AcC;

    .line 1683675
    iget-object v1, v1, LX/AcC;->b:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1683676
    :cond_3
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->o:Lcom/facebook/facecast/FacecastFacepileView;

    invoke-virtual {v1, v2}, Lcom/facebook/facecast/FacecastFacepileView;->setFBIDs(Ljava/util/List;)V

    .line 1683677
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    if-eqz v1, :cond_4

    .line 1683678
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    invoke-virtual {v2, p1, p2}, LX/AYh;->a(Ljava/util/List;I)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683679
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1683680
    :cond_4
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->o:Lcom/facebook/facecast/FacecastFacepileView;

    invoke-virtual {v1, p3}, Lcom/facebook/facecast/FacecastFacepileView;->setVisibility(I)V

    .line 1683681
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->m:Landroid/view/View;

    invoke-virtual {v1, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
