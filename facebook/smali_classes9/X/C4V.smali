.class public LX/C4V;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4V",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847349
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847350
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C4V;->b:LX/0Zi;

    .line 1847351
    iput-object p1, p0, LX/C4V;->a:LX/0Ot;

    .line 1847352
    return-void
.end method

.method public static a(LX/0QB;)LX/C4V;
    .locals 4

    .prologue
    .line 1847363
    const-class v1, LX/C4V;

    monitor-enter v1

    .line 1847364
    :try_start_0
    sget-object v0, LX/C4V;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847365
    sput-object v2, LX/C4V;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847368
    new-instance v3, LX/C4V;

    const/16 p0, 0x1ef6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C4V;-><init>(LX/0Ot;)V

    .line 1847369
    move-object v0, v3

    .line 1847370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1847374
    check-cast p2, LX/C4U;

    .line 1847375
    iget-object v0, p0, LX/C4V;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;

    iget-object v1, p2, LX/C4U;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p2, LX/C4U;->b:I

    iget-object v3, p2, LX/C4U;->c:LX/1Pb;

    const/4 p2, 0x3

    const/4 p0, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1847376
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    .line 1847377
    invoke-static {v0, v1}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v9

    .line 1847378
    if-eqz v9, :cond_0

    move v4, v5

    move v7, v6

    move v2, v6

    .line 1847379
    :goto_0
    invoke-static {v8, v2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a(LX/1Dh;I)V

    .line 1847380
    invoke-static {v0, p1, v1, v3}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;

    move-result-object v10

    invoke-interface {v8, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    const v11, 0x7f020bbd

    invoke-virtual {v8, v11}, LX/1o5;->h(I)LX/1o5;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v11, 0x7f0b1bc2

    invoke-interface {v8, v11}, LX/1Di;->q(I)LX/1Di;

    move-result-object v8

    invoke-interface {v8, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v11

    invoke-static {v2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->b(I)Z

    move-result v8

    if-eqz v8, :cond_1

    move v8, v5

    :goto_1
    invoke-interface {v11, v6, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8, v5, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8, p0, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    invoke-interface {v10, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    iget-object v8, v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->e:LX/1VD;

    invoke-virtual {v8, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v1}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v8

    const v11, 0x7f0e0a02

    invoke-virtual {v8, v11}, LX/1X4;->n(I)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v8, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v11

    invoke-static {v2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->b(I)Z

    move-result v8

    if-eqz v8, :cond_2

    move v8, v5

    :goto_2
    invoke-interface {v11, v6, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v8

    invoke-interface {v10, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    const v11, 0x7f020bbc

    invoke-virtual {v8, v11}, LX/1o5;->h(I)LX/1o5;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v11, 0x7f0b1bc3

    invoke-interface {v8, v11}, LX/1Di;->q(I)LX/1Di;

    move-result-object v8

    invoke-interface {v8, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v11

    invoke-static {v2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->c(I)Z

    move-result v8

    if-eqz v8, :cond_3

    move v8, v5

    :goto_3
    invoke-interface {v11, p2, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8, v5, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8, p0, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    invoke-interface {v10, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    if-eqz v9, :cond_4

    const/4 v4, 0x0

    :goto_4
    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1847381
    return-object v0

    :cond_0
    move-object v4, v3

    .line 1847382
    check-cast v4, LX/1Po;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    sget-object v7, LX/1Qt;->PERMALINK:LX/1Qt;

    invoke-virtual {v4, v7}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v6

    move v7, v5

    move v2, v5

    .line 1847383
    goto/16 :goto_0

    .line 1847384
    :cond_1
    const v8, 0x7f0b00d5

    goto/16 :goto_1

    :cond_2
    const v8, 0x7f0b00d5

    goto :goto_2

    :cond_3
    const v8, 0x7f0b00d5

    goto :goto_3

    :cond_4
    iget-object v9, v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->f:LX/C4M;

    invoke-virtual {v9, p1}, LX/C4M;->c(LX/1De;)LX/C4K;

    move-result-object v9

    invoke-virtual {v9, v1}, LX/C4K;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C4K;

    move-result-object v9

    check-cast v3, LX/1Pr;

    invoke-virtual {v9, v3}, LX/C4K;->a(LX/1Pr;)LX/C4K;

    move-result-object v9

    const v10, 0x7f0e0a01

    .line 1847385
    iget-object v11, v9, LX/C4K;->a:LX/C4L;

    iput v10, v11, LX/C4L;->c:I

    .line 1847386
    move-object v9, v9

    .line 1847387
    const v10, 0x7f0a07ac

    .line 1847388
    iget-object v11, v9, LX/C4K;->a:LX/C4L;

    invoke-virtual {v9, v10}, LX/1Dp;->d(I)I

    move-result v0

    iput v0, v11, LX/C4L;->d:I

    .line 1847389
    move-object v9, v9

    .line 1847390
    iget-object v10, v9, LX/C4K;->a:LX/C4L;

    iput-boolean v4, v10, LX/C4L;->f:Z

    .line 1847391
    move-object v4, v9

    .line 1847392
    iget-object v9, v4, LX/C4K;->a:LX/C4L;

    iput-boolean v7, v9, LX/C4L;->e:Z

    .line 1847393
    move-object v4, v4

    .line 1847394
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    invoke-static {v2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->c(I)Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, v5

    :goto_5
    invoke-interface {v6, p2, v4}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v5, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    goto :goto_4

    :cond_5
    const v4, 0x7f0b00d5

    goto :goto_5

    :cond_6
    move v4, v5

    move v7, v6

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847361
    invoke-static {}, LX/1dS;->b()V

    .line 1847362
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C4T;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C4V",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1847353
    new-instance v1, LX/C4U;

    invoke-direct {v1, p0}, LX/C4U;-><init>(LX/C4V;)V

    .line 1847354
    iget-object v2, p0, LX/C4V;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C4T;

    .line 1847355
    if-nez v2, :cond_0

    .line 1847356
    new-instance v2, LX/C4T;

    invoke-direct {v2, p0}, LX/C4T;-><init>(LX/C4V;)V

    .line 1847357
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C4T;->a$redex0(LX/C4T;LX/1De;IILX/C4U;)V

    .line 1847358
    move-object v1, v2

    .line 1847359
    move-object v0, v1

    .line 1847360
    return-object v0
.end method
