.class public final LX/Afo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Afp;


# direct methods
.method public constructor <init>(LX/Afp;)V
    .locals 0

    .prologue
    .line 1699707
    iput-object p1, p0, LX/Afo;->a:LX/Afp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1699708
    iget-object v2, p0, LX/Afo;->a:LX/Afp;

    monitor-enter v2

    .line 1699709
    :try_start_0
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v1, v1, LX/Afp;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v1, v1, LX/Afp;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1699710
    :cond_0
    monitor-exit v2

    .line 1699711
    :goto_0
    return-void

    .line 1699712
    :cond_1
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v3, v1, LX/Afp;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/Afp;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_graphFailure"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Failed to get like events for "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/Afo;->a:LX/Afp;

    iget-object v5, v5, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v1, v1, LX/AeQ;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v4, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699713
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-boolean v1, v1, LX/AeQ;->e:Z

    if-nez v1, :cond_4

    .line 1699714
    :goto_2
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    const/4 v3, 0x1

    .line 1699715
    iput-boolean v3, v1, LX/Afp;->e:Z

    .line 1699716
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    if-eqz v1, :cond_2

    .line 1699717
    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    iget-object v3, p0, LX/Afo;->a:LX/Afp;

    invoke-virtual {v3}, LX/Afp;->g()LX/AeN;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/AeV;->a(LX/AeN;Z)V

    .line 1699718
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1699719
    :cond_3
    :try_start_1
    const-string v1, "no story id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1699720
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699721
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1699722
    iget-object v2, p0, LX/Afo;->a:LX/Afp;

    monitor-enter v2

    .line 1699723
    :try_start_0
    iget-object v0, p0, LX/Afo;->a:LX/Afp;

    iget-object v0, v0, LX/Afp;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Afo;->a:LX/Afp;

    iget-object v0, v0, LX/Afp;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1699724
    :cond_0
    monitor-exit v2

    .line 1699725
    :goto_0
    return-void

    .line 1699726
    :cond_1
    if-nez p1, :cond_2

    .line 1699727
    monitor-exit v2

    goto :goto_0

    .line 1699728
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1699729
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699730
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;

    .line 1699731
    if-nez v0, :cond_3

    .line 1699732
    monitor-exit v2

    goto :goto_0

    .line 1699733
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel;

    move-result-object v0

    .line 1699734
    if-nez v0, :cond_4

    .line 1699735
    monitor-exit v2

    goto :goto_0

    .line 1699736
    :cond_4
    iget-object v3, p0, LX/Afo;->a:LX/Afp;

    const/4 v4, 0x1

    .line 1699737
    iput-boolean v4, v3, LX/Afp;->e:Z

    .line 1699738
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel;

    move-result-object v0

    .line 1699739
    if-nez v0, :cond_5

    .line 1699740
    monitor-exit v2

    goto :goto_0

    .line 1699741
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel;->a()LX/0Px;

    move-result-object v3

    .line 1699742
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1699743
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel;

    .line 1699744
    new-instance v6, LX/Afm;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel$NodeModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v7

    invoke-static {v7}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveWatchLikeEventsQueryModel$FeedbackModel$NotableLikedWatchEventsModel$EdgesModel$NodeModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object p1

    invoke-static {p1}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object p1

    invoke-direct {v6, v7, p1}, LX/Afm;-><init>(LX/AcC;LX/AcC;)V

    move-object v0, v6

    .line 1699745
    if-eqz v0, :cond_6

    .line 1699746
    const/4 v6, 0x0

    invoke-interface {v4, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1699747
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1699748
    :cond_7
    iget-object v0, p0, LX/Afo;->a:LX/Afp;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    if-eqz v0, :cond_8

    .line 1699749
    iget-object v0, p0, LX/Afo;->a:LX/Afp;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    iget-object v1, p0, LX/Afo;->a:LX/Afp;

    invoke-virtual {v1}, LX/Afp;->g()LX/AeN;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v4, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    .line 1699750
    :cond_8
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
