.class public final LX/Aus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Auj;


# instance fields
.field public final synthetic a:LX/Auw;


# direct methods
.method public constructor <init>(LX/Auw;)V
    .locals 0

    .prologue
    .line 1723365
    iput-object p1, p0, LX/Aus;->a:LX/Auw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 1723366
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    .line 1723367
    sget-object v2, LX/870;->TEXT_EDITING:LX/870;

    invoke-static {v0, v2}, LX/Auw;->b(LX/Auw;LX/870;)V

    .line 1723368
    iget-object v2, v0, LX/Auw;->f:LX/Av1;

    .line 1723369
    iget-object v3, v0, LX/Auw;->l:LX/0tS;

    invoke-virtual {v3}, LX/0tS;->d()I

    move-result v3

    .line 1723370
    invoke-static {v0}, LX/Auw;->g(LX/Auw;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget-object v4, v0, LX/Auw;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1a5f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, v0, LX/Auw;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1a8b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    move v3, v3

    .line 1723371
    iget-object v4, v2, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setTranslationY(F)V

    .line 1723372
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Auw;->i:Z

    .line 1723373
    iget-object v2, v0, LX/Auw;->g:LX/Aur;

    .line 1723374
    const/16 v6, 0x8

    .line 1723375
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v5, :cond_0

    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-gtz v5, :cond_1

    .line 1723376
    :cond_0
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1723377
    iget-object v5, v2, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1723378
    :goto_0
    iget-object v2, v0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    sget-object v3, LX/Auw;->a:LX/0jK;

    sget-object v4, LX/86o;->COLOR_PICKER:LX/86o;

    invoke-static {v2, v3, v4}, LX/87N;->a(LX/0il;LX/0jK;LX/86o;)V

    .line 1723379
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    iget-object v0, v0, LX/Auw;->k:LX/Ar5;

    .line 1723380
    iget-object v2, v0, LX/Ar5;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v3, LX/ArJ;->TAP_TEXT_BUTTON:LX/ArJ;

    .line 1723381
    iget-object v4, v2, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setTextSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    iget-object v5, v2, LX/ArT;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setTextSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v2, v4}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1723382
    iget-object v4, v2, LX/ArT;->b:LX/ArL;

    .line 1723383
    sget-object v5, LX/ArH;->START_TEXT_SESSION:LX/ArH;

    invoke-static {v4, v5, v3}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v6}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-static {v4, v5}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1723384
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Auw;->a(Z)V

    .line 1723385
    return-void

    .line 1723386
    :cond_1
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v5

    iget v6, v2, LX/Aur;->p:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-nez v5, :cond_2

    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v5

    iget v6, v2, LX/Aur;->q:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_3

    .line 1723387
    :cond_2
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v5

    iget-object v6, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v2, LX/Aur;->l:F

    .line 1723388
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v5

    iget-object v6, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v2, LX/Aur;->m:F

    .line 1723389
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getRotation()F

    move-result v5

    iput v5, v2, LX/Aur;->n:F

    .line 1723390
    const/4 v5, 0x1

    iput-boolean v5, v2, LX/Aur;->o:Z

    .line 1723391
    :cond_3
    iget-object v5, v2, LX/Aur;->w:LX/Av3;

    iget-object v6, v2, LX/Aur;->b:Landroid/widget/ImageView;

    new-instance v7, Landroid/graphics/Rect;

    iget v8, v2, LX/Aur;->p:I

    iget v9, v2, LX/Aur;->q:I

    iget v10, v2, LX/Aur;->p:I

    iget v11, v2, LX/Aur;->r:I

    add-int/2addr v10, v11

    iget v11, v2, LX/Aur;->q:I

    iget v12, v2, LX/Aur;->s:I

    add-int/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v8, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getRotation()F

    move-result v8

    const/4 v9, 0x0

    new-instance v10, LX/Aul;

    invoke-direct {v10, v2}, LX/Aul;-><init>(LX/Aur;)V

    invoke-virtual/range {v5 .. v10}, LX/Av3;->a(Landroid/widget/ImageView;Landroid/graphics/Rect;FFLX/Auk;)V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 14

    .prologue
    .line 1723392
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    invoke-static {v0, p1}, LX/Auw;->b(LX/Auw;Z)V

    .line 1723393
    if-nez p1, :cond_0

    .line 1723394
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    invoke-static {v0}, LX/Auw;->f(LX/Auw;)V

    .line 1723395
    :goto_0
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    iget-object v0, v0, LX/Auw;->k:LX/Ar5;

    .line 1723396
    iget-object v2, v0, LX/Ar5;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v3, LX/ArJ;->TAP_CONFIRM_BUTTON:LX/ArJ;

    .line 1723397
    iget-object v5, v2, LX/ArT;->b:LX/ArL;

    iget-object v4, v2, LX/ArT;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v4, v2, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionStartTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-float v4, v6

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v4, v6

    .line 1723398
    sget-object v10, LX/ArH;->END_TEXT_SESSION:LX/ArH;

    invoke-static {v5, v10, v3}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    sget-object v11, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v11}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v11

    float-to-double v12, v4

    invoke-virtual {v10, v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    invoke-static {v5, v10}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1723399
    iget-object v4, v2, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setTextSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v2, v4}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1723400
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Auw;->a(Z)V

    .line 1723401
    return-void

    .line 1723402
    :cond_0
    iget-object v0, p0, LX/Aus;->a:LX/Auw;

    sget-object v1, LX/870;->TEXT_DRAGGING:LX/870;

    invoke-static {v0, v1}, LX/Auw;->b(LX/Auw;LX/870;)V

    goto :goto_0
.end method
