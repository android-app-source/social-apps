.class public final LX/Btn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPlace;

.field public final synthetic b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 0

    .prologue
    .line 1829527
    iput-object p1, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    iput-object p2, p0, LX/Btn;->a:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const v0, 0x636e1441

    invoke-static {v4, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1829528
    iget-object v1, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    iget-object v1, v1, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->k:LX/17Y;

    iget-object v2, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    invoke-virtual {v2}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aL:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/Btn;->a:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "android_feed_add_photo_button"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1829529
    if-eqz v1, :cond_0

    .line 1829530
    const-string v2, "profile_name"

    iget-object v3, p0, LX/Btn;->a:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1829531
    iget-object v2, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    iget-object v2, v2, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    invoke-virtual {v3}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1829532
    :goto_0
    const v1, 0x50d97b1f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1829533
    :cond_0
    iget-object v1, p0, LX/Btn;->b:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    iget-object v1, v1, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->j:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Could not create Suggest Edits intent for Add Photo button in News Feed"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
