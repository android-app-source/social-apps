.class public LX/BVF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field private b:[B

.field private c:[B

.field private d:Ljava/lang/String;

.field private final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private f:Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;


# direct methods
.method public constructor <init>([B[BLjava/lang/String;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .param p1    # [B
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # [B
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1790307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790308
    iput-object p1, p0, LX/BVF;->b:[B

    .line 1790309
    iput-object p2, p0, LX/BVF;->c:[B

    .line 1790310
    iput-object p3, p0, LX/BVF;->d:Ljava/lang/String;

    .line 1790311
    iget-object v0, p0, LX/BVF;->b:[B

    .line 1790312
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 1790313
    new-instance p2, LX/BES;

    invoke-direct {p2}, LX/BES;-><init>()V

    .line 1790314
    sget-object p3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result p3

    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr p3, v0

    .line 1790315
    iput p3, p2, LX/BES;->a:I

    iput-object p1, p2, LX/BES;->b:Ljava/nio/ByteBuffer;

    move-object p3, p2

    .line 1790316
    move-object p2, p3

    .line 1790317
    move-object p1, p2

    .line 1790318
    const/4 p2, 0x6

    invoke-virtual {p1, p2}, LX/0eW;->a(I)I

    move-result p2

    if-eqz p2, :cond_0

    iget-object p3, p1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v0, p1, LX/0eW;->a:I

    add-int/2addr p2, v0

    invoke-virtual {p3, p2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p2

    :goto_0
    move p1, p2

    .line 1790319
    move v0, p1

    .line 1790320
    iput v0, p0, LX/BVF;->a:I

    .line 1790321
    iput-object p4, p0, LX/BVF;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1790322
    return-void

    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;
    .locals 4

    .prologue
    .line 1790323
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BVF;->f:Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    if-nez v0, :cond_0

    .line 1790324
    new-instance v0, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    iget-object v1, p0, LX/BVF;->b:[B

    iget-object v2, p0, LX/BVF;->c:[B

    iget-object v3, p0, LX/BVF;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;-><init>([B[BLjava/lang/String;)V

    iput-object v0, p0, LX/BVF;->f:Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    .line 1790325
    const/4 v0, 0x0

    iput-object v0, p0, LX/BVF;->b:[B

    .line 1790326
    const/4 v0, 0x0

    iput-object v0, p0, LX/BVF;->c:[B

    .line 1790327
    const/4 v0, 0x0

    iput-object v0, p0, LX/BVF;->d:Ljava/lang/String;

    .line 1790328
    :cond_0
    iget-object v0, p0, LX/BVF;->f:Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1790329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final b()LX/0P1;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/5Pg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1790330
    invoke-static {p0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->describeLayout()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1790331
    new-instance v1, LX/BEU;

    invoke-direct {v1}, LX/BEU;-><init>()V

    .line 1790332
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v2, v3

    .line 1790333
    iput v2, v1, LX/BEU;->a:I

    iput-object v0, v1, LX/BEU;->b:Ljava/nio/ByteBuffer;

    move-object v2, v1

    .line 1790334
    move-object v1, v2

    .line 1790335
    move-object v1, v1

    .line 1790336
    invoke-static {p0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->particlesBuffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1790337
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 1790338
    const/4 v0, 0x0

    .line 1790339
    :goto_0
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v4}, LX/0eW;->d(I)I

    move-result v4

    :goto_1
    move v4, v4

    .line 1790340
    if-ge v0, v4, :cond_0

    .line 1790341
    new-instance v4, LX/BEX;

    invoke-direct {v4}, LX/BEX;-><init>()V

    .line 1790342
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1, v5}, LX/0eW;->e(I)I

    move-result v5

    mul-int/lit8 v6, v0, 0x4

    add-int/2addr v5, v6

    invoke-virtual {v1, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1790343
    iput v5, v4, LX/BEX;->a:I

    iput-object v6, v4, LX/BEX;->b:Ljava/nio/ByteBuffer;

    move-object v5, v4

    .line 1790344
    :goto_2
    move-object v4, v5

    .line 1790345
    move-object v4, v4

    .line 1790346
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_3

    iget v6, v4, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v4, v5}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    move-object v5, v5

    .line 1790347
    new-instance v6, LX/5Pg;

    .line 1790348
    const/4 v7, 0x6

    invoke-virtual {v4, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_4

    iget-object v8, v4, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v4, LX/0eW;->a:I

    add-int/2addr v7, p0

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v7

    :goto_4
    move v7, v7

    .line 1790349
    const/4 v8, 0x6

    invoke-virtual {v1, v8}, LX/0eW;->a(I)I

    move-result v8

    if-eqz v8, :cond_5

    iget-object v9, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v8, p0

    invoke-virtual {v9, v8}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v8

    :goto_5
    move v8, v8

    .line 1790350
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, LX/0eW;->a(I)I

    move-result v9

    if-eqz v9, :cond_6

    iget-object v10, v4, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v4, LX/0eW;->a:I

    add-int/2addr v9, p0

    invoke-virtual {v10, v9}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v9

    :goto_6
    move v4, v9

    .line 1790351
    invoke-direct {v6, v2, v7, v8, v4}, LX/5Pg;-><init>(Ljava/nio/ByteBuffer;III)V

    invoke-virtual {v3, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1790352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1790353
    :cond_0
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    goto :goto_5

    :cond_6
    const/4 v9, 0x0

    goto :goto_6
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const v2, 0xaf0001

    .line 1790354
    :try_start_0
    iget-object v0, p0, LX/BVF;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xaf0001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1790355
    invoke-static {p0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->update()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1790356
    iget-object v1, p0, LX/BVF;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVF;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1790357
    invoke-static {p0}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->getCapabilities()I

    move-result v0

    return v0
.end method
