.class public LX/Av8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Av8;


# instance fields
.field public final a:LX/AvB;

.field public final b:LX/Av5;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/03V;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AvE;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z

.field private final g:LX/0y3;


# direct methods
.method public constructor <init>(LX/AvB;LX/Av5;Ljava/util/concurrent/Executor;LX/03V;LX/0Ot;LX/0W3;LX/0y3;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AvB;",
            "LX/Av5;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/AvE;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0y3;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1723745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723746
    iput-object p1, p0, LX/Av8;->a:LX/AvB;

    .line 1723747
    iput-object p2, p0, LX/Av8;->b:LX/Av5;

    .line 1723748
    iput-object p3, p0, LX/Av8;->c:Ljava/util/concurrent/Executor;

    .line 1723749
    iput-object p4, p0, LX/Av8;->d:LX/03V;

    .line 1723750
    iput-object p5, p0, LX/Av8;->e:LX/0Ot;

    .line 1723751
    sget-wide v0, LX/0X5;->jR:J

    invoke-interface {p6, v0, v1}, LX/0W4;->a(J)Z

    move-result v0

    iput-boolean v0, p0, LX/Av8;->f:Z

    .line 1723752
    iput-object p7, p0, LX/Av8;->g:LX/0y3;

    .line 1723753
    return-void
.end method

.method public static a(LX/0QB;)LX/Av8;
    .locals 11

    .prologue
    .line 1723770
    sget-object v0, LX/Av8;->h:LX/Av8;

    if-nez v0, :cond_1

    .line 1723771
    const-class v1, LX/Av8;

    monitor-enter v1

    .line 1723772
    :try_start_0
    sget-object v0, LX/Av8;->h:LX/Av8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1723773
    if-eqz v2, :cond_0

    .line 1723774
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1723775
    new-instance v3, LX/Av8;

    invoke-static {v0}, LX/AvB;->a(LX/0QB;)LX/AvB;

    move-result-object v4

    check-cast v4, LX/AvB;

    invoke-static {v0}, LX/Av5;->b(LX/0QB;)LX/Av5;

    move-result-object v5

    check-cast v5, LX/Av5;

    invoke-static {v0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const/16 v8, 0x2296

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v10

    check-cast v10, LX/0y3;

    invoke-direct/range {v3 .. v10}, LX/Av8;-><init>(LX/AvB;LX/Av5;Ljava/util/concurrent/Executor;LX/03V;LX/0Ot;LX/0W3;LX/0y3;)V

    .line 1723776
    move-object v0, v3

    .line 1723777
    sput-object v0, LX/Av8;->h:LX/Av8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1723778
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1723779
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1723780
    :cond_1
    sget-object v0, LX/Av8;->h:LX/Av8;

    return-object v0

    .line 1723781
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1723782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0is;",
            ">(",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            "TModelData;)",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;"
        }
    .end annotation

    .prologue
    .line 1723754
    invoke-static {p1}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1723755
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1752514608329267"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1723756
    :cond_0
    :goto_0
    return-object p0

    .line 1723757
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "178267072637018"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1723758
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/87Q;->c(LX/0Px;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1723759
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/87Q;->b(LX/0Px;Ljava/lang/String;)I

    move-result v1

    .line 1723760
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1723761
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    .line 1723762
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 1723763
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1723764
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1723765
    :goto_1
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v3

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v2

    move-object p0, v2

    .line 1723766
    goto :goto_0

    .line 1723767
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {v3, p1, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1723768
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1723769
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    invoke-virtual {v3, v1, p1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_1
.end method
