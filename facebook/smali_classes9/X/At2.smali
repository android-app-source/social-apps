.class public LX/At2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# instance fields
.field private final a:LX/86e;

.field private final b:LX/AsL;

.field private c:Z

.field private d:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;


# direct methods
.method public constructor <init>(LX/AsL;LX/86e;)V
    .locals 0
    .param p1    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1721151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721152
    iput-object p1, p0, LX/At2;->b:LX/AsL;

    .line 1721153
    iput-object p2, p0, LX/At2;->a:LX/86e;

    .line 1721154
    return-void
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1721157
    iget-object v0, p0, LX/At2;->a:LX/86e;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 0

    .prologue
    .line 1721155
    iput-object p1, p0, LX/At2;->d:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1721156
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1721145
    iput-boolean p1, p0, LX/At2;->c:Z

    .line 1721146
    if-eqz p1, :cond_1

    .line 1721147
    iget-object v0, p0, LX/At2;->b:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1721148
    :cond_0
    :goto_0
    return-void

    .line 1721149
    :cond_1
    iget-object v0, p0, LX/At2;->d:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    if-eqz v0, :cond_0

    .line 1721150
    iget-object v0, p0, LX/At2;->b:LX/AsL;

    iget-object v1, p0, LX/At2;->d:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1721144
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1721158
    iget-boolean v0, p0, LX/At2;->c:Z

    if-eqz v0, :cond_0

    .line 1721159
    iget-object v0, p0, LX/At2;->b:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1721160
    :cond_0
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1721143
    iget-object v0, p0, LX/At2;->d:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721142
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721139
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721141
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721140
    const/4 v0, 0x0

    return-object v0
.end method
