.class public final LX/BXN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793040
    iput-object p1, p0, LX/BXN;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    .line 1793043
    iget-object v0, p0, LX/BXN;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1793044
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1793045
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1793046
    const/4 v2, 0x1

    .line 1793047
    iget-object v5, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->y:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1793048
    :cond_0
    const/4 v2, 0x0

    .line 1793049
    :cond_1
    iput-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->y:Ljava/lang/String;

    .line 1793050
    move v1, v2

    .line 1793051
    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1793052
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1793053
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1793054
    invoke-virtual {v1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v6

    .line 1793055
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p0

    invoke-interface {p1, v3, p0}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1793056
    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v6, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v0, v1, v2, v6}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Ljava/util/List;)V

    move v1, v4

    :goto_1
    move v2, v1

    .line 1793057
    goto :goto_0

    :cond_2
    move v2, v3

    .line 1793058
    :cond_3
    if-eqz v2, :cond_4

    .line 1793059
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->w()V

    .line 1793060
    :cond_4
    iget-boolean v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v1, :cond_5

    .line 1793061
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n:LX/BWn;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-eqz v2, :cond_6

    .line 1793062
    :goto_2
    iput-boolean v4, v1, LX/BWn;->h:Z

    .line 1793063
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->x()V

    .line 1793064
    return-void

    :cond_6
    move v4, v3

    .line 1793065
    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1793042
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1793041
    return-void
.end method
