.class public final LX/CWO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/CU5;",
        "LX/CWQ;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CWP;


# direct methods
.method public constructor <init>(LX/CWP;)V
    .locals 0

    .prologue
    .line 1908053
    iput-object p1, p0, LX/CWO;->a:LX/CWP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1908054
    check-cast p1, LX/CU5;

    .line 1908055
    if-nez p1, :cond_0

    .line 1908056
    const/4 v0, 0x0

    .line 1908057
    :goto_0
    return-object v0

    .line 1908058
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1908059
    iget v0, p1, LX/CU5;->c:I

    if-lez v0, :cond_1

    .line 1908060
    iget-object v0, p0, LX/CWO;->a:LX/CWP;

    iget-object v0, v0, LX/CWP;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082a95

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, LX/CU5;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908061
    :cond_1
    iget-object v0, p1, LX/CU5;->d:LX/CU0;

    if-eqz v0, :cond_5

    .line 1908062
    iget-object v0, p1, LX/CU5;->d:LX/CU0;

    iget-object v0, v0, LX/CU0;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1908063
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1908064
    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908065
    :cond_2
    iget-object v0, p1, LX/CU5;->d:LX/CU0;

    iget-object v0, v0, LX/CU0;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908066
    :cond_3
    iget-object v0, p1, LX/CU5;->d:LX/CU0;

    iget-object v0, v0, LX/CU0;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1908067
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 1908068
    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908069
    :cond_4
    iget-object v0, p1, LX/CU5;->d:LX/CU0;

    iget-object v0, v0, LX/CU0;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1908070
    :cond_5
    new-instance v0, LX/CWQ;

    iget-object v2, p0, LX/CWO;->a:LX/CWP;

    iget-object v2, v2, LX/CWP;->e:Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    iget-object v4, p1, LX/CU5;->b:LX/CTu;

    iget-wide v4, v4, LX/CTu;->a:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p1, LX/CU5;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v1, v3}, LX/CWQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908071
    const/4 v0, 0x0

    return v0
.end method
