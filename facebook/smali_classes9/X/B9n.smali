.class public LX/B9n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0f6;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/B9m;

.field private final b:LX/B9m;

.field private final c:LX/0SG;

.field private d:LX/B9o;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1752243
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/B9n;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SF;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752245
    new-instance v0, LX/B9m;

    invoke-direct {v0}, LX/B9m;-><init>()V

    iput-object v0, p0, LX/B9n;->a:LX/B9m;

    .line 1752246
    new-instance v0, LX/B9m;

    invoke-direct {v0}, LX/B9m;-><init>()V

    iput-object v0, p0, LX/B9n;->b:LX/B9m;

    .line 1752247
    const/4 v3, 0x0

    .line 1752248
    new-instance v0, LX/B9o;

    sget-object v1, LX/B9l;->UNKNOWN:LX/B9l;

    const/4 v2, 0x0

    invoke-direct {v0, v3, v1, v3, v2}, LX/B9o;-><init>(Ljava/lang/Throwable;LX/B9l;Ljava/lang/String;Z)V

    move-object v0, v0

    .line 1752249
    iput-object v0, p0, LX/B9n;->d:LX/B9o;

    .line 1752250
    iput-object p1, p0, LX/B9n;->c:LX/0SG;

    .line 1752251
    return-void
.end method

.method public static a(LX/0QB;)LX/B9n;
    .locals 7

    .prologue
    .line 1752252
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1752253
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1752254
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1752255
    if-nez v1, :cond_0

    .line 1752256
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1752257
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1752258
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1752259
    sget-object v1, LX/B9n;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1752260
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1752261
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1752262
    :cond_1
    if-nez v1, :cond_4

    .line 1752263
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1752264
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1752265
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1752266
    new-instance p0, LX/B9n;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SF;

    invoke-direct {p0, v1}, LX/B9n;-><init>(LX/0SF;)V

    .line 1752267
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1752268
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1752269
    if-nez v1, :cond_2

    .line 1752270
    sget-object v0, LX/B9n;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1752271
    :goto_1
    if-eqz v0, :cond_3

    .line 1752272
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1752273
    :goto_3
    check-cast v0, LX/B9n;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1752274
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1752275
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1752276
    :catchall_1
    move-exception v0

    .line 1752277
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1752278
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1752279
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1752280
    :cond_2
    :try_start_8
    sget-object v0, LX/B9n;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a(II)V
    .locals 5

    .prologue
    .line 1752281
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B9n;->a:LX/B9m;

    .line 1752282
    iput p1, v0, LX/B9m;->a:I

    .line 1752283
    iget-object v0, p0, LX/B9n;->a:LX/B9m;

    .line 1752284
    iput p2, v0, LX/B9m;->b:I

    .line 1752285
    iget-object v0, p0, LX/B9n;->a:LX/B9m;

    iget-object v1, p0, LX/B9n;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1752286
    iput-wide v2, v0, LX/B9m;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1752287
    monitor-exit p0

    return-void

    .line 1752288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(III)V
    .locals 1

    .prologue
    .line 1752289
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B9n;->a:LX/B9m;

    .line 1752290
    iput p3, v0, LX/B9m;->c:I

    .line 1752291
    invoke-virtual {p0, p1, p2}, LX/B9n;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1752292
    monitor-exit p0

    return-void

    .line 1752293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/B9o;)V
    .locals 1

    .prologue
    .line 1752294
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/B9n;->d:LX/B9o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1752295
    monitor-exit p0

    return-void

    .line 1752296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getDebugInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1752297
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "latestInboxFromPush"

    iget-object v2, p0, LX/B9n;->a:LX/B9m;

    invoke-virtual {v2}, LX/B9m;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "latestInboxFromFetch"

    iget-object v2, p0, LX/B9n;->b:LX/B9m;

    invoke-virtual {v2}, LX/B9m;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "latestMarkFolderSeenResult"

    iget-object v2, p0, LX/B9n;->d:LX/B9o;

    invoke-virtual {v2}, LX/B9o;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
