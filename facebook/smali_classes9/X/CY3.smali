.class public LX/CY3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910726
    iput-object p1, p0, LX/CY3;->a:LX/0Zb;

    .line 1910727
    return-void
.end method

.method public static a(LX/0QB;)LX/CY3;
    .locals 1

    .prologue
    .line 1910728
    invoke-static {p0}, LX/CY3;->b(LX/0QB;)LX/CY3;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1910729
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/CY4;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pages_public_view"

    .line 1910730
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1910731
    move-object v0, v0

    .line 1910732
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/CY3;
    .locals 2

    .prologue
    .line 1910733
    new-instance v1, LX/CY3;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/CY3;-><init>(LX/0Zb;)V

    .line 1910734
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/CY5;)V
    .locals 3

    .prologue
    .line 1910735
    iget-object v0, p0, LX/CY3;->a:LX/0Zb;

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_ERROR:LX/CY4;

    invoke-static {v1, p1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1910736
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1910737
    iget-object v0, p0, LX/CY3;->a:LX/0Zb;

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_NEXT_BUTTON:LX/CY4;

    invoke-static {v1, p1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "step"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1910738
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1910739
    iget-object v0, p0, LX/CY3;->a:LX/0Zb;

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    invoke-static {v1, p1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "step"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1910740
    return-void
.end method
