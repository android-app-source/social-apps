.class public LX/BpX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1zC;

.field public final b:LX/1Uf;


# direct methods
.method public constructor <init>(LX/1Uf;LX/1zC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1823169
    iput-object p1, p0, LX/BpX;->b:LX/1Uf;

    .line 1823170
    iput-object p2, p0, LX/BpX;->a:LX/1zC;

    .line 1823171
    return-void
.end method

.method public static a(LX/0QB;)LX/BpX;
    .locals 5

    .prologue
    .line 1823172
    const-class v1, LX/BpX;

    monitor-enter v1

    .line 1823173
    :try_start_0
    sget-object v0, LX/BpX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823174
    sput-object v2, LX/BpX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823177
    new-instance p0, LX/BpX;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v3

    check-cast v3, LX/1Uf;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v4

    check-cast v4, LX/1zC;

    invoke-direct {p0, v3, v4}, LX/BpX;-><init>(LX/1Uf;LX/1zC;)V

    .line 1823178
    move-object v0, p0

    .line 1823179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BpX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;
    .locals 3

    .prologue
    .line 1823183
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 1823184
    if-nez p1, :cond_1

    .line 1823185
    const-string v0, ""

    .line 1823186
    :goto_0
    move-object v0, v0

    .line 1823187
    :goto_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1823188
    iget-object v0, p0, LX/BpX;->a:LX/1zC;

    invoke-virtual {v0, v1, p2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 1823189
    return-object v1

    .line 1823190
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, LX/BpX;->b:LX/1Uf;

    invoke-static {p1}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2, v2}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method
