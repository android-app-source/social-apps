.class public LX/B3K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/B3O;

.field private final c:LX/B4O;

.field private final d:LX/B4H;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final h:LX/5vm;

.field private final i:LX/B5y;

.field private final j:LX/B4D;

.field private k:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/B3O;LX/B4O;LX/B4H;Lcom/facebook/content/SecureContextHelper;LX/0Ot;Ljava/util/concurrent/Executor;LX/5vm;LX/B5y;LX/B4D;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B3O;",
            "LX/B4O;",
            "LX/B4H;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/5vm;",
            "LX/B5y;",
            "LX/B4D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1740115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740116
    const/4 v0, 0x0

    iput-object v0, p0, LX/B3K;->k:LX/1Mv;

    .line 1740117
    iput-object p1, p0, LX/B3K;->b:LX/B3O;

    .line 1740118
    iput-object p2, p0, LX/B3K;->c:LX/B4O;

    .line 1740119
    iput-object p3, p0, LX/B3K;->d:LX/B4H;

    .line 1740120
    iput-object p4, p0, LX/B3K;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1740121
    iput-object p5, p0, LX/B3K;->f:LX/0Ot;

    .line 1740122
    iput-object p6, p0, LX/B3K;->g:Ljava/util/concurrent/Executor;

    .line 1740123
    iput-object p7, p0, LX/B3K;->h:LX/5vm;

    .line 1740124
    iput-object p8, p0, LX/B3K;->i:LX/B5y;

    .line 1740125
    iput-object p9, p0, LX/B3K;->j:LX/B4D;

    .line 1740126
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1740127
    const-string v0, "camera"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1740128
    const-string v0, "photo_taken"

    .line 1740129
    :goto_0
    return-object v0

    .line 1740130
    :cond_0
    const-string v0, "existing"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1740131
    invoke-direct {p0}, LX/B3K;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "current_profile_picture"

    goto :goto_0

    :cond_1
    const-string v0, "existing_photo"

    goto :goto_0

    .line 1740132
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Don\'t have a PhotoSource matching the profilePhotoMethod: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;Landroid/content/Intent;Landroid/app/Activity;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 1740133
    iget-object v1, p0, LX/B3K;->i:LX/B5y;

    invoke-virtual {p1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x4

    const/4 v6, 0x3

    iget-object v0, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->i()LX/5QV;

    move-result-object v7

    iget-object v0, p0, LX/B3K;->j:LX/B4D;

    invoke-virtual {p1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/B4D;->a(J)J

    move-result-wide v10

    move-object v2, p3

    move-object v9, v8

    invoke-interface/range {v1 .. v11}, LX/B5y;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    .line 1740134
    return-void
.end method

.method private a(Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;Landroid/content/Intent;Landroid/app/Activity;LX/B4P;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1740135
    const-string v0, "staging_ground_selected_frame"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Expected a selected frame from Staging Ground"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1740136
    const-string v0, "staging_ground_selected_frame"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/5QV;

    .line 1740137
    const-string v0, "staging_ground_all_frames"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1740138
    iget-object v1, p0, LX/B3K;->b:LX/B3O;

    iget-object v3, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v3}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v3

    .line 1740139
    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/B3N;->f:Ljava/lang/String;

    .line 1740140
    invoke-static {v0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-result-object v4

    iput-object v4, v3, LX/B3N;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1740141
    move-object v0, v3

    .line 1740142
    invoke-virtual {v0}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1740143
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1740144
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p5}, LX/B3K;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->f()Ljava/lang/String;

    move-result-object v5

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/B4O;->a(Ljava/lang/String;Ljava/lang/String;LX/B4P;Ljava/lang/String;Ljava/lang/String;)V

    .line 1740145
    iget-object v0, p0, LX/B3K;->d:LX/B4H;

    const-string v1, "extra_profile_pic_expiration"

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v1, "staging_ground_photo_caption"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, v6

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/B4H;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1740146
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1740147
    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1740148
    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 1740149
    iget-object v0, p0, LX/B3K;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1mR;

    .line 1740150
    new-instance v0, LX/B3J;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/B3J;-><init>(LX/B3K;Landroid/app/ProgressDialog;LX/1mR;Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    .line 1740151
    iget-object v1, p0, LX/B3K;->g:Ljava/util/concurrent/Executor;

    invoke-static {v6, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740152
    new-instance v1, LX/1Mv;

    invoke-direct {v1, v6, v0}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v1, p0, LX/B3K;->k:LX/1Mv;

    .line 1740153
    return-void
.end method

.method public static b(LX/0QB;)LX/B3K;
    .locals 10

    .prologue
    .line 1740154
    new-instance v0, LX/B3K;

    invoke-static {p0}, LX/B3O;->a(LX/0QB;)LX/B3O;

    move-result-object v1

    check-cast v1, LX/B3O;

    invoke-static {p0}, LX/B4O;->a(LX/0QB;)LX/B4O;

    move-result-object v2

    check-cast v2, LX/B4O;

    invoke-static {p0}, LX/B4H;->a(LX/0QB;)LX/B4H;

    move-result-object v3

    check-cast v3, LX/B4H;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v7

    check-cast v7, LX/5vm;

    invoke-static {p0}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v8

    check-cast v8, LX/B5y;

    invoke-static {p0}, LX/B4D;->a(LX/0QB;)LX/B4D;

    move-result-object v9

    check-cast v9, LX/B4D;

    invoke-direct/range {v0 .. v9}, LX/B3K;-><init>(LX/B3O;LX/B4O;LX/B4H;Lcom/facebook/content/SecureContextHelper;LX/0Ot;Ljava/util/concurrent/Executor;LX/5vm;LX/B5y;LX/B4D;)V

    .line 1740155
    const/16 v1, 0xb0b

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1740156
    iput-object v1, v0, LX/B3K;->a:LX/0Or;

    .line 1740157
    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 1740158
    iget-object v0, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1740159
    iget-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1740160
    iget-object v1, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    .line 1740161
    iget-object p0, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    if-eqz p0, :cond_0

    iget-object p0, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;->j()Ljava/lang/String;

    move-result-object p0

    :goto_0
    move-object v1, p0

    .line 1740162
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1740163
    iget-object v0, p0, LX/B3K;->k:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1740164
    iget-object v0, p0, LX/B3K;->k:LX/1Mv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1740165
    :cond_0
    return-void
.end method

.method public final a(IILandroid/content/Intent;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;LX/B4P;Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 1740166
    iget-boolean v0, p6, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->D:Z

    move v0, v0

    .line 1740167
    if-eqz v0, :cond_0

    .line 1740168
    :goto_0
    return-void

    .line 1740169
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1740170
    :pswitch_0
    if-ne p2, v3, :cond_1

    .line 1740171
    const-string v5, "camera"

    move-object v0, p0

    move-object v1, p4

    move-object v2, p3

    move-object v3, p6

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LX/B3K;->a(Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;Landroid/content/Intent;Landroid/app/Activity;LX/B4P;Ljava/lang/String;)V

    goto :goto_0

    .line 1740172
    :cond_1
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/B4O;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1740173
    :pswitch_1
    if-ne p2, v3, :cond_2

    .line 1740174
    invoke-direct {p0, p4, p3, p6}, LX/B3K;->a(Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;Landroid/content/Intent;Landroid/app/Activity;)V

    goto :goto_0

    .line 1740175
    :cond_2
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/B4O;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1740176
    :pswitch_2
    if-ne p2, v3, :cond_3

    .line 1740177
    const-string v5, "existing"

    move-object v0, p0

    move-object v1, p4

    move-object v2, p3

    move-object v3, p6

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, LX/B3K;->a(Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;Landroid/content/Intent;Landroid/app/Activity;LX/B4P;Ljava/lang/String;)V

    goto :goto_0

    .line 1740178
    :cond_3
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/B4O;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1740179
    :pswitch_3
    if-ne p2, v3, :cond_6

    .line 1740180
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->i()LX/5QV;

    move-result-object v2

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    .line 1740181
    const-string v5, "heisman_profile_picture_set"

    const-string v6, "heisman_composer_session_id"

    const-string p2, "profile_pic_frame_id"

    const-string p4, "media_type"

    const-string p5, "video"

    move-object v4, v0

    move-object p1, v1

    move-object p3, v2

    .line 1740182
    iget-object v0, v4, LX/B4O;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, v5, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1740183
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1740184
    const-string v1, "profile_picture_overlay"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1740185
    invoke-virtual {v0, v6, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1740186
    invoke-virtual {v0, p2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1740187
    invoke-virtual {v0, p4, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1740188
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1740189
    :cond_4
    iget-object v0, p0, LX/B3K;->h:LX/5vm;

    invoke-virtual {v0}, LX/5vm;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1740190
    if-eqz v0, :cond_5

    .line 1740191
    iget-object v1, p0, LX/B3K;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1740192
    :cond_5
    invoke-virtual {p6, v3}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->setResult(I)V

    .line 1740193
    invoke-virtual {p6}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->finish()V

    goto/16 :goto_0

    .line 1740194
    :cond_6
    iget-object v0, p0, LX/B3K;->c:LX/B4O;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B3K;->b:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/B4O;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
