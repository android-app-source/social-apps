.class public LX/CC7;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/CCE;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pf;LX/25M;Lcom/facebook/feed/rows/core/props/FeedProps;LX/CCE;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;",
            "LX/1Pf;",
            "LX/25M;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/CCE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857211
    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1857212
    iput-object p6, p0, LX/CC7;->c:LX/CCE;

    .line 1857213
    iput-object p5, p0, LX/CC7;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1857214
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1857187
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1857189
    check-cast p2, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1857190
    iget-object v0, p0, LX/CC7;->c:LX/CCE;

    const/4 v1, 0x0

    .line 1857191
    new-instance v2, LX/CCD;

    invoke-direct {v2, v0}, LX/CCD;-><init>(LX/CCE;)V

    .line 1857192
    sget-object v3, LX/CCE;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CCC;

    .line 1857193
    if-nez v3, :cond_0

    .line 1857194
    new-instance v3, LX/CCC;

    invoke-direct {v3}, LX/CCC;-><init>()V

    .line 1857195
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CCC;->a$redex0(LX/CCC;LX/1De;IILX/CCD;)V

    .line 1857196
    move-object v2, v3

    .line 1857197
    move-object v1, v2

    .line 1857198
    move-object v0, v1

    .line 1857199
    iget-object v1, v0, LX/CCC;->a:LX/CCD;

    iput-object p2, v1, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1857200
    iget-object v1, v0, LX/CCC;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1857201
    move-object v0, v0

    .line 1857202
    iget-object v1, p0, LX/CC7;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1857203
    iget-object v2, v0, LX/CCC;->a:LX/CCD;

    iput-object v1, v2, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1857204
    iget-object v2, v0, LX/CCC;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1857205
    move-object v1, v0

    .line 1857206
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    .line 1857207
    iget-object v2, v1, LX/CCC;->a:LX/CCD;

    iput-object v0, v2, LX/CCD;->c:LX/1Pr;

    .line 1857208
    iget-object v2, v1, LX/CCC;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1857209
    move-object v0, v1

    .line 1857210
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1857188
    const/4 v0, 0x0

    return v0
.end method
