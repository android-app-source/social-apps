.class public final LX/Bpu;
.super LX/2jf;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V
    .locals 0

    .prologue
    .line 1823636
    iput-object p1, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-direct {p0}, LX/2jf;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 1823647
    if-nez p2, :cond_1

    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->K:LX/195;

    .line 1823648
    iget-boolean p1, v0, LX/195;->n:Z

    move v0, p1

    .line 1823649
    if-eqz v0, :cond_1

    .line 1823650
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->K:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 1823651
    :cond_0
    :goto_0
    return-void

    .line 1823652
    :cond_1
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->K:LX/195;

    .line 1823653
    iget-boolean p1, v0, LX/195;->n:Z

    move v0, p1

    .line 1823654
    if-nez v0, :cond_0

    .line 1823655
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->K:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 4

    .prologue
    .line 1823637
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v0, :cond_1

    .line 1823638
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 1823639
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->A:LX/23N;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Kt;->a(LX/0g8;III)V

    .line 1823640
    return-void

    .line 1823641
    :cond_1
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v0, :cond_0

    .line 1823642
    iget-object v0, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    iget-object v1, p0, LX/Bpu;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    .line 1823643
    iget-object v2, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    iget-object v3, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    invoke-interface {v3}, LX/1Qq;->getCount()I

    move-result v3

    sub-int v3, p2, v3

    invoke-interface {v2, v3}, LX/1Qr;->h_(I)I

    move-result v2

    move v1, v2

    .line 1823644
    add-int/lit8 v2, v1, 0x1

    iget v3, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->d:I

    add-int/lit8 v3, v3, -0x5

    if-lt v2, v3, :cond_2

    iget-object v2, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    invoke-virtual {v2}, LX/9g2;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1823645
    iget-object v2, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, LX/9g2;->a(I)V

    .line 1823646
    :cond_2
    goto :goto_0
.end method
