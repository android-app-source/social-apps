.class public LX/Bpd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1V2;

.field private final b:LX/1V8;

.field private final c:LX/1dp;


# direct methods
.method public constructor <init>(LX/1V2;LX/1V8;LX/1dp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1823389
    iput-object p1, p0, LX/Bpd;->a:LX/1V2;

    .line 1823390
    iput-object p2, p0, LX/Bpd;->b:LX/1V8;

    .line 1823391
    iput-object p3, p0, LX/Bpd;->c:LX/1dp;

    .line 1823392
    return-void
.end method

.method public static a(LX/0QB;)LX/Bpd;
    .locals 6

    .prologue
    .line 1823393
    const-class v1, LX/Bpd;

    monitor-enter v1

    .line 1823394
    :try_start_0
    sget-object v0, LX/Bpd;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823395
    sput-object v2, LX/Bpd;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823398
    new-instance p0, LX/Bpd;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v3

    check-cast v3, LX/1V2;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V8;

    invoke-static {v0}, LX/1do;->a(LX/0QB;)LX/1do;

    move-result-object v5

    check-cast v5, LX/1dp;

    invoke-direct {p0, v3, v4, v5}, LX/Bpd;-><init>(LX/1V2;LX/1V8;LX/1dp;)V

    .line 1823399
    move-object v0, p0

    .line 1823400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bpd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;LX/Bpe;LX/1Ps;)LX/1Dg;
    .locals 11
    .param p2    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/Bpe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Ps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;",
            "LX/Bpe;",
            "LX/1Ps;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1823404
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 1823405
    const/4 v0, 0x0

    iget-object v1, p3, LX/Bpe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Bpd;->a:LX/1V2;

    invoke-interface {p4}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    invoke-interface {p4}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    invoke-interface {p4}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    invoke-interface {p4}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p4}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v0

    .line 1823406
    const/4 v1, 0x0

    iget-object v2, p0, LX/Bpd;->b:LX/1V8;

    iget-object v3, p3, LX/Bpe;->b:LX/1Ua;

    move-object v4, p1

    move-object v5, v10

    invoke-static/range {v0 .. v5}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 1823407
    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x1

    iget-object v4, p0, LX/Bpd;->c:LX/1dp;

    iget-object v7, p3, LX/Bpe;->b:LX/1Ua;

    iget-object v8, p0, LX/Bpd;->b:LX/1V8;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-object v5, p1

    move-object v6, v10

    invoke-static/range {v0 .. v9}, LX/1X7;->a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1823408
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1823409
    invoke-static {p1, p2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, v10, Landroid/graphics/Rect;->left:I

    invoke-interface {v0, v1, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    iget v2, v10, Landroid/graphics/Rect;->top:I

    invoke-interface {v0, v1, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, v10, Landroid/graphics/Rect;->right:I

    invoke-interface {v0, v1, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x3

    iget v2, v10, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v0, v1, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
