.class public LX/BcF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/2dx;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/198;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1801701
    const/4 v0, 0x0

    sput-object v0, LX/BcF;->a:LX/2dx;

    return-void
.end method

.method public constructor <init>(LX/198;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1801702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801703
    iput-object p1, p0, LX/BcF;->b:LX/198;

    .line 1801704
    return-void
.end method

.method public static a(LX/0QB;)LX/BcF;
    .locals 4

    .prologue
    .line 1801705
    const-class v1, LX/BcF;

    monitor-enter v1

    .line 1801706
    :try_start_0
    sget-object v0, LX/BcF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1801707
    sput-object v2, LX/BcF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1801708
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1801709
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1801710
    new-instance p0, LX/BcF;

    invoke-static {v0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v3

    check-cast v3, LX/198;

    invoke-direct {p0, v3}, LX/BcF;-><init>(LX/198;)V

    .line 1801711
    move-object v0, p0

    .line 1801712
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1801713
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BcF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1801714
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1801715
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
