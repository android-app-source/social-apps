.class public LX/BTf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:LX/0Tn;


# instance fields
.field public a:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0hs;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1787831
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "upload_video_in_hd_toggle_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BTf;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787833
    iput-object p1, p0, LX/BTf;->a:LX/3kp;

    .line 1787834
    return-void
.end method

.method public static a(LX/0QB;)LX/BTf;
    .locals 1

    .prologue
    .line 1787835
    invoke-static {p0}, LX/BTf;->b(LX/0QB;)LX/BTf;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BTf;
    .locals 3

    .prologue
    .line 1787836
    new-instance v2, LX/BTf;

    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v0

    check-cast v0, LX/3kp;

    invoke-direct {v2, v0}, LX/BTf;-><init>(LX/3kp;)V

    .line 1787837
    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v0

    check-cast v0, LX/3kp;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    .line 1787838
    iput-object v0, v2, LX/BTf;->a:LX/3kp;

    iput-object v1, v2, LX/BTf;->b:LX/0wM;

    .line 1787839
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1787840
    iget-object v0, p0, LX/BTf;->a:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1787841
    :goto_0
    return-void

    .line 1787842
    :cond_0
    iget-object v0, p0, LX/BTf;->c:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1787843
    iget-object v0, p0, LX/BTf;->a:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1787844
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/BTf;->c:LX/0hs;

    .line 1787845
    iget-object v0, p0, LX/BTf;->c:LX/0hs;

    invoke-virtual {v0, p2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1787846
    iget-object v0, p0, LX/BTf;->b:LX/0wM;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a1f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1787847
    iget-object v1, p0, LX/BTf;->c:LX/0hs;

    invoke-virtual {v1, v0}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1787848
    iget-object v0, p0, LX/BTf;->c:LX/0hs;

    .line 1787849
    iput v3, v0, LX/0hs;->t:I

    .line 1787850
    iget-object v0, p0, LX/BTf;->c:LX/0hs;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1787851
    iget-object v0, p0, LX/BTf;->c:LX/0hs;

    new-instance v1, LX/BTe;

    invoke-direct {v1, p0, p2}, LX/BTe;-><init>(LX/BTf;Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 1787852
    iget-object v0, p0, LX/BTf;->a:LX/3kp;

    sget-object v1, LX/BTf;->d:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1787853
    return-void
.end method
