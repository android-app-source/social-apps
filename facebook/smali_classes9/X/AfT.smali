.class public LX/AfT;
.super LX/AeK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeK",
        "<",
        "LX/AfR;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/facebook/resources/ui/FbTextView;

.field private final m:Lcom/facebook/fbui/glyph/GlyphView;

.field private final n:Landroid/graphics/drawable/GradientDrawable;

.field private final o:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/0wM;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1699326
    invoke-direct {p0, p1}, LX/AeK;-><init>(Landroid/view/View;)V

    .line 1699327
    const v0, 0x7f0d1949

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AfT;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1699328
    const v0, 0x7f0d1948

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1699329
    iget-object v0, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    .line 1699330
    iput-object p2, p0, LX/AfT;->o:LX/0wM;

    .line 1699331
    return-void
.end method


# virtual methods
.method public final a(LX/AeO;LX/AeU;)V
    .locals 10

    .prologue
    .line 1699332
    check-cast p1, LX/AfR;

    const/4 v5, -0x1

    const v4, -0xcac97

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1699333
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1699334
    sget-object v1, LX/AfS;->a:[I

    iget-object v2, p1, LX/AfR;->a:LX/AfQ;

    invoke-virtual {v2}, LX/AfQ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1699335
    :goto_0
    return-void

    .line 1699336
    :pswitch_0
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1699337
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 1699338
    iget-object v1, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/AfT;->o:LX/0wM;

    const v3, 0x7f0208cd

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080c53

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/AfR;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1699340
    iget-object v1, p0, LX/AfT;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1699341
    :pswitch_1
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    const v2, 0x7f0a008d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1699342
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 1699343
    iget-object v1, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/AfT;->o:LX/0wM;

    const v3, 0x7f02082c

    invoke-virtual {v2, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699344
    new-instance v1, LX/AZN;

    iget v2, p1, LX/AfR;->d:I

    iget-object v3, p1, LX/AfR;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    invoke-direct {v1, v2, v3}, LX/AZN;-><init>(ILcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)V

    .line 1699345
    const v2, 0x7f080c54

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1699346
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/CharSequence;

    iget-object v4, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v4}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v3, v7

    const-string v2, "\n"

    aput-object v2, v3, v8

    const v2, 0x7f080c55

    new-array v4, v9, [Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "$"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1699347
    iget v6, v1, LX/AZN;->b:I

    move v6, v6

    .line 1699348
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "$"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1699349
    iget v6, v1, LX/AZN;->a:I

    move v1, v6

    .line 1699350
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699351
    iget-object v1, p0, LX/AfT;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1699352
    :pswitch_2
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1699353
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 1699354
    iget-object v1, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/AfT;->o:LX/0wM;

    const v3, 0x7f02082c

    invoke-virtual {v2, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699355
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const v2, 0x7f080c56

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "\n"

    aput-object v2, v1, v8

    const v2, 0x7f080c57

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699356
    iget-object v1, p0, LX/AfT;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1699357
    :pswitch_3
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1699358
    iget-object v1, p0, LX/AfT;->n:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    .line 1699359
    iget-object v1, p0, LX/AfT;->m:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/AfT;->o:LX/0wM;

    const v3, 0x7f020862

    invoke-virtual {v2, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699360
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const v2, 0x7f080c58

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p1, LX/AfR;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "\n"

    aput-object v2, v1, v8

    const v2, 0x7f080c59

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699361
    iget-object v1, p0, LX/AfT;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
