.class public final LX/Bmr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;)V
    .locals 0

    .prologue
    .line 1819225
    iput-object p1, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x76e960fb

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1819226
    check-cast p1, Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->toggle()V

    .line 1819227
    iget-object v1, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v2, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v2}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->isChecked()Z

    move-result v2

    .line 1819228
    iput-boolean v2, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->c:Z

    .line 1819229
    iget-object v1, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    .line 1819230
    iput-boolean v4, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b:Z

    .line 1819231
    iget-object v1, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v1, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    if-eqz v1, :cond_0

    .line 1819232
    iget-object v1, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v1, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    iget-object v2, p0, LX/Bmr;->a:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-boolean v2, v2, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->c:Z

    invoke-interface {v1, v2}, LX/Bj5;->a(Z)V

    .line 1819233
    :cond_0
    const v1, 0x71bf06bc

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
