.class public final LX/CDZ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CDa;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2pa;

.field public b:LX/3Ge;

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/3Fa;

.field public f:LX/CDg;

.field public g:LX/04D;

.field public h:LX/04H;

.field public i:LX/3It;

.field public j:LX/2oL;

.field public k:LX/093;

.field public l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public n:LX/1Pe;

.field public o:Z

.field public p:Z

.field public q:I

.field public r:LX/3Iv;

.field public final synthetic s:LX/CDa;


# direct methods
.method public constructor <init>(LX/CDa;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1859662
    iput-object p1, p0, LX/CDZ;->s:LX/CDa;

    .line 1859663
    move-object v0, p1

    .line 1859664
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1859665
    iput-boolean v1, p0, LX/CDZ;->c:Z

    .line 1859666
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CDZ;->o:Z

    .line 1859667
    iput-boolean v1, p0, LX/CDZ;->p:Z

    .line 1859668
    iput v1, p0, LX/CDZ;->q:I

    .line 1859669
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1859670
    const-string v0, "RichVideoPlayerComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/CDa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859671
    check-cast p1, LX/CDZ;

    .line 1859672
    iget-object v0, p1, LX/CDZ;->e:LX/3Fa;

    iput-object v0, p0, LX/CDZ;->e:LX/3Fa;

    .line 1859673
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1859674
    if-ne p0, p1, :cond_1

    .line 1859675
    :cond_0
    :goto_0
    return v0

    .line 1859676
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1859677
    goto :goto_0

    .line 1859678
    :cond_3
    check-cast p1, LX/CDZ;

    .line 1859679
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1859680
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1859681
    if-eq v2, v3, :cond_0

    .line 1859682
    iget-object v2, p0, LX/CDZ;->a:LX/2pa;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDZ;->a:LX/2pa;

    iget-object v3, p1, LX/CDZ;->a:LX/2pa;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1859683
    goto :goto_0

    .line 1859684
    :cond_5
    iget-object v2, p1, LX/CDZ;->a:LX/2pa;

    if-nez v2, :cond_4

    .line 1859685
    :cond_6
    iget-object v2, p0, LX/CDZ;->b:LX/3Ge;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CDZ;->b:LX/3Ge;

    iget-object v3, p1, LX/CDZ;->b:LX/3Ge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1859686
    goto :goto_0

    .line 1859687
    :cond_8
    iget-object v2, p1, LX/CDZ;->b:LX/3Ge;

    if-nez v2, :cond_7

    .line 1859688
    :cond_9
    iget-boolean v2, p0, LX/CDZ;->c:Z

    iget-boolean v3, p1, LX/CDZ;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1859689
    goto :goto_0

    .line 1859690
    :cond_a
    iget-object v2, p0, LX/CDZ;->d:LX/0Px;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/CDZ;->d:LX/0Px;

    iget-object v3, p1, LX/CDZ;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1859691
    goto :goto_0

    .line 1859692
    :cond_c
    iget-object v2, p1, LX/CDZ;->d:LX/0Px;

    if-nez v2, :cond_b

    .line 1859693
    :cond_d
    iget-object v2, p0, LX/CDZ;->f:LX/CDg;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/CDZ;->f:LX/CDg;

    iget-object v3, p1, LX/CDZ;->f:LX/CDg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1859694
    goto :goto_0

    .line 1859695
    :cond_f
    iget-object v2, p1, LX/CDZ;->f:LX/CDg;

    if-nez v2, :cond_e

    .line 1859696
    :cond_10
    iget-object v2, p0, LX/CDZ;->g:LX/04D;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/CDZ;->g:LX/04D;

    iget-object v3, p1, LX/CDZ;->g:LX/04D;

    invoke-virtual {v2, v3}, LX/04D;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1859697
    goto :goto_0

    .line 1859698
    :cond_12
    iget-object v2, p1, LX/CDZ;->g:LX/04D;

    if-nez v2, :cond_11

    .line 1859699
    :cond_13
    iget-object v2, p0, LX/CDZ;->h:LX/04H;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/CDZ;->h:LX/04H;

    iget-object v3, p1, LX/CDZ;->h:LX/04H;

    invoke-virtual {v2, v3}, LX/04H;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 1859700
    goto/16 :goto_0

    .line 1859701
    :cond_15
    iget-object v2, p1, LX/CDZ;->h:LX/04H;

    if-nez v2, :cond_14

    .line 1859702
    :cond_16
    iget-object v2, p0, LX/CDZ;->i:LX/3It;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/CDZ;->i:LX/3It;

    iget-object v3, p1, LX/CDZ;->i:LX/3It;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 1859703
    goto/16 :goto_0

    .line 1859704
    :cond_18
    iget-object v2, p1, LX/CDZ;->i:LX/3It;

    if-nez v2, :cond_17

    .line 1859705
    :cond_19
    iget-object v2, p0, LX/CDZ;->j:LX/2oL;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/CDZ;->j:LX/2oL;

    iget-object v3, p1, LX/CDZ;->j:LX/2oL;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 1859706
    goto/16 :goto_0

    .line 1859707
    :cond_1b
    iget-object v2, p1, LX/CDZ;->j:LX/2oL;

    if-nez v2, :cond_1a

    .line 1859708
    :cond_1c
    iget-object v2, p0, LX/CDZ;->k:LX/093;

    if-eqz v2, :cond_1e

    iget-object v2, p0, LX/CDZ;->k:LX/093;

    iget-object v3, p1, LX/CDZ;->k:LX/093;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 1859709
    goto/16 :goto_0

    .line 1859710
    :cond_1e
    iget-object v2, p1, LX/CDZ;->k:LX/093;

    if-nez v2, :cond_1d

    .line 1859711
    :cond_1f
    iget-object v2, p0, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    if-eqz v2, :cond_21

    iget-object v2, p0, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v3, p1, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 1859712
    goto/16 :goto_0

    .line 1859713
    :cond_21
    iget-object v2, p1, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    if-nez v2, :cond_20

    .line 1859714
    :cond_22
    iget-object v2, p0, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_24

    iget-object v2, p0, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 1859715
    goto/16 :goto_0

    .line 1859716
    :cond_24
    iget-object v2, p1, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_23

    .line 1859717
    :cond_25
    iget-object v2, p0, LX/CDZ;->n:LX/1Pe;

    if-eqz v2, :cond_27

    iget-object v2, p0, LX/CDZ;->n:LX/1Pe;

    iget-object v3, p1, LX/CDZ;->n:LX/1Pe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 1859718
    goto/16 :goto_0

    .line 1859719
    :cond_27
    iget-object v2, p1, LX/CDZ;->n:LX/1Pe;

    if-nez v2, :cond_26

    .line 1859720
    :cond_28
    iget-boolean v2, p0, LX/CDZ;->o:Z

    iget-boolean v3, p1, LX/CDZ;->o:Z

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 1859721
    goto/16 :goto_0

    .line 1859722
    :cond_29
    iget-boolean v2, p0, LX/CDZ;->p:Z

    iget-boolean v3, p1, LX/CDZ;->p:Z

    if-eq v2, v3, :cond_2a

    move v0, v1

    .line 1859723
    goto/16 :goto_0

    .line 1859724
    :cond_2a
    iget v2, p0, LX/CDZ;->q:I

    iget v3, p1, LX/CDZ;->q:I

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 1859725
    goto/16 :goto_0

    .line 1859726
    :cond_2b
    iget-object v2, p0, LX/CDZ;->r:LX/3Iv;

    if-eqz v2, :cond_2c

    iget-object v2, p0, LX/CDZ;->r:LX/3Iv;

    iget-object v3, p1, LX/CDZ;->r:LX/3Iv;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1859727
    goto/16 :goto_0

    .line 1859728
    :cond_2c
    iget-object v2, p1, LX/CDZ;->r:LX/3Iv;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1859729
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDZ;

    .line 1859730
    const/4 v1, 0x0

    iput-object v1, v0, LX/CDZ;->e:LX/3Fa;

    .line 1859731
    return-object v0
.end method
