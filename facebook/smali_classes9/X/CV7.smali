.class public final LX/CV7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "provider"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "provider"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "rawPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "rawPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1900554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1900555
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;
    .locals 14

    .prologue
    .line 1900556
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1900557
    iget-object v1, p0, LX/CV7;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1900558
    iget-object v2, p0, LX/CV7;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1900559
    iget-object v3, p0, LX/CV7;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1900560
    iget-object v4, p0, LX/CV7;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1900561
    iget-object v5, p0, LX/CV7;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1900562
    iget-object v6, p0, LX/CV7;->f:LX/0Px;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1900563
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, LX/CV7;->g:LX/15i;

    iget v9, p0, LX/CV7;->h:I

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v7, 0x15d473dd

    invoke-static {v8, v9, v7}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v7

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1900564
    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_1
    iget-object v9, p0, LX/CV7;->i:LX/15i;

    iget v10, p0, LX/CV7;->j:I

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v8, 0x26f30b65

    invoke-static {v9, v10, v8}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v8

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1900565
    iget-object v9, p0, LX/CV7;->k:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1900566
    iget-object v10, p0, LX/CV7;->l:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1900567
    iget-object v11, p0, LX/CV7;->m:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1900568
    iget-object v12, p0, LX/CV7;->n:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1900569
    const/16 v13, 0xc

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1900570
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1900571
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1900572
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1900573
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1900574
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1900575
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1900576
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1900577
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1900578
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1900579
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1900580
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1900581
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1900582
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1900583
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1900584
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1900585
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1900586
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1900587
    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    invoke-direct {v1, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;-><init>(LX/15i;)V

    .line 1900588
    return-object v1

    .line 1900589
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1900590
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
