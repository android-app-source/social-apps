.class public LX/Acr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Acr;


# instance fields
.field public final a:LX/0if;

.field public b:LX/Acq;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1693209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1693210
    iput-object p1, p0, LX/Acr;->a:LX/0if;

    .line 1693211
    return-void
.end method

.method public static a(LX/0QB;)LX/Acr;
    .locals 4

    .prologue
    .line 1693212
    sget-object v0, LX/Acr;->c:LX/Acr;

    if-nez v0, :cond_1

    .line 1693213
    const-class v1, LX/Acr;

    monitor-enter v1

    .line 1693214
    :try_start_0
    sget-object v0, LX/Acr;->c:LX/Acr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1693215
    if-eqz v2, :cond_0

    .line 1693216
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1693217
    new-instance p0, LX/Acr;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/Acr;-><init>(LX/0if;)V

    .line 1693218
    move-object v0, p0

    .line 1693219
    sput-object v0, LX/Acr;->c:LX/Acr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1693220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1693221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1693222
    :cond_1
    sget-object v0, LX/Acr;->c:LX/Acr;

    return-object v0

    .line 1693223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1693224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 1693225
    iget-object v0, p0, LX/Acr;->a:LX/0if;

    sget-object v1, LX/0ig;->D:LX/0ih;

    const-string v2, "click_donate_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1693226
    return-void
.end method
