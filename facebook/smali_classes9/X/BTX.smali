.class public LX/BTX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BTl;

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(LX/BTl;)V
    .locals 1
    .param p1    # LX/BTl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1787766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787767
    iput-object p1, p0, LX/BTX;->a:LX/BTl;

    .line 1787768
    iput v0, p0, LX/BTX;->b:I

    .line 1787769
    iput v0, p0, LX/BTX;->c:I

    .line 1787770
    const/4 v0, 0x0

    iput v0, p0, LX/BTX;->d:I

    .line 1787771
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 1787772
    iget v0, p0, LX/BTX;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1787773
    iget v0, p0, LX/BTX;->c:I

    mul-int/2addr v0, p1

    iget v1, p0, LX/BTX;->b:I

    div-int/2addr v0, v1

    iget v1, p0, LX/BTX;->d:I

    add-int/2addr v0, v1

    return v0

    .line 1787774
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IZ)I
    .locals 2

    .prologue
    .line 1787775
    iget v0, p0, LX/BTX;->c:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1787776
    if-eqz p2, :cond_0

    .line 1787777
    iget-object v0, p0, LX/BTX;->a:LX/BTl;

    invoke-virtual {v0, p1}, LX/BTl;->a(I)I

    move-result p1

    .line 1787778
    :cond_0
    iget v0, p0, LX/BTX;->d:I

    sub-int v0, p1, v0

    iget v1, p0, LX/BTX;->b:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/BTX;->c:I

    div-int/2addr v0, v1

    return v0

    .line 1787779
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZII)I
    .locals 2

    .prologue
    .line 1787780
    invoke-virtual {p0, p3, p1}, LX/BTX;->a(IZ)I

    move-result v0

    invoke-virtual {p0, p2, p1}, LX/BTX;->a(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
