.class public final LX/AaM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AaP;


# direct methods
.method public constructor <init>(LX/AaP;)V
    .locals 0

    .prologue
    .line 1688413
    iput-object p1, p0, LX/AaM;->a:LX/AaP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6245d7f7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688414
    iget-object v1, p0, LX/AaM;->a:LX/AaP;

    .line 1688415
    sget-object v3, LX/6yO;->SIMPLE:LX/6yO;

    const-string v4, "TIPJAR"

    sget-object p0, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {p0}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object p0

    invoke-virtual {p0}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object p0

    invoke-static {v4, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v4

    const-string p0, "tip_jar"

    .line 1688416
    iput-object p0, v4, LX/6xw;->d:Ljava/lang/String;

    .line 1688417
    move-object v4, v4

    .line 1688418
    invoke-virtual {v4}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v4

    sget-object p0, LX/6xg;->MOR_P2P_TRANSFER:LX/6xg;

    invoke-static {v3, v4, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v3

    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v4

    invoke-virtual {v1}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f080cd7

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 1688419
    iput-object p0, v4, LX/6yR;->a:Ljava/lang/String;

    .line 1688420
    move-object v4, v4

    .line 1688421
    invoke-virtual {v4}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v4

    .line 1688422
    iput-object v4, v3, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1688423
    move-object v3, v3

    .line 1688424
    invoke-virtual {v3}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    .line 1688425
    invoke-virtual {v1}, LX/AaP;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v3

    .line 1688426
    iget-object v4, v1, LX/AaP;->j:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/AaP;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v4, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1688427
    const v1, -0x2c1329e2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
