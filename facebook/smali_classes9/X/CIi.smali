.class public LX/CIi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/CIb;

.field public c:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/CIb;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874666
    iput-object p1, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1874667
    iput-object p2, p0, LX/CIi;->b:LX/CIb;

    .line 1874668
    iput-object p3, p0, LX/CIi;->c:LX/0ad;

    .line 1874669
    return-void
.end method

.method public static a(LX/0QB;)LX/CIi;
    .locals 6

    .prologue
    .line 1874670
    const-class v1, LX/CIi;

    monitor-enter v1

    .line 1874671
    :try_start_0
    sget-object v0, LX/CIi;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1874672
    sput-object v2, LX/CIi;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1874673
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874674
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1874675
    new-instance p0, LX/CIi;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v4

    check-cast v4, LX/CIb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/CIi;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/CIb;LX/0ad;)V

    .line 1874676
    move-object v0, p0

    .line 1874677
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1874678
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CIi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874679
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1874680
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0dN;LX/0Tn;)V
    .locals 2

    .prologue
    .line 1874681
    iget-object v0, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    if-eqz v0, :cond_0

    .line 1874682
    iget-object v1, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/CIi;->b:LX/CIb;

    .line 1874683
    iget-object p0, v0, LX/CIb;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1874684
    invoke-virtual {p2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 1874685
    :cond_0
    return-void
.end method

.method public final a(LX/0Tn;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1874686
    const/4 v0, 0x1

    .line 1874687
    iget-object v1, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    if-eqz v1, :cond_1

    .line 1874688
    iget-object v1, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/CIi;->b:LX/CIb;

    .line 1874689
    iget-object v3, v0, LX/CIb;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1874690
    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1874691
    iget-object v0, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v0, p0, LX/CIi;->b:LX/CIb;

    .line 1874692
    iget-object v3, v0, LX/CIb;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1874693
    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1874694
    :cond_0
    iget-object v1, p0, LX/CIi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/CIi;->b:LX/CIb;

    .line 1874695
    iget-object v3, v0, LX/CIb;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1874696
    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1874697
    :cond_1
    return v0
.end method
