.class public LX/Caz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cay;


# instance fields
.field public a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:LX/CbI;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final c:LX/Cb1;

.field public final d:LX/9hh;

.field public final e:LX/CbH;

.field public final f:Landroid/widget/FrameLayout;

.field public final g:LX/CbC;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1Ck;

.field public final j:LX/1L1;

.field public final k:Ljava/lang/String;

.field public final l:LX/7iV;

.field public final m:LX/7j7;

.field private final n:LX/CbM;

.field private final o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

.field private final p:Landroid/graphics/Matrix;

.field public q:LX/5kD;

.field public r:LX/Cau;

.field public s:LX/Cb5;

.field public t:Z

.field public u:I

.field public v:Ljava/lang/String;

.field public w:LX/8Jo;

.field public x:LX/0ad;

.field public final y:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;LX/7iV;LX/0ad;LX/Cb1;LX/CbH;LX/9hh;LX/CbJ;LX/CbC;LX/0Ot;LX/CbM;LX/1Ck;LX/1L1;LX/8Jo;LX/7j7;)V
    .locals 3
    .param p1    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/7iV;",
            "LX/0ad;",
            "LX/Cb1;",
            "LX/CbH;",
            "LX/9hh;",
            "LX/CbJ;",
            "LX/CbC;",
            "LX/0Ot",
            "<",
            "LX/745;",
            ">;",
            "LX/CbM;",
            "LX/1Ck;",
            "LX/1L1;",
            "LX/8Jo;",
            "LX/7j7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1918854
    iput-object p6, p0, LX/Caz;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1918855
    iput-object p7, p0, LX/Caz;->l:LX/7iV;

    .line 1918856
    iput-object p8, p0, LX/Caz;->x:LX/0ad;

    .line 1918857
    iput-object p9, p0, LX/Caz;->c:LX/Cb1;

    .line 1918858
    iput-object p11, p0, LX/Caz;->d:LX/9hh;

    .line 1918859
    iput-object p10, p0, LX/Caz;->e:LX/CbH;

    .line 1918860
    iput-object p1, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    .line 1918861
    invoke-direct {p0}, LX/Caz;->j()LX/Cau;

    move-result-object v1

    iput-object v1, p0, LX/Caz;->r:LX/Cau;

    .line 1918862
    new-instance v1, Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v2, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    .line 1918863
    iget-object v1, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-direct {p0}, LX/Caz;->i()LX/8JH;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setFaceboxClickedListener(LX/8JH;)V

    .line 1918864
    iget-object v1, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Caz;->r:LX/Cau;

    invoke-virtual {p12, v1, v2}, LX/CbJ;->a(Landroid/content/Context;LX/Cau;)LX/CbI;

    move-result-object v1

    iput-object v1, p0, LX/Caz;->b:LX/CbI;

    .line 1918865
    iget-object v1, p0, LX/Caz;->e:LX/CbH;

    invoke-virtual {v1, p0}, LX/CbH;->a(LX/Cay;)V

    .line 1918866
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Caz;->g:LX/CbC;

    .line 1918867
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Caz;->h:LX/0Ot;

    .line 1918868
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Caz;->i:LX/1Ck;

    .line 1918869
    iput-object p5, p0, LX/Caz;->k:Ljava/lang/String;

    .line 1918870
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Caz;->j:LX/1L1;

    .line 1918871
    iget-object v1, p0, LX/Caz;->e:LX/CbH;

    invoke-virtual {v1}, LX/CbH;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1918872
    iget-object v1, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1918873
    iget-object v1, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1918874
    :cond_0
    iput-object p4, p0, LX/Caz;->v:Ljava/lang/String;

    .line 1918875
    iput p3, p0, LX/Caz;->u:I

    .line 1918876
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Caz;->w:LX/8Jo;

    .line 1918877
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Caz;->n:LX/CbM;

    .line 1918878
    iput-object p2, p0, LX/Caz;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    .line 1918879
    iget-object v1, p0, LX/Caz;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-direct {p0}, LX/Caz;->k()LX/7US;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(LX/7US;)V

    .line 1918880
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, LX/Caz;->p:Landroid/graphics/Matrix;

    .line 1918881
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Caz;->m:LX/7j7;

    .line 1918882
    return-void
.end method

.method public static a$redex0(LX/Caz;Landroid/graphics/PointF;)V
    .locals 9

    .prologue
    .line 1918849
    invoke-virtual {p0}, LX/Caz;->a()LX/CbL;

    move-result-object v1

    .line 1918850
    invoke-virtual {v1, p1}, LX/CbL;->a(Landroid/graphics/PointF;)V

    .line 1918851
    iget-object v0, p0, LX/Caz;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, LX/Caz;->h:LX/0Ot;

    iget-object v3, p0, LX/Caz;->d:LX/9hh;

    iget-object v4, p0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, LX/Caz;->l(LX/Caz;)LX/Caw;

    move-result-object v5

    invoke-static {p0}, LX/Caz;->p(LX/Caz;)LX/8JW;

    move-result-object v6

    iget-object v7, p0, LX/Caz;->w:LX/8Jo;

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, LX/Cb5;->a(Landroid/content/Context;LX/CbL;LX/0Ot;LX/9hh;Ljava/lang/String;LX/Caw;LX/8JW;LX/8Jo;Z)LX/Cb5;

    move-result-object v0

    iput-object v0, p0, LX/Caz;->s:LX/Cb5;

    .line 1918852
    return-void
.end method

.method public static b$redex0(LX/Caz;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1918839
    invoke-virtual {p0}, LX/Caz;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918840
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Caz;->t:Z

    .line 1918841
    :cond_0
    :goto_0
    return-void

    .line 1918842
    :cond_1
    iget-object v0, p0, LX/Caz;->s:LX/Cb5;

    if-nez v0, :cond_0

    .line 1918843
    iget-object v0, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setVisibility(I)V

    .line 1918844
    iget-object v0, p0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0, v1}, LX/CbI;->setVisibility(I)V

    .line 1918845
    if-eqz p1, :cond_2

    .line 1918846
    iget-object v0, p0, LX/Caz;->b:LX/CbI;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/tagging/MediaTaggingController$5;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/tagging/MediaTaggingController$5;-><init>(LX/Caz;)V

    invoke-static {v0, v1}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1918847
    goto :goto_0

    .line 1918848
    :cond_2
    invoke-virtual {p0}, LX/Caz;->d()V

    goto :goto_0
.end method

.method public static e(LX/Caz;LX/5kD;)Z
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1918818
    iget-object v0, p0, LX/Caz;->q:LX/5kD;

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/Caz;->f(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918819
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 1918820
    :goto_1
    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1918821
    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918822
    iget-object v4, p0, LX/Caz;->q:LX/5kD;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1918823
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1918824
    invoke-static {v4}, LX/Caz;->f(LX/5kD;)Z

    move-result v6

    if-nez v6, :cond_9

    move-object v6, v8

    .line 1918825
    :cond_2
    :goto_3
    move-object v4, v6

    .line 1918826
    if-nez v4, :cond_3

    move v2, v3

    .line 1918827
    goto :goto_0

    .line 1918828
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v5

    if-nez v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 1918829
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v5

    if-nez v5, :cond_6

    :cond_5
    move v2, v3

    .line 1918830
    goto :goto_0

    .line 1918831
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-eq v0, v4, :cond_7

    move v2, v3

    .line 1918832
    goto :goto_0

    .line 1918833
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_8
    move v6, v7

    .line 1918834
    goto :goto_2

    .line 1918835
    :cond_9
    invoke-interface {v4}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    :goto_4
    if-ge v7, v10, :cond_a

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918836
    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1918837
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_a
    move-object v6, v8

    .line 1918838
    goto :goto_3
.end method

.method public static f(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1918817
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()LX/8JH;
    .locals 1

    .prologue
    .line 1918816
    new-instance v0, LX/Car;

    invoke-direct {v0, p0}, LX/Car;-><init>(LX/Caz;)V

    return-object v0
.end method

.method private j()LX/Cau;
    .locals 1

    .prologue
    .line 1918815
    new-instance v0, LX/Cau;

    invoke-direct {v0, p0}, LX/Cau;-><init>(LX/Caz;)V

    return-object v0
.end method

.method private k()LX/7US;
    .locals 1

    .prologue
    .line 1918814
    new-instance v0, LX/Cav;

    invoke-direct {v0, p0}, LX/Cav;-><init>(LX/Caz;)V

    return-object v0
.end method

.method public static l(LX/Caz;)LX/Caw;
    .locals 1

    .prologue
    .line 1918813
    new-instance v0, LX/Caw;

    invoke-direct {v0, p0}, LX/Caw;-><init>(LX/Caz;)V

    return-object v0
.end method

.method public static m$redex0(LX/Caz;)V
    .locals 12

    .prologue
    .line 1918757
    iget-object v0, p0, LX/Caz;->b:LX/CbI;

    iget-object v1, p0, LX/Caz;->c:LX/Cb1;

    .line 1918758
    iget-object v2, v1, LX/Cb1;->c:Ljava/util/LinkedList;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1918759
    iget-object v2, p0, LX/Caz;->c:LX/Cb1;

    invoke-virtual {v2}, LX/Cb1;->a()LX/0Rh;

    move-result-object v2

    invoke-virtual {v2}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    iget-object v3, p0, LX/Caz;->c:LX/Cb1;

    .line 1918760
    iget-object v4, v3, LX/Cb1;->e:LX/0Ri;

    invoke-static {v4}, LX/0Rh;->a(Ljava/util/Map;)LX/0Rh;

    move-result-object v4

    move-object v3, v4

    .line 1918761
    const/4 v10, -0x2

    .line 1918762
    invoke-virtual {v0}, LX/CbI;->removeAllViews()V

    .line 1918763
    iget-object v4, v0, LX/CbI;->e:LX/0Ri;

    invoke-interface {v4}, LX/0Ri;->clear()V

    .line 1918764
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1918765
    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1918766
    iget-object v4, v0, LX/CbI;->b:LX/0ad;

    sget-short v6, LX/1xL;->d:S

    const/4 v7, 0x0

    invoke-interface {v4, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v6

    .line 1918767
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918768
    invoke-static {v0, v4, v6}, LX/CbI;->a(LX/CbI;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;Z)LX/CbA;

    move-result-object v8

    .line 1918769
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v9, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1918770
    invoke-virtual {v0, v8, v9}, LX/CbI;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1918771
    iget-object v9, v0, LX/CbI;->e:LX/0Ri;

    invoke-interface {v9, v8, v4}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1918772
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 1918773
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918774
    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->c()LX/1f8;

    move-result-object v5

    invoke-interface {v5}, LX/1f8;->a()D

    move-result-wide v10

    double-to-float v5, v10

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->c()LX/1f8;

    move-result-object v9

    invoke-interface {v9}, LX/1f8;->b()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-direct {v8, v5, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1918775
    invoke-virtual {v3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918776
    new-instance v9, LX/8Jk;

    if-eqz v5, :cond_1

    invoke-static {v5}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v5

    :goto_2
    invoke-direct {v9, v8, v5}, LX/8Jk;-><init>(Landroid/graphics/PointF;Landroid/graphics/RectF;)V

    .line 1918777
    iget-object v5, v0, LX/CbI;->e:LX/0Ri;

    invoke-interface {v5}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v5

    invoke-interface {v5, v4}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v6, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1918778
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    .line 1918779
    :cond_2
    iget-object v4, v0, LX/CbI;->c:LX/8Jm;

    invoke-virtual {v4, v2}, LX/8Jm;->a(Ljava/util/Collection;)V

    .line 1918780
    iget-object v4, v0, LX/CbI;->c:LX/8Jm;

    invoke-virtual {v4, v6}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1918781
    iget-object v0, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v1, p0, LX/Caz;->c:LX/Cb1;

    invoke-virtual {v1}, LX/Cb1;->a()LX/0Rh;

    move-result-object v1

    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setFaceBoxes(Ljava/util/Collection;)V

    .line 1918782
    return-void
.end method

.method public static o(LX/Caz;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1918810
    iget-object v0, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setVisibility(I)V

    .line 1918811
    iget-object v0, p0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0, v1}, LX/CbI;->setVisibility(I)V

    .line 1918812
    return-void
.end method

.method public static p(LX/Caz;)LX/8JW;
    .locals 1

    .prologue
    .line 1918809
    new-instance v0, LX/Cax;

    invoke-direct {v0, p0}, LX/Cax;-><init>(LX/Caz;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/CbL;
    .locals 5

    .prologue
    .line 1918804
    iget-object v0, p0, LX/Caz;->n:LX/CbM;

    iget-object v1, p0, LX/Caz;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    .line 1918805
    iget v2, p0, LX/Caz;->u:I

    move v2, v2

    .line 1918806
    iget-object v3, p0, LX/Caz;->v:Ljava/lang/String;

    move-object v3, v3

    .line 1918807
    iget-object v4, p0, LX/Caz;->k:Ljava/lang/String;

    move-object v4, v4

    .line 1918808
    invoke-virtual {v0, v1, v2, v3, v4}, LX/CbM;->a(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;)LX/CbL;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1918799
    if-eqz p1, :cond_0

    .line 1918800
    invoke-static {p0}, LX/Caz;->m$redex0(LX/Caz;)V

    .line 1918801
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/Caz;->b$redex0(LX/Caz;Z)V

    .line 1918802
    :goto_0
    return-void

    .line 1918803
    :cond_0
    invoke-static {p0}, LX/Caz;->o(LX/Caz;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1918786
    iget-object v0, p0, LX/Caz;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v1

    .line 1918787
    iget-object v0, p0, LX/Caz;->p:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, LX/5ua;->a(Landroid/graphics/Matrix;)V

    .line 1918788
    iget-object v0, p0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v2, p0, LX/Caz;->p:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setDraweeMatrix(Landroid/graphics/Matrix;)V

    .line 1918789
    iget-object v2, p0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v1}, LX/5ua;->m()F

    move-result v0

    .line 1918790
    iget v3, v1, LX/5ua;->i:F

    move v3, v3

    .line 1918791
    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 1918792
    :goto_0
    iget-object v3, v2, LX/CbI;->c:LX/8Jm;

    .line 1918793
    new-instance p0, Landroid/graphics/Matrix;

    invoke-direct {p0}, Landroid/graphics/Matrix;-><init>()V

    .line 1918794
    invoke-virtual {v1, p0}, LX/5ua;->a(Landroid/graphics/Matrix;)V

    .line 1918795
    new-instance v2, LX/8Jl;

    invoke-direct {v2, p0}, LX/8Jl;-><init>(Landroid/graphics/Matrix;)V

    move-object p0, v2

    .line 1918796
    invoke-static {v3, p0, v0}, LX/8Jm;->a(LX/8Jm;LX/8Jl;Z)V

    .line 1918797
    return-void

    .line 1918798
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1918783
    iget-object v0, p0, LX/Caz;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    .line 1918784
    iget-boolean p0, v0, LX/5ua;->e:Z

    move v0, p0

    .line 1918785
    return v0
.end method
