.class public LX/B9E;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/B6N;


# instance fields
.field public a:LX/B8r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B81;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/B6U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/B6k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/FbScrollView;

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/B8s;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/widget/LinearLayout;

.field public i:LX/B6T;

.field public j:LX/B7F;

.field private k:Lcom/facebook/common/locale/Country;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1751407
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1751408
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/B9E;

    invoke-static {p1}, LX/B8r;->a(LX/0QB;)LX/B8r;

    move-result-object v3

    check-cast v3, LX/B8r;

    invoke-static {p1}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v4

    check-cast v4, LX/B6l;

    invoke-static {p1}, LX/B81;->a(LX/0QB;)LX/B81;

    move-result-object v5

    check-cast v5, LX/B81;

    const-class v6, LX/B6U;

    invoke-interface {p1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/B6U;

    const-class v0, LX/B6k;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B6k;

    iput-object v3, v2, LX/B9E;->a:LX/B8r;

    iput-object v4, v2, LX/B9E;->b:LX/B6l;

    iput-object v5, v2, LX/B9E;->c:LX/B81;

    iput-object v6, v2, LX/B9E;->d:LX/B6U;

    iput-object p1, v2, LX/B9E;->e:LX/B6k;

    .line 1751409
    const v0, 0x7f0309d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1751410
    return-void
.end method

.method private static a(LX/B7m;LX/B8s;)V
    .locals 3

    .prologue
    .line 1751405
    invoke-interface {p0}, LX/B7m;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, LX/B7m;->getInputCustomToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, LX/B8s;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)V

    .line 1751406
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 1751397
    iget-object v0, p0, LX/B9E;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1751398
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B9E;->g:Ljava/util/HashMap;

    .line 1751399
    :cond_0
    invoke-direct {p0}, LX/B9E;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751400
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v0

    .line 1751401
    iget-object v4, p0, LX/B9E;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1751402
    iget-object v4, p0, LX/B9E;->g:Ljava/util/HashMap;

    new-instance v5, LX/B8s;

    invoke-direct {v5}, LX/B8s;-><init>()V

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1751403
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1751404
    :cond_2
    return-void
.end method

.method private getLeadGenFieldInputs()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B7m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1751387
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1751388
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1751389
    iget-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1751390
    instance-of v0, v1, LX/B7m;

    if-eqz v0, :cond_1

    .line 1751391
    instance-of v0, v1, LX/B8R;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1751392
    check-cast v0, LX/B8R;

    invoke-virtual {v0}, LX/B8R;->getCountry()Lcom/facebook/common/locale/Country;

    move-result-object v0

    iput-object v0, p0, LX/B9E;->k:Lcom/facebook/common/locale/Country;

    .line 1751393
    :cond_0
    check-cast v1, LX/B7m;

    .line 1751394
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1751395
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1751396
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1751382
    invoke-direct {p0}, LX/B9E;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751383
    invoke-interface {v0}, LX/B7m;->a()V

    .line 1751384
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1751385
    :cond_0
    iget-object v0, p0, LX/B9E;->i:LX/B6T;

    invoke-virtual {v0}, LX/B6T;->a()V

    .line 1751386
    return-void
.end method

.method public final a(LX/B7B;ILX/B7F;ILX/B7w;)V
    .locals 8

    .prologue
    .line 1751338
    invoke-virtual {p3}, LX/B7F;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1751339
    :cond_0
    :goto_0
    return-void

    .line 1751340
    :cond_1
    check-cast p1, LX/B7N;

    .line 1751341
    iput-object p3, p0, LX/B9E;->j:LX/B7F;

    .line 1751342
    const v0, 0x7f0d18f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d18f6

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0d18f7

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, LX/B6U;->a(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)LX/B6T;

    move-result-object v0

    iput-object v0, p0, LX/B9E;->i:LX/B6T;

    .line 1751343
    const v0, 0x7f0d18f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1751344
    const v1, 0x7f0d0083

    const-string v2, "cta_lead_gen_visit_privacy_page_click"

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 1751345
    iget-object v0, p0, LX/B9E;->i:LX/B6T;

    new-instance v1, LX/B9C;

    invoke-direct {v1, p0, p4}, LX/B9C;-><init>(LX/B9E;I)V

    invoke-virtual {v0, v1}, LX/B6T;->a(Landroid/view/View$OnClickListener;)V

    .line 1751346
    iget-object v0, p0, LX/B9E;->i:LX/B6T;

    const/4 p4, 0x0

    .line 1751347
    new-instance v1, Landroid/text/SpannableString;

    .line 1751348
    iget-object v2, p1, LX/B7N;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1751349
    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1751350
    new-instance v2, LX/B9D;

    invoke-direct {v2, p0, p1}, LX/B9D;-><init>(LX/B9E;LX/B7N;)V

    .line 1751351
    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1751352
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1751353
    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1751354
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/B9E;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1751355
    move-object v1, v1

    .line 1751356
    invoke-virtual {v0, p1, v1}, LX/B6T;->a(LX/B7N;Landroid/text/SpannableString;)V

    .line 1751357
    iget-object v0, p0, LX/B9E;->i:LX/B6T;

    iget-object v1, p0, LX/B9E;->j:LX/B7F;

    sget-object v2, LX/B77;->NO_ERROR:LX/B77;

    invoke-virtual {v0, v1, v2, p1}, LX/B6T;->a(LX/B7F;LX/B77;LX/B7N;)V

    .line 1751358
    const v0, 0x7f0d18f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    .line 1751359
    iget-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1751360
    iget-object v0, p1, LX/B7N;->a:LX/0Px;

    move-object v4, v0

    .line 1751361
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1751362
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 1751363
    iget-object v1, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/B9E;->getContext()Landroid/content/Context;

    move-result-object v2

    const v6, 0x7f0309b9

    const/4 v7, 0x0

    invoke-static {v2, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1751364
    :cond_2
    invoke-static {p0, v0}, LX/B81;->a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Landroid/view/View;

    move-result-object v2

    move-object v1, v2

    .line 1751365
    check-cast v1, LX/B7m;

    iget-object v6, p0, LX/B9E;->j:LX/B7F;

    invoke-interface {v1, v0, v6, p2}, LX/B7m;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V

    move-object v0, v2

    .line 1751366
    check-cast v0, LX/B7m;

    invoke-interface {v0, p5}, LX/B7m;->setOnDataChangeListener(LX/B7w;)V

    .line 1751367
    iget-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1751368
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1751369
    :cond_3
    const v0, 0x7f0d04f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;

    .line 1751370
    const v1, 0x7f0d18c5

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;

    .line 1751371
    iget-object v2, p1, LX/B7N;->g:LX/B7K;

    move-object v2, v2

    .line 1751372
    iget-object v3, p0, LX/B9E;->j:LX/B7F;

    invoke-virtual {v3}, LX/B7F;->u()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->a(LX/B7K;Z)V

    .line 1751373
    iget-object v0, p0, LX/B9E;->j:LX/B7F;

    invoke-virtual {v0}, LX/B7F;->u()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1751374
    iget-object v0, p1, LX/B7N;->g:LX/B7K;

    move-object v0, v0

    .line 1751375
    invoke-virtual {v1, v0}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->setUpView(LX/B7K;)V

    .line 1751376
    :cond_4
    iget-object v0, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/B9E;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751377
    instance-of v1, v0, LX/B8K;

    if-eqz v1, :cond_5

    .line 1751378
    check-cast v0, LX/B8K;

    invoke-virtual {v0}, LX/B8K;->d()V

    .line 1751379
    :cond_5
    const v0, 0x7f0d18c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, LX/B9E;->f:Lcom/facebook/widget/FbScrollView;

    .line 1751380
    iget-object v0, p0, LX/B9E;->j:LX/B7F;

    invoke-virtual {v0}, LX/B7F;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1751381
    iget-object v0, p0, LX/B9E;->f:Lcom/facebook/widget/FbScrollView;

    new-instance v1, Lcom/facebook/leadgen/view/LeadGenUserInfoFormView$1;

    invoke-direct {v1, p0}, Lcom/facebook/leadgen/view/LeadGenUserInfoFormView$1;-><init>(LX/B9E;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbScrollView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 5

    .prologue
    .line 1751332
    invoke-direct {p0}, LX/B9E;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751333
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1751334
    if-eqz v4, :cond_0

    .line 1751335
    invoke-interface {v0, v4}, LX/B7m;->setInputValue(Ljava/lang/String;)V

    .line 1751336
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1751337
    :cond_1
    return-void
.end method

.method public final b()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B8s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1751325
    invoke-direct {p0}, LX/B9E;->f()V

    .line 1751326
    invoke-direct {p0}, LX/B9E;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751327
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v1

    .line 1751328
    iget-object v5, p0, LX/B9E;->g:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1751329
    iget-object v5, p0, LX/B9E;->g:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B8s;

    invoke-static {v0, v1}, LX/B9E;->a(LX/B7m;LX/B8s;)V

    .line 1751330
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1751331
    :cond_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, LX/B9E;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1751301
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentScrollView()Lcom/facebook/widget/FbScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1751324
    iget-object v0, p0, LX/B9E;->f:Lcom/facebook/widget/FbScrollView;

    return-object v0
.end method

.method public final l_(I)LX/B77;
    .locals 9

    .prologue
    .line 1751302
    invoke-direct {p0}, LX/B9E;->f()V

    .line 1751303
    sget-object v3, LX/B77;->NO_ERROR:LX/B77;

    .line 1751304
    const/4 v2, 0x0

    .line 1751305
    invoke-direct {p0}, LX/B9E;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1751306
    iget-object v1, p0, LX/B9E;->g:Ljava/util/HashMap;

    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B8s;

    .line 1751307
    if-nez v1, :cond_1

    .line 1751308
    sget-object v3, LX/B77;->UNKNOWN_ERROR:LX/B77;

    .line 1751309
    :cond_0
    :goto_1
    return-object v3

    .line 1751310
    :cond_1
    invoke-static {v0, v1}, LX/B9E;->a(LX/B7m;LX/B8s;)V

    .line 1751311
    iget-object v7, p0, LX/B9E;->a:LX/B8r;

    invoke-virtual {v7, v1}, LX/B8r;->a(LX/B8s;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, LX/B9E;->k:Lcom/facebook/common/locale/Country;

    invoke-static {v1, v7}, LX/B8r;->a(LX/B8s;Lcom/facebook/common/locale/Country;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1751312
    :cond_2
    if-nez v2, :cond_3

    move-object v2, v0

    .line 1751313
    :cond_3
    iget-object v3, v1, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v3, v3

    .line 1751314
    invoke-static {v3}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v3

    .line 1751315
    iget-object v7, p0, LX/B9E;->j:LX/B7F;

    invoke-virtual {v7, v3}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v7

    .line 1751316
    invoke-interface {v0, v7}, LX/B7m;->a(Ljava/lang/String;)V

    .line 1751317
    iget-object v0, p0, LX/B9E;->b:LX/B6l;

    const-string v7, "cta_lead_gen_user_info_validation_error"

    .line 1751318
    iget-object v8, v1, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v1, v8

    .line 1751319
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v1, p1}, LX/B6l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v3

    .line 1751320
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v3, v0

    goto :goto_0

    .line 1751321
    :cond_4
    invoke-interface {v0}, LX/B7m;->c()V

    move-object v0, v3

    goto :goto_2

    .line 1751322
    :cond_5
    if-eqz v2, :cond_0

    .line 1751323
    invoke-interface {v2}, LX/B7m;->b()V

    goto :goto_1
.end method
