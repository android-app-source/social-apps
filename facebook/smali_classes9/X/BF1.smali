.class public LX/BF1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AaV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AaV",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

.field private b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765332
    return-void
.end method

.method public static a(LX/0QB;)LX/BF1;
    .locals 1

    .prologue
    .line 1765333
    invoke-static {}, LX/BF1;->e()LX/BF1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765341
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a()V

    .line 1765342
    invoke-direct {p0, p1}, LX/BF1;->b(LX/0Px;)V

    .line 1765343
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1765344
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    iget-object v1, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-static {v1}, LX/BF3;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(Landroid/view/View;)V

    .line 1765345
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-direct {p0}, LX/BF1;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(Landroid/view/View;)V

    .line 1765346
    :cond_0
    return-void
.end method

.method private b(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765334
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    .line 1765335
    iget-object v3, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-static {v0, v3}, LX/BF3;->a(Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1765336
    if-eqz v3, :cond_0

    .line 1765337
    new-instance v4, LX/BEy;

    invoke-direct {v4, p0, v0}, LX/BEy;-><init>(LX/BF1;Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1765338
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v0, v3}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(Landroid/view/View;)V

    .line 1765339
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1765340
    :cond_1
    return-void
.end method

.method private d()Landroid/view/View;
    .locals 4

    .prologue
    .line 1765323
    new-instance v0, LX/70u;

    iget-object v1, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/70u;-><init>(Landroid/content/Context;)V

    .line 1765324
    new-instance v1, LX/BEz;

    invoke-direct {v1, p0}, LX/BEz;-><init>(LX/BF1;)V

    .line 1765325
    iput-object v1, v0, LX/70u;->b:LX/6qh;

    .line 1765326
    new-instance v1, LX/71G;

    iget-object v2, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c75

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/71F;->LEARN_MORE_AND_TERMS:LX/71F;

    invoke-direct {v1, v2, v3}, LX/71G;-><init>(Ljava/lang/String;LX/71F;)V

    .line 1765327
    invoke-virtual {v0, v1}, LX/70u;->a(LX/71G;)V

    .line 1765328
    return-object v0
.end method

.method private static e()LX/BF1;
    .locals 1

    .prologue
    .line 1765329
    new-instance v0, LX/BF1;

    invoke-direct {v0}, LX/BF1;-><init>()V

    .line 1765330
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1765309
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a()V

    .line 1765310
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->b()V

    .line 1765311
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V
    .locals 1

    .prologue
    .line 1765312
    iput-object p1, p0, LX/BF1;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1765313
    iget-object v0, p0, LX/BF1;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-virtual {v0, p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(LX/AaV;)V

    .line 1765314
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;)V
    .locals 4

    .prologue
    .line 1765315
    iput-object p1, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    .line 1765316
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    new-instance v1, LX/716;

    iget-object v2, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08271d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(LX/716;)V

    .line 1765317
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1765318
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/BF1;->a(LX/0Px;)V

    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1765319
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->c()V

    .line 1765320
    return-void
.end method

.method public final iK_()V
    .locals 2

    .prologue
    .line 1765321
    iget-object v0, p0, LX/BF1;->b:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    new-instance v1, LX/BF0;

    invoke-direct {v1, p0}, LX/BF0;-><init>(LX/BF1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(LX/1DI;)V

    .line 1765322
    return-void
.end method
