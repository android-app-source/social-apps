.class public LX/BUx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/BV2;

.field public c:Landroid/graphics/SurfaceTexture;

.field public d:Landroid/view/Surface;

.field private e:Ljavax/microedition/khronos/egl/EGL10;

.field private f:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private g:Ljavax/microedition/khronos/egl/EGLContext;

.field private h:Ljavax/microedition/khronos/egl/EGLSurface;

.field public final i:Landroid/graphics/RectF;

.field public final j:I

.field public final k:I

.field public final l:LX/1FZ;

.field public final m:LX/60x;

.field private final n:Ljava/lang/Object;

.field private o:Z

.field public p:Ljava/nio/ByteBuffer;

.field public q:LX/5Pc;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1789752
    const-class v0, LX/BUx;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BUx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/60x;Landroid/graphics/RectF;LX/7Sv;Ljava/util/List;LX/1FZ;LX/5Pc;)V
    .locals 2
    .param p1    # LX/60x;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/RectF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/7Sv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/60x;",
            "Landroid/graphics/RectF;",
            "LX/7Sv;",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;",
            "LX/1FZ;",
            "LX/5Pc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1789816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1789817
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1789818
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1789819
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1789820
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/BUx;->n:Ljava/lang/Object;

    .line 1789821
    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1789822
    iput-object p4, p0, LX/BUx;->r:Ljava/util/List;

    .line 1789823
    :goto_0
    iput-object p1, p0, LX/BUx;->m:LX/60x;

    .line 1789824
    iput-object p5, p0, LX/BUx;->l:LX/1FZ;

    .line 1789825
    iput-object p6, p0, LX/BUx;->q:LX/5Pc;

    .line 1789826
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    .line 1789827
    iput-object p2, p0, LX/BUx;->i:Landroid/graphics/RectF;

    .line 1789828
    iget-object v0, p0, LX/BUx;->m:LX/60x;

    iget v0, v0, LX/60x;->d:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_2

    .line 1789829
    iget-object v0, p0, LX/BUx;->m:LX/60x;

    iget v0, v0, LX/60x;->b:I

    int-to-float v0, v0

    iget-object v1, p0, LX/BUx;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/BUx;->j:I

    .line 1789830
    iget-object v0, p0, LX/BUx;->m:LX/60x;

    iget v0, v0, LX/60x;->c:I

    int-to-float v0, v0

    iget-object v1, p0, LX/BUx;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/BUx;->k:I

    .line 1789831
    :goto_1
    invoke-direct {p0}, LX/BUx;->f()V

    .line 1789832
    invoke-virtual {p0}, LX/BUx;->c()V

    .line 1789833
    new-instance v0, LX/BV2;

    iget-object v1, p0, LX/BUx;->r:Ljava/util/List;

    iget-object p1, p0, LX/BUx;->m:LX/60x;

    iget p1, p1, LX/60x;->d:I

    iget-object p2, p0, LX/BUx;->i:Landroid/graphics/RectF;

    invoke-direct {v0, v1, p1, p3, p2}, LX/BV2;-><init>(Ljava/util/List;ILX/7Sv;Landroid/graphics/RectF;)V

    iput-object v0, p0, LX/BUx;->b:LX/BV2;

    .line 1789834
    iget-object v0, p0, LX/BUx;->b:LX/BV2;

    const p3, 0x812f

    const/16 p2, 0x2601

    .line 1789835
    new-instance v1, LX/5Pe;

    invoke-direct {v1}, LX/5Pe;-><init>()V

    const p1, 0x8d65

    .line 1789836
    iput p1, v1, LX/5Pe;->a:I

    .line 1789837
    move-object v1, v1

    .line 1789838
    const/16 p1, 0x2801

    invoke-virtual {v1, p1, p2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 p1, 0x2800

    invoke-virtual {v1, p1, p2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 p1, 0x2802

    invoke-virtual {v1, p1, p3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    const/16 p1, 0x2803

    invoke-virtual {v1, p1, p3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v1

    invoke-virtual {v1}, LX/5Pe;->a()LX/5Pf;

    move-result-object v1

    iput-object v1, v0, LX/BV2;->f:LX/5Pf;

    .line 1789839
    iget-object v0, p0, LX/BUx;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61B;

    .line 1789840
    iget-object p1, p0, LX/BUx;->q:LX/5Pc;

    invoke-interface {v0, p1}, LX/61B;->a(LX/5Pc;)V

    .line 1789841
    iget p1, p0, LX/BUx;->j:I

    iget p2, p0, LX/BUx;->k:I

    invoke-interface {v0, p1, p2}, LX/61B;->a(II)V

    goto :goto_2

    .line 1789842
    :cond_0
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/BUx;->b:LX/BV2;

    .line 1789843
    iget-object p1, v1, LX/BV2;->f:LX/5Pf;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1789844
    iget-object p1, v1, LX/BV2;->f:LX/5Pf;

    move-object v1, p1

    .line 1789845
    iget v1, v1, LX/5Pf;->b:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    .line 1789846
    iget-object v0, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1789847
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/BUx;->d:Landroid/view/Surface;

    .line 1789848
    iget v0, p0, LX/BUx;->j:I

    iget v1, p0, LX/BUx;->k:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    .line 1789849
    iget-object v0, p0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1789850
    return-void

    .line 1789851
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1789852
    new-instance v1, LX/7SP;

    invoke-direct {v1}, LX/7SP;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1789853
    iput-object v0, p0, LX/BUx;->r:Ljava/util/List;

    goto/16 :goto_0

    .line 1789854
    :cond_2
    iget-object v0, p0, LX/BUx;->m:LX/60x;

    iget v0, v0, LX/60x;->c:I

    int-to-float v0, v0

    iget-object v1, p0, LX/BUx;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/BUx;->j:I

    .line 1789855
    iget-object v0, p0, LX/BUx;->m:LX/60x;

    iget v0, v0, LX/60x;->b:I

    int-to-float v0, v0

    iget-object v1, p0, LX/BUx;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/BUx;->k:I

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1789813
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1789814
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": EGL error: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1789815
    :cond_0
    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 1789787
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1789788
    iget-object v0, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_0

    .line 1789789
    iput-object v3, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1789790
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to get EGL14 display"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789791
    :cond_0
    new-array v0, v8, [I

    .line 1789792
    iget-object v1, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1789793
    iput-object v3, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1789794
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789795
    :cond_1
    const/16 v0, 0xd

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    .line 1789796
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 1789797
    new-array v5, v4, [I

    .line 1789798
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1789799
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to find RGB888+recordable ES2 EGL config"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789800
    :cond_2
    new-array v0, v9, [I

    fill-array-data v0, :array_1

    .line 1789801
    iget-object v1, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    aget-object v5, v3, v7

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v5, v6, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1789802
    const-string v0, "eglCreateContext"

    invoke-direct {p0, v0}, LX/BUx;->a(Ljava/lang/String;)V

    .line 1789803
    iget-object v0, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v0, :cond_3

    .line 1789804
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "null context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789805
    :cond_3
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/16 v1, 0x3057

    aput v1, v0, v7

    iget v1, p0, LX/BUx;->j:I

    aput v1, v0, v4

    const/16 v1, 0x3056

    aput v1, v0, v8

    iget v1, p0, LX/BUx;->k:I

    aput v1, v0, v9

    const/4 v1, 0x4

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 1789806
    iget-object v1, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    aget-object v3, v3, v7

    invoke-interface {v1, v2, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1789807
    const-string v0, "eglCreatePbufferSurface"

    invoke-direct {p0, v0}, LX/BUx;->a(Ljava/lang/String;)V

    .line 1789808
    iget-object v0, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    if-nez v0, :cond_4

    .line 1789809
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "surface was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789810
    :cond_4
    return-void

    .line 1789811
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3040
        0x4
        0x3033
        0x1
        0x3038
    .end array-data

    .line 1789812
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1789856
    iget-object v0, p0, LX/BUx;->b:LX/BV2;

    if-eqz v0, :cond_0

    .line 1789857
    iget-object v0, p0, LX/BUx;->b:LX/BV2;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1789858
    new-array v1, v4, [I

    .line 1789859
    iget-object v2, v0, LX/BV2;->f:LX/5Pf;

    iget v2, v2, LX/5Pf;->b:I

    aput v2, v1, v3

    .line 1789860
    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1789861
    const/4 v1, 0x0

    iput-object v1, v0, LX/BV2;->f:LX/5Pf;

    .line 1789862
    iget-object v1, v0, LX/BV2;->e:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1789863
    iget-object v1, v0, LX/BV2;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/61B;

    .line 1789864
    invoke-interface {v1}, LX/61B;->b()V

    goto :goto_0

    .line 1789865
    :cond_0
    iget-object v0, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eq v0, v1, :cond_1

    .line 1789866
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 1789867
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1789868
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1789869
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 1789870
    :cond_1
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1789871
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1789872
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1789873
    iget-object v0, p0, LX/BUx;->d:Landroid/view/Surface;

    if-eqz v0, :cond_2

    .line 1789874
    iget-object v0, p0, LX/BUx;->d:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1789875
    :cond_2
    iput-object v5, p0, LX/BUx;->b:LX/BV2;

    .line 1789876
    iput-object v5, p0, LX/BUx;->d:Landroid/view/Surface;

    .line 1789877
    iput-object v5, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    .line 1789878
    return-void
.end method

.method public final a(J)V
    .locals 11

    .prologue
    .line 1789776
    iget-object v0, p0, LX/BUx;->b:LX/BV2;

    iget-object v1, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    const/4 v9, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1789777
    iget-object v3, v0, LX/BV2;->e:Ljava/util/List;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1789778
    iget-object v3, v0, LX/BV2;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1789779
    invoke-static {v4, v5, v4, v5}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1789780
    const/16 v3, 0x4000

    invoke-static {v3}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1789781
    iget-object v3, v0, LX/BV2;->b:[F

    invoke-virtual {v1, v3}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1789782
    iget-object v3, v0, LX/BV2;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/61B;

    .line 1789783
    iget-object v4, v0, LX/BV2;->b:[F

    iget-object v5, v0, LX/BV2;->c:[F

    iget-object v6, v0, LX/BV2;->d:[F

    move-wide v7, p1

    invoke-interface/range {v3 .. v8}, LX/61B;->a([F[F[FJ)V

    goto :goto_1

    :cond_0
    move v3, v9

    .line 1789784
    goto :goto_0

    .line 1789785
    :cond_1
    const v3, 0x8d65

    invoke-static {v3, v9}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1789786
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1789773
    iget-object v0, p0, LX/BUx;->e:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, LX/BUx;->f:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, LX/BUx;->h:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, LX/BUx;->g:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1789774
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789775
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 1789760
    iget-object v1, p0, LX/BUx;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 1789761
    :cond_0
    :try_start_0
    iget-boolean v0, p0, LX/BUx;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1789762
    :try_start_1
    iget-object v0, p0, LX/BUx;->n:Ljava/lang/Object;

    const-wide/16 v2, 0x9c4

    const v4, 0x5d3e9053

    invoke-static {v0, v2, v3, v4}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 1789763
    iget-boolean v0, p0, LX/BUx;->o:Z

    if-nez v0, :cond_0

    .line 1789764
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "frame wait timed out"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1789765
    :catch_0
    move-exception v0

    .line 1789766
    :try_start_2
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 1789767
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1789768
    :cond_1
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, LX/BUx;->o:Z

    .line 1789769
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1789770
    const-string v0, "before updateTexImage"

    invoke-static {v0}, LX/BV2;->a(Ljava/lang/String;)V

    .line 1789771
    iget-object v0, p0, LX/BUx;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1789772
    return-void
.end method

.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 1789753
    iget-object v1, p0, LX/BUx;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 1789754
    :try_start_0
    iget-boolean v0, p0, LX/BUx;->o:Z

    if-eqz v0, :cond_0

    .line 1789755
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "mFrameAvailable already set, frame could be dropped"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1789756
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1789757
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/BUx;->o:Z

    .line 1789758
    iget-object v0, p0, LX/BUx;->n:Ljava/lang/Object;

    const v2, -0x6f2d7383

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1789759
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
