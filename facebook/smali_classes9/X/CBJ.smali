.class public final LX/CBJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

.field public final b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

.field public final c:Lcom/facebook/graphql/model/GraphQLImage;

.field public final d:Lcom/facebook/graphql/model/GraphQLImage;

.field public final e:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final i:Landroid/view/View$OnClickListener;

.field public final j:Z

.field public final k:LX/CBo;

.field public final l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLQuickPromotion;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Landroid/view/View$OnClickListener;ZLX/CBo;Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;)V
    .locals 0

    .prologue
    .line 1856237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856238
    iput-object p1, p0, LX/CBJ;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    .line 1856239
    iput-object p2, p0, LX/CBJ;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 1856240
    iput-object p3, p0, LX/CBJ;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1856241
    iput-object p4, p0, LX/CBJ;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1856242
    iput-object p5, p0, LX/CBJ;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1856243
    iput-object p6, p0, LX/CBJ;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1856244
    iput-object p7, p0, LX/CBJ;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1856245
    iput-object p8, p0, LX/CBJ;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1856246
    iput-object p9, p0, LX/CBJ;->i:Landroid/view/View$OnClickListener;

    .line 1856247
    iput-boolean p10, p0, LX/CBJ;->j:Z

    .line 1856248
    iput-object p11, p0, LX/CBJ;->k:LX/CBo;

    .line 1856249
    iput-object p12, p0, LX/CBJ;->l:Lcom/facebook/graphql/model/GraphQLQuickPromotionCounter;

    .line 1856250
    return-void
.end method
