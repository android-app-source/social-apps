.class public final LX/BJf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public final synthetic a:LX/BKm;

.field public final synthetic b:LX/BJi;


# direct methods
.method public constructor <init>(LX/BJi;LX/BKm;)V
    .locals 0

    .prologue
    .line 1772274
    iput-object p1, p0, LX/BJf;->b:LX/BJi;

    iput-object p2, p0, LX/BJf;->a:LX/BKm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1772275
    iget-object v0, p0, LX/BJf;->b:LX/BJi;

    iget-object v0, v0, LX/BJi;->a:LX/BJ7;

    iget-object v1, p0, LX/BJf;->a:LX/BKm;

    iget-object v1, v1, LX/BKm;->a:Ljava/lang/String;

    .line 1772276
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/BJ6;->DRAFT_DIALOG_BACK_CLICKED:LX/BJ6;

    iget-object p1, p1, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "composer_session_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v0, p0}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1772277
    return-void
.end method
