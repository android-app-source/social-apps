.class public final LX/AYd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)V
    .locals 0

    .prologue
    .line 1685858
    iput-object p1, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, 0x1cfc4aa6

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1685859
    iget-object v0, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, LX/0ew;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1685860
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "broadcast_comment_dialog"

    invoke-virtual {v2, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1685861
    :cond_0
    const v0, -0x4ce3aa3b

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1685862
    :goto_0
    return-void

    .line 1685863
    :cond_1
    iget-object v2, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    if-nez v2, :cond_2

    .line 1685864
    iget-object v2, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    new-instance v3, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-direct {v3}, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;-><init>()V

    .line 1685865
    iput-object v3, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    .line 1685866
    iget-object v2, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    new-instance v3, LX/AYc;

    invoke-direct {v3, p0}, LX/AYc;-><init>(LX/AYd;)V

    .line 1685867
    iput-object v3, v2, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->u:LX/AYb;

    .line 1685868
    :cond_2
    iget-object v2, p0, LX/AYd;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const-string v3, "broadcast_comment_dialog"

    invoke-virtual {v2, v0, v3, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1685869
    const v0, 0x744c7a44

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
