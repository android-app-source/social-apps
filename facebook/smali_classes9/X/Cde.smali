.class public LX/Cde;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1923011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923012
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1923015
    check-cast p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;

    .line 1923016
    new-instance v0, LX/4cQ;

    const-string v1, "file"

    iget-object v2, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->b:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1923017
    new-instance v3, Ljava/io/File;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1923018
    new-instance v4, LX/4ct;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 1923019
    invoke-direct {v0, v1, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1923020
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1923021
    iget-object v2, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1923022
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "source"

    iget-object v4, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1923023
    :cond_0
    iget-object v2, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1923024
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "entry_point"

    iget-object v4, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1923025
    :cond_1
    iget-object v2, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1923026
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "end_point"

    iget-object v4, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1923027
    :cond_2
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "page-profile-suggestion-upload"

    .line 1923028
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1923029
    move-object v2, v2

    .line 1923030
    const-string v3, "POST"

    .line 1923031
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1923032
    move-object v2, v2

    .line 1923033
    const-string v3, "%d/suggestions"

    iget-wide v4, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1923034
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 1923035
    move-object v2, v2

    .line 1923036
    iput-object v1, v2, LX/14O;->g:Ljava/util/List;

    .line 1923037
    move-object v1, v2

    .line 1923038
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1923039
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1923040
    move-object v1, v1

    .line 1923041
    const/4 v2, 0x1

    new-array v2, v2, [LX/4cQ;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1923042
    iput-object v0, v1, LX/14O;->l:Ljava/util/List;

    .line 1923043
    move-object v0, v1

    .line 1923044
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1923013
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    .line 1923014
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
