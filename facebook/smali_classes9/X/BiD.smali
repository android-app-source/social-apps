.class public final LX/BiD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V
    .locals 0

    .prologue
    .line 1810002
    iput-object p1, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;B)V
    .locals 0

    .prologue
    .line 1810016
    invoke-direct {p0, p1}, LX/BiD;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const v0, -0x7c0a561c

    invoke-static {v7, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    move-object v0, p1

    .line 1810003
    check-cast v0, LX/BiF;

    .line 1810004
    invoke-virtual {v0, v2}, LX/BiF;->a(Z)V

    .line 1810005
    new-instance v2, LX/5Ni;

    invoke-direct {v2}, LX/5Ni;-><init>()V

    move-object v2, v2

    .line 1810006
    const-string v3, "node_id"

    iget-object v4, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v4, v4, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v4}, LX/Bi8;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "context_items_source"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "context_items_source_id"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->j:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "context_items_surface"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1810007
    iget-object v6, v5, LX/Bi8;->f:LX/Bi3;

    move-object v5, v6

    .line 1810008
    iget-object v5, v5, LX/Bi3;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "context_item_icon_size"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->c:Landroid/content/res/Resources;

    const v6, 0x7f0b0d14

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "after_cursor"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1810009
    iget-object v6, v5, LX/Bi8;->i:Ljava/lang/String;

    move-object v5, v6

    .line 1810010
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "first_count"

    iget-object v5, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v5, v5, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->c:Landroid/content/res/Resources;

    const v6, 0x7f0c002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1810011
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1810012
    iget-object v3, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v3, v3, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1810013
    iget-object v3, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v3, v3, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->d:LX/1Ck;

    sget-object v4, LX/BiE;->LOAD_MORE_CONTEXT_ITEMS:LX/BiE;

    new-instance v5, LX/BiB;

    invoke-direct {v5, p0, v2}, LX/BiB;-><init>(LX/BiD;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v2, LX/BiC;

    invoke-direct {v2, p0, v0}, LX/BiC;-><init>(LX/BiD;LX/BiF;)V

    invoke-virtual {v3, v4, v5, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1810014
    iget-object v0, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v2, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a:LX/Bi1;

    iget-object v0, p0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v0, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v0}, LX/Bi8;->c()Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0d00df

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/Bi1;->b(Ljava/lang/String;I)V

    .line 1810015
    const v0, 0x1515bab2

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
