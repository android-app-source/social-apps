.class public final LX/CVc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1904226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1904227
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904228
    :goto_0
    return v1

    .line 1904229
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904230
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1904231
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1904232
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1904233
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1904234
    const-string v6, "secondary_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1904235
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1904236
    :cond_2
    const-string v6, "subtitles"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1904237
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1904238
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 1904239
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1904240
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v5

    .line 1904241
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1904242
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1904243
    goto :goto_1

    .line 1904244
    :cond_4
    const-string v6, "thumbnail"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1904245
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1904246
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_f

    .line 1904247
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904248
    :goto_3
    move v2, v5

    .line 1904249
    goto :goto_1

    .line 1904250
    :cond_5
    const-string v6, "title_richtext"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1904251
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1904252
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1904253
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1904254
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1904255
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1904256
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1904257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 1904258
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1904259
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1904260
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1904261
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_8

    if-eqz v11, :cond_8

    .line 1904262
    const-string v12, "height"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1904263
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_4

    .line 1904264
    :cond_9
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1904265
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 1904266
    :cond_a
    const-string v12, "width"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1904267
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v8, v2

    move v2, v6

    goto :goto_4

    .line 1904268
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1904269
    :cond_c
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1904270
    if-eqz v7, :cond_d

    .line 1904271
    invoke-virtual {p1, v5, v10, v5}, LX/186;->a(III)V

    .line 1904272
    :cond_d
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1904273
    if-eqz v2, :cond_e

    .line 1904274
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v5}, LX/186;->a(III)V

    .line 1904275
    :cond_e
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_f
    move v2, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1904276
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1904277
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904278
    if-eqz v0, :cond_0

    .line 1904279
    const-string v1, "secondary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904280
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904281
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904282
    if-eqz v0, :cond_2

    .line 1904283
    const-string v1, "subtitles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904284
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1904285
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1904286
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904287
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1904288
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1904289
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904290
    if-eqz v0, :cond_6

    .line 1904291
    const-string v1, "thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904292
    const/4 v3, 0x0

    .line 1904293
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1904294
    invoke-virtual {p0, v0, v3, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1904295
    if-eqz v1, :cond_3

    .line 1904296
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904297
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1904298
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1904299
    if-eqz v1, :cond_4

    .line 1904300
    const-string v2, "url"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904301
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1904302
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 1904303
    if-eqz v1, :cond_5

    .line 1904304
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904305
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1904306
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1904307
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904308
    if-eqz v0, :cond_7

    .line 1904309
    const-string v1, "title_richtext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904310
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904311
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1904312
    return-void
.end method
