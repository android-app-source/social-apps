.class public final LX/AvQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

.field public b:LX/AvG;

.field public c:LX/AvS;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;LX/AvG;LX/AvS;)V
    .locals 0

    .prologue
    .line 1724066
    iput-object p1, p0, LX/AvQ;->a:Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724067
    iput-object p2, p0, LX/AvQ;->b:LX/AvG;

    .line 1724068
    iput-object p3, p0, LX/AvQ;->c:LX/AvS;

    .line 1724069
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1724070
    iget-object v0, p0, LX/AvQ;->a:Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->d:LX/AvM;

    iget-object v1, p0, LX/AvQ;->b:LX/AvG;

    .line 1724071
    iget-object v2, v1, LX/AvG;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1724072
    new-instance v2, LX/AvP;

    invoke-direct {v2, p0}, LX/AvP;-><init>(LX/AvQ;)V

    .line 1724073
    :try_start_0
    invoke-static {p1}, LX/0aP;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 1724074
    iget-object p2, v0, LX/AvM;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {p2, v1, p0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    .line 1724075
    iget-object p2, v0, LX/AvM;->b:LX/1Ck;

    sget-object p3, LX/AvK;->WRITE_TO_CACHE:LX/AvK;

    new-instance p4, LX/AvH;

    invoke-direct {p4, v0, v1, v2}, LX/AvH;-><init>(LX/AvM;Ljava/lang/String;LX/AvL;)V

    invoke-virtual {p2, p3, p0, p4}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1724076
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1724077
    :catch_0
    move-exception p0

    .line 1724078
    invoke-interface {v2, p0}, LX/AvL;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
