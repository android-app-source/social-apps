.class public final LX/B6Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/B6S;

.field public final synthetic b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;LX/B6S;)V
    .locals 0

    .prologue
    .line 1746802
    iput-object p1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iput-object p2, p0, LX/B6Y;->a:LX/B6S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, -0x249bf609

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746803
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->k()LX/B77;

    move-result-object v1

    .line 1746804
    iget-object v2, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    .line 1746805
    iget v3, v2, LX/B6u;->h:I

    move v2, v3

    .line 1746806
    invoke-virtual {v1}, LX/B77;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1746807
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1746808
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a()V

    .line 1746809
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b:LX/B7W;

    new-instance v3, LX/B7e;

    invoke-direct {v3, v4}, LX/B7e;-><init>(Z)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1746810
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "click_submit_button"

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1746811
    iget-object v1, p0, LX/B6Y;->a:LX/B6S;

    iget-object v3, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v3, v3, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    iget-object v4, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v4, v4, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v4}, LX/B6u;->e()LX/0P1;

    move-result-object v4

    iget-object v5, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v5, v5, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v5}, LX/B6u;->g()LX/0P1;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5, v2}, LX/B6S;->a(LX/B7F;LX/0P1;LX/0P1;I)V

    .line 1746812
    :cond_0
    :goto_0
    const v1, 0x216e5fe7

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1746813
    :cond_1
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->a()Z

    .line 1746814
    iget-object v1, p0, LX/B6Y;->b:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "cta_lead_gen_next_button_click"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v4, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
