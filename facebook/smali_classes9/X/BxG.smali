.class public final LX/BxG;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BxI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:LX/1Pm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/BxI;


# direct methods
.method public constructor <init>(LX/BxI;)V
    .locals 1

    .prologue
    .line 1835184
    iput-object p1, p0, LX/BxG;->c:LX/BxI;

    .line 1835185
    move-object v0, p1

    .line 1835186
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1835187
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1835188
    const-string v0, "ArticleShareButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1835189
    if-ne p0, p1, :cond_1

    .line 1835190
    :cond_0
    :goto_0
    return v0

    .line 1835191
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1835192
    goto :goto_0

    .line 1835193
    :cond_3
    check-cast p1, LX/BxG;

    .line 1835194
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1835195
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1835196
    if-eq v2, v3, :cond_0

    .line 1835197
    iget-object v2, p0, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1835198
    goto :goto_0

    .line 1835199
    :cond_5
    iget-object v2, p1, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_4

    .line 1835200
    :cond_6
    iget-object v2, p0, LX/BxG;->b:LX/1Pm;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/BxG;->b:LX/1Pm;

    iget-object v3, p1, LX/BxG;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1835201
    goto :goto_0

    .line 1835202
    :cond_7
    iget-object v2, p1, LX/BxG;->b:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
