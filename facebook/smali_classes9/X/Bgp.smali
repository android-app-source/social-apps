.class public final LX/Bgp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Bgq;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Bgq;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1808506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808507
    iput-object p1, p0, LX/Bgp;->a:LX/0QB;

    .line 1808508
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1808509
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/Bgp;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1808510
    packed-switch p2, :pswitch_data_0

    .line 1808511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1808512
    :pswitch_0
    new-instance v0, LX/Bgr;

    invoke-direct {v0}, LX/Bgr;-><init>()V

    .line 1808513
    move-object v0, v0

    .line 1808514
    move-object v0, v0

    .line 1808515
    :goto_0
    return-object v0

    .line 1808516
    :pswitch_1
    new-instance v2, LX/Bgv;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p1}, LX/Bez;->b(LX/0QB;)LX/Bez;

    move-result-object v4

    check-cast v4, LX/Bez;

    invoke-static {p1}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v5

    check-cast v5, Ljava/util/Locale;

    invoke-static {p1}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v6

    check-cast v6, LX/BfJ;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct/range {v2 .. v7}, LX/Bgv;-><init>(LX/03V;LX/Bez;Ljava/util/Locale;LX/BfJ;LX/0Uh;)V

    .line 1808517
    move-object v0, v2

    .line 1808518
    goto :goto_0

    .line 1808519
    :pswitch_2
    new-instance v3, LX/Bgz;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p1}, LX/Bg8;->b(LX/0QB;)LX/Bg8;

    move-result-object v1

    check-cast v1, LX/Bg8;

    invoke-static {p1}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v2

    check-cast v2, LX/BfJ;

    invoke-direct {v3, v0, v1, v2}, LX/Bgz;-><init>(LX/03V;LX/Bg8;LX/BfJ;)V

    .line 1808520
    move-object v0, v3

    .line 1808521
    goto :goto_0

    .line 1808522
    :pswitch_3
    invoke-static {p1}, LX/Bh5;->b(LX/0QB;)LX/Bh5;

    move-result-object v0

    goto :goto_0

    .line 1808523
    :pswitch_4
    new-instance v2, LX/Bh9;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p1}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v1

    check-cast v1, LX/BfJ;

    invoke-direct {v2, v0, v1}, LX/Bh9;-><init>(LX/03V;LX/BfJ;)V

    .line 1808524
    move-object v0, v2

    .line 1808525
    goto :goto_0

    .line 1808526
    :pswitch_5
    new-instance v2, LX/BhD;

    const/16 v3, 0x147f

    invoke-static {p1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1480

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    .line 1808527
    new-instance v8, LX/BgQ;

    const/16 v7, 0x147f

    invoke-static {p1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p1}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v7

    check-cast v7, LX/BfJ;

    invoke-direct {v8, v9, v7}, LX/BgQ;-><init>(LX/0Or;LX/BfJ;)V

    .line 1808528
    move-object v7, v8

    .line 1808529
    check-cast v7, LX/BgQ;

    invoke-direct/range {v2 .. v7}, LX/BhD;-><init>(LX/0Or;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/BgQ;)V

    .line 1808530
    move-object v0, v2

    .line 1808531
    goto/16 :goto_0

    .line 1808532
    :pswitch_6
    new-instance v0, LX/BhE;

    invoke-direct {v0}, LX/BhE;-><init>()V

    .line 1808533
    move-object v0, v0

    .line 1808534
    move-object v0, v0

    .line 1808535
    goto/16 :goto_0

    .line 1808536
    :pswitch_7
    new-instance v3, LX/BhJ;

    invoke-static {p1}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v0

    check-cast v0, LX/BfJ;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/Bg8;->b(LX/0QB;)LX/Bg8;

    move-result-object v2

    check-cast v2, LX/Bg8;

    invoke-direct {v3, v0, v1, v2}, LX/BhJ;-><init>(LX/BfJ;LX/03V;LX/Bg8;)V

    .line 1808537
    move-object v0, v3

    .line 1808538
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1808539
    const/16 v0, 0x8

    return v0
.end method
