.class public LX/BDq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763865
    const-class v0, LX/BDq;

    sput-object v0, LX/BDq;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1763866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763867
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1763868
    check-cast p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;

    .line 1763869
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763870
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1763871
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1763872
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1763873
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "alert_types_on"

    .line 1763874
    iget-object v2, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1763875
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763876
    :cond_0
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1763877
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1763878
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "alert_types_off"

    .line 1763879
    iget-object v2, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1763880
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763881
    :cond_1
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1763882
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1763883
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "type"

    .line 1763884
    iget-object v2, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1763885
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763886
    :cond_2
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1763887
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1763888
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "setting_id"

    .line 1763889
    iget-object v2, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1763890
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763891
    :cond_3
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1763892
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1763893
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "setting_value"

    .line 1763894
    iget-object v2, p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1763895
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763896
    :cond_4
    new-instance v0, LX/14N;

    const-string v1, "graphNotificationsSettings"

    const-string v2, "POST"

    const-string v3, "me/notificationssettings"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1763897
    check-cast p1, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;

    .line 1763898
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763899
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1763900
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    .line 1763901
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1763902
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1763903
    :goto_0
    return-object v0

    .line 1763904
    :cond_0
    sget-object v1, LX/BDq;->a:Ljava/lang/Class;

    const-string v2, "Failed to change notification settings"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1763905
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
