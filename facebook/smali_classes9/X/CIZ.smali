.class public LX/CIZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CIZ;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874355
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CIZ;->a:Ljava/util/Map;

    .line 1874356
    return-void
.end method

.method public static a(LX/0QB;)LX/CIZ;
    .locals 3

    .prologue
    .line 1874357
    sget-object v0, LX/CIZ;->b:LX/CIZ;

    if-nez v0, :cond_1

    .line 1874358
    const-class v1, LX/CIZ;

    monitor-enter v1

    .line 1874359
    :try_start_0
    sget-object v0, LX/CIZ;->b:LX/CIZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1874360
    if-eqz v2, :cond_0

    .line 1874361
    :try_start_1
    new-instance v0, LX/CIZ;

    invoke-direct {v0}, LX/CIZ;-><init>()V

    .line 1874362
    move-object v0, v0

    .line 1874363
    sput-object v0, LX/CIZ;->b:LX/CIZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874364
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1874365
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1874366
    :cond_1
    sget-object v0, LX/CIZ;->b:LX/CIZ;

    return-object v0

    .line 1874367
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1874368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1874369
    iget-object v0, p0, LX/CIZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CIZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
