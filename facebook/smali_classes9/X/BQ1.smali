.class public LX/BQ1;
.super LX/BPy;
.source ""

# interfaces
.implements LX/5w5;


# instance fields
.field public a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:LX/5wQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/BQ0;

.field public final g:LX/BPw;

.field private h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$TimelineContextListItemsConnectionFields;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/BQ6;

.field public j:LX/BQ6;

.field public k:Z

.field public l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/15i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5SB;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1781255
    invoke-direct {p0, p1}, LX/BPy;-><init>(LX/5SB;)V

    .line 1781256
    new-instance v0, LX/BQ0;

    invoke-direct {v0}, LX/BQ0;-><init>()V

    iput-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781257
    new-instance v0, LX/BPw;

    invoke-direct {v0}, LX/BPw;-><init>()V

    iput-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781258
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->h:LX/0am;

    .line 1781259
    sget-object v0, LX/BQ6;->NONE:LX/BQ6;

    iput-object v0, p0, LX/BQ1;->i:LX/BQ6;

    .line 1781260
    sget-object v0, LX/BQ6;->NONE:LX/BQ6;

    iput-object v0, p0, LX/BQ1;->j:LX/BQ6;

    .line 1781261
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BQ1;->k:Z

    .line 1781262
    iput-object v1, p0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    .line 1781263
    iput-object v1, p0, LX/BQ1;->o:Ljava/lang/String;

    .line 1781264
    return-void
.end method

.method private a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V
    .locals 3

    .prologue
    .line 1781269
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1781270
    iput-object p1, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    .line 1781271
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->v()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    .line 1781272
    iput-object p1, p0, LX/BQ1;->d:LX/5wQ;

    .line 1781273
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->y()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1781274
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->y()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;->a()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->e:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    .line 1781275
    :goto_0
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781276
    iput-object p1, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    .line 1781277
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->w()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    .line 1781278
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781279
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    .line 1781280
    instance-of v0, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1781281
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->I()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->h:LX/0am;

    .line 1781282
    :cond_0
    invoke-direct {p0, p1}, LX/BQ1;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V

    .line 1781283
    invoke-virtual {p0}, LX/BPy;->l()V

    .line 1781284
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_3

    .line 1781285
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1781286
    iget-object v1, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v1}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 1781287
    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_2

    :cond_1
    if-eqz v1, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v1, v0, :cond_3

    .line 1781288
    :cond_2
    invoke-virtual {p0}, LX/BPy;->m()V

    .line 1781289
    :cond_3
    return-void

    .line 1781290
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, LX/BQ1;->e:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    goto :goto_0
.end method

.method public static ao(LX/BQ1;)LX/5zT;
    .locals 3

    .prologue
    .line 1781291
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1781292
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    .line 1781293
    instance-of v1, v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    if-eqz v1, :cond_0

    .line 1781294
    check-cast v0, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    .line 1781295
    new-instance v1, LX/5zT;

    invoke-direct {v1}, LX/5zT;-><init>()V

    .line 1781296
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->l()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->a:Z

    .line 1781297
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->m()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->b:Z

    .line 1781298
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->d()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->c:Z

    .line 1781299
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->e()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->d:Z

    .line 1781300
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->p()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->e:Z

    .line 1781301
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->n()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->f:Z

    .line 1781302
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->u()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->g:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 1781303
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1781304
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->i:Ljava/lang/String;

    .line 1781305
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->q()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->j:Z

    .line 1781306
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->r()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->k:Z

    .line 1781307
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->ce_()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->l:Ljava/lang/String;

    .line 1781308
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->v()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->m:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1781309
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->n:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1781310
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->o:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1781311
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/5zT;->p:Ljava/lang/String;

    .line 1781312
    invoke-virtual {v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;->t()Z

    move-result v2

    iput-boolean v2, v1, LX/5zT;->q:Z

    .line 1781313
    move-object v0, v1

    .line 1781314
    :goto_0
    return-object v0

    .line 1781315
    :cond_0
    new-instance v1, LX/5zT;

    invoke-direct {v1}, LX/5zT;-><init>()V

    invoke-interface {v0}, LX/5wQ;->d()Z

    move-result v2

    .line 1781316
    iput-boolean v2, v1, LX/5zT;->c:Z

    .line 1781317
    move-object v1, v1

    .line 1781318
    invoke-interface {v0}, LX/5wQ;->e()Z

    move-result v2

    .line 1781319
    iput-boolean v2, v1, LX/5zT;->d:Z

    .line 1781320
    move-object v1, v1

    .line 1781321
    invoke-interface {v0}, LX/5wQ;->p()Z

    move-result v2

    .line 1781322
    iput-boolean v2, v1, LX/5zT;->e:Z

    .line 1781323
    move-object v1, v1

    .line 1781324
    invoke-interface {v0}, LX/5wQ;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    .line 1781325
    iput-object v2, v1, LX/5zT;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1781326
    move-object v1, v1

    .line 1781327
    invoke-interface {v0}, LX/5wQ;->s()LX/2rX;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;->a(LX/2rX;)Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    .line 1781328
    iput-object v2, v1, LX/5zT;->m:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1781329
    move-object v1, v1

    .line 1781330
    invoke-interface {v0}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    .line 1781331
    iput-object v0, v1, LX/5zT;->o:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1781332
    move-object v0, v1

    .line 1781333
    goto :goto_0
.end method

.method private b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V
    .locals 4

    .prologue
    .line 1781334
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 1781335
    if-nez p1, :cond_0

    .line 1781336
    :goto_0
    return-void

    .line 1781337
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1781338
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->ce_()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, p0

    iput-object v1, v0, LX/BPw;->d:[Ljava/lang/String;

    goto :goto_0

    .line 1781339
    :cond_1
    new-array v1, p0, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->ce_()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    iput-object v1, v0, LX/BPw;->d:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1781340
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_1

    .line 1781341
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1781342
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 2

    .prologue
    .line 1781343
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_1

    .line 1781344
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1781345
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final F()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 2

    .prologue
    .line 1781346
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v1, :cond_1

    .line 1781347
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1781348
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final G()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781349
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v0, :cond_0

    .line 1781350
    const/4 v0, 0x0

    .line 1781351
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final K()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781352
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781353
    iget-object p0, v0, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;->c()Ljava/lang/String;

    move-result-object p0

    :goto_0
    move-object v0, p0

    .line 1781354
    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final M()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781355
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781356
    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p0

    :goto_0
    move-object v0, p0

    .line 1781357
    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final N()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781384
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781385
    iget-object p0, v0, LX/BQ0;->c:Landroid/net/Uri;

    if-eqz p0, :cond_0

    .line 1781386
    iget-object p0, v0, LX/BQ0;->c:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1781387
    :goto_0
    move-object v0, p0

    .line 1781388
    return-object v0

    .line 1781389
    :cond_0
    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 1781390
    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1781391
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final P()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781358
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v0, :cond_0

    .line 1781359
    const/4 v0, 0x0

    .line 1781360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->ce_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 1781381
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781382
    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->F()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1781383
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;
    .locals 1

    .prologue
    .line 1781378
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781379
    iget-object p0, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-object v0, p0

    .line 1781380
    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781370
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781371
    iget-object p0, v0, LX/BPw;->e:Landroid/net/Uri;

    if-eqz p0, :cond_0

    .line 1781372
    iget-object p0, v0, LX/BPw;->e:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1781373
    :goto_0
    move-object v0, p0

    .line 1781374
    return-object v0

    .line 1781375
    :cond_0
    iget-object p0, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 1781376
    iget-object p0, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object p0

    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1781377
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final W()Z
    .locals 1

    .prologue
    .line 1781369
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final X()Z
    .locals 1

    .prologue
    .line 1781368
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Y()LX/2rX;
    .locals 1

    .prologue
    .line 1781365
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-nez v0, :cond_0

    .line 1781366
    const/4 v0, 0x0

    .line 1781367
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v0}, LX/5wQ;->s()LX/2rX;

    move-result-object v0

    goto :goto_0
.end method

.method public final Z()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781362
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781363
    iget-object p0, v0, LX/BPw;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, p0

    .line 1781364
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781361
    iget-object v0, p0, LX/BQ1;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1781265
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    .line 1781266
    iput-object p1, v0, LX/BQ0;->c:Landroid/net/Uri;

    .line 1781267
    invoke-virtual {p0}, LX/BPy;->l()V

    .line 1781268
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 1

    .prologue
    .line 1781203
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-nez v0, :cond_0

    .line 1781204
    :goto_0
    return-void

    .line 1781205
    :cond_0
    invoke-static {p0}, LX/BQ1;->ao(LX/BQ1;)LX/5zT;

    move-result-object v0

    .line 1781206
    iput-object p1, v0, LX/5zT;->n:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1781207
    move-object v0, v0

    .line 1781208
    invoke-virtual {v0}, LX/5zT;->a()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    move-result-object v0

    .line 1781209
    iput-object v0, p0, LX/BQ1;->d:LX/5wQ;

    .line 1781210
    invoke-virtual {p0}, LX/BPy;->m()V

    .line 1781211
    invoke-virtual {p0}, LX/BPy;->l()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 1

    .prologue
    .line 1781191
    iget-object v0, p0, LX/BQ1;->d:LX/5wQ;

    if-nez v0, :cond_0

    .line 1781192
    :goto_0
    return-void

    .line 1781193
    :cond_0
    invoke-static {p0}, LX/BQ1;->ao(LX/BQ1;)LX/5zT;

    move-result-object v0

    .line 1781194
    iput-object p1, v0, LX/5zT;->o:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1781195
    move-object v0, v0

    .line 1781196
    invoke-virtual {v0}, LX/5zT;->a()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    move-result-object v0

    .line 1781197
    iput-object v0, p0, LX/BQ1;->d:LX/5wQ;

    .line 1781198
    invoke-virtual {p0}, LX/BPy;->m()V

    .line 1781199
    invoke-virtual {p0}, LX/BPy;->l()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1781178
    instance-of v0, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    if-eqz v0, :cond_0

    .line 1781179
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    .line 1781180
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BQ1;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V

    .line 1781181
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-result-object v0

    .line 1781182
    iput-object v0, p0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    .line 1781183
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->d()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/BQ1;->m:LX/15i;

    iput v0, p0, LX/BQ1;->n:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1781184
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->o:Ljava/lang/String;

    .line 1781185
    :goto_0
    invoke-virtual {p0}, LX/BPy;->d()V

    .line 1781186
    return-void

    .line 1781187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1781188
    :cond_0
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-direct {p0, p1}, LX/BQ1;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V

    .line 1781189
    iput-object v1, p0, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    .line 1781190
    iput-object v1, p0, LX/BQ1;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/1Fb;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3
    .param p4    # Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1781151
    new-instance v0, LX/5wa;

    invoke-direct {v0}, LX/5wa;-><init>()V

    .line 1781152
    iput-object p1, v0, LX/5wa;->l:Ljava/lang/String;

    .line 1781153
    move-object v0, v0

    .line 1781154
    iput-object p2, v0, LX/5wa;->s:Ljava/lang/String;

    .line 1781155
    move-object v1, v0

    .line 1781156
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 1781157
    :goto_0
    iput-object v0, v1, LX/5wa;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1781158
    move-object v0, v1

    .line 1781159
    invoke-static {p4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v1

    .line 1781160
    iput-object v1, v0, LX/5wa;->h:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    .line 1781161
    move-object v0, v0

    .line 1781162
    iput-object p5, v0, LX/5wa;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1781163
    move-object v0, v0

    .line 1781164
    invoke-virtual {v0}, LX/5wa;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BQ1;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;)V

    .line 1781165
    sget-object v0, LX/BPx;->PRELIM_DATA:LX/BPx;

    .line 1781166
    iput-object v0, p0, LX/BPy;->e:LX/BPx;

    .line 1781167
    return-void

    .line 1781168
    :cond_0
    new-instance v0, LX/4aO;

    invoke-direct {v0}, LX/4aO;-><init>()V

    invoke-interface {p3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1781169
    iput-object v2, v0, LX/4aO;->c:Ljava/lang/String;

    .line 1781170
    move-object v0, v0

    .line 1781171
    invoke-interface {p3}, LX/1Fb;->c()I

    move-result v2

    .line 1781172
    iput v2, v0, LX/4aO;->d:I

    .line 1781173
    move-object v0, v0

    .line 1781174
    invoke-interface {p3}, LX/1Fb;->a()I

    move-result v2

    .line 1781175
    iput v2, v0, LX/4aO;->b:I

    .line 1781176
    move-object v0, v0

    .line 1781177
    invoke-virtual {v0}, LX/4aO;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z
    .locals 2

    .prologue
    .line 1781148
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    .line 1781149
    invoke-static {p1}, LX/BQ3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->ch_()Ljava/lang/String;

    move-result-object v1

    iget-object p0, v0, LX/BPw;->b:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1781150
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    .prologue
    .line 1781147
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->c()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->c()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ac()Z
    .locals 1

    .prologue
    .line 1781146
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ad()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781143
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1781144
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1781145
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 1781142
    invoke-virtual {p0}, LX/BQ1;->af()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final af()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781139
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->p()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->p()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1781140
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1781141
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->p()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final ag()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$ExternalLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781212
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1781213
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1781214
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->k()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final ak()LX/1vs;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileWizardRefresher"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781215
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/BQ1;->m:LX/15i;

    iget v2, p0, LX/BQ1;->n:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781216
    iget-object v0, p0, LX/BQ1;->e:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    return-object v0
.end method

.method public final p()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1781217
    invoke-super {p0}, LX/BPy;->p()V

    .line 1781218
    iput-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    .line 1781219
    iput-object v0, p0, LX/BQ1;->d:LX/5wQ;

    .line 1781220
    iput-object v0, p0, LX/BQ1;->e:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    .line 1781221
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/BQ1;->h:LX/0am;

    .line 1781222
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    const/4 v1, 0x0

    .line 1781223
    iput-object v1, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    .line 1781224
    iput-object v1, v0, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    .line 1781225
    iget-object v0, p0, LX/BQ1;->g:LX/BPw;

    const/4 v1, 0x0

    .line 1781226
    iput-object v1, v0, LX/BPw;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    .line 1781227
    iput-object v1, v0, LX/BPw;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1781228
    iput-object v1, v0, LX/BPw;->d:[Ljava/lang/String;

    .line 1781229
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1781230
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v0, :cond_0

    .line 1781231
    const/4 v0, 0x0

    .line 1781232
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->D()Z

    move-result v0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1781200
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v0, :cond_0

    .line 1781201
    const/4 v0, 0x0

    .line 1781202
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->A()Z

    move-result v0

    goto :goto_0
.end method

.method public final t()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$TimelineContextListItemsConnectionFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781233
    iget-object v0, p0, LX/BQ1;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781234
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1781235
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BQ1;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1781236
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    const/4 v1, 0x0

    .line 1781237
    iget-object p0, v0, LX/BQ0;->c:Landroid/net/Uri;

    if-eqz p0, :cond_1

    .line 1781238
    :cond_0
    :goto_0
    move v0, v1

    .line 1781239
    return v0

    :cond_1
    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->H()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final x()Z
    .locals 7

    .prologue
    .line 1781240
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    const/4 v1, 0x0

    .line 1781241
    iget-object v2, v0, LX/BQ0;->c:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 1781242
    :cond_0
    :goto_0
    move v0, v1

    .line 1781243
    return v0

    :cond_1
    iget-object v2, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->G()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final y()J
    .locals 6

    .prologue
    .line 1781244
    invoke-virtual {p0}, LX/BQ1;->x()Z

    move-result v0

    const-string v1, "getProfilePictureExpirationTimeSeconds is meaningless for a non-expiring picture"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1781245
    iget-object v0, p0, LX/BQ1;->f:LX/BQ0;

    const-wide/16 v2, 0x0

    .line 1781246
    iget-object v4, v0, LX/BQ0;->c:Landroid/net/Uri;

    if-eqz v4, :cond_1

    .line 1781247
    :cond_0
    :goto_0
    move-wide v0, v2

    .line 1781248
    return-wide v0

    :cond_1
    iget-object v4, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-eqz v4, :cond_0

    iget-object v2, v0, LX/BQ0;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->G()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    goto :goto_0
.end method

.method public final z()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781249
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v0, :cond_1

    .line 1781250
    const/4 v0, 0x0

    .line 1781251
    :goto_0
    move-object v0, v0

    .line 1781252
    if-nez v0, :cond_0

    .line 1781253
    const/4 v0, 0x0

    .line 1781254
    :goto_1
    return-object v0

    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderStructuredNameModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderStructuredNameModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/16z;->a(Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->E()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderStructuredNameModel;

    move-result-object v0

    goto :goto_0
.end method
