.class public final LX/CBQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

.field public final synthetic c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V
    .locals 0

    .prologue
    .line 1856417
    iput-object p1, p0, LX/CBQ;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    iput-object p2, p0, LX/CBQ;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    iput-object p3, p0, LX/CBQ;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x741d46f5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1856418
    iget-object v0, p0, LX/CBQ;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->c:LX/2mJ;

    iget-object v2, p0, LX/CBQ;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856419
    iget-object v0, p0, LX/CBQ;->c:Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13P;

    sget-object v2, LX/77m;->PRIMARY_ACTION:LX/77m;

    iget-object v3, p0, LX/CBQ;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    .line 1856420
    const v0, -0xa74a22c

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
