.class public final LX/Aps;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Apv;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Apt;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1716883
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1716884
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "checkedDrawableRes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "uncheckedDrawableRes"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Aps;->b:[Ljava/lang/String;

    .line 1716885
    iput v3, p0, LX/Aps;->c:I

    .line 1716886
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Aps;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Aps;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Aps;LX/1De;IILX/Apt;)V
    .locals 1

    .prologue
    .line 1716915
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1716916
    iput-object p4, p0, LX/Aps;->a:LX/Apt;

    .line 1716917
    iget-object v0, p0, LX/Aps;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1716918
    return-void
.end method


# virtual methods
.method public final a(LX/1dQ;)LX/Aps;
    .locals 1

    .prologue
    .line 1716913
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput-object p1, v0, LX/Apt;->l:LX/1dQ;

    .line 1716914
    return-object p0
.end method

.method public final a(Z)LX/Aps;
    .locals 1

    .prologue
    .line 1716911
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput-boolean p1, v0, LX/Apt;->c:Z

    .line 1716912
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1716907
    invoke-super {p0}, LX/1X5;->a()V

    .line 1716908
    const/4 v0, 0x0

    iput-object v0, p0, LX/Aps;->a:LX/Apt;

    .line 1716909
    sget-object v0, LX/Apv;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1716910
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Apv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1716897
    iget-object v1, p0, LX/Aps;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Aps;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Aps;->c:I

    if-ge v1, v2, :cond_2

    .line 1716898
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1716899
    :goto_0
    iget v2, p0, LX/Aps;->c:I

    if-ge v0, v2, :cond_1

    .line 1716900
    iget-object v2, p0, LX/Aps;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1716901
    iget-object v2, p0, LX/Aps;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1716902
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1716903
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1716904
    :cond_2
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    .line 1716905
    invoke-virtual {p0}, LX/Aps;->a()V

    .line 1716906
    return-object v0
.end method

.method public final h(I)LX/Aps;
    .locals 2

    .prologue
    .line 1716894
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput p1, v0, LX/Apt;->a:I

    .line 1716895
    iget-object v0, p0, LX/Aps;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1716896
    return-object p0
.end method

.method public final i(I)LX/Aps;
    .locals 2

    .prologue
    .line 1716891
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput p1, v0, LX/Apt;->b:I

    .line 1716892
    iget-object v0, p0, LX/Aps;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1716893
    return-object p0
.end method

.method public final j(I)LX/Aps;
    .locals 1

    .prologue
    .line 1716889
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput p1, v0, LX/Apt;->h:I

    .line 1716890
    return-object p0
.end method

.method public final k(I)LX/Aps;
    .locals 1

    .prologue
    .line 1716887
    iget-object v0, p0, LX/Aps;->a:LX/Apt;

    iput p1, v0, LX/Apt;->i:I

    .line 1716888
    return-object p0
.end method
