.class public final LX/Bw5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic d:LX/Bur;


# direct methods
.method public constructor <init>(LX/Bur;Lcom/facebook/graphql/model/GraphQLStory;ZLcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 1833256
    iput-object p1, p0, LX/Bw5;->d:LX/Bur;

    iput-object p2, p0, LX/Bw5;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-boolean p3, p0, LX/Bw5;->b:Z

    iput-object p4, p0, LX/Bw5;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1833257
    iget-object v0, p0, LX/Bw5;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1833258
    iget-boolean v1, p0, LX/Bw5;->b:Z

    if-eqz v1, :cond_0

    .line 1833259
    iget-object v1, p0, LX/Bw5;->d:LX/Bur;

    iget-object v1, v1, LX/Bur;->g:LX/3Gp;

    iget-object v2, p0, LX/Bw5;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CHEVRON"

    invoke-virtual {v1, v2, v3, v0}, LX/3Gp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1833260
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1833261
    :cond_0
    iget-object v1, p0, LX/Bw5;->d:LX/Bur;

    iget-object v1, v1, LX/Bur;->g:LX/3Gp;

    iget-object v2, p0, LX/Bw5;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CHEVRON"

    invoke-virtual {v1, v2, v3, v0}, LX/3Gp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
