.class public LX/CNU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1881782
    const-class v0, LX/CNU;

    sput-object v0, LX/CNU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1881783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/CNb;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1881784
    const-string v0, "%s.augmentTemplatesWithClientFields"

    sget-object v1, LX/CNU;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0xfd3778f

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1881785
    :try_start_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1881786
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881787
    invoke-static {v0, p1, p2}, LX/CNU;->a(LX/CNb;LX/CNb;Ljava/lang/String;)LX/CNb;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881788
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1881789
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1881790
    const v1, 0x516630d6

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x8b7291a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/CNb;LX/CNb;Ljava/lang/String;)LX/CNb;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1881791
    invoke-static {p0}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    .line 1881792
    new-instance v5, LX/CNa;

    invoke-direct {v5}, LX/CNa;-><init>()V

    .line 1881793
    const-string v1, "client_pp"

    invoke-virtual {p0, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1881794
    new-instance v1, LX/CNV;

    invoke-direct {v1}, LX/CNV;-><init>()V

    .line 1881795
    const-string v2, "client_pp"

    invoke-virtual {v5, v2, v1}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881796
    invoke-virtual {v0, v5}, LX/3j9;->a(LX/CNa;)V

    .line 1881797
    :cond_0
    invoke-virtual {v0, p0}, LX/3j9;->a(LX/CNb;)Ljava/util/Set;

    move-result-object v6

    .line 1881798
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    move v2, v3

    .line 1881799
    :goto_0
    invoke-virtual {p0}, LX/CNb;->b()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1881800
    invoke-virtual {p0, v2}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v8

    .line 1881801
    invoke-virtual {p0, v2}, LX/CNb;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 1881802
    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, LX/CNS;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1881803
    :cond_1
    invoke-virtual {v5, v8, v0}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881804
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1881805
    :cond_3
    check-cast v0, LX/0Px;

    move v4, v3

    .line 1881806
    :goto_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_2

    .line 1881807
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CNb;

    .line 1881808
    invoke-static {v1, p0, v8}, LX/CNU;->a(LX/CNb;LX/CNb;Ljava/lang/String;)LX/CNb;

    move-result-object v9

    .line 1881809
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1881810
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-virtual {v1, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881811
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1881812
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1881813
    :cond_5
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1881814
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_3

    .line 1881815
    :cond_6
    invoke-virtual {v5}, LX/CNa;->a()LX/CNb;

    move-result-object v1

    .line 1881816
    const-string v0, "client_pp"

    invoke-virtual {v1, v0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNV;

    .line 1881817
    invoke-virtual {v0, p1, p2}, LX/CNV;->a(LX/CNb;Ljava/lang/String;)V

    .line 1881818
    return-object v1
.end method

.method public static a(LX/CNb;LX/CNL;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1881819
    invoke-static {p0}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    .line 1881820
    const-string v1, "id"

    invoke-virtual {p0, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1881821
    const-string v1, "id"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p0, v1}, LX/CNL;->a(LX/CNb;Ljava/lang/String;)V

    .line 1881822
    :cond_0
    invoke-virtual {v0, p0}, LX/3j9;->a(LX/CNb;)Ljava/util/Set;

    move-result-object v6

    move v3, v4

    .line 1881823
    :goto_0
    invoke-virtual {p0}, LX/CNb;->b()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1881824
    invoke-virtual {p0, v3}, LX/CNb;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 1881825
    invoke-virtual {p0, v3}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, LX/CNS;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1881826
    check-cast v0, LX/0Px;

    move v5, v4

    .line 1881827
    :goto_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_2

    .line 1881828
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CNb;

    .line 1881829
    const-string v2, "client_pp"

    invoke-virtual {v1, v2}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CNV;

    .line 1881830
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/CNV;->a()LX/CNb;

    move-result-object v7

    if-eq p0, v7, :cond_1

    .line 1881831
    invoke-virtual {p0, v3}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, p0, v7}, LX/CNV;->a(LX/CNb;Ljava/lang/String;)V

    .line 1881832
    invoke-static {v1, p1}, LX/CNU;->a(LX/CNb;LX/CNL;)V

    .line 1881833
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 1881834
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1881835
    :cond_3
    return-void
.end method
