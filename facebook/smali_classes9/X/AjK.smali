.class public final LX/AjK;
.super LX/22z;
.source ""


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/feed/data/FeedDataLoader;

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;J)V
    .locals 2

    .prologue
    .line 1707771
    iput-object p1, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iput-wide p2, p0, LX/AjK;->a:J

    invoke-direct {p0}, LX/22z;-><init>()V

    .line 1707772
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/AjK;->c:J

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1707773
    iget-wide v0, p0, LX/AjK;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1707774
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    .line 1707775
    iput-object v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    .line 1707776
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/AjK;->c:J

    sub-long/2addr v2, v4

    .line 1707777
    iput-wide v2, v0, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 1707778
    :goto_0
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->U(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 1707779
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, LX/0gD;->m:LX/0qk;

    const/4 v1, 0x0

    sget-object v2, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    iget-object v3, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v3, v3, LX/0gD;->b:LX/0fz;

    invoke-virtual {v3}, LX/0fz;->i()I

    move-result v3

    sget-object v4, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0qk;->a(ILX/1lq;ILX/0ta;)V

    .line 1707780
    return-void

    .line 1707781
    :cond_0
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/22v;->INSTANT_RERANKED:LX/22v;

    .line 1707782
    iput-object v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    .line 1707783
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->c(Lcom/facebook/feed/data/FeedDataLoader;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1707784
    check-cast p1, Lcom/facebook/api/feed/FetchFeedResult;

    const-wide/16 v2, 0x0

    .line 1707785
    const-string v0, "FeedDataLoader.loadAfterCacheDataSuccess"

    const v1, 0x7031578

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1707786
    :try_start_0
    iget-wide v0, p0, LX/AjK;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1707787
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0pW;->a(Z)V

    .line 1707788
    :cond_0
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v2, LX/23E;->BEFORE:LX/23E;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v1, p1

    .line 1707789
    invoke-static/range {v0 .. v5}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedResult;LX/23E;ZJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1707790
    const v0, -0x767d4af7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1707791
    return-void

    .line 1707792
    :catchall_0
    move-exception v0

    const v1, -0x15f03016

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 1707793
    iget-wide v0, p0, LX/AjK;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1707794
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/AjK;->c:J

    sub-long/2addr v2, v4

    .line 1707795
    iput-wide v2, v0, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 1707796
    :cond_0
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->U(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 1707797
    sget-object v0, Lcom/facebook/feed/data/FeedDataLoader;->E:Ljava/lang/String;

    const-string v1, "PTR Reranking failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1707798
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rj;->PTR_RERANKING_FAILURE:LX/0rj;

    invoke-virtual {v0, v1}, LX/0gD;->a(LX/0rj;)V

    .line 1707799
    iget-object v0, p0, LX/AjK;->b:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/23E;->BEFORE:LX/23E;

    invoke-static {v0, p1, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Ljava/lang/Throwable;LX/23E;)V

    .line 1707800
    return-void
.end method
