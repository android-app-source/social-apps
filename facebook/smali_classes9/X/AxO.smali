.class public final LX/AxO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1728190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1728191
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1728192
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1728193
    const/4 v4, 0x1

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    .line 1728194
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_6

    .line 1728195
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1728196
    :goto_1
    move v1, v9

    .line 1728197
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1728198
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1728199
    :cond_1
    const-string v12, "feature_value"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1728200
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v5

    move v3, v4

    .line 1728201
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 1728202
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1728203
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1728204
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_2

    if-eqz v11, :cond_2

    .line 1728205
    const-string v12, "feature_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1728206
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_2

    .line 1728207
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1728208
    :cond_4
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1728209
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 1728210
    if-eqz v3, :cond_5

    move-object v3, p1

    .line 1728211
    invoke-virtual/range {v3 .. v8}, LX/186;->a(IDD)V

    .line 1728212
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_1

    :cond_6
    move v3, v9

    move-wide v5, v7

    move v10, v9

    goto :goto_2
.end method
