.class public final LX/CVf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 1904360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1904361
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1904362
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1904363
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1904364
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1904365
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904366
    :goto_1
    move v1, v2

    .line 1904367
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1904368
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1904369
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1904370
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1904371
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1904372
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1904373
    const-string v9, "duration_min"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1904374
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_2

    .line 1904375
    :cond_2
    const-string v9, "product"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1904376
    invoke-static {p0, p1}, LX/CVe;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_2

    .line 1904377
    :cond_3
    const-string v9, "time_start"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1904378
    invoke-static {p0, p1}, LX/CVS;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_2

    .line 1904379
    :cond_4
    const-string v9, "timeslot_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1904380
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 1904381
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1904382
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1904383
    if-eqz v1, :cond_7

    .line 1904384
    invoke-virtual {p1, v2, v7, v2}, LX/186;->a(III)V

    .line 1904385
    :cond_7
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1904386
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1904387
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1904388
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1904389
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1904390
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1904391
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 v2, 0x0

    .line 1904392
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1904393
    invoke-virtual {p0, v1, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 1904394
    if-eqz v2, :cond_0

    .line 1904395
    const-string v3, "duration_min"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904396
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1904397
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1904398
    if-eqz v2, :cond_1

    .line 1904399
    const-string v3, "product"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904400
    invoke-static {p0, v2, p2, p3}, LX/CVe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904401
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1904402
    if-eqz v2, :cond_2

    .line 1904403
    const-string v3, "time_start"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904404
    invoke-static {p0, v2, p2}, LX/CVS;->a(LX/15i;ILX/0nX;)V

    .line 1904405
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1904406
    if-eqz v2, :cond_3

    .line 1904407
    const-string v3, "timeslot_id"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904408
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1904409
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1904410
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1904411
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1904412
    return-void
.end method
