.class public final LX/Cbh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

.field public final synthetic b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V
    .locals 0

    .prologue
    .line 1919743
    iput-object p1, p0, LX/Cbh;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iput-object p2, p0, LX/Cbh;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x63b7092b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919744
    iget-object v1, p0, LX/Cbh;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v1

    .line 1919745
    iget-object v2, p0, LX/Cbh;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->s:LX/9hh;

    iget-object v3, p0, LX/Cbh;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v3}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, LX/Cbh;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->b()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x0

    .line 1919746
    new-instance v6, LX/9hg;

    invoke-direct {v6, v2, v3, v1, v4}, LX/9hg;-><init>(LX/9hh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1919747
    iget-object v7, v2, LX/9hh;->f:LX/0Uh;

    const/16 v8, 0x405

    invoke-virtual {v7, v8, p1}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1919748
    const/4 v7, 0x2

    new-array v7, v7, [LX/4VT;

    new-instance v8, LX/9hy;

    invoke-direct {v8, v3}, LX/9hy;-><init>(Ljava/lang/String;)V

    aput-object v8, v7, p1

    const/4 v8, 0x1

    new-instance p1, LX/9hx;

    invoke-direct {p1, v3}, LX/9hx;-><init>(Ljava/lang/String;)V

    aput-object p1, v7, v8

    invoke-static {v2, v6, v7}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1919749
    :goto_0
    iget-object v1, p0, LX/Cbh;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t:LX/9iE;

    iget-object v2, p0, LX/Cbh;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    iget-object v3, p0, LX/Cbh;->b:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    .line 1919750
    const-string v4, "photo_location_suggestion_reject"

    invoke-static {v1, v4, v2, v3}, LX/9iE;->a(LX/9iE;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;LX/5kD;)V

    .line 1919751
    const v1, 0x7a11dd26

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    iget-object v7, v2, LX/9hh;->d:LX/9hs;

    .line 1919752
    new-instance v8, LX/9hk;

    new-instance p1, LX/9hr;

    invoke-direct {p1, v7, v3}, LX/9hr;-><init>(LX/9hs;Ljava/lang/String;)V

    invoke-direct {v8, v3, p1}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    move-object v7, v8

    .line 1919753
    invoke-static {v2, v6, v7}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
