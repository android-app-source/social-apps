.class public final LX/Aka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/1SX;


# direct methods
.method public constructor <init>(LX/1SX;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1709221
    iput-object p1, p0, LX/Aka;->e:LX/1SX;

    iput-object p2, p0, LX/Aka;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aka;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aka;->c:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    iput-object p5, p0, LX/Aka;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1709222
    iget-object v0, p0, LX/Aka;->e:LX/1SX;

    iget-object v1, p0, LX/Aka;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Aka;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1709223
    iget-object v0, p0, LX/Aka;->c:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    .line 1709224
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Aka;->e:LX/1SX;

    invoke-virtual {v0}, LX/1SX;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1709225
    :cond_0
    iget-object v0, p0, LX/Aka;->e:LX/1SX;

    iget-object v1, p0, LX/Aka;->c:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    iget-object v2, p0, LX/Aka;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Aka;->d:Landroid/view/View;

    invoke-static {v0, v1, v2, v3}, LX/1SX;->a(LX/1SX;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1709226
    :goto_0
    return v4

    .line 1709227
    :cond_1
    iget-object v0, p0, LX/Aka;->e:LX/1SX;

    iget-object v1, p0, LX/Aka;->c:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    iget-object v2, p0, LX/Aka;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Aka;->d:Landroid/view/View;

    .line 1709228
    new-instance v5, LX/0ju;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v5, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p0, 0x7f081048

    invoke-virtual {v5, p0}, LX/0ju;->a(I)LX/0ju;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    const p0, 0x7f0810da

    new-instance p1, LX/Akc;

    invoke-direct {p1, v0, v1, v2, v3}, LX/Akc;-><init>(LX/1SX;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    invoke-virtual {v5, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const p0, 0x7f0810d9

    new-instance p1, LX/Akb;

    invoke-direct {p1, v0}, LX/Akb;-><init>(LX/1SX;)V

    invoke-virtual {v5, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    .line 1709229
    goto :goto_0
.end method
