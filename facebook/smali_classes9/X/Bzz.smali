.class public LX/Bzz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C00;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bzz",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C00;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839713
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839714
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bzz;->b:LX/0Zi;

    .line 1839715
    iput-object p1, p0, LX/Bzz;->a:LX/0Ot;

    .line 1839716
    return-void
.end method

.method public static a(LX/0QB;)LX/Bzz;
    .locals 4

    .prologue
    .line 1839717
    const-class v1, LX/Bzz;

    monitor-enter v1

    .line 1839718
    :try_start_0
    sget-object v0, LX/Bzz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839719
    sput-object v2, LX/Bzz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839720
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839721
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839722
    new-instance v3, LX/Bzz;

    const/16 p0, 0x1e54

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bzz;-><init>(LX/0Ot;)V

    .line 1839723
    move-object v0, v3

    .line 1839724
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839725
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bzz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839726
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1839728
    check-cast p2, LX/Bzy;

    .line 1839729
    iget-object v0, p0, LX/Bzz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C00;

    iget-object v1, p2, LX/Bzy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bzy;->b:LX/1Pm;

    .line 1839730
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    .line 1839731
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1839732
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1839733
    const/4 v5, 0x0

    .line 1839734
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    if-nez p0, :cond_2

    .line 1839735
    :cond_0
    :goto_0
    move-object p0, v5

    .line 1839736
    iget-object v5, v0, LX/C00;->c:LX/Ap5;

    invoke-virtual {v5, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object p2

    if-eqz p0, :cond_1

    const/4 v5, 0x3

    :goto_1
    invoke-virtual {p2, v5}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object v5

    iget-object p2, v0, LX/C00;->f:LX/1qb;

    invoke-virtual {p2, v4}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object p2

    invoke-virtual {v5, p2}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v5

    invoke-static {v4}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const/4 p0, 0x2

    invoke-interface {v4, v5, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    .line 1839737
    const v5, -0x6665946e

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1839738
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v4, v4

    .line 1839739
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    .line 1839740
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a011d

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const/4 p0, 0x2

    invoke-interface {v4, v5, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v4, v4

    .line 1839741
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    .line 1839742
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1839743
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v5, -0x67292209

    invoke-static {v4, v5}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 1839744
    iget-object v5, v0, LX/C00;->d:LX/AnV;

    invoke-virtual {v5, p1}, LX/AnV;->c(LX/1De;)LX/AnT;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/AnT;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/AnT;

    move-result-object v4

    invoke-static {v1}, LX/1WF;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 1839745
    iget-object v0, v4, LX/AnT;->a:LX/AnU;

    iput-object v5, v0, LX/AnU;->b:LX/162;

    .line 1839746
    move-object v4, v4

    .line 1839747
    invoke-virtual {v4}, LX/1X5;->b()LX/1Dg;

    move-result-object v4

    move-object v4, v4

    .line 1839748
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1839749
    return-object v0

    :cond_1
    const/4 v5, 0x4

    goto/16 :goto_1

    .line 1839750
    :cond_2
    iget-object p0, v0, LX/C00;->a:Landroid/content/res/Resources;

    const p2, 0x7f0b00a2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 1839751
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-static {p2, p0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    .line 1839752
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1839753
    invoke-static {}, LX/1dS;->b()V

    .line 1839754
    iget v0, p1, LX/1dQ;->b:I

    .line 1839755
    packed-switch v0, :pswitch_data_0

    .line 1839756
    :goto_0
    return-object v2

    .line 1839757
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1839758
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1839759
    check-cast v1, LX/Bzy;

    .line 1839760
    iget-object v3, p0, LX/Bzz;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C00;

    iget-object p1, v1, LX/Bzy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/Bzy;->b:LX/1Pm;

    .line 1839761
    iget-object p0, v3, LX/C00;->e:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1839762
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x6665946e
        :pswitch_0
    .end packed-switch
.end method
