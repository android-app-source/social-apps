.class public abstract LX/BT2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1786695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)V
.end method

.method public abstract b()I
.end method

.method public abstract b(I)V
.end method

.method public abstract c()V
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1786681
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    .line 1786682
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    .line 1786683
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 1786684
    invoke-virtual {p0}, LX/BT2;->a()I

    move-result v3

    if-lt v1, v3, :cond_0

    invoke-virtual {p0}, LX/BT2;->b()I

    move-result v3

    if-le v1, v3, :cond_2

    .line 1786685
    :cond_0
    const/4 v1, 0x2

    if-ne v2, v1, :cond_1

    .line 1786686
    invoke-virtual {p0}, LX/BT2;->c()V

    .line 1786687
    :goto_0
    return v0

    .line 1786688
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1786689
    :cond_2
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1786690
    :pswitch_0
    invoke-virtual {p0, v1}, LX/BT2;->a(I)V

    .line 1786691
    iput v1, p0, LX/BT2;->a:I

    goto :goto_0

    .line 1786692
    :pswitch_1
    invoke-virtual {p0, v1}, LX/BT2;->b(I)V

    .line 1786693
    iput v1, p0, LX/BT2;->a:I

    goto :goto_0

    .line 1786694
    :pswitch_2
    invoke-virtual {p0}, LX/BT2;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
