.class public final enum LX/Bi3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bi3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bi3;

.field public static final enum PAGE_HEADER:LX/Bi3;

.field public static final enum PAGE_INFO_CARD:LX/Bi3;

.field public static final enum PLACE_TIPS:LX/Bi3;

.field public static final enum STORE_LOCATOR:LX/Bi3;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1809890
    new-instance v0, LX/Bi3;

    const-string v1, "PAGE_HEADER"

    const-string v2, "page_header"

    invoke-direct {v0, v1, v3, v2}, LX/Bi3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Bi3;->PAGE_HEADER:LX/Bi3;

    .line 1809891
    new-instance v0, LX/Bi3;

    const-string v1, "PAGE_INFO_CARD"

    const-string v2, "page_info_card"

    invoke-direct {v0, v1, v4, v2}, LX/Bi3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Bi3;->PAGE_INFO_CARD:LX/Bi3;

    .line 1809892
    new-instance v0, LX/Bi3;

    const-string v1, "STORE_LOCATOR"

    const-string v2, "store_locator"

    invoke-direct {v0, v1, v5, v2}, LX/Bi3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Bi3;->STORE_LOCATOR:LX/Bi3;

    .line 1809893
    new-instance v0, LX/Bi3;

    const-string v1, "PLACE_TIPS"

    const-string v2, "place_tips"

    invoke-direct {v0, v1, v6, v2}, LX/Bi3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Bi3;->PLACE_TIPS:LX/Bi3;

    .line 1809894
    const/4 v0, 0x4

    new-array v0, v0, [LX/Bi3;

    sget-object v1, LX/Bi3;->PAGE_HEADER:LX/Bi3;

    aput-object v1, v0, v3

    sget-object v1, LX/Bi3;->PAGE_INFO_CARD:LX/Bi3;

    aput-object v1, v0, v4

    sget-object v1, LX/Bi3;->STORE_LOCATOR:LX/Bi3;

    aput-object v1, v0, v5

    sget-object v1, LX/Bi3;->PLACE_TIPS:LX/Bi3;

    aput-object v1, v0, v6

    sput-object v0, LX/Bi3;->$VALUES:[LX/Bi3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1809895
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1809896
    iput-object p3, p0, LX/Bi3;->name:Ljava/lang/String;

    .line 1809897
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bi3;
    .locals 1

    .prologue
    .line 1809898
    const-class v0, LX/Bi3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bi3;

    return-object v0
.end method

.method public static values()[LX/Bi3;
    .locals 1

    .prologue
    .line 1809899
    sget-object v0, LX/Bi3;->$VALUES:[LX/Bi3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bi3;

    return-object v0
.end method
