.class public final LX/BJw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772490
    iput-object p1, p0, LX/BJw;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7c7f772d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772491
    iget-object v1, p0, LX/BJw;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Y:LX/BJG;

    iget-object v2, p0, LX/BJw;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->ALBUM:LX/BKc;

    invoke-virtual {v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;)Landroid/view/View;

    move-result-object v2

    .line 1772492
    iget-boolean v3, v1, LX/BJG;->l:Z

    if-eqz v3, :cond_1

    .line 1772493
    :goto_0
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    .line 1772494
    iget-boolean p0, v3, LX/0ht;->r:Z

    move v3, p0

    .line 1772495
    if-eqz v3, :cond_0

    .line 1772496
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    invoke-virtual {v3}, LX/0ht;->l()V

    .line 1772497
    :cond_0
    new-instance v3, LX/0ht;

    iget-object p0, v1, LX/BJG;->e:Landroid/content/Context;

    invoke-direct {v3, p0}, LX/0ht;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, LX/BJG;->m:LX/0ht;

    .line 1772498
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    invoke-static {v1}, LX/BJG;->d(LX/BJG;)Landroid/view/View;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/0ht;->d(Landroid/view/View;)V

    .line 1772499
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, LX/0ht;->c(Z)V

    .line 1772500
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    new-instance p0, LX/BJC;

    invoke-direct {p0, v1}, LX/BJC;-><init>(LX/BJG;)V

    .line 1772501
    iput-object p0, v3, LX/0ht;->H:LX/2dD;

    .line 1772502
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    invoke-virtual {v3, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1772503
    const v1, -0x1dce1a90

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1772504
    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/BJG;->l:Z

    .line 1772505
    new-instance v3, LX/BJD;

    invoke-direct {v3, v1}, LX/BJD;-><init>(LX/BJG;)V

    iput-object v3, v1, LX/BJG;->h:LX/BJD;

    .line 1772506
    invoke-virtual {v1}, LX/BJG;->a()V

    goto :goto_0
.end method
