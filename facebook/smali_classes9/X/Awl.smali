.class public LX/Awl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1727144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727145
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1727147
    invoke-static {p0}, LX/Awl;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727148
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1727149
    :goto_0
    return-void

    .line 1727150
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/Awk;

    invoke-direct {v1, p0, p1}, LX/Awk;-><init>(Landroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1727146
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
