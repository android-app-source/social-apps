.class public abstract LX/BIG;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/BIF;


# static fields
.field public static final e:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public a:LX/0wL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I

.field public f:I

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field public i:I

.field private j:I

.field public k:Z

.field private l:LX/BH5;

.field public m:LX/BHu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1770159
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/BIG;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1770157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BIG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770146
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770147
    const/4 v0, 0x0

    iput v0, p0, LX/BIG;->d:I

    .line 1770148
    const/4 p1, 0x1

    .line 1770149
    const-class v0, LX/BIG;

    invoke-static {v0, p0}, LX/BIG;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770150
    sget-object v0, LX/BIG;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, LX/BIG;->f:I

    .line 1770151
    invoke-virtual {p0}, LX/BIG;->getLayoutResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1770152
    iput-boolean p1, p0, LX/BIG;->k:Z

    .line 1770153
    invoke-virtual {p0, p1}, LX/BIG;->setFocusable(Z)V

    .line 1770154
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, LX/BIG;->setDescendantFocusability(I)V

    .line 1770155
    invoke-static {p0}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1770156
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770141
    if-nez p2, :cond_0

    .line 1770142
    :goto_0
    return-object p1

    .line 1770143
    :cond_0
    if-nez p1, :cond_1

    move-object p1, p2

    .line 1770144
    goto :goto_0

    .line 1770145
    :cond_1
    invoke-virtual {p0}, LX/BIG;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081388

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/BIG;

    invoke-static {p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v1

    check-cast v1, LX/0wL;

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->b(LX/0QB;)Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    iput-object v1, p1, LX/BIG;->a:LX/0wL;

    iput-object p0, p1, LX/BIG;->b:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    return-void
.end method

.method public static getXRayDescription(LX/BIG;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1770137
    const/4 v0, 0x0

    .line 1770138
    invoke-virtual {p0}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1770139
    invoke-virtual {p0}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1770140
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1770122
    iget-object v0, p0, LX/BIG;->g:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1770123
    const v0, 0x7f0d2516

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/BIG;->g:Landroid/widget/ImageView;

    .line 1770124
    :cond_0
    iget-object v1, p0, LX/BIG;->g:Landroid/widget/ImageView;

    if-eqz p2, :cond_2

    const v0, 0x7f0217f9

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1770125
    iget-object v0, p0, LX/BIG;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770126
    iput p1, p0, LX/BIG;->j:I

    .line 1770127
    if-eqz p2, :cond_1

    .line 1770128
    iget-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    if-nez v0, :cond_3

    .line 1770129
    const v0, 0x7f0d2517

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    .line 1770130
    :goto_1
    iget-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1770131
    :cond_1
    iget-object v0, p0, LX/BIG;->a:LX/0wL;

    invoke-virtual {p0}, LX/BIG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081386

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1770132
    invoke-static {v0, p0, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1770133
    invoke-static {p0}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1770134
    return-void

    .line 1770135
    :cond_2
    const v0, 0x7f02183f

    goto :goto_0

    .line 1770136
    :cond_3
    iget-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(LX/1bp;LX/BH5;)V
    .locals 2
    .param p1    # LX/1bp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770114
    if-eqz p1, :cond_1

    .line 1770115
    new-instance v0, LX/BIJ;

    invoke-direct {v0, p0}, LX/BIJ;-><init>(LX/BIG;)V

    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    .line 1770116
    iget-object v0, p0, LX/BIG;->m:LX/BHu;

    if-eqz v0, :cond_0

    .line 1770117
    iget-object v0, p0, LX/BIG;->m:LX/BHu;

    invoke-virtual {p1, v0}, LX/1bp;->a(LX/1Ai;)V

    .line 1770118
    :cond_0
    iput-object p2, p0, LX/BIG;->l:LX/BH5;

    .line 1770119
    :cond_1
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1770120
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1770121
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1770101
    iput v2, p0, LX/BIG;->d:I

    .line 1770102
    iget-object v0, p0, LX/BIG;->l:LX/BH5;

    if-eqz v0, :cond_1

    .line 1770103
    iget-object v0, p0, LX/BIG;->l:LX/BH5;

    iget-object v1, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    .line 1770104
    iget-object v3, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean v3, v3, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h:Z

    if-nez v3, :cond_1

    .line 1770105
    iget-object v3, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v3, v3, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->q:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770106
    iget-object v3, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v3, v3, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->q:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    .line 1770107
    const/16 v4, 0xc

    if-eq v3, v4, :cond_0

    iget-object v4, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v4, v4, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    .line 1770108
    iget-object v1, v4, LX/BHj;->b:LX/BHl;

    invoke-virtual {v1}, LX/BHl;->b()I

    move-result v1

    move v4, v1

    .line 1770109
    if-lt v3, v4, :cond_1

    .line 1770110
    :cond_0
    iget-object v3, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h:Z

    .line 1770111
    iget-object v3, v0, LX/BH5;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b()V

    .line 1770112
    :cond_1
    invoke-static {p0}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1770113
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1770098
    const/4 v0, 0x2

    iput v0, p0, LX/BIG;->d:I

    .line 1770099
    invoke-static {p0}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1770100
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 1770089
    iget-object v0, p0, LX/BIG;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1770090
    iget-object v0, p0, LX/BIG;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770091
    :cond_0
    iget-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1770092
    iget-object v0, p0, LX/BIG;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1770093
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, LX/BIG;->j:I

    .line 1770094
    iget-object v0, p0, LX/BIG;->a:LX/0wL;

    invoke-virtual {p0}, LX/BIG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1770095
    invoke-static {v0, p0, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1770096
    invoke-static {p0}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1770097
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1770088
    invoke-virtual {p0}, LX/BIG;->getController()LX/1aZ;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/BIG;->d:I

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, LX/BIG;->k:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getController()LX/1aZ;
    .locals 1

    .prologue
    .line 1770086
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1770087
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 1770085
    iget v0, p0, LX/BIG;->f:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 1770084
    iget v0, p0, LX/BIG;->i:I

    return v0
.end method

.method public getMediaItem()Lcom/facebook/ipc/media/MediaItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1770083
    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    return-object v0
.end method

.method public getSelectedOrder()I
    .locals 1

    .prologue
    .line 1770082
    iget v0, p0, LX/BIG;->j:I

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 1770081
    const/4 v0, 0x1

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .prologue
    .line 1770080
    iget v0, p0, LX/BIG;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 0

    .prologue
    .line 1770078
    invoke-super {p0, p1, p1}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1770079
    return-void
.end method

.method public setAccessibilityTalkback(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1770067
    invoke-virtual {p0}, LX/BIG;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, LX/BIG;->getItemType()LX/BHH;

    move-result-object v1

    invoke-virtual {v1}, LX/BHH;->getStringResource()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1770068
    invoke-virtual {p0}, LX/BIG;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1770069
    invoke-direct {p0, v0, p1}, LX/BIG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1770070
    :goto_0
    return-void

    .line 1770071
    :cond_0
    invoke-virtual {p0}, LX/BIG;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1770072
    invoke-virtual {p0}, LX/BIG;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1770073
    invoke-virtual {p0}, LX/BIG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081385

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1770074
    invoke-direct {p0, v0, p1}, LX/BIG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1770075
    :cond_1
    invoke-direct {p0, v0, p1}, LX/BIG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1770076
    :cond_2
    invoke-virtual {p0}, LX/BIG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081384

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1770077
    invoke-direct {p0, v0, p1}, LX/BIG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BIG;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setController(LX/1bp;)V
    .locals 1
    .param p1    # LX/1bp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770065
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/BIG;->a(LX/1bp;LX/BH5;)V

    .line 1770066
    return-void
.end method
