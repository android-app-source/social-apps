.class public LX/Alf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Alf;


# instance fields
.field private final a:LX/0Zb;

.field public b:LX/5oW;


# direct methods
.method public constructor <init>(LX/0Zb;LX/5oW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710056
    iput-object p1, p0, LX/Alf;->a:LX/0Zb;

    .line 1710057
    iput-object p2, p0, LX/Alf;->b:LX/5oW;

    .line 1710058
    return-void
.end method

.method public static a(LX/0QB;)LX/Alf;
    .locals 5

    .prologue
    .line 1710059
    sget-object v0, LX/Alf;->c:LX/Alf;

    if-nez v0, :cond_1

    .line 1710060
    const-class v1, LX/Alf;

    monitor-enter v1

    .line 1710061
    :try_start_0
    sget-object v0, LX/Alf;->c:LX/Alf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1710062
    if-eqz v2, :cond_0

    .line 1710063
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1710064
    new-instance p0, LX/Alf;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/5oW;->a(LX/0QB;)LX/5oW;

    move-result-object v4

    check-cast v4, LX/5oW;

    invoke-direct {p0, v3, v4}, LX/Alf;-><init>(LX/0Zb;LX/5oW;)V

    .line 1710065
    move-object v0, p0

    .line 1710066
    sput-object v0, LX/Alf;->c:LX/Alf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1710067
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1710068
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1710069
    :cond_1
    sget-object v0, LX/Alf;->c:LX/Alf;

    return-object v0

    .line 1710070
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1710071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1710072
    iget-object v0, p0, LX/Alf;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "photo_reminder_animation_start"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "photo_reminder"

    .line 1710073
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1710074
    move-object v1, v1

    .line 1710075
    const-string v2, "prompt_type"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1710076
    iput-object p1, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1710077
    move-object v1, v1

    .line 1710078
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1710079
    return-void
.end method
