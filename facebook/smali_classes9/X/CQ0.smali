.class public final LX/CQ0;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1885312
    invoke-static {}, LX/CQ1;->a()LX/CQ1;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 1885313
    iput v1, p0, LX/CQ0;->a:I

    .line 1885314
    iput v1, p0, LX/CQ0;->b:I

    .line 1885315
    iput v1, p0, LX/CQ0;->c:I

    .line 1885316
    iput v1, p0, LX/CQ0;->d:I

    .line 1885317
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1885318
    const-string v0, "NTBackgroundReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1885319
    if-ne p0, p1, :cond_1

    .line 1885320
    :cond_0
    :goto_0
    return v0

    .line 1885321
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1885322
    goto :goto_0

    .line 1885323
    :cond_3
    check-cast p1, LX/CQ0;

    .line 1885324
    iget v2, p0, LX/CQ0;->a:I

    iget v3, p1, LX/CQ0;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1885325
    goto :goto_0

    .line 1885326
    :cond_4
    iget v2, p0, LX/CQ0;->b:I

    iget v3, p1, LX/CQ0;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1885327
    goto :goto_0

    .line 1885328
    :cond_5
    iget v2, p0, LX/CQ0;->c:I

    iget v3, p1, LX/CQ0;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1885329
    goto :goto_0

    .line 1885330
    :cond_6
    iget v2, p0, LX/CQ0;->d:I

    iget v3, p1, LX/CQ0;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1885331
    goto :goto_0
.end method
