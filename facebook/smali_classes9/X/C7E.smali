.class public LX/C7E;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7I;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7E",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7I;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851082
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851083
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C7E;->b:LX/0Zi;

    .line 1851084
    iput-object p1, p0, LX/C7E;->a:LX/0Ot;

    .line 1851085
    return-void
.end method

.method public static a(LX/0QB;)LX/C7E;
    .locals 4

    .prologue
    .line 1851071
    const-class v1, LX/C7E;

    monitor-enter v1

    .line 1851072
    :try_start_0
    sget-object v0, LX/C7E;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851073
    sput-object v2, LX/C7E;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851074
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851075
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851076
    new-instance v3, LX/C7E;

    const/16 p0, 0x1f89

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7E;-><init>(LX/0Ot;)V

    .line 1851077
    move-object v0, v3

    .line 1851078
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851079
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851080
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1851070
    const v0, 0xa4f2351

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1851063
    check-cast p2, LX/C7C;

    .line 1851064
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1851065
    iget-object v0, p0, LX/C7E;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7I;

    iget-object v2, p2, LX/C7C;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C7C;->b:LX/1Pr;

    invoke-virtual {v0, p1, v2, v3, v1}, LX/C7I;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1np;)LX/1Dg;

    move-result-object v2

    .line 1851066
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1851067
    check-cast v0, LX/8pR;

    iput-object v0, p2, LX/C7C;->c:LX/8pR;

    .line 1851068
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1851069
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1851053
    invoke-static {}, LX/1dS;->b()V

    .line 1851054
    iget v0, p1, LX/1dQ;->b:I

    .line 1851055
    packed-switch v0, :pswitch_data_0

    .line 1851056
    :goto_0
    return-object v2

    .line 1851057
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1851058
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1851059
    check-cast v1, LX/C7C;

    .line 1851060
    iget-object p1, p0, LX/C7E;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C7I;

    iget-object p2, v1, LX/C7C;->c:LX/8pR;

    .line 1851061
    invoke-virtual {p2, v0}, LX/8pR;->a(Landroid/view/View;)V

    .line 1851062
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa4f2351
        :pswitch_0
    .end packed-switch
.end method
