.class public abstract LX/AUE;
.super LX/0k9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "LX/0k9",
        "<TD;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Landroid/os/Handler;

.field public c:LX/AUD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUD",
            "<TD;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "@MainThread"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 1677827
    invoke-direct {p0, p1}, LX/0k9;-><init>(Landroid/content/Context;)V

    .line 1677828
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/AUE;->b:Landroid/os/Handler;

    .line 1677829
    iput-object p2, p0, LX/AUE;->a:Ljava/util/concurrent/ExecutorService;

    .line 1677830
    return-void
.end method

.method public static a$redex0(LX/AUE;LX/AUD;)V
    .locals 2
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUD",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 1677841
    iget-object v0, p0, LX/AUE;->c:LX/AUD;

    if-eq v0, p1, :cond_0

    .line 1677842
    iget-object v0, p1, LX/AUD;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/AUE;->a(Ljava/lang/Object;)V

    .line 1677843
    :goto_0
    return-void

    .line 1677844
    :cond_0
    iget-boolean v0, p0, LX/0k9;->q:Z

    move v0, v0

    .line 1677845
    if-eqz v0, :cond_1

    .line 1677846
    iget-object v0, p1, LX/AUD;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/AUE;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1677847
    :cond_1
    invoke-virtual {p0}, LX/0k9;->u()V

    .line 1677848
    const/4 v0, 0x0

    iput-object v0, p0, LX/AUE;->c:LX/AUD;

    .line 1677849
    :try_start_0
    iget-object v0, p1, LX/AUD;->a:Ljava/util/concurrent/Future;

    const v1, 0x10798f58

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1677850
    :goto_1
    iget-object v0, p1, LX/AUD;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/0k9;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 1677851
    :catch_0
    move-exception v0

    .line 1677852
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1677853
    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1677836
    invoke-super {p0}, LX/0k9;->a()V

    .line 1677837
    invoke-virtual {p0}, LX/AUE;->c()Z

    .line 1677838
    new-instance v0, LX/AUD;

    invoke-direct {v0}, LX/AUD;-><init>()V

    iput-object v0, p0, LX/AUE;->c:LX/AUD;

    .line 1677839
    iget-object v0, p0, LX/AUE;->c:LX/AUD;

    iget-object v1, p0, LX/AUE;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;

    iget-object v3, p0, LX/AUE;->c:LX/AUD;

    invoke-direct {v2, p0, v3}, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;-><init>(LX/AUE;LX/AUD;)V

    const v3, -0x2d583beb

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, v0, LX/AUD;->a:Ljava/util/concurrent/Future;

    .line 1677840
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 1677835
    return-void
.end method

.method public abstract b()Ljava/lang/Object;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method

.method public final c()Z
    .locals 2
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1677831
    iget-object v1, p0, LX/AUE;->c:LX/AUD;

    if-eqz v1, :cond_0

    .line 1677832
    iget-object v1, p0, LX/AUE;->c:LX/AUD;

    iget-object v1, v1, LX/AUD;->a:Ljava/util/concurrent/Future;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v0

    .line 1677833
    const/4 v1, 0x0

    iput-object v1, p0, LX/AUE;->c:LX/AUD;

    .line 1677834
    :cond_0
    return v0
.end method
