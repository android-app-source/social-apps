.class public LX/C4c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/23P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1847675
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1847676
    sput-object v0, LX/C4c;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847678
    iput-object p1, p0, LX/C4c;->b:LX/23P;

    .line 1847679
    return-void
.end method

.method public static a(LX/0QB;)LX/C4c;
    .locals 4

    .prologue
    .line 1847680
    const-class v1, LX/C4c;

    monitor-enter v1

    .line 1847681
    :try_start_0
    sget-object v0, LX/C4c;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847682
    sput-object v2, LX/C4c;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847683
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847684
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847685
    new-instance p0, LX/C4c;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, LX/C4c;-><init>(LX/23P;)V

    .line 1847686
    move-object v0, p0

    .line 1847687
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847688
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847689
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847690
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1847691
    if-eqz p0, :cond_0

    .line 1847692
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847693
    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 1847694
    :cond_1
    :goto_0
    return-object v0

    .line 1847695
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847696
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1847697
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_4

    :cond_3
    move-object v0, v1

    .line 1847698
    goto :goto_0

    .line 1847699
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 1847700
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1847701
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x790111ba

    if-eq v5, v6, :cond_1

    .line 1847702
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 1847703
    goto :goto_0
.end method
