.class public final enum LX/CMs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CMs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CMs;

.field public static final enum HIGH:LX/CMs;

.field public static final enum LOW:LX/CMs;

.field public static final enum UNKNOWN:LX/CMs;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1881051
    new-instance v0, LX/CMs;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/CMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMs;->UNKNOWN:LX/CMs;

    new-instance v0, LX/CMs;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3}, LX/CMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMs;->LOW:LX/CMs;

    new-instance v0, LX/CMs;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, LX/CMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMs;->HIGH:LX/CMs;

    .line 1881052
    const/4 v0, 0x3

    new-array v0, v0, [LX/CMs;

    sget-object v1, LX/CMs;->UNKNOWN:LX/CMs;

    aput-object v1, v0, v2

    sget-object v1, LX/CMs;->LOW:LX/CMs;

    aput-object v1, v0, v3

    sget-object v1, LX/CMs;->HIGH:LX/CMs;

    aput-object v1, v0, v4

    sput-object v0, LX/CMs;->$VALUES:[LX/CMs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1881050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CMs;
    .locals 1

    .prologue
    .line 1881048
    const-class v0, LX/CMs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CMs;

    return-object v0
.end method

.method public static values()[LX/CMs;
    .locals 1

    .prologue
    .line 1881049
    sget-object v0, LX/CMs;->$VALUES:[LX/CMs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CMs;

    return-object v0
.end method
