.class public final LX/BP6;
.super LX/BP5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;)V
    .locals 0

    .prologue
    .line 1780663
    iput-object p1, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    invoke-direct {p0}, LX/BP5;-><init>()V

    return-void
.end method

.method private a(LX/BPF;)V
    .locals 5

    .prologue
    .line 1780654
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    if-nez v0, :cond_0

    .line 1780655
    :goto_0
    return-void

    .line 1780656
    :cond_0
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->y:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->B:LX/0ad;

    sget-short v1, LX/AzO;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1780657
    iget-boolean v0, p1, LX/BPF;->a:Z

    if-eqz v0, :cond_1

    .line 1780658
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    iget-object v1, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v1, v1, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->B:LX/0ad;

    sget-char v2, LX/AzO;->b:C

    iget-object v3, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    invoke-virtual {v3}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0815bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 1780659
    :cond_1
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    iget-object v1, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v1, v1, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->B:LX/0ad;

    sget-char v2, LX/AzO;->c:C

    iget-object v3, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    invoke-virtual {v3}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0815bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 1780660
    :cond_2
    iget-boolean v0, p1, LX/BPF;->a:Z

    if-eqz v0, :cond_3

    .line 1780661
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    const v1, 0x7f0815bb

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0

    .line 1780662
    :cond_3
    iget-object v0, p0, LX/BP6;->a:Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    iget-object v0, v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    const v1, 0x7f0815bc

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1780653
    check-cast p1, LX/BPF;

    invoke-direct {p0, p1}, LX/BP6;->a(LX/BPF;)V

    return-void
.end method
