.class public final enum LX/BTF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTF;

.field public static final enum ENTRY_ACTION:LX/BTF;

.field public static final enum ENTRY_POINT:LX/BTF;

.field public static final enum SESSION_ID:LX/BTF;

.field public static final enum VIDEO_ITEM_IDENTIFIER:LX/BTF;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1787036
    new-instance v0, LX/BTF;

    const-string v1, "VIDEO_ITEM_IDENTIFIER"

    const-string v2, "asset_id"

    invoke-direct {v0, v1, v3, v2}, LX/BTF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    .line 1787037
    new-instance v0, LX/BTF;

    const-string v1, "SESSION_ID"

    const-string v2, "external_session_id"

    invoke-direct {v0, v1, v4, v2}, LX/BTF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTF;->SESSION_ID:LX/BTF;

    .line 1787038
    new-instance v0, LX/BTF;

    const-string v1, "ENTRY_POINT"

    const-string v2, "source"

    invoke-direct {v0, v1, v5, v2}, LX/BTF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTF;->ENTRY_POINT:LX/BTF;

    .line 1787039
    new-instance v0, LX/BTF;

    const-string v1, "ENTRY_ACTION"

    const-string v2, "entry_action"

    invoke-direct {v0, v1, v6, v2}, LX/BTF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTF;->ENTRY_ACTION:LX/BTF;

    .line 1787040
    const/4 v0, 0x4

    new-array v0, v0, [LX/BTF;

    sget-object v1, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    aput-object v1, v0, v3

    sget-object v1, LX/BTF;->SESSION_ID:LX/BTF;

    aput-object v1, v0, v4

    sget-object v1, LX/BTF;->ENTRY_POINT:LX/BTF;

    aput-object v1, v0, v5

    sget-object v1, LX/BTF;->ENTRY_ACTION:LX/BTF;

    aput-object v1, v0, v6

    sput-object v0, LX/BTF;->$VALUES:[LX/BTF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1787041
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1787042
    iput-object p3, p0, LX/BTF;->name:Ljava/lang/String;

    .line 1787043
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTF;
    .locals 1

    .prologue
    .line 1787044
    const-class v0, LX/BTF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTF;

    return-object v0
.end method

.method public static values()[LX/BTF;
    .locals 1

    .prologue
    .line 1787045
    sget-object v0, LX/BTF;->$VALUES:[LX/BTF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTF;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1787046
    iget-object v0, p0, LX/BTF;->name:Ljava/lang/String;

    return-object v0
.end method
