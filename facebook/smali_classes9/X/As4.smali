.class public LX/As4;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public l:LX/As1;

.field public m:LX/1po;

.field public n:Landroid/net/Uri;

.field public final synthetic o:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1720006
    iput-object p1, p0, LX/As4;->o:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    .line 1720007
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1720008
    new-instance v1, LX/As1;

    const v0, 0x7f0d0883

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v1, v0}, LX/As1;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    iput-object v1, p0, LX/As4;->l:LX/As1;

    .line 1720009
    iget-object v0, p0, LX/As4;->l:LX/As1;

    iget-object v0, v0, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1720010
    iget-object v1, p1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->d:Landroid/content/Context;

    iget v2, p1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    invoke-static {v1, v2}, LX/As2;->a(Landroid/content/Context;I)LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1720011
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/1po;)V
    .locals 2

    .prologue
    .line 1720012
    iput-object p1, p0, LX/As4;->n:Landroid/net/Uri;

    .line 1720013
    iget-object v0, p0, LX/As4;->o:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->e:LX/As2;

    iget-object v1, p0, LX/As4;->l:LX/As1;

    invoke-virtual {v0, v1, p1, p0}, LX/As2;->a(LX/As1;Landroid/net/Uri;Landroid/view/View$OnClickListener;)V

    .line 1720014
    iput-object p2, p0, LX/As4;->m:LX/1po;

    .line 1720015
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x269faf5f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1720016
    iget-object v1, p0, LX/As4;->n:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 1720017
    const v1, 0x25db2aa2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1720018
    :goto_0
    return-void

    .line 1720019
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/1a1;->a(Z)V

    .line 1720020
    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v1

    .line 1720021
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1720022
    iget-object v1, p0, LX/As4;->o:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->g:LX/As7;

    iget-object v2, p0, LX/As4;->n:Landroid/net/Uri;

    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v3

    .line 1720023
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    .line 1720024
    iget-object v5, v1, LX/As7;->a:LX/AsB;

    iget-object v5, v5, LX/AsB;->k:LX/74n;

    invoke-static {v5, v2}, LX/2rf;->a(LX/74n;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object p0

    move-object v5, v4

    .line 1720025
    check-cast v5, LX/0im;

    invoke-interface {v5}, LX/0im;->c()LX/0jJ;

    move-result-object v5

    const-class p1, LX/AsB;

    invoke-static {p1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object p1

    invoke-virtual {v5, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v5

    check-cast v5, LX/0jL;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iq;

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    sget-object p1, LX/875;->CAMERA_ROLL:LX/875;

    invoke-virtual {v4, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setMediaSource(LX/875;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-virtual {v4}, LX/0jL;->a()V

    .line 1720026
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->l:LX/ArL;

    .line 1720027
    sget-object v5, LX/ArH;->GALLERY_SELECT:LX/ArH;

    invoke-static {v4, v5}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const/4 p1, 0x1

    invoke-static {v4, p1}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object p1, LX/ArI;->MEDIA_INDEX:LX/ArI;

    invoke-virtual {p1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-static {v4, p0}, LX/ArL;->a(LX/ArL;Lcom/facebook/ipc/media/MediaItem;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-static {v4, v5}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1720028
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->m:LX/ArT;

    sget-object v5, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    invoke-virtual {v4, v5}, LX/ArT;->k(LX/ArJ;)V

    .line 1720029
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->m:LX/ArT;

    sget-object v5, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    invoke-virtual {v4, v5}, LX/ArT;->q(LX/ArJ;)V

    .line 1720030
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->m:LX/ArT;

    sget-object v5, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    invoke-virtual {v4, v5}, LX/ArT;->c(LX/ArJ;)V

    .line 1720031
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    check-cast v4, LX/0im;

    invoke-interface {v4}, LX/0im;->c()LX/0jJ;

    move-result-object v4

    sget-object v5, LX/AsB;->a:LX/0jK;

    invoke-virtual {v4, v5}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v5

    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iq;

    invoke-static {p0}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object p0

    invoke-virtual {p0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object p0

    sget-object p1, LX/875;->CAMERA_ROLL:LX/875;

    invoke-static {v5, v4, p0, p1}, LX/2rf;->a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;

    move-result-object v4

    invoke-virtual {v4}, LX/0jL;->a()V

    .line 1720032
    iget-object v4, v1, LX/As7;->a:LX/AsB;

    iget-object v4, v4, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    sget-object v5, LX/AsB;->a:LX/0jK;

    iget-object p0, v1, LX/As7;->a:LX/AsB;

    invoke-virtual {p0}, LX/AsB;->b()LX/86o;

    move-result-object p0

    invoke-static {v4, v5, p0}, LX/87N;->b(LX/0il;LX/0jK;LX/86o;)V

    .line 1720033
    :cond_1
    const v1, -0x34aff907    # -1.3633273E7f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
