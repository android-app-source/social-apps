.class public final LX/CAi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

.field public final synthetic c:LX/355;


# direct methods
.method public constructor <init>(LX/355;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLImageOverlay;)V
    .locals 0

    .prologue
    .line 1855622
    iput-object p1, p0, LX/CAi;->c:LX/355;

    iput-object p2, p0, LX/CAi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/CAi;->b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x2

    const v1, -0x1cb128ee

    invoke-static {v0, v7, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1855623
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1855624
    if-eqz v0, :cond_0

    .line 1855625
    iget-object v0, p0, LX/CAi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1855626
    new-instance v1, LX/89I;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->USER:LX/2rw;

    invoke-direct {v1, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    .line 1855627
    iput-object v3, v1, LX/89I;->d:Ljava/lang/String;

    .line 1855628
    move-object v1, v1

    .line 1855629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    .line 1855630
    iput-object v3, v1, LX/89I;->c:Ljava/lang/String;

    .line 1855631
    move-object v1, v1

    .line 1855632
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 1855633
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082a17

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1855634
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    const-string v4, "profilePictureOverlayCallToAction"

    invoke-static {v0, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v0, p0, LX/CAi;->c:LX/355;

    iget-object v0, v0, LX/355;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nq;

    invoke-static {v3}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1855635
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1855636
    iget-object v0, p0, LX/CAi;->c:LX/355;

    iget-object v0, v0, LX/355;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B4O;

    iget-object v1, p0, LX/CAi;->b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/CAi;->b:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const-string v5, "birthday_post_cta"

    invoke-virtual {v0, v4, v1, v5}, LX/B4O;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1855637
    iget-object v0, p0, LX/CAi;->c:LX/355;

    iget-object v0, v0, LX/355;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v4, v3, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1855638
    :cond_0
    const v0, -0x3b35a0f9

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1855639
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
