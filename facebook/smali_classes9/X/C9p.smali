.class public LX/C9p;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C9q;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C9p",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C9q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854564
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1854565
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C9p;->b:LX/0Zi;

    .line 1854566
    iput-object p1, p0, LX/C9p;->a:LX/0Ot;

    .line 1854567
    return-void
.end method

.method public static a(LX/0QB;)LX/C9p;
    .locals 4

    .prologue
    .line 1854592
    const-class v1, LX/C9p;

    monitor-enter v1

    .line 1854593
    :try_start_0
    sget-object v0, LX/C9p;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854594
    sput-object v2, LX/C9p;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854595
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854596
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854597
    new-instance v3, LX/C9p;

    const/16 p0, 0x206d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C9p;-><init>(LX/0Ot;)V

    .line 1854598
    move-object v0, v3

    .line 1854599
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854600
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854601
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1854590
    invoke-static {}, LX/1dS;->b()V

    .line 1854591
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1854587
    iget-object v0, p0, LX/C9p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1854588
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    move-object v0, v0

    .line 1854589
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 5

    .prologue
    .line 1854577
    check-cast p2, LX/C9o;

    .line 1854578
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1854579
    iget-object v0, p0, LX/C9p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C9q;

    iget-object v2, p2, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C9o;->b:LX/1Pq;

    .line 1854580
    iget-object v4, v0, LX/C9q;->a:LX/C9u;

    .line 1854581
    iget-object p0, v4, LX/C9u;->a:LX/16I;

    sget-object p1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryNetworkManagerComponentLogic$1;

    invoke-direct {v0, v4, v2, v3}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryNetworkManagerComponentLogic$1;-><init>(LX/C9u;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    invoke-virtual {p0, p1, v0}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object p0

    move-object p0, p0

    .line 1854582
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1854583
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1854584
    check-cast v0, LX/0Yb;

    iput-object v0, p2, LX/C9o;->c:LX/0Yb;

    .line 1854585
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1854586
    return-void
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1854574
    check-cast p3, LX/C9o;

    .line 1854575
    iget-object v0, p0, LX/C9p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1854576
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1854573
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1854569
    check-cast p3, LX/C9o;

    .line 1854570
    iget-object v0, p0, LX/C9p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/C9o;->c:LX/0Yb;

    .line 1854571
    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1854572
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1854568
    const/16 v0, 0xf

    return v0
.end method
