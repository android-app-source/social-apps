.class public final LX/AjE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/AjG;


# direct methods
.method public constructor <init>(LX/AjG;Lcom/facebook/graphql/model/GraphQLStory;LX/0TF;)V
    .locals 0

    .prologue
    .line 1707680
    iput-object p1, p0, LX/AjE;->c:LX/AjG;

    iput-object p2, p0, LX/AjE;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/AjE;->b:LX/0TF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1707681
    iget-object v0, p0, LX/AjE;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1707682
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1707683
    check-cast p1, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    const/4 v1, 0x0

    .line 1707684
    if-nez p1, :cond_1

    .line 1707685
    :cond_0
    :goto_0
    return-void

    .line 1707686
    :cond_1
    iget-object v0, p0, LX/AjE;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 1707687
    if-eqz v2, :cond_0

    .line 1707688
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v3

    .line 1707689
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1707690
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1707691
    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, LX/15i;->h(II)Z

    move-result v0

    .line 1707692
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_3

    .line 1707693
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1707694
    invoke-virtual {v5, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1707695
    :goto_2
    invoke-static {v2}, LX/17L;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;

    move-result-object v2

    .line 1707696
    iput-boolean v0, v2, LX/17L;->d:Z

    .line 1707697
    move-object v0, v2

    .line 1707698
    iput-object v1, v0, LX/17L;->c:Ljava/lang/String;

    .line 1707699
    move-object v0, v0

    .line 1707700
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 1707701
    invoke-virtual {p1}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->k()I

    move-result v2

    .line 1707702
    iget-object v0, p0, LX/AjE;->c:LX/AjG;

    iget-object v0, v0, LX/AjG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    iget-object v4, p0, LX/AjE;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1707703
    invoke-static {v4}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v6

    .line 1707704
    invoke-static {v4}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v7

    invoke-static {v6}, LX/4Z1;->a(Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)LX/4Z1;

    move-result-object v8

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1707705
    iput-object v6, v8, LX/4Z1;->c:LX/0Px;

    .line 1707706
    move-object v6, v8

    .line 1707707
    iput-object v1, v6, LX/4Z1;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1707708
    move-object v6, v6

    .line 1707709
    iput v2, v6, LX/4Z1;->e:I

    .line 1707710
    move-object v6, v6

    .line 1707711
    invoke-virtual {v6}, LX/4Z1;->a()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v6

    .line 1707712
    iput-object v6, v7, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 1707713
    move-object v6, v7

    .line 1707714
    iget-object v7, v0, LX/189;->i:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    .line 1707715
    iput-wide v8, v6, LX/23u;->G:J

    .line 1707716
    move-object v6, v6

    .line 1707717
    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    move-object v1, v6

    .line 1707718
    iget-object v0, p0, LX/AjE;->c:LX/AjG;

    iget-object v0, v0, LX/AjG;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2di;

    invoke-virtual {v0, v1}, LX/2di;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 1707719
    iget-object v0, p0, LX/AjE;->b:LX/0TF;

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 1707720
    goto/16 :goto_1

    .line 1707721
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method
