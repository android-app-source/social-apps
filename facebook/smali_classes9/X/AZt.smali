.class public LX/AZt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AZT;
.implements LX/AZU;


# static fields
.field private static final a:LX/0Tn;


# instance fields
.field public final b:LX/AVT;

.field public final c:LX/AMO;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0Sh;

.field private final f:Landroid/content/Context;

.field private final g:LX/AVP;

.field private final h:LX/AV1;

.field private final i:Landroid/support/v7/widget/RecyclerView;

.field public final j:LX/AZr;

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/AZq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/AZm;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1687636
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "facecast_mask_effect_id_used_"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AZt;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/AVP;LX/AV1;LX/AVT;LX/AMO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AVP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AV1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1687616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687617
    iput-object p1, p0, LX/AZt;->f:Landroid/content/Context;

    .line 1687618
    iput-object p2, p0, LX/AZt;->g:LX/AVP;

    .line 1687619
    iput-object p3, p0, LX/AZt;->h:LX/AV1;

    .line 1687620
    iput-object p4, p0, LX/AZt;->b:LX/AVT;

    .line 1687621
    iput-object p5, p0, LX/AZt;->c:LX/AMO;

    .line 1687622
    iput-object p6, p0, LX/AZt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1687623
    iput-object p7, p0, LX/AZt;->e:LX/0Sh;

    .line 1687624
    new-instance v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    .line 1687625
    new-instance v0, LX/1P1;

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 1687626
    invoke-virtual {v0, v4}, LX/1P1;->b(I)V

    .line 1687627
    iget-object v1, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1687628
    iget-object v0, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/AZs;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b05ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v2}, LX/AZs;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1687629
    new-instance v0, LX/AZr;

    invoke-direct {v0, p0}, LX/AZr;-><init>(LX/AZt;)V

    iput-object v0, p0, LX/AZt;->j:LX/AZr;

    .line 1687630
    iget-object v0, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/AZt;->j:LX/AZr;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1687631
    iget-object v0, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    .line 1687632
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 1687633
    iput-boolean v4, v0, LX/1Of;->g:Z

    .line 1687634
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/AZt;->k:Ljava/util/List;

    .line 1687635
    return-void
.end method

.method public static a(LX/AZt;LX/AZl;I)V
    .locals 3

    .prologue
    .line 1687608
    sget-object v0, LX/AZk;->DOWNLOADING:LX/AZk;

    .line 1687609
    iput-object v0, p1, LX/AZl;->g:LX/AZk;

    .line 1687610
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    invoke-virtual {v0, p2}, LX/1OM;->i_(I)V

    .line 1687611
    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 1687612
    iget-object v1, p0, LX/AZt;->k:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687613
    if-nez v0, :cond_0

    .line 1687614
    :goto_0
    return-void

    .line 1687615
    :cond_0
    invoke-direct {p0, p1, p2}, LX/AZt;->b(LX/AZl;I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/AZt;IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1687594
    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1687595
    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1687596
    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1687597
    iget-object v0, p0, LX/AZt;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1687598
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0, v1}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZl;

    .line 1687599
    if-eqz v0, :cond_0

    .line 1687600
    invoke-direct {p0, v0, v1}, LX/AZt;->b(LX/AZl;I)V

    .line 1687601
    :cond_0
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0, p1}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZl;

    .line 1687602
    if-eqz v0, :cond_1

    .line 1687603
    if-eqz p2, :cond_2

    sget-object v1, LX/AZk;->DOWNLOADED:LX/AZk;

    .line 1687604
    :goto_0
    iput-object v1, v0, LX/AZl;->g:LX/AZk;

    .line 1687605
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    invoke-virtual {v0, p1}, LX/1OM;->i_(I)V

    .line 1687606
    :cond_1
    return-void

    .line 1687607
    :cond_2
    sget-object v1, LX/AZk;->NOT_DOWNLOADED:LX/AZk;

    goto :goto_0
.end method

.method public static a$redex0(LX/AZt;LX/AZl;)V
    .locals 5

    .prologue
    .line 1687503
    iget-object v0, p1, LX/AZl;->k:LX/AN1;

    move-object v0, v0

    .line 1687504
    if-eqz v0, :cond_0

    .line 1687505
    iget-object v0, p1, LX/AZl;->l:LX/AN2;

    move-object v0, v0

    .line 1687506
    if-nez v0, :cond_1

    .line 1687507
    :cond_0
    :goto_0
    return-void

    .line 1687508
    :cond_1
    new-instance v0, LX/BA5;

    invoke-direct {v0}, LX/BA5;-><init>()V

    iget-object v1, p0, LX/AZt;->f:Landroid/content/Context;

    .line 1687509
    iput-object v1, v0, LX/BA5;->a:Landroid/content/Context;

    .line 1687510
    move-object v0, v0

    .line 1687511
    iget-object v1, p1, LX/AZl;->l:LX/AN2;

    move-object v1, v1

    .line 1687512
    iget-object v2, v1, LX/AN2;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1687513
    iget-object v2, p1, LX/AZl;->l:LX/AN2;

    move-object v2, v2

    .line 1687514
    iget-object v3, v2, LX/AN2;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1687515
    iget-object v3, p1, LX/AZl;->l:LX/AN2;

    move-object v3, v3

    .line 1687516
    iget-object v4, v3, LX/AN2;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1687517
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BA5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/BA5;

    move-result-object v0

    .line 1687518
    iget-object v1, p1, LX/AZl;->k:LX/AN1;

    move-object v1, v1

    .line 1687519
    iget-object v2, v1, LX/AN1;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1687520
    iput-object v1, v0, LX/BA5;->f:Ljava/lang/String;

    .line 1687521
    move-object v0, v0

    .line 1687522
    const-string v1, "0"

    .line 1687523
    iput-object v1, v0, LX/BA5;->m:Ljava/lang/String;

    .line 1687524
    move-object v0, v0

    .line 1687525
    iget-object v1, p0, LX/AZt;->h:LX/AV1;

    invoke-virtual {v1, v0}, LX/AV1;->a(LX/BA5;)V

    .line 1687526
    iget-object v1, p0, LX/AZt;->g:LX/AVP;

    invoke-virtual {v0}, LX/BA5;->a()LX/BA6;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AVP;->a(LX/BA6;)V

    .line 1687527
    iget-object v0, p0, LX/AZt;->l:LX/AZq;

    if-eqz v0, :cond_2

    .line 1687528
    iget-object v0, p0, LX/AZt;->l:LX/AZq;

    invoke-interface {v0}, LX/AZq;->b()V

    .line 1687529
    :cond_2
    iget-object v0, p0, LX/AZt;->b:LX/AVT;

    iget-object v1, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v1}, LX/AZm;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "maskApply"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1687530
    iget-object v3, p1, LX/AZl;->k:LX/AN1;

    move-object v3, v3

    .line 1687531
    iget-object v4, v3, LX/AN1;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1687532
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/AZl;->b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/Aa6;->a(LX/AVT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(LX/AZl;)LX/0Tn;
    .locals 2

    .prologue
    .line 1687593
    sget-object v0, LX/AZt;->a:LX/0Tn;

    iget-object v1, p0, LX/AZl;->b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method private b(LX/AZl;I)V
    .locals 6

    .prologue
    .line 1687589
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1687590
    new-instance v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskTrayController$1;

    invoke-direct {v1, p0, p2, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskTrayController$1;-><init>(LX/AZt;ILjava/util/concurrent/atomic/AtomicBoolean;)V

    .line 1687591
    iget-object v2, p0, LX/AZt;->c:LX/AMO;

    iget-object v3, p1, LX/AZl;->e:LX/0Px;

    new-instance v4, LX/AZn;

    invoke-direct {v4, p0, p1, p2}, LX/AZn;-><init>(LX/AZt;LX/AZl;I)V

    new-instance v5, LX/AZo;

    invoke-direct {v5, p0, p1, v0, v1}, LX/AZo;-><init>(LX/AZt;LX/AZl;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3, v4, v5}, LX/AMO;->a(LX/0Px;LX/AMN;LX/AZo;)V

    .line 1687592
    return-void
.end method

.method public static f(LX/AZt;)V
    .locals 2

    .prologue
    .line 1687637
    new-instance v0, LX/BA5;

    invoke-direct {v0}, LX/BA5;-><init>()V

    .line 1687638
    iget-object v1, p0, LX/AZt;->h:LX/AV1;

    invoke-virtual {v1, v0}, LX/AV1;->a(LX/BA5;)V

    .line 1687639
    iget-object v1, p0, LX/AZt;->g:LX/AVP;

    invoke-virtual {v0}, LX/BA5;->a()LX/BA6;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AVP;->a(LX/BA6;)V

    .line 1687640
    return-void
.end method


# virtual methods
.method public final a(LX/AZX;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1687560
    instance-of v0, p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    if-nez v0, :cond_1

    .line 1687561
    :cond_0
    :goto_0
    return-void

    .line 1687562
    :cond_1
    check-cast p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    .line 1687563
    iget-object v0, p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->d:LX/AZl;

    move-object v1, v0

    .line 1687564
    if-eqz v1, :cond_0

    .line 1687565
    iget-object v0, p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v2

    .line 1687566
    iget v0, p0, LX/AZt;->n:I

    if-eq v2, v0, :cond_0

    .line 1687567
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    iget v3, p0, LX/AZt;->n:I

    invoke-virtual {v0, v3}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZl;

    const/4 v3, 0x0

    .line 1687568
    iput-boolean v3, v0, LX/AZl;->i:Z

    .line 1687569
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    iget v3, p0, LX/AZt;->n:I

    invoke-virtual {v0, v3}, LX/1OM;->i_(I)V

    .line 1687570
    iput v2, p0, LX/AZt;->n:I

    .line 1687571
    iput-boolean v4, v1, LX/AZl;->i:Z

    .line 1687572
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    iget v3, p0, LX/AZt;->n:I

    invoke-virtual {v0, v3}, LX/1OM;->i_(I)V

    .line 1687573
    iget-boolean v0, v1, LX/AZl;->f:Z

    if-eqz v0, :cond_2

    .line 1687574
    invoke-static {p0}, LX/AZt;->f(LX/AZt;)V

    .line 1687575
    iget-object v0, p0, LX/AZt;->b:LX/AVT;

    iget-object v1, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v1}, LX/AZm;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "maskRemove"

    invoke-static {v0, v1, v2}, LX/Aa6;->c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1687576
    :cond_2
    sget-object v0, LX/AZp;->a:[I

    .line 1687577
    iget-object v3, v1, LX/AZl;->g:LX/AZk;

    move-object v3, v3

    .line 1687578
    invoke-virtual {v3}, LX/AZk;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1687579
    :goto_1
    :pswitch_0
    iget-boolean v0, v1, LX/AZl;->h:Z

    move v0, v0

    .line 1687580
    if-nez v0, :cond_0

    .line 1687581
    iput-boolean v4, v1, LX/AZl;->h:Z

    .line 1687582
    iget-object v0, p0, LX/AZt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {v1}, LX/AZt;->b(LX/AZl;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1687583
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    invoke-virtual {v0, v2}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 1687584
    :pswitch_1
    invoke-static {p0, v1, v2}, LX/AZt;->a(LX/AZt;LX/AZl;I)V

    goto :goto_1

    .line 1687585
    :pswitch_2
    iget-object v0, v1, LX/AZl;->k:LX/AN1;

    move-object v0, v0

    .line 1687586
    if-nez v0, :cond_3

    .line 1687587
    invoke-direct {p0, v1, v2}, LX/AZt;->b(LX/AZl;I)V

    goto :goto_1

    .line 1687588
    :cond_3
    invoke-static {p0, v1}, LX/AZt;->a$redex0(LX/AZt;LX/AZl;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/AZm;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1687537
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    if-ne p1, v0, :cond_0

    .line 1687538
    :goto_0
    return-void

    .line 1687539
    :cond_0
    iput-object p1, p0, LX/AZt;->m:LX/AZm;

    move v1, v2

    .line 1687540
    :goto_1
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0}, LX/AZZ;->e()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1687541
    iget-object v0, p0, LX/AZt;->m:LX/AZm;

    invoke-virtual {v0, v1}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZl;

    .line 1687542
    iget v3, p0, LX/AZt;->n:I

    if-ne v3, v1, :cond_1

    move v3, v4

    .line 1687543
    :goto_2
    iput-boolean v3, v0, LX/AZl;->i:Z

    .line 1687544
    iget-boolean v3, v0, LX/AZl;->f:Z

    if-eqz v3, :cond_2

    .line 1687545
    sget-object v3, LX/AZk;->DOWNLOADED:LX/AZk;

    .line 1687546
    iput-object v3, v0, LX/AZl;->g:LX/AZk;

    .line 1687547
    iput-boolean v4, v0, LX/AZl;->h:Z

    .line 1687548
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v3, v2

    .line 1687549
    goto :goto_2

    .line 1687550
    :cond_2
    iget-object v3, p0, LX/AZt;->c:LX/AMO;

    iget-object v5, v0, LX/AZl;->e:LX/0Px;

    invoke-virtual {v3, v5}, LX/AMO;->c(LX/0Px;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1687551
    sget-object v3, LX/AZk;->DOWNLOADED:LX/AZk;

    .line 1687552
    iput-object v3, v0, LX/AZl;->g:LX/AZk;

    .line 1687553
    :goto_4
    iget-object v3, p0, LX/AZt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/AZt;->b(LX/AZl;)LX/0Tn;

    move-result-object v5

    invoke-interface {v3, v5, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 1687554
    iput-boolean v3, v0, LX/AZl;->h:Z

    .line 1687555
    goto :goto_3

    .line 1687556
    :cond_3
    sget-object v3, LX/AZk;->NOT_DOWNLOADED:LX/AZk;

    .line 1687557
    iput-object v3, v0, LX/AZl;->g:LX/AZk;

    .line 1687558
    goto :goto_4

    .line 1687559
    :cond_4
    iget-object v0, p0, LX/AZt;->j:LX/AZr;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 0

    .prologue
    .line 1687536
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1687535
    iget-object v0, p0, LX/AZt;->i:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public final b(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 0

    .prologue
    .line 1687534
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1687533
    const/4 v0, 0x0

    return-object v0
.end method
