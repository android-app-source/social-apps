.class public LX/CbA;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/8Jj;


# static fields
.field private static final g:LX/8JG;


# instance fields
.field public final a:Lcom/facebook/photos/tagging/shared/BubbleLayout;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final c:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final d:Lcom/facebook/widget/PhotoButton;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final e:Landroid/widget/ImageView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public f:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:Z

.field private final i:Z

.field public final j:LX/Cau;

.field public k:LX/8Hs;

.field public l:LX/8Hs;

.field public m:LX/8Hs;

.field public n:LX/8Hm;

.field public o:LX/8Hm;

.field private p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public q:I

.field private r:I

.field public final s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1919221
    sget-object v0, LX/8JG;->UP:LX/8JG;

    sput-object v0, LX/CbA;->g:LX/8JG;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZLjava/lang/String;LX/Cau;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1919222
    invoke-direct {p0, p1, v1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1919223
    const-class v0, LX/CbA;

    invoke-static {v0, p0}, LX/CbA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1919224
    iput-boolean p6, p0, LX/CbA;->s:Z

    .line 1919225
    iput-boolean p2, p0, LX/CbA;->h:Z

    .line 1919226
    iput-boolean p3, p0, LX/CbA;->i:Z

    .line 1919227
    iput-object p5, p0, LX/CbA;->j:LX/Cau;

    .line 1919228
    iget-boolean v0, p0, LX/CbA;->s:Z

    if-eqz v0, :cond_0

    .line 1919229
    const v0, 0x7f030a90

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1919230
    const v0, 0x7f0d1b01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iput-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1919231
    const v0, 0x7f0d1b04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CbA;->e:Landroid/widget/ImageView;

    .line 1919232
    const v0, 0x7f0d1b05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/PhotoButton;

    iput-object v0, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    .line 1919233
    const v0, 0x7f0d1b03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CbA;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1919234
    const v0, 0x7f0d1b02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CbA;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1919235
    iget-object v0, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    iget-object v1, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {v1}, Lcom/facebook/widget/PhotoButton;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {v2}, Lcom/facebook/widget/PhotoButton;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1013

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v5, v1, v5, v2}, Lcom/facebook/widget/PhotoButton;->setPadding(IIII)V

    .line 1919236
    :goto_0
    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/CbA;->q:I

    .line 1919237
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    sget-object v1, LX/CbA;->g:LX/8JG;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setArrowDirection(LX/8JG;)V

    .line 1919238
    iget-object v0, p0, LX/CbA;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919239
    iget-object v0, p0, LX/CbA;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, LX/CbA;->g()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919240
    iget-object v0, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    .line 1919241
    new-instance v1, LX/Cb8;

    invoke-direct {v1, p0}, LX/Cb8;-><init>(LX/CbA;)V

    move-object v1, v1

    .line 1919242
    invoke-virtual {v0, v1}, Lcom/facebook/widget/PhotoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919243
    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/CbA;->r:I

    .line 1919244
    invoke-direct {p0}, LX/CbA;->e()V

    .line 1919245
    return-void

    .line 1919246
    :cond_0
    const v0, 0x7f030a8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1919247
    const v0, 0x7f0d1afd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iput-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1919248
    iput-object v1, p0, LX/CbA;->e:Landroid/widget/ImageView;

    .line 1919249
    const v0, 0x7f0d1b00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/PhotoButton;

    iput-object v0, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    .line 1919250
    const v0, 0x7f0d1aff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CbA;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1919251
    const v0, 0x7f0d1afe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CbA;->b:Lcom/facebook/resources/ui/FbTextView;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZZLjava/lang/String;Ljava/lang/String;LX/Cau;Z)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1919252
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, LX/CbA;-><init>(Landroid/content/Context;ZZLjava/lang/String;LX/Cau;Z)V

    .line 1919253
    iget-object v0, p0, LX/CbA;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919254
    iget-object v0, p0, LX/CbA;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1919255
    iget-object v0, p0, LX/CbA;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, LX/CbA;->g()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919256
    iget-boolean v0, p0, LX/CbA;->s:Z

    if-eqz v0, :cond_0

    .line 1919257
    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1919258
    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1919259
    iget-object v2, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {v2, v7, v7, v1, v7}, Lcom/facebook/widget/PhotoButton;->setPadding(IIII)V

    .line 1919260
    iget-object v2, p0, LX/CbA;->e:Landroid/widget/ImageView;

    add-int/2addr v1, v0

    invoke-virtual {v2, v0, v0, v1, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1919261
    invoke-virtual {p0}, LX/CbA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/CbA;->q:I

    .line 1919262
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->invalidate()V

    .line 1919263
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CbA;

    invoke-static {p0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object p0

    check-cast p0, LX/4mV;

    iput-object p0, p1, LX/CbA;->f:LX/4mV;

    return-void
.end method

.method private e()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 1919264
    iget-object v3, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    iget-boolean v0, p0, LX/CbA;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/CbA;->i:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/PhotoButton;->setVisibility(I)V

    .line 1919265
    iget-boolean v0, p0, LX/CbA;->s:Z

    if-eqz v0, :cond_0

    .line 1919266
    iget-object v0, p0, LX/CbA;->e:Landroid/widget/ImageView;

    iget-boolean v3, p0, LX/CbA;->h:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, LX/CbA;->i:Z

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1919267
    :cond_0
    iget-boolean v0, p0, LX/CbA;->h:Z

    if-nez v0, :cond_4

    .line 1919268
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 1919269
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1919270
    goto :goto_1

    .line 1919271
    :cond_4
    new-instance v0, LX/8Hs;

    const-wide/16 v2, 0xc8

    iget-object v5, p0, LX/CbA;->f:LX/4mV;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, LX/CbA;->m:LX/8Hs;

    .line 1919272
    new-instance v0, LX/8Hs;

    iget-object v1, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    iget-object v5, p0, LX/CbA;->f:LX/4mV;

    move-wide v2, v6

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, LX/CbA;->k:LX/8Hs;

    .line 1919273
    iget-boolean v0, p0, LX/CbA;->s:Z

    if-eqz v0, :cond_1

    .line 1919274
    new-instance v0, LX/8Hs;

    iget-object v1, p0, LX/CbA;->e:Landroid/widget/ImageView;

    iget-object v5, p0, LX/CbA;->f:LX/4mV;

    move-wide v2, v6

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, LX/CbA;->l:LX/8Hs;

    goto :goto_2
.end method

.method private g()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1919217
    new-instance v0, LX/Cb9;

    invoke-direct {v0, p0}, LX/Cb9;-><init>(LX/CbA;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 1919218
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 1919219
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    const/high16 v1, 0x3f000000    # 0.5f

    int-to-float v2, p1

    iget-object v3, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setArrowPosition(F)V

    .line 1919220
    :cond_0
    return-void
.end method

.method public final a(LX/8JG;LX/8Jh;)V
    .locals 3

    .prologue
    .line 1919210
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(LX/8JG;LX/8Jh;)V

    .line 1919211
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, LX/CbA;->r:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1919212
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/CbA;->r:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1919213
    iget-boolean v0, p0, LX/CbA;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {v0}, Lcom/facebook/widget/PhotoButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1919214
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/CbA;->q:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1919215
    :cond_0
    iget-object v0, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1919216
    return-void
.end method

.method public getArrowDirection()LX/8JG;
    .locals 1

    .prologue
    .line 1919207
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1919208
    iget-object p0, v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    move-object v0, p0

    .line 1919209
    return-object v0
.end method

.method public getArrowLength()I
    .locals 1

    .prologue
    .line 1919204
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1919205
    iget p0, v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    move v0, p0

    .line 1919206
    float-to-int v0, v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x34324935

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919198
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1919199
    iget-boolean v1, p0, LX/CbA;->h:Z

    if-nez v1, :cond_0

    .line 1919200
    const/16 v1, 0x2d

    const v2, -0x9acb465

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1919201
    :goto_0
    return-void

    .line 1919202
    :cond_0
    new-instance v1, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;-><init>(LX/CbA;)V

    invoke-static {p0, v1}, LX/8He;->b(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    iput-object v1, p0, LX/CbA;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1919203
    const v1, 0x11079edb

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5ef8dc55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919193
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1919194
    iget-object v1, p0, LX/CbA;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 1919195
    iget-object v1, p0, LX/CbA;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {p0, v1}, LX/8He;->b(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1919196
    const/4 v1, 0x0

    iput-object v1, p0, LX/CbA;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1919197
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x3227b3f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setArrowDirection(LX/8JG;)V
    .locals 1

    .prologue
    .line 1919191
    iget-object v0, p0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setArrowDirection(LX/8JG;)V

    .line 1919192
    return-void
.end method
