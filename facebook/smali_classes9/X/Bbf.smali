.class public final LX/Bbf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 0

    .prologue
    .line 1801008
    iput-object p1, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1801000
    iget-object v0, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1801001
    if-nez v0, :cond_0

    .line 1801002
    iget-object v0, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->w:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": null token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Returned by getItem("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), row id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801003
    :goto_0
    return-void

    .line 1801004
    :cond_0
    iget-object v1, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    .line 1801005
    invoke-static {v1, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a$redex0(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1801006
    iget-object v0, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->m(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801007
    iget-object v0, p0, LX/Bbf;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->o(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    goto :goto_0
.end method
