.class public final LX/AoI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1713915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1713916
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1713917
    :goto_0
    return v1

    .line 1713918
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1713919
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1713920
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1713921
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1713922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1713923
    const-string v5, "action_links"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1713924
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1713925
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 1713926
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 1713927
    invoke-static {p0, p1}, LX/AoG;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1713928
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1713929
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1713930
    goto :goto_1

    .line 1713931
    :cond_3
    const-string v5, "style_list"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1713932
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1713933
    :cond_4
    const-string v5, "target"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1713934
    invoke-static {p0, p1}, LX/AoH;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1713935
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1713936
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1713937
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1713938
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1713939
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1713940
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1713941
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1713942
    if-eqz v0, :cond_1

    .line 1713943
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713944
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1713945
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1713946
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/AoG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1713947
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1713948
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1713949
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1713950
    if-eqz v0, :cond_2

    .line 1713951
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713952
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1713953
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1713954
    if-eqz v0, :cond_3

    .line 1713955
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713956
    invoke-static {p0, v0, p2}, LX/AoH;->a(LX/15i;ILX/0nX;)V

    .line 1713957
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1713958
    return-void
.end method
