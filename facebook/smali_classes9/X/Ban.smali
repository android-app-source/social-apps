.class public LX/Ban;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/63R;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:LX/Bam;

.field public e:Lcom/facebook/widget/CustomLinearLayout;

.field public f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

.field public g:Landroid/view/ViewStub;

.field public h:Landroid/view/ViewStub;

.field public i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

.field public j:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileGridView;",
            ">;"
        }
    .end annotation
.end field

.field public k:Landroid/view/View;

.field public l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

.field private final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/graphics/Paint;

.field public o:F

.field public p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1799617
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1799618
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    .line 1799619
    sget-object v0, LX/Bam;->STANDARD:LX/Bam;

    iput-object v0, p0, LX/Ban;->d:LX/Bam;

    .line 1799620
    invoke-direct {p0}, LX/Ban;->b()V

    .line 1799621
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1799622
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799623
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    .line 1799624
    sget-object v0, LX/Bam;->STANDARD:LX/Bam;

    iput-object v0, p0, LX/Ban;->d:LX/Bam;

    .line 1799625
    invoke-direct {p0}, LX/Ban;->b()V

    .line 1799626
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1799627
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1799628
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    .line 1799629
    sget-object v0, LX/Bam;->STANDARD:LX/Bam;

    iput-object v0, p0, LX/Ban;->d:LX/Bam;

    .line 1799630
    invoke-direct {p0}, LX/Ban;->b()V

    .line 1799631
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ban;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, LX/Ban;->a:LX/0hB;

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1799632
    const-class v0, LX/Ban;

    invoke-static {v0, p0}, LX/Ban;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1799633
    invoke-virtual {p0, v3}, LX/Ban;->setWillNotDraw(Z)V

    .line 1799634
    const v0, 0x7f0313b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1799635
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/Ban;->n:Landroid/graphics/Paint;

    .line 1799636
    iget-object v0, p0, LX/Ban;->n:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1799637
    const v0, 0x7f0d2d78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ban;->k:Landroid/view/View;

    .line 1799638
    const v0, 0x7f0d2d70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iput-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    .line 1799639
    const v0, 0x7f0d2d7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    iput-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1799640
    const v0, 0x7f0d2d6e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    .line 1799641
    const v0, 0x7f0d2d73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iput-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    .line 1799642
    const v0, 0x7f0d2d77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Ban;->g:Landroid/view/ViewStub;

    .line 1799643
    const v0, 0x7f0d2d76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Ban;->h:Landroid/view/ViewStub;

    .line 1799644
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2d6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Ban;->j:LX/0zw;

    .line 1799645
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/Ban;->b:I

    .line 1799646
    invoke-virtual {p0}, LX/Ban;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    const v2, 0x7f0102bd

    aput v2, v1, v3

    const v2, 0x7f0102b8

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1799647
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1799648
    iget-object v1, p0, LX/Ban;->m:Ljava/util/ArrayList;

    iget-object v2, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1799649
    iget-object v1, p0, LX/Ban;->m:Ljava/util/ArrayList;

    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1799650
    :cond_0
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/Ban;->p:I

    .line 1799651
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1799652
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 2

    .prologue
    .line 1799653
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0878

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 1799654
    const/high16 v0, 0x3f800000    # 1.0f

    const v3, 0x3e4ccccd    # 0.2f

    .line 1799655
    cmpg-float v1, p1, v3

    if-gez v1, :cond_1

    .line 1799656
    :goto_0
    move v2, v0

    .line 1799657
    iget-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1799658
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1799659
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1799660
    :cond_0
    return-void

    .line 1799661
    :cond_1
    cmpl-float v1, p1, v3

    if-ltz v1, :cond_2

    iget v1, p0, LX/Ban;->o:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_2

    .line 1799662
    sub-float v1, p1, v3

    iget v2, p0, LX/Ban;->o:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0

    .line 1799663
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1799664
    iget-object v0, p0, LX/Ban;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1799665
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1799666
    const/4 v0, 0x1

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1799667
    invoke-virtual {p0}, LX/Ban;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, LX/Ban;->c:I

    if-le v0, v1, :cond_0

    .line 1799668
    const/4 v1, 0x0

    iget v0, p0, LX/Ban;->c:I

    int-to-float v2, v0

    invoke-virtual {p0}, LX/Ban;->getMeasuredWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, LX/Ban;->getMeasuredHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, LX/Ban;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1799669
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1799670
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1799600
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setVisibility(I)V

    .line 1799601
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1799602
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 1799603
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0881

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1799604
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0881

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1799605
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0881

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1799606
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 1799607
    sget-object v0, LX/Bal;->b:[I

    iget-object v1, p0, LX/Ban;->d:LX/Bam;

    invoke-virtual {v1}, LX/Bam;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1799608
    :goto_0
    return-void

    .line 1799609
    :pswitch_0
    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v0

    iget v1, p0, LX/Ban;->b:I

    .line 1799610
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1799611
    const v2, 0x3faa9fbe    # 1.333f

    .line 1799612
    :goto_1
    int-to-float v3, v0

    div-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move v0, v2

    .line 1799613
    invoke-virtual {p0, v0}, LX/Ban;->setCoverHeight(I)V

    goto :goto_0

    .line 1799614
    :pswitch_1
    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v0

    iget v1, p0, LX/Ban;->b:I

    invoke-static {v0, v1}, LX/Baq;->b(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Ban;->setCoverHeight(I)V

    goto :goto_0

    .line 1799615
    :pswitch_2
    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v0

    iget v1, p0, LX/Ban;->b:I

    invoke-virtual {p0, v0, v1}, LX/Ban;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Ban;->setCoverHeight(I)V

    goto :goto_0

    .line 1799616
    :cond_0
    const v2, 0x402ced91    # 2.702f

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCoverPhotoHeight()I
    .locals 1

    .prologue
    .line 1799564
    iget v0, p0, LX/Ban;->c:I

    return v0
.end method

.method public getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;
    .locals 1

    .prologue
    .line 1799568
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    return-object v0
.end method

.method public getFadingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1799569
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    return-object v0
.end method

.method public final getLazyProfileVideoIcon()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799565
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1799566
    iget-object p0, v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    move-object v0, p0

    .line 1799567
    return-object v0
.end method

.method public final getLazyProfileVideoView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799570
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1799571
    iget-object p0, v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->c:LX/0zw;

    move-object v0, p0

    .line 1799572
    return-object v0
.end method

.method public final getProfileEditIconViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1799573
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1799574
    iget-object p0, v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->e:Landroid/view/ViewStub;

    move-object v0, p0

    .line 1799575
    return-object v0
.end method

.method public final getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;
    .locals 1

    .prologue
    .line 1799576
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1799577
    iget-object p0, v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-object v0, p0

    .line 1799578
    return-object v0
.end method

.method public getProfileVideoView()Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1799579
    invoke-virtual {p0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    .line 1799580
    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScreenWidth()I
    .locals 1

    .prologue
    .line 1799581
    iget-object v0, p0, LX/Ban;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    return v0
.end method

.method public setCoverHeight(I)V
    .locals 2

    .prologue
    .line 1799582
    iput p1, p0, LX/Ban;->c:I

    .line 1799583
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1799584
    iget p1, p0, LX/Ban;->c:I

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1799585
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1799586
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0429

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1799587
    iget v1, p0, LX/Ban;->p:I

    div-int/lit8 v1, v1, 0x2

    .line 1799588
    const/high16 p1, 0x3f800000    # 1.0f

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/Ban;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    sub-float v0, p1, v0

    iput v0, p0, LX/Ban;->o:F

    .line 1799589
    return-void
.end method

.method public setCoverType(LX/Bap;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1799590
    sget-object v0, LX/Bal;->a:[I

    invoke-virtual {p1}, LX/Bap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1799591
    :cond_0
    :goto_0
    return-void

    .line 1799592
    :pswitch_0
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0, v3}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setVisibility(I)V

    .line 1799593
    iget-object v0, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799594
    iget-object v0, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setVisibility(I)V

    goto :goto_0

    .line 1799595
    :pswitch_1
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setVisibility(I)V

    .line 1799596
    iget-object v0, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileGridView;->setVisibility(I)V

    goto :goto_0

    .line 1799597
    :pswitch_2
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setVisibility(I)V

    .line 1799598
    iget-object v0, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799599
    iget-object v0, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
