.class public final LX/AiU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;II)V
    .locals 0

    .prologue
    .line 1706252
    iput-object p1, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iput p2, p0, LX/AiU;->a:I

    iput p3, p0, LX/AiU;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6a029f87

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706253
    iget-object v1, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iget-boolean v1, v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->g:Z

    if-eqz v1, :cond_0

    .line 1706254
    iget-object v1, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    const/4 v2, 0x0

    .line 1706255
    iput-boolean v2, v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->g:Z

    .line 1706256
    iget-object v1, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iget-object v1, v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    .line 1706257
    iget-wide v5, v1, LX/Aiv;->j:J

    iget-object v7, v1, LX/Aiv;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    add-long/2addr v5, v7

    iget-wide v7, v1, LX/Aiv;->i:J

    sub-long/2addr v5, v7

    iput-wide v5, v1, LX/Aiv;->e:J

    .line 1706258
    :cond_0
    iget-object v1, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iget-object v1, v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    iget v2, p0, LX/AiU;->a:I

    .line 1706259
    iget-object v3, v1, LX/Aiv;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1706260
    iget v3, v1, LX/Aiv;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/Aiv;->d:I

    .line 1706261
    :cond_1
    iget-object v3, v1, LX/Aiv;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1706262
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iget-object v2, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "target_fragment"

    iget v3, p0, LX/AiU;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "session_id"

    iget-object v3, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    iget-object v3, v3, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1706263
    iget-object v2, p0, LX/AiU;->c:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 1706264
    const v1, 0x13b6a6e9

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
