.class public final LX/Aok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7ll;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/Aol;


# direct methods
.method public constructor <init>(LX/Aol;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1714746
    iput-object p1, p0, LX/Aok;->b:LX/Aol;

    iput-object p2, p0, LX/Aok;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 1714721
    return-void
.end method

.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 8

    .prologue
    .line 1714738
    iget-object v0, p0, LX/Aok;->b:LX/Aol;

    iget-object v0, v0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aok;->b:LX/Aol;

    iget-object v1, v1, LX/Aol;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Aok;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aok;->b:LX/Aol;

    iget-object v3, v3, LX/Aol;->i:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714739
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714740
    iget-object v4, p0, LX/Aok;->b:LX/Aol;

    iget-object v4, v4, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aok;->b:LX/Aol;

    iget-object v5, v5, LX/Aol;->e:Ljava/lang/String;

    .line 1714741
    iget-object v6, v0, LX/1EQ;->a:LX/0Zb;

    const-string v7, "feed_share_action"

    const/4 p0, 0x1

    invoke-interface {v6, v7, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1714742
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1714743
    invoke-virtual {v6, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string p0, "share_type"

    const-string p1, "quick_share_publish_start"

    invoke-virtual {v7, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string p0, "story_id"

    invoke-virtual {v7, p0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string p0, "user_id"

    invoke-virtual {v7, p0, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string p0, "shareable_id"

    invoke-virtual {v7, p0, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714744
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 1714745
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 8

    .prologue
    .line 1714730
    iget-object v0, p0, LX/Aok;->b:LX/Aol;

    iget-object v0, v0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aok;->b:LX/Aol;

    iget-object v1, v1, LX/Aol;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Aok;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aok;->b:LX/Aol;

    iget-object v3, v3, LX/Aol;->i:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714731
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714732
    iget-object v4, p0, LX/Aok;->b:LX/Aol;

    iget-object v4, v4, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aok;->b:LX/Aol;

    iget-object v5, v5, LX/Aol;->e:Ljava/lang/String;

    iget-object v6, p0, LX/Aok;->b:LX/Aol;

    iget-object v6, v6, LX/Aol;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v6}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 1714733
    iget-object v7, v0, LX/1EQ;->a:LX/0Zb;

    const-string p0, "feed_share_action"

    const/4 p1, 0x1

    invoke-interface {v7, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 1714734
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1714735
    invoke-virtual {v7, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "share_type"

    const-string p2, "quick_share_publish_success"

    invoke-virtual {p0, p1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "story_id"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "user_id"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "shareable_id"

    invoke-virtual {p0, p1, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "tracking"

    invoke-virtual {p0, p1, v6}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714736
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 1714737
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1714722
    iget-object v0, p0, LX/Aok;->b:LX/Aol;

    iget-object v0, v0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aok;->b:LX/Aol;

    iget-object v1, v1, LX/Aol;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Aok;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aok;->b:LX/Aol;

    iget-object v3, v3, LX/Aol;->i:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714723
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714724
    iget-object v4, p0, LX/Aok;->b:LX/Aol;

    iget-object v4, v4, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aok;->b:LX/Aol;

    iget-object v5, v5, LX/Aol;->e:Ljava/lang/String;

    iget-object v6, p0, LX/Aok;->b:LX/Aol;

    iget-object v6, v6, LX/Aol;->i:LX/Aov;

    iget-object v6, v6, LX/Aov;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1CW;

    invoke-virtual {v6, p3, v7, v7}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v6

    .line 1714725
    iget-object v7, v0, LX/1EQ;->a:LX/0Zb;

    const-string p0, "feed_share_action"

    const/4 p1, 0x1

    invoke-interface {v7, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 1714726
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1714727
    invoke-virtual {v7, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "share_type"

    const-string p2, "quick_share_publish_failure"

    invoke-virtual {p0, p1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "story_id"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "user_id"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "shareable_id"

    invoke-virtual {p0, p1, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "error_msg"

    invoke-virtual {p0, p1, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714728
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 1714729
    :cond_0
    return-void
.end method
