.class public LX/Be6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0uf;


# direct methods
.method public constructor <init>(LX/0uf;)V
    .locals 0

    .prologue
    .line 1804580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804581
    iput-object p1, p0, LX/Be6;->a:LX/0uf;

    .line 1804582
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1804583
    const-string v0, "Contextual"

    const-string v1, "Validating alpha: %s , beta: %s , gamma: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 1804584
    iget-object v5, p0, LX/Be6;->a:LX/0uf;

    sget-wide v7, LX/0X5;->iE:J

    invoke-virtual {v5, v7, v8}, LX/0uf;->a(J)LX/1jr;

    move-result-object v5

    .line 1804585
    const/4 v6, 0x0

    const-string v7, "alpha_hard_coded"

    invoke-virtual {v5, v6, v7}, LX/1jr;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 1804586
    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1804587
    iget-object v5, p0, LX/Be6;->a:LX/0uf;

    sget-wide v7, LX/0X5;->iF:J

    invoke-virtual {v5, v7, v8}, LX/0uf;->a(J)LX/1jr;

    move-result-object v5

    .line 1804588
    const/4 v6, 0x0

    const-string v7, "beta_hard_coded"

    invoke-virtual {v5, v6, v7}, LX/1jr;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 1804589
    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 1804590
    iget-object v5, p0, LX/Be6;->a:LX/0uf;

    sget-wide v7, LX/0X5;->iG:J

    invoke-virtual {v5, v7, v8}, LX/0uf;->a(J)LX/1jr;

    move-result-object v5

    .line 1804591
    const/4 v6, 0x0

    const-string v7, "gamma_hard_coded"

    invoke-virtual {v5, v6, v7}, LX/1jr;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 1804592
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1804593
    return-void
.end method
