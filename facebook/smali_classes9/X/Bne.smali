.class public LX/Bne;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0wM;

.field private final d:LX/BiT;

.field public final e:LX/Bni;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1821081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Bne;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/Bni;Landroid/content/Context;LX/0wM;LX/BiT;)V
    .locals 0
    .param p1    # LX/Bni;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1821064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821065
    iput-object p2, p0, LX/Bne;->b:Landroid/content/Context;

    .line 1821066
    iput-object p3, p0, LX/Bne;->c:LX/0wM;

    .line 1821067
    iput-object p1, p0, LX/Bne;->e:LX/Bni;

    .line 1821068
    iput-object p4, p0, LX/Bne;->d:LX/BiT;

    .line 1821069
    return-void
.end method

.method public static synthetic a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/BiU;Z)I
    .locals 1

    .prologue
    .line 1821070
    sget-object v0, LX/BnX;->a:[I

    invoke-virtual {p1}, LX/BiU;->ordinal()I

    move-result p2

    aget v0, v0, p2

    packed-switch v0, :pswitch_data_0

    .line 1821071
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 1821072
    return v0

    .line 1821073
    :pswitch_0
    const v0, 0x7f08129e

    goto :goto_0

    .line 1821074
    :pswitch_1
    const v0, 0x7f08129b

    goto :goto_0

    .line 1821075
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p0, v0, :cond_0

    .line 1821076
    const v0, 0x7f0812a1

    goto :goto_0

    .line 1821077
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p0, v0, :cond_1

    .line 1821078
    const v0, 0x7f0812a2

    goto :goto_0

    .line 1821079
    :cond_1
    const v0, 0x7f0812a0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(IZ)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1821080
    if-eqz p2, :cond_0

    invoke-static {p0, p1}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/Bne;->b(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/Bne;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1821082
    iget-object v0, p0, LX/Bne;->c:LX/0wM;

    const v1, -0xa76f01

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z
    .locals 3
    .param p1    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1821054
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p0, v2, :cond_2

    .line 1821055
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq p1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 1821056
    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {p2}, LX/Bne;->j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/Bne;LX/BiU;Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 1821083
    sget-object v0, LX/BnX;->a:[I

    invoke-virtual {p1}, LX/BiU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1821084
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rsvp type for icons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1821085
    :pswitch_0
    const v0, 0x7f02085a

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1821086
    :goto_0
    return-object v0

    .line 1821087
    :pswitch_1
    const v0, 0x7f020857

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1821088
    :pswitch_2
    const v0, 0x7f020818

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 1821057
    sget-object v0, LX/BnX;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1821058
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rsvp type for icons: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1821059
    :pswitch_0
    const v0, 0x7f020857

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1821060
    :goto_0
    return-object v0

    .line 1821061
    :pswitch_1
    const v0, 0x7f02085c

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1821062
    :pswitch_2
    const v0, 0x7f02085e

    invoke-direct {p0, v0, p2}, LX/Bne;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/Bne;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1821063
    iget-object v0, p0, LX/Bne;->c:LX/0wM;

    const v1, -0x6e685d

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I
    .locals 1
    .param p1    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1820969
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p0, v0, :cond_0

    .line 1820970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_1

    const v0, 0x7f08129b

    :goto_0
    move v0, v0

    .line 1820971
    :goto_1
    return v0

    .line 1820972
    :cond_0
    invoke-static {p1}, LX/Bne;->j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1820973
    const v0, 0x7f08129b

    .line 1820974
    :goto_2
    move v0, v0

    .line 1820975
    goto :goto_1

    :cond_1
    const v0, 0x7f08129e

    goto :goto_0

    .line 1820976
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_3

    .line 1820977
    const v0, 0x7f08129c

    goto :goto_2

    .line 1820978
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_4

    .line 1820979
    const v0, 0x7f08129d

    goto :goto_2

    .line 1820980
    :cond_4
    const v0, 0x7f08129f

    goto :goto_2
.end method

.method private c(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;
    .locals 5

    .prologue
    .line 1820981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v0, :cond_1

    .line 1820982
    :cond_0
    new-instance v0, LX/Bnd;

    invoke-static {}, LX/BiT;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, LX/Bnd;-><init>(LX/Bne;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 1820983
    :goto_0
    new-instance v1, LX/BnW;

    .line 1820984
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v2, :cond_2

    .line 1820985
    const v2, 0x7f020643

    invoke-static {p0, v2}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1820986
    :goto_1
    move-object v2, v2

    .line 1820987
    iget-object v3, p0, LX/Bne;->b:Landroid/content/Context;

    .line 1820988
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v4, :cond_5

    .line 1820989
    const v4, 0x7f0812a5

    .line 1820990
    :goto_2
    move v4, v4

    .line 1820991
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1820992
    invoke-direct {v1, v2, v3, v0}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v1

    .line 1820993
    :cond_1
    new-instance v0, LX/Bnb;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {v0, p0, p1, v1}, LX/Bnb;-><init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0

    .line 1820994
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v2, :cond_3

    .line 1820995
    const v2, 0x7f020642

    invoke-static {p0, v2}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 1820996
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v2, :cond_4

    .line 1820997
    const v2, 0x7f020641

    invoke-static {p0, v2}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 1820998
    :cond_4
    const v2, 0x7f02085a

    invoke-static {p0, v2}, LX/Bne;->b(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    .line 1820999
    :cond_5
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v4, :cond_6

    .line 1821000
    const v4, 0x7f0812a8

    goto :goto_2

    .line 1821001
    :cond_6
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p1, v4, :cond_7

    .line 1821002
    const v4, 0x7f0812a7

    goto :goto_2

    .line 1821003
    :cond_7
    const v4, 0x7f0812a4

    goto :goto_2
.end method

.method public static f(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # LX/Bne;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821004
    invoke-static {p1}, LX/Bne;->j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821005
    const v0, 0x7f020857

    invoke-static {p0, v0}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1821006
    :goto_0
    return-object v0

    .line 1821007
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_1

    .line 1821008
    const v0, 0x7f02085c

    invoke-static {p0, v0}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1821009
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_2

    .line 1821010
    const v0, 0x7f02085e

    invoke-static {p0, v0}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1821011
    :cond_2
    const v0, 0x7f020855

    invoke-static {p0, v0}, LX/Bne;->b(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 2
    .param p0    # LX/Bne;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821012
    iget-object v0, p0, LX/Bne;->b:Landroid/content/Context;

    .line 1821013
    invoke-static {p1}, LX/Bne;->j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1821014
    const v1, 0x7f0812a5

    .line 1821015
    :goto_0
    move v1, v1

    .line 1821016
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1821017
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v1, :cond_1

    .line 1821018
    const v1, 0x7f0812a6

    goto :goto_0

    .line 1821019
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v1, :cond_2

    .line 1821020
    const v1, 0x7f0812a7

    goto :goto_0

    .line 1821021
    :cond_2
    const v1, 0x7f0812a3

    goto :goto_0
.end method

.method public static j(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821022
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;
    .locals 4
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821023
    const/4 v0, 0x1

    move v0, v0

    .line 1821024
    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p1, v0, :cond_0

    .line 1821025
    invoke-direct {p0, p3}, LX/Bne;->c(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    .line 1821026
    :goto_0
    return-object v0

    .line 1821027
    :cond_0
    sget-object v0, LX/Bne;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821028
    new-instance v0, LX/BnW;

    invoke-static {p0, p2}, LX/Bne;->f(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, p2}, LX/Bne;->g(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Bna;

    invoke-direct {v3, p0, p2}, LX/Bna;-><init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1821029
    :goto_1
    move-object v0, v0

    .line 1821030
    goto :goto_0

    :cond_1
    new-instance v0, LX/BnW;

    invoke-static {p0, p2}, LX/Bne;->f(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, p2}, LX/Bne;->g(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/BnY;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {v3, p0, p2, p1}, LX/BnY;-><init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;
    .locals 2
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821031
    const/4 v0, 0x1

    move v0, v0

    .line 1821032
    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p1, v0, :cond_0

    .line 1821033
    invoke-direct {p0, p3}, LX/Bne;->c(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    .line 1821034
    :goto_0
    return-object v0

    .line 1821035
    :cond_0
    new-instance v0, LX/BnW;

    .line 1821036
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v1, :cond_1

    .line 1821037
    const v1, 0x7f020642

    invoke-static {p0, v1}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1821038
    :goto_1
    move-object v1, v1

    .line 1821039
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, p1, :cond_4

    .line 1821040
    iget-object p1, p0, LX/Bne;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0812a9

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1821041
    :goto_2
    move-object p1, p1

    .line 1821042
    new-instance p3, LX/Bna;

    invoke-direct {p3, p0, p2}, LX/Bna;-><init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    invoke-direct {v0, v1, p1, p3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    move-object v0, v0

    .line 1821043
    goto :goto_0

    .line 1821044
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v1, :cond_2

    .line 1821045
    const v1, 0x7f020644

    invoke-static {p0, v1}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 1821046
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v1, :cond_3

    .line 1821047
    const v1, 0x7f020641

    invoke-static {p0, v1}, LX/Bne;->a(LX/Bne;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 1821048
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1821049
    :cond_4
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, p1, :cond_5

    .line 1821050
    iget-object p1, p0, LX/Bne;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0812aa

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 1821051
    :cond_5
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, p1, :cond_6

    .line 1821052
    iget-object p1, p0, LX/Bne;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0812ab

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 1821053
    :cond_6
    const/4 p1, 0x0

    goto :goto_2
.end method
