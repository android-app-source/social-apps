.class public LX/BsU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/5KP;

.field public final b:LX/0dk;


# direct methods
.method public constructor <init>(LX/5KP;LX/0dk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827604
    iput-object p1, p0, LX/BsU;->a:LX/5KP;

    .line 1827605
    iput-object p2, p0, LX/BsU;->b:LX/0dk;

    .line 1827606
    return-void
.end method

.method public static a(LX/0QB;)LX/BsU;
    .locals 5

    .prologue
    .line 1827607
    const-class v1, LX/BsU;

    monitor-enter v1

    .line 1827608
    :try_start_0
    sget-object v0, LX/BsU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827609
    sput-object v2, LX/BsU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827610
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827611
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827612
    new-instance p0, LX/BsU;

    const-class v3, LX/5KP;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/5KP;

    invoke-static {v0}, LX/0dk;->a(LX/0QB;)LX/0dk;

    move-result-object v4

    check-cast v4, LX/0dk;

    invoke-direct {p0, v3, v4}, LX/BsU;-><init>(LX/5KP;LX/0dk;)V

    .line 1827613
    move-object v0, p0

    .line 1827614
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827615
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BsU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827616
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827617
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
