.class public LX/Axt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/net/Uri;

.field private static volatile y:LX/Axt;


# instance fields
.field public final c:LX/0Sh;

.field public final d:LX/0ad;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AzE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BOn;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final h:LX/Axb;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/AyE;

.field public final k:Ljava/util/concurrent/ScheduledExecutorService;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/lang/Runnable;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AxX;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AxW;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/0SG;

.field public final r:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final s:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final t:Ljava/lang/Runnable;

.field public final u:Ljava/lang/Runnable;

.field public v:J

.field public w:J

.field public x:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1729053
    const-class v0, LX/Axt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Axt;->a:Ljava/lang/String;

    .line 1729054
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, LX/Axt;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0Or;LX/0ad;LX/0Ot;LX/Axb;LX/AyE;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;LX/0SG;LX/0Ot;LX/0Ot;)V
    .locals 4
    .param p7    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/BOn;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/AzE;",
            ">;",
            "LX/Axb;",
            "LX/AyE;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/AxX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AxW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1729061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729062
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/Axt;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1729063
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/Axt;->i:Ljava/util/Map;

    .line 1729064
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Axt;->p:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1729065
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/Axt;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1729066
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/Axt;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1729067
    iput-object p1, p0, LX/Axt;->c:LX/0Sh;

    .line 1729068
    iput-object p2, p0, LX/Axt;->f:LX/0Or;

    .line 1729069
    iput-object p3, p0, LX/Axt;->d:LX/0ad;

    .line 1729070
    iput-object p4, p0, LX/Axt;->e:LX/0Ot;

    .line 1729071
    iput-object p5, p0, LX/Axt;->h:LX/Axb;

    .line 1729072
    iput-object p6, p0, LX/Axt;->j:LX/AyE;

    .line 1729073
    iput-object p7, p0, LX/Axt;->k:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1729074
    iput-object p8, p0, LX/Axt;->l:LX/0Ot;

    .line 1729075
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$1;-><init>(LX/Axt;)V

    iput-object v0, p0, LX/Axt;->m:Ljava/lang/Runnable;

    .line 1729076
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;-><init>(LX/Axt;)V

    iput-object v0, p0, LX/Axt;->t:Ljava/lang/Runnable;

    .line 1729077
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$3;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$3;-><init>(LX/Axt;)V

    iput-object v0, p0, LX/Axt;->u:Ljava/lang/Runnable;

    .line 1729078
    iput-object p9, p0, LX/Axt;->q:LX/0SG;

    .line 1729079
    iput-wide v2, p0, LX/Axt;->v:J

    .line 1729080
    iput-wide v2, p0, LX/Axt;->w:J

    .line 1729081
    iput-wide v2, p0, LX/Axt;->x:J

    .line 1729082
    iput-object p10, p0, LX/Axt;->n:LX/0Ot;

    .line 1729083
    iput-object p11, p0, LX/Axt;->o:LX/0Ot;

    .line 1729084
    return-void
.end method

.method private static a(Ljava/util/Collection;)LX/0Px;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729055
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v0}, LX/Axt;->a([Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)[Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    .line 1729056
    array-length v1, v0

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 1729057
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1729058
    :goto_0
    return-object v0

    .line 1729059
    :cond_0
    new-instance v1, LX/Axl;

    invoke-direct {v1}, LX/Axl;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1729060
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Axt;
    .locals 15

    .prologue
    .line 1729031
    sget-object v0, LX/Axt;->y:LX/Axt;

    if-nez v0, :cond_1

    .line 1729032
    const-class v1, LX/Axt;

    monitor-enter v1

    .line 1729033
    :try_start_0
    sget-object v0, LX/Axt;->y:LX/Axt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729034
    if-eqz v2, :cond_0

    .line 1729035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729036
    new-instance v3, LX/Axt;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    const/16 v5, 0x35b1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    const/16 v7, 0x22d2

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 1729037
    new-instance v12, LX/Axb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/Axy;->b(LX/0QB;)LX/Axy;

    move-result-object v9

    check-cast v9, LX/Axy;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, Lcom/facebook/storyteller/ImageSimilarity4a;->b(LX/0QB;)Lcom/facebook/storyteller/ImageSimilarity4a;

    move-result-object v11

    check-cast v11, Lcom/facebook/storyteller/ImageSimilarity4a;

    invoke-direct {v12, v8, v9, v10, v11}, LX/Axb;-><init>(LX/0ad;LX/Axy;LX/0SG;Lcom/facebook/storyteller/ImageSimilarity4a;)V

    .line 1729038
    move-object v8, v12

    .line 1729039
    check-cast v8, LX/Axb;

    invoke-static {v0}, LX/AyE;->a(LX/0QB;)LX/AyE;

    move-result-object v9

    check-cast v9, LX/AyE;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v11, 0x259

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0x22b7

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x22b6

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/Axt;-><init>(LX/0Sh;LX/0Or;LX/0ad;LX/0Ot;LX/Axb;LX/AyE;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;LX/0SG;LX/0Ot;LX/0Ot;)V

    .line 1729040
    move-object v0, v3

    .line 1729041
    sput-object v0, LX/Axt;->y:LX/Axt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729044
    :cond_1
    sget-object v0, LX/Axt;->y:LX/Axt;

    return-object v0

    .line 1729045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)[Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 2

    .prologue
    .line 1729047
    array-length v0, p0

    if-lez v0, :cond_0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p0, v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    .line 1729048
    :goto_0
    return-object v0

    .line 1729049
    :cond_1
    const/4 v0, 0x0

    .line 1729050
    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_2

    aget-object v1, p0, v0

    if-eqz v1, :cond_2

    .line 1729051
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1729052
    :cond_2
    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    goto :goto_0
.end method

.method private static b(Ljava/util/Collection;)LX/0Px;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729025
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v0}, LX/Axt;->a([Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)[Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    .line 1729026
    array-length v1, v0

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 1729027
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1729028
    :goto_0
    return-object v0

    .line 1729029
    :cond_0
    new-instance v1, LX/Axm;

    invoke-direct {v1}, LX/Axm;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1729030
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(LX/Axt;)V
    .locals 3

    .prologue
    .line 1729022
    iget-object v0, p0, LX/Axt;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1729023
    iget-object v0, p0, LX/Axt;->k:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/Axt;->m:Ljava/lang/Runnable;

    const v2, 0x5c1eb047

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1729024
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729018
    iget-object v0, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 1729019
    iget-object v1, p0, LX/Axt;->d:LX/0ad;

    sget-short v2, LX/1kO;->P:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1729020
    invoke-static {v0}, LX/Axt;->b(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1729021
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/Axt;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728963
    iget-object v0, p0, LX/Axt;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1728964
    iget-object v0, p0, LX/Axt;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v2

    .line 1728965
    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1728966
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BOn;

    invoke-virtual {v0}, LX/BOn;->a()LX/BOy;

    move-result-object v0

    move-object v1, v0

    .line 1728967
    :goto_0
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BOy;

    invoke-virtual {v0}, LX/BOy;->d()B

    move-result v0

    if-eqz v0, :cond_2

    .line 1728968
    iget-object v0, p0, LX/Axt;->d:LX/0ad;

    sget-short v4, LX/1kO;->p:S

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1728969
    iget-object v0, p0, LX/Axt;->h:LX/Axb;

    invoke-virtual {v0, v1}, LX/Axb;->b(LX/BOy;)Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;

    move-result-object v4

    .line 1728970
    iget-object v0, p0, LX/Axt;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AxX;

    .line 1728971
    new-instance v5, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;

    iget-object v6, v0, LX/AxX;->a:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;

    invoke-virtual {v6, v4}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;->classifySouvenir(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;)Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;-><init>(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;)V

    move-object v0, v5

    .line 1728972
    iget-boolean v4, v0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mIsOverThreshold:Z

    move v4, v4

    .line 1728973
    if-eqz v4, :cond_0

    .line 1728974
    iget-object v4, p0, LX/Axt;->h:LX/Axb;

    .line 1728975
    iget-boolean v6, v0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mIsOverThreshold:Z

    move v6, v6

    .line 1728976
    if-eqz v6, :cond_7

    .line 1728977
    invoke-static {v1}, LX/Axb;->c(LX/BOy;)LX/Ay4;

    move-result-object v6

    .line 1728978
    iget-wide v10, v0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mScore:D

    move-wide v8, v10

    .line 1728979
    iput-wide v8, v6, LX/Ay4;->f:D

    .line 1728980
    move-object v6, v6

    .line 1728981
    iget-object v7, v0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mLoggingData:Ljava/util/Map;

    move-object v7, v7

    .line 1728982
    iput-object v7, v6, LX/Ay4;->g:Ljava/util/Map;

    .line 1728983
    move-object v6, v6

    .line 1728984
    invoke-static {v4, v1, v6}, LX/Axb;->a(LX/Axb;LX/BOy;LX/Ay4;)LX/Ay6;

    move-result-object v6

    invoke-virtual {v6}, LX/Ay6;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v6

    .line 1728985
    :goto_1
    move-object v0, v6

    .line 1728986
    iget-object v1, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728987
    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1728988
    :goto_2
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BOn;

    invoke-virtual {v0}, LX/BOn;->a()LX/BOy;

    move-result-object v0

    move-object v1, v0

    .line 1728989
    goto :goto_0

    .line 1728990
    :cond_0
    iget-object v0, p0, LX/Axt;->h:LX/Axb;

    invoke-virtual {v0, v1}, LX/Axb;->a(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    .line 1728991
    iget-object v1, p0, LX/Axt;->j:LX/AyE;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v4

    .line 1728992
    sget-object v5, LX/AyI;->a:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 1728993
    iget-object v6, v1, LX/AyE;->b:LX/AyH;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "models"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1728994
    iget-object v1, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1728995
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1728996
    :try_start_1
    iget-object v0, p0, LX/Axt;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, LX/Axt;->a:Ljava/lang/String;

    const-string v4, "Error running Storyteller"

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728997
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1728998
    :goto_3
    return-void

    .line 1728999
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/Axt;->h:LX/Axb;

    invoke-virtual {v0, v1}, LX/Axb;->a(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    .line 1729000
    iget-object v1, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729001
    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1729002
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1FJ;->close()V

    throw v0

    .line 1729003
    :cond_2
    :try_start_3
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1729004
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    .line 1729005
    iget-object v0, p0, LX/Axt;->j:LX/AyE;

    invoke-virtual {v0, v3}, LX/AyE;->a(Ljava/util/Collection;)Ljava/util/Collection;

    .line 1729006
    :cond_3
    iget-object v0, p0, LX/Axt;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1729007
    iget-object v0, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1729008
    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1729009
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v0}, LX/Az7;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1729010
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 1729011
    :cond_5
    iget-object v0, p0, LX/Axt;->d:LX/0ad;

    sget-short v1, LX/1kO;->y:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1729012
    iget-object v0, p0, LX/Axt;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AzE;

    invoke-virtual {v0}, LX/AzE;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    .line 1729013
    iget-object v1, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1729014
    iget-object v1, p0, LX/Axt;->i:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729015
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, LX/Axt;->p:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1729016
    iget-object v0, p0, LX/Axt;->q:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Axt;->v:J
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1729017
    invoke-virtual {v2}, LX/1FJ;->close()V

    goto/16 :goto_3

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_1
.end method
