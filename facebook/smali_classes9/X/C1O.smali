.class public LX/C1O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1842396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842397
    iput-object p1, p0, LX/C1O;->a:Ljava/lang/String;

    .line 1842398
    iput-object p2, p0, LX/C1O;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842399
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/C1O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842400
    iput-boolean p3, p0, LX/C1O;->c:Z

    .line 1842401
    return-void
.end method


# virtual methods
.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842395
    iget-object v0, p0, LX/C1O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
