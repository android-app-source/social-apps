.class public LX/BbC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BbC;


# instance fields
.field public final a:LX/0if;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1800484
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BbC;->b:Z

    .line 1800485
    iput-object p1, p0, LX/BbC;->a:LX/0if;

    .line 1800486
    return-void
.end method

.method public static a(LX/0QB;)LX/BbC;
    .locals 4

    .prologue
    .line 1800470
    sget-object v0, LX/BbC;->c:LX/BbC;

    if-nez v0, :cond_1

    .line 1800471
    const-class v1, LX/BbC;

    monitor-enter v1

    .line 1800472
    :try_start_0
    sget-object v0, LX/BbC;->c:LX/BbC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1800473
    if-eqz v2, :cond_0

    .line 1800474
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1800475
    new-instance p0, LX/BbC;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/BbC;-><init>(LX/0if;)V

    .line 1800476
    move-object v0, p0

    .line 1800477
    sput-object v0, LX/BbC;->c:LX/BbC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1800478
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1800479
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1800480
    :cond_1
    sget-object v0, LX/BbC;->c:LX/BbC;

    return-object v0

    .line 1800481
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1800482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1800463
    iget-object v0, p0, LX/BbC;->a:LX/0if;

    sget-object v1, LX/0ig;->az:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1800464
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BbC;->b:Z

    .line 1800465
    return-void
.end method

.method public final a(LX/BbB;)V
    .locals 3

    .prologue
    .line 1800468
    iget-object v0, p0, LX/BbC;->a:LX/0if;

    sget-object v1, LX/0ig;->az:LX/0ih;

    invoke-virtual {p1}, LX/BbB;->getActionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1800469
    return-void
.end method

.method public final a(LX/BbB;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1800466
    iget-object v0, p0, LX/BbC;->a:LX/0if;

    sget-object v1, LX/0ig;->az:LX/0ih;

    invoke-virtual {p1}, LX/BbB;->getActionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1800467
    return-void
.end method
