.class public final LX/B5n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/B5m;

.field public c:I

.field public d:LX/1po;

.field public e:Landroid/net/Uri;

.field private f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1745978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745979
    iput-object v0, p0, LX/B5n;->a:Ljava/lang/String;

    .line 1745980
    iput-object v0, p0, LX/B5n;->b:LX/B5m;

    .line 1745981
    iput v1, p0, LX/B5n;->c:I

    .line 1745982
    iput-object v0, p0, LX/B5n;->d:LX/1po;

    .line 1745983
    iput-object v0, p0, LX/B5n;->e:Landroid/net/Uri;

    .line 1745984
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/B5n;->f:LX/0am;

    .line 1745985
    iput v1, p0, LX/B5n;->j:I

    return-void
.end method


# virtual methods
.method public final a()LX/B5p;
    .locals 12

    .prologue
    .line 1745986
    new-instance v0, LX/B5p;

    iget-object v1, p0, LX/B5n;->a:Ljava/lang/String;

    iget-object v2, p0, LX/B5n;->b:LX/B5m;

    iget v3, p0, LX/B5n;->c:I

    iget-object v4, p0, LX/B5n;->d:LX/1po;

    iget-object v5, p0, LX/B5n;->e:Landroid/net/Uri;

    iget-object v6, p0, LX/B5n;->f:LX/0am;

    iget-object v7, p0, LX/B5n;->g:Ljava/lang/String;

    iget-object v8, p0, LX/B5n;->h:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iget-object v9, p0, LX/B5n;->i:Landroid/net/Uri;

    iget v10, p0, LX/B5n;->j:I

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LX/B5p;-><init>(Ljava/lang/String;LX/B5m;ILX/1po;Landroid/net/Uri;LX/0am;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Landroid/net/Uri;IB)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/B5n;
    .locals 1

    .prologue
    .line 1745976
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/B5n;->f:LX/0am;

    .line 1745977
    return-object p0
.end method
