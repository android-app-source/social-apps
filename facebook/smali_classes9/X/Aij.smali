.class public abstract LX/Aij;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:LX/Aiv;

.field public b:Ljava/lang/String;

.field public final c:LX/03V;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/17Y;

.field public g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1706823
    const-class v0, LX/Aij;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Aij;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 1

    .prologue
    .line 1706816
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1706817
    const-string v0, "/feed/control/feed_awesomizer/learn_more/?card="

    iput-object v0, p0, LX/Aij;->b:Ljava/lang/String;

    .line 1706818
    iput-object p1, p0, LX/Aij;->c:LX/03V;

    .line 1706819
    iput-object p2, p0, LX/Aij;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1706820
    iput-object p3, p0, LX/Aij;->f:LX/17Y;

    .line 1706821
    const/4 v0, 0x1

    invoke-super {p0, v0}, LX/1OM;->a(Z)V

    .line 1706822
    return-void
.end method

.method public static a(Landroid/view/View;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1706815
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082398

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082397

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1706814
    int-to-long v0, p1

    return-wide v0
.end method

.method public a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 1706803
    if-nez p2, :cond_0

    .line 1706804
    new-instance v0, LX/Aih;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030154

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aih;-><init>(LX/Aij;Landroid/view/View;)V

    .line 1706805
    :goto_0
    return-object v0

    .line 1706806
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 1706807
    invoke-virtual {p0, p1}, LX/Aij;->c(Landroid/view/ViewGroup;)LX/Aii;

    move-result-object v0

    goto :goto_0

    .line 1706808
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 1706809
    invoke-virtual {p0, p1}, LX/Aij;->a(Landroid/view/ViewGroup;)LX/Aii;

    move-result-object v0

    goto :goto_0

    .line 1706810
    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    .line 1706811
    invoke-virtual {p0, p1}, LX/Aij;->b(Landroid/view/ViewGroup;)LX/Aii;

    move-result-object v0

    goto :goto_0

    .line 1706812
    :cond_3
    iget-object v0, p0, LX/Aij;->c:LX/03V;

    sget-object v1, LX/Aij;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid view type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in the awesomizer card adapter"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706813
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)LX/Aii;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<TT;>.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1706802
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Landroid/view/View;I)V
.end method

.method public final a(Ljava/lang/Object;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ZI)V"
        }
    .end annotation

    .prologue
    .line 1706797
    iput-object p1, p0, LX/Aij;->g:Ljava/lang/Object;

    .line 1706798
    iput-boolean p2, p0, LX/Aij;->h:Z

    .line 1706799
    iput p3, p0, LX/Aij;->i:I

    .line 1706800
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1706801
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)LX/Aii;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<TT;>.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1706796
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract c(Landroid/view/ViewGroup;)LX/Aii;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<TT;>.ViewHolderItem;"
        }
    .end annotation
.end method

.method public e(I)Z
    .locals 1

    .prologue
    .line 1706795
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)Z
    .locals 1

    .prologue
    .line 1706794
    const/4 v0, 0x0

    return v0
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public g(I)Z
    .locals 1

    .prologue
    .line 1706793
    const/4 v0, 0x0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 1706785
    invoke-virtual {p0, p1}, LX/Aij;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1706786
    const/4 v0, 0x0

    .line 1706787
    :goto_0
    return v0

    .line 1706788
    :cond_0
    invoke-virtual {p0, p1}, LX/Aij;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1706789
    const/4 v0, 0x2

    goto :goto_0

    .line 1706790
    :cond_1
    invoke-virtual {p0, p1}, LX/Aij;->g(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1706791
    const/4 v0, 0x3

    goto :goto_0

    .line 1706792
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract ij_()I
.end method
