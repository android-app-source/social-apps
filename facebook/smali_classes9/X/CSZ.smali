.class public final LX/CSZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CSa;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

.field private final e:Lorg/json/JSONArray;

.field private final f:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1894805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894806
    iput-object v0, p0, LX/CSZ;->b:Ljava/lang/String;

    .line 1894807
    iput-object v0, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    .line 1894808
    iput-object p1, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1894809
    iput-object v0, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    .line 1894810
    iput-object v0, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    .line 1894811
    sget-object v0, LX/CSa;->CREDIT_CARD:LX/CSa;

    iput-object v0, p0, LX/CSZ;->a:LX/CSa;

    .line 1894812
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1894821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894822
    iput-object p1, p0, LX/CSZ;->b:Ljava/lang/String;

    .line 1894823
    iput-object v0, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    .line 1894824
    iput-object v0, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1894825
    iput-object v0, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    .line 1894826
    iput-object v0, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    .line 1894827
    sget-object v0, LX/CSa;->STRING:LX/CSa;

    iput-object v0, p0, LX/CSZ;->a:LX/CSa;

    .line 1894828
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1894813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894814
    iput-object v0, p0, LX/CSZ;->b:Ljava/lang/String;

    .line 1894815
    iput-object p1, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    .line 1894816
    iput-object v0, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1894817
    iput-object v0, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    .line 1894818
    iput-object v0, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    .line 1894819
    sget-object v0, LX/CSa;->STRING_ARRAY:LX/CSa;

    iput-object v0, p0, LX/CSZ;->a:LX/CSa;

    .line 1894820
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONArray;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1894789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894790
    iput-object v0, p0, LX/CSZ;->b:Ljava/lang/String;

    .line 1894791
    iput-object v0, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    .line 1894792
    iput-object v0, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1894793
    iput-object p1, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    .line 1894794
    iput-object v0, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    .line 1894795
    sget-object v0, LX/CSa;->JSON_ARRAY:LX/CSa;

    iput-object v0, p0, LX/CSZ;->a:LX/CSa;

    .line 1894796
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1894797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894798
    iput-object v0, p0, LX/CSZ;->b:Ljava/lang/String;

    .line 1894799
    iput-object v0, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    .line 1894800
    iput-object v0, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1894801
    iput-object v0, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    .line 1894802
    iput-object p1, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    .line 1894803
    sget-object v0, LX/CSa;->JSON_OBJECT:LX/CSa;

    iput-object v0, p0, LX/CSZ;->a:LX/CSa;

    .line 1894804
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1894786
    iget-object v0, p0, LX/CSZ;->a:LX/CSa;

    sget-object v1, LX/CSa;->STRING:LX/CSa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894787
    iget-object v0, p0, LX/CSZ;->b:Ljava/lang/String;

    return-object v0

    .line 1894788
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1894783
    iget-object v0, p0, LX/CSZ;->a:LX/CSa;

    sget-object v1, LX/CSa;->STRING_ARRAY:LX/CSa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894784
    iget-object v0, p0, LX/CSZ;->c:Ljava/util/ArrayList;

    return-object v0

    .line 1894785
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .locals 2

    .prologue
    .line 1894780
    iget-object v0, p0, LX/CSZ;->a:LX/CSa;

    sget-object v1, LX/CSa;->CREDIT_CARD:LX/CSa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894781
    iget-object v0, p0, LX/CSZ;->d:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    return-object v0

    .line 1894782
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lorg/json/JSONArray;
    .locals 2

    .prologue
    .line 1894777
    iget-object v0, p0, LX/CSZ;->a:LX/CSa;

    sget-object v1, LX/CSa;->JSON_ARRAY:LX/CSa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894778
    iget-object v0, p0, LX/CSZ;->e:Lorg/json/JSONArray;

    return-object v0

    .line 1894779
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 1894774
    iget-object v0, p0, LX/CSZ;->a:LX/CSa;

    sget-object v1, LX/CSa;->JSON_OBJECT:LX/CSa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894775
    iget-object v0, p0, LX/CSZ;->f:Lorg/json/JSONObject;

    return-object v0

    .line 1894776
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
