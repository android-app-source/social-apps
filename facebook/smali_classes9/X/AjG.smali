.class public LX/AjG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/AjG;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2di;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1707726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707727
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1707728
    iput-object v0, p0, LX/AjG;->a:LX/0Ot;

    .line 1707729
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1707730
    iput-object v0, p0, LX/AjG;->e:LX/0Ot;

    .line 1707731
    return-void
.end method

.method public static a(LX/0QB;)LX/AjG;
    .locals 8

    .prologue
    .line 1707732
    sget-object v0, LX/AjG;->f:LX/AjG;

    if-nez v0, :cond_1

    .line 1707733
    const-class v1, LX/AjG;

    monitor-enter v1

    .line 1707734
    :try_start_0
    sget-object v0, LX/AjG;->f:LX/AjG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1707735
    if-eqz v2, :cond_0

    .line 1707736
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1707737
    new-instance v3, LX/AjG;

    invoke-direct {v3}, LX/AjG;-><init>()V

    .line 1707738
    const/16 v4, 0x473

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0xaf8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1707739
    iput-object v4, v3, LX/AjG;->a:LX/0Ot;

    iput-object v5, v3, LX/AjG;->b:LX/0tX;

    iput-object v6, v3, LX/AjG;->c:LX/1Ck;

    iput-object v7, v3, LX/AjG;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p0, v3, LX/AjG;->e:LX/0Ot;

    .line 1707740
    move-object v0, v3

    .line 1707741
    sput-object v0, LX/AjG;->f:LX/AjG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1707742
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1707743
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1707744
    :cond_1
    sget-object v0, LX/AjG;->f:LX/AjG;

    return-object v0

    .line 1707745
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1707746
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;ILX/0TF;)V
    .locals 4

    .prologue
    .line 1707747
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 1707748
    :cond_0
    :goto_0
    return-void

    .line 1707749
    :cond_1
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->l()I

    move-result v0

    .line 1707750
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1707751
    iget-object v1, p0, LX/AjG;->c:LX/1Ck;

    const/4 v2, 0x0

    new-instance v3, LX/AjD;

    invoke-direct {v3, p0, p1, v0}, LX/AjD;-><init>(LX/AjG;Lcom/facebook/graphql/model/GraphQLStory;I)V

    new-instance v0, LX/AjE;

    invoke-direct {v0, p0, p1, p3}, LX/AjE;-><init>(LX/AjG;Lcom/facebook/graphql/model/GraphQLStory;LX/0TF;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
