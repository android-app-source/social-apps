.class public LX/CNm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNc;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 0

    .prologue
    .line 1882062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882063
    iput-object p1, p0, LX/CNm;->a:LX/CNb;

    .line 1882064
    iput-object p2, p0, LX/CNm;->b:LX/CNc;

    .line 1882065
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1882066
    iget-object v0, p0, LX/CNm;->a:LX/CNb;

    const-string v1, "target-id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1882067
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->a()V

    .line 1882068
    iget-object v0, p0, LX/CNm;->a:LX/CNb;

    invoke-virtual {v0}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1882069
    :goto_1
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->b()V

    .line 1882070
    return-void

    .line 1882071
    :sswitch_0
    const-string v3, "NT:ACTION:APPEND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "NT:ACTION:INSERT_AFTER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "NT:ACTION:INSERT_BEFORE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "NT:ACTION:PREPEND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "NT:ACTION:REPLACE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "NT:ACTION:REMOVE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    .line 1882072
    :pswitch_0
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iget-object v2, p0, LX/CNm;->a:LX/CNb;

    const-string v3, "children"

    invoke-virtual {v2, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882073
    iget-object v3, v0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v3, v1}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1882074
    if-nez v3, :cond_1

    .line 1882075
    :goto_2
    goto :goto_1

    .line 1882076
    :pswitch_1
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iget-object v2, p0, LX/CNm;->a:LX/CNb;

    const-string v3, "children"

    invoke-virtual {v2, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882077
    iget-object v3, v0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v3, v1}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1882078
    if-nez v3, :cond_2

    .line 1882079
    :goto_3
    goto :goto_1

    .line 1882080
    :pswitch_2
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iget-object v2, p0, LX/CNm;->a:LX/CNb;

    const-string v3, "children"

    invoke-virtual {v2, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882081
    iget-object v3, v0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v3, v1}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1882082
    if-nez v3, :cond_4

    .line 1882083
    :goto_4
    goto/16 :goto_1

    .line 1882084
    :pswitch_3
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iget-object v2, p0, LX/CNm;->a:LX/CNb;

    const-string v3, "children"

    invoke-virtual {v2, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882085
    iget-object v3, v0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v3, v1}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1882086
    if-nez v3, :cond_6

    .line 1882087
    :goto_5
    goto/16 :goto_1

    .line 1882088
    :pswitch_4
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iget-object v2, p0, LX/CNm;->a:LX/CNb;

    const-string v3, "children"

    invoke-virtual {v2, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/CNS;->e(LX/0Px;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1882089
    :pswitch_5
    iget-object v0, p0, LX/CNm;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    .line 1882090
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1882091
    invoke-virtual {v0, v2, v1}, LX/CNS;->e(LX/0Px;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1882092
    :cond_1
    const-string v4, "children"

    invoke-static {v2, v3, v4}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 1882093
    new-instance v5, LX/CNN;

    invoke-direct {v5, v0, v4}, LX/CNN;-><init>(LX/CNS;LX/0Px;)V

    const-string v4, "children"

    invoke-static {v0, v5, v3, v4}, LX/CNS;->a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V

    goto :goto_2

    .line 1882094
    :cond_2
    invoke-static {v3}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v4

    .line 1882095
    invoke-static {v3}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v5

    .line 1882096
    invoke-static {v2, v5, v4}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 1882097
    if-nez v5, :cond_3

    .line 1882098
    iget-object v4, v0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v4, v3}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v0, v6, v3}, LX/CNS;->a(LX/CNS;LX/0Px;I)V

    goto :goto_3

    .line 1882099
    :cond_3
    new-instance v7, LX/CNP;

    invoke-direct {v7, v0, v3, v6}, LX/CNP;-><init>(LX/CNS;LX/CNb;LX/0Px;)V

    invoke-static {v0, v7, v5, v4}, LX/CNS;->a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V

    goto :goto_3

    .line 1882100
    :cond_4
    invoke-static {v3}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v4

    .line 1882101
    invoke-static {v3}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v5

    .line 1882102
    invoke-static {v2, v5, v4}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 1882103
    if-nez v5, :cond_5

    .line 1882104
    iget-object v5, v0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v5, v3}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v0, v4, v3}, LX/CNS;->a(LX/CNS;LX/0Px;I)V

    goto :goto_4

    .line 1882105
    :cond_5
    new-instance v6, LX/CNQ;

    invoke-direct {v6, v0, v3, v4}, LX/CNQ;-><init>(LX/CNS;LX/CNb;LX/0Px;)V

    invoke-static {v3}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v6, v5, v3}, LX/CNS;->a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1882106
    :cond_6
    const-string v4, "children"

    invoke-static {v2, v3, v4}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 1882107
    new-instance v5, LX/CNO;

    invoke-direct {v5, v0, v4}, LX/CNO;-><init>(LX/CNS;LX/0Px;)V

    const-string v4, "children"

    invoke-static {v0, v5, v3, v4}, LX/CNS;->a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V

    goto/16 :goto_5

    :sswitch_data_0
    .sparse-switch
        -0x5a91bc3a -> :sswitch_1
        -0x1b35cda2 -> :sswitch_3
        0x9fabeb5 -> :sswitch_2
        0x3900c984 -> :sswitch_4
        0x50ca1a2a -> :sswitch_0
        0x6d304394 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
