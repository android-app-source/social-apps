.class public abstract LX/ByU;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1837394
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ByU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1837395
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1837396
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ByU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1837397
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1837398
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1837399
    invoke-virtual {p0}, LX/ByU;->getLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1837400
    const v0, 0x7f0d2d52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/ByU;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1837401
    const v0, 0x7f0d2d51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/ByU;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1837402
    return-void
.end method


# virtual methods
.method public abstract getLayout()I
.end method
