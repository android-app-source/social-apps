.class public final LX/CU1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CU2;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTx;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;)V
    .locals 4

    .prologue
    .line 1896171
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, LX/CU1;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;)V

    .line 1896172
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1896173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896174
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896175
    iput-object p1, p0, LX/CU1;->a:Ljava/lang/String;

    .line 1896176
    iput-object p4, p0, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    .line 1896177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CU1;->b:Ljava/util/ArrayList;

    .line 1896178
    if-eqz p2, :cond_0

    .line 1896179
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    .line 1896180
    iget-object v2, p0, LX/CU1;->b:Ljava/util/ArrayList;

    new-instance v3, LX/CU2;

    invoke-direct {v3, v0}, LX/CU2;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1896181
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CU1;->c:Ljava/util/ArrayList;

    .line 1896182
    if-eqz p3, :cond_1

    .line 1896183
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    .line 1896184
    iget-object v2, p0, LX/CU1;->c:Ljava/util/ArrayList;

    new-instance v3, LX/CTx;

    invoke-direct {v3, v0}, LX/CTx;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1896185
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/CU1;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1896186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CU1;->b:Ljava/util/ArrayList;

    .line 1896188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CU1;->c:Ljava/util/ArrayList;

    .line 1896189
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU1;

    iget-object v0, v0, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    :goto_0
    iput-object v0, p0, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    .line 1896190
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1896191
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v2

    move v1, v2

    :goto_1
    if-ge v4, v7, :cond_4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU1;

    .line 1896192
    add-int/lit8 v5, v1, 0x1

    .line 1896193
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    .line 1896194
    iget-object v1, v0, LX/CU1;->a:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1896195
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v5, v1, :cond_0

    .line 1896196
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1896197
    :cond_0
    iget-object v9, v0, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v3, v2

    :goto_2
    if-ge v3, v10, :cond_2

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU2;

    .line 1896198
    iget-object v11, p0, LX/CU1;->b:Ljava/util/ArrayList;

    new-instance v12, LX/CU2;

    invoke-direct {v12, v1, v8}, LX/CU2;-><init>(LX/CU2;I)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896199
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1896200
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    goto :goto_0

    .line 1896201
    :cond_2
    iget-object v3, v0, LX/CU1;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v1, v2

    :goto_3
    if-ge v1, v9, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTx;

    .line 1896202
    iget-object v10, p0, LX/CU1;->c:Ljava/util/ArrayList;

    new-instance v11, LX/CTx;

    invoke-direct {v11, v0, v8}, LX/CTx;-><init>(LX/CTx;I)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896203
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1896204
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v1, v5

    goto :goto_1

    .line 1896205
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU1;->a:Ljava/lang/String;

    .line 1896206
    return-void
.end method
