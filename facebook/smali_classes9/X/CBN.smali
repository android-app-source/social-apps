.class public LX/CBN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CBL;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CBP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1856388
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CBN;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CBP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856341
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1856342
    iput-object p1, p0, LX/CBN;->b:LX/0Ot;

    .line 1856343
    return-void
.end method

.method public static a(LX/0QB;)LX/CBN;
    .locals 4

    .prologue
    .line 1856377
    const-class v1, LX/CBN;

    monitor-enter v1

    .line 1856378
    :try_start_0
    sget-object v0, LX/CBN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856379
    sput-object v2, LX/CBN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856380
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856381
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856382
    new-instance v3, LX/CBN;

    const/16 p0, 0x2139

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CBN;-><init>(LX/0Ot;)V

    .line 1856383
    move-object v0, v3

    .line 1856384
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856385
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856386
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1856375
    invoke-static {}, LX/1dS;->b()V

    .line 1856376
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1856372
    iget-object v0, p0, LX/CBN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1856373
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 1856374
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 5

    .prologue
    .line 1856356
    check-cast p2, LX/CBM;

    .line 1856357
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1856358
    iget-object v0, p0, LX/CBN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBP;

    iget-object v2, p2, LX/CBM;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    .line 1856359
    invoke-static {v2}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v4

    .line 1856360
    const/4 v3, 0x0

    .line 1856361
    invoke-static {v2}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object p0

    .line 1856362
    if-nez p0, :cond_3

    .line 1856363
    :cond_0
    :goto_0
    move-object p0, v3

    .line 1856364
    if-eqz v4, :cond_1

    if-nez p0, :cond_2

    .line 1856365
    :cond_1
    const/4 v3, 0x0

    .line 1856366
    :goto_1
    move-object v3, v3

    .line 1856367
    iput-object v3, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1856368
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1856369
    check-cast v0, LX/0aC;

    iput-object v0, p2, LX/CBM;->b:LX/0aC;

    .line 1856370
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1856371
    return-void

    :cond_2
    new-instance v3, LX/CBO;

    invoke-direct {v3, v0, p0, v4}, LX/CBO;-><init>(LX/CBP;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->j()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->j()LX/0Px;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1856355
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1856350
    check-cast p3, LX/CBM;

    .line 1856351
    iget-object v0, p0, LX/CBN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBP;

    iget-object v1, p3, LX/CBM;->b:LX/0aC;

    .line 1856352
    if-eqz v1, :cond_0

    .line 1856353
    iget-object p0, v0, LX/CBP;->a:LX/CNw;

    const-string p1, "quick_promotion_action"

    invoke-virtual {p0, p1, v1}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1856354
    :cond_0
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1856345
    check-cast p3, LX/CBM;

    .line 1856346
    iget-object v0, p0, LX/CBN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBP;

    iget-object v1, p3, LX/CBM;->b:LX/0aC;

    .line 1856347
    if-eqz v1, :cond_0

    .line 1856348
    iget-object p0, v0, LX/CBP;->a:LX/CNw;

    const-string p1, "quick_promotion_action"

    invoke-virtual {p0, p1, v1}, LX/0UI;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1856349
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1856344
    const/16 v0, 0xf

    return v0
.end method
