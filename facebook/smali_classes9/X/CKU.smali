.class public final LX/CKU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CKR;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;)V
    .locals 0

    .prologue
    .line 1877226
    iput-object p1, p0, LX/CKU;->a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1877218
    iget-object v0, p0, LX/CKU;->a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    sget-object v1, LX/CKW;->SUBSCRIBED:LX/CKW;

    .line 1877219
    invoke-static {v0, v1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a$redex0(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/CKW;)V

    .line 1877220
    iget-object v0, p0, LX/CKU;->a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    iget-object v0, v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->f:LX/CKX;

    if-eqz v0, :cond_0

    .line 1877221
    iget-object v0, p0, LX/CKU;->a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    iget-object v0, v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->f:LX/CKX;

    invoke-interface {v0}, LX/CKX;->a()V

    .line 1877222
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1877223
    iget-object v0, p0, LX/CKU;->a:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    sget-object v1, LX/CKW;->SUBSCRIBE:LX/CKW;

    .line 1877224
    invoke-static {v0, v1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a$redex0(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/CKW;)V

    .line 1877225
    return-void
.end method
