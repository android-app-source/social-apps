.class public final LX/Bcu;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Bcw;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:I

.field public c:Landroid/os/Handler;

.field public d:LX/1rs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTResponseModel;>;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:J

.field public g:I

.field public h:LX/Bcm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bcm",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public i:I

.field public j:LX/Bcp;

.field public k:LX/BcQ;

.field public final synthetic l:LX/Bcw;


# direct methods
.method public constructor <init>(LX/Bcw;)V
    .locals 2

    .prologue
    .line 1802895
    iput-object p1, p0, LX/Bcu;->l:LX/Bcw;

    .line 1802896
    move-object v0, p1

    .line 1802897
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1802898
    const/16 v0, 0xa

    iput v0, p0, LX/Bcu;->b:I

    .line 1802899
    const-wide/16 v0, 0x258

    iput-wide v0, p0, LX/Bcu;->f:J

    .line 1802900
    const/4 v0, 0x0

    iput v0, p0, LX/Bcu;->g:I

    .line 1802901
    const/4 v0, 0x5

    iput v0, p0, LX/Bcu;->i:I

    .line 1802902
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 1802903
    const/4 v1, 0x0

    .line 1802904
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Bcu;

    .line 1802905
    if-nez p1, :cond_0

    .line 1802906
    iput-object v1, v0, LX/Bcu;->c:Landroid/os/Handler;

    .line 1802907
    iput-object v1, v0, LX/Bcu;->h:LX/Bcm;

    .line 1802908
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1802909
    if-ne p0, p1, :cond_1

    .line 1802910
    :cond_0
    :goto_0
    return v0

    .line 1802911
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1802912
    goto :goto_0

    .line 1802913
    :cond_3
    check-cast p1, LX/Bcu;

    .line 1802914
    iget v2, p0, LX/Bcu;->b:I

    iget v3, p1, LX/Bcu;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1802915
    goto :goto_0

    .line 1802916
    :cond_4
    iget-object v2, p0, LX/Bcu;->c:Landroid/os/Handler;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bcu;->c:Landroid/os/Handler;

    iget-object v3, p1, LX/Bcu;->c:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1802917
    goto :goto_0

    .line 1802918
    :cond_6
    iget-object v2, p1, LX/Bcu;->c:Landroid/os/Handler;

    if-nez v2, :cond_5

    .line 1802919
    :cond_7
    iget-object v2, p0, LX/Bcu;->d:LX/1rs;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Bcu;->d:LX/1rs;

    iget-object v3, p1, LX/Bcu;->d:LX/1rs;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1802920
    goto :goto_0

    .line 1802921
    :cond_9
    iget-object v2, p1, LX/Bcu;->d:LX/1rs;

    if-nez v2, :cond_8

    .line 1802922
    :cond_a
    iget-object v2, p0, LX/Bcu;->e:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Bcu;->e:Ljava/lang/String;

    iget-object v3, p1, LX/Bcu;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1802923
    goto :goto_0

    .line 1802924
    :cond_c
    iget-object v2, p1, LX/Bcu;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1802925
    :cond_d
    iget-wide v2, p0, LX/Bcu;->f:J

    iget-wide v4, p1, LX/Bcu;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    move v0, v1

    .line 1802926
    goto :goto_0

    .line 1802927
    :cond_e
    iget v2, p0, LX/Bcu;->g:I

    iget v3, p1, LX/Bcu;->g:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1802928
    goto :goto_0

    .line 1802929
    :cond_f
    iget-object v2, p0, LX/Bcu;->h:LX/Bcm;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/Bcu;->h:LX/Bcm;

    iget-object v3, p1, LX/Bcu;->h:LX/Bcm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1802930
    goto :goto_0

    .line 1802931
    :cond_11
    iget-object v2, p1, LX/Bcu;->h:LX/Bcm;

    if-nez v2, :cond_10

    .line 1802932
    :cond_12
    iget v2, p0, LX/Bcu;->i:I

    iget v3, p1, LX/Bcu;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1802933
    goto/16 :goto_0
.end method
