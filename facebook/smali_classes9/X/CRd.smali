.class public final LX/CRd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1892306
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1892307
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1892308
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1892309
    invoke-static {p0, p1}, LX/CRd;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1892310
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1892311
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1892300
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1892301
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1892302
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/CRd;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1892303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1892304
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1892305
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1892194
    const/16 v17, 0x0

    .line 1892195
    const/16 v16, 0x0

    .line 1892196
    const-wide/16 v14, 0x0

    .line 1892197
    const/4 v13, 0x0

    .line 1892198
    const/4 v12, 0x0

    .line 1892199
    const/4 v11, 0x0

    .line 1892200
    const/4 v10, 0x0

    .line 1892201
    const/4 v9, 0x0

    .line 1892202
    const/4 v8, 0x0

    .line 1892203
    const-wide/16 v6, 0x0

    .line 1892204
    const/4 v5, 0x0

    .line 1892205
    const/4 v4, 0x0

    .line 1892206
    const/4 v3, 0x0

    .line 1892207
    const/4 v2, 0x0

    .line 1892208
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 1892209
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1892210
    const/4 v2, 0x0

    .line 1892211
    :goto_0
    return v2

    .line 1892212
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_b

    .line 1892213
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1892214
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1892215
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1892216
    const-string v7, "album"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1892217
    invoke-static/range {p0 .. p1}, LX/CRe;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1892218
    :cond_1
    const-string v7, "can_viewer_add_tags"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1892219
    const/4 v2, 0x1

    .line 1892220
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v18, v6

    move v6, v2

    goto :goto_1

    .line 1892221
    :cond_2
    const-string v7, "created_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1892222
    const/4 v2, 0x1

    .line 1892223
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1892224
    :cond_3
    const-string v7, "focus"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1892225
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1892226
    :cond_4
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1892227
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1892228
    :cond_5
    const-string v7, "image"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1892229
    invoke-static/range {p0 .. p1}, LX/CR5;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1892230
    :cond_6
    const-string v7, "image320"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1892231
    invoke-static/range {p0 .. p1}, LX/CR5;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1892232
    :cond_7
    const-string v7, "is_disturbing"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1892233
    const/4 v2, 0x1

    .line 1892234
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v9, v2

    move v13, v7

    goto/16 :goto_1

    .line 1892235
    :cond_8
    const-string v7, "message"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1892236
    invoke-static/range {p0 .. p1}, LX/8ZT;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1892237
    :cond_9
    const-string v7, "modified_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1892238
    const/4 v2, 0x1

    .line 1892239
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v10

    move v8, v2

    goto/16 :goto_1

    .line 1892240
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1892241
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1892242
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892243
    if-eqz v6, :cond_c

    .line 1892244
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1892245
    :cond_c
    if-eqz v3, :cond_d

    .line 1892246
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1892247
    :cond_d
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892248
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892249
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1892250
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1892251
    if-eqz v9, :cond_e

    .line 1892252
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 1892253
    :cond_e
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1892254
    if-eqz v8, :cond_f

    .line 1892255
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1892256
    :cond_f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v18, v16

    move/from16 v19, v17

    move/from16 v16, v12

    move/from16 v17, v13

    move v12, v8

    move v13, v9

    move v8, v2

    move v9, v3

    move v3, v4

    move/from16 v21, v11

    move/from16 v22, v5

    move-wide v4, v14

    move/from16 v15, v21

    move v14, v10

    move-wide v10, v6

    move/from16 v6, v22

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1892257
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1892258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892259
    if-eqz v0, :cond_0

    .line 1892260
    const-string v1, "album"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892261
    invoke-static {p0, v0, p2, p3}, LX/CRe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1892262
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1892263
    if-eqz v0, :cond_1

    .line 1892264
    const-string v1, "can_viewer_add_tags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892265
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1892266
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1892267
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 1892268
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892269
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1892270
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892271
    if-eqz v0, :cond_3

    .line 1892272
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892273
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1892274
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1892275
    if-eqz v0, :cond_4

    .line 1892276
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892277
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1892278
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892279
    if-eqz v0, :cond_5

    .line 1892280
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892281
    invoke-static {p0, v0, p2}, LX/CR5;->a(LX/15i;ILX/0nX;)V

    .line 1892282
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892283
    if-eqz v0, :cond_6

    .line 1892284
    const-string v1, "image320"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892285
    invoke-static {p0, v0, p2}, LX/CR5;->a(LX/15i;ILX/0nX;)V

    .line 1892286
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1892287
    if-eqz v0, :cond_7

    .line 1892288
    const-string v1, "is_disturbing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892289
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1892290
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892291
    if-eqz v0, :cond_8

    .line 1892292
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892293
    invoke-static {p0, v0, p2, p3}, LX/8ZT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1892294
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1892295
    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    .line 1892296
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892297
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1892298
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1892299
    return-void
.end method
