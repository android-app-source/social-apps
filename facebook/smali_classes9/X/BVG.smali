.class public final LX/BVG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:F

.field private I:F

.field private J:F

.field private K:F

.field public L:F

.field private a:I

.field private b:I

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:Z

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/high16 v5, 0x40c00000    # 6.0f

    const/high16 v4, 0x40400000    # 3.0f

    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1790409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790410
    const/4 v0, 0x0

    iput v0, p0, LX/BVG;->a:I

    .line 1790411
    const/16 v0, 0x2710

    iput v0, p0, LX/BVG;->b:I

    .line 1790412
    iput v3, p0, LX/BVG;->c:F

    .line 1790413
    iput v3, p0, LX/BVG;->d:F

    .line 1790414
    iput v3, p0, LX/BVG;->e:F

    .line 1790415
    iput v3, p0, LX/BVG;->f:F

    .line 1790416
    iput v2, p0, LX/BVG;->g:F

    .line 1790417
    iput v2, p0, LX/BVG;->h:F

    .line 1790418
    iput v2, p0, LX/BVG;->i:F

    .line 1790419
    iput v2, p0, LX/BVG;->j:F

    .line 1790420
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BVG;->k:Z

    .line 1790421
    iput v1, p0, LX/BVG;->l:F

    .line 1790422
    iput v1, p0, LX/BVG;->m:F

    .line 1790423
    iput v1, p0, LX/BVG;->n:F

    .line 1790424
    iput v1, p0, LX/BVG;->o:F

    .line 1790425
    iput v1, p0, LX/BVG;->p:F

    .line 1790426
    iput v1, p0, LX/BVG;->q:F

    .line 1790427
    iput v2, p0, LX/BVG;->r:F

    .line 1790428
    iput v2, p0, LX/BVG;->s:F

    .line 1790429
    iput v2, p0, LX/BVG;->t:F

    .line 1790430
    iput v2, p0, LX/BVG;->u:F

    .line 1790431
    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, LX/BVG;->v:F

    .line 1790432
    iput v4, p0, LX/BVG;->w:F

    .line 1790433
    iput v5, p0, LX/BVG;->x:F

    .line 1790434
    const/16 v0, 0x3e8

    iput v0, p0, LX/BVG;->y:I

    .line 1790435
    const/16 v0, 0x2710

    iput v0, p0, LX/BVG;->z:I

    .line 1790436
    const/high16 v0, -0x3fc00000    # -3.0f

    iput v0, p0, LX/BVG;->A:F

    .line 1790437
    iput v5, p0, LX/BVG;->B:F

    .line 1790438
    iput v4, p0, LX/BVG;->C:F

    .line 1790439
    const/high16 v0, 0x41900000    # 18.0f

    iput v0, p0, LX/BVG;->D:F

    .line 1790440
    const/high16 v0, 0x40600000    # 3.5f

    iput v0, p0, LX/BVG;->E:F

    .line 1790441
    const/high16 v0, 0x40b00000    # 5.5f

    iput v0, p0, LX/BVG;->F:F

    .line 1790442
    iput v1, p0, LX/BVG;->G:F

    .line 1790443
    iput v1, p0, LX/BVG;->H:F

    .line 1790444
    iput v1, p0, LX/BVG;->I:F

    .line 1790445
    iput v1, p0, LX/BVG;->J:F

    .line 1790446
    iput v1, p0, LX/BVG;->K:F

    .line 1790447
    const/high16 v0, 0x44160000    # 600.0f

    iput v0, p0, LX/BVG;->L:F

    return-void
.end method


# virtual methods
.method public final a(F)LX/BVG;
    .locals 1

    .prologue
    .line 1790448
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790449
    iput p1, p0, LX/BVG;->v:F

    .line 1790450
    return-object p0

    .line 1790451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790452
    iput p1, p0, LX/BVG;->e:F

    .line 1790453
    iput p2, p0, LX/BVG;->f:F

    .line 1790454
    return-object p0
.end method

.method public final a(FFFF)LX/BVG;
    .locals 0

    .prologue
    .line 1790455
    iput p1, p0, LX/BVG;->n:F

    .line 1790456
    iput p2, p0, LX/BVG;->o:F

    .line 1790457
    iput p3, p0, LX/BVG;->p:F

    .line 1790458
    iput p4, p0, LX/BVG;->q:F

    .line 1790459
    return-object p0
.end method

.method public final a(I)LX/BVG;
    .locals 1

    .prologue
    .line 1790526
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790527
    iput p1, p0, LX/BVG;->a:I

    .line 1790528
    return-object p0

    .line 1790529
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()[B
    .locals 9

    .prologue
    .line 1790460
    new-instance v0, LX/0eX;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1790461
    const/16 v1, 0x19

    invoke-virtual {v0, v1}, LX/0eX;->b(I)V

    .line 1790462
    iget v1, p0, LX/BVG;->a:I

    const/4 v2, 0x0

    .line 1790463
    invoke-virtual {v0, v2, v1, v2}, LX/0eX;->b(III)V

    .line 1790464
    iget v1, p0, LX/BVG;->b:I

    .line 1790465
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790466
    iget v1, p0, LX/BVG;->c:F

    iget v2, p0, LX/BVG;->d:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790467
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790468
    iget v1, p0, LX/BVG;->g:F

    iget v2, p0, LX/BVG;->h:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790469
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790470
    iget v1, p0, LX/BVG;->e:F

    iget v2, p0, LX/BVG;->f:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790471
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790472
    iget v1, p0, LX/BVG;->i:F

    iget v2, p0, LX/BVG;->j:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790473
    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790474
    iget-boolean v1, p0, LX/BVG;->k:Z

    .line 1790475
    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->a(IZZ)V

    .line 1790476
    iget v1, p0, LX/BVG;->l:F

    iget v2, p0, LX/BVG;->m:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790477
    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790478
    iget v1, p0, LX/BVG;->n:F

    iget v2, p0, LX/BVG;->o:F

    iget v3, p0, LX/BVG;->p:F

    iget v4, p0, LX/BVG;->q:F

    invoke-static {v0, v1, v2, v3, v4}, LX/BEW;->a(LX/0eX;FFFF)I

    move-result v1

    .line 1790479
    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790480
    iget v1, p0, LX/BVG;->r:F

    iget v2, p0, LX/BVG;->s:F

    iget v3, p0, LX/BVG;->t:F

    iget v4, p0, LX/BVG;->u:F

    invoke-static {v0, v1, v2, v3, v4}, LX/BEW;->a(LX/0eX;FFFF)I

    move-result v1

    .line 1790481
    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790482
    iget v1, p0, LX/BVG;->v:F

    .line 1790483
    const/16 v5, 0xa

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790484
    iget v1, p0, LX/BVG;->w:F

    .line 1790485
    const/16 v5, 0xb

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790486
    iget v1, p0, LX/BVG;->x:F

    .line 1790487
    const/16 v5, 0xc

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790488
    iget v1, p0, LX/BVG;->y:I

    .line 1790489
    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790490
    iget v1, p0, LX/BVG;->z:I

    .line 1790491
    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790492
    iget v1, p0, LX/BVG;->A:F

    iget v2, p0, LX/BVG;->B:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790493
    const/16 v2, 0xf

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790494
    iget v1, p0, LX/BVG;->C:F

    iget v2, p0, LX/BVG;->D:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790495
    const/16 v2, 0x10

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790496
    iget v1, p0, LX/BVG;->E:F

    .line 1790497
    const/16 v5, 0x11

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790498
    iget v1, p0, LX/BVG;->F:F

    .line 1790499
    const/16 v5, 0x12

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790500
    iget v1, p0, LX/BVG;->G:F

    .line 1790501
    const/16 v5, 0x13

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790502
    iget v1, p0, LX/BVG;->H:F

    .line 1790503
    const/16 v5, 0x14

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790504
    iget v1, p0, LX/BVG;->I:F

    .line 1790505
    const/16 v5, 0x15

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790506
    iget v1, p0, LX/BVG;->J:F

    .line 1790507
    const/16 v5, 0x16

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790508
    iget v1, p0, LX/BVG;->K:F

    .line 1790509
    const/16 v5, 0x17

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790510
    iget v1, p0, LX/BVG;->L:F

    .line 1790511
    const/16 v5, 0x18

    const-wide/16 v7, 0x0

    invoke-virtual {v0, v5, v1, v7, v8}, LX/0eX;->a(IFD)V

    .line 1790512
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v1

    .line 1790513
    move v1, v1

    .line 1790514
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1790515
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v0

    return-object v0
.end method

.method public final b(F)LX/BVG;
    .locals 0

    .prologue
    .line 1790516
    iput p1, p0, LX/BVG;->K:F

    .line 1790517
    return-object p0
.end method

.method public final b(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790518
    iput p1, p0, LX/BVG;->i:F

    .line 1790519
    iput p2, p0, LX/BVG;->j:F

    .line 1790520
    return-object p0
.end method

.method public final b(FFFF)LX/BVG;
    .locals 0

    .prologue
    .line 1790521
    iput p1, p0, LX/BVG;->r:F

    .line 1790522
    iput p2, p0, LX/BVG;->s:F

    .line 1790523
    iput p3, p0, LX/BVG;->t:F

    .line 1790524
    iput p4, p0, LX/BVG;->u:F

    .line 1790525
    return-object p0
.end method

.method public final b(I)LX/BVG;
    .locals 1

    .prologue
    .line 1790403
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790404
    iput p1, p0, LX/BVG;->b:I

    .line 1790405
    return-object p0

    .line 1790406
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(F)LX/BVG;
    .locals 0

    .prologue
    .line 1790407
    iput p1, p0, LX/BVG;->L:F

    .line 1790408
    return-object p0
.end method

.method public final c(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790363
    iput p1, p0, LX/BVG;->c:F

    .line 1790364
    iput p2, p0, LX/BVG;->d:F

    .line 1790365
    return-object p0
.end method

.method public final c(I)LX/BVG;
    .locals 1

    .prologue
    .line 1790366
    if-gez p1, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790367
    iput p1, p0, LX/BVG;->y:I

    .line 1790368
    return-object p0

    .line 1790369
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790370
    iput p1, p0, LX/BVG;->g:F

    .line 1790371
    iput p2, p0, LX/BVG;->h:F

    .line 1790372
    return-object p0
.end method

.method public final d(I)LX/BVG;
    .locals 1

    .prologue
    .line 1790373
    if-gez p1, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790374
    iput p1, p0, LX/BVG;->z:I

    .line 1790375
    return-object p0

    .line 1790376
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790400
    iput p1, p0, LX/BVG;->l:F

    .line 1790401
    iput p2, p0, LX/BVG;->m:F

    .line 1790402
    return-object p0
.end method

.method public final f(FF)LX/BVG;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1790377
    cmpl-float v0, p2, p1

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790378
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 1790379
    iput p1, p0, LX/BVG;->w:F

    .line 1790380
    iput p2, p0, LX/BVG;->x:F

    .line 1790381
    return-object p0

    :cond_0
    move v0, v2

    .line 1790382
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1790383
    goto :goto_1
.end method

.method public final g(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790384
    iput p1, p0, LX/BVG;->A:F

    .line 1790385
    iput p2, p0, LX/BVG;->B:F

    .line 1790386
    return-object p0
.end method

.method public final h(FF)LX/BVG;
    .locals 0

    .prologue
    .line 1790387
    iput p1, p0, LX/BVG;->C:F

    .line 1790388
    iput p2, p0, LX/BVG;->D:F

    .line 1790389
    return-object p0
.end method

.method public final i(FF)LX/BVG;
    .locals 1

    .prologue
    .line 1790390
    cmpl-float v0, p2, p1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790391
    iput p1, p0, LX/BVG;->E:F

    .line 1790392
    iput p2, p0, LX/BVG;->F:F

    .line 1790393
    return-object p0

    .line 1790394
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(FF)LX/BVG;
    .locals 1

    .prologue
    .line 1790395
    cmpl-float v0, p2, p1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790396
    iput p1, p0, LX/BVG;->G:F

    .line 1790397
    iput p2, p0, LX/BVG;->H:F

    .line 1790398
    return-object p0

    .line 1790399
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k(FF)LX/BVG;
    .locals 1

    .prologue
    .line 1790358
    cmpl-float v0, p2, p1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1790359
    iput p1, p0, LX/BVG;->I:F

    .line 1790360
    iput p2, p0, LX/BVG;->J:F

    .line 1790361
    return-object p0

    .line 1790362
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
