.class public LX/Bya;
.super LX/37T;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStory;

.field private final b:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Byd;Landroid/content/Context;LX/1Uf;LX/0Or;)V
    .locals 6
    .param p1    # LX/Byd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Byd;",
            "Landroid/content/Context;",
            "LX/1Uf;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837469
    invoke-direct {p0}, LX/37T;-><init>()V

    .line 1837470
    iget-object v0, p1, LX/Byd;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837471
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1837472
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, LX/Bya;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1837473
    new-instance v0, LX/ByZ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v4}, LX/ByZ;-><init>(LX/Byd;Landroid/content/Context;LX/1Uf;LX/0Or;)V

    iput-object v0, p0, LX/Bya;->b:LX/1KL;

    .line 1837474
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 1837475
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837476
    iget-object v0, p0, LX/Bya;->b:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1837477
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 1837478
    iget-object v0, p0, LX/Bya;->a:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
