.class public LX/BzX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35j;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BzX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/35j;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838844
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1838845
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BzX;->b:LX/0Zi;

    .line 1838846
    iput-object p1, p0, LX/BzX;->a:LX/0Ot;

    .line 1838847
    return-void
.end method

.method public static a(LX/0QB;)LX/BzX;
    .locals 4

    .prologue
    .line 1838848
    const-class v1, LX/BzX;

    monitor-enter v1

    .line 1838849
    :try_start_0
    sget-object v0, LX/BzX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838850
    sput-object v2, LX/BzX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838851
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838852
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838853
    new-instance v3, LX/BzX;

    const/16 p0, 0x803

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BzX;-><init>(LX/0Ot;)V

    .line 1838854
    move-object v0, v3

    .line 1838855
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838856
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838857
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1838859
    check-cast p2, LX/BzW;

    .line 1838860
    iget-object v0, p0, LX/BzX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35j;

    iget-object v2, p2, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/BzW;->b:LX/1Po;

    iget-boolean v4, p2, LX/BzW;->c:Z

    iget-boolean v5, p2, LX/BzW;->d:Z

    iget-object v6, p2, LX/BzW;->e:LX/33B;

    move-object v1, p1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1838861
    const/4 v8, 0x0

    .line 1838862
    invoke-static {v3}, LX/35u;->a(LX/1Po;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, v0, LX/35j;->f:LX/0Uh;

    sget v12, LX/2SU;->V:I

    invoke-virtual {v7, v12, v8}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v0, LX/35j;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-virtual {v7}, LX/0kb;->g()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, v0, LX/35j;->f:LX/0Uh;

    sget v12, LX/2SU;->U:I

    invoke-virtual {v7, v12, v8}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_0
    move v7, v8

    .line 1838863
    :goto_0
    move v12, v7

    .line 1838864
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1838865
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838866
    iget-object v8, v0, LX/35j;->b:LX/2sO;

    invoke-virtual {v8, v2}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v8

    .line 1838867
    if-nez v8, :cond_6

    .line 1838868
    const/4 v8, 0x0

    .line 1838869
    :goto_1
    move-object p0, v8

    .line 1838870
    invoke-static {v7}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object p1

    .line 1838871
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    if-eqz v4, :cond_2

    move-object v8, v9

    :goto_2
    invoke-interface {p2, v8}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v8

    iget-object p2, v0, LX/35j;->e:LX/Ap5;

    invoke-virtual {p2, v1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object p2

    invoke-virtual {p2, v11}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/Ap4;->b(LX/1X1;)LX/Ap4;

    move-result-object p0

    iget-object p2, v0, LX/35j;->a:LX/1qb;

    invoke-virtual {p2, v7}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/Ap4;->b(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    .line 1838872
    iget-object p1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1838873
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p1}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object p1

    move-object p1, p1

    .line 1838874
    invoke-virtual {p0, p1}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    if-eqz v12, :cond_1

    .line 1838875
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    const v12, 0x3ff745d1

    iget-object p1, v0, LX/35j;->a:LX/1qb;

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/1qb;)LX/2yA;

    move-result-object p1

    invoke-static {v9, v12, p1}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    move-object v9, v9

    .line 1838876
    :cond_1
    invoke-virtual {p0, v9}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v9

    if-nez v5, :cond_3

    .line 1838877
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1838878
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v7}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleShareAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v7

    move v7, v7

    .line 1838879
    if-eqz v7, :cond_3

    move v7, v10

    :goto_3
    invoke-virtual {v9, v7}, LX/Ap4;->i(I)LX/Ap4;

    move-result-object v7

    .line 1838880
    iget-object v9, v7, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object v6, v9, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    .line 1838881
    move-object v7, v7

    .line 1838882
    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 1838883
    return-object v0

    .line 1838884
    :cond_2
    const v8, 0x245c9afb

    const/4 v4, 0x0

    invoke-static {v1, v8, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v8, v8

    .line 1838885
    goto :goto_2

    :cond_3
    move v7, v11

    goto :goto_3

    .line 1838886
    :cond_4
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1838887
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    if-eqz v7, :cond_5

    iget-object v12, v0, LX/35j;->c:LX/2y3;

    .line 1838888
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1838889
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    invoke-virtual {v12, v7}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_5
    move v7, v8

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v8, v1, v3, v2, v10}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v8

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1838890
    invoke-static {}, LX/1dS;->b()V

    .line 1838891
    iget v0, p1, LX/1dQ;->b:I

    .line 1838892
    packed-switch v0, :pswitch_data_0

    .line 1838893
    :goto_0
    return-object v2

    .line 1838894
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1838895
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1838896
    check-cast v1, LX/BzW;

    .line 1838897
    iget-object v3, p0, LX/BzX;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/35j;

    iget-object p1, v1, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/BzW;->b:LX/1Po;

    .line 1838898
    iget-object p0, v3, LX/35j;->d:LX/C0D;

    check-cast p2, LX/1Pq;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1838899
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x245c9afb
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/BzV;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/BzX",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1838900
    new-instance v1, LX/BzW;

    invoke-direct {v1, p0}, LX/BzW;-><init>(LX/BzX;)V

    .line 1838901
    iget-object v2, p0, LX/BzX;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BzV;

    .line 1838902
    if-nez v2, :cond_0

    .line 1838903
    new-instance v2, LX/BzV;

    invoke-direct {v2, p0}, LX/BzV;-><init>(LX/BzX;)V

    .line 1838904
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BzV;->a$redex0(LX/BzV;LX/1De;IILX/BzW;)V

    .line 1838905
    move-object v1, v2

    .line 1838906
    move-object v0, v1

    .line 1838907
    return-object v0
.end method
