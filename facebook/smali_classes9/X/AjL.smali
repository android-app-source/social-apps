.class public final LX/AjL;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/18G;

.field public final synthetic c:Lcom/facebook/feed/data/FeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;ZLX/18G;)V
    .locals 0

    .prologue
    .line 1707827
    iput-object p1, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iput-boolean p2, p0, LX/AjL;->a:Z

    iput-object p3, p0, LX/AjL;->b:LX/18G;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1707840
    iget-object v0, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->r:LX/1Mv;

    .line 1707841
    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 0

    .prologue
    .line 1707838
    invoke-direct {p0}, LX/AjL;->a()V

    .line 1707839
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 1707836
    invoke-direct {p0}, LX/AjL;->a()V

    .line 1707837
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1707828
    const/4 v2, 0x0

    .line 1707829
    invoke-direct {p0}, LX/AjL;->a()V

    .line 1707830
    iget-object v0, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, LX/0gD;->t()V

    .line 1707831
    iget-object v0, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v1, p0, LX/AjL;->a:Z

    iget-object v3, p0, LX/AjL;->b:LX/18G;

    invoke-static {v0, v1, v2, v3, v2}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;)V

    .line 1707832
    iget-object v0, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v1, p0, LX/AjL;->a:Z

    iget-object v3, p0, LX/AjL;->b:LX/18G;

    const/4 v5, -0x1

    sget-object v6, LX/0qw;->FULL:LX/0qw;

    sget-object v7, LX/18O;->Perform:LX/18O;

    move-object v4, v2

    .line 1707833
    invoke-static/range {v0 .. v7}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V

    .line 1707834
    iget-object v0, p0, LX/AjL;->c:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, Lcom/facebook/feed/data/FeedDataLoader;->j()Z

    .line 1707835
    return-void
.end method
