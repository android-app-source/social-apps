.class public final LX/ByZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1eK;

.field private final e:Ljava/lang/String;

.field public final f:Landroid/content/Context;

.field private final g:LX/1Uf;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Byd;Landroid/content/Context;LX/1Uf;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Byd;",
            "Landroid/content/Context;",
            "LX/1Uf;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1837447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837448
    iput-object p2, p0, LX/ByZ;->f:Landroid/content/Context;

    .line 1837449
    iput-object p3, p0, LX/ByZ;->g:LX/1Uf;

    .line 1837450
    iput-object p4, p0, LX/ByZ;->h:LX/0Or;

    .line 1837451
    iget-object v0, p1, LX/Byd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837452
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1837453
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/ByZ;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837454
    iget-object v0, p1, LX/Byd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, LX/ByZ;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837455
    iget-object v0, p1, LX/Byd;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/ByZ;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1837456
    iget-object v0, p1, LX/Byd;->b:LX/1eK;

    iput-object v0, p0, LX/ByZ;->d:LX/1eK;

    .line 1837457
    iget-object v0, p1, LX/Byd;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837458
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1837459
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v1, p1, LX/Byd;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/6Bx;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ByZ;->e:Ljava/lang/String;

    .line 1837460
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1837461
    const/4 v5, 0x0

    .line 1837462
    iget-object v0, p0, LX/ByZ;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ByZ;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1837463
    :cond_0
    new-instance v0, LX/1yT;

    new-instance v1, LX/1zR;

    const-string v2, ""

    invoke-direct {v1, v2}, LX/1zR;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v5}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 1837464
    :goto_0
    return-object v0

    .line 1837465
    :cond_1
    iget-object v0, p0, LX/ByZ;->g:LX/1Uf;

    iget-object v1, p0, LX/ByZ;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v1}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/ByZ;->d:LX/1eK;

    iget-object v4, p0, LX/ByZ;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1837466
    iget-object v1, p0, LX/ByZ;->g:LX/1Uf;

    new-instance v2, LX/ByY;

    invoke-direct {v2, p0}, LX/ByY;-><init>(LX/ByZ;)V

    invoke-virtual {v1, v0, v2}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v1

    .line 1837467
    new-instance v2, LX/1yT;

    if-nez v1, :cond_2

    :goto_1
    invoke-direct {v2, v0, v5}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    move-object v0, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1837468
    iget-object v0, p0, LX/ByZ;->e:Ljava/lang/String;

    return-object v0
.end method
