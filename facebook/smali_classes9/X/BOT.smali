.class public final enum LX/BOT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BOT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BOT;

.field public static final enum DATE:LX/BOT;

.field public static final enum TIME:LX/BOT;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1779829
    new-instance v0, LX/BOT;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v2}, LX/BOT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOT;->DATE:LX/BOT;

    .line 1779830
    new-instance v0, LX/BOT;

    const-string v1, "TIME"

    invoke-direct {v0, v1, v3}, LX/BOT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOT;->TIME:LX/BOT;

    .line 1779831
    const/4 v0, 0x2

    new-array v0, v0, [LX/BOT;

    sget-object v1, LX/BOT;->DATE:LX/BOT;

    aput-object v1, v0, v2

    sget-object v1, LX/BOT;->TIME:LX/BOT;

    aput-object v1, v0, v3

    sput-object v0, LX/BOT;->$VALUES:[LX/BOT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1779832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BOT;
    .locals 1

    .prologue
    .line 1779833
    const-class v0, LX/BOT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BOT;

    return-object v0
.end method

.method public static values()[LX/BOT;
    .locals 1

    .prologue
    .line 1779834
    sget-object v0, LX/BOT;->$VALUES:[LX/BOT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BOT;

    return-object v0
.end method
