.class public final LX/CVC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1902265
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1902266
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902267
    :goto_0
    return v1

    .line 1902268
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902269
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1902270
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1902271
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1902273
    const-string v4, "confirmation_description"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1902274
    const/4 v3, 0x0

    .line 1902275
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1902276
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902277
    :goto_2
    move v2, v3

    .line 1902278
    goto :goto_1

    .line 1902279
    :cond_2
    const-string v4, "confirmation_heading"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1902280
    const/4 v3, 0x0

    .line 1902281
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_c

    .line 1902282
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902283
    :goto_3
    move v0, v3

    .line 1902284
    goto :goto_1

    .line 1902285
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1902286
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1902287
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1902288
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1902289
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902290
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1902291
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1902292
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902293
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1902294
    const-string v5, "plain_text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1902295
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 1902296
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1902297
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1902298
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_4

    .line 1902299
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902300
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_b

    .line 1902301
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1902302
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902303
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_a

    if-eqz v4, :cond_a

    .line 1902304
    const-string v5, "plain_text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1902305
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1902306
    :cond_b
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1902307
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1902308
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_c
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1902309
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1902310
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1902311
    if-eqz v0, :cond_1

    .line 1902312
    const-string v1, "confirmation_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902313
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1902314
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1902315
    if-eqz v1, :cond_0

    .line 1902316
    const-string p3, "plain_text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902317
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1902318
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1902319
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1902320
    if-eqz v0, :cond_3

    .line 1902321
    const-string v1, "confirmation_heading"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902322
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1902323
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1902324
    if-eqz v1, :cond_2

    .line 1902325
    const-string p1, "plain_text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902326
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1902327
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1902328
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1902329
    return-void
.end method
