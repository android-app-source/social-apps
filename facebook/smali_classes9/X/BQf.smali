.class public final LX/BQf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1782480
    iput-object p1, p0, LX/BQf;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iput-object p2, p0, LX/BQf;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1782481
    iget-object v0, p0, LX/BQf;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v0, v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQQ;

    iget-object v1, p0, LX/BQf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BQQ;->b(Ljava/lang/String;)V

    .line 1782482
    iget-object v0, p0, LX/BQf;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v0, v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->d:Landroid/content/Context;

    const v1, 0x7f08273f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1782483
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782484
    iget-object v0, p0, LX/BQf;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v0, v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQQ;

    iget-object v1, p0, LX/BQf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BQQ;->b(Ljava/lang/String;)V

    .line 1782485
    iget-object v0, p0, LX/BQf;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v0, v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQP;

    invoke-virtual {v0}, LX/BQP;->d()V

    .line 1782486
    return-void
.end method
