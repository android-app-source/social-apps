.class public final LX/Axm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1728854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1728855
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    check-cast p2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1728856
    invoke-virtual {p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->d()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->d()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method
