.class public LX/B30;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B30;


# instance fields
.field public a:LX/0sa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1739419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739420
    return-void
.end method

.method public static a(LX/0QB;)LX/B30;
    .locals 4

    .prologue
    .line 1739421
    sget-object v0, LX/B30;->b:LX/B30;

    if-nez v0, :cond_1

    .line 1739422
    const-class v1, LX/B30;

    monitor-enter v1

    .line 1739423
    :try_start_0
    sget-object v0, LX/B30;->b:LX/B30;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1739424
    if-eqz v2, :cond_0

    .line 1739425
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1739426
    new-instance p0, LX/B30;

    invoke-direct {p0}, LX/B30;-><init>()V

    .line 1739427
    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    .line 1739428
    iput-object v3, p0, LX/B30;->a:LX/0sa;

    .line 1739429
    move-object v0, p0

    .line 1739430
    sput-object v0, LX/B30;->b:LX/B30;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1739431
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1739432
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1739433
    :cond_1
    sget-object v0, LX/B30;->b:LX/B30;

    return-object v0

    .line 1739434
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1739435
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
