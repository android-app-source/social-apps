.class public final LX/Als;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/Alu;

.field private final b:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Alu;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710309
    iput-object p1, p0, LX/Als;->a:LX/Alu;

    .line 1710310
    iput-object p2, p0, LX/Als;->b:LX/1Nq;

    .line 1710311
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 3
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1710312
    iget-object v1, p0, LX/Als;->a:LX/Alu;

    iget-object v0, p0, LX/Als;->b:LX/1Nq;

    const-class v2, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    invoke-virtual {v0, p1, v2}, LX/1Nq;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    .line 1710313
    new-instance p1, LX/Alt;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/BMQ;->b(LX/0QB;)LX/BMQ;

    move-result-object p0

    check-cast p0, LX/BMQ;

    invoke-direct {p1, p2, v0, v2, p0}, LX/Alt;-><init>(LX/B5j;Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;Landroid/content/Context;LX/BMQ;)V

    .line 1710314
    move-object v0, p1

    .line 1710315
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1710316
    const-string v0, "PHOTO_REMINDER_PERSIST_KEY"

    return-object v0
.end method
