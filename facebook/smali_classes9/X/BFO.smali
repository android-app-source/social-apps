.class public final LX/BFO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/89m;

.field public b:LX/89g;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/21y;

.field public j:Z

.field public k:LX/21C;

.field public l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public m:Lcom/facebook/graphql/model/GraphQLComment;

.field public n:Lcom/facebook/graphql/model/GraphQLComment;

.field public o:Z

.field public p:Z

.field public q:I

.field public r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

.field public v:Lcom/facebook/tagging/model/TaggingProfile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1765669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/permalink/PermalinkParams;)LX/BFO;
    .locals 2

    .prologue
    .line 1765675
    new-instance v0, LX/BFO;

    invoke-direct {v0}, LX/BFO;-><init>()V

    .line 1765676
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    move-object v1, v1

    .line 1765677
    iput-object v1, v0, LX/BFO;->a:LX/89m;

    .line 1765678
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    move-object v1, v1

    .line 1765679
    iput-object v1, v0, LX/BFO;->b:LX/89g;

    .line 1765680
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1765681
    iput-object v1, v0, LX/BFO;->c:Ljava/lang/String;

    .line 1765682
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1765683
    iput-object v1, v0, LX/BFO;->d:Ljava/lang/String;

    .line 1765684
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1765685
    iput-object v1, v0, LX/BFO;->e:Ljava/lang/String;

    .line 1765686
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1765687
    iput-object v1, v0, LX/BFO;->f:Ljava/lang/String;

    .line 1765688
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1765689
    iput-object v1, v0, LX/BFO;->g:Ljava/lang/String;

    .line 1765690
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    move-object v1, v1

    .line 1765691
    iput-object v1, v0, LX/BFO;->i:LX/21y;

    .line 1765692
    iget-boolean v1, p0, Lcom/facebook/permalink/PermalinkParams;->j:Z

    move v1, v1

    .line 1765693
    iput-boolean v1, v0, LX/BFO;->j:Z

    .line 1765694
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-object v1, v1

    .line 1765695
    iput-object v1, v0, LX/BFO;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1765696
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v1, v1

    .line 1765697
    iput-object v1, v0, LX/BFO;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765698
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v1, v1

    .line 1765699
    iput-object v1, v0, LX/BFO;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765700
    iget-boolean v1, p0, Lcom/facebook/permalink/PermalinkParams;->o:Z

    move v1, v1

    .line 1765701
    iput-boolean v1, v0, LX/BFO;->o:Z

    .line 1765702
    iget-boolean v1, p0, Lcom/facebook/permalink/PermalinkParams;->p:Z

    move v1, v1

    .line 1765703
    iput-boolean v1, v0, LX/BFO;->p:Z

    .line 1765704
    iget v1, p0, Lcom/facebook/permalink/PermalinkParams;->q:I

    move v1, v1

    .line 1765705
    iput v1, v0, LX/BFO;->q:I

    .line 1765706
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 1765707
    iput-object v1, v0, LX/BFO;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1765708
    iget-boolean v1, p0, Lcom/facebook/permalink/PermalinkParams;->t:Z

    move v1, v1

    .line 1765709
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BFO;->a(Ljava/lang/Boolean;)LX/BFO;

    .line 1765710
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-object v1, v1

    .line 1765711
    iput-object v1, v0, LX/BFO;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1765712
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    move-object v1, v1

    .line 1765713
    iput-object v1, v0, LX/BFO;->v:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1765714
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)LX/BFO;
    .locals 1

    .prologue
    .line 1765673
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/BFO;->t:Z

    .line 1765674
    return-object p0
.end method

.method public final a()Lcom/facebook/permalink/PermalinkParams;
    .locals 2

    .prologue
    .line 1765670
    iget-object v0, p0, LX/BFO;->a:LX/89m;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1765671
    new-instance v0, Lcom/facebook/permalink/PermalinkParams;

    invoke-direct {v0, p0}, Lcom/facebook/permalink/PermalinkParams;-><init>(LX/BFO;)V

    .line 1765672
    return-object v0
.end method
