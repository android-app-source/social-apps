.class public LX/Ah5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile d:LX/Ah5;


# instance fields
.field private final b:LX/0if;

.field public c:LX/Ah4;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1702109
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_HARD_CODE_option1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DO_NOT_HARD_CODE_option2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "DO_NOT_HARD_CODE_option3"

    aput-object v2, v0, v1

    sput-object v0, LX/Ah5;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1702106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1702107
    iput-object p1, p0, LX/Ah5;->b:LX/0if;

    .line 1702108
    return-void
.end method

.method public static a(LX/0QB;)LX/Ah5;
    .locals 3

    .prologue
    .line 1702126
    sget-object v0, LX/Ah5;->d:LX/Ah5;

    if-nez v0, :cond_1

    .line 1702127
    const-class v1, LX/Ah5;

    monitor-enter v1

    .line 1702128
    :try_start_0
    sget-object v0, LX/Ah5;->d:LX/Ah5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1702129
    if-eqz v2, :cond_0

    .line 1702130
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Ah5;->b(LX/0QB;)LX/Ah5;

    move-result-object v0

    sput-object v0, LX/Ah5;->d:LX/Ah5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1702131
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1702132
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1702133
    :cond_1
    sget-object v0, LX/Ah5;->d:LX/Ah5;

    return-object v0

    .line 1702134
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1702135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/Ah5;
    .locals 2

    .prologue
    .line 1702136
    new-instance v1, LX/Ah5;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    invoke-direct {v1, v0}, LX/Ah5;-><init>(LX/0if;)V

    .line 1702137
    return-object v1
.end method

.method private f()LX/1rQ;
    .locals 3

    .prologue
    .line 1702138
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "video_id"

    iget-object v2, p0, LX/Ah5;->c:LX/Ah4;

    iget-object v2, v2, LX/Ah4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "broadcaster_id"

    iget-object v2, p0, LX/Ah5;->c:LX/Ah4;

    iget-object v2, v2, LX/Ah4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "use_overlay_design"

    iget-object v2, p0, LX/Ah5;->c:LX/Ah4;

    iget-boolean v2, v2, LX/Ah4;->c:Z

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1702120
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1702121
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "start_session"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1702122
    return-void
.end method

.method public final a(I)V
    .locals 7

    .prologue
    .line 1702123
    if-ltz p1, :cond_0

    sget-object v0, LX/Ah5;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 1702124
    :cond_0
    :goto_0
    return-void

    .line 1702125
    :cond_1
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "click_option"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    const-string v5, "option"

    sget-object v6, LX/Ah5;->a:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v4, v5, v6}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 1702118
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "click_payment_method"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    const-string v5, "has_payment_method"

    invoke-virtual {v4, v5, p1}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1702119
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1702116
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "click_send"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1702117
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1702114
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "send_success"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1702115
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 1702112
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    const-string v2, "send_failure"

    const/4 v3, 0x0

    invoke-direct {p0}, LX/Ah5;->f()LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1702113
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1702110
    iget-object v0, p0, LX/Ah5;->b:LX/0if;

    sget-object v1, LX/0ig;->F:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1702111
    return-void
.end method
