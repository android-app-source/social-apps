.class public final LX/CP7;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CP7;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CP5;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CP8;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883982
    const/4 v0, 0x0

    sput-object v0, LX/CP7;->a:LX/CP7;

    .line 1883983
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CP7;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883984
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883985
    new-instance v0, LX/CP8;

    invoke-direct {v0}, LX/CP8;-><init>()V

    iput-object v0, p0, LX/CP7;->c:LX/CP8;

    .line 1883986
    return-void
.end method

.method public static declared-synchronized q()LX/CP7;
    .locals 2

    .prologue
    .line 1883987
    const-class v1, LX/CP7;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CP7;->a:LX/CP7;

    if-nez v0, :cond_0

    .line 1883988
    new-instance v0, LX/CP7;

    invoke-direct {v0}, LX/CP7;-><init>()V

    sput-object v0, LX/CP7;->a:LX/CP7;

    .line 1883989
    :cond_0
    sget-object v0, LX/CP7;->a:LX/CP7;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883990
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1883991
    check-cast p2, LX/CP6;

    .line 1883992
    iget-object v0, p2, LX/CP6;->a:LX/CNb;

    iget-object v1, p2, LX/CP6;->b:LX/CNc;

    iget-object v2, p2, LX/CP6;->c:Ljava/util/List;

    const/4 p2, 0x1

    .line 1883993
    const-string v3, "children"

    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1883994
    if-nez v3, :cond_0

    .line 1883995
    const/4 v3, 0x0

    .line 1883996
    :goto_0
    move-object v0, v3

    .line 1883997
    return-object v0

    .line 1883998
    :cond_0
    invoke-static {v3, v1, p1}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v3

    .line 1883999
    invoke-static {p1, v3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    .line 1884000
    const-string v4, "bottom"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    .line 1884001
    const-string v5, "top"

    invoke-virtual {v0, v5}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v5

    .line 1884002
    const-string v6, "right"

    invoke-virtual {v0, v6}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v6

    .line 1884003
    const-string v7, "left"

    invoke-virtual {v0, v7}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v7

    .line 1884004
    if-eqz v4, :cond_1

    .line 1884005
    const/4 v8, 0x3

    const-string p0, "bottom"

    invoke-virtual {v0, p0, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result p0

    invoke-interface {v3, v8, p0}, LX/1Di;->k(II)LX/1Di;

    .line 1884006
    :cond_1
    if-nez v5, :cond_2

    if-nez v4, :cond_3

    .line 1884007
    :cond_2
    const-string v4, "top"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    invoke-interface {v3, p2, v4}, LX/1Di;->k(II)LX/1Di;

    .line 1884008
    :cond_3
    if-eqz v6, :cond_4

    .line 1884009
    const/4 v4, 0x2

    const-string v5, "right"

    invoke-virtual {v0, v5, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v5

    invoke-interface {v3, v4, v5}, LX/1Di;->k(II)LX/1Di;

    .line 1884010
    :cond_4
    if-nez v7, :cond_5

    if-nez v6, :cond_6

    .line 1884011
    :cond_5
    const/4 v4, 0x0

    const-string v5, "left"

    invoke-virtual {v0, v5, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v5

    invoke-interface {v3, v4, v5}, LX/1Di;->k(II)LX/1Di;

    .line 1884012
    :cond_6
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884013
    if-eqz v4, :cond_7

    .line 1884014
    const v5, -0x1e23ea52

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884015
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884016
    :cond_7
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884017
    if-eqz v4, :cond_8

    .line 1884018
    const v5, -0x391c5fb5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884019
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884020
    :cond_8
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884021
    invoke-static {}, LX/1dS;->b()V

    .line 1884022
    iget v0, p1, LX/1dQ;->b:I

    .line 1884023
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884024
    :goto_0
    return-object v0

    .line 1884025
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884026
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884027
    move-object v0, v1

    .line 1884028
    goto :goto_0

    .line 1884029
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884030
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884031
    const/4 v2, 0x1

    move v2, v2

    .line 1884032
    move v0, v2

    .line 1884033
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x391c5fb5 -> :sswitch_1
        -0x1e23ea52 -> :sswitch_0
    .end sparse-switch
.end method
