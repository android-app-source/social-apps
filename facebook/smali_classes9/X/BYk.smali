.class public LX/BYk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BAB;


# instance fields
.field private a:I

.field private b:Z

.field public c:LX/BYj;

.field public d:LX/BZR;

.field public e:Z

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Avi;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/7ex;

.field public final h:LX/BZS;


# direct methods
.method public constructor <init>(LX/7ex;LX/BZS;)V
    .locals 1

    .prologue
    .line 1796496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796497
    iput-object p1, p0, LX/BYk;->g:LX/7ex;

    .line 1796498
    iput-object p2, p0, LX/BYk;->h:LX/BZS;

    .line 1796499
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/BYk;->f:Ljava/util/Set;

    .line 1796500
    return-void
.end method

.method public static c(LX/BYk;)V
    .locals 14

    .prologue
    .line 1796481
    iget-object v0, p0, LX/BYk;->d:LX/BZR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BYk;->c:LX/BYj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BYk;->e:Z

    if-eqz v0, :cond_0

    .line 1796482
    iget-object v0, p0, LX/BYk;->d:LX/BZR;

    .line 1796483
    iget-object v1, v0, LX/BZR;->i:Ljava/util/concurrent/Executor;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, LX/BZR;->g:Z

    if-eqz v1, :cond_1

    .line 1796484
    :cond_0
    :goto_0
    return-void

    .line 1796485
    :cond_1
    iget-object v1, v0, LX/BZR;->j:LX/BZP;

    if-nez v1, :cond_2

    .line 1796486
    new-instance v1, LX/BZP;

    .line 1796487
    new-instance v2, LX/BZQ;

    invoke-direct {v2, v0}, LX/BZQ;-><init>(LX/BZR;)V

    move-object v2, v2

    .line 1796488
    iget-object v3, v0, LX/BZR;->i:Ljava/util/concurrent/Executor;

    invoke-direct {v1, v2, v3}, LX/BZP;-><init>(LX/BZQ;Ljava/util/concurrent/Executor;)V

    iput-object v1, v0, LX/BZR;->j:LX/BZP;

    .line 1796489
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BZR;->g:Z

    .line 1796490
    iget-object v1, v0, LX/BZR;->j:LX/BZP;

    iget-boolean v2, v0, LX/BZR;->f:Z

    iget-object v3, v0, LX/BZR;->h:Landroid/content/Context;

    iget-object v4, v0, LX/BZR;->c:Ljava/lang/String;

    iget-object v5, v0, LX/BZR;->d:Ljava/lang/String;

    iget-object v6, v0, LX/BZR;->e:Ljava/lang/String;

    .line 1796491
    if-eqz v2, :cond_3

    .line 1796492
    iget-object v13, v1, LX/BZP;->b:Ljava/util/concurrent/Executor;

    new-instance v7, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;

    move-object v8, v1

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    move-object v12, v6

    invoke-direct/range {v7 .. v12}, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;-><init>(LX/BZP;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v8, 0x75aedd46

    invoke-static {v13, v7, v8}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1796493
    :goto_1
    goto :goto_0

    .line 1796494
    :cond_3
    iget-object v7, v1, LX/BZP;->b:Ljava/util/concurrent/Executor;

    new-instance v8, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$2;

    invoke-direct {v8, v1, v4, v5, v6}, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$2;-><init>(LX/BZP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v9, 0x6444c1e2

    invoke-static {v7, v8, v9}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1796495
    goto :goto_1
.end method

.method private static d(LX/BYk;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1796541
    const-string v1, "NativeMsqrdRenderer:handleFaceCountUpdate"

    invoke-static {v1}, LX/7ey;->a(Ljava/lang/String;)V

    .line 1796542
    :try_start_0
    iget-object v3, p0, LX/BYk;->h:LX/BZS;

    .line 1796543
    invoke-static {v3}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->getCurrentEffectNeedsFaceTracker(J)Z

    move-result v4

    move v3, v4

    .line 1796544
    if-nez v3, :cond_4

    .line 1796545
    const/4 v3, -0x2

    .line 1796546
    :goto_0
    move v1, v3

    .line 1796547
    iget-boolean v2, p0, LX/BYk;->b:Z

    if-nez v2, :cond_0

    iget v2, p0, LX/BYk;->a:I

    if-eq v2, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, LX/BYk;->b:Z

    .line 1796548
    iput v1, p0, LX/BYk;->a:I

    .line 1796549
    const/4 v0, 0x0

    .line 1796550
    iget-object v1, p0, LX/BYk;->f:Ljava/util/Set;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1796551
    :try_start_1
    iget-object v2, p0, LX/BYk;->f:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, LX/BYk;->b:Z

    if-eqz v2, :cond_2

    .line 1796552
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/BYk;->f:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1796553
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BYk;->b:Z

    .line 1796554
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1796555
    if-eqz v0, :cond_3

    .line 1796556
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Avi;

    .line 1796557
    iget v2, p0, LX/BYk;->a:I

    invoke-interface {v0, v2}, LX/Avi;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1796558
    :catchall_0
    move-exception v0

    invoke-static {}, LX/7ey;->a()V

    throw v0

    .line 1796559
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1796560
    :cond_3
    invoke-static {}, LX/7ey;->a()V

    .line 1796561
    return-void

    .line 1796562
    :cond_4
    iget-object v3, p0, LX/BYk;->h:LX/BZS;

    .line 1796563
    invoke-static {v3}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->isFaceTrackerReady(J)Z

    move-result v4

    move v3, v4

    .line 1796564
    if-nez v3, :cond_5

    .line 1796565
    const/4 v3, -0x1

    goto :goto_0

    .line 1796566
    :cond_5
    iget-object v3, p0, LX/BYk;->h:LX/BZS;

    .line 1796567
    invoke-static {v3}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->getFacesCount(J)I

    move-result v4

    move v3, v4

    .line 1796568
    goto :goto_0
.end method

.method public static f(LX/BYk;)V
    .locals 1

    .prologue
    .line 1796539
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BYk;->b:Z

    .line 1796540
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 1796519
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BYk;->e:Z

    .line 1796520
    iget-object v0, p0, LX/BYk;->h:LX/BZS;

    iget-object v1, p0, LX/BYk;->g:LX/7ex;

    .line 1796521
    iget-boolean v2, v1, LX/7ex;->b:Z

    move v1, v2

    .line 1796522
    iget-object v2, p0, LX/BYk;->g:LX/7ex;

    .line 1796523
    iget-boolean v3, v2, LX/7ex;->c:Z

    move v2, v3

    .line 1796524
    iget-object v3, p0, LX/BYk;->g:LX/7ex;

    .line 1796525
    iget v4, v3, LX/7ex;->d:I

    move v3, v4

    .line 1796526
    iget-object v4, p0, LX/BYk;->g:LX/7ex;

    .line 1796527
    iget v5, v4, LX/7ex;->e:I

    move v4, v5

    .line 1796528
    iget-object v5, p0, LX/BYk;->g:LX/7ex;

    .line 1796529
    iget v6, v5, LX/7ex;->f:I

    move v5, v6

    .line 1796530
    iget-object v6, p0, LX/BYk;->g:LX/7ex;

    .line 1796531
    iget-boolean v7, v6, LX/7ex;->g:Z

    move v6, v7

    .line 1796532
    invoke-static {v0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v7

    move v9, v1

    move v10, v2

    move v11, v3

    move v12, v4

    move v13, v5

    move v14, v6

    invoke-static/range {v7 .. v14}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->prepareGl(JZZIIIZ)V

    .line 1796533
    invoke-static {p0}, LX/BYk;->c(LX/BYk;)V

    .line 1796534
    iget-object v0, p0, LX/BYk;->c:LX/BYj;

    if-eqz v0, :cond_0

    .line 1796535
    iget-object v0, p0, LX/BYk;->h:LX/BZS;

    iget-object v1, p0, LX/BYk;->c:LX/BYj;

    .line 1796536
    iget-object v2, v1, LX/BYj;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1796537
    invoke-virtual {v0, v1}, LX/BZS;->a(Ljava/lang/String;)V

    .line 1796538
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1796515
    iget-boolean v0, p0, LX/BYk;->e:Z

    if-eqz v0, :cond_0

    .line 1796516
    iget-object v0, p0, LX/BYk;->h:LX/BZS;

    .line 1796517
    invoke-static {v0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v1

    invoke-static {v1, v2, p1, p2}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->resize(JII)V

    .line 1796518
    :cond_0
    return-void
.end method

.method public final a(LX/Avi;)V
    .locals 2

    .prologue
    .line 1796511
    iget-object v1, p0, LX/BYk;->f:Ljava/util/Set;

    monitor-enter v1

    .line 1796512
    :try_start_0
    iget-object v0, p0, LX/BYk;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1796513
    invoke-static {p0}, LX/BYk;->f(LX/BYk;)V

    .line 1796514
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1796504
    const-string v0, "NativeMsqrdRenderer:doFrame"

    invoke-static {v0}, LX/7ey;->a(Ljava/lang/String;)V

    .line 1796505
    :try_start_0
    iget-object v0, p0, LX/BYk;->c:LX/BYj;

    if-eqz v0, :cond_0

    .line 1796506
    iget-object v0, p0, LX/BYk;->h:LX/BZS;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/BZS;->a([F[F[FJ)V

    .line 1796507
    invoke-static {p0}, LX/BYk;->d(LX/BYk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1796508
    :cond_0
    invoke-static {}, LX/7ey;->a()V

    .line 1796509
    return-void

    .line 1796510
    :catchall_0
    move-exception v0

    invoke-static {}, LX/7ey;->a()V

    throw v0
.end method

.method public final b(LX/Avi;)V
    .locals 2

    .prologue
    .line 1796501
    iget-object v1, p0, LX/BYk;->f:Ljava/util/Set;

    monitor-enter v1

    .line 1796502
    :try_start_0
    iget-object v0, p0, LX/BYk;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1796503
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
