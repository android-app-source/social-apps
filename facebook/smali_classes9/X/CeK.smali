.class public LX/CeK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1924544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/175;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1924545
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1924546
    const/4 v0, 0x0

    .line 1924547
    :goto_0
    return-object v0

    .line 1924548
    :cond_0
    if-nez p1, :cond_1

    .line 1924549
    const-string p1, "%1$s"

    .line 1924550
    :cond_1
    const-string v0, "%1$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1924551
    if-gez v0, :cond_2

    .line 1924552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " %1$s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 1924553
    const-string v0, "%1$s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1924554
    :cond_2
    new-instance v1, LX/4af;

    invoke-direct {v1}, LX/4af;-><init>()V

    invoke-static {p1, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1924555
    iput-object v2, v1, LX/4af;->b:Ljava/lang/String;

    .line 1924556
    move-object v1, v1

    .line 1924557
    new-instance v2, LX/4ag;

    invoke-direct {v2}, LX/4ag;-><init>()V

    .line 1924558
    iput v0, v2, LX/4ag;->c:I

    .line 1924559
    move-object v0, v2

    .line 1924560
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 1924561
    iput v2, v0, LX/4ag;->b:I

    .line 1924562
    move-object v0, v0

    .line 1924563
    invoke-virtual {v0}, LX/4ag;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1924564
    iput-object v0, v1, LX/4af;->a:LX/0Px;

    .line 1924565
    move-object v0, v1

    .line 1924566
    invoke-virtual {v0}, LX/4af;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    goto :goto_0
.end method
