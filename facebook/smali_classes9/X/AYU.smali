.class public final LX/AYU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AYL;

.field public final synthetic b:LX/AYY;


# direct methods
.method public constructor <init>(LX/AYY;LX/AYL;)V
    .locals 0

    .prologue
    .line 1685543
    iput-object p1, p0, LX/AYU;->b:LX/AYY;

    iput-object p2, p0, LX/AYU;->a:LX/AYL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x561bf69e

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1685544
    iget-object v1, p0, LX/AYU;->b:LX/AYY;

    iget-object v1, v1, LX/AYY;->l:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->b()V

    .line 1685545
    iget-object v1, p0, LX/AYU;->a:LX/AYL;

    .line 1685546
    iget-object v3, v1, LX/AYL;->a:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-virtual {v3}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j()V

    .line 1685547
    iget-object v3, v1, LX/AYL;->a:Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    iget-object v3, v3, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    const/4 v5, 0x0

    .line 1685548
    iget-object v4, v3, LX/AZK;->F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-eqz v4, :cond_0

    .line 1685549
    iput v5, v3, LX/AZK;->H:I

    .line 1685550
    iget-object v4, v3, LX/AZK;->F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1685551
    iget-object v4, v3, LX/AZK;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1685552
    const/high16 v1, 0x41700000    # 15.0f

    .line 1685553
    invoke-virtual {v3}, LX/AZK;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 1685554
    iget p0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 p1, 0x50

    if-lt p0, p1, :cond_4

    .line 1685555
    iget-object v4, v3, LX/AZK;->t:Lcom/facebook/widget/text/BetterTextView;

    const/high16 p0, 0x41f00000    # 30.0f

    invoke-virtual {v4, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1685556
    iget-object v4, v3, LX/AZK;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1685557
    :goto_0
    iget-object v4, v3, LX/AZK;->n:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingViolation;

    invoke-virtual {v4, v5}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingViolation;->setVisibility(I)V

    .line 1685558
    :goto_1
    iget-object v4, v3, LX/AZK;->E:LX/AYN;

    if-eqz v4, :cond_0

    .line 1685559
    iget-object v4, v3, LX/AZK;->E:LX/AYN;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/AYN;->c(Z)V

    .line 1685560
    :cond_0
    const v1, -0x206ae616

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1685561
    :cond_1
    iget-object v4, v3, LX/AZK;->F:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v4, v4, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->onboardingFlowSteps:Ljava/util/List;

    iput-object v4, v3, LX/AZK;->G:Ljava/util/List;

    .line 1685562
    iget-object v4, v3, LX/AZK;->G:Ljava/util/List;

    sget-object v5, Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;->LEGAL_AGREEMENT:Lcom/facebook/video/videostreaming/protocol/CommercialBreakOnboardingFlowSteps;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1685563
    const/4 p1, 0x0

    .line 1685564
    const-string v4, "https://m.facebook.com/policies/instreamads/?show_chrome=false"

    invoke-static {v3, v4}, LX/AZK;->setCookies(LX/AZK;Ljava/lang/String;)V

    .line 1685565
    iget-boolean v4, v3, LX/AZK;->J:Z

    if-nez v4, :cond_2

    .line 1685566
    iget-object v4, v3, LX/AZK;->o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    new-instance v5, LX/AZI;

    invoke-direct {v5, v3}, LX/AZI;-><init>(LX/AZK;)V

    invoke-virtual {v4, v5}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1685567
    iget-object v4, v3, LX/AZK;->c:LX/48V;

    iget-object v5, v3, LX/AZK;->o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    const-string p0, "https://m.facebook.com/policies/instreamads/?show_chrome=false"

    invoke-virtual {v4, v5, p0}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1685568
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/AZK;->J:Z

    .line 1685569
    :cond_2
    iget-object v4, v3, LX/AZK;->o:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    invoke-virtual {v4, p1, p1}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->scrollTo(II)V

    .line 1685570
    :cond_3
    invoke-static {v3}, LX/AZK;->l(LX/AZK;)V

    goto :goto_1

    .line 1685571
    :cond_4
    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 p0, 0x3c

    if-lt v4, p0, :cond_5

    .line 1685572
    iget-object v4, v3, LX/AZK;->t:Lcom/facebook/widget/text/BetterTextView;

    const/high16 p0, 0x41a00000    # 20.0f

    invoke-virtual {v4, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1685573
    iget-object v4, v3, LX/AZK;->v:Lcom/facebook/widget/text/BetterTextView;

    const/high16 p0, 0x41200000    # 10.0f

    invoke-virtual {v4, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    goto :goto_0

    .line 1685574
    :cond_5
    iget-object v4, v3, LX/AZK;->t:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1685575
    iget-object v4, v3, LX/AZK;->v:Lcom/facebook/widget/text/BetterTextView;

    const/high16 p0, 0x41000000    # 8.0f

    invoke-virtual {v4, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    goto :goto_0
.end method
