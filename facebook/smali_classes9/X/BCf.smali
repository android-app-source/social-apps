.class public final LX/BCf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1760891
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_d

    .line 1760892
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1760893
    :goto_0
    return v1

    .line 1760894
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1760895
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1760896
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1760897
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1760898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1760899
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1760900
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1760901
    :cond_3
    const-string v12, "bg_color"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1760902
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1760903
    :cond_4
    const-string v12, "image_glyph"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1760904
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1760905
    :cond_5
    const-string v12, "image_source"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1760906
    invoke-static {p0, p1}, LX/BCe;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1760907
    :cond_6
    const-string v12, "style"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1760908
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1760909
    :cond_7
    const-string v12, "subtext"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1760910
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1760911
    :cond_8
    const-string v12, "tertiary_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1760912
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1760913
    :cond_9
    const-string v12, "tertiary_text_color"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1760914
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1760915
    :cond_a
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1760916
    invoke-static {p0, p1}, LX/4at;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1760917
    :cond_b
    const-string v12, "undo_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1760918
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 1760919
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1760920
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1760921
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1760922
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1760923
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1760924
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1760925
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1760926
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1760927
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1760928
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1760929
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1760930
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_d
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1760931
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1760932
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1760933
    if-eqz v0, :cond_0

    .line 1760934
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760935
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1760936
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1760937
    if-eqz v0, :cond_1

    .line 1760938
    const-string v1, "bg_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760939
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1760940
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1760941
    if-eqz v0, :cond_2

    .line 1760942
    const-string v1, "image_glyph"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760943
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1760944
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1760945
    if-eqz v0, :cond_3

    .line 1760946
    const-string v1, "image_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760947
    invoke-static {p0, v0, p2}, LX/BCe;->a(LX/15i;ILX/0nX;)V

    .line 1760948
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1760949
    if-eqz v0, :cond_4

    .line 1760950
    const-string v0, "style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760951
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1760952
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1760953
    if-eqz v0, :cond_5

    .line 1760954
    const-string v1, "subtext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760955
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1760956
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1760957
    if-eqz v0, :cond_6

    .line 1760958
    const-string v1, "tertiary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760959
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1760960
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1760961
    if-eqz v0, :cond_7

    .line 1760962
    const-string v1, "tertiary_text_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760963
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1760964
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1760965
    if-eqz v0, :cond_8

    .line 1760966
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760967
    invoke-static {p0, v0, p2, p3}, LX/4at;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1760968
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1760969
    if-eqz v0, :cond_9

    .line 1760970
    const-string v1, "undo_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760971
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1760972
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1760973
    return-void
.end method
