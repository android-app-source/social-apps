.class public final LX/Bdj;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Bdj;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bdh;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Bdk;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1804020
    const/4 v0, 0x0

    sput-object v0, LX/Bdj;->a:LX/Bdj;

    .line 1804021
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bdj;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1804017
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1804018
    new-instance v0, LX/Bdk;

    invoke-direct {v0}, LX/Bdk;-><init>()V

    iput-object v0, p0, LX/Bdj;->c:LX/Bdk;

    .line 1804019
    return-void
.end method

.method public static c(LX/1De;)LX/Bdh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1804000
    new-instance v1, LX/Bdi;

    invoke-direct {v1}, LX/Bdi;-><init>()V

    .line 1804001
    sget-object v2, LX/Bdj;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bdh;

    .line 1804002
    if-nez v2, :cond_0

    .line 1804003
    new-instance v2, LX/Bdh;

    invoke-direct {v2}, LX/Bdh;-><init>()V

    .line 1804004
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Bdh;->a$redex0(LX/Bdh;LX/1De;IILX/Bdi;)V

    .line 1804005
    move-object v1, v2

    .line 1804006
    move-object v0, v1

    .line 1804007
    return-object v0
.end method

.method public static declared-synchronized q()LX/Bdj;
    .locals 2

    .prologue
    .line 1804013
    const-class v1, LX/Bdj;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bdj;->a:LX/Bdj;

    if-nez v0, :cond_0

    .line 1804014
    new-instance v0, LX/Bdj;

    invoke-direct {v0}, LX/Bdj;-><init>()V

    sput-object v0, LX/Bdj;->a:LX/Bdj;

    .line 1804015
    :cond_0
    sget-object v0, LX/Bdj;->a:LX/Bdj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1804016
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1804010
    const/16 p2, 0x18

    .line 1804011
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x7

    const p0, 0x7f0b0060

    invoke-interface {v0, v1, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1804012
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1804008
    invoke-static {}, LX/1dS;->b()V

    .line 1804009
    const/4 v0, 0x0

    return-object v0
.end method
