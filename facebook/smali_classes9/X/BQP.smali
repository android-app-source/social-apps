.class public LX/BQP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/0Xl;

.field private final c:LX/1E2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782064
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/BQP;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/0Xl;LX/1E2;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782066
    iput-object p1, p0, LX/BQP;->a:LX/0Xl;

    .line 1782067
    iput-object p2, p0, LX/BQP;->b:LX/0Xl;

    .line 1782068
    iput-object p3, p0, LX/BQP;->c:LX/1E2;

    .line 1782069
    return-void
.end method

.method public static a(LX/0QB;)LX/BQP;
    .locals 9

    .prologue
    .line 1782070
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1782071
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1782072
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1782073
    if-nez v1, :cond_0

    .line 1782074
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1782075
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1782076
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1782077
    sget-object v1, LX/BQP;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1782078
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1782079
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1782080
    :cond_1
    if-nez v1, :cond_4

    .line 1782081
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1782082
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1782083
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1782084
    new-instance p0, LX/BQP;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/1E2;->a(LX/0QB;)LX/1E2;

    move-result-object v8

    check-cast v8, LX/1E2;

    invoke-direct {p0, v1, v7, v8}, LX/BQP;-><init>(LX/0Xl;LX/0Xl;LX/1E2;)V

    .line 1782085
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1782086
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1782087
    if-nez v1, :cond_2

    .line 1782088
    sget-object v0, LX/BQP;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1782089
    :goto_1
    if-eqz v0, :cond_3

    .line 1782090
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782091
    :goto_3
    check-cast v0, LX/BQP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1782092
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1782093
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1782094
    :catchall_1
    move-exception v0

    .line 1782095
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782096
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1782097
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1782098
    :cond_2
    :try_start_8
    sget-object v0, LX/BQP;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1782099
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_OPTIMISTIC_PROFILE_PIC_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782100
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1782101
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_OPTIMISTIC_PROFILE_PIC_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782102
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_REFRESH_TIMELINE"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782103
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.PROFILE_PIC_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782104
    iget-object v0, p0, LX/BQP;->b:LX/0Xl;

    const-string v1, "com.facebook.intent.action.PROFILE_PIC_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782105
    iget-object v0, p0, LX/BQP;->c:LX/1E2;

    .line 1782106
    iget-object v1, v0, LX/1E2;->a:LX/1E3;

    .line 1782107
    iget-object v2, v1, LX/1E3;->a:LX/0tX;

    .line 1782108
    iget-object v3, v1, LX/1E3;->c:LX/0zO;

    if-nez v3, :cond_0

    .line 1782109
    new-instance v3, LX/3k3;

    invoke-direct {v3}, LX/3k3;-><init>()V

    .line 1782110
    const-string v4, "square_profile_pic_size_small"

    invoke-static {}, LX/0wB;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "square_profile_pic_size_big"

    invoke-static {}, LX/0wB;->c()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "square_profile_pic_size_huge"

    invoke-static {}, LX/0wB;->d()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1782111
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    iput-object v3, v1, LX/1E3;->c:LX/0zO;

    .line 1782112
    :cond_0
    iget-object v3, v1, LX/1E3;->c:LX/0zO;

    move-object v3, v3

    .line 1782113
    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/8pB;

    invoke-direct {v3, v1}, LX/8pB;-><init>(LX/1E3;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 1782114
    new-instance v2, LX/8pA;

    invoke-direct {v2, v0}, LX/8pA;-><init>(LX/1E2;)V

    iget-object v3, v0, LX/1E2;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1782115
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1782116
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_OPTIMISTIC_COVER_PHOTO_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782117
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1782118
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_REFRESH_TIMELINE"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782119
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.COVER_PHOTO_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782120
    iget-object v0, p0, LX/BQP;->b:LX/0Xl;

    const-string v1, "com.facebook.intent.action.COVER_PHOTO_UPDATED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782121
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1782122
    iget-object v0, p0, LX/BQP;->a:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_REFRESH_TIMELINE"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1782123
    return-void
.end method
