.class public final LX/BfR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BfQ;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

.field public final synthetic c:LX/BfX;


# direct methods
.method public constructor <init>(LX/BfX;ILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V
    .locals 0

    .prologue
    .line 1806023
    iput-object p1, p0, LX/BfR;->c:LX/BfX;

    iput p2, p0, LX/BfR;->a:I

    iput-object p3, p0, LX/BfR;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    .line 1806024
    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget v1, p0, LX/BfR;->a:I

    invoke-static {v0, v1, p1}, LX/BfX;->a$redex0(LX/BfX;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1806025
    iget-object v1, p0, LX/BfR;->c:LX/BfX;

    iget-object v0, p0, LX/BfR;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget-object v3, p0, LX/BfR;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iget v4, p0, LX/BfR;->a:I

    .line 1806026
    new-instance v5, LX/BfS;

    invoke-direct {v5, v0, v4, p1, v3}, LX/BfS;-><init>(LX/BfX;IILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V

    move-object v3, v5

    .line 1806027
    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget-object v0, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v4, p0, LX/BfR;->a:I

    invoke-virtual {v0, v4}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget-object v0, v0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v4, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-static {v1, v2, v3, v4, v5}, LX/BfX;->a$redex0(LX/BfX;Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;J)V

    .line 1806028
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    .line 1806029
    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget v1, p0, LX/BfR;->a:I

    invoke-static {v0, v1, p1}, LX/BfX;->a$redex0(LX/BfX;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1806030
    iget-object v1, p0, LX/BfR;->c:LX/BfX;

    iget-object v0, p0, LX/BfR;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget-object v3, p0, LX/BfR;->b:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iget v4, p0, LX/BfR;->a:I

    .line 1806031
    new-instance v5, LX/BfT;

    invoke-direct {v5, v0, v4, p1, v3}, LX/BfT;-><init>(LX/BfX;IILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V

    move-object v3, v5

    .line 1806032
    iget-object v0, p0, LX/BfR;->c:LX/BfX;

    iget-object v0, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v4, p0, LX/BfR;->a:I

    invoke-virtual {v0, v4}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget-object v0, v0, LX/Bex;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v4, v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-static {v1, v2, v3, v4, v5}, LX/BfX;->a$redex0(LX/BfX;Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;J)V

    .line 1806033
    :cond_0
    return-void
.end method
