.class public final LX/B6Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0P1;

.field public final synthetic d:LX/0P1;

.field public final synthetic e:LX/B6S;


# direct methods
.method public constructor <init>(LX/B6S;Ljava/lang/String;Ljava/lang/String;LX/0P1;LX/0P1;)V
    .locals 0

    .prologue
    .line 1746674
    iput-object p1, p0, LX/B6Q;->e:LX/B6S;

    iput-object p2, p0, LX/B6Q;->a:Ljava/lang/String;

    iput-object p3, p0, LX/B6Q;->b:Ljava/lang/String;

    iput-object p4, p0, LX/B6Q;->c:LX/0P1;

    iput-object p5, p0, LX/B6Q;->d:LX/0P1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1746675
    iget-object v0, p0, LX/B6Q;->e:LX/B6S;

    iget-object v0, v0, LX/B6S;->b:LX/B7U;

    iget-object v1, p0, LX/B6Q;->a:Ljava/lang/String;

    iget-object v2, p0, LX/B6Q;->b:Ljava/lang/String;

    iget-object v3, p0, LX/B6Q;->c:LX/0P1;

    iget-object v4, p0, LX/B6Q;->d:LX/0P1;

    .line 1746676
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1746677
    invoke-virtual {v4}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v5

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1746678
    new-instance v9, LX/4K4;

    invoke-direct {v9}, LX/4K4;-><init>()V

    .line 1746679
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1746680
    const-string v4, "checkbox_key"

    invoke-virtual {v9, v4, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746681
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1746682
    if-eqz v5, :cond_2

    .line 1746683
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    .line 1746684
    :goto_1
    move-object v5, v6

    .line 1746685
    const-string v6, "is_checked"

    invoke-virtual {v9, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1746686
    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1746687
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 1746688
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1746689
    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v6

    invoke-virtual {v6}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1746690
    new-instance v9, LX/4K5;

    invoke-direct {v9}, LX/4K5;-><init>()V

    .line 1746691
    invoke-virtual {v9, v6}, LX/4K5;->a(Ljava/lang/String;)LX/4K5;

    .line 1746692
    invoke-virtual {v3, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/4K5;->a(Ljava/util/List;)LX/4K5;

    .line 1746693
    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1746694
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v6, v6

    .line 1746695
    new-instance v7, LX/4Ge;

    invoke-direct {v7}, LX/4Ge;-><init>()V

    .line 1746696
    const-string v8, "ad_id"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746697
    const-string v8, "lead_gen_data_id"

    invoke-virtual {v7, v8, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746698
    const-string v8, "fields_data"

    invoke-virtual {v7, v8, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1746699
    const-string v6, "disclaimer_responses"

    invoke-virtual {v7, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1746700
    invoke-static {}, LX/B7U;->a()LX/0jT;

    move-result-object v5

    .line 1746701
    new-instance v6, LX/ADb;

    invoke-direct {v6}, LX/ADb;-><init>()V

    move-object v6, v6

    .line 1746702
    const-string v8, "input"

    invoke-virtual {v6, v8, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1746703
    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v5

    .line 1746704
    iget-object v6, v0, LX/B7U;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1746705
    new-instance v6, LX/B7T;

    invoke-direct {v6, v0}, LX/B7T;-><init>(LX/B7U;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1746706
    return-object v0

    .line 1746707
    :cond_2
    iget-object v6, v0, LX/B7U;->b:LX/03V;

    const-string v10, "LEADGEN"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string v4, "Error attempting to parse Illegal Enum Value for IsChecked. Value - ("

    invoke-direct {p0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string v4, ")"

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v10, p0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v10

    invoke-virtual {v10}, LX/0VK;->g()LX/0VG;

    move-result-object v10

    invoke-virtual {v6, v10}, LX/03V;->a(LX/0VG;)V

    .line 1746708
    sget-object v6, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_1
.end method
