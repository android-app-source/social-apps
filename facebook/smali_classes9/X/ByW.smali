.class public LX/ByW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/26L",
        "<",
        "LX/ByV;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/ByV;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 5
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837422
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ByW;->b:Ljava/util/Map;

    .line 1837423
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1837424
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1837425
    new-instance v3, LX/ByV;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v3, v0}, LX/ByV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1837426
    iget-object v0, p0, LX/ByW;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1837427
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1837428
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1837429
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ByW;->a:LX/0Px;

    .line 1837430
    return-void
.end method


# virtual methods
.method public final a(LX/26N;)I
    .locals 1

    .prologue
    .line 1837431
    check-cast p1, LX/ByV;

    .line 1837432
    iget-object v0, p0, LX/ByW;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rem-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837433
    iget-object v0, p0, LX/ByW;->a:LX/0Px;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1837434
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/26N;)I
    .locals 1

    .prologue
    .line 1837435
    check-cast p1, LX/ByV;

    .line 1837436
    iget-object v0, p0, LX/ByW;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1837437
    const/4 v0, 0x3

    return v0
.end method

.method public final c(LX/26N;)I
    .locals 1

    .prologue
    .line 1837438
    const/4 v0, 0x1

    return v0
.end method

.method public final d(LX/26N;)I
    .locals 1

    .prologue
    .line 1837439
    const/4 v0, 0x1

    return v0
.end method
