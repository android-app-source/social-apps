.class public LX/B3e;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/category/RemoveFrameViewBinder;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/B3p;

.field public final g:LX/B3j;

.field private final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/B3p;LX/B3j;Ljava/lang/String;)V
    .locals 1
    .param p1    # LX/B3p;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B3j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740558
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1740559
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1740560
    iput-object v0, p0, LX/B3e;->e:LX/0Ot;

    .line 1740561
    iput-object p1, p0, LX/B3e;->f:LX/B3p;

    .line 1740562
    iput-object p2, p0, LX/B3e;->g:LX/B3j;

    .line 1740563
    iput-object p3, p0, LX/B3e;->h:Ljava/lang/String;

    .line 1740564
    return-void
.end method

.method private e(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 1740565
    iget-object v0, p0, LX/B3e;->g:LX/B3j;

    .line 1740566
    invoke-static {v0}, LX/B3j;->c(LX/B3j;)V

    .line 1740567
    invoke-static {v0}, LX/B3j;->b(LX/B3j;)Z

    move-result p0

    if-eqz p0, :cond_0

    if-nez p1, :cond_0

    .line 1740568
    iget-object p0, v0, LX/B3j;->a:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740569
    :goto_0
    move-object v0, p0

    .line 1740570
    return-object v0

    .line 1740571
    :cond_0
    invoke-static {v0, p1}, LX/B3j;->c(LX/B3j;I)V

    .line 1740572
    iget-object p0, v0, LX/B3j;->b:LX/2nf;

    invoke-interface {p0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1740573
    packed-switch p2, :pswitch_data_0

    .line 1740574
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected viewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1740575
    :pswitch_0
    iget-object v0, p0, LX/B3e;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f031078

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/header/FigHeader;

    .line 1740576
    new-instance v1, LX/B3b;

    invoke-direct {v1, v0}, LX/B3b;-><init>(Lcom/facebook/fig/header/FigHeader;)V

    move-object v0, v1

    .line 1740577
    :goto_0
    return-object v0

    .line 1740578
    :pswitch_1
    iget-object v0, p0, LX/B3e;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f03107b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 1740579
    new-instance v1, LX/B3a;

    iget-object v2, p0, LX/B3e;->f:LX/B3p;

    invoke-direct {v1, p0, v0, v2}, LX/B3a;-><init>(LX/B3e;Lcom/facebook/fig/listitem/FigListItem;LX/B3p;)V

    move-object v0, v1

    goto :goto_0

    .line 1740580
    :pswitch_2
    iget-object v0, p0, LX/B3e;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f031077

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1740581
    new-instance v0, LX/B3d;

    iget-object v2, p0, LX/B3e;->f:LX/B3p;

    invoke-direct {v0, v1, v2}, LX/B3d;-><init>(Landroid/view/View;LX/B3p;)V

    goto :goto_0

    .line 1740582
    :pswitch_3
    iget-object v0, p0, LX/B3e;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030259

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1740583
    new-instance v1, LX/B3c;

    iget-object v2, p0, LX/B3e;->f:LX/B3p;

    invoke-direct {v1, v0, v2}, LX/B3c;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;LX/B3p;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1740584
    iget-object v0, p0, LX/B3e;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;

    .line 1740585
    iget-object v1, p0, LX/B3e;->g:LX/B3j;

    invoke-virtual {v1, p2}, LX/B3j;->b(I)I

    move-result v1

    .line 1740586
    packed-switch v1, :pswitch_data_0

    .line 1740587
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected viewType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1740588
    :pswitch_0
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1740589
    instance-of v1, p1, LX/B3b;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1740590
    check-cast p1, LX/B3b;

    .line 1740591
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->c()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/B3b;->a(Ljava/lang/CharSequence;)V

    .line 1740592
    iget-object v0, p1, LX/B3b;->l:Lcom/facebook/fig/header/FigHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1740593
    :goto_0
    return-void

    .line 1740594
    :pswitch_1
    const/4 v2, 0x0

    .line 1740595
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    if-eqz v1, :cond_0

    .line 1740596
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    invoke-static {v1}, LX/B3w;->a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v1

    .line 1740597
    :goto_1
    instance-of v2, p1, LX/B3a;

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1740598
    check-cast p1, LX/B3a;

    .line 1740599
    invoke-virtual {p1, v1}, LX/B3a;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    .line 1740600
    iget-object v2, p1, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    move-object v2, v2

    .line 1740601
    invoke-virtual {v0, v1, v2}, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Lcom/facebook/fig/listitem/FigListItem;)V

    goto :goto_0

    .line 1740602
    :cond_0
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    if-eqz v1, :cond_1

    .line 1740603
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    goto :goto_1

    .line 1740604
    :cond_1
    iget-object v1, p0, LX/B3e;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to recognize the model type"

    invoke-virtual {v1, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_1

    .line 1740605
    :pswitch_2
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;

    .line 1740606
    iget-object v2, p0, LX/B3e;->h:Ljava/lang/String;

    .line 1740607
    new-instance v3, LX/B5x;

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->j()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->a(LX/5QV;)Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->j()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->l()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/B5x;-><init>(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->j()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1740608
    iput-object v4, v3, LX/B5x;->c:Ljava/lang/String;

    .line 1740609
    move-object v3, v3

    .line 1740610
    iput-object v2, v3, LX/B5x;->f:Ljava/lang/String;

    .line 1740611
    move-object v4, v3

    .line 1740612
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$ContextDescriptionModel;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$ContextDescriptionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$ContextDescriptionModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1740613
    :goto_2
    iput-object v3, v4, LX/B5x;->d:Ljava/lang/String;

    .line 1740614
    move-object v3, v4

    .line 1740615
    invoke-virtual {v3}, LX/B5x;->a()Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v3

    move-object v1, v3

    .line 1740616
    instance-of v2, p1, LX/B3a;

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1740617
    check-cast p1, LX/B3a;

    .line 1740618
    invoke-virtual {p1, v1}, LX/B3a;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    .line 1740619
    iget-object v2, p1, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    move-object v2, v2

    .line 1740620
    invoke-virtual {v0, v1, v2}, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Lcom/facebook/fig/listitem/FigListItem;)V

    goto/16 :goto_0

    .line 1740621
    :pswitch_3
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1740622
    instance-of v1, p1, LX/B3a;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1740623
    check-cast p1, LX/B3a;

    .line 1740624
    const/4 v1, 0x0

    iput-object v1, p1, LX/B3a;->o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740625
    iput-object v0, p1, LX/B3a;->p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1740626
    iget-object v1, p1, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    move-object v1, v1

    .line 1740627
    invoke-static {v0, v1}, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->a(Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;Lcom/facebook/fig/listitem/FigListItem;)V

    goto/16 :goto_0

    .line 1740628
    :pswitch_4
    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1740629
    instance-of v1, p1, LX/B3d;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1740630
    check-cast p1, LX/B3d;

    .line 1740631
    iput-object v0, p1, LX/B3d;->n:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1740632
    goto/16 :goto_0

    .line 1740633
    :pswitch_5
    new-instance v1, LX/B5x;

    invoke-direct {p0, p2}, LX/B3e;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    invoke-direct {v1, v0}, LX/B5x;-><init>(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    iget-object v0, p0, LX/B3e;->h:Ljava/lang/String;

    .line 1740634
    iput-object v0, v1, LX/B5x;->f:Ljava/lang/String;

    .line 1740635
    move-object v0, v1

    .line 1740636
    invoke-virtual {v0}, LX/B5x;->a()Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v1

    .line 1740637
    instance-of v0, p1, LX/B3c;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1740638
    check-cast p1, LX/B3c;

    .line 1740639
    iget-object v0, p1, LX/B3c;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    move-object v2, v0

    .line 1740640
    iget-object v0, p0, LX/B3e;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    .line 1740641
    iget-object v3, v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    .line 1740642
    iget-object v4, v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/B3x;

    .line 1740643
    iget-object v5, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1740644
    if-eqz v5, :cond_5

    .line 1740645
    iget-object v4, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1740646
    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1740647
    :cond_2
    :goto_3
    const v4, 0x7f08274b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1740648
    iget-object v4, v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->d:LX/23P;

    invoke-virtual {v4, v3, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1740649
    goto/16 :goto_0

    .line 1740650
    :pswitch_6
    instance-of v0, p1, LX/B3b;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1740651
    check-cast p1, LX/B3b;

    .line 1740652
    iget-object v0, p0, LX/B3e;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 1740653
    const v1, 0x7f082749

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/B3b;->a(Ljava/lang/CharSequence;)V

    .line 1740654
    iget-object v0, p1, LX/B3b;->m:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 1740655
    new-instance v0, LX/0wM;

    iget-object v1, p1, LX/B3b;->l:Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {v1}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    const v1, 0x7f0209e1

    const v2, -0xa76f01

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p1, LX/B3b;->m:Landroid/graphics/drawable/Drawable;

    .line 1740656
    :cond_3
    iget-object v0, p1, LX/B3b;->l:Lcom/facebook/fig/header/FigHeader;

    iget-object v1, p1, LX/B3b;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1740657
    goto/16 :goto_0

    :cond_4
    const-string v3, ""

    goto/16 :goto_2

    .line 1740658
    :cond_5
    iget-object v5, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v5, v5

    .line 1740659
    if-eqz v5, :cond_2

    .line 1740660
    iget-object v5, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1740661
    if-eqz v5, :cond_2

    .line 1740662
    iget-object v5, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1740663
    iget-object p0, v1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object p0, p0

    .line 1740664
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 1740665
    const p1, 0x7f0b1a1b

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 1740666
    sget-object p2, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p2, v5, p0, p1}, LX/B3x;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1740667
    iget-object v0, p0, LX/B3e;->g:LX/B3j;

    .line 1740668
    iput-boolean p1, v0, LX/B3j;->c:Z

    .line 1740669
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1740670
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1740671
    iget-object v0, p0, LX/B3e;->g:LX/B3j;

    invoke-virtual {v0, p1}, LX/B3j;->b(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1740672
    iget-object v0, p0, LX/B3e;->g:LX/B3j;

    .line 1740673
    iget-object v1, v0, LX/B3j;->b:LX/2nf;

    if-nez v1, :cond_1

    .line 1740674
    const/4 v1, 0x0

    .line 1740675
    :cond_0
    :goto_0
    move v0, v1

    .line 1740676
    return v0

    .line 1740677
    :cond_1
    iget-object v1, v0, LX/B3j;->b:LX/2nf;

    invoke-interface {v1}, LX/2nf;->getCount()I

    move-result v1

    .line 1740678
    invoke-static {v0}, LX/B3j;->b(LX/B3j;)Z

    move-result p0

    if-eqz p0, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
