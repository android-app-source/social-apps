.class public final enum LX/BSS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSS;

.field public static final enum END:LX/BSS;

.field public static final enum NONE:LX/BSS;

.field public static final enum START:LX/BSS;

.field public static final enum START_AD:LX/BSS;

.field public static final enum START_SHOW_NOTHING:LX/BSS;

.field public static final enum STATIC_COUNTDOWN:LX/BSS;

.field public static final enum TRANSIT:LX/BSS;

.field public static final enum VOD_NO_VIDEO_AD:LX/BSS;

.field public static final enum WAIT_FOR:LX/BSS;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1785479
    new-instance v0, LX/BSS;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->NONE:LX/BSS;

    .line 1785480
    new-instance v0, LX/BSS;

    const-string v1, "START"

    invoke-direct {v0, v1, v4}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->START:LX/BSS;

    .line 1785481
    new-instance v0, LX/BSS;

    const-string v1, "START_AD"

    invoke-direct {v0, v1, v5}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->START_AD:LX/BSS;

    .line 1785482
    new-instance v0, LX/BSS;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v6}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->TRANSIT:LX/BSS;

    .line 1785483
    new-instance v0, LX/BSS;

    const-string v1, "WAIT_FOR"

    invoke-direct {v0, v1, v7}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->WAIT_FOR:LX/BSS;

    .line 1785484
    new-instance v0, LX/BSS;

    const-string v1, "STATIC_COUNTDOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->STATIC_COUNTDOWN:LX/BSS;

    .line 1785485
    new-instance v0, LX/BSS;

    const-string v1, "END"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->END:LX/BSS;

    .line 1785486
    new-instance v0, LX/BSS;

    const-string v1, "VOD_NO_VIDEO_AD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->VOD_NO_VIDEO_AD:LX/BSS;

    .line 1785487
    new-instance v0, LX/BSS;

    const-string v1, "START_SHOW_NOTHING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/BSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSS;->START_SHOW_NOTHING:LX/BSS;

    .line 1785488
    const/16 v0, 0x9

    new-array v0, v0, [LX/BSS;

    sget-object v1, LX/BSS;->NONE:LX/BSS;

    aput-object v1, v0, v3

    sget-object v1, LX/BSS;->START:LX/BSS;

    aput-object v1, v0, v4

    sget-object v1, LX/BSS;->START_AD:LX/BSS;

    aput-object v1, v0, v5

    sget-object v1, LX/BSS;->TRANSIT:LX/BSS;

    aput-object v1, v0, v6

    sget-object v1, LX/BSS;->WAIT_FOR:LX/BSS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BSS;->STATIC_COUNTDOWN:LX/BSS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BSS;->END:LX/BSS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BSS;->VOD_NO_VIDEO_AD:LX/BSS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BSS;->START_SHOW_NOTHING:LX/BSS;

    aput-object v2, v0, v1

    sput-object v0, LX/BSS;->$VALUES:[LX/BSS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1785489
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSS;
    .locals 1

    .prologue
    .line 1785478
    const-class v0, LX/BSS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSS;

    return-object v0
.end method

.method public static values()[LX/BSS;
    .locals 1

    .prologue
    .line 1785477
    sget-object v0, LX/BSS;->$VALUES:[LX/BSS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSS;

    return-object v0
.end method
