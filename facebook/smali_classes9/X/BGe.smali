.class public LX/BGe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field private b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1767359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767360
    iput-object p1, p0, LX/BGe;->b:LX/0Zb;

    .line 1767361
    return-void
.end method

.method public static a(LX/0QB;)LX/BGe;
    .locals 1

    .prologue
    .line 1767362
    invoke-static {p0}, LX/BGe;->b(LX/0QB;)LX/BGe;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1767363
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/BGd;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1767364
    const-string v0, "composer"

    .line 1767365
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1767366
    iget-object v0, p0, LX/BGe;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1767367
    iget-object v0, p0, LX/BGe;->a:Ljava/lang/String;

    .line 1767368
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1767369
    :cond_0
    iget-object v0, p0, LX/BGe;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1767370
    return-void
.end method

.method public static b(LX/0QB;)LX/BGe;
    .locals 2

    .prologue
    .line 1767371
    new-instance v1, LX/BGe;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/BGe;-><init>(LX/0Zb;)V

    .line 1767372
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1767373
    sget-object v0, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GALLERY:LX/BGd;

    invoke-static {v0}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "media_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767374
    return-void
.end method
