.class public LX/BIP;
.super LX/BIH;
.source ""

# interfaces
.implements LX/BIO;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field public i:Lcom/facebook/video/player/RichVideoPlayer;

.field public j:LX/BIN;

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1770343
    const-class v0, LX/BIP;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BIP;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1770344
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BIP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770345
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770346
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BIP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1770347
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770374
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BIH;-><init>(Landroid/content/Context;B)V

    .line 1770375
    const/high16 v0, -0x80000000

    iput v0, p0, LX/BIP;->k:I

    .line 1770376
    const-class v0, LX/BIP;

    invoke-static {v0, p0}, LX/BIP;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770377
    new-instance v0, LX/BIN;

    invoke-direct {v0, p0}, LX/BIN;-><init>(LX/BIP;)V

    iput-object v0, p0, LX/BIP;->j:LX/BIN;

    .line 1770378
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/BIP;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/BIP;->d:LX/0ad;

    iput-object v2, p1, LX/BIP;->e:LX/0Ot;

    return-void
.end method

.method private m()V
    .locals 15

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    .line 1770348
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1770349
    const v0, 0x7f0d2519

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    .line 1770350
    :cond_0
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 1770351
    const v0, 0x7f0d251a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    .line 1770352
    :cond_1
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770353
    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    if-nez v0, :cond_4

    move-wide v0, v2

    .line 1770354
    :goto_0
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1770355
    iget-object v2, p0, LX/BIP;->h:Landroid/widget/TextView;

    const-wide/16 v9, 0x3c

    const-wide/16 v13, 0x9

    const/16 v11, 0x30

    .line 1770356
    const-wide/16 v5, 0x3e8

    div-long v5, v0, v5

    .line 1770357
    div-long v7, v5, v9

    .line 1770358
    rem-long/2addr v5, v9

    .line 1770359
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1770360
    cmp-long v10, v7, v13

    if-gtz v10, :cond_2

    .line 1770361
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1770362
    :cond_2
    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1770363
    cmp-long v7, v5, v13

    if-gtz v7, :cond_3

    .line 1770364
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1770365
    :cond_3
    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1770366
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 1770367
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1770368
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1770369
    :goto_1
    return-void

    .line 1770370
    :cond_4
    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1770371
    iget-wide v5, v0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v0, v5

    .line 1770372
    goto :goto_0

    .line 1770373
    :cond_5
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 1770331
    iput p1, p0, LX/BIP;->k:I

    .line 1770332
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1770333
    invoke-virtual {p0}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1770334
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/photos/base/media/VideoItem;

    if-nez v1, :cond_0

    .line 1770335
    const-string v1, "Cannot render a %s view as a PickerGridVideoItemDraweeView"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1770336
    iget-object v0, p0, LX/BIP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/BIP;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1770337
    :goto_0
    return-void

    .line 1770338
    :cond_0
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 1770339
    invoke-virtual {p0}, LX/BIP;->i()V

    .line 1770340
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1770341
    :cond_1
    invoke-direct {p0}, LX/BIP;->m()V

    .line 1770342
    invoke-super {p0}, LX/BIH;->b()V

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1770328
    invoke-direct {p0}, LX/BIP;->m()V

    .line 1770329
    invoke-super {p0}, LX/BIH;->e()V

    .line 1770330
    return-void
.end method

.method public getItemType()LX/BHH;
    .locals 1

    .prologue
    .line 1770327
    sget-object v0, LX/BHH;->VIDEO:LX/BHH;

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 1770326
    const v0, 0x7f030f5b

    return v0
.end method

.method public getPlayPriority()I
    .locals 2

    .prologue
    .line 1770320
    iget v0, p0, LX/BIP;->k:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1770321
    iget v0, p0, LX/BIP;->k:I

    .line 1770322
    :goto_0
    return v0

    .line 1770323
    :cond_0
    invoke-virtual {p0}, LX/BIP;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1770324
    invoke-virtual {p0}, LX/BIG;->getSelectedOrder()I

    move-result v0

    goto :goto_0

    .line 1770325
    :cond_1
    invoke-virtual {p0}, LX/BIG;->getIndex()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1770310
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    .line 1770311
    :goto_0
    return-void

    .line 1770312
    :cond_0
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1770313
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770314
    :cond_1
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1770315
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1770316
    :cond_2
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1770317
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/BIP;->j:LX/BIN;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1770318
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->invalidate()V

    .line 1770319
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->requestLayout()V

    goto :goto_0
.end method

.method public final j()V
    .locals 7

    .prologue
    const/4 v1, 0x4

    .line 1770275
    const/4 v5, 0x1

    .line 1770276
    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v2, :cond_0

    .line 1770277
    const v2, 0x7f0d1338

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1770278
    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, LX/BIP;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1770279
    invoke-static {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1770280
    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v5}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1770281
    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v3, LX/04D;->SIMPLE_PICKER:LX/04D;

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1770282
    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1770283
    :cond_0
    iget-object v2, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v2, :cond_5

    .line 1770284
    :cond_1
    :goto_0
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_2

    .line 1770285
    :goto_1
    return-void

    .line 1770286
    :cond_2
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1770287
    iget-object v0, p0, LX/BIP;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770288
    :cond_3
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 1770289
    iget-object v0, p0, LX/BIP;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1770290
    :cond_4
    iget-object v0, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1

    .line 1770291
    :cond_5
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v2

    iget-object v3, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    .line 1770292
    iput-object v3, v2, LX/2oE;->a:Landroid/net/Uri;

    .line 1770293
    move-object v2, v2

    .line 1770294
    sget-object v3, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1770295
    iput-object v3, v2, LX/2oE;->e:LX/097;

    .line 1770296
    move-object v2, v2

    .line 1770297
    invoke-virtual {v2}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v2

    .line 1770298
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v2

    .line 1770299
    iput-boolean v5, v2, LX/2oH;->g:Z

    .line 1770300
    move-object v2, v2

    .line 1770301
    new-instance v3, LX/2pZ;

    invoke-direct {v3}, LX/2pZ;-><init>()V

    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    .line 1770302
    iput-object v2, v3, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1770303
    move-object v2, v3

    .line 1770304
    iget-object v3, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v3

    float-to-double v4, v3

    .line 1770305
    iput-wide v4, v2, LX/2pZ;->e:D

    .line 1770306
    move-object v2, v2

    .line 1770307
    invoke-virtual {v2}, LX/2pZ;->b()LX/2pa;

    move-result-object v2

    .line 1770308
    iget-object v3, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, p0, LX/BIP;->j:LX/BIN;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1770309
    iget-object v3, p0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1770273
    const/high16 v0, -0x80000000

    iput v0, p0, LX/BIP;->k:I

    .line 1770274
    return-void
.end method
