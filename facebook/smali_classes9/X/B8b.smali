.class public final LX/B8b;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1749946
    const/4 v10, 0x0

    .line 1749947
    const/4 v9, 0x0

    .line 1749948
    const/4 v8, 0x0

    .line 1749949
    const/4 v7, 0x0

    .line 1749950
    const/4 v6, 0x0

    .line 1749951
    const/4 v5, 0x0

    .line 1749952
    const/4 v4, 0x0

    .line 1749953
    const/4 v3, 0x0

    .line 1749954
    const/4 v2, 0x0

    .line 1749955
    const/4 v1, 0x0

    .line 1749956
    const/4 v0, 0x0

    .line 1749957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1749958
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749959
    const/4 v0, 0x0

    .line 1749960
    :goto_0
    return v0

    .line 1749961
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749962
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1749963
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1749964
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1749965
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1749966
    const-string v12, "context_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1749967
    invoke-static {p0, p1}, LX/B8Y;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1749968
    :cond_2
    const-string v12, "follow_up_title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1749969
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1749970
    :cond_3
    const-string v12, "info_fields_data"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1749971
    invoke-static {p0, p1}, LX/B8g;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1749972
    :cond_4
    const-string v12, "legal_content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1749973
    invoke-static {p0, p1}, LX/B8j;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1749974
    :cond_5
    const-string v12, "need_split_flow"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1749975
    const/4 v1, 0x1

    .line 1749976
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1749977
    :cond_6
    const-string v12, "pages"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1749978
    invoke-static {p0, p1}, LX/B8a;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1749979
    :cond_7
    const-string v12, "policy_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1749980
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1749981
    :cond_8
    const-string v12, "split_flow_request_method"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1749982
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1749983
    :cond_9
    const-string v12, "split_flow_use_post"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1749984
    const/4 v0, 0x1

    .line 1749985
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    goto/16 :goto_1

    .line 1749986
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1749987
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1749988
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1749989
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1749990
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1749991
    if-eqz v1, :cond_b

    .line 1749992
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1749993
    :cond_b
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1749994
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1749995
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1749996
    if-eqz v0, :cond_c

    .line 1749997
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1749998
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
