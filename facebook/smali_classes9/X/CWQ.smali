.class public LX/CWQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1908126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1908127
    iput-object p1, p0, LX/CWQ;->a:Ljava/lang/String;

    .line 1908128
    iput-object p2, p0, LX/CWQ;->b:Ljava/lang/String;

    .line 1908129
    iput-object p3, p0, LX/CWQ;->c:Ljava/lang/String;

    .line 1908130
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1908131
    instance-of v1, p1, LX/CWQ;

    if-nez v1, :cond_1

    .line 1908132
    :cond_0
    :goto_0
    return v0

    .line 1908133
    :cond_1
    check-cast p1, LX/CWQ;

    .line 1908134
    iget-object v1, p0, LX/CWQ;->a:Ljava/lang/String;

    iget-object v2, p1, LX/CWQ;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CWQ;->b:Ljava/lang/String;

    iget-object v2, p1, LX/CWQ;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CWQ;->c:Ljava/lang/String;

    iget-object v2, p1, LX/CWQ;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1908135
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/CWQ;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/CWQ;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/CWQ;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
