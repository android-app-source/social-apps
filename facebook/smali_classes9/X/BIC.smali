.class public LX/BIC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private final b:LX/BHJ;

.field public final c:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

.field private final d:LX/0wd;

.field public final e:LX/BIF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1770041
    const-wide v0, 0x4050400000000000L    # 65.0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/BIC;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/BHJ;Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;LX/BIF;LX/0Ot;)V
    .locals 4
    .param p1    # LX/BHJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BIF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BHJ;",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            "LX/BIF;",
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1770042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770043
    iput-object p1, p0, LX/BIC;->b:LX/BHJ;

    .line 1770044
    iput-object p2, p0, LX/BIC;->c:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    .line 1770045
    iput-object p3, p0, LX/BIC;->e:LX/BIF;

    .line 1770046
    invoke-interface {p4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/BIC;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1770047
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1770048
    move-object v0, v0

    .line 1770049
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/BIC;->d:LX/0wd;

    .line 1770050
    iget-object v0, p0, LX/BIC;->d:LX/0wd;

    new-instance v1, LX/BIB;

    invoke-direct {v1, p0}, LX/BIB;-><init>(LX/BIC;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1770051
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1770052
    iget-object v0, p0, LX/BIC;->e:LX/BIF;

    invoke-interface {v0}, LX/BIF;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1770053
    const/4 v0, 0x0

    .line 1770054
    :goto_0
    return v0

    .line 1770055
    :cond_0
    iget-object v0, p0, LX/BIC;->e:LX/BIF;

    invoke-interface {v0}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1770056
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1770057
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1770058
    :pswitch_1
    iget-object v0, p0, LX/BIC;->d:LX/0wd;

    const-wide v2, 0x3fef5c28f5c28f5cL    # 0.98

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1

    .line 1770059
    :pswitch_2
    iget-object v1, p0, LX/BIC;->c:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    invoke-virtual {v1}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1770060
    iget-object v1, p0, LX/BIC;->b:LX/BHJ;

    iget-object v2, p0, LX/BIC;->e:LX/BIF;

    invoke-virtual {v1, v2}, LX/BHJ;->b(LX/BIF;)V

    .line 1770061
    :cond_1
    :pswitch_3
    iget-object v1, p0, LX/BIC;->d:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1770062
    iget-object v1, p0, LX/BIC;->c:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->b(Lcom/facebook/ipc/media/MediaItem;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
