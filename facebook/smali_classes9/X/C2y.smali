.class public LX/C2y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C2w;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1844851
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C2y;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844852
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1844853
    iput-object p1, p0, LX/C2y;->b:LX/0Ot;

    .line 1844854
    return-void
.end method

.method public static a(LX/0QB;)LX/C2y;
    .locals 4

    .prologue
    .line 1844855
    const-class v1, LX/C2y;

    monitor-enter v1

    .line 1844856
    :try_start_0
    sget-object v0, LX/C2y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844857
    sput-object v2, LX/C2y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844858
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844859
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844860
    new-instance v3, LX/C2y;

    const/16 p0, 0x882

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C2y;-><init>(LX/0Ot;)V

    .line 1844861
    move-object v0, v3

    .line 1844862
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844863
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C2y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844864
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844865
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;LX/C2l;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/C2l;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844866
    const v0, -0x3abbaddb

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1844867
    check-cast p2, LX/C2x;

    .line 1844868
    iget-object v0, p0, LX/C2y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;

    iget-object v2, p2, LX/C2x;->a:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/C2x;->b:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/C2x;->c:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/C2x;->d:Ljava/lang/CharSequence;

    iget-object v6, p2, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, p1

    .line 1844869
    iget-object v7, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1844870
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1844871
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v10

    .line 1844872
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result p0

    const/4 v8, 0x0

    move v9, v8

    :goto_0
    if-ge v9, p0, :cond_3

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;

    .line 1844873
    const-string p1, "item_id"

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 1844874
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    .line 1844875
    :goto_1
    move-object v7, v8

    .line 1844876
    if-nez v7, :cond_1

    const/4 v7, 0x0

    .line 1844877
    :goto_2
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x4

    invoke-interface {v8, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-static {v0, v1, v6, v7}, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->a(Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C2l;)LX/1Dg;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x0

    .line 1844878
    new-instance v10, LX/C2o;

    invoke-direct {v10}, LX/C2o;-><init>()V

    .line 1844879
    sget-object p0, LX/C2p;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C2n;

    .line 1844880
    if-nez p0, :cond_0

    .line 1844881
    new-instance p0, LX/C2n;

    invoke-direct {p0}, LX/C2n;-><init>()V

    .line 1844882
    :cond_0
    invoke-static {p0, v1, v9, v9, v10}, LX/C2n;->a$redex0(LX/C2n;LX/1De;IILX/C2o;)V

    .line 1844883
    move-object v10, p0

    .line 1844884
    move-object v9, v10

    .line 1844885
    move-object v9, v9

    .line 1844886
    iget-object v10, v9, LX/C2n;->a:LX/C2o;

    iput-object v2, v10, LX/C2o;->a:Ljava/lang/CharSequence;

    .line 1844887
    iget-object v10, v9, LX/C2n;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 1844888
    move-object v9, v9

    .line 1844889
    iget-object v10, v9, LX/C2n;->a:LX/C2o;

    iput-object v3, v10, LX/C2o;->b:Ljava/lang/CharSequence;

    .line 1844890
    iget-object v10, v9, LX/C2n;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 1844891
    move-object v9, v9

    .line 1844892
    iget-object v10, v9, LX/C2n;->a:LX/C2o;

    iput-object v4, v10, LX/C2o;->c:Ljava/lang/CharSequence;

    .line 1844893
    iget-object v10, v9, LX/C2n;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 1844894
    move-object v9, v9

    .line 1844895
    iget-object v10, v9, LX/C2n;->a:LX/C2o;

    iput-object v5, v10, LX/C2o;->d:Ljava/lang/CharSequence;

    .line 1844896
    iget-object v10, v9, LX/C2n;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 1844897
    move-object v9, v9

    .line 1844898
    iget-object v10, v9, LX/C2n;->a:LX/C2o;

    iput-object v7, v10, LX/C2o;->e:LX/C2l;

    .line 1844899
    move-object v7, v9

    .line 1844900
    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x1

    const v9, 0x7f0b010f

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0a0097

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 1844901
    return-object v0

    .line 1844902
    :cond_1
    iget-object v8, v0, Lcom/facebook/feedplugins/commerce/SingleProductShareComponentSpec;->d:LX/C2m;

    .line 1844903
    new-instance v10, LX/C2l;

    invoke-static {v8}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v9

    check-cast v9, LX/7j6;

    invoke-direct {v10, v9, v7}, LX/C2l;-><init>(LX/7j6;Ljava/lang/String;)V

    .line 1844904
    move-object v7, v10

    .line 1844905
    goto/16 :goto_2

    .line 1844906
    :cond_2
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto/16 :goto_0

    .line 1844907
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1844908
    invoke-static {}, LX/1dS;->b()V

    .line 1844909
    iget v0, p1, LX/1dQ;->b:I

    .line 1844910
    packed-switch v0, :pswitch_data_0

    .line 1844911
    :goto_0
    return-object v3

    .line 1844912
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1844913
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/C2l;

    .line 1844914
    iget-object p1, p0, LX/C2y;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1844915
    if-eqz v0, :cond_0

    .line 1844916
    invoke-virtual {v0}, LX/C2l;->onClick()V

    .line 1844917
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3abbaddb
        :pswitch_0
    .end packed-switch
.end method
