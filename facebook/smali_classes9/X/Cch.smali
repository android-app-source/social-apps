.class public LX/Cch;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ccj;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Cch",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ccj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921536
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1921537
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Cch;->b:LX/0Zi;

    .line 1921538
    iput-object p1, p0, LX/Cch;->a:LX/0Ot;

    .line 1921539
    return-void
.end method

.method public static a(LX/0QB;)LX/Cch;
    .locals 4

    .prologue
    .line 1921525
    const-class v1, LX/Cch;

    monitor-enter v1

    .line 1921526
    :try_start_0
    sget-object v0, LX/Cch;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921527
    sput-object v2, LX/Cch;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921530
    new-instance v3, LX/Cch;

    const/16 p0, 0x2f13

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Cch;-><init>(LX/0Ot;)V

    .line 1921531
    move-object v0, v3

    .line 1921532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cch;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1921549
    invoke-static {}, LX/1dS;->b()V

    .line 1921550
    iget v0, p1, LX/1dQ;->b:I

    .line 1921551
    packed-switch v0, :pswitch_data_0

    .line 1921552
    :goto_0
    return-object v1

    .line 1921553
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1921554
    check-cast v0, LX/Ccg;

    .line 1921555
    iget-object v2, p0, LX/Cch;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ccj;

    iget-object v3, v0, LX/Ccg;->d:LX/1Pq;

    iget-object v4, v0, LX/Ccg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v0, LX/Ccg;->a:Ljava/lang/String;

    iget-boolean p2, v0, LX/Ccg;->b:Z

    .line 1921556
    iget-object p0, v2, LX/Ccj;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1WM;

    invoke-virtual {p0, p1, p2}, LX/1WM;->a(Ljava/lang/String;Z)V

    .line 1921557
    const/4 p0, 0x1

    new-array p0, p0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    aput-object v4, p0, v0

    invoke-interface {v3, p0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1921558
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x35848919
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1921559
    check-cast p4, LX/Ccg;

    .line 1921560
    iget-object v0, p0, LX/Cch;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ccj;

    iget-object v3, p4, LX/Ccg;->a:Ljava/lang/String;

    iget-boolean v4, p4, LX/Ccg;->b:Z

    iget-boolean v5, p4, LX/Ccg;->c:Z

    move-object v1, p1

    move v2, p3

    const/4 p4, 0x2

    const/4 p3, 0x1

    const/4 v7, 0x0

    .line 1921561
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0b19ef

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 1921562
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b19f0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    .line 1921563
    invoke-virtual {v1}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v5}, LX/Cce;->c(Z)I

    move-result p0

    invoke-static {v0, v9, v3, p0, v4}, LX/Ccj;->a(LX/Ccj;Landroid/content/Context;Ljava/lang/String;IZ)Landroid/text/SpannableString;

    move-result-object v9

    .line 1921564
    invoke-static {v2}, LX/1mh;->b(I)I

    move-result p0

    .line 1921565
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, p3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p1

    if-ge p0, v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v9, 0x7f0b0050

    invoke-virtual {v6, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v9, 0x7f0a0102

    invoke-virtual {v6, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v6, v9}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    if-lt p0, v8, :cond_1

    const v6, 0x7f0b19f2

    :goto_1
    invoke-interface {v9, p3, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const/4 v9, 0x3

    if-lt p0, v8, :cond_2

    const v6, 0x7f0b19f2

    :goto_2
    invoke-interface {v7, v9, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x6

    const v8, 0x7f0b19f4

    invoke-interface {v6, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/Ccj;->e:LX/2g9;

    invoke-virtual {v7, v1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v7

    const/16 v8, 0x402

    invoke-virtual {v7, v8}, LX/2gA;->h(I)LX/2gA;

    move-result-object v7

    invoke-static {v5}, LX/Cce;->d(Z)I

    move-result v8

    invoke-virtual {v7, v8}, LX/2gA;->i(I)LX/2gA;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    .line 1921566
    const v8, -0x35848919    # -4119993.8f

    const/4 v9, 0x0

    invoke-static {v1, v8, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v8, v8

    .line 1921567
    invoke-interface {v7, v8}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1921568
    return-object v0

    :cond_0
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const p2, 0x7f021ae3

    invoke-virtual {v6, p2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const p2, 0x7f0b19f1

    invoke-interface {v6, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const p2, 0x7f0b19f1

    invoke-interface {v6, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    goto/16 :goto_0

    :cond_1
    move v6, v7

    goto :goto_1

    :cond_2
    const v6, 0x7f0b19f3

    goto :goto_2
.end method

.method public final c(LX/1De;)LX/Ccf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Cch",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1921541
    new-instance v1, LX/Ccg;

    invoke-direct {v1, p0}, LX/Ccg;-><init>(LX/Cch;)V

    .line 1921542
    iget-object v2, p0, LX/Cch;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ccf;

    .line 1921543
    if-nez v2, :cond_0

    .line 1921544
    new-instance v2, LX/Ccf;

    invoke-direct {v2, p0}, LX/Ccf;-><init>(LX/Cch;)V

    .line 1921545
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Ccf;->a$redex0(LX/Ccf;LX/1De;IILX/Ccg;)V

    .line 1921546
    move-object v1, v2

    .line 1921547
    move-object v0, v1

    .line 1921548
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1921540
    const/4 v0, 0x1

    return v0
.end method
