.class public LX/AaQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:LX/0Tn;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1688519
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "broadcaster_tip_jar_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AaQ;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688517
    iput-object p1, p0, LX/AaQ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1688518
    return-void
.end method

.method public static a(LX/0QB;)LX/AaQ;
    .locals 1

    .prologue
    .line 1688510
    invoke-static {p0}, LX/AaQ;->b(LX/0QB;)LX/AaQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AaQ;
    .locals 2

    .prologue
    .line 1688514
    new-instance v1, LX/AaQ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/AaQ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1688515
    return-object v1
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1688512
    iget-object v0, p0, LX/AaQ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AaQ;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1688513
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1688511
    iget-object v0, p0, LX/AaQ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AaQ;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
