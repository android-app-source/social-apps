.class public final LX/BLo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4zq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4zq",
        "<",
        "LX/2rw;",
        "LX/BLp;",
        "LX/BLq;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BKL;

.field public final synthetic b:LX/BLr;


# direct methods
.method public constructor <init>(LX/BLr;LX/BKL;)V
    .locals 0

    .prologue
    .line 1776449
    iput-object p1, p0, LX/BLo;->b:LX/BLr;

    iput-object p2, p0, LX/BLo;->a:LX/BKL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1776431
    check-cast p1, LX/2rw;

    check-cast p2, LX/BLp;

    const/4 v1, 0x1

    .line 1776432
    iget-object v0, p0, LX/BLo;->a:LX/BKL;

    .line 1776433
    iget-object v2, v0, LX/BKL;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->m(LX/0Px;)Z

    move-result v2

    move v0, v2

    .line 1776434
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BLo;->b:LX/BLr;

    iget-object v0, v0, LX/BLr;->h:LX/31w;

    invoke-virtual {v0, p1}, LX/31w;->a(LX/2rw;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1776435
    iput-boolean v1, p2, LX/BLp;->a:Z

    .line 1776436
    move-object v0, p2

    .line 1776437
    invoke-virtual {v0}, LX/BLp;->a()LX/BLq;

    move-result-object v0

    .line 1776438
    :goto_0
    return-object v0

    .line 1776439
    :cond_0
    iget-object v0, p0, LX/BLo;->a:LX/BKL;

    .line 1776440
    iget-object v2, v0, LX/BKL;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v2

    move v0, v2

    .line 1776441
    if-eqz v0, :cond_1

    .line 1776442
    sget-object v2, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne p1, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1776443
    move v0, v2

    .line 1776444
    if-nez v0, :cond_1

    .line 1776445
    iput-boolean v1, p2, LX/BLp;->a:Z

    .line 1776446
    move-object v0, p2

    .line 1776447
    invoke-virtual {v0}, LX/BLp;->a()LX/BLq;

    move-result-object v0

    goto :goto_0

    .line 1776448
    :cond_1
    invoke-virtual {p2}, LX/BLp;->a()LX/BLq;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
