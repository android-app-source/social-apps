.class public LX/AwT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/AwR;

.field private final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1726644
    const-class v0, LX/1uy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AwT;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AwR;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1726645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726646
    iput-object p1, p0, LX/AwT;->b:LX/AwR;

    .line 1726647
    iput-object p2, p0, LX/AwT;->c:Ljava/lang/String;

    .line 1726648
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1726649
    const/4 v0, 0x0

    .line 1726650
    :try_start_0
    iget-object v1, p0, LX/AwT;->b:LX/AwR;

    iget-object v2, p0, LX/AwT;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/AwR;->a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1726651
    :goto_0
    if-nez v1, :cond_0

    :goto_1
    return-object v0

    .line 1726652
    :catch_0
    move-exception v1

    .line 1726653
    sget-object v2, LX/AwT;->a:Ljava/lang/String;

    const-string v3, "error while streaming."

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    goto :goto_0

    .line 1726654
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
