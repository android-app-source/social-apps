.class public LX/BKU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/ATP;

.field public final b:LX/26H;

.field public final c:LX/26G;

.field public final d:LX/1Ad;

.field public final e:LX/0W3;

.field public final f:LX/0ad;

.field public final g:LX/7Dh;

.field public final h:Landroid/view/View;

.field public final i:Landroid/view/View;

.field public final j:Landroid/view/View;

.field public final k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

.field public final l:Landroid/view/View;

.field public m:Landroid/widget/ScrollView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:LX/ATO;

.field public p:Z

.field private final q:LX/Aj7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Aj7",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;LX/ATP;LX/26H;LX/26G;LX/1Ad;LX/0ad;LX/7Dh;LX/0W3;)V
    .locals 2
    .param p1    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1773829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1773830
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BKU;->p:Z

    .line 1773831
    new-instance v0, LX/BKS;

    invoke-direct {v0, p0}, LX/BKS;-><init>(LX/BKU;)V

    iput-object v0, p0, LX/BKU;->q:LX/Aj7;

    .line 1773832
    iput-object p2, p0, LX/BKU;->a:LX/ATP;

    .line 1773833
    iput-object p3, p0, LX/BKU;->b:LX/26H;

    .line 1773834
    iput-object p4, p0, LX/BKU;->c:LX/26G;

    .line 1773835
    iput-object p5, p0, LX/BKU;->d:LX/1Ad;

    .line 1773836
    iput-object p6, p0, LX/BKU;->f:LX/0ad;

    .line 1773837
    iput-object p7, p0, LX/BKU;->g:LX/7Dh;

    .line 1773838
    iput-object p8, p0, LX/BKU;->e:LX/0W3;

    .line 1773839
    const v0, 0x7f030fc2

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1773840
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BKU;->h:Landroid/view/View;

    .line 1773841
    iget-object v0, p0, LX/BKU;->h:Landroid/view/View;

    const v1, 0x7f0d2606

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BKU;->j:Landroid/view/View;

    .line 1773842
    iget-object v0, p0, LX/BKU;->h:Landroid/view/View;

    const v1, 0x7f0d1cc1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iput-object v0, p0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1773843
    iget-object v0, p0, LX/BKU;->h:Landroid/view/View;

    const v1, 0x7f0d2607

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BKU;->l:Landroid/view/View;

    .line 1773844
    iget-object v0, p0, LX/BKU;->h:Landroid/view/View;

    const v1, 0x7f0d2605

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BKU;->i:Landroid/view/View;

    .line 1773845
    iget-object v0, p0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v1, p0, LX/BKU;->q:LX/Aj7;

    .line 1773846
    iput-object v1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 1773847
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1773848
    iget-object v0, p0, LX/BKU;->o:LX/ATO;

    sget-object v1, LX/03R;->NO:LX/03R;

    invoke-virtual {v0, p1, v1}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 1773849
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1773850
    iget-object v0, p0, LX/BKU;->o:LX/ATO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/ATO;->b(Z)V

    .line 1773851
    iget-object v0, p0, LX/BKU;->o:LX/ATO;

    invoke-virtual {v0}, LX/ATO;->l()V

    .line 1773852
    return-void
.end method
