.class public final LX/AfF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AfG;


# direct methods
.method public constructor <init>(LX/AfG;)V
    .locals 0

    .prologue
    .line 1698836
    iput-object p1, p0, LX/AfF;->a:LX/AfG;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1698866
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698867
    :cond_0
    :goto_0
    return-void

    .line 1698868
    :cond_1
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v1, v0, LX/AfG;->f:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AfG;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_graphFailure"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get vod comment events for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AfF;->a:LX/AfG;

    iget-object v3, v3, LX/Aec;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/Aec;->b:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1698869
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    if-eqz v0, :cond_0

    .line 1698870
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/Aec;->a:LX/Aeb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/Aeb;->c(Z)V

    goto :goto_0

    .line 1698871
    :cond_2
    const-string v0, "no story id"

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1698837
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 1698838
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v0, v0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698839
    :cond_0
    :goto_0
    return-void

    .line 1698840
    :cond_1
    if-eqz p1, :cond_2

    .line 1698841
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698842
    if-eqz v0, :cond_2

    .line 1698843
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698844
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1698845
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698846
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1698847
    iget-object v0, p0, LX/AfF;->a:LX/AfG;

    iget-object v1, v0, LX/AfG;->h:LX/Aee;

    .line 1698848
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698849
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;->s()LX/0Px;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v3, v2}, LX/Aee;->a(LX/0Px;ZI)V

    .line 1698850
    :cond_2
    const/4 v1, 0x0

    .line 1698851
    if-nez p1, :cond_3

    move-object v0, v1

    .line 1698852
    :goto_1
    move-object v0, v0

    .line 1698853
    iget-object v1, p0, LX/AfF;->a:LX/AfG;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    if-eqz v1, :cond_0

    .line 1698854
    iget-object v1, p0, LX/AfF;->a:LX/AfG;

    iget-object v1, v1, LX/Aec;->a:LX/Aeb;

    invoke-interface {v1, v0, v3}, LX/Aeb;->a(Ljava/util/List;Z)V

    goto :goto_0

    .line 1698855
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698856
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    .line 1698857
    if-nez v0, :cond_4

    move-object v0, v1

    .line 1698858
    goto :goto_1

    .line 1698859
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;

    move-result-object v0

    .line 1698860
    if-nez v0, :cond_5

    move-object v0, v1

    .line 1698861
    goto :goto_1

    .line 1698862
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;->t()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;

    move-result-object v2

    .line 1698863
    if-nez v2, :cond_6

    move-object v0, v1

    .line 1698864
    goto :goto_1

    .line 1698865
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoVODCommentsFragmentModel;->t()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;

    move-result-object v0

    invoke-static {v0}, LX/AfG;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;)Ljava/util/LinkedList;

    move-result-object v0

    goto :goto_1
.end method
