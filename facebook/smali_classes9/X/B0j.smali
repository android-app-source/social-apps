.class public abstract LX/B0j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;
.implements LX/1jy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TField:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/3Bq;",
        "LX/1jy;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/18L;

.field private final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<TTField;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTField;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;ILjava/lang/String;LX/0Rf;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<TTField;>;TTField;)V"
        }
    .end annotation

    .prologue
    .line 1734141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734142
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    iput-object v0, p0, LX/B0j;->b:LX/18L;

    .line 1734143
    iput p2, p0, LX/B0j;->d:I

    .line 1734144
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/B0j;->c:LX/0Rf;

    .line 1734145
    iput-object p4, p0, LX/B0j;->f:LX/0Rf;

    .line 1734146
    iput-object p3, p0, LX/B0j;->e:Ljava/lang/String;

    .line 1734147
    iput-object p5, p0, LX/B0j;->g:Ljava/lang/Object;

    .line 1734148
    return-void
.end method

.method private a(LX/0jT;)V
    .locals 3

    .prologue
    .line 1734149
    invoke-interface {p1}, LX/0jT;->f()I

    move-result v0

    iget v1, p0, LX/B0j;->d:I

    if-eq v0, v1, :cond_1

    .line 1734150
    :cond_0
    :goto_0
    return-void

    .line 1734151
    :cond_1
    instance-of v0, p1, LX/16f;

    if-eqz v0, :cond_0

    .line 1734152
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/B0j;->c:LX/0Rf;

    move-object v0, p1

    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734153
    check-cast p1, LX/16f;

    .line 1734154
    iget-object v0, p0, LX/B0j;->e:Ljava/lang/String;

    iget-object v1, p0, LX/B0j;->b:LX/18L;

    invoke-interface {p1, v0, v1}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 1734155
    iget-object v0, p0, LX/B0j;->b:LX/18L;

    iget-object v0, v0, LX/18L;->a:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B0j;->f:LX/0Rf;

    iget-object v1, p0, LX/B0j;->b:LX/18L;

    iget-object v1, v1, LX/18L;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734156
    :cond_2
    iget-object v0, p0, LX/B0j;->e:Ljava/lang/String;

    iget-object v1, p0, LX/B0j;->g:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TModel:",
            "Ljava/lang/Object;",
            ">(TTModel;Z)TTModel;"
        }
    .end annotation

    .prologue
    .line 1734157
    monitor-enter p0

    :try_start_0
    instance-of v1, p1, LX/0jT;

    if-eqz v1, :cond_0

    .line 1734158
    move-object v0, p1

    check-cast v0, LX/0jT;

    move-object v1, v0

    invoke-virtual {p0, v1}, LX/B0j;->b(LX/0jT;)LX/0jT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1734159
    :cond_0
    monitor-exit p0

    return-object p1

    .line 1734160
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1734161
    iget-object v0, p0, LX/B0j;->c:LX/0Rf;

    return-object v0
.end method

.method public final b(LX/0jT;)LX/0jT;
    .locals 0

    .prologue
    .line 1734162
    invoke-direct {p0, p1}, LX/B0j;->a(LX/0jT;)V

    .line 1734163
    invoke-interface {p1, p0}, LX/0jT;->a(LX/1jy;)LX/0jT;

    .line 1734164
    return-object p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1734165
    const-string v0, "ConditionalFieldUpdatingVisitor"

    return-object v0
.end method
