.class public final LX/Akj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:Z

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:Ljava/lang/String;

.field public final synthetic i:Ljava/lang/String;

.field public final synthetic j:Ljava/lang/String;

.field public final synthetic k:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1709279
    iput-object p1, p0, LX/Akj;->k:LX/1wH;

    iput-object p2, p0, LX/Akj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Akj;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Akj;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iput-boolean p5, p0, LX/Akj;->d:Z

    iput-object p6, p0, LX/Akj;->e:Ljava/lang/String;

    iput-object p7, p0, LX/Akj;->f:Ljava/lang/String;

    iput-object p8, p0, LX/Akj;->g:Ljava/lang/String;

    iput-object p9, p0, LX/Akj;->h:Ljava/lang/String;

    iput-object p10, p0, LX/Akj;->i:Ljava/lang/String;

    iput-object p11, p0, LX/Akj;->j:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1709280
    iget-object v0, p0, LX/Akj;->k:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v1, p0, LX/Akj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Akj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v9}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1709281
    iget-object v0, p0, LX/Akj;->k:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->M:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1709282
    iget-object v0, p0, LX/Akj;->k:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v1, v0, LX/1SX;->N:LX/0tX;

    iget-object v0, p0, LX/Akj;->k:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v2, v0, LX/1SX;->O:LX/1Sm;

    iget-object v3, p0, LX/Akj;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iget-boolean v0, p0, LX/Akj;->d:Z

    if-nez v0, :cond_0

    move v0, v9

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/1Sm;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)LX/Al2;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/4V2;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1709283
    :goto_1
    return v9

    :cond_0
    move v0, v8

    .line 1709284
    goto :goto_0

    .line 1709285
    :cond_1
    iget-object v0, p0, LX/Akj;->k:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v10, v0, LX/1SX;->l:LX/0bH;

    new-instance v0, LX/1Nc;

    iget-object v1, p0, LX/Akj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Akj;->e:Ljava/lang/String;

    iget-object v3, p0, LX/Akj;->f:Ljava/lang/String;

    iget-object v4, p0, LX/Akj;->g:Ljava/lang/String;

    iget-object v5, p0, LX/Akj;->h:Ljava/lang/String;

    iget-object v6, p0, LX/Akj;->i:Ljava/lang/String;

    iget-object v7, p0, LX/Akj;->j:Ljava/lang/String;

    iget-boolean v11, p0, LX/Akj;->d:Z

    if-nez v11, :cond_2

    move v8, v9

    :cond_2
    invoke-direct/range {v0 .. v8}, LX/1Nc;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v10, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method
