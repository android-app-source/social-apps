.class public LX/BTS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Landroid/net/Uri;

.field public final c:LX/0TD;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/1FZ;

.field private final f:LX/1Ad;

.field public final g:LX/BV0;

.field public final h:LX/7SE;

.field public final i:J

.field private final j:I

.field private k:I

.field private l:I

.field public m:F

.field public n:Landroid/net/Uri;

.field public o:LX/7Sv;

.field public p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;ZLX/7Sv;LX/0TD;Ljava/util/concurrent/Executor;LX/1FZ;LX/1Ad;LX/BV0;LX/7SE;)V
    .locals 4
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/7Sv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787506
    iput-object p1, p0, LX/BTS;->a:Landroid/content/Context;

    .line 1787507
    iput-object p2, p0, LX/BTS;->b:Landroid/net/Uri;

    .line 1787508
    iput-object p3, p0, LX/BTS;->n:Landroid/net/Uri;

    .line 1787509
    iput-object p5, p0, LX/BTS;->o:LX/7Sv;

    .line 1787510
    iput-object p6, p0, LX/BTS;->c:LX/0TD;

    .line 1787511
    iput-object p7, p0, LX/BTS;->d:Ljava/util/concurrent/Executor;

    .line 1787512
    iput-object p8, p0, LX/BTS;->e:LX/1FZ;

    .line 1787513
    iput-object p9, p0, LX/BTS;->f:LX/1Ad;

    .line 1787514
    iput-object p10, p0, LX/BTS;->g:LX/BV0;

    .line 1787515
    iput-object p11, p0, LX/BTS;->h:LX/7SE;

    .line 1787516
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1787517
    iget-object v0, p0, LX/BTS;->a:Landroid/content/Context;

    iget-object v2, p0, LX/BTS;->b:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1787518
    invoke-static {v1}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;)J

    move-result-wide v2

    iput-wide v2, p0, LX/BTS;->i:J

    .line 1787519
    invoke-static {v1}, LX/BTk;->e(Landroid/media/MediaMetadataRetriever;)I

    move-result v0

    iput v0, p0, LX/BTS;->j:I

    .line 1787520
    invoke-static {v1}, LX/BTk;->c(Landroid/media/MediaMetadataRetriever;)I

    move-result v0

    iput v0, p0, LX/BTS;->k:I

    .line 1787521
    invoke-static {v1}, LX/BTk;->d(Landroid/media/MediaMetadataRetriever;)I

    move-result v0

    iput v0, p0, LX/BTS;->l:I

    .line 1787522
    iget v0, p0, LX/BTS;->j:I

    rem-int/lit16 v0, v0, 0xb4

    if-eqz v0, :cond_0

    .line 1787523
    iget v0, p0, LX/BTS;->k:I

    .line 1787524
    iget v2, p0, LX/BTS;->l:I

    iput v2, p0, LX/BTS;->k:I

    .line 1787525
    iput v0, p0, LX/BTS;->l:I

    .line 1787526
    :cond_0
    if-eqz p4, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, LX/BTS;->m:F

    .line 1787527
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1787528
    return-void

    .line 1787529
    :cond_1
    iget v0, p0, LX/BTS;->k:I

    int-to-float v0, v0

    iget v2, p0, LX/BTS;->l:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_0
.end method

.method public static a(LX/BTS;IIILjava/io/File;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/io/File;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1787498
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1787499
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 1787500
    int-to-long v2, v0

    iget-wide v4, p0, LX/BTS;->i:J

    mul-long/2addr v2, v4

    int-to-long v4, p1

    div-long/2addr v2, v4

    .line 1787501
    new-instance v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v7, p0, LX/BTS;->f:LX/1Ad;

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;-><init>(JIILjava/io/File;LX/1Ad;)V

    .line 1787502
    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1787503
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1787504
    :cond_0
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/BTS;LX/0Px;LX/0Px;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1787483
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 1787484
    :cond_0
    return-void

    .line 1787485
    :cond_1
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v4, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->b:Landroid/net/Uri;

    .line 1787486
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    .line 1787487
    iget-object v5, p0, LX/BTS;->f:LX/1Ad;

    invoke-virtual {v0, v5, v4}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a(LX/1Ad;Landroid/net/Uri;)V

    .line 1787488
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1787489
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-wide v2, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    .line 1787490
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v7

    move v6, v1

    :goto_1
    if-ge v6, v7, :cond_0

    invoke-virtual {p2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    .line 1787491
    iget-wide v8, v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    cmp-long v5, v8, v2

    if-lez v5, :cond_3

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 1787492
    add-int/lit8 v4, v1, 0x1

    .line 1787493
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-object v5, v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->b:Landroid/net/Uri;

    .line 1787494
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    if-ge v1, v8, :cond_4

    .line 1787495
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    iget-wide v2, v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    move v1, v4

    move-object v4, v5

    .line 1787496
    :cond_3
    :goto_2
    iget-object v5, p0, LX/BTS;->f:LX/1Ad;

    invoke-virtual {v0, v5, v4}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a(LX/1Ad;Landroid/net/Uri;)V

    .line 1787497
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_4
    move v1, v4

    move-object v4, v5

    goto :goto_2
.end method

.method public static synthetic a(LX/BUz;LX/1FZ;JFIILjava/lang/String;)V
    .locals 8

    .prologue
    .line 1787450
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1787451
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1787452
    :cond_0
    :goto_0
    return-void

    .line 1787453
    :cond_1
    long-to-int v0, p2

    const/4 v3, 0x0

    .line 1787454
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    const/4 v1, 0x2

    if-ge v4, v1, :cond_3

    .line 1787455
    :try_start_0
    invoke-virtual {p0, v0, p4}, LX/BUz;->a(IF)LX/1FJ;

    move-result-object v1

    .line 1787456
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    move-result-object v5

    if-eqz v5, :cond_2

    .line 1787457
    :goto_2
    move-object v3, v1

    .line 1787458
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1787459
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1787460
    int-to-float v1, p5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    .line 1787461
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1787462
    invoke-virtual {v4, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1787463
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1, p5, p6, v1}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v5

    .line 1787464
    invoke-virtual {v5}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 1787465
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1787466
    const/4 v7, 0x0

    invoke-virtual {v6, v0, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1787467
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ".tmp"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1787468
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1787469
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x32

    invoke-virtual {v1, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1787470
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1787471
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1787472
    invoke-virtual {v3}, LX/1FJ;->close()V

    .line 1787473
    invoke-virtual {v5}, LX/1FJ;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1787474
    :catch_0
    move-exception v0

    .line 1787475
    const-string v1, "VideoStripController"

    const-string v2, "No video file found at given location"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1787476
    :catch_1
    move-exception v0

    .line 1787477
    const-string v1, "VideoStripController"

    const-string v2, "Ran into a problem extracting thumbnail"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1787478
    :catch_2
    move-exception v1

    .line 1787479
    const-string v4, "VideoStripController"

    const-string v5, "Unable to extract frame"

    invoke-static {v4, v5, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    .line 1787480
    goto :goto_2

    .line 1787481
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_1

    :cond_3
    move-object v1, v3

    .line 1787482
    goto/16 :goto_2
.end method

.method public static b(LX/BTS;)Ljava/io/File;
    .locals 3

    .prologue
    .line 1787420
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/BTS;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "video-creative-editing"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static e(LX/BTS;)V
    .locals 3

    .prologue
    .line 1787448
    iget-object v0, p0, LX/BTS;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$2;

    invoke-direct {v1, p0}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$2;-><init>(LX/BTS;)V

    const v2, -0x47d7568d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1787449
    return-void
.end method

.method public static f(LX/BTS;)V
    .locals 2

    .prologue
    .line 1787443
    iget-object v0, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 1787444
    :goto_0
    return-void

    .line 1787445
    :cond_0
    iget-object v0, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1787446
    iget-object v0, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1787447
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;II)V
    .locals 9

    .prologue
    .line 1787421
    iget-object v0, p0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    if-eqz v0, :cond_0

    .line 1787422
    :goto_0
    return-void

    .line 1787423
    :cond_0
    iput-object p1, p0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1787424
    iget-object v0, p0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getWidth()I

    move-result v0

    iput v0, p0, LX/BTS;->q:I

    .line 1787425
    iget-object v0, p0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getHeight()I

    move-result v0

    iput v0, p0, LX/BTS;->r:I

    .line 1787426
    iget v0, p0, LX/BTS;->r:I

    iput v0, p0, LX/BTS;->t:I

    .line 1787427
    iget v0, p0, LX/BTS;->t:I

    int-to-float v0, v0

    iget p1, p0, LX/BTS;->m:F

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, LX/BTS;->s:I

    .line 1787428
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, LX/BTS;->b(LX/BTS;)Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "strip-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1787429
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1787430
    move-object v1, v1

    .line 1787431
    iget v2, p0, LX/BTS;->q:I

    sub-int/2addr v2, p2

    sub-int/2addr v2, p3

    .line 1787432
    iget v3, p0, LX/BTS;->s:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, LX/BTS;->s:I

    div-int/2addr v2, v3

    .line 1787433
    mul-int/lit8 v3, v2, 0x2

    iget-wide v5, p0, LX/BTS;->i:J

    const-wide/16 v7, 0x3e7

    add-long/2addr v5, v7

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1787434
    iget v4, p0, LX/BTS;->s:I

    iget v5, p0, LX/BTS;->t:I

    invoke-static {p0, v2, v4, v5, v1}, LX/BTS;->a(LX/BTS;IIILjava/io/File;)LX/0Px;

    move-result-object v2

    .line 1787435
    iget v4, p0, LX/BTS;->s:I

    iget v5, p0, LX/BTS;->t:I

    invoke-static {p0, v3, v4, v5, v1}, LX/BTS;->a(LX/BTS;IIILjava/io/File;)LX/0Px;

    move-result-object v1

    .line 1787436
    invoke-static {p0, v2, v1}, LX/BTS;->a(LX/BTS;LX/0Px;LX/0Px;)V

    .line 1787437
    new-instance v3, LX/BTR;

    iget v4, p0, LX/BTS;->q:I

    invoke-direct {v3, v2, p2, p3, v4}, LX/BTR;-><init>(LX/0Px;III)V

    .line 1787438
    new-instance v4, LX/BTR;

    const/4 v5, 0x0

    invoke-direct {v4, v1, p2, p3, v5}, LX/BTR;-><init>(LX/0Px;III)V

    .line 1787439
    iget-object v5, p0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v5, v3, v4}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a(LX/BTR;LX/BTR;)V

    .line 1787440
    invoke-static {p0}, LX/BTS;->f(LX/BTS;)V

    .line 1787441
    iget-object v3, p0, LX/BTS;->c:LX/0TD;

    new-instance v4, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;

    invoke-direct {v4, p0, v2, v1}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;-><init>(LX/BTS;LX/0Px;LX/0Px;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, p0, LX/BTS;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1787442
    goto/16 :goto_0
.end method
