.class public LX/CXd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910473
    iput-object p1, p0, LX/CXd;->a:LX/0Ot;

    .line 1910474
    return-void
.end method

.method public static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1910475
    const-string v0, "[PLATFORM][FATAL]"

    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910476
    return-void
.end method

.method public static b(LX/0QB;)LX/CXd;
    .locals 2

    .prologue
    .line 1910477
    new-instance v0, LX/CXd;

    const/16 v1, 0x259

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CXd;-><init>(LX/0Ot;)V

    .line 1910478
    return-object v0
.end method


# virtual methods
.method public final a(LX/CXc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1910479
    sget-object v0, LX/CXb;->a:[I

    invoke-virtual {p1}, LX/CXc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1910480
    :goto_0
    return-void

    .line 1910481
    :pswitch_0
    iget-object v0, p0, LX/CXd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, p2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
