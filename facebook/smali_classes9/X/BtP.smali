.class public LX/BtP;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1829098
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BtP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829099
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1829100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BtP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1829101
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1829102
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1829103
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1829104
    return-void
.end method

.method public static a(Landroid/content/Context;I)LX/BtP;
    .locals 5

    .prologue
    .line 1829105
    new-instance v1, LX/BtP;

    invoke-direct {v1, p0}, LX/BtP;-><init>(Landroid/content/Context;)V

    .line 1829106
    const v0, 0x7f0d04fd

    invoke-virtual {v1, v0}, LX/BtP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1829107
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1829108
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, p1, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1829109
    return-object v1
.end method
