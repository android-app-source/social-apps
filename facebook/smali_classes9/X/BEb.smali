.class public final LX/BEb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;",
        ">;",
        "Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BEd;


# direct methods
.method public constructor <init>(LX/BEd;)V
    .locals 0

    .prologue
    .line 1764517
    iput-object p1, p0, LX/BEb;->a:LX/BEd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1764518
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1764519
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1764520
    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    .line 1764521
    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->c()LX/2uF;

    move-result-object v3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1764522
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1764523
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 1764524
    const-class v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v7, v8, v11, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764525
    invoke-virtual {v7, v8, v12}, LX/15i;->g(II)I

    move-result v4

    const v9, 0x4f20e94d    # 2.69964416E9f

    invoke-static {v7, v4, v9}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1764526
    invoke-virtual {v7, v8, v12}, LX/15i;->g(II)I

    move-result v4

    invoke-virtual {v7, v4, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764527
    invoke-virtual {v7, v8, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764528
    invoke-virtual {v7, v8, v12}, LX/15i;->g(II)I

    move-result v9

    .line 1764529
    new-instance v10, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;

    const-class v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v7, v8, v11, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4}, LX/BEe;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/BEp;

    move-result-object v4

    invoke-virtual {v7, v9, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v7, v8, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v10, v4, v9, v7}, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;-><init>(LX/BEp;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1764530
    invoke-virtual {v5, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1764531
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 1764532
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->b()LX/2uF;

    move-result-object v4

    const/4 v0, 0x3

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v13, 0x0

    .line 1764533
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1764534
    invoke-virtual {v4}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object v8, v5, LX/1vs;->a:LX/15i;

    iget v9, v5, LX/1vs;->b:I

    .line 1764535
    const-class v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v8, v9, v13, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764536
    invoke-virtual {v8, v9, p0}, LX/15i;->g(II)I

    move-result v5

    const v10, -0x286be87c

    invoke-static {v8, v5, v10}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1764537
    invoke-virtual {v8, v9, p0}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v8, v5, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764538
    invoke-virtual {v8, v9, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764539
    invoke-virtual {v8, v9, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764540
    invoke-virtual {v8, v9, p0}, LX/15i;->g(II)I

    move-result v10

    .line 1764541
    new-instance v11, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    const-class v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v8, v9, v13, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v5}, LX/BEe;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/BEp;

    move-result-object v5

    invoke-virtual {v8, v10, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v8, v9, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v8, v9, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v11, v5, v10, v12, v8}, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;-><init>(LX/BEp;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1764542
    invoke-virtual {v6, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1764543
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v4, v5

    .line 1764544
    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    move-object v0, v1

    .line 1764545
    return-object v0
.end method
