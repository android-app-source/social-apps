.class public LX/Bzs;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bzs",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839437
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839438
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bzs;->b:LX/0Zi;

    .line 1839439
    iput-object p1, p0, LX/Bzs;->a:LX/0Ot;

    .line 1839440
    return-void
.end method

.method public static a(LX/0QB;)LX/Bzs;
    .locals 4

    .prologue
    .line 1839441
    const-class v1, LX/Bzs;

    monitor-enter v1

    .line 1839442
    :try_start_0
    sget-object v0, LX/Bzs;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839443
    sput-object v2, LX/Bzs;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839444
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839445
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839446
    new-instance v3, LX/Bzs;

    const/16 p0, 0x1e52

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bzs;-><init>(LX/0Ot;)V

    .line 1839447
    move-object v0, v3

    .line 1839448
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839449
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bzs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839450
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1839452
    check-cast p2, LX/Bzr;

    .line 1839453
    iget-object v0, p0, LX/Bzs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;

    iget-object v1, p2, LX/Bzr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bzr;->b:LX/1Po;

    const/4 v7, 0x4

    const/4 v10, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    .line 1839454
    const/4 v4, 0x0

    .line 1839455
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1839456
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1839457
    if-nez v3, :cond_2

    move-object v3, v4

    .line 1839458
    :goto_0
    move-object v4, v3

    .line 1839459
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x6

    const v6, 0x7f0b00bf

    invoke-interface {v3, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0b00f6

    invoke-interface {v3, v10, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    .line 1839460
    const v5, -0xd9417e5

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1839461
    invoke-interface {v3, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    if-nez v4, :cond_1

    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v3

    const v7, 0x7f021749

    invoke-virtual {v3, v7}, LX/1nm;->h(I)LX/1nm;

    move-result-object v3

    :goto_1
    invoke-interface {v6, v3}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    const v6, 0x7f0b1c89

    invoke-interface {v3, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    const/4 p0, 0x1

    .line 1839462
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-interface {v3, v7, v8}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v3

    const/4 v7, 0x2

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    move-object v7, v3

    .line 1839463
    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->d:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v8

    move-object v3, v2

    check-cast v3, LX/1Pp;

    invoke-virtual {v8, v3}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v4

    const/4 v8, -0x1

    .line 1839464
    iput v8, v4, LX/4Ab;->f:I

    .line 1839465
    move-object v4, v4

    .line 1839466
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b1c8b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v4, v8}, LX/4Ab;->c(F)LX/4Ab;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v3

    const/4 p2, -0x1

    .line 1839467
    new-instance v4, LX/Bzu;

    invoke-direct {v4}, LX/Bzu;-><init>()V

    .line 1839468
    sget-object v8, LX/Bzv;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Bzt;

    .line 1839469
    if-nez v8, :cond_0

    .line 1839470
    new-instance v8, LX/Bzt;

    invoke-direct {v8}, LX/Bzt;-><init>()V

    .line 1839471
    :cond_0
    invoke-static {v8, p1, v4}, LX/Bzt;->a$redex0(LX/Bzt;LX/1De;LX/Bzu;)V

    .line 1839472
    move-object v4, v8

    .line 1839473
    move-object v8, v4

    .line 1839474
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1839475
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    .line 1839476
    iget-object v9, v8, LX/Bzt;->a:LX/Bzu;

    iput-object v4, v9, LX/Bzu;->a:Ljava/lang/String;

    .line 1839477
    iget-object v9, v8, LX/Bzt;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 1839478
    move-object v4, v8

    .line 1839479
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a019a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    const v9, 0x3f4ccccd    # 0.8f

    invoke-static {v8, v9}, LX/47Z;->a(IF)I

    move-result v8

    .line 1839480
    iget-object v9, v4, LX/Bzt;->a:LX/Bzu;

    iput v8, v9, LX/Bzu;->e:I

    .line 1839481
    move-object v4, v4

    .line 1839482
    sget-object v8, LX/8uh;->ONE_LETTER:LX/8uh;

    .line 1839483
    iget-object v9, v4, LX/Bzt;->a:LX/Bzu;

    iput-object v8, v9, LX/Bzu;->b:LX/8uh;

    .line 1839484
    move-object v4, v4

    .line 1839485
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b005b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    .line 1839486
    iget-object v9, v4, LX/Bzt;->a:LX/Bzu;

    iput v8, v9, LX/Bzu;->f:F

    .line 1839487
    move-object v4, v4

    .line 1839488
    iget-object v8, v4, LX/Bzt;->a:LX/Bzu;

    iput p2, v8, LX/Bzu;->d:I

    .line 1839489
    move-object v4, v4

    .line 1839490
    sget-object v8, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v9, LX/0xr;->REGULAR:LX/0xr;

    const/4 p0, 0x0

    invoke-static {p1, v8, v9, p0}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v8

    .line 1839491
    iget-object v9, v4, LX/Bzt;->a:LX/Bzu;

    iput-object v8, v9, LX/Bzu;->c:Landroid/graphics/Typeface;

    .line 1839492
    move-object v4, v4

    .line 1839493
    iget-object v8, v4, LX/Bzt;->a:LX/Bzu;

    iput p2, v8, LX/Bzu;->g:I

    .line 1839494
    move-object v4, v4

    .line 1839495
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b1c8b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    .line 1839496
    iget-object v9, v4, LX/Bzt;->a:LX/Bzu;

    iput v8, v9, LX/Bzu;->h:F

    .line 1839497
    move-object v4, v4

    .line 1839498
    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    move-object v4, v4

    .line 1839499
    iget-object v8, v3, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object v4, v8, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    .line 1839500
    move-object v3, v3

    .line 1839501
    sget-object v4, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1c8a

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/Ap8;->c(LX/1De;)LX/Ap6;

    move-result-object v4

    .line 1839502
    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->b:LX/1qb;

    .line 1839503
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1839504
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v6, v5}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    move-object v5, v5

    .line 1839505
    invoke-virtual {v4, v5}, LX/Ap6;->a(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v4

    .line 1839506
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1839507
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v5}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1839508
    invoke-virtual {v4, v5}, LX/Ap6;->b(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v4

    .line 1839509
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1839510
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1839511
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1839512
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 1839513
    :goto_2
    move-object v5, v5

    .line 1839514
    invoke-virtual {v4, v5}, LX/Ap6;->c(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v4

    .line 1839515
    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->c:LX/2sO;

    invoke-virtual {v5, v1}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v5

    .line 1839516
    if-nez v5, :cond_6

    const/4 v5, 0x0

    :goto_3
    move-object v5, v5

    .line 1839517
    invoke-virtual {v4, v5}, LX/Ap6;->a(LX/1X1;)LX/Ap6;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1839518
    return-object v0

    :cond_1
    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v3

    const v7, 0x7f0a019a

    invoke-virtual {v3, v7}, LX/1nh;->i(I)LX/1nh;

    move-result-object v3

    goto/16 :goto_1

    .line 1839519
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1839520
    if-nez v3, :cond_3

    move-object v3, v4

    .line 1839521
    goto/16 :goto_0

    .line 1839522
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    .line 1839523
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object v3, v4

    goto/16 :goto_0

    :cond_4
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    :cond_6
    invoke-virtual {v5, p1, v2, v1, v10}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v5

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1839524
    invoke-static {}, LX/1dS;->b()V

    .line 1839525
    iget v0, p1, LX/1dQ;->b:I

    .line 1839526
    packed-switch v0, :pswitch_data_0

    .line 1839527
    :goto_0
    return-object v2

    .line 1839528
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1839529
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1839530
    check-cast v1, LX/Bzr;

    .line 1839531
    iget-object v3, p0, LX/Bzs;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;

    iget-object p1, v1, LX/Bzr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839532
    iget-object p2, v3, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->e:LX/36X;

    invoke-virtual {p2, p1}, LX/36X;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1839533
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xd9417e5
        :pswitch_0
    .end packed-switch
.end method
