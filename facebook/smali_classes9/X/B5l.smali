.class public LX/B5l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5oY;


# direct methods
.method public constructor <init>(LX/5oY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1745954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745955
    iput-object p1, p0, LX/B5l;->a:LX/5oY;

    .line 1745956
    return-void
.end method

.method public static a(Ljava/lang/String;LX/B5m;)LX/B5n;
    .locals 1

    .prologue
    .line 1745963
    new-instance v0, LX/B5n;

    invoke-direct {v0}, LX/B5n;-><init>()V

    .line 1745964
    iput-object p0, v0, LX/B5n;->a:Ljava/lang/String;

    .line 1745965
    move-object v0, v0

    .line 1745966
    iput-object p1, v0, LX/B5n;->b:LX/B5m;

    .line 1745967
    move-object v0, v0

    .line 1745968
    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/B5p;
    .locals 1

    .prologue
    .line 1745962
    sget-object v0, LX/B5m;->PHOTO_REMINDER_TAP_ON_MORE:LX/B5m;

    invoke-static {p0, v0}, LX/B5l;->a(Ljava/lang/String;LX/B5m;)LX/B5n;

    move-result-object v0

    invoke-virtual {v0}, LX/B5n;->a()LX/B5p;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/B5l;
    .locals 2

    .prologue
    .line 1745960
    new-instance v1, LX/B5l;

    invoke-static {p0}, LX/5oY;->a(LX/0QB;)LX/5oY;

    move-result-object v0

    check-cast v0, LX/5oY;

    invoke-direct {v1, v0}, LX/B5l;-><init>(LX/5oY;)V

    .line 1745961
    return-object v1
.end method


# virtual methods
.method public final a(LX/1RN;)LX/B5n;
    .locals 2

    .prologue
    .line 1745957
    iget-object v0, p0, LX/B5l;->a:LX/5oY;

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1745958
    sget-object v1, LX/B5m;->DEFAULT_TAP_ON_PROMPT:LX/B5m;

    invoke-static {v0, v1}, LX/B5l;->a(Ljava/lang/String;LX/B5m;)LX/B5n;

    move-result-object v1

    move-object v0, v1

    .line 1745959
    return-object v0
.end method
