.class public LX/C2i;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/facepile/FacepileView;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private final d:Landroid/graphics/Paint;

.field private e:F

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1844548
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1844549
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/C2i;->setOrientation(I)V

    .line 1844550
    const v0, 0x7f0302cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1844551
    const v0, 0x7f0d09c7

    invoke-virtual {p0, v0}, LX/C2i;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1844552
    invoke-virtual {p0}, LX/C2i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1d75

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/C2i;->f:I

    .line 1844553
    iget-object v0, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setReverseFacesZIndex(Z)V

    .line 1844554
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    .line 1844555
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/C2i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1844556
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/C2i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1844557
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1844558
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1844559
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setLinearText(Z)V

    .line 1844560
    iget-object v0, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/C2i;->e:F

    .line 1844561
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1844537
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1844538
    iget-boolean v0, p0, LX/C2i;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/C2i;->b:Ljava/lang/String;

    .line 1844539
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1844540
    iget-object v1, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1}, Lcom/facebook/fbui/facepile/FacepileView;->getWidth()I

    move-result v1

    iget v2, p0, LX/C2i;->f:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, LX/C2i;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2}, Lcom/facebook/fbui/facepile/FacepileView;->getTop()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v3}, Lcom/facebook/fbui/facepile/FacepileView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, LX/C2i;->e:F

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, LX/C2i;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1844541
    :cond_0
    return-void

    .line 1844542
    :cond_1
    iget-object v0, p0, LX/C2i;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1844543
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1844544
    invoke-virtual {p0}, LX/C2i;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, LX/C2i;->getPaddingStart()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/C2i;->getPaddingEnd()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1}, Lcom/facebook/fbui/facepile/FacepileView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, LX/C2i;->f:I

    sub-int/2addr v0, v1

    .line 1844545
    iget-object v1, p0, LX/C2i;->d:Landroid/graphics/Paint;

    iget-object v2, p0, LX/C2i;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/C2i;->g:Z

    .line 1844546
    return-void

    .line 1844547
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
