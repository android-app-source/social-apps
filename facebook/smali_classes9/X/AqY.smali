.class public LX/AqY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field private a:Lcom/facebook/productionprompts/model/ProductionPrompt;

.field private b:LX/AqX;

.field private c:LX/AqW;

.field private d:LX/1RN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1RN;LX/AqW;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1718053
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1718054
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1718055
    iput-object p2, p0, LX/AqY;->d:LX/1RN;

    .line 1718056
    check-cast v0, LX/1kW;

    .line 1718057
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1718058
    iput-object v0, p0, LX/AqY;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1718059
    new-instance v0, LX/AqX;

    invoke-direct {v0, p1}, LX/AqX;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AqY;->b:LX/AqX;

    .line 1718060
    iput-object p3, p0, LX/AqY;->c:LX/AqW;

    .line 1718061
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718070
    iget-object v0, p0, LX/AqY;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718071
    iget-object v0, p0, LX/AqY;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718068
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718069
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718067
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718066
    iget-object v0, p0, LX/AqY;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718063
    iget-object v0, p0, LX/AqY;->c:LX/AqW;

    invoke-virtual {v0}, LX/AqW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718064
    iget-object v0, p0, LX/AqY;->b:LX/AqX;

    iget-object v1, p0, LX/AqY;->c:LX/AqW;

    iget-object v2, p0, LX/AqY;->d:LX/1RN;

    invoke-virtual {v1, v2}, LX/AqW;->b(LX/1RN;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AqX;->a(LX/0Px;)V

    .line 1718065
    :cond_0
    iget-object v0, p0, LX/AqY;->b:LX/AqX;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1718062
    const/4 v0, 0x0

    return-object v0
.end method
