.class public final LX/Auq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Aur;


# direct methods
.method public constructor <init>(LX/Aur;)V
    .locals 0

    .prologue
    .line 1723277
    iput-object p1, p0, LX/Auq;->a:LX/Aur;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1723278
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->d:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1723279
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->e:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1723280
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->f:LX/9e2;

    invoke-virtual {v0, p2}, LX/9e2;->a(Landroid/view/MotionEvent;)Z

    .line 1723281
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1723282
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    invoke-static {v0}, LX/Aur;->i(LX/Aur;)V

    .line 1723283
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->v:LX/Aut;

    invoke-virtual {v0}, LX/Aut;->b()V

    .line 1723284
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->v:LX/Aut;

    sget-object v1, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    invoke-virtual {v0, v1}, LX/Aut;->a(LX/870;)V

    .line 1723285
    :cond_0
    :goto_0
    return v2

    .line 1723286
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 1723287
    iget-object v0, p0, LX/Auq;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->v:LX/Aut;

    sget-object v1, LX/870;->TEXT_DRAGGING:LX/870;

    invoke-virtual {v0, v1}, LX/Aut;->a(LX/870;)V

    goto :goto_0
.end method
