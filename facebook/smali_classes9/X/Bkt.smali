.class public LX/Bkt;
.super LX/1a1;
.source ""


# instance fields
.field private l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Lcom/facebook/widget/FbImageView;

.field private o:Landroid/view/View$OnClickListener;

.field public p:LX/BjC;

.field public q:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1815100
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1815101
    const v0, 0x7f0d0b89

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Bkt;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1815102
    const v0, 0x7f0d0b8b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Bkt;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1815103
    const v0, 0x7f0d0b8c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/Bkt;->n:Lcom/facebook/widget/FbImageView;

    .line 1815104
    new-instance v0, LX/Bks;

    invoke-direct {v0, p0}, LX/Bks;-><init>(LX/Bkt;)V

    iput-object v0, p0, LX/Bkt;->o:Landroid/view/View$OnClickListener;

    .line 1815105
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;LX/BjC;I)V
    .locals 2
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1815106
    iget-object v0, p0, LX/Bkt;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1815107
    if-eqz p2, :cond_0

    .line 1815108
    iget-object v0, p0, LX/Bkt;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1815109
    :cond_0
    iget-object v0, p0, LX/Bkt;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/Bkt;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1815110
    iget-object v0, p0, LX/Bkt;->n:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1815111
    iput-object p4, p0, LX/Bkt;->p:LX/BjC;

    .line 1815112
    iput p5, p0, LX/Bkt;->q:I

    .line 1815113
    return-void
.end method
