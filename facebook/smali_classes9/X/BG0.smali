.class public final LX/BG0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5fk;

.field public final synthetic b:LX/BG1;


# direct methods
.method public constructor <init>(LX/BG1;LX/5fk;)V
    .locals 0

    .prologue
    .line 1766476
    iput-object p1, p0, LX/BG0;->b:LX/BG1;

    iput-object p2, p0, LX/BG0;->a:LX/5fk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1766477
    sget-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v1, "photo save failed, %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1766478
    iget-object v0, p0, LX/BG0;->b:LX/BG1;

    iget-object v0, v0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1766479
    iput-boolean v4, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->t:Z

    .line 1766480
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766481
    check-cast p1, Landroid/net/Uri;

    .line 1766482
    iget-object v0, p0, LX/BG0;->b:LX/BG1;

    iget-object v0, v0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    .line 1766483
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 1766484
    iput-object p1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->G:Landroid/net/Uri;

    .line 1766485
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 1766486
    iget-boolean v2, v1, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->d:Z

    move v1, v2

    .line 1766487
    if-nez v1, :cond_0

    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->X:Z

    if-eqz v1, :cond_1

    .line 1766488
    :cond_0
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v1}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->C$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;)V

    .line 1766489
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v1, p1}, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->a$redex0(Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;Landroid/net/Uri;)V

    .line 1766490
    :cond_1
    iget-object v0, p0, LX/BG0;->b:LX/BG1;

    iget-object v0, v0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    const/4 v1, 0x0

    .line 1766491
    iput-boolean v1, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->t:Z

    .line 1766492
    return-void
.end method
