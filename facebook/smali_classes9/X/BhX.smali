.class public final LX/BhX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;)V
    .locals 0

    .prologue
    .line 1809313
    iput-object p1, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1809314
    iget-object v0, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    invoke-virtual {v0, p1, p2}, LX/CIm;->a(Landroid/preference/Preference;Ljava/lang/Object;)V

    .line 1809315
    iget-object v0, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    .line 1809316
    iput-boolean v1, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->h:Z

    .line 1809317
    iget-object v0, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->d()V

    .line 1809318
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809319
    iget-object v0, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->e()V

    .line 1809320
    :goto_0
    return v1

    .line 1809321
    :cond_0
    iget-object v0, p0, LX/BhX;->a:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13h;

    invoke-virtual {v0}, LX/13h;->f()V

    goto :goto_0
.end method
