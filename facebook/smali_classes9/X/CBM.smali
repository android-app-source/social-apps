.class public final LX/CBM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CBN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

.field public b:LX/0aC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aC",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/CBN;


# direct methods
.method public constructor <init>(LX/CBN;)V
    .locals 1

    .prologue
    .line 1856319
    iput-object p1, p0, LX/CBM;->c:LX/CBN;

    .line 1856320
    move-object v0, p1

    .line 1856321
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1856322
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856323
    const-string v0, "QuickPromotionNativeTemplateListenerComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/CBN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1856324
    check-cast p1, LX/CBM;

    .line 1856325
    iget-object v0, p1, LX/CBM;->b:LX/0aC;

    iput-object v0, p0, LX/CBM;->b:LX/0aC;

    .line 1856326
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1856327
    if-ne p0, p1, :cond_1

    .line 1856328
    :cond_0
    :goto_0
    return v0

    .line 1856329
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1856330
    goto :goto_0

    .line 1856331
    :cond_3
    check-cast p1, LX/CBM;

    .line 1856332
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1856333
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1856334
    if-eq v2, v3, :cond_0

    .line 1856335
    iget-object v2, p0, LX/CBM;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/CBM;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    iget-object v3, p1, LX/CBM;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1856336
    goto :goto_0

    .line 1856337
    :cond_4
    iget-object v2, p1, LX/CBM;->a:Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1856338
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CBM;

    .line 1856339
    const/4 v1, 0x0

    iput-object v1, v0, LX/CBM;->b:LX/0aC;

    .line 1856340
    return-object v0
.end method
