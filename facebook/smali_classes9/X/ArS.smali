.class public LX/ArS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0Yi;

.field public final c:LX/ArG;

.field public final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final e:LX/199;

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Sh;LX/0Yi;LX/ArG;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/199;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719207
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ArS;->f:Z

    .line 1719208
    iput-object p1, p0, LX/ArS;->a:LX/0Sh;

    .line 1719209
    iput-object p2, p0, LX/ArS;->b:LX/0Yi;

    .line 1719210
    iput-object p3, p0, LX/ArS;->c:LX/ArG;

    .line 1719211
    iput-object p4, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1719212
    iput-object p5, p0, LX/ArS;->e:LX/199;

    .line 1719213
    return-void
.end method

.method public static a(LX/0QB;)LX/ArS;
    .locals 1

    .prologue
    .line 1719205
    invoke-static {p0}, LX/ArS;->b(LX/0QB;)LX/ArS;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1719204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719203
    if-eqz p0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, " 0"

    goto :goto_0
.end method

.method public static a(LX/ArS;II)V
    .locals 3

    .prologue
    .line 1719200
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1719201
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "product_name"

    sget-object v2, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v2}, LX/6KV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 1719202
    return-void
.end method

.method public static a(LX/ArS;IIS)V
    .locals 1

    .prologue
    .line 1719198
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1719199
    return-void
.end method

.method public static a(LX/ArS;ILjava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1719193
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1719194
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1719195
    iget-object v3, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v1, v0}, LX/ArS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v3, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1719196
    goto :goto_0

    .line 1719197
    :cond_0
    return-void
.end method

.method public static a(LX/ArS;IS)V
    .locals 1

    .prologue
    .line 1719191
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1719192
    return-void
.end method

.method public static a(Ljava/util/Map;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719187
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/87Q;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1719188
    sget-object v1, LX/ArO;->PROMPT_ID:LX/ArO;

    invoke-virtual {v1}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->isLoggingDisabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719189
    return-void

    .line 1719190
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/ArS;
    .locals 6

    .prologue
    .line 1719182
    new-instance v0, LX/ArS;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v2

    check-cast v2, LX/0Yi;

    .line 1719183
    new-instance v5, LX/ArG;

    const-class v3, LX/197;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/197;

    invoke-static {p0}, LX/19A;->a(LX/0QB;)LX/19A;

    move-result-object v4

    check-cast v4, LX/19A;

    invoke-direct {v5, v3, v4}, LX/ArG;-><init>(LX/197;LX/19A;)V

    .line 1719184
    move-object v3, v5

    .line 1719185
    check-cast v3, LX/ArG;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/199;->a(LX/0QB;)LX/199;

    move-result-object v5

    check-cast v5, LX/199;

    invoke-direct/range {v0 .. v5}, LX/ArS;-><init>(LX/0Sh;LX/0Yi;LX/ArG;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/199;)V

    .line 1719186
    return-object v0
.end method

.method public static b(LX/ArS;II)V
    .locals 1

    .prologue
    .line 1719180
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 1719181
    return-void
.end method

.method public static d(LX/ArS;I)V
    .locals 3

    .prologue
    .line 1719117
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1719118
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "product_name"

    sget-object v2, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v2}, LX/6KV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 1719119
    return-void
.end method

.method public static f(LX/ArS;)V
    .locals 2

    .prologue
    .line 1719177
    iget-object v0, p0, LX/ArS;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1719178
    iget-boolean v0, p0, LX/ArS;->f:Z

    const-string v1, "Didn\'t start previous page transition marker with #markPageTransitionStart()"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1719179
    return-void
.end method


# virtual methods
.method public final a(LX/ArQ;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const v1, 0xb6000a

    .line 1719162
    invoke-static {p0}, LX/ArS;->f(LX/ArS;)V

    .line 1719163
    iput-boolean v2, p0, LX/ArS;->f:Z

    .line 1719164
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1719165
    :goto_0
    return-void

    .line 1719166
    :cond_0
    iget-object v0, p0, LX/ArS;->c:LX/ArG;

    invoke-virtual {v0}, LX/ArG;->b()V

    .line 1719167
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1719168
    const-string v4, "fps"

    iget-object v5, p0, LX/ArS;->c:LX/ArG;

    const-wide/16 v9, 0x0

    .line 1719169
    iget-wide v7, v5, LX/ArG;->d:D

    cmpl-double v7, v7, v9

    if-eqz v7, :cond_1

    iget-wide v7, v5, LX/ArG;->e:D

    cmpl-double v7, v7, v9

    if-nez v7, :cond_2

    :cond_1
    const-wide/high16 v7, -0x4010000000000000L    # -1.0

    :goto_1
    move-wide v5, v7

    .line 1719170
    invoke-static {v5, v6}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719171
    const-string v4, "to_page"

    invoke-virtual {p1}, LX/ArQ;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719172
    const-string v4, "to_index"

    iget v5, p1, LX/ArQ;->index:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719173
    move-object v0, v3

    .line 1719174
    invoke-static {p0, v1, v0}, LX/ArS;->a(LX/ArS;ILjava/util/Map;)V

    .line 1719175
    const/4 v0, 0x2

    invoke-static {p0, v1, v0}, LX/ArS;->a(LX/ArS;IS)V

    .line 1719176
    iput-boolean v2, p0, LX/ArS;->f:Z

    goto :goto_0

    :cond_2
    iget-wide v7, v5, LX/ArG;->d:D

    iget-wide v9, v5, LX/ArG;->e:D

    div-double/2addr v7, v9

    const-wide v9, 0x408f400000000000L    # 1000.0

    mul-double/2addr v7, v9

    goto :goto_1
.end method

.method public final a(LX/ArQ;LX/ArP;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const v1, 0xb6000a

    .line 1719139
    iget-object v0, p0, LX/ArS;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1719140
    iget-boolean v0, p0, LX/ArS;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Didn\'t end previous page transition marker with #markPageTransitionEnd()"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1719141
    iput-boolean v2, p0, LX/ArS;->f:Z

    .line 1719142
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1719143
    iget-object v0, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1719144
    :goto_1
    return-void

    .line 1719145
    :cond_0
    iget-object v0, p0, LX/ArS;->c:LX/ArG;

    const-wide/16 v3, 0x0

    .line 1719146
    iput-wide v3, v0, LX/ArG;->d:D

    .line 1719147
    iput-wide v3, v0, LX/ArG;->e:D

    .line 1719148
    iget-object v3, v0, LX/ArG;->a:LX/19D;

    invoke-virtual {v3}, LX/19D;->a()V

    .line 1719149
    invoke-static {p0, v1}, LX/ArS;->d(LX/ArS;I)V

    .line 1719150
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1719151
    const-string v4, "from_page"

    invoke-virtual {p1}, LX/ArQ;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719152
    const-string v4, "from_index"

    iget v5, p1, LX/ArQ;->index:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719153
    const-string v4, "transition_trigger_method"

    invoke-virtual {p2}, LX/ArP;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719154
    const-string v4, "time_since_startup"

    iget-object v5, p0, LX/ArS;->e:LX/199;

    invoke-virtual {v5}, LX/199;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719155
    const-string v4, "startup_type"

    iget-object v5, p0, LX/ArS;->b:LX/0Yi;

    .line 1719156
    iget-wide v7, v5, LX/0Yi;->E:J

    iget-wide v9, v5, LX/0Yi;->D:J

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    sget-object v7, LX/5Ol;->WARM:LX/5Ol;

    :goto_2
    move-object v5, v7

    .line 1719157
    invoke-virtual {v5}, LX/5Ol;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719158
    move-object v0, v3

    .line 1719159
    invoke-static {p0, v1, v0}, LX/ArS;->a(LX/ArS;ILjava/util/Map;)V

    .line 1719160
    iput-boolean v2, p0, LX/ArS;->f:Z

    goto :goto_1

    .line 1719161
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    sget-object v7, LX/5Ol;->COLD:LX/5Ol;

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)V
    .locals 5

    .prologue
    const v1, 0xb60006

    .line 1719127
    invoke-static {p0, v1}, LX/ArS;->d(LX/ArS;I)V

    .line 1719128
    const/4 v2, 0x0

    .line 1719129
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1719130
    sget-object v0, LX/ArO;->SESSION_ID:LX/ArO;

    invoke-virtual {v0}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719131
    invoke-static {v3, p2}, LX/ArS;->a(Ljava/util/Map;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V

    .line 1719132
    sget-object v0, LX/ArO;->EFFECT_TRAY:LX/ArO;

    invoke-virtual {v0}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->isFromTray()Z

    move-result v4

    invoke-static {v4}, LX/ArS;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719133
    sget-object v0, LX/ArO;->FIRST_EFFECT:LX/ArO;

    invoke-virtual {v0}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->hasChangedInspiration()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/ArS;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719134
    sget-object v0, LX/ArO;->HAS_CACHE:LX/ArO;

    invoke-virtual {v0}, LX/ArO;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, LX/ArS;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719135
    move-object v0, v3

    .line 1719136
    invoke-static {p0, v1, v0}, LX/ArS;->a(LX/ArS;ILjava/util/Map;)V

    .line 1719137
    return-void

    :cond_0
    move v0, v2

    .line 1719138
    goto :goto_0
.end method

.method public final a(SLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1719123
    invoke-static {p2}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1719124
    const v0, 0xb60007

    invoke-static {p0, v0, p1}, LX/ArS;->a(LX/ArS;IS)V

    .line 1719125
    :goto_0
    return-void

    .line 1719126
    :cond_0
    const v0, 0xb60008

    invoke-static {p0, v0, p1}, LX/ArS;->a(LX/ArS;IS)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1719120
    const v0, 0xb60006

    .line 1719121
    iget-object v1, p0, LX/ArS;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1719122
    return-void
.end method
