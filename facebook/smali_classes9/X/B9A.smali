.class public final LX/B9A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final synthetic b:Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 1751170
    iput-object p1, p0, LX/B9A;->b:Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;

    iput-object p2, p0, LX/B9A;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1751171
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751172
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 1751173
    if-eqz p1, :cond_0

    .line 1751174
    iget-object v0, p0, LX/B9A;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1751175
    iget-object v0, p0, LX/B9A;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, LX/1af;->a(I)V

    .line 1751176
    :cond_0
    return-void
.end method
