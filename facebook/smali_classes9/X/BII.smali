.class public LX/BII;
.super LX/BIH;
.source ""


# instance fields
.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1770192
    invoke-direct {p0, p1}, LX/BIH;-><init>(Landroid/content/Context;)V

    .line 1770193
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 1770194
    iget-object v0, p0, LX/BII;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1770195
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, LX/BII;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1770196
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1770197
    invoke-virtual {p0}, LX/BII;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0307ad

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BII;->d:Landroid/view/View;

    .line 1770198
    iget-object v0, p0, LX/BII;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/BII;->addView(Landroid/view/View;)V

    .line 1770199
    :cond_0
    iget-object v0, p0, LX/BII;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1770200
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 0

    .prologue
    .line 1770201
    invoke-direct {p0}, LX/BII;->i()V

    .line 1770202
    invoke-super {p0}, LX/BIH;->b()V

    .line 1770203
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1770204
    invoke-direct {p0}, LX/BII;->i()V

    .line 1770205
    invoke-super {p0}, LX/BIH;->e()V

    .line 1770206
    return-void
.end method

.method public getItemType()LX/BHH;
    .locals 1

    .prologue
    .line 1770207
    sget-object v0, LX/BHH;->GIF:LX/BHH;

    return-object v0
.end method
