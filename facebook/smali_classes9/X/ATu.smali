.class public LX/ATu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/drawable/AnimationDrawable;

.field private b:Landroid/widget/Button;


# direct methods
.method public constructor <init>(LX/0Px;Landroid/widget/Button;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/widget/Button;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x190

    const/4 v1, 0x1

    .line 1677331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677332
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1677333
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1677334
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1677335
    iput-object p2, p0, LX/ATu;->b:Landroid/widget/Button;

    .line 1677336
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    .line 1677337
    iget-object v0, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 1677338
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    .line 1677339
    iget-object v0, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->setEnterFadeDuration(I)V

    .line 1677340
    iget-object v0, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->setExitFadeDuration(I)V

    .line 1677341
    :cond_0
    const/4 v2, 0x0

    .line 1677342
    iget-object v1, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    move v3, v2

    .line 1677343
    :goto_1
    const/4 v0, 0x2

    if-ge v3, v0, :cond_2

    move v1, v2

    .line 1677344
    :goto_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1677345
    iget-object v4, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    const/16 p2, 0x4b0

    invoke-virtual {v4, v0, p2}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    .line 1677346
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1677347
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1677348
    :cond_2
    return-void

    .line 1677349
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1677350
    iget-object v0, p0, LX/ATu;->b:Landroid/widget/Button;

    iget-object v1, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1677351
    iget-object v0, p0, LX/ATu;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 1677352
    return-void
.end method
