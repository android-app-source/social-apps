.class public final LX/CMz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:LX/2Mt;


# direct methods
.method public constructor <init>(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 1881211
    iput-object p1, p0, LX/CMz;->b:LX/2Mt;

    iput-object p2, p0, LX/CMz;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1881199
    sget-object v0, LX/2Mt;->a:Ljava/lang/Class;

    const-string v1, "Failed to get response for thread: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/CMz;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1881200
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1881201
    check-cast p1, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;

    .line 1881202
    if-eqz p1, :cond_0

    .line 1881203
    iget-object v0, p0, LX/CMz;->b:LX/2Mt;

    iget-object v1, p0, LX/CMz;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, p1, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;->resolution:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p1, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;->thumbnailResolution:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1881204
    iget-object v4, v0, LX/2Mt;->h:LX/0QI;

    new-instance v5, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    .line 1881205
    sget-object v6, LX/0SF;->a:LX/0SF;

    move-object v6, v6

    .line 1881206
    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x240c8400

    add-long/2addr v8, v6

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v10

    move v6, v2

    move v7, v3

    invoke-direct/range {v5 .. v10}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;-><init>(IIJLjava/lang/String;)V

    invoke-interface {v4, v1, v5}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1881207
    iget-object v4, v0, LX/2Mt;->h:LX/0QI;

    invoke-interface {v4}, LX/0QI;->b()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v4

    .line 1881208
    invoke-static {v4}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    .line 1881209
    iget-object v5, v0, LX/2Mt;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/2N1;->c:LX/0Tn;

    invoke-interface {v5, v6, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 1881210
    :cond_0
    return-void
.end method
