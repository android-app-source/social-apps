.class public final LX/CWY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CSY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CT7;

.field public final synthetic d:LX/CTK;

.field public final synthetic e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;)V
    .locals 0

    .prologue
    .line 1908345
    iput-object p1, p0, LX/CWY;->e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;

    iput-object p2, p0, LX/CWY;->a:LX/CSY;

    iput-object p3, p0, LX/CWY;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CWY;->c:LX/CT7;

    iput-object p5, p0, LX/CWY;->d:LX/CTK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x63a7da0

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1908346
    iget-object v1, p0, LX/CWY;->a:LX/CSY;

    iget-object v2, p0, LX/CWY;->b:Ljava/lang/String;

    iget-object v3, p0, LX/CWY;->e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->isChecked()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908347
    iget-object v1, p0, LX/CWY;->c:LX/CT7;

    iget-object v2, p0, LX/CWY;->d:LX/CTK;

    iget-object v2, v2, LX/CTJ;->o:Ljava/lang/String;

    iget-object v3, p0, LX/CWY;->d:LX/CTK;

    iget-object v3, v3, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v4, p0, LX/CWY;->a:LX/CSY;

    invoke-virtual {v1, v2, v3, v4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908348
    const v1, -0x762e2f53

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
