.class public LX/C1A;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C1B;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C1A",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C1B;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841976
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1841977
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C1A;->b:LX/0Zi;

    .line 1841978
    iput-object p1, p0, LX/C1A;->a:LX/0Ot;

    .line 1841979
    return-void
.end method

.method public static a(LX/0QB;)LX/C1A;
    .locals 4

    .prologue
    .line 1842004
    const-class v1, LX/C1A;

    monitor-enter v1

    .line 1842005
    :try_start_0
    sget-object v0, LX/C1A;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842006
    sput-object v2, LX/C1A;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842007
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842008
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842009
    new-instance v3, LX/C1A;

    const/16 p0, 0x1e88

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C1A;-><init>(LX/0Ot;)V

    .line 1842010
    move-object v0, v3

    .line 1842011
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842012
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842013
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842014
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842015
    const v0, 0x1e716573

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1841998
    check-cast p2, LX/C19;

    .line 1841999
    iget-object v0, p0, LX/C1A;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1842000
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    .line 1842001
    const p0, 0x1e716573

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1842002
    invoke-interface {v0, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x2

    invoke-interface {v0, p0}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f081a4d

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a010e

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1842003
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1841980
    invoke-static {}, LX/1dS;->b()V

    .line 1841981
    iget v0, p1, LX/1dQ;->b:I

    .line 1841982
    packed-switch v0, :pswitch_data_0

    .line 1841983
    :goto_0
    return-object v2

    .line 1841984
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1841985
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1841986
    check-cast v1, LX/C19;

    .line 1841987
    iget-object v3, p0, LX/C1A;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/C19;->c:Ljava/lang/String;

    .line 1841988
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const-class p0, LX/0ew;

    invoke-static {p2, p0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0ew;

    .line 1841989
    if-nez p2, :cond_0

    .line 1841990
    :goto_1
    goto :goto_0

    .line 1841991
    :cond_0
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;-><init>()V

    .line 1841992
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1841993
    const-string v0, "question_id"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841994
    const-string v0, "story_attachment"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1841995
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1841996
    move-object p0, p0

    .line 1841997
    invoke-interface {p2}, LX/0ew;->iC_()LX/0gc;

    move-result-object p2

    const-string v1, "question_add_poll_option_option_tag"

    invoke-virtual {p0, p2, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1e716573
        :pswitch_0
    .end packed-switch
.end method
