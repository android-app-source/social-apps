.class public LX/CeJ;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/2dM;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/CeJ;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1SH;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1924515
    const-class v0, LX/CeJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CeJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1SH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1924516
    invoke-direct {p0, p2}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1924517
    iput-object p1, p0, LX/CeJ;->b:LX/0Ot;

    .line 1924518
    iput-object p3, p0, LX/CeJ;->c:LX/0Ot;

    .line 1924519
    return-void
.end method

.method public static a(LX/0QB;)LX/CeJ;
    .locals 6

    .prologue
    .line 1924520
    sget-object v0, LX/CeJ;->d:LX/CeJ;

    if-nez v0, :cond_1

    .line 1924521
    const-class v1, LX/CeJ;

    monitor-enter v1

    .line 1924522
    :try_start_0
    sget-object v0, LX/CeJ;->d:LX/CeJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1924523
    if-eqz v2, :cond_0

    .line 1924524
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1924525
    new-instance v3, LX/CeJ;

    const/16 v4, 0xf63

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf5a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/CeJ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1924526
    move-object v0, v3

    .line 1924527
    sput-object v0, LX/CeJ;->d:LX/CeJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1924528
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1924529
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1924530
    :cond_1
    sget-object v0, LX/CeJ;->d:LX/CeJ;

    return-object v0

    .line 1924531
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1924532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/CeJ;Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 0

    .prologue
    .line 1924533
    invoke-super {p0, p1, p2, p3}, LX/0aT;->onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x7c57b841

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1924534
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 1924535
    :goto_0
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1924536
    const v0, -0x6d91eebf

    invoke-static {v0, v1}, LX/02F;->e(II)V

    .line 1924537
    :goto_1
    return-void

    .line 1924538
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1924539
    :cond_1
    iget-object v0, p0, LX/CeJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/CeH;

    invoke-direct {v2, p0, p1, p2, p3}, LX/CeH;-><init>(LX/CeJ;Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1924540
    const v0, -0x3ffd4fc6

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_1
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1924541
    check-cast p3, LX/2dM;

    .line 1924542
    new-instance v0, LX/CeI;

    invoke-direct {v0, p0}, LX/CeI;-><init>(LX/CeJ;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p3, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1924543
    return-void
.end method
