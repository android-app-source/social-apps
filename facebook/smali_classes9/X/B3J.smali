.class public final LX/B3J;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:LX/1mR;

.field public final synthetic c:Landroid/app/Activity;

.field public final synthetic d:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

.field public final synthetic e:LX/B3K;


# direct methods
.method public constructor <init>(LX/B3K;Landroid/app/ProgressDialog;LX/1mR;Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V
    .locals 0

    .prologue
    .line 1740104
    iput-object p1, p0, LX/B3J;->e:LX/B3K;

    iput-object p2, p0, LX/B3J;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, LX/B3J;->b:LX/1mR;

    iput-object p4, p0, LX/B3J;->c:Landroid/app/Activity;

    iput-object p5, p0, LX/B3J;->d:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1740105
    iget-object v0, p0, LX/B3J;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1740106
    if-eqz p1, :cond_0

    .line 1740107
    iget-object v0, p0, LX/B3J;->b:LX/1mR;

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ProfileImageRequest"

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1740108
    iget-object v0, p0, LX/B3J;->e:LX/B3K;

    iget-object v0, v0, LX/B3K;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/B3J;->c:Landroid/app/Activity;

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1740109
    :cond_0
    iget-object v0, p0, LX/B3J;->c:Landroid/app/Activity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, LX/B3J;->d:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->g()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    const-class v4, LX/1kW;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1740110
    iget-object v0, p0, LX/B3J;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1740111
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1740112
    iget-object v0, p0, LX/B3J;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1740113
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1740114
    check-cast p1, Landroid/content/Intent;

    invoke-direct {p0, p1}, LX/B3J;->a(Landroid/content/Intent;)V

    return-void
.end method
