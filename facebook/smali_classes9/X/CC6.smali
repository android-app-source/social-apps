.class public LX/CC6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/CC2;

.field public final b:LX/2g9;


# direct methods
.method public constructor <init>(LX/2g9;LX/CC2;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1857172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1857173
    iput-object p1, p0, LX/CC6;->b:LX/2g9;

    .line 1857174
    iput-object p2, p0, LX/CC6;->a:LX/CC2;

    .line 1857175
    return-void
.end method

.method public static a(LX/0QB;)LX/CC6;
    .locals 5

    .prologue
    .line 1857176
    const-class v1, LX/CC6;

    monitor-enter v1

    .line 1857177
    :try_start_0
    sget-object v0, LX/CC6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1857178
    sput-object v2, LX/CC6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1857179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1857181
    new-instance p0, LX/CC6;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static {v0}, LX/CC2;->b(LX/0QB;)LX/CC2;

    move-result-object v4

    check-cast v4, LX/CC2;

    invoke-direct {p0, v3, v4}, LX/CC6;-><init>(LX/2g9;LX/CC2;)V

    .line 1857182
    move-object v0, p0

    .line 1857183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1857184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CC6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1857185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1857186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
