.class public LX/Bs7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/1V8;

.field public final b:LX/1VF;

.field private final c:LX/1JC;

.field public final d:LX/14w;

.field public final e:F

.field private final f:F

.field private final g:F

.field public final h:F

.field public final i:F

.field public final j:F

.field public final k:F


# direct methods
.method public constructor <init>(LX/1V8;LX/1VF;LX/1JC;LX/14w;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827056
    iput-object p1, p0, LX/Bs7;->a:LX/1V8;

    .line 1827057
    iput-object p2, p0, LX/Bs7;->b:LX/1VF;

    .line 1827058
    iput-object p3, p0, LX/Bs7;->c:LX/1JC;

    .line 1827059
    iput-object p4, p0, LX/Bs7;->d:LX/14w;

    .line 1827060
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1827061
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, LX/Bs7;->e:F

    .line 1827062
    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/Bs7;->f:F

    .line 1827063
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1827064
    iput v1, p0, LX/Bs7;->g:F

    .line 1827065
    iput v1, p0, LX/Bs7;->h:F

    .line 1827066
    const v1, 0x7f0b00f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/Bs7;->i:F

    .line 1827067
    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, LX/Bs7;->j:F

    .line 1827068
    const v1, 0x7f0217ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/Bs7;->k:F

    .line 1827069
    return-void
.end method

.method public static a(LX/0QB;)LX/Bs7;
    .locals 9

    .prologue
    .line 1827070
    const-class v1, LX/Bs7;

    monitor-enter v1

    .line 1827071
    :try_start_0
    sget-object v0, LX/Bs7;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827072
    sput-object v2, LX/Bs7;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827073
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827074
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827075
    new-instance v3, LX/Bs7;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V8;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v5

    check-cast v5, LX/1VF;

    invoke-static {v0}, LX/1JC;->a(LX/0QB;)LX/1JC;

    move-result-object v6

    check-cast v6, LX/1JC;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v7

    check-cast v7, LX/14w;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct/range {v3 .. v8}, LX/Bs7;-><init>(LX/1V8;LX/1VF;LX/1JC;LX/14w;Landroid/content/Context;)V

    .line 1827076
    move-object v0, v3

    .line 1827077
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827078
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bs7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827079
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;II)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper;",
            "II)I"
        }
    .end annotation

    .prologue
    .line 1827081
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827082
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827083
    iget-object v1, p0, LX/Bs7;->d:LX/14w;

    invoke-virtual {v1, p1}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    .line 1827084
    :goto_0
    if-eqz v1, :cond_3

    sget-object v1, LX/1Ua;->j:LX/1Ua;

    .line 1827085
    :goto_1
    iget-object v2, p0, LX/Bs7;->a:LX/1V8;

    iget v3, p0, LX/Bs7;->e:F

    invoke-virtual {v2, v1, p1, v3}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v1

    move v2, v1

    .line 1827086
    invoke-static {v0}, LX/1wE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    move-result-object v0

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/Bs7;->f:F

    move v1, v0

    .line 1827087
    :goto_2
    const/4 v0, -0x1

    if-eq p4, v0, :cond_1

    int-to-float v0, p4

    .line 1827088
    :goto_3
    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p3, v2

    int-to-float v2, v2

    sub-float v1, v2, v1

    sub-float v0, v1, v0

    iget v1, p0, LX/Bs7;->g:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 1827089
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    .line 1827090
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827091
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827092
    iget-object v3, p0, LX/Bs7;->b:LX/1VF;

    const/4 p4, 0x0

    invoke-virtual {v3, p1, p1, p2, p4}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;Z)Z

    move-result v3

    .line 1827093
    invoke-static {v0}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    .line 1827094
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    .line 1827095
    iget v0, p0, LX/Bs7;->j:F

    iget v3, p0, LX/Bs7;->i:F

    add-float/2addr v0, v3

    .line 1827096
    :goto_4
    move v0, v0

    .line 1827097
    goto :goto_3

    .line 1827098
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1827099
    :cond_3
    sget-object v1, LX/1Ua;->h:LX/1Ua;

    goto :goto_1

    .line 1827100
    :cond_4
    if-eqz v3, :cond_5

    .line 1827101
    iget v0, p0, LX/Bs7;->j:F

    goto :goto_4

    .line 1827102
    :cond_5
    if-eqz v0, :cond_6

    .line 1827103
    iget v0, p0, LX/Bs7;->k:F

    iget v3, p0, LX/Bs7;->h:F

    add-float/2addr v0, v3

    goto :goto_4

    .line 1827104
    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method
