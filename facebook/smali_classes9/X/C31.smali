.class public LX/C31;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/C32;


# direct methods
.method public constructor <init>(LX/C32;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845012
    iput-object p1, p0, LX/C31;->a:LX/C32;

    .line 1845013
    return-void
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/C34;",
            ")",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 1845014
    sget-object v0, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-ne p1, v0, :cond_0

    .line 1845015
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->r:LX/1Ua;

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, p0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 1845016
    :goto_0
    return-object v0

    .line 1845017
    :cond_0
    sget-object v0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-ne p1, v0, :cond_1

    .line 1845018
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->r:LX/1Ua;

    sget-object v2, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, p0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    goto :goto_0

    .line 1845019
    :cond_1
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->j:LX/1Ua;

    sget-object v2, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, p0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/C31;
    .locals 2

    .prologue
    .line 1845020
    new-instance v1, LX/C31;

    .line 1845021
    new-instance v0, LX/C32;

    invoke-direct {v0}, LX/C32;-><init>()V

    .line 1845022
    move-object v0, v0

    .line 1845023
    move-object v0, v0

    .line 1845024
    check-cast v0, LX/C32;

    invoke-direct {v1, v0}, LX/C31;-><init>(LX/C32;)V

    .line 1845025
    return-object v1
.end method


# virtual methods
.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/C34;",
            ")",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 1845026
    sget-object v0, LX/C30;->a:[I

    invoke-virtual {p2}, LX/C34;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1845027
    new-instance v0, LX/1X6;

    iget-object v1, p0, LX/C31;->a:LX/C32;

    .line 1845028
    iget-object v2, v1, LX/C32;->b:LX/1Ua;

    if-nez v2, :cond_0

    .line 1845029
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    const/high16 p0, -0x3f400000    # -6.0f

    .line 1845030
    iput p0, v2, LX/1UY;->c:F

    .line 1845031
    move-object v2, v2

    .line 1845032
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    iput-object v2, v1, LX/C32;->b:LX/1Ua;

    .line 1845033
    :cond_0
    iget-object v2, v1, LX/C32;->b:LX/1Ua;

    move-object v1, v2

    .line 1845034
    sget-object v2, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v0, p1, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    :goto_0
    return-object v0

    .line 1845035
    :pswitch_0
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->a:LX/1Ua;

    sget-object v2, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    invoke-direct {v0, p1, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    goto :goto_0

    .line 1845036
    :pswitch_1
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->a:LX/1Ua;

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, p1, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
