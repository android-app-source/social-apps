.class public final LX/CEx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/CEy;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:LX/1Cn;


# direct methods
.method public constructor <init>(LX/1Cn;LX/CEy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1862310
    iput-object p1, p0, LX/CEx;->h:LX/1Cn;

    iput-object p2, p0, LX/CEx;->a:LX/CEy;

    iput-object p3, p0, LX/CEx;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CEx;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CEx;->d:Ljava/lang/String;

    iput-object p6, p0, LX/CEx;->e:Ljava/lang/String;

    iput-object p7, p0, LX/CEx;->f:Ljava/lang/String;

    iput-object p8, p0, LX/CEx;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x2c3d7a06

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1862311
    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/7m7;->SUCCESS:LX/7m7;

    invoke-virtual {v1}, LX/7m7;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_result"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CEx;->a:LX/CEy;

    const-string v2, "extra_request_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/CEy;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1862312
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_POST:LX/3lw;

    iget-object v2, v2, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "goodwill"

    .line 1862313
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1862314
    move-object v1, v1

    .line 1862315
    const-string v2, "campaign_id"

    iget-object v3, p0, LX/CEx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    iget-object v3, p0, LX/CEx;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    iget-object v3, p0, LX/CEx;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_type"

    iget-object v3, p0, LX/CEx;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "share_source"

    iget-object v3, p0, LX/CEx;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "render_style"

    iget-object v3, p0, LX/CEx;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1862316
    iget-object v2, p0, LX/CEx;->h:LX/1Cn;

    iget-object v2, v2, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1862317
    const/16 v1, 0x27

    const v2, 0x6dba3e43

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1862318
    :goto_0
    return-void

    :cond_0
    const v1, -0x4f8a876f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
