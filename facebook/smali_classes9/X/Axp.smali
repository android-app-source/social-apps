.class public final LX/Axp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Axt;


# direct methods
.method public constructor <init>(LX/Axt;)V
    .locals 0

    .prologue
    .line 1728870
    iput-object p1, p0, LX/Axp;->a:LX/Axt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1728871
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1728872
    check-cast p1, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;

    .line 1728873
    iget-object v0, p0, LX/Axp;->a:LX/Axt;

    iget-object v0, v0, LX/Axt;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AxX;

    .line 1728874
    iget-object v1, v0, LX/AxX;->a:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;

    invoke-virtual {v1, p1}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;->setServerFeatures(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;)V

    .line 1728875
    iget-object v0, p0, LX/Axp;->a:LX/Axt;

    iget-object v1, p0, LX/Axp;->a:LX/Axt;

    iget-object v1, v1, LX/Axt;->q:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1728876
    iput-wide v2, v0, LX/Axt;->x:J

    .line 1728877
    return-void
.end method
