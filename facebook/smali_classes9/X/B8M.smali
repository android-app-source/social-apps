.class public final LX/B8M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/B8R;


# direct methods
.method public constructor <init>(LX/B8R;)V
    .locals 0

    .prologue
    .line 1748747
    iput-object p1, p0, LX/B8M;->a:LX/B8R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 1748748
    if-eqz p2, :cond_0

    .line 1748749
    iget-object v0, p0, LX/B8M;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8M;->a:LX/B8R;

    iget-object v1, v1, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 1748750
    iget-object v0, p0, LX/B8M;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->c:LX/B7W;

    new-instance v1, LX/B7Z;

    const/4 v2, 0x0

    iget-object v3, p0, LX/B8M;->a:LX/B8R;

    iget-object v3, v3, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-direct {v1, v2, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1748751
    iget-object v0, p0, LX/B8M;->a:LX/B8R;

    invoke-static {v0}, LX/B8R;->f(LX/B8R;)V

    .line 1748752
    iget-object v0, p0, LX/B8M;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748753
    :cond_0
    return-void
.end method
