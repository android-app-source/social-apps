.class public LX/BaD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/1EO;

.field private static j:LX/0Xm;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/17Q;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/23R;

.field private final f:LX/9hF;

.field private final g:LX/9i4;

.field private final h:LX/17S;

.field private final i:LX/26J;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1798751
    sget-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    sput-object v0, LX/BaD;->a:LX/1EO;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/17Q;LX/0Or;LX/23R;LX/9hF;LX/9i4;LX/17S;LX/26J;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/23R;",
            "LX/9hF;",
            "LX/9i4;",
            "LX/17S;",
            "LX/26J;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798753
    iput-object p1, p0, LX/BaD;->b:LX/0Zb;

    .line 1798754
    iput-object p2, p0, LX/BaD;->c:LX/17Q;

    .line 1798755
    iput-object p4, p0, LX/BaD;->e:LX/23R;

    .line 1798756
    iput-object p5, p0, LX/BaD;->f:LX/9hF;

    .line 1798757
    iput-object p6, p0, LX/BaD;->g:LX/9i4;

    .line 1798758
    iput-object p7, p0, LX/BaD;->h:LX/17S;

    .line 1798759
    iput-object p3, p0, LX/BaD;->d:LX/0Or;

    .line 1798760
    iput-object p8, p0, LX/BaD;->i:LX/26J;

    .line 1798761
    return-void
.end method

.method public static a(Landroid/view/View;)LX/9hP;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1798673
    instance-of v0, p0, LX/24e;

    if-eqz v0, :cond_0

    .line 1798674
    check-cast p0, LX/24e;

    invoke-interface {p0}, LX/24e;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v0

    .line 1798675
    invoke-static {v0}, LX/9hP;->a(Lcom/facebook/drawee/view/DraweeView;)LX/9hP;

    move-result-object v0

    .line 1798676
    :goto_0
    return-object v0

    .line 1798677
    :cond_0
    instance-of v0, p0, Lcom/facebook/drawee/view/DraweeView;

    if-eqz v0, :cond_1

    .line 1798678
    check-cast p0, Lcom/facebook/drawee/view/DraweeView;

    .line 1798679
    invoke-static {p0}, LX/9hP;->a(Lcom/facebook/drawee/view/DraweeView;)LX/9hP;

    move-result-object v0

    goto :goto_0

    .line 1798680
    :cond_1
    instance-of v0, p0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1798681
    check-cast p0, Landroid/widget/ImageView;

    .line 1798682
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1798683
    invoke-static {p0}, LX/9hP;->a(Landroid/widget/ImageView;)LX/9hP;

    move-result-object v0

    goto :goto_0

    .line 1798684
    :cond_2
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_6

    .line 1798685
    check-cast p0, LX/1cn;

    .line 1798686
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1798687
    const/4 v0, 0x0

    iget-object v2, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v2}, LX/0YU;->a()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 1798688
    iget-object v0, p0, LX/1cn;->e:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 1798689
    iget-object v5, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v5

    .line 1798690
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1798691
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1798692
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1798693
    :cond_3
    move-object v0, v3

    .line 1798694
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1798695
    :goto_2
    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 1798696
    goto :goto_2

    .line 1798697
    :cond_5
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-static {p0, v1}, LX/9hP;->a(Landroid/view/View;Landroid/graphics/RectF;)LX/9hP;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 1798698
    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/1aZ;)LX/9hP;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1798773
    invoke-interface {p1}, LX/1aZ;->c()LX/1aY;

    move-result-object v0

    .line 1798774
    instance-of v1, v0, LX/1oH;

    if-eqz v1, :cond_0

    .line 1798775
    check-cast v0, LX/1oH;

    .line 1798776
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    move-object v0, v1

    .line 1798777
    :cond_0
    instance-of v1, v0, LX/1af;

    if-nez v1, :cond_1

    .line 1798778
    const/4 v0, 0x0

    .line 1798779
    :goto_0
    return-object v0

    .line 1798780
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1798781
    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1798782
    invoke-static {p0, v1}, LX/9hP;->a(Landroid/view/View;Landroid/graphics/RectF;)LX/9hP;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/BaD;
    .locals 12

    .prologue
    .line 1798762
    const-class v1, LX/BaD;

    monitor-enter v1

    .line 1798763
    :try_start_0
    sget-object v0, LX/BaD;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1798764
    sput-object v2, LX/BaD;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1798765
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1798766
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1798767
    new-instance v3, LX/BaD;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v7

    check-cast v7, LX/23R;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v8

    check-cast v8, LX/9hF;

    invoke-static {v0}, LX/9i4;->b(LX/0QB;)LX/9i4;

    move-result-object v9

    check-cast v9, LX/9i4;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v10

    check-cast v10, LX/17S;

    invoke-static {v0}, LX/26J;->a(LX/0QB;)LX/26J;

    move-result-object v11

    check-cast v11, LX/26J;

    invoke-direct/range {v3 .. v11}, LX/BaD;-><init>(LX/0Zb;LX/17Q;LX/0Or;LX/23R;LX/9hF;LX/9i4;LX/17S;LX/26J;)V

    .line 1798768
    move-object v0, v3

    .line 1798769
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1798770
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BaD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1798771
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1798772
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/BaD;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1798743
    iget-object v0, p0, LX/BaD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1798744
    invoke-static {v0}, LX/3Bc;->a(Landroid/content/Context;)V

    .line 1798745
    return-object v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1798746
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1798747
    invoke-static {v1}, LX/14w;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 1798748
    invoke-static {v1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BaD;->h:LX/17S;

    sget-object v3, LX/99r;->Photo:LX/99r;

    invoke-virtual {v1, v3}, LX/17S;->a(LX/99r;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1798749
    const/4 v1, 0x0

    invoke-static {v1, v0, v2}, LX/17S;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v0

    .line 1798750
    :cond_0
    return-object v0
.end method

.method private static a(LX/BaD;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V
    .locals 11
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            "LX/1aZ;",
            "LX/1bf;",
            "ZI",
            "Landroid/content/DialogInterface$OnDismissListener;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/photos/mediagallery/environment/MediaGalleryEnvironment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1798733
    if-nez p2, :cond_1

    move-object v1, p1

    .line 1798734
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1798735
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1798736
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v1, p2

    .line 1798737
    goto :goto_0

    .line 1798738
    :cond_2
    invoke-direct {p0, v1}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    move-result-object v4

    .line 1798739
    invoke-direct {p0, v1, p3}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1798740
    instance-of v0, p3, Lcom/facebook/drawee/view/DraweeView;

    if-nez v0, :cond_3

    if-eqz p4, :cond_3

    invoke-static {p3, p4}, LX/BaD;->a(Landroid/view/View;LX/1aZ;)LX/9hP;

    move-result-object v2

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object/from16 v3, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object v7, p2

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    .line 1798741
    invoke-direct/range {v0 .. v10}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9hP;LX/1bf;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;ZILcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    goto :goto_1

    .line 1798742
    :cond_3
    invoke-static {p3}, LX/BaD;->a(Landroid/view/View;)LX/9hP;

    move-result-object v2

    goto :goto_2
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9hP;LX/1bf;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;ZILcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V
    .locals 7
    .param p2    # LX/9hP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/9hK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/9hP;",
            "LX/1bf;",
            "Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;",
            "ZI",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/content/DialogInterface$OnDismissListener;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/photos/mediagallery/environment/MediaGalleryEnvironment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1798709
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1798710
    if-eqz p7, :cond_3

    invoke-virtual {p7}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 1798711
    :goto_0
    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1798712
    invoke-static {v0}, LX/9hF;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;

    move-result-object v0

    move-object v1, v0

    .line 1798713
    :goto_1
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1798714
    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1798715
    sget-object v3, LX/BaD;->a:LX/1EO;

    invoke-static {v3}, LX/26J;->a(LX/1EO;)LX/74S;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/9hD;->a(Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/9hD;->c(Z)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/9hD;->a(I)LX/9hD;

    .line 1798716
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1798717
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->c(Ljava/lang/String;)LX/9hD;

    .line 1798718
    :cond_0
    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    .line 1798719
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1798720
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    invoke-virtual {v1, v0}, LX/9hD;->c(I)LX/9hD;

    .line 1798721
    :cond_1
    if-eqz v2, :cond_2

    .line 1798722
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->d(Ljava/lang/String;)LX/9hD;

    .line 1798723
    :cond_2
    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 1798724
    new-instance v3, LX/BaC;

    invoke-direct {v3, p0, p2, p3}, LX/BaC;-><init>(LX/BaD;LX/9hP;LX/1bf;)V

    .line 1798725
    iget-object v0, p0, LX/BaD;->e:LX/23R;

    invoke-static {p0}, LX/BaD;->a(LX/BaD;)Landroid/content/Context;

    move-result-object v1

    move-object v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    invoke-interface/range {v0 .. v6}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    .line 1798726
    return-void

    .line 1798727
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1798728
    :cond_4
    if-eqz v1, :cond_5

    .line 1798729
    invoke-virtual {p7}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1798730
    iget-object v2, p0, LX/BaD;->g:LX/9i4;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/9i4;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->a(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    move-object v1, v0

    .line 1798731
    goto/16 :goto_1

    .line 1798732
    :cond_5
    invoke-static {v0}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1798701
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1798702
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1798703
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1798704
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    sget-object v3, LX/BaD;->a:LX/1EO;

    invoke-static {v0, v2, v1, v3}, LX/17Q;->a(Ljava/lang/String;ZLX/0lF;LX/1EO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    move-object v0, v0

    .line 1798705
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1798706
    invoke-static {v0, p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1798707
    :cond_0
    iget-object v1, p0, LX/BaD;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1798708
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V
    .locals 11
    .param p3    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/9hK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            "LX/1aZ;",
            "LX/1bf;",
            "ZI",
            "Landroid/content/DialogInterface$OnDismissListener;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/photos/mediagallery/environment/MediaGalleryEnvironment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1798699
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v0 .. v10}, LX/BaD;->a(LX/BaD;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    .line 1798700
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;)V
    .locals 9
    .param p6    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            "LX/1bf;",
            "ZI",
            "Landroid/content/DialogInterface$OnDismissListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1798671
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;)V

    .line 1798672
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;)V
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/view/View;",
            "LX/1aZ;",
            "LX/1bf;",
            "ZI",
            "Landroid/content/DialogInterface$OnDismissListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1798669
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v0 .. v10}, LX/BaD;->a(LX/BaD;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)V

    .line 1798670
    return-void
.end method
