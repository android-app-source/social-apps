.class public LX/Bxu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bxv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bxu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bxv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836308
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1836309
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bxu;->b:LX/0Zi;

    .line 1836310
    iput-object p1, p0, LX/Bxu;->a:LX/0Ot;

    .line 1836311
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxu;
    .locals 4

    .prologue
    .line 1836340
    const-class v1, LX/Bxu;

    monitor-enter v1

    .line 1836341
    :try_start_0
    sget-object v0, LX/Bxu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836342
    sput-object v2, LX/Bxu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836345
    new-instance v3, LX/Bxu;

    const/16 p0, 0x1e16

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bxu;-><init>(LX/0Ot;)V

    .line 1836346
    move-object v0, v3

    .line 1836347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836339
    const v0, -0x1047677d

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1836331
    check-cast p2, LX/Bxs;

    .line 1836332
    iget-object v0, p0, LX/Bxu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bxv;

    iget-object v1, p2, LX/Bxs;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836333
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    iget-object v3, v0, LX/Bxv;->f:LX/0ad;

    sget-char v4, LX/0wi;->c:C

    iget-object p0, v0, LX/Bxv;->e:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f081a58

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b1169

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b1169

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const/16 v3, 0x8

    const/4 v4, 0x5

    invoke-interface {v2, v3, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f02004f

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    .line 1836334
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1836335
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836336
    const v4, -0x1047677d

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v2, p0, p2

    invoke-static {p1, v4, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v2, v4

    .line 1836337
    invoke-interface {v3, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1836338
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1836312
    invoke-static {}, LX/1dS;->b()V

    .line 1836313
    iget v0, p1, LX/1dQ;->b:I

    .line 1836314
    packed-switch v0, :pswitch_data_0

    .line 1836315
    :goto_0
    return-object v2

    .line 1836316
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836317
    iget-object v3, p0, LX/Bxu;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bxv;

    .line 1836318
    iget-object v4, v3, LX/Bxv;->d:LX/6VI;

    invoke-virtual {v4}, LX/6VI;->b()V

    .line 1836319
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v5

    .line 1836320
    sget-object v6, LX/21D;->NEWSFEED:LX/21D;

    const-string v7, "birthdayComponentEntryInFeedAggregatedStory"

    invoke-static {v6, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    .line 1836321
    new-instance v7, LX/89I;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    sget-object v10, LX/2rw;->USER:LX/2rw;

    invoke-direct {v7, v8, v9, v10}, LX/89I;-><init>(JLX/2rw;)V

    .line 1836322
    iput-object v5, v7, LX/89I;->c:Ljava/lang/String;

    .line 1836323
    move-object v7, v7

    .line 1836324
    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    .line 1836325
    iget-object v7, v3, LX/Bxv;->b:LX/CEF;

    invoke-virtual {v7}, LX/CEF;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, LX/89K;

    invoke-direct {v7}, LX/89K;-><init>()V

    invoke-static {}, LX/CEK;->c()LX/CEK;

    move-result-object v7

    invoke-static {v7}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v7

    :goto_1
    move-object v7, v7

    .line 1836326
    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    .line 1836327
    iget-object v7, v3, LX/Bxv;->a:LX/1Kf;

    iget-object v8, v3, LX/Bxv;->d:LX/6VI;

    .line 1836328
    iget-object v9, v8, LX/6VI;->b:Ljava/lang/String;

    move-object v8, v9

    .line 1836329
    const/16 v9, 0x6dc

    iget-object v10, v3, LX/Bxv;->c:Landroid/app/Activity;

    invoke-interface {v7, v8, v6, v9, v10}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1836330
    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x1047677d
        :pswitch_0
    .end packed-switch
.end method
