.class public final enum LX/Bxn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bxn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bxn;

.field public static final enum LOAD_ON_CLICK:LX/Bxn;

.field public static final enum PAUSE_ON_CLICK:LX/Bxn;

.field public static final enum RESUME_ON_CLICK:LX/Bxn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1835992
    new-instance v0, LX/Bxn;

    const-string v1, "LOAD_ON_CLICK"

    invoke-direct {v0, v1, v2}, LX/Bxn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    new-instance v0, LX/Bxn;

    const-string v1, "PAUSE_ON_CLICK"

    invoke-direct {v0, v1, v3}, LX/Bxn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bxn;->PAUSE_ON_CLICK:LX/Bxn;

    new-instance v0, LX/Bxn;

    const-string v1, "RESUME_ON_CLICK"

    invoke-direct {v0, v1, v4}, LX/Bxn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bxn;->RESUME_ON_CLICK:LX/Bxn;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Bxn;

    sget-object v1, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    aput-object v1, v0, v2

    sget-object v1, LX/Bxn;->PAUSE_ON_CLICK:LX/Bxn;

    aput-object v1, v0, v3

    sget-object v1, LX/Bxn;->RESUME_ON_CLICK:LX/Bxn;

    aput-object v1, v0, v4

    sput-object v0, LX/Bxn;->$VALUES:[LX/Bxn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1835993
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bxn;
    .locals 1

    .prologue
    .line 1835994
    const-class v0, LX/Bxn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bxn;

    return-object v0
.end method

.method public static values()[LX/Bxn;
    .locals 1

    .prologue
    .line 1835995
    sget-object v0, LX/Bxn;->$VALUES:[LX/Bxn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bxn;

    return-object v0
.end method
