.class public final LX/CfD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1925647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x26

    const v1, -0x14afed92

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925648
    if-eqz p2, :cond_0

    const-string v1, "com.facebook.push.registration.ACTION_TOKEN_REQUEST_RETRY"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "serviceType"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1925649
    :cond_0
    sget-object v1, Lcom/facebook/push/registration/RegistrarHelperReceiver;->a:Ljava/lang/Class;

    const-string v2, "Incorrect intent %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925650
    const/16 v1, 0x27

    const v2, 0x10b00f62

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925651
    :goto_0
    return-void

    .line 1925652
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/facebook/push/registration/RegistrarHelperService;

    invoke-direct {v2, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1925653
    const v1, -0x107597e4

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
