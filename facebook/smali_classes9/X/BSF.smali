.class public LX/BSF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1785311
    sget-object v0, LX/BSN;->a:LX/0Tn;

    const-string v1, "info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BSF;->a:LX/0Tn;

    .line 1785312
    sget-object v0, LX/BSN;->a:LX/0Tn;

    const-string v1, "settings"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BSF;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1785313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785314
    iput-object p1, p0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1785315
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1785316
    iget-object v0, p0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BSF;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1785317
    iget-object v1, p0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/BSN;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BSF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/BSF;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
