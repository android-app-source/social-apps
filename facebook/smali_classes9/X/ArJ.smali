.class public final enum LX/ArJ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/5oU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArJ;",
        ">;",
        "LX/5oU;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArJ;

.field public static final enum BACKGROUND_APP:LX/ArJ;

.field public static final enum CAMERA_CAPTURE:LX/ArJ;

.field public static final enum CANCEL_COMPOSER:LX/ArJ;

.field public static final enum CLICK_THUMBNAIL:LX/ArJ;

.field public static final enum DIRECT:LX/ArJ;

.field public static final enum FETCH_AFTER_LOCATION_GRANTED:LX/ArJ;

.field public static final enum FOREGROUND_APP:LX/ArJ;

.field public static final enum GALLERY_SELECT:LX/ArJ;

.field public static final enum NEWSFEED:LX/ArJ;

.field public static final enum NEWSFEED_DIRECT:LX/ArJ;

.field public static final enum NUX_COMPLETE:LX/ArJ;

.field public static final enum POST_PROMPT:LX/ArJ;

.field public static final enum PREVIEW_CONFIRM:LX/ArJ;

.field public static final enum SWIPE_DOWN:LX/ArJ;

.field public static final enum SWIPE_TO_CAMERA:LX/ArJ;

.field public static final enum SWIPE_TO_FEED:LX/ArJ;

.field public static final enum SWIPE_UP:LX/ArJ;

.field public static final enum TAP_BACK_BUTTON:LX/ArJ;

.field public static final enum TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

.field public static final enum TAP_CAMERA_BUTTON_IN_FEED:LX/ArJ;

.field public static final enum TAP_CHEVRON_BUTTON:LX/ArJ;

.field public static final enum TAP_CONFIRM_BUTTON:LX/ArJ;

.field public static final enum TAP_DOODLE_BUTTON:LX/ArJ;

.field public static final enum TAP_EFFECTS_TRAY_BUTTON:LX/ArJ;

.field public static final enum TAP_GALLERY_BUTTON:LX/ArJ;

.field public static final enum TAP_PR_CTA_IN_FEED:LX/ArJ;

.field public static final enum TAP_SCREEN:LX/ArJ;

.field public static final enum TAP_TEXT_BUTTON:LX/ArJ;

.field public static final enum TAP_TO_FEED_BUTTON:LX/ArJ;

.field public static final enum UNKNOWN:LX/ArJ;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1718853
    new-instance v0, LX/ArJ;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->UNKNOWN:LX/ArJ;

    .line 1718854
    new-instance v0, LX/ArJ;

    const-string v1, "SWIPE_DOWN"

    const-string v2, "swipe_down"

    invoke-direct {v0, v1, v5, v2}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->SWIPE_DOWN:LX/ArJ;

    .line 1718855
    new-instance v0, LX/ArJ;

    const-string v1, "SWIPE_UP"

    const-string v2, "swipe_up"

    invoke-direct {v0, v1, v6, v2}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->SWIPE_UP:LX/ArJ;

    .line 1718856
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_CAMERA_BUTTON_IN_FEED"

    const-string v2, "tap_camera_button_in_feed"

    invoke-direct {v0, v1, v7, v2}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_CAMERA_BUTTON_IN_FEED:LX/ArJ;

    .line 1718857
    new-instance v0, LX/ArJ;

    const-string v1, "SWIPE_TO_CAMERA"

    const-string v2, "swipe_to_camera"

    invoke-direct {v0, v1, v8, v2}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->SWIPE_TO_CAMERA:LX/ArJ;

    .line 1718858
    new-instance v0, LX/ArJ;

    const-string v1, "FOREGROUND_APP"

    const/4 v2, 0x5

    const-string v3, "foreground_app"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->FOREGROUND_APP:LX/ArJ;

    .line 1718859
    new-instance v0, LX/ArJ;

    const-string v1, "SWIPE_TO_FEED"

    const/4 v2, 0x6

    const-string v3, "swipe_to_feed"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->SWIPE_TO_FEED:LX/ArJ;

    .line 1718860
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_TO_FEED_BUTTON"

    const/4 v2, 0x7

    const-string v3, "tap_to_feed_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_TO_FEED_BUTTON:LX/ArJ;

    .line 1718861
    new-instance v0, LX/ArJ;

    const-string v1, "BACKGROUND_APP"

    const/16 v2, 0x8

    const-string v3, "background_app"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->BACKGROUND_APP:LX/ArJ;

    .line 1718862
    new-instance v0, LX/ArJ;

    const-string v1, "CLICK_THUMBNAIL"

    const/16 v2, 0x9

    const-string v3, "click_thumbnail"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->CLICK_THUMBNAIL:LX/ArJ;

    .line 1718863
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_EFFECTS_TRAY_BUTTON"

    const/16 v2, 0xa

    const-string v3, "tap_effects_tray_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_EFFECTS_TRAY_BUTTON:LX/ArJ;

    .line 1718864
    new-instance v0, LX/ArJ;

    const-string v1, "CAMERA_CAPTURE"

    const/16 v2, 0xb

    const-string v3, "camera_capture"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    .line 1718865
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_GALLERY_BUTTON"

    const/16 v2, 0xc

    const-string v3, "tap_gallery_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_GALLERY_BUTTON:LX/ArJ;

    .line 1718866
    new-instance v0, LX/ArJ;

    const-string v1, "GALLERY_SELECT"

    const/16 v2, 0xd

    const-string v3, "gallery_select"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    .line 1718867
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_BACK_TO_CAMERA_BUTTON"

    const/16 v2, 0xe

    const-string v3, "tap_back_to_camera_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    .line 1718868
    new-instance v0, LX/ArJ;

    const-string v1, "PREVIEW_CONFIRM"

    const/16 v2, 0xf

    const-string v3, "preview_confirm"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->PREVIEW_CONFIRM:LX/ArJ;

    .line 1718869
    new-instance v0, LX/ArJ;

    const-string v1, "CANCEL_COMPOSER"

    const/16 v2, 0x10

    const-string v3, "cancel_composer"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->CANCEL_COMPOSER:LX/ArJ;

    .line 1718870
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_TEXT_BUTTON"

    const/16 v2, 0x11

    const-string v3, "tap_text_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_TEXT_BUTTON:LX/ArJ;

    .line 1718871
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_DOODLE_BUTTON"

    const/16 v2, 0x12

    const-string v3, "tap_doodle_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_DOODLE_BUTTON:LX/ArJ;

    .line 1718872
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_CHEVRON_BUTTON"

    const/16 v2, 0x13

    const-string v3, "tap_chevron_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_CHEVRON_BUTTON:LX/ArJ;

    .line 1718873
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_BACK_BUTTON"

    const/16 v2, 0x14

    const-string v3, "tap_back_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_BACK_BUTTON:LX/ArJ;

    .line 1718874
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_CONFIRM_BUTTON"

    const/16 v2, 0x15

    const-string v3, "tap_confirm_button"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_CONFIRM_BUTTON:LX/ArJ;

    .line 1718875
    new-instance v0, LX/ArJ;

    const-string v1, "POST_PROMPT"

    const/16 v2, 0x16

    const-string v3, "post_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->POST_PROMPT:LX/ArJ;

    .line 1718876
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_SCREEN"

    const/16 v2, 0x17

    const-string v3, "tap_screen"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_SCREEN:LX/ArJ;

    .line 1718877
    new-instance v0, LX/ArJ;

    const-string v1, "NEWSFEED"

    const/16 v2, 0x18

    const-string v3, "newsfeed"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->NEWSFEED:LX/ArJ;

    .line 1718878
    new-instance v0, LX/ArJ;

    const-string v1, "DIRECT"

    const/16 v2, 0x19

    const-string v3, "direct"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->DIRECT:LX/ArJ;

    .line 1718879
    new-instance v0, LX/ArJ;

    const-string v1, "NEWSFEED_DIRECT"

    const/16 v2, 0x1a

    const-string v3, "newsfeed_direct"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->NEWSFEED_DIRECT:LX/ArJ;

    .line 1718880
    new-instance v0, LX/ArJ;

    const-string v1, "NUX_COMPLETE"

    const/16 v2, 0x1b

    const-string v3, "nux_complete"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->NUX_COMPLETE:LX/ArJ;

    .line 1718881
    new-instance v0, LX/ArJ;

    const-string v1, "TAP_PR_CTA_IN_FEED"

    const/16 v2, 0x1c

    const-string v3, "tap_pr_cta_in_feed"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->TAP_PR_CTA_IN_FEED:LX/ArJ;

    .line 1718882
    new-instance v0, LX/ArJ;

    const-string v1, "FETCH_AFTER_LOCATION_GRANTED"

    const/16 v2, 0x1d

    const-string v3, "fetch_after_location_granted"

    invoke-direct {v0, v1, v2, v3}, LX/ArJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArJ;->FETCH_AFTER_LOCATION_GRANTED:LX/ArJ;

    .line 1718883
    const/16 v0, 0x1e

    new-array v0, v0, [LX/ArJ;

    sget-object v1, LX/ArJ;->UNKNOWN:LX/ArJ;

    aput-object v1, v0, v4

    sget-object v1, LX/ArJ;->SWIPE_DOWN:LX/ArJ;

    aput-object v1, v0, v5

    sget-object v1, LX/ArJ;->SWIPE_UP:LX/ArJ;

    aput-object v1, v0, v6

    sget-object v1, LX/ArJ;->TAP_CAMERA_BUTTON_IN_FEED:LX/ArJ;

    aput-object v1, v0, v7

    sget-object v1, LX/ArJ;->SWIPE_TO_CAMERA:LX/ArJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ArJ;->FOREGROUND_APP:LX/ArJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ArJ;->SWIPE_TO_FEED:LX/ArJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ArJ;->TAP_TO_FEED_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ArJ;->BACKGROUND_APP:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ArJ;->CLICK_THUMBNAIL:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/ArJ;->TAP_EFFECTS_TRAY_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/ArJ;->TAP_GALLERY_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/ArJ;->TAP_BACK_TO_CAMERA_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/ArJ;->PREVIEW_CONFIRM:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/ArJ;->CANCEL_COMPOSER:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/ArJ;->TAP_TEXT_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/ArJ;->TAP_DOODLE_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/ArJ;->TAP_CHEVRON_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/ArJ;->TAP_BACK_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/ArJ;->TAP_CONFIRM_BUTTON:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/ArJ;->POST_PROMPT:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/ArJ;->TAP_SCREEN:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/ArJ;->NEWSFEED:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/ArJ;->DIRECT:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/ArJ;->NEWSFEED_DIRECT:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/ArJ;->NUX_COMPLETE:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/ArJ;->TAP_PR_CTA_IN_FEED:LX/ArJ;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/ArJ;->FETCH_AFTER_LOCATION_GRANTED:LX/ArJ;

    aput-object v2, v0, v1

    sput-object v0, LX/ArJ;->$VALUES:[LX/ArJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1718884
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1718885
    iput-object p3, p0, LX/ArJ;->mName:Ljava/lang/String;

    .line 1718886
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArJ;
    .locals 1

    .prologue
    .line 1718887
    const-class v0, LX/ArJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArJ;

    return-object v0
.end method

.method public static values()[LX/ArJ;
    .locals 1

    .prologue
    .line 1718888
    sget-object v0, LX/ArJ;->$VALUES:[LX/ArJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArJ;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718889
    iget-object v0, p0, LX/ArJ;->mName:Ljava/lang/String;

    return-object v0
.end method
