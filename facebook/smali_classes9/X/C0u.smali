.class public final LX/C0u;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/36c;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public b:LX/1Pm;

.field public c:Landroid/view/View$OnClickListener;

.field public final synthetic d:LX/36c;


# direct methods
.method public constructor <init>(LX/36c;)V
    .locals 1

    .prologue
    .line 1841601
    iput-object p1, p0, LX/C0u;->d:LX/36c;

    .line 1841602
    move-object v0, p1

    .line 1841603
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1841604
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1841605
    const-string v0, "PhotoCallToActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1841606
    if-ne p0, p1, :cond_1

    .line 1841607
    :cond_0
    :goto_0
    return v0

    .line 1841608
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1841609
    goto :goto_0

    .line 1841610
    :cond_3
    check-cast p1, LX/C0u;

    .line 1841611
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1841612
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1841613
    if-eq v2, v3, :cond_0

    .line 1841614
    iget-object v2, p0, LX/C0u;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0u;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v3, p1, LX/C0u;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1841615
    goto :goto_0

    .line 1841616
    :cond_5
    iget-object v2, p1, LX/C0u;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v2, :cond_4

    .line 1841617
    :cond_6
    iget-object v2, p0, LX/C0u;->b:LX/1Pm;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C0u;->b:LX/1Pm;

    iget-object v3, p1, LX/C0u;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1841618
    goto :goto_0

    .line 1841619
    :cond_8
    iget-object v2, p1, LX/C0u;->b:LX/1Pm;

    if-nez v2, :cond_7

    .line 1841620
    :cond_9
    iget-object v2, p0, LX/C0u;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/C0u;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/C0u;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1841621
    goto :goto_0

    .line 1841622
    :cond_a
    iget-object v2, p1, LX/C0u;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
