.class public final LX/AY5;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Z

.field public final synthetic c:Landroid/animation/Animator$AnimatorListener;

.field public final synthetic d:LX/AYE;


# direct methods
.method public constructor <init>(LX/AYE;IZLandroid/animation/Animator$AnimatorListener;)V
    .locals 0

    .prologue
    .line 1684771
    iput-object p1, p0, LX/AY5;->d:LX/AYE;

    iput p2, p0, LX/AY5;->a:I

    iput-boolean p3, p0, LX/AY5;->b:Z

    iput-object p4, p0, LX/AY5;->c:Landroid/animation/Animator$AnimatorListener;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 1684772
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 1684773
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AWT;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 1684774
    :cond_0
    :goto_0
    return-void

    .line 1684775
    :cond_1
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget v1, p0, LX/AY5;->a:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setTranslationY(F)V

    .line 1684776
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->f:LX/Aby;

    iget-boolean v1, p0, LX/AY5;->b:Z

    const/4 v3, 0x0

    .line 1684777
    iput-boolean v1, v0, LX/Aby;->G:Z

    .line 1684778
    iget-object v2, v0, LX/Aby;->a:LX/AcX;

    .line 1684779
    iput-boolean v1, v2, LX/AcX;->A:Z

    .line 1684780
    if-eqz v1, :cond_2

    iget-object v4, v2, LX/AcX;->n:LX/AeS;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    if-lez v4, :cond_2

    .line 1684781
    iget-object v4, v2, LX/AVi;->a:Landroid/view/View;

    move-object v4, v4

    .line 1684782
    check-cast v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    .line 1684783
    invoke-virtual {v4}, LX/9Bh;->b()V

    .line 1684784
    iget-object v4, v2, LX/AVi;->a:Landroid/view/View;

    move-object v4, v4

    .line 1684785
    check-cast v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object p1, v2, LX/AcX;->n:LX/AeS;

    invoke-virtual {p1}, LX/1OM;->ij_()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v4, p1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 1684786
    :cond_2
    iget-object v4, v2, LX/AVi;->a:Landroid/view/View;

    move-object v4, v4

    .line 1684787
    check-cast v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v4, v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setMinimized(Z)V

    .line 1684788
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684789
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v4, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    if-nez v1, :cond_4

    const/4 v2, 0x1

    .line 1684790
    :goto_1
    iput-boolean v2, v4, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 1684791
    if-eqz v1, :cond_5

    .line 1684792
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684793
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    iput v2, v0, LX/Aby;->D:I

    .line 1684794
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684795
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget v4, v0, LX/Aby;->A:I

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1684796
    :goto_2
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684797
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v2, :cond_3

    .line 1684798
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684799
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v2, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setMinimizedMode(Z)V

    .line 1684800
    :cond_3
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/AY5;->c:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1684801
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AYE;->r:LX/AaA;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/AY5;->b:Z

    if-nez v0, :cond_0

    .line 1684802
    iget-object v0, p0, LX/AY5;->d:LX/AYE;

    iget-object v0, v0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0}, LX/AaA;->c()V

    goto/16 :goto_0

    :cond_4
    move v2, v3

    .line 1684803
    goto :goto_1

    .line 1684804
    :cond_5
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1684805
    check-cast v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget v4, v0, LX/Aby;->D:I

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_2
.end method
