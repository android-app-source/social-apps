.class public final LX/CWh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CX3;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/CTS;

.field public final synthetic e:LX/CSY;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/CT7;

.field public final synthetic h:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;LX/CX3;Ljava/util/ArrayList;Ljava/lang/String;LX/CTS;LX/CSY;Ljava/lang/String;LX/CT7;)V
    .locals 0

    .prologue
    .line 1908641
    iput-object p1, p0, LX/CWh;->h:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;

    iput-object p2, p0, LX/CWh;->a:LX/CX3;

    iput-object p3, p0, LX/CWh;->b:Ljava/util/ArrayList;

    iput-object p4, p0, LX/CWh;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CWh;->d:LX/CTS;

    iput-object p6, p0, LX/CWh;->e:LX/CSY;

    iput-object p7, p0, LX/CWh;->f:Ljava/lang/String;

    iput-object p8, p0, LX/CWh;->g:LX/CT7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3264a52c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1908633
    iget-object v1, p0, LX/CWh;->a:LX/CX3;

    invoke-virtual {v1}, LX/CX3;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1908634
    iget-object v1, p0, LX/CWh;->b:Ljava/util/ArrayList;

    iget-object v2, p0, LX/CWh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1908635
    :goto_0
    iget-object v1, p0, LX/CWh;->h:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;

    iget-object v2, p0, LX/CWh;->d:LX/CTS;

    iget v2, v2, LX/CTS;->d:I

    iget-object v3, p0, LX/CWh;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1908636
    invoke-static {v1, v2, v3}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;II)V

    .line 1908637
    iget-object v1, p0, LX/CWh;->e:LX/CSY;

    iget-object v2, p0, LX/CWh;->f:Ljava/lang/String;

    iget-object v3, p0, LX/CWh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908638
    iget-object v1, p0, LX/CWh;->g:LX/CT7;

    iget-object v2, p0, LX/CWh;->d:LX/CTS;

    iget-object v2, v2, LX/CTJ;->o:Ljava/lang/String;

    iget-object v3, p0, LX/CWh;->d:LX/CTS;

    iget-object v3, v3, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v4, p0, LX/CWh;->e:LX/CSY;

    invoke-virtual {v1, v2, v3, v4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908639
    const v1, 0x261f5b9c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1908640
    :cond_0
    iget-object v1, p0, LX/CWh;->b:Ljava/util/ArrayList;

    iget-object v2, p0, LX/CWh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
