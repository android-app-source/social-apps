.class public final LX/CES;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V
    .locals 0

    .prologue
    .line 1861214
    iput-object p1, p0, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 1861236
    iget-object v0, p0, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 1861237
    :goto_0
    return-void

    .line 1861238
    :cond_0
    iget-object v0, p0, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    iget-object v0, p0, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861239
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1861240
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1861241
    new-instance v2, LX/8qL;

    invoke-direct {v2}, LX/8qL;-><init>()V

    .line 1861242
    iput-object v0, v2, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1861243
    move-object v2, v2

    .line 1861244
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    .line 1861245
    iput-object v3, v2, LX/8qL;->d:Ljava/lang/String;

    .line 1861246
    move-object v2, v2

    .line 1861247
    new-instance v3, LX/21A;

    invoke-direct {v3}, LX/21A;-><init>()V

    const-string v4, "native_newsfeed"

    .line 1861248
    iput-object v4, v3, LX/21A;->c:Ljava/lang/String;

    .line 1861249
    move-object v3, v3

    .line 1861250
    invoke-virtual {v3}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v3

    .line 1861251
    iput-object v3, v2, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1861252
    move-object v2, v2

    .line 1861253
    invoke-virtual {v2}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v2

    .line 1861254
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861255
    iget-object v5, v1, LX/Ca4;->g:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    iput-object v5, v1, LX/Ca4;->k:LX/1My;

    .line 1861256
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v11

    .line 1861257
    new-instance v6, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v8, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v9, 0x0

    move-object v7, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 1861258
    iget-object v5, v1, LX/Ca4;->k:LX/1My;

    iget-object v7, v1, LX/Ca4;->h:LX/0TF;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1861259
    iget-object v3, v1, LX/Ca4;->e:LX/1nI;

    iget-object v4, v1, LX/Ca4;->a:Landroid/content/Context;

    invoke-interface {v3, v4, v2}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1861260
    goto :goto_0
.end method

.method public final a(LX/Ca6;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1861215
    iget-object v0, p0, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    const/4 v5, 0x0

    .line 1861216
    iget-boolean v2, p1, LX/Ca6;->j:Z

    move v2, v2

    .line 1861217
    if-ne v2, p2, :cond_0

    .line 1861218
    :goto_0
    return-void

    .line 1861219
    :cond_0
    iget-object v2, p1, LX/Ca6;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v2

    .line 1861220
    if-eqz v2, :cond_1

    .line 1861221
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    .line 1861222
    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-direct {v4, v1, v5, v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1861223
    iget-object v5, v0, LX/Ca4;->i:LX/3id;

    const/4 v6, 0x0

    new-instance v7, LX/Ca2;

    invoke-direct {v7, v0}, LX/Ca2;-><init>(LX/Ca4;)V

    invoke-virtual {v5, v2, v4, v6, v7}, LX/3id;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    .line 1861224
    :goto_1
    iget-object v4, v0, LX/Ca4;->c:LX/0Zb;

    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz p2, :cond_2

    sget-object v2, LX/CZw;->a:Ljava/lang/String;

    :goto_2
    invoke-direct {v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1861225
    iput-object v3, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1861226
    move-object v2, v5

    .line 1861227
    const-string v3, "fbobj"

    .line 1861228
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1861229
    move-object v2, v2

    .line 1861230
    invoke-interface {v4, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1861231
    :cond_1
    iget-wide v8, p1, LX/Ca6;->e:J

    move-wide v2, v8

    .line 1861232
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 1861233
    new-instance v6, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-direct {v6, v1, v5, v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1861234
    iget-object v2, v0, LX/Ca4;->j:LX/961;

    new-instance v7, LX/Ca3;

    invoke-direct {v7, v0, p1, p2}, LX/Ca3;-><init>(LX/Ca4;LX/Ca6;Z)V

    move v4, p2

    invoke-virtual/range {v2 .. v7}, LX/961;->a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1L9;)V

    goto :goto_1

    .line 1861235
    :cond_2
    sget-object v2, LX/CZw;->c:Ljava/lang/String;

    goto :goto_2
.end method
