.class public final LX/CRT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1891823
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1891824
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1891825
    :goto_0
    return v1

    .line 1891826
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1891827
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1891828
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1891829
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1891830
    const-string v7, "cache_radius"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1891831
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1891832
    :cond_1
    const-string v7, "display_region_hint"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1891833
    invoke-static {p0, p1}, LX/CR7;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1891834
    :cond_2
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1891835
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1891836
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_3

    .line 1891837
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_3

    .line 1891838
    invoke-static {p0, p1}, LX/CRS;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1891839
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1891840
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1891841
    goto :goto_1

    .line 1891842
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1891843
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1891844
    if-eqz v0, :cond_6

    .line 1891845
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1891846
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1891847
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1891848
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1891849
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1891850
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1891851
    if-eqz v0, :cond_0

    .line 1891852
    const-string v1, "cache_radius"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1891853
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1891854
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1891855
    if-eqz v0, :cond_1

    .line 1891856
    const-string v1, "display_region_hint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1891857
    invoke-static {p0, v0, p2}, LX/CR7;->a(LX/15i;ILX/0nX;)V

    .line 1891858
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1891859
    if-eqz v0, :cond_3

    .line 1891860
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1891861
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1891862
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_2

    .line 1891863
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/CRS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1891864
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1891865
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1891866
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1891867
    return-void
.end method
