.class public LX/Bwa;
.super LX/3Gn;
.source ""


# instance fields
.field public o:LX/1Sj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/15X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1834019
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bwa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1834020
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1834017
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bwa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834018
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1834014
    invoke-direct {p0, p1, p2, p3}, LX/3Gn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834015
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Bwa;

    invoke-static {v0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v3

    check-cast v3, LX/1Sj;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-static {v0}, LX/15X;->a(LX/0QB;)LX/15X;

    move-result-object p1

    check-cast p1, LX/15X;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p2

    check-cast p2, LX/0wM;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p3

    check-cast p3, LX/0kL;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    iput-object v3, v2, LX/Bwa;->o:LX/1Sj;

    iput-object v4, v2, LX/Bwa;->p:LX/0tQ;

    iput-object p1, v2, LX/Bwa;->q:LX/15X;

    iput-object p2, v2, LX/Bwa;->r:LX/0wM;

    iput-object p3, v2, LX/Bwa;->s:LX/0kL;

    iput-object v0, v2, LX/Bwa;->t:LX/0kb;

    .line 1834016
    return-void
.end method

.method private setButtonImage(I)V
    .locals 3

    .prologue
    .line 1833983
    iget-object v0, p0, LX/3Gn;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Bwa;->r:LX/0wM;

    const/4 v2, -0x1

    invoke-virtual {v1, p1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1833984
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1833998
    invoke-static {p1}, LX/393;->o(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1833999
    if-eqz v0, :cond_0

    iget-object v3, p0, LX/Bwa;->p:LX/0tQ;

    .line 1834000
    iget-object v4, v3, LX/0tQ;->a:LX/0ad;

    sget-short v5, LX/0wh;->S:S

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0ad;->a(SZ)Z

    move-result v4

    move v3, v4

    .line 1834001
    if-eqz v3, :cond_2

    iget-object v3, p0, LX/Bwa;->t:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->v()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1834002
    :cond_0
    iput-boolean v2, p0, LX/Bwa;->f:Z

    .line 1834003
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1834004
    :cond_1
    :goto_0
    return-void

    .line 1834005
    :cond_2
    iput-boolean v1, p0, LX/Bwa;->f:Z

    .line 1834006
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1834007
    iget-object v3, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    new-instance v4, LX/BwZ;

    invoke-direct {v4, p0, v0}, LX/BwZ;-><init>(LX/Bwa;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1834008
    if-eqz p2, :cond_3

    .line 1834009
    iget-object v3, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1834010
    iget-object v3, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1834011
    :cond_3
    invoke-virtual {p0}, LX/3Gn;->j()V

    .line 1834012
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1834013
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, LX/Bwa;->a(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1833993
    invoke-super {p0, p1}, LX/3Gn;->a(Z)V

    .line 1833994
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1833995
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/Bwa;->q:LX/15X;

    invoke-virtual {p0}, LX/Bwa;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/15X;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 1833996
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833997
    return-void

    :cond_0
    iget-object v1, p0, LX/Bwa;->q:LX/15X;

    invoke-virtual {p0}, LX/Bwa;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/15X;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final b(Z)I
    .locals 1

    .prologue
    .line 1833992
    const/4 v0, -0x1

    return v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1833989
    if-eqz p1, :cond_0

    const v0, 0x7f0207d6

    :goto_0
    invoke-direct {p0, v0}, LX/Bwa;->setButtonImage(I)V

    .line 1833990
    return-void

    .line 1833991
    :cond_0
    const v0, 0x7f0207fd

    goto :goto_0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 3

    .prologue
    .line 1833985
    const v0, 0x7f0207fd

    invoke-direct {p0, v0}, LX/Bwa;->setButtonImage(I)V

    .line 1833986
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Bwa;->q:LX/15X;

    invoke-virtual {p0}, LX/Bwa;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/15X;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833987
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/Bwa;->q:LX/15X;

    invoke-virtual {p0}, LX/Bwa;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/15X;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1833988
    return-void
.end method
