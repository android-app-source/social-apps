.class public final LX/Avs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Avr;


# instance fields
.field public final synthetic a:LX/0iJ;


# direct methods
.method public constructor <init>(LX/0iJ;)V
    .locals 0

    .prologue
    .line 1724674
    iput-object p1, p0, LX/Avs;->a:LX/0iJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 1724675
    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    iget-object v1, v0, LX/0iJ;->g:LX/ArL;

    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    iget v0, v0, LX/0iJ;->v:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/ArL;->a(I)V

    .line 1724676
    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    invoke-static {v0, p1}, LX/0iJ;->a$redex0(LX/0iJ;I)V

    .line 1724677
    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    iget-object v0, v0, LX/0iJ;->k:LX/Avn;

    .line 1724678
    iget-object v1, v0, LX/Avn;->c:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1724679
    iget-object v1, v0, LX/Avn;->e:LX/7Cb;

    iget-object v2, v0, LX/Avn;->g:Landroid/net/Uri;

    const/4 p0, 0x3

    invoke-virtual {v1, v2, p0}, LX/7Cb;->a(Landroid/net/Uri;I)V

    .line 1724680
    :cond_0
    return-void

    :cond_1
    move v0, p1

    .line 1724681
    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 1724682
    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    iget-boolean v0, v0, LX/0iJ;->y:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1724683
    iget-object v0, p0, LX/Avs;->a:LX/0iJ;

    .line 1724684
    iget-object v1, v0, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v3, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollFactor(D)V

    .line 1724685
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0iJ;->y:Z

    .line 1724686
    :cond_0
    return-void
.end method
