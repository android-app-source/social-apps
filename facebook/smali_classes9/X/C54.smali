.class public final LX/C54;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/35K;

.field public final synthetic c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/35K;)V
    .locals 0

    .prologue
    .line 1848293
    iput-object p1, p0, LX/C54;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iput-object p2, p0, LX/C54;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/C54;->b:LX/35K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x41a938ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1848294
    iget-object v0, p0, LX/C54;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Landroid/view/View;)LX/AnD;

    move-result-object v2

    .line 1848295
    if-nez v2, :cond_0

    .line 1848296
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find HScrollChainingView."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const v2, -0xe2490c6

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0

    .line 1848297
    :cond_0
    iget-object v3, p0, LX/C54;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    .line 1848298
    iget-object v0, v2, LX/AnD;->h:LX/AnF;

    move-object v4, v0

    .line 1848299
    iget-object v0, p0, LX/C54;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848300
    if-nez v4, :cond_1

    .line 1848301
    :goto_0
    iget-object v0, p0, LX/C54;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iget-object v3, p0, LX/C54;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v4, p0, LX/C54;->b:LX/35K;

    invoke-static {v0, v3, v2, v4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->c(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V

    .line 1848302
    const v0, -0xb9ad1cf

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1848303
    :cond_1
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/AnF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 1848304
    const/4 p1, 0x0

    invoke-static {v5, p1}, LX/17Q;->c(LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1848305
    iget-object p1, v3, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->g:LX/0Zb;

    invoke-interface {p1, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
