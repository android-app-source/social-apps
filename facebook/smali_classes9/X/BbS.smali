.class public final LX/BbS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1PT;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/3BN;


# direct methods
.method public constructor <init>(LX/3BN;LX/1PT;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1800796
    iput-object p1, p0, LX/BbS;->c:LX/3BN;

    iput-object p2, p0, LX/BbS;->a:LX/1PT;

    iput-object p3, p0, LX/BbS;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, 0x6f2238cb

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800797
    iget-object v1, p0, LX/BbS;->a:LX/1PT;

    invoke-static {v1}, LX/9Ir;->b(LX/1PT;)LX/21D;

    move-result-object v1

    iget-object v2, p0, LX/BbS;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1800798
    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    sget-object v2, LX/Baz;->a:LX/89L;

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsPlacelistPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1800799
    iget-object v2, p0, LX/BbS;->c:LX/3BN;

    iget-object v2, v2, LX/3BN;->b:LX/1Kf;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iget-object v4, p0, LX/BbS;->c:LX/3BN;

    iget-object v4, v4, LX/3BN;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v1, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1800800
    const v1, -0x577e23f8

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
