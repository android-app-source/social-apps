.class public final LX/BuB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BuJ;


# direct methods
.method public constructor <init>(LX/BuJ;)V
    .locals 0

    .prologue
    .line 1830534
    iput-object p1, p0, LX/BuB;->a:LX/BuJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v0, 0x4a992955    # 5018794.5f

    invoke-static {v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1830535
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_0

    .line 1830536
    const v0, -0x6f6d4626

    invoke-static {v1, v1, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1830537
    :goto_0
    return-void

    .line 1830538
    :cond_0
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1830539
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1830540
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v4

    .line 1830541
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2os;

    invoke-direct {v1, v2}, LX/2os;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830542
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    invoke-static {v0}, LX/BuJ;->k(LX/BuJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1830543
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    .line 1830544
    iput-boolean v2, v0, LX/BuJ;->G:Z

    .line 1830545
    new-instance v0, LX/BwG;

    iget-object v1, p0, LX/BuB;->a:LX/BuJ;

    iget-object v1, v1, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830546
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1830547
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BuB;->a:LX/BuJ;

    iget-object v2, v2, LX/BuJ;->E:LX/2pa;

    iget-object v3, p0, LX/BuB;->a:LX/BuJ;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->h()I

    move-result v3

    iget-object v5, p0, LX/BuB;->a:LX/BuJ;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->s()LX/04D;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/BwG;-><init>(Ljava/lang/String;LX/2pa;IZLX/04D;)V

    .line 1830548
    iget-object v1, p0, LX/BuB;->a:LX/BuJ;

    iget-object v1, v1, LX/BuJ;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7I8;

    iget-boolean v1, v1, LX/7I8;->c:Z

    if-eqz v1, :cond_1

    .line 1830549
    iget-object v1, p0, LX/BuB;->a:LX/BuJ;

    invoke-static {v1}, LX/BuJ;->getFullScreenVideoPlayer(LX/BuJ;)Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    move-result-object v1

    .line 1830550
    iput-object v1, v0, LX/BwG;->f:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1830551
    :cond_1
    iget-object v1, p0, LX/BuB;->a:LX/BuJ;

    iget-object v2, p0, LX/BuB;->a:LX/BuJ;

    invoke-virtual {v2}, LX/BuJ;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1830552
    iget-object v3, v0, LX/BwG;->a:Ljava/lang/String;

    .line 1830553
    new-instance v4, LX/8qL;

    invoke-direct {v4}, LX/8qL;-><init>()V

    .line 1830554
    iput-object v3, v4, LX/8qL;->d:Ljava/lang/String;

    .line 1830555
    move-object v4, v4

    .line 1830556
    invoke-virtual {v4}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v4

    .line 1830557
    new-instance v5, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    invoke-direct {v5}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;-><init>()V

    .line 1830558
    invoke-virtual {v4}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->v()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1830559
    move-object v5, v5

    .line 1830560
    iput-object v0, v5, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    .line 1830561
    const-class v3, LX/0ew;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ew;

    .line 1830562
    const-class v4, Landroid/app/Activity;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 1830563
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830564
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830565
    new-instance v7, Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;

    invoke-direct {v7}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;-><init>()V

    .line 1830566
    iput-object v5, v7, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->p:LX/8qC;

    .line 1830567
    move-object p0, v7

    .line 1830568
    invoke-interface {v3}, LX/0ew;->iC_()LX/0gc;

    move-result-object v3

    invoke-virtual {v4}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-static {v2}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, v3, v4, p1}, Lcom/facebook/widget/popover/PopoverFragment;->a(LX/0gc;Landroid/view/Window;Landroid/view/View;)V

    .line 1830569
    iput-object v5, v7, Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;->m:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830570
    move-object v0, v5

    .line 1830571
    iput-object v0, v1, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830572
    :goto_1
    const v0, -0x580604ca

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1830573
    :cond_2
    iget-object v0, p0, LX/BuB;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->A:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    goto :goto_1
.end method
