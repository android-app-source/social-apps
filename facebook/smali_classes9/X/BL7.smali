.class public final LX/BL7;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1775274
    iput-object p1, p0, LX/BL7;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    iput-object p2, p0, LX/BL7;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1775275
    if-nez p1, :cond_1

    .line 1775276
    :cond_0
    :goto_0
    return-object v4

    .line 1775277
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1775278
    sget-object v0, LX/0P0;->i:Landroid/net/Uri;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1775279
    iget-object v0, p0, LX/BL7;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/9UW;->a:[Ljava/lang/String;

    const-string v3, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1775280
    :goto_1
    if-eqz v0, :cond_0

    .line 1775281
    new-instance v4, Landroid/widget/Filter$FilterResults;

    invoke-direct {v4}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1775282
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, v4, Landroid/widget/Filter$FilterResults;->count:I

    .line 1775283
    iput-object v0, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto :goto_0

    .line 1775284
    :cond_2
    iget-object v0, p0, LX/BL7;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/0P0;->g:Landroid/net/Uri;

    sget-object v2, LX/9UW;->a:[Ljava/lang/String;

    const-string v3, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1775285
    if-eqz p2, :cond_0

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1775286
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    .line 1775287
    iget-object v1, p0, LX/BL7;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {v1, v0}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1775288
    :cond_0
    iget-object v0, p0, LX/BL7;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    const v1, -0x138e74c1

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1775289
    return-void
.end method
