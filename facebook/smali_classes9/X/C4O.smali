.class public LX/C4O;
.super LX/3lC;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile d:LX/C4O;


# instance fields
.field private final b:LX/20l;

.field private final c:LX/20v;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1847228
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHOOSE_LOVE_REACTION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/C4O;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/20l;LX/20v;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1847224
    invoke-direct {p0}, LX/3lC;-><init>()V

    .line 1847225
    iput-object p1, p0, LX/C4O;->b:LX/20l;

    .line 1847226
    iput-object p2, p0, LX/C4O;->c:LX/20v;

    .line 1847227
    return-void
.end method

.method public static a(LX/0QB;)LX/C4O;
    .locals 5

    .prologue
    .line 1847211
    sget-object v0, LX/C4O;->d:LX/C4O;

    if-nez v0, :cond_1

    .line 1847212
    const-class v1, LX/C4O;

    monitor-enter v1

    .line 1847213
    :try_start_0
    sget-object v0, LX/C4O;->d:LX/C4O;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1847214
    if-eqz v2, :cond_0

    .line 1847215
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1847216
    new-instance p0, LX/C4O;

    invoke-static {v0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v3

    check-cast v3, LX/20l;

    invoke-static {v0}, LX/20v;->a(LX/0QB;)LX/20v;

    move-result-object v4

    check-cast v4, LX/20v;

    invoke-direct {p0, v3, v4}, LX/C4O;-><init>(LX/20l;LX/20v;)V

    .line 1847217
    move-object v0, p0

    .line 1847218
    sput-object v0, LX/C4O;->d:LX/C4O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847219
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1847220
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1847221
    :cond_1
    sget-object v0, LX/C4O;->d:LX/C4O;

    return-object v0

    .line 1847222
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1847223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1847229
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1847208
    iget-object v0, p0, LX/C4O;->b:LX/20l;

    invoke-virtual {v0}, LX/20l;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/C4O;->c:LX/20v;

    const/4 v1, 0x0

    .line 1847209
    invoke-virtual {v0}, LX/20v;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/20v;->c:LX/0ad;

    sget-short p1, LX/98c;->e:S

    invoke-interface {p0, p1, v1}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1847210
    if-eqz v0, :cond_1

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1847199
    new-instance v0, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0723

    const v3, 0x7f030290

    invoke-direct {v0, v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;II)V

    .line 1847200
    const v1, 0x7f0819a3

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 1847201
    const v1, 0x7f0819a4

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1847202
    const/4 v1, -0x1

    .line 1847203
    iput v1, v0, LX/0hs;->t:I

    .line 1847204
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1847205
    invoke-virtual {v0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1847206
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1847207
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1847198
    const-string v0, "4363"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1847197
    sget-object v0, LX/C4O;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
