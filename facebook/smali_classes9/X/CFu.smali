.class public LX/CFu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1864585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(IILX/0Px;LX/0Px;Z)LX/CFt;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;Z)",
            "LX/CFt;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1864586
    if-nez p2, :cond_0

    .line 1864587
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p2, v0

    .line 1864588
    :cond_0
    if-nez p3, :cond_1

    .line 1864589
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p3, v0

    .line 1864590
    :cond_1
    new-instance v0, LX/CFq;

    invoke-direct {v0}, LX/CFq;-><init>()V

    move-object v6, v0

    .line 1864591
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_7

    invoke-virtual {p2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864592
    if-ge p1, p0, :cond_3

    const-string v1, "magic-move-before"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1864593
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move-before"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v0, v1}, LX/CFq;->b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1864594
    :cond_2
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1864595
    :cond_3
    if-le p1, p0, :cond_4

    const-string v1, "magic-move-after"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1864596
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move-after"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v0, v1}, LX/CFq;->b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_1

    .line 1864597
    :cond_4
    const-string v1, "magic-move"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1864598
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v0, v1}, LX/CFq;->b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_1

    .line 1864599
    :cond_5
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-static {p3, v1}, LX/CFu;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1864600
    if-eqz p4, :cond_6

    .line 1864601
    invoke-virtual {v6, v0, v0}, LX/CFq;->b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_1

    .line 1864602
    :cond_6
    sget-object v1, LX/CFt;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v0, v1}, LX/CFq;->b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1864603
    goto :goto_1

    .line 1864604
    :cond_7
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_d

    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864605
    if-ge p0, p1, :cond_8

    const-string v1, "magic-move-before"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1864606
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move-before"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v1, v0}, LX/CFq;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1864607
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1864608
    :cond_8
    if-le p0, p1, :cond_9

    const-string v1, "magic-move-after"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1864609
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move-after"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v1, v0}, LX/CFq;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_3

    .line 1864610
    :cond_9
    const-string v1, "magic-move"

    invoke-static {v0, v1}, LX/CFu;->a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1864611
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    const-string v5, "magic-move"

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v1, v0}, LX/CFq;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_3

    .line 1864612
    :cond_a
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-static {p2, v1}, LX/CFu;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1864613
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-static {p2, v1}, LX/CFu;->b(LX/0Px;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMView;

    move-result-object v1

    .line 1864614
    iget-object v5, v6, LX/CFq;->a:Ljava/util/ArrayList;

    new-instance v7, LX/CFr;

    sget-object v8, LX/CFs;->MOVE:LX/CFs;

    invoke-direct {v7, v8, v1, v0}, LX/CFr;-><init>(LX/CFs;Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1864615
    goto :goto_3

    .line 1864616
    :cond_b
    if-eqz p4, :cond_c

    .line 1864617
    invoke-virtual {v6, v0, v0}, LX/CFq;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    goto :goto_3

    .line 1864618
    :cond_c
    sget-object v1, LX/CFt;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v6, v1, v0}, LX/CFq;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1864619
    goto :goto_3

    .line 1864620
    :cond_d
    invoke-virtual {v6}, LX/CFq;->a()LX/CFt;

    move-result-object v0

    iget-object v7, v0, LX/CFt;->a:LX/0Px;

    .line 1864621
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v2

    move v5, v2

    :goto_4
    if-ge v4, v8, :cond_10

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFr;

    .line 1864622
    iget-object v1, v0, LX/CFr;->a:LX/CFs;

    sget-object v2, LX/CFs;->IN:LX/CFs;

    if-ne v1, v2, :cond_e

    .line 1864623
    const/4 v3, 0x0

    .line 1864624
    add-int/lit8 v1, v5, 0x1

    move v2, v1

    :goto_5
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_11

    .line 1864625
    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CFr;

    iget-object v1, v1, LX/CFr;->a:LX/CFs;

    sget-object v9, LX/CFs;->MOVE:LX/CFs;

    if-ne v1, v9, :cond_f

    .line 1864626
    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CFr;

    .line 1864627
    :goto_6
    if-eqz v1, :cond_e

    .line 1864628
    invoke-virtual {v6, v0}, LX/CFq;->a(LX/CFr;)I

    move-result v2

    .line 1864629
    iget-object v3, v6, LX/CFq;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1864630
    invoke-virtual {v6, v1}, LX/CFq;->a(LX/CFr;)I

    move-result v1

    .line 1864631
    iget-object v2, v6, LX/CFq;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1864632
    :cond_e
    add-int/lit8 v2, v5, 0x1

    .line 1864633
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v2

    goto :goto_4

    .line 1864634
    :cond_f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 1864635
    :cond_10
    invoke-virtual {v6}, LX/CFq;->a()LX/CFt;

    move-result-object v0

    return-object v0

    :cond_11
    move-object v1, v3

    goto :goto_6
.end method

.method private static a(LX/0Px;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1864636
    invoke-static {p0, p1}, LX/CFu;->b(LX/0Px;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/greetingcards/verve/model/VMView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1864637
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0Px;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMView;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/greetingcards/verve/model/VMView;"
        }
    .end annotation

    .prologue
    .line 1864638
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864639
    iget-object v3, v0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    invoke-static {v3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1864640
    :goto_1
    return-object v0

    .line 1864641
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1864642
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
