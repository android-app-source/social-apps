.class public LX/B3R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/B3R;


# instance fields
.field public a:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740300
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1740301
    iput-object v0, p0, LX/B3R;->d:LX/0Ot;

    .line 1740302
    return-void
.end method

.method public static a(LX/0QB;)LX/B3R;
    .locals 7

    .prologue
    .line 1740303
    sget-object v0, LX/B3R;->e:LX/B3R;

    if-nez v0, :cond_1

    .line 1740304
    const-class v1, LX/B3R;

    monitor-enter v1

    .line 1740305
    :try_start_0
    sget-object v0, LX/B3R;->e:LX/B3R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1740306
    if-eqz v2, :cond_0

    .line 1740307
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1740308
    new-instance v6, LX/B3R;

    invoke-direct {v6}, LX/B3R;-><init>()V

    .line 1740309
    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const/16 p0, 0x1032

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1740310
    iput-object v3, v6, LX/B3R;->a:LX/0wM;

    iput-object v4, v6, LX/B3R;->b:LX/23P;

    iput-object v5, v6, LX/B3R;->c:Landroid/content/res/Resources;

    iput-object p0, v6, LX/B3R;->d:LX/0Ot;

    .line 1740311
    move-object v0, v6

    .line 1740312
    sput-object v0, LX/B3R;->e:LX/B3R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740313
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1740314
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1740315
    :cond_1
    sget-object v0, LX/B3R;->e:LX/B3R;

    return-object v0

    .line 1740316
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1740317
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
