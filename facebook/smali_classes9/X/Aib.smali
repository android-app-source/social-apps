.class public final LX/Aib;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Aie;


# direct methods
.method public constructor <init>(LX/Aie;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1706526
    iput-object p1, p0, LX/Aib;->c:LX/Aie;

    iput p2, p0, LX/Aib;->a:I

    iput-object p3, p0, LX/Aib;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1706527
    new-instance v0, LX/Ahe;

    invoke-direct {v0}, LX/Ahe;-><init>()V

    move-object v0, v0

    .line 1706528
    const-string v1, "num_profiles"

    iget v2, p0, LX/Aib;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706529
    invoke-static {v0}, LX/Aie;->b(LX/0gW;)V

    .line 1706530
    iget-object v1, p0, LX/Aib;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1706531
    const-string v1, "after_cursor"

    iget-object v2, p0, LX/Aib;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1706532
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1706533
    iget-object v1, p0, LX/Aib;->c:LX/Aie;

    iget-object v1, v1, LX/Aie;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
