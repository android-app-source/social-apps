.class public final LX/CZi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/permalink/rows/SeenByPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V
    .locals 0

    .prologue
    .line 1916505
    iput-object p1, p0, LX/CZi;->c:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    iput-object p2, p0, LX/CZi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/CZi;->b:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x3df18fd0

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916506
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1916507
    iget-object v2, p0, LX/CZi;->c:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    iget-object v2, v2, Lcom/facebook/permalink/rows/SeenByPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/CZi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v4, p0, LX/CZi;->b:LX/1Po;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Qt;)Landroid/content/Intent;

    move-result-object v2

    .line 1916508
    const-string v3, "fragment_title"

    const v4, 0x7f0810db

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1916509
    iget-object v3, p0, LX/CZi;->c:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    iget-object v3, v3, Lcom/facebook/permalink/rows/SeenByPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1916510
    const v1, 0x374abfb6

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
