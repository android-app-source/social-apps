.class public LX/AsN;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AsM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720417
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1720418
    return-void
.end method


# virtual methods
.method public final a(LX/Arh;Landroid/app/Activity;Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/Ats;Ljava/lang/Object;)LX/AsM;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraEffectsApplier;",
            "Landroid/app/Activity;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            "Lcom/facebook/ipc/friendsharing/inspiration/InspirationViewController$Delegate;",
            "TServices;)",
            "LX/AsM",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1720419
    new-instance v0, LX/AsM;

    move-object/from16 v5, p5

    check-cast v5, LX/0il;

    const-class v1, LX/At7;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/At7;

    const-class v1, LX/At3;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/At3;

    const-class v1, LX/AvX;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/AvX;

    const-class v1, LX/Avk;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Avk;

    const-class v1, LX/AwD;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/AwD;

    const-class v1, LX/Awd;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Awd;

    const-class v1, LX/AwL;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AwL;

    const-class v1, LX/AtQ;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/AtQ;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v0 .. v13}, LX/AsM;-><init>(LX/Arh;Landroid/app/Activity;Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/Ats;LX/0il;LX/At7;LX/At3;LX/AvX;LX/Avk;LX/AwD;LX/Awd;LX/AwL;LX/AtQ;)V

    .line 1720420
    return-object v0
.end method
