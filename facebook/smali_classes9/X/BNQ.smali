.class public LX/BNQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778707
    const-class v0, LX/BNQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BNQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778704
    iput-object p1, p0, LX/BNQ;->b:LX/0Ot;

    .line 1778705
    iput-object p2, p0, LX/BNQ;->c:LX/03V;

    .line 1778706
    return-void
.end method

.method public static a(LX/0QB;)LX/BNQ;
    .locals 5

    .prologue
    .line 1778692
    const-class v1, LX/BNQ;

    monitor-enter v1

    .line 1778693
    :try_start_0
    sget-object v0, LX/BNQ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1778694
    sput-object v2, LX/BNQ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1778695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1778696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1778697
    new-instance v4, LX/BNQ;

    const/16 v3, 0x31d4

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, p0, v3}, LX/BNQ;-><init>(LX/0Ot;LX/03V;)V

    .line 1778698
    move-object v0, v4

    .line 1778699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1778700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BNQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1778702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryActionLink;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1778691
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1778682
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    const v1, -0x452d50ee

    invoke-static {v0, v1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1778683
    invoke-static {v0}, LX/BNQ;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1778684
    iget-object v0, p0, LX/BNQ;->c:LX/03V;

    sget-object v1, LX/BNQ;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No valid information to edit review for story "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1778685
    const/4 v0, 0x0

    .line 1778686
    :goto_0
    return v0

    .line 1778687
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v1

    .line 1778688
    iget-object v0, p0, LX/BNQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNP;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j()I

    move-result v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v11, 0x0

    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v12

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v13

    move/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-virtual/range {v0 .. v13}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    .line 1778689
    const/4 v0, 0x1

    goto :goto_0

    .line 1778690
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v11

    goto :goto_1
.end method
