.class public LX/BaP;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/BaT;

.field private final b:LX/0fD;

.field private final c:LX/7ga;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;


# direct methods
.method public constructor <init>(LX/BaT;LX/0Or;LX/0fD;LX/7ga;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # LX/0fD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BaT;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0fD;",
            "LX/7ga;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798974
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1798975
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1798976
    iput-object v0, p0, LX/BaP;->d:LX/0Px;

    .line 1798977
    iput-object p1, p0, LX/BaP;->a:LX/BaT;

    .line 1798978
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1798979
    invoke-static {}, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->newBuilder()LX/AKK;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object p1

    .line 1798980
    iput-object p1, v1, LX/AKK;->c:Ljava/lang/String;

    .line 1798981
    move-object v1, v1

    .line 1798982
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object p1

    .line 1798983
    iget-object p2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p2, p2

    .line 1798984
    invoke-virtual {p1, p2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object p1

    .line 1798985
    iput-object p1, v1, LX/AKK;->b:Lcom/facebook/audience/model/AudienceControlData;

    .line 1798986
    move-object v1, v1

    .line 1798987
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 1798988
    iput-object p1, v1, LX/AKK;->d:LX/0Px;

    .line 1798989
    move-object v1, v1

    .line 1798990
    invoke-virtual {v1}, LX/AKK;->a()Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    move-result-object v1

    move-object v0, v1

    .line 1798991
    iput-object v0, p0, LX/BaP;->e:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1798992
    iput-object p3, p0, LX/BaP;->b:LX/0fD;

    .line 1798993
    iput-object p4, p0, LX/BaP;->c:LX/7ga;

    .line 1798994
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 9

    .prologue
    .line 1798995
    packed-switch p2, :pswitch_data_0

    .line 1798996
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "view type not recognized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1798997
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030172

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1798998
    iget-object v1, p0, LX/BaP;->a:LX/BaT;

    iget-object v2, p0, LX/BaP;->b:LX/0fD;

    .line 1798999
    new-instance v3, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    const-class v4, Landroid/content/Context;

    invoke-interface {v1, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    .line 1799000
    new-instance v7, LX/BaM;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v7, v4, v5}, LX/BaM;-><init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;)V

    .line 1799001
    move-object v7, v7

    .line 1799002
    check-cast v7, LX/BaM;

    invoke-static {v1}, LX/7ga;->a(LX/0QB;)LX/7ga;

    move-result-object v8

    check-cast v8, LX/7ga;

    move-object v4, v0

    move-object v5, v2

    invoke-direct/range {v3 .. v8}, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;-><init>(Landroid/view/View;LX/0fD;Landroid/content/Context;LX/BaM;LX/7ga;)V

    .line 1799003
    move-object v0, v3

    .line 1799004
    return-object v0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1799005
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1799006
    check-cast p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    .line 1799007
    packed-switch v0, :pswitch_data_0

    .line 1799008
    :goto_0
    return-void

    .line 1799009
    :pswitch_0
    if-nez p2, :cond_0

    .line 1799010
    iget-object v0, p0, LX/BaP;->e:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1799011
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v1

    .line 1799012
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v1

    .line 1799013
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v4, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1799014
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v1, v1

    .line 1799015
    if-eqz v1, :cond_2

    .line 1799016
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v1, v1

    .line 1799017
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 1799018
    :goto_1
    if-eqz v1, :cond_3

    .line 1799019
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082ac1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1799020
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->o:Lcom/facebook/widget/FbImageView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f021858

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1799021
    :goto_2
    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v4, LX/BaS;

    invoke-direct {v4, p1, v1, v0, p2}, LX/BaS;-><init>(Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;ZLcom/facebook/audience/snacks/model/SnacksMyUserThread;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799022
    iget-object v0, p0, LX/BaP;->c:LX/7ga;

    iget-object v1, p0, LX/BaP;->e:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1799023
    iget-object v3, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v3

    .line 1799024
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/7gZ;->SELF:LX/7gZ;

    invoke-virtual {v0, v1, p2, v2, v3}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;)V

    goto :goto_0

    .line 1799025
    :cond_0
    add-int/lit8 v0, p2, -0x1

    move v1, v0

    .line 1799026
    iget-object v0, p0, LX/BaP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799027
    iget-object v3, p0, LX/BaP;->d:LX/0Px;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    const/4 v6, 0x0

    .line 1799028
    iget-object v3, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v3

    .line 1799029
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v3

    .line 1799030
    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v5, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1799031
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1799032
    iget-object v4, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v4, v4

    .line 1799033
    invoke-virtual {v4}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1799034
    iget v3, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    move v3, v3

    .line 1799035
    if-lez v3, :cond_4

    .line 1799036
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1799037
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    sget-object v4, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1799038
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->o:Lcom/facebook/widget/FbImageView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f021858

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1799039
    :goto_3
    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v4, LX/BaR;

    invoke-direct {v4, p1, v1, p2}, LX/BaR;-><init>(Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799040
    iget-object v1, p0, LX/BaP;->c:LX/7ga;

    .line 1799041
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v3

    .line 1799042
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1799043
    iget v4, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    move v0, v4

    .line 1799044
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_4
    sget-object v2, LX/7gZ;->FRIEND:LX/7gZ;

    invoke-virtual {v1, v3, p2, v0, v2}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;)V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_4

    .line 1799045
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1799046
    :cond_3
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082ac2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1799047
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->o:Lcom/facebook/widget/FbImageView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f021857

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1799048
    :cond_4
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00e5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1799049
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    sget-object v4, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4, v6}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1799050
    iget-object v3, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->o:Lcom/facebook/widget/FbImageView;

    iget-object v4, p1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f021857

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1799051
    const/4 v0, 0x3

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1799052
    iget-object v0, p0, LX/BaP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799053
    const/4 v0, 0x1

    .line 1799054
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BaP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
