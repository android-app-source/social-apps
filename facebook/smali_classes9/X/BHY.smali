.class public LX/BHY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/BHY;


# instance fields
.field private final a:LX/0hB;

.field private final b:D

.field private final c:D

.field private final d:D


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769325
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1769326
    const v1, 0x7f0b0bbb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-double v2, v1

    iput-wide v2, p0, LX/BHY;->b:D

    .line 1769327
    const v1, 0x7f0b0bba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-double v2, v1

    iput-wide v2, p0, LX/BHY;->c:D

    .line 1769328
    const v1, 0x7f0b0bb3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, LX/BHY;->d:D

    .line 1769329
    iput-object p2, p0, LX/BHY;->a:LX/0hB;

    .line 1769330
    return-void
.end method

.method public static a(LX/0QB;)LX/BHY;
    .locals 5

    .prologue
    .line 1769331
    sget-object v0, LX/BHY;->e:LX/BHY;

    if-nez v0, :cond_1

    .line 1769332
    const-class v1, LX/BHY;

    monitor-enter v1

    .line 1769333
    :try_start_0
    sget-object v0, LX/BHY;->e:LX/BHY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1769334
    if-eqz v2, :cond_0

    .line 1769335
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1769336
    new-instance p0, LX/BHY;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-direct {p0, v3, v4}, LX/BHY;-><init>(Landroid/content/Context;LX/0hB;)V

    .line 1769337
    move-object v0, p0

    .line 1769338
    sput-object v0, LX/BHY;->e:LX/BHY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1769339
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1769340
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1769341
    :cond_1
    sget-object v0, LX/BHY;->e:LX/BHY;

    return-object v0

    .line 1769342
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1769343
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 8

    .prologue
    .line 1769344
    iget-object v0, p0, LX/BHY;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 1769345
    iget-object v1, p0, LX/BHY;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 1769346
    int-to-double v2, v0

    iget-wide v4, p0, LX/BHY;->c:D

    add-double/2addr v2, v4

    iget-wide v4, p0, LX/BHY;->b:D

    iget-wide v6, p0, LX/BHY;->c:D

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 1769347
    int-to-double v4, v0

    iget-wide v6, p0, LX/BHY;->c:D

    add-double/2addr v4, v6

    int-to-double v6, v2

    div-double/2addr v4, v6

    .line 1769348
    int-to-double v0, v1

    iget-wide v6, p0, LX/BHY;->d:D

    sub-double/2addr v0, v6

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1769349
    mul-int/2addr v0, v2

    return v0
.end method
