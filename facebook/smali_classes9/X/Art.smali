.class public LX/Art;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0wT;


# instance fields
.field public final b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

.field public final c:Landroid/content/Context;

.field public final d:LX/0wW;

.field public final e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1719891
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Art;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Landroid/content/Context;LX/0wW;)V
    .locals 3
    .param p1    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1719881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719882
    iput-object p1, p0, LX/Art;->b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1719883
    iput-object p2, p0, LX/Art;->c:Landroid/content/Context;

    .line 1719884
    iput-object p3, p0, LX/Art;->d:LX/0wW;

    .line 1719885
    new-instance v1, LX/0zw;

    iget-object v0, p0, LX/Art;->b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    const v2, 0x7f0d179d

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Art;->e:LX/0zw;

    .line 1719886
    new-instance v1, LX/0zw;

    iget-object v0, p0, LX/Art;->b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    const v2, 0x7f0d179e

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Art;->f:LX/0zw;

    .line 1719887
    new-instance v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;-><init>(LX/Art;)V

    iput-object v0, p0, LX/Art;->g:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    .line 1719888
    return-void
.end method

.method public static synthetic a(LX/Art;FFF)F
    .locals 1

    .prologue
    .line 1719889
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p3

    mul-float/2addr v0, p1

    mul-float p0, p2, p3

    add-float/2addr v0, p0

    move v0, v0

    .line 1719890
    return v0
.end method
