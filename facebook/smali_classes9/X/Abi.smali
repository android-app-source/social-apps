.class public final LX/Abi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;)V
    .locals 0

    .prologue
    .line 1691254
    iput-object p1, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1691256
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1691257
    iget-object v1, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    iget-object v2, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    iget-object v2, v2, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v2}, Lcom/facebook/facecast/model/FacecastPrivacyData;->d()LX/AWS;

    move-result-object v2

    new-instance v3, LX/8QV;

    invoke-direct {v3}, LX/8QV;-><init>()V

    .line 1691258
    iput-object p1, v3, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1691259
    move-object v3, v3

    .line 1691260
    invoke-virtual {v3, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v3

    invoke-virtual {v3}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v3

    .line 1691261
    iput-object v3, v2, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1691262
    move-object v2, v2

    .line 1691263
    invoke-virtual {v2}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v2

    .line 1691264
    iput-object v2, v1, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691265
    iget-object v1, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-static {v3, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1691266
    invoke-static {v1, v2, v0, v4}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a$redex0(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V

    .line 1691267
    iget-object v0, p0, LX/Abi;->a:Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-virtual {v0, v4}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setClickable(Z)V

    .line 1691268
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1691269
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1691255
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-direct {p0, p1}, LX/Abi;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    return-void
.end method
