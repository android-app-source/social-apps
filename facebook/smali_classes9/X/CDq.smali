.class public final LX/CDq;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3Ac;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1860135
    invoke-static {}, LX/3Ac;->q()LX/3Ac;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860136
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860137
    const-string v0, "FigDivider"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860138
    if-ne p0, p1, :cond_1

    .line 1860139
    :cond_0
    :goto_0
    return v0

    .line 1860140
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860141
    goto :goto_0

    .line 1860142
    :cond_3
    check-cast p1, LX/CDq;

    .line 1860143
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860144
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860145
    if-eq v2, v3, :cond_0

    .line 1860146
    iget v2, p0, LX/CDq;->a:I

    iget v3, p1, LX/CDq;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1860147
    goto :goto_0

    .line 1860148
    :cond_4
    iget v2, p0, LX/CDq;->b:I

    iget v3, p1, LX/CDq;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1860149
    goto :goto_0

    .line 1860150
    :cond_5
    iget v2, p0, LX/CDq;->c:I

    iget v3, p1, LX/CDq;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1860151
    goto :goto_0

    .line 1860152
    :cond_6
    iget v2, p0, LX/CDq;->d:I

    iget v3, p1, LX/CDq;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1860153
    goto :goto_0

    .line 1860154
    :cond_7
    iget v2, p0, LX/CDq;->e:I

    iget v3, p1, LX/CDq;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1860155
    goto :goto_0
.end method
