.class public final LX/CI8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1872254
    const/4 v10, 0x0

    .line 1872255
    const/4 v9, 0x0

    .line 1872256
    const/4 v8, 0x0

    .line 1872257
    const/4 v7, 0x0

    .line 1872258
    const/4 v6, 0x0

    .line 1872259
    const/4 v5, 0x0

    .line 1872260
    const/4 v4, 0x0

    .line 1872261
    const/4 v3, 0x0

    .line 1872262
    const/4 v2, 0x0

    .line 1872263
    const/4 v1, 0x0

    .line 1872264
    const/4 v0, 0x0

    .line 1872265
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1872266
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1872267
    const/4 v0, 0x0

    .line 1872268
    :goto_0
    return v0

    .line 1872269
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1872270
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1872271
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1872272
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1872273
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1872274
    const-string v12, "background_color"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1872275
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1872276
    :cond_2
    const-string v12, "custom_fonts"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1872277
    invoke-static {p0, p1}, LX/CIB;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1872278
    :cond_3
    const-string v12, "document_body_elements"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1872279
    invoke-static {p0, p1}, LX/CI4;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1872280
    :cond_4
    const-string v12, "footer"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1872281
    invoke-static {p0, p1}, LX/CI6;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1872282
    :cond_5
    const-string v12, "footer_elements"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1872283
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1872284
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_6

    .line 1872285
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_6

    .line 1872286
    invoke-static {p0, p1}, LX/CI5;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1872287
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1872288
    :cond_6
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1872289
    goto :goto_1

    .line 1872290
    :cond_7
    const-string v12, "header_elements"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1872291
    invoke-static {p0, p1}, LX/CI7;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1872292
    :cond_8
    const-string v12, "show_see_more_chrome"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1872293
    const/4 v0, 0x1

    .line 1872294
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 1872295
    :cond_9
    const-string v12, "style_list"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1872296
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1872297
    :cond_a
    const-string v12, "third_party_logging_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1872298
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1872299
    :cond_b
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1872300
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1872301
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1872302
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1872303
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1872304
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1872305
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1872306
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1872307
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1872308
    if-eqz v0, :cond_d

    .line 1872309
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1872310
    :cond_d
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1872311
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1872312
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1872313
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x7

    .line 1872314
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1872315
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872316
    if-eqz v0, :cond_0

    .line 1872317
    const-string v1, "background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872318
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872319
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872320
    if-eqz v0, :cond_1

    .line 1872321
    const-string v1, "custom_fonts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872322
    invoke-static {p0, v0, p2, p3}, LX/CIB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872323
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872324
    if-eqz v0, :cond_2

    .line 1872325
    const-string v1, "document_body_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872326
    invoke-static {p0, v0, p2, p3}, LX/CI4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872327
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872328
    if-eqz v0, :cond_3

    .line 1872329
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872330
    invoke-static {p0, v0, p2, p3}, LX/CI6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872331
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872332
    if-eqz v0, :cond_5

    .line 1872333
    const-string v1, "footer_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872334
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1872335
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 1872336
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/CI5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872337
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1872338
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1872339
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872340
    if-eqz v0, :cond_d

    .line 1872341
    const-string v1, "header_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872342
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1872343
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_c

    .line 1872344
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1872345
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1872346
    invoke-virtual {p0, v4, v6}, LX/15i;->g(II)I

    move-result v5

    .line 1872347
    if-eqz v5, :cond_6

    .line 1872348
    const-string v5, "__type__"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872349
    invoke-static {p0, v4, v6, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1872350
    :cond_6
    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1872351
    if-eqz v5, :cond_7

    .line 1872352
    const-string v6, "action"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872353
    invoke-static {p0, v5, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872354
    :cond_7
    invoke-virtual {p0, v4, v7}, LX/15i;->g(II)I

    move-result v5

    .line 1872355
    if-eqz v5, :cond_8

    .line 1872356
    const-string v5, "header_element_type"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872357
    invoke-virtual {p0, v4, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872358
    :cond_8
    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1872359
    if-eqz v5, :cond_9

    .line 1872360
    const-string v6, "logging_token"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872361
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872362
    :cond_9
    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 1872363
    if-eqz v5, :cond_a

    .line 1872364
    const-string v6, "selected"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872365
    invoke-virtual {p2, v5}, LX/0nX;->a(Z)V

    .line 1872366
    :cond_a
    const/4 v5, 0x5

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1872367
    if-eqz v5, :cond_b

    .line 1872368
    const-string v6, "tooltip_title"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872369
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872370
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1872371
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1872372
    :cond_c
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1872373
    :cond_d
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1872374
    if-eqz v0, :cond_e

    .line 1872375
    const-string v1, "show_see_more_chrome"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872376
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1872377
    :cond_e
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1872378
    if-eqz v0, :cond_f

    .line 1872379
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872380
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1872381
    :cond_f
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1872382
    if-eqz v0, :cond_10

    .line 1872383
    const-string v0, "third_party_logging_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872384
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1872385
    :cond_10
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872386
    if-eqz v0, :cond_11

    .line 1872387
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872388
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872389
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1872390
    return-void
.end method
