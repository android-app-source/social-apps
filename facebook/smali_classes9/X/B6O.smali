.class public LX/B6O;
.super LX/0gG;
.source ""


# instance fields
.field private final a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/B7B;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/B7w;

.field private final d:LX/B7F;

.field public e:LX/B6N;

.field public f:LX/2sb;


# direct methods
.method public constructor <init>(LX/B7F;Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;LX/B7w;LX/2sb;)V
    .locals 9
    .param p1    # LX/B7F;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/B7w;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1746566
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1746567
    iput-object p1, p0, LX/B6O;->d:LX/B7F;

    .line 1746568
    iput-object p2, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    .line 1746569
    iput-object p3, p0, LX/B6O;->c:LX/B7w;

    .line 1746570
    iget-object v0, p0, LX/B6O;->d:LX/B7F;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1746571
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1746572
    invoke-virtual {v0}, LX/B7F;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1746573
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1746574
    const/4 v2, 0x0

    .line 1746575
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    .line 1746576
    invoke-static {v1}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenPage;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1746577
    invoke-interface {v4, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :goto_1
    move-object v2, v1

    .line 1746578
    goto :goto_0

    .line 1746579
    :cond_0
    invoke-static {v0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1746580
    new-instance v1, LX/B7C;

    invoke-static {v0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v5

    invoke-virtual {v0}, LX/B7F;->s()LX/B7K;

    move-result-object v6

    invoke-direct {v1, v5, v6}, LX/B7C;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;LX/B7K;)V

    move-object v1, v1

    .line 1746581
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1746582
    iput-boolean v8, v0, LX/B7F;->f:Z

    .line 1746583
    :goto_2
    iget-object v1, v0, LX/B7F;->i:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1746584
    invoke-static {v0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->j()LX/0Px;

    move-result-object v1

    .line 1746585
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1746586
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1746587
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    .line 1746588
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->j()LX/0Px;

    move-result-object v5

    .line 1746589
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;

    .line 1746590
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object p2

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->SECURE_SHARING_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    if-eq p2, p3, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object p2

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->PRIVACY_LINK_TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    if-ne p2, p3, :cond_1

    .line 1746591
    :cond_2
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1746592
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    .line 1746593
    new-instance p1, LX/4X5;

    invoke-direct {p1}, LX/4X5;-><init>()V

    .line 1746594
    invoke-virtual {v5}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1746595
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->a()LX/0Px;

    move-result-object p2

    iput-object p2, p1, LX/4X5;->b:LX/0Px;

    .line 1746596
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->j()LX/0Px;

    move-result-object p2

    iput-object p2, p1, LX/4X5;->c:LX/0Px;

    .line 1746597
    invoke-static {p1, v5}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1746598
    move-object v5, p1

    .line 1746599
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    .line 1746600
    iput-object p1, v5, LX/4X5;->b:LX/0Px;

    .line 1746601
    move-object v5, v5

    .line 1746602
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 1746603
    iput-object v6, v5, LX/4X5;->c:LX/0Px;

    .line 1746604
    move-object v5, v5

    .line 1746605
    new-instance v6, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;-><init>(LX/4X5;)V

    .line 1746606
    move-object v5, v6

    .line 1746607
    move-object v1, v5

    .line 1746608
    invoke-static {v0, v1}, LX/B7F;->b(LX/B7F;Lcom/facebook/graphql/model/GraphQLLeadGenPage;)LX/B7N;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1746609
    :cond_4
    invoke-static {v0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v1

    if-nez v1, :cond_5

    if-eqz v2, :cond_9

    .line 1746610
    :cond_5
    invoke-static {v0, v2}, LX/B7F;->a(LX/B7F;Lcom/facebook/graphql/model/GraphQLLeadGenPage;)LX/B7E;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1746611
    iput-boolean v8, v0, LX/B7F;->e:Z

    .line 1746612
    :cond_6
    :goto_4
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 1746613
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v0}, LX/B7F;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1746614
    iget-object v1, v0, LX/B7F;->i:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1746615
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "scrollable_design"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1746616
    :goto_5
    iget-boolean v1, v0, LX/B7F;->f:Z

    move v1, v1

    .line 1746617
    if-eqz v1, :cond_c

    .line 1746618
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "has_context_card"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1746619
    :goto_6
    iget-boolean v1, v0, LX/B7F;->e:Z

    move v1, v1

    .line 1746620
    if-eqz v1, :cond_d

    .line 1746621
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "has_disclaimer"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1746622
    :goto_7
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1746623
    iput-object v0, p0, LX/B6O;->b:Ljava/util/List;

    .line 1746624
    iput-object p4, p0, LX/B6O;->f:LX/2sb;

    .line 1746625
    return-void

    .line 1746626
    :cond_7
    iput-boolean v7, v0, LX/B7F;->f:Z

    .line 1746627
    goto/16 :goto_2

    .line 1746628
    :cond_8
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenPage;

    .line 1746629
    invoke-static {v0, v1}, LX/B7F;->b(LX/B7F;Lcom/facebook/graphql/model/GraphQLLeadGenPage;)LX/B7N;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1746630
    :cond_9
    iput-boolean v7, v0, LX/B7F;->e:Z

    .line 1746631
    goto :goto_4

    :cond_a
    move-object v1, v2

    goto/16 :goto_1

    .line 1746632
    :cond_b
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "pagination_design"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_5

    .line 1746633
    :cond_c
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "no_context_card"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_6

    .line 1746634
    :cond_d
    iget-object v1, v0, LX/B7F;->h:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v4, "no_disclaimer"

    invoke-virtual {v1, v2, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_7
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1746649
    iget-object v0, p0, LX/B6O;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B7B;

    .line 1746650
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1746651
    instance-of v2, v1, LX/B7C;

    if-eqz v2, :cond_3

    .line 1746652
    iget-object v2, p0, LX/B6O;->f:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1746653
    new-instance v2, LX/B90;

    invoke-direct {v2, v0}, LX/B90;-><init>(Landroid/content/Context;)V

    .line 1746654
    :goto_0
    move-object v0, v2

    .line 1746655
    if-nez v0, :cond_0

    .line 1746656
    const/4 v0, 0x0

    .line 1746657
    :goto_1
    return-object v0

    .line 1746658
    :cond_0
    iget-object v3, p0, LX/B6O;->d:LX/B7F;

    iget-object v5, p0, LX/B6O;->c:LX/B7w;

    move v2, p2

    move v4, p2

    invoke-interface/range {v0 .. v5}, LX/B6N;->a(LX/B7B;ILX/B7F;ILX/B7w;)V

    .line 1746659
    iget-object v1, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    if-eqz v1, :cond_1

    .line 1746660
    iget-object v1, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    invoke-interface {v0, v1}, LX/B6N;->a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V

    :cond_1
    move-object v1, v0

    .line 1746661
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1746662
    :cond_2
    new-instance v2, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;

    invoke-direct {v2, v0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1746663
    :cond_3
    instance-of v2, v1, LX/B7E;

    if-eqz v2, :cond_5

    .line 1746664
    iget-object v2, p0, LX/B6O;->f:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1746665
    new-instance v2, LX/B96;

    invoke-direct {v2, v0}, LX/B96;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1746666
    :cond_4
    new-instance v2, LX/B6g;

    invoke-direct {v2, v0}, LX/B6g;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1746667
    :cond_5
    instance-of v2, v1, LX/B7N;

    if-eqz v2, :cond_7

    .line 1746668
    iget-object v2, p0, LX/B6O;->f:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1746669
    new-instance v2, LX/B9E;

    invoke-direct {v2, v0}, LX/B9E;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1746670
    :cond_6
    new-instance v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;

    invoke-direct {v2, v0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1746671
    :cond_7
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/B7F;Ljava/util/Map;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B7F;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1746640
    iget-object v0, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    iget-object v0, v0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, LX/B6O;->e:LX/B6N;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 1746641
    iget-object v0, p0, LX/B6O;->e:LX/B6N;

    new-instance v1, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    invoke-virtual {p1}, LX/B7F;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p3, p2}, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;-><init>(Ljava/lang/String;ILjava/util/Map;)V

    invoke-interface {v0, v1}, LX/B6N;->a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V

    .line 1746642
    :cond_1
    :goto_0
    return-void

    .line 1746643
    :cond_2
    iget-object v0, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/B6O;->e:LX/B6N;

    if-eqz v0, :cond_1

    .line 1746644
    iget-object v0, p0, LX/B6O;->e:LX/B6N;

    iget-object v1, p0, LX/B6O;->a:Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    invoke-interface {v0, v1}, LX/B6N;->a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1746645
    check-cast p3, LX/B6N;

    .line 1746646
    invoke-interface {p3}, LX/B6N;->a()V

    .line 1746647
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1746648
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1746639
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1746638
    iget-object v0, p0, LX/B6O;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1746635
    move-object v0, p3

    check-cast v0, LX/B6N;

    iput-object v0, p0, LX/B6O;->e:LX/B6N;

    .line 1746636
    invoke-super {p0, p1, p2, p3}, LX/0gG;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1746637
    return-void
.end method
