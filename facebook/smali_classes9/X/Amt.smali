.class public final LX/Amt;
.super LX/8q8;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V
    .locals 0

    .prologue
    .line 1711409
    iput-object p1, p0, LX/Amt;->a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    invoke-direct {p0}, LX/8q8;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;B)V
    .locals 0

    .prologue
    .line 1711408
    invoke-direct {p0, p1}, LX/Amt;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    return-void
.end method

.method private a(LX/8q7;)V
    .locals 2

    .prologue
    .line 1711401
    iget-object v0, p0, LX/Amt;->a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iget-boolean v0, v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Amt;->a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iget-object v0, v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711402
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1711403
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p1, LX/8q7;->a:Ljava/lang/String;

    iget-object v0, p0, LX/Amt;->a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    iget-object v0, v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711404
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1711405
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711406
    iget-object v0, p0, LX/Amt;->a:Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->e(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;Z)V

    .line 1711407
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1711400
    check-cast p1, LX/8q7;

    invoke-direct {p0, p1}, LX/Amt;->a(LX/8q7;)V

    return-void
.end method
