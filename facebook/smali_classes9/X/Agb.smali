.class public final LX/Agb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Agc;


# direct methods
.method public constructor <init>(LX/Agc;)V
    .locals 0

    .prologue
    .line 1701331
    iput-object p1, p0, LX/Agb;->a:LX/Agc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1701332
    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v0, v0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v0, v0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1701333
    :cond_0
    :goto_0
    return-void

    .line 1701334
    :cond_1
    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v1, v0, LX/Agc;->d:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Agc;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_graphFailure"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get vod reactions for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Agb;->a:LX/Agc;

    iget-object v3, v3, LX/Agc;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v0, v0, LX/Agc;->l:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    const-string v0, "no story id"

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701335
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1701336
    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v0, v0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    iget-object v0, v0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1701337
    :cond_0
    :goto_0
    return-void

    .line 1701338
    :cond_1
    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    .line 1701339
    if-eqz p1, :cond_2

    .line 1701340
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1701341
    if-nez v1, :cond_3

    .line 1701342
    :cond_2
    iget-object v0, p0, LX/Agb;->a:LX/Agc;

    const/16 v1, 0x3c

    .line 1701343
    iget v2, v0, LX/Agc;->h:I

    add-int/2addr v2, v1

    iput v2, v0, LX/Agc;->h:I

    .line 1701344
    goto :goto_0

    .line 1701345
    :cond_3
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1701346
    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;

    .line 1701347
    if-eqz v1, :cond_2

    .line 1701348
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;

    move-result-object v1

    .line 1701349
    if-eqz v1, :cond_2

    .line 1701350
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel;->a()LX/0Px;

    move-result-object v3

    .line 1701351
    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1701352
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;

    .line 1701353
    if-eqz v1, :cond_4

    .line 1701354
    iget-object v5, v0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v5, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1701355
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method
