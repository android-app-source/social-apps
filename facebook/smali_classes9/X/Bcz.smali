.class public LX/Bcz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/DiffSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        "TResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/1rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1803100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1803101
    return-void
.end method

.method public static a(LX/0QB;)LX/Bcz;
    .locals 4

    .prologue
    .line 1803102
    const-class v1, LX/Bcz;

    monitor-enter v1

    .line 1803103
    :try_start_0
    sget-object v0, LX/Bcz;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1803104
    sput-object v2, LX/Bcz;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1803105
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1803106
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1803107
    new-instance p0, LX/Bcz;

    invoke-direct {p0}, LX/Bcz;-><init>()V

    .line 1803108
    const-class v3, LX/1rq;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1rq;

    .line 1803109
    iput-object v3, p0, LX/Bcz;->a:LX/1rq;

    .line 1803110
    move-object v0, p0

    .line 1803111
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1803112
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bcz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1803113
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1803114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
