.class public final LX/C9R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/C9Y;


# direct methods
.method public constructor <init>(LX/C9Y;)V
    .locals 0

    .prologue
    .line 1854117
    iput-object p1, p0, LX/C9R;->a:LX/C9Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x929a41e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1854118
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1854119
    sget-object v2, LX/8Lr;->OPTIMISTIC_STORY:LX/8Lr;

    invoke-static {v1, v2}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v3, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1854120
    :try_start_0
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1854121
    :goto_0
    const v1, 0x40ee369c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1854122
    :catch_0
    move-exception v1

    .line 1854123
    iget-object v2, p0, LX/C9R;->a:LX/C9Y;

    iget-object v2, v2, LX/C9Y;->g:LX/03V;

    const-string v3, "OfflineFailedController"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
