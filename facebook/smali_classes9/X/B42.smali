.class public final LX/B42;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;",
        ">;",
        "Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1741204
    iput-object p1, p0, LX/B42;->b:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    iput-object p2, p0, LX/B42;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1741187
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1741188
    const/4 v1, 0x0

    .line 1741189
    if-eqz p1, :cond_1

    .line 1741190
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741191
    if-eqz v0, :cond_1

    .line 1741192
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741193
    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1741194
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741195
    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1741196
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741197
    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPagesConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1741198
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741199
    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPagesConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPagesConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1741200
    if-nez v0, :cond_0

    .line 1741201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed result for image overlay ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/B42;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1741202
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741203
    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$AssociatedPagesModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
