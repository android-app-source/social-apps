.class public LX/AhQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3us;
.implements LX/5OO;
.implements LX/2dD;


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/5OM;

.field public c:LX/3v0;

.field private d:Landroid/view/View;

.field public e:Z

.field private f:LX/AhT;

.field public g:LX/3uE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3v0;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1702839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1702840
    iput-object p1, p0, LX/AhQ;->a:Landroid/content/Context;

    .line 1702841
    iput-object p2, p0, LX/AhQ;->c:LX/3v0;

    .line 1702842
    iput-boolean p4, p0, LX/AhQ;->e:Z

    .line 1702843
    iput-object p3, p0, LX/AhQ;->d:Landroid/view/View;

    .line 1702844
    invoke-virtual {p2, p0}, LX/3v0;->a(LX/3us;)V

    .line 1702845
    return-void
.end method


# virtual methods
.method public final a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 1702834
    iget-object v0, p0, LX/AhQ;->c:LX/3v0;

    if-eq p1, v0, :cond_1

    .line 1702835
    :cond_0
    :goto_0
    return-void

    .line 1702836
    :cond_1
    invoke-virtual {p0}, LX/AhQ;->c()V

    .line 1702837
    iget-object v0, p0, LX/AhQ;->g:LX/3uE;

    if-eqz v0, :cond_0

    .line 1702838
    iget-object v0, p0, LX/AhQ;->g:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 0

    .prologue
    .line 1702833
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1702820
    new-instance v0, LX/6WS;

    iget-object v1, p0, LX/AhQ;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AhQ;->b:LX/5OM;

    .line 1702821
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    .line 1702822
    iput-object p0, v0, LX/0ht;->H:LX/2dD;

    .line 1702823
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    .line 1702824
    iput-object p0, v0, LX/5OM;->p:LX/5OO;

    .line 1702825
    new-instance v0, LX/AhT;

    iget-object v1, p0, LX/AhQ;->a:Landroid/content/Context;

    iget-object v2, p0, LX/AhQ;->c:LX/3v0;

    invoke-direct {v0, p0, v1, v2}, LX/AhT;-><init>(LX/AhQ;Landroid/content/Context;LX/3v0;)V

    iput-object v0, p0, LX/AhQ;->f:LX/AhT;

    .line 1702826
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    iget-object v1, p0, LX/AhQ;->f:LX/AhT;

    invoke-virtual {v0, v1}, LX/5OM;->a(LX/5OG;)V

    .line 1702827
    iget-object v0, p0, LX/AhQ;->d:Landroid/view/View;

    .line 1702828
    if-eqz v0, :cond_0

    .line 1702829
    iget-object v1, p0, LX/AhQ;->b:LX/5OM;

    invoke-virtual {v1, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 1702830
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1702831
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1702832
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/0ht;)Z
    .locals 1

    .prologue
    .line 1702817
    const/4 v0, 0x0

    iput-object v0, p0, LX/AhQ;->b:LX/5OM;

    .line 1702818
    iget-object v0, p0, LX/AhQ;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->close()V

    .line 1702819
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/3vG;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1702808
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1702809
    new-instance v1, LX/AhQ;

    iget-object v2, p0, LX/AhQ;->a:Landroid/content/Context;

    iget-object v3, p0, LX/AhQ;->d:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3, v0}, LX/AhQ;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;Z)V

    .line 1702810
    iget-object v2, p0, LX/AhQ;->g:LX/3uE;

    .line 1702811
    iput-object v2, v1, LX/AhQ;->g:LX/3uE;

    .line 1702812
    invoke-virtual {v1}, LX/AhQ;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1702813
    iget-object v0, p0, LX/AhQ;->g:LX/3uE;

    if-eqz v0, :cond_0

    .line 1702814
    iget-object v0, p0, LX/AhQ;->g:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    .line 1702815
    :cond_0
    const/4 v0, 0x1

    .line 1702816
    :cond_1
    return v0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1702846
    iget-object v0, p0, LX/AhQ;->f:LX/AhT;

    .line 1702847
    iget-object v0, v0, LX/AhT;->d:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    .line 1702848
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1702805
    iget-object v0, p0, LX/AhQ;->f:LX/AhT;

    if-eqz v0, :cond_0

    .line 1702806
    iget-object v0, p0, LX/AhQ;->f:LX/AhT;

    const v1, -0x535eb1e9

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1702807
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1702804
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 1702803
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1702800
    invoke-virtual {p0}, LX/AhQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1702801
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1702802
    :cond_0
    return-void
.end method

.method public final c(LX/3v3;)Z
    .locals 1

    .prologue
    .line 1702799
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1702796
    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AhQ;->b:LX/5OM;

    .line 1702797
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1702798
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
