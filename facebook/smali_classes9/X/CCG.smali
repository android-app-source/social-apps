.class public LX/CCG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;)V
    .locals 0

    .prologue
    .line 1857445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1857446
    iput-object p1, p0, LX/CCG;->a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;

    .line 1857447
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1857448
    iget-object v0, p0, LX/CCG;->a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;->a()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1857449
    check-cast p2, LX/4Yr;

    .line 1857450
    iget-object v0, p0, LX/CCG;->a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;->a()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 1857451
    if-nez v0, :cond_0

    .line 1857452
    :goto_0
    move-object v0, v1

    .line 1857453
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "text_format_metadata"

    invoke-virtual {v1, p0, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1857454
    return-void

    .line 1857455
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object p0

    if-nez p0, :cond_1

    .line 1857456
    :goto_1
    new-instance p0, LX/4Z8;

    invoke-direct {p0}, LX/4Z8;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 1857457
    iput-object p1, p0, LX/4Z8;->b:Ljava/lang/String;

    .line 1857458
    move-object p0, p0

    .line 1857459
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->m()Ljava/lang/String;

    move-result-object p1

    .line 1857460
    iput-object p1, p0, LX/4Z8;->f:Ljava/lang/String;

    .line 1857461
    move-object p0, p0

    .line 1857462
    iput-object v1, p0, LX/4Z8;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1857463
    move-object v1, p0

    .line 1857464
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->j()Ljava/lang/String;

    move-result-object p0

    .line 1857465
    iput-object p0, v1, LX/4Z8;->c:Ljava/lang/String;

    .line 1857466
    move-object v1, v1

    .line 1857467
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->k()Ljava/lang/String;

    move-result-object p0

    .line 1857468
    iput-object p0, v1, LX/4Z8;->d:Ljava/lang/String;

    .line 1857469
    move-object v1, v1

    .line 1857470
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->n()Ljava/lang/String;

    move-result-object p0

    .line 1857471
    iput-object p0, v1, LX/4Z8;->g:Ljava/lang/String;

    .line 1857472
    move-object v1, v1

    .line 1857473
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->o()Ljava/lang/String;

    move-result-object p0

    .line 1857474
    iput-object p0, v1, LX/4Z8;->h:Ljava/lang/String;

    .line 1857475
    move-object v1, v1

    .line 1857476
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->p()Ljava/lang/String;

    move-result-object p0

    .line 1857477
    iput-object p0, v1, LX/4Z8;->i:Ljava/lang/String;

    .line 1857478
    move-object v1, v1

    .line 1857479
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->r()Ljava/lang/String;

    move-result-object p0

    .line 1857480
    iput-object p0, v1, LX/4Z8;->k:Ljava/lang/String;

    .line 1857481
    move-object v1, v1

    .line 1857482
    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->q()Ljava/lang/String;

    move-result-object p0

    .line 1857483
    iput-object p0, v1, LX/4Z8;->j:Ljava/lang/String;

    .line 1857484
    move-object v1, v1

    .line 1857485
    invoke-virtual {v1}, LX/4Z8;->a()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v1

    goto :goto_0

    .line 1857486
    :cond_1
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 1857487
    iput-object p0, v1, LX/2dc;->h:Ljava/lang/String;

    .line 1857488
    move-object v1, v1

    .line 1857489
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1857490
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1857491
    const-string v0, "RichTextMutatingVisitor"

    return-object v0
.end method
