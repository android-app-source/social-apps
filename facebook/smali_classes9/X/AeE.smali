.class public LX/AeE;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/AeG;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/1FZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field public final e:Landroid/graphics/Path;

.field private f:Landroid/graphics/Canvas;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final g:Landroid/graphics/PorterDuffXfermode;

.field private final h:Landroid/graphics/RectF;

.field private final i:Landroid/view/animation/Interpolator;

.field public final j:Landroid/animation/ValueAnimator;

.field private final k:Landroid/animation/AnimatorListenerAdapter;

.field private final l:Ljava/lang/Runnable;

.field private m:Landroid/graphics/RectF;

.field public n:F

.field public o:F

.field private p:F

.field public q:Z

.field private r:Z

.field private s:LX/1FJ;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1697245
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AeE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1697246
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1697243
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AeE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1697244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1697226
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1697227
    const v0, 0x3f47ae14    # 0.78f

    const v1, 0x3c23d70a    # 0.01f

    const v2, 0x3e947ae1    # 0.29f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, LX/AeE;->i:Landroid/view/animation/Interpolator;

    .line 1697228
    iput-boolean v4, p0, LX/AeE;->q:Z

    .line 1697229
    const-class v0, LX/AeE;

    invoke-static {v0, p0}, LX/AeE;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1697230
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    .line 1697231
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/AeE;->h:Landroid/graphics/RectF;

    .line 1697232
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, LX/AeE;->g:Landroid/graphics/PorterDuffXfermode;

    .line 1697233
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/AeE;->c:Landroid/graphics/Paint;

    .line 1697234
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/AeE;->d:Landroid/graphics/Paint;

    .line 1697235
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    .line 1697236
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1697237
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/AeE;->i:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1697238
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    new-instance v1, LX/AeH;

    invoke-direct {v1, p0}, LX/AeH;-><init>(LX/AeE;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1697239
    new-instance v0, LX/AeI;

    invoke-direct {v0, p0}, LX/AeI;-><init>(LX/AeE;)V

    iput-object v0, p0, LX/AeE;->k:Landroid/animation/AnimatorListenerAdapter;

    .line 1697240
    new-instance v0, Lcom/facebook/facecastdisplay/heatmap/view/GraphView$3;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/heatmap/view/GraphView$3;-><init>(LX/AeE;)V

    iput-object v0, p0, LX/AeE;->l:Ljava/lang/Runnable;

    .line 1697241
    return-void

    .line 1697242
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(LX/AeE;I)I
    .locals 1

    .prologue
    .line 1697221
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0}, LX/AeG;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 1697222
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0}, LX/AeG;->a()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    .line 1697223
    :cond_0
    :goto_0
    return p1

    .line 1697224
    :cond_1
    if-gez p1, :cond_0

    .line 1697225
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1697214
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/AeE;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/AeE;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, LX/AeE;->getWidth()I

    move-result v3

    invoke-virtual {p0}, LX/AeE;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, LX/AeE;->getHeight()I

    move-result v4

    invoke-virtual {p0}, LX/AeE;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/AeE;->m:Landroid/graphics/RectF;

    .line 1697215
    iget-object v0, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/AeE;->n:F

    .line 1697216
    iget-object v0, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iput v0, p0, LX/AeE;->o:F

    .line 1697217
    iget-object v0, p0, LX/AeE;->b:LX/1FZ;

    iget v1, p0, LX/AeE;->n:F

    float-to-int v1, v1

    iget v2, p0, LX/AeE;->o:F

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v2, v3}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/AeE;->s:LX/1FJ;

    .line 1697218
    iget-object v0, p0, LX/AeE;->s:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1697219
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v0, p0, LX/AeE;->s:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    .line 1697220
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AeE;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object p0

    check-cast p0, LX/1FZ;

    iput-object p0, p1, LX/AeE;->b:LX/1FZ;

    return-void
.end method

.method public static b(LX/AeE;Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1697200
    iget-object v0, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 1697201
    invoke-direct {p0}, LX/AeE;->a()V

    .line 1697202
    :cond_0
    iget-object v0, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1697203
    iget-object v0, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    iget-object v1, p0, LX/AeE;->e:Landroid/graphics/Path;

    iget-object v2, p0, LX/AeE;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1697204
    iget-object v0, p0, LX/AeE;->d:Landroid/graphics/Paint;

    iget-object v1, p0, LX/AeE;->g:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1697205
    iget-object v0, p0, LX/AeE;->h:Landroid/graphics/RectF;

    iget v1, p0, LX/AeE;->p:F

    iget v2, p0, LX/AeE;->n:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/AeE;->n:F

    iget v3, p0, LX/AeE;->o:F

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1697206
    iget-object v0, p0, LX/AeE;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/AeE;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0397

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1697207
    iget-object v0, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    iget-object v1, p0, LX/AeE;->h:Landroid/graphics/RectF;

    iget-object v2, p0, LX/AeE;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1697208
    iget-object v0, p0, LX/AeE;->h:Landroid/graphics/RectF;

    iget v1, p0, LX/AeE;->p:F

    iget v2, p0, LX/AeE;->n:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/AeE;->o:F

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1697209
    iget-object v0, p0, LX/AeE;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/AeE;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0396

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1697210
    iget-object v0, p0, LX/AeE;->f:Landroid/graphics/Canvas;

    iget-object v1, p0, LX/AeE;->h:Landroid/graphics/RectF;

    iget-object v2, p0, LX/AeE;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1697211
    iget-object v0, p0, LX/AeE;->s:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1697212
    iget-object v0, p0, LX/AeE;->s:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/AeE;->m:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1697213
    :cond_1
    return-void
.end method

.method public static d(LX/AeE;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const v10, 0x3e19999a    # 0.15f

    .line 1697166
    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v1}, LX/AeG;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1697167
    :cond_0
    return-void

    .line 1697168
    :cond_1
    iget-object v1, p0, LX/AeE;->e:Landroid/graphics/Path;

    iget-object v2, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v2, v0}, LX/AeG;->a(I)LX/AeF;

    move-result-object v2

    .line 1697169
    iget v3, v2, LX/AeF;->b:F

    move v2, v3

    .line 1697170
    iget-object v3, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v3, v0}, LX/AeG;->a(I)LX/AeF;

    move-result-object v3

    .line 1697171
    iget v4, v3, LX/AeF;->c:F

    move v3, v4

    .line 1697172
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    move v7, v0

    .line 1697173
    :goto_0
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0}, LX/AeG;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_0

    .line 1697174
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0, v7}, LX/AeG;->a(I)LX/AeF;

    move-result-object v0

    .line 1697175
    iget v1, v0, LX/AeF;->b:F

    move v0, v1

    .line 1697176
    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v1, v7}, LX/AeG;->a(I)LX/AeF;

    move-result-object v1

    .line 1697177
    iget v2, v1, LX/AeF;->c:F

    move v2, v2

    .line 1697178
    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v1, v3}, LX/AeG;->a(I)LX/AeF;

    move-result-object v1

    .line 1697179
    iget v3, v1, LX/AeF;->b:F

    move v5, v3

    .line 1697180
    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v1, v3}, LX/AeG;->a(I)LX/AeF;

    move-result-object v1

    .line 1697181
    iget v3, v1, LX/AeF;->c:F

    move v6, v3

    .line 1697182
    iget-object v1, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v3, v7, -0x1

    invoke-static {p0, v3}, LX/AeE;->a(LX/AeE;I)I

    move-result v3

    invoke-virtual {v1, v3}, LX/AeG;->a(I)LX/AeF;

    move-result-object v1

    .line 1697183
    iget v3, v1, LX/AeF;->b:F

    move v1, v3

    .line 1697184
    sub-float v1, v5, v1

    .line 1697185
    iget-object v3, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v4, v7, -0x1

    invoke-static {p0, v4}, LX/AeE;->a(LX/AeE;I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/AeG;->a(I)LX/AeF;

    move-result-object v3

    .line 1697186
    iget v4, v3, LX/AeF;->c:F

    move v3, v4

    .line 1697187
    sub-float v3, v6, v3

    .line 1697188
    iget-object v4, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v8, v7, 0x2

    invoke-static {p0, v8}, LX/AeE;->a(LX/AeE;I)I

    move-result v8

    invoke-virtual {v4, v8}, LX/AeG;->a(I)LX/AeF;

    move-result-object v4

    .line 1697189
    iget v8, v4, LX/AeF;->b:F

    move v4, v8

    .line 1697190
    sub-float/2addr v4, v0

    .line 1697191
    iget-object v8, p0, LX/AeE;->a:LX/AeG;

    add-int/lit8 v9, v7, 0x2

    invoke-static {p0, v9}, LX/AeE;->a(LX/AeE;I)I

    move-result v9

    invoke-virtual {v8, v9}, LX/AeG;->a(I)LX/AeF;

    move-result-object v8

    .line 1697192
    iget v9, v8, LX/AeF;->c:F

    move v8, v9

    .line 1697193
    sub-float/2addr v8, v2

    .line 1697194
    mul-float/2addr v1, v10

    add-float/2addr v1, v0

    .line 1697195
    mul-float v0, v10, v3

    add-float/2addr v2, v0

    .line 1697196
    mul-float v0, v10, v4

    sub-float v3, v5, v0

    .line 1697197
    mul-float v0, v10, v8

    sub-float v4, v6, v0

    .line 1697198
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1697199
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1697160
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1697161
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/AeE;->k:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1697162
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 1697163
    :goto_0
    return-void

    .line 1697164
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AeE;->q:Z

    .line 1697165
    invoke-virtual {p0}, LX/AeE;->invalidate()V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1697157
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/AeE;->b(Z)V

    .line 1697158
    iget-object v0, p0, LX/AeE;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, LX/AeE;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1697159
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1697146
    iget-object v0, p0, LX/AeE;->l:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/AeE;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1697147
    if-eqz p1, :cond_0

    .line 1697148
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/AeE;->k:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1697149
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 1697150
    iget-object v1, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 1697151
    iget-object v1, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x43fa0000    # 500.0f

    mul-float/2addr v0, v2

    float-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 1697152
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1697153
    iput-boolean v4, p0, LX/AeE;->q:Z

    .line 1697154
    :goto_0
    return-void

    .line 1697155
    :cond_0
    iput-boolean v4, p0, LX/AeE;->q:Z

    .line 1697156
    invoke-virtual {p0}, LX/AeE;->invalidate()V

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 1697115
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1697116
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/AeE;->q:Z

    if-nez v0, :cond_0

    .line 1697117
    iget-boolean v0, p0, LX/AeE;->r:Z

    if-eqz v0, :cond_1

    .line 1697118
    invoke-static {p0, p1}, LX/AeE;->b(LX/AeE;Landroid/graphics/Canvas;)V

    .line 1697119
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AeE;->r:Z

    .line 1697120
    :cond_0
    :goto_0
    return-void

    .line 1697121
    :cond_1
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0}, LX/AeG;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1697122
    :cond_2
    const/4 v3, 0x0

    .line 1697123
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    if-nez v0, :cond_5

    .line 1697124
    :goto_1
    goto :goto_0

    .line 1697125
    :cond_3
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    .line 1697126
    iget v1, v0, LX/AeG;->b:F

    move v1, v1

    .line 1697127
    iget-object v0, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v0}, LX/AeG;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v2, v0

    .line 1697128
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v3}, LX/AeG;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1697129
    iget-object v3, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v3, v0}, LX/AeG;->a(I)LX/AeF;

    move-result-object v3

    int-to-float v4, v0

    .line 1697130
    div-float v5, v4, v2

    iget v6, p0, LX/AeE;->n:F

    mul-float/2addr v5, v6

    move v4, v5

    .line 1697131
    iget-object v5, p0, LX/AeE;->a:LX/AeG;

    invoke-virtual {v5, v0}, LX/AeG;->a(I)LX/AeF;

    move-result-object v5

    .line 1697132
    iget v6, v5, LX/AeF;->a:F

    move v5, v6

    .line 1697133
    iget-object v6, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v6}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v6}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v6

    .line 1697134
    :goto_3
    iget v7, p0, LX/AeE;->o:F

    div-float v8, v5, v1

    iget v9, p0, LX/AeE;->o:F

    mul-float/2addr v8, v9

    mul-float/2addr v6, v8

    sub-float v6, v7, v6

    move v5, v6

    .line 1697135
    iput v4, v3, LX/AeF;->b:F

    .line 1697136
    iput v5, v3, LX/AeF;->c:F

    .line 1697137
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1697138
    :cond_4
    const/high16 v6, 0x3f800000    # 1.0f

    goto :goto_3

    .line 1697139
    :cond_5
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1697140
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    iget v1, p0, LX/AeE;->o:F

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1697141
    invoke-static {p0}, LX/AeE;->d(LX/AeE;)V

    .line 1697142
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    iget v1, p0, LX/AeE;->n:F

    iget v2, p0, LX/AeE;->o:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1697143
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    iget v1, p0, LX/AeE;->o:F

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1697144
    iget-object v0, p0, LX/AeE;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1697145
    invoke-static {p0, p1}, LX/AeE;->b(LX/AeE;Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5137cb5b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1697111
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1697112
    if-ne p3, p1, :cond_0

    if-eq p4, p2, :cond_1

    .line 1697113
    :cond_0
    invoke-direct {p0}, LX/AeE;->a()V

    .line 1697114
    :cond_1
    const/16 v1, 0x2d

    const v2, -0x7ebf9fff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCursor(F)V
    .locals 1

    .prologue
    .line 1697106
    iput p1, p0, LX/AeE;->p:F

    .line 1697107
    iget-object v0, p0, LX/AeE;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1697108
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AeE;->r:Z

    .line 1697109
    invoke-virtual {p0}, LX/AeE;->invalidate()V

    .line 1697110
    :cond_0
    return-void
.end method
