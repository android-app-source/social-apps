.class public final LX/AqL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AqM;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z

.field public e:LX/1dQ;

.field public final synthetic f:LX/AqM;


# direct methods
.method public constructor <init>(LX/AqM;)V
    .locals 1

    .prologue
    .line 1717514
    iput-object p1, p0, LX/AqL;->f:LX/AqM;

    .line 1717515
    move-object v0, p1

    .line 1717516
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1717517
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1717518
    const-string v0, "FigToggleButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1717519
    if-ne p0, p1, :cond_1

    .line 1717520
    :cond_0
    :goto_0
    return v0

    .line 1717521
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1717522
    goto :goto_0

    .line 1717523
    :cond_3
    check-cast p1, LX/AqL;

    .line 1717524
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1717525
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1717526
    if-eq v2, v3, :cond_0

    .line 1717527
    iget v2, p0, LX/AqL;->a:I

    iget v3, p1, LX/AqL;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1717528
    goto :goto_0

    .line 1717529
    :cond_4
    iget v2, p0, LX/AqL;->b:I

    iget v3, p1, LX/AqL;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1717530
    goto :goto_0

    .line 1717531
    :cond_5
    iget v2, p0, LX/AqL;->c:I

    iget v3, p1, LX/AqL;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1717532
    goto :goto_0

    .line 1717533
    :cond_6
    iget-boolean v2, p0, LX/AqL;->d:Z

    iget-boolean v3, p1, LX/AqL;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1717534
    goto :goto_0
.end method
