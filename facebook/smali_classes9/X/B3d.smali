.class public final LX/B3d;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final l:Landroid/view/View;

.field private final m:LX/B3p;

.field public n:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/B3p;)V
    .locals 1

    .prologue
    .line 1740547
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1740548
    iput-object p1, p0, LX/B3d;->l:Landroid/view/View;

    .line 1740549
    iput-object p2, p0, LX/B3d;->m:LX/B3p;

    .line 1740550
    iget-object v0, p0, LX/B3d;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1740551
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x20272f3f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1740552
    iget-object v1, p0, LX/B3d;->l:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 1740553
    iget-object v1, p0, LX/B3d;->m:LX/B3p;

    iget-object v2, p0, LX/B3d;->n:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1740554
    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->c()LX/174;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1740555
    iget-object p0, v1, LX/B3p;->a:LX/B3Z;

    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->c()LX/174;

    move-result-object p1

    invoke-interface {p1}, LX/174;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/B3Z;->b(Ljava/lang/String;)V

    .line 1740556
    :cond_0
    iget-object p0, v1, LX/B3p;->a:LX/B3Z;

    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/B3Z;->a(Ljava/lang/String;)V

    .line 1740557
    :cond_1
    const v1, -0x51aae274

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
