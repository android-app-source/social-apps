.class public final LX/Cf9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/CfC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1925579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Cf9;

    invoke-static {v0}, LX/CfC;->a(LX/0QB;)LX/CfC;

    move-result-object v0

    check-cast v0, LX/CfC;

    iput-object v0, p0, LX/Cf9;->a:LX/CfC;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x26

    const v1, -0x7c5d040

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925580
    if-eqz p2, :cond_0

    const-string v1, "com.facebook.push.registration.ACTION_ALARM"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "serviceType"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1925581
    :cond_0
    sget-object v1, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a:Ljava/lang/Class;

    const-string v2, "Incorrect intent %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925582
    const/16 v1, 0x27

    const v2, 0x2de430bf

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925583
    :goto_0
    return-void

    .line 1925584
    :cond_1
    const-string v1, "serviceType"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1925585
    if-nez v1, :cond_2

    .line 1925586
    const v1, 0x5e93db0

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1925587
    :cond_2
    invoke-static {p0, p1}, LX/Cf9;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925588
    invoke-static {p1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925589
    iget-object v2, p0, LX/Cf9;->a:LX/CfC;

    invoke-virtual {v2, v1}, LX/CfC;->a(Ljava/lang/String;)V

    .line 1925590
    const v1, -0x30805a9e

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
