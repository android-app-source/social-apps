.class public final LX/CUF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/CU1;

.field public final b:LX/CTw;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1896409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896410
    const v0, -0x64c54929

    invoke-static {p1, p2, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1896411
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v3, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896412
    new-instance v1, LX/CU1;

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v3, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-direct {v1, v0}, LX/CU1;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;)V

    iput-object v1, p0, LX/CUF;->a:LX/CU1;

    .line 1896413
    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/CTw;

    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v1

    invoke-direct {v0, p1, v1}, LX/CTw;-><init>(LX/15i;I)V

    :goto_0
    iput-object v0, p0, LX/CUF;->b:LX/CTw;

    .line 1896414
    return-void

    .line 1896415
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
