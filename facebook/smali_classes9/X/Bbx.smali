.class public final LX/Bbx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;)V
    .locals 0

    .prologue
    .line 1801305
    iput-object p1, p0, LX/Bbx;->a:Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1801306
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801307
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1801308
    if-eqz p1, :cond_0

    .line 1801309
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801310
    if-eqz v0, :cond_0

    .line 1801311
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801312
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1801313
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1801314
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel;->a()LX/0Px;

    move-result-object v0

    .line 1801315
    iget-object v1, p0, LX/Bbx;->a:Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->e:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    new-instance v2, LX/Bbw;

    invoke-direct {v2, p0, v0}, LX/Bbw;-><init>(LX/Bbx;LX/0Px;)V

    invoke-virtual {v1, v2}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a(LX/6Zz;)V

    .line 1801316
    :cond_0
    return-void
.end method
