.class public LX/CN2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/CN2;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/1Er;

.field private final d:LX/2MM;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/43C;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1881270
    const-string v0, "image/jpeg"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/CN2;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/1Er;LX/2MM;LX/0Or;LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1Er;",
            "LX/2MM;",
            "LX/0Or",
            "<",
            "LX/43C;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1881259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881260
    iput-object p1, p0, LX/CN2;->b:LX/0Sh;

    .line 1881261
    iput-object p2, p0, LX/CN2;->c:LX/1Er;

    .line 1881262
    iput-object p3, p0, LX/CN2;->d:LX/2MM;

    .line 1881263
    iput-object p4, p0, LX/CN2;->e:LX/0Or;

    .line 1881264
    iput-object p5, p0, LX/CN2;->f:LX/0ad;

    .line 1881265
    return-void
.end method

.method private a(Landroid/net/Uri;)LX/46f;
    .locals 3

    .prologue
    .line 1881266
    iget-object v0, p0, LX/CN2;->d:LX/2MM;

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, p1, v1}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;

    move-result-object v0

    .line 1881267
    iget-object v1, v0, LX/46f;->a:Ljava/io/File;

    if-nez v1, :cond_0

    .line 1881268
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to resolve backing file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881269
    :cond_0
    return-object v0
.end method

.method public static a(LX/0QB;)LX/CN2;
    .locals 9

    .prologue
    .line 1881246
    sget-object v0, LX/CN2;->g:LX/CN2;

    if-nez v0, :cond_1

    .line 1881247
    const-class v1, LX/CN2;

    monitor-enter v1

    .line 1881248
    :try_start_0
    sget-object v0, LX/CN2;->g:LX/CN2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1881249
    if-eqz v2, :cond_0

    .line 1881250
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1881251
    new-instance v3, LX/CN2;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v5

    check-cast v5, LX/1Er;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v6

    check-cast v6, LX/2MM;

    const/16 v7, 0x1802

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/CN2;-><init>(LX/0Sh;LX/1Er;LX/2MM;LX/0Or;LX/0ad;)V

    .line 1881252
    move-object v0, v3

    .line 1881253
    sput-object v0, LX/CN2;->g:LX/CN2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1881254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1881255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1881256
    :cond_1
    sget-object v0, LX/CN2;->g:LX/CN2;

    return-object v0

    .line 1881257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1881258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1881242
    iget-object v0, p0, LX/CN2;->c:LX/1Er;

    invoke-virtual {v0, p1, p2, p3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1881243
    if-nez v0, :cond_0

    .line 1881244
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to create temp file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881245
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;IILjava/lang/String;Ljava/lang/String;LX/46h;)LX/CN4;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1881224
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1881225
    iget-object v0, p0, LX/CN2;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1881226
    :try_start_0
    invoke-direct {p0, p4, p5, p6}, LX/CN2;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1881227
    :try_start_1
    invoke-direct {p0, p1}, LX/CN2;->a(Landroid/net/Uri;)LX/46f;

    move-result-object v2

    .line 1881228
    new-instance v3, LX/43G;

    const/4 v0, 0x1

    invoke-direct {v3, p2, p2, v0, p3}, LX/43G;-><init>(IIZI)V

    .line 1881229
    iget-object v0, p0, LX/CN2;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/43C;

    iget-object v4, v2, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5, v3}, LX/43C;->a(Ljava/lang/String;Ljava/lang/String;LX/43G;)LX/43G;

    .line 1881230
    new-instance v0, LX/CN4;

    sget-object v3, LX/CN3;->SUCCESS:LX/CN3;

    const/4 v4, 0x0

    invoke-direct {v0, v3, v1, v4}, LX/CN4;-><init>(LX/CN3;Ljava/io/File;Ljava/lang/Exception;)V

    move-object v0, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1881231
    if-eqz v2, :cond_0

    .line 1881232
    invoke-virtual {v2}, LX/46f;->a()V

    :cond_0
    :goto_0
    return-object v0

    .line 1881233
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1881234
    :goto_1
    if-eqz v1, :cond_1

    .line 1881235
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1881236
    :cond_1
    :goto_2
    :try_start_3
    new-instance v1, LX/CN4;

    sget-object v3, LX/CN3;->FAILURE:LX/CN3;

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4, v0}, LX/CN4;-><init>(LX/CN3;Ljava/io/File;Ljava/lang/Exception;)V

    move-object v0, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1881237
    if-eqz v2, :cond_0

    .line 1881238
    invoke-virtual {v2}, LX/46f;->a()V

    goto :goto_0

    .line 1881239
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 1881240
    invoke-virtual {v2}, LX/46f;->a()V

    :cond_2
    throw v0

    :catch_1
    goto :goto_2

    .line 1881241
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1881222
    sget-object v1, LX/CN2;->a:LX/0Rf;

    invoke-virtual {v1, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 1881223
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CN2;->f:LX/0ad;

    sget-short v2, LX/CN1;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
