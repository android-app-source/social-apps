.class public LX/CUU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6uV",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6ui;


# direct methods
.method public constructor <init>(LX/6ui;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1896582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896583
    iput-object p1, p0, LX/CUU;->a:LX/6ui;

    .line 1896584
    return-void
.end method

.method private static a(LX/0Pz;Lcom/facebook/payments/confirmation/ConfirmationParams;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6uG;",
            ">;",
            "Lcom/facebook/payments/confirmation/ConfirmationParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1896585
    new-instance v0, LX/CUQ;

    check-cast p1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;

    invoke-direct {v0, p1}, LX/CUQ;-><init>(Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1896586
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/confirmation/ConfirmationData;)LX/0Px;
    .locals 9

    .prologue
    .line 1896587
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    const/4 v2, 0x0

    .line 1896588
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1896589
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v4

    .line 1896590
    sget-object v0, LX/6uT;->CHECK_MARK:LX/6uT;

    sget-object v1, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    sget-object v5, LX/6uT;->DIVIDER:LX/6uT;

    sget-object v6, LX/6uT;->SEE_RECEIPT:LX/6uT;

    invoke-static {v0, v1, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v5, v0

    .line 1896591
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uT;

    .line 1896592
    sget-object v7, LX/CUT;->a:[I

    invoke-virtual {v0}, LX/6uT;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1896593
    iget-object v7, p0, LX/CUU;->a:LX/6ui;

    invoke-virtual {v7, v3, v0, p1, v2}, LX/6ui;->a(LX/0Pz;LX/6uT;Lcom/facebook/payments/confirmation/SimpleConfirmationData;Z)V

    .line 1896594
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1896595
    :pswitch_0
    invoke-static {v3, v4}, LX/CUU;->a(LX/0Pz;Lcom/facebook/payments/confirmation/ConfirmationParams;)V

    goto :goto_1

    .line 1896596
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
