.class public final LX/BxN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 1835318
    iput-object p1, p0, LX/BxN;->b:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    iput-object p2, p0, LX/BxN;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x39f54b30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1835319
    iget-object v1, p0, LX/BxN;->b:Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    iget-object v2, p0, LX/BxN;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835320
    iget-object v4, v1, LX/AnF;->d:LX/1PW;

    instance-of v4, v4, LX/1Po;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "Unsupported environment: "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v1, LX/AnF;->d:LX/1PW;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1835321
    iget-object v4, v1, LX/AnF;->d:LX/1PW;

    check-cast v4, LX/1Po;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    iget-object v5, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v4}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v4

    .line 1835322
    const-string v5, "articleChaining"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object p0

    invoke-virtual {p0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p0

    invoke-static {v4, v5, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    sget-object v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    .line 1835323
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1835324
    iget-object p0, v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->n:LX/1Kf;

    iget-object p1, v1, LX/AnF;->a:Landroid/content/Context;

    invoke-interface {p0, v5, v4, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1835325
    const v1, 0x6d7c0bb2

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
