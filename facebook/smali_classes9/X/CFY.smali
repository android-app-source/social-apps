.class public final LX/CFY;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;

.field public final synthetic d:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V
    .locals 0

    .prologue
    .line 1863449
    iput-object p1, p0, LX/CFY;->d:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    iput-object p2, p0, LX/CFY;->a:Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    iput-object p3, p0, LX/CFY;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CFY;->c:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1863450
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1863451
    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 1863452
    :cond_0
    :goto_0
    return-void

    .line 1863453
    :cond_1
    iget-object v0, p0, LX/CFY;->d:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    iget-object v1, p0, LX/CFY;->d:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    iget-object v1, v1, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->m:LX/2HB;

    iget-object v2, p0, LX/CFY;->a:Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a$redex0(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/2HB;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Z)V

    .line 1863454
    iget-object v0, p0, LX/CFY;->d:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    iget-object v0, v0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->k:LX/03V;

    const-string v1, "GoodwillPublishUpload"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to publish goodwill event of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/CFY;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1863455
    iget-object v0, p0, LX/CFY;->c:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;

    if-eqz v0, :cond_0

    .line 1863456
    iget-object v0, p0, LX/CFY;->c:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;

    invoke-virtual {v0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;->b()V

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1863457
    return-void
.end method
