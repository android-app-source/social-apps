.class public final enum LX/Cf5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cf5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cf5;

.field public static final enum REGISTER:LX/Cf5;

.field public static final enum RETRY:LX/Cf5;

.field public static final enum UNREGISTER:LX/Cf5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1925420
    new-instance v0, LX/Cf5;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v2}, LX/Cf5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf5;->REGISTER:LX/Cf5;

    .line 1925421
    new-instance v0, LX/Cf5;

    const-string v1, "UNREGISTER"

    invoke-direct {v0, v1, v3}, LX/Cf5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf5;->UNREGISTER:LX/Cf5;

    .line 1925422
    new-instance v0, LX/Cf5;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v4}, LX/Cf5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf5;->RETRY:LX/Cf5;

    .line 1925423
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cf5;

    sget-object v1, LX/Cf5;->REGISTER:LX/Cf5;

    aput-object v1, v0, v2

    sget-object v1, LX/Cf5;->UNREGISTER:LX/Cf5;

    aput-object v1, v0, v3

    sget-object v1, LX/Cf5;->RETRY:LX/Cf5;

    aput-object v1, v0, v4

    sput-object v0, LX/Cf5;->$VALUES:[LX/Cf5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1925424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cf5;
    .locals 1

    .prologue
    .line 1925425
    const-class v0, LX/Cf5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cf5;

    return-object v0
.end method

.method public static values()[LX/Cf5;
    .locals 1

    .prologue
    .line 1925426
    sget-object v0, LX/Cf5;->$VALUES:[LX/Cf5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cf5;

    return-object v0
.end method
