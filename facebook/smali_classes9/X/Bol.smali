.class public LX/Bol;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1QU;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Bom;


# direct methods
.method public constructor <init>(LX/Bom;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1821951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821952
    iput-object p1, p0, LX/Bol;->a:LX/Bom;

    .line 1821953
    return-void
.end method

.method public static a(LX/0QB;)LX/Bol;
    .locals 4

    .prologue
    .line 1821954
    const-class v1, LX/Bol;

    monitor-enter v1

    .line 1821955
    :try_start_0
    sget-object v0, LX/Bol;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1821956
    sput-object v2, LX/Bol;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1821957
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821958
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1821959
    new-instance p0, LX/Bol;

    const-class v3, LX/Bom;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Bom;

    invoke-direct {p0, v3}, LX/Bol;-><init>(LX/Bom;)V

    .line 1821960
    move-object v0, p0

    .line 1821961
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1821962
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bol;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1821963
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1821964
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Pf;)LX/1SX;
    .locals 1

    .prologue
    .line 1821965
    iget-object v0, p0, LX/Bol;->a:LX/Bom;

    invoke-virtual {v0, p1}, LX/Bom;->a(LX/1Pf;)LX/Bok;

    move-result-object v0

    return-object v0
.end method
