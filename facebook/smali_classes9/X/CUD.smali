.class public LX/CUD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/CUA;

.field public final b:LX/CUC;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTJ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/CU8;

.field public final f:LX/CTJ;

.field public final g:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;",
            "LX/CTv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;LX/CSr;Lcom/facebook/graphql/enums/GraphQLObjectType;)V
    .locals 7

    .prologue
    .line 1896369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896370
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896371
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896372
    iput-object p3, p0, LX/CUD;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1896373
    new-instance v0, LX/CUA;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CUA;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;)V

    iput-object v0, p0, LX/CUD;->a:LX/CUA;

    .line 1896374
    new-instance v0, LX/CUC;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CUC;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;)V

    iput-object v0, p0, LX/CUD;->b:LX/CUC;

    .line 1896375
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1896376
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 1896377
    new-instance v0, LX/CU8;

    iget-object p3, p0, LX/CUD;->a:LX/CUA;

    iget-object p3, p3, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-direct {v0, v1, v2, p3}, LX/CU8;-><init>(LX/15i;ILcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;)V

    :goto_0
    move-object v0, v0

    .line 1896378
    iput-object v0, p0, LX/CUD;->e:LX/CU8;

    .line 1896379
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    .line 1896380
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1896381
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->o()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1896382
    const/4 v5, 0x1

    const-class v6, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const/4 p3, 0x0

    invoke-virtual {v4, v1, v5, v6, p3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v5

    const/4 v6, 0x0

    const-class p3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-virtual {v4, v1, v6, p3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-static {v1}, LX/CTv;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;)LX/CTv;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1896383
    :cond_0
    move-object v0, v2

    .line 1896384
    iput-object v0, p0, LX/CUD;->h:Ljava/util/HashMap;

    .line 1896385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1896386
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p2, v0}, LX/CUD;->a(LX/0Px;LX/CSr;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/CUD;->c:Ljava/util/ArrayList;

    .line 1896387
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->q()LX/0Px;

    move-result-object v1

    invoke-static {v1, p2, v0}, LX/CUD;->a(LX/0Px;LX/CSr;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/CUD;->d:Ljava/util/ArrayList;

    .line 1896388
    iget-object v1, p0, LX/CUD;->a:LX/CUA;

    iget-object v1, v1, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1896389
    sget-object v4, LX/CU7;->b:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1896390
    const/4 v2, 0x0

    :goto_2
    move-object v0, v2

    .line 1896391
    iput-object v0, p0, LX/CUD;->f:LX/CTJ;

    .line 1896392
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1896393
    :pswitch_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v2, :cond_2

    :goto_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1896394
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/CTX;

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1896395
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CTJ;

    goto :goto_2

    :cond_2
    move v2, v3

    .line 1896396
    goto :goto_3

    .line 1896397
    :pswitch_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v2, :cond_3

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1896398
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/CTb;

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1896399
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CTJ;

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1896400
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/CUD;Ljava/lang/String;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1896359
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1896360
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    iget-object v0, v0, LX/CTJ;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1896361
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1896362
    :cond_0
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 1896363
    const/4 v0, 0x0

    .line 1896364
    :goto_1
    return-object v0

    :cond_1
    move v2, v1

    .line 1896365
    :goto_2
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1896366
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    iget-object v0, v0, LX/CTJ;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1896367
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1896368
    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static a(LX/0Px;LX/CSr;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;",
            ">;",
            "LX/CSr;",
            "Ljava/util/ArrayList",
            "<",
            "LX/CTJ;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/CTJ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1896333
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1896334
    if-eqz p0, :cond_2

    .line 1896335
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_2

    invoke-virtual {p0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    .line 1896336
    sget-object v1, LX/CU7;->a:[I

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1896337
    invoke-interface {p1, v0, v10}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896338
    :cond_0
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1896339
    :pswitch_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1896340
    invoke-interface {p1, v0, v10}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1896341
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->K()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    invoke-virtual {v2, v1, v4, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v2, v1

    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v3, v4

    :goto_3
    if-ge v3, v8, :cond_0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    .line 1896342
    invoke-static {v1}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->o()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v1, v9}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896343
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1896344
    :cond_1
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1896345
    move-object v2, v1

    goto :goto_2

    .line 1896346
    :cond_2
    return-object v6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/CU9;LX/0Px;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CU9;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1896347
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1896348
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1896349
    invoke-static {p0, v0}, LX/CUD;->a(LX/CUD;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 1896350
    if-eqz v7, :cond_1

    .line 1896351
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896352
    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v1, v0, :cond_1

    .line 1896353
    iget-object v0, p0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    sget-object v2, LX/CU9;->SHIMMER:LX/CU9;

    invoke-virtual {p1, v2}, LX/CU9;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 1896354
    :goto_2
    iput-boolean v2, v0, LX/CTJ;->a:Z

    .line 1896355
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    move v2, v3

    .line 1896356
    goto :goto_2

    .line 1896357
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1896358
    :cond_2
    return-object v5
.end method
