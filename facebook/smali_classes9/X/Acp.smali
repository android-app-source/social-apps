.class public LX/Acp;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Acl;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/graphics/Paint;

.field public d:Landroid/view/WindowManager;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/view/WindowManager$LayoutParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/3Hd;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693165
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Acp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693166
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1693167
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Acp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693168
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1693169
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693170
    invoke-virtual {p0, v2}, LX/Acp;->setOrientation(I)V

    .line 1693171
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/Acp;->a:Ljava/util/Map;

    .line 1693172
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Acp;->b:Landroid/view/LayoutInflater;

    .line 1693173
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    .line 1693174
    iget-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    const v1, 0x7f0a00d6

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1693175
    iget-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1693176
    iget-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1693177
    iget-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/Acp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b19f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1693178
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;)V
    .locals 3

    .prologue
    .line 1693179
    invoke-virtual {p0, p1}, LX/Acp;->removeView(Landroid/view/View;)V

    .line 1693180
    iget-object v0, p0, LX/Acp;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1693181
    iget-object v0, p0, LX/Acp;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1693182
    iget-object v0, p0, LX/Acp;->f:LX/3Hd;

    if-eqz v0, :cond_0

    .line 1693183
    iget-object v0, p0, LX/Acp;->f:LX/3Hd;

    .line 1693184
    iget-object v1, v0, LX/3Hd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1693185
    sget-object v2, LX/3Hd;->b:LX/0Tn;

    invoke-virtual {p0}, LX/Acp;->getPosition()Landroid/graphics/Point;

    move-result-object p1

    iget p1, p1, Landroid/graphics/Point;->x:I

    invoke-interface {v1, v2, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 1693186
    sget-object v2, LX/3Hd;->c:LX/0Tn;

    invoke-virtual {p0}, LX/Acp;->getPosition()Landroid/graphics/Point;

    move-result-object p1

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-interface {v1, v2, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 1693187
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1693188
    iget-object v1, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    if-eqz v1, :cond_0

    .line 1693189
    iget-object v1, v0, LX/3Hd;->f:Landroid/content/Context;

    iget-object v2, v0, LX/3Hd;->g:LX/3He;

    const p1, 0x5ad76dd0

    invoke-static {v1, v2, p1}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 1693190
    const/4 v1, 0x0

    iput-object v1, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 1693191
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1693192
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1693193
    iget-object v0, p0, LX/Acp;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 1693194
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v3, v0, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v4, v0, v1

    iget-object v5, p0, LX/Acp;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1693195
    return-void
.end method

.method public getPosition()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 1693196
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method
