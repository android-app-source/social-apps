.class public final enum LX/CfA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CfA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CfA;

.field public static final enum CURRENT:LX/CfA;

.field public static final enum EXPIRED:LX/CfA;

.field public static final enum NONE:LX/CfA;

.field public static final enum WRONG_USER:LX/CfA;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1925593
    new-instance v0, LX/CfA;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v2}, LX/CfA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CfA;->CURRENT:LX/CfA;

    .line 1925594
    new-instance v0, LX/CfA;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v3}, LX/CfA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CfA;->EXPIRED:LX/CfA;

    .line 1925595
    new-instance v0, LX/CfA;

    const-string v1, "WRONG_USER"

    invoke-direct {v0, v1, v4}, LX/CfA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CfA;->WRONG_USER:LX/CfA;

    .line 1925596
    new-instance v0, LX/CfA;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/CfA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CfA;->NONE:LX/CfA;

    .line 1925597
    const/4 v0, 0x4

    new-array v0, v0, [LX/CfA;

    sget-object v1, LX/CfA;->CURRENT:LX/CfA;

    aput-object v1, v0, v2

    sget-object v1, LX/CfA;->EXPIRED:LX/CfA;

    aput-object v1, v0, v3

    sget-object v1, LX/CfA;->WRONG_USER:LX/CfA;

    aput-object v1, v0, v4

    sget-object v1, LX/CfA;->NONE:LX/CfA;

    aput-object v1, v0, v5

    sput-object v0, LX/CfA;->$VALUES:[LX/CfA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1925598
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CfA;
    .locals 1

    .prologue
    .line 1925592
    const-class v0, LX/CfA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CfA;

    return-object v0
.end method

.method public static values()[LX/CfA;
    .locals 1

    .prologue
    .line 1925591
    sget-object v0, LX/CfA;->$VALUES:[LX/CfA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CfA;

    return-object v0
.end method
