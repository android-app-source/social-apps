.class public LX/Afc;
.super LX/AeK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeK",
        "<",
        "LX/Afa;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1699462
    invoke-direct {p0, p1}, LX/AeK;-><init>(Landroid/view/View;)V

    .line 1699463
    const v0, 0x7f0d197d

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Afc;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1699464
    return-void
.end method


# virtual methods
.method public final a(LX/AeO;LX/AeU;)V
    .locals 4

    .prologue
    .line 1699465
    check-cast p1, LX/Afa;

    .line 1699466
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1699467
    sget-object v1, LX/Afb;->a:[I

    iget-object v2, p1, LX/Afa;->a:LX/AfZ;

    invoke-virtual {v2}, LX/AfZ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1699468
    :goto_0
    return-void

    .line 1699469
    :pswitch_0
    new-instance v1, LX/47x;

    invoke-direct {v1, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1699470
    const v2, 0x7f080c1b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1699471
    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    const/4 v3, 0x1

    invoke-static {v3, v0}, LX/3Hk;->a(ZLandroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 1699472
    iget-object v1, p0, LX/Afc;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
