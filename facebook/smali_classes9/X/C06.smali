.class public LX/C06;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/BzX;

.field public final b:LX/1WM;

.field public final c:LX/Cch;


# direct methods
.method public constructor <init>(LX/BzX;LX/1WM;LX/Cch;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1839906
    iput-object p1, p0, LX/C06;->a:LX/BzX;

    .line 1839907
    iput-object p2, p0, LX/C06;->b:LX/1WM;

    .line 1839908
    iput-object p3, p0, LX/C06;->c:LX/Cch;

    .line 1839909
    return-void
.end method

.method public static a(LX/0QB;)LX/C06;
    .locals 6

    .prologue
    .line 1839910
    const-class v1, LX/C06;

    monitor-enter v1

    .line 1839911
    :try_start_0
    sget-object v0, LX/C06;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839912
    sput-object v2, LX/C06;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839915
    new-instance p0, LX/C06;

    invoke-static {v0}, LX/BzX;->a(LX/0QB;)LX/BzX;

    move-result-object v3

    check-cast v3, LX/BzX;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v4

    check-cast v4, LX/1WM;

    invoke-static {v0}, LX/Cch;->a(LX/0QB;)LX/Cch;

    move-result-object v5

    check-cast v5, LX/Cch;

    invoke-direct {p0, v3, v4, v5}, LX/C06;-><init>(LX/BzX;LX/1WM;LX/Cch;)V

    .line 1839916
    move-object v0, p0

    .line 1839917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C06;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
