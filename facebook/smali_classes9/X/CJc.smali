.class public LX/CJc;
.super LX/16B;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/CJb;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CJb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875607
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 1875608
    iput-object p1, p0, LX/CJc;->a:Landroid/content/Context;

    .line 1875609
    iput-object p2, p0, LX/CJc;->b:LX/CJb;

    .line 1875610
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 1

    .prologue
    .line 1875605
    iget-object v0, p0, LX/CJc;->b:LX/CJb;

    invoke-virtual {v0}, LX/CJb;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/CJc;->c:Z

    .line 1875606
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1875600
    iget-boolean v0, p0, LX/CJc;->c:Z

    if-nez v0, :cond_0

    .line 1875601
    :goto_0
    return-void

    .line 1875602
    :cond_0
    iget-object v0, p0, LX/CJc;->a:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/049;->f(Landroid/content/Context;ZLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1875603
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CJc;->c:Z

    .line 1875604
    return-void
.end method
