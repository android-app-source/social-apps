.class public LX/AVl;
.super LX/AVk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVk",
        "<",
        "Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideCommentsEventModel;",
        ">;"
    }
.end annotation


# instance fields
.field public f:Landroid/view/View;

.field public g:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680237
    invoke-direct {p0}, LX/AVk;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1680238
    check-cast p1, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    .line 1680239
    iget-object v0, p1, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->a:Landroid/view/View;

    iput-object v0, p0, LX/AVl;->g:Landroid/view/View;

    .line 1680240
    iget-object v0, p1, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->b:Landroid/view/View;

    iput-object v0, p0, LX/AVl;->f:Landroid/view/View;

    .line 1680241
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 1680242
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVl;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVl;->g:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1680243
    :cond_0
    :goto_0
    return-void

    .line 1680244
    :cond_1
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->l()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->l()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->l()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1680245
    :cond_2
    :goto_1
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680246
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->setVisibility(I)V

    goto :goto_0

    .line 1680247
    :cond_3
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680248
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    .line 1680249
    iget-object v1, p0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1680250
    check-cast v1, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->getDisplayedChild()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->setDisplayedChild(I)V

    .line 1680251
    iget-object v0, p0, LX/AVl;->g:Landroid/view/View;

    .line 1680252
    iget-object v1, p0, LX/AVl;->f:Landroid/view/View;

    iput-object v1, p0, LX/AVl;->g:Landroid/view/View;

    .line 1680253
    iput-object v0, p0, LX/AVl;->f:Landroid/view/View;

    .line 1680254
    new-instance v1, LX/Ac5;

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->l()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Ac5;-><init>(Ljava/lang/String;)V

    .line 1680255
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->l()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1680256
    iget-object v2, p0, LX/AVl;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b04bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/Ac5;->a(Ljava/lang/String;I)LX/Ac5;

    .line 1680257
    iget-object v0, p0, LX/AVl;->f:Landroid/view/View;

    const v2, 0x7f0d0f67

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastUserTileView;

    .line 1680258
    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastUserTileView;->setParam(LX/Ac5;)V

    .line 1680259
    iget-object v0, p0, LX/AVl;->f:Landroid/view/View;

    const v1, 0x7f0d1940

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1680260
    iget-object v1, p0, LX/AVj;->b:LX/AW2;

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1680261
    iget-object v1, p0, LX/AVj;->b:LX/AW2;

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1680262
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iget-object v5, p0, LX/AVl;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v2, v5}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x1

    const-string v4, " "

    aput-object v4, v3, v2

    const/4 v2, 0x2

    aput-object v1, v3, v2

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1680263
    iget-object v0, p0, LX/AVl;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1680264
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680265
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    iget-object v1, p0, LX/AVl;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->addView(Landroid/view/View;)V

    .line 1680266
    :cond_4
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680267
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->showNext()V

    goto/16 :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1680268
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680269
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->setVisibility(I)V

    .line 1680270
    return-void
.end method
