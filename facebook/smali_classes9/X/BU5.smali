.class public final LX/BU5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field public final synthetic a:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;)V
    .locals 0

    .prologue
    .line 1788256
    iput-object p1, p0, LX/BU5;->a:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1788246
    iget-object v0, p0, LX/BU5;->a:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-virtual {v0, v1}, LX/19w;->a(LX/1A0;)Ljava/util/List;

    move-result-object v1

    .line 1788247
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1788248
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1788249
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 1788250
    iget-object v3, p0, LX/BU5;->a:LX/BUA;

    iget-object v3, v3, LX/BUA;->u:LX/BYc;

    .line 1788251
    iget-object v4, v3, LX/BYc;->c:LX/0yI;

    sget-object v5, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v4, v5}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v4

    move v3, v4

    .line 1788252
    if-nez v3, :cond_0

    iget-object v0, v0, LX/7Jg;->n:LX/2ft;

    sget-object v3, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    if-ne v0, v3, :cond_0

    .line 1788253
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1788254
    :cond_1
    iget-object v0, p0, LX/BU5;->a:LX/BUA;

    invoke-static {v0, v1}, LX/BUA;->b$redex0(LX/BUA;Ljava/util/List;)V

    .line 1788255
    const/4 v0, 0x0

    return-object v0
.end method
