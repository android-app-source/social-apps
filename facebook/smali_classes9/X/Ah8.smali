.class public final LX/Ah8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarSettingQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ae2;

.field public final synthetic b:LX/Ah9;


# direct methods
.method public constructor <init>(LX/Ah9;LX/Ae2;)V
    .locals 0

    .prologue
    .line 1702188
    iput-object p1, p0, LX/Ah8;->b:LX/Ah9;

    iput-object p2, p0, LX/Ah8;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1702189
    iget-object v0, p0, LX/Ah8;->a:LX/Ae2;

    if-eqz v0, :cond_0

    .line 1702190
    iget-object v0, p0, LX/Ah8;->a:LX/Ae2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Ae2;->c(Z)V

    .line 1702191
    :cond_0
    iget-object v0, p0, LX/Ah8;->b:LX/Ah9;

    iget-object v0, v0, LX/Ah9;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Ah9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get tip jar enabled states from video"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1702192
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1702177
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1702178
    if-eqz p1, :cond_0

    .line 1702179
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1702180
    if-nez v0, :cond_2

    .line 1702181
    :cond_0
    iget-object v0, p0, LX/Ah8;->a:LX/Ae2;

    if-eqz v0, :cond_1

    .line 1702182
    iget-object v0, p0, LX/Ah8;->a:LX/Ae2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Ae2;->c(Z)V

    .line 1702183
    :cond_1
    :goto_0
    return-void

    .line 1702184
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1702185
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarSettingQueryModel;

    .line 1702186
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarSettingQueryModel$VideoTipJarSettingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarSettingQueryModel$VideoTipJarSettingModel;->j()Z

    move-result v0

    .line 1702187
    iget-object v1, p0, LX/Ah8;->b:LX/Ah9;

    iget-object v1, v1, LX/Ah9;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecastdisplay/tipjar/LiveVideoTipJarSettingHelper$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveVideoTipJarSettingHelper$1$1;-><init>(LX/Ah8;Z)V

    const v0, 0x38ee743

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
