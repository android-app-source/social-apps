.class public final LX/CaN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917444
    iput-object p1, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1917445
    sget-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->F:Ljava/lang/String;

    const-string v1, "error inflating media gallery"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1917446
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1917447
    check-cast p1, Landroid/view/View;

    .line 1917448
    iget-object v0, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ab:Z

    if-nez v0, :cond_2

    .line 1917449
    check-cast p1, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    .line 1917450
    const/4 v3, 0x0

    .line 1917451
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    new-array v4, v2, [Landroid/view/View;

    move v2, v3

    .line 1917452
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1917453
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v2

    .line 1917454
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1917455
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1917456
    :goto_1
    array-length v2, v4

    if-ge v3, v2, :cond_1

    .line 1917457
    aget-object v2, v4, v3

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1917458
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1917459
    :cond_1
    iget-object v0, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->J:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Landroid/view/View;)V

    .line 1917460
    iget-object v0, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->q(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V

    .line 1917461
    iget-object v0, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->W:LX/9eC;

    if-eqz v0, :cond_2

    .line 1917462
    iget-object v0, p0, LX/CaN;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->W:LX/9eC;

    .line 1917463
    iget-object v1, v0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    const/4 v2, 0x1

    .line 1917464
    iput-boolean v2, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->W:Z

    .line 1917465
    iget-object v1, v0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_WAIT:LX/9eK;

    if-ne v1, v2, :cond_2

    .line 1917466
    iget-object v1, v0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 1917467
    :cond_2
    return-void
.end method
