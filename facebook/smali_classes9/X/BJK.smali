.class public final LX/BJK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Landroid/text/SpannableStringBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BJN;


# direct methods
.method public constructor <init>(LX/BJN;)V
    .locals 0

    .prologue
    .line 1771797
    iput-object p1, p0, LX/BJK;->a:LX/BJN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1771776
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1771777
    if-nez p1, :cond_0

    .line 1771778
    const/4 v0, 0x0

    .line 1771779
    :goto_0
    return-object v0

    .line 1771780
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 1771781
    iget-object v1, p0, LX/BJK;->a:LX/BJN;

    const/16 p1, 0x21

    .line 1771782
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1771783
    iget-object v2, v1, LX/BJN;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a00d1

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 1771784
    if-eqz v0, :cond_1

    .line 1771785
    iget-object v2, v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1771786
    iget-object v5, v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    move-object v5, v5

    .line 1771787
    if-eqz v2, :cond_1

    .line 1771788
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    .line 1771789
    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1771790
    new-instance v2, LX/8zE;

    iget-object v7, v1, LX/BJN;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p0, 0x7f0a074a

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v2, v7}, LX/8zE;-><init>(I)V

    const/4 v7, 0x0

    invoke-virtual {v3, v2, v7, v6, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1771791
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;

    .line 1771792
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1771793
    iget v7, v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;->mOffset:I

    move v7, v7

    .line 1771794
    invoke-virtual {v2}, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;->b()I

    move-result v2

    invoke-virtual {v3, v6, v7, v2, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 1771795
    :cond_1
    move-object v0, v3

    .line 1771796
    goto :goto_0
.end method
