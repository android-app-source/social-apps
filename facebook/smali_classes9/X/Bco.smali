.class public final LX/Bco;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<TTEdge;TTUserInfo;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bcp;


# direct methods
.method public constructor <init>(LX/Bcp;)V
    .locals 0

    .prologue
    .line 1802747
    iput-object p1, p0, LX/Bco;->a:LX/Bcp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1802745
    iget-object v0, p0, LX/Bco;->a:LX/Bcp;

    invoke-static {v0, p1, p4}, LX/Bcp;->b(LX/Bcp;LX/0Px;LX/2kM;)V

    .line 1802746
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1802748
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 1802737
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 1802738
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    invoke-virtual {v0, v1}, LX/2nk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1802739
    iget-object v1, p0, LX/Bco;->a:LX/Bcp;

    monitor-enter v1

    .line 1802740
    :try_start_0
    iget-object v0, p0, LX/Bco;->a:LX/Bcp;

    iget-object v0, v0, LX/Bcp;->d:LX/Bcn;

    if-eqz v0, :cond_0

    .line 1802741
    iget-object v0, p0, LX/Bco;->a:LX/Bcp;

    iget-object v0, v0, LX/Bcp;->d:LX/Bcn;

    invoke-interface {v0}, LX/Bcn;->b()V

    .line 1802742
    :cond_0
    monitor-exit v1

    .line 1802743
    :cond_1
    return-void

    .line 1802744
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1802729
    iget-object v1, p0, LX/Bco;->a:LX/Bcp;

    .line 1802730
    iput-boolean v0, v1, LX/Bcp;->k:Z

    .line 1802731
    iget-object v1, p0, LX/Bco;->a:LX/Bcp;

    monitor-enter v1

    .line 1802732
    :try_start_0
    iget-object v2, p0, LX/Bco;->a:LX/Bcp;

    iget-object v2, v2, LX/Bcp;->d:LX/Bcn;

    if-eqz v2, :cond_1

    .line 1802733
    iget-object v2, p0, LX/Bco;->a:LX/Bcp;

    iget-object v2, v2, LX/Bcp;->d:LX/Bcn;

    iget-object v3, p0, LX/Bco;->a:LX/Bcp;

    iget-object v3, v3, LX/Bcp;->a:LX/2kW;

    .line 1802734
    iget-object p0, v3, LX/2kW;->o:LX/2kM;

    move-object v3, p0

    .line 1802735
    invoke-interface {v3}, LX/2kM;->c()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v2, v0, p4}, LX/Bcn;->a(ZLjava/lang/Throwable;)V

    .line 1802736
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1802723
    iget-object v1, p0, LX/Bco;->a:LX/Bcp;

    monitor-enter v1

    .line 1802724
    :try_start_0
    iget-object v2, p0, LX/Bco;->a:LX/Bcp;

    const/4 v3, 0x0

    .line 1802725
    iput-boolean v3, v2, LX/Bcp;->k:Z

    .line 1802726
    iget-object v2, p0, LX/Bco;->a:LX/Bcp;

    iget-object v2, v2, LX/Bcp;->d:LX/Bcn;

    if-eqz v2, :cond_1

    .line 1802727
    iget-object v2, p0, LX/Bco;->a:LX/Bcp;

    iget-object v2, v2, LX/Bcp;->d:LX/Bcn;

    iget-object v3, p0, LX/Bco;->a:LX/Bcp;

    iget v3, v3, LX/Bcp;->j:I

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v2, v0}, LX/Bcn;->a(Z)V

    .line 1802728
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
