.class public final LX/BmD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Z)V
    .locals 0

    .prologue
    .line 1817990
    iput-object p1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iput-boolean p2, p0, LX/BmD;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2da9e4c2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1817991
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, p0, LX/BmD;->a:Z

    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/calendar/CalendarView;->setIsSelectingSecondDate(Z)V

    .line 1817992
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-boolean v2, p0, LX/BmD;->a:Z

    iput-boolean v2, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->A:Z

    .line 1817993
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1817994
    iget-boolean v2, p0, LX/BmD;->a:Z

    if-eqz v2, :cond_1

    .line 1817995
    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0a010e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1817996
    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1817997
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 1817998
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v3, v3, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1817999
    invoke-static {v1, v2, v3}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;II)V

    .line 1818000
    :cond_0
    :goto_0
    const v1, -0x61e802d8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1818001
    :cond_1
    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1818002
    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0a010e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1818003
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 1818004
    iget-object v1, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, LX/BmD;->b:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v3, v3, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1818005
    invoke-static {v1, v2, v3}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;II)V

    .line 1818006
    goto :goto_0
.end method
