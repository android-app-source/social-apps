.class public final LX/CJ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;",
        ">;",
        "Lcom/facebook/messaging/model/attribution/ContentAppAttribution;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

.field public final synthetic b:LX/CJ5;


# direct methods
.method public constructor <init>(LX/CJ5;Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)V
    .locals 0

    .prologue
    .line 1875042
    iput-object p1, p0, LX/CJ4;->b:LX/CJ5;

    iput-object p2, p0, LX/CJ4;->a:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1875043
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1875044
    iget-object v2, p0, LX/CJ4;->a:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1875045
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1875046
    check-cast v0, Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;

    .line 1875047
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/5dd;->a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)LX/5dd;

    move-result-object p0

    .line 1875048
    invoke-static {p0, v0}, LX/3N1;->a(LX/5dd;Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$AppAttributionInfoModel;)LX/5dd;

    move-result-object p0

    invoke-virtual {p0}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object p0

    move-object v0, p0

    .line 1875049
    return-object v0
.end method
