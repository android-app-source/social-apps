.class public LX/C4l;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4l",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847834
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847835
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C4l;->b:LX/0Zi;

    .line 1847836
    iput-object p1, p0, LX/C4l;->a:LX/0Ot;

    .line 1847837
    return-void
.end method

.method public static a(LX/0QB;)LX/C4l;
    .locals 4

    .prologue
    .line 1847838
    const-class v1, LX/C4l;

    monitor-enter v1

    .line 1847839
    :try_start_0
    sget-object v0, LX/C4l;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847840
    sput-object v2, LX/C4l;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847841
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847842
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847843
    new-instance v3, LX/C4l;

    const/16 p0, 0x1f4b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C4l;-><init>(LX/0Ot;)V

    .line 1847844
    move-object v0, v3

    .line 1847845
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847846
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847847
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1847849
    check-cast p2, LX/C4k;

    .line 1847850
    iget-object v0, p0, LX/C4l;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;

    iget-object v1, p2, LX/C4k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C4k;->b:LX/1Ps;

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1Dg;

    move-result-object v0

    .line 1847851
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847852
    invoke-static {}, LX/1dS;->b()V

    .line 1847853
    const/4 v0, 0x0

    return-object v0
.end method
