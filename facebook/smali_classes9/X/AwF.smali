.class public LX/AwF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0y3;


# direct methods
.method public constructor <init>(LX/0y3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1725211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1725212
    iput-object p1, p0, LX/AwF;->a:LX/0y3;

    .line 1725213
    return-void
.end method

.method public static a(LX/AwF;Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1725216
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    move-result-object v4

    .line 1725217
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1725218
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setTrackingString(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725219
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1725220
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setThumbnailUri(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725221
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v1

    .line 1725222
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1725223
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1725224
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->a()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setFrame(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725225
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->j()Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    .line 1725226
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->m()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1725227
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->p()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    move-result-object v5

    .line 1725228
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1725229
    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$InstructionsModel;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->m()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$InstructionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$InstructionsModel;->a()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v6, v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->setInstructionText(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionTextModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionTextModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionTextModel;->a()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v6, v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->setAttributionText(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionThumbnailModel;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionThumbnailModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel$AttributionThumbnailModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_3
    invoke-virtual {v6, v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->setAttributionThumbnail(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setMask(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725230
    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->l()Z

    move-result v2

    or-int/2addr v1, v2

    .line 1725231
    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;->n()Z

    move-result v2

    or-int/2addr v0, v2

    .line 1725232
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->q()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v2

    .line 1725233
    if-eqz v2, :cond_3

    .line 1725234
    invoke-static {v2}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;)Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setParticleEffect(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725235
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->k()Z

    move-result v5

    or-int/2addr v1, v5

    .line 1725236
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->l()Z

    move-result v2

    or-int/2addr v0, v2

    .line 1725237
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->r()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v2

    .line 1725238
    if-eqz v2, :cond_4

    .line 1725239
    invoke-static {v2}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setShaderFilter(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725240
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->s()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v2

    .line 1725241
    if-eqz v2, :cond_5

    .line 1725242
    invoke-static {v2}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setStyleTransfer(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725243
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->l()Z

    move-result v5

    or-int/2addr v1, v5

    .line 1725244
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->o()Z

    move-result v2

    or-int/2addr v0, v2

    .line 1725245
    :cond_5
    invoke-virtual {v4, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->setIsLoggingDisabled(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1725246
    if-eqz v1, :cond_6

    .line 1725247
    iget-object v0, p0, LX/AwF;->a:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1725248
    if-eqz v0, :cond_7

    .line 1725249
    :cond_6
    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v3

    .line 1725250
    :cond_7
    return-object v3

    :cond_8
    move-object v2, v3

    .line 1725251
    goto/16 :goto_1

    :cond_9
    move-object v2, v3

    goto/16 :goto_2

    :cond_a
    move-object v2, v3

    goto :goto_3

    :cond_b
    move v1, v0

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static b(LX/0QB;)LX/AwF;
    .locals 2

    .prologue
    .line 1725214
    new-instance v1, LX/AwF;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-direct {v1, v0}, LX/AwF;-><init>(LX/0y3;)V

    .line 1725215
    return-object v1
.end method
