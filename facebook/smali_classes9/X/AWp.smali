.class public final LX/AWp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:LX/AWy;


# direct methods
.method public constructor <init>(LX/AWy;)V
    .locals 0

    .prologue
    .line 1683407
    iput-object p1, p0, LX/AWp;->a:LX/AWy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 1683408
    iget-object v0, p0, LX/AWp;->a:LX/AWy;

    iget v0, v0, LX/AWy;->E:I

    if-gez v0, :cond_0

    .line 1683409
    iget-object v0, p0, LX/AWp;->a:LX/AWy;

    iget-object v1, p0, LX/AWp;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->z:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, LX/AWp;->a:LX/AWy;

    iget-object v2, v2, LX/AWy;->z:Landroid/widget/RelativeLayout;

    invoke-static {v2}, LX/AWy;->b(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, LX/AWp;->a:LX/AWy;

    iget-object v2, v2, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-static {v2}, LX/AWy;->b(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1683410
    iput v1, v0, LX/AWy;->E:I

    .line 1683411
    :cond_0
    iget-object v0, p0, LX/AWp;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 1683412
    iget-object v1, p0, LX/AWp;->a:LX/AWy;

    iget v1, v1, LX/AWy;->B:I

    if-ne v0, v1, :cond_1

    .line 1683413
    :goto_0
    return-void

    .line 1683414
    :cond_1
    iget-object v1, p0, LX/AWp;->a:LX/AWy;

    .line 1683415
    iput v0, v1, LX/AWy;->B:I

    .line 1683416
    iget-object v0, p0, LX/AWp;->a:LX/AWy;

    iget v0, v0, LX/AWy;->B:I

    iget-object v1, p0, LX/AWp;->a:LX/AWy;

    iget v1, v1, LX/AWy;->E:I

    sub-int/2addr v0, v1

    .line 1683417
    iget-object v1, p0, LX/AWp;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setMaxHeight(I)V

    goto :goto_0
.end method
