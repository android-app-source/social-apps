.class public LX/Bqw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Qt;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:J

.field private static j:LX/0Xm;


# instance fields
.field private final c:LX/1VF;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/2Pb;

.field private final f:LX/0W3;

.field public final g:LX/03V;

.field public final h:LX/00H;

.field public final i:LX/2Pc;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1825569
    sget-wide v0, LX/0X5;->l:J

    sput-wide v0, LX/Bqw;->b:J

    .line 1825570
    sget-object v0, LX/1Qt;->PERMALINK:LX/1Qt;

    const-string v1, "permalink"

    sget-object v2, LX/1Qt;->PAGE_TIMELINE:LX/1Qt;

    const-string v3, "new_timeline"

    sget-object v4, LX/1Qt;->FEED:LX/1Qt;

    const-string v5, "story"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/Bqw;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/1VF;Lcom/facebook/content/SecureContextHelper;LX/2Pb;LX/0W3;LX/03V;LX/00H;LX/2Pc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825572
    iput-object p1, p0, LX/Bqw;->c:LX/1VF;

    .line 1825573
    iput-object p2, p0, LX/Bqw;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1825574
    iput-object p3, p0, LX/Bqw;->e:LX/2Pb;

    .line 1825575
    iput-object p4, p0, LX/Bqw;->f:LX/0W3;

    .line 1825576
    iput-object p5, p0, LX/Bqw;->g:LX/03V;

    .line 1825577
    iput-object p6, p0, LX/Bqw;->h:LX/00H;

    .line 1825578
    iput-object p7, p0, LX/Bqw;->i:LX/2Pc;

    .line 1825579
    return-void
.end method

.method private static a(LX/Bqw;Lcom/facebook/graphql/model/GraphQLStory;I)I
    .locals 1

    .prologue
    .line 1825580
    invoke-static {p1}, LX/Bqw;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825581
    const v0, 0x7f0810bc

    .line 1825582
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f080148

    if-ne p2, v0, :cond_1

    const v0, 0x7f080148

    goto :goto_0

    :cond_1
    const v0, 0x7f080146

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Bqw;
    .locals 11

    .prologue
    .line 1825583
    const-class v1, LX/Bqw;

    monitor-enter v1

    .line 1825584
    :try_start_0
    sget-object v0, LX/Bqw;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825585
    sput-object v2, LX/Bqw;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825586
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825587
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825588
    new-instance v3, LX/Bqw;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v4

    check-cast v4, LX/1VF;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v6

    check-cast v6, LX/2Pb;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const-class v9, LX/00H;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/00H;

    invoke-static {v0}, LX/2Pc;->a(LX/0QB;)LX/2Pc;

    move-result-object v10

    check-cast v10, LX/2Pc;

    invoke-direct/range {v3 .. v10}, LX/Bqw;-><init>(LX/1VF;Lcom/facebook/content/SecureContextHelper;LX/2Pb;LX/0W3;LX/03V;LX/00H;LX/2Pc;)V

    .line 1825589
    move-object v0, v3

    .line 1825590
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825591
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825592
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/Br2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pr;",
            ")",
            "LX/Br2;"
        }
    .end annotation

    .prologue
    .line 1825594
    new-instance v1, LX/Br1;

    .line 1825595
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825596
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, v0}, LX/Br1;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-static {p0}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br2;

    return-object v0
.end method

.method public static b(LX/Bqw;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1825597
    invoke-static {p2}, LX/Bqw;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1825598
    invoke-static {p2}, LX/Bqw;->e(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    :cond_0
    move-object v0, p1

    move v1, p4

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1825599
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    :cond_2
    invoke-static {p1, p4, v0, v4, p3}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1825600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x403827a

    if-ne v0, v1, :cond_0

    .line 1825601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    .line 1825602
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1825568
    invoke-static {p0}, LX/Bqw;->e(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const v0, 0x390b53d6

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1825496
    invoke-static {p0}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;Z)I
    .locals 2

    .prologue
    .line 1825547
    invoke-static {p1}, LX/Bqw;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825548
    sget-object v0, LX/Bqt;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result p3

    aget v0, v0, p3

    packed-switch v0, :pswitch_data_0

    .line 1825549
    :pswitch_0
    invoke-static {p1}, LX/Bqw;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080149

    :goto_0
    move v0, v0

    .line 1825550
    :goto_1
    return v0

    .line 1825551
    :cond_0
    sget-object v0, LX/Bqt;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1825552
    invoke-static {p1}, LX/Bqw;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f080149

    :goto_2
    move v0, v0

    .line 1825553
    goto :goto_1

    .line 1825554
    :pswitch_1
    const v0, 0x7f0810bc

    goto :goto_0

    .line 1825555
    :pswitch_2
    const v0, 0x7f0810c0

    goto :goto_0

    .line 1825556
    :pswitch_3
    const v0, 0x7f0810bd

    goto :goto_0

    .line 1825557
    :pswitch_4
    const v0, 0x7f0810be

    goto :goto_0

    .line 1825558
    :pswitch_5
    const v0, 0x7f0810bf

    goto :goto_0

    .line 1825559
    :pswitch_6
    const v0, 0x7f0810c1

    goto :goto_0

    .line 1825560
    :pswitch_7
    const v0, 0x7f0810c2

    goto :goto_0

    .line 1825561
    :cond_1
    const v0, 0x7f080145

    goto :goto_0

    .line 1825562
    :pswitch_8
    if-eqz p3, :cond_2

    const v0, 0x7f080148

    goto :goto_2

    :cond_2
    const v0, 0x7f080146

    goto :goto_2

    .line 1825563
    :pswitch_9
    const v0, 0x7f0810b9

    goto :goto_2

    .line 1825564
    :pswitch_a
    const v0, 0x7f0810b8

    goto :goto_2

    .line 1825565
    :pswitch_b
    const v0, 0x7f0810b6

    goto :goto_2

    .line 1825566
    :pswitch_c
    const v0, 0x7f0810b7

    goto :goto_2

    .line 1825567
    :cond_3
    const v0, 0x7f080145

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method public final a(ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/style/ClickableSpan;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/Br2;",
            "LX/1Pq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/text/style/ClickableSpan;"
        }
    .end annotation

    .prologue
    .line 1825544
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v0

    .line 1825545
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825546
    new-instance v0, LX/Bqq;

    move-object v1, p0

    move-object v3, p2

    move v4, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/Bqq;-><init>(LX/Bqw;Lcom/facebook/graphql/model/GraphQLStory;LX/Br2;ZLX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;LX/Br2;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1825503
    invoke-static {p2}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-eq v0, v1, :cond_0

    invoke-static {p2}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v0, v1, :cond_3

    :cond_0
    invoke-static {p2}, LX/Bqw;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1825504
    if-eqz v0, :cond_1

    .line 1825505
    iget-object v0, p3, LX/Br2;->a:Ljava/lang/Integer;

    move-object v0, v0

    .line 1825506
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p1, p2, p4, v0}, LX/Bqw;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)V

    .line 1825507
    :goto_1
    return-void

    .line 1825508
    :cond_1
    invoke-static {p2}, LX/Bqw;->h(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1825509
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v1

    .line 1825510
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v1

    :goto_2
    move-object v1, v1

    .line 1825511
    if-nez v1, :cond_4

    .line 1825512
    :goto_3
    goto :goto_1

    .line 1825513
    :cond_2
    iget-object v0, p3, LX/Br2;->a:Ljava/lang/Integer;

    move-object v0, v0

    .line 1825514
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, p2, v0}, LX/Bqw;->a(LX/Bqw;Lcom/facebook/graphql/model/GraphQLStory;I)I

    move-result v0

    .line 1825515
    invoke-static {p0, p1, p2, p4, v0}, LX/Bqw;->b(LX/Bqw;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 1825516
    iget-object v2, p0, LX/Bqw;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1825517
    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1825518
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v6

    .line 1825519
    invoke-static {p2}, LX/Bqw;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v3, LX/8wV;->BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

    .line 1825520
    :goto_4
    const/4 v4, 0x0

    .line 1825521
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1825522
    const-string v2, "post_id"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825523
    const-string v7, "page_id"

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    :goto_5
    invoke-interface {v5, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825524
    const-string v2, "placement"

    invoke-interface {v5, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825525
    invoke-static {p2}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {p2}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 1825526
    :goto_6
    const-string v7, "ineligible_reason"

    invoke-interface {v5, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825527
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1825528
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object v7

    .line 1825529
    if-eqz v7, :cond_b

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->j()Lcom/facebook/graphql/model/GraphQLError;

    move-result-object v2

    .line 1825530
    :goto_7
    if-eqz v2, :cond_6

    .line 1825531
    const-string v8, "error_code"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLError;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825532
    const-string v8, "error_type"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLError;->k()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825533
    const-string v8, "error_description"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLError;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825534
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->a()Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->j()Ljava/lang/String;

    move-result-object v4

    .line 1825535
    :cond_5
    const-string v2, "link"

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825536
    :cond_6
    const-string v2, "flow"

    const-string v4, "ineligible"

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825537
    move-object v4, v5

    .line 1825538
    iget-object v2, p0, LX/Bqw;->e:LX/2Pb;

    sget-object v5, LX/8wW;->EVENT_CREATE_DIALOG_OPEN:LX/8wW;

    iget-object v7, p0, LX/Bqw;->g:LX/03V;

    invoke-virtual {v2, v3, v5, v4, v7}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/util/Map;LX/03V;)V

    .line 1825539
    new-instance v2, LX/31Y;

    invoke-direct {v2, p1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v7

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    new-instance v1, LX/Bqs;

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, LX/Bqs;-><init>(LX/Bqw;LX/8wV;Ljava/util/Map;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;)V

    invoke-virtual {v7, v8, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080147

    new-instance v5, LX/Bqr;

    invoke-direct {v5, p0, v3, v4}, LX/Bqr;-><init>(LX/Bqw;LX/8wV;Ljava/util/Map;)V

    invoke-virtual {v1, v2, v5}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    goto/16 :goto_3

    .line 1825540
    :cond_7
    sget-object v3, LX/8wV;->BOOSTED_POST_MOBILE_MODULE:LX/8wV;

    goto/16 :goto_4

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_9
    move-object v2, v4

    .line 1825541
    goto/16 :goto_5

    :cond_a
    move-object v2, v4

    .line 1825542
    goto/16 :goto_6

    :cond_b
    move-object v2, v4

    .line 1825543
    goto/16 :goto_7
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1825499
    const-string v0, "post_insights"

    invoke-static {p0, p2, p4}, LX/Bqw;->a(LX/Bqw;Lcom/facebook/graphql/model/GraphQLStory;I)I

    move-result v1

    invoke-static {p0, p1, p2, v0, v1}, LX/Bqw;->b(LX/Bqw;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1825500
    const v1, 0x7f080145

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, p3, v2, v0}, LX/8wJ;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 1825501
    iget-object v1, p0, LX/Bqw;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1825502
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1825498
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-static {p1}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Bqw;->i:LX/2Pc;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Pc;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Bqw;->f:LX/0W3;

    sget-wide v2, LX/Bqw;->b:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    .line 1825497
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bqw;->i:LX/2Pc;

    iget-object v1, p0, LX/Bqw;->i:LX/2Pc;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Pc;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Pc;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
