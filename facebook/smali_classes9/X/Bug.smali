.class public LX/Bug;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/widget/RelativeLayout$LayoutParams;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AjP;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field public e:LX/2pa;

.field public f:LX/3FT;

.field public g:LX/3FT;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/3It;

.field private j:LX/Bud;

.field private k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

.field private l:Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

.field public m:LX/AjN;

.field public n:LX/3HY;


# direct methods
.method public constructor <init>(LX/0Ot;LX/Bud;Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AjP;",
            ">;",
            "LX/Bud;",
            "Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;",
            "Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1831432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1831433
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Bug;->c:Z

    .line 1831434
    sget-object v0, LX/3HY;->REGULAR:LX/3HY;

    iput-object v0, p0, LX/Bug;->n:LX/3HY;

    .line 1831435
    iput-object p1, p0, LX/Bug;->b:LX/0Ot;

    .line 1831436
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, LX/Bug;->a:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1831437
    iput-object p2, p0, LX/Bug;->j:LX/Bud;

    .line 1831438
    iput-object p3, p0, LX/Bug;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    .line 1831439
    iput-object p4, p0, LX/Bug;->l:Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

    .line 1831440
    return-void
.end method

.method public static a(LX/0QB;)LX/Bug;
    .locals 1

    .prologue
    .line 1831441
    invoke-static {p0}, LX/Bug;->b(LX/0QB;)LX/Bug;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Bug;
    .locals 7

    .prologue
    .line 1831442
    new-instance v3, LX/Bug;

    const/16 v0, 0x1c86

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v0, LX/Bud;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Bud;

    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->b(LX/0QB;)Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    .line 1831443
    new-instance v6, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-direct {v6, v2, v5}, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;-><init>(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 1831444
    move-object v2, v6

    .line 1831445
    check-cast v2, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

    invoke-direct {v3, v4, v0, v1, v2}, LX/Bug;-><init>(LX/0Ot;LX/Bud;Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;)V

    .line 1831446
    return-object v3
.end method


# virtual methods
.method public final a(LX/3FT;LX/3FT;LX/2pa;LX/Bue;LX/7Lf;Ljava/lang/Boolean;)Lcom/facebook/video/player/RichVideoPlayer;
    .locals 5
    .param p5    # LX/7Lf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1831447
    iput-object p1, p0, LX/Bug;->f:LX/3FT;

    .line 1831448
    iput-object p2, p0, LX/Bug;->g:LX/3FT;

    .line 1831449
    iput-object p3, p0, LX/Bug;->e:LX/2pa;

    .line 1831450
    iget-object v0, p0, LX/Bug;->j:LX/Bud;

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/Bud;->a(Z)Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

    move-result-object v2

    .line 1831451
    const/4 v1, 0x0

    .line 1831452
    if-eqz p1, :cond_4

    iget-boolean v0, p0, LX/Bug;->c:Z

    if-nez v0, :cond_4

    .line 1831453
    invoke-interface {p1}, LX/3FT;->c()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1831454
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->p()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1831455
    :goto_0
    if-eqz v0, :cond_1

    .line 1831456
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Bug;->d:Z

    .line 1831457
    iget-object v1, p0, LX/Bug;->a:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1831458
    invoke-interface {p2, v0}, LX/3FT;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1831459
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 1831460
    if-nez v1, :cond_5

    sget-object v1, LX/3HY;->REGULAR:LX/3HY;

    :goto_1
    move-object v1, v1

    .line 1831461
    iput-object v1, p0, LX/Bug;->n:LX/3HY;

    .line 1831462
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/Bug;->h:Ljava/util/List;

    .line 1831463
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    move-object v1, v1

    .line 1831464
    iput-object v1, p0, LX/Bug;->i:LX/3It;

    .line 1831465
    iget-object v1, p0, LX/Bug;->e:LX/2pa;

    iget-object v1, v1, LX/2pa;->b:LX/0P1;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Bug;->e:LX/2pa;

    iget-object v1, v1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v1, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Bug;->e:LX/2pa;

    iget-object v1, v1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v1, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v1, :cond_6

    .line 1831466
    :cond_0
    :goto_2
    sget-object v1, LX/Bue;->WATCH_AND_BROWSE:LX/Bue;

    if-ne p4, v1, :cond_2

    .line 1831467
    iget-object v1, p0, LX/Bug;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    invoke-virtual {v1, v0, p3, p5}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1831468
    :goto_3
    invoke-interface {p2}, LX/3FT;->getPlayerType()LX/04G;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1831469
    return-object v0

    .line 1831470
    :cond_1
    iput-boolean v4, p0, LX/Bug;->d:Z

    .line 1831471
    invoke-interface {p2}, LX/3FT;->d()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1831472
    const-string v1, "Not reusing player but no default player provided."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1831473
    invoke-virtual {v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    goto :goto_2

    .line 1831474
    :cond_2
    sget-object v1, LX/Bue;->WATCH_IN_CANVAS:LX/Bue;

    if-ne p4, v1, :cond_3

    .line 1831475
    iget-object v1, p0, LX/Bug;->l:Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

    invoke-virtual {v1, v0, p3, p5}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    goto :goto_3

    .line 1831476
    :cond_3
    invoke-virtual {v2, v0, p3, p5}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 1831477
    :cond_5
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v1, v1

    .line 1831478
    invoke-virtual {v1}, LX/2pa;->f()LX/3HY;

    move-result-object v1

    goto :goto_1

    .line 1831479
    :cond_6
    iget-object v1, p0, LX/Bug;->e:LX/2pa;

    iget-object v1, v1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v1, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831480
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1831481
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831482
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1831483
    iget-object v3, p0, LX/Bug;->m:LX/AjN;

    if-nez v3, :cond_7

    .line 1831484
    new-instance v3, LX/Buf;

    invoke-direct {v3, p0}, LX/Buf;-><init>(LX/Bug;)V

    iput-object v3, p0, LX/Bug;->m:LX/AjN;

    .line 1831485
    iget-object v3, p0, LX/Bug;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AjP;

    iget-object v4, p0, LX/Bug;->m:LX/AjN;

    .line 1831486
    iput-object v4, v3, LX/AjP;->c:LX/AjN;

    .line 1831487
    :cond_7
    iget-object v3, p0, LX/Bug;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AjP;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    goto :goto_2
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1831488
    iget-boolean v0, p0, LX/Bug;->d:Z

    if-eqz v0, :cond_1

    .line 1831489
    const/4 v5, 0x0

    .line 1831490
    iget-object v0, p0, LX/Bug;->g:LX/3FT;

    invoke-interface {v0}, LX/3FT;->c()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1831491
    iget-object v2, p0, LX/Bug;->e:LX/2pa;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Bug;->e:LX/2pa;

    invoke-virtual {v2}, LX/2pa;->f()LX/3HY;

    move-result-object v2

    iget-object v3, p0, LX/Bug;->n:LX/3HY;

    if-eq v2, v3, :cond_0

    .line 1831492
    iget-object v2, p0, LX/Bug;->e:LX/2pa;

    invoke-virtual {v2}, LX/2pa;->g()LX/2pZ;

    move-result-object v2

    const-string v3, "VideoPlayerViewSizeKey"

    iget-object v4, p0, LX/Bug;->n:LX/3HY;

    invoke-virtual {v2, v3, v4}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v2

    invoke-virtual {v2}, LX/2pZ;->b()LX/2pa;

    move-result-object v2

    iput-object v2, p0, LX/Bug;->e:LX/2pa;

    .line 1831493
    :cond_0
    iget-object v2, p0, LX/Bug;->f:LX/3FT;

    invoke-interface {v2}, LX/3FT;->getPlayerType()LX/04G;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1831494
    iget-object v2, p0, LX/Bug;->i:LX/3It;

    .line 1831495
    iput-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1831496
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    .line 1831497
    iget-object v2, p0, LX/Bug;->h:Ljava/util/List;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    .line 1831498
    iget-object v2, p0, LX/Bug;->e:LX/2pa;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1831499
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1831500
    iget-object v2, p0, LX/Bug;->f:LX/3FT;

    invoke-interface {v2, v0}, LX/3FT;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1831501
    iget-object v0, p0, LX/Bug;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1831502
    iput-object v5, p0, LX/Bug;->h:Ljava/util/List;

    .line 1831503
    iput-object v5, p0, LX/Bug;->i:LX/3It;

    .line 1831504
    iget-object v0, p0, LX/Bug;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AjP;

    invoke-virtual {v0}, LX/AjP;->b()V

    .line 1831505
    :goto_0
    iput-object v1, p0, LX/Bug;->f:LX/3FT;

    .line 1831506
    iput-object v1, p0, LX/Bug;->g:LX/3FT;

    .line 1831507
    iput-object v1, p0, LX/Bug;->e:LX/2pa;

    .line 1831508
    return-void

    .line 1831509
    :cond_1
    iget-object v0, p0, LX/Bug;->g:LX/3FT;

    invoke-interface {v0}, LX/3FT;->d()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1831510
    const-string v2, "Not reusing player but no default player provided."

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1831511
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1831512
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1831513
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    .line 1831514
    goto :goto_0
.end method
