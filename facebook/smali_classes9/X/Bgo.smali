.class public LX/Bgo;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/resources/ui/FbButton;

.field public c:Lcom/facebook/resources/ui/FbButton;

.field public final d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1808496
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808497
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bgo;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1808498
    const-class v0, LX/Bgo;

    invoke-static {v0, p0}, LX/Bgo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1808499
    iget-object v0, p0, LX/Bgo;->a:LX/0W3;

    sget-wide v2, LX/0X5;->hv:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1808500
    :goto_0
    const v0, 0x7f0d2672

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Bgo;->b:Lcom/facebook/resources/ui/FbButton;

    .line 1808501
    const v0, 0x7f0d2671

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Bgo;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1808502
    return-void

    .line 1808503
    :pswitch_0
    const v0, 0x7f030ff7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    goto :goto_0

    .line 1808504
    :pswitch_1
    const v0, 0x7f030ff8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808505
    const v0, 0x7f0d2673

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Bgo;->c:Lcom/facebook/resources/ui/FbButton;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bgo;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    iput-object p0, p1, LX/Bgo;->a:LX/0W3;

    return-void
.end method
