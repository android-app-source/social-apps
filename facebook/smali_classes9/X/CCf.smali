.class public final LX/CCf;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2v2;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/2v2;


# direct methods
.method public constructor <init>(LX/2v2;)V
    .locals 1

    .prologue
    .line 1858165
    iput-object p1, p0, LX/CCf;->d:LX/2v2;

    .line 1858166
    move-object v0, p1

    .line 1858167
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1858168
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1858169
    const-string v0, "CreateStorylineVideoCallToActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1858170
    if-ne p0, p1, :cond_1

    .line 1858171
    :cond_0
    :goto_0
    return v0

    .line 1858172
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1858173
    goto :goto_0

    .line 1858174
    :cond_3
    check-cast p1, LX/CCf;

    .line 1858175
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1858176
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1858177
    if-eq v2, v3, :cond_0

    .line 1858178
    iget-object v2, p0, LX/CCf;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CCf;->a:Ljava/lang/String;

    iget-object v3, p1, LX/CCf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1858179
    goto :goto_0

    .line 1858180
    :cond_5
    iget-object v2, p1, LX/CCf;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1858181
    :cond_6
    iget-object v2, p0, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1858182
    goto :goto_0

    .line 1858183
    :cond_8
    iget-object v2, p1, LX/CCf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1858184
    :cond_9
    iget-object v2, p0, LX/CCf;->c:LX/1Pf;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CCf;->c:LX/1Pf;

    iget-object v3, p1, LX/CCf;->c:LX/1Pf;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1858185
    goto :goto_0

    .line 1858186
    :cond_a
    iget-object v2, p1, LX/CCf;->c:LX/1Pf;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
