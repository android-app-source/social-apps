.class public abstract LX/Ag8;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Hc;


# direct methods
.method public constructor <init>(LX/3Hc;)V
    .locals 0

    .prologue
    .line 1700318
    iput-object p1, p0, LX/Ag8;->a:LX/3Hc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1700319
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->c:LX/03V;

    sget-object v1, LX/3Hc;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "video broadcast poll failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v3, v3, LX/3Hc;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1700320
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->k:LX/0Zc;

    iget-object v1, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v1, v1, LX/3Hc;->m:Ljava/lang/String;

    sget-object v2, LX/7IM;->LIVE_POLLER_FAIL:LX/7IM;

    const-string v3, "Live status polling failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700321
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    const-string v1, "error"

    .line 1700322
    iput-object v1, v0, LX/3Hc;->t:Ljava/lang/String;

    .line 1700323
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    invoke-static {v0}, LX/3Hc;->h(LX/3Hc;)V

    .line 1700324
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700325
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1700326
    if-eqz p1, :cond_0

    .line 1700327
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700328
    if-nez v0, :cond_2

    .line 1700329
    :cond_0
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_1

    .line 1700330
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    iget-object v1, p0, LX/Ag8;->a:LX/3Hc;

    iget-object v1, v1, LX/3Hc;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/3Ha;->a(Ljava/lang/String;)V

    .line 1700331
    :cond_1
    :goto_0
    return-void

    .line 1700332
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700333
    invoke-virtual {p0, v0}, LX/Ag8;->a(Ljava/lang/Object;)V

    .line 1700334
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    const-string v1, "success"

    .line 1700335
    iput-object v1, v0, LX/3Hc;->t:Ljava/lang/String;

    .line 1700336
    iget-object v0, p0, LX/Ag8;->a:LX/3Hc;

    invoke-static {v0}, LX/3Hc;->h(LX/3Hc;)V

    goto :goto_0
.end method
