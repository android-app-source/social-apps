.class public final LX/Bds;
.super LX/BcH;
.source ""


# instance fields
.field private final a:LX/BcG;

.field private final b:LX/1De;


# direct methods
.method public constructor <init>(LX/BcG;LX/1De;)V
    .locals 0

    .prologue
    .line 1804308
    invoke-direct {p0}, LX/BcH;-><init>()V

    .line 1804309
    iput-object p1, p0, LX/Bds;->a:LX/BcG;

    .line 1804310
    iput-object p2, p0, LX/Bds;->b:LX/1De;

    .line 1804311
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1804288
    iget-object v0, p0, LX/Bds;->b:LX/1De;

    sget-object v1, LX/BcL;->LOADING:LX/BcL;

    .line 1804289
    iget-object v2, v0, LX/1De;->g:LX/1X1;

    move-object v2, v2

    .line 1804290
    if-nez v2, :cond_1

    .line 1804291
    :goto_0
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    if-eqz v0, :cond_0

    .line 1804292
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    invoke-interface {v0}, LX/BcG;->a()V

    .line 1804293
    :cond_0
    return-void

    .line 1804294
    :cond_1
    check-cast v2, LX/Bdm;

    .line 1804295
    new-instance v3, LX/Bdp;

    invoke-direct {v3, v1}, LX/Bdp;-><init>(LX/BcL;)V

    move-object v3, v3

    .line 1804296
    move-object v2, v3

    .line 1804297
    invoke-virtual {v0, v2}, LX/1De;->b(LX/48B;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1804303
    iget-object v0, p0, LX/Bds;->b:LX/1De;

    sget-object v1, LX/BcL;->SUCCEEDED:LX/BcL;

    invoke-static {v0, v1, p1}, LX/Bdq;->a(LX/1De;LX/BcL;Z)V

    .line 1804304
    iget-object v0, p0, LX/Bds;->b:LX/1De;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Bdq;->a(LX/1De;Z)V

    .line 1804305
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    if-eqz v0, :cond_0

    .line 1804306
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    invoke-interface {v0, p1}, LX/BcG;->a(Z)V

    .line 1804307
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1804298
    iget-object v0, p0, LX/Bds;->b:LX/1De;

    sget-object v1, LX/BcL;->FAILED:LX/BcL;

    invoke-static {v0, v1, p1}, LX/Bdq;->a(LX/1De;LX/BcL;Z)V

    .line 1804299
    iget-object v0, p0, LX/Bds;->b:LX/1De;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Bdq;->a(LX/1De;Z)V

    .line 1804300
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    if-eqz v0, :cond_0

    .line 1804301
    iget-object v0, p0, LX/Bds;->a:LX/BcG;

    invoke-interface {v0, p1}, LX/BcG;->b(Z)V

    .line 1804302
    :cond_0
    return-void
.end method
