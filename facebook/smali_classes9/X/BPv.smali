.class public final LX/BPv;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;)V
    .locals 0

    .prologue
    .line 1781018
    iput-object p1, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1781019
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;B)V
    .locals 0

    .prologue
    .line 1781007
    invoke-direct {p0, p1}, LX/BPv;-><init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;)V

    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1781010
    :try_start_0
    iget-object v1, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget-object v1, v1, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->c:Ljava/lang/String;

    iget-object v2, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v2, v2, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    iget-object v3, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v3, v3, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    invoke-static {v1, v2, v3}, LX/2Qx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch LX/430; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/42x; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1781011
    :goto_0
    return-object v0

    .line 1781012
    :catch_0
    iget-object v1, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    const-string v2, "out of memory"

    .line 1781013
    invoke-static {v1, v2}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;Ljava/lang/String;)V

    .line 1781014
    goto :goto_0

    .line 1781015
    :catch_1
    iget-object v1, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    const-string v2, "decode failed"

    .line 1781016
    invoke-static {v1, v2}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;Ljava/lang/String;)V

    .line 1781017
    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1781020
    if-nez p1, :cond_0

    .line 1781021
    iget-object v0, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0815ba

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1781022
    :goto_0
    return-void

    .line 1781023
    :cond_0
    iget-object v0, p0, LX/BPv;->a:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    .line 1781024
    invoke-static {v0, p1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;Landroid/graphics/Bitmap;)V

    .line 1781025
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1781009
    invoke-direct {p0}, LX/BPv;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1781008
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, LX/BPv;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
