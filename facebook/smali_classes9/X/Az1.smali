.class public LX/Az1;
.super LX/0Rc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rc",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1732064
    invoke-direct {p0}, LX/0Rc;-><init>()V

    .line 1732065
    iput-object p1, p0, LX/Az1;->a:LX/0Rc;

    .line 1732066
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1732067
    iget-object v0, p0, LX/Az1;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1732068
    iget-object v0, p0, LX/Az1;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1732069
    instance-of v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    if-eqz v1, :cond_0

    .line 1732070
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    .line 1732071
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->c()I

    move-result v0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1732072
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    goto :goto_0
.end method
