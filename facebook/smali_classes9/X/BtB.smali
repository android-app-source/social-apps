.class public LX/BtB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1WE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1WE",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bt8;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1VI;


# direct methods
.method public constructor <init>(LX/1WE;Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;LX/0Or;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1WE;",
            "Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;",
            "LX/0Or",
            "<",
            "LX/Bt8;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828695
    iput-object p1, p0, LX/BtB;->a:LX/1WE;

    .line 1828696
    iput-object p2, p0, LX/BtB;->b:Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    .line 1828697
    iput-object p3, p0, LX/BtB;->c:LX/0Or;

    .line 1828698
    iput-object p4, p0, LX/BtB;->d:LX/1VI;

    .line 1828699
    return-void
.end method

.method public static a(LX/0QB;)LX/BtB;
    .locals 7

    .prologue
    .line 1828683
    const-class v1, LX/BtB;

    monitor-enter v1

    .line 1828684
    :try_start_0
    sget-object v0, LX/BtB;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828685
    sput-object v2, LX/BtB;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828686
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828687
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828688
    new-instance v6, LX/BtB;

    invoke-static {v0}, LX/1WE;->a(LX/0QB;)LX/1WE;

    move-result-object v3

    check-cast v3, LX/1WE;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;

    const/16 v5, 0x1d44

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v5

    check-cast v5, LX/1VI;

    invoke-direct {v6, v3, v4, p0, v5}, LX/BtB;-><init>(LX/1WE;Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;LX/0Or;LX/1VI;)V

    .line 1828689
    move-object v0, v6

    .line 1828690
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828691
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BtB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828692
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828693
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
