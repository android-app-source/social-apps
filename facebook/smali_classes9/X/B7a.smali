.class public final enum LX/B7a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B7a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B7a;

.field public static final enum VideoAttachment:LX/B7a;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1748127
    new-instance v0, LX/B7a;

    const-string v1, "VideoAttachment"

    invoke-direct {v0, v1, v2}, LX/B7a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B7a;->VideoAttachment:LX/B7a;

    .line 1748128
    const/4 v0, 0x1

    new-array v0, v0, [LX/B7a;

    sget-object v1, LX/B7a;->VideoAttachment:LX/B7a;

    aput-object v1, v0, v2

    sput-object v0, LX/B7a;->$VALUES:[LX/B7a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1748129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B7a;
    .locals 1

    .prologue
    .line 1748130
    const-class v0, LX/B7a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B7a;

    return-object v0
.end method

.method public static values()[LX/B7a;
    .locals 1

    .prologue
    .line 1748131
    sget-object v0, LX/B7a;->$VALUES:[LX/B7a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B7a;

    return-object v0
.end method
