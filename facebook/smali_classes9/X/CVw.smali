.class public final LX/CVw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1905765
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1905766
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905767
    :goto_0
    return v1

    .line 1905768
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905769
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1905770
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1905771
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905772
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1905773
    const-string v4, "time_slots"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1905774
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1905775
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1905776
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1905777
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1905778
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_c

    .line 1905779
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905780
    :goto_3
    move v3, v4

    .line 1905781
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1905782
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1905783
    goto :goto_1

    .line 1905784
    :cond_3
    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1905785
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1905786
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1905787
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1905788
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1905789
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1905790
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1905791
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1905792
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905793
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_6

    if-eqz v9, :cond_6

    .line 1905794
    const-string v10, "is_disabled"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1905795
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v5

    goto :goto_4

    .line 1905796
    :cond_7
    const-string v10, "product"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1905797
    const/4 v9, 0x0

    .line 1905798
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_10

    .line 1905799
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905800
    :goto_5
    move v7, v9

    .line 1905801
    goto :goto_4

    .line 1905802
    :cond_8
    const-string v10, "timeslot_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1905803
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1905804
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1905805
    :cond_a
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1905806
    if-eqz v3, :cond_b

    .line 1905807
    invoke-virtual {p1, v4, v8}, LX/186;->a(IZ)V

    .line 1905808
    :cond_b
    invoke-virtual {p1, v5, v7}, LX/186;->b(II)V

    .line 1905809
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1905810
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_c
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    goto :goto_4

    .line 1905811
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905812
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_f

    .line 1905813
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1905814
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905815
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_e

    if-eqz v10, :cond_e

    .line 1905816
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 1905817
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 1905818
    :cond_f
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1905819
    invoke-virtual {p1, v9, v7}, LX/186;->b(II)V

    .line 1905820
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_10
    move v7, v9

    goto :goto_6
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1905821
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1905822
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905823
    if-eqz v0, :cond_1

    .line 1905824
    const-string v1, "time_slots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905825
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1905826
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1905827
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/CVv;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905828
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1905829
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1905830
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905831
    if-eqz v0, :cond_2

    .line 1905832
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905833
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905834
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1905835
    return-void
.end method
