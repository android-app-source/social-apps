.class public LX/BvH;
.super LX/3Ga;
.source ""


# instance fields
.field private b:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832125
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BvH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832126
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832127
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BvH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832128
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1832115
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832116
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1832123
    invoke-super {p0, p1, p2}, LX/3Ga;->a(LX/2pa;Z)V

    .line 1832124
    return-void
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 1832129
    const/4 v0, 0x0

    return v0
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1832122
    const v0, 0x7f030a32

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 1832121
    const v0, 0x7f030a33

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 3

    .prologue
    .line 1832119
    iget-object v0, p0, LX/BvH;->b:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    new-instance v1, LX/BvF;

    invoke-direct {v1, p0}, LX/BvF;-><init>(LX/BvH;)V

    new-instance v2, LX/BvG;

    invoke-direct {v2, p0}, LX/BvG;-><init>(LX/BvH;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 1832120
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1832117
    const v0, 0x7f0d19ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iput-object v0, p0, LX/BvH;->b:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    .line 1832118
    return-void
.end method
