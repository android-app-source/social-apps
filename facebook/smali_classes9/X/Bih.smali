.class public LX/Bih;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/0wM;

.field public c:Lcom/facebook/resources/ui/FbCheckedTextView;

.field public d:Lcom/facebook/resources/ui/FbCheckedTextView;

.field public e:Lcom/facebook/events/create/EventCompositionModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810727
    iput-object p1, p0, LX/Bih;->a:Landroid/content/Context;

    .line 1810728
    iput-object p2, p0, LX/Bih;->b:LX/0wM;

    .line 1810729
    return-void
.end method

.method public static a(LX/0QB;)LX/Bih;
    .locals 3

    .prologue
    .line 1810723
    new-instance v2, LX/Bih;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v1}, LX/Bih;-><init>(Landroid/content/Context;LX/0wM;)V

    .line 1810724
    move-object v0, v2

    .line 1810725
    return-object v0
.end method

.method public static a(LX/Bih;ILandroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1810720
    iget-object v0, p0, LX/Bih;->b:LX/0wM;

    iget-object v1, p0, LX/Bih;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, -0x423e37

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1810721
    invoke-virtual {p2, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1810722
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/create/EventCompositionModel;Lcom/facebook/resources/ui/FbCheckedTextView;Lcom/facebook/resources/ui/FbCheckedTextView;)V
    .locals 0

    .prologue
    .line 1810730
    iput-object p1, p0, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810731
    iput-object p2, p0, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 1810732
    iput-object p3, p0, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 1810733
    iget-object p1, p0, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    iget-object p2, p0, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810734
    iget-boolean p3, p2, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    move p2, p3

    .line 1810735
    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 1810736
    iget-object p1, p0, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    new-instance p2, LX/Bif;

    invoke-direct {p2, p0}, LX/Bif;-><init>(LX/Bih;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810737
    const p1, 0x7f020851

    iget-object p2, p0, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-static {p0, p1, p2}, LX/Bih;->a(LX/Bih;ILandroid/widget/TextView;)V

    .line 1810738
    iget-object p1, p0, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    iget-object p2, p0, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810739
    iget-object p3, p2, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    move-object p2, p3

    .line 1810740
    const/4 p3, 0x0

    invoke-virtual {p2, p3}, LX/03R;->asBoolean(Z)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 1810741
    iget-object p1, p0, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    new-instance p2, LX/Big;

    invoke-direct {p2, p0}, LX/Big;-><init>(LX/Bih;)V

    invoke-virtual {p1, p2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810742
    const p1, 0x7f02088b

    iget-object p2, p0, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-static {p0, p1, p2}, LX/Bih;->a(LX/Bih;ILandroid/widget/TextView;)V

    .line 1810743
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1810717
    invoke-virtual {p0, p1}, LX/Bih;->b(Z)V

    .line 1810718
    invoke-virtual {p0, p1}, LX/Bih;->c(Z)V

    .line 1810719
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1810714
    iget-object v1, p0, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    .line 1810715
    return-void

    .line 1810716
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1810711
    iget-object v1, p0, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->setVisibility(I)V

    .line 1810712
    return-void

    .line 1810713
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
