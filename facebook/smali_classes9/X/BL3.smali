.class public final LX/BL3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V
    .locals 0

    .prologue
    .line 1775157
    iput-object p1, p0, LX/BL3;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1775158
    iget-object v0, p0, LX/BL3;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    .line 1775159
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {v1, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1775160
    :cond_0
    :goto_0
    return-void

    .line 1775161
    :cond_1
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {v1, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1775162
    if-eqz v1, :cond_0

    .line 1775163
    new-instance v2, LX/89I;

    iget-wide v3, v1, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    sget-object v5, LX/2rw;->USER:LX/2rw;

    invoke-direct {v2, v3, v4, v5}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v3, v1, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1775164
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 1775165
    move-object v2, v2

    .line 1775166
    iget-object v1, v1, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1775167
    iput-object v1, v2, LX/89I;->d:Ljava/lang/String;

    .line 1775168
    move-object v1, v2

    .line 1775169
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 1775170
    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->p:LX/BJU;

    if-eqz v2, :cond_2

    .line 1775171
    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->p:LX/BJU;

    invoke-virtual {v2, v1}, LX/BJU;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1775172
    :cond_2
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->d:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "target_privacy_picker_friend_selected"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method
