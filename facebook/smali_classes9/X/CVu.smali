.class public final LX/CVu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1905677
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1905678
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1905679
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1905680
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1905681
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1905682
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905683
    :goto_1
    move v1, v2

    .line 1905684
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1905685
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1905686
    :cond_1
    const-string v12, "count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1905687
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v3

    .line 1905688
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 1905689
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1905690
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905691
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_2

    if-eqz v11, :cond_2

    .line 1905692
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_3

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1905693
    :cond_3
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_2

    .line 1905694
    :cond_4
    const-string v12, "edit_screen_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1905695
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_2

    .line 1905696
    :cond_5
    const-string v12, "fee"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1905697
    invoke-static {p0, p1}, LX/CVR;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_2

    .line 1905698
    :cond_6
    const-string v12, "max_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1905699
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_2

    .line 1905700
    :cond_7
    const-string v12, "product"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1905701
    invoke-static {p0, p1}, LX/CVX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_2

    .line 1905702
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1905703
    :cond_9
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1905704
    invoke-virtual {p1, v2, v10}, LX/186;->b(II)V

    .line 1905705
    if-eqz v4, :cond_a

    .line 1905706
    invoke-virtual {p1, v3, v9, v2}, LX/186;->a(III)V

    .line 1905707
    :cond_a
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v8}, LX/186;->b(II)V

    .line 1905708
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 1905709
    if-eqz v1, :cond_b

    .line 1905710
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6, v2}, LX/186;->a(III)V

    .line 1905711
    :cond_b
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1905712
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1905713
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1905714
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 1905715
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 v4, 0x0

    .line 1905716
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1905717
    invoke-virtual {p0, v1, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1905718
    if-eqz v2, :cond_0

    .line 1905719
    const-string v2, "__type__"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905720
    invoke-static {p0, v1, v4, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1905721
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1905722
    if-eqz v2, :cond_1

    .line 1905723
    const-string v3, "count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905724
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1905725
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1905726
    if-eqz v2, :cond_2

    .line 1905727
    const-string v3, "edit_screen_id"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905728
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1905729
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1905730
    if-eqz v2, :cond_3

    .line 1905731
    const-string v3, "fee"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905732
    invoke-static {p0, v2, p2}, LX/CVR;->a(LX/15i;ILX/0nX;)V

    .line 1905733
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1905734
    if-eqz v2, :cond_4

    .line 1905735
    const-string v3, "max_count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905736
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1905737
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1905738
    if-eqz v2, :cond_5

    .line 1905739
    const-string v3, "product"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905740
    invoke-static {p0, v2, p2, p3}, LX/CVX;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905741
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1905742
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1905743
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1905744
    return-void
.end method
