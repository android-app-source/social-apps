.class public final LX/Box;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/36K;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;)V
    .locals 0

    .prologue
    .line 1822402
    iput-object p1, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1822403
    iget-object v0, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822404
    iget-object v0, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->c:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822405
    iget-object v0, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822406
    iget-object v0, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822407
    iget-object v0, p0, LX/Box;->a:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822408
    return-void
.end method
