.class public LX/Auh;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/8GY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/drawingview/DrawingView;

.field public c:LX/86w;

.field public d:LX/Auc;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/graphics/Rect;

.field public f:F

.field public g:I

.field public h:I

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1723059
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1723060
    const/4 v0, 0x0

    iput v0, p0, LX/Auh;->f:F

    .line 1723061
    iput v1, p0, LX/Auh;->g:I

    .line 1723062
    iput v1, p0, LX/Auh;->h:I

    .line 1723063
    iput v1, p0, LX/Auh;->i:I

    .line 1723064
    iput v1, p0, LX/Auh;->j:I

    .line 1723065
    const-class v0, LX/Auh;

    invoke-static {v0, p0}, LX/Auh;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1723066
    invoke-virtual {p0}, LX/Auh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1723067
    const v1, 0x7f03093f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1723068
    const v1, 0x7f0d17bb

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    iput-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    .line 1723069
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    new-instance v1, LX/Auf;

    invoke-direct {v1, p0}, LX/Auf;-><init>(LX/Auh;)V

    .line 1723070
    iput-object v1, v0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    .line 1723071
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723072
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Auh;

    invoke-static {p0}, LX/8GY;->b(LX/0QB;)LX/8GY;

    move-result-object p0

    check-cast p0, LX/8GY;

    iput-object p0, p1, LX/Auh;->a:LX/8GY;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1723073
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->b()V

    .line 1723074
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->invalidate()V

    .line 1723075
    invoke-virtual {p0}, LX/Auh;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1723076
    sget-object v0, LX/86w;->ACTIVE_EMPTY:LX/86w;

    invoke-virtual {p0, v0}, LX/Auh;->setDrawingMode(LX/86w;)V

    .line 1723077
    :cond_0
    iget v0, p0, LX/Auh;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Auh;->j:I

    .line 1723078
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1723079
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->getHistorySize()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDrawingMode(LX/86w;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1723080
    iget-object v0, p0, LX/Auh;->c:LX/86w;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Auh;->c:LX/86w;

    if-eq v0, p1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    .line 1723081
    :cond_1
    :goto_0
    return-void

    .line 1723082
    :cond_2
    iput-object p1, p0, LX/Auh;->c:LX/86w;

    .line 1723083
    sget-object v0, LX/Aug;->a:[I

    invoke-virtual {p1}, LX/86w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1723084
    :pswitch_0
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723085
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawingview/DrawingView;->setVisibility(I)V

    .line 1723086
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    if-eqz v0, :cond_1

    .line 1723087
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    .line 1723088
    iget-object v1, v0, LX/Auc;->a:LX/Aud;

    iget-object v1, v1, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v1

    .line 1723089
    sget-object v2, LX/870;->DOODLE_EMPTY:LX/870;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/870;->DOODLE_HAS_DRAWING:LX/870;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/870;->DOODLE_DRAWING:LX/870;

    if-ne v1, v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    .line 1723090
    :goto_1
    if-eqz v2, :cond_4

    sget-object v1, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    :cond_4
    sget-object v2, LX/86w;->HIDDEN:LX/86w;

    invoke-static {v0, v1, v2}, LX/Auc;->a(LX/Auc;LX/870;LX/86w;)V

    .line 1723091
    goto :goto_0

    .line 1723092
    :pswitch_1
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723093
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setVisibility(I)V

    .line 1723094
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    if-eqz v0, :cond_1

    .line 1723095
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    .line 1723096
    sget-object v1, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    sget-object v2, LX/86w;->AVAILABLE:LX/86w;

    invoke-static {v0, v1, v2}, LX/Auc;->a(LX/Auc;LX/870;LX/86w;)V

    .line 1723097
    goto :goto_0

    .line 1723098
    :pswitch_2
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723099
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setVisibility(I)V

    .line 1723100
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723101
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    .line 1723102
    sget-object v1, LX/870;->DOODLE_EMPTY:LX/870;

    sget-object v2, LX/86w;->ACTIVE_EMPTY:LX/86w;

    invoke-static {v0, v1, v2}, LX/Auc;->a(LX/Auc;LX/870;LX/86w;)V

    .line 1723103
    goto/16 :goto_0

    .line 1723104
    :pswitch_3
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723105
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setVisibility(I)V

    .line 1723106
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723107
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    .line 1723108
    sget-object v1, LX/870;->DOODLE_HAS_DRAWING:LX/870;

    sget-object v2, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    invoke-static {v0, v1, v2}, LX/Auc;->a(LX/Auc;LX/870;LX/86w;)V

    .line 1723109
    goto/16 :goto_0

    .line 1723110
    :pswitch_4
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 1723111
    iget-object v0, p0, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawingview/DrawingView;->setVisibility(I)V

    .line 1723112
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723113
    iget-object v0, p0, LX/Auh;->d:LX/Auc;

    .line 1723114
    sget-object v1, LX/870;->DOODLE_DRAWING:LX/870;

    sget-object v2, LX/86w;->ACTIVE_DRAWING:LX/86w;

    invoke-static {v0, v1, v2}, LX/Auc;->a(LX/Auc;LX/870;LX/86w;)V

    .line 1723115
    goto/16 :goto_0

    .line 1723116
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
