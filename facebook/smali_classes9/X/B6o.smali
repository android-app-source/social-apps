.class public LX/B6o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# instance fields
.field private a:LX/B6D;

.field private b:LX/B7W;

.field private c:LX/0if;

.field private d:Landroid/content/Context;

.field private e:I

.field private f:Z

.field public g:Z

.field public h:Landroid/view/View;

.field private i:I

.field private final j:LX/B6m;


# direct methods
.method public constructor <init>(LX/B7W;LX/0if;LX/B6D;Landroid/content/Context;)V
    .locals 1
    .param p3    # LX/B6D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1747330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747331
    new-instance v0, LX/B6n;

    invoke-direct {v0, p0}, LX/B6n;-><init>(LX/B6o;)V

    iput-object v0, p0, LX/B6o;->j:LX/B6m;

    .line 1747332
    iput-object p3, p0, LX/B6o;->a:LX/B6D;

    .line 1747333
    iput-object p1, p0, LX/B6o;->b:LX/B7W;

    .line 1747334
    iput-object p2, p0, LX/B6o;->c:LX/0if;

    .line 1747335
    iput-object p4, p0, LX/B6o;->d:Landroid/content/Context;

    .line 1747336
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1747337
    iget-object v0, p0, LX/B6o;->b:LX/B7W;

    iget-object v1, p0, LX/B6o;->j:LX/B6m;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1747338
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1747339
    if-eqz p1, :cond_0

    .line 1747340
    const/4 v0, 0x0

    iput v0, p0, LX/B6o;->e:I

    .line 1747341
    :goto_0
    return-void

    .line 1747342
    :cond_0
    iget-object v0, p0, LX/B6o;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1988

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/B6o;->e:I

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1747343
    iget-object v0, p0, LX/B6o;->b:LX/B7W;

    iget-object v1, p0, LX/B6o;->j:LX/B6m;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1747344
    return-void
.end method

.method public final onScrollChanged()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1747345
    iget-object v0, p0, LX/B6o;->a:LX/B6D;

    invoke-interface {v0}, LX/B6D;->a()Lcom/facebook/widget/FbScrollView;

    move-result-object v1

    .line 1747346
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v0

    iget v2, p0, LX/B6o;->i:I

    if-ne v0, v2, :cond_1

    .line 1747347
    :cond_0
    :goto_0
    return-void

    .line 1747348
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, LX/B6o;->i:I

    .line 1747349
    iget-boolean v0, p0, LX/B6o;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B6o;->h:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1747350
    iget-object v0, p0, LX/B6o;->d:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1747351
    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1747352
    iget-object v0, p0, LX/B6o;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1747353
    iget-object v0, p0, LX/B6o;->b:LX/B7W;

    new-instance v2, LX/B7Z;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1747354
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v0

    .line 1747355
    iget v2, p0, LX/B6o;->e:I

    if-le v0, v2, :cond_3

    .line 1747356
    iget-object v0, p0, LX/B6o;->b:LX/B7W;

    new-instance v2, LX/B7f;

    invoke-direct {v2, v4}, LX/B7f;-><init>(Z)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1747357
    :goto_1
    iget-object v0, p0, LX/B6o;->a:LX/B6D;

    invoke-interface {v0}, LX/B6D;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/B6o;->f:Z

    if-nez v0, :cond_0

    .line 1747358
    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1747359
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v1

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1747360
    if-nez v0, :cond_0

    .line 1747361
    iget-object v0, p0, LX/B6o;->c:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "scroll_to_bottom"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1747362
    iput-boolean v4, p0, LX/B6o;->f:Z

    goto :goto_0

    .line 1747363
    :cond_3
    iget-object v0, p0, LX/B6o;->b:LX/B7W;

    new-instance v2, LX/B7f;

    invoke-direct {v2, v5}, LX/B7f;-><init>(Z)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method
