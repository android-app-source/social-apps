.class public LX/Apv;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Aps;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Apw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717036
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apv;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Apw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1717033
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717034
    iput-object p1, p0, LX/Apv;->b:LX/0Ot;

    .line 1717035
    return-void
.end method

.method public static a(LX/0QB;)LX/Apv;
    .locals 4

    .prologue
    .line 1717022
    const-class v1, LX/Apv;

    monitor-enter v1

    .line 1717023
    :try_start_0
    sget-object v0, LX/Apv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1717024
    sput-object v2, LX/Apv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1717025
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1717026
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1717027
    new-instance v3, LX/Apv;

    const/16 p0, 0x2215

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Apv;-><init>(LX/0Ot;)V

    .line 1717028
    move-object v0, v3

    .line 1717029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1717030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1717031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1717032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 1

    .prologue
    .line 1717016
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1717017
    if-nez v0, :cond_0

    .line 1717018
    const/4 v0, 0x0

    .line 1717019
    :goto_0
    return-object v0

    .line 1717020
    :cond_0
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1717021
    check-cast v0, LX/Apt;

    iget-object v0, v0, LX/Apt;->l:LX/1dQ;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1717037
    check-cast p2, LX/Apt;

    .line 1717038
    iget-object v0, p0, LX/Apv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Apw;

    iget v2, p2, LX/Apt;->a:I

    iget v3, p2, LX/Apt;->b:I

    iget-boolean v4, p2, LX/Apt;->c:Z

    iget v5, p2, LX/Apt;->d:I

    iget v6, p2, LX/Apt;->e:I

    iget v7, p2, LX/Apt;->f:I

    iget v8, p2, LX/Apt;->g:I

    iget v9, p2, LX/Apt;->h:I

    iget v10, p2, LX/Apt;->i:I

    iget-object v11, p2, LX/Apt;->j:Ljava/lang/Boolean;

    move-object v1, p1

    .line 1717039
    if-nez v11, :cond_0

    .line 1717040
    :goto_0
    invoke-static {v1}, LX/Apv;->d(LX/1De;)LX/1dQ;

    move-result-object p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    .line 1717041
    :goto_1
    if-eqz v4, :cond_3

    .line 1717042
    if-eqz p0, :cond_2

    .line 1717043
    :goto_2
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p1

    iget-object p2, v0, LX/Apw;->g:LX/1vg;

    invoke-virtual {p2, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p2

    if-eqz v4, :cond_5

    :goto_3
    invoke-virtual {p2, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->c()LX/1Di;

    move-result-object p1

    const p2, 0x7f0b02ba

    invoke-interface {p1, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object p1

    const p2, 0x7f0b02ba

    invoke-interface {p1, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object p1

    if-eqz v4, :cond_6

    :goto_4
    invoke-interface {p1, v9}, LX/1Di;->x(I)LX/1Di;

    move-result-object p1

    if-eqz p0, :cond_7

    .line 1717044
    const p0, 0x48eabe0c    # 480752.38f

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p2, v0

    const/4 v0, 0x1

    aput-object v1, p2, v0

    invoke-static {v1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1717045
    :goto_5
    invoke-interface {p1, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1717046
    return-object v0

    .line 1717047
    :cond_0
    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    goto :goto_0

    .line 1717048
    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    move v5, v6

    .line 1717049
    goto :goto_2

    .line 1717050
    :cond_3
    if-eqz p0, :cond_4

    :goto_6
    move v5, v7

    goto :goto_2

    :cond_4
    move v7, v8

    goto :goto_6

    :cond_5
    move v2, v3

    .line 1717051
    goto :goto_3

    :cond_6
    move v9, v10

    goto :goto_4

    :cond_7
    const/4 p0, 0x0

    goto :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1716993
    invoke-static {}, LX/1dS;->b()V

    .line 1716994
    iget v0, p1, LX/1dQ;->b:I

    .line 1716995
    packed-switch v0, :pswitch_data_0

    .line 1716996
    :goto_0
    return-object v3

    .line 1716997
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1716998
    check-cast v2, LX/Apt;

    .line 1716999
    iget-object p1, p0, LX/Apv;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean p1, v2, LX/Apt;->k:Z

    .line 1717000
    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    .line 1717001
    :cond_0
    if-nez v1, :cond_2

    const/4 p0, 0x1

    .line 1717002
    :goto_1
    iget-object v2, v0, LX/1De;->g:LX/1X1;

    move-object v2, v2

    .line 1717003
    if-nez v2, :cond_3

    .line 1717004
    :goto_2
    invoke-static {v0}, LX/Apv;->d(LX/1De;)LX/1dQ;

    move-result-object v2

    .line 1717005
    if-eqz v2, :cond_1

    .line 1717006
    new-instance p1, LX/Apr;

    invoke-direct {p1}, LX/Apr;-><init>()V

    .line 1717007
    iput-boolean p0, p1, LX/Apr;->a:Z

    .line 1717008
    iget-object v1, v2, LX/1dQ;->a:LX/1X1;

    .line 1717009
    iget-object p0, v1, LX/1X1;->e:LX/1S3;

    move-object v1, p0

    .line 1717010
    invoke-virtual {v1, v2, p1}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1717011
    :cond_1
    goto :goto_0

    .line 1717012
    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    .line 1717013
    :cond_3
    check-cast v2, LX/Apt;

    .line 1717014
    new-instance p1, LX/Apu;

    iget-object v1, v2, LX/Apt;->m:LX/Apv;

    invoke-direct {p1, v1, p0}, LX/Apu;-><init>(LX/Apv;Z)V

    move-object v2, p1

    .line 1717015
    invoke-virtual {v0, v2}, LX/1De;->a(LX/48B;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x48eabe0c
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 1716989
    check-cast p1, LX/Apt;

    .line 1716990
    check-cast p2, LX/Apt;

    .line 1716991
    iget-object v0, p1, LX/Apt;->j:Ljava/lang/Boolean;

    iput-object v0, p2, LX/Apt;->j:Ljava/lang/Boolean;

    .line 1716992
    return-void
.end method

.method public final c(LX/1De;)LX/Aps;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716981
    new-instance v1, LX/Apt;

    invoke-direct {v1, p0}, LX/Apt;-><init>(LX/Apv;)V

    .line 1716982
    sget-object v2, LX/Apv;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Aps;

    .line 1716983
    if-nez v2, :cond_0

    .line 1716984
    new-instance v2, LX/Aps;

    invoke-direct {v2}, LX/Aps;-><init>()V

    .line 1716985
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Aps;->a$redex0(LX/Aps;LX/1De;IILX/Apt;)V

    .line 1716986
    move-object v1, v2

    .line 1716987
    move-object v0, v1

    .line 1716988
    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1716980
    const/4 v0, 0x1

    return v0
.end method
