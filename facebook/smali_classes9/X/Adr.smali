.class public final LX/Adr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ae2;


# direct methods
.method public constructor <init>(LX/Ae2;)V
    .locals 0

    .prologue
    .line 1695633
    iput-object p1, p0, LX/Adr;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x383ab1e3

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1695634
    iget-object v1, p0, LX/Adr;->a:LX/Ae2;

    const/4 v5, 0x0

    .line 1695635
    iget-object v2, v1, LX/Ae2;->T:LX/3Af;

    if-nez v2, :cond_6

    .line 1695636
    iget-object v2, v1, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1695637
    check-cast v2, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1695638
    new-instance v6, LX/3Af;

    invoke-direct {v6, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v6, v1, LX/Ae2;->T:LX/3Af;

    .line 1695639
    new-instance v6, LX/Ae0;

    invoke-direct {v6, v1}, LX/Ae0;-><init>(LX/Ae2;)V

    .line 1695640
    iget-object v7, v1, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v7, v6}, LX/3Af;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1695641
    iget-object v7, v1, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v7, v6}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1695642
    new-instance v7, LX/7TY;

    invoke-direct {v7, v2}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1695643
    iget-object v2, v1, LX/Ae2;->i:LX/1b4;

    .line 1695644
    iget-object v6, v2, LX/1b4;->a:LX/0ad;

    sget-char v8, LX/1v6;->A:C

    const/4 v9, 0x0

    invoke-interface {v6, v8, v9}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1695645
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1695646
    sget-object v6, LX/6Rh;->a:[Ljava/lang/String;

    .line 1695647
    :goto_0
    move-object v8, v6

    .line 1695648
    array-length v9, v8

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_5

    aget-object v10, v8, v6

    .line 1695649
    const/4 v2, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 1695650
    sget-object v2, LX/Ae2;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string p1, "Unable to add option: "

    invoke-direct {v11, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695651
    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_1

    .line 1695652
    :sswitch_0
    const-string v11, "invite_friends"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    move v2, v5

    goto :goto_2

    :sswitch_1
    const-string v11, "share_now"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string v11, "write_post"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v11, "send_message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v11, "copy_link"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const/4 v2, 0x4

    goto :goto_2

    .line 1695653
    :pswitch_0
    invoke-static {v1, v7}, LX/Ae2;->a(LX/Ae2;LX/7TY;)V

    goto :goto_3

    .line 1695654
    :pswitch_1
    invoke-static {v1}, LX/Ae2;->q(LX/Ae2;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1695655
    const v2, 0x7f080c24

    invoke-virtual {v7, v2}, LX/34c;->e(I)LX/3Ai;

    move-result-object v10

    .line 1695656
    const v2, 0x7f0209af

    invoke-virtual {v10, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v11, LX/Adx;

    invoke-direct {v11, v1}, LX/Adx;-><init>(LX/Ae2;)V

    invoke-interface {v2, v11}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1695657
    iget-object v2, v1, LX/Ae2;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2sP;

    invoke-virtual {v2}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    .line 1695658
    invoke-static {v2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1695659
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v2, v11, :cond_9

    .line 1695660
    const v2, 0x7f080c25

    invoke-virtual {v10, v2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 1695661
    :cond_1
    :goto_4
    goto :goto_3

    .line 1695662
    :pswitch_2
    invoke-static {v1}, LX/Ae2;->s(LX/Ae2;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1695663
    const v2, 0x7f080c27

    invoke-virtual {v7, v2}, LX/34c;->e(I)LX/3Ai;

    move-result-object v2

    .line 1695664
    const v10, 0x7f02080f

    invoke-virtual {v2, v10}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v10, LX/Ady;

    invoke-direct {v10, v1}, LX/Ady;-><init>(LX/Ae2;)V

    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1695665
    :cond_2
    goto/16 :goto_3

    .line 1695666
    :pswitch_3
    iget-object v2, v1, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_a

    iget-object v2, v1, LX/Ae2;->C:LX/14w;

    iget-object v10, v1, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v10}, LX/14w;->v(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_5
    move v2, v2

    .line 1695667
    if-eqz v2, :cond_3

    .line 1695668
    const v2, 0x7f080c28

    invoke-virtual {v7, v2}, LX/34c;->e(I)LX/3Ai;

    move-result-object v2

    .line 1695669
    const v10, 0x7f020740

    invoke-virtual {v2, v10}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v10, LX/Adz;

    invoke-direct {v10, v1}, LX/Adz;-><init>(LX/Ae2;)V

    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1695670
    :cond_3
    goto/16 :goto_3

    .line 1695671
    :pswitch_4
    invoke-static {v1}, LX/Ae2;->r(LX/Ae2;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1695672
    const v2, 0x7f080c2b

    invoke-virtual {v7, v2}, LX/34c;->e(I)LX/3Ai;

    move-result-object v2

    .line 1695673
    const v10, 0x7f0208fd

    invoke-virtual {v2, v10}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v10, LX/Adl;

    invoke-direct {v10, v1}, LX/Adl;-><init>(LX/Ae2;)V

    invoke-interface {v2, v10}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1695674
    :cond_4
    goto/16 :goto_3

    .line 1695675
    :cond_5
    iget-object v2, v1, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v2, v7}, LX/3Af;->a(LX/1OM;)V

    .line 1695676
    :cond_6
    iget-object v2, v1, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v2}, LX/3Af;->isShowing()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1695677
    const-string v2, "share_button_tapped"

    invoke-static {v1, v2}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695678
    iget-object v2, v1, LX/Ae2;->T:LX/3Af;

    invoke-virtual {v2}, LX/3Af;->show()V

    .line 1695679
    :cond_7
    iget-object v1, p0, LX/Adr;->a:LX/Ae2;

    iget-object v1, v1, LX/Ae2;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Ae2;->d:LX/0Tn;

    invoke-interface {v1, v2, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1695680
    const v1, 0x53fab8e3

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_8
    const-string v8, ","

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1695681
    :cond_9
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v2, v11, :cond_1

    .line 1695682
    const v2, 0x7f080c26

    invoke-virtual {v10, v2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto/16 :goto_4

    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5e46dd8a -> :sswitch_1
        -0x25ebf300 -> :sswitch_2
        -0x253391b0 -> :sswitch_3
        0x5706065f -> :sswitch_0
        0x59bb1a84 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
