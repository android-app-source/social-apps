.class public final LX/AZh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/AZi;

.field public b:I

.field public c:LX/7S9;


# direct methods
.method public constructor <init>(LX/AZi;)V
    .locals 1

    .prologue
    .line 1687311
    iput-object p1, p0, LX/AZh;->a:LX/AZi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687312
    const/4 v0, -0x1

    iput v0, p0, LX/AZh;->b:I

    .line 1687313
    const/4 v0, 0x0

    iput-object v0, p0, LX/AZh;->c:LX/7S9;

    return-void
.end method

.method private a(Ljava/util/List;FFI)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7S9;",
            ">;FFI)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1687314
    iget-object v1, p0, LX/AZh;->c:LX/7S9;

    if-eqz v1, :cond_0

    .line 1687315
    iget-object v1, p0, LX/AZh;->c:LX/7S9;

    iget v1, v1, LX/7S9;->b:F

    sub-float/2addr v1, p2

    .line 1687316
    iget-object v2, p0, LX/AZh;->c:LX/7S9;

    iget v2, v2, LX/7S9;->c:F

    sub-float/2addr v2, p3

    .line 1687317
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x41200000    # 10.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    .line 1687318
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1687319
    new-instance v0, LX/7S9;

    sget-object v1, LX/7S8;->MOVE:LX/7S8;

    invoke-direct {v0, v1, p2, p3, p4}, LX/7S9;-><init>(LX/7S8;FFI)V

    iput-object v0, p0, LX/AZh;->c:LX/7S9;

    .line 1687320
    iget-object v0, p0, LX/AZh;->c:LX/7S9;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687321
    :cond_1
    return-void

    .line 1687322
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1687323
    iget-object v2, p0, LX/AZh;->a:LX/AZi;

    iget-object v2, v2, LX/AZi;->c:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    invoke-virtual {v2}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getCurrentlySelectedColor()I

    move-result v4

    .line 1687324
    if-nez v4, :cond_1

    .line 1687325
    :cond_0
    :goto_0
    return v1

    .line 1687326
    :cond_1
    invoke-static {p2}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1687327
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1687328
    packed-switch v2, :pswitch_data_0

    :cond_2
    :pswitch_0
    move v2, v1

    move v0, v1

    move v3, v1

    .line 1687329
    :goto_1
    if-eqz v0, :cond_3

    .line 1687330
    invoke-static {p2, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, LX/AZh;->b:I

    .line 1687331
    new-instance v0, LX/7S9;

    sget-object v6, LX/7S8;->START:LX/7S8;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    invoke-direct {v0, v6, v7, v8, v4}, LX/7S9;-><init>(LX/7S8;FFI)V

    iput-object v0, p0, LX/AZh;->c:LX/7S9;

    .line 1687332
    iget-object v0, p0, LX/AZh;->c:LX/7S9;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687333
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->i:LX/AYB;

    if-eqz v0, :cond_3

    .line 1687334
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->i:LX/AYB;

    invoke-interface {v0}, LX/AYB;->i()V

    .line 1687335
    :cond_3
    if-eqz v3, :cond_5

    .line 1687336
    iget v0, p0, LX/AZh;->b:I

    invoke-static {p2, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1687337
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    move v0, v1

    .line 1687338
    :goto_2
    if-ge v0, v6, :cond_4

    .line 1687339
    invoke-virtual {p2, v3, v0}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v7

    .line 1687340
    invoke-virtual {p2, v3, v0}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v8

    .line 1687341
    invoke-direct {p0, v5, v7, v8, v4}, LX/AZh;->a(Ljava/util/List;FFI)V

    .line 1687342
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1687343
    :pswitch_1
    iget v2, p0, LX/AZh;->b:I

    if-ne v2, v9, :cond_2

    move v2, v1

    move v3, v1

    .line 1687344
    goto :goto_1

    :pswitch_2
    move v2, v0

    move v3, v1

    move v0, v1

    .line 1687345
    goto :goto_1

    .line 1687346
    :pswitch_3
    invoke-static {p2}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1687347
    invoke-static {p2, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 1687348
    iget v3, p0, LX/AZh;->b:I

    if-ne v2, v3, :cond_2

    move v2, v0

    move v3, v1

    move v0, v1

    .line 1687349
    goto :goto_1

    .line 1687350
    :pswitch_4
    iget v2, p0, LX/AZh;->b:I

    if-eq v2, v9, :cond_2

    move v2, v1

    move v3, v0

    move v0, v1

    .line 1687351
    goto :goto_1

    .line 1687352
    :cond_4
    invoke-static {p2, v3}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1687353
    invoke-static {p2, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1687354
    invoke-direct {p0, v5, v0, v3, v4}, LX/AZh;->a(Ljava/util/List;FFI)V

    .line 1687355
    :cond_5
    if-eqz v2, :cond_7

    .line 1687356
    iput v9, p0, LX/AZh;->b:I

    .line 1687357
    const/4 v0, 0x0

    iput-object v0, p0, LX/AZh;->c:LX/7S9;

    .line 1687358
    new-instance v0, LX/7S9;

    sget-object v2, LX/7S8;->END:LX/7S8;

    invoke-direct {v0, v2}, LX/7S9;-><init>(LX/7S8;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687359
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->i:LX/AYB;

    if-eqz v0, :cond_6

    .line 1687360
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->i:LX/AYB;

    invoke-interface {v0}, LX/AYB;->j()V

    .line 1687361
    :cond_6
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->h:LX/AZe;

    if-eqz v0, :cond_7

    .line 1687362
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->b:LX/AVT;

    iget-object v2, p0, LX/AZh;->a:LX/AZi;

    iget-object v2, v2, LX/AZi;->h:LX/AZe;

    invoke-virtual {v2}, LX/AZe;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "stroke"

    invoke-static {v0, v2, v3}, LX/Aa6;->c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687363
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1687364
    iget-object v0, p0, LX/AZh;->a:LX/AZi;

    iget-object v0, v0, LX/AZi;->a:LX/AVP;

    invoke-virtual {v0, v5}, LX/AVP;->a(Ljava/util/List;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
