.class public interface abstract LX/B5a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Yw;
.implements LX/8Z3;
.implements LX/8Yz;
.implements LX/8Z0;
.implements LX/8Z1;
.implements LX/8Z2;


# virtual methods
.method public abstract A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()LX/8Yr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract G()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract K()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract L()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract M()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract em_()LX/8Yr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract iU_()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/8Ys;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()I
.end method

.method public abstract n()I
.end method

.method public abstract o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()Z
.end method

.method public abstract r()Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract w()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract y()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
