.class public final LX/Ajv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1EE;

.field public final synthetic b:LX/1aw;

.field public final synthetic c:LX/1Rh;

.field public final synthetic d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;LX/1EE;LX/1aw;LX/1Rh;)V
    .locals 0

    .prologue
    .line 1708557
    iput-object p1, p0, LX/Ajv;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iput-object p2, p0, LX/Ajv;->a:LX/1EE;

    iput-object p3, p0, LX/Ajv;->b:LX/1aw;

    iput-object p4, p0, LX/Ajv;->c:LX/1Rh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x359522f2    # -3848003.5f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1708558
    iget-object v0, p0, LX/Ajv;->a:LX/1EE;

    invoke-virtual {v0}, LX/1EE;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708559
    iget-object v0, p0, LX/Ajv;->b:LX/1aw;

    invoke-virtual {v0}, LX/1aw;->a()V

    .line 1708560
    :goto_0
    const v0, -0x1ddc1888

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1708561
    :cond_0
    iget-object v2, p0, LX/Ajv;->b:LX/1aw;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Ajv;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v4, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->n:Landroid/support/v4/app/FragmentActivity;

    const-string v5, "inlineComposerGoodFriendsCreativeCameraButton"

    iget-object v0, p0, LX/Ajv;->d:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerV2HeaderPartDefinition;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nq;

    iget-object v6, p0, LX/Ajv;->c:LX/1Rh;

    iget-object v6, v6, LX/1Rh;->b:LX/1EE;

    .line 1708562
    iget-object p0, v6, LX/1EE;->i:Ljava/lang/String;

    move-object v6, p0

    .line 1708563
    invoke-static {v6}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    const/4 v12, 0x1

    .line 1708564
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 1708565
    invoke-static {v2, v5, v7}, LX/1aw;->a(LX/1aw;Ljava/lang/String;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v8

    .line 1708566
    iget-object v7, v2, LX/1aw;->m:LX/1b0;

    const/16 v9, 0x6dc

    new-instance v10, LX/89V;

    invoke-direct {v10}, LX/89V;-><init>()V

    .line 1708567
    iput-boolean v12, v10, LX/89V;->b:Z

    .line 1708568
    move-object v10, v10

    .line 1708569
    sget-object v11, LX/4gI;->ALL:LX/4gI;

    .line 1708570
    iput-object v11, v10, LX/89V;->f:LX/4gI;

    .line 1708571
    move-object v10, v10

    .line 1708572
    iput-boolean v12, v10, LX/89V;->d:Z

    .line 1708573
    move-object v10, v10

    .line 1708574
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v8

    .line 1708575
    iput-object v8, v10, LX/89V;->k:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1708576
    move-object v8, v10

    .line 1708577
    invoke-virtual {v8}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v10

    const/4 v12, 0x0

    move-object v8, v4

    move-object v11, v3

    invoke-interface/range {v7 .. v12}, LX/1b0;->a(Landroid/app/Activity;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1708578
    goto :goto_0
.end method
