.class public LX/Ap5;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ap5",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715333
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715334
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Ap5;->b:LX/0Zi;

    .line 1715335
    iput-object p1, p0, LX/Ap5;->a:LX/0Ot;

    .line 1715336
    return-void
.end method

.method public static a(LX/0QB;)LX/Ap5;
    .locals 4

    .prologue
    .line 1715322
    const-class v1, LX/Ap5;

    monitor-enter v1

    .line 1715323
    :try_start_0
    sget-object v0, LX/Ap5;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715324
    sput-object v2, LX/Ap5;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715325
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715326
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715327
    new-instance v3, LX/Ap5;

    const/16 p0, 0x21f6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ap5;-><init>(LX/0Ot;)V

    .line 1715328
    move-object v0, v3

    .line 1715329
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715330
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ap5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715331
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715332
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 15

    .prologue
    .line 1715319
    check-cast p2, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715320
    iget-object v1, p0, LX/Ap5;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;

    move-object/from16 v0, p2

    iget v3, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->a:I

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget v6, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->d:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v14}, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ILX/1Po;LX/33B;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1X1;LX/1X1;)LX/1Dg;

    move-result-object v1

    .line 1715321
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715317
    invoke-static {}, LX/1dS;->b()V

    .line 1715318
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Ap4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1715309
    new-instance v1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;-><init>(LX/Ap5;)V

    .line 1715310
    iget-object v2, p0, LX/Ap5;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ap4;

    .line 1715311
    if-nez v2, :cond_0

    .line 1715312
    new-instance v2, LX/Ap4;

    invoke-direct {v2, p0}, LX/Ap4;-><init>(LX/Ap5;)V

    .line 1715313
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Ap4;->a$redex0(LX/Ap4;LX/1De;IILcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;)V

    .line 1715314
    move-object v1, v2

    .line 1715315
    move-object v0, v1

    .line 1715316
    return-object v0
.end method
