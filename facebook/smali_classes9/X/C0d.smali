.class public final LX/C0d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic c:LX/2yV;

.field public final synthetic d:LX/1Pf;

.field public final synthetic e:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/2yV;LX/1Pf;)V
    .locals 0

    .prologue
    .line 1841077
    iput-object p1, p0, LX/C0d;->e:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    iput-object p2, p0, LX/C0d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C0d;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p4, p0, LX/C0d;->c:LX/2yV;

    iput-object p5, p0, LX/C0d;->d:LX/1Pf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x652b84db

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1841078
    iget-object v1, p0, LX/C0d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    .line 1841079
    iget-object v1, p0, LX/C0d;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1841080
    :goto_0
    iget-object v3, p0, LX/C0d;->c:LX/2yV;

    if-eqz v0, :cond_1

    sget-object v1, LX/C0a;->LIKED_PAGE:LX/C0a;

    .line 1841081
    :goto_1
    iput-object v1, v3, LX/2yV;->a:LX/C0a;

    .line 1841082
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v1

    iget-object v3, p0, LX/C0d;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    .line 1841083
    iput-object v3, v1, LX/5H8;->a:Ljava/lang/String;

    .line 1841084
    move-object v1, v1

    .line 1841085
    iput-boolean v0, v1, LX/5H8;->b:Z

    .line 1841086
    move-object v0, v1

    .line 1841087
    iget-object v1, p0, LX/C0d;->e:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->c:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1841088
    iput-object v1, v0, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1841089
    move-object v0, v0

    .line 1841090
    const-string v1, "feed_share_link"

    .line 1841091
    iput-object v1, v0, LX/5H8;->e:Ljava/lang/String;

    .line 1841092
    move-object v0, v0

    .line 1841093
    invoke-virtual {v0}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v0

    .line 1841094
    iget-object v1, p0, LX/C0d;->e:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->d:LX/1Ck;

    sget-object v3, LX/8Dp;->LIKE:LX/8Dp;

    new-instance v4, LX/C0b;

    invoke-direct {v4, p0, v0}, LX/C0b;-><init>(LX/C0d;Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V

    new-instance v0, LX/C0c;

    invoke-direct {v0, p0}, LX/C0c;-><init>(LX/C0d;)V

    invoke-virtual {v1, v3, v4, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1841095
    const v0, -0x794824e

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1841096
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1841097
    :cond_1
    sget-object v1, LX/C0a;->LIKE_PAGE:LX/C0a;

    goto :goto_1
.end method
