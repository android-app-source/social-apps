.class public LX/BgV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1807647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/97f;I)LX/97f;
    .locals 4

    .prologue
    .line 1807635
    invoke-static {p0}, LX/Bgb;->e(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    .line 1807636
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 1807637
    :cond_0
    :goto_0
    return-object p0

    .line 1807638
    :cond_1
    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    .line 1807639
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v1

    .line 1807640
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1807641
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    if-ge v2, p0, :cond_3

    .line 1807642
    if-eq v2, p1, :cond_2

    .line 1807643
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807644
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1807645
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1807646
    invoke-static {v0, v1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/97f;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/97f;
    .locals 3

    .prologue
    .line 1807624
    invoke-static {p0}, LX/Bgb;->e(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    .line 1807625
    if-nez v0, :cond_0

    .line 1807626
    :goto_0
    return-object p0

    .line 1807627
    :cond_0
    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    .line 1807628
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v2

    .line 1807629
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    .line 1807630
    const/4 p2, 0x0

    invoke-virtual {v1, p2, p1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1807631
    invoke-virtual {p0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807632
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p2

    invoke-virtual {v1, p1, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1807633
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    move-object v1, p0

    .line 1807634
    invoke-static {v0, v1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97f;
    .locals 2

    .prologue
    .line 1807618
    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    .line 1807619
    new-instance v1, LX/97l;

    invoke-direct {v1}, LX/97l;-><init>()V

    .line 1807620
    iput-object p1, v1, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1807621
    move-object v1, v1

    .line 1807622
    invoke-virtual {v1}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1807623
    invoke-static {v0, v1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/97f;
    .locals 1

    .prologue
    .line 1807616
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1807617
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0, v0, p1}, LX/BgV;->a(LX/97f;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/97f;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/97f;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)LX/97f;
    .locals 1

    .prologue
    .line 1807526
    new-instance v0, LX/97w;

    invoke-direct {v0}, LX/97w;-><init>()V

    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    invoke-static {v0}, LX/97w;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;)LX/97w;

    move-result-object v0

    .line 1807527
    iput-object p1, v0, LX/97w;->g:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 1807528
    move-object v0, v0

    .line 1807529
    invoke-virtual {v0}, LX/97w;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/97f;Lcom/facebook/ipc/model/PageTopic;)LX/97f;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1807593
    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    .line 1807594
    if-nez v0, :cond_1

    .line 1807595
    :cond_0
    :goto_0
    return-object p0

    .line 1807596
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v5

    .line 1807597
    iget-wide v0, p1, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 1807598
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_7

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    .line 1807599
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1807600
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 1807601
    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    if-eqz v1, :cond_5

    .line 1807602
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1807603
    invoke-virtual {v8, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_3
    if-eqz v1, :cond_6

    .line 1807604
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1807605
    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_4
    if-nez v0, :cond_0

    .line 1807606
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    move v1, v3

    .line 1807607
    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_4

    .line 1807608
    :cond_7
    iget-object v0, p1, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    new-instance v1, LX/186;

    const/16 v4, 0x400

    invoke-direct {v1, v4}, LX/186;-><init>(I)V

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, LX/186;->c(I)V

    invoke-virtual {v1, v3, v4}, LX/186;->b(II)V

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    const v0, 0x31f7b0b9

    invoke-static {v1, v0}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1807609
    new-instance v2, LX/97m;

    invoke-direct {v2}, LX/97m;-><init>()V

    invoke-virtual {v2, v1, v0}, LX/97m;->a(LX/15i;I)LX/97m;

    move-result-object v0

    invoke-virtual {v0}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1807610
    new-instance v1, LX/97l;

    invoke-direct {v1}, LX/97l;-><init>()V

    .line 1807611
    iput-object v0, v1, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1807612
    move-object v0, v1

    .line 1807613
    invoke-virtual {v0}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v0

    .line 1807614
    invoke-static {p0, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/97f;

    move-result-object p0

    goto/16 :goto_0

    .line 1807615
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/97f;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/97f;
    .locals 9
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x0

    .line 1807574
    invoke-static {p0}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v8

    .line 1807575
    if-eqz v8, :cond_0

    if-nez p1, :cond_1

    .line 1807576
    :cond_0
    :goto_0
    return-object p0

    .line 1807577
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1807578
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v2

    .line 1807579
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v0

    move-wide v6, v0

    .line 1807580
    :goto_1
    new-instance v0, LX/186;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    const/4 v1, 0x1

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    const v1, -0x1327ce11

    invoke-static {v0, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1807581
    new-instance v2, LX/97h;

    invoke-direct {v2}, LX/97h;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v3

    .line 1807582
    iput-object v3, v2, LX/97h;->a:Ljava/lang/String;

    .line 1807583
    move-object v2, v2

    .line 1807584
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 1807585
    iput-object v3, v2, LX/97h;->d:Ljava/lang/String;

    .line 1807586
    move-object v2, v2

    .line 1807587
    invoke-virtual {v2, v1, v0}, LX/97h;->a(LX/15i;I)LX/97h;

    move-result-object v0

    invoke-virtual {v0}, LX/97h;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    .line 1807588
    new-instance v1, LX/97m;

    invoke-direct {v1}, LX/97m;-><init>()V

    invoke-static {v8}, LX/97m;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97m;

    move-result-object v1

    .line 1807589
    iput-object v0, v1, LX/97m;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    .line 1807590
    move-object v0, v1

    .line 1807591
    invoke-virtual {v0}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1807592
    invoke-static {p0, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97f;

    move-result-object p0

    goto :goto_0

    :cond_2
    move-wide v6, v4

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;
    .locals 1

    .prologue
    .line 1807570
    new-instance v0, LX/97m;

    invoke-direct {v0}, LX/97m;-><init>()V

    invoke-static {p0}, LX/97m;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97m;

    move-result-object v0

    .line 1807571
    iput-object p1, v0, LX/97m;->m:Ljava/lang/String;

    .line 1807572
    move-object v0, v0

    .line 1807573
    invoke-virtual {v0}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;",
            ">;)",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;"
        }
    .end annotation

    .prologue
    .line 1807562
    new-instance v0, LX/97k;

    invoke-direct {v0}, LX/97k;-><init>()V

    .line 1807563
    iput-object p1, v0, LX/97k;->a:LX/0Px;

    .line 1807564
    move-object v0, v0

    .line 1807565
    invoke-virtual {v0}, LX/97k;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    .line 1807566
    new-instance v1, LX/97j;

    invoke-direct {v1}, LX/97j;-><init>()V

    invoke-static {p0}, LX/97j;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)LX/97j;

    move-result-object v1

    .line 1807567
    iput-object v0, v1, LX/97j;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    .line 1807568
    move-object v0, v1

    .line 1807569
    invoke-virtual {v0}, LX/97j;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;",
            ">;)",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;"
        }
    .end annotation

    .prologue
    .line 1807557
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->l()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-static {v0, p1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    .line 1807558
    new-instance v1, LX/97w;

    invoke-direct {v1}, LX/97w;-><init>()V

    invoke-static {p0}, LX/97w;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;)LX/97w;

    move-result-object v1

    .line 1807559
    iput-object v0, v1, LX/97w;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1807560
    move-object v0, v1

    .line 1807561
    invoke-virtual {v0}, LX/97w;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;
    .locals 9

    .prologue
    const v8, 0x7a3687e0

    const/16 v7, 0x400

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1807530
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    .line 1807531
    invoke-static {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v1

    .line 1807532
    new-instance v2, LX/186;

    invoke-direct {v2, v7}, LX/186;-><init>(I)V

    invoke-virtual {v2, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v6}, LX/186;->c(I)V

    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    invoke-static {v2, v8}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1807533
    new-instance v4, LX/186;

    invoke-direct {v4, v7}, LX/186;-><init>(I)V

    invoke-static {v3, v2, v8, v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    invoke-virtual {v4, v6}, LX/186;->c(I)V

    invoke-virtual {v4, v5, v2}, LX/186;->b(II)V

    const v2, 0x72e9906d

    invoke-static {v4, v2}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1807534
    new-instance v4, LX/97m;

    invoke-direct {v4}, LX/97m;-><init>()V

    invoke-virtual {v4, v3, v2}, LX/97m;->b(LX/15i;I)LX/97m;

    move-result-object v2

    invoke-virtual {v2}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1807535
    new-instance v3, LX/97l;

    invoke-direct {v3}, LX/97l;-><init>()V

    .line 1807536
    iput-object v2, v3, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1807537
    move-object v2, v3

    .line 1807538
    invoke-virtual {v2}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v2

    .line 1807539
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v3

    .line 1807540
    new-instance v4, LX/97k;

    invoke-direct {v4}, LX/97k;-><init>()V

    .line 1807541
    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/97k;->a:LX/0Px;

    .line 1807542
    move-object v3, v4

    .line 1807543
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1807544
    iput-object v2, v3, LX/97k;->a:LX/0Px;

    .line 1807545
    move-object v2, v3

    .line 1807546
    invoke-virtual {v2}, LX/97k;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v2

    .line 1807547
    invoke-static {v0}, LX/97j;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)LX/97j;

    move-result-object v0

    .line 1807548
    iput-object v2, v0, LX/97j;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    .line 1807549
    move-object v0, v0

    .line 1807550
    invoke-virtual {v0}, LX/97j;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    .line 1807551
    invoke-static {v1}, LX/97z;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)LX/97z;

    move-result-object v1

    .line 1807552
    iput-object v0, v1, LX/97z;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1807553
    move-object v0, v1

    .line 1807554
    invoke-virtual {v0}, LX/97z;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    return-object v0

    .line 1807555
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1807556
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
