.class public final LX/CaO;
.super LX/7UT;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917498
    iput-object p1, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, LX/7UT;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1917499
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "double_tap_on_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917500
    return-void
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 3

    .prologue
    .line 1917501
    invoke-super {p0, p1, p2}, LX/7UT;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 1917502
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-nez v0, :cond_1

    .line 1917503
    :cond_0
    :goto_0
    return-void

    .line 1917504
    :cond_1
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    .line 1917505
    iget-boolean v1, v0, LX/CbH;->g:Z

    move v0, v1

    .line 1917506
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getCurrentMedia()LX/5kD;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getCurrentMedia()LX/5kD;

    move-result-object v0

    invoke-interface {v0}, LX/5kD;->r()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1917507
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "single_tap_on_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917508
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    invoke-virtual {v0}, LX/CbH;->d()V

    goto :goto_0

    .line 1917509
    :cond_2
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    .line 1917510
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1917511
    if-nez v0, :cond_0

    .line 1917512
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "single_tap_on_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917513
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)LX/8Hs;

    move-result-object v0

    invoke-virtual {v0}, LX/8Hs;->b()V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 3

    .prologue
    .line 1917514
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    .line 1917515
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1917516
    if-eqz v0, :cond_1

    .line 1917517
    :cond_0
    :goto_0
    return-void

    .line 1917518
    :cond_1
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "long_press_on_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917519
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    sget-object v1, LX/74P;->LONGPRESS:LX/74P;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/74P;LX/0am;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1917520
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "drag_to_scale_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917521
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1917522
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-nez v0, :cond_1

    .line 1917523
    :cond_0
    :goto_0
    return-void

    .line 1917524
    :cond_1
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    .line 1917525
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1917526
    if-nez v0, :cond_0

    .line 1917527
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)LX/8Hs;

    move-result-object v0

    invoke-virtual {v0}, LX/8Hs;->c()V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1917528
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-nez v0, :cond_1

    .line 1917529
    :cond_0
    :goto_0
    return-void

    .line 1917530
    :cond_1
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->m:LX/CbH;

    .line 1917531
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1917532
    if-nez v0, :cond_0

    .line 1917533
    iget-object v0, p0, LX/CaO;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)LX/8Hs;

    move-result-object v0

    invoke-virtual {v0}, LX/8Hs;->d()V

    goto :goto_0
.end method
