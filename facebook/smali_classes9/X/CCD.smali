.class public final LX/CCD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CCE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pr;

.field public final synthetic d:LX/CCE;


# direct methods
.method public constructor <init>(LX/CCE;)V
    .locals 1

    .prologue
    .line 1857376
    iput-object p1, p0, LX/CCD;->d:LX/CCE;

    .line 1857377
    move-object v0, p1

    .line 1857378
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1857379
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1857375
    const-string v0, "RichTextPickerStyleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1857358
    if-ne p0, p1, :cond_1

    .line 1857359
    :cond_0
    :goto_0
    return v0

    .line 1857360
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1857361
    goto :goto_0

    .line 1857362
    :cond_3
    check-cast p1, LX/CCD;

    .line 1857363
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1857364
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1857365
    if-eq v2, v3, :cond_0

    .line 1857366
    iget-object v2, p0, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v3, p1, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1857367
    goto :goto_0

    .line 1857368
    :cond_5
    iget-object v2, p1, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-nez v2, :cond_4

    .line 1857369
    :cond_6
    iget-object v2, p0, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1857370
    goto :goto_0

    .line 1857371
    :cond_8
    iget-object v2, p1, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1857372
    :cond_9
    iget-object v2, p0, LX/CCD;->c:LX/1Pr;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CCD;->c:LX/1Pr;

    iget-object v3, p1, LX/CCD;->c:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1857373
    goto :goto_0

    .line 1857374
    :cond_a
    iget-object v2, p1, LX/CCD;->c:LX/1Pr;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
