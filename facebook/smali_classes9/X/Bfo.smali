.class public final LX/Bfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/Bfc",
        "<TT;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bfc;

.field public final synthetic b:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic c:LX/BeU;


# direct methods
.method public constructor <init>(LX/BeU;LX/Bfc;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1806611
    iput-object p1, p0, LX/Bfo;->c:LX/BeU;

    iput-object p2, p0, LX/Bfo;->a:LX/Bfc;

    iput-object p3, p0, LX/Bfo;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1806612
    iget-object v0, p0, LX/Bfo;->c:LX/BeU;

    iget-object v0, v0, LX/BeU;->k:LX/03V;

    sget-object v1, LX/BeU;->a:Ljava/lang/String;

    const-string v2, "Top card failed to load"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806613
    iget-object v0, p0, LX/Bfo;->a:LX/Bfc;

    new-instance v1, LX/Bfn;

    invoke-direct {v1, p0}, LX/Bfn;-><init>(LX/Bfo;)V

    .line 1806614
    iget-object v2, v0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, LX/Bfc;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f08293a

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1806615
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1806616
    iget-object v0, p0, LX/Bfo;->c:LX/BeU;

    const/4 v2, 0x0

    .line 1806617
    iget-boolean v1, v0, LX/BeU;->c:Z

    if-nez v1, :cond_1

    move v1, v2

    .line 1806618
    :goto_0
    invoke-virtual {v0}, LX/BeU;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1806619
    invoke-static {v0, v1}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v3

    .line 1806620
    iget v8, v3, LX/Bfc;->f:I

    .line 1806621
    iget v4, v3, LX/Bfc;->g:I

    if-eqz v4, :cond_2

    iget v9, v3, LX/Bfc;->g:I

    .line 1806622
    :goto_1
    iget v4, v3, LX/Bfc;->b:I

    iget v5, v3, LX/Bfc;->a:I

    mul-int v6, v4, v5

    .line 1806623
    iget v4, v3, LX/Bfc;->b:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, LX/Bfc;->b:I

    iget v5, v3, LX/Bfc;->a:I

    mul-int v7, v4, v5

    .line 1806624
    invoke-static {v3}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3}, LX/Bfc;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f08293d

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/Bg1;->a(Landroid/view/View;Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 1806625
    new-instance v4, LX/Bfa;

    move-object v5, v3

    invoke-direct/range {v4 .. v10}, LX/Bfa;-><init>(LX/Bfc;IIIILX/0Px;)V

    .line 1806626
    invoke-virtual {v3}, LX/Bfc;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0052

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1806627
    new-instance v5, LX/Bfb;

    invoke-direct {v5, v3}, LX/Bfb;-><init>(LX/Bfc;)V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1806628
    invoke-static {v3}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1806629
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1806630
    :cond_0
    invoke-static {v0}, LX/BeU;->f(LX/BeU;)V

    .line 1806631
    invoke-static {v0, v2}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v1

    invoke-virtual {v1}, LX/Bfc;->requestFocus()Z

    .line 1806632
    :cond_1
    iput-boolean v2, v0, LX/BeU;->c:Z

    .line 1806633
    iget v1, v0, LX/BeU;->b:I

    invoke-virtual {v0, v1}, LX/BeU;->b(I)V

    .line 1806634
    return-void

    :cond_2
    move v9, v8

    .line 1806635
    goto :goto_1
.end method
