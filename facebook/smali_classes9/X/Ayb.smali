.class public LX/Ayb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kK;


# instance fields
.field public final a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)V
    .locals 1

    .prologue
    .line 1730191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730192
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ayb;->b:Z

    .line 1730193
    iput-object p1, p0, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1730194
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1730195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "647619478700283:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1730196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/5ob;
    .locals 1

    .prologue
    .line 1730197
    sget-object v0, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1730198
    iget-object v0, p0, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1730199
    instance-of v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    if-eqz v1, :cond_0

    .line 1730200
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1730201
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v1

    .line 1730202
    :goto_0
    return-object v0

    .line 1730203
    :cond_0
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1730204
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v1

    .line 1730205
    goto :goto_0
.end method
