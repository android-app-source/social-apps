.class public final synthetic LX/AkY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1709213
    invoke-static {}, LX/2qY;->values()[LX/2qY;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AkY;->d:[I

    :try_start_0
    sget-object v0, LX/AkY;->d:[I

    sget-object v1, LX/2qY;->SAVE_OFFLINE:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_0
    :try_start_1
    sget-object v0, LX/AkY;->d:[I

    sget-object v1, LX/2qY;->DOWNLOAD:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_1
    :try_start_2
    sget-object v0, LX/AkY;->d:[I

    sget-object v1, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    .line 1709214
    :goto_2
    invoke-static {}, LX/0wD;->values()[LX/0wD;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AkY;->c:[I

    :try_start_3
    sget-object v0, LX/AkY;->c:[I

    sget-object v1, LX/0wD;->PERMALINK:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_3
    :try_start_4
    sget-object v0, LX/AkY;->c:[I

    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    .line 1709215
    :goto_4
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->values()[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AkY;->b:[I

    :try_start_5
    sget-object v0, LX/AkY;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_5
    :try_start_6
    sget-object v0, LX/AkY;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->GROUP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_6
    :try_start_7
    sget-object v0, LX/AkY;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    .line 1709216
    :goto_7
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->values()[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AkY;->a:[I

    :try_start_8
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_8
    :try_start_9
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_9
    :try_start_a
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_a
    :try_start_b
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_b
    :try_start_c
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_c
    :try_start_d
    sget-object v0, LX/AkY;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_d
    return-void

    :catch_0
    goto :goto_d

    :catch_1
    goto :goto_c

    :catch_2
    goto :goto_b

    :catch_3
    goto :goto_a

    :catch_4
    goto :goto_9

    :catch_5
    goto :goto_8

    :catch_6
    goto :goto_7

    :catch_7
    goto :goto_6

    :catch_8
    goto :goto_5

    :catch_9
    goto :goto_4

    :catch_a
    goto/16 :goto_3

    :catch_b
    goto/16 :goto_2

    :catch_c
    goto/16 :goto_1

    :catch_d
    goto/16 :goto_0
.end method
