.class public final LX/BkP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/BkU;


# direct methods
.method public constructor <init>(LX/BkU;)V
    .locals 0

    .prologue
    .line 1814194
    iput-object p1, p0, LX/BkP;->a:LX/BkU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    .line 1814195
    iget-object v0, p0, LX/BkP;->a:LX/BkU;

    iget-object v0, v0, LX/BkU;->b:LX/BiY;

    iget-object v1, p0, LX/BkP;->a:LX/BkU;

    iget-object v1, v1, LX/BkU;->j:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814196
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v1, v2

    .line 1814197
    iget-object v2, p0, LX/BkP;->a:LX/BkU;

    iget-object v2, v2, LX/BkU;->k:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v2}, LX/BkU;->c(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v2

    iget-object v3, p0, LX/BkP;->a:LX/BkU;

    iget-object v3, v3, LX/BkU;->h:LX/9el;

    .line 1814198
    iput-object v1, v0, LX/BiY;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814199
    iput-boolean v2, v0, LX/BiY;->h:Z

    .line 1814200
    iput-object v3, v0, LX/BiY;->i:LX/9el;

    .line 1814201
    iget-object v0, p0, LX/BkP;->a:LX/BkU;

    iget-object v0, v0, LX/BkU;->b:LX/BiY;

    const/4 v6, 0x0

    const/4 v10, 0x0

    .line 1814202
    iget-object v4, v0, LX/BiY;->a:Landroid/content/Context;

    const-class v5, LX/0ew;

    invoke-static {v4, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ew;

    .line 1814203
    if-eqz v4, :cond_0

    invoke-interface {v4}, LX/0ew;->iC_()LX/0gc;

    move-result-object v5

    invoke-virtual {v5}, LX/0gc;->c()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move v4, v6

    .line 1814204
    :goto_0
    move v0, v4

    .line 1814205
    return v0

    .line 1814206
    :cond_1
    new-instance v5, LX/9fD;

    invoke-interface {v4}, LX/0ew;->iC_()LX/0gc;

    move-result-object v4

    invoke-direct {v5, v4}, LX/9fD;-><init>(LX/0gc;)V

    iput-object v5, v0, LX/BiY;->f:LX/9fD;

    .line 1814207
    iget-object v4, v0, LX/BiY;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814208
    iget-object v5, v4, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    move-object v4, v5

    .line 1814209
    if-eqz v4, :cond_2

    iget-object v4, v0, LX/BiY;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814210
    iget-object v5, v4, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    move-object v5, v5

    .line 1814211
    :goto_1
    if-nez v5, :cond_3

    move v4, v6

    .line 1814212
    goto :goto_0

    .line 1814213
    :cond_2
    iget-object v4, v0, LX/BiY;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814214
    iget-object v5, v4, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    move-object v5, v5

    .line 1814215
    goto :goto_1

    .line 1814216
    :cond_3
    iget-object v4, v0, LX/BiY;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0b1941

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 1814217
    invoke-static {v5, v10}, LX/74d;->a(Landroid/net/Uri;Lcom/facebook/photos/base/media/PhotoItem;)F

    move-result v4

    .line 1814218
    int-to-float v7, v6

    div-float v4, v7, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1814219
    iget-object v4, v0, LX/BiY;->c:LX/8GP;

    invoke-virtual {v4, v6, v7}, LX/8GP;->a(II)LX/8GO;

    move-result-object v4

    iget-object v8, v0, LX/BiY;->b:LX/8GN;

    invoke-virtual {v8}, LX/8GN;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/8GO;->a(LX/0Px;)LX/8GO;

    move-result-object v4

    invoke-virtual {v4}, LX/8GO;->b()LX/0Px;

    move-result-object v8

    .line 1814220
    new-instance v9, LX/5Rw;

    invoke-direct {v9}, LX/5Rw;-><init>()V

    const/4 v4, 0x0

    .line 1814221
    iget-boolean v11, v0, LX/BiY;->h:Z

    if-eqz v11, :cond_4

    iget-object v11, v0, LX/BiY;->d:LX/0ad;

    sget-short v12, LX/8Fn;->a:S

    invoke-interface {v11, v12, v4}, LX/0ad;->a(SZ)Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v4, 0x1

    :cond_4
    move v4, v4

    .line 1814222
    if-nez v4, :cond_5

    sget-object v4, LX/5Rr;->DOODLE:LX/5Rr;

    :goto_2
    invoke-virtual {v9, v4}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v9

    iget-boolean v4, v0, LX/BiY;->h:Z

    if-nez v4, :cond_6

    sget-object v4, LX/5Rr;->STICKER:LX/5Rr;

    :goto_3
    invoke-virtual {v9, v4}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v9

    iget-boolean v4, v0, LX/BiY;->e:Z

    if-nez v4, :cond_7

    sget-object v4, LX/5Rr;->FILTER:LX/5Rr;

    :goto_4
    invoke-virtual {v9, v4}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v4

    sget-object v9, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v4, v9}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v4

    .line 1814223
    iput-object v8, v4, LX/5Rw;->l:LX/0Px;

    .line 1814224
    move-object v4, v4

    .line 1814225
    invoke-virtual {v4}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v8

    .line 1814226
    iget-object v4, v0, LX/BiY;->f:LX/9fD;

    iget-object v9, v0, LX/BiY;->i:LX/9el;

    move-object v11, v10

    invoke-virtual/range {v4 .. v11}, LX/9fD;->a(Landroid/net/Uri;IILcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;LX/9el;Ljava/util/List;LX/9fh;)V

    .line 1814227
    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_5
    move-object v4, v10

    .line 1814228
    goto :goto_2

    :cond_6
    move-object v4, v10

    goto :goto_3

    :cond_7
    move-object v4, v10

    goto :goto_4
.end method
