.class public final LX/B2w;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B2y;


# direct methods
.method public constructor <init>(LX/B2y;)V
    .locals 0

    .prologue
    .line 1739339
    iput-object p1, p0, LX/B2w;->a:LX/B2y;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1739341
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739342
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1739343
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739344
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1739345
    iget-object v2, p0, LX/B2w;->a:LX/B2y;

    invoke-virtual {v2, v1, v0}, LX/B2y;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1739346
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739347
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1739348
    iget-object v0, p0, LX/B2w;->a:LX/B2y;

    iget-object v0, v0, LX/B2y;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "heisman_fetch_self_profile_picture_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739349
    :cond_0
    iget-object v0, p0, LX/B2w;->a:LX/B2y;

    invoke-static {v0}, LX/B2y;->c(LX/B2y;)V

    .line 1739350
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739340
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/B2w;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
