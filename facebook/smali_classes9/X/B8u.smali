.class public final enum LX/B8u;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B8u;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B8u;

.field public static final enum AUTOFILL:LX/B8u;

.field public static final enum GET:LX/B8u;

.field public static final enum HASH:LX/B8u;

.field public static final enum NONE:LX/B8u;

.field public static final enum POST:LX/B8u;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1750723
    new-instance v0, LX/B8u;

    const-string v1, "POST"

    const-string v2, "POST"

    invoke-direct {v0, v1, v3, v2}, LX/B8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B8u;->POST:LX/B8u;

    .line 1750724
    new-instance v0, LX/B8u;

    const-string v1, "GET"

    const-string v2, "GET"

    invoke-direct {v0, v1, v4, v2}, LX/B8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B8u;->GET:LX/B8u;

    .line 1750725
    new-instance v0, LX/B8u;

    const-string v1, "HASH"

    const-string v2, "HASH"

    invoke-direct {v0, v1, v5, v2}, LX/B8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B8u;->HASH:LX/B8u;

    .line 1750726
    new-instance v0, LX/B8u;

    const-string v1, "AUTOFILL"

    const-string v2, "AUTOFILL"

    invoke-direct {v0, v1, v6, v2}, LX/B8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B8u;->AUTOFILL:LX/B8u;

    .line 1750727
    new-instance v0, LX/B8u;

    const-string v1, "NONE"

    const-string v2, ""

    invoke-direct {v0, v1, v7, v2}, LX/B8u;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B8u;->NONE:LX/B8u;

    .line 1750728
    const/4 v0, 0x5

    new-array v0, v0, [LX/B8u;

    sget-object v1, LX/B8u;->POST:LX/B8u;

    aput-object v1, v0, v3

    sget-object v1, LX/B8u;->GET:LX/B8u;

    aput-object v1, v0, v4

    sget-object v1, LX/B8u;->HASH:LX/B8u;

    aput-object v1, v0, v5

    sget-object v1, LX/B8u;->AUTOFILL:LX/B8u;

    aput-object v1, v0, v6

    sget-object v1, LX/B8u;->NONE:LX/B8u;

    aput-object v1, v0, v7

    sput-object v0, LX/B8u;->$VALUES:[LX/B8u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1750729
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1750730
    iput-object p3, p0, LX/B8u;->name:Ljava/lang/String;

    .line 1750731
    return-void
.end method

.method public static fromValue(Ljava/lang/String;)LX/B8u;
    .locals 5

    .prologue
    .line 1750732
    invoke-static {}, LX/B8u;->values()[LX/B8u;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1750733
    iget-object v4, v0, LX/B8u;->name:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1750734
    :goto_1
    return-object v0

    .line 1750735
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1750736
    :cond_1
    sget-object v0, LX/B8u;->NONE:LX/B8u;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/B8u;
    .locals 1

    .prologue
    .line 1750737
    const-class v0, LX/B8u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B8u;

    return-object v0
.end method

.method public static values()[LX/B8u;
    .locals 1

    .prologue
    .line 1750738
    sget-object v0, LX/B8u;->$VALUES:[LX/B8u;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B8u;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1750739
    iget-object v0, p0, LX/B8u;->name:Ljava/lang/String;

    return-object v0
.end method
