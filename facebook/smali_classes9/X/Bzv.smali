.class public final LX/Bzv;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/Bzv;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bzt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Bzw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1839641
    const/4 v0, 0x0

    sput-object v0, LX/Bzv;->a:LX/Bzv;

    .line 1839642
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bzv;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1839643
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 1839644
    new-instance v0, LX/Bzw;

    invoke-direct {v0}, LX/Bzw;-><init>()V

    iput-object v0, p0, LX/Bzv;->c:LX/Bzw;

    .line 1839645
    return-void
.end method

.method public static declared-synchronized a()LX/Bzv;
    .locals 2

    .prologue
    .line 1839646
    const-class v1, LX/Bzv;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bzv;->a:LX/Bzv;

    if-nez v0, :cond_0

    .line 1839647
    new-instance v0, LX/Bzv;

    invoke-direct {v0}, LX/Bzv;-><init>()V

    sput-object v0, LX/Bzv;->a:LX/Bzv;

    .line 1839648
    :cond_0
    sget-object v0, LX/Bzv;->a:LX/Bzv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1839649
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1839650
    check-cast p2, LX/Bzu;

    .line 1839651
    iget-object v0, p2, LX/Bzu;->a:Ljava/lang/String;

    iget-object v1, p2, LX/Bzu;->b:LX/8uh;

    iget-object v2, p2, LX/Bzu;->c:Landroid/graphics/Typeface;

    iget v3, p2, LX/Bzu;->d:I

    iget v4, p2, LX/Bzu;->e:I

    iget v5, p2, LX/Bzu;->f:F

    iget v6, p2, LX/Bzu;->g:I

    iget v7, p2, LX/Bzu;->h:F

    .line 1839652
    sget-object p0, LX/Bzw;->b:LX/0Zk;

    invoke-interface {p0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/8ui;

    .line 1839653
    if-nez p0, :cond_0

    .line 1839654
    new-instance p0, LX/8ui;

    invoke-direct {p0}, LX/8ui;-><init>()V

    .line 1839655
    :cond_0
    invoke-virtual {p0, v0}, LX/8ui;->a(Ljava/lang/String;)Z

    .line 1839656
    iput-object v1, p0, LX/8ui;->k:LX/8uh;

    .line 1839657
    invoke-virtual {p0, v4}, LX/8ui;->c(I)V

    .line 1839658
    invoke-virtual {p0, v2}, LX/8ui;->a(Landroid/graphics/Typeface;)V

    .line 1839659
    invoke-virtual {p0, v3}, LX/8ui;->b(I)V

    .line 1839660
    invoke-virtual {p0, v5}, LX/8ui;->a(F)V

    .line 1839661
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8ui;->g:Z

    .line 1839662
    iput v6, p0, LX/8ui;->h:I

    .line 1839663
    iput v7, p0, LX/8ui;->i:F

    .line 1839664
    move-object v0, p0

    .line 1839665
    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 0

    .prologue
    .line 1839666
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1839667
    sget-object p0, LX/Bzw;->b:LX/0Zk;

    check-cast p2, LX/8ui;

    invoke-interface {p0, p2}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 1839668
    return-void
.end method
