.class public LX/Brq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/0lC;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1826738
    const-class v0, LX/Brq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Brq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826735
    iput-object p1, p0, LX/Brq;->b:LX/0lC;

    .line 1826736
    iput-object p2, p0, LX/Brq;->c:LX/03V;

    .line 1826737
    return-void
.end method

.method public static a(LX/0QB;)LX/Brq;
    .locals 5

    .prologue
    .line 1826723
    const-class v1, LX/Brq;

    monitor-enter v1

    .line 1826724
    :try_start_0
    sget-object v0, LX/Brq;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826725
    sput-object v2, LX/Brq;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826726
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826727
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826728
    new-instance p0, LX/Brq;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/Brq;-><init>(LX/0lC;LX/03V;)V

    .line 1826729
    move-object v0, p0

    .line 1826730
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826731
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Brq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826732
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
