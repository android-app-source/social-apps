.class public LX/BI6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final d:Landroid/content/Context;

.field public final e:LX/0iA;

.field public final f:Landroid/view/View;

.field public final g:Z

.field public final h:Z

.field public final i:I

.field public final j:LX/BHh;

.field public final k:Ljava/lang/String;

.field private l:LX/3ko;

.field public m:LX/3kn;

.field public n:LX/3l0;

.field public o:LX/3kr;

.field public p:LX/3lG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1770007
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/BI6;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1770008
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_DETECTED_RECENT_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/BI6;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1770009
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_HIGHLIGHT_CLUSTER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/BI6;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0iA;Landroid/view/View;ZZILX/BHh;Ljava/lang/String;)V
    .locals 0
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/BHh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769998
    iput-object p1, p0, LX/BI6;->d:Landroid/content/Context;

    .line 1769999
    iput-object p2, p0, LX/BI6;->e:LX/0iA;

    .line 1770000
    iput-boolean p4, p0, LX/BI6;->g:Z

    .line 1770001
    iput-object p3, p0, LX/BI6;->f:Landroid/view/View;

    .line 1770002
    iput-boolean p5, p0, LX/BI6;->h:Z

    .line 1770003
    iput p6, p0, LX/BI6;->i:I

    .line 1770004
    iput-object p7, p0, LX/BI6;->j:LX/BHh;

    .line 1770005
    iput-object p8, p0, LX/BI6;->k:Ljava/lang/String;

    .line 1770006
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1769968
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    const-string v1, "3883"

    const-class v2, LX/3kn;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kn;

    iput-object v0, p0, LX/BI6;->m:LX/3kn;

    .line 1769969
    iget-object v0, p0, LX/BI6;->m:LX/3kn;

    iget-object v1, p0, LX/BI6;->f:Landroid/view/View;

    iget-boolean v2, p0, LX/BI6;->g:Z

    .line 1769970
    iput-boolean v2, v0, LX/3kn;->e:Z

    .line 1769971
    iget-object v3, v0, LX/3kn;->b:LX/3kp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, LX/BI3;->forControllerClass(Ljava/lang/Class;)LX/BI3;

    move-result-object v4

    iget-object v4, v4, LX/BI3;->prefKey:LX/0Tn;

    invoke-virtual {v3, v4}, LX/3kp;->a(LX/0Tn;)V

    .line 1769972
    const v3, 0x7f0d00f3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, LX/3kn;->d:Landroid/view/View;

    .line 1769973
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    const-string v1, "4369"

    const-class v2, LX/3lG;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3lG;

    iput-object v0, p0, LX/BI6;->p:LX/3lG;

    .line 1769974
    iget-object v0, p0, LX/BI6;->p:LX/3lG;

    iget-object v1, p0, LX/BI6;->f:Landroid/view/View;

    .line 1769975
    iget-object v2, v0, LX/3lG;->b:LX/3kp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, LX/BI3;->forControllerClass(Ljava/lang/Class;)LX/BI3;

    move-result-object v3

    iget-object v3, v3, LX/BI3;->prefKey:LX/0Tn;

    invoke-virtual {v2, v3}, LX/3kp;->a(LX/0Tn;)V

    .line 1769976
    iget-object v2, v0, LX/3lG;->b:LX/3kp;

    const/4 v3, 0x2

    .line 1769977
    iput v3, v2, LX/3kp;->b:I

    .line 1769978
    const v2, 0x7f0d00f3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v0, LX/3lG;->d:Landroid/view/View;

    .line 1769979
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    const-string v1, "4194"

    const-class v2, LX/3l0;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3l0;

    iput-object v0, p0, LX/BI6;->n:LX/3l0;

    .line 1769980
    iget-object v0, p0, LX/BI6;->n:LX/3l0;

    iget-boolean v1, p0, LX/BI6;->h:Z

    iget v2, p0, LX/BI6;->i:I

    iget-object v3, p0, LX/BI6;->j:LX/BHh;

    iget-object v4, p0, LX/BI6;->k:Ljava/lang/String;

    .line 1769981
    iget-object v5, v0, LX/3l0;->b:LX/3kp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, LX/BI3;->forControllerClass(Ljava/lang/Class;)LX/BI3;

    move-result-object v6

    iget-object v6, v6, LX/BI3;->prefKey:LX/0Tn;

    invoke-virtual {v5, v6}, LX/3kp;->a(LX/0Tn;)V

    .line 1769982
    iput-boolean v1, v0, LX/3l0;->f:Z

    .line 1769983
    iput-object v3, v0, LX/3l0;->h:LX/BHh;

    .line 1769984
    iput v2, v0, LX/3l0;->g:I

    .line 1769985
    iget-object v5, v0, LX/3l0;->c:LX/3l1;

    .line 1769986
    iput-object v4, v5, LX/3l1;->b:Ljava/lang/String;

    .line 1769987
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    const-string v1, "4169"

    const-class v2, LX/3kr;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kr;

    iput-object v0, p0, LX/BI6;->o:LX/3kr;

    .line 1769988
    iget-object v0, p0, LX/BI6;->o:LX/3kr;

    iget-object v1, p0, LX/BI6;->d:Landroid/content/Context;

    iget-object v2, p0, LX/BI6;->f:Landroid/view/View;

    .line 1769989
    iput-object v1, v0, LX/3kr;->e:Landroid/content/Context;

    .line 1769990
    iput-object v2, v0, LX/3kr;->f:Landroid/view/View;

    .line 1769991
    iget-object v3, v0, LX/3kr;->c:LX/3kp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, LX/BI3;->forControllerClass(Ljava/lang/Class;)LX/BI3;

    move-result-object v4

    iget-object v4, v4, LX/BI3;->prefKey:LX/0Tn;

    invoke-virtual {v3, v4}, LX/3kp;->a(LX/0Tn;)V

    .line 1769992
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    sget-object v1, LX/BI6;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3ko;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ko;

    iput-object v0, p0, LX/BI6;->l:LX/3ko;

    .line 1769993
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    if-eqz v0, :cond_0

    .line 1769994
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v1}, LX/3ko;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1769995
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v0}, LX/3ko;->e()V

    .line 1769996
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1769962
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    if-eqz v0, :cond_1

    .line 1769963
    :cond_0
    :goto_0
    return-void

    .line 1769964
    :cond_1
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    sget-object v1, LX/BI6;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3ko;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ko;

    iput-object v0, p0, LX/BI6;->l:LX/3ko;

    .line 1769965
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    if-eqz v0, :cond_0

    .line 1769966
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v1}, LX/3ko;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1769967
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v0}, LX/3ko;->e()V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1769956
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    if-eqz v0, :cond_1

    .line 1769957
    :cond_0
    :goto_0
    return-void

    .line 1769958
    :cond_1
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    sget-object v1, LX/BI6;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3ko;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ko;

    iput-object v0, p0, LX/BI6;->l:LX/3ko;

    .line 1769959
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    if-eqz v0, :cond_0

    .line 1769960
    iget-object v0, p0, LX/BI6;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v1}, LX/3ko;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1769961
    iget-object v0, p0, LX/BI6;->l:LX/3ko;

    invoke-virtual {v0}, LX/3ko;->e()V

    goto :goto_0
.end method
