.class public LX/BVk;
.super LX/BVZ;
.source ""


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791401
    invoke-direct {p0, p1, p2}, LX/BVZ;-><init>(LX/BW4;LX/BW5;)V

    .line 1791402
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1791418
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1791403
    check-cast p1, Landroid/widget/LinearLayout;

    .line 1791404
    sget-object v0, LX/BVi;->a:[I

    invoke-static {p2}, LX/BVj;->from(Ljava/lang/String;)LX/BVj;

    move-result-object v1

    invoke-virtual {v1}, LX/BVj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1791405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown linear layout attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791406
    :pswitch_0
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBaselineAligned(Z)V

    .line 1791407
    :goto_0
    return-void

    .line 1791408
    :pswitch_1
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBaselineAlignedChildIndex(I)V

    goto :goto_0

    .line 1791409
    :pswitch_2
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->p(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto :goto_0

    .line 1791410
    :pswitch_3
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1791411
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setMeasureWithLargestChildEnabled(Z)V

    .line 1791412
    :pswitch_4
    const-string v0, "horizontal"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791413
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0

    .line 1791414
    :cond_0
    const-string v0, "vertical"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1791415
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0

    .line 1791416
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "orientation attribute must be either \"horizontal\" or \"vertical\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791417
    :pswitch_5
    invoke-static {p3}, LX/BW4;->c(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
