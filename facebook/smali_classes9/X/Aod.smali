.class public final LX/Aod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9g5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9g5",
        "<",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

.field private b:Lcom/facebook/graphql/model/GraphQLStory;

.field private c:LX/Aoe;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;Lcom/facebook/graphql/model/GraphQLStory;LX/Aoe;)V
    .locals 0

    .prologue
    .line 1714659
    iput-object p1, p0, LX/Aod;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1714660
    iput-object p2, p0, LX/Aod;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714661
    iput-object p3, p0, LX/Aod;->c:LX/Aoe;

    .line 1714662
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1714663
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1714664
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    .line 1714665
    iget-object v3, p0, LX/Aod;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    const/4 v6, 0x0

    .line 1714666
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    :goto_1
    if-ge v7, v8, :cond_3

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1714667
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    iget-object v9, v3, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->d:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    .line 1714668
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v9, v3, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->d:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    invoke-static {v5, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1714669
    const/4 v5, 0x1

    .line 1714670
    :goto_2
    move v3, v5

    .line 1714671
    if-eqz v3, :cond_1

    iget-object v3, p0, LX/Aod;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    iget-object v4, p0, LX/Aod;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1714672
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1714673
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move v5, v6

    .line 1714674
    :goto_3
    move v0, v5

    .line 1714675
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Aod;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    .line 1714676
    iget-object v5, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->f:LX/1Wv;

    invoke-virtual {v5}, LX/1Wv;->d()I

    .line 1714677
    iget-object v5, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->f:LX/1Wv;

    invoke-virtual {v5}, LX/1Wv;->d()I

    move-result v5

    iget v6, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->k:I

    if-ge v5, v6, :cond_8

    iget-object v5, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->i:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iget-object v7, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->f:LX/1Wv;

    .line 1714678
    iget-object v9, v7, LX/1Wv;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/1Wv;->c:LX/0Tn;

    const-wide/16 v11, 0x0

    invoke-interface {v9, v10, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v9

    move-wide v7, v9

    .line 1714679
    sub-long/2addr v5, v7

    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-lez v5, :cond_8

    .line 1714680
    const/4 v5, 0x1

    .line 1714681
    :goto_4
    move v0, v5

    .line 1714682
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Aod;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1714683
    iget-object v0, p0, LX/Aod;->a:Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;

    iget-object v0, v0, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->n:Ljava/util/Set;

    iget-object v1, p0, LX/Aod;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1714684
    :cond_0
    return-void

    .line 1714685
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1714686
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_1

    :cond_3
    move v5, v6

    .line 1714687
    goto :goto_2

    .line 1714688
    :cond_4
    invoke-static {v4}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    if-nez v5, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1714689
    :goto_5
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    const v9, 0x285feb

    if-eq v8, v9, :cond_6

    .line 1714690
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move v5, v6

    .line 1714691
    goto :goto_3

    .line 1714692
    :cond_5
    invoke-static {v4}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    goto :goto_5

    .line 1714693
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    iget-object v8, v3, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellImpressionHelper;->d:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v5, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    if-gt v5, v7, :cond_7

    .line 1714694
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move v5, v6

    .line 1714695
    goto/16 :goto_3

    .line 1714696
    :cond_7
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move v5, v7

    .line 1714697
    goto/16 :goto_3

    :cond_8
    const/4 v5, 0x0

    goto :goto_4
.end method

.method public final a(LX/9g6;)V
    .locals 1

    .prologue
    .line 1714698
    sget-object v0, LX/9g6;->CLOSED:LX/9g6;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/9g6;->DONE:LX/9g6;

    if-eq p1, v0, :cond_0

    .line 1714699
    :cond_0
    return-void
.end method
