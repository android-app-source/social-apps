.class public LX/Amb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/55N;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/55T;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/55H;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/827;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/55b;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/55Z;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/80O;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/81B;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/81m;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/81n;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5ul;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6UR;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public final o:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

.field public final p:LX/22l;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6UT;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Tv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/22l;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/0Ot",
            "<",
            "LX/827;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/55N;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/55T;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/55H;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/55b;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/55Z;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/80O;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/81B;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/81m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/81n;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5ul;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6UR;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;",
            "Lcom/facebook/feed/server/NewsFeedServiceImplementation;",
            "LX/22l;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Tv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6UT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710954
    iput-object p1, p0, LX/Amb;->a:LX/18V;

    .line 1710955
    iput-object p3, p0, LX/Amb;->b:LX/0Ot;

    .line 1710956
    iput-object p4, p0, LX/Amb;->c:LX/0Ot;

    .line 1710957
    iput-object p5, p0, LX/Amb;->d:LX/0Ot;

    .line 1710958
    iput-object p2, p0, LX/Amb;->e:LX/0Ot;

    .line 1710959
    iput-object p6, p0, LX/Amb;->f:LX/0Ot;

    .line 1710960
    iput-object p7, p0, LX/Amb;->g:LX/0Ot;

    .line 1710961
    iput-object p8, p0, LX/Amb;->h:LX/0Ot;

    .line 1710962
    iput-object p9, p0, LX/Amb;->i:LX/0Ot;

    .line 1710963
    iput-object p10, p0, LX/Amb;->j:LX/0Ot;

    .line 1710964
    iput-object p11, p0, LX/Amb;->k:LX/0Ot;

    .line 1710965
    iput-object p12, p0, LX/Amb;->l:LX/0Ot;

    .line 1710966
    iput-object p13, p0, LX/Amb;->m:LX/0Ot;

    .line 1710967
    iput-object p14, p0, LX/Amb;->n:LX/0Ot;

    .line 1710968
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Amb;->o:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    .line 1710969
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Amb;->p:LX/22l;

    .line 1710970
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Amb;->q:LX/0Ot;

    .line 1710971
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Amb;->r:LX/0Ot;

    .line 1710972
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Amb;->s:LX/0Ot;

    .line 1710973
    return-void
.end method

.method public static a(LX/Amb;Lcom/facebook/api/feed/FetchFeedParams;)LX/0rB;
    .locals 4

    .prologue
    .line 1710974
    iget-object v0, p0, LX/Amb;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 1710975
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1710976
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v3

    .line 1710977
    iget-object v3, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v3, v3

    .line 1710978
    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1710979
    return-object v0

    .line 1710980
    :cond_1
    new-instance v0, LX/4B3;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown FeedType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1710981
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1710982
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v3

    .line 1710983
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(LX/0QB;)LX/Amb;
    .locals 22

    .prologue
    .line 1710984
    new-instance v2, LX/Amb;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    const/16 v4, 0x1cc6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1754

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1755

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1753

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1757

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1756

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1cba

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1cc0

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1cc3

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1cc4

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x329c

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x1c17

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0oa;->a(LX/0QB;)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a(LX/0QB;)Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    move-result-object v17

    check-cast v17, Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    invoke-static/range {p0 .. p0}, LX/22l;->a(LX/0QB;)LX/22l;

    move-result-object v18

    check-cast v18, LX/22l;

    const/16 v19, 0x3279

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x1c16

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1c18

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-direct/range {v2 .. v21}, LX/Amb;-><init>(LX/18V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/22l;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1710985
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1710986
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1710987
    invoke-static {v0}, LX/3nN;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1710988
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1710989
    const-string v1, "fetchFeedParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1710990
    invoke-static {p0, v0}, LX/Amb;->a(LX/Amb;Lcom/facebook/api/feed/FetchFeedParams;)LX/0rB;

    move-result-object v1

    .line 1710991
    new-instance v2, LX/Ama;

    invoke-direct {v2, p0, v1}, LX/Ama;-><init>(LX/Amb;LX/0rB;)V

    .line 1710992
    iget-object v1, p0, LX/Amb;->p:LX/22l;

    .line 1710993
    iget-object v3, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v3, v3

    .line 1710994
    invoke-virtual {v1, v0, v3, v2}, LX/22l;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;LX/232;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1710995
    :goto_0
    return-object v0

    .line 1710996
    :cond_0
    const-string v1, "feed_hide_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1710997
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1710998
    const-string v1, "hideFeedStoryParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;

    .line 1710999
    iget-boolean v1, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->e:Z

    if-eqz v1, :cond_13

    iget-object v1, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/StoryVisibility;->isHiddenOrVisible()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1711000
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1711001
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1711002
    :goto_1
    move-object v0, v0

    .line 1711003
    goto :goto_0

    .line 1711004
    :cond_1
    const-string v1, "feed_negative_feedback_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1711005
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711006
    const-string v1, "negativeFeedbackActionOnFeedParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;

    .line 1711007
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1711008
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1711009
    goto :goto_0

    .line 1711010
    :cond_2
    const-string v1, "feed_delete_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1711011
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711012
    const-string v1, "deleteStoryParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    .line 1711013
    :try_start_0
    iget-object v1, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    sget-object v2, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    if-ne v1, v2, :cond_3

    .line 1711014
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    .line 1711015
    :cond_3
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711016
    :goto_2
    move-object v0, v0

    .line 1711017
    goto/16 :goto_0

    .line 1711018
    :cond_4
    const-string v1, "feed_claim_coupon"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1711019
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711020
    const-string v1, "claimCouponParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/coupons/ClaimCouponParams;

    .line 1711021
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1711022
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1711023
    goto/16 :goto_0

    .line 1711024
    :cond_5
    const-string v1, "feed_clear_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1711025
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711026
    goto/16 :goto_0

    .line 1711027
    :cond_6
    const-string v1, "feed_mark_impression_logged"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1711028
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711029
    goto/16 :goto_0

    .line 1711030
    :cond_7
    const-string v1, "feed_submit_survey_response"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1711031
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711032
    const-string v1, "submitSurveyResponseParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/SubmitSurveyResponseParams;

    .line 1711033
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/SubmitSurveyResponseResult;

    .line 1711034
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1711035
    goto/16 :goto_0

    .line 1711036
    :cond_8
    const-string v1, "feed_mark_survey_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1711037
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711038
    goto/16 :goto_0

    .line 1711039
    :cond_9
    const-string v1, "feed_submit_research_poll_response"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1711040
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711041
    const-string v1, "submitResearchPollResponseParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;

    .line 1711042
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1711043
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711044
    move-object v0, v0

    .line 1711045
    goto/16 :goto_0

    .line 1711046
    :cond_a
    const-string v1, "feed_mark_research_poll_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1711047
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711048
    goto/16 :goto_0

    .line 1711049
    :cond_b
    const-string v1, "feed_fetch_followup_feed_unit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1711050
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711051
    const-string v1, "fetchFollowUpFeedUnitParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    .line 1711052
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1711053
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1711054
    goto/16 :goto_0

    .line 1711055
    :cond_c
    const-string v1, "feed_fetch_paginated_related_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1711056
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711057
    const-string v1, "fetchPaginatedRelatedStoryParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchPaginatedRelatedStoryParams;

    .line 1711058
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 1711059
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1711060
    goto/16 :goto_0

    .line 1711061
    :cond_d
    const-string v1, "toggle_save_place"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1711062
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711063
    const-string v1, "togglePlaceParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;

    .line 1711064
    iget-boolean v1, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    if-eqz v1, :cond_15

    .line 1711065
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1711066
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1711067
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711068
    :goto_3
    move-object v0, v0

    .line 1711069
    goto/16 :goto_0

    .line 1711070
    :cond_e
    const-string v1, "update_story_saved_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1711071
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711072
    if-nez v0, :cond_17

    .line 1711073
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1711074
    :goto_4
    move-object v0, v0

    .line 1711075
    goto/16 :goto_0

    .line 1711076
    :cond_f
    const-string v1, "live_video_log_watch_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1711077
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711078
    const-string v1, "liveVideoLogWatchTimeParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;

    .line 1711079
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1711080
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711081
    move-object v0, v0

    .line 1711082
    goto/16 :goto_0

    .line 1711083
    :cond_10
    const-string v1, "live_video_invite_friends"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1711084
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711085
    const-string v1, "liveVideoInviteFriendsParamKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;

    .line 1711086
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1711087
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711088
    move-object v0, v0

    .line 1711089
    goto/16 :goto_0

    .line 1711090
    :cond_11
    const-string v1, "live_video_watch_like"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1711091
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711092
    const-string v1, "LiveVideoWatchLikeParamKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;

    .line 1711093
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1711094
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711095
    move-object v0, v0

    .line 1711096
    goto/16 :goto_0

    .line 1711097
    :cond_12
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1711098
    :cond_13
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711099
    goto/16 :goto_1

    .line 1711100
    :catch_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_2

    .line 1711101
    :cond_14
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 1711102
    :cond_15
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1711103
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1711104
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711105
    goto/16 :goto_3

    :cond_16
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    .line 1711106
    :cond_17
    const-string v1, "updateStorySavedStateParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/server/UpdateSavedStateParams;

    .line 1711107
    :try_start_1
    iget-object v2, p0, LX/Amb;->a:LX/18V;

    iget-object v1, p0, LX/Amb;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1711108
    iget-object v0, p0, LX/Amb;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNw;

    invoke-virtual {v0}, LX/BNw;->a()V

    .line 1711109
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1711110
    goto/16 :goto_4

    .line 1711111
    :cond_18
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto/16 :goto_4

    .line 1711112
    :catch_1
    move-exception v0

    .line 1711113
    instance-of v1, v0, Ljava/net/UnknownHostException;

    if-nez v1, :cond_19

    instance-of v1, v0, LX/4bK;

    if-eqz v1, :cond_1a

    .line 1711114
    :cond_19
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_4

    .line 1711115
    :cond_1a
    sget-object v1, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_4
.end method
