.class public LX/AaY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/AaY;


# instance fields
.field public a:Z

.field private b:Z

.field public final c:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688683
    iput-object p1, p0, LX/AaY;->c:LX/0if;

    .line 1688684
    return-void
.end method

.method public static a(LX/0QB;)LX/AaY;
    .locals 4

    .prologue
    .line 1688685
    sget-object v0, LX/AaY;->d:LX/AaY;

    if-nez v0, :cond_1

    .line 1688686
    const-class v1, LX/AaY;

    monitor-enter v1

    .line 1688687
    :try_start_0
    sget-object v0, LX/AaY;->d:LX/AaY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1688688
    if-eqz v2, :cond_0

    .line 1688689
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1688690
    new-instance p0, LX/AaY;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/AaY;-><init>(LX/0if;)V

    .line 1688691
    move-object v0, p0

    .line 1688692
    sput-object v0, LX/AaY;->d:LX/AaY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1688693
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1688694
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1688695
    :cond_1
    sget-object v0, LX/AaY;->d:LX/AaY;

    return-object v0

    .line 1688696
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1688697
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/AaY;)LX/1rQ;
    .locals 3

    .prologue
    .line 1688698
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "is_tip_jar_enabled"

    iget-boolean v2, p0, LX/AaY;->a:Z

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/AaY;)LX/1rQ;
    .locals 3

    .prologue
    .line 1688699
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "is_tip_jar_enabled"

    iget-boolean v2, p0, LX/AaY;->a:Z

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    const-string v1, "is_paypal_connected"

    iget-boolean v2, p0, LX/AaY;->b:Z

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final e()V
    .locals 5

    .prologue
    .line 1688700
    iget-object v0, p0, LX/AaY;->c:LX/0if;

    sget-object v1, LX/0ig;->G:LX/0ih;

    const-string v2, "dismiss_composer_page"

    const/4 v3, 0x0

    invoke-static {p0}, LX/AaY;->h(LX/AaY;)LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1688701
    iget-object v0, p0, LX/AaY;->c:LX/0if;

    sget-object v1, LX/0ig;->G:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1688702
    return-void
.end method
