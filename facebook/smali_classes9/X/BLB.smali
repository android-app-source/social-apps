.class public final LX/BLB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V
    .locals 0

    .prologue
    .line 1775331
    iput-object p1, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1775332
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;Z)V

    .line 1775333
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08248d

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1775334
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1775335
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x0

    .line 1775336
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    invoke-static {v0, v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;Z)V

    .line 1775337
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->a(Ljava/util/List;)V

    .line 1775338
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v1, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1775339
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1775340
    :cond_0
    iget-object v0, p0, LX/BLB;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775341
    return-void
.end method
