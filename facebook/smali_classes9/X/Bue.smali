.class public final enum LX/Bue;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bue;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bue;

.field public static final enum FULLSCREEN:LX/Bue;

.field public static final enum WATCH_AND_BROWSE:LX/Bue;

.field public static final enum WATCH_IN_CANVAS:LX/Bue;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1831421
    new-instance v0, LX/Bue;

    const-string v1, "FULLSCREEN"

    invoke-direct {v0, v1, v2}, LX/Bue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bue;->FULLSCREEN:LX/Bue;

    .line 1831422
    new-instance v0, LX/Bue;

    const-string v1, "WATCH_AND_BROWSE"

    invoke-direct {v0, v1, v3}, LX/Bue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bue;->WATCH_AND_BROWSE:LX/Bue;

    .line 1831423
    new-instance v0, LX/Bue;

    const-string v1, "WATCH_IN_CANVAS"

    invoke-direct {v0, v1, v4}, LX/Bue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bue;->WATCH_IN_CANVAS:LX/Bue;

    .line 1831424
    const/4 v0, 0x3

    new-array v0, v0, [LX/Bue;

    sget-object v1, LX/Bue;->FULLSCREEN:LX/Bue;

    aput-object v1, v0, v2

    sget-object v1, LX/Bue;->WATCH_AND_BROWSE:LX/Bue;

    aput-object v1, v0, v3

    sget-object v1, LX/Bue;->WATCH_IN_CANVAS:LX/Bue;

    aput-object v1, v0, v4

    sput-object v0, LX/Bue;->$VALUES:[LX/Bue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1831425
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bue;
    .locals 1

    .prologue
    .line 1831426
    const-class v0, LX/Bue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bue;

    return-object v0
.end method

.method public static values()[LX/Bue;
    .locals 1

    .prologue
    .line 1831427
    sget-object v0, LX/Bue;->$VALUES:[LX/Bue;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bue;

    return-object v0
.end method
