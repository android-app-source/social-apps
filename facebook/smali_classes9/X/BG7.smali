.class public LX/BG7;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766704
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1766705
    return-void
.end method


# virtual methods
.method public final a(LX/Jvd;Lcom/facebook/optic/CameraPreviewView;Lcom/facebook/photos/creativecam/ui/FocusView;LX/5fM;LX/9c7;LX/89Z;)Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;
    .locals 18

    .prologue
    .line 1766706
    new-instance v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-static/range {p0 .. p0}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageProcessor;

    move-result-object v8

    check-cast v8, Lcom/facebook/bitmaps/NativeImageProcessor;

    const/16 v2, 0xb91

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v2, 0xb99

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v2, 0x2d9

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v12

    check-cast v12, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v13

    check-cast v13, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v15

    check-cast v15, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v16

    check-cast v16, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v17

    check-cast v17, LX/03V;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v17}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;-><init>(LX/Jvd;Lcom/facebook/optic/CameraPreviewView;Lcom/facebook/photos/creativecam/ui/FocusView;LX/5fM;LX/9c7;LX/89Z;Lcom/facebook/bitmaps/NativeImageProcessor;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/0Sh;LX/0kL;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    .line 1766707
    return-object v1
.end method
