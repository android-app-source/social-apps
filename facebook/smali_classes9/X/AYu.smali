.class public final LX/AYu;
.super LX/3Gy;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Gy",
        "<",
        "LX/Adf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AYv;


# direct methods
.method public constructor <init>(LX/AYv;)V
    .locals 0

    .prologue
    .line 1686213
    iput-object p1, p0, LX/AYu;->a:LX/AYv;

    invoke-direct {p0}, LX/3Gy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Adf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1686214
    const-class v0, LX/Adf;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1686215
    check-cast p1, LX/Adf;

    .line 1686216
    iget-object v0, p0, LX/AYu;->a:LX/AYv;

    iget-boolean v0, v0, LX/AYv;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AYu;->a:LX/AYv;

    iget-object v0, v0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-eqz v0, :cond_0

    .line 1686217
    iget-object v1, p0, LX/AYu;->a:LX/AYv;

    iget v0, p1, LX/Adf;->a:I

    iget-object v2, p0, LX/AYu;->a:LX/AYv;

    iget-object v2, v2, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v2, v2, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    .line 1686218
    :goto_0
    iput-boolean v0, v1, LX/AYv;->k:Z

    .line 1686219
    :cond_0
    return-void

    .line 1686220
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
