.class public final LX/Atl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ar8;


# instance fields
.field public final synthetic a:LX/Atm;


# direct methods
.method public constructor <init>(LX/Atm;)V
    .locals 0

    .prologue
    .line 1721758
    iput-object p1, p0, LX/Atl;->a:LX/Atm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1721760
    iget-object v0, p0, LX/Atl;->a:LX/Atm;

    iget-object v0, v0, LX/Atm;->a:LX/Atp;

    iget-object v0, v0, LX/Atp;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721761
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    .line 1721762
    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    move-object v1, v0

    .line 1721763
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    .line 1721764
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    .line 1721765
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1721766
    iget-object v3, p0, LX/Atl;->a:LX/Atm;

    iget-object v3, v3, LX/Atm;->a:LX/Atp;

    iget-object v3, v3, LX/Atp;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/Atl;->a:LX/Atm;

    iget-object v3, v3, LX/Atm;->a:LX/Atp;

    .line 1721767
    iget-object v4, v3, LX/Atp;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v4, v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, LX/Atp;->l:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, LX/Atp;->k:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    invoke-static {v4, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v3, v4

    .line 1721768
    if-eqz v3, :cond_0

    .line 1721769
    iget-object v0, p0, LX/Atl;->a:LX/Atm;

    iget-object v0, v0, LX/Atm;->a:LX/Atp;

    const v1, 0x7f08278d

    invoke-static {v0, v1}, LX/Atp;->a$redex0(LX/Atp;I)V

    .line 1721770
    :goto_1
    return-void

    .line 1721771
    :cond_0
    iget-object v3, p0, LX/Atl;->a:LX/Atm;

    iget-object v3, v3, LX/Atm;->a:LX/Atp;

    .line 1721772
    iget-object v4, v3, LX/Atp;->o:LX/0hs;

    invoke-virtual {v4}, LX/0ht;->l()V

    .line 1721773
    iget-object v4, v3, LX/Atp;->n:LX/0wd;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v4, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1721774
    iget-object v3, p0, LX/Atl;->a:LX/Atm;

    iget-object v3, v3, LX/Atm;->a:LX/Atp;

    iget-object v3, v3, LX/Atp;->e:LX/ArL;

    .line 1721775
    sget-object v4, LX/ArH;->SAVE_TO_CAMERA_ROLL:LX/ArH;

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v5}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object v5, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v5}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721776
    iget-object v3, p0, LX/Atl;->a:LX/Atm;

    iget-object v3, v3, LX/Atm;->a:LX/Atp;

    .line 1721777
    iput-object v1, v3, LX/Atp;->l:Ljava/lang/String;

    .line 1721778
    iget-object v1, p0, LX/Atl;->a:LX/Atm;

    iget-object v1, v1, LX/Atm;->a:LX/Atp;

    .line 1721779
    iput-object v2, v1, LX/Atp;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1721780
    iget-object v1, p0, LX/Atl;->a:LX/Atm;

    iget-object v1, v1, LX/Atm;->a:LX/Atp;

    .line 1721781
    iput-object v0, v1, LX/Atp;->k:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 1721782
    iget-object v0, p0, LX/Atl;->a:LX/Atm;

    iget-object v0, v0, LX/Atm;->a:LX/Atp;

    invoke-static {v0}, LX/Atp;->e$redex0(LX/Atp;)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1721759
    return-void
.end method
