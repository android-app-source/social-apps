.class public LX/AVX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:D

.field public final d:D


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679918
    sget v0, LX/1v6;->d:I

    const/16 v1, 0x190

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/AVX;->a:I

    .line 1679919
    sget v0, LX/1v6;->f:I

    const/16 v1, 0xfa

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/AVX;->b:I

    .line 1679920
    sget v0, LX/1v6;->g:F

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/AVX;->c:D

    .line 1679921
    sget v0, LX/1v6;->e:F

    const v1, 0x3d4ccccd    # 0.05f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/AVX;->d:D

    .line 1679922
    return-void
.end method
