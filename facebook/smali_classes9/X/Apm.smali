.class public final LX/Apm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Apn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/view/View$OnClickListener;

.field public h:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field public j:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final synthetic k:LX/Apn;


# direct methods
.method public constructor <init>(LX/Apn;)V
    .locals 1

    .prologue
    .line 1716646
    iput-object p1, p0, LX/Apm;->k:LX/Apn;

    .line 1716647
    move-object v0, p1

    .line 1716648
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716649
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716650
    const-string v0, "FigActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716651
    if-ne p0, p1, :cond_1

    .line 1716652
    :cond_0
    :goto_0
    return v0

    .line 1716653
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716654
    goto :goto_0

    .line 1716655
    :cond_3
    check-cast p1, LX/Apm;

    .line 1716656
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716657
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716658
    if-eq v2, v3, :cond_0

    .line 1716659
    iget v2, p0, LX/Apm;->a:I

    iget v3, p1, LX/Apm;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716660
    goto :goto_0

    .line 1716661
    :cond_4
    iget-object v2, p0, LX/Apm;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Apm;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apm;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1716662
    goto :goto_0

    .line 1716663
    :cond_6
    iget-object v2, p1, LX/Apm;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 1716664
    :cond_7
    iget v2, p0, LX/Apm;->c:I

    iget v3, p1, LX/Apm;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1716665
    goto :goto_0

    .line 1716666
    :cond_8
    iget-object v2, p0, LX/Apm;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Apm;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apm;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1716667
    goto :goto_0

    .line 1716668
    :cond_a
    iget-object v2, p1, LX/Apm;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_9

    .line 1716669
    :cond_b
    iget-object v2, p0, LX/Apm;->e:Landroid/util/SparseArray;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Apm;->e:Landroid/util/SparseArray;

    iget-object v3, p1, LX/Apm;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 1716670
    goto :goto_0

    .line 1716671
    :cond_d
    iget-object v2, p1, LX/Apm;->e:Landroid/util/SparseArray;

    if-nez v2, :cond_c

    .line 1716672
    :cond_e
    iget-object v2, p0, LX/Apm;->f:LX/1dQ;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/Apm;->f:LX/1dQ;

    iget-object v3, p1, LX/Apm;->f:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 1716673
    goto :goto_0

    .line 1716674
    :cond_10
    iget-object v2, p1, LX/Apm;->f:LX/1dQ;

    if-nez v2, :cond_f

    .line 1716675
    :cond_11
    iget-object v2, p0, LX/Apm;->g:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/Apm;->g:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/Apm;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 1716676
    goto/16 :goto_0

    .line 1716677
    :cond_13
    iget-object v2, p1, LX/Apm;->g:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_12

    .line 1716678
    :cond_14
    iget-object v2, p0, LX/Apm;->h:LX/1dQ;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/Apm;->h:LX/1dQ;

    iget-object v3, p1, LX/Apm;->h:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1716679
    goto/16 :goto_0

    .line 1716680
    :cond_16
    iget-object v2, p1, LX/Apm;->h:LX/1dQ;

    if-nez v2, :cond_15

    .line 1716681
    :cond_17
    iget-object v2, p0, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v3, p1, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 1716682
    goto/16 :goto_0

    .line 1716683
    :cond_19
    iget-object v2, p1, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-nez v2, :cond_18

    .line 1716684
    :cond_1a
    iget-object v2, p0, LX/Apm;->j:LX/1X1;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/Apm;->j:LX/1X1;

    iget-object v3, p1, LX/Apm;->j:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1716685
    goto/16 :goto_0

    .line 1716686
    :cond_1b
    iget-object v2, p1, LX/Apm;->j:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1716687
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Apm;

    .line 1716688
    iget-object v1, v0, LX/Apm;->j:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Apm;->j:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Apm;->j:LX/1X1;

    .line 1716689
    return-object v0

    .line 1716690
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
