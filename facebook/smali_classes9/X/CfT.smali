.class public LX/CfT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zT;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1925959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925960
    iput-object p1, p0, LX/CfT;->c:Ljava/lang/String;

    .line 1925961
    iput-object p2, p0, LX/CfT;->d:Ljava/lang/String;

    .line 1925962
    iput-object p3, p0, LX/CfT;->e:Ljava/lang/String;

    .line 1925963
    return-void
.end method


# virtual methods
.method public final a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1925964
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v0

    .line 1925965
    iget-object v1, p0, LX/CfT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    iget-object v1, p0, LX/CfT;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    iget-object v1, p0, LX/CfT;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    invoke-virtual {v0}, LX/1l6;->hashCode()I

    move-result v0

    .line 1925966
    iget-object v1, p1, LX/0zO;->m:LX/0gW;

    move-object v1, v1

    .line 1925967
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
