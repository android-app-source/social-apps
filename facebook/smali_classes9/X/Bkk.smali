.class public final LX/Bkk;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/ui/EventNameEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/ui/EventNameEditText;)V
    .locals 0

    .prologue
    .line 1814817
    iput-object p1, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1814808
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    iget v1, v1, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    if-le v0, v1, :cond_0

    .line 1814809
    iget-object v0, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 1814810
    iput v1, v0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    .line 1814811
    :cond_0
    iget-object v0, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v0, v0, Lcom/facebook/events/create/ui/EventNameEditText;->c:Lcom/facebook/events/create/EventCompositionModel;

    if-eqz v0, :cond_1

    .line 1814812
    iget-object v0, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v0, v0, Lcom/facebook/events/create/ui/EventNameEditText;->c:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1814813
    iput-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    .line 1814814
    :cond_1
    iget-object v0, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v0, v0, Lcom/facebook/events/create/ui/EventNameEditText;->g:LX/BjE;

    if-eqz v0, :cond_2

    .line 1814815
    iget-object v0, p0, LX/Bkk;->a:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v0, v0, Lcom/facebook/events/create/ui/EventNameEditText;->g:LX/BjE;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/BjE;->a(Ljava/lang/String;)V

    .line 1814816
    :cond_2
    return-void
.end method
