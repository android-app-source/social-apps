.class public final LX/CDn;
.super LX/451;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/451",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic c:LX/3iF;


# direct methods
.method public constructor <init>(LX/3iF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 1860128
    iput-object p1, p0, LX/CDn;->c:LX/3iF;

    iput-object p2, p0, LX/CDn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/CDn;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0}, LX/451;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1860113
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1860114
    if-nez p1, :cond_0

    .line 1860115
    iget-object v0, p0, LX/CDn;->c:LX/3iF;

    iget-object v0, v0, LX/3iF;->i:LX/03V;

    sget-object v1, LX/3iF;->a:Ljava/lang/String;

    const-string v2, "Fetched feedback is null!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860116
    iget-object v0, p0, LX/CDn;->c:LX/3iF;

    .line 1860117
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860118
    :goto_0
    return-void

    .line 1860119
    :cond_0
    iget-object v0, p0, LX/CDn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1860120
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1860121
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1860122
    iget-object v1, p0, LX/CDn;->c:LX/3iF;

    iget-object v2, p0, LX/CDn;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v1, v0, v2}, LX/3iF;->a$redex0(LX/3iF;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1860123
    iget-object v0, p0, LX/CDn;->c:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860124
    return-void
.end method

.method public final a(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1860125
    invoke-super {p0, p1}, LX/451;->a(Ljava/util/concurrent/CancellationException;)V

    .line 1860126
    iget-object v0, p0, LX/CDn;->c:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860127
    return-void
.end method
