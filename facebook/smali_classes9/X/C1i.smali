.class public final LX/C1i;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C1k;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C1j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C1k",
            "<TE;>.Scheduled",
            "LiveAttachmentTickerComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C1k;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C1k;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1843125
    iput-object p1, p0, LX/C1i;->b:LX/C1k;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1843126
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "broadcastSchedule"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C1i;->c:[Ljava/lang/String;

    .line 1843127
    iput v3, p0, LX/C1i;->d:I

    .line 1843128
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C1i;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C1i;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C1i;LX/1De;IILX/C1j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C1k",
            "<TE;>.Scheduled",
            "LiveAttachmentTickerComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1843121
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1843122
    iput-object p4, p0, LX/C1i;->a:LX/C1j;

    .line 1843123
    iget-object v0, p0, LX/C1i;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1843124
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/C1i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C1k",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1843118
    iget-object v0, p0, LX/C1i;->a:LX/C1j;

    iput-object p1, v0, LX/C1j;->c:LX/1Pq;

    .line 1843119
    iget-object v0, p0, LX/C1i;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1843120
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C1i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/C1k",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1843129
    iget-object v0, p0, LX/C1i;->a:LX/C1j;

    iput-object p1, v0, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1843130
    iget-object v0, p0, LX/C1i;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1843131
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/C1i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;",
            ")",
            "LX/C1k",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1843115
    iget-object v0, p0, LX/C1i;->a:LX/C1j;

    iput-object p1, v0, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1843116
    iget-object v0, p0, LX/C1i;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1843117
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1843111
    invoke-super {p0}, LX/1X5;->a()V

    .line 1843112
    const/4 v0, 0x0

    iput-object v0, p0, LX/C1i;->a:LX/C1j;

    .line 1843113
    iget-object v0, p0, LX/C1i;->b:LX/C1k;

    iget-object v0, v0, LX/C1k;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1843114
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C1k;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1843101
    iget-object v1, p0, LX/C1i;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C1i;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C1i;->d:I

    if-ge v1, v2, :cond_2

    .line 1843102
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1843103
    :goto_0
    iget v2, p0, LX/C1i;->d:I

    if-ge v0, v2, :cond_1

    .line 1843104
    iget-object v2, p0, LX/C1i;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1843105
    iget-object v2, p0, LX/C1i;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1843106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1843107
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1843108
    :cond_2
    iget-object v0, p0, LX/C1i;->a:LX/C1j;

    .line 1843109
    invoke-virtual {p0}, LX/C1i;->a()V

    .line 1843110
    return-object v0
.end method
