.class public final enum LX/B66;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B66;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B66;

.field public static final enum SIMPLE_PICKER:LX/B66;

.field public static final enum SLIDESHOW_ATTACHMENT_EDIT:LX/B66;


# instance fields
.field private final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1746258
    new-instance v0, LX/B66;

    const-string v1, "SLIDESHOW_ATTACHMENT_EDIT"

    const-string v2, "composer_attachment"

    invoke-direct {v0, v1, v3, v2}, LX/B66;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B66;->SLIDESHOW_ATTACHMENT_EDIT:LX/B66;

    .line 1746259
    new-instance v0, LX/B66;

    const-string v1, "SIMPLE_PICKER"

    const-string v2, "photo_picker"

    invoke-direct {v0, v1, v4, v2}, LX/B66;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/B66;->SIMPLE_PICKER:LX/B66;

    .line 1746260
    const/4 v0, 0x2

    new-array v0, v0, [LX/B66;

    sget-object v1, LX/B66;->SLIDESHOW_ATTACHMENT_EDIT:LX/B66;

    aput-object v1, v0, v3

    sget-object v1, LX/B66;->SIMPLE_PICKER:LX/B66;

    aput-object v1, v0, v4

    sput-object v0, LX/B66;->$VALUES:[LX/B66;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1746255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1746256
    iput-object p3, p0, LX/B66;->analyticsName:Ljava/lang/String;

    .line 1746257
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B66;
    .locals 1

    .prologue
    .line 1746261
    const-class v0, LX/B66;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B66;

    return-object v0
.end method

.method public static values()[LX/B66;
    .locals 1

    .prologue
    .line 1746254
    sget-object v0, LX/B66;->$VALUES:[LX/B66;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B66;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1746253
    iget-object v0, p0, LX/B66;->analyticsName:Ljava/lang/String;

    return-object v0
.end method
