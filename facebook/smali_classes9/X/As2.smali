.class public LX/As2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/common/callercontext/CallerContext;

.field public final b:LX/1Ad;

.field public c:LX/1o9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;LX/1Ad;)V
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719999
    iput-object p1, p0, LX/As2;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1720000
    iput-object p2, p0, LX/As2;->b:LX/1Ad;

    .line 1720001
    return-void
.end method

.method public static a(Landroid/content/Context;I)LX/1af;
    .locals 8

    .prologue
    .line 1719994
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a(Landroid/content/Context;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-result-object v2

    int-to-double v4, p1

    const-wide v6, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1719995
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1719996
    move-object v0, v0

    .line 1719997
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1719992
    new-instance v0, LX/1o9;

    invoke-direct {v0, p1, p2}, LX/1o9;-><init>(II)V

    iput-object v0, p0, LX/As2;->c:LX/1o9;

    .line 1719993
    return-void
.end method

.method public final a(LX/As1;Landroid/net/Uri;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p2    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1719974
    iget-object v0, p1, LX/As1;->a:LX/1aZ;

    .line 1719975
    if-nez p2, :cond_1

    .line 1719976
    const/4 v1, 0x0

    .line 1719977
    :goto_0
    move-object v0, v1

    .line 1719978
    iget-object v1, p1, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1719979
    iget-object v1, p1, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1719980
    if-eqz v0, :cond_0

    .line 1719981
    iput-object v0, p1, LX/As1;->a:LX/1aZ;

    .line 1719982
    :cond_0
    return-void

    .line 1719983
    :cond_1
    iget-object v1, p0, LX/As2;->c:LX/1o9;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719984
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    iget-object v2, p0, LX/As2;->c:LX/1o9;

    .line 1719985
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 1719986
    move-object v1, v1

    .line 1719987
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1719988
    iget-object v2, p0, LX/As2;->b:LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->o()LX/1Ad;

    .line 1719989
    if-eqz v0, :cond_2

    .line 1719990
    iget-object v2, p0, LX/As2;->b:LX/1Ad;

    invoke-virtual {v2, v0}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 1719991
    :cond_2
    iget-object v2, p0, LX/As2;->b:LX/1Ad;

    invoke-virtual {v2, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v2, p0, LX/As2;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    goto :goto_0
.end method
