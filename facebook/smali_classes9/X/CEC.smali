.class public final LX/CEC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CED;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1860969
    invoke-static {}, LX/CED;->q()LX/CED;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860970
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860971
    const-string v0, "ClickAdapterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860972
    if-ne p0, p1, :cond_1

    .line 1860973
    :cond_0
    :goto_0
    return v0

    .line 1860974
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860975
    goto :goto_0

    .line 1860976
    :cond_3
    check-cast p1, LX/CEC;

    .line 1860977
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860978
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860979
    if-eq v2, v3, :cond_0

    .line 1860980
    iget-object v2, p0, LX/CEC;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CEC;->a:LX/1X1;

    iget-object v3, p1, LX/CEC;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1860981
    goto :goto_0

    .line 1860982
    :cond_5
    iget-object v2, p1, LX/CEC;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1860983
    :cond_6
    iget-object v2, p0, LX/CEC;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CEC;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/CEC;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1860984
    goto :goto_0

    .line 1860985
    :cond_7
    iget-object v2, p1, LX/CEC;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1860986
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CEC;

    .line 1860987
    iget-object v1, v0, LX/CEC;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/CEC;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/CEC;->a:LX/1X1;

    .line 1860988
    return-object v0

    .line 1860989
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
