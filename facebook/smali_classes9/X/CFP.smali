.class public final LX/CFP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V
    .locals 0

    .prologue
    .line 1863166
    iput-object p1, p0, LX/CFP;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1863167
    iget-object v0, p0, LX/CFP;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->f:Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, LX/CFP;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-virtual {v2}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, LX/CFP;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-virtual {v3}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1863168
    const/4 v0, 0x0

    return v0
.end method
