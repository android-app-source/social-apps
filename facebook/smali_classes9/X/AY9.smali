.class public final LX/AY9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AY8;


# instance fields
.field public final synthetic a:LX/AYE;


# direct methods
.method public constructor <init>(LX/AYE;)V
    .locals 0

    .prologue
    .line 1684811
    iput-object p1, p0, LX/AY9;->a:LX/AYE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1684812
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->p:LX/AYv;

    if-nez v0, :cond_0

    .line 1684813
    :goto_0
    :pswitch_0
    return-void

    .line 1684814
    :cond_0
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->p:LX/AYv;

    iget-object v1, p0, LX/AY9;->a:LX/AYE;

    iget-wide v2, v1, LX/AYE;->G:J

    iget-object v1, p0, LX/AY9;->a:LX/AYE;

    iget-wide v4, v1, LX/AYE;->H:J

    invoke-virtual {v0, v2, v3, v4, v5}, LX/AYv;->a(JJ)LX/AZC;

    move-result-object v1

    .line 1684815
    sget-object v0, LX/AY7;->a:[I

    invoke-virtual {v1}, LX/AZC;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1684816
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->p:LX/AYv;

    iget-object v2, p0, LX/AY9;->a:LX/AYE;

    iget-wide v2, v2, LX/AYE;->G:J

    iget-object v4, p0, LX/AY9;->a:LX/AYE;

    iget-wide v4, v4, LX/AYE;->H:J

    invoke-virtual/range {v0 .. v5}, LX/AYv;->a(LX/AZC;JJ)V

    .line 1684817
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->h:LX/AYs;

    invoke-virtual {v0}, LX/AYs;->c()V

    goto :goto_0

    .line 1684818
    :pswitch_1
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->c:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_PROMPT:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 1684819
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->h:LX/AYs;

    invoke-virtual {v0}, LX/AYs;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1684820
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->c:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 1684821
    iget-object v0, p0, LX/AY9;->a:LX/AYE;

    iget-object v0, v0, LX/AYE;->h:LX/AYs;

    .line 1684822
    iget-object v1, v0, LX/AYs;->a:LX/0if;

    sget-object v2, LX/0ig;->C:LX/0ih;

    const-string v3, "commercial_break_cancel"

    const/4 v4, 0x0

    invoke-static {v0}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object p0

    invoke-virtual {v1, v2, v3, v4, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1684823
    return-void
.end method
