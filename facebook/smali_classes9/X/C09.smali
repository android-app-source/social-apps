.class public LX/C09;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C07;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0A;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1839962
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C09;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0A;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839963
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839964
    iput-object p1, p0, LX/C09;->b:LX/0Ot;

    .line 1839965
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1839966
    check-cast p2, LX/C08;

    .line 1839967
    iget-object v0, p0, LX/C09;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0A;

    iget-object v1, p2, LX/C08;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C08;->b:LX/1Pm;

    .line 1839968
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1839969
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1839970
    const/4 v4, 0x1

    .line 1839971
    iget-object v5, v0, LX/C0A;->b:LX/2sO;

    invoke-virtual {v5, v1}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v5

    .line 1839972
    if-nez v5, :cond_0

    .line 1839973
    const/4 v5, 0x0

    .line 1839974
    :goto_0
    move-object v5, v5

    .line 1839975
    invoke-static {v3}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    .line 1839976
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1839977
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v4}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v4

    .line 1839978
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1839979
    iget-object p0, v0, LX/C0A;->d:LX/Ap5;

    invoke-virtual {p0, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/Ap4;->b(LX/1X1;)LX/Ap4;

    move-result-object v5

    iget-object p0, v0, LX/C0A;->a:LX/1qb;

    invoke-virtual {p0, v3}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/Ap4;->b(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1839980
    const v4, 0x47f44914

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1839981
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1839982
    return-object v0

    :cond_0
    invoke-virtual {v5, p1, v2, v1, v4}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1839983
    invoke-static {}, LX/1dS;->b()V

    .line 1839984
    iget v0, p1, LX/1dQ;->b:I

    .line 1839985
    packed-switch v0, :pswitch_data_0

    .line 1839986
    :goto_0
    return-object v2

    .line 1839987
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1839988
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1839989
    check-cast v1, LX/C08;

    .line 1839990
    iget-object v3, p0, LX/C09;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C0A;

    iget-object p1, v1, LX/C08;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/C08;->b:LX/1Pm;

    .line 1839991
    iget-object p0, v3, LX/C0A;->c:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1839992
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x47f44914
        :pswitch_0
    .end packed-switch
.end method
