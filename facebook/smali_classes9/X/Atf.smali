.class public final LX/Atf;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Ath;


# direct methods
.method public constructor <init>(LX/Ath;I)V
    .locals 0

    .prologue
    .line 1721679
    iput-object p1, p0, LX/Atf;->b:LX/Ath;

    iput p2, p0, LX/Atf;->a:I

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 3

    .prologue
    .line 1721672
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1721673
    iget-object v1, p0, LX/Atf;->b:LX/Ath;

    iget-object v1, v1, LX/Ath;->b:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    iget v2, p0, LX/Atf;->a:I

    neg-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 1721674
    iget-object v1, p0, LX/Atf;->b:LX/Ath;

    iget-object v1, v1, LX/Ath;->b:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1721675
    iget-object v1, p0, LX/Atf;->b:LX/Ath;

    iget-object v1, v1, LX/Ath;->b:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1721676
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1721677
    iget-object v0, p0, LX/Atf;->b:LX/Ath;

    iget-object v0, v0, LX/Ath;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1721678
    :cond_0
    return-void
.end method
