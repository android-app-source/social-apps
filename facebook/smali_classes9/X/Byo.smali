.class public LX/Byo;
.super LX/Byn;
.source ""

# interfaces
.implements LX/35p;
.implements LX/2fC;


# instance fields
.field private final a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

.field private final b:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1837868
    const v0, 0x7f031413

    invoke-direct {p0, p1, v0}, LX/Byo;-><init>(Landroid/content/Context;I)V

    .line 1837869
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1837863
    invoke-direct {p0, p1, p2}, LX/Byn;-><init>(Landroid/content/Context;I)V

    .line 1837864
    const v0, 0x7f02199e

    invoke-virtual {p0, v0}, LX/Byo;->setBackgroundResource(I)V

    .line 1837865
    const v0, 0x7f0d2e1e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, LX/Byo;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 1837866
    const v0, 0x7f0d2e18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Byo;->b:Landroid/widget/LinearLayout;

    .line 1837867
    return-void
.end method


# virtual methods
.method public final a(IZZ)V
    .locals 0

    .prologue
    .line 1837861
    invoke-virtual {p0, p1}, LX/Byo;->setWidth(I)V

    .line 1837862
    return-void
.end method

.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1837860
    iget-object v0, p0, LX/Byo;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1837851
    iget-object v0, p0, LX/Byo;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 1837852
    iget-object p0, v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, p0

    .line 1837853
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1837854
    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1837855
    iget-object v0, p0, LX/Byo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1837856
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1837857
    iget-object v1, p0, LX/Byo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1837858
    iget-object v0, p0, LX/Byo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 1837859
    return-void
.end method
