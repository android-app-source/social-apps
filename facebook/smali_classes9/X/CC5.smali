.class public LX/CC5;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CC3;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CC6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1857130
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CC5;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CC6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857131
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1857132
    iput-object p1, p0, LX/CC5;->b:LX/0Ot;

    .line 1857133
    return-void
.end method

.method public static a(LX/0QB;)LX/CC5;
    .locals 4

    .prologue
    .line 1857134
    const-class v1, LX/CC5;

    monitor-enter v1

    .line 1857135
    :try_start_0
    sget-object v0, LX/CC5;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1857136
    sput-object v2, LX/CC5;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1857137
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857138
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1857139
    new-instance v3, LX/CC5;

    const/16 p0, 0x2160

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CC5;-><init>(LX/0Ot;)V

    .line 1857140
    move-object v0, v3

    .line 1857141
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1857142
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CC5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1857143
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1857144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1857145
    const v0, 0xe8f3824

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1857146
    check-cast p2, LX/CC4;

    .line 1857147
    iget-object v0, p0, LX/CC5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CC6;

    iget-object v1, p2, LX/CC4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CC4;->b:LX/1Pr;

    .line 1857148
    sget-object p0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    new-instance p2, LX/CCF;

    .line 1857149
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1857150
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p2, v3}, LX/CCF;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1857151
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1857152
    check-cast v3, LX/0jW;

    invoke-interface {v2, p2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p0, v3}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result p0

    .line 1857153
    iget-object v3, v0, LX/CC6;->b:LX/2g9;

    invoke-virtual {v3, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v3

    const/16 p2, 0x102

    invoke-virtual {v3, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v3

    const p2, 0x7f082a3f

    invoke-virtual {v3, p2}, LX/2gA;->i(I)LX/2gA;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    if-nez p0, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-interface {p2, v3}, LX/1Di;->b(Z)LX/1Di;

    move-result-object p2

    if-eqz p0, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1857154
    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 1857155
    :cond_1
    const v3, 0xe8f3824

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1857156
    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1857157
    invoke-static {}, LX/1dS;->b()V

    .line 1857158
    iget v0, p1, LX/1dQ;->b:I

    .line 1857159
    packed-switch v0, :pswitch_data_0

    .line 1857160
    :goto_0
    return-object v2

    .line 1857161
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1857162
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1857163
    check-cast v1, LX/CC4;

    .line 1857164
    iget-object v3, p0, LX/CC5;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CC6;

    iget-object v4, v1, LX/CC4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/CC4;->b:LX/1Pr;

    .line 1857165
    new-instance p0, LX/CCF;

    .line 1857166
    iget-object p2, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1857167
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, p2}, LX/CCF;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    sget-object p2, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-interface {p1, p0, p2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1857168
    iget-object p0, v3, LX/CC6;->a:LX/CC2;

    .line 1857169
    iget-object p2, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1857170
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v1, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {p0, p2, v1}, LX/CC2;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V

    .line 1857171
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xe8f3824
        :pswitch_0
    .end packed-switch
.end method
