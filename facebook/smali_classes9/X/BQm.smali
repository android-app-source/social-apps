.class public final LX/BQm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:LX/5SF;

.field public final synthetic b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;LX/5SF;)V
    .locals 0

    .prologue
    .line 1782692
    iput-object p1, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iput-object p2, p0, LX/BQm;->a:LX/5SF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 1782673
    iget-object v0, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iget-boolean v0, v0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->A:Z

    if-eqz v0, :cond_0

    .line 1782674
    :goto_0
    return-void

    .line 1782675
    :cond_0
    new-instance v0, LX/5SJ;

    invoke-direct {v0}, LX/5SJ;-><init>()V

    iget-object v1, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    sget-object v2, LX/74j;->REMOTE_MEDIA:LX/74j;

    invoke-virtual {v2}, LX/74j;->getValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v0

    sget-object v1, LX/5Rz;->NATIVE_SHARE_SHEET:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v1

    .line 1782676
    iput-object v1, v0, LX/5SJ;->c:Ljava/lang/String;

    .line 1782677
    move-object v0, v0

    .line 1782678
    iput-boolean v6, v0, LX/5SJ;->o:Z

    .line 1782679
    move-object v0, v0

    .line 1782680
    iget-object v1, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iget-wide v2, v1, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->y:J

    .line 1782681
    iput-wide v2, v0, LX/5SJ;->e:J

    .line 1782682
    move-object v0, v0

    .line 1782683
    invoke-virtual {v0}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v0

    .line 1782684
    new-instance v1, LX/5Rw;

    invoke-direct {v1}, LX/5Rw;-><init>()V

    iget-object v2, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    sget-object v3, LX/74j;->REMOTE_MEDIA:LX/74j;

    invoke-virtual {v3}, LX/74j;->getValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v1

    sget-object v2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v1, v2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v1, v2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v1

    const/4 v2, 0x0

    .line 1782685
    iput-boolean v2, v1, LX/5Rw;->i:Z

    .line 1782686
    move-object v1, v1

    .line 1782687
    invoke-virtual {v1}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v1

    .line 1782688
    iget-object v2, p0, LX/BQm;->a:LX/5SF;

    iget-object v3, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    invoke-interface {v2, v3, v0, v1}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1782689
    iget-object v1, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    .line 1782690
    iput-boolean v6, v1, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->A:Z

    .line 1782691
    iget-object v1, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->r:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x3e9

    iget-object v3, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1782693
    iget-object v0, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->finish()V

    .line 1782694
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1782671
    iget-object v0, p0, LX/BQm;->b:Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->finish()V

    .line 1782672
    return-void
.end method
