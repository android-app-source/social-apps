.class public LX/BM5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AkM;


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1776822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776823
    new-instance v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {v0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1776824
    const/4 v2, 0x0

    .line 1776825
    iget-object v0, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f0e09c7

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1776826
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a09

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1776827
    iget-object v1, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1776828
    iget-object v0, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 1776829
    iget-object v0, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1776830
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1776831
    iget-object v0, p0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public final getCollapseAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776832
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getExpandAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776833
    const/4 v0, 0x0

    return-object v0
.end method
