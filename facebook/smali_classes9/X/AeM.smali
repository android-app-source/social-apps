.class public LX/AeM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/facecastdisplay/liveevent/ListFactory",
        "<",
        "LX/AeO;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/Ac6;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AeW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ac6;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ac6;",
            "LX/0Ot",
            "<",
            "LX/AeW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1697333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697334
    iput-object p1, p0, LX/AeM;->a:LX/Ac6;

    .line 1697335
    iput-object p2, p0, LX/AeM;->b:LX/0Ot;

    .line 1697336
    return-void
.end method

.method public static a(LX/0QB;)LX/AeM;
    .locals 3

    .prologue
    .line 1697337
    new-instance v1, LX/AeM;

    invoke-static {p0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v0

    check-cast v0, LX/Ac6;

    const/16 v2, 0x1c06

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/AeM;-><init>(LX/Ac6;LX/0Ot;)V

    .line 1697338
    move-object v0, v1

    .line 1697339
    return-object v0
.end method
