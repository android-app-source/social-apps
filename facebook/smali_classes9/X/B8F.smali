.class public LX/B8F;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B8F;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public e:Landroid/widget/Spinner;

.field private f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;

.field public h:LX/B7w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748641
    new-instance v0, LX/B8B;

    invoke-direct {v0}, LX/B8B;-><init>()V

    sput-object v0, LX/B8F;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1748634
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748635
    const v0, 0x7f0309ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748636
    const v0, 0x7f0d18ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    .line 1748637
    const v0, 0x7f0d18e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B8F;->f:Landroid/widget/TextView;

    .line 1748638
    invoke-virtual {p0}, LX/B8F;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d18d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B8F;->g:Landroid/widget/TextView;

    .line 1748639
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B8F;

    invoke-static {v0}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object p1

    check-cast p1, LX/B6l;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object p1, p0, LX/B8F;->b:LX/B6l;

    iput-object v0, p0, LX/B8F;->c:LX/B7W;

    .line 1748640
    return-void
.end method

.method public static e(LX/B8F;)V
    .locals 1

    .prologue
    .line 1748598
    new-instance v0, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;

    invoke-direct {v0, p0}, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;-><init>(LX/B8F;)V

    invoke-virtual {p0, v0}, LX/B8F;->post(Ljava/lang/Runnable;)Z

    .line 1748599
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748631
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1748632
    iput-object v1, p0, LX/B8F;->h:LX/B7w;

    .line 1748633
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 4

    .prologue
    .line 1748611
    iput-object p1, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748612
    iget-object v0, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    .line 1748613
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1748614
    :goto_0
    return-void

    .line 1748615
    :cond_0
    iget-object v1, p0, LX/B8F;->f:Landroid/widget/TextView;

    iget-object v2, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748616
    iget-object v1, p2, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v1, :cond_1

    .line 1748617
    const-string v1, ""

    .line 1748618
    :goto_1
    move-object v1, v1

    .line 1748619
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1748620
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1748621
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1748622
    new-instance v3, LX/B8C;

    invoke-virtual {p0}, LX/B8F;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x1090008

    invoke-direct {v3, p0, p1, p2, v2}, LX/B8C;-><init>(LX/B8F;Landroid/content/Context;ILjava/util/List;)V

    .line 1748623
    const v2, 0x1090009

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1748624
    move-object v0, v3

    .line 1748625
    iget-object v1, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1748626
    iget-object v1, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1748627
    invoke-static {p0}, LX/B8F;->e(LX/B8F;)V

    .line 1748628
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    new-instance v1, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$3;

    invoke-direct {v1, p0, p3}, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$3;-><init>(LX/B8F;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->post(Ljava/lang/Runnable;)Z

    .line 1748629
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    new-instance v1, LX/B8D;

    invoke-direct {v1, p0}, LX/B8D;-><init>(LX/B8F;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1748630
    goto :goto_0

    :cond_1
    iget-object v1, p2, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aI()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748609
    iget-object v0, p0, LX/B8F;->g:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748610
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748642
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    iget-object v1, p0, LX/B8F;->g:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748643
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748607
    iget-object v0, p0, LX/B8F;->g:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748608
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748606
    iget-object v0, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1748601
    iget-object v0, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->a()LX/0Px;

    move-result-object v0

    .line 1748602
    iget-object v1, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 1748603
    if-ltz v1, :cond_0

    iget-object v2, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getCount()I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1748604
    :cond_0
    const-string v0, ""

    .line 1748605
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748600
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1748594
    iget-object v0, p0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/2sb;->a(Ljava/lang/String;LX/0Px;)I

    move-result v0

    .line 1748595
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1748596
    iget-object v1, p0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1748597
    :cond_0
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748592
    iput-object p1, p0, LX/B8F;->h:LX/B7w;

    .line 1748593
    return-void
.end method
