.class public LX/C1k;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C1l;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C1k",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C1l;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843210
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1843211
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C1k;->b:LX/0Zi;

    .line 1843212
    iput-object p1, p0, LX/C1k;->a:LX/0Ot;

    .line 1843213
    return-void
.end method

.method public static a(LX/0QB;)LX/C1k;
    .locals 4

    .prologue
    .line 1843199
    const-class v1, LX/C1k;

    monitor-enter v1

    .line 1843200
    :try_start_0
    sget-object v0, LX/C1k;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1843201
    sput-object v2, LX/C1k;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1843202
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843203
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1843204
    new-instance v3, LX/C1k;

    const/16 p0, 0x1e96

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C1k;-><init>(LX/0Ot;)V

    .line 1843205
    move-object v0, v3

    .line 1843206
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1843207
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1843208
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1843209
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1843197
    invoke-static {}, LX/1dS;->b()V

    .line 1843198
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/16 v0, 0x1e

    const v1, -0x365cb0c8    # -1337831.0f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1843194
    check-cast p6, LX/C1j;

    .line 1843195
    iget-object v0, p0, LX/C1k;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1l;

    iget-object v5, p6, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, LX/C1l;->a(LX/1De;IILX/1no;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1843196
    const/16 v0, 0x1f

    const v1, -0xf96404c

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1843191
    iget-object v0, p0, LX/C1k;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1843192
    new-instance v0, LX/C2A;

    invoke-direct {v0, p1}, LX/C2A;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1843193
    return-object v0
.end method

.method public final c(LX/1De;)LX/C1i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C1k",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1843154
    new-instance v1, LX/C1j;

    invoke-direct {v1, p0}, LX/C1j;-><init>(LX/C1k;)V

    .line 1843155
    iget-object v2, p0, LX/C1k;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C1i;

    .line 1843156
    if-nez v2, :cond_0

    .line 1843157
    new-instance v2, LX/C1i;

    invoke-direct {v2, p0}, LX/C1i;-><init>(LX/C1k;)V

    .line 1843158
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C1i;->a$redex0(LX/C1i;LX/1De;IILX/C1j;)V

    .line 1843159
    move-object v1, v2

    .line 1843160
    move-object v0, v1

    .line 1843161
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1843190
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 1843186
    check-cast p3, LX/C1j;

    .line 1843187
    iget-object v0, p0, LX/C1k;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1l;

    check-cast p2, LX/C2A;

    iget-object v1, p3, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1843188
    iget-object v2, v0, LX/C1l;->a:LX/Abd;

    invoke-virtual {v2, v1}, LX/Abd;->c(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v2

    invoke-static {p1, p2, v2, v3}, LX/C1l;->a(LX/1De;LX/C2A;J)V

    .line 1843189
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1843185
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1843171
    check-cast p3, LX/C1j;

    .line 1843172
    iget-object v0, p0, LX/C1k;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/C2A;

    iget-object v0, p3, LX/C1j;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p3, LX/C1j;->c:LX/1Pq;

    iget-object v2, p3, LX/C1j;->a:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1843173
    iput-object v0, p2, LX/C2A;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1843174
    iput-object v1, p2, LX/C2A;->c:LX/1Pq;

    .line 1843175
    iget-object p0, p2, LX/C2A;->a:LX/Abg;

    invoke-virtual {p0, v2}, LX/Abg;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1843176
    iget-object p0, p2, LX/C2A;->a:LX/Abg;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result p1

    .line 1843177
    iput-boolean p1, p0, LX/Abg;->f:Z

    .line 1843178
    iget-object p0, p2, LX/C2A;->a:LX/Abg;

    invoke-virtual {p0}, LX/Abg;->a()LX/Abe;

    move-result-object p0

    .line 1843179
    sget-object p1, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    if-ne p0, p1, :cond_1

    .line 1843180
    iget-object p1, p2, LX/C2A;->e:Landroid/animation/Animator;

    if-nez p1, :cond_0

    .line 1843181
    invoke-static {p2}, LX/Abd;->a(Ljava/lang/Object;)Landroid/animation/Animator;

    move-result-object p1

    iput-object p1, p2, LX/C2A;->e:Landroid/animation/Animator;

    .line 1843182
    :cond_0
    iget-object p1, p2, LX/C2A;->e:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 1843183
    :cond_1
    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {p2, p0}, LX/C2A;->setAlpha(F)V

    .line 1843184
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1843163
    iget-object v0, p0, LX/C1k;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/C2A;

    .line 1843164
    const/4 v0, 0x0

    .line 1843165
    iput-object v0, p2, LX/C2A;->c:LX/1Pq;

    .line 1843166
    iput-object v0, p2, LX/C2A;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1843167
    iget-object v0, p2, LX/C2A;->a:LX/Abg;

    invoke-virtual {v0}, LX/Abg;->b()V

    .line 1843168
    iget-object v0, p2, LX/C2A;->e:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 1843169
    iget-object v0, p2, LX/C2A;->e:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 1843170
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1843162
    const/16 v0, 0xf

    return v0
.end method
