.class public final LX/CaR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9g5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9g5",
        "<",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917556
    iput-object p1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1917557
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    .line 1917558
    :goto_0
    iget-object v1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c:LX/CaK;

    .line 1917559
    iget-object v2, v1, LX/CaK;->c:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1917560
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_1

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5kD;

    .line 1917561
    iget-object v3, v1, LX/CaK;->a:Ljava/util/Map;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CaJ;

    .line 1917562
    if-eqz v3, :cond_0

    .line 1917563
    invoke-interface {v3, v2}, LX/CaJ;->a(LX/5kD;)V

    .line 1917564
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1917565
    :cond_1
    iput-object p1, v1, LX/CaK;->b:LX/0Px;

    .line 1917566
    iget-object v1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 1917567
    if-eqz v0, :cond_2

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1917568
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    const/4 v3, 0x0

    .line 1917569
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1917570
    :cond_2
    :goto_2
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 1917571
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v0

    .line 1917572
    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c(LX/5kD;)Ljava/lang/String;

    move-result-object v1

    .line 1917573
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1917574
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    invoke-virtual {v2, v1}, LX/CcX;->c(Ljava/lang/String;)V

    .line 1917575
    :cond_3
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1917576
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    invoke-virtual {v2, v1}, LX/CcS;->c(Ljava/lang/String;)V

    .line 1917577
    :cond_4
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v2

    iget-object v3, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    invoke-virtual {v2, v3, v0, v4}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/base/fragment/FbFragment;LX/5kD;LX/21D;)V

    .line 1917578
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1917579
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    iget-object v3, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-boolean v3, v3, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ae:Z

    invoke-virtual {v2, v1, v3}, LX/CcX;->a(Ljava/lang/String;Z)V

    .line 1917580
    :cond_5
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1917581
    iget-object v2, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    invoke-virtual {v2, v1}, LX/CcS;->d(Ljava/lang/String;)V

    .line 1917582
    :cond_6
    iget-object v1, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v1, v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;LX/5kD;)V

    .line 1917583
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    .line 1917584
    iget-object v1, v0, LX/9g2;->e:LX/9g6;

    move-object v0, v1

    .line 1917585
    sget-object v1, LX/9g6;->DONE:LX/9g6;

    if-ne v0, v1, :cond_9

    .line 1917586
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->U:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1917587
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->U:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9eB;

    .line 1917588
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_IN:LX/9eK;

    if-ne v1, v2, :cond_8

    .line 1917589
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    .line 1917590
    iget-object v2, v1, LX/9eQ;->c:LX/9eP;

    move-object v1, v2

    .line 1917591
    invoke-virtual {v1}, LX/9eP;->a()V

    .line 1917592
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 1917593
    :cond_8
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    sget-object v2, LX/9eK;->ANIMATE_OUT:LX/9eK;

    .line 1917594
    iput-object v2, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 1917595
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    const/4 v2, 0x0

    .line 1917596
    iput-boolean v2, v1, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->l:Z

    .line 1917597
    iget-object v1, v0, LX/9eB;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1917598
    invoke-static {v1, v2}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->b$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V

    .line 1917599
    :cond_9
    return-void

    .line 1917600
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1917601
    :cond_b
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-eqz v0, :cond_7

    .line 1917602
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVisibility(I)V

    goto :goto_3

    :cond_c
    move v2, v3

    .line 1917603
    :goto_4
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v2, v1, :cond_d

    .line 1917604
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v4, v2}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v4

    invoke-interface {v4}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1917605
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1917606
    if-nez v2, :cond_d

    .line 1917607
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->aa:LX/CaS;

    invoke-virtual {v1, v3}, LX/CaS;->B_(I)V

    .line 1917608
    :cond_d
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->T:LX/0am;

    goto/16 :goto_2

    .line 1917609
    :cond_e
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4
.end method

.method public final a(LX/9g6;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1917610
    sget-object v0, LX/9g6;->LOADING:LX/9g6;

    if-ne p1, v0, :cond_3

    .line 1917611
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->S:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1917612
    :cond_0
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Z)V

    .line 1917613
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->C:LX/1BA;

    const v1, 0x140015

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 1917614
    :goto_0
    sget-object v0, LX/9g6;->DONE:LX/9g6;

    if-ne p1, v0, :cond_1

    .line 1917615
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    .line 1917616
    iget-boolean v1, v0, LX/23g;->h:Z

    if-nez v1, :cond_4

    .line 1917617
    :cond_1
    :goto_1
    return-void

    .line 1917618
    :cond_2
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Z)V

    .line 1917619
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->C:LX/1BA;

    const v1, 0x140016

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    goto :goto_0

    .line 1917620
    :cond_3
    iget-object v0, p0, LX/CaR;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Z)V

    goto :goto_0

    .line 1917621
    :cond_4
    iget-object v1, v0, LX/23g;->c:LX/23h;

    const-string p0, "DataFetch"

    invoke-virtual {v1, p0}, LX/23h;->d(Ljava/lang/String;)V

    goto :goto_1
.end method
