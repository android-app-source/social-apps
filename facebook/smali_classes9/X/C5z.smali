.class public final enum LX/C5z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C5z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C5z;

.field public static final enum SHOWING_BLING_BAR:LX/C5z;

.field public static final enum SHOWING_REAL_TIME_ACTIVITY:LX/C5z;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1849485
    new-instance v0, LX/C5z;

    const-string v1, "SHOWING_REAL_TIME_ACTIVITY"

    invoke-direct {v0, v1, v2}, LX/C5z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5z;->SHOWING_REAL_TIME_ACTIVITY:LX/C5z;

    .line 1849486
    new-instance v0, LX/C5z;

    const-string v1, "SHOWING_BLING_BAR"

    invoke-direct {v0, v1, v3}, LX/C5z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    .line 1849487
    const/4 v0, 0x2

    new-array v0, v0, [LX/C5z;

    sget-object v1, LX/C5z;->SHOWING_REAL_TIME_ACTIVITY:LX/C5z;

    aput-object v1, v0, v2

    sget-object v1, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    aput-object v1, v0, v3

    sput-object v0, LX/C5z;->$VALUES:[LX/C5z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1849488
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C5z;
    .locals 1

    .prologue
    .line 1849489
    const-class v0, LX/C5z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C5z;

    return-object v0
.end method

.method public static values()[LX/C5z;
    .locals 1

    .prologue
    .line 1849490
    sget-object v0, LX/C5z;->$VALUES:[LX/C5z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C5z;

    return-object v0
.end method
