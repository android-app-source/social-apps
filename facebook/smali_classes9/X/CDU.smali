.class public LX/CDU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static B:LX/0Xm;


# instance fields
.field private final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LX/CDa;

.field private final b:LX/1qa;

.field private final c:LX/2mZ;

.field private final d:LX/1VK;

.field private final e:LX/2mk;

.field public final f:LX/2ml;

.field private final g:LX/23s;

.field private final h:LX/0tK;

.field private final i:LX/2mn;

.field private final j:LX/1CK;

.field private final k:LX/2mq;

.field private final l:LX/0iX;

.field private final m:LX/2mu;

.field private final n:LX/198;

.field private final o:LX/094;

.field private final p:LX/03V;

.field private final q:LX/2mr;

.field private final r:LX/1C2;

.field private final s:LX/1Yd;

.field private final t:LX/0bH;

.field private final u:LX/1YQ;

.field private final v:LX/23q;

.field private final w:LX/2mo;

.field private final x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/2ms;

.field private final z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tK;LX/2mn;LX/CDa;LX/1qa;LX/2ml;LX/1CK;LX/2mq;LX/2mZ;LX/1VK;LX/2mk;LX/23s;LX/0iX;LX/2mu;LX/198;LX/094;LX/03V;LX/2mr;LX/1C2;LX/1Yd;LX/0bH;LX/1YQ;LX/23q;LX/2mo;LX/2ms;LX/0Or;LX/0Or;)V
    .locals 2
    .param p26    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsAlwaysPlayVideoUnmutedEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0tK;",
            "LX/2mn;",
            "LX/CDa;",
            "LX/1qa;",
            "LX/2ml;",
            "LX/1CK;",
            "LX/2mq;",
            "LX/2mZ;",
            "LX/1VK;",
            "LX/2mk;",
            "LX/23s;",
            "LX/0iX;",
            "LX/2mu;",
            "LX/198;",
            "LX/094;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2mr;",
            "LX/1C2;",
            "LX/1Yd;",
            "LX/0bH;",
            "LX/1YQ;",
            "LX/23q;",
            "LX/2mo;",
            "LX/2ms;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/121;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859386
    iput-object p2, p0, LX/CDU;->h:LX/0tK;

    .line 1859387
    iput-object p3, p0, LX/CDU;->i:LX/2mn;

    .line 1859388
    iput-object p5, p0, LX/CDU;->b:LX/1qa;

    .line 1859389
    iput-object p6, p0, LX/CDU;->f:LX/2ml;

    .line 1859390
    iput-object p7, p0, LX/CDU;->j:LX/1CK;

    .line 1859391
    iput-object p10, p0, LX/CDU;->d:LX/1VK;

    .line 1859392
    iput-object p11, p0, LX/CDU;->e:LX/2mk;

    .line 1859393
    iput-object p12, p0, LX/CDU;->g:LX/23s;

    .line 1859394
    iput-object p13, p0, LX/CDU;->l:LX/0iX;

    .line 1859395
    iput-object p4, p0, LX/CDU;->a:LX/CDa;

    .line 1859396
    iput-object p8, p0, LX/CDU;->k:LX/2mq;

    .line 1859397
    iput-object p9, p0, LX/CDU;->c:LX/2mZ;

    .line 1859398
    move-object/from16 v0, p14

    iput-object v0, p0, LX/CDU;->m:LX/2mu;

    .line 1859399
    move-object/from16 v0, p15

    iput-object v0, p0, LX/CDU;->n:LX/198;

    .line 1859400
    move-object/from16 v0, p16

    iput-object v0, p0, LX/CDU;->o:LX/094;

    .line 1859401
    move-object/from16 v0, p17

    iput-object v0, p0, LX/CDU;->p:LX/03V;

    .line 1859402
    move-object/from16 v0, p18

    iput-object v0, p0, LX/CDU;->q:LX/2mr;

    .line 1859403
    move-object/from16 v0, p19

    iput-object v0, p0, LX/CDU;->r:LX/1C2;

    .line 1859404
    move-object/from16 v0, p20

    iput-object v0, p0, LX/CDU;->s:LX/1Yd;

    .line 1859405
    move-object/from16 v0, p21

    iput-object v0, p0, LX/CDU;->t:LX/0bH;

    .line 1859406
    move-object/from16 v0, p22

    iput-object v0, p0, LX/CDU;->u:LX/1YQ;

    .line 1859407
    move-object/from16 v0, p23

    iput-object v0, p0, LX/CDU;->v:LX/23q;

    .line 1859408
    move-object/from16 v0, p24

    iput-object v0, p0, LX/CDU;->w:LX/2mo;

    .line 1859409
    move-object/from16 v0, p26

    iput-object v0, p0, LX/CDU;->x:LX/0Or;

    .line 1859410
    move-object/from16 v0, p25

    iput-object v0, p0, LX/CDU;->y:LX/2ms;

    .line 1859411
    move-object/from16 v0, p27

    iput-object v0, p0, LX/CDU;->z:LX/0Or;

    .line 1859412
    const v1, 0x7f080e4a

    invoke-static {p1, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/CDU;->A:LX/0Ot;

    .line 1859413
    return-void
.end method

.method private static a(LX/04D;LX/1Pe;)LX/04D;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/04D;",
            "TE;)",
            "LX/04D;"
        }
    .end annotation

    .prologue
    .line 1859418
    if-eqz p0, :cond_0

    .line 1859419
    :goto_0
    return-object p0

    .line 1859420
    :cond_0
    instance-of v0, p1, LX/3Ij;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1859421
    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    check-cast p1, LX/3Ij;

    .line 1859422
    iget-object v1, p1, LX/3Ij;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1859423
    invoke-static {v0, v1}, LX/3Ik;->a(LX/1PT;Ljava/lang/String;)LX/04D;

    move-result-object p0

    goto :goto_0

    .line 1859424
    :cond_1
    check-cast p1, LX/1Po;

    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(LX/CDU;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/3FO;)LX/1bf;
    .locals 4

    .prologue
    .line 1859417
    iget-object v0, p0, LX/CDU;->b:LX/1qa;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    iget v2, p2, LX/3FO;->a:I

    sget-object v3, LX/26P;->Video:LX/26P;

    invoke-virtual {v0, v1, v2, v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2oK;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/2oL;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2oK;",
            "LX/1Pr;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ")",
            "LX/2oL;"
        }
    .end annotation

    .prologue
    .line 1859414
    invoke-static {p2}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    invoke-interface {p1, p0, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    .line 1859415
    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/2oO;->a(Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 1859416
    return-object v0
.end method

.method private static a(LX/CDU;LX/2oL;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/3FO;LX/1bf;ZZ)LX/2pa;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2oL;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "LX/3FO;",
            "LX/1bf;",
            "ZZ)",
            "LX/2pa;"
        }
    .end annotation

    .prologue
    .line 1859360
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1859361
    const-string v0, "GraphQLStoryProps"

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "SubtitlesLocalesKey"

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "ShowDeleteOptionKey"

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1859362
    if-eqz p5, :cond_0

    .line 1859363
    const-string v0, "CoverImageParamsKey"

    invoke-virtual {v2, v0, p5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1859364
    :cond_0
    iget v0, p4, LX/3FO;->d:I

    if-lez v0, :cond_3

    iget v0, p4, LX/3FO;->a:I

    int-to-double v0, v0

    iget v3, p4, LX/3FO;->d:I

    int-to-double v4, v3

    div-double/2addr v0, v4

    .line 1859365
    :goto_0
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859366
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 1859367
    if-eqz v3, :cond_1

    .line 1859368
    const-string v4, "BlurredCoverImageParamsKey"

    invoke-virtual {v2, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1859369
    :cond_1
    const-string v3, "WATCH_AND_SHOP_PLUGIN_VIDEO_PARAMS_KEY"

    iget-object v4, p0, LX/CDU;->e:LX/2mk;

    invoke-virtual {v4, p2}, LX/2mk;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1859370
    iget-object v3, p1, LX/2oL;->i:LX/7DJ;

    move-object v3, v3

    .line 1859371
    if-eqz v3, :cond_2

    .line 1859372
    const-string v3, "SphericalViewportStateKey"

    .line 1859373
    iget-object v4, p1, LX/2oL;->i:LX/7DJ;

    move-object v4, v4

    .line 1859374
    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1859375
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 1859376
    iget-object v3, p0, LX/CDU;->c:LX/2mZ;

    invoke-virtual {v3, p2, p3}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v3

    .line 1859377
    invoke-virtual {v3, p6, p7}, LX/3Im;->a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 1859378
    new-instance v4, LX/2pZ;

    invoke-direct {v4}, LX/2pZ;-><init>()V

    .line 1859379
    iput-object v3, v4, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1859380
    move-object v3, v4

    .line 1859381
    iput-wide v0, v3, LX/2pZ;->e:D

    .line 1859382
    move-object v0, v3

    .line 1859383
    invoke-virtual {v0, v2}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    return-object v0

    .line 1859384
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static a(LX/CDU;Lcom/facebook/feed/rows/core/props/FeedProps;LX/CDf;)LX/3FO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/CDf;",
            ")",
            "LX/3FO;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1859352
    if-eqz p2, :cond_0

    .line 1859353
    new-instance v0, LX/3FO;

    iget v1, p2, LX/CDf;->a:I

    iget v2, p2, LX/CDf;->b:I

    iget v3, p2, LX/CDf;->a:I

    iget v4, p2, LX/CDf;->b:I

    invoke-direct/range {v0 .. v5}, LX/3FO;-><init>(IIIII)V

    .line 1859354
    :goto_0
    return-object v0

    .line 1859355
    :cond_0
    iget-object v0, p0, LX/CDU;->h:LX/0tK;

    invoke-virtual {v0, v5}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1859356
    iget-object v0, p0, LX/CDU;->i:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2mn;->b(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    goto :goto_0

    .line 1859357
    :cond_1
    iget-object v1, p0, LX/CDU;->i:LX/2mn;

    iget-object v2, p0, LX/CDU;->k:LX/2mq;

    .line 1859358
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1859359
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v0

    invoke-virtual {v1, p1, v0}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2oL;LX/1Pq;)LX/3It;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2oL;",
            "LX/1Pq;",
            ")",
            "LX/3It;"
        }
    .end annotation

    .prologue
    .line 1859351
    new-instance v0, LX/CDR;

    invoke-direct {v0, p0, p2, p3, p1}, LX/CDR;-><init>(LX/CDU;LX/2oL;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/2oL;LX/1Pq;)LX/3Iv;
    .locals 1

    .prologue
    .line 1859425
    iget-object v0, p0, LX/CDU;->f:LX/2ml;

    iget-boolean v0, v0, LX/2ml;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/CDS;

    invoke-direct {v0, p0, p2, p3, p1}, LX/CDS;-><init>(LX/CDU;LX/2oL;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3FV;LX/3J0;LX/1Pe;)LX/3Qx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/3FV;",
            "LX/3J0;",
            "TE;)",
            "LX/3Qx;"
        }
    .end annotation

    .prologue
    .line 1859350
    iget-object v0, p0, LX/CDU;->v:LX/23q;

    if-eqz p2, :cond_0

    :goto_0
    check-cast p4, LX/1Po;

    invoke-virtual {v0, p2, p4}, LX/23q;->a(LX/3FV;LX/1Po;)LX/3Qx;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance p2, LX/3FV;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, p3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p2, p1, v1}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/CDU;
    .locals 3

    .prologue
    .line 1859342
    const-class v1, LX/CDU;

    monitor-enter v1

    .line 1859343
    :try_start_0
    sget-object v0, LX/CDU;->B:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1859344
    sput-object v2, LX/CDU;->B:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1859345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/CDU;->b(LX/0QB;)LX/CDU;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1859347
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859348
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1859349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CDf;LX/3FV;LX/1Pe;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLVideo;LX/2pa;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/CDg;)Landroid/view/View$OnClickListener;
    .locals 19
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/CDf;",
            "LX/3FV;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "LX/2pa;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/CDg;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1859332
    invoke-static/range {p6 .. p6}, LX/CDU;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1859333
    const/4 v6, 0x0

    .line 1859334
    :cond_0
    :goto_0
    return-object v6

    .line 1859335
    :cond_1
    new-instance v5, LX/CDV;

    move-object/from16 v7, p4

    check-cast v7, LX/1Pq;

    move-object/from16 v8, p4

    check-cast v8, LX/1Pr;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/CDU;->j:LX/1CK;

    move-object/from16 v6, p10

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p9

    invoke-direct/range {v5 .. v12}, LX/CDV;-><init>(LX/CDg;LX/1Pq;LX/1Pr;LX/1CK;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/video/analytics/VideoFeedStoryInfo;)V

    .line 1859336
    move-object/from16 v0, p0

    iget-object v7, v0, LX/CDU;->w:LX/2mo;

    move-object/from16 v6, p4

    check-cast v6, LX/1Pq;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v5, v6}, LX/2mo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3Iy;LX/1Pq;)LX/3Iz;

    move-result-object v18

    .line 1859337
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, v18

    move-object/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, LX/CDU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3FV;LX/3J0;LX/1Pe;)LX/3Qx;

    move-result-object v6

    .line 1859338
    if-nez v6, :cond_0

    .line 1859339
    new-instance v5, LX/CDX;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/CDU;->u:LX/1YQ;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/CDU;->n:LX/198;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/CDU;->r:LX/1C2;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/CDU;->s:LX/1Yd;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/CDU;->t:LX/0bH;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/CDU;->y:LX/2ms;

    move-object/from16 v6, p10

    move-object/from16 v13, p2

    move-object/from16 v14, p5

    move-object/from16 v15, p1

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    invoke-direct/range {v5 .. v17}, LX/CDX;-><init>(LX/CDg;LX/1YQ;LX/198;LX/1C2;LX/1Yd;LX/0bH;LX/2ms;LX/CDf;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2pa;Lcom/facebook/video/analytics/VideoFeedStoryInfo;)V

    .line 1859340
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, LX/CDX;->a(LX/3J0;)V

    .line 1859341
    new-instance v6, LX/CDe;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/CDU;->n:LX/198;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/CDU;->o:LX/094;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/CDU;->p:LX/03V;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/CDU;->q:LX/2mr;

    move-object/from16 v7, p10

    move-object/from16 v12, p7

    move-object v13, v5

    invoke-direct/range {v6 .. v13}, LX/CDe;-><init>(LX/CDg;LX/198;LX/094;LX/03V;LX/2mr;Lcom/facebook/graphql/model/GraphQLVideo;LX/CDX;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1859292
    invoke-static {p0}, LX/CDU;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/CDU;
    .locals 30

    .prologue
    .line 1859330
    new-instance v2, LX/CDU;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v4

    check-cast v4, LX/0tK;

    invoke-static/range {p0 .. p0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v5

    check-cast v5, LX/2mn;

    invoke-static/range {p0 .. p0}, LX/CDa;->a(LX/0QB;)LX/CDa;

    move-result-object v6

    check-cast v6, LX/CDa;

    invoke-static/range {p0 .. p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v7

    check-cast v7, LX/1qa;

    invoke-static/range {p0 .. p0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v8

    check-cast v8, LX/2ml;

    invoke-static/range {p0 .. p0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v9

    check-cast v9, LX/1CK;

    invoke-static/range {p0 .. p0}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v10

    check-cast v10, LX/2mq;

    const-class v11, LX/2mZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/2mZ;

    const-class v12, LX/1VK;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1VK;

    invoke-static/range {p0 .. p0}, LX/2mk;->a(LX/0QB;)LX/2mk;

    move-result-object v13

    check-cast v13, LX/2mk;

    invoke-static/range {p0 .. p0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v14

    check-cast v14, LX/23s;

    invoke-static/range {p0 .. p0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v15

    check-cast v15, LX/0iX;

    const-class v16, LX/2mu;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/2mu;

    invoke-static/range {p0 .. p0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v17

    check-cast v17, LX/198;

    invoke-static/range {p0 .. p0}, LX/094;->a(LX/0QB;)LX/094;

    move-result-object v18

    check-cast v18, LX/094;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v19

    check-cast v19, LX/03V;

    invoke-static/range {p0 .. p0}, LX/2mr;->a(LX/0QB;)LX/2mr;

    move-result-object v20

    check-cast v20, LX/2mr;

    invoke-static/range {p0 .. p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v21

    check-cast v21, LX/1C2;

    invoke-static/range {p0 .. p0}, LX/1Yd;->a(LX/0QB;)LX/1Yd;

    move-result-object v22

    check-cast v22, LX/1Yd;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v23

    check-cast v23, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v24

    check-cast v24, LX/1YQ;

    invoke-static/range {p0 .. p0}, LX/23q;->a(LX/0QB;)LX/23q;

    move-result-object v25

    check-cast v25, LX/23q;

    const-class v26, LX/2mo;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/2mo;

    invoke-static/range {p0 .. p0}, LX/2ms;->a(LX/0QB;)LX/2ms;

    move-result-object v27

    check-cast v27, LX/2ms;

    const/16 v28, 0x1494

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    const/16 v29, 0xbd8

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    invoke-direct/range {v2 .. v29}, LX/CDU;-><init>(Landroid/content/Context;LX/0tK;LX/2mn;LX/CDa;LX/1qa;LX/2ml;LX/1CK;LX/2mq;LX/2mZ;LX/1VK;LX/2mk;LX/23s;LX/0iX;LX/2mu;LX/198;LX/094;LX/03V;LX/2mr;LX/1C2;LX/1Yd;LX/0bH;LX/1YQ;LX/23q;LX/2mo;LX/2ms;LX/0Or;LX/0Or;)V

    .line 1859331
    return-object v2
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 1859329
    invoke-static {p0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;Landroid/view/View$OnClickListener;ZZZLX/CDf;LX/3FV;LX/1Pe;LX/1np;LX/1np;LX/1np;)LX/1Dg;
    .locals 26
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/04D;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/CDf;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/3FV;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1Pe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/04D;",
            "Landroid/view/View$OnClickListener;",
            "ZZZ",
            "LX/CDf;",
            "LX/3FV;",
            "TE;",
            "LX/1np",
            "<",
            "LX/CDT;",
            ">;",
            "LX/1np",
            "<",
            "LX/121;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1859293
    move-object/from16 v0, p3

    move-object/from16 v1, p10

    invoke-static {v0, v1}, LX/CDU;->a(LX/04D;LX/1Pe;)LX/04D;

    move-result-object v24

    .line 1859294
    invoke-static/range {p2 .. p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859295
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    move-object/from16 v23, v3

    check-cast v23, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1859296
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v18

    check-cast v18, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1859297
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    .line 1859298
    new-instance v15, LX/2oK;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/CDU;->d:LX/1VK;

    move-object/from16 v0, v17

    invoke-direct {v15, v0, v6, v3}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    move-object/from16 v3, p10

    .line 1859299
    check-cast v3, LX/1Pr;

    move-object/from16 v0, v17

    invoke-static {v15, v3, v0, v6}, LX/CDU;->a(LX/2oK;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/2oL;

    move-result-object v4

    .line 1859300
    move-object/from16 v0, p0

    iget-object v5, v0, LX/CDU;->g:LX/23s;

    move-object/from16 v3, p10

    check-cast v3, LX/1Po;

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v5, v0, v3}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v25

    .line 1859301
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p8

    invoke-static {v0, v1, v2}, LX/CDU;->a(LX/CDU;Lcom/facebook/feed/rows/core/props/FeedProps;LX/CDf;)LX/3FO;

    move-result-object v7

    .line 1859302
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, LX/CDU;->a(LX/CDU;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/3FO;)LX/1bf;

    move-result-object v8

    move-object/from16 v3, p0

    move-object/from16 v5, p2

    move/from16 v9, p6

    move/from16 v10, p7

    .line 1859303
    invoke-static/range {v3 .. v10}, LX/CDU;->a(LX/CDU;LX/2oL;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/3FO;LX/1bf;ZZ)LX/2pa;

    move-result-object v20

    .line 1859304
    invoke-static/range {v17 .. v17}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 1859305
    new-instance v5, LX/0AW;

    invoke-direct {v5, v3}, LX/0AW;-><init>(LX/162;)V

    invoke-static/range {v17 .. v17}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    invoke-virtual {v5, v3}, LX/0AW;->a(Z)LX/0AW;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, LX/0AW;->a(LX/04H;)LX/0AW;

    move-result-object v3

    sget-object v5, LX/04g;->UNSET:LX/04g;

    invoke-virtual {v3, v5}, LX/0AW;->a(LX/04g;)LX/0AW;

    move-result-object v3

    invoke-virtual {v3}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v11

    .line 1859306
    new-instance v9, LX/395;

    new-instance v3, LX/0AV;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v10

    move-object v12, v8

    move-object v13, v6

    move-object/from16 v14, p2

    invoke-direct/range {v9 .. v14}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1859307
    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, LX/395;->a(LX/04D;)LX/395;

    .line 1859308
    new-instance v22, LX/CDg;

    invoke-direct/range {v22 .. v22}, LX/CDg;-><init>()V

    .line 1859309
    invoke-virtual {v4}, LX/2oL;->b()LX/2oO;

    move-result-object v3

    move-object/from16 v0, v22

    iput-object v3, v0, LX/CDg;->c:LX/2oO;

    .line 1859310
    move-object/from16 v0, v22

    iput-object v9, v0, LX/CDg;->a:LX/395;

    .line 1859311
    move-object/from16 v0, v22

    iput-object v4, v0, LX/CDg;->d:LX/2oL;

    .line 1859312
    move-object/from16 v0, p0

    iget-object v3, v0, LX/CDU;->x:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move-object/from16 v0, v22

    iput-boolean v3, v0, LX/CDg;->f:Z

    .line 1859313
    move-object/from16 v0, p0

    iget-object v3, v0, LX/CDU;->l:LX/0iX;

    move-object/from16 v0, v22

    iput-object v3, v0, LX/CDg;->g:LX/0iX;

    .line 1859314
    move-object/from16 v0, v22

    iput-object v15, v0, LX/CDg;->k:LX/2oK;

    .line 1859315
    if-eqz p7, :cond_0

    .line 1859316
    move-object/from16 v0, v22

    iget-object v3, v0, LX/CDg;->c:LX/2oO;

    move/from16 v0, p7

    invoke-virtual {v3, v0}, LX/2oO;->b(Z)V

    .line 1859317
    :cond_0
    if-eqz p5, :cond_1

    if-eqz p4, :cond_1

    .line 1859318
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Cannot accept a overrideClickListener if disableClickListener is true"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1859319
    :cond_1
    if-nez p5, :cond_2

    .line 1859320
    if-eqz p4, :cond_3

    .line 1859321
    move-object/from16 v0, p13

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    move-object/from16 v3, p10

    .line 1859322
    check-cast v3, LX/1Pq;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v4, v3}, LX/CDU;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/2oL;LX/1Pq;)LX/3Iv;

    move-result-object v8

    move-object/from16 v3, p10

    .line 1859323
    check-cast v3, LX/1Pq;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v4, v3}, LX/CDU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2oL;LX/1Pq;)LX/3It;

    move-result-object v9

    .line 1859324
    new-instance v3, LX/CDT;

    invoke-direct {v3}, LX/CDT;-><init>()V

    move-object/from16 v0, p11

    invoke-virtual {v0, v3}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859325
    move-object/from16 v0, p0

    iget-object v3, v0, LX/CDU;->z:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/121;

    sget-object v10, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/CDU;->A:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual/range {p11 .. p11}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/39A;

    invoke-virtual {v3, v10, v5, v7}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    move-result-object v3

    move-object/from16 v0, p12

    invoke-virtual {v0, v3}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859326
    move-object/from16 v0, p0

    iget-object v3, v0, LX/CDU;->a:LX/CDa;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/CDa;->c(LX/1De;)LX/CDY;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/CDY;->a(LX/3Iv;)LX/CDY;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/CDY;->a(LX/3It;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, LX/CDY;->a(LX/CDg;)LX/CDY;

    move-result-object v3

    invoke-static/range {p8 .. p8}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/CDf;)I

    move-result v5

    invoke-virtual {v3, v5}, LX/CDY;->h(I)LX/CDY;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, LX/CDY;->a(LX/04H;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, LX/CDY;->a(LX/04D;)LX/CDY;

    move-result-object v3

    invoke-static/range {v18 .. v18}, LX/1VO;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    invoke-virtual {v3, v5}, LX/CDY;->c(Z)LX/CDY;

    move-result-object v3

    invoke-static/range {v18 .. v18}, LX/CDU;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    invoke-virtual {v3, v5}, LX/CDY;->b(Z)LX/CDY;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/CDY;->a(Z)LX/CDY;

    move-result-object v3

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/CDY;->a(LX/0Px;)LX/CDY;

    move-result-object v3

    invoke-virtual {v3, v11}, LX/CDY;->a(Lcom/facebook/video/analytics/VideoFeedStoryInfo;)LX/CDY;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/CDY;->a(LX/2oL;)LX/CDY;

    move-result-object v3

    new-instance v4, LX/093;

    invoke-direct {v4}, LX/093;-><init>()V

    invoke-virtual {v3, v4}, LX/CDY;->a(LX/093;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, LX/CDY;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, LX/CDY;->a(LX/1Pe;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/CDU;->m:LX/2mu;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2mu;->a(Ljava/lang/Boolean;)Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CDY;->a(LX/3Ge;)LX/CDY;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, LX/CDY;->a(LX/2pa;)LX/CDY;

    move-result-object v3

    .line 1859327
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-static/range {p1 .. p1}, LX/CDQ;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    return-object v3

    :cond_3
    move-object/from16 v12, p0

    move-object/from16 v13, p2

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v16, p10

    move-object/from16 v19, v6

    move-object/from16 v21, v11

    .line 1859328
    invoke-direct/range {v12 .. v22}, LX/CDU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CDf;LX/3FV;LX/1Pe;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLVideo;LX/2pa;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/CDg;)Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v0, v3}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
