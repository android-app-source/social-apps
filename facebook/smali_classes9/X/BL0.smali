.class public final LX/BL0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BL2;


# direct methods
.method public constructor <init>(LX/BL2;)V
    .locals 0

    .prologue
    .line 1775107
    iput-object p1, p0, LX/BL0;->a:LX/BL2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1775108
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v9, 0x0

    .line 1775109
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1775110
    if-nez p1, :cond_0

    move-object v0, v7

    .line 1775111
    :goto_0
    return-object v0

    .line 1775112
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1775113
    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;->a()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel;->a()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel$GroupsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v8, v9

    .line 1775114
    :goto_1
    if-ge v8, v11, :cond_2

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    .line 1775115
    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 1775116
    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v9}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v9}, LX/15i;->g(II)I

    move-result v1

    .line 1775117
    invoke-virtual {v2, v1, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1775118
    :goto_2
    new-instance v1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x3

    invoke-direct/range {v1 .. v6}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 1775119
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1775120
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 1775121
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    move-object v0, v7

    .line 1775122
    goto :goto_0
.end method
