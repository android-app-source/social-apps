.class public final LX/B7E;
.super LX/B7B;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/text/Spanned;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:LX/B7K;

.field private k:LX/B7H;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenPage;Lcom/facebook/graphql/model/GraphQLLeadGenData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/B7K;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1747778
    invoke-direct {p0}, LX/B7B;-><init>()V

    .line 1747779
    if-nez p2, :cond_0

    .line 1747780
    :goto_0
    return-void

    .line 1747781
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->k()Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;

    move-result-object v0

    .line 1747782
    const/4 v1, 0x0

    .line 1747783
    iput-object p3, p0, LX/B7E;->f:Ljava/lang/String;

    .line 1747784
    iput-object p4, p0, LX/B7E;->e:Ljava/lang/String;

    .line 1747785
    iput-object p6, p0, LX/B7E;->i:Ljava/lang/String;

    .line 1747786
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/B7E;->g:Ljava/lang/String;

    .line 1747787
    iput-object p7, p0, LX/B7E;->j:LX/B7K;

    .line 1747788
    if-eqz p1, :cond_1

    .line 1747789
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->j()LX/0Px;

    move-result-object v4

    .line 1747790
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;

    .line 1747791
    if-eqz v0, :cond_3

    .line 1747792
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->a()Ljava/lang/String;

    move-result-object v2

    .line 1747793
    sget-object v6, LX/B7D;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 1747794
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_1

    .line 1747795
    :pswitch_0
    iput-object v2, p0, LX/B7E;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1747796
    goto :goto_2

    .line 1747797
    :pswitch_1
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, LX/B7E;->c:Landroid/text/Spanned;

    move-object v0, v1

    .line 1747798
    goto :goto_2

    :pswitch_2
    move-object v0, v2

    .line 1747799
    goto :goto_2

    .line 1747800
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1747801
    :cond_2
    const/4 v2, 0x0

    .line 1747802
    :goto_3
    move v2, v2

    .line 1747803
    if-eqz v2, :cond_3

    .line 1747804
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->k()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/B7E;->b:LX/0Px;

    .line 1747805
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7E;->a:Ljava/lang/String;

    .line 1747806
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->j()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/B7E;->d:LX/0Px;

    .line 1747807
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContent;->a()Ljava/lang/String;

    move-result-object v1

    .line 1747808
    :cond_3
    if-eqz v1, :cond_4

    .line 1747809
    iput-object v1, p0, LX/B7E;->h:Ljava/lang/String;

    .line 1747810
    :goto_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1747811
    invoke-interface {v0, p8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1747812
    new-instance v1, LX/B7H;

    invoke-direct {v1, v0}, LX/B7H;-><init>(Ljava/util/List;)V

    iput-object v1, p0, LX/B7E;->k:LX/B7H;

    goto/16 :goto_0

    .line 1747813
    :cond_4
    iput-object p5, p0, LX/B7E;->h:Ljava/lang/String;

    goto :goto_4

    :cond_5
    const/4 v2, 0x1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
