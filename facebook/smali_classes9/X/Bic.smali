.class public final LX/Bic;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:LX/0oG;

.field public final synthetic c:LX/Bj7;

.field public final synthetic d:Landroid/net/Uri;

.field public final synthetic e:Landroid/os/Handler;

.field public final synthetic f:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final synthetic g:Lcom/facebook/events/common/ActionMechanism;

.field public final synthetic h:LX/Bid;


# direct methods
.method public constructor <init>(LX/Bid;Landroid/support/v4/app/DialogFragment;LX/0oG;LX/Bj7;Landroid/net/Uri;Landroid/os/Handler;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 0

    .prologue
    .line 1810606
    iput-object p1, p0, LX/Bic;->h:LX/Bid;

    iput-object p2, p0, LX/Bic;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/Bic;->b:LX/0oG;

    iput-object p4, p0, LX/Bic;->c:LX/Bj7;

    iput-object p5, p0, LX/Bic;->d:Landroid/net/Uri;

    iput-object p6, p0, LX/Bic;->e:Landroid/os/Handler;

    iput-object p7, p0, LX/Bic;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object p8, p0, LX/Bic;->g:Lcom/facebook/events/common/ActionMechanism;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1810607
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v0, v0, LX/Bid;->c:LX/1nQ;

    .line 1810608
    const-string v1, "event_composer_create_failure"

    const-string v2, "event_composer"

    invoke-static {v0, v1, v2}, LX/1nQ;->f(LX/1nQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 1810609
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v0, v0, LX/Bid;->b:LX/BiW;

    .line 1810610
    iget-object v1, v0, LX/BiW;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, v0, LX/BiW;->a:LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 1810611
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1810612
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v1, p0, LX/Bic;->a:Landroid/support/v4/app/DialogFragment;

    invoke-static {v0, v1}, LX/Bid;->a$redex0(LX/Bid;Landroid/support/v4/app/DialogFragment;)V

    .line 1810613
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    .line 1810614
    iget-object v1, v0, LX/Bid;->g:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f082142

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1810615
    sget-object v0, LX/Bid;->a:Ljava/lang/Class;

    const-string v1, "Error creating event"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1810616
    invoke-direct {p0}, LX/Bic;->a()V

    .line 1810617
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1810618
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1810619
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v1, p0, LX/Bic;->a:Landroid/support/v4/app/DialogFragment;

    invoke-static {v0, v1}, LX/Bid;->a$redex0(LX/Bid;Landroid/support/v4/app/DialogFragment;)V

    .line 1810620
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1810621
    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    .line 1810622
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1810623
    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1810624
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_2

    .line 1810625
    iget-object v0, p0, LX/Bic;->b:LX/0oG;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Bic;->b:LX/0oG;

    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1810626
    iget-object v0, p0, LX/Bic;->b:LX/0oG;

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1810627
    :goto_0
    iget-object v0, p0, LX/Bic;->c:LX/Bj7;

    invoke-virtual {v0, v4, v5}, LX/Bj7;->a(J)V

    .line 1810628
    iget-object v0, p0, LX/Bic;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1810629
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v1, v0, LX/Bid;->i:LX/Bib;

    iget-object v2, p0, LX/Bic;->e:Landroid/os/Handler;

    iget-object v3, p0, LX/Bic;->d:Landroid/net/Uri;

    iget-object v6, p0, LX/Bic;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v7, p0, LX/Bic;->g:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v1 .. v7}, LX/Bib;->a(Landroid/os/Handler;Landroid/net/Uri;JLcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1810630
    :cond_0
    :goto_1
    return-void

    .line 1810631
    :cond_1
    iget-object v0, p0, LX/Bic;->h:LX/Bid;

    iget-object v0, v0, LX/Bid;->c:LX/1nQ;

    .line 1810632
    const-string v1, "event_composer_create_success"

    const-string v2, "event_composer"

    invoke-static {v0, v1, v2}, LX/1nQ;->f(LX/1nQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 1810633
    goto :goto_0

    .line 1810634
    :cond_2
    invoke-direct {p0}, LX/Bic;->a()V

    goto :goto_1

    .line 1810635
    :cond_3
    invoke-direct {p0}, LX/Bic;->a()V

    goto :goto_1
.end method
