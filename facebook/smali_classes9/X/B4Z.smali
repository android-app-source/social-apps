.class public final LX/B4Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1742366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1742367
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742368
    :goto_0
    return v1

    .line 1742369
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742370
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1742371
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1742372
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1742373
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1742374
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1742375
    invoke-static {p0, p1}, LX/B52;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1742376
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1742377
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1742378
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_9

    .line 1742379
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742380
    :goto_2
    move v0, v3

    .line 1742381
    goto :goto_1

    .line 1742382
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1742383
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1742384
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1742385
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1742386
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 1742387
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1742388
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1742389
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_5

    if-eqz v6, :cond_5

    .line 1742390
    const-string v7, "has_next_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1742391
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_3

    .line 1742392
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1742393
    :cond_7
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1742394
    if-eqz v0, :cond_8

    .line 1742395
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 1742396
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v0, v3

    move v5, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1742350
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1742351
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1742352
    if-eqz v0, :cond_0

    .line 1742353
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742354
    invoke-static {p0, v0, p2, p3}, LX/B52;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1742355
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1742356
    if-eqz v0, :cond_2

    .line 1742357
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742358
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1742359
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1742360
    if-eqz v1, :cond_1

    .line 1742361
    const-string p1, "has_next_page"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742362
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1742363
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1742364
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1742365
    return-void
.end method
