.class public LX/AnB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1712160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 1712159
    int-to-float v0, p0

    const v1, 0x3ff745d1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(LX/0hB;II)I
    .locals 2

    .prologue
    .line 1712161
    invoke-virtual {p0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1712162
    mul-int/lit8 v1, p1, 0x2

    sub-int/2addr v0, v1

    div-int/lit8 v1, p2, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/widget/TextView;I)V
    .locals 2

    .prologue
    .line 1712152
    invoke-interface {p1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1712153
    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1712154
    invoke-interface {p1}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1712155
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1712156
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1712157
    return-void

    .line 1712158
    :cond_0
    invoke-virtual {p0, p3, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
