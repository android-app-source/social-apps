.class public LX/BPC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:LX/BPA;

.field public e:LX/BPA;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1780788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780789
    iput-boolean v0, p0, LX/BPC;->a:Z

    .line 1780790
    iput-boolean v0, p0, LX/BPC;->b:Z

    .line 1780791
    iput-boolean v0, p0, LX/BPC;->c:Z

    .line 1780792
    sget-object v0, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    iput-object v0, p0, LX/BPC;->d:LX/BPA;

    .line 1780793
    sget-object v0, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    iput-object v0, p0, LX/BPC;->e:LX/BPA;

    .line 1780794
    return-void
.end method

.method public static a(LX/0QB;)LX/BPC;
    .locals 3

    .prologue
    .line 1780795
    const-class v1, LX/BPC;

    monitor-enter v1

    .line 1780796
    :try_start_0
    sget-object v0, LX/BPC;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1780797
    sput-object v2, LX/BPC;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1780798
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780799
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1780800
    new-instance v0, LX/BPC;

    invoke-direct {v0}, LX/BPC;-><init>()V

    .line 1780801
    move-object v0, v0

    .line 1780802
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1780803
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BPC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780804
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1780805
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final e()Z
    .locals 1

    .prologue
    .line 1780786
    iget-boolean v0, p0, LX/BPC;->c:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1780785
    iget-boolean v0, p0, LX/BPC;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BPC;->e:LX/BPA;

    invoke-static {v0}, LX/BPA;->isFailLoadState(LX/BPA;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BPC;->d:LX/BPA;

    invoke-static {v0}, LX/BPA;->isFailLoadState(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1780787
    iget-boolean v0, p0, LX/BPC;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPC;->e:LX/BPA;

    invoke-static {v0}, LX/BPA;->isPhotoLoaded(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1780784
    iget-boolean v0, p0, LX/BPC;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPC;->d:LX/BPA;

    invoke-static {v0}, LX/BPA;->isPhotoLoaded(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1780783
    invoke-virtual {p0}, LX/BPC;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPC;->e:LX/BPA;

    invoke-static {v0}, LX/BPA;->isPhotoLoaded(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1780782
    iget-boolean v0, p0, LX/BPC;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPC;->e:LX/BPA;

    invoke-static {v0}, LX/BPA;->isFinalPhotoLoadState(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPC;->d:LX/BPA;

    invoke-static {v0}, LX/BPA;->isFinalPhotoLoadState(LX/BPA;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
