.class public LX/B2y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/B4H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/B3W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/app/Activity;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

.field private k:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1739406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739407
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1739408
    iput-object v0, p0, LX/B2y;->g:LX/0Ot;

    .line 1739409
    const/4 v0, 0x0

    iput-object v0, p0, LX/B2y;->k:LX/1Mv;

    .line 1739410
    iput-object p1, p0, LX/B2y;->h:Landroid/app/Activity;

    .line 1739411
    iput-object p2, p0, LX/B2y;->i:Ljava/lang/String;

    .line 1739412
    iput-object p3, p0, LX/B2y;->j:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1739413
    return-void
.end method

.method public static a(LX/B2y;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0tX;Ljava/util/concurrent/Executor;LX/B4H;LX/B3W;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B2y;",
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0tX;",
            "Ljava/util/concurrent/Executor;",
            "LX/B4H;",
            "LX/B3W;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1739405
    iput-object p1, p0, LX/B2y;->a:LX/0Or;

    iput-object p2, p0, LX/B2y;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, LX/B2y;->c:LX/0tX;

    iput-object p4, p0, LX/B2y;->d:Ljava/util/concurrent/Executor;

    iput-object p5, p0, LX/B2y;->e:LX/B4H;

    iput-object p6, p0, LX/B2y;->f:LX/B3W;

    iput-object p7, p0, LX/B2y;->g:LX/0Ot;

    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1739397
    iget-object v0, p0, LX/B2y;->e:LX/B4H;

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v2, "extra_profile_pic_expiration"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "staging_ground_photo_caption"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "existing"

    invoke-virtual/range {v0 .. v5}, LX/B4H;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1739398
    new-instance v1, LX/4BY;

    invoke-direct {v1, p1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1739399
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1739400
    invoke-virtual {v1}, LX/4BY;->show()V

    .line 1739401
    new-instance v2, LX/B2x;

    invoke-direct {v2, p0, v1, p1}, LX/B2x;-><init>(LX/B2y;LX/4BY;Landroid/app/Activity;)V

    .line 1739402
    iget-object v1, p0, LX/B2y;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1739403
    new-instance v1, LX/1Mv;

    invoke-direct {v1, v0, v2}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v1, p0, LX/B2y;->k:LX/1Mv;

    .line 1739404
    return-void
.end method

.method public static c(LX/B2y;)V
    .locals 3

    .prologue
    .line 1739395
    iget-object v0, p0, LX/B2y;->h:Landroid/app/Activity;

    iget-object v1, p0, LX/B2y;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1739396
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1739390
    iget-object v1, p0, LX/B2y;->i:Ljava/lang/String;

    invoke-static {v1}, LX/B3W;->b(Ljava/lang/String;)LX/B4o;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1739391
    iget-object v1, p0, LX/B2y;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1739392
    new-instance v1, LX/B2w;

    invoke-direct {v1, p0}, LX/B2w;-><init>(LX/B2y;)V

    .line 1739393
    iget-object v2, p0, LX/B2y;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1739394
    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1739360
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 1739361
    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    .line 1739362
    :cond_0
    :goto_0
    return-void

    .line 1739363
    :cond_1
    invoke-direct {p0, p1, p4}, LX/B2y;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 1739367
    iget-object v0, p0, LX/B2y;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SF;

    .line 1739368
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1739369
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, Ljava/util/Calendar;->add(II)V

    .line 1739370
    new-instance v2, LX/5SJ;

    invoke-direct {v2}, LX/5SJ;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v2

    iget-object v3, p0, LX/B2y;->j:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1739371
    iput-object v3, v2, LX/5SJ;->l:LX/5QV;

    .line 1739372
    move-object v2, v2

    .line 1739373
    iput-boolean v8, v2, LX/5SJ;->o:Z

    .line 1739374
    move-object v2, v2

    .line 1739375
    sget-object v3, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v3}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v3

    .line 1739376
    iput-object v3, v2, LX/5SJ;->c:Ljava/lang/String;

    .line 1739377
    move-object v2, v2

    .line 1739378
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1739379
    iput-wide v4, v2, LX/5SJ;->e:J

    .line 1739380
    move-object v1, v2

    .line 1739381
    const-string v2, "profile_picture_overlay_birthday"

    .line 1739382
    iput-object v2, v1, LX/5SJ;->j:Ljava/lang/String;

    .line 1739383
    move-object v1, v1

    .line 1739384
    iput-boolean v8, v1, LX/5SJ;->r:Z

    .line 1739385
    move-object v1, v1

    .line 1739386
    invoke-virtual {v1}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    .line 1739387
    iget-object v2, p0, LX/B2y;->h:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1739388
    iget-object v1, p0, LX/B2y;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/B2y;->h:Landroid/app/Activity;

    invoke-interface {v1, v0, v8, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1739389
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1739364
    iget-object v0, p0, LX/B2y;->k:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1739365
    iget-object v0, p0, LX/B2y;->k:LX/1Mv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1739366
    :cond_0
    return-void
.end method
