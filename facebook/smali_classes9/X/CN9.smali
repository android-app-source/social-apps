.class public LX/CN9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Vc;


# instance fields
.field private final a:LX/3Rb;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;

.field private final d:Z

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/8ue;

.field private final g:Ljava/lang/String;

.field private final h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private i:I

.field private j:Z


# direct methods
.method private constructor <init>(LX/3Rb;Ljava/lang/String;Landroid/net/Uri;ZLX/0Px;LX/8ue;Ljava/lang/String;I)V
    .locals 0
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Rb;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/8ue;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1881333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881334
    iput-object p1, p0, LX/CN9;->a:LX/3Rb;

    .line 1881335
    iput-object p2, p0, LX/CN9;->b:Ljava/lang/String;

    .line 1881336
    iput-object p3, p0, LX/CN9;->c:Landroid/net/Uri;

    .line 1881337
    iput-boolean p4, p0, LX/CN9;->d:Z

    .line 1881338
    iput-object p5, p0, LX/CN9;->e:LX/0Px;

    .line 1881339
    iput-object p6, p0, LX/CN9;->f:LX/8ue;

    .line 1881340
    iput-object p7, p0, LX/CN9;->g:Ljava/lang/String;

    .line 1881341
    iput p8, p0, LX/CN9;->h:I

    .line 1881342
    return-void
.end method

.method public synthetic constructor <init>(LX/3Rb;Ljava/lang/String;Landroid/net/Uri;ZLX/0Px;LX/8ue;Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 1881332
    invoke-direct/range {p0 .. p8}, LX/CN9;-><init>(LX/3Rb;Ljava/lang/String;Landroid/net/Uri;ZLX/0Px;LX/8ue;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1881326
    iget-boolean v0, p0, LX/CN9;->d:Z

    if-eqz v0, :cond_0

    .line 1881327
    const/4 v0, 0x0

    .line 1881328
    :goto_0
    return v0

    .line 1881329
    :cond_0
    iget-object v0, p0, LX/CN9;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1881330
    const/4 v0, 0x1

    goto :goto_0

    .line 1881331
    :cond_1
    iget-object v0, p0, LX/CN9;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(III)LX/1bf;
    .locals 2

    .prologue
    .line 1881320
    invoke-virtual {p0}, LX/CN9;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1881321
    iget-object v0, p0, LX/CN9;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1881322
    iget-object v0, p0, LX/CN9;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1881323
    :goto_1
    return-object v0

    .line 1881324
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1881325
    :cond_1
    iget-object v1, p0, LX/CN9;->a:LX/3Rb;

    iget-object v0, p0, LX/CN9;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0, p2, p3}, LX/3Rb;->a(LX/8t9;II)LX/1bf;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(III)LX/1bf;
    .locals 1

    .prologue
    .line 1881317
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/CN9;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1881318
    :cond_0
    const/4 v0, 0x0

    .line 1881319
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/CN9;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {v0, p2}, LX/3Rb;->a(Lcom/facebook/user/model/UserKey;I)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/8ue;
    .locals 1

    .prologue
    .line 1881316
    iget-object v0, p0, LX/CN9;->f:LX/8ue;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1881343
    iget-object v0, p0, LX/CN9;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1881313
    invoke-virtual {p0}, LX/CN9;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1881314
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1881315
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/CN9;->e:LX/0Px;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1881312
    iget-object v0, p0, LX/CN9;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1881297
    if-ne p0, p1, :cond_1

    .line 1881298
    :cond_0
    :goto_0
    return v0

    .line 1881299
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1881300
    :cond_3
    invoke-virtual {p0}, LX/CN9;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1881301
    goto :goto_0

    .line 1881302
    :cond_4
    check-cast p1, LX/CN9;

    .line 1881303
    iget-object v2, p0, LX/CN9;->b:Ljava/lang/String;

    iget-object v3, p1, LX/CN9;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CN9;->c:Landroid/net/Uri;

    iget-object v3, p1, LX/CN9;->c:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CN9;->e:LX/0Px;

    if-nez v2, :cond_5

    iget-object v2, p1, LX/CN9;->e:LX/0Px;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, LX/CN9;->e:LX/0Px;

    iget-object v3, p1, LX/CN9;->e:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/CN9;->f:LX/8ue;

    iget-object v3, p1, LX/CN9;->f:LX/8ue;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/CN9;->g:Ljava/lang/String;

    iget-object v3, p1, LX/CN9;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget v2, p0, LX/CN9;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, LX/CN9;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 1881311
    iget v0, p0, LX/CN9;->h:I

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1881305
    iget-boolean v0, p0, LX/CN9;->j:Z

    if-eqz v0, :cond_0

    .line 1881306
    iget v0, p0, LX/CN9;->i:I

    .line 1881307
    :goto_0
    return v0

    .line 1881308
    :cond_0
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/CN9;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    iget-object v1, p0, LX/CN9;->c:Landroid/net/Uri;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, LX/CN9;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/CN9;->f:LX/8ue;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/CN9;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/CN9;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, LX/CN9;->i:I

    .line 1881309
    iput-boolean v3, p0, LX/CN9;->j:Z

    .line 1881310
    iget v0, p0, LX/CN9;->i:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1881304
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mDisplayName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/CN9;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSingleImageUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/CN9;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTileUserKeys="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/CN9;->e:LX/0Px;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
