.class public LX/CNz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1882398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882399
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CNz;->c:Ljava/util/HashMap;

    .line 1882400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CNz;->d:Ljava/util/ArrayList;

    .line 1882401
    iput v1, p0, LX/CNz;->e:I

    .line 1882402
    iput-boolean v1, p0, LX/CNz;->a:Z

    .line 1882403
    const/4 v0, 0x0

    iput-object v0, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1882404
    return-void
.end method

.method public static a(LX/CNz;LX/CNb;LX/CNq;Ljava/util/List;Ljava/util/List;I)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CNb;",
            "LX/CNq;",
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1882405
    const-string v2, "canceled"

    invoke-virtual {p1, v2, v0}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/CNz;->a:Z

    if-eqz v2, :cond_1

    .line 1882406
    :cond_0
    :goto_0
    return v0

    .line 1882407
    :cond_1
    const-string v2, "type"

    const-string v3, "FIRE_ONCE"

    invoke-virtual {p1, v2, v3}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1882408
    const-string v3, "BLOCKING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1882409
    iput-boolean v1, p0, LX/CNz;->g:Z

    .line 1882410
    iput-boolean v0, p0, LX/CNz;->f:Z

    .line 1882411
    iget-object v2, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v2, :cond_0

    .line 1882412
    :cond_2
    :goto_1
    iget-object v0, p0, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1882413
    if-eqz p3, :cond_7

    .line 1882414
    iget-object v0, p2, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->a()V

    .line 1882415
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNe;

    .line 1882416
    invoke-interface {v0}, LX/CNe;->a()V

    goto :goto_2

    .line 1882417
    :cond_3
    const-string v3, "CANCEL_PENDING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1882418
    iput-boolean v0, p0, LX/CNz;->g:Z

    .line 1882419
    iput-boolean v0, p0, LX/CNz;->f:Z

    .line 1882420
    iget-object v2, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_2

    .line 1882421
    iget-object v2, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1882422
    const/4 v0, 0x0

    iput-object v0, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 1882423
    :cond_4
    const-string v3, "REQUEST_ORDER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1882424
    iput-boolean v1, p0, LX/CNz;->g:Z

    .line 1882425
    iput-boolean v1, p0, LX/CNz;->f:Z

    goto :goto_1

    .line 1882426
    :cond_5
    const-string v3, "FIRE_ONCE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1882427
    if-eqz p5, :cond_2

    goto :goto_0

    .line 1882428
    :cond_6
    iget-object v0, p2, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->b()V

    .line 1882429
    :cond_7
    new-instance v0, LX/5eJ;

    invoke-direct {v0}, LX/5eJ;-><init>()V

    move-object v0, v0

    .line 1882430
    const-string v2, "params"

    .line 1882431
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1882432
    const-string v5, "pixel_ratio"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    invoke-virtual {v6}, LX/0wC;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1882433
    const-string v5, "styles_id"

    const-string v6, "701268200040015"

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1882434
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 1882435
    const-string v6, "async-payload"

    const-string v7, ""

    invoke-virtual {p1, v6, v7}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1882436
    const-string v7, "nt_context"

    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v7, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1882437
    const-string v4, "payload"

    invoke-virtual {v5, v4, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1882438
    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1882439
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1882440
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1882441
    invoke-virtual {p2}, LX/CNq;->g()LX/0tX;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1882442
    iget-object v0, p0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/CNy;

    invoke-direct {v2, p0, p2, p5, p4}, LX/CNy;-><init>(LX/CNz;LX/CNq;ILjava/util/List;)V

    invoke-virtual {p2}, LX/CNq;->n()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move v0, v1

    .line 1882443
    goto/16 :goto_0
.end method
