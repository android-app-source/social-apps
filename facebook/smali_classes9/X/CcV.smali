.class public LX/CcV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CcT;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CcW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1921270
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CcV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CcW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921271
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1921272
    iput-object p1, p0, LX/CcV;->b:LX/0Ot;

    .line 1921273
    return-void
.end method

.method public static a(LX/0QB;)LX/CcV;
    .locals 4

    .prologue
    .line 1921259
    const-class v1, LX/CcV;

    monitor-enter v1

    .line 1921260
    :try_start_0
    sget-object v0, LX/CcV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921261
    sput-object v2, LX/CcV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921262
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921263
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921264
    new-instance v3, LX/CcV;

    const/16 p0, 0x2e7f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CcV;-><init>(LX/0Ot;)V

    .line 1921265
    move-object v0, v3

    .line 1921266
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921267
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CcV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921268
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1921251
    check-cast p2, LX/CcU;

    .line 1921252
    iget-object v0, p0, LX/CcV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CcW;

    iget-object v1, p2, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p2, 0x0

    .line 1921253
    invoke-static {v1, p2, p2}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1921254
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 p0, 0x18

    invoke-interface {v2, p0}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 1921255
    :goto_0
    move-object v0, v2

    .line 1921256
    return-object v0

    :cond_0
    iget-object v2, v0, LX/CcW;->a:LX/1WX;

    const p0, 0x7f0e0a01

    invoke-virtual {v2, p1, p2, p0}, LX/1WX;->a(LX/1De;II)LX/1XJ;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v2

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, LX/1XJ;->c(Z)LX/1XJ;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const/16 p2, 0xc

    invoke-interface {v2, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1921257
    invoke-static {}, LX/1dS;->b()V

    .line 1921258
    const/4 v0, 0x0

    return-object v0
.end method
