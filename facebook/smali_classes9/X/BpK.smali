.class public LX/BpK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xN",
            "<",
            "LX/Bpg;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1xN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822927
    iput-object p1, p0, LX/BpK;->a:LX/0Ot;

    .line 1822928
    return-void
.end method

.method public static a(LX/0QB;)LX/BpK;
    .locals 4

    .prologue
    .line 1822929
    const-class v1, LX/BpK;

    monitor-enter v1

    .line 1822930
    :try_start_0
    sget-object v0, LX/BpK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822931
    sput-object v2, LX/BpK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822932
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822933
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822934
    new-instance v3, LX/BpK;

    const/16 p0, 0x823

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BpK;-><init>(LX/0Ot;)V

    .line 1822935
    move-object v0, v3

    .line 1822936
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822937
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BpK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822938
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822939
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/5kD;LX/Bpg;Lcom/facebook/common/callercontext/CallerContext;LX/1np;LX/1np;)LX/1Dg;
    .locals 2
    .param p2    # LX/5kD;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/Bpg;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/5kD;",
            "LX/Bpg;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "LX/1bf;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1822940
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1822941
    if-nez v0, :cond_0

    .line 1822942
    const/4 v0, 0x0

    .line 1822943
    :goto_0
    return-object v0

    .line 1822944
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1822945
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    .line 1822946
    iput-object v1, p6, LX/1np;->a:Ljava/lang/Object;

    .line 1822947
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1eo;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1822948
    iput-object v1, p5, LX/1np;->a:Ljava/lang/Object;

    .line 1822949
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    .line 1822950
    iput-object v0, v1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1822951
    move-object v1, v1

    .line 1822952
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1822953
    new-instance p2, LX/23u;

    invoke-direct {p2}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p5

    .line 1822954
    iput-object p5, p2, LX/23u;->m:Ljava/lang/String;

    .line 1822955
    move-object p2, p2

    .line 1822956
    invoke-virtual {p2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    move-object p2, p2

    .line 1822957
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p2

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    move-object v1, v1

    .line 1822958
    iget-object v0, p0, LX/BpK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xN;

    invoke-virtual {v0, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/22O;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/22O;

    move-result-object v0

    const/4 v1, 0x0

    .line 1822959
    iget-object p0, v0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-boolean v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->g:Z

    .line 1822960
    move-object v0, v0

    .line 1822961
    const v1, -0x739a120

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 1822962
    iget-object p0, v0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    .line 1822963
    move-object v0, v0

    .line 1822964
    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method
