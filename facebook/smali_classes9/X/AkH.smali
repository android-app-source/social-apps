.class public final LX/AkH;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final l:LX/AkE;

.field public m:LX/BYG;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/AkE;)V
    .locals 0

    .prologue
    .line 1708918
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1708919
    iput-object p2, p0, LX/AkH;->l:LX/AkE;

    .line 1708920
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1708921
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3fdbe064

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1708922
    iget-object v1, p0, LX/AkH;->m:LX/BYG;

    if-eqz v1, :cond_0

    .line 1708923
    iget-object v1, p0, LX/AkH;->l:LX/AkE;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/AkH;->m:LX/BYG;

    .line 1708924
    iget-object v4, v1, LX/AkE;->a:Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;

    iget-object v4, v4, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->f:LX/AkN;

    .line 1708925
    if-nez v3, :cond_1

    .line 1708926
    :goto_0
    const v1, -0x6aba4c05

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1708927
    :cond_0
    iget-object v1, p0, LX/AkH;->l:LX/AkE;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1708928
    iget-object v3, v1, LX/AkE;->a:Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;

    iget-object v3, v3, Lcom/facebook/feed/inlinecomposer/multirow/work/InlineComposerHscrollRecyclerViewPartDefinition;->f:LX/AkN;

    invoke-virtual {v3, v2}, LX/AkN;->a(Landroid/content/Context;)V

    .line 1708929
    goto :goto_0

    .line 1708930
    :cond_1
    sget-object v5, LX/21D;->NEWSFEED:LX/21D;

    const-string v6, "workTopGroupsComposer"

    invoke-static {v5, v6}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    new-instance v6, LX/89I;

    .line 1708931
    iget-object v7, v3, LX/BYG;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1708932
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    sget-object v9, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v6, v7, v8, v9}, LX/89I;-><init>(JLX/2rw;)V

    .line 1708933
    iget-object v7, v3, LX/BYG;->b:Ljava/lang/String;

    move-object v7, v7

    .line 1708934
    iput-object v7, v6, LX/89I;->c:Ljava/lang/String;

    .line 1708935
    move-object v6, v6

    .line 1708936
    invoke-virtual {v6}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    .line 1708937
    iget-object v6, v4, LX/AkN;->c:LX/1Kf;

    const/4 v7, 0x0

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v8

    const/16 v9, 0x6dc

    const-class v5, Landroid/app/Activity;

    invoke-static {v2, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-interface {v6, v7, v8, v9, v5}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method
