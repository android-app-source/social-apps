.class public final LX/Bb6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V
    .locals 0

    .prologue
    .line 1800219
    iput-object p1, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1800220
    if-eqz p1, :cond_0

    .line 1800221
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800222
    if-nez v0, :cond_2

    .line 1800223
    :cond_0
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u:LX/03V;

    sget-object v1, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->x:Ljava/lang/String;

    const-string v2, "Unable to retrieve valid StoryConversionInfo"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800224
    :cond_1
    :goto_0
    return-void

    .line 1800225
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800226
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1800227
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800228
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->b(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1800229
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->POST_ALREADY_CONVERTED:LX/BbB;

    invoke-virtual {v0, v1}, LX/BbC;->a(LX/BbB;)V

    .line 1800230
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    invoke-virtual {v0}, LX/BbC;->a()V

    .line 1800231
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto :goto_0

    .line 1800232
    :cond_3
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto :goto_0

    .line 1800233
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800234
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1800235
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800236
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    .line 1800237
    :goto_1
    if-eqz v0, :cond_7

    .line 1800238
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1800239
    goto :goto_1

    .line 1800240
    :cond_7
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800241
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;

    .line 1800242
    iget-object v2, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;)Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    move-result-object v0

    .line 1800243
    iput-object v0, v2, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800244
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-boolean v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    if-eqz v0, :cond_1

    .line 1800245
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    .line 1800246
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    .line 1800247
    iget-object v0, p0, LX/Bb6;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1800248
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800249
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bb6;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
