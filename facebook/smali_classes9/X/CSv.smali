.class public LX/CSv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1895052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895053
    return-void
.end method

.method public static a()LX/CSr;
    .locals 1

    .prologue
    .line 1895054
    new-instance v0, LX/CSs;

    invoke-direct {v0}, LX/CSs;-><init>()V

    return-object v0
.end method

.method public static c(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1895055
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895056
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895057
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895058
    sget-object v0, LX/CSu;->a:[I

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1895059
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1895060
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1895061
    goto :goto_0

    .line 1895062
    :pswitch_0
    const/4 v1, 0x0

    .line 1895063
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895064
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895065
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895066
    sget-object v0, LX/CSu;->b:[I

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 1895067
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1895068
    const/4 v0, 0x0

    :goto_3
    move-object v0, v0

    .line 1895069
    goto :goto_1

    .line 1895070
    :pswitch_1
    new-instance v0, LX/CTU;

    invoke-direct {v0, p0, p1}, LX/CTU;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895071
    :pswitch_2
    new-instance v0, LX/CTW;

    invoke-direct {v0, p0, p1}, LX/CTW;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895072
    :pswitch_3
    new-instance v0, LX/CTV;

    invoke-direct {v0, p0, p1}, LX/CTV;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895073
    :pswitch_4
    new-instance v0, LX/CTX;

    invoke-direct {v0, p0, p1}, LX/CTX;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895074
    :pswitch_5
    new-instance v0, LX/CTY;

    invoke-direct {v0, p0, p1}, LX/CTY;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895075
    :pswitch_6
    new-instance v0, LX/CTZ;

    invoke-direct {v0, p0, p1}, LX/CTZ;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895076
    :pswitch_7
    new-instance v0, LX/CTa;

    invoke-direct {v0, p0, p1}, LX/CTa;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895077
    :pswitch_8
    new-instance v0, LX/CTb;

    invoke-direct {v0, p0, p1}, LX/CTb;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895078
    :pswitch_9
    new-instance v0, LX/CTc;

    invoke-direct {v0, p0, p1}, LX/CTc;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_1

    .line 1895079
    :pswitch_a
    new-instance v0, LX/CTd;

    invoke-direct {v0, p0, p1}, LX/CTd;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1895080
    :pswitch_b
    new-instance v0, LX/CTe;

    new-instance v1, LX/CSt;

    invoke-direct {v1}, LX/CSt;-><init>()V

    invoke-direct {v0, p0, v1, p1}, LX/CTe;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSr;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1895081
    :pswitch_c
    new-instance v0, LX/CTf;

    invoke-direct {v0, p0, p1}, LX/CTf;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1895082
    :pswitch_d
    new-instance v0, LX/CTg;

    invoke-direct {v0, p0, p1}, LX/CTg;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1895083
    :pswitch_e
    new-instance v0, LX/CTJ;

    invoke-direct {v0, p0, p1}, LX/CTJ;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1895084
    :pswitch_f
    new-instance v0, LX/CTh;

    invoke-direct {v0, p0, p1}, LX/CTh;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1
    move v0, v1

    .line 1895085
    goto/16 :goto_2

    .line 1895086
    :pswitch_10
    new-instance v0, LX/CTN;

    sget-object v1, LX/CSS;->DATE_PICKER_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTN;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto :goto_3

    .line 1895087
    :pswitch_11
    new-instance v0, LX/CTr;

    sget-object v1, LX/CSS;->TIME_SLOT_PICKER_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTr;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto :goto_3

    .line 1895088
    :pswitch_12
    new-instance v0, LX/CTm;

    invoke-direct {v0, p0, p1}, LX/CTm;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V

    goto :goto_3

    .line 1895089
    :pswitch_13
    new-instance v0, LX/CTK;

    sget-object v1, LX/CSS;->CHECKBOX_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895090
    :pswitch_14
    new-instance v0, LX/CTK;

    sget-object v1, LX/CSS;->PAYMENT_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895091
    :pswitch_15
    new-instance v0, LX/CTL;

    sget-object v1, LX/CSS;->ADDRESS_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTL;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895092
    :pswitch_16
    new-instance v0, LX/CTM;

    sget-object v1, LX/CSS;->CONTACT_INFO_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTM;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895093
    :pswitch_17
    new-instance v0, LX/CTO;

    sget-object v1, LX/CSS;->DATE_TIME_PICKER_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTO;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895094
    :pswitch_18
    new-instance v0, LX/CTQ;

    sget-object v1, LX/CSS;->DATE_TIME_SELECTION_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTQ;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895095
    :pswitch_19
    new-instance v0, LX/CTR;

    sget-object v1, LX/CSS;->MARKET_OPT_IN:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTR;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895096
    :pswitch_1a
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1895097
    new-instance v0, LX/CTS;

    sget-object v1, LX/CSS;->SELECTION_PRODUCT_MULTI_SELECT:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTS;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895098
    :cond_2
    new-instance v0, LX/CTS;

    sget-object v1, LX/CSS;->SELECTION_PRODUCT_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTS;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895099
    :pswitch_1b
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1895100
    new-instance v0, LX/CTT;

    sget-object v1, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_MULTI:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTT;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895101
    :cond_3
    new-instance v0, LX/CTT;

    sget-object v1, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_SINGLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTT;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895102
    :pswitch_1c
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1895103
    new-instance v0, LX/CTn;

    sget-object v1, LX/CSS;->SELECTION_STRING_MULTI_SELECT:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTn;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895104
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    if-ne v0, v1, :cond_5

    .line 1895105
    new-instance v0, LX/CTn;

    sget-object v1, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_DROPDOWN:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTn;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895106
    :cond_5
    new-instance v0, LX/CTn;

    sget-object v1, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_EXPANDED:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTn;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895107
    :pswitch_1d
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->CITY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    if-ne v0, v1, :cond_6

    .line 1895108
    new-instance v0, LX/CTo;

    sget-object v1, LX/CSS;->TEXT_CITY_TYPEAHEAD:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTo;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1895109
    :cond_6
    new-instance v0, LX/CTo;

    sget-object v1, LX/CSS;->TEXT_SIMPLE:LX/CSS;

    invoke-direct {v0, p0, v1, p1}, LX/CTo;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method
