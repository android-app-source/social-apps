.class public final LX/ApQ;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/ApS;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/ApR;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1716017
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, LX/ApQ;->b:[Ljava/lang/String;

    .line 1716018
    sput v3, LX/ApQ;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1716015
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1716016
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/ApQ;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ApQ;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/ApQ;LX/1De;IILX/ApR;)V
    .locals 1

    .prologue
    .line 1716011
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1716012
    iput-object p4, p0, LX/ApQ;->a:LX/ApR;

    .line 1716013
    iget-object v0, p0, LX/ApQ;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1716014
    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/ApQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;)",
            "LX/ApQ;"
        }
    .end annotation

    .prologue
    .line 1716009
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->e:LX/1X1;

    .line 1716010
    return-object p0
.end method

.method public final a(LX/1dQ;)LX/ApQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;)",
            "LX/ApQ;"
        }
    .end annotation

    .prologue
    .line 1716007
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->g:LX/1dQ;

    .line 1716008
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/ApQ;
    .locals 2

    .prologue
    .line 1716004
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->b:Ljava/lang/CharSequence;

    .line 1716005
    iget-object v0, p0, LX/ApQ;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1716006
    return-object p0
.end method

.method public final a(Z)LX/ApQ;
    .locals 1

    .prologue
    .line 1716002
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-boolean p1, v0, LX/ApR;->h:Z

    .line 1716003
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1715979
    invoke-super {p0}, LX/1X5;->a()V

    .line 1715980
    const/4 v0, 0x0

    iput-object v0, p0, LX/ApQ;->a:LX/ApR;

    .line 1715981
    sget-object v0, LX/ApS;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1715982
    return-void
.end method

.method public final b(LX/1X1;)LX/ApQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/Apj;",
            ">;)",
            "LX/ApQ;"
        }
    .end annotation

    .prologue
    .line 1715983
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->f:LX/1X1;

    .line 1715984
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/ApQ;
    .locals 1

    .prologue
    .line 1715985
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->c:Ljava/lang/CharSequence;

    .line 1715986
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/ApQ;
    .locals 1

    .prologue
    .line 1715987
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput-object p1, v0, LX/ApR;->d:Ljava/lang/CharSequence;

    .line 1715988
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/ApS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1715989
    iget-object v1, p0, LX/ApQ;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ApQ;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/ApQ;->c:I

    if-ge v1, v2, :cond_2

    .line 1715990
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1715991
    :goto_0
    sget v2, LX/ApQ;->c:I

    if-ge v0, v2, :cond_1

    .line 1715992
    iget-object v2, p0, LX/ApQ;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1715993
    sget-object v2, LX/ApQ;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1715994
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1715995
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715996
    :cond_2
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    .line 1715997
    invoke-virtual {p0}, LX/ApQ;->a()V

    .line 1715998
    return-object v0
.end method

.method public final h(I)LX/ApQ;
    .locals 2
    .param p1    # I
        .annotation build Lcom/facebook/fig/components/hscroll/annotations/FigHscrollType;
        .end annotation
    .end param

    .prologue
    .line 1715999
    iget-object v0, p0, LX/ApQ;->a:LX/ApR;

    iput p1, v0, LX/ApR;->a:I

    .line 1716000
    iget-object v0, p0, LX/ApQ;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1716001
    return-object p0
.end method
