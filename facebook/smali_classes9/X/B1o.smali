.class public final LX/B1o;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1736574
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1736575
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1736576
    :goto_0
    return v1

    .line 1736577
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1736578
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1736579
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1736580
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1736581
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1736582
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1736583
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1736584
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1736585
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1736586
    invoke-static {p0, p1}, LX/B1n;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1736587
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1736588
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1736589
    goto :goto_1

    .line 1736590
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1736591
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1736592
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_b

    .line 1736593
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1736594
    :goto_3
    move v0, v3

    .line 1736595
    goto :goto_1

    .line 1736596
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1736597
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1736598
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1736599
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1736600
    :cond_6
    const-string v8, "has_next_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1736601
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    .line 1736602
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 1736603
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1736604
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1736605
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 1736606
    const-string v8, "end_cursor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1736607
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1736608
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1736609
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1736610
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1736611
    if-eqz v0, :cond_a

    .line 1736612
    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1736613
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_b
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1736614
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1736615
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1736616
    if-eqz v0, :cond_1

    .line 1736617
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1736618
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1736619
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1736620
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/B1n;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1736621
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1736622
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1736623
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1736624
    if-eqz v0, :cond_4

    .line 1736625
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1736626
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1736627
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1736628
    if-eqz v1, :cond_2

    .line 1736629
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1736630
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1736631
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1736632
    if-eqz v1, :cond_3

    .line 1736633
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1736634
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1736635
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1736636
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1736637
    return-void
.end method
