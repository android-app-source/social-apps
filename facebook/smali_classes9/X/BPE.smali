.class public LX/BPE;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/BP4;",
        "LX/BPD;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BPE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780819
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1780820
    return-void
.end method

.method public static a(LX/0QB;)LX/BPE;
    .locals 3

    .prologue
    .line 1780807
    sget-object v0, LX/BPE;->a:LX/BPE;

    if-nez v0, :cond_1

    .line 1780808
    const-class v1, LX/BPE;

    monitor-enter v1

    .line 1780809
    :try_start_0
    sget-object v0, LX/BPE;->a:LX/BPE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1780810
    if-eqz v2, :cond_0

    .line 1780811
    :try_start_1
    new-instance v0, LX/BPE;

    invoke-direct {v0}, LX/BPE;-><init>()V

    .line 1780812
    move-object v0, v0

    .line 1780813
    sput-object v0, LX/BPE;->a:LX/BPE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780814
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1780815
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1780816
    :cond_1
    sget-object v0, LX/BPE;->a:LX/BPE;

    return-object v0

    .line 1780817
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1780818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
