.class public final enum LX/AZ8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AZ8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AZ8;

.field public static final enum BEGIN_TRANSITION:LX/AZ8;

.field public static final enum COUNTDOWN:LX/AZ8;

.field public static final enum END_TRANSITION:LX/AZ8;

.field public static final enum NONE:LX/AZ8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1686534
    new-instance v0, LX/AZ8;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/AZ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZ8;->NONE:LX/AZ8;

    .line 1686535
    new-instance v0, LX/AZ8;

    const-string v1, "BEGIN_TRANSITION"

    invoke-direct {v0, v1, v3}, LX/AZ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZ8;->BEGIN_TRANSITION:LX/AZ8;

    .line 1686536
    new-instance v0, LX/AZ8;

    const-string v1, "COUNTDOWN"

    invoke-direct {v0, v1, v4}, LX/AZ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZ8;->COUNTDOWN:LX/AZ8;

    .line 1686537
    new-instance v0, LX/AZ8;

    const-string v1, "END_TRANSITION"

    invoke-direct {v0, v1, v5}, LX/AZ8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZ8;->END_TRANSITION:LX/AZ8;

    .line 1686538
    const/4 v0, 0x4

    new-array v0, v0, [LX/AZ8;

    sget-object v1, LX/AZ8;->NONE:LX/AZ8;

    aput-object v1, v0, v2

    sget-object v1, LX/AZ8;->BEGIN_TRANSITION:LX/AZ8;

    aput-object v1, v0, v3

    sget-object v1, LX/AZ8;->COUNTDOWN:LX/AZ8;

    aput-object v1, v0, v4

    sget-object v1, LX/AZ8;->END_TRANSITION:LX/AZ8;

    aput-object v1, v0, v5

    sput-object v0, LX/AZ8;->$VALUES:[LX/AZ8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1686539
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AZ8;
    .locals 1

    .prologue
    .line 1686540
    const-class v0, LX/AZ8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AZ8;

    return-object v0
.end method

.method public static values()[LX/AZ8;
    .locals 1

    .prologue
    .line 1686541
    sget-object v0, LX/AZ8;->$VALUES:[LX/AZ8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AZ8;

    return-object v0
.end method
