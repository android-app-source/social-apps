.class public final LX/CVQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1903723
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1903724
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903725
    :goto_0
    return v1

    .line 1903726
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903727
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1903728
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1903729
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1903730
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1903731
    const-string v5, "address"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1903732
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1903733
    :cond_2
    const-string v5, "thumbnail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1903734
    invoke-static {p0, p1}, LX/CVV;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1903735
    :cond_3
    const-string v5, "title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1903736
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1903737
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1903738
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1903739
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1903740
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1903741
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1903742
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903743
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903744
    if-eqz v0, :cond_0

    .line 1903745
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903746
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903747
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903748
    if-eqz v0, :cond_1

    .line 1903749
    const-string v1, "thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903750
    invoke-static {p0, v0, p2}, LX/CVV;->a(LX/15i;ILX/0nX;)V

    .line 1903751
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903752
    if-eqz v0, :cond_2

    .line 1903753
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903754
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903755
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903756
    return-void
.end method
