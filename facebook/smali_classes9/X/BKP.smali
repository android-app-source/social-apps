.class public final enum LX/BKP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BKP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BKP;

.field public static final enum FETCH_APP_NAME:LX/BKP;

.field public static final enum FETCH_PLACE_INFO:LX/BKP;

.field public static final enum FETCH_ROBOTEXT:LX/BKP;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1772843
    new-instance v0, LX/BKP;

    const-string v1, "FETCH_ROBOTEXT"

    invoke-direct {v0, v1, v2}, LX/BKP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKP;->FETCH_ROBOTEXT:LX/BKP;

    .line 1772844
    new-instance v0, LX/BKP;

    const-string v1, "FETCH_APP_NAME"

    invoke-direct {v0, v1, v3}, LX/BKP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKP;->FETCH_APP_NAME:LX/BKP;

    .line 1772845
    new-instance v0, LX/BKP;

    const-string v1, "FETCH_PLACE_INFO"

    invoke-direct {v0, v1, v4}, LX/BKP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKP;->FETCH_PLACE_INFO:LX/BKP;

    .line 1772846
    const/4 v0, 0x3

    new-array v0, v0, [LX/BKP;

    sget-object v1, LX/BKP;->FETCH_ROBOTEXT:LX/BKP;

    aput-object v1, v0, v2

    sget-object v1, LX/BKP;->FETCH_APP_NAME:LX/BKP;

    aput-object v1, v0, v3

    sget-object v1, LX/BKP;->FETCH_PLACE_INFO:LX/BKP;

    aput-object v1, v0, v4

    sput-object v0, LX/BKP;->$VALUES:[LX/BKP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1772847
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BKP;
    .locals 1

    .prologue
    .line 1772841
    const-class v0, LX/BKP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BKP;

    return-object v0
.end method

.method public static values()[LX/BKP;
    .locals 1

    .prologue
    .line 1772842
    sget-object v0, LX/BKP;->$VALUES:[LX/BKP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BKP;

    return-object v0
.end method
