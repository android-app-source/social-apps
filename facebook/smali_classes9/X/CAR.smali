.class public LX/CAR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field private final c:LX/0SG;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1855402
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "feedplugins/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "platform/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "calltoaction/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1855403
    sput-object v0, LX/CAR;->a:LX/0Tn;

    const-string v1, "impressions/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CAR;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855405
    iput-object p1, p0, LX/CAR;->c:LX/0SG;

    .line 1855406
    iput-object p2, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1855407
    iput-object p3, p0, LX/CAR;->e:LX/0ad;

    .line 1855408
    return-void
.end method

.method private static c(LX/CAR;)J
    .locals 4

    .prologue
    .line 1855409
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/CAR;->e:LX/0ad;

    sget v2, LX/CAO;->b:I

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 1855410
    invoke-static {p0}, LX/CAR;->c(LX/CAR;)J

    move-result-wide v4

    .line 1855411
    iget-object v0, p0, LX/CAR;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1855412
    iget-object v0, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/CAR;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v3

    .line 1855413
    iget-object v0, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v8

    .line 1855414
    const/4 v0, 0x0

    .line 1855415
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1855416
    iget-object v2, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v10, 0x0

    invoke-interface {v2, v0, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    .line 1855417
    sub-long v12, v6, v4

    cmp-long v0, v12, v10

    if-gtz v0, :cond_1

    .line 1855418
    sget-object v0, LX/CAR;->b:LX/0Tn;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v8, v0, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move v0, v2

    :goto_1
    move v1, v0

    .line 1855419
    goto :goto_0

    .line 1855420
    :cond_0
    sget-object v0, LX/CAR;->b:LX/0Tn;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v8, v0, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1855421
    iget-object v0, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 1855422
    invoke-interface {v8}, LX/0hN;->commit()V

    .line 1855423
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final b()Z
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1855424
    invoke-static {p0}, LX/CAR;->c(LX/CAR;)J

    move-result-wide v4

    .line 1855425
    iget-object v0, p0, LX/CAR;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1855426
    iget-object v0, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/CAR;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1855427
    sub-long v8, v6, v4

    iget-object v10, p0, LX/CAR;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v12, 0x0

    invoke-interface {v10, v0, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-gtz v0, :cond_2

    .line 1855428
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1855429
    goto :goto_0

    .line 1855430
    :cond_0
    iget-object v0, p0, LX/CAR;->e:LX/0ad;

    sget v3, LX/CAO;->c:I

    invoke-interface {v0, v3, v2}, LX/0ad;->a(II)I

    move-result v0

    if-ge v1, v0, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    :cond_2
    move v0, v1

    goto :goto_1
.end method
