.class public final enum LX/ArR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArR;

.field public static final enum DRAG_DURATION:LX/ArR;


# instance fields
.field private final tag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1719114
    new-instance v0, LX/ArR;

    const-string v1, "DRAG_DURATION"

    const-string v2, "drag_duration"

    invoke-direct {v0, v1, v3, v2}, LX/ArR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArR;->DRAG_DURATION:LX/ArR;

    .line 1719115
    const/4 v0, 0x1

    new-array v0, v0, [LX/ArR;

    sget-object v1, LX/ArR;->DRAG_DURATION:LX/ArR;

    aput-object v1, v0, v3

    sput-object v0, LX/ArR;->$VALUES:[LX/ArR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1719111
    iput-object p3, p0, LX/ArR;->tag:Ljava/lang/String;

    .line 1719112
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArR;
    .locals 1

    .prologue
    .line 1719116
    const-class v0, LX/ArR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArR;

    return-object v0
.end method

.method public static values()[LX/ArR;
    .locals 1

    .prologue
    .line 1719113
    sget-object v0, LX/ArR;->$VALUES:[LX/ArR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArR;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719109
    iget-object v0, p0, LX/ArR;->tag:Ljava/lang/String;

    return-object v0
.end method
