.class public LX/CEw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CEw;


# instance fields
.field public a:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862297
    return-void
.end method

.method public static a(LX/0QB;)LX/CEw;
    .locals 3

    .prologue
    .line 1862298
    sget-object v0, LX/CEw;->b:LX/CEw;

    if-nez v0, :cond_1

    .line 1862299
    const-class v1, LX/CEw;

    monitor-enter v1

    .line 1862300
    :try_start_0
    sget-object v0, LX/CEw;->b:LX/CEw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1862301
    if-eqz v2, :cond_0

    .line 1862302
    :try_start_1
    new-instance v0, LX/CEw;

    invoke-direct {v0}, LX/CEw;-><init>()V

    .line 1862303
    move-object v0, v0

    .line 1862304
    sput-object v0, LX/CEw;->b:LX/CEw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1862305
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1862306
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1862307
    :cond_1
    sget-object v0, LX/CEw;->b:LX/CEw;

    return-object v0

    .line 1862308
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1862309
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
