.class public final enum LX/BSV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSV;

.field public static final enum COUNTDOWN:LX/BSV;

.field public static final enum LIVE:LX/BSV;

.field public static final enum POST_AD:LX/BSV;

.field public static final enum PRE_AD:LX/BSV;

.field public static final enum VIDEO_AD:LX/BSV;

.field public static final enum WAIT_FOR:LX/BSV;


# instance fields
.field private final mCommercialBreakStatus:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1785516
    new-instance v0, LX/BSV;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v4, v2}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->LIVE:LX/BSV;

    .line 1785517
    new-instance v0, LX/BSV;

    const-string v1, "PRE_AD"

    const-string v2, "pre_ad"

    invoke-direct {v0, v1, v5, v2}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->PRE_AD:LX/BSV;

    .line 1785518
    new-instance v0, LX/BSV;

    const-string v1, "WAIT_FOR"

    const-string v2, "wait_for"

    invoke-direct {v0, v1, v6, v2}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->WAIT_FOR:LX/BSV;

    .line 1785519
    new-instance v0, LX/BSV;

    const-string v1, "VIDEO_AD"

    const-string v2, "video_ad"

    invoke-direct {v0, v1, v7, v2}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->VIDEO_AD:LX/BSV;

    .line 1785520
    new-instance v0, LX/BSV;

    const-string v1, "COUNTDOWN"

    const-string v2, "count_down"

    invoke-direct {v0, v1, v8, v2}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->COUNTDOWN:LX/BSV;

    .line 1785521
    new-instance v0, LX/BSV;

    const-string v1, "POST_AD"

    const/4 v2, 0x5

    const-string v3, "post_ad"

    invoke-direct {v0, v1, v2, v3}, LX/BSV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSV;->POST_AD:LX/BSV;

    .line 1785522
    const/4 v0, 0x6

    new-array v0, v0, [LX/BSV;

    sget-object v1, LX/BSV;->LIVE:LX/BSV;

    aput-object v1, v0, v4

    sget-object v1, LX/BSV;->PRE_AD:LX/BSV;

    aput-object v1, v0, v5

    sget-object v1, LX/BSV;->WAIT_FOR:LX/BSV;

    aput-object v1, v0, v6

    sget-object v1, LX/BSV;->VIDEO_AD:LX/BSV;

    aput-object v1, v0, v7

    sget-object v1, LX/BSV;->COUNTDOWN:LX/BSV;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BSV;->POST_AD:LX/BSV;

    aput-object v2, v0, v1

    sput-object v0, LX/BSV;->$VALUES:[LX/BSV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1785526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1785527
    iput-object p3, p0, LX/BSV;->mCommercialBreakStatus:Ljava/lang/String;

    .line 1785528
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSV;
    .locals 1

    .prologue
    .line 1785525
    const-class v0, LX/BSV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSV;

    return-object v0
.end method

.method public static values()[LX/BSV;
    .locals 1

    .prologue
    .line 1785524
    sget-object v0, LX/BSV;->$VALUES:[LX/BSV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSV;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785523
    iget-object v0, p0, LX/BSV;->mCommercialBreakStatus:Ljava/lang/String;

    return-object v0
.end method
