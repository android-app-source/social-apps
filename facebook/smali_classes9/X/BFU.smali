.class public LX/BFU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/189;

.field public final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/9Eb;

.field private d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Ea;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/9Eb;LX/189;)V
    .locals 1
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9Eb;",
            "LX/189;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765899
    iput-object p1, p0, LX/BFU;->b:LX/0QK;

    .line 1765900
    iput-object p3, p0, LX/BFU;->a:LX/189;

    .line 1765901
    iput-object p2, p0, LX/BFU;->c:LX/9Eb;

    .line 1765902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BFU;->e:Ljava/util/List;

    .line 1765903
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1765904
    iget-object v0, p0, LX/BFU;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1765905
    :goto_0
    return-void

    .line 1765906
    :cond_0
    iget-object v0, p0, LX/BFU;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ea;

    .line 1765907
    invoke-virtual {v0}, LX/9Ea;->a()V

    goto :goto_1

    .line 1765908
    :cond_1
    iget-object v0, p0, LX/BFU;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1765909
    if-nez p1, :cond_1

    .line 1765910
    :cond_0
    return-void

    .line 1765911
    :cond_1
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/BFU;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1765912
    invoke-virtual {p0}, LX/BFU;->a()V

    .line 1765913
    iget-object v0, p0, LX/BFU;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1765914
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1765915
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1765916
    iget-object v0, p0, LX/BFU;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1765917
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1765918
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 1765919
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_0

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765920
    iget-object v0, p0, LX/BFU;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v7}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v11

    .line 1765921
    iget-object v0, p0, LX/BFU;->c:LX/9Eb;

    new-instance v1, LX/BFT;

    invoke-direct {v1, p0, v11}, LX/BFT;-><init>(LX/BFU;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v0

    .line 1765922
    iget-object v1, p0, LX/BFU;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1765923
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1765924
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1765925
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/BFU;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method
