.class public LX/Aq3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Apv;


# direct methods
.method public constructor <init>(LX/Apv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1717203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1717204
    iput-object p1, p0, LX/Aq3;->a:LX/Apv;

    .line 1717205
    return-void
.end method

.method public static a(LX/0QB;)LX/Aq3;
    .locals 4

    .prologue
    .line 1717206
    const-class v1, LX/Aq3;

    monitor-enter v1

    .line 1717207
    :try_start_0
    sget-object v0, LX/Aq3;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1717208
    sput-object v2, LX/Aq3;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1717209
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1717210
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1717211
    new-instance p0, LX/Aq3;

    invoke-static {v0}, LX/Apv;->a(LX/0QB;)LX/Apv;

    move-result-object v3

    check-cast v3, LX/Apv;

    invoke-direct {p0, v3}, LX/Aq3;-><init>(LX/Apv;)V

    .line 1717212
    move-object v0, p0

    .line 1717213
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1717214
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Aq3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1717215
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1717216
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
