.class public LX/BHJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/BI6;

.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/74q;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/BGe;

.field private final d:I

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/BH4;

.field public final g:LX/11i;

.field private final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/BHi;

.field private j:Z

.field private k:Z

.field public l:Z

.field public m:I

.field private n:LX/27l;

.field public o:Z

.field public final p:Z

.field public q:Lcom/facebook/ipc/media/MediaItem;

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/BIO;

.field private t:LX/BIO;

.field public u:I

.field public v:I

.field public w:Z

.field private x:Z

.field private y:Z

.field public z:LX/BHZ;


# direct methods
.method public constructor <init>(Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;LX/BH4;LX/0Px;LX/0Ot;LX/BGe;LX/BHa;LX/0Or;LX/0Or;LX/11i;LX/0Ot;)V
    .locals 3
    .param p1    # Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BH4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/gating/SimplePickerShouldLogDetailedInfo;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/annotation/MaxNumberPhotosPerUpload;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;",
            "Lcom/facebook/photos/simplepicker/PrefilledTaggingCallback;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/BGe;",
            "LX/BHa;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/11i;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1768852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1768853
    iput-object v0, p0, LX/BHJ;->s:LX/BIO;

    .line 1768854
    iput-object v0, p0, LX/BHJ;->t:LX/BIO;

    .line 1768855
    iput v1, p0, LX/BHJ;->u:I

    .line 1768856
    iput v1, p0, LX/BHJ;->v:I

    .line 1768857
    iget-boolean v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    move v0, v0

    .line 1768858
    iput-boolean v0, p0, LX/BHJ;->j:Z

    .line 1768859
    iget-boolean v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    move v0, v0

    .line 1768860
    iput-boolean v0, p0, LX/BHJ;->p:Z

    .line 1768861
    iput-object p2, p0, LX/BHJ;->f:LX/BH4;

    .line 1768862
    iput-object p4, p0, LX/BHJ;->e:LX/0Ot;

    .line 1768863
    iput-object p5, p0, LX/BHJ;->c:LX/BGe;

    .line 1768864
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/BHJ;->o:Z

    .line 1768865
    iget-boolean v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    move v0, v0

    .line 1768866
    iput-boolean v0, p0, LX/BHJ;->w:Z

    .line 1768867
    iput-object p3, p0, LX/BHJ;->h:LX/0Px;

    .line 1768868
    iget-object v0, p0, LX/BHJ;->h:LX/0Px;

    .line 1768869
    new-instance p4, LX/BHZ;

    invoke-static {p6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p6}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object p2

    check-cast p2, LX/4mV;

    .line 1768870
    new-instance p5, LX/BHt;

    invoke-static {p6}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p3

    check-cast p3, LX/0Zb;

    invoke-direct {p5, p3}, LX/BHt;-><init>(LX/0Zb;)V

    .line 1768871
    move-object p3, p5

    .line 1768872
    check-cast p3, LX/BHt;

    invoke-direct {p4, v0, v2, p2, p3}, LX/BHZ;-><init>(LX/0Px;LX/0ad;LX/4mV;LX/BHt;)V

    .line 1768873
    move-object v0, p4

    .line 1768874
    iput-object v0, p0, LX/BHJ;->z:LX/BHZ;

    .line 1768875
    iget v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    move v0, v0

    .line 1768876
    if-gtz v0, :cond_1

    .line 1768877
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/BHJ;->d:I

    .line 1768878
    :goto_1
    iput-object p9, p0, LX/BHJ;->g:LX/11i;

    .line 1768879
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    .line 1768880
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/BHJ;->b:Ljava/util/Set;

    .line 1768881
    invoke-static {p0, v1}, LX/BHJ;->a(LX/BHJ;Z)V

    .line 1768882
    iput-object p10, p0, LX/BHJ;->r:LX/0Ot;

    .line 1768883
    iget-boolean v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    move v0, v0

    .line 1768884
    iput-boolean v0, p0, LX/BHJ;->x:Z

    .line 1768885
    iget-boolean v0, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    move v0, v0

    .line 1768886
    iput-boolean v0, p0, LX/BHJ;->y:Z

    .line 1768887
    return-void

    :cond_0
    move v0, v1

    .line 1768888
    goto :goto_0

    .line 1768889
    :cond_1
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1768890
    iget v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    move v2, v2

    .line 1768891
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/BHJ;->d:I

    goto :goto_1
.end method

.method public static a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;LX/74p;)V
    .locals 2

    .prologue
    .line 1768848
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768849
    iget-object v0, p0, LX/BHJ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74q;

    .line 1768850
    invoke-interface {v0, p1, p2}, LX/74q;->a(Lcom/facebook/ipc/media/MediaItem;LX/74p;)V

    goto :goto_0

    .line 1768851
    :cond_0
    return-void
.end method

.method public static a(LX/BHJ;Z)V
    .locals 1

    .prologue
    .line 1768908
    if-nez p1, :cond_0

    .line 1768909
    const/4 v0, 0x0

    iput-object v0, p0, LX/BHJ;->q:Lcom/facebook/ipc/media/MediaItem;

    .line 1768910
    :cond_0
    iput-boolean p1, p0, LX/BHJ;->k:Z

    .line 1768911
    invoke-static {p0}, LX/BHJ;->d(LX/BHJ;)V

    .line 1768912
    return-void
.end method

.method public static a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1768892
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1768893
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1768894
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1768895
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/32 v6, 0x40000000

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 1768896
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v3, LX/27k;

    if-eqz p2, :cond_0

    const v1, 0x7f081366

    :goto_0
    invoke-direct {v3, v1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    move v0, v2

    .line 1768897
    :goto_1
    return v0

    .line 1768898
    :cond_0
    const v1, 0x7f081364

    goto :goto_0

    .line 1768899
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v4, 0x400

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    .line 1768900
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v3, LX/27k;

    if-eqz p2, :cond_2

    const v1, 0x7f081367

    :goto_2
    invoke-direct {v3, v1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    move v0, v2

    .line 1768901
    goto :goto_1

    .line 1768902
    :cond_2
    const v1, 0x7f081365

    goto :goto_2

    .line 1768903
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private b(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 1

    .prologue
    .line 1768904
    instance-of v0, p1, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v0, :cond_0

    .line 1768905
    iput-object p1, p0, LX/BHJ;->q:Lcom/facebook/ipc/media/MediaItem;

    .line 1768906
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/BHJ;->a(LX/BHJ;Z)V

    .line 1768907
    :cond_0
    return-void
.end method

.method public static b(LX/BHJ;Lcom/facebook/photos/base/media/VideoItem;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1768837
    iget-wide v4, p1, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v0, v4

    .line 1768838
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1768839
    iget-wide v4, p1, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v0, v4

    .line 1768840
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 1768841
    :cond_0
    const/4 v0, 0x1

    .line 1768842
    :goto_0
    return v0

    .line 1768843
    :cond_1
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081368

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1768844
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 2

    .prologue
    .line 1768845
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1768846
    iget-object p0, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, p0

    .line 1768847
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/BHJ;)V
    .locals 1

    .prologue
    .line 1768834
    iget-boolean v0, p0, LX/BHJ;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/BHJ;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BHJ;->p:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, LX/BHJ;->w:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/BHJ;->v:I

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/BHJ;->l:Z

    .line 1768835
    return-void

    .line 1768836
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1768833
    iget v1, p0, LX/BHJ;->u:I

    if-ne v1, v0, :cond_0

    iget v1, p0, LX/BHJ;->m:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1768814
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768815
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    move v0, v2

    .line 1768816
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1768817
    iget-object v1, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768818
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1768819
    :cond_0
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LX/BHJ;->m:I

    .line 1768820
    iput v2, p0, LX/BHJ;->v:I

    .line 1768821
    invoke-static {p0, v2}, LX/BHJ;->a(LX/BHJ;Z)V

    .line 1768822
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1768823
    invoke-static {v0}, LX/BHJ;->c(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1768824
    iget v0, p0, LX/BHJ;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BHJ;->v:I

    .line 1768825
    invoke-static {p0}, LX/BHJ;->d(LX/BHJ;)V

    .line 1768826
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1768827
    :cond_1
    invoke-direct {p0, v0}, LX/BHJ;->b(Lcom/facebook/ipc/media/MediaItem;)V

    goto :goto_2

    .line 1768828
    :cond_2
    iget-object v0, p0, LX/BHJ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/74q;

    .line 1768829
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1768830
    sget-object v6, LX/74p;->PRESELECTED:LX/74p;

    invoke-interface {v1, v0, v6}, LX/74q;->a(Lcom/facebook/ipc/media/MediaItem;LX/74p;)V

    .line 1768831
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1768832
    :cond_4
    return-void
.end method

.method public final a(LX/74q;)V
    .locals 1

    .prologue
    .line 1768811
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768812
    iget-object v0, p0, LX/BHJ;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1768813
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1768733
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1768734
    :cond_0
    :goto_0
    return v2

    .line 1768735
    :cond_1
    iget-boolean v0, p0, LX/BHJ;->w:Z

    if-eqz v0, :cond_13

    .line 1768736
    invoke-static {p1}, LX/BHJ;->c(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    .line 1768737
    if-eqz v0, :cond_3

    .line 1768738
    iget v1, p0, LX/BHJ;->m:I

    if-lez v1, :cond_4

    .line 1768739
    iget v0, p0, LX/BHJ;->v:I

    if-lez v0, :cond_2

    .line 1768740
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081355

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 1768741
    :cond_2
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081356

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 1768742
    :cond_3
    iget v1, p0, LX/BHJ;->v:I

    if-lez v1, :cond_4

    .line 1768743
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081357

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 1768744
    :cond_4
    invoke-static {p0, p1, v0}, LX/BHJ;->a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 1768745
    :goto_1
    instance-of v0, p1, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v0, :cond_6

    if-nez v1, :cond_6

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    const/4 v4, 0x0

    .line 1768746
    invoke-static {p0, v0, v4}, LX/BHJ;->a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p0, v0}, LX/BHJ;->b(LX/BHJ;Lcom/facebook/photos/base/media/VideoItem;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v4, 0x1

    :cond_5
    move v0, v4

    .line 1768747
    if-eqz v0, :cond_0

    .line 1768748
    :cond_6
    iget-object v0, p0, LX/BHJ;->z:LX/BHZ;

    invoke-virtual {v0, p1}, LX/BHZ;->a(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1768749
    iget-object v0, p0, LX/BHJ;->z:LX/BHZ;

    iget v4, p0, LX/BHJ;->m:I

    iget-object v5, p0, LX/BHJ;->i:LX/BHi;

    iget-object v6, p0, LX/BHJ;->A:LX/BI6;

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1768750
    if-nez v4, :cond_14

    move v7, v9

    .line 1768751
    :goto_2
    iget-object v8, v0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_15

    .line 1768752
    iget-boolean v7, v0, LX/BHZ;->d:Z

    if-eqz v7, :cond_7

    .line 1768753
    invoke-virtual {v0, v5}, LX/BHZ;->a(LX/BHi;)V

    .line 1768754
    :cond_7
    :goto_3
    iget-boolean v0, p0, LX/BHJ;->x:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, LX/BHJ;->y:Z

    if-nez v0, :cond_9

    .line 1768755
    instance-of v0, p1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_b

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1768756
    iget-boolean v4, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v4

    .line 1768757
    if-eqz v0, :cond_b

    .line 1768758
    invoke-direct {p0}, LX/BHJ;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1768759
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081353

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_0

    .line 1768760
    :cond_8
    iget v0, p0, LX/BHJ;->u:I

    if-nez v0, :cond_12

    iget v0, p0, LX/BHJ;->m:I

    if-lez v0, :cond_12

    move v0, v3

    .line 1768761
    :goto_4
    if-eqz v0, :cond_9

    .line 1768762
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v4, LX/27k;

    const v5, 0x7f081354

    invoke-direct {v4, v5}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v4}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1768763
    :cond_9
    iget v0, p0, LX/BHJ;->m:I

    if-lez v0, :cond_f

    .line 1768764
    iget-boolean v0, p0, LX/BHJ;->p:Z

    if-nez v0, :cond_c

    instance-of v0, p1, Lcom/facebook/photos/base/media/VideoItem;

    iget-boolean v4, p0, LX/BHJ;->k:Z

    xor-int/2addr v0, v4

    if-eqz v0, :cond_c

    .line 1768765
    iget-object v0, p0, LX/BHJ;->n:LX/27l;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/BHJ;->n:LX/27l;

    invoke-virtual {v0}, LX/27l;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1768766
    :cond_a
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081352

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    move-result-object v0

    iput-object v0, p0, LX/BHJ;->n:LX/27l;

    goto/16 :goto_0

    .line 1768767
    :cond_b
    invoke-direct {p0}, LX/BHJ;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v3

    .line 1768768
    goto :goto_4

    .line 1768769
    :cond_c
    iget v0, p0, LX/BHJ;->m:I

    iget v4, p0, LX/BHJ;->d:I

    if-lt v0, v4, :cond_d

    .line 1768770
    iget-object v0, p0, LX/BHJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v4, 0x7f081358

    new-array v3, v3, [Ljava/lang/Object;

    iget v5, p0, LX/BHJ;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-direct {v1, v4, v3}, LX/27k;-><init>(I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_0

    .line 1768771
    :cond_d
    iget-boolean v0, p0, LX/BHJ;->l:Z

    if-nez v0, :cond_f

    .line 1768772
    iget-object v0, p0, LX/BHJ;->i:LX/BHi;

    invoke-virtual {v0}, LX/BHi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIF;

    .line 1768773
    invoke-interface {v0}, LX/BIF;->f()V

    goto :goto_5

    .line 1768774
    :cond_e
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1768775
    iput v2, p0, LX/BHJ;->m:I

    .line 1768776
    iput v2, p0, LX/BHJ;->u:I

    .line 1768777
    iput v2, p0, LX/BHJ;->v:I

    .line 1768778
    :cond_f
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    iget v2, p0, LX/BHJ;->m:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/BHJ;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768779
    if-eqz v1, :cond_11

    .line 1768780
    iget v0, p0, LX/BHJ;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BHJ;->v:I

    .line 1768781
    invoke-static {p0}, LX/BHJ;->d(LX/BHJ;)V

    .line 1768782
    :goto_6
    instance-of v0, p1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_10

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1768783
    iget-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v1

    .line 1768784
    if-eqz v0, :cond_10

    .line 1768785
    iget v0, p0, LX/BHJ;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BHJ;->u:I

    .line 1768786
    :cond_10
    sget-object v0, LX/74p;->SELECT:LX/74p;

    invoke-static {p0, p1, v0}, LX/BHJ;->a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;LX/74p;)V

    move v2, v3

    .line 1768787
    goto/16 :goto_0

    .line 1768788
    :cond_11
    invoke-direct {p0, p1}, LX/BHJ;->b(Lcom/facebook/ipc/media/MediaItem;)V

    goto :goto_6

    :cond_12
    move v0, v2

    goto/16 :goto_4

    :cond_13
    move v1, v2

    goto/16 :goto_1

    :cond_14
    move v7, v10

    .line 1768789
    goto/16 :goto_2

    .line 1768790
    :cond_15
    iget-object v8, v0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_16

    iget-object v8, v0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_16

    .line 1768791
    invoke-virtual {v0, v5}, LX/BHZ;->a(LX/BHi;)V

    goto/16 :goto_3

    .line 1768792
    :cond_16
    if-eqz v7, :cond_7

    .line 1768793
    invoke-virtual {v5}, LX/BHi;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_17
    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/BIF;

    .line 1768794
    instance-of v8, v7, LX/BIE;

    if-eqz v8, :cond_17

    invoke-interface {v7}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v8

    if-eqz v8, :cond_17

    iget-object v8, v0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-interface {v7}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_17

    move-object v8, v7

    check-cast v8, LX/BIE;

    invoke-interface {v8}, LX/BIE;->getHighlightLayerView()Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_17

    move-object v8, v7

    .line 1768795
    check-cast v8, LX/BIE;

    invoke-interface {v8}, LX/BIE;->getHighlightLayerView()Landroid/view/View;

    move-result-object v8

    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Landroid/view/View;->setAlpha(F)V

    move-object v8, v7

    .line 1768796
    check-cast v8, LX/BIE;

    invoke-interface {v8}, LX/BIE;->getHighlightLayerView()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1768797
    invoke-static {v0, v7}, LX/BHZ;->a(LX/BHZ;LX/BIF;)LX/8Hs;

    move-result-object v7

    invoke-virtual {v7}, LX/8Hs;->d()V

    goto :goto_7

    .line 1768798
    :cond_18
    iput-boolean v9, v0, LX/BHZ;->d:Z

    .line 1768799
    iget-object v7, v0, LX/BHZ;->h:Ljava/util/ArrayList;

    iget-object v8, v0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1768800
    if-eqz v6, :cond_19

    .line 1768801
    invoke-virtual {v6}, LX/BI6;->d()V

    .line 1768802
    :cond_19
    invoke-static {v0}, LX/BHZ;->d(LX/BHZ;)V

    .line 1768803
    iget v7, v0, LX/BHZ;->e:I

    iget v8, v0, LX/BHZ;->f:I

    .line 1768804
    iget-object v9, v0, LX/BHZ;->k:LX/BHt;

    iget-object v10, v0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 1768805
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 1768806
    const-string v12, "highlighted_items_count"

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768807
    const-string v12, "highlighted_items_photo_count"

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768808
    const-string v12, "highlighted_items_video_count"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768809
    const-string v12, "picker_highlights_cluster_details"

    invoke-static {v9, v12, v11}, LX/BHt;->a(LX/BHt;Ljava/lang/String;Ljava/util/Map;)V

    .line 1768810
    goto/16 :goto_3
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1768664
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1768665
    new-instance v1, LX/BHI;

    invoke-direct {v1, p0}, LX/BHI;-><init>(LX/BHJ;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1768666
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/BIF;)V
    .locals 6

    .prologue
    .line 1768667
    invoke-interface {p1}, LX/BIF;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1768668
    :cond_0
    :goto_0
    return-void

    .line 1768669
    :cond_1
    invoke-interface {p1}, LX/BIF;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1768670
    iget-object v0, p0, LX/BHJ;->s:LX/BIO;

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/BIF;->getItemType()LX/BHH;

    move-result-object v0

    sget-object v1, LX/BHH;->VIDEO:LX/BHH;

    invoke-virtual {v0, v1}, LX/BHH;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/BIF;->getId()I

    move-result v1

    iget-object v0, p0, LX/BHJ;->s:LX/BIO;

    check-cast v0, LX/BIF;

    invoke-interface {v0}, LX/BIF;->getId()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 1768671
    iget-object v0, p0, LX/BHJ;->t:LX/BIO;

    if-eqz v0, :cond_2

    .line 1768672
    iget-object v0, p0, LX/BHJ;->t:LX/BIO;

    invoke-interface {v0}, LX/BIO;->k()V

    :cond_2
    move-object v0, p1

    .line 1768673
    check-cast v0, LX/BIO;

    invoke-interface {p1}, LX/BIF;->getSelectedOrder()I

    move-result v1

    invoke-interface {v0, v1}, LX/BIO;->a(I)V

    move-object v0, p1

    .line 1768674
    check-cast v0, LX/BIO;

    iput-object v0, p0, LX/BHJ;->t:LX/BIO;

    .line 1768675
    :cond_3
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1768676
    iget-object v0, p0, LX/BHJ;->z:LX/BHZ;

    iget v2, p0, LX/BHJ;->m:I

    iget-object v3, p0, LX/BHJ;->i:LX/BHi;

    .line 1768677
    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    iget-object v4, v0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1768678
    invoke-virtual {v0, v3}, LX/BHZ;->a(LX/BHi;)V

    .line 1768679
    :cond_4
    iget-boolean v0, p0, LX/BHJ;->o:Z

    if-eqz v0, :cond_6

    .line 1768680
    iget-object v0, p0, LX/BHJ;->c:LX/BGe;

    .line 1768681
    if-eqz p1, :cond_5

    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v2

    if-nez v2, :cond_16

    .line 1768682
    :cond_5
    const-string v2, ""

    .line 1768683
    :goto_1
    move-object v2, v2

    .line 1768684
    sget-object v3, LX/BGd;->MEDIA_ITEM_DESELECTED:LX/BGd;

    invoke-static {v3}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "media_type"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v0, v3}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1768685
    :cond_6
    invoke-interface {p1}, LX/BIF;->getSelectedOrder()I

    move-result v2

    .line 1768686
    invoke-interface {p1}, LX/BIF;->f()V

    .line 1768687
    if-eqz v1, :cond_7

    iget-object v0, p0, LX/BHJ;->q:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/media/MediaItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, LX/BHJ;->p:Z

    if-nez v0, :cond_7

    .line 1768688
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/BHJ;->a(LX/BHJ;Z)V

    .line 1768689
    :cond_7
    if-eqz v1, :cond_b

    .line 1768690
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768691
    iget v0, p0, LX/BHJ;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/BHJ;->m:I

    .line 1768692
    :goto_2
    instance-of v0, v1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_8

    move-object v0, v1

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1768693
    iget-boolean v3, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v3

    .line 1768694
    if-eqz v0, :cond_8

    .line 1768695
    iget v0, p0, LX/BHJ;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/BHJ;->u:I

    .line 1768696
    :cond_8
    iget-boolean v0, p0, LX/BHJ;->w:Z

    if-eqz v0, :cond_9

    invoke-static {v1}, LX/BHJ;->c(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1768697
    iget v0, p0, LX/BHJ;->v:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/BHJ;->v:I

    .line 1768698
    invoke-static {p0}, LX/BHJ;->d(LX/BHJ;)V

    .line 1768699
    :cond_9
    iget-object v0, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1768700
    iget-object v1, p0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1768701
    if-le v1, v2, :cond_a

    .line 1768702
    iget-object v4, p0, LX/BHJ;->a:Ljava/util/Map;

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1768703
    :cond_b
    iget-object v0, p0, LX/BHJ;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v3, LX/BHJ;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "getMediaItem is null"

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1768704
    :cond_c
    iget-object v0, p0, LX/BHJ;->i:LX/BHi;

    invoke-virtual {v0}, LX/BHi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIF;

    .line 1768705
    invoke-interface {v0}, LX/BIF;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v0}, LX/BIF;->getSelectedOrder()I

    move-result v3

    if-le v3, v2, :cond_d

    .line 1768706
    invoke-interface {v0}, LX/BIF;->getSelectedOrder()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1768707
    iget-boolean v4, p0, LX/BHJ;->l:Z

    move v4, v4

    .line 1768708
    invoke-interface {v0, v3, v4}, LX/BIF;->a(IZ)V

    goto :goto_4

    .line 1768709
    :cond_e
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1768710
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    sget-object v1, LX/74p;->DESELECT:LX/74p;

    invoke-static {p0, v0, v1}, LX/BHJ;->a(LX/BHJ;Lcom/facebook/ipc/media/MediaItem;LX/74p;)V

    .line 1768711
    :cond_f
    goto/16 :goto_0

    .line 1768712
    :cond_10
    iget-object v0, p0, LX/BHJ;->f:LX/BH4;

    .line 1768713
    iget-object v1, v0, LX/BH4;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c()V

    .line 1768714
    iget-object v0, p0, LX/BHJ;->g:LX/11i;

    sget-object v1, LX/BHx;->a:LX/BHw;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1768715
    if-eqz v0, :cond_11

    .line 1768716
    const-string v1, "GridItemSelected"

    const v2, -0x1d679ac5

    invoke-static {v0, v1, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1768717
    :cond_11
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1768718
    if-eqz v0, :cond_13

    .line 1768719
    invoke-virtual {p0, v0}, LX/BHJ;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1768720
    iget-boolean v1, p0, LX/BHJ;->o:Z

    if-eqz v1, :cond_12

    .line 1768721
    iget-object v1, p0, LX/BHJ;->c:LX/BGe;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    invoke-virtual {v0}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/BIF;->getIndex()I

    move-result v2

    .line 1768722
    sget-object v3, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GRID:LX/BGd;

    invoke-static {v3}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "media_type"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "index"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v1, v3}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1768723
    :cond_12
    iget v0, p0, LX/BHJ;->m:I

    .line 1768724
    iget-boolean v1, p0, LX/BHJ;->l:Z

    move v1, v1

    .line 1768725
    invoke-interface {p1, v0, v1}, LX/BIF;->a(IZ)V

    .line 1768726
    :cond_13
    invoke-interface {p1}, LX/BIF;->getItemType()LX/BHH;

    move-result-object v0

    sget-object v1, LX/BHH;->VIDEO:LX/BHH;

    invoke-virtual {v0, v1}, LX/BHH;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768727
    iget-object v0, p0, LX/BHJ;->s:LX/BIO;

    if-eqz v0, :cond_14

    .line 1768728
    iget-object v0, p0, LX/BHJ;->s:LX/BIO;

    invoke-interface {v0}, LX/BIO;->i()V

    .line 1768729
    :cond_14
    iget-object v0, p0, LX/BHJ;->t:LX/BIO;

    if-eqz v0, :cond_15

    .line 1768730
    iget-object v0, p0, LX/BHJ;->t:LX/BIO;

    invoke-interface {v0}, LX/BIO;->k()V

    .line 1768731
    :cond_15
    check-cast p1, LX/BIO;

    iput-object p1, p0, LX/BHJ;->s:LX/BIO;

    .line 1768732
    iget-object v0, p0, LX/BHJ;->s:LX/BIO;

    invoke-interface {v0}, LX/BIO;->j()V

    goto/16 :goto_0

    :cond_16
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v2

    invoke-virtual {v2}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method
