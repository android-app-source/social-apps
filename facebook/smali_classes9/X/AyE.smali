.class public LX/AyE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile e:LX/AyE;


# instance fields
.field public final b:LX/AyH;

.field public final c:LX/0lB;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729843
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "souvenir_db_model_version"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AyE;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/AyH;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729805
    iput-object p1, p0, LX/AyE;->b:LX/AyH;

    .line 1729806
    iput-object p2, p0, LX/AyE;->c:LX/0lB;

    .line 1729807
    iput-object p3, p0, LX/AyE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1729808
    return-void
.end method

.method public static a(LX/0QB;)LX/AyE;
    .locals 6

    .prologue
    .line 1729830
    sget-object v0, LX/AyE;->e:LX/AyE;

    if-nez v0, :cond_1

    .line 1729831
    const-class v1, LX/AyE;

    monitor-enter v1

    .line 1729832
    :try_start_0
    sget-object v0, LX/AyE;->e:LX/AyE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729833
    if-eqz v2, :cond_0

    .line 1729834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729835
    new-instance p0, LX/AyE;

    invoke-static {v0}, LX/AyH;->a(LX/0QB;)LX/AyH;

    move-result-object v3

    check-cast v3, LX/AyH;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/AyE;-><init>(LX/AyH;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1729836
    move-object v0, p0

    .line 1729837
    sput-object v0, LX/AyE;->e:LX/AyE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729838
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729839
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729840
    :cond_1
    sget-object v0, LX/AyE;->e:LX/AyE;

    return-object v0

    .line 1729841
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1729844
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1729845
    const-string v1, "models"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1729846
    iget-object v1, p0, LX/AyE;->b:LX/AyH;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1729847
    if-nez v1, :cond_0

    .line 1729848
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1729849
    :goto_0
    return-object v0

    .line 1729850
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1729851
    new-instance v2, LX/0cA;

    invoke-direct {v2}, LX/0cA;-><init>()V

    .line 1729852
    sget-object v0, LX/AyI;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 1729853
    sget-object v0, LX/AyI;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 1729854
    sget-object v0, LX/AyI;->c:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 1729855
    :cond_1
    iget-object v0, p0, LX/AyE;->c:LX/0lB;

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v0, v6, v7}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1729856
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1729857
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v9

    .line 1729858
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "The value in the \'id\' column does not match that in the blob"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "\nID Column: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "\nModel ID: "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1729859
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1729860
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->b()J

    move-result-wide v12

    .line 1729861
    cmp-long v8, v12, v10

    if-nez v8, :cond_3

    const/4 v8, 0x1

    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v14, "The value in the \'start_date\' column does not match that in the blob"

    invoke-direct {v9, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "\nStart Date Column: "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\nModel Start Date: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1729862
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1729863
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1729864
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;
    :try_end_0
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1729865
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1729866
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1729867
    goto/16 :goto_0

    .line 1729868
    :catch_0
    move-exception v0

    .line 1729869
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "When deserializing JSON blob into SouvenirModel, we had a mapping issue"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729870
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1729871
    :catch_1
    move-exception v0

    .line 1729872
    :try_start_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "When deserializing JSON blob into SouvenirModel, we had a parsing error"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1729873
    :catch_2
    move-exception v0

    .line 1729874
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1729875
    :cond_3
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729811
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729812
    iget-object v0, p0, LX/AyE;->b:LX/AyH;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1729813
    const v0, -0x76d9acc0

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1729814
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1729815
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v4

    .line 1729816
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->b()J

    move-result-wide v6

    .line 1729817
    iget-object v5, p0, LX/AyE;->c:LX/0lB;

    invoke-virtual {v5, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1729818
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1729819
    sget-object v9, LX/AyI;->a:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1729820
    sget-object v4, LX/AyI;->b:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v8, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729821
    sget-object v4, LX/AyI;->c:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1729822
    const-string v4, "models"

    const-string v5, ""

    const v6, -0x4d104374

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v1, v4, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, -0x368fcf14    # -983822.75f

    invoke-static {v4}, LX/03h;->a(I)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1729823
    goto :goto_0

    .line 1729824
    :catch_0
    move-exception v0

    .line 1729825
    :try_start_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "SouvenirModel couldn\'t be serialized into JSON for storage"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729826
    :catchall_0
    move-exception v0

    const v2, 0x59f77f91

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1729827
    :cond_0
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch LX/28F; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1729828
    const v0, -0x20d04c2d

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1729829
    return-object p1
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1729810
    iget-object v0, p0, LX/AyE;->b:LX/AyH;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "models"

    const-string v2, "1"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 1729809
    iget-object v0, p0, LX/AyE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AyE;->a:LX/0Tn;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method
