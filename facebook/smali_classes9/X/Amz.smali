.class public LX/Amz;
.super LX/1SX;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Amz;


# instance fields
.field private final b:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1711706
    new-instance v0, LX/Amx;

    invoke-direct {v0}, LX/Amx;-><init>()V

    sput-object v0, LX/Amz;->a:LX/0Or;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Zb;LX/17Q;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711707
    sget-object v15, LX/Amz;->a:LX/0Or;

    sget-object v16, LX/Amz;->a:LX/0Or;

    sget-object v17, LX/Amz;->a:LX/0Or;

    const/16 v20, 0x0

    sget-object v24, LX/Amz;->a:LX/0Or;

    const/16 v29, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v18, p14

    move-object/from16 v19, p15

    move-object/from16 v21, p16

    move-object/from16 v22, p17

    move-object/from16 v23, p18

    move-object/from16 v25, p19

    move-object/from16 v26, p20

    move-object/from16 v27, p21

    move-object/from16 v28, p22

    move-object/from16 v30, p23

    move-object/from16 v31, p24

    move-object/from16 v32, p25

    move-object/from16 v33, p26

    move-object/from16 v34, p27

    move-object/from16 v35, p28

    move-object/from16 v36, p29

    move-object/from16 v40, p30

    invoke-direct/range {v1 .. v40}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 1711708
    new-instance v1, LX/Amy;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, LX/Amy;-><init>(LX/Amz;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/Amz;->b:LX/1Sp;

    .line 1711709
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(Ljava/lang/String;)V

    .line 1711710
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Amz;->b:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(LX/1Sp;)V

    .line 1711711
    return-void
.end method

.method public static a(LX/0QB;)LX/Amz;
    .locals 3

    .prologue
    .line 1711712
    sget-object v0, LX/Amz;->c:LX/Amz;

    if-nez v0, :cond_1

    .line 1711713
    const-class v1, LX/Amz;

    monitor-enter v1

    .line 1711714
    :try_start_0
    sget-object v0, LX/Amz;->c:LX/Amz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1711715
    if-eqz v2, :cond_0

    .line 1711716
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Amz;->b(LX/0QB;)LX/Amz;

    move-result-object v0

    sput-object v0, LX/Amz;->c:LX/Amz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711717
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1711718
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1711719
    :cond_1
    sget-object v0, LX/Amz;->c:LX/Amz;

    return-object v0

    .line 1711720
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1711721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/Amz;
    .locals 33

    .prologue
    .line 1711722
    new-instance v2, LX/Amz;

    const/16 v3, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 v7, 0x236a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v9

    check-cast v9, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v10

    check-cast v10, LX/1Sj;

    const/16 v11, 0x327a

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v12

    check-cast v12, LX/16H;

    const/16 v13, 0xde

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v14

    check-cast v14, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v15

    check-cast v15, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v16

    check-cast v16, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v17

    check-cast v17, LX/17Q;

    const/16 v18, 0x12c4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v19

    check-cast v19, LX/0SG;

    const/16 v20, 0x2fd7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x31d5

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v22

    check-cast v22, LX/14w;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    const/16 v24, 0x1399

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v25

    check-cast v25, LX/0qn;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v26

    check-cast v26, LX/0W3;

    const/16 v27, 0x31aa

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x122d

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    invoke-static/range {p0 .. p0}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v29

    check-cast v29, LX/1Sl;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v30

    check-cast v30, LX/0tX;

    const-class v31, LX/1Sm;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/1Sm;

    invoke-static/range {p0 .. p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v32

    check-cast v32, LX/0wL;

    invoke-direct/range {v2 .. v32}, LX/Amz;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Zb;LX/17Q;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V

    .line 1711723
    return-object v2
.end method
