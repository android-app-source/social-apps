.class public LX/B0e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0N;


# instance fields
.field private final a:LX/B0d;

.field private final b:Lcom/facebook/graphql/cursor/edgestore/PageInfo;


# direct methods
.method public constructor <init>(LX/B0d;)V
    .locals 2

    .prologue
    .line 1734098
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/B0e;-><init>(LX/B0d;Ljava/lang/String;Z)V

    .line 1734099
    return-void
.end method

.method private constructor <init>(LX/B0d;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1734100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734101
    invoke-static {p1, p2, p3}, LX/B0d;->a(LX/B0d;Ljava/lang/String;Z)LX/B0d;

    move-result-object v0

    iput-object v0, p0, LX/B0e;->a:LX/B0d;

    .line 1734102
    iget-object v0, p0, LX/B0e;->a:LX/B0d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/B0d;->a(LX/B0d;I)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    iput-object v0, p0, LX/B0e;->b:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1734103
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 1734093
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()LX/95b;
    .locals 1

    .prologue
    .line 1734097
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734096
    iget-object v0, p0, LX/B0e;->b:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    return-object v0
.end method

.method public final d()LX/B0d;
    .locals 1

    .prologue
    .line 1734095
    iget-object v0, p0, LX/B0e;->a:LX/B0d;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1734094
    const/4 v0, 0x0

    return v0
.end method
