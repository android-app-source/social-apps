.class public final LX/C6O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/C6Y;

.field public final synthetic b:LX/1Pq;


# direct methods
.method public constructor <init>(LX/C6Y;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1849864
    iput-object p1, p0, LX/C6O;->a:LX/C6Y;

    iput-object p2, p0, LX/C6O;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, -0x7b19e49b

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1849865
    iget-object v0, p0, LX/C6O;->a:LX/C6Y;

    iget-boolean v0, v0, LX/C6Y;->a:Z

    if-eqz v0, :cond_3

    .line 1849866
    iget-object v0, p0, LX/C6O;->a:LX/C6Y;

    iget-object v0, v0, LX/C6Y;->g:LX/C6a;

    .line 1849867
    iget v5, v0, LX/C6a;->e:I

    move v0, v5

    .line 1849868
    if-ne v0, v4, :cond_1

    move v0, v1

    .line 1849869
    :goto_0
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {v4}, LX/C6a;->f()V

    .line 1849870
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    .line 1849871
    iput v5, v4, LX/C6a;->e:I

    .line 1849872
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    .line 1849873
    iget-boolean v5, v4, LX/C6a;->a:Z

    move v4, v5

    .line 1849874
    if-eqz v4, :cond_2

    .line 1849875
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {v4}, LX/C6a;->c()V

    .line 1849876
    :goto_1
    if-eqz v0, :cond_0

    .line 1849877
    iget-object v0, p0, LX/C6O;->b:LX/1Pq;

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1849878
    :cond_0
    const v0, -0x3399e1e5    # -6.0323948E7f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_1
    move v0, v2

    .line 1849879
    goto :goto_0

    .line 1849880
    :cond_2
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {v4}, LX/C6a;->h()V

    goto :goto_1

    .line 1849881
    :cond_3
    iget-object v0, p0, LX/C6O;->a:LX/C6Y;

    iget-object v0, v0, LX/C6Y;->g:LX/C6a;

    .line 1849882
    iget v5, v0, LX/C6a;->f:I

    move v0, v5

    .line 1849883
    if-ne v0, v4, :cond_4

    move v0, v1

    .line 1849884
    :goto_2
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    .line 1849885
    iput v5, v4, LX/C6a;->f:I

    .line 1849886
    iget-object v4, p0, LX/C6O;->a:LX/C6Y;

    iget-object v4, v4, LX/C6Y;->g:LX/C6a;

    invoke-virtual {v4}, LX/C6a;->h()V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1849887
    goto :goto_2
.end method
