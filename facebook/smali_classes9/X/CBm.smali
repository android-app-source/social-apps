.class public final enum LX/CBm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBm;

.field public static final enum CIRCLE_CROP:LX/CBm;

.field public static final enum FACEBOOK_BADGE:LX/CBm;

.field public static final enum MESSENGER_BADGE:LX/CBm;

.field public static final enum UNKNOWN:LX/CBm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856814
    new-instance v0, LX/CBm;

    const-string v1, "CIRCLE_CROP"

    invoke-direct {v0, v1, v2}, LX/CBm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBm;->CIRCLE_CROP:LX/CBm;

    .line 1856815
    new-instance v0, LX/CBm;

    const-string v1, "FACEBOOK_BADGE"

    invoke-direct {v0, v1, v3}, LX/CBm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBm;->FACEBOOK_BADGE:LX/CBm;

    .line 1856816
    new-instance v0, LX/CBm;

    const-string v1, "MESSENGER_BADGE"

    invoke-direct {v0, v1, v4}, LX/CBm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBm;->MESSENGER_BADGE:LX/CBm;

    .line 1856817
    new-instance v0, LX/CBm;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/CBm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBm;->UNKNOWN:LX/CBm;

    .line 1856818
    const/4 v0, 0x4

    new-array v0, v0, [LX/CBm;

    sget-object v1, LX/CBm;->CIRCLE_CROP:LX/CBm;

    aput-object v1, v0, v2

    sget-object v1, LX/CBm;->FACEBOOK_BADGE:LX/CBm;

    aput-object v1, v0, v3

    sget-object v1, LX/CBm;->MESSENGER_BADGE:LX/CBm;

    aput-object v1, v0, v4

    sget-object v1, LX/CBm;->UNKNOWN:LX/CBm;

    aput-object v1, v0, v5

    sput-object v0, LX/CBm;->$VALUES:[LX/CBm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856813
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBm;
    .locals 1

    .prologue
    .line 1856806
    if-nez p0, :cond_0

    .line 1856807
    :try_start_0
    sget-object v0, LX/CBm;->UNKNOWN:LX/CBm;

    .line 1856808
    :goto_0
    return-object v0

    .line 1856809
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBm;->valueOf(Ljava/lang/String;)LX/CBm;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856810
    :catch_0
    sget-object v0, LX/CBm;->UNKNOWN:LX/CBm;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBm;
    .locals 1

    .prologue
    .line 1856812
    const-class v0, LX/CBm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBm;

    return-object v0
.end method

.method public static values()[LX/CBm;
    .locals 1

    .prologue
    .line 1856811
    sget-object v0, LX/CBm;->$VALUES:[LX/CBm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBm;

    return-object v0
.end method
