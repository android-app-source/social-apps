.class public final LX/CAF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V
    .locals 0

    .prologue
    .line 1855062
    iput-object p1, p0, LX/CAF;->c:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    iput-object p2, p0, LX/CAF;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/CAF;->b:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x6f809e94

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1855063
    iget-object v0, p0, LX/CAF;->c:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->c:LX/0if;

    sget-object v2, LX/0ig;->aH:LX/0ih;

    iget-object v3, p0, LX/CAF;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    const-string v3, "open_seen_state"

    invoke-virtual {v0, v2, v4, v5, v3}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1855064
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1855065
    iget-object v0, p0, LX/CAF;->c:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/CAF;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v4, p0, LX/CAF;->b:LX/1Po;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Qt;)Landroid/content/Intent;

    move-result-object v3

    .line 1855066
    const-string v0, "fragment_title"

    const v4, 0x7f0810db

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855067
    iget-object v0, p0, LX/CAF;->c:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1855068
    const v0, 0x5128ac2

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
