.class public final LX/BXQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793078
    iput-object p1, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2f0d9a77

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1793079
    iget-object v1, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v1, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 1793080
    const v1, -0x7ba87106

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1793081
    :goto_0
    return-void

    .line 1793082
    :cond_0
    iget-object v1, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    const/4 v2, 0x0

    .line 1793083
    invoke-static {v1, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    .line 1793084
    iget-object v1, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v1, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->requestFocus()Z

    .line 1793085
    iget-object v1, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v2, p0, LX/BXQ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v2, v2, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1793086
    iget-object v3, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->g:Landroid/view/inputmethod/InputMethodManager;

    const/4 p0, 0x0

    invoke-virtual {v3, v2, p0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1793087
    const v1, -0x3901eb3c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
