.class public final LX/AaK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AaD;

.field public final synthetic c:LX/AaL;


# direct methods
.method public constructor <init>(LX/AaL;Ljava/lang/String;LX/AaD;)V
    .locals 0

    .prologue
    .line 1688350
    iput-object p1, p0, LX/AaK;->c:LX/AaL;

    iput-object p2, p0, LX/AaK;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AaK;->b:LX/AaD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 1688351
    iget-object v0, p0, LX/AaK;->c:LX/AaL;

    iget-object v0, v0, LX/AaL;->c:LX/AaB;

    .line 1688352
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string v3, "go_live_with_fundraiser_error"

    const/4 v4, 0x0

    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "broadcast_id"

    iget-object p1, v0, LX/AaB;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1688353
    invoke-static {v0}, LX/AaB;->i(LX/AaB;)V

    .line 1688354
    iget-object v0, p0, LX/AaK;->b:LX/AaD;

    if-eqz v0, :cond_0

    .line 1688355
    iget-object v0, p0, LX/AaK;->b:LX/AaD;

    invoke-interface {v0}, LX/AaD;->j()V

    .line 1688356
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688357
    iget-object v0, p0, LX/AaK;->c:LX/AaL;

    iget-object v0, v0, LX/AaL;->c:LX/AaB;

    iget-object v1, p0, LX/AaK;->a:Ljava/lang/String;

    .line 1688358
    iget-object v2, v0, LX/AaB;->d:LX/0if;

    sget-object v3, LX/0ig;->E:LX/0ih;

    const-string v4, "go_live_with_fundraiser"

    const/4 v5, 0x0

    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v6

    const-string p0, "broadcast_id"

    iget-object p1, v0, LX/AaB;->c:Ljava/lang/String;

    invoke-virtual {v6, p0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    const-string p0, "fundraiser_id"

    invoke-virtual {v6, p0, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    const-string p0, "fundraiser_type"

    const p1, -0x4e6785e3

    invoke-static {p1}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1688359
    invoke-static {v0}, LX/AaB;->i(LX/AaB;)V

    .line 1688360
    return-void
.end method
