.class public LX/BdF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/Egm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/components/list/fb/datasources/GraphQLRootQuerySectionSpec$Configuration",
            "<TTModel;>;"
        }
    .end annotation
.end field

.field public c:LX/BdB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/components/list/fb/datasources/GraphQLRootQueryService$Listener",
            "<TTModel;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TTModel;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Egm;LX/0tX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/list/fb/datasources/GraphQLRootQuerySectionSpec$Configuration",
            "<TTModel;>;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1803501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1803502
    iput-object p1, p0, LX/BdF;->b:LX/Egm;

    .line 1803503
    iput-object p2, p0, LX/BdF;->a:LX/0tX;

    .line 1803504
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/BdB;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/list/fb/datasources/GraphQLRootQueryService$Listener",
            "<TTModel;>;Z)V"
        }
    .end annotation

    .prologue
    .line 1803505
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/BdF;->c:LX/BdB;

    .line 1803506
    if-eqz p2, :cond_0

    .line 1803507
    iget-object v0, p0, LX/BdF;->c:LX/BdB;

    invoke-virtual {v0}, LX/BdB;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1803508
    :cond_0
    monitor-exit p0

    return-void

    .line 1803509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1803510
    iget-object v0, p0, LX/BdF;->a:LX/0tX;

    .line 1803511
    new-instance v2, LX/EgT;

    invoke-direct {v2}, LX/EgT;-><init>()V

    move-object v2, v2

    .line 1803512
    const-string v3, "device"

    const-string v4, "ANDROID"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "iconStyle"

    const-string v4, "CASPIAN"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "admined_page_count"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    move-object v1, v2

    .line 1803513
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/BdF;->d:LX/1Zp;

    .line 1803514
    iget-object v0, p0, LX/BdF;->c:LX/BdB;

    if-eqz v0, :cond_0

    .line 1803515
    iget-object v0, p0, LX/BdF;->c:LX/BdB;

    invoke-virtual {v0}, LX/BdB;->a()V

    .line 1803516
    :cond_0
    iget-object v0, p0, LX/BdF;->d:LX/1Zp;

    new-instance v1, LX/BdE;

    invoke-direct {v1, p0}, LX/BdE;-><init>(LX/BdF;)V

    .line 1803517
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1803518
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1803519
    return-void
.end method
