.class public final LX/B8H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/B8K;


# direct methods
.method public constructor <init>(LX/B8K;)V
    .locals 0

    .prologue
    .line 1748646
    iput-object p1, p0, LX/B8H;->a:LX/B8K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 1748647
    if-eqz p2, :cond_0

    .line 1748648
    iget-object v0, p0, LX/B8H;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B8H;->a:LX/B8K;

    iget-object v1, v1, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setSelection(I)V

    .line 1748649
    iget-object v0, p0, LX/B8H;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->d:LX/B7W;

    new-instance v1, LX/B7Z;

    const/4 v2, 0x0

    iget-object v3, p0, LX/B8H;->a:LX/B8K;

    iget-object v3, v3, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-direct {v1, v2, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1748650
    iget-object v0, p0, LX/B8H;->a:LX/B8K;

    invoke-static {v0}, LX/B8K;->g(LX/B8K;)V

    .line 1748651
    iget-object v0, p0, LX/B8H;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748652
    :cond_0
    return-void
.end method
