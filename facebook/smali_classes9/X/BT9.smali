.class public LX/BT9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BSi;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final A:I

.field private final B:I

.field private C:Landroid/view/ViewStub;

.field private D:Landroid/view/View;

.field public E:Landroid/widget/ImageView;

.field public F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

.field public G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field public H:Landroid/view/View;

.field public I:Landroid/view/View;

.field public J:Landroid/view/View;

.field public K:Landroid/view/View;

.field public L:LX/BT8;

.field public M:LX/BT8;

.field public N:LX/BT8;

.field public O:Landroid/view/View;

.field private P:Landroid/view/ViewStub;

.field private Q:Lcom/facebook/resources/ui/FbTextView;

.field private R:Lcom/facebook/resources/ui/FbTextView;

.field private S:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

.field public T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

.field public U:J

.field public V:Z

.field public final W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

.field private X:Z

.field public Y:LX/60x;

.field public Z:Z

.field private final a:Landroid/content/Context;

.field public aa:Z

.field public ab:Z

.field public ac:Z

.field public final b:LX/BSr;

.field public final c:LX/BSr;

.field private final d:LX/0ad;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Z

.field public final h:Z

.field public final i:Z

.field private final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:LX/BTl;

.field public final o:LX/BTX;

.field private final p:LX/BTZ;

.field public final q:LX/BTJ;

.field public final r:LX/BTL;

.field public final s:LX/BTN;

.field public final t:LX/BTP;

.field public final u:LX/BTS;

.field private final v:LX/BTG;

.field private final w:Ljava/lang/String;

.field private final x:LX/2Mj;

.field public final y:LX/8tH;

.field private final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BTT;LX/BTl;LX/BTY;LX/BTO;LX/BTQ;LX/BTG;LX/8tH;LX/8tK;LX/0ad;Landroid/net/Uri;Landroid/net/Uri;LX/7Sv;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/model/VideoTrimParams;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/BSr;LX/BSr;)V
    .locals 7
    .param p11    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/7Sv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p18    # Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p19    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p20    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p21    # Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p22    # LX/BSr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p23    # LX/BSr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1786864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786865
    new-instance v2, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    invoke-direct {v2}, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;-><init>()V

    iput-object v2, p0, LX/BT9;->W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    .line 1786866
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->X:Z

    .line 1786867
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->Z:Z

    .line 1786868
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->aa:Z

    .line 1786869
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->ab:Z

    .line 1786870
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->ac:Z

    .line 1786871
    iput-object p1, p0, LX/BT9;->a:Landroid/content/Context;

    .line 1786872
    move-object/from16 v0, p22

    iput-object v0, p0, LX/BT9;->c:LX/BSr;

    .line 1786873
    move-object/from16 v0, p23

    iput-object v0, p0, LX/BT9;->b:LX/BSr;

    .line 1786874
    iput-object p7, p0, LX/BT9;->v:LX/BTG;

    .line 1786875
    move-object/from16 v0, p9

    iput-object v0, p0, LX/BT9;->x:LX/2Mj;

    .line 1786876
    iput-object p8, p0, LX/BT9;->y:LX/8tH;

    .line 1786877
    move-object/from16 v0, p19

    iput-object v0, p0, LX/BT9;->w:Ljava/lang/String;

    .line 1786878
    move-object/from16 v0, p10

    iput-object v0, p0, LX/BT9;->d:LX/0ad;

    .line 1786879
    move-object/from16 v0, p11

    iput-object v0, p0, LX/BT9;->e:Landroid/net/Uri;

    .line 1786880
    move-object/from16 v0, p14

    iput-object v0, p0, LX/BT9;->f:Ljava/lang/String;

    .line 1786881
    move-object/from16 v0, p15

    iput-object v0, p0, LX/BT9;->C:Landroid/view/ViewStub;

    .line 1786882
    move-object/from16 v0, p21

    iput-object v0, p0, LX/BT9;->S:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    .line 1786883
    move-object/from16 v0, p20

    iput-object v0, p0, LX/BT9;->P:Landroid/view/ViewStub;

    .line 1786884
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j()Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->h:Z

    .line 1786885
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l()I

    move-result v2

    iput v2, p0, LX/BT9;->j:I

    .line 1786886
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m()I

    move-result v2

    iput v2, p0, LX/BT9;->k:I

    .line 1786887
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f()Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->g:Z

    .line 1786888
    move-object/from16 v0, p18

    iput-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786889
    move-object/from16 v0, p16

    iput-object v0, p0, LX/BT9;->T:Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1786890
    iget-object v2, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0c42

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, LX/BT9;->l:I

    .line 1786891
    iget-object v2, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0c43

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, LX/BT9;->z:I

    .line 1786892
    iget-object v2, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0c40

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, LX/BT9;->A:I

    .line 1786893
    iget-object v2, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0c41

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, LX/BT9;->B:I

    .line 1786894
    iget v2, p0, LX/BT9;->l:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/BT9;->m:I

    .line 1786895
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e()Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->i:Z

    .line 1786896
    iget-object v2, p0, LX/BT9;->e:Landroid/net/Uri;

    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i()Z

    move-result v3

    move-object/from16 v0, p12

    move-object/from16 v1, p13

    invoke-virtual {p2, v2, v0, v3, v1}, LX/BTT;->a(Landroid/net/Uri;Landroid/net/Uri;ZLX/7Sv;)LX/BTS;

    move-result-object v2

    iput-object v2, p0, LX/BT9;->u:LX/BTS;

    .line 1786897
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1786898
    iget-object v3, p0, LX/BT9;->a:Landroid/content/Context;

    iget-object v4, p0, LX/BT9;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1786899
    invoke-static {v2}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;)J

    move-result-wide v4

    iput-wide v4, p0, LX/BT9;->U:J

    .line 1786900
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1786901
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/BT9;->V:Z

    .line 1786902
    iput-object p3, p0, LX/BT9;->n:LX/BTl;

    .line 1786903
    iget-object v2, p0, LX/BT9;->n:LX/BTl;

    invoke-static {v2}, LX/BTY;->a(LX/BTl;)LX/BTX;

    move-result-object v2

    iput-object v2, p0, LX/BT9;->o:LX/BTX;

    .line 1786904
    new-instance v2, LX/BTZ;

    iget v3, p0, LX/BT9;->j:I

    iget v4, p0, LX/BT9;->k:I

    invoke-direct {v2, v3, v4}, LX/BTZ;-><init>(II)V

    iput-object v2, p0, LX/BT9;->p:LX/BTZ;

    .line 1786905
    new-instance v2, LX/BTJ;

    iget v3, p0, LX/BT9;->l:I

    iget v4, p0, LX/BT9;->m:I

    invoke-direct {v2, v3, v4}, LX/BTJ;-><init>(II)V

    iput-object v2, p0, LX/BT9;->q:LX/BTJ;

    .line 1786906
    new-instance v2, LX/BTL;

    invoke-direct {p0}, LX/BT9;->r()LX/BT5;

    move-result-object v3

    iget-object v4, p0, LX/BT9;->o:LX/BTX;

    iget-object v5, p0, LX/BT9;->c:LX/BSr;

    iget-object v6, p0, LX/BT9;->q:LX/BTJ;

    invoke-direct {v2, v3, v4, v5, v6}, LX/BTL;-><init>(LX/BT5;LX/BTX;LX/BSr;LX/BTJ;)V

    iput-object v2, p0, LX/BT9;->r:LX/BTL;

    .line 1786907
    iget-object v2, p0, LX/BT9;->o:LX/BTX;

    iget-object v3, p0, LX/BT9;->q:LX/BTJ;

    iget-object v4, p0, LX/BT9;->p:LX/BTZ;

    invoke-direct {p0}, LX/BT9;->s()LX/BT6;

    move-result-object v5

    invoke-virtual {p5, v2, v3, v4, v5}, LX/BTO;->a(LX/BTX;LX/BTJ;LX/BTZ;LX/BT6;)LX/BTN;

    move-result-object v2

    iput-object v2, p0, LX/BT9;->s:LX/BTN;

    .line 1786908
    iget-object v2, p0, LX/BT9;->n:LX/BTl;

    iget-object v3, p0, LX/BT9;->q:LX/BTJ;

    iget-object v4, p0, LX/BT9;->r:LX/BTL;

    invoke-direct {p0}, LX/BT9;->t()LX/BT7;

    move-result-object v5

    invoke-virtual {p6, v2, v3, v4, v5}, LX/BTQ;->a(LX/BTl;LX/BTJ;LX/BTL;LX/BT7;)LX/BTP;

    move-result-object v2

    iput-object v2, p0, LX/BT9;->t:LX/BTP;

    .line 1786909
    invoke-virtual/range {p17 .. p17}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1786910
    iget-object v2, p0, LX/BT9;->d:LX/0ad;

    sget-short v3, LX/8tG;->k:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->aa:Z

    .line 1786911
    iget-object v2, p0, LX/BT9;->d:LX/0ad;

    sget-short v3, LX/8tG;->j:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->ab:Z

    .line 1786912
    iget-object v2, p0, LX/BT9;->d:LX/0ad;

    sget-short v3, LX/8tG;->f:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    iput-boolean v2, p0, LX/BT9;->ac:Z

    .line 1786913
    :cond_0
    return-void
.end method

.method public static B(LX/BT9;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1786914
    iget-object v0, p0, LX/BT9;->o:LX/BTX;

    iget-object v1, p0, LX/BT9;->t:LX/BTP;

    .line 1786915
    iget-boolean v2, v1, LX/BTP;->h:Z

    move v1, v2

    .line 1786916
    iget-object v2, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v2}, LX/BTJ;->a()I

    move-result v2

    iget-object v3, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v3}, LX/BTJ;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/BTX;->a(ZII)I

    move-result v0

    int-to-long v0, v0

    .line 1786917
    iget-boolean v2, p0, LX/BT9;->ac:Z

    if-eqz v2, :cond_0

    .line 1786918
    invoke-static {v0, v1}, LX/1lQ;->n(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    .line 1786919
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0813f3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, LX/1lQ;->n(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static C(LX/BT9;)Ljava/lang/String;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1786920
    iget-object v0, p0, LX/BT9;->Y:LX/60x;

    if-nez v0, :cond_0

    .line 1786921
    const/4 v0, 0x0

    .line 1786922
    :goto_0
    return-object v0

    .line 1786923
    :cond_0
    iget-object v0, p0, LX/BT9;->x:LX/2Mj;

    iget-object v1, p0, LX/BT9;->Y:LX/60x;

    invoke-virtual {p0}, LX/BT9;->m()I

    move-result v2

    invoke-virtual {p0}, LX/BT9;->n()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/2Mj;->a(LX/60x;II)LX/7Sw;

    move-result-object v0

    .line 1786924
    iget v0, v0, LX/7Sw;->c:I

    int-to-long v0, v0

    .line 1786925
    const/4 v4, 0x0

    .line 1786926
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-lez v5, :cond_1

    .line 1786927
    iget-object v4, p0, LX/BT9;->y:LX/8tH;

    long-to-int v5, v0

    invoke-virtual {v4, v5}, LX/8tH;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 1786928
    :cond_1
    move-object v0, v4

    .line 1786929
    goto :goto_0
.end method

.method public static D(LX/BT9;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1786747
    iget-object v0, p0, LX/BT9;->o:LX/BTX;

    iget-object v1, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v1}, LX/BTL;->e()I

    move-result v1

    iget-object v2, p0, LX/BT9;->t:LX/BTP;

    .line 1786748
    iget-boolean v3, v2, LX/BTP;->h:Z

    move v2, v3

    .line 1786749
    invoke-virtual {v0, v1, v2}, LX/BTX;->a(IZ)I

    move-result v0

    iget-object v1, p0, LX/BT9;->o:LX/BTX;

    iget-object v2, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v2}, LX/BTJ;->a()I

    move-result v2

    iget-object v3, p0, LX/BT9;->t:LX/BTP;

    .line 1786750
    iget-boolean p0, v3, LX/BTP;->h:Z

    move v3, p0

    .line 1786751
    invoke-virtual {v1, v2, v3}, LX/BTX;->a(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 1786752
    invoke-static {v0, v1}, LX/1lQ;->n(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILandroid/view/View;)LX/BT8;
    .locals 2

    .prologue
    .line 1786930
    new-instance v0, LX/BT8;

    iget-object v1, p0, LX/BT9;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, LX/BT8;-><init>(LX/BT9;Landroid/content/Context;I)V

    .line 1786931
    const/4 v1, -0x1

    .line 1786932
    iput v1, v0, LX/0hs;->t:I

    .line 1786933
    invoke-virtual {v0, p2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1786934
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1786935
    const/4 v1, 0x0

    .line 1786936
    iput v1, v0, LX/0hs;->q:I

    .line 1786937
    return-object v0
.end method

.method public static a(LX/BT9;LX/BTI;)Landroid/view/View$OnTouchListener;
    .locals 15

    .prologue
    .line 1786938
    new-instance v0, LX/BTK;

    iget-object v1, p0, LX/BT9;->q:LX/BTJ;

    iget-object v2, p0, LX/BT9;->t:LX/BTP;

    iget-object v3, p0, LX/BT9;->s:LX/BTN;

    iget-object v4, p0, LX/BT9;->o:LX/BTX;

    iget-object v5, p0, LX/BT9;->r:LX/BTL;

    iget-object v6, p0, LX/BT9;->p:LX/BTZ;

    iget v7, p0, LX/BT9;->B:I

    iget v8, p0, LX/BT9;->z:I

    iget v9, p0, LX/BT9;->A:I

    iget-object v10, p0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    iget-object v11, p0, LX/BT9;->H:Landroid/view/View;

    iget-object v12, p0, LX/BT9;->I:Landroid/view/View;

    new-instance v14, LX/BT4;

    invoke-direct/range {v14 .. v16}, LX/BT4;-><init>(LX/BT9;LX/BTI;)V

    move-object/from16 v13, p1

    invoke-direct/range {v0 .. v14}, LX/BTK;-><init>(LX/BTJ;LX/BTP;LX/BTN;LX/BTX;LX/BTL;LX/BTZ;IIILcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;Landroid/view/View;Landroid/view/View;LX/BTI;LX/BT4;)V

    return-object v0
.end method

.method public static a$redex0(LX/BT9;IZ)V
    .locals 2

    .prologue
    .line 1786854
    invoke-static {p0, p1, p2}, LX/BT9;->c(LX/BT9;IZ)V

    .line 1786855
    iget-object v0, p0, LX/BT9;->b:LX/BSr;

    iget-object v1, p0, LX/BT9;->o:LX/BTX;

    invoke-virtual {v1, p1, p2}, LX/BTX;->a(IZ)I

    move-result v1

    invoke-virtual {v0, v1}, LX/BSr;->a(I)V

    .line 1786856
    return-void
.end method

.method public static a$redex0(LX/BT9;LX/0hs;)V
    .locals 2

    .prologue
    .line 1786939
    if-nez p1, :cond_1

    .line 1786940
    :cond_0
    :goto_0
    return-void

    .line 1786941
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1786942
    iget-boolean v1, p0, LX/BT9;->aa:Z

    if-eqz v1, :cond_2

    .line 1786943
    invoke-static {p0}, LX/BT9;->B(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1786944
    :cond_2
    iget-boolean v1, p0, LX/BT9;->aa:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/BT9;->ab:Z

    if-eqz v1, :cond_3

    .line 1786945
    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1786946
    :cond_3
    iget-boolean v1, p0, LX/BT9;->ab:Z

    if-eqz v1, :cond_4

    .line 1786947
    invoke-static {p0}, LX/BT9;->C(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1786948
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1786949
    if-eqz v0, :cond_0

    .line 1786950
    invoke-virtual {p1, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1786951
    invoke-virtual {p1}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public static b$redex0(LX/BT9;IZ)V
    .locals 3

    .prologue
    .line 1786952
    invoke-static {p0, p1, p2}, LX/BT9;->d(LX/BT9;IZ)V

    .line 1786953
    iget-object v0, p0, LX/BT9;->b:LX/BSr;

    iget-object v1, p0, LX/BT9;->o:LX/BTX;

    iget-object v2, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v2}, LX/BTJ;->b()I

    move-result v2

    invoke-virtual {v1, v2, p2}, LX/BTX;->a(IZ)I

    move-result v1

    invoke-virtual {v0, v1}, LX/BSr;->a(I)V

    .line 1786954
    return-void
.end method

.method public static c(LX/BT9;IZ)V
    .locals 1

    .prologue
    .line 1786955
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786956
    iget-object v0, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0, p1, p2}, LX/BTJ;->a(IZ)V

    .line 1786957
    return-void
.end method

.method public static d(LX/BT9;IZ)V
    .locals 1

    .prologue
    .line 1786958
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786959
    iget-object v0, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0, p1, p2}, LX/BTJ;->b(IZ)V

    .line 1786960
    return-void
.end method

.method private r()LX/BT5;
    .locals 1

    .prologue
    .line 1786961
    new-instance v0, LX/BT5;

    invoke-direct {v0, p0}, LX/BT5;-><init>(LX/BT9;)V

    return-object v0
.end method

.method private s()LX/BT6;
    .locals 1

    .prologue
    .line 1786962
    new-instance v0, LX/BT6;

    invoke-direct {v0, p0}, LX/BT6;-><init>(LX/BT9;)V

    return-object v0
.end method

.method private t()LX/BT7;
    .locals 1

    .prologue
    .line 1786963
    new-instance v0, LX/BT7;

    invoke-direct {v0, p0}, LX/BT7;-><init>(LX/BT9;)V

    return-object v0
.end method

.method public static u$redex0(LX/BT9;)V
    .locals 2

    .prologue
    .line 1786964
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786965
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    iget-object v1, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v1}, LX/BTJ;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BTL;->a(I)V

    .line 1786966
    invoke-static {p0}, LX/BT9;->y(LX/BT9;)V

    .line 1786967
    return-void
.end method

.method public static w(LX/BT9;)V
    .locals 9

    .prologue
    .line 1786857
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    .line 1786858
    iget-object v1, v0, LX/BTL;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1786859
    if-nez v0, :cond_0

    .line 1786860
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    iget-wide v2, p0, LX/BT9;->U:J

    iget-object v1, p0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutStripContentWidth()I

    move-result v1

    .line 1786861
    const-wide/16 v4, 0x3

    mul-long/2addr v4, v2

    int-to-long v6, v1

    div-long/2addr v4, v6

    long-to-int v4, v4

    .line 1786862
    new-instance v5, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;

    invoke-direct {v5, v0, v4}, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;-><init>(LX/BTL;I)V

    iput-object v5, v0, LX/BTL;->a:Ljava/lang/Runnable;

    .line 1786863
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static y(LX/BT9;)V
    .locals 2

    .prologue
    .line 1786758
    iget-object v0, p0, LX/BT9;->b:LX/BSr;

    .line 1786759
    iget-object v1, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->c()V

    .line 1786760
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->b()V

    .line 1786761
    return-void
.end method

.method public static z(LX/BT9;)V
    .locals 2

    .prologue
    .line 1786753
    iget-boolean v0, p0, LX/BT9;->ac:Z

    if-nez v0, :cond_0

    .line 1786754
    :goto_0
    return-void

    .line 1786755
    :cond_0
    iget-object v0, p0, LX/BT9;->Q:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0}, LX/BT9;->C(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1786756
    iget-object v0, p0, LX/BT9;->R:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0}, LX/BT9;->B(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1786757
    iget-object v0, p0, LX/BT9;->a:Landroid/content/Context;

    const v1, 0x7f0813e2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1786762
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setVisibility(I)V

    .line 1786763
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1786764
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1786765
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786766
    const/4 v0, 0x1

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1786767
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1786768
    iget-object v0, p0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1786769
    invoke-static {p0}, LX/BT9;->w(LX/BT9;)V

    .line 1786770
    invoke-static {p0}, LX/BT9;->u$redex0(LX/BT9;)V

    .line 1786771
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1786772
    iget-object v0, p0, LX/BT9;->u:LX/BTS;

    .line 1786773
    const/4 v1, 0x0

    iput-object v1, v0, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786774
    iget-object v1, v0, LX/BTS;->c:LX/0TD;

    new-instance v2, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$1;

    invoke-direct {v2, v0}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$1;-><init>(LX/BTS;)V

    const v3, -0xdf055af

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1786775
    invoke-static {v0}, LX/BTS;->f(LX/BTS;)V

    .line 1786776
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786777
    return-void
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1786778
    iget-boolean v0, p0, LX/BT9;->X:Z

    if-eqz v0, :cond_1

    .line 1786779
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setVisibility(I)V

    .line 1786780
    iget-object v0, p0, LX/BT9;->O:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1786781
    iget-object v0, p0, LX/BT9;->O:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1786782
    :cond_0
    :goto_0
    return-void

    .line 1786783
    :cond_1
    iget-object v0, p0, LX/BT9;->D:Landroid/view/View;

    if-nez v0, :cond_2

    .line 1786784
    iget-object v0, p0, LX/BT9;->C:Landroid/view/ViewStub;

    const v1, 0x7f031590

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1786785
    iget-object v0, p0, LX/BT9;->C:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BT9;->D:Landroid/view/View;

    .line 1786786
    :cond_2
    iget-boolean v0, p0, LX/BT9;->g:Z

    if-eqz v0, :cond_4

    .line 1786787
    iget-object v0, p0, LX/BT9;->D:Landroid/view/View;

    .line 1786788
    iget-object v1, p0, LX/BT9;->E:Landroid/widget/ImageView;

    if-nez v1, :cond_3

    .line 1786789
    const v1, 0x7f0d309f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, LX/BT9;->E:Landroid/widget/ImageView;

    .line 1786790
    iget-object v1, p0, LX/BT9;->E:Landroid/widget/ImageView;

    new-instance v4, LX/BT1;

    invoke-direct {v4, p0}, LX/BT1;-><init>(LX/BT9;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1786791
    :cond_3
    iget-object v1, p0, LX/BT9;->E:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1786792
    :cond_4
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786793
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    move-object v0, v1

    .line 1786794
    iput-object v0, p0, LX/BT9;->G:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786795
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786796
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->b:Landroid/view/View;

    move-object v0, v1

    .line 1786797
    iput-object v0, p0, LX/BT9;->H:Landroid/view/View;

    .line 1786798
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786799
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->c:Landroid/view/View;

    move-object v0, v1

    .line 1786800
    iput-object v0, p0, LX/BT9;->I:Landroid/view/View;

    .line 1786801
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786802
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->f:Landroid/view/View;

    move-object v0, v1

    .line 1786803
    iput-object v0, p0, LX/BT9;->J:Landroid/view/View;

    .line 1786804
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786805
    iget-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->g:Landroid/view/View;

    move-object v0, v1

    .line 1786806
    iput-object v0, p0, LX/BT9;->K:Landroid/view/View;

    .line 1786807
    iget-object v0, p0, LX/BT9;->F:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/BSz;

    invoke-direct {v1, p0}, LX/BSz;-><init>(LX/BT9;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1786808
    iget-boolean v0, p0, LX/BT9;->aa:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, LX/BT9;->ab:Z

    if-eqz v0, :cond_6

    .line 1786809
    :cond_5
    iget-object v0, p0, LX/BT9;->H:Landroid/view/View;

    invoke-direct {p0, v3, v0}, LX/BT9;->a(ILandroid/view/View;)LX/BT8;

    move-result-object v0

    iput-object v0, p0, LX/BT9;->L:LX/BT8;

    .line 1786810
    iget-object v0, p0, LX/BT9;->I:Landroid/view/View;

    invoke-direct {p0, v3, v0}, LX/BT9;->a(ILandroid/view/View;)LX/BT8;

    move-result-object v0

    iput-object v0, p0, LX/BT9;->M:LX/BT8;

    .line 1786811
    iget-object v0, p0, LX/BT9;->K:Landroid/view/View;

    invoke-direct {p0, v2, v0}, LX/BT9;->a(ILandroid/view/View;)LX/BT8;

    move-result-object v0

    iput-object v0, p0, LX/BT9;->N:LX/BT8;

    .line 1786812
    :cond_6
    iget-boolean v0, p0, LX/BT9;->ac:Z

    if-eqz v0, :cond_7

    .line 1786813
    iget-object v0, p0, LX/BT9;->P:Landroid/view/ViewStub;

    const v1, 0x7f031589

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1786814
    iget-object v0, p0, LX/BT9;->P:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BT9;->O:Landroid/view/View;

    .line 1786815
    iget-object v0, p0, LX/BT9;->O:Landroid/view/View;

    const v1, 0x7f0d308a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/BT9;->Q:Lcom/facebook/resources/ui/FbTextView;

    .line 1786816
    iget-object v0, p0, LX/BT9;->O:Landroid/view/View;

    const v1, 0x7f0d308b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/BT9;->R:Lcom/facebook/resources/ui/FbTextView;

    .line 1786817
    iget-object v0, p0, LX/BT9;->S:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v1, p0, LX/BT9;->O:Landroid/view/View;

    .line 1786818
    iput-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    .line 1786819
    :cond_7
    iput-boolean v2, p0, LX/BT9;->X:Z

    goto/16 :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1786820
    iget-object v0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786821
    iget-object v0, p0, LX/BT9;->E:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1786822
    iget-object v0, p0, LX/BT9;->E:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1786823
    :cond_0
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1786824
    sget-object v0, LX/5SK;->TRIM:LX/5SK;

    return-object v0
.end method

.method public final k()V
    .locals 7

    .prologue
    .line 1786825
    iget-object v0, p0, LX/BT9;->v:LX/BTG;

    iget-object v1, p0, LX/BT9;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BT9;->f:Ljava/lang/String;

    iget-object v3, p0, LX/BT9;->w:Ljava/lang/String;

    iget-object v4, p0, LX/BT9;->W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    .line 1786826
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "video_editing_handle_interactions"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "video_editing_module"

    .line 1786827
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1786828
    move-object v5, v5

    .line 1786829
    sget-object v6, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->LEFT_HANDLE_MOVES:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    sget-object p0, LX/BTI;->LEFT:LX/BTI;

    invoke-virtual {v4, p0}, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c(LX/BTI;)I

    move-result p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->RIGHT_HANDLE_MOVES:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    sget-object p0, LX/BTI;->RIGHT:LX/BTI;

    invoke-virtual {v4, p0}, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c(LX/BTI;)I

    move-result p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1786830
    iget-object v6, v0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1786831
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "video_editing_zoom_interactions"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "video_editing_module"

    .line 1786832
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1786833
    move-object v5, v5

    .line 1786834
    sget-object v6, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->LEFT_HANDLE_ZOOM_INS:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    sget-object p0, LX/BTI;->LEFT:LX/BTI;

    invoke-virtual {v4, p0}, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d(LX/BTI;)I

    move-result p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->RIGHT_HANDLE_ZOOM_INS:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    sget-object p0, LX/BTI;->RIGHT:LX/BTI;

    invoke-virtual {v4, p0}, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d(LX/BTI;)I

    move-result p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1786835
    iget-object v6, v0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1786836
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "video_editing_scrubber_interactions"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "video_editing_module"

    .line 1786837
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1786838
    move-object v5, v5

    .line 1786839
    sget-object v6, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v6}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    sget-object v6, LX/BTE;->SCRUBBER_MOVES:LX/BTE;

    invoke-virtual {v6}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v6

    .line 1786840
    iget p0, v4, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->e:I

    move p0, p0

    .line 1786841
    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1786842
    iget-object v6, v0, LX/BTG;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1786843
    return-void
.end method

.method public final m()I
    .locals 3

    .prologue
    .line 1786844
    iget-boolean v0, p0, LX/BT9;->Z:Z

    if-nez v0, :cond_0

    .line 1786845
    const/4 v0, -0x1

    .line 1786846
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BT9;->o:LX/BTX;

    iget-object v1, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v1}, LX/BTJ;->a()I

    move-result v1

    iget-object v2, p0, LX/BT9;->t:LX/BTP;

    .line 1786847
    iget-boolean p0, v2, LX/BTP;->h:Z

    move v2, p0

    .line 1786848
    invoke-virtual {v0, v1, v2}, LX/BTX;->a(IZ)I

    move-result v0

    goto :goto_0
.end method

.method public final n()I
    .locals 3

    .prologue
    .line 1786849
    iget-boolean v0, p0, LX/BT9;->Z:Z

    if-nez v0, :cond_0

    .line 1786850
    const/4 v0, -0x1

    .line 1786851
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BT9;->o:LX/BTX;

    iget-object v1, p0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v1}, LX/BTJ;->b()I

    move-result v1

    iget-object v2, p0, LX/BT9;->t:LX/BTP;

    .line 1786852
    iget-boolean p0, v2, LX/BTP;->h:Z

    move v2, p0

    .line 1786853
    invoke-virtual {v0, v1, v2}, LX/BTX;->a(IZ)I

    move-result v0

    goto :goto_0
.end method
