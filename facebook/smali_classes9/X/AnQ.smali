.class public LX/AnQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/AnQ;


# instance fields
.field private final a:LX/0bH;

.field private final b:LX/189;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2KR;


# direct methods
.method public constructor <init>(LX/0bH;LX/189;LX/0Ot;LX/0Or;LX/2KR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "LX/189;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/2KR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712417
    iput-object p1, p0, LX/AnQ;->a:LX/0bH;

    .line 1712418
    iput-object p2, p0, LX/AnQ;->b:LX/189;

    .line 1712419
    iput-object p3, p0, LX/AnQ;->c:LX/0Ot;

    .line 1712420
    iput-object p4, p0, LX/AnQ;->d:LX/0Or;

    .line 1712421
    iput-object p5, p0, LX/AnQ;->e:LX/2KR;

    .line 1712422
    return-void
.end method

.method public static a(LX/0QB;)LX/AnQ;
    .locals 9

    .prologue
    .line 1712423
    sget-object v0, LX/AnQ;->f:LX/AnQ;

    if-nez v0, :cond_1

    .line 1712424
    const-class v1, LX/AnQ;

    monitor-enter v1

    .line 1712425
    :try_start_0
    sget-object v0, LX/AnQ;->f:LX/AnQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1712426
    if-eqz v2, :cond_0

    .line 1712427
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1712428
    new-instance v3, LX/AnQ;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    const/16 v6, 0x115

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x123

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2KR;->a(LX/0QB;)LX/2KR;

    move-result-object v8

    check-cast v8, LX/2KR;

    invoke-direct/range {v3 .. v8}, LX/AnQ;-><init>(LX/0bH;LX/189;LX/0Ot;LX/0Or;LX/2KR;)V

    .line 1712429
    move-object v0, v3

    .line 1712430
    sput-object v0, LX/AnQ;->f:LX/AnQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712431
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1712432
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1712433
    :cond_1
    sget-object v0, LX/AnQ;->f:LX/AnQ;

    return-object v0

    .line 1712434
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1712435
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 1712436
    if-eqz p1, :cond_2

    .line 1712437
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1712438
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, v0

    .line 1712439
    :goto_0
    sget-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    if-ne p2, v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1712440
    if-nez v0, :cond_0

    .line 1712441
    sget-object v0, LX/1EO;->GOOD_FRIENDS:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 1712442
    if-eqz v0, :cond_4

    .line 1712443
    :cond_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1712444
    :goto_2
    iget-object v0, p0, LX/AnQ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-virtual {v0, p1}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1712445
    if-eqz v6, :cond_1

    .line 1712446
    iget-object v0, p0, LX/AnQ;->e:LX/2KR;

    .line 1712447
    const-string v1, "like_main"

    invoke-static {v0, v1}, LX/2KR;->b$redex0(LX/2KR;Ljava/lang/String;)V

    .line 1712448
    :cond_1
    :goto_3
    return-void

    :cond_2
    move-object v2, v4

    .line 1712449
    goto :goto_0

    .line 1712450
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 1712451
    :cond_4
    iget-object v0, p0, LX/AnQ;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1712452
    iget-object v1, p0, LX/AnQ;->b:LX/189;

    invoke-virtual {v1, p1, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1712453
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1712454
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1712455
    invoke-static {v1}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 1712456
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    .line 1712457
    iget-object v8, p0, LX/AnQ;->a:LX/0bH;

    new-instance v0, LX/1Zj;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_5

    invoke-interface {v5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    :cond_5
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v5

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/1Zj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v8, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method
