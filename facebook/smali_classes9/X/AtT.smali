.class public LX/AtT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1721515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721516
    return-void
.end method

.method public static a(IIII)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1721517
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    .line 1721518
    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 1721519
    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1721520
    int-to-float v0, p2

    int-to-float v1, p0

    div-float/2addr v0, v1

    .line 1721521
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1721522
    div-int/lit8 v1, p3, 0x2

    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 1721523
    div-int/lit8 v2, p3, 0x2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 1721524
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setLeft(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setTop(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setRight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setBottom(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    .line 1721525
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setLeft(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setTop(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setRight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setBottom(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    goto :goto_0
.end method
