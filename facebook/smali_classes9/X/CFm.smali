.class public final LX/CFm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field private f:Ljava/lang/String;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/greetingcards/verve/model/VMColor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1864057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMSlide;)LX/CFm;
    .locals 1

    .prologue
    .line 1864048
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    iput-object v0, p0, LX/CFm;->a:Ljava/lang/String;

    .line 1864049
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->size:LX/0Px;

    iput-object v0, p0, LX/CFm;->b:LX/0Px;

    .line 1864050
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    iput-object v0, p0, LX/CFm;->c:LX/0Px;

    .line 1864051
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    iput-object v0, p0, LX/CFm;->d:LX/0P1;

    .line 1864052
    iget-boolean v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    iput-boolean v0, p0, LX/CFm;->e:Z

    .line 1864053
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    iput-object v0, p0, LX/CFm;->f:Ljava/lang/String;

    .line 1864054
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    iput-object v0, p0, LX/CFm;->g:LX/0Px;

    .line 1864055
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object v0, p0, LX/CFm;->h:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864056
    return-object p0
.end method

.method public final a()Lcom/facebook/greetingcards/verve/model/VMSlide;
    .locals 9

    .prologue
    .line 1864047
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    iget-object v1, p0, LX/CFm;->a:Ljava/lang/String;

    iget-object v2, p0, LX/CFm;->b:LX/0Px;

    iget-object v3, p0, LX/CFm;->c:LX/0Px;

    iget-object v4, p0, LX/CFm;->d:LX/0P1;

    iget-boolean v5, p0, LX/CFm;->e:Z

    iget-object v6, p0, LX/CFm;->f:Ljava/lang/String;

    iget-object v7, p0, LX/CFm;->g:LX/0Px;

    iget-object v8, p0, LX/CFm;->h:Lcom/facebook/greetingcards/verve/model/VMColor;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/greetingcards/verve/model/VMSlide;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0P1;ZLjava/lang/String;LX/0Px;Lcom/facebook/greetingcards/verve/model/VMColor;)V

    return-object v0
.end method
