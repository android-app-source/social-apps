.class public LX/C8C;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1852530
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1852531
    const v0, 0x7f03062e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1852532
    const v0, 0x7f0d1115

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C8C;->a:Landroid/widget/TextView;

    .line 1852533
    const v0, 0x7f0d1114

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/C8C;->b:Landroid/widget/ImageView;

    .line 1852534
    const v0, 0x7f0d1113

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/C8C;->c:Landroid/widget/ProgressBar;

    .line 1852535
    return-void
.end method


# virtual methods
.method public final a(Z)LX/C8C;
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1852536
    iget-object v3, p0, LX/C8C;->c:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1852537
    iget-object v0, p0, LX/C8C;->b:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1852538
    return-object p0

    :cond_0
    move v0, v2

    .line 1852539
    goto :goto_0

    :cond_1
    move v2, v1

    .line 1852540
    goto :goto_1
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1852541
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setEnabled(Z)V

    .line 1852542
    iget-object v0, p0, LX/C8C;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1852543
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1852544
    iget-object v0, p0, LX/C8C;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1852545
    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .prologue
    .line 1852546
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setSelected(Z)V

    .line 1852547
    invoke-virtual {p0, p1}, LX/C8C;->a(Z)LX/C8C;

    .line 1852548
    return-void
.end method
