.class public LX/CWL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CWI;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/23P;

.field private final c:Ljava/util/Calendar;

.field private final d:Ljava/text/SimpleDateFormat;

.field private final e:Ljava/text/SimpleDateFormat;

.field private final f:Ljava/text/SimpleDateFormat;

.field private final g:Ljava/text/SimpleDateFormat;

.field public final h:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(LX/0Or;LX/23P;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/23P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1907938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1907939
    iput-object p1, p0, LX/CWL;->a:LX/0Or;

    .line 1907940
    iput-object p2, p0, LX/CWL;->b:LX/23P;

    .line 1907941
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    .line 1907942
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd"

    iget-object v0, p0, LX/CWL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWL;->d:Ljava/text/SimpleDateFormat;

    .line 1907943
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "d"

    iget-object v0, p0, LX/CWL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWL;->f:Ljava/text/SimpleDateFormat;

    .line 1907944
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM"

    iget-object v0, p0, LX/CWL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWL;->g:Ljava/text/SimpleDateFormat;

    .line 1907945
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMM"

    iget-object v0, p0, LX/CWL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    .line 1907946
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE"

    iget-object v0, p0, LX/CWL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, LX/CWL;->e:Ljava/text/SimpleDateFormat;

    .line 1907947
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1907937
    iget-object v0, p0, LX/CWL;->b:LX/23P;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/CWF;ILjava/util/Date;)LX/CWF;
    .locals 6

    .prologue
    .line 1907904
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907905
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1907906
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1907907
    iget-object v0, p1, LX/CWF;->d:[LX/CWE;

    array-length v2, v0

    .line 1907908
    new-array v3, v2, [LX/CWE;

    .line 1907909
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    .line 1907910
    add-int v4, p2, v1

    if-ne v0, v4, :cond_0

    .line 1907911
    sget-object v4, LX/CWE;->SELECTED:LX/CWE;

    aput-object v4, v3, v0

    .line 1907912
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1907913
    :cond_0
    iget-object v4, p1, LX/CWF;->d:[LX/CWE;

    aget-object v4, v4, v0

    sget-object v5, LX/CWE;->UNAVAILABLE:LX/CWE;

    invoke-virtual {v4, v5}, LX/CWE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1907914
    sget-object v4, LX/CWE;->UNAVAILABLE:LX/CWE;

    aput-object v4, v3, v0

    goto :goto_1

    .line 1907915
    :cond_1
    sget-object v4, LX/CWE;->AVAILABLE:LX/CWE;

    aput-object v4, v3, v0

    goto :goto_1

    .line 1907916
    :cond_2
    new-instance v0, LX/CWG;

    invoke-direct {v0}, LX/CWG;-><init>()V

    iget-object v1, p1, LX/CWF;->e:Ljava/lang/String;

    .line 1907917
    iput-object v1, v0, LX/CWG;->e:Ljava/lang/String;

    .line 1907918
    move-object v0, v0

    .line 1907919
    iget-boolean v1, p1, LX/CWF;->g:Z

    .line 1907920
    iput-boolean v1, v0, LX/CWG;->g:Z

    .line 1907921
    move-object v0, v0

    .line 1907922
    iget-boolean v1, p1, LX/CWF;->f:Z

    .line 1907923
    iput-boolean v1, v0, LX/CWG;->f:Z

    .line 1907924
    move-object v0, v0

    .line 1907925
    iget-object v1, p1, LX/CWF;->a:[Ljava/lang/String;

    .line 1907926
    iput-object v1, v0, LX/CWG;->a:[Ljava/lang/String;

    .line 1907927
    move-object v0, v0

    .line 1907928
    iget-object v1, p1, LX/CWF;->b:[Ljava/lang/String;

    .line 1907929
    iput-object v1, v0, LX/CWG;->b:[Ljava/lang/String;

    .line 1907930
    move-object v0, v0

    .line 1907931
    iget-object v1, p1, LX/CWF;->c:[Ljava/lang/String;

    .line 1907932
    iput-object v1, v0, LX/CWG;->c:[Ljava/lang/String;

    .line 1907933
    move-object v0, v0

    .line 1907934
    iput-object v3, v0, LX/CWG;->d:[LX/CWE;

    .line 1907935
    move-object v0, v0

    .line 1907936
    invoke-virtual {v0}, LX/CWG;->a()LX/CWF;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Date;Ljava/util/TreeMap;Ljava/util/Date;Ljava/util/Date;)LX/CWF;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/CU5;",
            ">;>;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "LX/CWF;"
        }
    .end annotation

    .prologue
    .line 1907854
    new-instance v1, LX/CWG;

    invoke-direct {v1}, LX/CWG;-><init>()V

    .line 1907855
    iget-object v0, p0, LX/CWL;->g:Ljava/text/SimpleDateFormat;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/CWL;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1907856
    iput-object v0, v1, LX/CWG;->e:Ljava/lang/String;

    .line 1907857
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907858
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x7

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1907859
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    .line 1907860
    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    const-wide/16 v6, 0x7

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 1907861
    iget-object v3, p0, LX/CWL;->e:Ljava/text/SimpleDateFormat;

    iget-object v4, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/CWL;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1907862
    iget-object v3, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 1907863
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1907864
    :cond_0
    iput-object v2, v1, LX/CWG;->a:[Ljava/lang/String;

    .line 1907865
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907866
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1907867
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 1907868
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    .line 1907869
    add-int v0, v2, v3

    .line 1907870
    new-array v4, v0, [Ljava/lang/String;

    .line 1907871
    new-array v5, v0, [Ljava/lang/String;

    .line 1907872
    new-array v6, v0, [LX/CWE;

    .line 1907873
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    .line 1907874
    const-string v7, ""

    aput-object v7, v4, v0

    .line 1907875
    const-string v7, ""

    aput-object v7, v5, v0

    .line 1907876
    sget-object v7, LX/CWE;->UNAVAILABLE:LX/CWE;

    aput-object v7, v6, v0

    .line 1907877
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1907878
    :cond_1
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907879
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v7, 0x5

    const/4 v8, 0x1

    invoke-virtual {v0, v7, v8}, Ljava/util/Calendar;->set(II)V

    .line 1907880
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_3

    .line 1907881
    add-int v7, v2, v0

    iget-object v8, p0, LX/CWL;->f:Ljava/text/SimpleDateFormat;

    iget-object v9, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v9}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 1907882
    add-int v7, v2, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    .line 1907883
    iget-object v7, p0, LX/CWL;->d:Ljava/text/SimpleDateFormat;

    iget-object v8, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1907884
    add-int v7, v2, v0

    sget-object v8, LX/CWE;->AVAILABLE:LX/CWE;

    aput-object v8, v6, v7

    .line 1907885
    :goto_3
    iget-object v7, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->add(II)V

    .line 1907886
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1907887
    :cond_2
    add-int v7, v2, v0

    sget-object v8, LX/CWE;->UNAVAILABLE:LX/CWE;

    aput-object v8, v6, v7

    goto :goto_3

    .line 1907888
    :cond_3
    iput-object v4, v1, LX/CWG;->b:[Ljava/lang/String;

    .line 1907889
    move-object v0, v1

    .line 1907890
    iput-object v5, v0, LX/CWG;->c:[Ljava/lang/String;

    .line 1907891
    move-object v0, v0

    .line 1907892
    iput-object v6, v0, LX/CWG;->d:[LX/CWE;

    .line 1907893
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907894
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x2

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1907895
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const/4 v0, 0x1

    .line 1907896
    :goto_4
    iput-boolean v0, v1, LX/CWG;->f:Z

    .line 1907897
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907898
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1907899
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    const/4 v0, 0x1

    .line 1907900
    :goto_5
    iput-boolean v0, v1, LX/CWG;->g:Z

    .line 1907901
    invoke-virtual {v1}, LX/CWG;->a()LX/CWF;

    move-result-object v0

    return-object v0

    .line 1907902
    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    .line 1907903
    :cond_7
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1907822
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907823
    sget-object v0, LX/CWK;->a:[I

    invoke-virtual {p2}, LX/CWH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1907824
    const/4 v0, 0x0

    const-string v1, "Unsupported calendar shift direction"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1907825
    const/4 v0, 0x0

    .line 1907826
    :goto_0
    return-object v0

    .line 1907827
    :pswitch_0
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 1907828
    :goto_1
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1907829
    :pswitch_1
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;
    .locals 3

    .prologue
    .line 1907845
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907846
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1907847
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 1907848
    if-eqz p2, :cond_1

    .line 1907849
    :goto_0
    sget-object v1, LX/CWH;->FORWARD:LX/CWH;

    invoke-virtual {p0, v0, v1}, LX/CWL;->a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object v1

    .line 1907850
    invoke-virtual {p2, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iget-object p1, p0, LX/CWL;->h:Ljava/text/SimpleDateFormat;

    invoke-virtual {p1, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 1907851
    if-eqz v1, :cond_1

    .line 1907852
    sget-object v1, LX/CWH;->FORWARD:LX/CWH;

    invoke-virtual {p0, v0, v1}, LX/CWL;->a(Ljava/util/Date;LX/CWH;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1907853
    :cond_1
    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/util/Date;Ljava/util/Date;)I
    .locals 2

    .prologue
    .line 1907843
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907844
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final b(Ljava/util/Date;LX/CWH;)Ljava/util/Date;
    .locals 6

    .prologue
    const/4 v2, 0x7

    const/4 v5, 0x5

    const/4 v1, 0x4

    const/4 v4, 0x2

    .line 1907830
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1907831
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1907832
    iget-object v1, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1907833
    sget-object v2, LX/CWK;->a:[I

    invoke-virtual {p2}, LX/CWH;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1907834
    const/4 v0, 0x0

    const-string v1, "Unsupported calendar shift direction"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1907835
    const/4 v0, 0x0

    .line 1907836
    :goto_0
    return-object v0

    .line 1907837
    :pswitch_0
    iget-object v2, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    .line 1907838
    :goto_1
    :try_start_0
    iget-object v2, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 1907839
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1907840
    :goto_2
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1907841
    :pswitch_1
    iget-object v2, p0, LX/CWL;->c:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    goto :goto_1

    .line 1907842
    :catch_0
    iget-object v0, p0, LX/CWL;->c:Ljava/util/Calendar;

    iget-object v1, p0, LX/CWL;->c:Ljava/util/Calendar;

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
