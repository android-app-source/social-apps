.class public LX/CX8;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;
.implements LX/BcB;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroid/widget/ImageButton;

.field private d:LX/Bc8;

.field public e:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909542
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CX8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909543
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1909507
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909508
    const p1, 0x7f030f9d

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909509
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/CX8;->setClickable(Z)V

    .line 1909510
    const p1, 0x7f0d032f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, LX/CX8;->c:Landroid/widget/ImageButton;

    .line 1909511
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {p0, p1}, LX/CSi;->b(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iput-object p1, p0, LX/CX8;->e:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    .line 1909512
    iget-object p1, p0, LX/CX8;->e:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {p0, p1}, LX/CX8;->addView(Landroid/view/View;)V

    .line 1909513
    const/16 p1, 0x8

    invoke-virtual {p0, p1}, LX/CX8;->setVisibility(I)V

    .line 1909514
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1909541
    iget-boolean v0, p0, LX/CX8;->a:Z

    return v0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1909537
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1909538
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1909539
    iget-boolean v0, p0, LX/CX8;->a:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 1909540
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1909532
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1909533
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1909534
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 1909535
    iget-boolean v0, p0, LX/CX8;->a:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 1909536
    return-void
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1909530
    invoke-virtual {p0}, LX/CX8;->toggle()V

    .line 1909531
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1909520
    iget-boolean v0, p0, LX/CX8;->a:Z

    if-eq v0, p1, :cond_0

    .line 1909521
    iput-boolean p1, p0, LX/CX8;->a:Z

    .line 1909522
    iget-object v0, p0, LX/CX8;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1909523
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->refreshDrawableState()V

    .line 1909524
    :cond_0
    iget-boolean v0, p0, LX/CX8;->b:Z

    if-eqz v0, :cond_1

    .line 1909525
    :goto_0
    return-void

    .line 1909526
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CX8;->b:Z

    .line 1909527
    iget-object v0, p0, LX/CX8;->d:LX/Bc8;

    if-eqz v0, :cond_2

    .line 1909528
    iget-object v0, p0, LX/CX8;->d:LX/Bc8;

    invoke-virtual {v0, p0}, LX/Bc8;->a(Landroid/view/View;)V

    .line 1909529
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CX8;->b:Z

    goto :goto_0
.end method

.method public setOnCheckedChangeWidgetListener(LX/Bc8;)V
    .locals 0

    .prologue
    .line 1909518
    iput-object p1, p0, LX/CX8;->d:LX/Bc8;

    .line 1909519
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1909515
    iget-boolean v0, p0, LX/CX8;->a:Z

    if-nez v0, :cond_0

    .line 1909516
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/CX8;->setChecked(Z)V

    .line 1909517
    :cond_0
    return-void
.end method
