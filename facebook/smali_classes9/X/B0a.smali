.class public LX/B0a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0U;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/B0V;

.field public final c:Ljava/lang/String;

.field public final d:LX/B0V;

.field private final e:J

.field private final f:Z

.field private final g:Z


# direct methods
.method public constructor <init>(LX/B0V;LX/B0V;JZZ)V
    .locals 9

    .prologue
    .line 1734048
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, LX/B0a;-><init>(LX/B0V;Ljava/lang/String;LX/B0V;JZZ)V

    .line 1734049
    return-void
.end method

.method private constructor <init>(LX/B0V;Ljava/lang/String;LX/B0V;JZZ)V
    .locals 4

    .prologue
    .line 1734003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734004
    iput-object p1, p0, LX/B0a;->b:LX/B0V;

    .line 1734005
    iput-object p2, p0, LX/B0a;->c:Ljava/lang/String;

    .line 1734006
    iput-object p3, p0, LX/B0a;->d:LX/B0V;

    .line 1734007
    iput-wide p4, p0, LX/B0a;->e:J

    .line 1734008
    iput-boolean p6, p0, LX/B0a;->f:Z

    .line 1734009
    iput-boolean p7, p0, LX/B0a;->g:Z

    .line 1734010
    const-string v0, "SequentialBatchConfiguration:%s:%s"

    iget-object v1, p0, LX/B0a;->b:LX/B0V;

    invoke-virtual {v1}, LX/B0V;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B0a;->d:LX/B0V;

    invoke-virtual {v2}, LX/B0V;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B0a;->a:Ljava/lang/String;

    .line 1734011
    return-void
.end method

.method public constructor <init>(LX/B0V;Ljava/lang/String;LX/B0Z;JZZ)V
    .locals 0

    .prologue
    .line 1734046
    invoke-direct/range {p0 .. p7}, LX/B0a;-><init>(LX/B0V;Ljava/lang/String;LX/B0V;JZZ)V

    .line 1734047
    return-void
.end method

.method public static a(LX/B0a;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;LX/0v6;LX/0zO;)V
    .locals 2

    .prologue
    .line 1734044
    invoke-virtual {p5, p6}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0zX;->b(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/B0Y;

    invoke-direct {v1, p0, p4, p1, p2}, LX/B0Y;-><init>(LX/B0a;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 1734045
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1734050
    iget-wide v0, p0, LX/B0a;->e:J

    return-wide v0
.end method

.method public final a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;)LX/0v6;
    .locals 11

    .prologue
    .line 1734016
    iget-object v0, p4, LX/B0d;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1734017
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 1734018
    iget-object v1, p0, LX/B0a;->b:LX/B0V;

    invoke-static {p1, v1, p4}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;)LX/0zO;

    move-result-object v8

    .line 1734019
    iget-object v1, v8, LX/0zO;->a:LX/0zS;

    move-object v1, v1

    .line 1734020
    sget-object v3, LX/0zS;->c:LX/0zS;

    if-ne v1, v3, :cond_2

    move v1, v7

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1734021
    new-instance v9, LX/0v6;

    iget-object v1, p0, LX/B0a;->a:Ljava/lang/String;

    invoke-direct {v9, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1734022
    sget-object v1, LX/0vU;->PHASED:LX/0vU;

    .line 1734023
    iput-object v1, v9, LX/0v6;->j:LX/0vU;

    .line 1734024
    iget-object v1, p0, LX/B0a;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/B0a;->d:LX/B0V;

    instance-of v1, v1, LX/B0Z;

    if-eqz v1, :cond_3

    move v6, v7

    .line 1734025
    :goto_1
    invoke-virtual {v9, v8}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v10

    new-instance v1, LX/B0X;

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, LX/B0X;-><init>(LX/B0a;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Z)V

    invoke-virtual {v10, v1}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 1734026
    if-eqz v6, :cond_0

    .line 1734027
    new-instance v5, LX/B0d;

    iget-object v1, p4, LX/B0d;->a:Ljava/lang/String;

    const/16 v2, 0x100

    invoke-static {v1, v2}, LX/2nU;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v5, v1, v2, v7}, LX/B0d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1734028
    iget-object v1, p0, LX/B0a;->d:LX/B0V;

    check-cast v1, LX/B0Z;

    iget-object v2, p0, LX/B0a;->c:Ljava/lang/String;

    .line 1734029
    sget-object v3, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v4, LX/4a0;->NOT_SET:LX/4a0;

    invoke-virtual {v8, v2, v3, v4}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v3

    move-object v2, v3

    .line 1734030
    invoke-static {p1, v1, v2}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0Z;LX/4a1;)LX/0zO;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, v9

    .line 1734031
    invoke-static/range {v1 .. v7}, LX/B0a;->a(LX/B0a;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;LX/0v6;LX/0zO;)V

    .line 1734032
    :cond_0
    move-object v0, v9

    .line 1734033
    :goto_2
    return-object v0

    .line 1734034
    :cond_1
    new-instance v6, LX/0v6;

    iget-object v1, p0, LX/B0a;->a:Ljava/lang/String;

    invoke-direct {v6, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1734035
    iget-object v1, p0, LX/B0a;->d:LX/B0V;

    invoke-static {p1, v1, p4}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;)LX/0zO;

    move-result-object v7

    .line 1734036
    iget-object v1, v7, LX/0zO;->a:LX/0zS;

    move-object v1, v1

    .line 1734037
    sget-object v2, LX/0zS;->c:LX/0zS;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 1734038
    invoke-static/range {v1 .. v7}, LX/B0a;->a(LX/B0a;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;LX/0v6;LX/0zO;)V

    .line 1734039
    move-object v0, v6

    .line 1734040
    goto :goto_2

    :cond_2
    move v1, v2

    .line 1734041
    goto :goto_0

    :cond_3
    move v6, v2

    .line 1734042
    goto :goto_1

    .line 1734043
    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final b()I
    .locals 1
    .annotation build Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$CachePolicy;
    .end annotation

    .prologue
    .line 1734013
    iget-boolean v0, p0, LX/B0a;->g:Z

    if-eqz v0, :cond_0

    .line 1734014
    const/4 v0, 0x2

    .line 1734015
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1734012
    iget-boolean v0, p0, LX/B0a;->f:Z

    return v0
.end method
