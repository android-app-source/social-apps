.class public LX/C3w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0yc;

.field public final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0yc;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846540
    iput-object p1, p0, LX/C3w;->a:LX/0yc;

    .line 1846541
    iput-object p2, p0, LX/C3w;->b:Ljava/util/concurrent/Executor;

    .line 1846542
    return-void
.end method

.method public static a(LX/0QB;)LX/C3w;
    .locals 5

    .prologue
    .line 1846543
    const-class v1, LX/C3w;

    monitor-enter v1

    .line 1846544
    :try_start_0
    sget-object v0, LX/C3w;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846545
    sput-object v2, LX/C3w;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846546
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846547
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846548
    new-instance p0, LX/C3w;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v3

    check-cast v3, LX/0yc;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4}, LX/C3w;-><init>(LX/0yc;Ljava/util/concurrent/Executor;)V

    .line 1846549
    move-object v0, p0

    .line 1846550
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846551
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846552
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846553
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
