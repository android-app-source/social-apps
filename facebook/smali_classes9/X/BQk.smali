.class public final LX/BQk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B3T;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;)V
    .locals 0

    .prologue
    .line 1782662
    iput-object p1, p0, LX/BQk;->a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;I)V
    .locals 3

    .prologue
    .line 1782654
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1782655
    const-string v1, "heisman_profile_overlay_item"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1782656
    iget-object v1, p0, LX/BQk;->a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;->setResult(ILandroid/content/Intent;)V

    .line 1782657
    iget-object v0, p0, LX/BQk;->a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;->finish()V

    .line 1782658
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1782659
    iget-object v0, p0, LX/BQk;->a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->q:LX/0h5;

    if-eqz v0, :cond_0

    .line 1782660
    iget-object v0, p0, LX/BQk;->a:Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->q:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1782661
    :cond_0
    return-void
.end method
