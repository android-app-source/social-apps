.class public final LX/Au6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsT;


# instance fields
.field public final synthetic a:LX/AuF;


# direct methods
.method public constructor <init>(LX/AuF;)V
    .locals 0

    .prologue
    .line 1722230
    iput-object p1, p0, LX/Au6;->a:LX/AuF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 13

    .prologue
    .line 1722231
    iget-object v0, p0, LX/Au6;->a:LX/AuF;

    iget-object v0, v0, LX/AuF;->x:LX/Aqw;

    .line 1722232
    iget-object v1, v0, LX/Aqw;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v2, LX/ArJ;->TAP_CONFIRM_BUTTON:LX/ArJ;

    .line 1722233
    iget-object v4, v1, LX/ArT;->b:LX/ArL;

    iget-object v3, v1, LX/ArT;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v5

    iget-object v3, v1, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionStartTime()J

    move-result-wide v7

    sub-long/2addr v5, v7

    long-to-float v3, v5

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v3, v5

    .line 1722234
    sget-object v9, LX/ArH;->END_DOODLE_SESSION:LX/ArH;

    invoke-static {v4, v9, v2}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    sget-object v10, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v10}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v10

    float-to-double v11, v3

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-static {v4, v9}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1722235
    iget-object v3, v1, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setDoodleSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v1, v3}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1722236
    iget-object v1, v0, LX/Aqw;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab:LX/Aud;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Aud;

    invoke-virtual {v1}, LX/Aud;->c()V

    .line 1722237
    return-void
.end method
