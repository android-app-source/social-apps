.class public final enum LX/CJJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CJJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CJJ;

.field public static final enum AUDIO_OR_VIDEO:LX/CJJ;

.field public static final enum PHOTO:LX/CJJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1875295
    new-instance v0, LX/CJJ;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/CJJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CJJ;->PHOTO:LX/CJJ;

    .line 1875296
    new-instance v0, LX/CJJ;

    const-string v1, "AUDIO_OR_VIDEO"

    invoke-direct {v0, v1, v3}, LX/CJJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CJJ;->AUDIO_OR_VIDEO:LX/CJJ;

    .line 1875297
    const/4 v0, 0x2

    new-array v0, v0, [LX/CJJ;

    sget-object v1, LX/CJJ;->PHOTO:LX/CJJ;

    aput-object v1, v0, v2

    sget-object v1, LX/CJJ;->AUDIO_OR_VIDEO:LX/CJJ;

    aput-object v1, v0, v3

    sput-object v0, LX/CJJ;->$VALUES:[LX/CJJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1875298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CJJ;
    .locals 1

    .prologue
    .line 1875299
    const-class v0, LX/CJJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CJJ;

    return-object v0
.end method

.method public static values()[LX/CJJ;
    .locals 1

    .prologue
    .line 1875300
    sget-object v0, LX/CJJ;->$VALUES:[LX/CJJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CJJ;

    return-object v0
.end method
