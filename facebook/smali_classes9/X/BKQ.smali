.class public final LX/BKQ;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772864
    iput-object p1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 12

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1772848
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->N:LX/0wd;

    if-ne p1, v0, :cond_1

    .line 1772849
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1772850
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1772851
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v10, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->S:I

    int-to-float v11, v1

    float-to-double v0, v0

    iget-object v6, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget v6, v6, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->S:I

    int-to-double v8, v6

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v11, v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1772852
    :cond_0
    :goto_0
    return-void

    .line 1772853
    :cond_1
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->O:LX/0wd;

    if-ne p1, v0, :cond_2

    .line 1772854
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1772855
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1772856
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    goto :goto_0

    .line 1772857
    :cond_2
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1772858
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v10, v0

    .line 1772859
    float-to-double v0, v10

    const-wide v8, 0x3ff199999999999aL    # 1.1

    move-wide v6, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1772860
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1772861
    iget-object v1, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1772862
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v11, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    float-to-double v0, v10

    iget-object v6, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget v6, v6, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->S:I

    int-to-double v8, v6

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    neg-float v0, v0

    invoke-virtual {v11, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1772863
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3fc00000    # 1.5f

    div-float v2, v10, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    const-wide/16 v2, 0x0

    .line 1772865
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->N:LX/0wd;

    if-ne p1, v0, :cond_1

    .line 1772866
    invoke-virtual {p1, v2, v3}, LX/0wd;->g(D)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1772867
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1772868
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1772869
    :cond_0
    :goto_0
    return-void

    .line 1772870
    :cond_1
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->O:LX/0wd;

    if-ne p1, v0, :cond_2

    .line 1772871
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1772872
    :cond_2
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1772873
    invoke-virtual {p1, v4, v5}, LX/0wd;->g(D)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1, v2, v3}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1772874
    :cond_3
    iget-object v0, p0, LX/BKQ;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_0
.end method
