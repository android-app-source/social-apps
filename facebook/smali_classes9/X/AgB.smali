.class public final LX/AgB;
.super LX/Ag8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ag8",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/3Hc;


# direct methods
.method public constructor <init>(LX/3Hc;)V
    .locals 1

    .prologue
    .line 1700364
    iput-object p1, p0, LX/AgB;->b:LX/3Hc;

    invoke-direct {p0, p1}, LX/Ag8;-><init>(LX/3Hc;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1700365
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1700366
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v3

    .line 1700367
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    .line 1700368
    iput-object v3, v0, LX/3Hc;->s:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1700369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v3, v0, :cond_1

    move v0, v1

    .line 1700370
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->l()I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->m()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1700371
    iget-object v5, p0, LX/AgB;->b:LX/3Hc;

    iget-object v5, v5, LX/3Hc;->k:LX/0Zc;

    iget-object v6, p0, LX/AgB;->b:LX/3Hc;

    iget-object v6, v6, LX/3Hc;->m:Ljava/lang/String;

    sget-object v7, LX/7IM;->LIVE_POLLER_SUCCEED:LX/7IM;

    const-string v8, "fetched update live streaming %s, viewers %d, broadcast status: %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->k()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v2, 0x2

    if-eqz v3, :cond_2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    aput-object v1, v9, v2

    invoke-virtual {v5, v6, v7, v8, v9}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700372
    if-eqz v0, :cond_3

    .line 1700373
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_0

    .line 1700374
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V

    .line 1700375
    :cond_0
    :goto_2
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    invoke-static {v0, v4}, LX/3Hc;->a$redex0(LX/3Hc;I)V

    .line 1700376
    return-void

    :cond_1
    move v0, v2

    .line 1700377
    goto :goto_0

    .line 1700378
    :cond_2
    const-string v1, "not available"

    goto :goto_1

    .line 1700379
    :cond_3
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1700380
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    if-eqz v0, :cond_0

    .line 1700381
    iget-object v0, p0, LX/AgB;->b:LX/3Hc;

    iget-object v0, v0, LX/3Hc;->l:LX/3Ha;

    invoke-interface {v0, p1}, LX/3Ha;->b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V

    goto :goto_2
.end method
