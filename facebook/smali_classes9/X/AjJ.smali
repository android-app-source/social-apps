.class public final enum LX/AjJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AjJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AjJ;

.field public static final enum CANCELED:LX/AjJ;

.field public static final enum EMPTY_DATASET:LX/AjJ;

.field public static final enum HAS_STORIES:LX/AjJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1707764
    new-instance v0, LX/AjJ;

    const-string v1, "HAS_STORIES"

    invoke-direct {v0, v1, v2}, LX/AjJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AjJ;->HAS_STORIES:LX/AjJ;

    .line 1707765
    new-instance v0, LX/AjJ;

    const-string v1, "EMPTY_DATASET"

    invoke-direct {v0, v1, v3}, LX/AjJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AjJ;->EMPTY_DATASET:LX/AjJ;

    .line 1707766
    new-instance v0, LX/AjJ;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4}, LX/AjJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AjJ;->CANCELED:LX/AjJ;

    .line 1707767
    const/4 v0, 0x3

    new-array v0, v0, [LX/AjJ;

    sget-object v1, LX/AjJ;->HAS_STORIES:LX/AjJ;

    aput-object v1, v0, v2

    sget-object v1, LX/AjJ;->EMPTY_DATASET:LX/AjJ;

    aput-object v1, v0, v3

    sget-object v1, LX/AjJ;->CANCELED:LX/AjJ;

    aput-object v1, v0, v4

    sput-object v0, LX/AjJ;->$VALUES:[LX/AjJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1707769
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AjJ;
    .locals 1

    .prologue
    .line 1707770
    const-class v0, LX/AjJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AjJ;

    return-object v0
.end method

.method public static values()[LX/AjJ;
    .locals 1

    .prologue
    .line 1707768
    sget-object v0, LX/AjJ;->$VALUES:[LX/AjJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AjJ;

    return-object v0
.end method
