.class public final LX/C2h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;

.field private final b:LX/38r;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/38r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/38r;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1844523
    iput-object p1, p0, LX/C2h;->a:Lcom/facebook/feedplugins/calltoaction/WatchAndShopPostClickExperienceLauncherPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1844524
    iput-object p3, p0, LX/C2h;->b:LX/38r;

    .line 1844525
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844526
    if-eqz v0, :cond_0

    .line 1844527
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844528
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/C2h;->c:Ljava/lang/String;

    .line 1844529
    return-void

    .line 1844530
    :cond_1
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844531
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x73a19178

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1844532
    iget-object v1, p0, LX/C2h;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1844533
    const v1, -0x5b4162e2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1844534
    :goto_0
    return-void

    .line 1844535
    :cond_0
    iget-object v1, p0, LX/C2h;->b:LX/38r;

    new-instance v2, LX/D8q;

    iget-object v3, p0, LX/C2h;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/D8q;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1844536
    const v1, -0x7491ba23

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
