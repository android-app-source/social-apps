.class public final LX/CAL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CAM;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStory;

.field public b:LX/1Po;

.field public final synthetic c:LX/CAM;


# direct methods
.method public constructor <init>(LX/CAM;)V
    .locals 1

    .prologue
    .line 1855213
    iput-object p1, p0, LX/CAL;->c:LX/CAM;

    .line 1855214
    move-object v0, p1

    .line 1855215
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1855216
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1855217
    const-string v0, "SeenByUFIFeedbackSummaryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1855218
    if-ne p0, p1, :cond_1

    .line 1855219
    :cond_0
    :goto_0
    return v0

    .line 1855220
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1855221
    goto :goto_0

    .line 1855222
    :cond_3
    check-cast p1, LX/CAL;

    .line 1855223
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1855224
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1855225
    if-eq v2, v3, :cond_0

    .line 1855226
    iget-object v2, p0, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1855227
    goto :goto_0

    .line 1855228
    :cond_5
    iget-object v2, p1, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_4

    .line 1855229
    :cond_6
    iget-object v2, p0, LX/CAL;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CAL;->b:LX/1Po;

    iget-object v3, p1, LX/CAL;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1855230
    goto :goto_0

    .line 1855231
    :cond_7
    iget-object v2, p1, LX/CAL;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
