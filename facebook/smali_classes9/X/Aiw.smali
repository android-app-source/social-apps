.class public LX/Aiw;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Aiw;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1707214
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1707215
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-short v0, LX/Aha;->a:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1707216
    :goto_0
    sget-object v1, LX/0ax;->cO:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-class v0, Lcom/facebook/base/activity/ReactFragmentActivity;

    :goto_1
    sget-object v2, LX/0cQ;->AWESOMIZER_HOME_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v1, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1707217
    sget-object v0, LX/0ax;->cP:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->AWESOMIZER_UNFOLLOW_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1707218
    sget-object v0, LX/0ax;->cQ:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->AWESOMIZER_REFOLLOW_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1707219
    sget-object v0, LX/0ax;->cR:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->AWESOMIZER_SEEFIRST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1707220
    sget-object v0, LX/0ax;->cS:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->AWESOMIZER_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1707221
    return-void

    :cond_0
    move v0, v1

    .line 1707222
    goto :goto_0

    .line 1707223
    :cond_1
    const-class v0, Lcom/facebook/base/activity/FragmentChromeActivity;

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/Aiw;
    .locals 5

    .prologue
    .line 1707224
    sget-object v0, LX/Aiw;->a:LX/Aiw;

    if-nez v0, :cond_1

    .line 1707225
    const-class v1, LX/Aiw;

    monitor-enter v1

    .line 1707226
    :try_start_0
    sget-object v0, LX/Aiw;->a:LX/Aiw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1707227
    if-eqz v2, :cond_0

    .line 1707228
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1707229
    new-instance v4, LX/Aiw;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 p0, 0x148e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Aiw;-><init>(LX/0ad;LX/0Or;)V

    .line 1707230
    move-object v0, v4

    .line 1707231
    sput-object v0, LX/Aiw;->a:LX/Aiw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1707232
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1707233
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1707234
    :cond_1
    sget-object v0, LX/Aiw;->a:LX/Aiw;

    return-object v0

    .line 1707235
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1707236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
