.class public LX/C0j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1xN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xN",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/C0m;


# direct methods
.method public constructor <init>(LX/1xN;LX/C0m;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841385
    iput-object p1, p0, LX/C0j;->a:LX/1xN;

    .line 1841386
    iput-object p2, p0, LX/C0j;->b:LX/C0m;

    .line 1841387
    return-void
.end method

.method public static a(LX/0QB;)LX/C0j;
    .locals 5

    .prologue
    .line 1841388
    const-class v1, LX/C0j;

    monitor-enter v1

    .line 1841389
    :try_start_0
    sget-object v0, LX/C0j;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841390
    sput-object v2, LX/C0j;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841391
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841392
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841393
    new-instance p0, LX/C0j;

    invoke-static {v0}, LX/1xN;->a(LX/0QB;)LX/1xN;

    move-result-object v3

    check-cast v3, LX/1xN;

    invoke-static {v0}, LX/C0m;->a(LX/0QB;)LX/C0m;

    move-result-object v4

    check-cast v4, LX/C0m;

    invoke-direct {p0, v3, v4}, LX/C0j;-><init>(LX/1xN;LX/C0m;)V

    .line 1841394
    move-object v0, p0

    .line 1841395
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841396
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841397
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841398
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
