.class public final LX/Ato;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/Atp;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/Atp;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1721811
    iput-object p1, p0, LX/Ato;->a:LX/Atp;

    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 1721812
    iput-object p2, p0, LX/Ato;->b:Landroid/view/View;

    .line 1721813
    iput-object p3, p0, LX/Ato;->c:Landroid/view/View;

    .line 1721814
    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1721815
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1721816
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    sub-double/2addr v2, v4

    double-to-float v1, v2

    .line 1721817
    iget-object v2, p0, LX/Ato;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1721818
    iget-object v2, p0, LX/Ato;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1721819
    iget-object v2, p0, LX/Ato;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1721820
    iget-object v2, p0, LX/Ato;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1721821
    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    .line 1721822
    iget-object v0, p0, LX/Ato;->b:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1721823
    :cond_0
    :goto_0
    cmpl-float v0, v1, v6

    if-nez v0, :cond_3

    .line 1721824
    iget-object v0, p0, LX/Ato;->c:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1721825
    :cond_1
    :goto_1
    return-void

    .line 1721826
    :cond_2
    iget-object v0, p0, LX/Ato;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1721827
    iget-object v0, p0, LX/Ato;->b:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1721828
    :cond_3
    iget-object v0, p0, LX/Ato;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1721829
    iget-object v0, p0, LX/Ato;->c:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
