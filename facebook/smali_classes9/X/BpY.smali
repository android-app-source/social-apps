.class public final LX/BpY;
.super LX/7Ji;
.source ""


# instance fields
.field private final a:LX/BpZ;

.field public b:LX/7Kf;


# direct methods
.method public constructor <init>(LX/BpZ;)V
    .locals 0

    .prologue
    .line 1823273
    invoke-direct {p0}, LX/7Ji;-><init>()V

    .line 1823274
    iput-object p1, p0, LX/BpY;->a:LX/BpZ;

    .line 1823275
    return-void
.end method

.method private d(LX/04g;)V
    .locals 2

    .prologue
    .line 1823276
    iget-object v0, p0, LX/BpY;->b:LX/7Kf;

    if-eqz v0, :cond_2

    sget-object v0, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/04g;->BY_NEWSFEED_ONPAUSE:LX/04g;

    if-eq p1, v0, :cond_1

    .line 1823277
    iget-object v0, p1, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_DIVEBAR:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_DIALOG:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_BOOKMARK:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1823278
    if-eqz v0, :cond_2

    .line 1823279
    :cond_1
    iget-object v0, p0, LX/BpY;->a:LX/BpZ;

    iget-object v0, v0, LX/BpZ;->h:LX/BqH;

    iget-object v1, p0, LX/BpY;->b:LX/7Kf;

    invoke-interface {v1}, LX/7Kf;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BqH;->a(I)V

    .line 1823280
    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1823281
    iget-object v0, p0, LX/BpY;->a:LX/BpZ;

    iget-object v0, v0, LX/BpZ;->h:LX/BqH;

    .line 1823282
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BqH;->b:Z

    .line 1823283
    iget-object v0, p0, LX/BpY;->a:LX/BpZ;

    iget-object v0, v0, LX/BpZ;->h:LX/BqH;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BqH;->a(I)V

    .line 1823284
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 0

    .prologue
    .line 1823285
    invoke-direct {p0, p1}, LX/BpY;->d(LX/04g;)V

    .line 1823286
    return-void
.end method

.method public final c(LX/04g;Z)V
    .locals 0

    .prologue
    .line 1823287
    if-eqz p2, :cond_0

    .line 1823288
    invoke-direct {p0, p1}, LX/BpY;->d(LX/04g;)V

    .line 1823289
    :cond_0
    return-void
.end method
