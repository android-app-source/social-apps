.class public final LX/C3D;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C3E;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C33;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public final synthetic e:LX/C3E;


# direct methods
.method public constructor <init>(LX/C3E;)V
    .locals 1

    .prologue
    .line 1845218
    iput-object p1, p0, LX/C3D;->e:LX/C3E;

    .line 1845219
    move-object v0, p1

    .line 1845220
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1845221
    sget v0, LX/C3F;->a:I

    iput v0, p0, LX/C3D;->c:I

    .line 1845222
    const/4 v0, 0x4

    iput v0, p0, LX/C3D;->d:I

    .line 1845223
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845242
    const-string v0, "CondensedStoryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1845224
    if-ne p0, p1, :cond_1

    .line 1845225
    :cond_0
    :goto_0
    return v0

    .line 1845226
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1845227
    goto :goto_0

    .line 1845228
    :cond_3
    check-cast p1, LX/C3D;

    .line 1845229
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1845230
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1845231
    if-eq v2, v3, :cond_0

    .line 1845232
    iget-object v2, p0, LX/C3D;->a:LX/C33;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C3D;->a:LX/C33;

    iget-object v3, p1, LX/C3D;->a:LX/C33;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1845233
    goto :goto_0

    .line 1845234
    :cond_5
    iget-object v2, p1, LX/C3D;->a:LX/C33;

    if-nez v2, :cond_4

    .line 1845235
    :cond_6
    iget-object v2, p0, LX/C3D;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C3D;->b:LX/1Pb;

    iget-object v3, p1, LX/C3D;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1845236
    goto :goto_0

    .line 1845237
    :cond_8
    iget-object v2, p1, LX/C3D;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 1845238
    :cond_9
    iget v2, p0, LX/C3D;->c:I

    iget v3, p1, LX/C3D;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1845239
    goto :goto_0

    .line 1845240
    :cond_a
    iget v2, p0, LX/C3D;->d:I

    iget v3, p1, LX/C3D;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1845241
    goto :goto_0
.end method
