.class public final LX/BQV;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BQX;


# direct methods
.method public constructor <init>(LX/BQX;)V
    .locals 0

    .prologue
    .line 1782267
    iput-object p1, p0, LX/BQV;->a:LX/BQX;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1782268
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1782269
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadFailedEvent;

    .line 1782270
    sget-object v0, LX/BQU;->a:[I

    .line 1782271
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782272
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v2

    .line 1782273
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1782274
    :goto_0
    return-void

    .line 1782275
    :pswitch_0
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->i:Ljava/lang/String;

    .line 1782276
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782277
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782278
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1782279
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    .line 1782280
    const/4 v3, 0x0

    .line 1782281
    iput-object v3, v0, LX/BQX;->i:Ljava/lang/String;

    .line 1782282
    iget-object v1, v0, LX/BQX;->k:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1782283
    iget-object v1, v0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/BQX;->k:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782284
    iput-object v3, v0, LX/BQX;->k:Ljava/lang/Runnable;

    .line 1782285
    :cond_0
    invoke-static {v0}, LX/BQX;->c(LX/BQX;)V

    .line 1782286
    :cond_1
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->f:LX/BQQ;

    .line 1782287
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782288
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782289
    invoke-virtual {v0, v1}, LX/BQQ;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1782290
    :pswitch_1
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->j:Ljava/lang/String;

    .line 1782291
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782292
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782293
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "unsupportedSessionId"

    .line 1782294
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782295
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782296
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1782297
    :cond_3
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    .line 1782298
    const/4 v3, 0x0

    .line 1782299
    iput-object v3, v0, LX/BQX;->j:Ljava/lang/String;

    .line 1782300
    iget-object v1, v0, LX/BQX;->l:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    .line 1782301
    iget-object v1, v0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v2, v0, LX/BQX;->l:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782302
    iput-object v3, v0, LX/BQX;->l:Ljava/lang/Runnable;

    .line 1782303
    :cond_4
    invoke-static {v0}, LX/BQX;->c(LX/BQX;)V

    .line 1782304
    :cond_5
    iget-object v0, p0, LX/BQV;->a:LX/BQX;

    iget-object v0, v0, LX/BQX;->f:LX/BQQ;

    .line 1782305
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782306
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1782307
    invoke-virtual {v0, v1}, LX/BQQ;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
