.class public final LX/Bem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bep;


# direct methods
.method public constructor <init>(LX/Bep;)V
    .locals 0

    .prologue
    .line 1805375
    iput-object p1, p0, LX/Bem;->a:LX/Bep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1805376
    iget-object v0, p0, LX/Bem;->a:LX/Bep;

    iget-object v0, v0, LX/Bep;->f:LX/Ber;

    invoke-virtual {v0}, LX/Ber;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1805377
    iget-object v0, p0, LX/Bem;->a:LX/Bep;

    iget-object v0, v0, LX/Bep;->f:LX/Ber;

    invoke-virtual {v0}, LX/Ber;->f()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1805378
    :goto_0
    return-object v0

    .line 1805379
    :cond_0
    iget-object v0, p0, LX/Bem;->a:LX/Bep;

    iget-object v0, v0, LX/Bep;->f:LX/Ber;

    .line 1805380
    iget-boolean v1, v0, LX/Ber;->d:Z

    move v0, v1

    .line 1805381
    if-eqz v0, :cond_1

    .line 1805382
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1805383
    :cond_1
    iget-object v0, p0, LX/Bem;->a:LX/Bep;

    iget-object v1, p0, LX/Bem;->a:LX/Bep;

    invoke-static {v1}, LX/Bep;->e(LX/Bep;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v0, v1}, LX/Bep;->a$redex0(LX/Bep;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
