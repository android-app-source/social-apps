.class public LX/Aiv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0SG;

.field private final c:Ljava/lang/String;

.field public d:I

.field public e:J

.field public f:J

.field public g:J

.field private h:J

.field public i:J

.field public j:J

.field public k:J

.field public l:J

.field public final m:Ljava/util/List;

.field public final n:Ljava/util/List;

.field public final o:Ljava/util/Set;

.field public final p:Ljava/util/Set;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1707200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707201
    const-string v0, "feed_awesomizer"

    iput-object v0, p0, LX/Aiv;->c:Ljava/lang/String;

    .line 1707202
    const/4 v0, 0x0

    iput v0, p0, LX/Aiv;->d:I

    .line 1707203
    iput-wide v2, p0, LX/Aiv;->f:J

    .line 1707204
    iput-wide v2, p0, LX/Aiv;->j:J

    .line 1707205
    iput-wide v2, p0, LX/Aiv;->k:J

    .line 1707206
    iput-wide v2, p0, LX/Aiv;->l:J

    .line 1707207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Aiv;->m:Ljava/util/List;

    .line 1707208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Aiv;->n:Ljava/util/List;

    .line 1707209
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Aiv;->o:Ljava/util/Set;

    .line 1707210
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Aiv;->p:Ljava/util/Set;

    .line 1707211
    iput-object p1, p0, LX/Aiv;->a:LX/0Zb;

    .line 1707212
    iput-object p2, p0, LX/Aiv;->b:LX/0SG;

    .line 1707213
    return-void
.end method

.method public static b(LX/0QB;)LX/Aiv;
    .locals 3

    .prologue
    .line 1707198
    new-instance v2, LX/Aiv;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/Aiv;-><init>(LX/0Zb;LX/0SG;)V

    .line 1707199
    return-object v2
.end method


# virtual methods
.method public final a(IZ)V
    .locals 3

    .prologue
    .line 1707182
    if-eqz p2, :cond_1

    .line 1707183
    iget-object v0, p0, LX/Aiv;->n:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1707184
    iget-object v0, p0, LX/Aiv;->p:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1707185
    iget-object v0, p0, LX/Aiv;->p:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1707186
    :goto_0
    return-void

    .line 1707187
    :cond_0
    iget-object v0, p0, LX/Aiv;->o:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1707188
    :cond_1
    iget-object v0, p0, LX/Aiv;->n:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1707189
    iget-object v0, p0, LX/Aiv;->o:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1707190
    iget-object v0, p0, LX/Aiv;->o:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1707191
    :cond_2
    iget-object v0, p0, LX/Aiv;->p:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1707195
    iget-wide v0, p0, LX/Aiv;->l:J

    iget-object v2, p0, LX/Aiv;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/Aiv;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/Aiv;->l:J

    .line 1707196
    iget-wide v0, p0, LX/Aiv;->j:J

    iget-object v2, p0, LX/Aiv;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/Aiv;->i:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/Aiv;->j:J

    .line 1707197
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1707192
    iget-object v0, p0, LX/Aiv;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Aiv;->h:J

    .line 1707193
    iget-object v0, p0, LX/Aiv;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Aiv;->i:J

    .line 1707194
    return-void
.end method
