.class public LX/BeI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1804795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804796
    iput-object p1, p0, LX/BeI;->a:Ljava/lang/String;

    .line 1804797
    iput-object p2, p0, LX/BeI;->b:Ljava/lang/String;

    .line 1804798
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1804799
    if-ne p0, p1, :cond_1

    .line 1804800
    :cond_0
    :goto_0
    return v0

    .line 1804801
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1804802
    goto :goto_0

    .line 1804803
    :cond_3
    check-cast p1, LX/BeI;

    .line 1804804
    iget-object v2, p1, LX/BeI;->a:Ljava/lang/String;

    iget-object v3, p0, LX/BeI;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, LX/BeI;->b:Ljava/lang/String;

    iget-object v3, p0, LX/BeI;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1804805
    const v2, 0x1d11f

    iget-object v0, p0, LX/BeI;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1804806
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/BeI;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1804807
    return v0

    .line 1804808
    :cond_0
    iget-object v0, p0, LX/BeI;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1804809
    :cond_1
    iget-object v1, p0, LX/BeI;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
