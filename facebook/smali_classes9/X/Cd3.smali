.class public final LX/Cd3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0lF;",
        "LX/0am",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;)V
    .locals 0

    .prologue
    .line 1922039
    iput-object p1, p0, LX/Cd3;->a:Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1922040
    check-cast p1, LX/0lF;

    .line 1922041
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_0

    .line 1922042
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1922043
    :goto_0
    return-object v0

    .line 1922044
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 1922045
    new-instance v1, LX/5m9;

    invoke-direct {v1}, LX/5m9;-><init>()V

    sget-object v2, LX/Cd4;->c:LX/6Wc;

    .line 1922046
    iget-object v3, v2, LX/6Wc;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1922047
    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    .line 1922048
    iput-object v2, v1, LX/5m9;->f:Ljava/lang/String;

    .line 1922049
    move-object v1, v1

    .line 1922050
    sget-object v2, LX/Cd4;->a:LX/6Wc;

    .line 1922051
    iget-object v3, v2, LX/6Wc;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1922052
    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 1922053
    iput-object v0, v1, LX/5m9;->h:Ljava/lang/String;

    .line 1922054
    move-object v0, v1

    .line 1922055
    new-instance v1, LX/5mE;

    invoke-direct {v1}, LX/5mE;-><init>()V

    iget-object v2, p0, LX/Cd3;->a:Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;

    iget-object v2, v2, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 1922056
    iput-wide v2, v1, LX/5mE;->a:D

    .line 1922057
    move-object v1, v1

    .line 1922058
    iget-object v2, p0, LX/Cd3;->a:Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;

    iget-object v2, v2, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    .line 1922059
    iput-wide v2, v1, LX/5mE;->b:D

    .line 1922060
    move-object v1, v1

    .line 1922061
    invoke-virtual {v1}, LX/5mE;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v1

    .line 1922062
    iput-object v1, v0, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1922063
    move-object v0, v0

    .line 1922064
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method
