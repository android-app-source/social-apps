.class public LX/CEg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/CEg;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0lB;

.field public final c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0lB;LX/0TD;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1861717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1861718
    iput-object p1, p0, LX/CEg;->a:Landroid/content/res/Resources;

    .line 1861719
    iput-object p2, p0, LX/CEg;->b:LX/0lB;

    .line 1861720
    iput-object p3, p0, LX/CEg;->c:LX/0TD;

    .line 1861721
    return-void
.end method

.method public static a(LX/0QB;)LX/CEg;
    .locals 6

    .prologue
    .line 1861704
    sget-object v0, LX/CEg;->d:LX/CEg;

    if-nez v0, :cond_1

    .line 1861705
    const-class v1, LX/CEg;

    monitor-enter v1

    .line 1861706
    :try_start_0
    sget-object v0, LX/CEg;->d:LX/CEg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1861707
    if-eqz v2, :cond_0

    .line 1861708
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1861709
    new-instance p0, LX/CEg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-direct {p0, v3, v4, v5}, LX/CEg;-><init>(Landroid/content/res/Resources;LX/0lB;LX/0TD;)V

    .line 1861710
    move-object v0, p0

    .line 1861711
    sput-object v0, LX/CEg;->d:LX/CEg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1861712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1861713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1861714
    :cond_1
    sget-object v0, LX/CEg;->d:LX/CEg;

    return-object v0

    .line 1861715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1861716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/CEg;)Lcom/facebook/greetingcards/verve/model/VMDeck;
    .locals 3

    .prologue
    .line 1861692
    const/4 v1, 0x0

    .line 1861693
    :try_start_0
    iget-object v0, p0, LX/CEg;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "souvenirs.json"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 1861694
    iget-object v0, p0, LX/CEg;->b:LX/0lB;

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v0

    .line 1861695
    const-class v2, Lcom/facebook/greetingcards/verve/model/VMDeck;

    invoke-virtual {v0, v2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMDeck;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1861696
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    return-object v0

    .line 1861697
    :catch_0
    move-exception v0

    .line 1861698
    :try_start_1
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1861699
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1861700
    :goto_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error deserializing Deck."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1861701
    :catch_1
    move-exception v0

    .line 1861702
    :try_start_2
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1861703
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v0
.end method
