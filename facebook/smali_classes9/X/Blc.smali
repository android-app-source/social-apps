.class public final enum LX/Blc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Blc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Blc;

.field public static final enum PRIVATE_GOING:LX/Blc;

.field public static final enum PRIVATE_INVITED:LX/Blc;

.field public static final enum PRIVATE_MAYBE:LX/Blc;

.field public static final enum PRIVATE_NOT_GOING:LX/Blc;

.field public static final enum PUBLIC_GOING:LX/Blc;

.field public static final enum PUBLIC_INVITED:LX/Blc;

.field public static final enum PUBLIC_WATCHED:LX/Blc;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1816319
    new-instance v0, LX/Blc;

    const-string v1, "PRIVATE_GOING"

    invoke-direct {v0, v1, v3}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PRIVATE_GOING:LX/Blc;

    .line 1816320
    new-instance v0, LX/Blc;

    const-string v1, "PRIVATE_MAYBE"

    invoke-direct {v0, v1, v4}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    .line 1816321
    new-instance v0, LX/Blc;

    const-string v1, "PRIVATE_INVITED"

    invoke-direct {v0, v1, v5}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    .line 1816322
    new-instance v0, LX/Blc;

    const-string v1, "PRIVATE_NOT_GOING"

    invoke-direct {v0, v1, v6}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    .line 1816323
    new-instance v0, LX/Blc;

    const-string v1, "PUBLIC_GOING"

    invoke-direct {v0, v1, v7}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PUBLIC_GOING:LX/Blc;

    .line 1816324
    new-instance v0, LX/Blc;

    const-string v1, "PUBLIC_WATCHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    .line 1816325
    new-instance v0, LX/Blc;

    const-string v1, "PUBLIC_INVITED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Blc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    .line 1816326
    const/4 v0, 0x7

    new-array v0, v0, [LX/Blc;

    sget-object v1, LX/Blc;->PRIVATE_GOING:LX/Blc;

    aput-object v1, v0, v3

    sget-object v1, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    aput-object v1, v0, v4

    sget-object v1, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    aput-object v1, v0, v5

    sget-object v1, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    aput-object v1, v0, v6

    sget-object v1, LX/Blc;->PUBLIC_GOING:LX/Blc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    aput-object v2, v0, v1

    sput-object v0, LX/Blc;->$VALUES:[LX/Blc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1816318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Blc;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1816302
    if-nez p0, :cond_1

    .line 1816303
    :cond_0
    :goto_0
    return-object v0

    .line 1816304
    :cond_1
    const-string v1, "PRIVATE_GOING"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1816305
    sget-object v0, LX/Blc;->PRIVATE_GOING:LX/Blc;

    goto :goto_0

    .line 1816306
    :cond_2
    const-string v1, "PRIVATE_MAYBE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1816307
    sget-object v0, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    goto :goto_0

    .line 1816308
    :cond_3
    const-string v1, "PRIVATE_INVITED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1816309
    sget-object v0, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    goto :goto_0

    .line 1816310
    :cond_4
    const-string v1, "PRIVATE_NOT_GOING"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1816311
    sget-object v0, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    goto :goto_0

    .line 1816312
    :cond_5
    const-string v1, "PUBLIC_GOING"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1816313
    sget-object v0, LX/Blc;->PUBLIC_GOING:LX/Blc;

    goto :goto_0

    .line 1816314
    :cond_6
    const-string v1, "PUBLIC_WATCHED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1816315
    sget-object v0, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    goto :goto_0

    .line 1816316
    :cond_7
    const-string v1, "PUBLIC_INVITED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1816317
    sget-object v0, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    goto :goto_0
.end method

.method public static readGuestListTypesList(Landroid/os/Bundle;Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1816287
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1816288
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1816289
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1816290
    if-eqz v0, :cond_0

    .line 1816291
    invoke-static {v0}, LX/Blc;->valueOf(Ljava/lang/String;)LX/Blc;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1816292
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Blc;
    .locals 1

    .prologue
    .line 1816301
    const-class v0, LX/Blc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Blc;

    return-object v0
.end method

.method public static values()[LX/Blc;
    .locals 1

    .prologue
    .line 1816300
    sget-object v0, LX/Blc;->$VALUES:[LX/Blc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Blc;

    return-object v0
.end method

.method public static writeGuestListTypesList(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/Blc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1816294
    if-nez p2, :cond_0

    .line 1816295
    :goto_0
    return-void

    .line 1816296
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1816297
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    .line 1816298
    invoke-virtual {v0}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1816299
    :cond_1
    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1816293
    invoke-virtual {p0}, LX/Blc;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
