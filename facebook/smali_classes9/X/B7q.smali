.class public final LX/B7q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/B7r;


# direct methods
.method public constructor <init>(LX/B7r;)V
    .locals 0

    .prologue
    .line 1748205
    iput-object p1, p0, LX/B7q;->a:LX/B7r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const v0, -0x500ccc

    invoke-static {v4, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1748206
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    invoke-virtual {v0}, LX/B7r;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1748207
    iget-object v2, p0, LX/B7q;->a:LX/B7r;

    invoke-virtual {v2}, LX/B7r;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1748208
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    iget-object v0, v0, LX/B7r;->c:LX/B7W;

    new-instance v2, LX/B7Z;

    const/4 v3, 0x0

    invoke-direct {v2, v6, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1748209
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    iget-object v0, v0, LX/B7r;->g:LX/7Tj;

    if-nez v0, :cond_0

    .line 1748210
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    iget-object v2, p0, LX/B7q;->a:LX/B7r;

    iget-object v2, v2, LX/B7r;->b:LX/7Tk;

    iget-object v3, p0, LX/B7q;->a:LX/B7r;

    invoke-virtual {v3}, LX/B7r;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, LX/7Tk;->a(Landroid/content/Context;Z)LX/7Tj;

    move-result-object v2

    .line 1748211
    iput-object v2, v0, LX/B7r;->g:LX/7Tj;

    .line 1748212
    :cond_0
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    iget-object v0, v0, LX/B7r;->g:LX/7Tj;

    new-instance v2, LX/B7p;

    invoke-direct {v2, p0}, LX/B7p;-><init>(LX/B7q;)V

    .line 1748213
    iput-object v2, v0, LX/7Tj;->u:LX/6u6;

    .line 1748214
    iget-object v0, p0, LX/B7q;->a:LX/B7r;

    iget-object v0, v0, LX/B7r;->g:LX/7Tj;

    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1748215
    const v0, 0x1d532c9

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
