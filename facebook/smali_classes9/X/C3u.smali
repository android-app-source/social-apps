.class public LX/C3u;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3s;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1846510
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C3u;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3w;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846511
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1846512
    iput-object p1, p0, LX/C3u;->b:LX/0Ot;

    .line 1846513
    return-void
.end method

.method public static a(LX/0QB;)LX/C3u;
    .locals 4

    .prologue
    .line 1846474
    const-class v1, LX/C3u;

    monitor-enter v1

    .line 1846475
    :try_start_0
    sget-object v0, LX/C3u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846476
    sput-object v2, LX/C3u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846477
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846478
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846479
    new-instance v3, LX/C3u;

    const/16 p0, 0x1ed4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3u;-><init>(LX/0Ot;)V

    .line 1846480
    move-object v0, v3

    .line 1846481
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846482
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846483
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1846508
    invoke-static {}, LX/1dS;->b()V

    .line 1846509
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1846505
    iget-object v0, p0, LX/C3u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1846506
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 1846507
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 5

    .prologue
    .line 1846514
    check-cast p2, LX/C3t;

    .line 1846515
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1846516
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1846517
    iget-object v0, p0, LX/C3u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3w;

    iget-object v3, p2, LX/C3t;->a:LX/1Pq;

    iget-object v4, p2, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846518
    new-instance p0, LX/C3v;

    invoke-direct {p0, v0, v3, v4}, LX/C3v;-><init>(LX/C3w;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1846519
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1846520
    iget-object p1, v0, LX/C3w;->a:LX/0yc;

    .line 1846521
    iget-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 1846522
    check-cast p0, LX/0yL;

    invoke-virtual {p1, p0}, LX/0yc;->a(LX/0yL;)V

    .line 1846523
    iget-object p0, v0, LX/C3w;->a:LX/0yc;

    invoke-virtual {p0}, LX/0yc;->j()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    .line 1846524
    iput-object p0, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1846525
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1846526
    check-cast v0, LX/0yL;

    iput-object v0, p2, LX/C3t;->c:LX/0yL;

    .line 1846527
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1846528
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1846529
    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p2, LX/C3t;->d:Ljava/lang/Boolean;

    .line 1846530
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1846531
    return-void
.end method

.method public final c(LX/1De;)LX/C3s;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1846497
    new-instance v1, LX/C3t;

    invoke-direct {v1, p0}, LX/C3t;-><init>(LX/C3u;)V

    .line 1846498
    sget-object v2, LX/C3u;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3s;

    .line 1846499
    if-nez v2, :cond_0

    .line 1846500
    new-instance v2, LX/C3s;

    invoke-direct {v2}, LX/C3s;-><init>()V

    .line 1846501
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3s;->a$redex0(LX/C3s;LX/1De;IILX/C3t;)V

    .line 1846502
    move-object v1, v2

    .line 1846503
    move-object v0, v1

    .line 1846504
    return-object v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1846496
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 5

    .prologue
    .line 1846490
    check-cast p3, LX/C3t;

    .line 1846491
    iget-object v0, p0, LX/C3u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3w;

    iget-object v1, p3, LX/C3t;->a:LX/1Pq;

    iget-object v2, p3, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p3, LX/C3t;->c:LX/0yL;

    iget-object v4, p3, LX/C3t;->d:Ljava/lang/Boolean;

    .line 1846492
    iget-object p0, v0, LX/C3w;->a:LX/0yc;

    invoke-virtual {p0, v3}, LX/0yc;->a(LX/0yL;)V

    .line 1846493
    iget-object p0, v0, LX/C3w;->a:LX/0yc;

    invoke-virtual {p0}, LX/0yc;->j()Z

    move-result p0

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eq p0, p1, :cond_0

    .line 1846494
    iget-object p0, v0, LX/C3w;->b:Ljava/util/concurrent/Executor;

    new-instance p1, Lcom/facebook/feedplugins/dialtone/DialtoneStateChangeInvalidatorComponentSpec$2;

    invoke-direct {p1, v0, v1, v2}, Lcom/facebook/feedplugins/dialtone/DialtoneStateChangeInvalidatorComponentSpec$2;-><init>(LX/C3w;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    const p2, 0x3ceaeb08

    invoke-static {p0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1846495
    :cond_0
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1846486
    check-cast p3, LX/C3t;

    .line 1846487
    iget-object v0, p0, LX/C3u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3w;

    iget-object v1, p3, LX/C3t;->c:LX/0yL;

    .line 1846488
    iget-object p0, v0, LX/C3w;->a:LX/0yc;

    invoke-virtual {p0, v1}, LX/0yc;->b(LX/0yL;)V

    .line 1846489
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1846485
    const/16 v0, 0xf

    return v0
.end method
