.class public LX/BSE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/15d;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BSF;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/app/Dialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/15d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1785292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785293
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1785294
    iput-object v0, p0, LX/BSE;->c:LX/0Ot;

    .line 1785295
    iput-object p1, p0, LX/BSE;->b:LX/0Or;

    .line 1785296
    return-void
.end method

.method public static a(LX/0QB;)LX/BSE;
    .locals 5

    .prologue
    .line 1785297
    const-class v1, LX/BSE;

    monitor-enter v1

    .line 1785298
    :try_start_0
    sget-object v0, LX/BSE;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1785299
    sput-object v2, LX/BSE;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1785300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1785301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1785302
    new-instance v3, LX/BSE;

    const/16 v4, 0x12fb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v4}, LX/BSE;-><init>(LX/0Or;)V

    .line 1785303
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v4

    const/16 p0, 0x37bb    # 1.9992E-41f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1785304
    iput-object v4, v3, LX/BSE;->a:LX/0Or;

    iput-object p0, v3, LX/BSE;->c:LX/0Ot;

    .line 1785305
    move-object v0, v3

    .line 1785306
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1785307
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BSE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1785308
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1785309
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1785310
    iget-object v0, p0, LX/BSE;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15d;

    invoke-virtual {v0}, LX/15d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BSE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSF;

    invoke-virtual {v0}, LX/BSF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BSE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSF;

    invoke-virtual {v0}, LX/BSF;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
