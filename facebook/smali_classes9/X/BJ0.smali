.class public final LX/BJ0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 0

    .prologue
    .line 1771128
    iput-object p1, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 1771129
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1771130
    iput-boolean v6, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    .line 1771131
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v1, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    .line 1771132
    iget-wide v7, p1, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v2, v7

    .line 1771133
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 1771134
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1771135
    iget-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v0, v2

    .line 1771136
    iget-object v2, p1, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1771137
    invoke-virtual {v0, v2}, Lcom/facebook/user/model/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1771138
    :goto_0
    return-void

    .line 1771139
    :cond_1
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v1, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    .line 1771140
    iget-object v2, v0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1771141
    iget-object v2, v0, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 1771142
    :goto_1
    move v0, v2

    .line 1771143
    const/16 v1, 0x32

    if-lt v0, v1, :cond_2

    .line 1771144
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081370

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1771145
    :cond_2
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v1, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/75Q;->a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1771146
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1771147
    sget-object v1, LX/74F;->ADD_TAG_SUCCESS:LX/74F;

    invoke-static {v1}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1771148
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9ip;->h()V

    .line 1771149
    iget-object v0, p0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9ip;->n()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
