.class public LX/BS4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1784692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5wP;LX/BS1;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 1784693
    const v2, 0x7f08153f

    .line 1784694
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-interface {p0}, LX/5wP;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1784695
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-interface {p0}, LX/5wP;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 1784696
    if-eqz v0, :cond_0

    .line 1784697
    const v2, 0x7f08157f

    .line 1784698
    const v0, 0x7f0217e8

    :goto_0
    move v8, v1

    move v3, v0

    move v6, v1

    move v5, v1

    .line 1784699
    :goto_1
    const/4 v4, 0x2

    move-object v0, p1

    move v7, v1

    invoke-interface/range {v0 .. v8}, LX/BS1;->a(IIIIZZZZ)V

    .line 1784700
    return-void

    .line 1784701
    :cond_0
    const v0, 0x7f020b47

    goto :goto_0

    .line 1784702
    :cond_1
    const v2, 0x7f08153e

    .line 1784703
    const v3, 0x7f02087f

    .line 1784704
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-interface {p0}, LX/5wP;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 1784705
    const/4 v8, 0x0

    move v5, v6

    goto :goto_1
.end method
