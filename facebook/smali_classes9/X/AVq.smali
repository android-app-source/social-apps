.class public LX/AVq;
.super LX/AVj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVj",
        "<",
        "Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/AVw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680462
    invoke-direct {p0}, LX/AVj;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1680463
    if-nez p1, :cond_1

    .line 1680464
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->l()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, LX/AVj;->c:I

    if-le v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/AW2;)Z
    .locals 1

    .prologue
    .line 1680430
    check-cast p1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-direct {p0, p1}, LX/AVq;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1680431
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-direct {p0, v0}, LX/AVq;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1680432
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680433
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->a(Z)V

    .line 1680434
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680435
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->b(Z)V

    .line 1680436
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVq;->b:LX/AW2;

    .line 1680437
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->a()I

    move-result v0

    iget v1, p0, LX/AVj;->c:I

    if-ge v0, v1, :cond_1

    .line 1680438
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    iput-object v0, p0, LX/AVq;->b:LX/AW2;

    .line 1680439
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680440
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    iget-object v1, p0, LX/AVj;->b:LX/AW2;

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->setText(Ljava/lang/String;)V

    .line 1680441
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->j()Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    move-result-object v4

    .line 1680442
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680443
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_LOGO:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    if-ne v4, v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->a(Z)V

    .line 1680444
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680445
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_BUTTON:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    if-ne v4, v1, :cond_6

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->b(Z)V

    .line 1680446
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680447
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->setVisibility(I)V

    .line 1680448
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680449
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    iget-object v1, p0, LX/AVq;->d:LX/AVw;

    iget-object v1, v1, LX/AVw;->a:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1680450
    :cond_1
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-nez v0, :cond_2

    .line 1680451
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680452
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->setVisibility(I)V

    .line 1680453
    :cond_2
    return-void

    .line 1680454
    :cond_3
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    const/4 v1, 0x1

    .line 1680455
    if-nez v0, :cond_7

    .line 1680456
    :cond_4
    :goto_3
    move v0, v1

    .line 1680457
    if-eqz v0, :cond_0

    .line 1680458
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680459
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    iget-object v1, p0, LX/AVq;->d:LX/AVw;

    iget-object v1, v1, LX/AVw;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    :cond_5
    move v1, v3

    .line 1680460
    goto :goto_1

    :cond_6
    move v2, v3

    .line 1680461
    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->a()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;->l()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, LX/AVj;->c:I

    add-int/lit16 v5, v5, 0xc8

    if-le v4, v5, :cond_4

    const/4 v1, 0x0

    goto :goto_3
.end method
