.class public LX/BQX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final n:Ljava/lang/Object;


# instance fields
.field private final a:LX/BQW;

.field private final b:LX/BQV;

.field private final c:LX/0b3;

.field public final d:LX/BQP;

.field public final e:Landroid/os/Handler;

.field public final f:LX/BQQ;

.field public final g:LX/0kL;

.field public final h:LX/0ad;

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782353
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/BQX;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0b3;LX/BQP;Landroid/os/Handler;LX/BQQ;LX/0kL;LX/0ad;)V
    .locals 2
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782355
    new-instance v0, LX/BQW;

    invoke-direct {v0, p0}, LX/BQW;-><init>(LX/BQX;)V

    iput-object v0, p0, LX/BQX;->a:LX/BQW;

    .line 1782356
    new-instance v0, LX/BQV;

    invoke-direct {v0, p0}, LX/BQV;-><init>(LX/BQX;)V

    iput-object v0, p0, LX/BQX;->b:LX/BQV;

    .line 1782357
    iput-object p1, p0, LX/BQX;->c:LX/0b3;

    .line 1782358
    iput-object p2, p0, LX/BQX;->d:LX/BQP;

    .line 1782359
    iput-object p3, p0, LX/BQX;->e:Landroid/os/Handler;

    .line 1782360
    iput-object p4, p0, LX/BQX;->f:LX/BQQ;

    .line 1782361
    iput-object p5, p0, LX/BQX;->g:LX/0kL;

    .line 1782362
    iput-object p6, p0, LX/BQX;->h:LX/0ad;

    .line 1782363
    return-void
.end method

.method public static a(LX/0QB;)LX/BQX;
    .locals 14

    .prologue
    .line 1782364
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1782365
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1782366
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1782367
    if-nez v1, :cond_0

    .line 1782368
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1782369
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1782370
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1782371
    sget-object v1, LX/BQX;->n:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1782372
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1782373
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1782374
    :cond_1
    if-nez v1, :cond_4

    .line 1782375
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1782376
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1782377
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1782378
    new-instance v7, LX/BQX;

    invoke-static {v0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v8

    check-cast v8, LX/0b3;

    invoke-static {v0}, LX/BQP;->a(LX/0QB;)LX/BQP;

    move-result-object v9

    check-cast v9, LX/BQP;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    invoke-static {v0}, LX/BQQ;->a(LX/0QB;)LX/BQQ;

    move-result-object v11

    check-cast v11, LX/BQQ;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v7 .. v13}, LX/BQX;-><init>(LX/0b3;LX/BQP;Landroid/os/Handler;LX/BQQ;LX/0kL;LX/0ad;)V

    .line 1782379
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1782380
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1782381
    if-nez v1, :cond_2

    .line 1782382
    sget-object v0, LX/BQX;->n:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1782383
    :goto_1
    if-eqz v0, :cond_3

    .line 1782384
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782385
    :goto_3
    check-cast v0, LX/BQX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1782386
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1782387
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1782388
    :catchall_1
    move-exception v0

    .line 1782389
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782390
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1782391
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1782392
    :cond_2
    :try_start_8
    sget-object v0, LX/BQX;->n:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/BQX;)V
    .locals 2

    .prologue
    .line 1782393
    iget-object v0, p0, LX/BQX;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BQX;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1782394
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->a:LX/BQW;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1782395
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->b:LX/BQV;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1782396
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BQX;->m:Z

    .line 1782397
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782398
    iget-object v0, p0, LX/BQX;->f:LX/BQQ;

    invoke-virtual {v0, p1, p2}, LX/BQQ;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1782399
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->a:LX/BQW;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1782400
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->b:LX/BQV;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1782401
    iput-object p2, p0, LX/BQX;->i:Ljava/lang/String;

    .line 1782402
    iget-object v0, p0, LX/BQX;->k:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1782403
    iget-object v0, p0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/BQX;->k:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782404
    :cond_0
    new-instance v0, Lcom/facebook/timeline/profilemedia/upload/ProfileMediaUploadReceiver$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilemedia/upload/ProfileMediaUploadReceiver$1;-><init>(LX/BQX;)V

    iput-object v0, p0, LX/BQX;->k:Ljava/lang/Runnable;

    .line 1782405
    iget-object v0, p0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/BQX;->k:Ljava/lang/Runnable;

    const-wide/32 v2, 0x36ee80

    const v4, -0x7766ad0b

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1782406
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782407
    iput-boolean p3, p0, LX/BQX;->m:Z

    .line 1782408
    iget-object v0, p0, LX/BQX;->f:LX/BQQ;

    invoke-virtual {v0, p1, p2}, LX/BQQ;->b(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1782409
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->a:LX/BQW;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1782410
    iget-object v0, p0, LX/BQX;->c:LX/0b3;

    iget-object v1, p0, LX/BQX;->b:LX/BQV;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1782411
    iput-object p2, p0, LX/BQX;->j:Ljava/lang/String;

    .line 1782412
    iget-object v0, p0, LX/BQX;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1782413
    iget-object v0, p0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/BQX;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1782414
    :cond_0
    new-instance v0, Lcom/facebook/timeline/profilemedia/upload/ProfileMediaUploadReceiver$2;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilemedia/upload/ProfileMediaUploadReceiver$2;-><init>(LX/BQX;)V

    iput-object v0, p0, LX/BQX;->l:Ljava/lang/Runnable;

    .line 1782415
    iget-object v0, p0, LX/BQX;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/BQX;->l:Ljava/lang/Runnable;

    const-wide/32 v2, 0x36ee80

    const v4, 0x28565d05

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1782416
    return-void
.end method
