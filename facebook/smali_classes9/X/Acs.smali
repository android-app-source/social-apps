.class public final LX/Acs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationCampaignViewerQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Acx;

.field public final synthetic b:LX/Acy;


# direct methods
.method public constructor <init>(LX/Acy;LX/Acx;)V
    .locals 0

    .prologue
    .line 1693230
    iput-object p1, p0, LX/Acs;->b:LX/Acy;

    iput-object p2, p0, LX/Acs;->a:LX/Acx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1693231
    iget-object v0, p0, LX/Acs;->a:LX/Acx;

    invoke-interface {v0, v1, v1, v1}, LX/Acx;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1693232
    iget-object v0, p0, LX/Acs;->b:LX/Acy;

    iget-object v0, v0, LX/Acy;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Acy;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get donation campaign from video"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1693233
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693234
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1693235
    if-eqz p1, :cond_0

    .line 1693236
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693237
    if-nez v0, :cond_1

    .line 1693238
    :cond_0
    iget-object v0, p0, LX/Acs;->a:LX/Acx;

    invoke-interface {v0, v1, v1, v1}, LX/Acx;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 1693239
    :goto_0
    return-void

    .line 1693240
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693241
    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationCampaignViewerQueryModel;

    .line 1693242
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationCampaignViewerQueryModel;->j()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v1

    .line 1693243
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationCampaignViewerQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1693244
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationCampaignViewerQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1693245
    iget-object v3, p0, LX/Acs;->b:LX/Acy;

    iget-object v3, v3, LX/Acy;->e:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$1$1;

    invoke-direct {v4, p0, v1, v2, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$1$1;-><init>(LX/Acs;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    const v0, -0x2c4d59b6

    invoke-static {v3, v4, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
