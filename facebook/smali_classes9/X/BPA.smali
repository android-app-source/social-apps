.class public final enum LX/BPA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BPA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BPA;

.field public static final enum PHOTO_HIGH_RES:LX/BPA;

.field public static final enum PHOTO_HIGH_RES_FAILED:LX/BPA;

.field public static final enum PHOTO_LOW_RES:LX/BPA;

.field public static final enum PHOTO_LOW_RES_FAILED:LX/BPA;

.field public static final enum PHOTO_MINI_PREVIEW:LX/BPA;

.field public static final enum PHOTO_MINI_PREVIEW_FAILED:LX/BPA;

.field public static final enum PHOTO_NONE:LX/BPA;

.field public static final enum PHOTO_NOT_LOADED:LX/BPA;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1780747
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_NOT_LOADED"

    invoke-direct {v0, v1, v3}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    .line 1780748
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_MINI_PREVIEW"

    invoke-direct {v0, v1, v4}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    .line 1780749
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_LOW_RES"

    invoke-direct {v0, v1, v5}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    .line 1780750
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_HIGH_RES"

    invoke-direct {v0, v1, v6}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    .line 1780751
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_NONE"

    invoke-direct {v0, v1, v7}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_NONE:LX/BPA;

    .line 1780752
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_MINI_PREVIEW_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_MINI_PREVIEW_FAILED:LX/BPA;

    .line 1780753
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_LOW_RES_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    .line 1780754
    new-instance v0, LX/BPA;

    const-string v1, "PHOTO_HIGH_RES_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/BPA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    .line 1780755
    const/16 v0, 0x8

    new-array v0, v0, [LX/BPA;

    sget-object v1, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    aput-object v1, v0, v3

    sget-object v1, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    aput-object v1, v0, v4

    sget-object v1, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    aput-object v1, v0, v5

    sget-object v1, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    aput-object v1, v0, v6

    sget-object v1, LX/BPA;->PHOTO_NONE:LX/BPA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BPA;->PHOTO_MINI_PREVIEW_FAILED:LX/BPA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    aput-object v2, v0, v1

    sput-object v0, LX/BPA;->$VALUES:[LX/BPA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1780756
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isFailLoadState(LX/BPA;)Z
    .locals 1

    .prologue
    .line 1780757
    sget-object v0, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFinalPhotoLoadState(LX/BPA;)Z
    .locals 1

    .prologue
    .line 1780758
    sget-object v0, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_NONE:LX/BPA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhotoLoaded(LX/BPA;)Z
    .locals 1

    .prologue
    .line 1780759
    sget-object v0, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_NONE:LX/BPA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BPA;
    .locals 1

    .prologue
    .line 1780760
    const-class v0, LX/BPA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BPA;

    return-object v0
.end method

.method public static values()[LX/BPA;
    .locals 1

    .prologue
    .line 1780761
    sget-object v0, LX/BPA;->$VALUES:[LX/BPA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BPA;

    return-object v0
.end method
