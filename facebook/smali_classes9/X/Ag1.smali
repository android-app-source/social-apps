.class public LX/Ag1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:LX/8pY;

.field private final c:LX/1b4;

.field private final d:LX/AaZ;

.field private final e:Landroid/content/res/Resources;

.field private f:I


# direct methods
.method public constructor <init>(LX/8pY;LX/1b4;LX/AaZ;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1700066
    iput-object p1, p0, LX/Ag1;->b:LX/8pY;

    .line 1700067
    iput-object p2, p0, LX/Ag1;->c:LX/1b4;

    .line 1700068
    iput-object p3, p0, LX/Ag1;->d:LX/AaZ;

    .line 1700069
    iput-object p4, p0, LX/Ag1;->e:Landroid/content/res/Resources;

    .line 1700070
    iget-object v0, p0, LX/Ag1;->c:LX/1b4;

    .line 1700071
    iget-object p1, v0, LX/1b4;->a:LX/0ad;

    sget p2, LX/1v6;->a:I

    const/16 p3, 0xa

    invoke-interface {p1, p2, p3}, LX/0ad;->a(II)I

    move-result p1

    move v0, p1

    .line 1700072
    iput v0, p0, LX/Ag1;->a:I

    .line 1700073
    const/4 v0, 0x0

    iput v0, p0, LX/Ag1;->f:I

    .line 1700074
    return-void
.end method

.method public static a(LX/0QB;)LX/Ag1;
    .locals 1

    .prologue
    .line 1700075
    invoke-static {p0}, LX/Ag1;->b(LX/0QB;)LX/Ag1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Ag1;
    .locals 5

    .prologue
    .line 1700076
    new-instance v4, LX/Ag1;

    invoke-static {p0}, LX/8pY;->b(LX/0QB;)LX/8pY;

    move-result-object v0

    check-cast v0, LX/8pY;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v1

    check-cast v1, LX/1b4;

    invoke-static {p0}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v2

    check-cast v2, LX/AaZ;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Ag1;-><init>(LX/8pY;LX/1b4;LX/AaZ;Landroid/content/res/Resources;)V

    .line 1700077
    return-object v4
.end method

.method public static b(LX/Aeu;)Z
    .locals 2

    .prologue
    .line 1700078
    iget-object v0, p0, LX/Aeu;->i:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Aeu;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Aeu;)LX/Aen;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1700079
    iget-object v1, p0, LX/Ag1;->c:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ag1;->d:LX/AaZ;

    invoke-virtual {v1}, LX/AaZ;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1700080
    :cond_0
    :goto_0
    return-object v0

    .line 1700081
    :cond_1
    invoke-static {p1}, LX/Ag1;->b(LX/Aeu;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1700082
    iget v1, p0, LX/Ag1;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/Ag1;->f:I

    .line 1700083
    :cond_2
    iget v1, p0, LX/Ag1;->f:I

    iget v2, p0, LX/Ag1;->a:I

    if-lt v1, v2, :cond_0

    .line 1700084
    const-string v0, "translate_comments_cta"

    const/4 v1, 0x0

    const-string v2, "internet"

    const v3, -0xb4b0aa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Ag1;->e:Landroid/content/res/Resources;

    const v5, 0x7f080c06

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "translate"

    iget-object v6, p0, LX/Ag1;->e:Landroid/content/res/Resources;

    const v7, 0x7f080c08

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/Ag1;->e:Landroid/content/res/Resources;

    const v8, 0x7f080c07

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "checkmark"

    iget-object v9, p0, LX/Ag1;->d:LX/AaZ;

    invoke-virtual {v9}, LX/AaZ;->a()Z

    move-result v9

    invoke-static/range {v0 .. v9}, LX/Aen;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Aen;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Aeu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1700085
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Ag1;->a(Ljava/util/List;LX/AcU;)V

    .line 1700086
    return-void
.end method

.method public final a(Ljava/util/List;LX/AcU;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Aeu;",
            ">;",
            "Lcom/facebook/facecastdisplay/liveevent/translation/FacecastCommentTranslationUtil$FacecastTranslationLoadedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1700087
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1700088
    :cond_0
    :goto_0
    return-void

    .line 1700089
    :cond_1
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1700090
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aeu;

    .line 1700091
    invoke-static {v0}, LX/Ag1;->b(LX/Aeu;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1700092
    iget-object v0, v0, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1700093
    :cond_3
    move-object v0, v1

    .line 1700094
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1700095
    iget-object v1, p0, LX/Ag1;->b:LX/8pY;

    new-instance v2, LX/Ag0;

    invoke-direct {v2, p0, p1, p2}, LX/Ag0;-><init>(LX/Ag1;Ljava/util/List;LX/AcU;)V

    invoke-virtual {v1, v0, v2}, LX/8pY;->a(Ljava/util/List;LX/3Wt;)V

    goto :goto_0
.end method
