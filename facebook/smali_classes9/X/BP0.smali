.class public LX/BP0;
.super LX/5SB;
.source ""


# instance fields
.field public final a:LX/5P2;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field public d:Z


# direct methods
.method private constructor <init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)V
    .locals 1
    .param p8    # LX/5P2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "LX/5SA;",
            "Landroid/os/ParcelUuid;",
            "LX/5P2;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1780631
    invoke-direct/range {p0 .. p7}, LX/5SB;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;)V

    .line 1780632
    iput-object p8, p0, LX/BP0;->a:LX/5P2;

    .line 1780633
    iput-object p9, p0, LX/BP0;->b:LX/0am;

    .line 1780634
    iput-boolean p10, p0, LX/BP0;->c:Z

    .line 1780635
    return-void
.end method

.method public static a(JJLjava/lang/String;Landroid/os/ParcelUuid;)LX/BP0;
    .locals 12

    .prologue
    .line 1780636
    new-instance v1, LX/BP0;

    sget-object v7, LX/5SA;->USER:LX/5SA;

    const/4 v9, 0x0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v10

    const/4 v11, 0x0

    move-wide v2, p0

    move-wide v4, p2

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v1 .. v11}, LX/BP0;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)V

    return-object v1
.end method

.method public static a(JJLjava/lang/String;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)LX/BP0;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Landroid/os/ParcelUuid;",
            "LX/5P2;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "LX/BP0;"
        }
    .end annotation

    .prologue
    .line 1780629
    new-instance v1, LX/BP0;

    sget-object v7, LX/5SA;->USER:LX/5SA;

    move-wide v2, p0

    move-wide v4, p2

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v1 .. v11}, LX/BP0;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;LX/5P2;LX/0am;Z)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1780630
    invoke-virtual {p0}, LX/5SB;->i()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1780628
    invoke-virtual {p0}, LX/5SB;->i()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1780627
    invoke-virtual {p0}, LX/5SB;->i()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1780626
    iget-boolean v0, p0, LX/BP0;->c:Z

    return v0
.end method

.method public final e()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1780625
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method
