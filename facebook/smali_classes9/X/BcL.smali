.class public final enum LX/BcL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BcL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BcL;

.field public static final enum FAILED:LX/BcL;

.field public static final enum INITIAL_LOAD:LX/BcL;

.field public static final enum LOADING:LX/BcL;

.field public static final enum SUCCEEDED:LX/BcL;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1801864
    new-instance v0, LX/BcL;

    const-string v1, "INITIAL_LOAD"

    invoke-direct {v0, v1, v2}, LX/BcL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BcL;->INITIAL_LOAD:LX/BcL;

    new-instance v0, LX/BcL;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/BcL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BcL;->LOADING:LX/BcL;

    new-instance v0, LX/BcL;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/BcL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BcL;->SUCCEEDED:LX/BcL;

    new-instance v0, LX/BcL;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/BcL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BcL;->FAILED:LX/BcL;

    const/4 v0, 0x4

    new-array v0, v0, [LX/BcL;

    sget-object v1, LX/BcL;->INITIAL_LOAD:LX/BcL;

    aput-object v1, v0, v2

    sget-object v1, LX/BcL;->LOADING:LX/BcL;

    aput-object v1, v0, v3

    sget-object v1, LX/BcL;->SUCCEEDED:LX/BcL;

    aput-object v1, v0, v4

    sget-object v1, LX/BcL;->FAILED:LX/BcL;

    aput-object v1, v0, v5

    sput-object v0, LX/BcL;->$VALUES:[LX/BcL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1801863
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BcL;
    .locals 1

    .prologue
    .line 1801862
    const-class v0, LX/BcL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BcL;

    return-object v0
.end method

.method public static values()[LX/BcL;
    .locals 1

    .prologue
    .line 1801861
    sget-object v0, LX/BcL;->$VALUES:[LX/BcL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BcL;

    return-object v0
.end method
