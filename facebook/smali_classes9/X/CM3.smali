.class public LX/CM3;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/CLu;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7HC;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

.field public final d:LX/1zU;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CML;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/7kc;

.field public final h:LX/CMN;

.field private final i:Landroid/view/View$OnTouchListener;

.field public j:LX/CM6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/CMK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;LX/1zU;LX/0Ot;LX/0Ot;LX/7kc;LX/CMN;)V
    .locals 1
    .param p7    # LX/7kc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/7HC;",
            ">;",
            "Lcom/facebook/messaging/emoji/EmojiButtonBuilder;",
            "LX/1zU;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CML;",
            ">;",
            "LX/7kc;",
            "LX/CMN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880046
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1880047
    new-instance v0, LX/CLy;

    invoke-direct {v0, p0}, LX/CLy;-><init>(LX/CM3;)V

    iput-object v0, p0, LX/CM3;->i:Landroid/view/View$OnTouchListener;

    .line 1880048
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1880049
    iput-object v0, p0, LX/CM3;->l:LX/0Px;

    .line 1880050
    iput-object p1, p0, LX/CM3;->a:Landroid/content/Context;

    .line 1880051
    iput-object p2, p0, LX/CM3;->b:LX/0Ot;

    .line 1880052
    iput-object p3, p0, LX/CM3;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    .line 1880053
    iput-object p4, p0, LX/CM3;->d:LX/1zU;

    .line 1880054
    iput-object p5, p0, LX/CM3;->e:LX/0Ot;

    .line 1880055
    iput-object p6, p0, LX/CM3;->f:LX/0Ot;

    .line 1880056
    iput-object p7, p0, LX/CM3;->g:LX/7kc;

    .line 1880057
    iput-object p8, p0, LX/CM3;->h:LX/CMN;

    .line 1880058
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 1880059
    iget-object v0, p0, LX/CM3;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a(Landroid/view/ViewGroup;)LX/CLu;

    move-result-object v0

    .line 1880060
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/1a3;

    const/4 v3, -0x1

    iget-object v4, p0, LX/CM3;->g:LX/7kc;

    .line 1880061
    iget p1, v4, LX/7kc;->j:I

    move v4, p1

    .line 1880062
    invoke-direct {v2, v3, v4}, LX/1a3;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1880063
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/CLz;

    invoke-direct {v2, p0, v0}, LX/CLz;-><init>(LX/CM3;LX/CLu;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1880064
    iget-object v1, p0, LX/CM3;->h:LX/CMN;

    const/4 v2, 0x0

    .line 1880065
    invoke-static {v1}, LX/CMN;->f(LX/CMN;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/CMN;->c:LX/0Uh;

    const/16 v4, 0x177

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 1880066
    if-eqz v1, :cond_1

    .line 1880067
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/CM0;

    invoke-direct {v2, p0, v0}, LX/CM0;-><init>(LX/CM3;LX/CLu;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1880068
    :cond_1
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    iget-object v2, p0, LX/CM3;->i:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1880069
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1880070
    iput-object p1, p0, LX/CM3;->l:LX/0Px;

    .line 1880071
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1880072
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 1880073
    check-cast p1, LX/CLu;

    .line 1880074
    iget-object v0, p0, LX/CM3;->l:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880075
    invoke-virtual {p1, v0}, LX/CLu;->b(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880076
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 1880077
    iget-object v0, p0, LX/CM3;->d:LX/1zU;

    invoke-virtual {v0, p2}, LX/1zU;->a(Lcom/facebook/ui/emoji/model/Emoji;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1880078
    :cond_0
    :goto_0
    return-void

    .line 1880079
    :cond_1
    iget-object v0, p0, LX/CM3;->h:LX/CMN;

    invoke-virtual {v0}, LX/CMN;->d()Z

    move-result v8

    .line 1880080
    new-instance v0, LX/CMK;

    iget-object v1, p0, LX/CM3;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v8}, LX/CMK;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, LX/CM3;->k:LX/CMK;

    .line 1880081
    iget-object v0, p0, LX/CM3;->k:LX/CMK;

    invoke-virtual {v0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1880082
    iget-object v0, p0, LX/CM3;->d:LX/1zU;

    invoke-virtual {v0, p2}, LX/1zU;->b(Lcom/facebook/ui/emoji/model/Emoji;)LX/0Px;

    move-result-object v9

    move v1, v7

    .line 1880083
    :goto_1
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1880084
    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880085
    new-instance v4, LX/CM1;

    invoke-direct {v4, p0, v0, v8}, LX/CM1;-><init>(LX/CM3;Lcom/facebook/ui/emoji/model/Emoji;Z)V

    .line 1880086
    new-instance v5, LX/CM2;

    invoke-direct {v5, p0, v0}, LX/CM2;-><init>(LX/CM3;Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880087
    iget-object v2, p0, LX/CM3;->a:Landroid/content/Context;

    iget-object v0, p0, LX/CM3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HC;

    invoke-interface {v0}, LX/7HC;->a()I

    move-result v0

    invoke-static {v2, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1880088
    iget-object v0, p0, LX/CM3;->k:LX/CMK;

    .line 1880089
    iget v3, p2, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    move v3, v3

    .line 1880090
    iget-object v6, p0, LX/CM3;->i:Landroid/view/View$OnTouchListener;

    .line 1880091
    iget-object v10, v0, LX/CMK;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 1880092
    invoke-virtual {v10, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1880093
    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1880094
    if-eqz v3, :cond_3

    iget-object p1, v0, LX/CMK;->a:Landroid/content/Context;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    invoke-virtual {v10, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1880095
    invoke-virtual {v10, v5}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1880096
    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1880097
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1880098
    :cond_2
    iget-object v0, p0, LX/CM3;->k:LX/CMK;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1880099
    if-eqz v8, :cond_0

    .line 1880100
    iget-object v0, p0, LX/CM3;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/CMI;->e:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, LX/CMI;->f:LX/0Tn;

    invoke-interface {v0, v1, v7}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto/16 :goto_0

    .line 1880101
    :cond_3
    const/4 p1, 0x0

    goto :goto_2
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1880102
    iget-object v0, p0, LX/CM3;->k:LX/CMK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CM3;->k:LX/CMK;

    .line 1880103
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1880104
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1880105
    iget-object v0, p0, LX/CM3;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
