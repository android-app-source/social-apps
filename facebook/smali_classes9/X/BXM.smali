.class public final LX/BXM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793022
    iput-object p1, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 1793023
    iget-object v0, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1793024
    const/4 v1, 0x0

    .line 1793025
    iget-object p1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {p1}, LX/8tB;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1793026
    :cond_0
    :goto_0
    move v1, v1

    .line 1793027
    move v0, v1

    .line 1793028
    if-eqz v0, :cond_1

    .line 1793029
    iget-object v0, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->l()V

    .line 1793030
    :cond_1
    return-void

    .line 1793031
    :cond_2
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 1793032
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x3

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 1793033
    packed-switch p2, :pswitch_data_0

    .line 1793034
    :goto_0
    return-void

    .line 1793035
    :pswitch_0
    iget-object v0, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->f:LX/0Sy;

    invoke-virtual {v0, p1}, LX/0Sy;->a(Landroid/view/View;)V

    .line 1793036
    iget-object v0, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1793037
    invoke-static {v0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b$redex0(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V

    .line 1793038
    goto :goto_0

    .line 1793039
    :pswitch_1
    iget-object v0, p0, LX/BXM;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->f:LX/0Sy;

    invoke-virtual {v0, p1}, LX/0Sy;->b(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
