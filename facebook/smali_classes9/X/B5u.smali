.class public final LX/B5u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Alg;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Alg;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1746044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746045
    iput-object p1, p0, LX/B5u;->a:LX/0QB;

    .line 1746046
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1746047
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/B5u;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1746048
    packed-switch p2, :pswitch_data_0

    .line 1746049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1746050
    :pswitch_0
    invoke-static {p1}, LX/Alh;->b(LX/0QB;)LX/Alh;

    move-result-object v0

    .line 1746051
    :goto_0
    return-object v0

    .line 1746052
    :pswitch_1
    new-instance v2, LX/AyP;

    invoke-static {p1}, LX/AzD;->b(LX/0QB;)LX/AzD;

    move-result-object v3

    check-cast v3, LX/AzD;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v4

    check-cast v4, LX/1Nq;

    const/16 v5, 0x3be

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x22c2

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x22c1

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/AyP;-><init>(LX/AzD;LX/1Nq;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1746053
    move-object v0, v2

    .line 1746054
    goto :goto_0

    .line 1746055
    :pswitch_2
    new-instance v1, LX/BMN;

    .line 1746056
    new-instance v0, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/B5t;

    invoke-direct {v3, p1}, LX/B5t;-><init>(LX/0QB;)V

    invoke-direct {v0, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v0

    .line 1746057
    new-instance v4, LX/BMJ;

    const-class v0, LX/1Ns;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/1Ns;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v3

    check-cast v3, LX/1Nq;

    invoke-direct {v4, v0, v3}, LX/BMJ;-><init>(LX/1Ns;LX/1Nq;)V

    .line 1746058
    move-object v0, v4

    .line 1746059
    check-cast v0, LX/BMJ;

    invoke-direct {v1, v2, v0}, LX/BMN;-><init>(Ljava/util/Set;LX/BMJ;)V

    .line 1746060
    move-object v0, v1

    .line 1746061
    goto :goto_0

    .line 1746062
    :pswitch_3
    new-instance v3, LX/BOg;

    invoke-direct {v3}, LX/BOg;-><init>()V

    .line 1746063
    const-class v0, LX/1Ns;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/1Ns;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    .line 1746064
    iput-object v0, v3, LX/BOg;->a:LX/1Ns;

    iput-object v1, v3, LX/BOg;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, v3, LX/BOg;->c:LX/0SG;

    .line 1746065
    move-object v0, v3

    .line 1746066
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1746067
    const/4 v0, 0x4

    return v0
.end method
