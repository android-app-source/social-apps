.class public LX/CZd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5w5;


# instance fields
.field public final a:J

.field public final b:Landroid/location/Location;

.field private c:LX/0ta;

.field public d:LX/CZc;

.field public e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

.field public f:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Z

.field private m:Z


# direct methods
.method public constructor <init>(JLandroid/location/Location;)V
    .locals 1

    .prologue
    .line 1916353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1916354
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    iput-object v0, p0, LX/CZd;->c:LX/0ta;

    .line 1916355
    sget-object v0, LX/CZc;->UNINITIALIZED:LX/CZc;

    iput-object v0, p0, LX/CZd;->d:LX/CZc;

    .line 1916356
    iput-wide p1, p0, LX/CZd;->a:J

    .line 1916357
    iput-object p3, p0, LX/CZd;->b:Landroid/location/Location;

    .line 1916358
    return-void
.end method

.method public static a(LX/CZd;Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V
    .locals 6

    .prologue
    .line 1916294
    iput-object p1, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1916295
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916296
    if-eqz v0, :cond_0

    .line 1916297
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916298
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1916299
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916300
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;->j()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/CZd;->f:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    .line 1916301
    :cond_0
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->t()Z

    move-result v0

    iput-boolean v0, p0, LX/CZd;->l:Z

    .line 1916302
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->w()Z

    move-result v0

    iput-boolean v0, p0, LX/CZd;->m:Z

    .line 1916303
    const/4 p1, 0x0

    const/4 v1, 0x0

    .line 1916304
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916305
    if-eqz v0, :cond_1

    .line 1916306
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916307
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1916308
    :cond_1
    iput-object p1, p0, LX/CZd;->g:LX/0Px;

    .line 1916309
    :goto_0
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916310
    if-eqz v0, :cond_2

    .line 1916311
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916312
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1916313
    :cond_2
    iput-object p1, p0, LX/CZd;->h:LX/0Px;

    .line 1916314
    :goto_1
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916315
    if-eqz v0, :cond_3

    .line 1916316
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916317
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v0

    if-nez v0, :cond_9

    .line 1916318
    :cond_3
    iput-object p1, p0, LX/CZd;->i:LX/0Px;

    .line 1916319
    :goto_2
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916320
    if-eqz v0, :cond_4

    .line 1916321
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916322
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v0

    if-nez v0, :cond_b

    .line 1916323
    :cond_4
    iput-object p1, p0, LX/CZd;->j:LX/0Px;

    .line 1916324
    :goto_3
    return-void

    .line 1916325
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1916326
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916327
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    .line 1916328
    :goto_4
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 1916329
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1916330
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1916331
    :cond_6
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CZd;->g:LX/0Px;

    goto :goto_0

    .line 1916332
    :cond_7
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1916333
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916334
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    .line 1916335
    :goto_5
    if-ge v2, v5, :cond_8

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 1916336
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1916337
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1916338
    :cond_8
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CZd;->h:LX/0Px;

    goto :goto_1

    .line 1916339
    :cond_9
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1916340
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916341
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    .line 1916342
    :goto_6
    if-ge v2, v5, :cond_a

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 1916343
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1916344
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1916345
    :cond_a
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CZd;->i:LX/0Px;

    goto/16 :goto_2

    .line 1916346
    :cond_b
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1916347
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916348
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1916349
    :goto_7
    if-ge v1, v4, :cond_c

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 1916350
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1916351
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1916352
    :cond_c
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CZd;->j:LX/0Px;

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1916293
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;LX/0ta;)V
    .locals 2

    .prologue
    .line 1916288
    iget-wide v0, p0, LX/CZd;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916289
    invoke-static {p0, p1}, LX/CZd;->a(LX/CZd;Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V

    .line 1916290
    iput-object p2, p0, LX/CZd;->c:LX/0ta;

    .line 1916291
    sget-object v0, LX/CZc;->FINAL_DATA:LX/CZc;

    iput-object v0, p0, LX/CZd;->d:LX/CZc;

    .line 1916292
    return-void
.end method

.method public final a(LX/8A3;)Z
    .locals 2

    .prologue
    .line 1916280
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916281
    if-eqz v0, :cond_0

    .line 1916282
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916283
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1916284
    :cond_0
    const/4 v0, 0x0

    .line 1916285
    :goto_0
    return v0

    :cond_1
    new-instance v0, LX/8A4;

    .line 1916286
    iget-object v1, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v1

    .line 1916287
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, p1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;
    .locals 1

    .prologue
    .line 1916279
    iget-object v0, p0, LX/CZd;->f:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1916278
    iget-object v0, p0, LX/CZd;->d:LX/CZc;

    sget-object v1, LX/CZc;->FINAL_DATA:LX/CZc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1916268
    iget-object v0, p0, LX/CZd;->d:LX/CZc;

    sget-object v1, LX/CZc;->UNINITIALIZED:LX/CZc;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1916277
    iget-wide v0, p0, LX/CZd;->a:J

    return-wide v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1916269
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916270
    if-eqz v0, :cond_0

    .line 1916271
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916272
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1916273
    :cond_0
    const/4 v0, 0x0

    .line 1916274
    :goto_0
    return-object v0

    .line 1916275
    :cond_1
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 1916276
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
