.class public final LX/Boj;
.super LX/1wG;
.source ""


# instance fields
.field public final synthetic c:LX/Bok;


# direct methods
.method public constructor <init>(LX/Bok;)V
    .locals 0

    .prologue
    .line 1821939
    iput-object p1, p0, LX/Boj;->c:LX/Bok;

    invoke-direct {p0, p1}, LX/1wG;-><init>(LX/1dt;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1821930
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821931
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1821932
    iget-object v1, p0, LX/Boj;->c:LX/Bok;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Boj;->c:LX/Bok;

    .line 1821933
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1821934
    if-nez v1, :cond_0

    iget-object v1, p0, LX/Boj;->c:LX/Bok;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Boj;->c:LX/Bok;

    .line 1821935
    invoke-virtual {v1, v0}, LX/1SX;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1821936
    if-nez v1, :cond_0

    .line 1821937
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1821938
    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/Boj;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1wG;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/Boj;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1821922
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821923
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1821924
    iget-object v1, p0, LX/Boj;->c:LX/Bok;

    iget-object v1, v1, LX/Bok;->b:LX/B0n;

    invoke-virtual {v1, v0}, LX/B0n;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 1821925
    new-instance v2, LX/Boi;

    invoke-direct {v2, p0, p2}, LX/Boi;-><init>(LX/Boj;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1821926
    iget-object v2, p0, LX/Boj;->c:LX/Bok;

    .line 1821927
    const v3, 0x7f020809

    move v3, v3

    .line 1821928
    invoke-virtual {v2, v1, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1821929
    return-void
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1821920
    invoke-virtual {p0, p1}, LX/1wG;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final f(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1821921
    invoke-static {p1}, LX/B0n;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method
