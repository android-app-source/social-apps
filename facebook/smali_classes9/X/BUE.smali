.class public final LX/BUE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BTr;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/BUF;


# direct methods
.method public constructor <init>(LX/BUF;LX/BTr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1788696
    iput-object p1, p0, LX/BUE;->c:LX/BUF;

    iput-object p2, p0, LX/BUE;->a:LX/BTr;

    iput-object p3, p0, LX/BUE;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1788697
    sget-object v0, LX/BUF;->a:Ljava/lang/String;

    const-string v1, "Fetch video uri failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788698
    iget-object v0, p0, LX/BUE;->c:LX/BUF;

    iget v0, v0, LX/BUF;->h:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1788699
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1788700
    new-instance v1, Lcom/facebook/video/downloadmanager/OfflineVideoInfoFetcher$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/downloadmanager/OfflineVideoInfoFetcher$2$1;-><init>(LX/BUE;)V

    iget-object v2, p0, LX/BUE;->c:LX/BUF;

    iget v2, v2, LX/BUF;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1788701
    iget-object v0, p0, LX/BUE;->c:LX/BUF;

    const/4 v1, 0x1

    .line 1788702
    iget v2, v0, LX/BUF;->h:I

    add-int/2addr v2, v1

    iput v2, v0, LX/BUF;->h:I

    .line 1788703
    iget-object v0, p0, LX/BUE;->c:LX/BUF;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/BUF;->d(LX/BUF;I)I

    .line 1788704
    :goto_0
    return-void

    .line 1788705
    :cond_0
    iget-object v0, p0, LX/BUE;->a:LX/BTr;

    iget-object v1, p0, LX/BUE;->b:Ljava/lang/String;

    .line 1788706
    iget-object v2, v0, LX/BTr;->a:LX/BUA;

    invoke-static {v2, v1, p1}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1788707
    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1788708
    check-cast p1, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    .line 1788709
    iget-object v0, p0, LX/BUE;->a:LX/BTr;

    iget-object v1, p0, LX/BUE;->b:Ljava/lang/String;

    .line 1788710
    if-eqz p1, :cond_0

    .line 1788711
    iget-object v2, v0, LX/BTr;->a:LX/BUA;

    .line 1788712
    iget-object v3, v2, LX/BUA;->i:LX/1fW;

    new-instance v0, LX/BTs;

    invoke-direct {v0, v2, p1, v1}, LX/BTs;-><init>(LX/BUA;Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0TT;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788713
    :goto_0
    iget-object v0, p0, LX/BUE;->c:LX/BUF;

    const/4 v1, 0x0

    .line 1788714
    iput v1, v0, LX/BUF;->h:I

    .line 1788715
    iget-object v0, p0, LX/BUE;->c:LX/BUF;

    const/16 v1, 0x1f4

    .line 1788716
    iput v1, v0, LX/BUF;->f:I

    .line 1788717
    return-void

    .line 1788718
    :cond_0
    iget-object v2, v0, LX/BTr;->a:LX/BUA;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
