.class public LX/Bcp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private c:I

.field public d:LX/Bcn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bcn",
            "<TTEdge;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Bcm",
            "<TTEdge;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final f:I

.field private final g:I

.field private final h:LX/1vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field private i:Z

.field public j:I

.field public k:Z


# direct methods
.method public constructor <init>(LX/2kW;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1802749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1802750
    const/4 v0, 0x1

    iput v0, p0, LX/Bcp;->c:I

    .line 1802751
    iput-boolean v1, p0, LX/Bcp;->i:Z

    .line 1802752
    iput-object p1, p0, LX/Bcp;->a:LX/2kW;

    .line 1802753
    iput p2, p0, LX/Bcp;->g:I

    .line 1802754
    iput p3, p0, LX/Bcp;->f:I

    .line 1802755
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    .line 1802756
    new-instance v0, LX/Bco;

    invoke-direct {v0, p0}, LX/Bco;-><init>(LX/Bcp;)V

    iput-object v0, p0, LX/Bcp;->h:LX/1vq;

    .line 1802757
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    iget-object v1, p0, LX/Bcp;->h:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 1802758
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/Bcp;->b:Landroid/os/Handler;

    .line 1802759
    return-void
.end method

.method public static b(LX/Bcp;LX/0Px;LX/2kM;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1802760
    const/4 v3, 0x0

    .line 1802761
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1802762
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 1802763
    iget v2, v0, LX/3CY;->b:I

    new-array v7, v2, [LX/Bcl;

    .line 1802764
    iget-object v2, v0, LX/3CY;->a:LX/3Ca;

    sget-object v8, LX/3Ca;->DELETE:LX/3Ca;

    if-ne v2, v8, :cond_0

    .line 1802765
    iget v2, v0, LX/3CY;->b:I

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 1802766
    iget v8, v0, LX/3CY;->b:I

    add-int/lit8 v8, v8, -0x1

    sub-int/2addr v8, v2

    new-instance v9, LX/Bcl;

    iget-object v10, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v11

    add-int/2addr v11, v2

    const/4 v12, 0x0

    invoke-direct {v9, v10, v11, v12}, LX/Bcl;-><init>(LX/3Ca;ILjava/lang/Object;)V

    aput-object v9, v7, v8

    .line 1802767
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_0
    move v2, v3

    .line 1802768
    :goto_2
    iget v8, v0, LX/3CY;->b:I

    if-ge v2, v8, :cond_1

    .line 1802769
    new-instance v8, LX/Bcl;

    iget-object v9, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v10

    add-int/2addr v10, v2

    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v11

    add-int/2addr v11, v2

    invoke-interface {p2, v11}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v11

    invoke-direct {v8, v9, v10, v11}, LX/Bcl;-><init>(LX/3Ca;ILjava/lang/Object;)V

    aput-object v8, v7, v2

    .line 1802770
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1802771
    :cond_1
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1802772
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1802773
    :cond_2
    move-object v0, v5

    .line 1802774
    new-instance v3, LX/Bcm;

    iget v2, p0, LX/Bcp;->c:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/Bcp;->c:I

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-interface {p2}, LX/2kM;->a()LX/2nj;

    move-result-object v4

    .line 1802775
    iget-boolean v5, v4, LX/2nj;->d:Z

    move v4, v5

    .line 1802776
    invoke-interface {p2}, LX/2kM;->b()LX/2nj;

    move-result-object v5

    .line 1802777
    iget-boolean v6, v5, LX/2nj;->d:Z

    move v5, v6

    .line 1802778
    invoke-direct {v3, v2, v0, v4, v5}, LX/Bcm;-><init>(ILX/0Px;ZZ)V

    .line 1802779
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    .line 1802780
    :goto_3
    if-ge v2, v4, :cond_5

    .line 1802781
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 1802782
    iget-object v5, v0, LX/3CY;->a:LX/3Ca;

    sget-object v6, LX/3Ca;->DELETE:LX/3Ca;

    if-ne v5, v6, :cond_4

    .line 1802783
    iget v5, p0, LX/Bcp;->j:I

    iget v0, v0, LX/3CY;->b:I

    sub-int v0, v5, v0

    iput v0, p0, LX/Bcp;->j:I

    .line 1802784
    :cond_3
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1802785
    :cond_4
    iget-object v5, v0, LX/3CY;->a:LX/3Ca;

    sget-object v6, LX/3Ca;->INSERT:LX/3Ca;

    if-ne v5, v6, :cond_3

    .line 1802786
    iget v5, p0, LX/Bcp;->j:I

    iget v0, v0, LX/3CY;->b:I

    add-int/2addr v0, v5

    iput v0, p0, LX/Bcp;->j:I

    goto :goto_4

    .line 1802787
    :cond_5
    monitor-enter p0

    .line 1802788
    :try_start_0
    iget-object v0, p0, LX/Bcp;->d:LX/Bcn;

    if-eqz v0, :cond_8

    .line 1802789
    iget-boolean v0, p0, LX/Bcp;->k:Z

    if-nez v0, :cond_6

    .line 1802790
    iget-object v2, p0, LX/Bcp;->d:LX/Bcn;

    iget v0, p0, LX/Bcp;->j:I

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_5
    invoke-interface {v2, v0}, LX/Bcn;->a(Z)V

    .line 1802791
    :cond_6
    iget-object v0, p0, LX/Bcp;->d:LX/Bcn;

    invoke-interface {v0, v3}, LX/Bcn;->a(LX/Bcm;)V

    .line 1802792
    :goto_6
    monitor-exit p0

    return-void

    :cond_7
    move v0, v1

    .line 1802793
    goto :goto_5

    .line 1802794
    :cond_8
    iget-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1802795
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(LX/Bcp;Z)V
    .locals 2

    .prologue
    .line 1802796
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    iget-object v1, p0, LX/Bcp;->h:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->b(LX/1vq;)V

    .line 1802797
    if-eqz p1, :cond_0

    .line 1802798
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    invoke-virtual {v0}, LX/2kW;->c()V

    .line 1802799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1802800
    iget v0, p0, LX/Bcp;->g:I

    const/4 v1, 0x0

    .line 1802801
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Bcp;->k:Z

    .line 1802802
    iget-object v2, p0, LX/Bcp;->a:LX/2kW;

    invoke-virtual {v2, v0, v1}, LX/2kW;->c(ILjava/lang/Object;)V

    .line 1802803
    return-void
.end method

.method public final declared-synchronized a(LX/Bcn;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bcn",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1802804
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/Bcp;->d:LX/Bcn;

    .line 1802805
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1802806
    iget-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1802807
    iget-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bcm;

    invoke-interface {p1, v0}, LX/Bcn;->a(LX/Bcm;)V

    .line 1802808
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1802809
    :cond_0
    iget v0, p0, LX/Bcp;->j:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {p1, v0}, LX/Bcn;->a(Z)V

    .line 1802810
    iget-object v0, p0, LX/Bcp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802811
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v0, v1

    .line 1802812
    goto :goto_1

    .line 1802813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTUserInfo;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1802814
    iget-boolean v0, p0, LX/Bcp;->i:Z

    if-eqz v0, :cond_0

    .line 1802815
    :goto_0
    return-void

    .line 1802816
    :cond_0
    monitor-enter p0

    .line 1802817
    :try_start_0
    iget-object v0, p0, LX/Bcp;->d:LX/Bcn;

    if-eqz v0, :cond_1

    .line 1802818
    iget-object v0, p0, LX/Bcp;->d:LX/Bcn;

    invoke-interface {v0}, LX/Bcn;->a()V

    .line 1802819
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802820
    iput-boolean v2, p0, LX/Bcp;->i:Z

    .line 1802821
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    .line 1802822
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 1802823
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v1

    if-lez v1, :cond_2

    .line 1802824
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1802825
    const/4 v2, 0x0

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v3

    invoke-static {v2, v3}, LX/3CY;->a(II)LX/3CY;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802826
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-static {p0, v1, v0}, LX/Bcp;->b(LX/Bcp;LX/0Px;LX/2kM;)V

    goto :goto_0

    .line 1802827
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1802828
    :cond_2
    iput-boolean v2, p0, LX/Bcp;->k:Z

    .line 1802829
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    iget v1, p0, LX/Bcp;->g:I

    invoke-virtual {v0, v1, p1}, LX/2kW;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1802830
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1802831
    invoke-static {p0, p1}, LX/Bcp;->b(LX/Bcp;Z)V

    .line 1802832
    :goto_0
    return-void

    .line 1802833
    :cond_0
    iget-object v0, p0, LX/Bcp;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionChangeSetSequencer$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionChangeSetSequencer$1;-><init>(LX/Bcp;Z)V

    const v2, 0x1df4eaf1

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1802834
    iget-object v0, p0, LX/Bcp;->a:LX/2kW;

    .line 1802835
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 1802836
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 1802837
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 1802838
    if-eqz v0, :cond_0

    .line 1802839
    iget v0, p0, LX/Bcp;->f:I

    const/4 v1, 0x0

    .line 1802840
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Bcp;->k:Z

    .line 1802841
    iget-object v2, p0, LX/Bcp;->a:LX/2kW;

    invoke-virtual {v2, v0, v1}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 1802842
    :cond_0
    return-void
.end method
