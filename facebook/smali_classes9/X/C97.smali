.class public final LX/C97;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Z

.field public final synthetic c:LX/C98;


# direct methods
.method public constructor <init>(LX/C98;Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 0

    .prologue
    .line 1853625
    iput-object p1, p0, LX/C97;->c:LX/C98;

    iput-object p2, p0, LX/C97;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-boolean p3, p0, LX/C97;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1853626
    iget-object v0, p0, LX/C97;->c:LX/C98;

    iget-object v0, v0, LX/C98;->a:LX/9A1;

    iget-object v1, p0, LX/C97;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-boolean v2, p0, LX/C97;->b:Z

    .line 1853627
    iget-object v3, v0, LX/9A1;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1EZ;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    .line 1853628
    iget-object p1, v3, LX/1EZ;->m:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0Sh;

    invoke-virtual {p1}, LX/0Sh;->a()V

    .line 1853629
    invoke-virtual {v3, v4}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object p2

    .line 1853630
    if-eqz p2, :cond_0

    .line 1853631
    iget-boolean p1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move p1, p1

    .line 1853632
    if-ne p1, v2, :cond_2

    .line 1853633
    :cond_0
    :goto_0
    iget-object v0, p0, LX/C97;->c:LX/C98;

    iget-object v0, v0, LX/C98;->d:LX/CA6;

    if-eqz v0, :cond_1

    .line 1853634
    iget-object v0, p0, LX/C97;->c:LX/C98;

    iget-object v0, v0, LX/C98;->d:LX/CA6;

    iget-boolean v1, p0, LX/C97;->b:Z

    invoke-virtual {v0, v1}, LX/CA6;->setStoryIsWaitingForWifi(Z)V

    .line 1853635
    :cond_1
    return-void

    .line 1853636
    :cond_2
    iput-boolean v2, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    .line 1853637
    iget-object p1, v3, LX/1EZ;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0cW;

    .line 1853638
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1853639
    const-string v1, "_upload_operation"

    invoke-static {p1, v0, v1}, LX/0cW;->b(LX/0cW;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1853640
    invoke-static {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->b(Ljava/io/File;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 1853641
    if-eqz v1, :cond_3

    .line 1853642
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1853643
    if-eqz v4, :cond_3

    .line 1853644
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v4

    .line 1853645
    iget-object v4, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1853646
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1853647
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1853648
    invoke-virtual {p2, v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Ljava/io/File;)Z

    .line 1853649
    :cond_3
    iget-object v0, p1, LX/0cW;->o:LX/0cY;

    invoke-static {p2, v0}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0cY;)V

    .line 1853650
    iget-object v0, p1, LX/0cW;->n:LX/0cY;

    invoke-static {p2, v0}, LX/0cW;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0cY;)V

    .line 1853651
    if-eqz v2, :cond_4

    .line 1853652
    invoke-static {v3, p2}, LX/1EZ;->n(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0

    .line 1853653
    :cond_4
    iget-boolean p1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move p1, p1

    .line 1853654
    if-nez p1, :cond_5

    iget-object p1, v3, LX/1EZ;->x:Ljava/util/Map;

    .line 1853655
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1853656
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 1853657
    :cond_5
    :goto_1
    goto :goto_0

    .line 1853658
    :cond_6
    iget-object p1, v3, LX/1EZ;->x:Ljava/util/Map;

    .line 1853659
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1853660
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1853661
    iget-object v0, v3, LX/1EZ;->x:Ljava/util/Map;

    .line 1853662
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object p1, v1

    .line 1853663
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1853664
    sget-object p1, LX/8Kx;->Resume:LX/8Kx;

    const-string v0, "Wi-Fi connected retry"

    invoke-virtual {v3, p2, p1, v0}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    goto :goto_1
.end method
