.class public final LX/Aws;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

.field public m:Lcom/facebook/drawee/view/DraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1727261
    iput-object p1, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    .line 1727262
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1727263
    const v0, 0x7f0d1b76

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    .line 1727264
    iget-object v0, p0, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1727265
    iget-object v0, p0, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1727266
    iget-object v0, p0, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    .line 1727267
    iget-object v1, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->h:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 1727268
    iget-object v1, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object v2, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->g:Landroid/content/res/Resources;

    const p1, 0x7f021af6

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1727269
    iput-object v2, v1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->h:Landroid/graphics/drawable/Drawable;

    .line 1727270
    :cond_0
    new-instance v1, LX/1Uo;

    iget-object v2, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->g:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object p1, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object p1, p1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/16 p2, 0x3e8

    invoke-direct {v2, p1, p2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1727271
    iput-object v2, v1, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1727272
    move-object v1, v1

    .line 1727273
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    move-object v1, v1

    .line 1727274
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1727275
    return-void
.end method

.method public static b(LX/Aws;Ljava/lang/String;)LX/1aZ;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1727276
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    iget-object v2, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget v2, v2, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->f:I

    iget-object v3, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget v3, v3, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->f:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1727277
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1727278
    move-object v0, v0

    .line 1727279
    invoke-virtual {v0, v4}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1727280
    iget-object v1, p0, LX/Aws;->l:Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->d:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1727281
    return-object v0
.end method
