.class public LX/BcP;
.super LX/1De;
.source ""


# instance fields
.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/BcY;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/BcO;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/BcQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1De;)V
    .locals 1

    .prologue
    .line 1801913
    invoke-virtual {p1}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 1801914
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1801915
    invoke-direct {p0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 1801916
    return-void
.end method

.method public static a(LX/BcP;LX/BcO;)LX/BcP;
    .locals 2

    .prologue
    .line 1801917
    new-instance v0, LX/BcP;

    invoke-direct {v0, p0}, LX/BcP;-><init>(LX/1De;)V

    .line 1801918
    iget-object v1, p0, LX/BcP;->c:Ljava/lang/ref/WeakReference;

    iput-object v1, v0, LX/BcP;->c:Ljava/lang/ref/WeakReference;

    .line 1801919
    iget-object v1, p0, LX/BcP;->e:LX/BcQ;

    iput-object v1, v0, LX/BcP;->e:LX/BcQ;

    .line 1801920
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/BcP;->d:Ljava/lang/ref/WeakReference;

    .line 1801921
    return-object v0
.end method


# virtual methods
.method public final a(LX/BcR;)V
    .locals 2

    .prologue
    .line 1801922
    iget-object v0, p0, LX/BcP;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801923
    iget-object v1, p0, LX/BcP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcY;

    .line 1801924
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1801925
    :cond_0
    :goto_0
    return-void

    .line 1801926
    :cond_1
    iget-object p0, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v0, p0

    .line 1801927
    invoke-virtual {v1, v0, p1}, LX/BcY;->a(Ljava/lang/String;LX/BcR;)V

    goto :goto_0
.end method

.method public final b(I[Ljava/lang/Object;)LX/BcQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I[",
            "Ljava/lang/Object;",
            ")",
            "LX/BcQ",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1801928
    iget-object v0, p0, LX/BcP;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801929
    if-nez v0, :cond_0

    .line 1801930
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called newEventHandler on a released Section"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1801931
    :cond_0
    new-instance v1, LX/BcQ;

    invoke-direct {v1, v0, p1, p2}, LX/BcQ;-><init>(LX/BcO;I[Ljava/lang/Object;)V

    return-object v1
.end method

.method public final i()LX/BcO;
    .locals 1

    .prologue
    .line 1801932
    iget-object v0, p0, LX/BcP;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801933
    return-object v0
.end method
