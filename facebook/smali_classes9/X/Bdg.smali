.class public LX/Bdg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bdb;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1803967
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    invoke-direct {p0, v0, v1, v2}, LX/Bdg;-><init>(IZI)V

    .line 1803968
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    .prologue
    .line 1803971
    const/high16 v0, -0x80000000

    invoke-direct {p0, p1, p2, v0}, LX/Bdg;-><init>(IZI)V

    .line 1803972
    return-void
.end method

.method public constructor <init>(IZI)V
    .locals 0

    .prologue
    .line 1803973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1803974
    iput p1, p0, LX/Bdg;->a:I

    .line 1803975
    iput-boolean p2, p0, LX/Bdg;->b:Z

    .line 1803976
    iput p3, p0, LX/Bdg;->c:I

    .line 1803977
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/5Je;
    .locals 4

    .prologue
    .line 1803970
    new-instance v0, LX/Bdf;

    iget v1, p0, LX/Bdg;->a:I

    iget-boolean v2, p0, LX/Bdg;->b:Z

    iget v3, p0, LX/Bdg;->c:I

    invoke-direct {v0, p1, v1, v2, v3}, LX/Bdf;-><init>(Landroid/content/Context;IZI)V

    return-object v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1803969
    iget v0, p0, LX/Bdg;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
