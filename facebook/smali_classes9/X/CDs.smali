.class public final LX/CDs;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/CDt;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1860181
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1860182
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "photoUrl"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CDs;->b:[Ljava/lang/String;

    .line 1860183
    iput v3, p0, LX/CDs;->c:I

    .line 1860184
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CDs;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CDs;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CDs;LX/1De;IILcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;)V
    .locals 1

    .prologue
    .line 1860157
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1860158
    iput-object p4, p0, LX/CDs;->a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    .line 1860159
    iget-object v0, p0, LX/CDs;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1860160
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/CDs;
    .locals 2

    .prologue
    .line 1860178
    iget-object v0, p0, LX/CDs;->a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1860179
    iget-object v0, p0, LX/CDs;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860180
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1860174
    invoke-super {p0}, LX/1X5;->a()V

    .line 1860175
    const/4 v0, 0x0

    iput-object v0, p0, LX/CDs;->a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    .line 1860176
    sget-object v0, LX/CDt;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1860177
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/CDs;
    .locals 2

    .prologue
    .line 1860171
    iget-object v0, p0, LX/CDs;->a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    .line 1860172
    iget-object v0, p0, LX/CDs;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860173
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/CDt;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1860161
    iget-object v1, p0, LX/CDs;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CDs;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CDs;->c:I

    if-ge v1, v2, :cond_2

    .line 1860162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1860163
    :goto_0
    iget v2, p0, LX/CDs;->c:I

    if-ge v0, v2, :cond_1

    .line 1860164
    iget-object v2, p0, LX/CDs;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1860165
    iget-object v2, p0, LX/CDs;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1860166
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1860167
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1860168
    :cond_2
    iget-object v0, p0, LX/CDs;->a:Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    .line 1860169
    invoke-virtual {p0}, LX/CDs;->a()V

    .line 1860170
    return-object v0
.end method
