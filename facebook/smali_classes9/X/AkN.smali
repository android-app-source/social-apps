.class public LX/AkN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Landroid/content/ComponentName;

.field public final c:LX/1Kf;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/1Kf;)V
    .locals 0
    .param p2    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709000
    iput-object p1, p0, LX/AkN;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1709001
    iput-object p2, p0, LX/AkN;->b:Landroid/content/ComponentName;

    .line 1709002
    iput-object p3, p0, LX/AkN;->c:LX/1Kf;

    .line 1709003
    return-void
.end method

.method public static b(LX/0QB;)LX/AkN;
    .locals 4

    .prologue
    .line 1709008
    new-instance v3, LX/AkN;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/2lB;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-direct {v3, v0, v1, v2}, LX/AkN;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/ComponentName;LX/1Kf;)V

    .line 1709009
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1709004
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/AkN;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1709005
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->WORK_GROUPS_TAB:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1709006
    iget-object v1, p0, LX/AkN;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1709007
    return-void
.end method
