.class public final enum LX/BOc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BOc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BOc;

.field public static final enum INVITE:LX/BOc;

.field public static final enum NONE:LX/BOc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1779919
    new-instance v0, LX/BOc;

    const-string v1, "INVITE"

    invoke-direct {v0, v1, v2}, LX/BOc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOc;->INVITE:LX/BOc;

    .line 1779920
    new-instance v0, LX/BOc;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/BOc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOc;->NONE:LX/BOc;

    .line 1779921
    const/4 v0, 0x2

    new-array v0, v0, [LX/BOc;

    sget-object v1, LX/BOc;->INVITE:LX/BOc;

    aput-object v1, v0, v2

    sget-object v1, LX/BOc;->NONE:LX/BOc;

    aput-object v1, v0, v3

    sput-object v0, LX/BOc;->$VALUES:[LX/BOc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1779924
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/BOc;
    .locals 1

    .prologue
    .line 1779925
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1779926
    sget-object v0, LX/BOc;->NONE:LX/BOc;

    .line 1779927
    :goto_0
    return-object v0

    .line 1779928
    :cond_0
    const-string v0, "invite"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1779929
    sget-object v0, LX/BOc;->INVITE:LX/BOc;

    goto :goto_0

    .line 1779930
    :cond_1
    sget-object v0, LX/BOc;->NONE:LX/BOc;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BOc;
    .locals 1

    .prologue
    .line 1779923
    const-class v0, LX/BOc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BOc;

    return-object v0
.end method

.method public static values()[LX/BOc;
    .locals 1

    .prologue
    .line 1779922
    sget-object v0, LX/BOc;->$VALUES:[LX/BOc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BOc;

    return-object v0
.end method
