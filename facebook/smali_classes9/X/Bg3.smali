.class public LX/Bg3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field private final c:LX/0tX;

.field private final d:LX/0ad;

.field private final e:LX/31f;

.field private final f:LX/1Ck;

.field private final g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/03V;LX/0tX;LX/0ad;LX/31f;LX/1Ck;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "LX/0ad;",
            "LX/31f;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1806991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1806992
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Bg3;->g:Ljava/util/HashSet;

    .line 1806993
    iput-object p1, p0, LX/Bg3;->a:LX/0Ot;

    .line 1806994
    iput-object p2, p0, LX/Bg3;->b:LX/03V;

    .line 1806995
    iput-object p3, p0, LX/Bg3;->c:LX/0tX;

    .line 1806996
    iput-object p4, p0, LX/Bg3;->d:LX/0ad;

    .line 1806997
    iput-object p5, p0, LX/Bg3;->e:LX/31f;

    .line 1806998
    iput-object p6, p0, LX/Bg3;->f:LX/1Ck;

    .line 1806999
    return-void
.end method
