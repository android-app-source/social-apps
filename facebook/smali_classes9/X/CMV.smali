.class public LX/CMV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/CMW;


# direct methods
.method public constructor <init>(LX/CMW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880715
    iput-object p1, p0, LX/CMV;->a:LX/CMW;

    .line 1880716
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 1880717
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1880718
    const-string v1, "fetch_recent_emoji"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1880719
    iget-object v0, p0, LX/CMV;->a:LX/CMW;

    .line 1880720
    iget-object v1, v0, LX/CMW;->a:LX/6e7;

    .line 1880721
    iget-object p0, v1, LX/6e7;->a:LX/0Px;

    move-object v1, p0

    .line 1880722
    if-nez v1, :cond_0

    .line 1880723
    iget-object v1, v0, LX/CMW;->b:LX/6eC;

    invoke-virtual {v1}, LX/6eC;->a()LX/0Px;

    move-result-object v1

    .line 1880724
    iget-object p0, v0, LX/CMW;->a:LX/6e7;

    invoke-virtual {p0, v1}, LX/6e7;->a(Ljava/util/List;)V

    .line 1880725
    :cond_0
    new-instance p0, Lcom/facebook/messaging/emoji/service/FetchRecentEmojiResult;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/emoji/service/FetchRecentEmojiResult;-><init>(Ljava/util/List;)V

    move-object v0, p0

    .line 1880726
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1880727
    :goto_0
    return-object v0

    .line 1880728
    :cond_1
    const-string v1, "update_recent_emoji"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1880729
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1880730
    const-string v1, "emoji"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880731
    iget-object v1, p0, LX/CMV;->a:LX/CMW;

    const/16 p1, 0x2d

    .line 1880732
    iget-object v2, v1, LX/CMW;->b:LX/6eC;

    invoke-virtual {v2}, LX/6eC;->a()LX/0Px;

    move-result-object v3

    .line 1880733
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-virtual {v2, v0}, Lcom/facebook/ui/emoji/model/Emoji;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1880734
    iget-object v2, v1, LX/CMW;->a:LX/6e7;

    .line 1880735
    iget-object v4, v2, LX/6e7;->a:LX/0Px;

    move-object v2, v4

    .line 1880736
    if-nez v2, :cond_2

    .line 1880737
    iget-object v2, v1, LX/CMW;->a:LX/6e7;

    invoke-virtual {v2, v3}, LX/6e7;->a(Ljava/util/List;)V

    .line 1880738
    :cond_2
    :goto_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1880739
    move-object v0, v0

    .line 1880740
    goto :goto_0

    .line 1880741
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1880742
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1880743
    invoke-static {v2}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1880744
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1880745
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880746
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result p0

    if-ge p0, p1, :cond_6

    .line 1880747
    invoke-virtual {v2, v0}, Lcom/facebook/ui/emoji/model/Emoji;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    .line 1880748
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1880749
    :cond_6
    iget-object v2, v1, LX/CMW;->b:LX/6eC;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6eC;->a(Ljava/util/List;)V

    .line 1880750
    iget-object v2, v1, LX/CMW;->a:LX/6e7;

    invoke-virtual {v2, v4}, LX/6e7;->a(Ljava/util/List;)V

    goto :goto_1
.end method
