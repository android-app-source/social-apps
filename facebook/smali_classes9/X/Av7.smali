.class public final LX/Av7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0is;

.field public final synthetic c:LX/Av8;


# direct methods
.method public constructor <init>(LX/Av8;LX/0Px;LX/0is;)V
    .locals 0

    .prologue
    .line 1723744
    iput-object p1, p0, LX/Av7;->c:LX/Av8;

    iput-object p2, p0, LX/Av7;->a:LX/0Px;

    iput-object p3, p0, LX/Av7;->b:LX/0is;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1723729
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 1723730
    iget-object v0, p0, LX/Av7;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Av7;->b:LX/0is;

    invoke-static {p1, v0}, LX/Av8;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Av7;->a:LX/0Px;

    const/4 v5, 0x0

    .line 1723731
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1723732
    :cond_1
    :goto_1
    move-object v0, p1

    .line 1723733
    goto :goto_0

    .line 1723734
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1723735
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_2
    if-ge v6, v9, :cond_5

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1723736
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v10

    move v4, v5

    move v3, v5

    :goto_3
    if-ge v4, v10, :cond_3

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1723737
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1723738
    const/4 v2, 0x1

    .line 1723739
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_3

    .line 1723740
    :cond_3
    if-nez v3, :cond_4

    .line 1723741
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1723742
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_2

    .line 1723743
    :cond_5
    invoke-static {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v1

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object p1

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_4
.end method
