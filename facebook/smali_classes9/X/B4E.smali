.class public final LX/B4E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

.field public final synthetic b:LX/B4H;


# direct methods
.method public constructor <init>(LX/B4H;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)V
    .locals 0

    .prologue
    .line 1741418
    iput-object p1, p0, LX/B4E;->b:LX/B4H;

    iput-object p2, p0, LX/B4E;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1741419
    iget-object v0, p0, LX/B4E;->b:LX/B4H;

    iget-object v0, v0, LX/B4H;->d:LX/8GV;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, LX/B4E;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1741420
    iget-object v3, v2, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v2, v3

    .line 1741421
    const/4 v3, 0x0

    iget-object v4, p0, LX/B4E;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1741422
    iget-object v5, v4, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v4, v5

    .line 1741423
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
