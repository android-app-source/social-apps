.class public final LX/B0d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1734084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734085
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/B0d;->a:Ljava/lang/String;

    .line 1734086
    iput-object p2, p0, LX/B0d;->b:Ljava/lang/String;

    .line 1734087
    iput-boolean p3, p0, LX/B0d;->c:Z

    .line 1734088
    return-void
.end method

.method public static a(J)LX/B0d;
    .locals 4

    .prologue
    .line 1734089
    new-instance v0, LX/B0d;

    invoke-static {p0, p1}, LX/2nU;->a(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LX/B0d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static a(LX/B0d;Ljava/lang/String;Z)LX/B0d;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1734090
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1734091
    new-instance v1, LX/B0d;

    iget-object v0, p0, LX/B0d;->a:Ljava/lang/String;

    invoke-static {v0}, LX/2nU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, p1, v0}, LX/B0d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/B0d;I)Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 6

    .prologue
    .line 1734092
    iget-object v0, p0, LX/B0d;->a:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, LX/B0d;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iget-boolean v4, p0, LX/B0d;->c:Z

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    return-object v0
.end method
