.class public final LX/CMy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:LX/2Mt;


# direct methods
.method public constructor <init>(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 1881197
    iput-object p1, p0, LX/CMy;->b:LX/2Mt;

    iput-object p2, p0, LX/CMy;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1881198
    iget-object v0, p0, LX/CMy;->b:LX/2Mt;

    iget-object v0, v0, LX/2Mt;->b:LX/18V;

    iget-object v1, p0, LX/CMy;->b:LX/2Mt;

    iget-object v1, v1, LX/2Mt;->g:LX/2Mu;

    iget-object v2, p0, LX/CMy;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;

    return-object v0
.end method
