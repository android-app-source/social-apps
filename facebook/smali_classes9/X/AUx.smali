.class public final LX/AUx;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/StringBuilder;

.field public final synthetic b:Ljava/lang/Exception;

.field public final synthetic c:Landroid/hardware/camera2/CameraManager;

.field public final synthetic d:Ljava/util/Iterator;

.field public final synthetic e:LX/AV1;


# direct methods
.method public constructor <init>(LX/AV1;Ljava/lang/StringBuilder;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 1678484
    iput-object p1, p0, LX/AUx;->e:LX/AV1;

    iput-object p2, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    iput-object p3, p0, LX/AUx;->b:Ljava/lang/Exception;

    iput-object p4, p0, LX/AUx;->c:Landroid/hardware/camera2/CameraManager;

    iput-object p5, p0, LX/AUx;->d:Ljava/util/Iterator;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 0

    .prologue
    .line 1678485
    return-void
.end method

.method public final onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 5

    .prologue
    .line 1678486
    iget-object v0, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    const-string v1, "{%1$s-[error:%2$d]},"

    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678487
    iget-object v0, p0, LX/AUx;->e:LX/AV1;

    iget-object v1, p0, LX/AUx;->b:Ljava/lang/Exception;

    iget-object v2, p0, LX/AUx;->c:Landroid/hardware/camera2/CameraManager;

    iget-object v3, p0, LX/AUx;->d:Ljava/util/Iterator;

    iget-object v4, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    invoke-static {v0, v1, v2, v3, v4}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;Ljava/lang/StringBuilder;)V

    .line 1678488
    return-void
.end method

.method public final onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 4

    .prologue
    invoke-static {p1}, LX/0Kw;->a(Landroid/hardware/camera2/CameraDevice;)V

    .line 1678489
    iget-object v0, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    const-string v1, "{%1$s-[opened]},"

    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678490
    :try_start_0
    invoke-static {p1}, LX/0J3;->a(Landroid/hardware/camera2/CameraDevice;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678491
    iget-object v0, p0, LX/AUx;->e:LX/AV1;

    iget-object v1, p0, LX/AUx;->b:Ljava/lang/Exception;

    iget-object v2, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1678492
    return-void

    .line 1678493
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/AUx;->e:LX/AV1;

    iget-object v2, p0, LX/AUx;->b:Ljava/lang/Exception;

    iget-object v3, p0, LX/AUx;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V

    throw v0
.end method
