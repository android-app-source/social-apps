.class public LX/CA2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0qn;

.field private final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0ad;LX/0qn;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854871
    iput-object p1, p0, LX/CA2;->a:LX/0ad;

    .line 1854872
    iput-object p2, p0, LX/CA2;->b:LX/0qn;

    .line 1854873
    iput-object p3, p0, LX/CA2;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1854874
    return-void
.end method

.method public static b(LX/0QB;)LX/CA2;
    .locals 4

    .prologue
    .line 1854882
    new-instance v3, LX/CA2;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v1

    check-cast v1, LX/0qn;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v3, v0, v1, v2}, LX/CA2;-><init>(LX/0ad;LX/0qn;Lcom/facebook/content/SecureContextHelper;)V

    .line 1854883
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1854884
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1854885
    sget-object v1, LX/8Lr;->OPTIMISTIC_STORY:LX/8Lr;

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;

    move-result-object v1

    .line 1854886
    iget-object v2, p0, LX/CA2;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1854887
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1854876
    invoke-static {p1}, LX/C9e;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/CA2;->a:LX/0ad;

    sget-short v2, LX/1aO;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1854877
    :cond_0
    :goto_0
    return v0

    .line 1854878
    :cond_1
    iget-object v1, p0, LX/CA2;->a:LX/0ad;

    sget-short v2, LX/1aO;->al:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 1854879
    if-eqz v1, :cond_0

    .line 1854880
    iget-object v1, p0, LX/CA2;->b:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    move v1, v1

    .line 1854881
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1854875
    iget-object v0, p0, LX/CA2;->a:LX/0ad;

    sget-short v1, LX/1aO;->af:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
