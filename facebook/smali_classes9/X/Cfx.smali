.class public LX/Cfx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Cfk;

.field private final b:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Cfx;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1927042
    invoke-direct {p0, p1, v0, v0, v0}, LX/Cfx;-><init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;LX/0Px;)V

    .line 1927043
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/Cfx;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1927044
    invoke-direct {p0, p1, v0, v0, p2}, LX/Cfx;-><init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;LX/0Px;)V

    .line 1927045
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V
    .locals 1

    .prologue
    .line 1927046
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/Cfx;-><init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;LX/0Px;)V

    .line 1927047
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/Cfk;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/Cfk;",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;",
            "LX/0Px",
            "<",
            "LX/Cfx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1927048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1927049
    iput-object p1, p0, LX/Cfx;->d:Ljava/lang/String;

    .line 1927050
    iput-object p2, p0, LX/Cfx;->a:LX/Cfk;

    .line 1927051
    iput-object p3, p0, LX/Cfx;->b:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 1927052
    iput-object p4, p0, LX/Cfx;->c:LX/0Px;

    .line 1927053
    return-void
.end method
