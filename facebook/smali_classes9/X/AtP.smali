.class public LX/AtP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/89u;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/86d;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/89u;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/89u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0il;LX/0Px;)V
    .locals 1
    .param p1    # Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            "TServices;",
            "LX/0Px",
            "<",
            "LX/89u;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1721491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721492
    new-instance v0, LX/AtO;

    invoke-direct {v0, p0}, LX/AtO;-><init>(LX/AtP;)V

    iput-object v0, p0, LX/AtP;->c:LX/86d;

    .line 1721493
    iput-object p1, p0, LX/AtP;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1721494
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AtP;->b:Ljava/lang/ref/WeakReference;

    .line 1721495
    iput-object p3, p0, LX/AtP;->d:LX/0Px;

    .line 1721496
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1721497
    iput-object v0, p0, LX/AtP;->e:LX/0Px;

    .line 1721498
    return-void
.end method

.method private a(LX/89u;)Z
    .locals 4

    .prologue
    .line 1721489
    iget-object v0, p0, LX/AtP;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    .line 1721490
    invoke-interface {p1}, LX/89u;->a()LX/86d;

    move-result-object v1

    iget-object v2, p0, LX/AtP;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v3

    invoke-static {v0}, LX/2rf;->a(LX/0io;)Z

    move-result v0

    invoke-interface {v1, v2, v3, v0}, LX/86d;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1721488
    iget-object v0, p0, LX/AtP;->c:LX/86d;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 4

    .prologue
    .line 1721483
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721484
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721485
    invoke-interface {v0, p1}, LX/89u;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V

    .line 1721486
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721487
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1721478
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721479
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721480
    invoke-interface {v0, p1}, LX/89u;->a(Z)V

    .line 1721481
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721482
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1721499
    iget-object v0, p0, LX/AtP;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/AtP;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721500
    invoke-interface {v0}, LX/89u;->b()V

    .line 1721501
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721502
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1721503
    iput-object v0, p0, LX/AtP;->e:LX/0Px;

    .line 1721504
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1721470
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1721471
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721472
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1721473
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1721474
    invoke-interface {v0}, LX/89u;->c()V

    .line 1721475
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721476
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AtP;->e:LX/0Px;

    .line 1721477
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 4

    .prologue
    .line 1721464
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721465
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721466
    invoke-interface {v0}, LX/89u;->d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1721467
    :goto_1
    return-object v0

    .line 1721468
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721469
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e()Landroid/net/Uri;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721456
    const/4 v1, 0x0

    .line 1721457
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721458
    invoke-interface {v0}, LX/89u;->e()Landroid/net/Uri;

    move-result-object v0

    .line 1721459
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1721460
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot have two edited URIs simultaneously."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1721461
    :cond_0
    if-eqz v0, :cond_2

    .line 1721462
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1721463
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final f()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721450
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721451
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721452
    invoke-interface {v0}, LX/89u;->f()Ljava/lang/String;

    move-result-object v0

    .line 1721453
    :goto_1
    return-object v0

    .line 1721454
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721455
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final g()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721444
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721445
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721446
    invoke-interface {v0}, LX/89u;->g()Ljava/lang/String;

    move-result-object v0

    .line 1721447
    :goto_1
    return-object v0

    .line 1721448
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721449
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final h()Landroid/net/Uri;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721438
    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/AtP;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89u;

    .line 1721439
    invoke-direct {p0, v0}, LX/AtP;->a(LX/89u;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721440
    invoke-interface {v0}, LX/89u;->h()Landroid/net/Uri;

    move-result-object v0

    .line 1721441
    :goto_1
    return-object v0

    .line 1721442
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1721443
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
