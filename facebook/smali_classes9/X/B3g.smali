.class public final LX/B3g;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0d;

.field public final synthetic b:LX/B0M;

.field public final synthetic c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;LX/B0M;)V
    .locals 0

    .prologue
    .line 1740681
    iput-object p1, p0, LX/B3g;->c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    iput-object p2, p0, LX/B3g;->a:LX/B0d;

    iput-object p3, p0, LX/B3g;->b:LX/B0M;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1740682
    iget-object v0, p0, LX/B3g;->b:LX/B0M;

    invoke-virtual {v0, p1}, LX/B0M;->a(Ljava/lang/Throwable;)V

    .line 1740683
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1740684
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1740685
    iget-object v0, p0, LX/B3g;->c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    iget-object v1, p0, LX/B3g;->a:LX/B0d;

    .line 1740686
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1740687
    if-eqz p1, :cond_0

    .line 1740688
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1740689
    if-eqz v3, :cond_0

    .line 1740690
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1740691
    check-cast v3, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1740692
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1740693
    check-cast v3, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v3, v7

    .line 1740694
    :goto_0
    move-object v2, v3

    .line 1740695
    move-object v0, v2

    .line 1740696
    if-eqz v0, :cond_1

    .line 1740697
    iget-object v1, p0, LX/B3g;->b:LX/B0M;

    invoke-virtual {v1, v0}, LX/B0M;->a(LX/B0N;)V

    .line 1740698
    iget-object v0, p0, LX/B3g;->b:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1740699
    :cond_1
    return-void

    .line 1740700
    :cond_2
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1740701
    check-cast v3, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v3}, LX/9JH;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;

    .line 1740702
    new-instance v5, LX/9JH;

    invoke-direct {v5}, LX/9JH;-><init>()V

    .line 1740703
    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v8

    .line 1740704
    :goto_1
    if-ge v6, v10, :cond_5

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;

    .line 1740705
    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->j()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->j()Z

    move-result v11

    if-eqz v11, :cond_3

    iget-boolean v11, v0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->f:Z

    if-eqz v11, :cond_4

    .line 1740706
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v11

    invoke-static {v3}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v3

    const/4 v12, 0x6

    invoke-virtual {v5, v11, v3, v7, v12}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1740707
    :cond_4
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    .line 1740708
    :cond_5
    new-instance v3, LX/B0i;

    move-object v6, v1

    invoke-direct/range {v3 .. v8}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0
.end method
