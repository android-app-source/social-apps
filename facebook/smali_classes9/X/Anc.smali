.class public LX/Anc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/17W;

.field public b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/17W;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712726
    iput-object p1, p0, LX/Anc;->a:LX/17W;

    .line 1712727
    iput-object p2, p0, LX/Anc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1712728
    return-void
.end method

.method public static a(LX/0QB;)LX/Anc;
    .locals 5

    .prologue
    .line 1712714
    const-class v1, LX/Anc;

    monitor-enter v1

    .line 1712715
    :try_start_0
    sget-object v0, LX/Anc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712716
    sput-object v2, LX/Anc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712719
    new-instance p0, LX/Anc;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/Anc;-><init>(LX/17W;Lcom/facebook/content/SecureContextHelper;)V

    .line 1712720
    move-object v0, p0

    .line 1712721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Anc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1712729
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1712730
    :cond_0
    :goto_0
    return-void

    .line 1712731
    :cond_1
    iget-object v0, p0, LX/Anc;->a:LX/17W;

    invoke-virtual {v0, p2, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 1712732
    if-nez v0, :cond_0

    .line 1712733
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1712734
    :goto_1
    goto :goto_0

    .line 1712735
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1712736
    iget-object v1, p0, LX/Anc;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method
