.class public LX/AeZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0gX;

.field private final c:LX/0Sh;

.field public final d:LX/AaZ;

.field public e:Z

.field public f:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field private i:LX/AeV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1697883
    const-class v0, LX/AeZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AeZ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gX;LX/0Sh;LX/AaZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1697878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697879
    iput-object p1, p0, LX/AeZ;->b:LX/0gX;

    .line 1697880
    iput-object p2, p0, LX/AeZ;->c:LX/0Sh;

    .line 1697881
    iput-object p3, p0, LX/AeZ;->d:LX/AaZ;

    .line 1697882
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/AeZ;Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)V
    .locals 4

    .prologue
    .line 1697842
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 1697843
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1697844
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1697845
    iget-object v1, p0, LX/AeZ;->i:LX/AeV;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1697846
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1697847
    invoke-static {p1}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1697848
    iget-object v1, p0, LX/AeZ;->i:LX/AeV;

    sget-object v2, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1697849
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 1697870
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeZ;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697871
    iget-boolean v0, p0, LX/AeZ;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1697872
    :goto_0
    monitor-exit p0

    return-void

    .line 1697873
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/AeZ;->f:LX/0gM;

    if-eqz v0, :cond_1

    .line 1697874
    iget-object v0, p0, LX/AeZ;->b:LX/0gX;

    iget-object v1, p0, LX/AeZ;->f:LX/0gM;

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1697875
    const/4 v0, 0x0

    iput-object v0, p0, LX/AeZ;->f:LX/0gM;

    .line 1697876
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AeZ;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1697877
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/AeV;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1697850
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeZ;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697851
    iput-object p1, p0, LX/AeZ;->i:LX/AeV;

    .line 1697852
    iput-object p2, p0, LX/AeZ;->g:Ljava/lang/String;

    .line 1697853
    iget-object v0, p0, LX/AeZ;->i:LX/AeV;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AeZ;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1697854
    iget-boolean v0, p0, LX/AeZ;->h:Z

    if-eqz v0, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697855
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1697856
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1697857
    :cond_1
    :try_start_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AeZ;->h:Z

    .line 1697858
    iget-object v0, p0, LX/AeZ;->g:Ljava/lang/String;

    .line 1697859
    new-instance v1, LX/AeY;

    invoke-direct {v1, p0}, LX/AeY;-><init>(LX/AeZ;)V

    move-object v2, v1

    .line 1697860
    new-instance v1, LX/6SK;

    invoke-direct {v1}, LX/6SK;-><init>()V

    move-object p1, v1

    .line 1697861
    new-instance v1, LX/4Gl;

    invoke-direct {v1}, LX/4Gl;-><init>()V

    .line 1697862
    const-string p2, "feedback_id"

    invoke-virtual {v1, p2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697863
    move-object v1, v1

    .line 1697864
    const-string p2, "input"

    invoke-virtual {p1, p2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1697865
    const-string p2, "translation_enabled"

    iget-object v1, p0, LX/AeZ;->d:LX/AaZ;

    invoke-virtual {v1}, LX/AaZ;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/AeZ;->e:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, p2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1697866
    :try_start_2
    iget-object v1, p0, LX/AeZ;->b:LX/0gX;

    invoke-virtual {v1, p1, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v1

    iput-object v1, p0, LX/AeZ;->f:LX/0gM;
    :try_end_2
    .catch LX/31B; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1697867
    :goto_2
    goto :goto_0

    .line 1697868
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1697869
    :catch_0
    goto :goto_2
.end method
