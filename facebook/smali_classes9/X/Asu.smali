.class public LX/Asu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89e;


# instance fields
.field public final a:LX/8GP;

.field private final b:LX/Ara;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/ArZ;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/Jvf;

.field private f:Landroid/view/View;

.field public g:I

.field private h:I

.field public i:Z

.field private j:Landroid/view/ViewParent;

.field private k:Landroid/view/View$OnClickListener;

.field public l:I


# direct methods
.method public constructor <init>(LX/Ara;LX/8GP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1721092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721093
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1721094
    iput-object v0, p0, LX/Asu;->c:LX/0Px;

    .line 1721095
    iput-object p1, p0, LX/Asu;->b:LX/Ara;

    .line 1721096
    iput-object p2, p0, LX/Asu;->a:LX/8GP;

    .line 1721097
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1721070
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    if-nez v0, :cond_0

    .line 1721071
    iget-object v0, p0, LX/Asu;->b:LX/Ara;

    iget-object v1, p0, LX/Asu;->f:Landroid/view/View;

    .line 1721072
    new-instance v4, LX/ArZ;

    const-class v2, LX/9dJ;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9dJ;

    invoke-static {v0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v3

    check-cast v3, LX/8GN;

    invoke-direct {v4, v2, v3, v1}, LX/ArZ;-><init>(LX/9dJ;LX/8GN;Landroid/view/View;)V

    .line 1721073
    move-object v0, v4

    .line 1721074
    iput-object v0, p0, LX/Asu;->d:LX/ArZ;

    .line 1721075
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    iget v1, p0, LX/Asu;->g:I

    .line 1721076
    iput v1, v0, LX/ArZ;->i:I

    .line 1721077
    iget-object v2, v0, LX/ArZ;->b:LX/9dI;

    iget-object v3, v0, LX/ArZ;->a:Landroid/view/View;

    iget-object v4, v0, LX/ArZ;->d:LX/9cu;

    invoke-virtual {v2, v3, v4}, LX/9dI;->a(Landroid/view/View;LX/9cu;)V

    .line 1721078
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    const/4 v1, 0x1

    iget-object v2, p0, LX/Asu;->j:Landroid/view/ViewParent;

    .line 1721079
    iget-object v3, v0, LX/ArZ;->b:LX/9dI;

    invoke-virtual {v3, v1, v2}, LX/9dI;->a(ZLandroid/view/ViewParent;)V

    .line 1721080
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    iget-object v1, p0, LX/Asu;->k:Landroid/view/View$OnClickListener;

    .line 1721081
    iget-object v2, v0, LX/ArZ;->b:LX/9dI;

    .line 1721082
    iput-object v1, v2, LX/9dI;->w:Landroid/view/View$OnClickListener;

    .line 1721083
    :cond_0
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    .line 1721084
    iget-object v1, p0, LX/Asu;->a:LX/8GP;

    iget v2, p0, LX/Asu;->g:I

    iget v3, p0, LX/Asu;->l:I

    invoke-virtual {v1, v2, v3}, LX/8GP;->a(II)LX/8GO;

    move-result-object v1

    iget-object v2, p0, LX/Asu;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/8GO;->c(LX/0Px;)LX/8GO;

    move-result-object v1

    .line 1721085
    iget-boolean v2, p0, LX/Asu;->i:Z

    if-nez v2, :cond_1

    .line 1721086
    invoke-virtual {v1}, LX/8GO;->a()LX/8GO;

    .line 1721087
    :cond_1
    invoke-virtual {v1}, LX/8GO;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1721088
    iget-object v2, p0, LX/Asu;->d:LX/ArZ;

    invoke-virtual {v2}, LX/ArZ;->a()Ljava/lang/String;

    move-result-object v2

    .line 1721089
    iput-object v1, v0, LX/ArZ;->e:LX/0Px;

    .line 1721090
    invoke-virtual {v0, v2}, LX/ArZ;->a(Ljava/lang/String;)V

    .line 1721091
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721067
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    if-nez v0, :cond_0

    .line 1721068
    const/4 v0, 0x0

    .line 1721069
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    invoke-virtual {v0}, LX/ArZ;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IIIZ)V
    .locals 2

    .prologue
    .line 1721062
    iput p1, p0, LX/Asu;->g:I

    .line 1721063
    iget v0, p0, LX/Asu;->g:I

    int-to-float v0, v0

    sget-object v1, LX/89U;->PORTRAIT_4_3:LX/89U;

    invoke-virtual {v1}, LX/89U;->getValue()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/Asu;->h:I

    .line 1721064
    iput-boolean p4, p0, LX/Asu;->i:Z

    .line 1721065
    invoke-direct {p0}, LX/Asu;->c()V

    .line 1721066
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1721047
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/Asu;->c:LX/0Px;

    .line 1721048
    iget v0, p0, LX/Asu;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/Asu;->l:I

    if-eqz v0, :cond_0

    .line 1721049
    invoke-direct {p0}, LX/Asu;->c()V

    .line 1721050
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1721051
    iget-object v1, p0, LX/Asu;->d:LX/ArZ;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ArZ;->a(Ljava/lang/String;)V

    .line 1721052
    :cond_0
    return-void
.end method

.method public final a(LX/Jvf;)V
    .locals 0

    .prologue
    .line 1721060
    iput-object p1, p0, LX/Asu;->e:LX/Jvf;

    .line 1721061
    return-void
.end method

.method public final a(Landroid/view/View;LX/0Px;)V
    .locals 1
    .param p2    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1721058
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/Asu;->f:Landroid/view/View;

    .line 1721059
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1721053
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    if-nez v0, :cond_0

    .line 1721054
    const/4 v0, 0x0

    .line 1721055
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Asu;->d:LX/ArZ;

    .line 1721056
    iget-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :goto_1
    move-object v0, p0

    .line 1721057
    goto :goto_0

    :cond_1
    iget-object p0, v0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object p0

    goto :goto_1
.end method
