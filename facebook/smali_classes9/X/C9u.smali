.class public LX/C9u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/16I;

.field public final b:LX/9A1;


# direct methods
.method public constructor <init>(LX/16I;LX/9A1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854732
    iput-object p1, p0, LX/C9u;->a:LX/16I;

    .line 1854733
    iput-object p2, p0, LX/C9u;->b:LX/9A1;

    .line 1854734
    return-void
.end method

.method public static a(LX/0QB;)LX/C9u;
    .locals 5

    .prologue
    .line 1854735
    const-class v1, LX/C9u;

    monitor-enter v1

    .line 1854736
    :try_start_0
    sget-object v0, LX/C9u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854737
    sput-object v2, LX/C9u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854740
    new-instance p0, LX/C9u;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v3

    check-cast v3, LX/16I;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v4

    check-cast v4, LX/9A1;

    invoke-direct {p0, v3, v4}, LX/C9u;-><init>(LX/16I;LX/9A1;)V

    .line 1854741
    move-object v0, p0

    .line 1854742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
