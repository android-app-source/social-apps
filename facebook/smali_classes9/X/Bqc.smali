.class public final LX/Bqc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bqe;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/text/style/ClickableSpan;

.field public b:Ljava/lang/Boolean;

.field public final synthetic c:LX/Bqe;


# direct methods
.method public constructor <init>(LX/Bqe;)V
    .locals 1

    .prologue
    .line 1824846
    iput-object p1, p0, LX/Bqc;->c:LX/Bqe;

    .line 1824847
    move-object v0, p1

    .line 1824848
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1824849
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1824864
    const-string v0, "BoostPostOfflineSubmissionMessageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1824850
    if-ne p0, p1, :cond_1

    .line 1824851
    :cond_0
    :goto_0
    return v0

    .line 1824852
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1824853
    goto :goto_0

    .line 1824854
    :cond_3
    check-cast p1, LX/Bqc;

    .line 1824855
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1824856
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1824857
    if-eq v2, v3, :cond_0

    .line 1824858
    iget-object v2, p0, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    iget-object v3, p1, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1824859
    goto :goto_0

    .line 1824860
    :cond_5
    iget-object v2, p1, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    if-nez v2, :cond_4

    .line 1824861
    :cond_6
    iget-object v2, p0, LX/Bqc;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Bqc;->b:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Bqc;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1824862
    goto :goto_0

    .line 1824863
    :cond_7
    iget-object v2, p1, LX/Bqc;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
