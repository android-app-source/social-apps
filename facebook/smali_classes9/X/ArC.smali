.class public final LX/ArC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "LX/0it;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsUserHasInteracted",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$SetsInspirationDoodleParams",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

.field private final b:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0il;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;)V"
        }
    .end annotation

    .prologue
    .line 1718683
    iput-object p1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1718684
    iput-object p2, p0, LX/ArC;->b:LX/0il;

    .line 1718685
    return-void
.end method

.method private static a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;TModelData;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1718686
    check-cast p0, LX/0io;

    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1718687
    check-cast p1, LX/0io;

    invoke-interface {p1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v4

    .line 1718688
    if-eqz v0, :cond_0

    if-nez v4, :cond_1

    :cond_0
    move v0, v3

    .line 1718689
    :goto_0
    return v0

    .line 1718690
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1718691
    :goto_1
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1718692
    :cond_2
    if-nez v0, :cond_5

    if-eqz v1, :cond_4

    move v0, v2

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 1718693
    goto :goto_1

    :cond_4
    move v0, v3

    .line 1718694
    goto :goto_0

    :cond_5
    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1718695
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1718696
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    const/4 v4, 0x0

    .line 1718697
    iget-object v0, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    .line 1718698
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isRecordingAtLimit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718699
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsRecordingAtLimit(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    sget-object v3, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    invoke-virtual {v1, v3}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    :cond_0
    move-object v0, p1

    .line 1718700
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    sget-object v1, LX/86t;->PREVIEW:LX/86t;

    invoke-static {v0, v2, v1}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1718701
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    invoke-virtual {v0}, LX/AtM;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1718702
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AtM;->a(Landroid/content/Context;)V

    .line 1718703
    :goto_0
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-static {v0, v4, v1}, LX/87R;->a(LX/0il;ZLX/0jK;)V

    .line 1718704
    :cond_1
    iget-object v0, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0, p1}, LX/ArC;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v2

    move-object v0, p1

    .line 1718705
    check-cast v0, LX/0io;

    iget-object v1, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {v0, v1}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iget-object v1, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    const/4 v4, 0x0

    .line 1718706
    move-object v3, v1

    check-cast v3, LX/0io;

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1718707
    if-nez v3, :cond_a

    move v3, v4

    .line 1718708
    :goto_1
    move v0, v3

    .line 1718709
    if-nez v0, :cond_2

    if-eqz v2, :cond_9

    .line 1718710
    :cond_2
    const/4 v0, 0x0

    .line 1718711
    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v1

    .line 1718712
    if-nez v2, :cond_3

    .line 1718713
    sget-object v2, LX/875;->CAPTURE:LX/875;

    if-ne v1, v2, :cond_8

    .line 1718714
    sget-object v0, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    .line 1718715
    :cond_3
    :goto_2
    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    sget-object v2, LX/86t;->PREVIEW:LX/86t;

    sget-object v3, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-static {v1, v2, v3}, LX/87R;->a(LX/0il;LX/86t;LX/0jK;)V

    .line 1718716
    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v1, v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->d(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V

    .line 1718717
    :cond_4
    :goto_3
    iget-object v0, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1718718
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v2, LX/86q;->UNINITIALIZED:LX/86q;

    if-ne v1, v2, :cond_c

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v2, LX/86q;->READY:LX/86q;

    if-ne v1, v2, :cond_c

    const/4 v1, 0x1

    :goto_4
    move v0, v1

    .line 1718719
    if-eqz v0, :cond_5

    .line 1718720
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v0}, LX/0iO;->a()V

    .line 1718721
    :cond_5
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getLocation()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getLocation()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1718722
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    sget-object v1, LX/ArJ;->FETCH_AFTER_LOCATION_GRANTED:LX/ArJ;

    .line 1718723
    invoke-static {v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->c$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V

    .line 1718724
    :cond_6
    return-void

    .line 1718725
    :cond_7
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    goto/16 :goto_0

    .line 1718726
    :cond_8
    sget-object v2, LX/875;->CAMERA_ROLL:LX/875;

    if-ne v1, v2, :cond_3

    .line 1718727
    sget-object v0, LX/ArJ;->GALLERY_SELECT:LX/ArJ;

    goto :goto_2

    :cond_9
    move-object v0, p1

    .line 1718728
    check-cast v0, LX/0io;

    iget-object v1, p0, LX/ArC;->b:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {v0, v1}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1718729
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1718730
    iget-object v1, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    sget-object v2, LX/86t;->PREVIEW:LX/86t;

    invoke-static {v0, v1, v2}, LX/87R;->a(LX/0jL;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/86t;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1718731
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1718732
    iget-object v0, p0, LX/ArC;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    goto/16 :goto_3

    .line 1718733
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    .line 1718734
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object p2

    .line 1718735
    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v3

    invoke-virtual {p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result p2

    if-eq v3, p2, :cond_b

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_b
    move v3, v4

    goto/16 :goto_1

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_4
.end method
