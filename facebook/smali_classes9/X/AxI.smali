.class public LX/AxI;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/1aS;


# instance fields
.field public j:Lcom/facebook/widget/text/BetterTextView;

.field public k:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1727788
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1727789
    const p1, 0x7f030ac3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1727790
    const p1, 0x7f0d1b7c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/AxI;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 1727791
    const p1, 0x7f0d1b7d

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/AxI;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1727792
    const p1, 0x7f0d1b7e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    iput-object p1, p0, LX/AxI;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    .line 1727793
    return-void
.end method


# virtual methods
.method public getPhotoTray()Landroid/view/View;
    .locals 1

    .prologue
    .line 1727786
    iget-object v0, p0, LX/AxI;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    move-object v0, v0

    .line 1727787
    invoke-virtual {v0}, LX/Ale;->getPhotoTray()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1727784
    iget-object v0, p0, LX/AxI;->k:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1727785
    iget-object v0, p0, LX/AxI;->j:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
