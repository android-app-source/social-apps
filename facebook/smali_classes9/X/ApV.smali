.class public final LX/ApV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ApW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1716131
    invoke-static {}, LX/ApW;->q()LX/ApW;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716132
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716133
    const-string v0, "FigHscrollFooterTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716134
    if-ne p0, p1, :cond_1

    .line 1716135
    :cond_0
    :goto_0
    return v0

    .line 1716136
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716137
    goto :goto_0

    .line 1716138
    :cond_3
    check-cast p1, LX/ApV;

    .line 1716139
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716140
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716141
    if-eq v2, v3, :cond_0

    .line 1716142
    iget-object v2, p0, LX/ApV;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ApV;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApV;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1716143
    goto :goto_0

    .line 1716144
    :cond_5
    iget-object v2, p1, LX/ApV;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1716145
    :cond_6
    iget-object v2, p0, LX/ApV;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ApV;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApV;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1716146
    goto :goto_0

    .line 1716147
    :cond_8
    iget-object v2, p1, LX/ApV;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1716148
    :cond_9
    iget-object v2, p0, LX/ApV;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ApV;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ApV;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1716149
    goto :goto_0

    .line 1716150
    :cond_b
    iget-object v2, p1, LX/ApV;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 1716151
    :cond_c
    iget-boolean v2, p0, LX/ApV;->d:Z

    iget-boolean v3, p1, LX/ApV;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1716152
    goto :goto_0
.end method
