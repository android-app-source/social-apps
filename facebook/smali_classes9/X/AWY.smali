.class public LX/AWY;
.super LX/AWT;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field public a:LX/3Hq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

.field public final c:I

.field public f:LX/AVY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683108
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683106
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683099
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683100
    const-class v0, LX/AWY;

    invoke-static {v0, p0}, LX/AWY;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683101
    const v0, 0x7f0305a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683102
    const v0, 0x7f0d0f71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    iput-object v0, p0, LX/AWY;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;

    .line 1683103
    invoke-virtual {p0}, LX/AWY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0627

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AWY;->c:I

    .line 1683104
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/AWY;

    const-class p0, LX/3Hq;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3Hq;

    iput-object v1, p1, LX/AWY;->a:LX/3Hq;

    return-void
.end method


# virtual methods
.method public setData(D)V
    .locals 13

    .prologue
    .line 1683082
    iget-object v0, p0, LX/AWY;->f:LX/AVY;

    .line 1683083
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 1683084
    const-wide v4, 0x4049800000000000L    # 51.0

    sub-double v4, p1, v4

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    div-double/2addr v4, v6

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 1683085
    invoke-static {v4, v5}, LX/AVY;->d(D)D

    move-result-wide v4

    .line 1683086
    iget-wide v6, v0, LX/AVY;->e:D

    iget-object v8, v0, LX/AVY;->b:[D

    iget v9, v0, LX/AVY;->c:I

    aget-wide v8, v8, v9

    sub-double/2addr v6, v8

    add-double/2addr v6, v4

    iput-wide v6, v0, LX/AVY;->e:D

    .line 1683087
    iget-object v6, v0, LX/AVY;->b:[D

    iget v7, v0, LX/AVY;->c:I

    aput-wide v4, v6, v7

    .line 1683088
    iget v6, v0, LX/AVY;->c:I

    add-int/lit8 v6, v6, 0x1

    rem-int/lit8 v6, v6, 0x64

    iput v6, v0, LX/AVY;->c:I

    .line 1683089
    iget v6, v0, LX/AVY;->d:I

    const/16 v7, 0x64

    if-ge v6, v7, :cond_0

    .line 1683090
    iget v6, v0, LX/AVY;->d:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, LX/AVY;->d:I

    .line 1683091
    :cond_0
    iget-wide v6, v0, LX/AVY;->f:D

    iget-object v8, v0, LX/AVY;->b:[D

    iget v9, v0, LX/AVY;->c:I

    add-int/lit8 v9, v9, -0x1

    add-int/lit8 v9, v9, -0x3

    add-int/lit8 v9, v9, 0x64

    rem-int/lit8 v9, v9, 0x64

    aget-wide v8, v8, v9

    sub-double/2addr v6, v8

    iput-wide v6, v0, LX/AVY;->f:D

    .line 1683092
    iget-wide v6, v0, LX/AVY;->f:D

    add-double/2addr v4, v6

    iput-wide v4, v0, LX/AVY;->f:D

    .line 1683093
    iget-wide v4, v0, LX/AVY;->f:D

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double/2addr v4, v6

    .line 1683094
    iget-wide v6, v0, LX/AVY;->e:D

    iget v8, v0, LX/AVY;->d:I

    int-to-double v8, v8

    div-double/2addr v6, v8

    .line 1683095
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v4

    div-double v6, v8, v6

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 1683096
    const-wide v8, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v6, v8

    const-wide v8, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v4, v8

    add-double/2addr v4, v6

    move-wide v1, v4

    .line 1683097
    invoke-static {v0, v1, v2}, LX/AVY;->c(LX/AVY;D)V

    .line 1683098
    return-void
.end method
