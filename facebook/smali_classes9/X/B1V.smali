.class public LX/B1V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:LX/B1W;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B1W",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/api/feedtype/FeedType;

.field private final c:LX/0pn;


# direct methods
.method public constructor <init>(LX/B1W;Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B1W",
            "<TT;>;",
            "Lcom/facebook/api/feedtype/FeedType;",
            "LX/0pn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1736076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1736077
    iput-object p1, p0, LX/B1V;->a:LX/B1W;

    .line 1736078
    iput-object p2, p0, LX/B1V;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1736079
    iput-object p3, p0, LX/B1V;->c:LX/0pn;

    .line 1736080
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1736081
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1736057
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 1736058
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736059
    if-eqz v0, :cond_2

    .line 1736060
    iget-object v0, p0, LX/B1V;->a:LX/B1W;

    .line 1736061
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1736062
    invoke-interface {v0, v1}, LX/B1W;->a(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1736063
    iget-object v0, p0, LX/B1V;->c:LX/0pn;

    iget-object v1, p0, LX/B1V;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v1}, LX/0pn;->b(Lcom/facebook/api/feedtype/FeedType;)LX/0Px;

    move-result-object v5

    .line 1736064
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1736065
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    move v2, v3

    .line 1736066
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 1736067
    iget-object v1, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1736068
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1736069
    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 1736070
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1736071
    add-int/lit8 v3, v3, 0x1

    .line 1736072
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1736073
    :cond_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1736074
    iget-object v0, p0, LX/B1V;->c:LX/0pn;

    iget-object v1, p0, LX/B1V;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/44w;

    iget-object v2, v2, LX/44w;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-object v3, LX/69H;->LTE:LX/69H;

    const/4 v4, 0x0

    sget-object v5, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-virtual/range {v0 .. v5}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;LX/69H;Ljava/lang/String;LX/37E;)V

    .line 1736075
    :cond_2
    return-void
.end method
