.class public final LX/C1D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;Lcom/facebook/graphql/model/GraphQLQuestionOption;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1842085
    iput-object p1, p0, LX/C1D;->c:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;

    iput-object p2, p0, LX/C1D;->a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    iput-object p3, p0, LX/C1D;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x5a379b68

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1842086
    iget-object v1, p0, LX/C1D;->c:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;

    iget-object v2, p0, LX/C1D;->a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/C1D;->a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v3

    iget-object v4, p0, LX/C1D;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842087
    iget-object v6, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1842088
    move-object v7, v6

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842089
    const-class v6, LX/C1E;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLNode;->hM()Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v6

    move-object v9, v6

    check-cast v9, LX/C1E;

    .line 1842090
    sget-object v6, LX/C1E;->CHOOSE_ONE:LX/C1E;

    invoke-virtual {v9, v6}, LX/C1E;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v3, :cond_0

    .line 1842091
    :goto_0
    const v1, -0x353e8017    # -6340596.5f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1842092
    :cond_0
    iget-object v6, v1, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->c:LX/C1W;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v8

    sget-object v7, LX/C1E;->CHOOSE_ONE:LX/C1E;

    invoke-virtual {v9, v7}, LX/C1E;->equals(Ljava/lang/Object;)Z

    move-result p0

    move-object v7, v2

    move v9, v3

    move-object p1, v4

    .line 1842093
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1842094
    iget-object v2, v6, LX/C1W;->a:LX/189;

    invoke-virtual {v2, p1, v7, v9, p0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZZ)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1842095
    invoke-static {v2}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 1842096
    iget-object v3, v6, LX/C1W;->c:LX/0bH;

    new-instance v4, LX/1Ne;

    invoke-direct {v4, v2}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 1842097
    if-nez v9, :cond_1

    .line 1842098
    iget-object v2, v6, LX/C1W;->e:Ljava/lang/String;

    .line 1842099
    new-instance v3, LX/4J1;

    invoke-direct {v3}, LX/4J1;-><init>()V

    .line 1842100
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842101
    const-string v4, "option_id"

    invoke-virtual {v3, v4, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842102
    const-string v4, "question_id"

    invoke-virtual {v3, v4, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842103
    new-instance v4, LX/81s;

    invoke-direct {v4}, LX/81s;-><init>()V

    move-object v4, v4

    .line 1842104
    const-string v9, "input"

    invoke-virtual {v4, v9, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842105
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1842106
    iget-object v4, v6, LX/C1W;->b:LX/1Ck;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "task_key_update_poll_vote"

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object p0, v6, LX/C1W;->d:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {v6, v1}, LX/C1W;->a(LX/C1W;Lcom/facebook/graphql/model/GraphQLStory;)LX/0Vd;

    move-result-object p0

    invoke-virtual {v4, v9, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1842107
    :goto_1
    goto :goto_0

    .line 1842108
    :cond_1
    iget-object v2, v6, LX/C1W;->e:Ljava/lang/String;

    .line 1842109
    new-instance v3, LX/4J2;

    invoke-direct {v3}, LX/4J2;-><init>()V

    .line 1842110
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842111
    const-string v4, "option_id"

    invoke-virtual {v3, v4, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842112
    const-string v4, "question_id"

    invoke-virtual {v3, v4, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842113
    new-instance v4, LX/81t;

    invoke-direct {v4}, LX/81t;-><init>()V

    move-object v4, v4

    .line 1842114
    const-string v9, "input"

    invoke-virtual {v4, v9, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842115
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1842116
    iget-object v4, v6, LX/C1W;->b:LX/1Ck;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "task_key_update_poll_vote"

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object p0, v6, LX/C1W;->d:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {v6, v1}, LX/C1W;->a(LX/C1W;Lcom/facebook/graphql/model/GraphQLStory;)LX/0Vd;

    move-result-object p0

    invoke-virtual {v4, v9, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1842117
    goto :goto_1
.end method
