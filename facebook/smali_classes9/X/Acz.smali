.class public final LX/Acz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1693356
    iput-object p1, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iput-object p2, p0, LX/Acz;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 1693357
    iget-object v0, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693358
    iget-object v1, v0, LX/Acr;->a:LX/0if;

    sget-object p1, LX/0ig;->D:LX/0ih;

    const-string p2, "click_donate_button_confirm"

    invoke-virtual {v1, p1, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1693359
    iget-object v0, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1693360
    iget-object v0, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1693361
    :cond_0
    iget-object v0, p0, LX/Acz;->b:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v1, p0, LX/Acz;->a:Landroid/content/Context;

    .line 1693362
    const/4 p0, 0x0

    .line 1693363
    iget-object p1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz p1, :cond_1

    .line 1693364
    iget-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->w()Ljava/lang/String;

    move-result-object p0

    .line 1693365
    :cond_1
    if-eqz p0, :cond_2

    iget-object p1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->n:Ljava/lang/String;

    if-nez p1, :cond_3

    .line 1693366
    :cond_2
    :goto_0
    return-void

    .line 1693367
    :cond_3
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 1693368
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    const-string p1, "live_donation_video_id"

    iget-object p2, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->n:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 1693369
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 1693370
    iget-object p1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->e:LX/17W;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
