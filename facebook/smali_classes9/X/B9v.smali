.class public LX/B9v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752341
    iput-object p1, p0, LX/B9v;->a:Landroid/content/Context;

    .line 1752342
    iput-object p2, p0, LX/B9v;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1752343
    iput-object p3, p0, LX/B9v;->c:LX/0Zb;

    .line 1752344
    return-void
.end method

.method public static b(LX/0QB;)LX/B9v;
    .locals 4

    .prologue
    .line 1752345
    new-instance v3, LX/B9v;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v0, v1, v2}, LX/B9v;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)V

    .line 1752346
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1752347
    const-string v0, "divebar"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1752348
    const-string p2, "diode_divebar"

    .line 1752349
    :cond_0
    :goto_0
    iget-object v0, p0, LX/B9v;->c:LX/0Zb;

    if-nez v0, :cond_5

    .line 1752350
    :cond_1
    :goto_1
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1752351
    new-instance v1, Landroid/content/Intent;

    const-string p2, "android.intent.action.VIEW"

    invoke-direct {v1, p2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1752352
    iget-object p2, p0, LX/B9v;->a:Landroid/content/Context;

    instance-of p2, p2, Landroid/app/Activity;

    if-nez p2, :cond_2

    .line 1752353
    const/high16 p2, 0x10000000

    invoke-virtual {v1, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1752354
    :cond_2
    iget-object p2, p0, LX/B9v;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, p0, LX/B9v;->a:Landroid/content/Context;

    invoke-interface {p2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1752355
    return-void

    .line 1752356
    :cond_3
    const-string v0, "inbox_jewel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1752357
    const-string p2, "diode_jewel"

    goto :goto_0

    .line 1752358
    :cond_4
    const-string v0, "timeline"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1752359
    const-string p2, "diode_timeline"

    goto :goto_0

    .line 1752360
    :cond_5
    iget-object v0, p0, LX/B9v;->c:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1752361
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1752362
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_1
.end method
