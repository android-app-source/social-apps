.class public final LX/AYy;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/AYz;


# direct methods
.method public constructor <init>(LX/AYz;J)V
    .locals 2

    .prologue
    .line 1686373
    iput-object p1, p0, LX/AYy;->a:LX/AYz;

    .line 1686374
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1686375
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 0

    .prologue
    .line 1686376
    return-void
.end method

.method public final onTick(J)V
    .locals 13

    .prologue
    const-wide/16 v10, 0xa

    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x3c

    .line 1686377
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    .line 1686378
    div-long v0, v2, v6

    div-long/2addr v0, v10

    cmp-long v0, v0, v8

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    div-long v4, v2, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1686379
    :goto_0
    rem-long v4, v2, v6

    div-long/2addr v4, v10

    cmp-long v1, v4, v8

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "0"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rem-long/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1686380
    :goto_1
    iget-object v2, p0, LX/AYy;->a:LX/AYz;

    iget-object v2, v2, LX/AYz;->j:Lcom/facebook/widget/text/BetterTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686381
    return-void

    .line 1686382
    :cond_0
    div-long v0, v2, v6

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1686383
    :cond_1
    rem-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
