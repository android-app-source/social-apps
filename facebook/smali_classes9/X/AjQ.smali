.class public final LX/AjQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0tW;
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0tW;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1Iz;

.field public c:LX/1J0;

.field public d:Lcom/facebook/api/feed/FetchFeedParams;

.field public e:Z

.field public f:I


# direct methods
.method public constructor <init>(LX/1Iz;)V
    .locals 1

    .prologue
    .line 1707994
    iput-object p1, p0, LX/AjQ;->b:LX/1Iz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707995
    const/4 v0, 0x0

    iput v0, p0, LX/AjQ;->f:I

    .line 1707996
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/AjQ;->a:LX/0Pz;

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 7

    .prologue
    .line 1707927
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/AjQ;->c:LX/1J0;

    iget-wide v4, v2, LX/1J0;->e:J

    const/4 v6, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 1707928
    iget-object v1, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v1, v1, LX/1Iz;->h:LX/0pm;

    invoke-virtual {v1, v0}, LX/0pm;->b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 1707929
    invoke-static {v0}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 1707930
    iget-object v1, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v1, v1, LX/1Iz;->i:LX/187;

    invoke-virtual {v1, v0}, LX/187;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    .line 1707931
    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1707990
    iget v0, p0, LX/AjQ;->f:I

    if-lez v0, :cond_0

    .line 1707991
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->j:LX/1J1;

    iget-object v1, p0, LX/AjQ;->a:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1707992
    const/4 p0, 0x7

    invoke-virtual {v0, p0, v1}, LX/1J1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/1J1;->sendMessage(Landroid/os/Message;)Z

    .line 1707993
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1707986
    const-string v0, "feedback_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1707987
    const/4 p0, 0x0

    .line 1707988
    :goto_0
    return-object p0

    .line 1707989
    :cond_0
    const-string v0, "feed_subscriber"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 1707976
    const-string v0, "NetworkRequestSubscriber.onCompleted"

    const v1, -0x3c7d2606

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1707977
    :try_start_0
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->l:LX/0pV;

    sget-object v1, LX/1Iz;->b:Ljava/lang/String;

    sget-object v2, LX/7zp;->NETWORK_SUCCESS:LX/7zp;

    const-string v3, "inUse"

    iget-boolean v4, p0, LX/AjQ;->e:Z

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 1707978
    iget-boolean v0, p0, LX/AjQ;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1707979
    const v0, 0x901f17e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1707980
    :goto_0
    return-void

    .line 1707981
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/AjQ;->c()V

    .line 1707982
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->j:LX/1J1;

    iget v1, p0, LX/AjQ;->f:I

    const/4 v2, 0x7

    .line 1707983
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, LX/1J1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1J1;->sendMessage(Landroid/os/Message;)Z

    .line 1707984
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    invoke-static {v0, p0}, LX/1Iz;->b(LX/1Iz;LX/AjQ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1707985
    const v0, 0x5c28167c

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x8a11902

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1707945
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1707946
    const-string v0, "NetworkRequestSubscriber.onNext"

    const v2, -0x56a2e039

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1707947
    :try_start_0
    iget-boolean v0, p0, LX/AjQ;->e:Z

    if-nez v0, :cond_0

    .line 1707948
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->l:LX/0pV;

    sget-object v1, LX/1Iz;->b:Ljava/lang/String;

    sget-object v2, LX/7zp;->NETWORK_NEXT_NOT_IN_USE:LX/7zp;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1707949
    const v0, -0x47f90eb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1707950
    :goto_0
    return-void

    .line 1707951
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/AjQ;->c:LX/1J0;

    iget-object v0, v0, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    iget-object v2, p0, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v2, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v3

    .line 1707952
    if-nez v3, :cond_1

    .line 1707953
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->l:LX/0pV;

    sget-object v1, LX/1Iz;->b:Ljava/lang/String;

    sget-object v2, LX/7zp;->NETWORK_NEXT_IS_NULL:LX/7zp;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1707954
    const v0, -0x646f010b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 1707955
    :cond_1
    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1707956
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1707957
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1707958
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1707959
    :cond_2
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->l:LX/0pV;

    sget-object v2, LX/1Iz;->b:Ljava/lang/String;

    sget-object v5, LX/7zp;->NETWORK_NEXT:LX/7zp;

    const-string v6, "cursorReturned"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v5, v6, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 1707960
    invoke-direct {p0, v3}, LX/AjQ;->a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    .line 1707961
    invoke-virtual {v2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v3

    .line 1707962
    iget-object v4, p0, LX/AjQ;->c:LX/1J0;

    iget v0, p0, LX/AjQ;->f:I

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 1707963
    :goto_2
    iget-object v1, v4, LX/1J0;->h:LX/0gf;

    invoke-virtual {v1}, LX/0gf;->isNewStoriesFetch()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1707964
    if-eqz v0, :cond_6

    const/4 v1, 0x0

    .line 1707965
    :goto_3
    move v0, v1

    .line 1707966
    if-nez v0, :cond_3

    .line 1707967
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->n:LX/0qf;

    invoke-virtual {v0, v2}, LX/0qf;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 1707968
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1707969
    iget-object v2, p0, LX/AjQ;->a:LX/0Pz;

    invoke-static {v0}, LX/0x0;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1707970
    iget v0, p0, LX/AjQ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/AjQ;->f:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 1707971
    :catchall_0
    move-exception v0

    const v1, -0x52170251

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_4
    move v0, v1

    .line 1707972
    goto :goto_2

    .line 1707973
    :cond_5
    const v0, -0x4ca2d71f

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 1707974
    :cond_6
    const/4 v1, 0x6

    goto :goto_3

    .line 1707975
    :cond_7
    const/4 v1, 0x1

    goto :goto_3
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1707932
    const-string v0, "NetworkRequestSubscriber.onError"

    const v1, -0x4c8e77fe

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1707933
    :try_start_0
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->l:LX/0pV;

    sget-object v1, LX/1Iz;->b:Ljava/lang/String;

    sget-object v2, LX/7zp;->NETWORK_ERROR:LX/7zp;

    const-string v3, "msg"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 1707934
    iget-boolean v0, p0, LX/AjQ;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1707935
    const v0, -0x5ec2b0ec

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1707936
    :goto_0
    return-void

    .line 1707937
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/AjQ;->c()V

    .line 1707938
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->m:LX/0pW;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    .line 1707939
    iget-object v4, v3, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v3, v4

    .line 1707940
    invoke-virtual {v0, v1, v2, v3}, LX/0pW;->a(Ljava/lang/String;ZLcom/facebook/api/feedtype/FeedType;)V

    .line 1707941
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    iget-object v0, v0, LX/1Iz;->j:LX/1J1;

    const/4 v1, 0x7

    .line 1707942
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3, p1}, LX/1J1;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1J1;->sendMessage(Landroid/os/Message;)Z

    .line 1707943
    iget-object v0, p0, LX/AjQ;->b:LX/1Iz;

    invoke-static {v0, p0}, LX/1Iz;->b(LX/1Iz;LX/AjQ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1707944
    const v0, -0x17160b9e

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x4d24283d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
