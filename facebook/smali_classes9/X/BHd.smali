.class public LX/BHd;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/BHX;

.field private final b:LX/BHc;

.field private final c:LX/BHJ;

.field private final d:Lcom/facebook/common/time/RealtimeSinceBootClock;

.field public e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZLX/BHX;LX/BHc;Lcom/facebook/common/time/RealtimeSinceBootClock;)V
    .locals 9
    .param p1    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BH5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BHJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/facebook/photos/simplepicker/SimplePickerFragment$BitmapRenderedCallback;",
            "LX/BHJ;",
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;ZZZ",
            "LX/BHX;",
            "LX/BHc;",
            "Lcom/facebook/common/time/RealtimeSinceBootClock;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769463
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1769464
    const/4 v1, 0x0

    iput v1, p0, LX/BHd;->f:I

    .line 1769465
    move-object/from16 v0, p8

    iput-object v0, p0, LX/BHd;->a:LX/BHX;

    .line 1769466
    iget-object v1, p0, LX/BHd;->a:LX/BHX;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, LX/BHX;->a(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZ)Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    move-result-object v1

    iput-object v1, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    .line 1769467
    move-object/from16 v0, p9

    iput-object v0, p0, LX/BHd;->b:LX/BHc;

    .line 1769468
    iput-object p3, p0, LX/BHd;->c:LX/BHJ;

    .line 1769469
    move-object/from16 v0, p10

    iput-object v0, p0, LX/BHd;->d:Lcom/facebook/common/time/RealtimeSinceBootClock;

    .line 1769470
    return-void
.end method

.method private f(I)Z
    .locals 1

    .prologue
    .line 1769462
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    if-ltz p1, :cond_1

    iget-object v0, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getViewTypeCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1769456
    invoke-direct {p0, p2}, LX/BHd;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769457
    iget-object v0, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v2}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/3tK;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1769458
    new-instance v1, LX/BHb;

    invoke-direct {v1, v0}, LX/BHb;-><init>(Landroid/view/View;)V

    .line 1769459
    move-object v0, v1

    .line 1769460
    return-object v0

    .line 1769461
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1769471
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1769472
    invoke-direct {p0, v0}, LX/BHd;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769473
    iget-object v0, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1769474
    iget-object v1, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    move-object v0, p1

    check-cast v0, LX/BHb;

    .line 1769475
    iget-object v2, v0, LX/BHb;->l:Landroid/view/View;

    move-object v0, v2

    .line 1769476
    check-cast p1, LX/BHb;

    .line 1769477
    iget-object v2, p1, LX/BHb;->l:Landroid/view/View;

    move-object v2, v2

    .line 1769478
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    iget-object v2, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v2}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/3tK;->a(Landroid/view/View;Landroid/database/Cursor;)V

    return-void

    .line 1769479
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(LX/1a1;)V
    .locals 4

    .prologue
    .line 1769443
    invoke-super {p0, p1}, LX/1OM;->c(LX/1a1;)V

    .line 1769444
    check-cast p1, LX/BHb;

    .line 1769445
    iget-object v0, p1, LX/BHb;->l:Landroid/view/View;

    move-object v1, v0

    .line 1769446
    move-object v0, v1

    .line 1769447
    check-cast v0, LX/BIG;

    invoke-virtual {v0}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1769448
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/BHd;->c:LX/BHJ;

    .line 1769449
    iget-object v3, v2, LX/BHJ;->z:LX/BHZ;

    move-object v2, v3

    .line 1769450
    invoke-virtual {v2, v0}, LX/BHZ;->b(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769451
    check-cast v1, LX/BIE;

    const/high16 v0, 0x3f400000    # 0.75f

    invoke-interface {v1, v0}, LX/BIE;->a(F)V

    .line 1769452
    :goto_0
    iget-object v0, p0, LX/BHd;->d:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 1769453
    iput-wide v0, p1, LX/BHb;->m:J

    .line 1769454
    return-void

    .line 1769455
    :cond_0
    check-cast v1, LX/BIE;

    invoke-interface {v1}, LX/BIE;->a()V

    goto :goto_0
.end method

.method public final d(LX/1a1;)V
    .locals 14

    .prologue
    const-wide/16 v2, -0x1

    .line 1769424
    invoke-super {p0, p1}, LX/1OM;->d(LX/1a1;)V

    .line 1769425
    instance-of v0, p1, LX/BHb;

    if-eqz v0, :cond_1

    .line 1769426
    check-cast p1, LX/BHb;

    .line 1769427
    iget-object v0, p0, LX/BHd;->d:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v0

    .line 1769428
    iput-wide v0, p1, LX/BHb;->n:J

    .line 1769429
    iget-object v0, p1, LX/BHb;->l:Landroid/view/View;

    move-object v0, v0

    .line 1769430
    instance-of v1, v0, LX/BIG;

    if-eqz v1, :cond_0

    .line 1769431
    check-cast v0, LX/BIG;

    .line 1769432
    iget-object v1, v0, LX/BIG;->m:LX/BHu;

    move-object v0, v1

    .line 1769433
    if-eqz v0, :cond_0

    .line 1769434
    const-wide/16 v10, -0x1

    .line 1769435
    iget-wide v12, p1, LX/BHb;->m:J

    move-wide v4, v12

    .line 1769436
    iget-wide v12, p1, LX/BHb;->n:J

    move-wide v6, v12

    .line 1769437
    cmp-long v8, v4, v10

    if-eqz v8, :cond_0

    cmp-long v8, v6, v10

    if-nez v8, :cond_2

    .line 1769438
    :cond_0
    :goto_0
    iput-wide v2, p1, LX/BHb;->m:J

    .line 1769439
    iput-wide v2, p1, LX/BHb;->n:J

    .line 1769440
    :cond_1
    return-void

    .line 1769441
    :cond_2
    invoke-virtual {v0, v4, v5}, LX/2xn;->a(J)V

    .line 1769442
    invoke-virtual {v0, v6, v7}, LX/2xn;->b(J)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1769423
    iget-object v0, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1769422
    iget-object v0, p0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v0}, LX/3tK;->getCount()I

    move-result v0

    return v0
.end method
