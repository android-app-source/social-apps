.class public final LX/AqA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AqB;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/1dQ;

.field public final synthetic c:LX/AqB;


# direct methods
.method public constructor <init>(LX/AqB;)V
    .locals 1

    .prologue
    .line 1717331
    iput-object p1, p0, LX/AqA;->c:LX/AqB;

    .line 1717332
    move-object v0, p1

    .line 1717333
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1717334
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1717320
    const-string v0, "FigRadioButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1717321
    if-ne p0, p1, :cond_1

    .line 1717322
    :cond_0
    :goto_0
    return v0

    .line 1717323
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1717324
    goto :goto_0

    .line 1717325
    :cond_3
    check-cast p1, LX/AqA;

    .line 1717326
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1717327
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1717328
    if-eq v2, v3, :cond_0

    .line 1717329
    iget-boolean v2, p0, LX/AqA;->a:Z

    iget-boolean v3, p1, LX/AqA;->a:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1717330
    goto :goto_0
.end method
