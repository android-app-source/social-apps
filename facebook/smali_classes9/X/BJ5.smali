.class public final enum LX/BJ5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BJ5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BJ5;

.field public static final enum FULL:LX/BJ5;

.field public static final enum MEDIUM:LX/BJ5;

.field public static final enum MINI:LX/BJ5;

.field public static final enum SMALL:LX/BJ5;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1771518
    new-instance v0, LX/BJ5;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/BJ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ5;->FULL:LX/BJ5;

    .line 1771519
    new-instance v0, LX/BJ5;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/BJ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ5;->MEDIUM:LX/BJ5;

    .line 1771520
    new-instance v0, LX/BJ5;

    const-string v1, "MINI"

    invoke-direct {v0, v1, v4}, LX/BJ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ5;->MINI:LX/BJ5;

    .line 1771521
    new-instance v0, LX/BJ5;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v5}, LX/BJ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BJ5;->SMALL:LX/BJ5;

    .line 1771522
    const/4 v0, 0x4

    new-array v0, v0, [LX/BJ5;

    sget-object v1, LX/BJ5;->FULL:LX/BJ5;

    aput-object v1, v0, v2

    sget-object v1, LX/BJ5;->MEDIUM:LX/BJ5;

    aput-object v1, v0, v3

    sget-object v1, LX/BJ5;->MINI:LX/BJ5;

    aput-object v1, v0, v4

    sget-object v1, LX/BJ5;->SMALL:LX/BJ5;

    aput-object v1, v0, v5

    sput-object v0, LX/BJ5;->$VALUES:[LX/BJ5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1771523
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BJ5;
    .locals 1

    .prologue
    .line 1771524
    const-class v0, LX/BJ5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BJ5;

    return-object v0
.end method

.method public static values()[LX/BJ5;
    .locals 1

    .prologue
    .line 1771525
    sget-object v0, LX/BJ5;->$VALUES:[LX/BJ5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BJ5;

    return-object v0
.end method
