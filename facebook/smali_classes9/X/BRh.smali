.class public LX/BRh;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BRg;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784307
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1784308
    return-void
.end method


# virtual methods
.method public final a(J)LX/BRg;
    .locals 11

    .prologue
    .line 1784309
    new-instance v1, LX/BRg;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v0, LX/BRj;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/BRj;

    .line 1784310
    new-instance v5, LX/BRl;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v0, v4}, LX/BRl;-><init>(Landroid/content/res/Resources;LX/0SG;)V

    .line 1784311
    move-object v4, v5

    .line 1784312
    check-cast v4, LX/BRl;

    .line 1784313
    new-instance v5, LX/BRk;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v5, v0}, LX/BRk;-><init>(Landroid/content/res/Resources;)V

    .line 1784314
    move-object v5, v5

    .line 1784315
    check-cast v5, LX/BRk;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v6

    check-cast v6, LX/23P;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    move-wide v8, p1

    invoke-direct/range {v1 .. v9}, LX/BRg;-><init>(Landroid/content/Context;LX/BRj;LX/BRl;LX/BRk;LX/23P;LX/0wM;J)V

    .line 1784316
    return-object v1
.end method
