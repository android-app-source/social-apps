.class public LX/C9e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0qn;

.field private final b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final c:LX/9A1;

.field private final d:LX/0SG;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/C9Y;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Mj;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;


# direct methods
.method public constructor <init>(LX/0qn;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/9A1;LX/0SG;Lcom/facebook/content/SecureContextHelper;LX/C9Y;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qn;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/9A1;",
            "LX/0SG;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/C9Y;",
            "LX/0Ot",
            "<",
            "LX/8Mj;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854295
    iput-object p1, p0, LX/C9e;->a:LX/0qn;

    .line 1854296
    iput-object p2, p0, LX/C9e;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1854297
    iput-object p3, p0, LX/C9e;->c:LX/9A1;

    .line 1854298
    iput-object p4, p0, LX/C9e;->d:LX/0SG;

    .line 1854299
    iput-object p5, p0, LX/C9e;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1854300
    iput-object p6, p0, LX/C9e;->f:LX/C9Y;

    .line 1854301
    iput-object p7, p0, LX/C9e;->g:LX/0Ot;

    .line 1854302
    iput-object p8, p0, LX/C9e;->h:LX/0ad;

    .line 1854303
    return-void
.end method

.method public static a(LX/0QB;)LX/C9e;
    .locals 12

    .prologue
    .line 1854328
    const-class v1, LX/C9e;

    monitor-enter v1

    .line 1854329
    :try_start_0
    sget-object v0, LX/C9e;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854330
    sput-object v2, LX/C9e;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854331
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854332
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854333
    new-instance v3, LX/C9e;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v4

    check-cast v4, LX/0qn;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v6

    check-cast v6, LX/9A1;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/C9Y;->a(LX/0QB;)LX/C9Y;

    move-result-object v9

    check-cast v9, LX/C9Y;

    const/16 v10, 0x2eee

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/C9e;-><init>(LX/0qn;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/9A1;LX/0SG;Lcom/facebook/content/SecureContextHelper;LX/C9Y;LX/0Ot;LX/0ad;)V

    .line 1854334
    move-object v0, v3

    .line 1854335
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854336
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854337
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1854327
    invoke-static {p0}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 1854326
    iget-object v0, p0, LX/C9e;->h:LX/0ad;

    sget-short v1, LX/1aO;->ak:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;Z)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1854312
    invoke-static {p1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/17E;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1854313
    if-eqz v1, :cond_2

    .line 1854314
    :cond_1
    :goto_1
    return v0

    .line 1854315
    :cond_2
    iget-object v1, p0, LX/C9e;->a:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    .line 1854316
    iget-object v2, p0, LX/C9e;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 1854317
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v3, :cond_8

    :cond_3
    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 1854318
    if-nez v3, :cond_1

    .line 1854319
    if-nez v2, :cond_4

    .line 1854320
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_1

    .line 1854321
    invoke-direct {p0, p1}, LX/C9e;->d(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 1854322
    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->j()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1854323
    invoke-direct {p0, p1}, LX/C9e;->d(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 1854324
    :cond_5
    iget-object v3, p0, LX/C9e;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v3

    .line 1854325
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v4, :cond_1

    iget-object v1, p0, LX/C9e;->f:LX/C9Y;

    invoke-virtual {v1, p1, p2}, LX/C9Y;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez p2, :cond_6

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x3e8

    if-ge v3, v1, :cond_1

    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private d(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 1854310
    iget-object v0, p0, LX/C9e;->c:LX/9A1;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, p1, v2, v3}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 1854311
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1854304
    invoke-static {p1}, LX/C9e;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1854305
    if-eqz p2, :cond_1

    .line 1854306
    :cond_0
    :goto_0
    return v0

    .line 1854307
    :cond_1
    invoke-direct {p0, p1, p2}, LX/C9e;->b(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    goto :goto_0

    .line 1854308
    :cond_2
    if-eqz p2, :cond_3

    invoke-direct {p0}, LX/C9e;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    if-nez p2, :cond_4

    invoke-direct {p0}, LX/C9e;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1854309
    :cond_4
    invoke-direct {p0, p1, p2}, LX/C9e;->b(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    goto :goto_0
.end method
