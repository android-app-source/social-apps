.class public LX/CdI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CdD;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/31g;

.field private final c:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;


# direct methods
.method public constructor <init>(LX/0Zb;LX/31g;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;)V
    .locals 0
    .param p3    # Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1922369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922370
    iput-object p1, p0, LX/CdI;->a:LX/0Zb;

    .line 1922371
    iput-object p2, p0, LX/CdI;->b:LX/31g;

    .line 1922372
    iput-object p3, p0, LX/CdI;->c:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1922373
    return-void
.end method

.method private a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1922374
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "place_creation_session"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "crowdsourcing_create"

    .line 1922375
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1922376
    move-object v0, v0

    .line 1922377
    const-string v1, "event"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "entry_point"

    iget-object v2, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "endpoint"

    iget-object v2, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_id"

    iget-object v2, p0, LX/CdI;->b:LX/31g;

    invoke-virtual {v2}, LX/31g;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1922378
    iget-object v0, p0, LX/CdI;->a:LX/0Zb;

    iget-object v1, p0, LX/CdI;->c:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v2, "current_location_tapped"

    invoke-direct {p0, v1, v2}, LX/CdI;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_obj_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1922379
    return-void
.end method

.method public final a(LX/969;J)V
    .locals 4

    .prologue
    .line 1922380
    iget-object v0, p0, LX/CdI;->a:LX/0Zb;

    iget-object v1, p0, LX/CdI;->c:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v2, "suggestion_tapped"

    invoke-direct {p0, v1, v2}, LX/CdI;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "field_type_name"

    iget-object v3, p1, LX/969;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_obj_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1922381
    return-void
.end method

.method public final a(LX/969;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1922382
    iget-object v0, p0, LX/CdI;->a:LX/0Zb;

    iget-object v1, p0, LX/CdI;->c:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v2, "search_result_tapped"

    invoke-direct {p0, v1, v2}, LX/CdI;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "field_type_name"

    iget-object v3, p1, LX/969;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_obj_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1922383
    return-void
.end method
