.class public final LX/CHP;
.super LX/CHO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CHO",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/CHQ;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;ILX/CHQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1867451
    iput-object p1, p0, LX/CHP;->d:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iput p2, p0, LX/CHP;->a:I

    iput-object p3, p0, LX/CHP;->b:LX/CHQ;

    iput-object p4, p0, LX/CHP;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/CHO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1867452
    iget-object v0, p0, LX/CHP;->d:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object v0, v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->h:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".prefetchArticleBlocks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error attempting to prefetch IS. Catalog id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/CHP;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1867453
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1867454
    move-object v1, v1

    .line 1867455
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1867456
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1867457
    invoke-static {p1}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v1

    .line 1867458
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1867459
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v3

    .line 1867460
    iget-object v2, p0, LX/CHP;->d:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    .line 1867461
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->j()LX/2uF;

    move-result-object v4

    invoke-virtual {v4}, LX/39O;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1867462
    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;

    .line 1867463
    iget v5, p0, LX/CHP;->a:I

    if-ne v1, v5, :cond_1

    .line 1867464
    :cond_0
    return-void

    .line 1867465
    :cond_1
    iget-object v5, p0, LX/CHP;->d:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v0

    iget-object v6, p0, LX/CHP;->b:LX/CHQ;

    const/4 v7, 0x0

    .line 1867466
    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v8, v9, :cond_9

    invoke-interface {v0}, LX/CHg;->p()LX/1Fb;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 1867467
    iget-object v7, v5, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    invoke-interface {v0}, LX/CHg;->p()LX/1Fb;

    move-result-object v8

    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8, v9}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v7

    .line 1867468
    iput-object v7, v6, LX/CHQ;->b:LX/1ca;

    .line 1867469
    :cond_2
    :goto_2
    const/4 v7, 0x1

    :cond_3
    move v0, v7

    .line 1867470
    if-eqz v0, :cond_4

    .line 1867471
    add-int/lit8 v0, v1, 0x1

    .line 1867472
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    .line 1867473
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->j()LX/2uF;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v6, 0x1

    .line 1867474
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1867475
    invoke-virtual {v4}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v9

    iget-object p1, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 1867476
    new-instance v1, LX/CHR;

    invoke-direct {v1, p1, v9}, LX/CHR;-><init>(LX/15i;I)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1867477
    :cond_6
    iget-object v8, v2, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->i:LX/8Yg;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v8, v7, v5, v6}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v7

    .line 1867478
    if-eqz v7, :cond_7

    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1867479
    :cond_7
    :goto_5
    goto/16 :goto_0

    .line 1867480
    :cond_8
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-static {v8, v7}, LX/8Yg;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v7

    .line 1867481
    iget-object v8, v2, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->j:LX/8Yh;

    .line 1867482
    iput-object v7, v8, LX/8Yh;->a:Ljava/util/Map;

    .line 1867483
    goto :goto_5

    .line 1867484
    :cond_9
    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v8, v9, :cond_b

    .line 1867485
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->D()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v8, v7

    :goto_6
    if-ge v8, v10, :cond_2

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/CHi;

    .line 1867486
    invoke-interface {v7}, LX/CHg;->p()LX/1Fb;

    move-result-object v11

    if-eqz v11, :cond_a

    .line 1867487
    iget-object v11, v5, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    invoke-interface {v7}, LX/CHg;->p()LX/1Fb;

    move-result-object v7

    invoke-interface {v7}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v7

    sget-object p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v11, v7, p1}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v7

    .line 1867488
    iput-object v7, v6, LX/CHQ;->b:LX/1ca;

    .line 1867489
    :cond_a
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_6

    .line 1867490
    :cond_b
    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v8, v9, :cond_3

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1867491
    iget-object v7, v5, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v8

    invoke-interface {v8}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v9

    invoke-interface {v9}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/8bW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1867492
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v7

    .line 1867493
    iput-object v7, v6, LX/CHQ;->d:LX/8Ys;

    .line 1867494
    goto/16 :goto_2
.end method
