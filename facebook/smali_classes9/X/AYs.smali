.class public LX/AYs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/AYs;


# instance fields
.field public final a:LX/0if;

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:I


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1686208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686209
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/AYs;->e:J

    .line 1686210
    iput-object p1, p0, LX/AYs;->a:LX/0if;

    .line 1686211
    return-void
.end method

.method public static a(LX/0QB;)LX/AYs;
    .locals 4

    .prologue
    .line 1686195
    sget-object v0, LX/AYs;->g:LX/AYs;

    if-nez v0, :cond_1

    .line 1686196
    const-class v1, LX/AYs;

    monitor-enter v1

    .line 1686197
    :try_start_0
    sget-object v0, LX/AYs;->g:LX/AYs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1686198
    if-eqz v2, :cond_0

    .line 1686199
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1686200
    new-instance p0, LX/AYs;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/AYs;-><init>(LX/0if;)V

    .line 1686201
    move-object v0, p0

    .line 1686202
    sput-object v0, LX/AYs;->g:LX/AYs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1686203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1686204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1686205
    :cond_1
    sget-object v0, LX/AYs;->g:LX/AYs;

    return-object v0

    .line 1686206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1686207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static i(LX/AYs;)LX/1rQ;
    .locals 4

    .prologue
    .line 1686194
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "video_id"

    iget-object v2, p0, LX/AYs;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "broadcaster_id"

    iget-object v2, p0, LX/AYs;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "video_timestamp_ms"

    iget-wide v2, p0, LX/AYs;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v0

    const-string v1, "last_break_timestamp_ms"

    iget-wide v2, p0, LX/AYs;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v0

    const-string v1, "number_of_breaks"

    iget v2, p0, LX/AYs;->f:I

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1686192
    iput-object p1, p0, LX/AYs;->b:Ljava/lang/String;

    .line 1686193
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1686190
    iput-object p1, p0, LX/AYs;->c:Ljava/lang/String;

    .line 1686191
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1686188
    iget-object v0, p0, LX/AYs;->a:LX/0if;

    sget-object v1, LX/0ig;->C:LX/0ih;

    const-string v2, "commercial_break_initiate"

    const/4 v3, 0x0

    invoke-static {p0}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1686189
    return-void
.end method
