.class public final LX/BWK;
.super LX/BWJ;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/webview/FacebookWebView;

.field private d:LX/BWI;


# direct methods
.method public constructor <init>(Lcom/facebook/webview/FacebookWebView;LX/BWI;)V
    .locals 0

    .prologue
    .line 1791871
    iput-object p1, p0, LX/BWK;->b:Lcom/facebook/webview/FacebookWebView;

    invoke-direct {p0, p1}, LX/BWJ;-><init>(Lcom/facebook/webview/FacebookWebView;)V

    .line 1791872
    iput-object p2, p0, LX/BWK;->d:LX/BWI;

    .line 1791873
    return-void
.end method


# virtual methods
.method public final onShowFileChooser(Landroid/webkit/WebView;Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/WebView;",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/webkit/WebChromeClient$FileChooserParams;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1791874
    iget-object v0, p0, LX/BWK;->d:LX/BWI;

    invoke-interface {v0, p2, p3}, LX/BWI;->a(Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z

    move-result v0

    return v0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1791875
    iget-object v0, p0, LX/BWK;->d:LX/BWI;

    invoke-interface {v0, p1}, LX/BWI;->a(Landroid/webkit/ValueCallback;)V

    .line 1791876
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791877
    iget-object v0, p0, LX/BWK;->d:LX/BWI;

    invoke-interface {v0, p1, p2}, LX/BWI;->a(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 1791878
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791879
    iget-object v0, p0, LX/BWK;->d:LX/BWI;

    invoke-interface {v0, p1, p2}, LX/BWI;->b(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 1791880
    return-void
.end method
