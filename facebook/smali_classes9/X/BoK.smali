.class public final LX/BoK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/BoM;


# direct methods
.method public constructor <init>(LX/BoM;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1821661
    iput-object p1, p0, LX/BoK;->d:LX/BoM;

    iput-object p2, p0, LX/BoK;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/BoK;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p4, p0, LX/BoK;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1821662
    iget-object v0, p0, LX/BoK;->d:LX/BoM;

    iget-object v1, p0, LX/BoK;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v1}, LX/BoM;->a$redex0(LX/BoM;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1821663
    iget-object v0, p0, LX/BoK;->d:LX/BoM;

    iget-object v1, p0, LX/BoK;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1}, LX/BoM;->a$redex0(LX/BoM;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 1821664
    if-eqz v0, :cond_0

    .line 1821665
    iget-object v1, p0, LX/BoK;->d:LX/BoM;

    iget-object v1, v1, LX/BoM;->a:LX/1dt;

    iget-object v1, v1, LX/1dt;->t:LX/1e5;

    .line 1821666
    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/1e5;->a(LX/1e5;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1821667
    iget-object v0, p0, LX/BoK;->d:LX/BoM;

    iget-object v1, p0, LX/BoK;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v2, "irrelevant"

    invoke-static {v0, v1, v2}, LX/BoM;->a$redex0(LX/BoM;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1821668
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
