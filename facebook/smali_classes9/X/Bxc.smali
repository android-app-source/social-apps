.class public final LX/Bxc;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Bxd;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Bxb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bxd",
            "<TE;>.AlbumAttachmentHScrollComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Bxd;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Bxd;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1835754
    iput-object p1, p0, LX/Bxc;->b:LX/Bxd;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1835755
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Bxc;->c:[Ljava/lang/String;

    .line 1835756
    iput v3, p0, LX/Bxc;->d:I

    .line 1835757
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Bxc;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bxc;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Bxc;LX/1De;IILX/Bxb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Bxd",
            "<TE;>.AlbumAttachmentHScrollComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1835758
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1835759
    iput-object p4, p0, LX/Bxc;->a:LX/Bxb;

    .line 1835760
    iget-object v0, p0, LX/Bxc;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1835761
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1835762
    invoke-super {p0}, LX/1X5;->a()V

    .line 1835763
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bxc;->a:LX/Bxb;

    .line 1835764
    iget-object v0, p0, LX/Bxc;->b:LX/Bxd;

    iget-object v0, v0, LX/Bxd;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1835765
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Bxd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1835766
    iget-object v1, p0, LX/Bxc;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bxc;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Bxc;->d:I

    if-ge v1, v2, :cond_2

    .line 1835767
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1835768
    :goto_0
    iget v2, p0, LX/Bxc;->d:I

    if-ge v0, v2, :cond_1

    .line 1835769
    iget-object v2, p0, LX/Bxc;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1835770
    iget-object v2, p0, LX/Bxc;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1835771
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1835772
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1835773
    :cond_2
    iget-object v0, p0, LX/Bxc;->a:LX/Bxb;

    .line 1835774
    invoke-virtual {p0}, LX/Bxc;->a()V

    .line 1835775
    return-object v0
.end method
