.class public final LX/Apt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Apv;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Ljava/lang/Boolean;

.field public k:Z

.field public l:LX/1dQ;

.field public final synthetic m:LX/Apv;


# direct methods
.method public constructor <init>(LX/Apv;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1716919
    iput-object p1, p0, LX/Apt;->m:LX/Apv;

    .line 1716920
    move-object v0, p1

    .line 1716921
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716922
    iput-boolean v1, p0, LX/Apt;->c:Z

    .line 1716923
    sget v0, LX/Apw;->a:I

    iput v0, p0, LX/Apt;->d:I

    .line 1716924
    sget v0, LX/Apw;->b:I

    iput v0, p0, LX/Apt;->e:I

    .line 1716925
    sget v0, LX/Apw;->c:I

    iput v0, p0, LX/Apt;->f:I

    .line 1716926
    sget v0, LX/Apw;->d:I

    iput v0, p0, LX/Apt;->g:I

    .line 1716927
    sget v0, LX/Apw;->e:I

    iput v0, p0, LX/Apt;->h:I

    .line 1716928
    sget v0, LX/Apw;->f:I

    iput v0, p0, LX/Apt;->i:I

    .line 1716929
    iput-boolean v1, p0, LX/Apt;->k:Z

    .line 1716930
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716931
    const-string v0, "CompoundButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716932
    if-ne p0, p1, :cond_1

    .line 1716933
    :cond_0
    :goto_0
    return v0

    .line 1716934
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716935
    goto :goto_0

    .line 1716936
    :cond_3
    check-cast p1, LX/Apt;

    .line 1716937
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716938
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716939
    if-eq v2, v3, :cond_0

    .line 1716940
    iget v2, p0, LX/Apt;->a:I

    iget v3, p1, LX/Apt;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716941
    goto :goto_0

    .line 1716942
    :cond_4
    iget v2, p0, LX/Apt;->b:I

    iget v3, p1, LX/Apt;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1716943
    goto :goto_0

    .line 1716944
    :cond_5
    iget-boolean v2, p0, LX/Apt;->c:Z

    iget-boolean v3, p1, LX/Apt;->c:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1716945
    goto :goto_0

    .line 1716946
    :cond_6
    iget v2, p0, LX/Apt;->d:I

    iget v3, p1, LX/Apt;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1716947
    goto :goto_0

    .line 1716948
    :cond_7
    iget v2, p0, LX/Apt;->e:I

    iget v3, p1, LX/Apt;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1716949
    goto :goto_0

    .line 1716950
    :cond_8
    iget v2, p0, LX/Apt;->f:I

    iget v3, p1, LX/Apt;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1716951
    goto :goto_0

    .line 1716952
    :cond_9
    iget v2, p0, LX/Apt;->g:I

    iget v3, p1, LX/Apt;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1716953
    goto :goto_0

    .line 1716954
    :cond_a
    iget v2, p0, LX/Apt;->h:I

    iget v3, p1, LX/Apt;->h:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1716955
    goto :goto_0

    .line 1716956
    :cond_b
    iget v2, p0, LX/Apt;->i:I

    iget v3, p1, LX/Apt;->i:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1716957
    goto :goto_0

    .line 1716958
    :cond_c
    iget-object v2, p0, LX/Apt;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Apt;->j:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Apt;->j:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1716959
    goto :goto_0

    .line 1716960
    :cond_e
    iget-object v2, p1, LX/Apt;->j:Ljava/lang/Boolean;

    if-nez v2, :cond_d

    .line 1716961
    :cond_f
    iget-boolean v2, p0, LX/Apt;->k:Z

    iget-boolean v3, p1, LX/Apt;->k:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1716962
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1716963
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Apt;

    .line 1716964
    const/4 v1, 0x0

    iput-object v1, v0, LX/Apt;->j:Ljava/lang/Boolean;

    .line 1716965
    return-object v0
.end method
