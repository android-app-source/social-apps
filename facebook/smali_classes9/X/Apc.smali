.class public final LX/Apc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Apd;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1716387
    invoke-static {}, LX/Apd;->q()LX/Apd;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716388
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716389
    const-string v0, "FigSmallHscrollFooterTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716390
    if-ne p0, p1, :cond_1

    .line 1716391
    :cond_0
    :goto_0
    return v0

    .line 1716392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716393
    goto :goto_0

    .line 1716394
    :cond_3
    check-cast p1, LX/Apc;

    .line 1716395
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716396
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716397
    if-eq v2, v3, :cond_0

    .line 1716398
    iget-object v2, p0, LX/Apc;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Apc;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apc;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1716399
    goto :goto_0

    .line 1716400
    :cond_5
    iget-object v2, p1, LX/Apc;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1716401
    :cond_6
    iget-object v2, p0, LX/Apc;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Apc;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apc;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1716402
    goto :goto_0

    .line 1716403
    :cond_8
    iget-object v2, p1, LX/Apc;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1716404
    :cond_9
    iget-object v2, p0, LX/Apc;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Apc;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apc;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1716405
    goto :goto_0

    .line 1716406
    :cond_a
    iget-object v2, p1, LX/Apc;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
