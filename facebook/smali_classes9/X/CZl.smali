.class public LX/CZl;
.super LX/9Bj;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private c:Lcom/facebook/content/SecureContextHelper;

.field private d:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1nL;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916577
    const-string v0, "story_view"

    invoke-direct {p0, v0, p1, p5}, LX/9Bj;-><init>(Ljava/lang/String;Landroid/content/Context;LX/1nL;)V

    .line 1916578
    iput-object p1, p0, LX/CZl;->a:Landroid/content/Context;

    .line 1916579
    iput-object p2, p0, LX/CZl;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1916580
    iput-object p3, p0, LX/CZl;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1916581
    iput-object p4, p0, LX/CZl;->d:LX/17Y;

    .line 1916582
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 7
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1916572
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    .line 1916573
    :goto_0
    iget-object v0, p0, LX/CZl;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v5

    move-object v2, p1

    move-object v4, p3

    move-object v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1916574
    iget-object v1, p0, LX/CZl;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CZl;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1916575
    return-void

    .line 1916576
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1916566
    sget-object v0, LX/0ax;->bn:Ljava/lang/String;

    invoke-static {v0, p3, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1916567
    iget-object v1, p0, LX/CZl;->d:LX/17Y;

    invoke-interface {v1, p2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1916568
    iget-object v1, p0, LX/CZl;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1916569
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 1

    .prologue
    .line 1916583
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, LX/CZl;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1916584
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 2

    .prologue
    .line 1916570
    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, LX/CZl;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1916571
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 4

    .prologue
    .line 1916562
    iget-object v0, p0, LX/CZl;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1916563
    const-string v1, "fragment_title"

    iget-object v2, p0, LX/CZl;->a:Landroid/content/Context;

    const v3, 0x7f080fe6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1916564
    iget-object v1, p0, LX/CZl;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CZl;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1916565
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 5

    .prologue
    .line 1916559
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iget-object v1, p0, LX/CZl;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v2, LX/8s1;->ACTIVITY_RESULT:LX/8s1;

    invoke-static {v0, v1, v2}, LX/9Ap;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/8s1;)Landroid/content/Intent;

    move-result-object v1

    .line 1916560
    iget-object v2, p0, LX/CZl;->c:Lcom/facebook/content/SecureContextHelper;

    const v3, 0xb256

    iget-object v0, p0, LX/CZl;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1916561
    return-void
.end method
