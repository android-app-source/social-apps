.class public final LX/C4L;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C4M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public final synthetic g:LX/C4M;


# direct methods
.method public constructor <init>(LX/C4M;)V
    .locals 1

    .prologue
    .line 1847101
    iput-object p1, p0, LX/C4L;->g:LX/C4M;

    .line 1847102
    move-object v0, p1

    .line 1847103
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1847104
    const v0, -0x1f1d1b

    iput v0, p0, LX/C4L;->d:I

    .line 1847105
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/C4L;->e:Z

    .line 1847106
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/C4L;->f:Z

    .line 1847107
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1847108
    const-string v0, "ReactionsFooterWithBlingbarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1847109
    if-ne p0, p1, :cond_1

    .line 1847110
    :cond_0
    :goto_0
    return v0

    .line 1847111
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1847112
    goto :goto_0

    .line 1847113
    :cond_3
    check-cast p1, LX/C4L;

    .line 1847114
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1847115
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1847116
    if-eq v2, v3, :cond_0

    .line 1847117
    iget-object v2, p0, LX/C4L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C4L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C4L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1847118
    goto :goto_0

    .line 1847119
    :cond_5
    iget-object v2, p1, LX/C4L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1847120
    :cond_6
    iget-object v2, p0, LX/C4L;->b:LX/1Pr;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C4L;->b:LX/1Pr;

    iget-object v3, p1, LX/C4L;->b:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1847121
    goto :goto_0

    .line 1847122
    :cond_8
    iget-object v2, p1, LX/C4L;->b:LX/1Pr;

    if-nez v2, :cond_7

    .line 1847123
    :cond_9
    iget v2, p0, LX/C4L;->c:I

    iget v3, p1, LX/C4L;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1847124
    goto :goto_0

    .line 1847125
    :cond_a
    iget v2, p0, LX/C4L;->d:I

    iget v3, p1, LX/C4L;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1847126
    goto :goto_0

    .line 1847127
    :cond_b
    iget-boolean v2, p0, LX/C4L;->e:Z

    iget-boolean v3, p1, LX/C4L;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1847128
    goto :goto_0

    .line 1847129
    :cond_c
    iget-boolean v2, p0, LX/C4L;->f:Z

    iget-boolean v3, p1, LX/C4L;->f:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1847130
    goto :goto_0
.end method
