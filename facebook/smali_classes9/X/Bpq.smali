.class public final LX/Bpq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V
    .locals 0

    .prologue
    .line 1823604
    iput-object p1, p0, LX/Bpq;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1823605
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v2, 0x0

    .line 1823606
    iget-object v0, p0, LX/Bpq;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1823607
    :cond_0
    :goto_0
    return-object v2

    .line 1823608
    :cond_1
    iget-object v0, p0, LX/Bpq;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823609
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1823610
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1823611
    if-eqz v0, :cond_0

    .line 1823612
    iget-object v1, p0, LX/Bpq;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
