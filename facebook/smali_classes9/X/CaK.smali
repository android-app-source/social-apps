.class public LX/CaK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CaJ;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1917398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1917399
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/CaK;->a:Ljava/util/Map;

    .line 1917400
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1917401
    iput-object v0, p0, LX/CaK;->b:LX/0Px;

    .line 1917402
    iput-object p1, p0, LX/CaK;->c:LX/0Sh;

    .line 1917403
    return-void
.end method

.method public static a(LX/0QB;)LX/CaK;
    .locals 4

    .prologue
    .line 1917404
    const-class v1, LX/CaK;

    monitor-enter v1

    .line 1917405
    :try_start_0
    sget-object v0, LX/CaK;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1917406
    sput-object v2, LX/CaK;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1917407
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917408
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1917409
    new-instance p0, LX/CaK;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/CaK;-><init>(LX/0Sh;)V

    .line 1917410
    move-object v0, p0

    .line 1917411
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1917412
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CaK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1917413
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1917414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1917415
    iget-object v0, p0, LX/CaK;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1917416
    iget-object v0, p0, LX/CaK;->a:Ljava/util/Map;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917417
    return-void
.end method

.method public final a(Ljava/lang/String;LX/CaJ;)V
    .locals 4

    .prologue
    .line 1917418
    iget-object v0, p0, LX/CaK;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1917419
    iget-object v0, p0, LX/CaK;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917420
    iget-object v0, p0, LX/CaK;->b:LX/0Px;

    if-eqz v0, :cond_0

    .line 1917421
    iget-object v0, p0, LX/CaK;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/CaK;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1917422
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1917423
    invoke-interface {p2, v0}, LX/CaJ;->a(LX/5kD;)V

    .line 1917424
    :cond_0
    return-void

    .line 1917425
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
