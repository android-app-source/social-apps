.class public LX/CDc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Z

.field public final b:Z

.field public final c:LX/198;

.field private final d:LX/2mj;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Fb;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1YQ;

.field public final g:LX/0iY;

.field public final h:LX/0iX;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bwf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/198;LX/2mj;LX/0Ot;LX/1YQ;LX/0iY;LX/0iX;LX/0Ot;LX/0Or;)V
    .locals 2
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/feedplugins/video/richvideoplayer/IsVideoGraphqlSubscriptionEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/198;",
            "LX/2mj;",
            "LX/0Ot",
            "<",
            "LX/3Fb;",
            ">;",
            "LX/1YQ;",
            "LX/0iY;",
            "LX/0iX;",
            "LX/0Ot",
            "<",
            "LX/Bwf;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859872
    sget-short v0, LX/0ws;->eG:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CDc;->a:Z

    .line 1859873
    invoke-interface {p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/CDc;->b:Z

    .line 1859874
    iput-object p2, p0, LX/CDc;->c:LX/198;

    .line 1859875
    iput-object p3, p0, LX/CDc;->d:LX/2mj;

    .line 1859876
    iput-object p4, p0, LX/CDc;->e:LX/0Ot;

    .line 1859877
    iput-object p5, p0, LX/CDc;->f:LX/1YQ;

    .line 1859878
    iput-object p6, p0, LX/CDc;->g:LX/0iY;

    .line 1859879
    iput-object p7, p0, LX/CDc;->h:LX/0iX;

    .line 1859880
    iput-object p8, p0, LX/CDc;->i:LX/0Ot;

    .line 1859881
    return-void
.end method

.method public static a(LX/0QB;)LX/CDc;
    .locals 13

    .prologue
    .line 1859860
    const-class v1, LX/CDc;

    monitor-enter v1

    .line 1859861
    :try_start_0
    sget-object v0, LX/CDc;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1859862
    sput-object v2, LX/CDc;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1859863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1859865
    new-instance v3, LX/CDc;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v5

    check-cast v5, LX/198;

    invoke-static {v0}, LX/2mj;->a(LX/0QB;)LX/2mj;

    move-result-object v6

    check-cast v6, LX/2mj;

    const/16 v7, 0x845

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v8

    check-cast v8, LX/1YQ;

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v9

    check-cast v9, LX/0iY;

    invoke-static {v0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v10

    check-cast v10, LX/0iX;

    const/16 v11, 0x1d74

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x14b9

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/CDc;-><init>(LX/0ad;LX/198;LX/2mj;LX/0Ot;LX/1YQ;LX/0iY;LX/0iX;LX/0Ot;LX/0Or;)V

    .line 1859866
    move-object v0, v3

    .line 1859867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1859868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1859870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/CDW;LX/2pa;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 1859856
    invoke-virtual {p1}, LX/2pa;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859857
    const-class v0, LX/3I7;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, LX/3I7;

    .line 1859858
    iput-object v0, p0, LX/CDW;->n:LX/3I8;

    .line 1859859
    :cond_0
    return-void
.end method

.method private static a(LX/CDW;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 1859852
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859853
    new-instance v0, LX/3GZ;

    invoke-virtual {p0}, LX/CDW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3GZ;-><init>(Landroid/content/Context;)V

    .line 1859854
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1859855
    :cond_0
    return-void
.end method

.method private static a(LX/CDc;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;Z)V
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 1859841
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1859842
    :cond_0
    invoke-virtual {p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1859843
    :goto_0
    return-void

    .line 1859844
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1859845
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1859846
    :cond_2
    if-eqz p3, :cond_3

    .line 1859847
    invoke-virtual {p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_0

    .line 1859848
    :cond_3
    iget-boolean v0, p0, LX/CDc;->a:Z

    if-eqz v0, :cond_4

    .line 1859849
    invoke-virtual {p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2pa;)V

    goto :goto_0

    .line 1859850
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1859851
    invoke-virtual {p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_0
.end method

.method private b()LX/3FX;
    .locals 1

    .prologue
    .line 1859840
    new-instance v0, LX/CDb;

    invoke-direct {v0, p0}, LX/CDb;-><init>(LX/CDc;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/CDW;LX/2pa;LX/CDg;LX/3Ge;LX/04D;LX/04H;LX/3It;LX/2oL;LX/093;Lcom/facebook/video/analytics/VideoFeedStoryInfo;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pe;ZZZILX/3Iv;LX/3Fa;)V
    .locals 10
    .param p2    # LX/2pa;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/CDg;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/3Ge;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/04D;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/04H;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/3It;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/2oL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/093;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Lcom/facebook/video/analytics/VideoFeedStoryInfo;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # LX/1Pe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # LX/3Iv;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1859795
    const-string v2, "RichVideoPlayerComponentSpec.onMount"

    const v3, 0x1e2b5696

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1859796
    :try_start_0
    invoke-virtual {p4, p2}, LX/3Ge;->b(LX/2pa;)V

    .line 1859797
    const/4 v2, 0x0

    invoke-virtual {p4, p1, p2, v2}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1859798
    invoke-static {p1, p2}, LX/CDc;->a(LX/CDW;LX/2pa;)V

    .line 1859799
    move-object/from16 v0, p11

    invoke-static {p1, v0}, LX/CDc;->a(LX/CDW;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1859800
    move/from16 v0, p14

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1859801
    move/from16 v0, p15

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setNeedCentering(Z)V

    .line 1859802
    move/from16 v0, p16

    invoke-virtual {p1, v0}, LX/CDW;->setBackgroundResource(I)V

    .line 1859803
    invoke-virtual {p1, p5}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1859804
    move-object/from16 v0, p6

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1859805
    const/4 v2, 0x1

    invoke-static {p0, p2, p1, v2}, LX/CDc;->a(LX/CDc;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;Z)V

    .line 1859806
    move-object/from16 v0, p7

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setRichVideoPlayerCallbackListener(LX/3It;)V

    .line 1859807
    move-object/from16 v0, p17

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setInstreamVideoAdBreakCallbackListener(LX/3Iv;)V

    .line 1859808
    iput-object p1, p3, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1859809
    if-eqz p12, :cond_1

    .line 1859810
    if-nez p11, :cond_0

    .line 1859811
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "RichVideoPlayerComponent cannot accept the `canShowVideoInFullScreen` property without `attachment`"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1859812
    :catchall_0
    move-exception v2

    const v3, 0x5e1cdce

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 1859813
    :cond_0
    :try_start_1
    move-object/from16 v0, p11

    move-object/from16 v1, p12

    invoke-static {v0, v1, p1}, LX/3no;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pe;Landroid/view/View;)V

    .line 1859814
    :cond_1
    if-eqz p13, :cond_2

    .line 1859815
    iget-object v2, p0, LX/CDc;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p18

    invoke-static {v0, p1}, LX/3Fb;->a(LX/3Fa;Landroid/view/View;)V

    .line 1859816
    :cond_2
    iget-object v2, p0, LX/CDc;->d:LX/2mj;

    iget-object v6, p2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {p0}, LX/CDc;->b()LX/3FX;

    move-result-object v9

    move-object v3, p1

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v7, p10

    move-object v8, p5

    invoke-virtual/range {v2 .. v9}, LX/2mj;->a(Landroid/view/View;LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    .line 1859817
    invoke-virtual/range {p8 .. p8}, LX/2oL;->a()I

    move-result v2

    if-lez v2, :cond_3

    .line 1859818
    invoke-virtual/range {p8 .. p8}, LX/2oL;->a()I

    move-result v2

    sget-object v3, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859819
    :cond_3
    const v2, 0x5c7d943a

    invoke-static {v2}, LX/02m;->a(I)V

    .line 1859820
    return-void
.end method

.method public final a(LX/CDW;LX/CDg;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pe;ZLX/3Fa;)V
    .locals 2
    .param p2    # LX/CDg;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1859821
    const-string v0, "RichVideoPlayerComponentSpec.onUnmount"

    const v1, -0x3a2df1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1859822
    :try_start_0
    iget-object v0, p0, LX/CDc;->d:LX/2mj;

    .line 1859823
    iget-object v1, v0, LX/2mj;->a:LX/1AV;

    invoke-virtual {v1, p1}, LX/1AV;->a(Landroid/view/View;)V

    .line 1859824
    if-eqz p4, :cond_0

    .line 1859825
    invoke-interface {p4, p3}, LX/1Pe;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1859826
    :cond_0
    if-eqz p5, :cond_1

    .line 1859827
    iget-object v0, p0, LX/CDc;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p6}, LX/3Fb;->a(LX/3Fa;)V

    .line 1859828
    :cond_1
    const/4 v0, 0x0

    .line 1859829
    iput-object v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1859830
    const/4 v0, 0x0

    .line 1859831
    iput-object v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->y:LX/3Iv;

    .line 1859832
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->m()V

    .line 1859833
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1859834
    const/4 v0, 0x0

    iput-object v0, p1, LX/CDW;->n:LX/3I8;

    .line 1859835
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/CDW;->m:Z

    .line 1859836
    const/4 v0, 0x0

    iput-object v0, p2, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1859837
    const v0, 0x14dc8f30

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1859838
    return-void

    .line 1859839
    :catchall_0
    move-exception v0

    const v1, -0x69bfc5b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
