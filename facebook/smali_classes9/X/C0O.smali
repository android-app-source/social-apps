.class public final LX/C0O;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C0P;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public final synthetic e:LX/C0P;


# direct methods
.method public constructor <init>(LX/C0P;)V
    .locals 1

    .prologue
    .line 1840496
    iput-object p1, p0, LX/C0O;->e:LX/C0P;

    .line 1840497
    move-object v0, p1

    .line 1840498
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1840499
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/C0O;->d:Z

    .line 1840500
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1840501
    const-string v0, "SquarePhotoShareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1840502
    if-ne p0, p1, :cond_1

    .line 1840503
    :cond_0
    :goto_0
    return v0

    .line 1840504
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1840505
    goto :goto_0

    .line 1840506
    :cond_3
    check-cast p1, LX/C0O;

    .line 1840507
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1840508
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1840509
    if-eq v2, v3, :cond_0

    .line 1840510
    iget-object v2, p0, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1840511
    goto :goto_0

    .line 1840512
    :cond_5
    iget-object v2, p1, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1840513
    :cond_6
    iget-object v2, p0, LX/C0O;->b:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C0O;->b:LX/1Pq;

    iget-object v3, p1, LX/C0O;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1840514
    goto :goto_0

    .line 1840515
    :cond_8
    iget-object v2, p1, LX/C0O;->b:LX/1Pq;

    if-nez v2, :cond_7

    .line 1840516
    :cond_9
    iget v2, p0, LX/C0O;->c:I

    iget v3, p1, LX/C0O;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1840517
    goto :goto_0

    .line 1840518
    :cond_a
    iget-boolean v2, p0, LX/C0O;->d:Z

    iget-boolean v3, p1, LX/C0O;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1840519
    goto :goto_0
.end method
