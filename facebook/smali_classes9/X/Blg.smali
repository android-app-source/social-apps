.class public final LX/Blg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/common/EventActionContext;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public g:Z

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1816388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816389
    iput-object p1, p0, LX/Blg;->b:Ljava/lang/String;

    .line 1816390
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/Blg;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;)",
            "LX/Blg;"
        }
    .end annotation

    .prologue
    .line 1816371
    iput-object p1, p0, LX/Blg;->h:LX/0Px;

    .line 1816372
    return-object p0
.end method

.method public final a(Lcom/facebook/events/common/EventActionContext;)LX/Blg;
    .locals 0

    .prologue
    .line 1816386
    iput-object p1, p0, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    .line 1816387
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)LX/Blg;
    .locals 0

    .prologue
    .line 1816384
    iput-object p1, p0, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1816385
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/Blg;
    .locals 0

    .prologue
    .line 1816382
    iput-object p1, p0, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1816383
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)LX/Blg;
    .locals 0

    .prologue
    .line 1816380
    iput-object p1, p0, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1816381
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/Blg;
    .locals 0

    .prologue
    .line 1816378
    iput-object p1, p0, LX/Blg;->c:Ljava/lang/String;

    .line 1816379
    return-object p0
.end method

.method public final a(Z)LX/Blg;
    .locals 0

    .prologue
    .line 1816376
    iput-boolean p1, p0, LX/Blg;->g:Z

    .line 1816377
    return-object p0
.end method

.method public final a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;
    .locals 2

    .prologue
    .line 1816375
    new-instance v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    invoke-direct {v0, p0}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;-><init>(LX/Blg;)V

    return-object v0
.end method

.method public final b(Z)LX/Blg;
    .locals 0

    .prologue
    .line 1816373
    iput-boolean p1, p0, LX/Blg;->i:Z

    .line 1816374
    return-object p0
.end method
