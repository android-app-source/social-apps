.class public final enum LX/BVe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVe;

.field public static final enum ADJUST_VIEW_BOUNDS:LX/BVe;

.field public static final enum BASELINE:LX/BVe;

.field public static final enum BASELINE_ALIGN_BOTTOM:LX/BVe;

.field public static final enum CROP_TO_PADDING:LX/BVe;

.field public static final enum MAX_HEIGHT:LX/BVe;

.field public static final enum MAX_WIDTH:LX/BVe;

.field public static final enum SCALE_TYPE:LX/BVe;

.field public static final enum SRC:LX/BVe;

.field public static final enum TINT:LX/BVe;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791294
    new-instance v0, LX/BVe;

    const-string v1, "ADJUST_VIEW_BOUNDS"

    const-string v2, "adjustViewBounds"

    invoke-direct {v0, v1, v4, v2}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->ADJUST_VIEW_BOUNDS:LX/BVe;

    .line 1791295
    new-instance v0, LX/BVe;

    const-string v1, "BASELINE"

    const-string v2, "baseline"

    invoke-direct {v0, v1, v5, v2}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->BASELINE:LX/BVe;

    .line 1791296
    new-instance v0, LX/BVe;

    const-string v1, "BASELINE_ALIGN_BOTTOM"

    const-string v2, "baselineAlignBottom"

    invoke-direct {v0, v1, v6, v2}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->BASELINE_ALIGN_BOTTOM:LX/BVe;

    .line 1791297
    new-instance v0, LX/BVe;

    const-string v1, "CROP_TO_PADDING"

    const-string v2, "cropToPadding"

    invoke-direct {v0, v1, v7, v2}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->CROP_TO_PADDING:LX/BVe;

    .line 1791298
    new-instance v0, LX/BVe;

    const-string v1, "MAX_HEIGHT"

    const-string v2, "maxHeight"

    invoke-direct {v0, v1, v8, v2}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->MAX_HEIGHT:LX/BVe;

    .line 1791299
    new-instance v0, LX/BVe;

    const-string v1, "MAX_WIDTH"

    const/4 v2, 0x5

    const-string v3, "maxWidth"

    invoke-direct {v0, v1, v2, v3}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->MAX_WIDTH:LX/BVe;

    .line 1791300
    new-instance v0, LX/BVe;

    const-string v1, "SCALE_TYPE"

    const/4 v2, 0x6

    const-string v3, "scaleType"

    invoke-direct {v0, v1, v2, v3}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->SCALE_TYPE:LX/BVe;

    .line 1791301
    new-instance v0, LX/BVe;

    const-string v1, "SRC"

    const/4 v2, 0x7

    const-string v3, "src"

    invoke-direct {v0, v1, v2, v3}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->SRC:LX/BVe;

    .line 1791302
    new-instance v0, LX/BVe;

    const-string v1, "TINT"

    const/16 v2, 0x8

    const-string v3, "tint"

    invoke-direct {v0, v1, v2, v3}, LX/BVe;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVe;->TINT:LX/BVe;

    .line 1791303
    const/16 v0, 0x9

    new-array v0, v0, [LX/BVe;

    sget-object v1, LX/BVe;->ADJUST_VIEW_BOUNDS:LX/BVe;

    aput-object v1, v0, v4

    sget-object v1, LX/BVe;->BASELINE:LX/BVe;

    aput-object v1, v0, v5

    sget-object v1, LX/BVe;->BASELINE_ALIGN_BOTTOM:LX/BVe;

    aput-object v1, v0, v6

    sget-object v1, LX/BVe;->CROP_TO_PADDING:LX/BVe;

    aput-object v1, v0, v7

    sget-object v1, LX/BVe;->MAX_HEIGHT:LX/BVe;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVe;->MAX_WIDTH:LX/BVe;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVe;->SCALE_TYPE:LX/BVe;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BVe;->SRC:LX/BVe;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BVe;->TINT:LX/BVe;

    aput-object v2, v0, v1

    sput-object v0, LX/BVe;->$VALUES:[LX/BVe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791282
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791283
    iput-object p3, p0, LX/BVe;->mValue:Ljava/lang/String;

    .line 1791284
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVe;
    .locals 4

    .prologue
    .line 1791285
    invoke-static {}, LX/BVe;->values()[LX/BVe;

    move-result-object v1

    .line 1791286
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791287
    aget-object v2, v1, v0

    .line 1791288
    iget-object v3, v2, LX/BVe;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791289
    return-object v2

    .line 1791290
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791291
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown image view attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVe;
    .locals 1

    .prologue
    .line 1791292
    const-class v0, LX/BVe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVe;

    return-object v0
.end method

.method public static values()[LX/BVe;
    .locals 1

    .prologue
    .line 1791293
    sget-object v0, LX/BVe;->$VALUES:[LX/BVe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVe;

    return-object v0
.end method
