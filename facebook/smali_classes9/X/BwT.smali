.class public final LX/BwT;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7M9;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 1833811
    iput-object p1, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7M9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1833801
    const-class v0, LX/7M9;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1833802
    check-cast p1, LX/7M9;

    .line 1833803
    iget-object v0, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->D:Z

    if-eqz v0, :cond_0

    .line 1833804
    iget-object v0, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v0, p1, LX/7M9;->a:LX/7M8;

    sget-object v2, LX/7M8;->FADE_IN:LX/7M8;

    if-ne v0, v2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1833805
    :cond_0
    sget-object v0, LX/BwR;->a:[I

    iget-object v1, p1, LX/7M9;->a:LX/7M8;

    invoke-virtual {v1}, LX/7M8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1833806
    :goto_1
    return-void

    .line 1833807
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1833808
    :pswitch_0
    iget-object v0, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-static {v0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->u(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V

    goto :goto_1

    .line 1833809
    :pswitch_1
    iget-object v0, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b()V

    .line 1833810
    iget-object v0, p0, LX/BwT;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-virtual {v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->i()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
