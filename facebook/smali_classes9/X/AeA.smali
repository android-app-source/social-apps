.class public final LX/AeA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AeB;


# direct methods
.method public constructor <init>(LX/AeB;)V
    .locals 0

    .prologue
    .line 1696763
    iput-object p1, p0, LX/AeA;->a:LX/AeB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1696764
    iget-object v0, p0, LX/AeA;->a:LX/AeB;

    iget-object v0, v0, LX/AeB;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AeB;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch recent invitees for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AeA;->a:LX/AeB;

    iget-object v3, v3, LX/AeB;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1696765
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1696766
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1696767
    if-eqz p1, :cond_0

    .line 1696768
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1696769
    if-nez v0, :cond_1

    .line 1696770
    :cond_0
    :goto_0
    return-void

    .line 1696771
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1696772
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;

    .line 1696773
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1696774
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1696775
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel;

    .line 1696776
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1696777
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel$RecentLiveViewerInviteesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1696778
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1696779
    :cond_3
    iget-object v0, p0, LX/AeA;->a:LX/AeB;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1696780
    iput-object v1, v0, LX/AeB;->j:LX/0Px;

    .line 1696781
    goto :goto_0
.end method
