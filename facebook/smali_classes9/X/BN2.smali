.class public final LX/BN2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/profilelist/ProfilesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profilelist/ProfilesListFragment;)V
    .locals 0

    .prologue
    .line 1778212
    iput-object p1, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1778213
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const v0, 0x108005a

    .line 1778214
    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    const v1, 0x7f0824ad

    .line 1778215
    :goto_1
    iget-object v2, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v2, v2, Lcom/facebook/profilelist/ProfilesListFragment;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1778216
    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->e:Landroid/widget/ImageView;

    iget-object v2, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1778217
    return-void

    .line 1778218
    :cond_0
    const v0, 0x7f020d3d

    goto :goto_0

    .line 1778219
    :cond_1
    const v1, 0x7f0824ac

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1778220
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1778221
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1778222
    iget-object v1, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    .line 1778223
    iput-object v0, v1, Lcom/facebook/profilelist/ProfilesListFragment;->h:Ljava/lang/String;

    .line 1778224
    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->k:LX/BMq;

    invoke-interface {v0}, LX/BMq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1778225
    :cond_0
    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    .line 1778226
    invoke-static {v0}, Lcom/facebook/profilelist/ProfilesListFragment;->e$redex0(Lcom/facebook/profilelist/ProfilesListFragment;)V

    .line 1778227
    :goto_0
    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollEnabled(Z)V

    .line 1778228
    return-void

    .line 1778229
    :cond_1
    iget-object v0, p0, LX/BN2;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-virtual {v0, v2, v2}, Lcom/facebook/profilelist/ProfilesListFragment;->a(ZZ)V

    goto :goto_0
.end method
