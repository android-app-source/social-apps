.class public final LX/BL4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V
    .locals 0

    .prologue
    .line 1775173
    iput-object p1, p0, LX/BL4;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1775178
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1775179
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1775174
    iget-object v0, p0, LX/BL4;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1775175
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->n:Ljava/lang/String;

    .line 1775176
    iget-object v0, p0, LX/BL4;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->c:Landroid/widget/Filter;

    iget-object v1, p0, LX/BL4;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1775177
    return-void
.end method
