.class public LX/CAv;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/CAv;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855827
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1855828
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1855829
    const-string v1, "profiles"

    const/4 v2, 0x0

    new-array v2, v2, [J

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 1855830
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PROMPT_INVITE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1855831
    const-string v1, "prompt/{%s}invite"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/CAr;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1855832
    return-void
.end method

.method public static a(LX/0QB;)LX/CAv;
    .locals 3

    .prologue
    .line 1855833
    sget-object v0, LX/CAv;->a:LX/CAv;

    if-nez v0, :cond_1

    .line 1855834
    const-class v1, LX/CAv;

    monitor-enter v1

    .line 1855835
    :try_start_0
    sget-object v0, LX/CAv;->a:LX/CAv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1855836
    if-eqz v2, :cond_0

    .line 1855837
    :try_start_1
    new-instance v0, LX/CAv;

    invoke-direct {v0}, LX/CAv;-><init>()V

    .line 1855838
    move-object v0, v0

    .line 1855839
    sput-object v0, LX/CAv;->a:LX/CAv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855840
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1855841
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1855842
    :cond_1
    sget-object v0, LX/CAv;->a:LX/CAv;

    return-object v0

    .line 1855843
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1855844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
