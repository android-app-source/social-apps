.class public LX/CfM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CfL;


# instance fields
.field public final a:LX/2d1;

.field public final b:Lcom/facebook/reaction/ReactionQueryParams;

.field private final c:Lcom/facebook/reaction/ReactionUtil;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfU;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Z

.field private final l:LX/2iz;

.field private final m:Z

.field private final n:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Landroid/content/Context;LX/0Ot;LX/0Or;LX/2d1;LX/2iz;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/CfU;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2d1;",
            "LX/2iz;",
            "Lcom/facebook/reaction/ReactionUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925892
    iput-object p1, p0, LX/CfM;->d:Ljava/lang/String;

    .line 1925893
    iput-object p4, p0, LX/CfM;->e:Landroid/content/Context;

    .line 1925894
    iput-object p7, p0, LX/CfM;->a:LX/2d1;

    .line 1925895
    iput-object p5, p0, LX/CfM;->j:LX/0Ot;

    .line 1925896
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iput-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925897
    iput-object p9, p0, LX/CfM;->c:Lcom/facebook/reaction/ReactionUtil;

    .line 1925898
    const-string v0, "ANDROID_FEED_COMPOSER"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1925899
    if-eqz v0, :cond_0

    .line 1925900
    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1925901
    const-string p3, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    .line 1925902
    :cond_0
    :goto_1
    move-object v0, p3

    .line 1925903
    iput-object v0, p0, LX/CfM;->n:Ljava/lang/String;

    .line 1925904
    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CfM;->o:Ljava/lang/String;

    .line 1925905
    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    .line 1925906
    iget-object v1, v0, LX/2d1;->a:LX/0Uh;

    const/16 p3, 0x5d9

    const/4 p4, 0x0

    invoke-virtual {v1, p3, p4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1925907
    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/CfM;->n:Ljava/lang/String;

    const/4 p2, 0x0

    const/4 v1, 0x1

    .line 1925908
    invoke-static {v0}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_6

    .line 1925909
    :goto_2
    :pswitch_0
    move v0, v1

    .line 1925910
    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, LX/CfM;->g:Z

    .line 1925911
    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    .line 1925912
    iget-object v1, v0, LX/2d1;->a:LX/0Uh;

    const/16 p2, 0x5da

    const/4 p3, 0x0

    invoke-virtual {v1, p2, p3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1925913
    iput-boolean v0, p0, LX/CfM;->h:Z

    .line 1925914
    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    .line 1925915
    iget-object v1, v0, LX/2d1;->a:LX/0Uh;

    const/16 p2, 0x5db

    const/4 p3, 0x0

    invoke-virtual {v1, p2, p3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1925916
    iput-boolean v0, p0, LX/CfM;->i:Z

    .line 1925917
    iget-object v0, p7, LX/2d1;->a:LX/0Uh;

    const/16 v1, 0x5de

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 1925918
    iput-boolean v0, p0, LX/CfM;->k:Z

    .line 1925919
    iget-object v0, p0, LX/CfM;->a:LX/2d1;

    .line 1925920
    iget-object v1, v0, LX/2d1;->a:LX/0Uh;

    const/16 p2, 0x5df

    const/4 p3, 0x0

    invoke-virtual {v1, p2, p3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1925921
    iput-boolean v0, p0, LX/CfM;->m:Z

    .line 1925922
    iput-object p8, p0, LX/CfM;->l:LX/2iz;

    .line 1925923
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-nez v0, :cond_2

    .line 1925924
    iget-object v0, p0, LX/CfM;->l:LX/2iz;

    iget-object v1, p0, LX/CfM;->n:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    .line 1925925
    :cond_2
    return-void

    .line 1925926
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 1925927
    :cond_4
    const-string p3, "ANDROID_FEED_CHECKIN_SUGGESTION"

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1925928
    :cond_6
    const/4 p3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p4

    sparse-switch p4, :sswitch_data_0

    :cond_7
    :goto_4
    packed-switch p3, :pswitch_data_0

    move v1, p2

    .line 1925929
    goto :goto_2

    .line 1925930
    :sswitch_0
    const-string p4, "ANDROID_PAGE_ADMIN_COMPOSER"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    move p3, p2

    goto :goto_4

    :sswitch_1
    const-string p4, "ANDROID_PMA_PAGE_ADMIN_COMPOSER"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    move p3, v1

    goto :goto_4

    :sswitch_2
    const-string p4, "ANDROID_PAGE_LIKES_CONTEXT_ITEM"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p3, 0x2

    goto :goto_4

    :sswitch_3
    const-string p4, "ANDROID_POPULAR_AT_PLACE_CONTEXT_ITEM"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p3, 0x3

    goto :goto_4

    :sswitch_4
    const-string p4, "ANDROID_PAGE_FRIENDS_CONTENT_CONTEXT_ITEM"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p3, 0x4

    goto :goto_4

    :sswitch_5
    const-string p4, "ANDROID_PAGE_SANDBOX"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p3, 0x5

    goto :goto_4

    :sswitch_6
    const-string p4, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_7

    const/4 p3, 0x6

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        -0xf7fcceb -> :sswitch_1
        0x2cd9d1f0 -> :sswitch_0
        0x4499dd46 -> :sswitch_2
        0x549034b1 -> :sswitch_3
        0x57386ce7 -> :sswitch_5
        0x721c4433 -> :sswitch_4
        0x7aeb3c99 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1925889
    iget-object v0, p0, LX/CfM;->c:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    iget-object v2, p0, LX/CfM;->d:Ljava/lang/String;

    iget-object v3, p0, LX/CfM;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925890
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1925885
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    invoke-virtual {v0, p1}, Lcom/facebook/reaction/ReactionQueryParams;->a(Ljava/util/List;)Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925886
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CfM;->f:Z

    .line 1925887
    invoke-direct {p0}, LX/CfM;->a()V

    .line 1925888
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1925878
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CfM;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925879
    iget-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    move-object v0, v1

    .line 1925880
    invoke-virtual {v0, p1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1925881
    :cond_0
    :goto_0
    return-void

    .line 1925882
    :cond_1
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925883
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    .line 1925884
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0
.end method

.method public final a(LX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1925873
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-eqz v0, :cond_0

    .line 1925874
    :goto_0
    return-void

    .line 1925875
    :cond_0
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925876
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    .line 1925877
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 2
    .param p1    # Lcom/facebook/composer/minutiae/model/MinutiaeObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1925860
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-eqz v0, :cond_0

    .line 1925861
    :goto_0
    return-void

    .line 1925862
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    if-eqz v0, :cond_1

    .line 1925863
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    iget-object v1, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1925864
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    .line 1925865
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    iget-object v1, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 1925866
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    .line 1925867
    :goto_1
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0

    .line 1925868
    :cond_1
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925869
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    .line 1925870
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925871
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    .line 1925872
    goto :goto_1
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V
    .locals 2

    .prologue
    .line 1925851
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CfM;->m:Z

    if-nez v0, :cond_1

    .line 1925852
    :cond_0
    :goto_0
    return-void

    .line 1925853
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_2

    .line 1925854
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    iget-object v1, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v1

    .line 1925855
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    .line 1925856
    :goto_1
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0

    .line 1925857
    :cond_2
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    const/4 v1, 0x0

    .line 1925858
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    .line 1925859
    goto :goto_1
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 4
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1925833
    iget-boolean v1, p0, LX/CfM;->g:Z

    if-eqz v1, :cond_1

    .line 1925834
    :cond_0
    :goto_0
    return-void

    .line 1925835
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    move-object v1, v0

    .line 1925836
    :goto_1
    if-nez p1, :cond_6

    .line 1925837
    :goto_2
    iget-object v2, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925838
    iget-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    move-object v2, v3

    .line 1925839
    if-nez v2, :cond_3

    if-eqz v1, :cond_0

    :cond_3
    iget-object v2, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925840
    iget-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    move-object v2, v3

    .line 1925841
    if-eqz v2, :cond_4

    iget-object v2, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925842
    iget-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    move-object v2, v3

    .line 1925843
    invoke-virtual {v2, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1925844
    :cond_4
    iget-object v2, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925845
    iput-object v1, v2, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 1925846
    iget-object v1, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925847
    iput-object v0, v1, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    .line 1925848
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0

    .line 1925849
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_1

    .line 1925850
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    .line 1925753
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CfM;->h:Z

    if-nez v0, :cond_1

    .line 1925754
    :cond_0
    :goto_0
    return-void

    .line 1925755
    :cond_1
    new-instance v0, LX/88v;

    invoke-direct {v0, p1}, LX/88v;-><init>(Ljava/lang/CharSequence;)V

    .line 1925756
    invoke-virtual {v0}, LX/88v;->b()Ljava/util/List;

    move-result-object v2

    .line 1925757
    const/4 v9, 0x2

    .line 1925758
    sget-object v1, LX/88v;->g:Ljava/util/regex/Pattern;

    iget-object v3, v0, LX/88v;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1925759
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v3

    .line 1925760
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1925761
    new-instance v4, LX/1yN;

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->start(I)I

    move-result v6

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->end(I)I

    move-result v7

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->start(I)I

    move-result v8

    sub-int/2addr v7, v8

    invoke-direct {v4, v6, v7}, LX/1yN;-><init>(II)V

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1925762
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v3, v1

    .line 1925763
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925764
    iget-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    move-object v4, v1

    .line 1925765
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1925766
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    .line 1925767
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    invoke-interface {v4, v2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2, v4}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1925768
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 1925769
    invoke-direct {p0, v2}, LX/CfM;->a(Ljava/util/List;)V

    goto :goto_0

    .line 1925770
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_7

    .line 1925771
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1925772
    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1925773
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/ReactionQueryParams;->a(Ljava/util/List;)Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925774
    iput-boolean v5, p0, LX/CfM;->f:Z

    goto/16 :goto_0

    .line 1925775
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1925776
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1925777
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yN;

    .line 1925778
    iget v4, v0, LX/1yN;->a:I

    move v4, v4

    .line 1925779
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yN;

    .line 1925780
    iget v1, v0, LX/1yN;->b:I

    move v0, v1

    .line 1925781
    add-int/2addr v0, v4

    .line 1925782
    iget-object v1, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925783
    iget-object v4, v1, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    move-object v1, v4

    .line 1925784
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_9

    iget-object v1, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925785
    iget-object v4, v1, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    move-object v1, v4

    .line 1925786
    invoke-virtual {v1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1925787
    if-eqz v0, :cond_8

    .line 1925788
    invoke-direct {p0, v2}, LX/CfM;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 1925789
    :cond_8
    iput-boolean v5, p0, LX/CfM;->f:Z

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 1925820
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-eqz v0, :cond_1

    .line 1925821
    :cond_0
    :goto_0
    return-void

    .line 1925822
    :cond_1
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925823
    iget-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    move-object v0, v1

    .line 1925824
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1925825
    if-nez v0, :cond_2

    iget-object v0, p0, LX/CfM;->o:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1925826
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925827
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 1925828
    goto :goto_0

    .line 1925829
    :cond_2
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925830
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 1925831
    iget-boolean v0, p0, LX/CfM;->k:Z

    if-eqz v0, :cond_0

    .line 1925832
    invoke-direct {p0}, LX/CfM;->a()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1925817
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-eqz v0, :cond_0

    .line 1925818
    :goto_0
    return-void

    .line 1925819
    :cond_0
    iget-object v0, p0, LX/CfM;->l:LX/2iz;

    iget-object v1, p0, LX/CfM;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2iz;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 1925790
    iget-boolean v0, p0, LX/CfM;->g:Z

    if-eqz v0, :cond_1

    .line 1925791
    :cond_0
    :goto_0
    return-void

    .line 1925792
    :cond_1
    iget-boolean v0, p0, LX/CfM;->f:Z

    if-eqz v0, :cond_2

    .line 1925793
    iget-object v0, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    new-instance v1, LX/88v;

    iget-object v2, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925794
    iget-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    move-object v2, v3

    .line 1925795
    invoke-direct {v1, v2}, LX/88v;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, LX/88v;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/ReactionQueryParams;->a(Ljava/util/List;)Lcom/facebook/reaction/ReactionQueryParams;

    .line 1925796
    invoke-direct {p0}, LX/CfM;->a()V

    .line 1925797
    :cond_2
    iget-object v0, p0, LX/CfM;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfU;

    iget-object v1, p0, LX/CfM;->l:LX/2iz;

    iget-object v2, p0, LX/CfM;->d:Ljava/lang/String;

    iget-object v3, p0, LX/CfM;->b:Lcom/facebook/reaction/ReactionQueryParams;

    const/4 v4, 0x1

    .line 1925798
    invoke-virtual {v1, v2}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v5

    .line 1925799
    if-eqz v5, :cond_3

    .line 1925800
    if-eqz v3, :cond_6

    .line 1925801
    iget-object v6, v3, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    move-object v6, v6

    .line 1925802
    if-eqz v6, :cond_6

    .line 1925803
    iget-object v6, v3, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    move-object v6, v6

    .line 1925804
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    const/4 v6, 0x1

    :goto_1
    move v6, v6

    .line 1925805
    if-eqz v6, :cond_3

    .line 1925806
    iget-object v6, v5, LX/2jY;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1925807
    invoke-static {v0, v6}, LX/CfU;->b(LX/CfU;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1925808
    :cond_3
    const/4 v4, 0x0

    .line 1925809
    :goto_2
    move v0, v4

    .line 1925810
    if-nez v0, :cond_0

    .line 1925811
    iget-object v0, p0, LX/CfM;->l:LX/2iz;

    iget-object v1, p0, LX/CfM;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2iz;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1925812
    :cond_4
    iget-boolean v6, v5, LX/2jY;->n:Z

    move v6, v6

    .line 1925813
    if-eqz v6, :cond_5

    .line 1925814
    iput-boolean v4, v5, LX/2jY;->q:Z

    .line 1925815
    goto :goto_2

    .line 1925816
    :cond_5
    invoke-virtual {v0, v1, v5, v3}, LX/CfU;->a(LX/2iz;LX/2jY;Lcom/facebook/reaction/ReactionQueryParams;)Z

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_1
.end method
