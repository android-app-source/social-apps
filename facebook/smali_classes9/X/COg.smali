.class public final LX/COg;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COg;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COe;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883388
    const/4 v0, 0x0

    sput-object v0, LX/COg;->a:LX/COg;

    .line 1883389
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COg;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883416
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883417
    new-instance v0, LX/COh;

    invoke-direct {v0}, LX/COh;-><init>()V

    iput-object v0, p0, LX/COg;->c:LX/COh;

    .line 1883418
    return-void
.end method

.method public static declared-synchronized q()LX/COg;
    .locals 2

    .prologue
    .line 1883412
    const-class v1, LX/COg;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COg;->a:LX/COg;

    if-nez v0, :cond_0

    .line 1883413
    new-instance v0, LX/COg;

    invoke-direct {v0}, LX/COg;-><init>()V

    sput-object v0, LX/COg;->a:LX/COg;

    .line 1883414
    :cond_0
    sget-object v0, LX/COg;->a:LX/COg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883415
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1883392
    check-cast p2, LX/COf;

    .line 1883393
    iget-object v0, p2, LX/COf;->a:LX/CNb;

    iget-object v1, p2, LX/COf;->b:LX/CNq;

    iget-object v2, p2, LX/COf;->c:Ljava/util/List;

    .line 1883394
    const-string v3, "video"

    invoke-virtual {v0, v3}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1883395
    instance-of v4, v3, Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v4, :cond_0

    invoke-virtual {v1}, LX/CNq;->j()LX/COk;

    move-result-object v4

    if-nez v4, :cond_1

    .line 1883396
    :cond_0
    const/4 v3, 0x0

    .line 1883397
    :goto_0
    move-object v0, v3

    .line 1883398
    return-object v0

    :cond_1
    invoke-virtual {v1}, LX/CNq;->j()LX/COk;

    move-result-object v4

    const/4 p0, 0x0

    .line 1883399
    new-instance p2, LX/COj;

    invoke-direct {p2, v4}, LX/COj;-><init>(LX/COk;)V

    .line 1883400
    sget-object v1, LX/COk;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/COi;

    .line 1883401
    if-nez v1, :cond_2

    .line 1883402
    new-instance v1, LX/COi;

    invoke-direct {v1}, LX/COi;-><init>()V

    .line 1883403
    :cond_2
    invoke-static {v1, p1, p0, p0, p2}, LX/COi;->a$redex0(LX/COi;LX/1De;IILX/COj;)V

    .line 1883404
    move-object p2, v1

    .line 1883405
    move-object p0, p2

    .line 1883406
    move-object v4, p0

    .line 1883407
    check-cast v3, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1883408
    iget-object p0, v4, LX/COi;->a:LX/COj;

    iput-object v3, p0, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1883409
    iget-object p0, v4, LX/COi;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1883410
    move-object v3, v4

    .line 1883411
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883390
    invoke-static {}, LX/1dS;->b()V

    .line 1883391
    const/4 v0, 0x0

    return-object v0
.end method
