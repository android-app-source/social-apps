.class public final LX/B3h;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0d;

.field public final synthetic b:LX/B0M;

.field public final synthetic c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;LX/B0M;)V
    .locals 0

    .prologue
    .line 1740709
    iput-object p1, p0, LX/B3h;->c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    iput-object p2, p0, LX/B3h;->a:LX/B0d;

    iput-object p3, p0, LX/B3h;->b:LX/B0M;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1740710
    iget-object v0, p0, LX/B3h;->b:LX/B0M;

    invoke-virtual {v0, p1}, LX/B0M;->a(Ljava/lang/Throwable;)V

    .line 1740711
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1740712
    check-cast p1, Ljava/util/List;

    .line 1740713
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1740714
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1740715
    iget-object v2, p0, LX/B3h;->c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    iget-object v3, p0, LX/B3h;->a:LX/B0d;

    invoke-static {v2, v3, v0}, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->b(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;

    move-result-object v3

    .line 1740716
    if-eqz v3, :cond_1

    invoke-interface {v3}, LX/B0N;->d()LX/B0d;

    move-result-object v0

    move-object v2, v0

    .line 1740717
    :goto_0
    iget-object v4, p0, LX/B3h;->c:Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    .line 1740718
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1740719
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v0}, LX/9JH;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;

    invoke-static {v4, v2, v0}, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->a$redex0(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;)LX/B0N;

    move-result-object v0

    .line 1740720
    if-eqz v3, :cond_0

    .line 1740721
    iget-object v1, p0, LX/B3h;->b:LX/B0M;

    invoke-virtual {v1, v3}, LX/B0M;->a(LX/B0N;)V

    .line 1740722
    :cond_0
    iget-object v1, p0, LX/B3h;->b:LX/B0M;

    invoke-virtual {v1, v0}, LX/B0M;->a(LX/B0N;)V

    .line 1740723
    iget-object v0, p0, LX/B3h;->b:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1740724
    return-void

    .line 1740725
    :cond_1
    iget-object v0, p0, LX/B3h;->a:LX/B0d;

    move-object v2, v0

    goto :goto_0
.end method
