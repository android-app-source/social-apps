.class public final LX/Bun;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V
    .locals 0

    .prologue
    .line 1831564
    iput-object p1, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;B)V
    .locals 0

    .prologue
    .line 1831563
    invoke-direct {p0, p1}, LX/Bun;-><init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    return-void
.end method

.method private a(LX/2ou;)V
    .locals 3

    .prologue
    .line 1831553
    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1831554
    :cond_0
    :goto_0
    return-void

    .line 1831555
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->n:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v0

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_2

    .line 1831556
    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iget-object v0, v0, LX/7MR;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1831557
    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    const v1, 0x7f020bbf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(ILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 1831558
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1831559
    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->e:Z

    .line 1831560
    :cond_3
    iget-object v0, p0, LX/Bun;->a:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->i(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1831562
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1831561
    check-cast p1, LX/2ou;

    invoke-direct {p0, p1}, LX/Bun;->a(LX/2ou;)V

    return-void
.end method
