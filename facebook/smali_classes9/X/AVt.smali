.class public LX/AVt;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

.field public final b:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

.field public final c:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

.field public final d:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

.field public final e:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1680491
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AVt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1680492
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1680493
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AVt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680494
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1680495
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680496
    const v0, 0x7f0305ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1680497
    const v0, 0x7f0d0f89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    iput-object v0, p0, LX/AVt;->a:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;

    .line 1680498
    const v0, 0x7f0d0f8a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    iput-object v0, p0, LX/AVt;->b:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;

    .line 1680499
    const v0, 0x7f0d0f8b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iput-object v0, p0, LX/AVt;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1680500
    const v0, 0x7f0d0f8c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    iput-object v0, p0, LX/AVt;->d:Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;

    .line 1680501
    const v0, 0x7f0d0f8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    iput-object v0, p0, LX/AVt;->e:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1680502
    return-void
.end method
