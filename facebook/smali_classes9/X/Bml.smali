.class public final LX/Bml;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/events/ui/location/EventLocationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1818928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1818929
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818930
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 1818931
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1818932
    new-instance v4, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {v4, v0, v2, v3, v1}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;JLjava/lang/String;)V

    return-object v4
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1818933
    new-array v0, p1, [Lcom/facebook/events/ui/location/EventLocationModel;

    return-object v0
.end method
