.class public LX/AnV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AnT;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1712536
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AnV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AnW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712537
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1712538
    iput-object p1, p0, LX/AnV;->b:LX/0Ot;

    .line 1712539
    return-void
.end method

.method public static a(LX/0QB;)LX/AnV;
    .locals 4

    .prologue
    .line 1712540
    const-class v1, LX/AnV;

    monitor-enter v1

    .line 1712541
    :try_start_0
    sget-object v0, LX/AnV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712542
    sput-object v2, LX/AnV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712543
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712544
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712545
    new-instance v3, LX/AnV;

    const/16 p0, 0x1e77

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/AnV;-><init>(LX/0Ot;)V

    .line 1712546
    move-object v0, v3

    .line 1712547
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712548
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AnV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712549
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1712551
    const v0, -0x5407fcd4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1712552
    check-cast p2, LX/AnU;

    .line 1712553
    iget-object v0, p0, LX/AnV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnW;

    iget-object v1, p2, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0, p1, v1}, LX/AnW;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/1Dg;

    move-result-object v0

    .line 1712554
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 1712555
    invoke-static {}, LX/1dS;->b()V

    .line 1712556
    iget v0, p1, LX/1dQ;->b:I

    .line 1712557
    sparse-switch v0, :sswitch_data_0

    .line 1712558
    :goto_0
    return-object v2

    .line 1712559
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1712560
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1712561
    check-cast v1, LX/AnU;

    .line 1712562
    iget-object v3, p0, LX/AnV;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AnW;

    iget-object v4, v1, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object p1, v1, LX/AnU;->b:LX/162;

    iget-boolean p2, v1, LX/AnU;->c:Z

    .line 1712563
    invoke-static {v3, v0, v4, p1, p2}, LX/AnW;->c(LX/AnW;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/162;Z)V

    .line 1712564
    goto :goto_0

    .line 1712565
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1712566
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1712567
    check-cast v1, LX/AnU;

    .line 1712568
    iget-object v3, p0, LX/AnV;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AnW;

    iget-object v4, v1, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object p1, v1, LX/AnU;->b:LX/162;

    iget-boolean p2, v1, LX/AnU;->c:Z

    .line 1712569
    invoke-static {v3, v0, v4, p1, p2}, LX/AnW;->c(LX/AnW;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/162;Z)V

    .line 1712570
    goto :goto_0

    .line 1712571
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 1712572
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1712573
    check-cast v1, LX/AnU;

    .line 1712574
    iget-object v3, p0, LX/AnV;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AnW;

    iget-object v4, v1, LX/AnU;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v5, v1, LX/AnU;->b:LX/162;

    iget-boolean v6, v1, LX/AnU;->c:Z

    .line 1712575
    iget-object v8, v3, LX/AnW;->d:LX/Anb;

    if-eqz v6, :cond_1

    const-string v7, "composer"

    :goto_1
    const-string v9, "ellipse"

    invoke-virtual {v8, v5, v7, v9}, LX/Anb;->a(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1712576
    iget-object v7, v3, LX/AnW;->e:LX/Ana;

    .line 1712577
    iget-object v8, v7, LX/Ana;->b:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3Af;

    .line 1712578
    invoke-virtual {v8}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Landroid/app/Activity;

    invoke-static {v9, v10}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/Activity;

    .line 1712579
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/app/Activity;->isFinishing()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1712580
    :cond_0
    :goto_2
    goto :goto_0

    .line 1712581
    :cond_1
    const-string v7, "native_newsfeed"

    goto :goto_1

    .line 1712582
    :cond_2
    new-instance v10, LX/7TY;

    invoke-direct {v10, v9}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1712583
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v3

    .line 1712584
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v9, 0x0

    move v1, v9

    :goto_3
    if-ge v1, v5, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/graphql/model/GraphQLMisinformationAction;

    .line 1712585
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->l()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v11

    .line 1712586
    sget-object p0, LX/AnZ;->a:[I

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    .line 1712587
    const p0, 0x7f0208ec

    :goto_4
    move p0, p0

    .line 1712588
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v0

    move-object v9, v7

    move-object v11, v10

    .line 1712589
    invoke-interface {v11, v12}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    .line 1712590
    instance-of v6, v4, LX/3Ai;

    if-eqz v6, :cond_3

    move-object v6, v4

    .line 1712591
    check-cast v6, LX/3Ai;

    invoke-virtual {v6, p1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1712592
    :cond_3
    invoke-interface {v4, p0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1712593
    new-instance v6, LX/AnY;

    invoke-direct {v6, v9, p2, v0}, LX/AnY;-><init>(LX/Ana;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;)V

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1712594
    add-int/lit8 v9, v1, 0x1

    move v1, v9

    goto :goto_3

    .line 1712595
    :cond_4
    invoke-virtual {v8, v10}, LX/3Af;->a(LX/1OM;)V

    .line 1712596
    invoke-static {v8}, LX/4ml;->a(Landroid/app/Dialog;)V

    .line 1712597
    new-instance v9, LX/AnX;

    invoke-direct {v9, v7}, LX/AnX;-><init>(LX/Ana;)V

    invoke-virtual {v8, v9}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_2

    .line 1712598
    :pswitch_0
    const p0, 0x7f0208ec

    goto :goto_4

    .line 1712599
    :pswitch_1
    const p0, 0x7f0208f1

    goto :goto_4

    .line 1712600
    :pswitch_2
    const p0, 0x7f0209ae

    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5407fcd4 -> :sswitch_0
        -0x5407e3a4 -> :sswitch_2
        -0x5407dcfb -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/AnT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1712601
    new-instance v1, LX/AnU;

    invoke-direct {v1, p0}, LX/AnU;-><init>(LX/AnV;)V

    .line 1712602
    sget-object v2, LX/AnV;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AnT;

    .line 1712603
    if-nez v2, :cond_0

    .line 1712604
    new-instance v2, LX/AnT;

    invoke-direct {v2}, LX/AnT;-><init>()V

    .line 1712605
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/AnT;->a$redex0(LX/AnT;LX/1De;IILX/AnU;)V

    .line 1712606
    move-object v1, v2

    .line 1712607
    move-object v0, v1

    .line 1712608
    return-object v0
.end method
