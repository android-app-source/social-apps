.class public final LX/Byd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/0jQ;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1eK;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1eK;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1eK;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1837488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837489
    iput-object p1, p0, LX/Byd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837490
    iput-object p2, p0, LX/Byd;->b:LX/1eK;

    .line 1837491
    iput-object p3, p0, LX/Byd;->c:Ljava/lang/String;

    .line 1837492
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/Byd;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837493
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1837494
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/Byd;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1837495
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 1837497
    iget-object v0, p0, LX/Byd;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837498
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1837499
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837496
    iget-object v0, p0, LX/Byd;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
