.class public LX/CBW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/text/Layout$Alignment;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1856476
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/CBW;->a:Landroid/text/Layout$Alignment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856478
    return-void
.end method

.method public static a(LX/0QB;)LX/CBW;
    .locals 3

    .prologue
    .line 1856479
    const-class v1, LX/CBW;

    monitor-enter v1

    .line 1856480
    :try_start_0
    sget-object v0, LX/CBW;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856481
    sput-object v2, LX/CBW;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856482
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856483
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1856484
    new-instance v0, LX/CBW;

    invoke-direct {v0}, LX/CBW;-><init>()V

    .line 1856485
    move-object v0, v0

    .line 1856486
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856487
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856488
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
