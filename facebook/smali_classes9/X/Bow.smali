.class public LX/Bow;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1822269
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1822270
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1822273
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1822274
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1822275
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1822276
    const v0, 0x7f030f2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1822277
    const v0, 0x7f0d24c7

    invoke-virtual {p0, v0}, LX/Bow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bow;->a:Landroid/view/View;

    .line 1822278
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1822271
    iget-object v0, p0, LX/Bow;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1822272
    return-void
.end method
