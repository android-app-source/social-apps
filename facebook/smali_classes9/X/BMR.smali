.class public LX/BMR;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/1aS;


# instance fields
.field private final j:Lcom/facebook/widget/text/BetterTextView;

.field private final k:Lcom/facebook/widget/text/BetterTextView;

.field private final l:Lcom/facebook/productionprompts/common/v3/OverlappingImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1777253
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BMR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1777254
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1777251
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BMR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777252
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/16 v1, 0x10

    .line 1777241
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777242
    const v0, 0x7f030df8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1777243
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1777244
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1777245
    const v0, 0x7f0d2225

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;

    iput-object v0, p0, LX/BMR;->l:Lcom/facebook/productionprompts/common/v3/OverlappingImageView;

    .line 1777246
    iget-object v0, p0, LX/BMR;->l:Lcom/facebook/productionprompts/common/v3/OverlappingImageView;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->setRotateDegree(I)V

    .line 1777247
    iget-object v0, p0, LX/BMR;->l:Lcom/facebook/productionprompts/common/v3/OverlappingImageView;

    invoke-virtual {p0}, LX/BMR;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0}, LX/BMR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1174

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(II)V

    .line 1777248
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BMR;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 1777249
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BMR;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1777250
    return-void
.end method


# virtual methods
.method public final a(LX/Alv;LX/Alv;)V
    .locals 1

    .prologue
    .line 1777239
    iget-object v0, p0, LX/BMR;->l:Lcom/facebook/productionprompts/common/v3/OverlappingImageView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(LX/Alv;LX/Alv;)V

    .line 1777240
    return-void
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1777238
    iget-object v0, p0, LX/BMR;->k:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1777237
    iget-object v0, p0, LX/BMR;->j:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
