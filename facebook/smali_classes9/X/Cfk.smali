.class public abstract LX/Cfk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2jb;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0o8;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/ViewGroup;

.field public d:Landroid/content/Context;

.field private e:Landroid/view/LayoutInflater;

.field private f:Z

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final i:LX/3Tx;

.field public j:LX/Cgb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Tx;)V
    .locals 1

    .prologue
    .line 1926761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926762
    iput-object p1, p0, LX/Cfk;->i:LX/3Tx;

    .line 1926763
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cfk;->f:Z

    .line 1926764
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1926765
    new-instance v0, LX/Cfj;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Cfj;-><init>(LX/Cfk;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V

    return-object v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1926766
    iget-object v0, p0, LX/Cfk;->e:Landroid/view/LayoutInflater;

    iget-object v1, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
.end method

.method public final a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1926767
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 1926768
    sget-object v1, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    .line 1926769
    sget-object v4, LX/11R;->b:LX/11S;

    if-nez v4, :cond_0

    .line 1926770
    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    .line 1926771
    invoke-static {v4}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11S;

    sput-object v4, LX/11R;->b:LX/11S;

    .line 1926772
    :cond_0
    sget-object v4, LX/11R;->b:LX/11S;

    invoke-interface {v4, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 1926773
    return-object v0
.end method

.method public a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V
    .locals 1
    .param p3    # LX/0o8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1926774
    iput-object p1, p0, LX/Cfk;->a:LX/2jb;

    .line 1926775
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Cfk;->b:Ljava/lang/ref/WeakReference;

    .line 1926776
    iput-object p2, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    .line 1926777
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    .line 1926778
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Cfk;->e:Landroid/view/LayoutInflater;

    .line 1926779
    iput-object p4, p0, LX/Cfk;->g:Ljava/lang/String;

    .line 1926780
    iput-object p5, p0, LX/Cfk;->h:Ljava/lang/String;

    .line 1926781
    iput-object p6, p0, LX/Cfk;->j:LX/Cgb;

    .line 1926782
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cfk;->f:Z

    .line 1926783
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1926784
    iget-object v0, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1926785
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1926786
    iget-object v0, p0, LX/Cfk;->a:LX/2jb;

    invoke-interface {v0, p1, p2}, LX/2jb;->a(Ljava/lang/String;I)V

    .line 1926787
    return-void
.end method

.method public a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1926755
    iget-object v0, p0, LX/Cfk;->i:LX/3Tx;

    invoke-virtual {p0}, LX/Cfk;->c()LX/0o8;

    move-result-object v1

    .line 1926756
    iget-object v2, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v2, v2

    .line 1926757
    invoke-virtual {v0, p1, p2, v1, v2}, LX/3Tx;->a(Ljava/lang/String;LX/Cfl;LX/0o8;Landroid/content/Context;)V

    .line 1926758
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1926759
    iget-object v0, p0, LX/Cfk;->a:LX/2jb;

    invoke-interface {v0, p1, p2}, LX/2jb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926760
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 1926734
    iget-object v0, p0, LX/Cfk;->a:LX/2jb;

    invoke-interface {v0, p1, p4}, LX/2jb;->b(Ljava/lang/String;I)V

    .line 1926735
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1

    .prologue
    .line 1926723
    iget-object v0, p0, LX/Cfk;->a:LX/2jb;

    invoke-interface {v0, p1, p2, p3}, LX/2jb;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 1926724
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1926725
    iget-boolean v0, p0, LX/Cfk;->f:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1926726
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z
    .locals 1

    .prologue
    .line 1926727
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1926728
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 1926729
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    .line 1926730
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/Cfk;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1926731
    :goto_1
    return v0

    .line 1926732
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1926733
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z
    .locals 1

    .prologue
    .line 1926736
    iget-boolean v0, p0, LX/Cfk;->f:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1926737
    invoke-virtual {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    .line 1926738
    if-lez v0, :cond_0

    .line 1926739
    invoke-virtual {p0, p1, p2, v0, v0}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1926740
    const/4 v0, 0x1

    .line 1926741
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1926742
    invoke-virtual {p3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    .line 1926743
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 1926744
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    .line 1926745
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/Cfk;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move v0, v1

    .line 1926746
    :goto_1
    return v0

    .line 1926747
    :cond_1
    invoke-virtual {p0, v0}, LX/Cfk;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;

    move-result-object v5

    .line 1926748
    if-nez v5, :cond_2

    move v0, v1

    .line 1926749
    goto :goto_1

    .line 1926750
    :cond_2
    new-instance v6, LX/Cfj;

    invoke-direct {v6, p0, p1, p2, v0}, LX/Cfj;-><init>(LX/Cfk;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1926751
    invoke-virtual {p0, v5}, LX/Cfk;->a(Landroid/view/View;)V

    .line 1926752
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1926753
    :cond_3
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    goto :goto_1
.end method

.method public abstract b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
.end method

.method public final c()LX/0o8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1926754
    iget-object v0, p0, LX/Cfk;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0o8;

    return-object v0
.end method
