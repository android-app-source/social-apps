.class public LX/AgH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AgG;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# instance fields
.field public A:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1My;

.field private final d:LX/0gX;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Landroid/os/Handler;

.field public final g:LX/0SG;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/3Hd;

.field public final j:Landroid/content/res/Resources;

.field private final k:F

.field private final l:I

.field public final m:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile p:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/AgW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile t:Z

.field private volatile u:Z

.field private volatile v:J

.field public volatile w:J

.field private volatile x:D

.field public volatile y:Z

.field public final z:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/text/SpannableString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700599
    const-class v0, LX/AgH;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AgH;->a:Ljava/lang/String;

    .line 1700600
    const-class v0, LX/AgH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AgH;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1My;LX/0gX;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0SG;LX/0Ot;LX/0ad;LX/3Hd;Landroid/content/res/Resources;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/0gX;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/os/Handler;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/1zf;",
            ">;",
            "LX/0ad;",
            "LX/3Hd;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1700583
    iput-object p1, p0, LX/AgH;->c:LX/1My;

    .line 1700584
    iput-object p2, p0, LX/AgH;->d:LX/0gX;

    .line 1700585
    iput-object p3, p0, LX/AgH;->e:Ljava/util/concurrent/ExecutorService;

    .line 1700586
    iput-object p4, p0, LX/AgH;->f:Landroid/os/Handler;

    .line 1700587
    iput-object p5, p0, LX/AgH;->g:LX/0SG;

    .line 1700588
    iput-object p6, p0, LX/AgH;->h:LX/0Ot;

    .line 1700589
    iput-object p8, p0, LX/AgH;->i:LX/3Hd;

    .line 1700590
    iput-object p9, p0, LX/AgH;->j:Landroid/content/res/Resources;

    .line 1700591
    sget v0, LX/AgC;->e:F

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-interface {p7, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/AgH;->k:F

    .line 1700592
    sget v0, LX/AgC;->h:I

    const/16 v1, 0x7d0

    invoke-interface {p7, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/AgH;->l:I

    .line 1700593
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/AgH;->m:Landroid/util/SparseArray;

    .line 1700594
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/AgH;->n:Landroid/util/SparseArray;

    .line 1700595
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/AgH;->o:Landroid/util/SparseArray;

    .line 1700596
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/AgH;->z:Landroid/util/SparseArray;

    .line 1700597
    iget-object v0, p0, LX/AgH;->m:Landroid/util/SparseArray;

    iput-object v0, p0, LX/AgH;->A:Landroid/util/SparseArray;

    .line 1700598
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1700570
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/AgH;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1700571
    :goto_0
    monitor-exit p0

    return-void

    .line 1700572
    :cond_0
    :try_start_1
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/AgH;->s:Ljava/lang/String;

    .line 1700573
    new-instance v0, LX/AgF;

    invoke-direct {v0, p0}, LX/AgF;-><init>(LX/AgH;)V

    .line 1700574
    new-instance v1, LX/6Rl;

    invoke-direct {v1}, LX/6Rl;-><init>()V

    move-object v1, v1

    .line 1700575
    new-instance v2, LX/4Ek;

    invoke-direct {v2}, LX/4Ek;-><init>()V

    iget-object v3, p0, LX/AgH;->s:Ljava/lang/String;

    .line 1700576
    const-string p1, "feedback_id"

    invoke-virtual {v2, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700577
    move-object v2, v2

    .line 1700578
    const-string v3, "input"

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1700579
    :try_start_2
    iget-object v2, p0, LX/AgH;->d:LX/0gX;

    invoke-virtual {v2, v1, v0}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, LX/AgH;->p:LX/0gM;
    :try_end_2
    .catch LX/31B; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1700580
    :catch_0
    goto :goto_0

    .line 1700581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/AgH;Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;I)V
    .locals 3

    .prologue
    .line 1700564
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;->k()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1700565
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1700566
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/AgH;->n:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1700567
    iget-object v1, p0, LX/AgH;->n:Landroid/util/SparseArray;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1700568
    iget-object v0, p0, LX/AgH;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$3;-><init>(LX/AgH;ILcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionSubscriptionModel$ReactorModel;)V

    const v2, 0x5c836a32

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1700569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/AgH;Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1700505
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AgH;->s:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/AgH;->p:LX/0gM;

    if-nez v1, :cond_0

    .line 1700506
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/AgH;->a(Ljava/lang/String;)V

    .line 1700507
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;->j()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    .line 1700508
    :goto_0
    monitor-exit p0

    return-void

    .line 1700509
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/AgH;->m:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, p0, LX/AgH;->A:Landroid/util/SparseArray;

    .line 1700510
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;

    .line 1700511
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$ReactionInfoModel;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1700512
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$ReactionInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel$ReactionInfoModel;->j()I

    move-result v6

    .line 1700513
    iget-object v1, p0, LX/AgH;->m:Landroid/util/SparseArray;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1700514
    iget-boolean v1, p0, LX/AgH;->u:Z

    if-eqz v1, :cond_8

    .line 1700515
    iget-object v1, p0, LX/AgH;->n:Landroid/util/SparseArray;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v6, v8}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1700516
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;->a()I

    move-result v8

    sub-int v7, v8, v7

    .line 1700517
    sub-int v8, v1, v7

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1700518
    sub-int/2addr v7, v1

    .line 1700519
    if-lez v7, :cond_7

    .line 1700520
    iget-object v1, p0, LX/AgH;->o:Landroid/util/SparseArray;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v6, v9}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1700521
    iget-object v9, p0, LX/AgH;->o:Landroid/util/SparseArray;

    add-int/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1700522
    add-int v1, v2, v7

    .line 1700523
    :goto_2
    iget-object v2, p0, LX/AgH;->n:Landroid/util/SparseArray;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1700524
    :goto_3
    iget-object v2, p0, LX/AgH;->m:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel$ReactionsStreamModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    .line 1700525
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto/16 :goto_1

    .line 1700526
    :cond_2
    iget-boolean v0, p0, LX/AgH;->u:Z

    if-eqz v0, :cond_6

    .line 1700527
    new-instance v0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;-><init>(LX/AgH;)V

    .line 1700528
    iget-object v1, p0, LX/AgH;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/AgH;->v:J

    sub-long/2addr v4, v6

    .line 1700529
    int-to-double v2, v2

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    div-double/2addr v2, v4

    iput-wide v2, p0, LX/AgH;->x:D

    .line 1700530
    iget-boolean v1, p0, LX/AgH;->y:Z

    if-nez v1, :cond_4

    .line 1700531
    iget-object v1, p0, LX/AgH;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/AgH;->w:J

    sub-long/2addr v2, v4

    .line 1700532
    iget-wide v4, p0, LX/AgH;->x:D

    iget v1, p0, LX/AgH;->k:F

    float-to-double v6, v1

    cmpg-double v1, v4, v6

    if-ltz v1, :cond_3

    iget v1, p0, LX/AgH;->l:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_5

    .line 1700533
    :cond_3
    iget-object v1, p0, LX/AgH;->f:Landroid/os/Handler;

    const v2, -0x21ab6f1b

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1700534
    :goto_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AgH;->y:Z

    .line 1700535
    :cond_4
    :goto_6
    iget-object v0, p0, LX/AgH;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/AgH;->v:J

    .line 1700536
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AgH;->u:Z

    .line 1700537
    const/4 v2, 0x0

    .line 1700538
    iget-object v0, p0, LX/AgH;->i:LX/3Hd;

    invoke-virtual {v0}, LX/3Hd;->a()Z

    move-result v0

    if-nez v0, :cond_a
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1700539
    :goto_7
    goto/16 :goto_0

    .line 1700540
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1700541
    :cond_5
    :try_start_2
    iget-object v1, p0, LX/AgH;->f:Landroid/os/Handler;

    iget v4, p0, LX/AgH;->l:I

    int-to-long v4, v4

    sub-long v2, v4, v2

    const v4, 0x10955b73

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_5

    .line 1700542
    :cond_6
    iget-object v0, p0, LX/AgH;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/AgH;->w:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_7
    move v1, v2

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto/16 :goto_3

    :cond_9
    move v0, v2

    goto/16 :goto_4

    .line 1700543
    :cond_a
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1700544
    iget-object v0, p0, LX/AgH;->m:Landroid/util/SparseArray;

    invoke-static {v0}, LX/2y6;->a(Landroid/util/SparseArray;)[I

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_8
    if-ge v1, v5, :cond_e

    aget v6, v4, v1

    .line 1700545
    iget-object v0, p0, LX/AgH;->m:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1700546
    if-eqz v7, :cond_d

    .line 1700547
    iget-object v0, p0, LX/AgH;->z:Landroid/util/SparseArray;

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v8}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 1700548
    if-nez v0, :cond_b

    .line 1700549
    iget-object v0, p0, LX/AgH;->j:Landroid/content/res/Resources;

    const v8, 0x7f0b0479

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v8, v0, 0x2

    .line 1700550
    iget-object v0, p0, LX/AgH;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zf;

    invoke-virtual {v0, v6}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    invoke-virtual {v0}, LX/1zt;->h()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1700551
    invoke-virtual {v0, v2, v2, v8, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1700552
    new-instance v8, Landroid/text/style/ImageSpan;

    invoke-direct {v8, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 1700553
    new-instance v0, Landroid/text/SpannableString;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1700554
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v9

    const/16 p1, 0x21

    invoke-virtual {v0, v8, v2, v9, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1700555
    iget-object v8, p0, LX/AgH;->z:Landroid/util/SparseArray;

    invoke-virtual {v8, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1700556
    :cond_b
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    if-eqz v8, :cond_c

    .line 1700557
    const-string v8, "\n"

    invoke-virtual {v3, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1700558
    :cond_c
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1700559
    iget-object v0, p0, LX/AgH;->A:Landroid/util/SparseArray;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v6, v8}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1700560
    if-eqz v0, :cond_d

    .line 1700561
    const-string v6, " "

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    sub-int v0, v7, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1700562
    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_8

    .line 1700563
    :cond_e
    iget-object v0, p0, LX/AgH;->i:LX/3Hd;

    sget-object v1, LX/AgH;->b:Ljava/lang/String;

    iget-object v2, p0, LX/AgH;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, v2}, LX/3Hd;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_7
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 1700495
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/AgH;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1700496
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1700497
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/AgH;->t:Z

    .line 1700498
    iget-object v0, p0, LX/AgH;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1700499
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AgH;->y:Z

    .line 1700500
    iget-object v0, p0, LX/AgH;->c:LX/1My;

    invoke-virtual {v0}, LX/1My;->b()V

    .line 1700501
    iget-object v0, p0, LX/AgH;->p:LX/0gM;

    if-eqz v0, :cond_0

    .line 1700502
    iget-object v0, p0, LX/AgH;->d:LX/0gX;

    iget-object v1, p0, LX/AgH;->p:LX/0gM;

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1700503
    const/4 v0, 0x0

    iput-object v0, p0, LX/AgH;->p:LX/0gM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1700504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 1700491
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AgH;->n:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1700492
    iget-object v1, p0, LX/AgH;->n:Landroid/util/SparseArray;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700493
    monitor-exit p0

    return-void

    .line 1700494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/AgW;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700472
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/AgH;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1700473
    :goto_0
    monitor-exit p0

    return-void

    .line 1700474
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/AgH;->t:Z

    .line 1700475
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgW;

    iput-object v0, p0, LX/AgH;->q:LX/AgW;

    .line 1700476
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/AgH;->r:Ljava/lang/String;

    .line 1700477
    iput-object p3, p0, LX/AgH;->s:Ljava/lang/String;

    .line 1700478
    new-instance v0, LX/AgE;

    invoke-direct {v0, p0}, LX/AgE;-><init>(LX/AgH;)V

    .line 1700479
    new-instance v1, LX/6Rj;

    invoke-direct {v1}, LX/6Rj;-><init>()V

    move-object v1, v1

    .line 1700480
    const-string v2, "targetID"

    iget-object v3, p0, LX/AgH;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1700481
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    const/4 v2, 0x1

    .line 1700482
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 1700483
    move-object v1, v1

    .line 1700484
    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1700485
    iget-object v2, p0, LX/AgH;->c:LX/1My;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LiveReactions"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1700486
    iget-object v2, p0, LX/AgH;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1700487
    iget-object v0, p0, LX/AgH;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1700488
    iget-object v0, p0, LX/AgH;->s:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/AgH;->a(Ljava/lang/String;)V

    .line 1700489
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AgH;->u:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1700490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1700471
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1700468
    iget-boolean v0, p0, LX/AgH;->t:Z

    return v0
.end method

.method public final c()D
    .locals 4

    .prologue
    .line 1700470
    iget-wide v0, p0, LX/AgH;->x:D

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1700469
    iget v0, p0, LX/AgH;->l:I

    return v0
.end method
