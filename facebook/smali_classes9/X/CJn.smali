.class public LX/CJn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CJl;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CJh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/UriMatcher;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/CJh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/CJh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875807
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875808
    iput-object v0, p0, LX/CJn;->a:LX/0Ot;

    .line 1875809
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875810
    iput-object v0, p0, LX/CJn;->b:LX/0Ot;

    .line 1875811
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875812
    iput-object v0, p0, LX/CJn;->c:LX/0Ot;

    .line 1875813
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, LX/CJn;->e:Landroid/content/UriMatcher;

    .line 1875814
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CJn;->f:Ljava/util/Map;

    .line 1875815
    iput-object p1, p0, LX/CJn;->d:Ljava/util/Set;

    .line 1875816
    const/4 v0, 0x1

    .line 1875817
    iget-object v1, p0, LX/CJn;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CJh;

    .line 1875818
    iget-object v3, p0, LX/CJn;->e:Landroid/content/UriMatcher;

    invoke-virtual {v0}, LX/CJh;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/CJh;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1875819
    iget-object v3, p0, LX/CJn;->f:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1875820
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1875821
    goto :goto_0

    .line 1875822
    :cond_0
    return-void
.end method
