.class public LX/B40;
.super LX/1OM;
.source ""

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/B3z;",
        ">;",
        "Landroid/widget/Filterable;"
    }
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/B4A;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/widget/Filter;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Px;LX/B4A;Landroid/view/LayoutInflater;LX/0Or;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B4A;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;",
            "Lcom/facebook/heisman/category/ProfilePictureOverlayPivotAdapter$ListItemClickListener;",
            "Landroid/view/LayoutInflater;",
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1741121
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1741122
    new-instance v0, LX/B3y;

    invoke-direct {v0, p0}, LX/B3y;-><init>(LX/B40;)V

    iput-object v0, p0, LX/B40;->e:Landroid/widget/Filter;

    .line 1741123
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {p1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B40;->a:LX/0Px;

    .line 1741124
    iput-object p2, p0, LX/B40;->b:LX/B4A;

    .line 1741125
    iput-object p3, p0, LX/B40;->c:Landroid/view/LayoutInflater;

    .line 1741126
    iput-object p4, p0, LX/B40;->d:LX/0Or;

    .line 1741127
    iget-object v0, p0, LX/B40;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/B40;->f:Ljava/util/List;

    .line 1741128
    const/4 v0, 0x0

    iput-object v0, p0, LX/B40;->g:Ljava/lang/String;

    .line 1741129
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1741130
    iget-object v0, p0, LX/B40;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03107b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 1741131
    new-instance v1, LX/B3z;

    iget-object v2, p0, LX/B40;->b:LX/B4A;

    invoke-direct {v1, v0, v2}, LX/B3z;-><init>(Lcom/facebook/fig/listitem/FigListItem;LX/B4A;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1741132
    check-cast p1, LX/B3z;

    .line 1741133
    iget-object v0, p0, LX/B40;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1741134
    iput-object v0, p1, LX/B3z;->n:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1741135
    iget-object v1, p0, LX/B40;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;

    iget-object v2, p1, LX/B3z;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Lcom/facebook/fig/listitem/FigListItem;)V

    .line 1741136
    return-void
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1741137
    iget-object v0, p0, LX/B40;->e:Landroid/widget/Filter;

    return-object v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1741138
    iget-object v0, p0, LX/B40;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
