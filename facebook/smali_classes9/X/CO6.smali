.class public LX/CO6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field public final b:LX/CNq;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 0

    .prologue
    .line 1882593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882594
    iput-object p1, p0, LX/CO6;->a:LX/CNb;

    .line 1882595
    iput-object p2, p0, LX/CO6;->b:LX/CNq;

    .line 1882596
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 1882565
    iget-object v0, p0, LX/CO6;->a:LX/CNb;

    const-string v1, "urls"

    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1882566
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1882567
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v5, 0x0

    .line 1882568
    iget-object v4, p0, LX/CO6;->b:LX/CNq;

    iget-object v7, v4, LX/CNc;->c:Landroid/content/Context;

    .line 1882569
    const/4 v6, 0x0

    .line 1882570
    :try_start_0
    iget-object v4, p0, LX/CO6;->b:LX/CNq;

    .line 1882571
    iget-object v8, v4, LX/CNq;->d:LX/CNs;

    iget-object v8, v8, LX/CNs;->l:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/17Y;

    move-object v4, v8

    .line 1882572
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1882573
    :goto_1
    if-nez v4, :cond_0

    .line 1882574
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v4, v6, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1882575
    const/high16 v6, 0x10000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1882576
    :cond_0
    iget-object v6, p0, LX/CO6;->b:LX/CNq;

    invoke-virtual {v6}, LX/CNq;->e()Lcom/facebook/content/SecureContextHelper;

    move-result-object v6

    .line 1882577
    :try_start_1
    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_1
    const-string v8, "force_external_browser"

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1882578
    :cond_2
    invoke-interface {v6, v4, v7}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1882579
    :goto_2
    const/4 v4, 0x1

    .line 1882580
    :goto_3
    move v1, v4

    .line 1882581
    if-eqz v1, :cond_4

    .line 1882582
    :cond_3
    return-void

    .line 1882583
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1882584
    :catch_0
    move-exception v4

    .line 1882585
    iget-object v8, p0, LX/CO6;->b:LX/CNq;

    invoke-virtual {v8}, LX/CNq;->o()LX/03V;

    move-result-object v8

    const-string v9, "Native_Templates_url_action_intent"

    invoke-virtual {v8, v9, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v6

    goto :goto_1

    .line 1882586
    :cond_5
    :try_start_2
    invoke-static {v1}, LX/32x;->c(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1882587
    invoke-interface {v6, v4, v7}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 1882588
    :catch_1
    move-exception v4

    .line 1882589
    :goto_4
    iget-object v6, p0, LX/CO6;->b:LX/CNq;

    invoke-virtual {v6}, LX/CNq;->o()LX/03V;

    move-result-object v6

    const-string v7, "Native_Templates_url_action"

    invoke-virtual {v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v4, v5

    .line 1882590
    goto :goto_3

    .line 1882591
    :cond_6
    :try_start_3
    invoke-interface {v6, v4, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 1882592
    :catch_2
    move-exception v4

    goto :goto_4
.end method
