.class public LX/AZ9;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1bG;


# instance fields
.field public a:LX/3RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3RX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AZ8;

.field public f:F

.field public final g:Lcom/facebook/widget/text/BetterTextView;

.field public final h:Lcom/facebook/widget/text/BetterTextView;

.field public final i:Landroid/widget/ProgressBar;

.field public final j:Landroid/widget/LinearLayout;

.field public final k:Lcom/facebook/widget/text/BetterTextView;

.field public l:LX/AY9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/AZ6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/AZ7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686600
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AZ9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686601
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686598
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AZ9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686599
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686586
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686587
    sget-object v0, LX/AZ8;->NONE:LX/AZ8;

    iput-object v0, p0, LX/AZ9;->c:LX/AZ8;

    .line 1686588
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/AZ9;->f:F

    .line 1686589
    const-class v0, LX/AZ9;

    invoke-static {v0, p0}, LX/AZ9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686590
    const v0, 0x7f0305e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686591
    const v0, 0x7f0d1040

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1686592
    const v0, 0x7f0d103f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 1686593
    const v0, 0x7f0d0fc7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/AZ9;->i:Landroid/widget/ProgressBar;

    .line 1686594
    const v0, 0x7f0d1041

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AZ9;->j:Landroid/widget/LinearLayout;

    .line 1686595
    const v0, 0x7f0d1042

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AZ9;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1686596
    new-instance v0, LX/AZ7;

    const-wide/16 v2, 0x9c4

    invoke-direct {v0, p0, v2, v3}, LX/AZ7;-><init>(LX/AZ9;J)V

    iput-object v0, p0, LX/AZ9;->n:LX/AZ7;

    .line 1686597
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 4

    .prologue
    const v2, 0x3f4ccccd    # 0.8f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1686602
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1686603
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 1686604
    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 1686605
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686606
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AZ9;

    invoke-static {p0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object v1

    check-cast v1, LX/3RZ;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object p0

    check-cast p0, LX/3RX;

    iput-object v1, p1, LX/AZ9;->a:LX/3RZ;

    iput-object p0, p1, LX/AZ9;->b:LX/3RX;

    return-void
.end method

.method public static c$redex0(LX/AZ9;)V
    .locals 3

    .prologue
    .line 1686555
    sget-object v0, LX/AZ5;->b:[I

    iget-object v1, p0, LX/AZ9;->c:LX/AZ8;

    invoke-virtual {v1}, LX/AZ8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1686556
    :goto_0
    return-void

    .line 1686557
    :pswitch_0
    const/4 v2, 0x0

    .line 1686558
    sget-object v0, LX/AZ8;->BEGIN_TRANSITION:LX/AZ8;

    iput-object v0, p0, LX/AZ9;->c:LX/AZ8;

    .line 1686559
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080cb4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1686560
    iget-object v0, p0, LX/AZ9;->k:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080cb5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1686561
    iget-object v0, p0, LX/AZ9;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1686562
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setAlpha(F)V

    .line 1686563
    iget-object v0, p0, LX/AZ9;->n:LX/AZ7;

    if-eqz v0, :cond_0

    .line 1686564
    iget-object v0, p0, LX/AZ9;->n:LX/AZ7;

    invoke-virtual {v0}, LX/AZ7;->start()Landroid/os/CountDownTimer;

    .line 1686565
    :cond_0
    iget-object v0, p0, LX/AZ9;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1686566
    goto :goto_0

    .line 1686567
    :pswitch_1
    const/4 v2, 0x0

    .line 1686568
    iget-object v0, p0, LX/AZ9;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1686569
    new-instance v0, LX/AZ6;

    invoke-direct {v0, p0}, LX/AZ6;-><init>(LX/AZ9;)V

    iput-object v0, p0, LX/AZ9;->m:LX/AZ6;

    .line 1686570
    iget-object v0, p0, LX/AZ9;->m:LX/AZ6;

    invoke-virtual {v0}, LX/AZ6;->start()Landroid/os/CountDownTimer;

    .line 1686571
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080cb2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1686572
    iget-object v0, p0, LX/AZ9;->k:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080cb3

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1686573
    iget-object v0, p0, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686574
    iget-object v0, p0, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/AZ9;->a(Landroid/view/View;)V

    .line 1686575
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/AZ9;->a(Landroid/view/View;)V

    .line 1686576
    goto :goto_0

    .line 1686577
    :pswitch_2
    const/4 v2, 0x0

    .line 1686578
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686579
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setAlpha(F)V

    .line 1686580
    iget-object v0, p0, LX/AZ9;->h:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f080cb6

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1686581
    iget-object v0, p0, LX/AZ9;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1686582
    iget-object v0, p0, LX/AZ9;->n:LX/AZ7;

    if-eqz v0, :cond_1

    .line 1686583
    iget-object v0, p0, LX/AZ9;->n:LX/AZ7;

    invoke-virtual {v0}, LX/AZ7;->start()Landroid/os/CountDownTimer;

    .line 1686584
    :cond_1
    iget-object v0, p0, LX/AZ9;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1686585
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1686542
    sget-object v0, LX/AZ5;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1686543
    :goto_0
    return-void

    .line 1686544
    :pswitch_0
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    if-eq p2, v0, :cond_0

    .line 1686545
    invoke-static {p0}, LX/AZ9;->c$redex0(LX/AZ9;)V

    goto :goto_0

    .line 1686546
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1686547
    iput v0, p0, LX/AZ9;->f:F

    .line 1686548
    goto :goto_0

    .line 1686549
    :pswitch_1
    iget-object v0, p0, LX/AZ9;->m:LX/AZ6;

    if-eqz v0, :cond_1

    .line 1686550
    iget-object v0, p0, LX/AZ9;->m:LX/AZ6;

    invoke-virtual {v0}, LX/AZ6;->cancel()V

    .line 1686551
    :cond_1
    goto :goto_0

    .line 1686552
    :pswitch_2
    const/4 v0, 0x0

    .line 1686553
    iput v0, p0, LX/AZ9;->f:F

    .line 1686554
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
