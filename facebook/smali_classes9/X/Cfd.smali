.class public final enum LX/Cfd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cfd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cfd;

.field public static final enum HEADER_CHECK_IN:LX/Cfd;

.field public static final enum HEADER_CONTEXT_ITEM_TAP:LX/Cfd;

.field public static final enum HEADER_LIKE_TAP:LX/Cfd;

.field public static final enum HEADER_SEE_LESS_TAP:LX/Cfd;

.field public static final enum HEADER_SEE_MORE_TAP:LX/Cfd;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1926664
    new-instance v0, LX/Cfd;

    const-string v1, "HEADER_CHECK_IN"

    const-string v2, "header_check_in"

    invoke-direct {v0, v1, v3, v2}, LX/Cfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfd;->HEADER_CHECK_IN:LX/Cfd;

    .line 1926665
    new-instance v0, LX/Cfd;

    const-string v1, "HEADER_CONTEXT_ITEM_TAP"

    const-string v2, "header_context_item_tap"

    invoke-direct {v0, v1, v4, v2}, LX/Cfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfd;->HEADER_CONTEXT_ITEM_TAP:LX/Cfd;

    .line 1926666
    new-instance v0, LX/Cfd;

    const-string v1, "HEADER_LIKE_TAP"

    const-string v2, "header_like_tap"

    invoke-direct {v0, v1, v5, v2}, LX/Cfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfd;->HEADER_LIKE_TAP:LX/Cfd;

    .line 1926667
    new-instance v0, LX/Cfd;

    const-string v1, "HEADER_SEE_LESS_TAP"

    const-string v2, "header_see_less_tap"

    invoke-direct {v0, v1, v6, v2}, LX/Cfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfd;->HEADER_SEE_LESS_TAP:LX/Cfd;

    .line 1926668
    new-instance v0, LX/Cfd;

    const-string v1, "HEADER_SEE_MORE_TAP"

    const-string v2, "header_see_more_tap"

    invoke-direct {v0, v1, v7, v2}, LX/Cfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfd;->HEADER_SEE_MORE_TAP:LX/Cfd;

    .line 1926669
    const/4 v0, 0x5

    new-array v0, v0, [LX/Cfd;

    sget-object v1, LX/Cfd;->HEADER_CHECK_IN:LX/Cfd;

    aput-object v1, v0, v3

    sget-object v1, LX/Cfd;->HEADER_CONTEXT_ITEM_TAP:LX/Cfd;

    aput-object v1, v0, v4

    sget-object v1, LX/Cfd;->HEADER_LIKE_TAP:LX/Cfd;

    aput-object v1, v0, v5

    sget-object v1, LX/Cfd;->HEADER_SEE_LESS_TAP:LX/Cfd;

    aput-object v1, v0, v6

    sget-object v1, LX/Cfd;->HEADER_SEE_MORE_TAP:LX/Cfd;

    aput-object v1, v0, v7

    sput-object v0, LX/Cfd;->$VALUES:[LX/Cfd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1926659
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1926660
    iput-object p3, p0, LX/Cfd;->name:Ljava/lang/String;

    .line 1926661
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cfd;
    .locals 1

    .prologue
    .line 1926663
    const-class v0, LX/Cfd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cfd;

    return-object v0
.end method

.method public static values()[LX/Cfd;
    .locals 1

    .prologue
    .line 1926662
    sget-object v0, LX/Cfd;->$VALUES:[LX/Cfd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cfd;

    return-object v0
.end method
