.class public LX/Auw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$ProvidesInspirationTextParams;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$SetsInspirationTextParams",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/widget/FrameLayout;

.field public final d:Landroid/content/Context;

.field private final e:LX/87P;

.field public f:LX/Av1;

.field public g:LX/Aur;

.field private h:LX/86n;

.field public i:Z

.field public j:LX/870;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/Ar5;

.field public l:LX/0tS;

.field private final m:LX/Auj;

.field private final n:LX/Aut;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1723560
    const-class v0, LX/Auw;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Auw;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;Landroid/widget/FrameLayout;LX/Ar5;Landroid/content/Context;LX/87P;LX/Aur;LX/86n;LX/0tS;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Ar5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationTextEditController$Delegate;",
            "Landroid/content/Context;",
            "LX/87P;",
            "LX/Aur;",
            "LX/86n;",
            "LX/0tS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1723526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723527
    new-instance v0, LX/Aus;

    invoke-direct {v0, p0}, LX/Aus;-><init>(LX/Auw;)V

    iput-object v0, p0, LX/Auw;->m:LX/Auj;

    .line 1723528
    new-instance v0, LX/Aut;

    invoke-direct {v0, p0}, LX/Aut;-><init>(LX/Auw;)V

    iput-object v0, p0, LX/Auw;->n:LX/Aut;

    .line 1723529
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    .line 1723530
    iput-object p2, p0, LX/Auw;->c:Landroid/widget/FrameLayout;

    .line 1723531
    iput-object p4, p0, LX/Auw;->d:Landroid/content/Context;

    .line 1723532
    iput-object p5, p0, LX/Auw;->e:LX/87P;

    .line 1723533
    new-instance v0, LX/Av1;

    iget-object v1, p0, LX/Auw;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Av1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Auw;->f:LX/Av1;

    .line 1723534
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v0}, LX/Av1;->a()V

    .line 1723535
    iput-object p6, p0, LX/Auw;->g:LX/Aur;

    .line 1723536
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    iget-object v1, p0, LX/Auw;->c:Landroid/widget/FrameLayout;

    .line 1723537
    iget-object v2, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1723538
    iget-object v2, v0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1723539
    iput-object p7, p0, LX/Auw;->h:LX/86n;

    .line 1723540
    iput-object p3, p0, LX/Auw;->k:LX/Ar5;

    .line 1723541
    iput-object p8, p0, LX/Auw;->l:LX/0tS;

    .line 1723542
    iget-object v0, p0, LX/Auw;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/Auw;->f:LX/Av1;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1723543
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    iget-object v1, p0, LX/Auw;->m:LX/Auj;

    .line 1723544
    iget-object v2, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    .line 1723545
    iput-object v1, v2, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->e:LX/Auj;

    .line 1723546
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    iget-object v1, p0, LX/Auw;->n:LX/Aut;

    .line 1723547
    iput-object v1, v0, LX/Aur;->v:LX/Aut;

    .line 1723548
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    iget-object v1, p0, LX/Auw;->g:LX/Aur;

    .line 1723549
    iget-object v2, v1, LX/Aur;->g:LX/Auq;

    move-object v1, v2

    .line 1723550
    iput-object v1, v0, LX/Av1;->c:LX/Auq;

    .line 1723551
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    .line 1723552
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->isKeyboardOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1723553
    iget-object v1, p0, LX/Auw;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/Auu;

    invoke-direct {v2, p0}, LX/Auu;-><init>(LX/Auw;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1723554
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1723555
    invoke-static {p0, v0}, LX/Auw;->b(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 1723556
    iget-object v1, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v1, v0}, LX/Aur;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 1723557
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->isKeyboardOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1723558
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v0}, LX/Aur;->b()V

    .line 1723559
    :cond_1
    return-void
.end method

.method private static a(LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1723524
    check-cast p0, LX/0jL;

    invoke-virtual {p0, p1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Ljava/lang/Object;

    .line 1723525
    return-void
.end method

.method public static a(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V
    .locals 2

    .prologue
    .line 1723521
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723522
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/Auw;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1723523
    return-void
.end method

.method public static b(LX/Auw;LX/870;)V
    .locals 3

    .prologue
    .line 1723518
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1723519
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/Auw;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setFormatMode(LX/870;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1723520
    return-void
.end method

.method public static b(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V
    .locals 4

    .prologue
    .line 1723512
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1723513
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    iget-object v1, p0, LX/Auw;->h:LX/86n;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Auw;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1, v2, p1, v3}, LX/86n;->a(ZLcom/facebook/friendsharing/inspiration/model/InspirationTextParams;Landroid/content/res/Resources;)LX/1FJ;

    move-result-object v1

    .line 1723514
    iget-object v2, v0, LX/Aur;->u:LX/1FJ;

    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 1723515
    iput-object v1, v0, LX/Aur;->u:LX/1FJ;

    .line 1723516
    iget-object v3, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1723517
    :cond_0
    return-void
.end method

.method public static b(LX/Auw;Z)V
    .locals 5

    .prologue
    .line 1723479
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v0}, LX/Av1;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1723480
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v0}, LX/Aur;->e()V

    .line 1723481
    :cond_0
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    const/16 v2, 0x8

    .line 1723482
    iget-object v1, v0, LX/Av1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1723483
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setVisibility(I)V

    .line 1723484
    sget-object v0, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    invoke-static {p0, v0}, LX/Auw;->b(LX/Auw;LX/870;)V

    .line 1723485
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Auw;->i:Z

    .line 1723486
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v0}, LX/Av1;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1723487
    if-eqz p1, :cond_1

    .line 1723488
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v0}, LX/Aur;->e()V

    .line 1723489
    :cond_1
    invoke-static {p0}, LX/Auw;->g(LX/Auw;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1723490
    iget-object v1, p0, LX/Auw;->f:LX/Av1;

    invoke-static {p0}, LX/Auw;->h(LX/Auw;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v2

    .line 1723491
    iget-object v3, v1, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v3, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v3

    move-object v1, v3

    .line 1723492
    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setMediaRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v1

    .line 1723493
    invoke-static {p0, v1}, LX/Auw;->a(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 1723494
    invoke-static {p0, v1}, LX/Auw;->b(LX/Auw;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 1723495
    iget-object v1, p0, LX/Auw;->g:LX/Aur;

    .line 1723496
    iput-object v0, v1, LX/Aur;->j:Landroid/graphics/Rect;

    .line 1723497
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    iget-object v1, p0, LX/Auw;->f:LX/Av1;

    .line 1723498
    iget-object v2, v1, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getLeft()I

    move-result v2

    move v1, v2

    .line 1723499
    iget-object v2, p0, LX/Auw;->f:LX/Av1;

    .line 1723500
    iget-object v3, v2, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getTranslationY()F

    move-result v3

    float-to-int v3, v3

    move v2, v3

    .line 1723501
    iget-object v3, p0, LX/Auw;->f:LX/Av1;

    .line 1723502
    iget-object v4, v3, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    .line 1723503
    iget v3, v4, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    move v4, v3

    .line 1723504
    move v3, v4

    .line 1723505
    iget-object v4, p0, LX/Auw;->f:LX/Av1;

    .line 1723506
    iget-object p1, v4, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    .line 1723507
    iget v4, p1, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    move p1, v4

    .line 1723508
    move v4, p1

    .line 1723509
    invoke-virtual {v0, v1, v2, v3, v4}, LX/Aur;->a(IIII)V

    .line 1723510
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v0}, LX/Aur;->b()V

    .line 1723511
    :cond_2
    return-void
.end method

.method public static f(LX/Auw;)V
    .locals 2

    .prologue
    .line 1723474
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723475
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/Auw;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1723476
    invoke-virtual {p0, v0}, LX/Auw;->a(LX/0jL;)V

    .line 1723477
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1723478
    return-void
.end method

.method public static g(LX/Auw;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1723472
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723473
    iget-object v1, p0, LX/Auw;->e:LX/87P;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1, v0}, LX/87P;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/Auw;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .locals 1

    .prologue
    .line 1723414
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723415
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1723460
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    const/4 p0, 0x0

    .line 1723461
    invoke-virtual {v0}, LX/Av1;->bringToFront()V

    .line 1723462
    iget-object v1, v0, LX/Av1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1723463
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1723464
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v1, p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setVisibility(I)V

    .line 1723465
    iget-object v1, v0, LX/Av1;->b:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1723466
    :goto_0
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a()V

    .line 1723467
    invoke-virtual {v0, p0}, LX/Av1;->setVisibility(I)V

    .line 1723468
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1723469
    return-void

    .line 1723470
    :cond_0
    iget-object v1, v0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setVisibility(I)V

    .line 1723471
    iget-object v1, v0, LX/Av1;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final a(LX/0jL;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;)V"
        }
    .end annotation

    .prologue
    .line 1723436
    iget-object v0, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v0}, LX/Av1;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1723437
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    invoke-static {p1, v0}, LX/Auw;->a(LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    .line 1723438
    :goto_0
    return-void

    .line 1723439
    :cond_0
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723440
    invoke-static {p0}, LX/Auw;->g(LX/Auw;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1723441
    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    .line 1723442
    iput-object v1, v2, LX/Aur;->j:Landroid/graphics/Rect;

    .line 1723443
    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    .line 1723444
    iget-object v3, v2, LX/Aur;->j:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 1723445
    iget-object v4, v2, LX/Aur;->j:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 1723446
    iget-object v5, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1723447
    iget-object v6, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723448
    const/4 v7, 0x4

    new-array v7, v7, [F

    .line 1723449
    const/4 v8, 0x0

    iget-object v9, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v9

    div-int/lit8 v10, v6, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    iget-object v10, v2, LX/Aur;->j:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    int-to-float v10, v3

    div-float/2addr v9, v10

    aput v9, v7, v8

    .line 1723450
    const/4 v8, 0x1

    iget-object v9, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v9

    div-int/lit8 v10, v5, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    iget-object v10, v2, LX/Aur;->j:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    int-to-float v10, v4

    div-float/2addr v9, v10

    aput v9, v7, v8

    .line 1723451
    const/4 v8, 0x2

    add-int v9, v5, v6

    int-to-float v9, v9

    int-to-float v3, v3

    div-float v3, v9, v3

    aput v3, v7, v8

    .line 1723452
    const/4 v3, 0x3

    add-int/2addr v5, v6

    int-to-float v5, v5

    int-to-float v4, v4

    div-float v4, v5, v4

    aput v4, v7, v3

    .line 1723453
    move-object v2, v7

    .line 1723454
    invoke-static {p0}, LX/Auw;->h(LX/Auw;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/4 v3, 0x0

    aget v3, v2, v3

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setLeftPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/4 v3, 0x1

    aget v3, v2, v3

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTopPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/4 v3, 0x2

    aget v3, v2, v3

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setWidthPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/4 v3, 0x3

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setHeightPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    .line 1723455
    iget-object v3, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRotation()F

    move-result v3

    move v2, v3

    .line 1723456
    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setRotation(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    .line 1723457
    iget-wide v4, v2, LX/Aur;->k:D

    move-wide v2, v4

    .line 1723458
    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setScaleFactor(D)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v2

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setMediaRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    .line 1723459
    invoke-static {p1, v0}, LX/Auw;->a(LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    goto/16 :goto_0
.end method

.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 1723432
    sget-object v0, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    if-ne p1, v0, :cond_0

    .line 1723433
    iget-object v0, p0, LX/Auw;->g:LX/Aur;

    .line 1723434
    iget-object p0, v0, LX/Aur;->u:LX/1FJ;

    invoke-static {p0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1723435
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1723419
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1723420
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    .line 1723421
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v1

    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 1723422
    iget-object v2, p0, LX/Auw;->j:LX/870;

    if-ne v1, v2, :cond_0

    .line 1723423
    :goto_0
    iget-object v1, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getTextColorSelection()I

    move-result v0

    .line 1723424
    iget-object v2, v1, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v2, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setTextColor(I)V

    .line 1723425
    return-void

    .line 1723426
    :cond_0
    iput-object v1, p0, LX/Auw;->j:LX/870;

    .line 1723427
    sget-object v2, LX/Auv;->a:[I

    invoke-virtual {v1}, LX/870;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1723428
    :pswitch_0
    iget-object v2, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v2, p2}, LX/Av1;->setTextInputEnabled(Z)V

    .line 1723429
    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v2, p2}, LX/Aur;->a(Z)V

    goto :goto_0

    .line 1723430
    :pswitch_1
    iget-object v2, p0, LX/Auw;->f:LX/Av1;

    invoke-virtual {v2, p1}, LX/Av1;->setTextInputEnabled(Z)V

    .line 1723431
    iget-object v2, p0, LX/Auw;->g:LX/Aur;

    invoke-virtual {v2, p1}, LX/Aur;->a(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1723416
    iget-object v0, p0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1723417
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/Auw;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setIsKeyboardOpen(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1723418
    return-void
.end method
