.class public LX/BNM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BNM;


# instance fields
.field private final a:LX/0wM;

.field public final b:Landroid/content/res/Resources;

.field private c:LX/0W9;


# direct methods
.method public constructor <init>(LX/0wM;Landroid/content/res/Resources;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778580
    iput-object p1, p0, LX/BNM;->a:LX/0wM;

    .line 1778581
    iput-object p2, p0, LX/BNM;->b:Landroid/content/res/Resources;

    .line 1778582
    iput-object p3, p0, LX/BNM;->c:LX/0W9;

    .line 1778583
    return-void
.end method

.method public static a(LX/0QB;)LX/BNM;
    .locals 6

    .prologue
    .line 1778584
    sget-object v0, LX/BNM;->d:LX/BNM;

    if-nez v0, :cond_1

    .line 1778585
    const-class v1, LX/BNM;

    monitor-enter v1

    .line 1778586
    :try_start_0
    sget-object v0, LX/BNM;->d:LX/BNM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778587
    if-eqz v2, :cond_0

    .line 1778588
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778589
    new-instance p0, LX/BNM;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-direct {p0, v3, v4, v5}, LX/BNM;-><init>(LX/0wM;Landroid/content/res/Resources;LX/0W9;)V

    .line 1778590
    move-object v0, p0

    .line 1778591
    sput-object v0, LX/BNM;->d:LX/BNM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778592
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778593
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778594
    :cond_1
    sget-object v0, LX/BNM;->d:LX/BNM;

    return-object v0

    .line 1778595
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BNM;Ljava/lang/String;III)Landroid/text/SpannableString;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 1778597
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1778598
    iget-object v0, p0, LX/BNM;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1778599
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v2, v0, v3

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v10

    const/4 v2, 0x2

    new-instance v4, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v4, p3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    aput-object v4, v0, v2

    invoke-static {v3, p2, v6, v0}, LX/47t;->a(IILandroid/text/SpannableString;[Ljava/lang/Object;)V

    .line 1778600
    int-to-double v4, p3

    const-wide/high16 v8, 0x3fe4000000000000L    # 0.625

    mul-double/2addr v4, v8

    double-to-int v7, v4

    .line 1778601
    int-to-double v4, p3

    const-wide v8, 0x3fbccccccccccccdL    # 0.1125

    mul-double/2addr v4, v8

    double-to-int v2, v4

    .line 1778602
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    iget-object v4, p0, LX/BNM;->a:LX/0wM;

    const v5, 0x7f021893

    invoke-virtual {v4, v5, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move v4, v3

    move v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1778603
    add-int v1, v2, v7

    add-int/2addr v2, v7

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/InsetDrawable;->setBounds(IIII)V

    .line 1778604
    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-direct {v4, v0, v10}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    aput-object v4, v2, v3

    invoke-static {p2, v1, v6, v2}, LX/47t;->a(IILandroid/text/SpannableString;[Ljava/lang/Object;)V

    .line 1778605
    return-object v6
.end method


# virtual methods
.method public final a(II)Landroid/text/SpannableString;
    .locals 1

    .prologue
    .line 1778606
    const v0, 0x7f0a00d2

    invoke-virtual {p0, p1, p2, v0}, LX/BNM;->a(III)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public final a(III)Landroid/text/SpannableString;
    .locals 5

    .prologue
    .line 1778607
    iget-object v0, p0, LX/BNM;->b:Landroid/content/res/Resources;

    const v1, 0x7f081505

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1778608
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    .line 1778609
    invoke-static {p0, v0, v1, p2, p3}, LX/BNM;->a(LX/BNM;Ljava/lang/String;III)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public final a(D)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1778610
    iget-object v0, p0, LX/BNM;->c:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/DecimalFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 1778611
    const-string v1, "#.#"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 1778612
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
