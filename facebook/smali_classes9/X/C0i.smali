.class public LX/C0i;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0j;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0i",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0j;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841311
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1841312
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C0i;->b:LX/0Zi;

    .line 1841313
    iput-object p1, p0, LX/C0i;->a:LX/0Ot;

    .line 1841314
    return-void
.end method

.method public static a(LX/0QB;)LX/C0i;
    .locals 4

    .prologue
    .line 1841317
    const-class v1, LX/C0i;

    monitor-enter v1

    .line 1841318
    :try_start_0
    sget-object v0, LX/C0i;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841319
    sput-object v2, LX/C0i;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841322
    new-instance v3, LX/C0i;

    const/16 p0, 0x1e7f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0i;-><init>(LX/0Ot;)V

    .line 1841323
    move-object v0, v3

    .line 1841324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1841328
    check-cast p2, LX/C0h;

    .line 1841329
    iget-object v0, p0, LX/C0i;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0j;

    iget-object v1, p2, LX/C0h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C0h;->b:LX/1Pb;

    .line 1841330
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x6

    const p2, 0x7f0b00bf

    invoke-interface {v3, p0, p2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    iget-object p0, v0, LX/C0j;->a:LX/1xN;

    invoke-virtual {p0, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/22O;->a(Z)LX/22O;

    move-result-object p0

    invoke-interface {v3, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object p0, v0, LX/C0j;->b:LX/C0m;

    const/4 p2, 0x0

    .line 1841331
    new-instance v0, LX/C0l;

    invoke-direct {v0, p0}, LX/C0l;-><init>(LX/C0m;)V

    .line 1841332
    sget-object v2, LX/C0m;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C0k;

    .line 1841333
    if-nez v2, :cond_0

    .line 1841334
    new-instance v2, LX/C0k;

    invoke-direct {v2}, LX/C0k;-><init>()V

    .line 1841335
    :cond_0
    invoke-static {v2, p1, p2, p2, v0}, LX/C0k;->a$redex0(LX/C0k;LX/1De;IILX/C0l;)V

    .line 1841336
    move-object v0, v2

    .line 1841337
    move-object p2, v0

    .line 1841338
    move-object p0, p2

    .line 1841339
    iget-object p2, p0, LX/C0k;->a:LX/C0l;

    iput-object v1, p2, LX/C0l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841340
    iget-object p2, p0, LX/C0k;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 1841341
    move-object p0, p0

    .line 1841342
    invoke-interface {v3, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1841343
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1841315
    invoke-static {}, LX/1dS;->b()V

    .line 1841316
    const/4 v0, 0x0

    return-object v0
.end method
