.class public final LX/B3G;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 0

    .prologue
    .line 1739850
    iput-object p1, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1739851
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739852
    if-nez v0, :cond_0

    .line 1739853
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null camera title result"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/B3G;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1739854
    :goto_0
    return-void

    .line 1739855
    :cond_0
    iget-object v0, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739856
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    .line 1739857
    iget-object v0, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->i:LX/B3R;

    .line 1739858
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739859
    check-cast v0, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    iget-object v2, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v2, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e:Landroid/support/v7/widget/Toolbar;

    new-instance v3, LX/B3F;

    invoke-direct {v3, p0}, LX/B3F;-><init>(LX/B3G;)V

    const/4 v6, 0x1

    const/4 p0, 0x0

    .line 1739860
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1739861
    :cond_1
    :goto_1
    goto :goto_0

    .line 1739862
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    .line 1739863
    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1739864
    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;->b()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_4

    .line 1739865
    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;->b()LX/1vs;

    move-result-object v5

    iget-object p1, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1739866
    invoke-virtual {p1, v5, p0}, LX/15i;->g(II)I

    move-result v5

    if-eqz v5, :cond_3

    move v5, v6

    :goto_2
    if-eqz v5, :cond_6

    .line 1739867
    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v5, v4, p0}, LX/15i;->g(II)I

    move-result v4

    .line 1739868
    invoke-virtual {v5, v4, p0}, LX/15i;->j(II)I

    move-result v4

    if-le v4, v6, :cond_5

    :goto_3
    if-eqz v6, :cond_1

    iget-object v4, v1, LX/B3R;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short v5, LX/B3X;->a:S

    invoke-interface {v4, v5, p0}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1739869
    iget-object v4, v1, LX/B3R;->b:LX/23P;

    iget-object v5, v1, LX/B3R;->c:Landroid/content/res/Resources;

    const v6, 0x7f082742

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1739870
    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1739871
    const v4, 0x7f0d2765

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1739872
    invoke-virtual {v4, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1739873
    invoke-virtual {v4, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    move v5, p0

    .line 1739874
    goto :goto_2

    :cond_4
    move v5, p0

    goto :goto_2

    :cond_5
    move v6, p0

    goto :goto_3

    :cond_6
    move v6, p0

    goto :goto_3
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739875
    iget-object v0, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739876
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    .line 1739877
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1739878
    iget-object v0, p0, LX/B3G;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "heisman_fetch_title_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739879
    :cond_0
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739880
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/B3G;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
