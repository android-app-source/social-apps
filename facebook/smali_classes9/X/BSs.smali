.class public LX/BSs;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BSr;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1786198
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1786199
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;Landroid/net/Uri;Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;LX/ATX;LX/9fh;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)LX/BSr;
    .locals 25

    .prologue
    .line 1786200
    new-instance v1, LX/BSr;

    invoke-static/range {p0 .. p0}, LX/2MU;->a(LX/0QB;)LX/2MU;

    move-result-object v2

    check-cast v2, LX/2MV;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {p0 .. p0}, LX/BSu;->a(LX/0QB;)LX/BSu;

    move-result-object v4

    check-cast v4, LX/BSu;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    const-class v6, LX/BTA;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BTA;

    const-class v7, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    const/16 v8, 0x2d9

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/BTG;->a(LX/0QB;)LX/BTG;

    move-result-object v9

    check-cast v9, LX/BTG;

    const-class v10, LX/BSk;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/BSk;

    const-class v11, LX/BTj;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/BTj;

    invoke-static/range {p0 .. p0}, LX/BTH;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v20

    check-cast v20, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/23P;->a(LX/0QB;)LX/23P;

    move-result-object v21

    check-cast v21, LX/23P;

    invoke-static/range {p0 .. p0}, LX/BTg;->a(LX/0QB;)LX/BTg;

    move-result-object v22

    check-cast v22, LX/BTg;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/BTh;->a(LX/0QB;)LX/BTh;

    move-result-object v24

    check-cast v24, LX/BTh;

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    move-object/from16 v17, p4

    move-object/from16 v18, p5

    move-object/from16 v19, p6

    invoke-direct/range {v1 .. v24}, LX/BSr;-><init>(LX/2MV;LX/03V;LX/BSu;LX/0Sh;LX/BTA;Landroid/content/Context;LX/0Ot;LX/BTG;LX/BSk;LX/BTj;Ljava/util/Set;Ljava/util/concurrent/ExecutorService;Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;Landroid/net/Uri;Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;LX/ATX;LX/9fh;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;LX/0hB;LX/23P;LX/BTg;LX/0ad;LX/BTh;)V

    .line 1786201
    return-object v1
.end method
