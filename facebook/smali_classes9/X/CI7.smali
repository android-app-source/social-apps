.class public final LX/CI7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1872219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1872220
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1872221
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1872222
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1872223
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1872224
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1872225
    :goto_1
    move v1, v2

    .line 1872226
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1872227
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1872228
    :cond_1
    const-string v11, "selected"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1872229
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 1872230
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1872231
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1872232
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1872233
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_2

    if-eqz v10, :cond_2

    .line 1872234
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1872235
    :cond_3
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_2

    .line 1872236
    :cond_4
    const-string v11, "action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1872237
    invoke-static {p0, p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_2

    .line 1872238
    :cond_5
    const-string v11, "header_element_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1872239
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_2

    .line 1872240
    :cond_6
    const-string v11, "logging_token"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1872241
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_2

    .line 1872242
    :cond_7
    const-string v11, "tooltip_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1872243
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 1872244
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1872245
    :cond_9
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1872246
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 1872247
    invoke-virtual {p1, v3, v8}, LX/186;->b(II)V

    .line 1872248
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1872249
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1872250
    if-eqz v1, :cond_a

    .line 1872251
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1872252
    :cond_a
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1872253
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_b
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_2
.end method
