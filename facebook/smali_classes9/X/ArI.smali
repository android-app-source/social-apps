.class public final enum LX/ArI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArI;

.field public static final enum ACTION:LX/ArI;

.field public static final enum APPLIED_PROMPT_IDS:LX/ArI;

.field public static final enum APPLIED_TRACKING_STRINGS:LX/ArI;

.field public static final enum CAMERA_ORIENTATION:LX/ArI;

.field public static final enum CAMERA_STATE:LX/ArI;

.field public static final enum DOODLE_COLOR_COUNT:LX/ArI;

.field public static final enum DOODLE_MAX_BRUSH_SIZE:LX/ArI;

.field public static final enum DOODLE_SIZE_COUNT:LX/ArI;

.field public static final enum DOODLE_STROKE_COUNT:LX/ArI;

.field public static final enum DOODLE_UNDO_COUNT:LX/ArI;

.field public static final enum DURATION:LX/ArI;

.field public static final enum EXTRA_ANNOTATIONS_DATA:LX/ArI;

.field public static final enum FEED_CTA_STORY_ID:LX/ArI;

.field public static final enum FEED_STORY_ELIGIBLE_EFFECT_IDS:LX/ArI;

.field public static final enum FLASH_MODE:LX/ArI;

.field public static final enum HAS_DOODLE:LX/ArI;

.field public static final enum HAS_TEXT:LX/ArI;

.field public static final enum INDEX:LX/ArI;

.field public static final enum IS_CAMERA_SYSTEM:LX/ArI;

.field public static final enum MEDIA_CONTENT_ID:LX/ArI;

.field public static final enum MEDIA_DATE:LX/ArI;

.field public static final enum MEDIA_HEIGHT:LX/ArI;

.field public static final enum MEDIA_INDEX:LX/ArI;

.field public static final enum MEDIA_SOURCE:LX/ArI;

.field public static final enum MEDIA_TYPE:LX/ArI;

.field public static final enum MEDIA_WIDTH:LX/ArI;

.field public static final enum PERMISSION:LX/ArI;

.field public static final enum PHOTO_STATE:LX/ArI;

.field public static final enum PROMPT_ID:LX/ArI;

.field public static final enum REASON:LX/ArI;

.field public static final enum RELATIVE_INDEX:LX/ArI;

.field public static final enum SESSION_IDS_MAP:LX/ArI;

.field public static final enum SURFACE:LX/ArI;

.field public static final enum TEXT_COUNT:LX/ArI;

.field public static final enum THUMBNAIL_INDEX:LX/ArI;

.field public static final enum THUMBNAIL_PROMPT_ID:LX/ArI;

.field public static final enum TRACKING_STRING:LX/ArI;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1718814
    new-instance v0, LX/ArI;

    const-string v1, "ACTION"

    const-string v2, "action"

    invoke-direct {v0, v1, v4, v2}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->ACTION:LX/ArI;

    .line 1718815
    new-instance v0, LX/ArI;

    const-string v1, "CAMERA_ORIENTATION"

    const-string v2, "camera_orientation"

    invoke-direct {v0, v1, v5, v2}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->CAMERA_ORIENTATION:LX/ArI;

    .line 1718816
    new-instance v0, LX/ArI;

    const-string v1, "FLASH_MODE"

    const-string v2, "flash_mode"

    invoke-direct {v0, v1, v6, v2}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->FLASH_MODE:LX/ArI;

    .line 1718817
    new-instance v0, LX/ArI;

    const-string v1, "CAMERA_STATE"

    const-string v2, "camera_state"

    invoke-direct {v0, v1, v7, v2}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->CAMERA_STATE:LX/ArI;

    .line 1718818
    new-instance v0, LX/ArI;

    const-string v1, "DURATION"

    const-string v2, "duration"

    invoke-direct {v0, v1, v8, v2}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DURATION:LX/ArI;

    .line 1718819
    new-instance v0, LX/ArI;

    const-string v1, "REASON"

    const/4 v2, 0x5

    const-string v3, "reason"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->REASON:LX/ArI;

    .line 1718820
    new-instance v0, LX/ArI;

    const-string v1, "INDEX"

    const/4 v2, 0x6

    const-string v3, "index"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->INDEX:LX/ArI;

    .line 1718821
    new-instance v0, LX/ArI;

    const-string v1, "RELATIVE_INDEX"

    const/4 v2, 0x7

    const-string v3, "relative_index"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->RELATIVE_INDEX:LX/ArI;

    .line 1718822
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_DATE"

    const/16 v2, 0x8

    const-string v3, "media_date"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_DATE:LX/ArI;

    .line 1718823
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_INDEX"

    const/16 v2, 0x9

    const-string v3, "media_index"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_INDEX:LX/ArI;

    .line 1718824
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_TYPE"

    const/16 v2, 0xa

    const-string v3, "media_type"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_TYPE:LX/ArI;

    .line 1718825
    new-instance v0, LX/ArI;

    const-string v1, "PERMISSION"

    const/16 v2, 0xb

    const-string v3, "permission"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->PERMISSION:LX/ArI;

    .line 1718826
    new-instance v0, LX/ArI;

    const-string v1, "PHOTO_STATE"

    const/16 v2, 0xc

    const-string v3, "photo_state"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->PHOTO_STATE:LX/ArI;

    .line 1718827
    new-instance v0, LX/ArI;

    const-string v1, "SURFACE"

    const/16 v2, 0xd

    const-string v3, "surface"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->SURFACE:LX/ArI;

    .line 1718828
    new-instance v0, LX/ArI;

    const-string v1, "PROMPT_ID"

    const/16 v2, 0xe

    const-string v3, "prompt_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->PROMPT_ID:LX/ArI;

    .line 1718829
    new-instance v0, LX/ArI;

    const-string v1, "TRACKING_STRING"

    const/16 v2, 0xf

    const-string v3, "tracking_string"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->TRACKING_STRING:LX/ArI;

    .line 1718830
    new-instance v0, LX/ArI;

    const-string v1, "APPLIED_PROMPT_IDS"

    const/16 v2, 0x10

    const-string v3, "applied_prompt_ids"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->APPLIED_PROMPT_IDS:LX/ArI;

    .line 1718831
    new-instance v0, LX/ArI;

    const-string v1, "APPLIED_TRACKING_STRINGS"

    const/16 v2, 0x11

    const-string v3, "applied_tracking_strings"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->APPLIED_TRACKING_STRINGS:LX/ArI;

    .line 1718832
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_SOURCE"

    const/16 v2, 0x12

    const-string v3, "media_source"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_SOURCE:LX/ArI;

    .line 1718833
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_CONTENT_ID"

    const/16 v2, 0x13

    const-string v3, "media_content_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_CONTENT_ID:LX/ArI;

    .line 1718834
    new-instance v0, LX/ArI;

    const-string v1, "THUMBNAIL_PROMPT_ID"

    const/16 v2, 0x14

    const-string v3, "thumbnail_prompt_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->THUMBNAIL_PROMPT_ID:LX/ArI;

    .line 1718835
    new-instance v0, LX/ArI;

    const-string v1, "THUMBNAIL_INDEX"

    const/16 v2, 0x15

    const-string v3, "thumbnail_index"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->THUMBNAIL_INDEX:LX/ArI;

    .line 1718836
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_HEIGHT"

    const/16 v2, 0x16

    const-string v3, "media_height"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_HEIGHT:LX/ArI;

    .line 1718837
    new-instance v0, LX/ArI;

    const-string v1, "MEDIA_WIDTH"

    const/16 v2, 0x17

    const-string v3, "media_width"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->MEDIA_WIDTH:LX/ArI;

    .line 1718838
    new-instance v0, LX/ArI;

    const-string v1, "SESSION_IDS_MAP"

    const/16 v2, 0x18

    const-string v3, "session_ids_map"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->SESSION_IDS_MAP:LX/ArI;

    .line 1718839
    new-instance v0, LX/ArI;

    const-string v1, "IS_CAMERA_SYSTEM"

    const/16 v2, 0x19

    const-string v3, "is_camera_system"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->IS_CAMERA_SYSTEM:LX/ArI;

    .line 1718840
    new-instance v0, LX/ArI;

    const-string v1, "EXTRA_ANNOTATIONS_DATA"

    const/16 v2, 0x1a

    const-string v3, "extra_annotations_data"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    .line 1718841
    new-instance v0, LX/ArI;

    const-string v1, "HAS_TEXT"

    const/16 v2, 0x1b

    const-string v3, "has_text"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->HAS_TEXT:LX/ArI;

    .line 1718842
    new-instance v0, LX/ArI;

    const-string v1, "TEXT_COUNT"

    const/16 v2, 0x1c

    const-string v3, "text_count"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->TEXT_COUNT:LX/ArI;

    .line 1718843
    new-instance v0, LX/ArI;

    const-string v1, "HAS_DOODLE"

    const/16 v2, 0x1d

    const-string v3, "has_doodle"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->HAS_DOODLE:LX/ArI;

    .line 1718844
    new-instance v0, LX/ArI;

    const-string v1, "DOODLE_STROKE_COUNT"

    const/16 v2, 0x1e

    const-string v3, "doodle_stroke_count"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DOODLE_STROKE_COUNT:LX/ArI;

    .line 1718845
    new-instance v0, LX/ArI;

    const-string v1, "DOODLE_SIZE_COUNT"

    const/16 v2, 0x1f

    const-string v3, "doodle_size_count"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DOODLE_SIZE_COUNT:LX/ArI;

    .line 1718846
    new-instance v0, LX/ArI;

    const-string v1, "DOODLE_COLOR_COUNT"

    const/16 v2, 0x20

    const-string v3, "doodle_color_count"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DOODLE_COLOR_COUNT:LX/ArI;

    .line 1718847
    new-instance v0, LX/ArI;

    const-string v1, "DOODLE_MAX_BRUSH_SIZE"

    const/16 v2, 0x21

    const-string v3, "doodle_max_brush_size"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DOODLE_MAX_BRUSH_SIZE:LX/ArI;

    .line 1718848
    new-instance v0, LX/ArI;

    const-string v1, "DOODLE_UNDO_COUNT"

    const/16 v2, 0x22

    const-string v3, "doodle_undo_count"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->DOODLE_UNDO_COUNT:LX/ArI;

    .line 1718849
    new-instance v0, LX/ArI;

    const-string v1, "FEED_CTA_STORY_ID"

    const/16 v2, 0x23

    const-string v3, "feed_cta_story_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->FEED_CTA_STORY_ID:LX/ArI;

    .line 1718850
    new-instance v0, LX/ArI;

    const-string v1, "FEED_STORY_ELIGIBLE_EFFECT_IDS"

    const/16 v2, 0x24

    const-string v3, "feed_story_eligible_effect_ids"

    invoke-direct {v0, v1, v2, v3}, LX/ArI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArI;->FEED_STORY_ELIGIBLE_EFFECT_IDS:LX/ArI;

    .line 1718851
    const/16 v0, 0x25

    new-array v0, v0, [LX/ArI;

    sget-object v1, LX/ArI;->ACTION:LX/ArI;

    aput-object v1, v0, v4

    sget-object v1, LX/ArI;->CAMERA_ORIENTATION:LX/ArI;

    aput-object v1, v0, v5

    sget-object v1, LX/ArI;->FLASH_MODE:LX/ArI;

    aput-object v1, v0, v6

    sget-object v1, LX/ArI;->CAMERA_STATE:LX/ArI;

    aput-object v1, v0, v7

    sget-object v1, LX/ArI;->DURATION:LX/ArI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ArI;->REASON:LX/ArI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ArI;->INDEX:LX/ArI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ArI;->RELATIVE_INDEX:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ArI;->MEDIA_DATE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ArI;->MEDIA_INDEX:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/ArI;->MEDIA_TYPE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/ArI;->PERMISSION:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/ArI;->PHOTO_STATE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/ArI;->SURFACE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/ArI;->PROMPT_ID:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/ArI;->TRACKING_STRING:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/ArI;->APPLIED_PROMPT_IDS:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/ArI;->APPLIED_TRACKING_STRINGS:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/ArI;->MEDIA_SOURCE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/ArI;->MEDIA_CONTENT_ID:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/ArI;->THUMBNAIL_PROMPT_ID:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/ArI;->THUMBNAIL_INDEX:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/ArI;->MEDIA_HEIGHT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/ArI;->MEDIA_WIDTH:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/ArI;->SESSION_IDS_MAP:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/ArI;->IS_CAMERA_SYSTEM:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/ArI;->HAS_TEXT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/ArI;->TEXT_COUNT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/ArI;->HAS_DOODLE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/ArI;->DOODLE_STROKE_COUNT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/ArI;->DOODLE_SIZE_COUNT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/ArI;->DOODLE_COLOR_COUNT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/ArI;->DOODLE_MAX_BRUSH_SIZE:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/ArI;->DOODLE_UNDO_COUNT:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/ArI;->FEED_CTA_STORY_ID:LX/ArI;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/ArI;->FEED_STORY_ELIGIBLE_EFFECT_IDS:LX/ArI;

    aput-object v2, v0, v1

    sput-object v0, LX/ArI;->$VALUES:[LX/ArI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1718811
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1718812
    iput-object p3, p0, LX/ArI;->mName:Ljava/lang/String;

    .line 1718813
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArI;
    .locals 1

    .prologue
    .line 1718852
    const-class v0, LX/ArI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArI;

    return-object v0
.end method

.method public static values()[LX/ArI;
    .locals 1

    .prologue
    .line 1718810
    sget-object v0, LX/ArI;->$VALUES:[LX/ArI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArI;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718809
    iget-object v0, p0, LX/ArI;->mName:Ljava/lang/String;

    return-object v0
.end method
