.class public final LX/BZg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/29q;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/app/Activity;

.field public final synthetic d:LX/BZj;


# direct methods
.method public constructor <init>(LX/BZj;LX/29q;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1797958
    iput-object p1, p0, LX/BZg;->d:LX/BZj;

    iput-object p2, p0, LX/BZg;->a:LX/29q;

    iput-object p3, p0, LX/BZg;->b:Ljava/lang/String;

    iput-object p4, p0, LX/BZg;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1797959
    iget-object v0, p0, LX/BZg;->a:LX/29q;

    if-eqz v0, :cond_0

    .line 1797960
    iget-object v0, p0, LX/BZg;->a:LX/29q;

    .line 1797961
    iget-object v1, v0, LX/29q;->a:LX/2CB;

    const/4 v2, 0x0

    .line 1797962
    iput-boolean v2, v1, LX/2CB;->a:Z

    .line 1797963
    iget-object v1, v0, LX/29q;->a:LX/2CB;

    iget-object v1, v1, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2tq;->g:LX/0Tn;

    iget-object p1, v0, LX/29q;->a:LX/2CB;

    iget-object p1, p1, LX/2CB;->d:LX/0WV;

    invoke-virtual {p1}, LX/0WV;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1797964
    :cond_0
    const-string v0, "market://details?id=%s"

    iget-object v1, p0, LX/BZg;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1797965
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1797966
    iget-object v1, p0, LX/BZg;->d:LX/BZj;

    iget-object v1, v1, LX/BZj;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/BZg;->c:Landroid/app/Activity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1797967
    return-void
.end method
