.class public LX/Apn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Apl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Apo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716737
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Apo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716734
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716735
    iput-object p1, p0, LX/Apn;->b:LX/0Ot;

    .line 1716736
    return-void
.end method

.method public static a(LX/0QB;)LX/Apn;
    .locals 4

    .prologue
    .line 1716691
    const-class v1, LX/Apn;

    monitor-enter v1

    .line 1716692
    :try_start_0
    sget-object v0, LX/Apn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716693
    sput-object v2, LX/Apn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716694
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716695
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716696
    new-instance v3, LX/Apn;

    const/16 p0, 0x2211

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Apn;-><init>(LX/0Ot;)V

    .line 1716697
    move-object v0, v3

    .line 1716698
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716699
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716700
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1716733
    const v0, -0xd398893

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1716732
    const v0, 0x5e316fe4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1716731
    const v0, 0x5e316fe4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1716728
    check-cast p2, LX/Apm;

    .line 1716729
    iget-object v0, p0, LX/Apn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Apo;

    iget v2, p2, LX/Apm;->a:I

    iget-object v3, p2, LX/Apm;->b:Ljava/lang/CharSequence;

    iget v4, p2, LX/Apm;->c:I

    iget-object v5, p2, LX/Apm;->d:Ljava/lang/CharSequence;

    iget-object v6, p2, LX/Apm;->e:Landroid/util/SparseArray;

    iget-object v7, p2, LX/Apm;->f:LX/1dQ;

    iget-object v8, p2, LX/Apm;->g:Landroid/view/View$OnClickListener;

    iget-object v9, p2, LX/Apm;->h:LX/1dQ;

    iget-object v10, p2, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v11, p2, LX/Apm;->j:LX/1X1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, LX/Apo;->a(LX/1De;ILjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/util/SparseArray;LX/1dQ;Landroid/view/View$OnClickListener;LX/1dQ;Landroid/widget/CompoundButton$OnCheckedChangeListener;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1716730
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1716710
    invoke-static {}, LX/1dS;->b()V

    .line 1716711
    iget v0, p1, LX/1dQ;->b:I

    .line 1716712
    sparse-switch v0, :sswitch_data_0

    .line 1716713
    :goto_0
    return-object v2

    .line 1716714
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1716715
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1716716
    check-cast v1, LX/Apm;

    .line 1716717
    iget-object p1, p0, LX/Apn;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Apm;->g:Landroid/view/View$OnClickListener;

    .line 1716718
    if-eqz p1, :cond_0

    .line 1716719
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1716720
    :cond_0
    goto :goto_0

    .line 1716721
    :sswitch_1
    check-cast p2, LX/Apr;

    .line 1716722
    iget-boolean v0, p2, LX/Apr;->a:Z

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1716723
    check-cast v1, LX/Apm;

    .line 1716724
    iget-object p1, p0, LX/Apn;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Apm;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1716725
    if-eqz p1, :cond_1

    .line 1716726
    const/4 p0, 0x0

    invoke-interface {p1, p0, v0}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 1716727
    :cond_1
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xd398893 -> :sswitch_1
        0x5e316fe4 -> :sswitch_0
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/Apl;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716702
    new-instance v1, LX/Apm;

    invoke-direct {v1, p0}, LX/Apm;-><init>(LX/Apn;)V

    .line 1716703
    sget-object v2, LX/Apn;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Apl;

    .line 1716704
    if-nez v2, :cond_0

    .line 1716705
    new-instance v2, LX/Apl;

    invoke-direct {v2}, LX/Apl;-><init>()V

    .line 1716706
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Apl;->a$redex0(LX/Apl;LX/1De;IILX/Apm;)V

    .line 1716707
    move-object v1, v2

    .line 1716708
    move-object v0, v1

    .line 1716709
    return-object v0
.end method
