.class public final enum LX/BZw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZw;

.field public static final enum PROVIDE_FEEDBACK:LX/BZw;

.field public static final enum RATE_ON_PLAY_STORE:LX/BZw;

.field public static final enum STAR_RATING:LX/BZw;

.field public static final enum THANKS_FOR_FEEDBACK:LX/BZw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1798238
    new-instance v0, LX/BZw;

    const-string v1, "STAR_RATING"

    invoke-direct {v0, v1, v2}, LX/BZw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZw;->STAR_RATING:LX/BZw;

    .line 1798239
    new-instance v0, LX/BZw;

    const-string v1, "PROVIDE_FEEDBACK"

    invoke-direct {v0, v1, v3}, LX/BZw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZw;->PROVIDE_FEEDBACK:LX/BZw;

    .line 1798240
    new-instance v0, LX/BZw;

    const-string v1, "THANKS_FOR_FEEDBACK"

    invoke-direct {v0, v1, v4}, LX/BZw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZw;->THANKS_FOR_FEEDBACK:LX/BZw;

    .line 1798241
    new-instance v0, LX/BZw;

    const-string v1, "RATE_ON_PLAY_STORE"

    invoke-direct {v0, v1, v5}, LX/BZw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZw;->RATE_ON_PLAY_STORE:LX/BZw;

    .line 1798242
    const/4 v0, 0x4

    new-array v0, v0, [LX/BZw;

    sget-object v1, LX/BZw;->STAR_RATING:LX/BZw;

    aput-object v1, v0, v2

    sget-object v1, LX/BZw;->PROVIDE_FEEDBACK:LX/BZw;

    aput-object v1, v0, v3

    sget-object v1, LX/BZw;->THANKS_FOR_FEEDBACK:LX/BZw;

    aput-object v1, v0, v4

    sget-object v1, LX/BZw;->RATE_ON_PLAY_STORE:LX/BZw;

    aput-object v1, v0, v5

    sput-object v0, LX/BZw;->$VALUES:[LX/BZw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1798243
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromInt(I)LX/BZw;
    .locals 2

    .prologue
    .line 1798244
    invoke-static {}, LX/BZw;->values()[LX/BZw;

    move-result-object v0

    .line 1798245
    if-ltz p0, :cond_0

    array-length v1, v0

    if-lt p0, v1, :cond_1

    .line 1798246
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized int value for Screen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1798247
    :cond_1
    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZw;
    .locals 1

    .prologue
    .line 1798248
    const-class v0, LX/BZw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZw;

    return-object v0
.end method

.method public static values()[LX/BZw;
    .locals 1

    .prologue
    .line 1798249
    sget-object v0, LX/BZw;->$VALUES:[LX/BZw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZw;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 1798250
    invoke-virtual {p0}, LX/BZw;->ordinal()I

    move-result v0

    return v0
.end method
