.class public LX/Bkn;
.super LX/3x6;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:LX/1OM;

.field private final c:I

.field public d:I


# direct methods
.method public constructor <init>(IILX/1OM;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1814911
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1814912
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    .line 1814913
    iget-object v0, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1814914
    iget-object v0, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1814915
    iget-object v0, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1814916
    iget-object v0, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1814917
    iput-object p3, p0, LX/Bkn;->b:LX/1OM;

    .line 1814918
    iput p4, p0, LX/Bkn;->c:I

    .line 1814919
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 8

    .prologue
    .line 1814920
    invoke-super {p0, p1, p2, p3}, LX/3x6;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1814921
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v7

    .line 1814922
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    .line 1814923
    invoke-virtual {p2, v6}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1814924
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    invoke-virtual {v1}, LX/1a1;->d()I

    move-result v1

    .line 1814925
    iget-object v2, p0, LX/Bkn;->b:LX/1OM;

    invoke-virtual {v2, v1}, LX/1OM;->getItemViewType(I)I

    move-result v1

    iget v2, p0, LX/Bkn;->c:I

    if-ne v1, v2, :cond_0

    .line 1814926
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    .line 1814927
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int v3, v2, v3

    .line 1814928
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget v2, p0, LX/Bkn;->d:I

    add-int/2addr v0, v2

    .line 1814929
    int-to-float v1, v1

    int-to-float v2, v0

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/Bkn;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1814930
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1814931
    :cond_1
    return-void
.end method
