.class public LX/Ac3;
.super Landroid/graphics/drawable/ColorDrawable;
.source ""


# instance fields
.field private final a:Landroid/animation/ArgbEvaluator;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 1692130
    invoke-direct {p0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1692131
    iput p1, p0, LX/Ac3;->b:I

    .line 1692132
    iput p2, p0, LX/Ac3;->c:I

    .line 1692133
    iget v0, p0, LX/Ac3;->b:I

    iget v1, p0, LX/Ac3;->c:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Ac3;->d:Z

    .line 1692134
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, LX/Ac3;->a:Landroid/animation/ArgbEvaluator;

    .line 1692135
    return-void

    .line 1692136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    .line 1692137
    iget-boolean v0, p0, LX/Ac3;->d:Z

    if-nez v0, :cond_0

    .line 1692138
    :goto_0
    return-void

    .line 1692139
    :cond_0
    iget-object v0, p0, LX/Ac3;->a:Landroid/animation/ArgbEvaluator;

    iget v1, p0, LX/Ac3;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, LX/Ac3;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/Ac3;->setColor(I)V

    goto :goto_0
.end method
