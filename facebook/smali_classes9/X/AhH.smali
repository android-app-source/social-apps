.class public LX/AhH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/0tX;

.field public final e:LX/03V;

.field public final f:Landroid/os/Handler;

.field public final g:Ljava/lang/Runnable;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:I

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/AhG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1702366
    const-class v0, LX/AhH;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AhH;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;Landroid/os/Handler;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1702367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1702368
    iput-object p2, p0, LX/AhH;->c:Ljava/util/concurrent/ExecutorService;

    .line 1702369
    iput-object p3, p0, LX/AhH;->d:LX/0tX;

    .line 1702370
    iput-object p4, p0, LX/AhH;->e:LX/03V;

    .line 1702371
    iput-object p5, p0, LX/AhH;->f:Landroid/os/Handler;

    .line 1702372
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AhH;->h:Ljava/util/List;

    .line 1702373
    new-instance v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingDownloader$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingDownloader$1;-><init>(LX/AhH;)V

    iput-object v0, p0, LX/AhH;->g:Ljava/lang/Runnable;

    .line 1702374
    sget v0, LX/1v6;->Z:I

    const/4 v1, 0x3

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, LX/AhH;->i:I

    .line 1702375
    return-void
.end method

.method public static e(LX/AhH;)V
    .locals 2

    .prologue
    .line 1702376
    iget-object v0, p0, LX/AhH;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AhH;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1702377
    iget-object v0, p0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1702378
    iget-object v0, p0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1702379
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1702380
    iget-object v0, p0, LX/AhH;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1702381
    :goto_0
    return-void

    .line 1702382
    :cond_0
    invoke-static {p0}, LX/AhH;->e(LX/AhH;)V

    .line 1702383
    iget-object v0, p0, LX/AhH;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AhH;->g:Ljava/lang/Runnable;

    const v2, -0x5d8c4fda

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1702384
    iget-object v0, p0, LX/AhH;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method
