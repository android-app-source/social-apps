.class public LX/ApZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Apa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Apa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716319
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716320
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ApZ;->b:LX/0Zi;

    .line 1716321
    iput-object p1, p0, LX/ApZ;->a:LX/0Ot;

    .line 1716322
    return-void
.end method

.method public static a(LX/0QB;)LX/ApZ;
    .locals 4

    .prologue
    .line 1716308
    const-class v1, LX/ApZ;

    monitor-enter v1

    .line 1716309
    :try_start_0
    sget-object v0, LX/ApZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716310
    sput-object v2, LX/ApZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716311
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716312
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716313
    new-instance v3, LX/ApZ;

    const/16 p0, 0x2201

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ApZ;-><init>(LX/0Ot;)V

    .line 1716314
    move-object v0, v3

    .line 1716315
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716316
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716317
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 15

    .prologue
    .line 1716303
    check-cast p2, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    .line 1716304
    iget-object v1, p0, LX/ApZ;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Apa;

    move-object/from16 v0, p2

    iget v3, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->a:I

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->f:LX/33B;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->g:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->i:LX/1dQ;

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->l:Z

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v14}, LX/Apa;->a(LX/1De;ILjava/lang/CharSequence;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1Po;LX/33B;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1X1;LX/1X1;Z)LX/1Dg;

    move-result-object v1

    .line 1716305
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716306
    invoke-static {}, LX/1dS;->b()V

    .line 1716307
    const/4 v0, 0x0

    return-object v0
.end method
