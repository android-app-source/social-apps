.class public final LX/CAp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/1Pf;

.field public final synthetic c:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)V
    .locals 0

    .prologue
    .line 1855743
    iput-object p1, p0, LX/CAp;->c:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    iput-object p2, p0, LX/CAp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/CAp;->b:LX/1Pf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7e5bc07d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855744
    iget-object v1, p0, LX/CAp;->c:Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;

    iget-object v2, p0, LX/CAp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/CAp;->b:LX/1Pf;

    const/4 v12, 0x0

    const/4 v7, 0x0

    .line 1855745
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1855746
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    .line 1855747
    iget-object v5, v1, Lcom/facebook/feedplugins/profile/introcard/favphotos/ViewAllFavPhotosPartDefinition;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/26I;

    invoke-static {v2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v9

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v10

    :goto_0
    move-object v8, p1

    move v11, v7

    move-object v13, v3

    invoke-virtual/range {v5 .. v13}, LX/26I;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILandroid/view/View;LX/1PT;Ljava/lang/String;ZLX/9hN;LX/1Pq;)V

    .line 1855748
    const v1, -0x4a9cf339

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v10, v12

    .line 1855749
    goto :goto_0
.end method
