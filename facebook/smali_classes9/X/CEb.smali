.class public final LX/CEb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;)V
    .locals 0

    .prologue
    .line 1861463
    iput-object p1, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1861464
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1861465
    check-cast p1, Ljava/util/List;

    .line 1861466
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1861467
    :cond_0
    :goto_0
    return-void

    .line 1861468
    :cond_1
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861469
    iput-object p1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->n:Ljava/util/List;

    .line 1861470
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Landroid/graphics/drawable/BitmapDrawable;

    .line 1861471
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 1861472
    iput v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    .line 1861473
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    iget-object v1, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    iget v1, v1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1861474
    iput-object v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->i:Ljava/lang/String;

    .line 1861475
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1861476
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1861477
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1861478
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v2, v1

    .line 1861479
    aget-object v0, v2, v1

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 1861480
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1861481
    :cond_3
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    new-instance v1, LX/1ap;

    .line 1861482
    array-length v3, v2

    if-lez v3, :cond_4

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v2, v3

    if-eqz v3, :cond_5

    :cond_4
    move-object v3, v2

    .line 1861483
    :goto_2
    move-object v2, v3

    .line 1861484
    invoke-direct {v1, v2}, LX/1ap;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1861485
    iput-object v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    .line 1861486
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, LX/1ap;->c(I)V

    .line 1861487
    iget-object v0, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    iget-object v1, p0, LX/CEb;->a:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    iget-object v1, v1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1861488
    :cond_5
    const/4 v3, 0x0

    .line 1861489
    :goto_3
    array-length v4, v2

    if-ge v3, v4, :cond_6

    aget-object v4, v2, v3

    if-eqz v4, :cond_6

    .line 1861490
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1861491
    :cond_6
    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_2
.end method
