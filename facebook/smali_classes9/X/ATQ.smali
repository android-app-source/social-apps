.class public LX/ATQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/73w;


# direct methods
.method public constructor <init>(LX/73w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1676018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1676019
    iput-object p1, p0, LX/ATQ;->a:LX/73w;

    .line 1676020
    return-void
.end method

.method public static a(LX/0QB;)LX/ATQ;
    .locals 4

    .prologue
    .line 1676007
    const-class v1, LX/ATQ;

    monitor-enter v1

    .line 1676008
    :try_start_0
    sget-object v0, LX/ATQ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1676009
    sput-object v2, LX/ATQ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1676010
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676011
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1676012
    new-instance p0, LX/ATQ;

    invoke-static {v0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v3

    check-cast v3, LX/73w;

    invoke-direct {p0, v3}, LX/ATQ;-><init>(LX/73w;)V

    .line 1676013
    move-object v0, p0

    .line 1676014
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1676015
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ATQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1676016
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1676017
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
