.class public final LX/B72;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V
    .locals 0

    .prologue
    .line 1747494
    iput-object p1, p0, LX/B72;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;LX/B76;)V
    .locals 6

    .prologue
    .line 1747495
    iget-object v0, p0, LX/B72;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    .line 1747496
    iget-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->n()Ljava/lang/String;

    move-result-object v1

    .line 1747497
    iget-object v2, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->i:LX/B8t;

    .line 1747498
    if-nez v1, :cond_2

    .line 1747499
    :goto_0
    sget-object v1, LX/B76;->SUCCESS:LX/B76;

    if-ne p2, v1, :cond_1

    iget-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    .line 1747500
    iget-object v2, v1, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-static {v2}, LX/2sb;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    move v1, v2

    .line 1747501
    if-eqz v1, :cond_1

    .line 1747502
    invoke-virtual {p1}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    move-result-object v1

    .line 1747503
    if-eqz v1, :cond_0

    .line 1747504
    iget-object v2, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->z:LX/B8p;

    iget-object v3, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    const p0, 0x7f0d18f0

    invoke-virtual {v5, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1747505
    if-nez v5, :cond_3

    .line 1747506
    :goto_1
    iget-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    invoke-interface {v1}, LX/B7i;->b()V

    .line 1747507
    :goto_2
    return-void

    .line 1747508
    :cond_0
    sget-object p2, LX/B76;->FAILURE:LX/B76;

    .line 1747509
    :cond_1
    iget-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    .line 1747510
    iget-object v2, v1, LX/B7F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1747511
    invoke-static {v1, p2}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/B76;)Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    move-result-object v1

    .line 1747512
    iget-object v2, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    invoke-interface {v2, v1}, LX/B7i;->a(LX/B6H;)V

    goto :goto_2

    .line 1747513
    :cond_2
    iget-object v3, v2, LX/B8t;->a:Ljava/util/Map;

    invoke-interface {v3, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1747514
    :cond_3
    sget-object p0, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {v5, p0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1747515
    const p0, 0x7f0d0083

    const-string p1, "cta_lead_gen_visit_offsite_click"

    invoke-virtual {v5, p0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1747516
    iget-object p0, v2, LX/B8p;->a:LX/B6k;

    .line 1747517
    iget-object p1, v3, LX/B7F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object p1, p1

    .line 1747518
    invoke-virtual {p0, p1}, LX/B6k;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/B6j;

    move-result-object p0

    .line 1747519
    invoke-virtual {v3}, LX/B7F;->f()Z

    move-result p1

    .line 1747520
    if-eqz p1, :cond_4

    .line 1747521
    iput-object v4, p0, LX/B6j;->j:Ljava/lang/String;

    .line 1747522
    :cond_4
    iget-object p1, v3, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-static {p1}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object p1

    move-object p1, p1

    .line 1747523
    invoke-static {p1}, LX/B8u;->fromValue(Ljava/lang/String;)LX/B8u;

    move-result-object p1

    .line 1747524
    sget-object p2, LX/B8u;->POST:LX/B8u;

    if-ne p1, p2, :cond_5

    .line 1747525
    iput-object v4, p0, LX/B6j;->j:Ljava/lang/String;

    .line 1747526
    :cond_5
    sget-object p2, LX/B8u;->AUTOFILL:LX/B8u;

    if-ne p1, p2, :cond_6

    .line 1747527
    iget-object p1, v2, LX/B8p;->c:LX/B6u;

    .line 1747528
    iget-object p2, p1, LX/B6u;->e:Ljava/util/Map;

    invoke-static {p2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object p2

    move-object p1, p2

    .line 1747529
    invoke-static {p1}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object p1

    .line 1747530
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 1747531
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v3, "dict = "

    invoke-direct {p1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, ";"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1747532
    new-instance p2, Ljava/lang/StringBuilder;

    const-string v3, "(function() {"

    invoke-direct {p2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "  for (var key in dict) {    var elmt = document.getElementById(key) || "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "      document.getElementsByName(key)[0];    if (elmt) {"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "      elmt.value=dict[key];    }"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "  }})();"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1747533
    move-object p1, p1

    .line 1747534
    iput-object p1, p0, LX/B6j;->k:Ljava/lang/String;

    .line 1747535
    :cond_6
    iget-object p1, v2, LX/B8p;->b:LX/B6l;

    const-string p2, "cta_lead_gen_visit_offsite_click"

    invoke-virtual {p1, p2}, LX/B6l;->a(Ljava/lang/String;)V

    .line 1747536
    const/4 p1, 0x1

    invoke-virtual {p0, v5, v1, p1}, LX/B6j;->b(Landroid/view/View;Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method
