.class public final LX/B5T;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1744401
    const-class v1, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;

    const v0, -0x50b2fe4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "InstantArticleQuery"

    const-string v6, "0900228c9373457c168d073da7f19eaa"

    const-string v7, "node"

    const-string v8, "10155248169791729"

    const/4 v9, 0x0

    .line 1744402
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1744403
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1744404
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1744405
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1744406
    sparse-switch v0, :sswitch_data_0

    .line 1744407
    :goto_0
    return-object p1

    .line 1744408
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1744409
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1744410
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1744411
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1744412
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1744413
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1744414
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1744415
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1744416
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1744417
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1744418
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1744419
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1744420
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1744421
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1744422
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1744423
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1744424
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1744425
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1744426
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1744427
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1744428
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1744429
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x75243e2a -> :sswitch_a
        -0x6855b65a -> :sswitch_15
        -0x64272472 -> :sswitch_7
        -0x493280e9 -> :sswitch_9
        -0x33f8633e -> :sswitch_14
        -0x2e03bc83 -> :sswitch_8
        -0x15afd767 -> :sswitch_b
        -0x1139a4c7 -> :sswitch_4
        -0x38aa96a -> :sswitch_f
        0x2ef6341 -> :sswitch_13
        0x683094a -> :sswitch_11
        0x956c6da -> :sswitch_5
        0x1f6fea44 -> :sswitch_2
        0x21597091 -> :sswitch_0
        0x2e61b03a -> :sswitch_e
        0x45e5f0b4 -> :sswitch_c
        0x66c8fb9c -> :sswitch_d
        0x7191d8b1 -> :sswitch_12
        0x73a026b5 -> :sswitch_6
        0x78668257 -> :sswitch_10
        0x78a3267b -> :sswitch_3
        0x7e6ceb8b -> :sswitch_1
    .end sparse-switch
.end method
