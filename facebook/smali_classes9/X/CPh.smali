.class public final LX/CPh;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPh;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884803
    const/4 v0, 0x0

    sput-object v0, LX/CPh;->a:LX/CPh;

    .line 1884804
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPh;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884778
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884779
    new-instance v0, LX/CPi;

    invoke-direct {v0}, LX/CPi;-><init>()V

    iput-object v0, p0, LX/CPh;->c:LX/CPi;

    .line 1884780
    return-void
.end method

.method public static declared-synchronized q()LX/CPh;
    .locals 2

    .prologue
    .line 1884781
    const-class v1, LX/CPh;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPh;->a:LX/CPh;

    if-nez v0, :cond_0

    .line 1884782
    new-instance v0, LX/CPh;

    invoke-direct {v0}, LX/CPh;-><init>()V

    sput-object v0, LX/CPh;->a:LX/CPh;

    .line 1884783
    :cond_0
    sget-object v0, LX/CPh;->a:LX/CPh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884784
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1884785
    invoke-static {}, LX/1dS;->b()V

    .line 1884786
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1884787
    check-cast p4, LX/CPg;

    .line 1884788
    iget-object v0, p4, LX/CPg;->a:LX/CNb;

    iget-object v1, p4, LX/CPg;->b:LX/CNc;

    const/high16 p4, 0x3f800000    # 1.0f

    .line 1884789
    const-string v2, "children"

    invoke-virtual {v0, v2}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1884790
    if-nez v2, :cond_0

    .line 1884791
    const/4 v2, 0x0

    .line 1884792
    :goto_0
    move-object v0, v2

    .line 1884793
    return-object v0

    .line 1884794
    :cond_0
    invoke-static {v2, v1, p1}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v2

    .line 1884795
    invoke-static {p2}, LX/CPi;->a(I)I

    move-result v3

    .line 1884796
    invoke-static {p3}, LX/CPi;->a(I)I

    move-result v4

    .line 1884797
    invoke-static {p1, v2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v2

    .line 1884798
    const-string v5, "ratio"

    invoke-virtual {v0, v5, p4}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v5

    .line 1884799
    new-instance p0, LX/1no;

    invoke-direct {p0}, LX/1no;-><init>()V

    .line 1884800
    div-float v5, p4, v5

    invoke-static {v3, v4, v5, p0}, LX/1oC;->a(IIFLX/1no;)V

    .line 1884801
    iget v3, p0, LX/1no;->a:I

    invoke-interface {v2, v3}, LX/1Di;->k(I)LX/1Di;

    move-result-object v2

    iget v3, p0, LX/1no;->a:I

    invoke-interface {v2, v3}, LX/1Di;->l(I)LX/1Di;

    move-result-object v2

    iget v3, p0, LX/1no;->b:I

    invoke-interface {v2, v3}, LX/1Di;->s(I)LX/1Di;

    move-result-object v2

    iget v3, p0, LX/1no;->b:I

    invoke-interface {v2, v3}, LX/1Di;->u(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1884802
    const/4 v0, 0x1

    return v0
.end method
