.class public final LX/AZS;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/AZV;


# direct methods
.method public constructor <init>(LX/AZV;)V
    .locals 0

    .prologue
    .line 1686982
    iput-object p1, p0, LX/AZS;->a:LX/AZV;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1686996
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1686983
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1686984
    :goto_0
    return v0

    .line 1686985
    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-lez v1, :cond_2

    .line 1686986
    iget-object v0, p0, LX/AZS;->a:LX/AZV;

    iget v0, v0, LX/AZV;->i:I

    add-int/lit8 v0, v0, -0x1

    .line 1686987
    if-gez v0, :cond_1

    .line 1686988
    iget-object v0, p0, LX/AZS;->a:LX/AZV;

    iget-object v0, v0, LX/AZV;->h:LX/AZa;

    invoke-virtual {v0}, LX/AZZ;->e()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1686989
    :cond_1
    const-string v1, "swipe_right"

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 1686990
    :goto_1
    iget-object v2, p0, LX/AZS;->a:LX/AZV;

    invoke-static {v2, v1}, LX/AZV;->a$redex0(LX/AZV;I)V

    .line 1686991
    iget-object v1, p0, LX/AZS;->a:LX/AZV;

    iget-object v1, v1, LX/AZV;->a:LX/AVT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/AZS;->a:LX/AZV;

    iget-object v3, v3, LX/AZV;->h:LX/AZa;

    invoke-virtual {v3}, LX/AZa;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "filter_"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/AZS;->a:LX/AZV;

    iget-object v0, v0, LX/AZV;->h:LX/AZa;

    iget-object v4, p0, LX/AZS;->a:LX/AZV;

    iget v4, v4, LX/AZV;->i:I

    invoke-virtual {v0, v4}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZO;

    iget-object v0, v0, LX/AZO;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/Aa6;->c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V

    .line 1686992
    const/4 v0, 0x1

    goto :goto_0

    .line 1686993
    :cond_2
    iget-object v1, p0, LX/AZS;->a:LX/AZV;

    iget v1, v1, LX/AZV;->i:I

    add-int/lit8 v1, v1, 0x1

    .line 1686994
    iget-object v2, p0, LX/AZS;->a:LX/AZV;

    iget-object v2, v2, LX/AZV;->h:LX/AZa;

    invoke-virtual {v2}, LX/AZZ;->e()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 1686995
    :goto_2
    const-string v1, "swipe_left"

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
