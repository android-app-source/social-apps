.class public interface abstract LX/CHi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CHg;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "InstantShoppingPhotoElementFragment"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract j()LX/CHX;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()LX/0Px;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAnnotations"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingAnnotationsFragment$;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract n()LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTouchTargets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method
