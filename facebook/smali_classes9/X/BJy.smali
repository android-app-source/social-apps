.class public final LX/BJy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772512
    iput-object p1, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5dbfa759

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1772513
    iget-object v0, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v2

    iget-object v3, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v3, v3, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->t()LX/0Rf;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 1772514
    iput-object v3, v2, LX/A5S;->j:LX/0Px;

    .line 1772515
    move-object v2, v2

    .line 1772516
    invoke-virtual {v2}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v2

    .line 1772517
    iget-object v0, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x14

    iget-object v4, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-interface {v0, v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1772518
    iget-object v0, p0, LX/BJy;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "people_tagging_button"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772519
    const v0, -0x247d1b7a

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
