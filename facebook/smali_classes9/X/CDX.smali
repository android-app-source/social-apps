.class public LX/CDX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field private final a:LX/CDg;

.field private final b:LX/198;

.field private final c:LX/1C2;

.field private final d:LX/1Yd;

.field private final e:LX/0bH;

.field private final f:LX/2ms;

.field private g:LX/3J0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:LX/CDf;

.field private final i:LX/1YQ;

.field private final j:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/2pa;

.field private final m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CDg;LX/1YQ;LX/198;LX/1C2;LX/1Yd;LX/0bH;LX/2ms;LX/CDf;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2pa;Lcom/facebook/video/analytics/VideoFeedStoryInfo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CDg;",
            "LX/1YQ;",
            "LX/198;",
            "LX/1C2;",
            "LX/1Yd;",
            "LX/0bH;",
            "LX/2ms;",
            "LX/CDf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2pa;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1859583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859584
    iput-object p1, p0, LX/CDX;->a:LX/CDg;

    .line 1859585
    iput-object p3, p0, LX/CDX;->b:LX/198;

    .line 1859586
    iput-object p4, p0, LX/CDX;->c:LX/1C2;

    .line 1859587
    iput-object p5, p0, LX/CDX;->d:LX/1Yd;

    .line 1859588
    iput-object p6, p0, LX/CDX;->e:LX/0bH;

    .line 1859589
    iput-object p7, p0, LX/CDX;->f:LX/2ms;

    .line 1859590
    iput-object p8, p0, LX/CDX;->h:LX/CDf;

    .line 1859591
    iput-object p2, p0, LX/CDX;->i:LX/1YQ;

    .line 1859592
    iput-object p9, p0, LX/CDX;->j:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859593
    iput-object p10, p0, LX/CDX;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859594
    iput-object p11, p0, LX/CDX;->l:LX/2pa;

    .line 1859595
    iput-object p12, p0, LX/CDX;->m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859596
    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 1859560
    iget-object v0, p0, LX/CDX;->d:LX/1Yd;

    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->a:LX/395;

    invoke-virtual {v0, v1}, LX/1Yd;->a(LX/395;)V

    .line 1859561
    iget-object v0, p0, LX/CDX;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CDX;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    move-object v9, v0

    .line 1859562
    :goto_0
    if-nez v9, :cond_1

    .line 1859563
    :goto_1
    return-void

    :cond_0
    move-object v9, v8

    .line 1859564
    goto :goto_0

    .line 1859565
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundResource(I)V

    .line 1859566
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    .line 1859567
    iget-boolean v1, v0, LX/1C2;->B:Z

    move v0, v1

    .line 1859568
    if-nez v0, :cond_2

    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    .line 1859569
    iget-boolean v1, v0, LX/1C2;->C:Z

    move v0, v1

    .line 1859570
    if-eqz v0, :cond_3

    .line 1859571
    :cond_2
    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->a:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/CDX;->a:LX/CDg;

    iget-object v4, v4, LX/CDg;->a:LX/395;

    invoke-virtual {v4}, LX/395;->p()I

    move-result v4

    iget-object v5, p0, LX/CDX;->a:LX/CDg;

    iget-object v5, v5, LX/CDg;->a:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/CDX;->a:LX/CDg;

    iget-object v6, v6, LX/CDg;->a:LX/395;

    .line 1859572
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 1859573
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 1859574
    new-instance v7, LX/AnR;

    iget-object v10, p0, LX/CDX;->a:LX/CDg;

    iget-object v10, v10, LX/CDg;->a:LX/395;

    iget-object v11, p0, LX/CDX;->l:LX/2pa;

    iget-object v11, v11, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v10, v11}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1859575
    :cond_3
    iget-object v0, p0, LX/CDX;->j:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859576
    if-eqz v0, :cond_4

    .line 1859577
    invoke-static {v0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1859578
    iget-object v2, p0, LX/CDX;->e:LX/0bH;

    new-instance v3, LX/1Ze;

    .line 1859579
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1859580
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v3, v4, v0}, LX/1Ze;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1859581
    :cond_4
    iget-object v0, p0, LX/CDX;->f:LX/2ms;

    iget-object v1, p0, LX/CDX;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v9, v1}, LX/2ms;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    :cond_5
    move-object v0, v8

    .line 1859582
    goto :goto_2
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1859518
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->b:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    .line 1859519
    iget-boolean v1, v0, LX/1C2;->B:Z

    move v0, v1

    .line 1859520
    if-nez v0, :cond_0

    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    .line 1859521
    iget-boolean v1, v0, LX/1C2;->C:Z

    move v0, v1

    .line 1859522
    if-eqz v0, :cond_1

    .line 1859523
    :cond_0
    iget-object v0, p0, LX/CDX;->c:LX/1C2;

    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->a:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget v4, p2, LX/7Jv;->c:I

    iget-object v5, p0, LX/CDX;->a:LX/CDg;

    iget-object v5, v5, LX/CDg;->a:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/CDX;->a:LX/CDg;

    iget-object v6, v6, LX/CDg;->a:LX/395;

    .line 1859524
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 1859525
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 1859526
    new-instance v7, LX/AnR;

    iget-object v8, p0, LX/CDX;->a:LX/CDg;

    iget-object v8, v8, LX/CDg;->a:LX/395;

    iget-object v9, p0, LX/CDX;->l:LX/2pa;

    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v8, v9}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1859527
    :cond_1
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iput-boolean v10, v0, LX/CDg;->i:Z

    .line 1859528
    iget-object v0, p0, LX/CDX;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1859529
    if-nez v0, :cond_2

    .line 1859530
    :goto_0
    return-void

    .line 1859531
    :cond_2
    iget-object v1, p0, LX/CDX;->h:LX/CDf;

    invoke-static {v1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/CDf;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundResource(I)V

    .line 1859532
    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->c:LX/2oO;

    iget-boolean v2, p2, LX/7Jv;->b:Z

    iget-boolean v3, p2, LX/7Jv;->a:Z

    invoke-virtual {v1, v2, v3}, LX/2oO;->a(ZZ)V

    .line 1859533
    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, v1, LX/CDg;->d:LX/2oL;

    iget v2, p2, LX/7Jv;->c:I

    invoke-virtual {v1, v2}, LX/2oL;->a(I)V

    .line 1859534
    iget-boolean v1, p2, LX/7Jv;->b:Z

    if-nez v1, :cond_7

    iget-boolean v1, p2, LX/7Jv;->a:Z

    if-nez v1, :cond_7

    iget v1, p2, LX/7Jv;->c:I

    if-lez v1, :cond_7

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1859535
    if-eqz v1, :cond_6

    .line 1859536
    iget-object v1, p0, LX/CDX;->m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859537
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v2

    .line 1859538
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1859539
    iget-object v1, p0, LX/CDX;->m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859540
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 1859541
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1859542
    iget-object v1, p0, LX/CDX;->a:LX/CDg;

    iget-object v2, p0, LX/CDX;->m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v3, p0, LX/CDX;->b:LX/198;

    sget-object v4, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    new-instance v5, LX/7K4;

    iget v6, p2, LX/7Jv;->c:I

    iget v7, p2, LX/7Jv;->d:I

    invoke-direct {v5, v6, v7}, LX/7K4;-><init>(II)V

    invoke-static/range {v0 .. v5}, LX/CDd;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/198;LX/04g;LX/7K4;)V

    .line 1859543
    :cond_3
    :goto_2
    iget-boolean v0, p2, LX/7Jv;->b:Z

    if-eqz v0, :cond_4

    .line 1859544
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iget-object v1, p0, LX/CDX;->m:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    sget-object v2, LX/04g;->UNSET:LX/04g;

    invoke-static {v0, v1, v2}, LX/CDd;->a(LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04g;)V

    .line 1859545
    :cond_4
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->d:LX/2oL;

    iget-object v1, p2, LX/7Jv;->i:LX/7DJ;

    .line 1859546
    iput-object v1, v0, LX/2oL;->i:LX/7DJ;

    .line 1859547
    iget-object v0, p0, LX/CDX;->g:LX/3J0;

    if-eqz v0, :cond_5

    .line 1859548
    iget-object v0, p0, LX/CDX;->g:LX/3J0;

    invoke-interface {v0, p2}, LX/3J0;->a(LX/7Jv;)V

    .line 1859549
    :cond_5
    iget-object v0, p0, LX/CDX;->i:LX/1YQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v10}, LX/1YQ;->a(LX/2oO;Z)V

    .line 1859550
    iget-object v0, p0, LX/CDX;->d:LX/1Yd;

    invoke-virtual {v0}, LX/1Yd;->a()V

    goto :goto_0

    .line 1859551
    :cond_6
    iget-boolean v0, p2, LX/7Jv;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/CDX;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859552
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1859553
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1859554
    iget-object v0, p0, LX/CDX;->a:LX/CDg;

    iget-object v0, v0, LX/CDg;->d:LX/2oL;

    const/4 v1, 0x1

    .line 1859555
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 1859556
    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/3J0;)V
    .locals 1

    .prologue
    .line 1859557
    const-string v0, "listener already set"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859558
    iput-object p1, p0, LX/CDX;->g:LX/3J0;

    .line 1859559
    return-void
.end method
