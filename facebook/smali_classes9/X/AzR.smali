.class public LX/AzR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/1RN;

.field private final b:LX/BMP;

.field private final c:LX/B5l;


# direct methods
.method public constructor <init>(LX/1RN;LX/BMP;LX/B5l;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732528
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    iput-object v0, p0, LX/AzR;->a:LX/1RN;

    .line 1732529
    iput-object p2, p0, LX/AzR;->b:LX/BMP;

    .line 1732530
    iput-object p3, p0, LX/AzR;->c:LX/B5l;

    .line 1732531
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x24f12b38

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732532
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732533
    iget-object v1, p0, LX/AzR;->b:LX/BMP;

    iget-object v2, p0, LX/AzR;->a:LX/1RN;

    iget-object v3, p0, LX/AzR;->c:LX/B5l;

    iget-object v4, p0, LX/AzR;->a:LX/1RN;

    invoke-virtual {v3, v4}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v3

    invoke-virtual {v3}, LX/B5n;->a()LX/B5p;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1732534
    const v1, -0x230767cd

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
