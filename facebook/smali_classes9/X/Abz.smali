.class public final enum LX/Abz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Abz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Abz;

.field public static final enum BROADCASTING:LX/Abz;

.field public static final enum QUIET_MODE:LX/Abz;

.field public static final enum WATCHING_LIVE:LX/Abz;

.field public static final enum WATCHING_VOD:LX/Abz;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1691961
    new-instance v0, LX/Abz;

    const-string v1, "BROADCASTING"

    invoke-direct {v0, v1, v2}, LX/Abz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abz;->BROADCASTING:LX/Abz;

    .line 1691962
    new-instance v0, LX/Abz;

    const-string v1, "WATCHING_LIVE"

    invoke-direct {v0, v1, v3}, LX/Abz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abz;->WATCHING_LIVE:LX/Abz;

    .line 1691963
    new-instance v0, LX/Abz;

    const-string v1, "WATCHING_VOD"

    invoke-direct {v0, v1, v4}, LX/Abz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abz;->WATCHING_VOD:LX/Abz;

    .line 1691964
    new-instance v0, LX/Abz;

    const-string v1, "QUIET_MODE"

    invoke-direct {v0, v1, v5}, LX/Abz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abz;->QUIET_MODE:LX/Abz;

    .line 1691965
    const/4 v0, 0x4

    new-array v0, v0, [LX/Abz;

    sget-object v1, LX/Abz;->BROADCASTING:LX/Abz;

    aput-object v1, v0, v2

    sget-object v1, LX/Abz;->WATCHING_LIVE:LX/Abz;

    aput-object v1, v0, v3

    sget-object v1, LX/Abz;->WATCHING_VOD:LX/Abz;

    aput-object v1, v0, v4

    sget-object v1, LX/Abz;->QUIET_MODE:LX/Abz;

    aput-object v1, v0, v5

    sput-object v0, LX/Abz;->$VALUES:[LX/Abz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1691966
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Abz;
    .locals 1

    .prologue
    .line 1691967
    const-class v0, LX/Abz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Abz;

    return-object v0
.end method

.method public static values()[LX/Abz;
    .locals 1

    .prologue
    .line 1691968
    sget-object v0, LX/Abz;->$VALUES:[LX/Abz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Abz;

    return-object v0
.end method
