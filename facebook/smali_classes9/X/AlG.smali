.class public LX/AlG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/ViewGroup;

.field private b:LX/0g2;

.field private c:LX/2t9;

.field public d:Landroid/view/View;

.field public final e:[I

.field public final f:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/2t9;LX/0g2;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # LX/2t9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0g2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709575
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/AlG;->e:[I

    .line 1709576
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/AlG;->f:Landroid/graphics/Rect;

    .line 1709577
    iput-object p1, p0, LX/AlG;->c:LX/2t9;

    .line 1709578
    iput-object p3, p0, LX/AlG;->a:Landroid/view/ViewGroup;

    .line 1709579
    iput-object p2, p0, LX/AlG;->b:LX/0g2;

    .line 1709580
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/AlG;I)V
    .locals 3

    .prologue
    .line 1709581
    iget-object v0, p0, LX/AlG;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1709582
    const/4 v2, -0x2

    .line 1709583
    new-instance v0, LX/Am8;

    iget-object v1, p0, LX/AlG;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Am8;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AlG;->d:Landroid/view/View;

    .line 1709584
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1709585
    const/16 v1, 0x51

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1709586
    iget-object v1, p0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1709587
    iget-object v0, p0, LX/AlG;->d:Landroid/view/View;

    new-instance v1, LX/AlF;

    invoke-direct {v1, p0}, LX/AlF;-><init>(LX/AlG;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1709588
    iget-object v0, p0, LX/AlG;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1709589
    invoke-virtual {p0}, LX/AlG;->b()V

    .line 1709590
    :cond_0
    iget-object v0, p0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1709591
    return-void
.end method

.method public static e(LX/AlG;)V
    .locals 3

    .prologue
    .line 1709592
    invoke-virtual {p0}, LX/AlG;->b()V

    .line 1709593
    iget-object v0, p0, LX/AlG;->b:LX/0g2;

    iget-object v1, p0, LX/AlG;->c:LX/2t9;

    invoke-virtual {v1}, LX/2t9;->a()I

    move-result v1

    .line 1709594
    iget-object v2, v0, LX/0g2;->ae:LX/1UQ;

    invoke-interface {v2, v1}, LX/1Qr;->m_(I)I

    move-result v2

    .line 1709595
    iget-object p0, v0, LX/0g2;->ae:LX/1UQ;

    invoke-interface {p0, v1}, LX/1Qr;->n_(I)I

    move-result p0

    .line 1709596
    sub-int/2addr p0, v2

    div-int/lit8 p0, p0, 0x2

    add-int/2addr v2, p0

    .line 1709597
    iget-object p0, v0, LX/0g2;->af:LX/0g5;

    invoke-virtual {p0, v2}, LX/0g7;->d(I)V

    .line 1709598
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1709599
    const/16 v0, 0x8

    invoke-static {p0, v0}, LX/AlG;->a(LX/AlG;I)V

    .line 1709600
    return-void
.end method
