.class public final enum LX/AZk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AZk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AZk;

.field public static final enum DOWNLOADED:LX/AZk;

.field public static final enum DOWNLOADING:LX/AZk;

.field public static final enum NOT_DOWNLOADED:LX/AZk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1687393
    new-instance v0, LX/AZk;

    const-string v1, "NOT_DOWNLOADED"

    invoke-direct {v0, v1, v2}, LX/AZk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZk;->NOT_DOWNLOADED:LX/AZk;

    .line 1687394
    new-instance v0, LX/AZk;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, LX/AZk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZk;->DOWNLOADING:LX/AZk;

    .line 1687395
    new-instance v0, LX/AZk;

    const-string v1, "DOWNLOADED"

    invoke-direct {v0, v1, v4}, LX/AZk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZk;->DOWNLOADED:LX/AZk;

    .line 1687396
    const/4 v0, 0x3

    new-array v0, v0, [LX/AZk;

    sget-object v1, LX/AZk;->NOT_DOWNLOADED:LX/AZk;

    aput-object v1, v0, v2

    sget-object v1, LX/AZk;->DOWNLOADING:LX/AZk;

    aput-object v1, v0, v3

    sget-object v1, LX/AZk;->DOWNLOADED:LX/AZk;

    aput-object v1, v0, v4

    sput-object v0, LX/AZk;->$VALUES:[LX/AZk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1687397
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AZk;
    .locals 1

    .prologue
    .line 1687398
    const-class v0, LX/AZk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AZk;

    return-object v0
.end method

.method public static values()[LX/AZk;
    .locals 1

    .prologue
    .line 1687399
    sget-object v0, LX/AZk;->$VALUES:[LX/AZk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AZk;

    return-object v0
.end method
