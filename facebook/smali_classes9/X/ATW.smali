.class public final LX/ATW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9el;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 1

    .prologue
    .line 1676165
    iput-object p1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1676166
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ATW;->b:Z

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 1676143
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ATW;->b:Z

    .line 1676144
    iget-object v0, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1676145
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v1

    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    rem-int/lit16 v1, v1, 0x168

    .line 1676146
    iget-object v2, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, LX/9bz;->e(Ljava/lang/String;I)V

    .line 1676147
    iget-object v0, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0, v1, p1}, LX/8GX;->a(Lcom/facebook/photos/base/media/PhotoItem;I)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1676148
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676149
    iget-object v0, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1, v3}, LX/ATL;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Z)V

    .line 1676150
    iput-boolean v3, p0, LX/ATW;->b:Z

    .line 1676151
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;Z)V
    .locals 4

    .prologue
    .line 1676152
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676153
    iget-object v0, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1676154
    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget v2, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->a:I

    invoke-interface {v1, v0, v2}, LX/9bz;->a(Ljava/lang/String;I)V

    .line 1676155
    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget v2, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->b:I

    invoke-interface {v1, v0, v2}, LX/9bz;->b(Ljava/lang/String;I)V

    .line 1676156
    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget v2, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->d:I

    invoke-interface {v1, v0, v2}, LX/9bz;->c(Ljava/lang/String;I)V

    .line 1676157
    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget v2, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;->e:I

    invoke-interface {v1, v0, v2}, LX/9bz;->d(Ljava/lang/String;I)V

    .line 1676158
    iget-boolean v0, p0, LX/ATW;->b:Z

    if-nez v0, :cond_0

    .line 1676159
    iget-object v0, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v1, p0, LX/ATW;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676160
    iget-object v2, v0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1676161
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1676162
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/ATW;->b:Z

    .line 1676163
    :cond_0
    return-void

    .line 1676164
    :cond_1
    iget-object v3, v0, LX/ATL;->a:LX/ATO;

    iget-object v3, v3, LX/ATO;->B:LX/ASe;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-interface {v3, v2, v1, p1, p2}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    goto :goto_0
.end method
