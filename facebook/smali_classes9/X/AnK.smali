.class public final LX/AnK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPage;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

.field public final synthetic d:LX/25E;

.field public final synthetic e:LX/AnC;

.field public final synthetic f:LX/AnL;


# direct methods
.method public constructor <init>(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;LX/AnC;)V
    .locals 0

    .prologue
    .line 1712331
    iput-object p1, p0, LX/AnK;->f:LX/AnL;

    iput-object p2, p0, LX/AnK;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p3, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object p4, p0, LX/AnK;->c:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    iput-object p5, p0, LX/AnK;->d:LX/25E;

    iput-object p6, p0, LX/AnK;->e:LX/AnC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v11, 0x2

    const v0, -0xf966a02

    invoke-static {v11, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 1712332
    iget-object v0, p0, LX/AnK;->f:LX/AnL;

    iget-object v1, p0, LX/AnK;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-static {v0, v1, v2, v4}, LX/AnL;->a$redex0(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;Lcom/facebook/graphql/model/GraphQLPage;Z)V

    .line 1712333
    iget-object v0, p0, LX/AnK;->f:LX/AnL;

    iget-object v0, v0, LX/AnL;->f:LX/961;

    iget-object v1, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v2

    const-string v4, "native_newsfeed"

    iget-object v5, p0, LX/AnK;->c:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-static {v5}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, LX/AnK;->d:LX/25E;

    iget-object v7, p0, LX/AnK;->c:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-static {v5, v7}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, LX/AnJ;

    invoke-direct {v9, p0}, LX/AnJ;-><init>(LX/AnK;)V

    move-object v5, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 1712334
    iget-object v1, p0, LX/AnK;->c:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    iget-object v2, p0, LX/AnK;->d:LX/25E;

    iget-object v3, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v3

    .line 1712335
    invoke-interface {v2}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v2, v1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    invoke-static {v4, v0, v3}, LX/17Q;->a(ZLX/0lF;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object v4, v4

    .line 1712336
    move-object v0, v4

    .line 1712337
    iget-object v1, p0, LX/AnK;->f:LX/AnL;

    iget-object v1, v1, LX/AnL;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1712338
    iget-object v0, p0, LX/AnK;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712339
    new-instance v0, Lcom/facebook/feed/ui/controllers/PymlPageLikeButtonController$1$2;

    invoke-direct {v0, p0}, Lcom/facebook/feed/ui/controllers/PymlPageLikeButtonController$1$2;-><init>(LX/AnK;)V

    .line 1712340
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 1712341
    new-instance v2, Lcom/facebook/feed/ui/controllers/PymlPageLikeButtonController$1$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feed/ui/controllers/PymlPageLikeButtonController$1$3;-><init>(LX/AnK;Ljava/lang/Runnable;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1712342
    :cond_0
    const v0, -0x43965c1a

    invoke-static {v11, v11, v0, v10}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
