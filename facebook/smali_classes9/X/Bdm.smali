.class public final LX/Bdm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bdq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/BcO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcO",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/Bdb;

.field public c:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public d:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public e:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public f:LX/1OX;

.field public g:LX/BcG;

.field public h:Z

.field public i:I

.field public j:LX/3x6;

.field public k:LX/5K7;

.field public l:LX/1Of;

.field public m:Z

.field public n:LX/BcL;

.field public o:Z

.field public p:LX/Bdv;

.field public q:LX/BcY;

.field public r:Z

.field public s:LX/25S;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1804061
    invoke-static {}, LX/Bdq;->q()LX/Bdq;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1804062
    const/4 v0, 0x0

    iput v0, p0, LX/Bdm;->i:I

    .line 1804063
    sget-object v0, LX/Bdu;->a:LX/1Of;

    iput-object v0, p0, LX/Bdm;->l:LX/1Of;

    .line 1804064
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1804065
    const-string v0, "RecyclerCollectionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1804066
    if-ne p0, p1, :cond_1

    .line 1804067
    :cond_0
    :goto_0
    return v0

    .line 1804068
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1804069
    goto :goto_0

    .line 1804070
    :cond_3
    check-cast p1, LX/Bdm;

    .line 1804071
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1804072
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1804073
    if-eq v2, v3, :cond_0

    .line 1804074
    iget-object v2, p0, LX/Bdm;->a:LX/BcO;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bdm;->a:LX/BcO;

    iget-object v3, p1, LX/Bdm;->a:LX/BcO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1804075
    goto :goto_0

    .line 1804076
    :cond_5
    iget-object v2, p1, LX/Bdm;->a:LX/BcO;

    if-nez v2, :cond_4

    .line 1804077
    :cond_6
    iget-object v2, p0, LX/Bdm;->b:LX/Bdb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bdm;->b:LX/Bdb;

    iget-object v3, p1, LX/Bdm;->b:LX/Bdb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1804078
    goto :goto_0

    .line 1804079
    :cond_8
    iget-object v2, p1, LX/Bdm;->b:LX/Bdb;

    if-nez v2, :cond_7

    .line 1804080
    :cond_9
    iget-object v2, p0, LX/Bdm;->c:LX/1X1;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Bdm;->c:LX/1X1;

    iget-object v3, p1, LX/Bdm;->c:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1804081
    goto :goto_0

    .line 1804082
    :cond_b
    iget-object v2, p1, LX/Bdm;->c:LX/1X1;

    if-nez v2, :cond_a

    .line 1804083
    :cond_c
    iget-object v2, p0, LX/Bdm;->d:LX/1X1;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Bdm;->d:LX/1X1;

    iget-object v3, p1, LX/Bdm;->d:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1804084
    goto :goto_0

    .line 1804085
    :cond_e
    iget-object v2, p1, LX/Bdm;->d:LX/1X1;

    if-nez v2, :cond_d

    .line 1804086
    :cond_f
    iget-object v2, p0, LX/Bdm;->e:LX/1X1;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/Bdm;->e:LX/1X1;

    iget-object v3, p1, LX/Bdm;->e:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1804087
    goto :goto_0

    .line 1804088
    :cond_11
    iget-object v2, p1, LX/Bdm;->e:LX/1X1;

    if-nez v2, :cond_10

    .line 1804089
    :cond_12
    iget-object v2, p0, LX/Bdm;->f:LX/1OX;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/Bdm;->f:LX/1OX;

    iget-object v3, p1, LX/Bdm;->f:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1804090
    goto/16 :goto_0

    .line 1804091
    :cond_14
    iget-object v2, p1, LX/Bdm;->f:LX/1OX;

    if-nez v2, :cond_13

    .line 1804092
    :cond_15
    iget-object v2, p0, LX/Bdm;->g:LX/BcG;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/Bdm;->g:LX/BcG;

    iget-object v3, p1, LX/Bdm;->g:LX/BcG;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 1804093
    goto/16 :goto_0

    .line 1804094
    :cond_17
    iget-object v2, p1, LX/Bdm;->g:LX/BcG;

    if-nez v2, :cond_16

    .line 1804095
    :cond_18
    iget-boolean v2, p0, LX/Bdm;->h:Z

    iget-boolean v3, p1, LX/Bdm;->h:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 1804096
    goto/16 :goto_0

    .line 1804097
    :cond_19
    iget v2, p0, LX/Bdm;->i:I

    iget v3, p1, LX/Bdm;->i:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 1804098
    goto/16 :goto_0

    .line 1804099
    :cond_1a
    iget-object v2, p0, LX/Bdm;->j:LX/3x6;

    if-eqz v2, :cond_1c

    iget-object v2, p0, LX/Bdm;->j:LX/3x6;

    iget-object v3, p1, LX/Bdm;->j:LX/3x6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 1804100
    goto/16 :goto_0

    .line 1804101
    :cond_1c
    iget-object v2, p1, LX/Bdm;->j:LX/3x6;

    if-nez v2, :cond_1b

    .line 1804102
    :cond_1d
    iget-object v2, p0, LX/Bdm;->k:LX/5K7;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/Bdm;->k:LX/5K7;

    iget-object v3, p1, LX/Bdm;->k:LX/5K7;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 1804103
    goto/16 :goto_0

    .line 1804104
    :cond_1f
    iget-object v2, p1, LX/Bdm;->k:LX/5K7;

    if-nez v2, :cond_1e

    .line 1804105
    :cond_20
    iget-object v2, p0, LX/Bdm;->l:LX/1Of;

    if-eqz v2, :cond_22

    iget-object v2, p0, LX/Bdm;->l:LX/1Of;

    iget-object v3, p1, LX/Bdm;->l:LX/1Of;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    :cond_21
    move v0, v1

    .line 1804106
    goto/16 :goto_0

    .line 1804107
    :cond_22
    iget-object v2, p1, LX/Bdm;->l:LX/1Of;

    if-nez v2, :cond_21

    .line 1804108
    :cond_23
    iget-boolean v2, p0, LX/Bdm;->m:Z

    iget-boolean v3, p1, LX/Bdm;->m:Z

    if-eq v2, v3, :cond_24

    move v0, v1

    .line 1804109
    goto/16 :goto_0

    .line 1804110
    :cond_24
    iget-object v2, p0, LX/Bdm;->n:LX/BcL;

    if-eqz v2, :cond_26

    iget-object v2, p0, LX/Bdm;->n:LX/BcL;

    iget-object v3, p1, LX/Bdm;->n:LX/BcL;

    invoke-virtual {v2, v3}, LX/BcL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    :cond_25
    move v0, v1

    .line 1804111
    goto/16 :goto_0

    .line 1804112
    :cond_26
    iget-object v2, p1, LX/Bdm;->n:LX/BcL;

    if-nez v2, :cond_25

    .line 1804113
    :cond_27
    iget-boolean v2, p0, LX/Bdm;->o:Z

    iget-boolean v3, p1, LX/Bdm;->o:Z

    if-eq v2, v3, :cond_28

    move v0, v1

    .line 1804114
    goto/16 :goto_0

    .line 1804115
    :cond_28
    iget-object v2, p0, LX/Bdm;->p:LX/Bdv;

    if-eqz v2, :cond_2a

    iget-object v2, p0, LX/Bdm;->p:LX/Bdv;

    iget-object v3, p1, LX/Bdm;->p:LX/Bdv;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2b

    :cond_29
    move v0, v1

    .line 1804116
    goto/16 :goto_0

    .line 1804117
    :cond_2a
    iget-object v2, p1, LX/Bdm;->p:LX/Bdv;

    if-nez v2, :cond_29

    .line 1804118
    :cond_2b
    iget-object v2, p0, LX/Bdm;->q:LX/BcY;

    if-eqz v2, :cond_2d

    iget-object v2, p0, LX/Bdm;->q:LX/BcY;

    iget-object v3, p1, LX/Bdm;->q:LX/BcY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    :cond_2c
    move v0, v1

    .line 1804119
    goto/16 :goto_0

    .line 1804120
    :cond_2d
    iget-object v2, p1, LX/Bdm;->q:LX/BcY;

    if-nez v2, :cond_2c

    .line 1804121
    :cond_2e
    iget-boolean v2, p0, LX/Bdm;->r:Z

    iget-boolean v3, p1, LX/Bdm;->r:Z

    if-eq v2, v3, :cond_2f

    move v0, v1

    .line 1804122
    goto/16 :goto_0

    .line 1804123
    :cond_2f
    iget-object v2, p0, LX/Bdm;->s:LX/25S;

    if-eqz v2, :cond_30

    iget-object v2, p0, LX/Bdm;->s:LX/25S;

    iget-object v3, p1, LX/Bdm;->s:LX/25S;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1804124
    goto/16 :goto_0

    .line 1804125
    :cond_30
    iget-object v2, p1, LX/Bdm;->s:LX/25S;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 4

    .prologue
    .line 1804126
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1804127
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Bdm;

    .line 1804128
    iget-object v1, v0, LX/Bdm;->c:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Bdm;->c:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Bdm;->c:LX/1X1;

    .line 1804129
    iget-object v1, v0, LX/Bdm;->d:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/Bdm;->d:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_1
    iput-object v1, v0, LX/Bdm;->d:LX/1X1;

    .line 1804130
    iget-object v1, v0, LX/Bdm;->e:LX/1X1;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/Bdm;->e:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_2
    iput-object v1, v0, LX/Bdm;->e:LX/1X1;

    .line 1804131
    iput-object v2, v0, LX/Bdm;->n:LX/BcL;

    .line 1804132
    iput-boolean v3, v0, LX/Bdm;->o:Z

    .line 1804133
    iput-object v2, v0, LX/Bdm;->p:LX/Bdv;

    .line 1804134
    iput-object v2, v0, LX/Bdm;->q:LX/BcY;

    .line 1804135
    iput-boolean v3, v0, LX/Bdm;->r:Z

    .line 1804136
    iput-object v2, v0, LX/Bdm;->s:LX/25S;

    .line 1804137
    return-object v0

    :cond_0
    move-object v1, v2

    .line 1804138
    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 1804139
    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 1804140
    goto :goto_2
.end method
