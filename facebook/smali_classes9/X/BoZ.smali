.class public final LX/BoZ;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 1821816
    iput-object p1, p0, LX/BoZ;->b:LX/1dt;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821817
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821818
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 1821819
    const v1, 0x7f08002c

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1821820
    instance-of v1, v2, LX/3Ai;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 1821821
    check-cast v1, LX/3Ai;

    const v3, 0x7f08289e

    invoke-virtual {v1, v3}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 1821822
    :cond_0
    new-instance v1, LX/BoY;

    invoke-direct {v1, p0, p3, v0}, LX/BoY;-><init>(LX/BoZ;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1821823
    const v1, 0x7f0208ed

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1821824
    invoke-static {v0}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 1821825
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1821826
    const/4 v1, 0x0

    .line 1821827
    :goto_0
    move-object v0, v1

    .line 1821828
    iget-object v1, p0, LX/BoZ;->b:LX/1dt;

    iget-object v1, v1, LX/1dt;->E:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1821829
    return-void

    .line 1821830
    :cond_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "pymi_learn_more"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 1821831
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1821832
    move-object v1, v1

    .line 1821833
    goto :goto_0
.end method
