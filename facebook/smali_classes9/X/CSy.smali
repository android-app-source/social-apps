.class public final enum LX/CSy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CSy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CSy;

.field public static final enum FETCH_FIRST_SCREEN:LX/CSy;

.field public static final enum FETCH_PARTIAL_SCREEN:LX/CSy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1895171
    new-instance v0, LX/CSy;

    const-string v1, "FETCH_FIRST_SCREEN"

    invoke-direct {v0, v1, v2}, LX/CSy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSy;->FETCH_FIRST_SCREEN:LX/CSy;

    .line 1895172
    new-instance v0, LX/CSy;

    const-string v1, "FETCH_PARTIAL_SCREEN"

    invoke-direct {v0, v1, v3}, LX/CSy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSy;->FETCH_PARTIAL_SCREEN:LX/CSy;

    .line 1895173
    const/4 v0, 0x2

    new-array v0, v0, [LX/CSy;

    sget-object v1, LX/CSy;->FETCH_FIRST_SCREEN:LX/CSy;

    aput-object v1, v0, v2

    sget-object v1, LX/CSy;->FETCH_PARTIAL_SCREEN:LX/CSy;

    aput-object v1, v0, v3

    sput-object v0, LX/CSy;->$VALUES:[LX/CSy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1895174
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CSy;
    .locals 1

    .prologue
    .line 1895175
    const-class v0, LX/CSy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CSy;

    return-object v0
.end method

.method public static values()[LX/CSy;
    .locals 1

    .prologue
    .line 1895176
    sget-object v0, LX/CSy;->$VALUES:[LX/CSy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CSy;

    return-object v0
.end method
