.class public final LX/Amd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/Amf;


# direct methods
.method public constructor <init>(LX/Amf;LX/0TF;)V
    .locals 0

    .prologue
    .line 1711236
    iput-object p1, p0, LX/Amd;->b:LX/Amf;

    iput-object p2, p0, LX/Amd;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1711223
    iget-object v0, p0, LX/Amd;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1711224
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1711225
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1711226
    if-eqz p1, :cond_0

    .line 1711227
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1711228
    if-eqz v0, :cond_0

    .line 1711229
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1711230
    check-cast v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;->a()Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1711231
    :cond_0
    iget-object v0, p0, LX/Amd;->a:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1711232
    :goto_0
    return-void

    .line 1711233
    :cond_1
    iget-object v1, p0, LX/Amd;->a:LX/0TF;

    .line 1711234
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1711235
    check-cast v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;->a()Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Amf;->b(LX/0Px;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
