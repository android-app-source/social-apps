.class public final LX/B6X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V
    .locals 0

    .prologue
    .line 1746791
    iput-object p1, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x7d3cc788

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746792
    iget-object v1, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    .line 1746793
    iget v2, v1, LX/B6u;->h:I

    move v1, v2

    .line 1746794
    iget-object v2, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1746795
    iget-object v1, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->i:LX/B7i;

    invoke-interface {v1}, LX/B7i;->b()V

    .line 1746796
    const v1, 0x417eea80

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1746797
    :goto_0
    return-void

    .line 1746798
    :cond_0
    iget-object v2, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->j()V

    .line 1746799
    iget-object v2, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->b()Z

    .line 1746800
    iget-object v2, p0, LX/B6X;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "cta_lead_gen_prev_button_click"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1746801
    const v1, 0x2434f6d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
