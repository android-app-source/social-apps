.class public final LX/Cae;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V
    .locals 0

    .prologue
    .line 1918236
    iput-object p1, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1918237
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 1918238
    :cond_0
    :goto_0
    return-void

    .line 1918239
    :cond_1
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1918240
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->r:LX/7N9;

    invoke-virtual {v0}, LX/7N9;->i()V

    .line 1918241
    if-nez p2, :cond_2

    .line 1918242
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1918243
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1918244
    :cond_2
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918245
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918246
    if-eqz v0, :cond_0

    .line 1918247
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918248
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1918249
    if-nez v0, :cond_3

    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    .line 1918250
    :goto_1
    iget-object v1, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918251
    iget-object v2, v1, LX/CcX;->a:LX/CaP;

    move-object v1, v2

    .line 1918252
    iget-object v2, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->l:Z

    invoke-virtual {v1, v2, v0}, LX/CaP;->a(ZLX/2oi;)V

    goto :goto_0

    .line 1918253
    :cond_3
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918254
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1918255
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1918256
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918257
    :cond_0
    :goto_0
    return-void

    .line 1918258
    :cond_1
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1918259
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1918260
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918261
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918262
    if-eqz v0, :cond_0

    .line 1918263
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918264
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918265
    iget-object v1, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result v1

    .line 1918266
    iget-object p1, v0, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    if-nez v1, :cond_4

    const/4 p0, 0x1

    .line 1918267
    :goto_2
    iput-boolean p0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ae:Z

    .line 1918268
    goto :goto_0

    .line 1918269
    :cond_3
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1918270
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918271
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918272
    if-eqz v0, :cond_2

    .line 1918273
    iget-object v0, p0, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918274
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918275
    invoke-virtual {v0}, LX/CaP;->a()V

    goto :goto_1

    .line 1918276
    :cond_4
    const/4 p0, 0x0

    goto :goto_2
.end method
