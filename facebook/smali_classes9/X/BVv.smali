.class public final enum LX/BVv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVv;

.field public static final enum HEIGHT:LX/BVv;

.field public static final enum WIDTH:LX/BVv;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1791506
    new-instance v0, LX/BVv;

    const-string v1, "WIDTH"

    const-string v2, "width"

    invoke-direct {v0, v1, v3, v2}, LX/BVv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVv;->WIDTH:LX/BVv;

    .line 1791507
    new-instance v0, LX/BVv;

    const-string v1, "HEIGHT"

    const-string v2, "height"

    invoke-direct {v0, v1, v4, v2}, LX/BVv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVv;->HEIGHT:LX/BVv;

    .line 1791508
    const/4 v0, 0x2

    new-array v0, v0, [LX/BVv;

    sget-object v1, LX/BVv;->WIDTH:LX/BVv;

    aput-object v1, v0, v3

    sget-object v1, LX/BVv;->HEIGHT:LX/BVv;

    aput-object v1, v0, v4

    sput-object v0, LX/BVv;->$VALUES:[LX/BVv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791509
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791510
    iput-object p3, p0, LX/BVv;->mValue:Ljava/lang/String;

    .line 1791511
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVv;
    .locals 4

    .prologue
    .line 1791512
    invoke-static {}, LX/BVv;->values()[LX/BVv;

    move-result-object v2

    .line 1791513
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 1791514
    aget-object v1, v2, v0

    .line 1791515
    iget-object v3, v1, LX/BVv;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1791516
    :goto_1
    return-object v0

    .line 1791517
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791518
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVv;
    .locals 1

    .prologue
    .line 1791519
    const-class v0, LX/BVv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVv;

    return-object v0
.end method

.method public static values()[LX/BVv;
    .locals 1

    .prologue
    .line 1791520
    sget-object v0, LX/BVv;->$VALUES:[LX/BVv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVv;

    return-object v0
.end method
