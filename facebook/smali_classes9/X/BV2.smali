.class public LX/BV2;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:[F

.field public final c:[F

.field public final d:[F

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/5Pf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1790155
    const-class v0, LX/BV2;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BV2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;ILX/7Sv;Landroid/graphics/RectF;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;I",
            "LX/7Sv;",
            "Landroid/graphics/RectF;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 1790134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790135
    new-array v0, v2, [F

    iput-object v0, p0, LX/BV2;->b:[F

    .line 1790136
    new-array v0, v2, [F

    iput-object v0, p0, LX/BV2;->c:[F

    .line 1790137
    new-array v0, v2, [F

    iput-object v0, p0, LX/BV2;->d:[F

    .line 1790138
    iput-object p1, p0, LX/BV2;->e:Ljava/util/List;

    .line 1790139
    iget-object v0, p0, LX/BV2;->b:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1790140
    iget-object v0, p0, LX/BV2;->c:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1790141
    iget-object v0, p0, LX/BV2;->d:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1790142
    iget-object v0, p0, LX/BV2;->d:[F

    const/high16 p1, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    const/high16 v4, -0x41000000    # -0.5f

    const/4 v2, 0x0

    .line 1790143
    invoke-static {v0, v2, v1, v1, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1790144
    int-to-float v1, v3

    invoke-static {v0, v2, p1, v1, p1}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1790145
    invoke-static {v0, v2, v4, v4, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1790146
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 1790147
    iget-object v0, p0, LX/BV2;->c:[F

    invoke-static {v0, p2}, LX/61I;->a([FI)V

    .line 1790148
    :cond_0
    sget-object v0, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    if-ne p3, v0, :cond_1

    .line 1790149
    iget-object v0, p0, LX/BV2;->c:[F

    const/high16 p1, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    const/high16 v4, -0x41000000    # -0.5f

    const/4 v2, 0x0

    .line 1790150
    invoke-static {v0, v2, v1, v1, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1790151
    int-to-float v1, v3

    invoke-static {v0, v2, v1, p1, p1}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1790152
    invoke-static {v0, v2, v4, v4, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1790153
    :cond_1
    iget-object v0, p0, LX/BV2;->c:[F

    invoke-static {v0, p4}, LX/61I;->a([FLandroid/graphics/RectF;)V

    .line 1790154
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1790129
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 1790130
    if-eqz v0, :cond_0

    .line 1790131
    sget-object v1, LX/BV2;->a:Ljava/lang/String;

    const-string v2, "%s: glError %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1790132
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1790133
    :cond_0
    return-void
.end method
