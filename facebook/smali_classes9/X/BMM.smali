.class public LX/BMM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasComposerSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasSelectedMemeInfo;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1ay;

.field private final b:LX/1Nq;

.field private final c:LX/Awp;


# direct methods
.method public constructor <init>(LX/1ay;LX/1Nq;LX/Awp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777089
    iput-object p1, p0, LX/BMM;->a:LX/1ay;

    .line 1777090
    iput-object p2, p0, LX/BMM;->b:LX/1Nq;

    .line 1777091
    iput-object p3, p0, LX/BMM;->c:LX/Awp;

    .line 1777092
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1RN;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1777114
    invoke-static {p4}, LX/BMM;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    .line 1777115
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    const-string v2, "memePrompt"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/BMM;->b:LX/1Nq;

    invoke-static {v0, p3, v4}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1777116
    invoke-static {v1, v0}, LX/BMU;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    .line 1777117
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v3

    move-object v0, p1

    move-object v1, p2

    move-object v4, p3

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->a(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;Lcom/facebook/productionprompts/logging/PromptAnalytics;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 2

    .prologue
    .line 1777109
    invoke-static {p0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1777110
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1777111
    check-cast v0, LX/1kW;

    .line 1777112
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1777113
    return-object v0
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 0
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777118
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777094
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Activity;

    move-object v0, p3

    .line 1777095
    check-cast v0, LX/B5p;

    .line 1777096
    iget-object v1, v0, LX/B5p;->j:LX/0am;

    move-object v0, v1

    .line 1777097
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 1777098
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    move-object v2, p3

    check-cast v2, LX/B5p;

    .line 1777099
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1777100
    move-object v3, p3

    check-cast v3, LX/B5p;

    .line 1777101
    iget-object v4, v3, LX/B5p;->j:LX/0am;

    move-object v3, v4

    .line 1777102
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    .line 1777103
    iget-object v8, p0, LX/BMM;->a:LX/1ay;

    check-cast p3, LX/B5p;

    .line 1777104
    iget-object v0, p3, LX/B5p;->g:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-object v5, v0

    .line 1777105
    move-object v0, p0

    move-object v1, v7

    move-object v2, v6

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/BMM;->a(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1RN;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x6dc

    invoke-virtual {v8, v0, v1, v6}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1777106
    iget-object v0, p0, LX/BMM;->c:LX/Awp;

    .line 1777107
    iget-object v1, v0, LX/Awp;->a:LX/0Zb;

    const-string v2, "tap_meme_prompt"

    invoke-static {v2, v7}, LX/Awp;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1777108
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1777093
    invoke-static {p1}, LX/BMM;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
