.class public final enum LX/B9u;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B9u;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B9u;

.field public static final enum CANONICAL_THREAD:LX/B9u;

.field public static final enum CREATE_THREAD:LX/B9u;

.field public static final enum GROUP_THREAD:LX/B9u;

.field public static final enum LEGACY_THREAD_ID:LX/B9u;

.field public static final enum THREAD_LIST:LX/B9u;

.field public static final enum THREAD_LIST_JEWEL:LX/B9u;


# instance fields
.field private final mTrigger:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1752327
    new-instance v0, LX/B9u;

    const-string v1, "THREAD_LIST"

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v5, v2}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->THREAD_LIST:LX/B9u;

    .line 1752328
    new-instance v0, LX/B9u;

    const-string v1, "CANONICAL_THREAD"

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CANONICAL_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v6, v2}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->CANONICAL_THREAD:LX/B9u;

    .line 1752329
    new-instance v0, LX/B9u;

    const-string v1, "GROUP_THREAD"

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_GROUP_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v7, v2}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->GROUP_THREAD:LX/B9u;

    .line 1752330
    new-instance v0, LX/B9u;

    const-string v1, "LEGACY_THREAD_ID"

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CANONICAL_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v8, v2}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->LEGACY_THREAD_ID:LX/B9u;

    .line 1752331
    new-instance v0, LX/B9u;

    const-string v1, "THREAD_LIST_JEWEL"

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v9, v2}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->THREAD_LIST_JEWEL:LX/B9u;

    .line 1752332
    new-instance v0, LX/B9u;

    const-string v1, "CREATE_THREAD"

    const/4 v2, 0x5

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MESSAGES_DIODE_CREATE_THREAD:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-direct {v0, v1, v2, v3}, LX/B9u;-><init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V

    sput-object v0, LX/B9u;->CREATE_THREAD:LX/B9u;

    .line 1752333
    const/4 v0, 0x6

    new-array v0, v0, [LX/B9u;

    sget-object v1, LX/B9u;->THREAD_LIST:LX/B9u;

    aput-object v1, v0, v5

    sget-object v1, LX/B9u;->CANONICAL_THREAD:LX/B9u;

    aput-object v1, v0, v6

    sget-object v1, LX/B9u;->GROUP_THREAD:LX/B9u;

    aput-object v1, v0, v7

    sget-object v1, LX/B9u;->LEGACY_THREAD_ID:LX/B9u;

    aput-object v1, v0, v8

    sget-object v1, LX/B9u;->THREAD_LIST_JEWEL:LX/B9u;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/B9u;->CREATE_THREAD:LX/B9u;

    aput-object v2, v0, v1

    sput-object v0, LX/B9u;->$VALUES:[LX/B9u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1752334
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1752335
    iput-object p3, p0, LX/B9u;->mTrigger:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1752336
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B9u;
    .locals 1

    .prologue
    .line 1752337
    const-class v0, LX/B9u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B9u;

    return-object v0
.end method

.method public static values()[LX/B9u;
    .locals 1

    .prologue
    .line 1752338
    sget-object v0, LX/B9u;->$VALUES:[LX/B9u;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B9u;

    return-object v0
.end method


# virtual methods
.method public final getTriggerForDestination()Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 1

    .prologue
    .line 1752339
    iget-object v0, p0, LX/B9u;->mTrigger:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-object v0
.end method
