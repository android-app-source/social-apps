.class public LX/BiM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Landroid/content/Context;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:LX/BNF;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public i:Z

.field private final j:LX/BiJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/BNF;LX/0Ot;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/LayoutInflater;",
            "LX/BNF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810208
    new-instance v0, LX/BiK;

    invoke-direct {v0, p0}, LX/BiK;-><init>(LX/BiM;)V

    iput-object v0, p0, LX/BiM;->j:LX/BiJ;

    .line 1810209
    iput-object p1, p0, LX/BiM;->c:Landroid/content/Context;

    .line 1810210
    iput-object p2, p0, LX/BiM;->d:Landroid/view/LayoutInflater;

    .line 1810211
    iput-object p3, p0, LX/BiM;->e:LX/BNF;

    .line 1810212
    iput-object p4, p0, LX/BiM;->f:LX/0Ot;

    .line 1810213
    iput-object p5, p0, LX/BiM;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1810214
    iput-object p6, p0, LX/BiM;->h:Lcom/facebook/content/SecureContextHelper;

    .line 1810215
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/BiM;->a:I

    .line 1810216
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/BiM;->b:I

    .line 1810217
    return-void
.end method

.method private static a(LX/BiM;LX/Bi6;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1810162
    iget-object v0, p0, LX/BiM;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030149

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1810163
    const v0, 0x7f0d061a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1810164
    iget-object v2, p0, LX/BiM;->e:LX/BNF;

    .line 1810165
    iget-object v3, p1, LX/Bi6;->b:Lcom/facebook/graphql/model/GraphQLRating;

    move-object v3, v3

    .line 1810166
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/BNF;->a(D)Ljava/lang/String;

    move-result-object v2

    .line 1810167
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1810168
    return-object v1
.end method

.method private a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;Landroid/content/res/Resources;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1810169
    const/4 v1, 0x0

    .line 1810170
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->RATINGS_AND_REVIEWS:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-eq v0, v3, :cond_3

    move v0, v1

    .line 1810171
    :goto_0
    move v0, v0

    .line 1810172
    if-eqz v0, :cond_1

    .line 1810173
    invoke-static {p0, p3}, LX/BiM;->a(LX/BiM;LX/Bi6;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Landroid/view/View;)V

    .line 1810174
    :cond_0
    :goto_1
    return-void

    .line 1810175
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1810176
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;->a()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1810177
    const v0, 0x7f0b0d15

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const v1, 0x7f0b00b2

    invoke-virtual {p4, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 1810178
    invoke-virtual {p1, v0, v1, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setPadding(IIII)V

    .line 1810179
    const v0, 0x7f0b0d14

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;->a()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$IconModel;->b()Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v5

    const-string v6, "entity_cards"

    move-object v0, p1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;Ljava/lang/String;)V

    goto :goto_1

    .line 1810180
    :cond_2
    iget-object v0, p0, LX/BiM;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Icon with no IconImage, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1810181
    :cond_3
    iget-object v0, p3, LX/Bi6;->b:Lcom/facebook/graphql/model/GraphQLRating;

    move-object v0, v0

    .line 1810182
    if-nez v0, :cond_4

    .line 1810183
    iget-object v0, p0, LX/BiM;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Review context without average rating for entity "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1810184
    iget-object v5, p3, LX/Bi6;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1810185
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1810186
    goto/16 :goto_0

    .line 1810187
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/BiM;
    .locals 7

    .prologue
    .line 1810188
    new-instance v0, LX/BiM;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/BNF;->a(LX/0QB;)LX/BNF;

    move-result-object v3

    check-cast v3, LX/BNF;

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v0 .. v6}, LX/BiM;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/BNF;LX/0Ot;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V

    .line 1810189
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1810190
    if-nez p2, :cond_0

    .line 1810191
    :goto_0
    return-void

    .line 1810192
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1810193
    invoke-direct {p0, p1, p2, p3, v2}, LX/BiM;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;Landroid/content/res/Resources;)V

    .line 1810194
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->j()LX/175;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1810195
    sget-object v0, LX/BiL;->a:[I

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1810196
    iget-object v0, p0, LX/BiM;->c:Landroid/content/Context;

    invoke-static {p2, v0}, LX/BiH;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    iget v3, p0, LX/BiM;->a:I

    invoke-virtual {p1, v0, v6, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 1810197
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->al_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->al_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget v3, p0, LX/BiM;->b:I

    invoke-virtual {p1, v0, v6, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 1810198
    const v0, 0x7f0b00b2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c(I)V

    .line 1810199
    sget-object v0, LX/BiL;->a:[I

    invoke-virtual {p2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 1810200
    :goto_3
    :pswitch_0
    invoke-virtual {p1, v5, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    goto :goto_0

    .line 1810201
    :pswitch_1
    iget-object v0, p0, LX/BiM;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a00e7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v3, p0, LX/BiM;->c:Landroid/content/Context;

    invoke-static {p2, v3}, LX/BiH;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    iget v4, p0, LX/BiM;->a:I

    invoke-virtual {p1, v0, v3, v6, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ILjava/lang/CharSequence;II)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1810202
    goto :goto_2

    .line 1810203
    :pswitch_2
    iput-boolean v5, p0, LX/BiM;->i:Z

    .line 1810204
    iget-object v0, p0, LX/BiM;->j:LX/BiJ;

    invoke-virtual {p1, v5, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLX/BiJ;)V

    goto :goto_3

    .line 1810205
    :pswitch_3
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BiM;->i:Z

    .line 1810206
    iget-object v0, p0, LX/BiM;->j:LX/BiJ;

    invoke-virtual {p1, v5, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLX/BiJ;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
