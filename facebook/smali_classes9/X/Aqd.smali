.class public final LX/Aqd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;I)V
    .locals 0

    .prologue
    .line 1718136
    iput-object p1, p0, LX/Aqd;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    iput p2, p0, LX/Aqd;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7a98809c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1718137
    iget-object v0, p0, LX/Aqd;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    iget-object v2, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->f:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, p0, LX/Aqd;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->c:LX/0Px;

    iget v3, p0, LX/Aqd;->a:I

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/gif/model/GifModelContainer;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/gif/model/GifModelContainer;->a()Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;->a()Lcom/facebook/friendsharing/gif/model/GifModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/gif/model/GifModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1718138
    iget-object v3, v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->g:LX/1Kf;

    iget-object v5, v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    iget-object v6, v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->j:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    .line 1718139
    const-string v7, "giphy.gif"

    .line 1718140
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 p0, 0x0

    const/16 p1, 0x2f

    invoke-virtual {v0, p1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v7, v7

    .line 1718141
    invoke-static {v7}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v7

    invoke-virtual {v7}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    const/16 v7, 0x4d8

    invoke-interface {v3, v5, v6, v7, v2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1718142
    iget-object v3, v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v5, v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718143
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "gif_picker_launch_composer"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "session_id"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 1718144
    iget-object v7, v3, LX/Aqg;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1718145
    const v0, -0x7c51891

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
