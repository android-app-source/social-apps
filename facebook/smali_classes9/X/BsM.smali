.class public final LX/BsM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/2pa;

.field public d:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic e:LX/1xZ;


# direct methods
.method public constructor <init>(LX/1xZ;)V
    .locals 1

    .prologue
    .line 1827408
    iput-object p1, p0, LX/BsM;->e:LX/1xZ;

    .line 1827409
    move-object v0, p1

    .line 1827410
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1827411
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1827412
    const-string v0, "ActorProfileVideoComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1xZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1827413
    check-cast p1, LX/BsM;

    .line 1827414
    iget-object v0, p1, LX/BsM;->c:LX/2pa;

    iput-object v0, p0, LX/BsM;->c:LX/2pa;

    .line 1827415
    iget-object v0, p1, LX/BsM;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object v0, p0, LX/BsM;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1827416
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1827417
    if-ne p0, p1, :cond_1

    .line 1827418
    :cond_0
    :goto_0
    return v0

    .line 1827419
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1827420
    goto :goto_0

    .line 1827421
    :cond_3
    check-cast p1, LX/BsM;

    .line 1827422
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1827423
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1827424
    if-eq v2, v3, :cond_0

    .line 1827425
    iget-object v2, p0, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1827426
    goto :goto_0

    .line 1827427
    :cond_5
    iget-object v2, p1, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1827428
    :cond_6
    iget-object v2, p0, LX/BsM;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/BsM;->b:LX/1Po;

    iget-object v3, p1, LX/BsM;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1827429
    goto :goto_0

    .line 1827430
    :cond_7
    iget-object v2, p1, LX/BsM;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1827431
    const/4 v1, 0x0

    .line 1827432
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/BsM;

    .line 1827433
    iput-object v1, v0, LX/BsM;->c:LX/2pa;

    .line 1827434
    iput-object v1, v0, LX/BsM;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1827435
    return-object v0
.end method
