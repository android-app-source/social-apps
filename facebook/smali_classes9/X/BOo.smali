.class public LX/BOo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field private static final d:Ljava/lang/String;

.field private static volatile k:LX/BOo;


# instance fields
.field private final e:LX/0hB;

.field public final f:LX/0ad;

.field private final g:LX/455;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private j:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1780411
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "storyteller/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1780412
    sput-object v0, LX/BOo;->a:LX/0Tn;

    const-string v1, "photo_pixel_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BOo;->b:LX/0Tn;

    .line 1780413
    sget-object v0, LX/BOo;->a:LX/0Tn;

    const-string v1, "video_pixel_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BOo;->c:LX/0Tn;

    .line 1780414
    const-class v0, LX/BOo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BOo;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0hB;LX/0ad;LX/455;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hB;",
            "LX/0ad;",
            "LX/455;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780324
    iput-object p1, p0, LX/BOo;->e:LX/0hB;

    .line 1780325
    iput-object p2, p0, LX/BOo;->f:LX/0ad;

    .line 1780326
    iput-object p3, p0, LX/BOo;->g:LX/455;

    .line 1780327
    iput-object p4, p0, LX/BOo;->h:LX/0Or;

    .line 1780328
    iput-object p5, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1780329
    return-void
.end method

.method public static a(LX/0QB;)LX/BOo;
    .locals 9

    .prologue
    .line 1780395
    sget-object v0, LX/BOo;->k:LX/BOo;

    if-nez v0, :cond_1

    .line 1780396
    const-class v1, LX/BOo;

    monitor-enter v1

    .line 1780397
    :try_start_0
    sget-object v0, LX/BOo;->k:LX/BOo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1780398
    if-eqz v2, :cond_0

    .line 1780399
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1780400
    new-instance v3, LX/BOo;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 1780401
    new-instance v7, LX/455;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PackageManager;

    invoke-direct {v7, v6}, LX/455;-><init>(Landroid/content/pm/PackageManager;)V

    .line 1780402
    move-object v6, v7

    .line 1780403
    check-cast v6, LX/455;

    const/16 v7, 0x2fd

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v8}, LX/BOo;-><init>(LX/0hB;LX/0ad;LX/455;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1780404
    move-object v0, v3

    .line 1780405
    sput-object v0, LX/BOo;->k:LX/BOo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1780407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1780408
    :cond_1
    sget-object v0, LX/BOo;->k:LX/BOo;

    return-object v0

    .line 1780409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1780410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([B)V
    .locals 7

    .prologue
    .line 1780415
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1780416
    :goto_0
    return-void

    .line 1780417
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/BOu;->a(Ljava/nio/ByteBuffer;)LX/BOu;

    move-result-object v0

    .line 1780418
    new-instance v1, LX/BOt;

    invoke-direct {v1}, LX/BOt;-><init>()V

    .line 1780419
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_7

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v2}, LX/0eW;->b(I)I

    move-result v2

    iget-object v3, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1780420
    iput v2, v1, LX/BOt;->a:I

    iput-object v3, v1, LX/BOt;->b:Ljava/nio/ByteBuffer;

    move-object v2, v1

    .line 1780421
    :goto_1
    move-object v1, v2

    .line 1780422
    move-object v1, v1

    .line 1780423
    new-instance v2, LX/BOx;

    invoke-direct {v2}, LX/BOx;-><init>()V

    .line 1780424
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_8

    iget v4, v0, LX/0eW;->a:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1780425
    iput v3, v2, LX/BOx;->a:I

    iput-object v4, v2, LX/BOx;->b:Ljava/nio/ByteBuffer;

    move-object v3, v2

    .line 1780426
    :goto_2
    move-object v2, v3

    .line 1780427
    move-object v2, v2

    .line 1780428
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Storyteller configuration: \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1780429
    const-string v4, "  cluster configuration: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780430
    const-string v4, "\t hourTimeThresholdIfNoKnownDistance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1780431
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_9

    iget-object v6, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    :goto_3
    move v5, v5

    .line 1780432
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780433
    const-string v4, "\t kmDistanceThresholdForCloseDistanceCheck: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, LX/BOt;->b()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780434
    const-string v4, "\t hourTimeThresholdIfPassesCloseDistanceCheck: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1780435
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_a

    iget-object v6, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    :goto_4
    move v5, v5

    .line 1780436
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780437
    const-string v4, "\t hourTimeThresholdForCloseTimeCheck: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1780438
    const/16 v5, 0xa

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_b

    iget-object v6, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    :goto_5
    move v5, v5

    .line 1780439
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780440
    const-string v4, "\t kmDistanceThresholdIfPassesCloseTimeCheck: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, LX/BOt;->b()F

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780441
    const-string v1, "  process configuration:  \n"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780442
    const-string v1, "\t minRecencyInDays: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780443
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_c

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_6
    move v4, v4

    .line 1780444
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780445
    const-string v1, "\t minAssetCount: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780446
    const/4 v4, 0x6

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_d

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_7
    move v4, v4

    .line 1780447
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780448
    const-string v1, "\t maxAssetCount: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780449
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_e

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_8
    move v4, v4

    .line 1780450
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780451
    const-string v1, "\t photoPixelCount: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780452
    const/16 v4, 0xa

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_f

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_9
    move v4, v4

    .line 1780453
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780454
    const-string v1, "\t faceDetectionEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x0

    .line 1780455
    const/16 v5, 0xc

    invoke-virtual {v2, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_1

    iget-object v6, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-eqz v5, :cond_1

    const/4 v4, 0x1

    :cond_1
    move v4, v4

    .line 1780456
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780457
    const-string v1, "\t blurryPhotoRemovalEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x0

    .line 1780458
    const/16 v5, 0xe

    invoke-virtual {v2, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_2

    iget-object v6, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    :cond_2
    move v4, v4

    .line 1780459
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780460
    const-string v1, "\t blurryPhotoDetectorThreshold: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780461
    const/16 v4, 0x10

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_10

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    :goto_a
    move v4, v4

    .line 1780462
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780463
    const-string v1, "\t blurryPhotoDetectorResolution: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780464
    const/16 v4, 0x12

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_11

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_b
    move v4, v4

    .line 1780465
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780466
    const-string v1, "\t panoramaRemovalEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x0

    .line 1780467
    const/16 v5, 0x14

    invoke-virtual {v2, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_3

    iget-object v6, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-eqz v5, :cond_3

    const/4 v4, 0x1

    :cond_3
    move v4, v4

    .line 1780468
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780469
    const-string v1, "\t panoramaMaxRatio: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780470
    const/16 v4, 0x16

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_12

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    :goto_c
    move v4, v4

    .line 1780471
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780472
    const-string v1, "\t nativeResolutionWidth: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780473
    const/16 v4, 0x1a

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_13

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_d
    move v4, v4

    .line 1780474
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780475
    const-string v1, "\t nativeResolutionHeight: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780476
    const/16 v4, 0x1c

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_14

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_e
    move v4, v4

    .line 1780477
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780478
    const-string v1, "\t burstProcessorEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780479
    const/16 v4, 0x1e

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_15

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-nez v4, :cond_15

    const/4 v4, 0x0

    :goto_f
    move v4, v4

    .line 1780480
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780481
    const-string v1, "\t burstSimilarityThreshold: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780482
    const/16 v4, 0x20

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_16

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    :goto_10
    move v4, v4

    .line 1780483
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780484
    const-string v1, "\t burstMinPhotosCountForLoop: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780485
    const/16 v4, 0x22

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_17

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_11
    move v4, v4

    .line 1780486
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780487
    const-string v1, "\t videoEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x0

    .line 1780488
    const/16 v5, 0x24

    invoke-virtual {v2, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_4

    iget-object v6, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x1

    :cond_4
    move v4, v4

    .line 1780489
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780490
    const-string v1, "\t minVideoDurationInSeconds: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780491
    const/16 v4, 0x28

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_18

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    :goto_12
    move v4, v4

    .line 1780492
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780493
    const-string v1, "\t maxVideoDurationInSeconds: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780494
    const/16 v4, 0x2a

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_19

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    :goto_13
    move v4, v4

    .line 1780495
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780496
    const-string v1, "\t videoPixelCount: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1780497
    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_1a

    iget-object v5, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v2, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_14
    move v2, v4

    .line 1780498
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780499
    const-string v1, "\t employeeLoggingEnabled: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    .line 1780500
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_5

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eqz v4, :cond_5

    const/4 v2, 0x1

    :cond_5
    move v2, v2

    .line 1780501
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1780502
    const-string v1, "\t useCompactDiskCache: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    .line 1780503
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_6

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x1

    :cond_6
    move v0, v2

    .line 1780504
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_c
    const/16 v4, 0xe

    goto/16 :goto_6

    :cond_d
    const/4 v4, 0x4

    goto/16 :goto_7

    :cond_e
    const/16 v4, 0x1e

    goto/16 :goto_8

    :cond_f
    const v4, 0x2625a0

    goto/16 :goto_9

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_a

    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_b

    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_c

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_15
    const/4 v4, 0x1

    goto/16 :goto_f

    :cond_16
    const/4 v4, 0x0

    goto/16 :goto_10

    :cond_17
    const/4 v4, 0x0

    goto/16 :goto_11

    :cond_18
    const/4 v4, 0x0

    goto/16 :goto_12

    :cond_19
    const/4 v4, 0x0

    goto/16 :goto_13

    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_14
.end method

.method private g()Landroid/util/Pair;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1780364
    :try_start_0
    iget-object v0, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1780365
    iget-object v0, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BOo;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BOo;->c:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780366
    iget-object v0, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BOo;->b:LX/0Tn;

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/BOo;->c:LX/0Tn;

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 1780367
    :goto_0
    return-object v0

    .line 1780368
    :catch_0
    move-exception v0

    .line 1780369
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1780370
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1780371
    :cond_0
    const v0, 0x2625a0

    .line 1780372
    const v1, 0xa8c00

    .line 1780373
    iget-object v2, p0, LX/BOo;->g:LX/455;

    .line 1780374
    iget-object v4, v2, LX/455;->a:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.camera"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v2, LX/455;->a:Landroid/content/pm/PackageManager;

    const-string v5, "android.hardware.camera.front"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_1
    const/4 v4, 0x1

    :goto_1
    move v2, v4

    .line 1780375
    if-eqz v2, :cond_7

    .line 1780376
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v6

    move v5, v3

    .line 1780377
    :goto_2
    if-ge v5, v6, :cond_3

    .line 1780378
    const v2, -0x1c352e3f

    :try_start_1
    invoke-static {v5, v2}, LX/0J2;->a(II)Landroid/hardware/Camera;

    move-result-object v2

    .line 1780379
    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    .line 1780380
    const v7, 0x6bc34ba8

    invoke-static {v2, v7}, LX/0J2;->a(Landroid/hardware/Camera;I)V

    .line 1780381
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 1780382
    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    iget v8, v2, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v7, v8

    if-ge v7, v0, :cond_6

    .line 1780383
    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    iget v0, v2, Landroid/hardware/Camera$Size;->height:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    mul-int v2, v7, v0

    .line 1780384
    :goto_3
    :try_start_2
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedVideoSizes()Ljava/util/List;

    move-result-object v0

    .line 1780385
    if-eqz v0, :cond_5

    .line 1780386
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v3

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1780387
    iget v8, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v8

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v4, v0

    .line 1780388
    goto :goto_4

    .line 1780389
    :cond_2
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    .line 1780390
    :goto_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    move v0, v2

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1780391
    :goto_6
    if-nez v2, :cond_4

    .line 1780392
    iget-object v2, p0, LX/BOo;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/BOo;->b:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    sget-object v3, LX/BOo;->c:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1780393
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_0

    .line 1780394
    :catch_1
    :goto_7
    const/4 v2, 0x1

    goto :goto_6

    :catch_2
    move v0, v2

    goto :goto_7

    :cond_5
    move v0, v1

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v2, v3

    goto :goto_6

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()[B
    .locals 38

    .prologue
    .line 1780332
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->j:[B

    if-eqz v1, :cond_0

    .line 1780333
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->j:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1780334
    :goto_0
    monitor-exit p0

    return-object v1

    .line 1780335
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->x:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move/from16 v18, v1

    .line 1780336
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, LX/007;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    move/from16 v23, v1

    .line 1780337
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget v2, LX/1kO;->D:I

    const/4 v3, 0x7

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v24

    .line 1780338
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget v2, LX/1kO;->C:I

    const/4 v3, 0x5

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v25

    .line 1780339
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget v2, LX/1kO;->z:I

    const/16 v3, 0x1e

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v26

    .line 1780340
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget v2, LX/1kO;->g:I

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v17

    .line 1780341
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->u:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v27

    .line 1780342
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 1780343
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->s:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v15

    .line 1780344
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget v2, LX/1kO;->t:F

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v2, v3}, LX/0ad;->a(FF)F

    move-result v16

    .line 1780345
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->q:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v28

    .line 1780346
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->n:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v29

    .line 1780347
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->j:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v30

    .line 1780348
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->l:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v31

    .line 1780349
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->k:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v32

    .line 1780350
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->i:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v33

    .line 1780351
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->m:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v34

    .line 1780352
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->h:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v35

    .line 1780353
    invoke-direct/range {p0 .. p0}, LX/BOo;->g()Landroid/util/Pair;

    move-result-object v2

    .line 1780354
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v36

    .line 1780355
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 1780356
    new-instance v1, LX/0eX;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/0eX;-><init>(I)V

    .line 1780357
    const/high16 v2, 0x40400000    # 3.0f

    const v3, 0x3e4ccccd    # 0.2f

    const/high16 v4, 0x40800000    # 4.0f

    const/high16 v5, 0x40200000    # 2.5f

    const/high16 v6, 0x41200000    # 10.0f

    invoke-static/range {v1 .. v6}, LX/BOt;->a(LX/0eX;FFFFF)I

    move-result v37

    const/4 v8, 0x0

    const/16 v9, 0x280

    const/4 v10, 0x1

    const/high16 v11, 0x40200000    # 2.5f

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BOo;->e:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, LX/BOo;->e:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v14

    if-eqz v18, :cond_2

    const v17, 0x7fffffff

    :cond_2
    const/16 v18, 0x1

    const/16 v19, 0x20

    const/high16 v20, 0x40000000    # 2.0f

    const/high16 v21, 0x41f00000    # 30.0f

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    move/from16 v5, v36

    move/from16 v6, v27

    invoke-static/range {v1 .. v22}, LX/BOx;->a(LX/0eX;IIIIZZFIZFZIIZFIZIFFI)I

    move-result v8

    move/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    move/from16 v5, v33

    move/from16 v6, v34

    move/from16 v7, v35

    invoke-static/range {v1 .. v7}, LX/BOr;->a(LX/0eX;ZZZZZZ)I

    move-result v7

    move/from16 v2, v37

    move v3, v8

    move/from16 v4, v23

    move/from16 v5, v28

    move/from16 v6, v29

    invoke-static/range {v1 .. v7}, LX/BOu;->a(LX/0eX;IIZZZI)I

    move-result v2

    invoke-static {v1, v2}, LX/BOu;->a(LX/0eX;I)V

    .line 1780358
    invoke-virtual {v1}, LX/0eX;->e()[B

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/BOo;->j:[B

    .line 1780359
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->j:[B

    invoke-static {v1}, LX/BOo;->a([B)V

    .line 1780360
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BOo;->j:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1780361
    :cond_3
    const/4 v1, 0x0

    move/from16 v18, v1

    goto/16 :goto_1

    .line 1780362
    :cond_4
    const/4 v1, 0x0

    move/from16 v23, v1

    goto/16 :goto_2

    .line 1780363
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 1780330
    iget-object v0, p0, LX/BOo;->f:LX/0ad;

    sget v1, LX/1kO;->D:I

    const/4 v2, 0x7

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 1780331
    const-wide/32 v2, 0x5265c00

    if-lez v0, :cond_0

    int-to-long v0, v0

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_0
    mul-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x8

    goto :goto_0
.end method
