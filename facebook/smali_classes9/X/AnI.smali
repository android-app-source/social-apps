.class public final LX/AnI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/AnF;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/AnF;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1712314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712315
    iput-object p1, p0, LX/AnI;->a:LX/0QB;

    .line 1712316
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1712305
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/AnI;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1712307
    packed-switch p2, :pswitch_data_0

    .line 1712308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1712309
    :pswitch_0
    invoke-static {p1}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->a(LX/0QB;)Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    move-result-object v0

    .line 1712310
    :goto_0
    return-object v0

    .line 1712311
    :pswitch_1
    invoke-static {p1}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->a(LX/0QB;)Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    move-result-object v0

    goto :goto_0

    .line 1712312
    :pswitch_2
    invoke-static {p1}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->a(LX/0QB;)Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;

    move-result-object v0

    goto :goto_0

    .line 1712313
    :pswitch_3
    invoke-static {p1}, LX/Gdt;->a(LX/0QB;)LX/Gdt;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1712306
    const/4 v0, 0x4

    return v0
.end method
