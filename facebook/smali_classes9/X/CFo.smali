.class public final enum LX/CFo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CFo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CFo;

.field public static final enum BUTTON:LX/CFo;

.field public static final enum GROUP:LX/CFo;

.field public static final enum LABEL:LX/CFo;

.field public static final enum MEDIA:LX/CFo;

.field public static final enum PLACEHOLDER:LX/CFo;

.field public static final enum RECT:LX/CFo;

.field public static final enum SEQUENCE:LX/CFo;

.field public static final enum UNKNOWN:LX/CFo;


# instance fields
.field private final mViewType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1864397
    new-instance v0, LX/CFo;

    const-string v1, "MEDIA"

    const-string v2, "media"

    invoke-direct {v0, v1, v4, v2}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->MEDIA:LX/CFo;

    .line 1864398
    new-instance v0, LX/CFo;

    const-string v1, "RECT"

    const-string v2, "rect"

    invoke-direct {v0, v1, v5, v2}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->RECT:LX/CFo;

    .line 1864399
    new-instance v0, LX/CFo;

    const-string v1, "LABEL"

    const-string v2, "label"

    invoke-direct {v0, v1, v6, v2}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->LABEL:LX/CFo;

    .line 1864400
    new-instance v0, LX/CFo;

    const-string v1, "BUTTON"

    const-string v2, "button"

    invoke-direct {v0, v1, v7, v2}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->BUTTON:LX/CFo;

    .line 1864401
    new-instance v0, LX/CFo;

    const-string v1, "SEQUENCE"

    const-string v2, "sequence"

    invoke-direct {v0, v1, v8, v2}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->SEQUENCE:LX/CFo;

    .line 1864402
    new-instance v0, LX/CFo;

    const-string v1, "GROUP"

    const/4 v2, 0x5

    const-string v3, "group"

    invoke-direct {v0, v1, v2, v3}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->GROUP:LX/CFo;

    .line 1864403
    new-instance v0, LX/CFo;

    const-string v1, "PLACEHOLDER"

    const/4 v2, 0x6

    const-string v3, "placeholder"

    invoke-direct {v0, v1, v2, v3}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->PLACEHOLDER:LX/CFo;

    .line 1864404
    new-instance v0, LX/CFo;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/CFo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CFo;->UNKNOWN:LX/CFo;

    .line 1864405
    const/16 v0, 0x8

    new-array v0, v0, [LX/CFo;

    sget-object v1, LX/CFo;->MEDIA:LX/CFo;

    aput-object v1, v0, v4

    sget-object v1, LX/CFo;->RECT:LX/CFo;

    aput-object v1, v0, v5

    sget-object v1, LX/CFo;->LABEL:LX/CFo;

    aput-object v1, v0, v6

    sget-object v1, LX/CFo;->BUTTON:LX/CFo;

    aput-object v1, v0, v7

    sget-object v1, LX/CFo;->SEQUENCE:LX/CFo;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CFo;->GROUP:LX/CFo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CFo;->PLACEHOLDER:LX/CFo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CFo;->UNKNOWN:LX/CFo;

    aput-object v2, v0, v1

    sput-object v0, LX/CFo;->$VALUES:[LX/CFo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864394
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1864395
    iput-object p3, p0, LX/CFo;->mViewType:Ljava/lang/String;

    .line 1864396
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CFo;
    .locals 5

    .prologue
    .line 1864406
    if-eqz p0, :cond_1

    .line 1864407
    invoke-static {}, LX/CFo;->values()[LX/CFo;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1864408
    iget-object v4, v0, LX/CFo;->mViewType:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1864409
    :goto_1
    return-object v0

    .line 1864410
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1864411
    :cond_1
    sget-object v0, LX/CFo;->UNKNOWN:LX/CFo;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/CFo;
    .locals 1

    .prologue
    .line 1864393
    const-class v0, LX/CFo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CFo;

    return-object v0
.end method

.method public static values()[LX/CFo;
    .locals 1

    .prologue
    .line 1864392
    sget-object v0, LX/CFo;->$VALUES:[LX/CFo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CFo;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1864391
    iget-object v0, p0, LX/CFo;->mViewType:Ljava/lang/String;

    return-object v0
.end method
