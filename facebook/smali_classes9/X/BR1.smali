.class public LX/BR1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Landroid/net/Uri;

.field public c:Landroid/graphics/RectF;

.field public d:LX/434;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1783113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783114
    if-eqz p1, :cond_1

    const-string v0, "mediaUriKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1783115
    const-string v0, "mediaUriKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LX/BR1;->a:Landroid/net/Uri;

    .line 1783116
    :goto_0
    if-eqz p1, :cond_2

    const-string v0, "unmodifiedFileSchemeUriKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_1
    iput-object v0, p0, LX/BR1;->b:Landroid/net/Uri;

    .line 1783117
    if-eqz p1, :cond_3

    const-string v0, "mediaCropBoxKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    :goto_2
    iput-object v0, p0, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783118
    if-eqz p1, :cond_4

    const-string v0, "uncroppedMediaWidth"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, LX/434;

    const-string v2, "uncroppedMediaWidth"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "uncroppedMediaHeight"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v0, v2, v3}, LX/434;-><init>(II)V

    :goto_3
    iput-object v0, p0, LX/BR1;->d:LX/434;

    .line 1783119
    iget-object v0, p2, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1783120
    iput-object v0, p0, LX/BR1;->f:Ljava/lang/String;

    .line 1783121
    iget-object v0, p2, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1783122
    iput-object v0, p0, LX/BR1;->e:Ljava/lang/String;

    .line 1783123
    iget-object v0, p2, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1783124
    iput-object v0, p0, LX/BR1;->g:Ljava/lang/String;

    .line 1783125
    if-eqz p1, :cond_0

    const-string v0, "captionKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/BR1;->h:Ljava/lang/String;

    .line 1783126
    return-void

    .line 1783127
    :cond_1
    iget-object v0, p2, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1783128
    iput-object v0, p0, LX/BR1;->a:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1783129
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1783130
    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 1783131
    goto :goto_3
.end method
