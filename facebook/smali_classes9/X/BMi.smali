.class public final LX/BMi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BMj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/BMj;


# direct methods
.method public constructor <init>(LX/BMj;)V
    .locals 1

    .prologue
    .line 1777817
    iput-object p1, p0, LX/BMi;->c:LX/BMj;

    .line 1777818
    move-object v0, p1

    .line 1777819
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1777820
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1777821
    const-string v0, "LowConfidencePromptIconComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1777822
    if-ne p0, p1, :cond_1

    .line 1777823
    :cond_0
    :goto_0
    return v0

    .line 1777824
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1777825
    goto :goto_0

    .line 1777826
    :cond_3
    check-cast p1, LX/BMi;

    .line 1777827
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1777828
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1777829
    if-eq v2, v3, :cond_0

    .line 1777830
    iget-object v2, p0, LX/BMi;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BMi;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/BMi;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1777831
    goto :goto_0

    .line 1777832
    :cond_5
    iget-object v2, p1, LX/BMi;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 1777833
    :cond_6
    iget-object v2, p0, LX/BMi;->b:LX/1Pp;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/BMi;->b:LX/1Pp;

    iget-object v3, p1, LX/BMi;->b:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1777834
    goto :goto_0

    .line 1777835
    :cond_7
    iget-object v2, p1, LX/BMi;->b:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
