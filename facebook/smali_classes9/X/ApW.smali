.class public final LX/ApW;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/ApW;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApU;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/ApX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716174
    const/4 v0, 0x0

    sput-object v0, LX/ApW;->a:LX/ApW;

    .line 1716175
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApW;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1716171
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716172
    new-instance v0, LX/ApX;

    invoke-direct {v0}, LX/ApX;-><init>()V

    iput-object v0, p0, LX/ApW;->c:LX/ApX;

    .line 1716173
    return-void
.end method

.method public static c(LX/1De;)LX/ApU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716163
    new-instance v1, LX/ApV;

    invoke-direct {v1}, LX/ApV;-><init>()V

    .line 1716164
    sget-object v2, LX/ApW;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ApU;

    .line 1716165
    if-nez v2, :cond_0

    .line 1716166
    new-instance v2, LX/ApU;

    invoke-direct {v2}, LX/ApU;-><init>()V

    .line 1716167
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/ApU;->a$redex0(LX/ApU;LX/1De;IILX/ApV;)V

    .line 1716168
    move-object v1, v2

    .line 1716169
    move-object v0, v1

    .line 1716170
    return-object v0
.end method

.method public static declared-synchronized q()LX/ApW;
    .locals 2

    .prologue
    .line 1716159
    const-class v1, LX/ApW;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/ApW;->a:LX/ApW;

    if-nez v0, :cond_0

    .line 1716160
    new-instance v0, LX/ApW;

    invoke-direct {v0}, LX/ApW;-><init>()V

    sput-object v0, LX/ApW;->a:LX/ApW;

    .line 1716161
    :cond_0
    sget-object v0, LX/ApW;->a:LX/ApW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1716162
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716157
    invoke-static {}, LX/1dS;->b()V

    .line 1716158
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1716153
    check-cast p4, LX/ApV;

    .line 1716154
    iget-object v3, p4, LX/ApV;->a:Ljava/lang/CharSequence;

    iget-object v4, p4, LX/ApV;->b:Ljava/lang/CharSequence;

    iget-object v5, p4, LX/ApV;->c:Ljava/lang/CharSequence;

    iget-boolean v6, p4, LX/ApV;->d:Z

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, LX/ApX;->a(LX/1De;IILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/1Dg;

    move-result-object v0

    .line 1716155
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1716156
    const/4 v0, 0x1

    return v0
.end method
