.class public LX/BZS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1797283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1797284
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/BZS;->a:J

    .line 1797285
    return-void
.end method

.method public static declared-synchronized f(LX/BZS;)J
    .locals 4

    .prologue
    .line 1797279
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/BZS;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1797280
    invoke-static {}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->newInstance()J

    move-result-wide v0

    iput-wide v0, p0, LX/BZS;->a:J

    .line 1797281
    :cond_0
    iget-wide v0, p0, LX/BZS;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 1797282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1797277
    invoke-static {p0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->setEffect(JLjava/lang/String;)V

    .line 1797278
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1797272
    invoke-static {p0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    invoke-static/range {v0 .. v6}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->doFrame(J[F[F[FJ)V

    .line 1797273
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 1797274
    iget-wide v0, p0, LX/BZS;->a:J

    invoke-static {v0, v1}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->deleteInstance(J)V

    .line 1797275
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1797276
    return-void
.end method
