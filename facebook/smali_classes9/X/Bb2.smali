.class public final LX/Bb2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;)V
    .locals 0

    .prologue
    .line 1800142
    iput-object p1, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1800143
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    .line 1800144
    iput-boolean v2, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    .line 1800145
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->setResult(I)V

    .line 1800146
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->finish()V

    .line 1800147
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    invoke-virtual {v0, v2, v2}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->overridePendingTransition(II)V

    .line 1800148
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1800149
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    const/4 v1, 0x0

    .line 1800150
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    .line 1800151
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->w:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1800152
    iget-object v0, p0, LX/Bb2;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->r:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0829f7

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1800153
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800154
    invoke-direct {p0}, LX/Bb2;->a()V

    return-void
.end method
