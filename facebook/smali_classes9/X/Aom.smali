.class public final LX/Aom;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Amm;

.field public final synthetic e:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/Amm;)V
    .locals 0

    .prologue
    .line 1714827
    iput-object p1, p0, LX/Aom;->e:LX/Aov;

    iput-object p2, p0, LX/Aom;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aom;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aom;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aom;->d:LX/Amm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 1714828
    iget-object v0, p0, LX/Aom;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1714829
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1714830
    move-object v4, v0

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714831
    iget-object v0, p0, LX/Aom;->e:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aom;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Aom;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1714832
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1714833
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aom;->e:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714834
    iget-object v5, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1714835
    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aom;->c:Ljava/lang/String;

    .line 1714836
    iget-object v6, v0, LX/1EQ;->a:LX/0Zb;

    const-string v7, "feed_share_action"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1714837
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1714838
    invoke-virtual {v6, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "share_type"

    const-string p1, "edit_and_share"

    invoke-virtual {v7, v8, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "composer_session_id"

    invoke-virtual {v7, v8, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "story_id"

    invoke-virtual {v7, v8, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "user_id"

    invoke-virtual {v7, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "shareable_id"

    invoke-virtual {v7, v8, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714839
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 1714840
    :cond_0
    iget-object v0, p0, LX/Aom;->d:LX/Amm;

    iget-object v1, p0, LX/Aom;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/Amm;->onClick(Ljava/lang/String;)V

    .line 1714841
    const/4 v0, 0x1

    return v0
.end method
