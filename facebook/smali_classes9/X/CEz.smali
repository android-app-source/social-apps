.class public final enum LX/CEz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CEz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CEz;

.field public static final enum PERMALINK:LX/CEz;

.field public static final enum PROMOTION:LX/CEz;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1862330
    new-instance v0, LX/CEz;

    const-string v1, "PROMOTION"

    const-string v2, "promotion"

    invoke-direct {v0, v1, v3, v2}, LX/CEz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CEz;->PROMOTION:LX/CEz;

    .line 1862331
    new-instance v0, LX/CEz;

    const-string v1, "PERMALINK"

    const-string v2, "permalink"

    invoke-direct {v0, v1, v4, v2}, LX/CEz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CEz;->PERMALINK:LX/CEz;

    .line 1862332
    const/4 v0, 0x2

    new-array v0, v0, [LX/CEz;

    sget-object v1, LX/CEz;->PROMOTION:LX/CEz;

    aput-object v1, v0, v3

    sget-object v1, LX/CEz;->PERMALINK:LX/CEz;

    aput-object v1, v0, v4

    sput-object v0, LX/CEz;->$VALUES:[LX/CEz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1862327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1862328
    iput-object p3, p0, LX/CEz;->name:Ljava/lang/String;

    .line 1862329
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CEz;
    .locals 1

    .prologue
    .line 1862319
    const-class v0, LX/CEz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CEz;

    return-object v0
.end method

.method public static values()[LX/CEz;
    .locals 1

    .prologue
    .line 1862326
    sget-object v0, LX/CEz;->$VALUES:[LX/CEz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CEz;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1862320
    iget-object v0, p0, LX/CEz;->name:Ljava/lang/String;

    sget-object v1, LX/CEz;->PROMOTION:LX/CEz;

    iget-object v1, v1, LX/CEz;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1862321
    const-string v0, "throwback_promotion"

    .line 1862322
    :goto_0
    return-object v0

    .line 1862323
    :cond_0
    iget-object v0, p0, LX/CEz;->name:Ljava/lang/String;

    sget-object v1, LX/CEz;->PERMALINK:LX/CEz;

    iget-object v1, v1, LX/CEz;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1862324
    const-string v0, "throwback_permalink"

    goto :goto_0

    .line 1862325
    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method
