.class public final LX/Cdk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0bV;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0bV;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1923370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923371
    iput-object p1, p0, LX/Cdk;->a:LX/0QB;

    .line 1923372
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1923369
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/Cdk;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1923359
    packed-switch p2, :pswitch_data_0

    .line 1923360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1923361
    :pswitch_0
    new-instance v1, LX/Jw0;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/Jw0;-><init>(LX/0ad;)V

    .line 1923362
    move-object v0, v1

    .line 1923363
    :goto_0
    return-object v0

    .line 1923364
    :pswitch_1
    invoke-static {p1}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v0

    goto :goto_0

    .line 1923365
    :pswitch_2
    new-instance v0, LX/JwF;

    const/16 v1, 0xc83

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0xf61

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0xa

    invoke-static {p1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {v0, v1, p0, p2}, LX/JwF;-><init>(LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 1923366
    move-object v0, v0

    .line 1923367
    goto :goto_0

    .line 1923368
    :pswitch_3
    invoke-static {p1}, LX/2cy;->b(LX/0QB;)LX/2cy;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1923358
    const/4 v0, 0x4

    return v0
.end method
