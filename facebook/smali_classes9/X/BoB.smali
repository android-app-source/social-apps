.class public final LX/BoB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1821576
    iput-object p1, p0, LX/BoB;->e:LX/1dt;

    iput-object p2, p0, LX/BoB;->a:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object p3, p0, LX/BoB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/BoB;->c:Ljava/lang/String;

    iput-object p5, p0, LX/BoB;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1821577
    iget-object v0, p0, LX/BoB;->e:LX/1dt;

    iget-object v0, v0, LX/1dt;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    iget-object v1, p0, LX/BoB;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "REGULAR_FOLLOW"

    const-string v5, "FEED_X_MENU"

    invoke-virtual {v0, v1, v4, v5}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1821578
    iget-object v0, p0, LX/BoB;->e:LX/1dt;

    iget-object v1, p0, LX/BoB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    iget-object v5, p0, LX/BoB;->c:Ljava/lang/String;

    .line 1821579
    invoke-virtual {v0, v1, v4, v5, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1821580
    iget-object v0, p0, LX/BoB;->e:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    new-instance v1, LX/1W1;

    iget-object v4, p0, LX/BoB;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, v4}, LX/1W1;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    iget-object v4, p0, LX/BoB;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v0, v1, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W2;

    .line 1821581
    iget-object v1, p0, LX/BoB;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0, v1}, LX/1W2;->a(Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 1821582
    iget-object v1, p0, LX/BoB;->a:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v1, :cond_0

    move v1, v2

    .line 1821583
    :goto_0
    iput-boolean v1, v0, LX/1W2;->d:Z

    .line 1821584
    iget-object v0, p0, LX/BoB;->e:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    new-array v1, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/BoB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1821585
    return v2

    :cond_0
    move v1, v3

    .line 1821586
    goto :goto_0
.end method
