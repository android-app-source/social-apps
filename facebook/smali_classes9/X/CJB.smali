.class public final LX/CJB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attribution/InlineReplyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V
    .locals 0

    .prologue
    .line 1875101
    iput-object p1, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1875102
    const-string v0, "InlineReplyFragment"

    const-string v1, "Failed to copy media resource into local storage"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1875103
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0809bb

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0809bc

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0809bd

    new-instance v2, LX/CJA;

    invoke-direct {v2, p0}, LX/CJA;-><init>(LX/CJB;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1875104
    iget-object v0, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1875105
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875106
    check-cast p1, Ljava/util/List;

    .line 1875107
    iget-object v0, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->r:LX/CNE;

    .line 1875108
    iget-object v1, v0, LX/CNE;->c:LX/0TD;

    new-instance v2, LX/CNC;

    invoke-direct {v2, v0, p1}, LX/CNC;-><init>(LX/CNE;Ljava/util/List;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1875109
    iget-object v1, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    .line 1875110
    new-instance v2, LX/CJC;

    invoke-direct {v2, v1}, LX/CJC;-><init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    move-object v1, v2

    .line 1875111
    iget-object v2, p0, LX/CJB;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    iget-object v2, v2, Lcom/facebook/messaging/attribution/InlineReplyFragment;->q:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1875112
    return-void
.end method
