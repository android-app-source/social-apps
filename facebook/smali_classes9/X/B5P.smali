.class public LX/B5P;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1744339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLInterfaces$ImageOverlayWithSwipeableOverlays;",
            "LX/5QV;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1744328
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;->b()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel$ProfilePictureOverlaysModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel;->b()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel$ProfilePictureOverlaysModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel$AssociatedPagesModel$ProfilePictureOverlaysModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1744329
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Malformed swipeable overlays"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1744330
    :cond_1
    invoke-static {p1, p0}, LX/B5R;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)LX/0Px;

    move-result-object v2

    .line 1744331
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1744332
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5QV;

    .line 1744333
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1744334
    invoke-static {v0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1744335
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/B5P;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    .line 1744336
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1744337
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1744338
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .locals 7

    .prologue
    const/16 v1, 0x3c0

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    .line 1744256
    new-instance v0, LX/5iU;

    invoke-direct {v0}, LX/5iU;-><init>()V

    .line 1744257
    iput-object p1, v0, LX/5iU;->b:Ljava/lang/String;

    .line 1744258
    move-object v0, v0

    .line 1744259
    iput v1, v0, LX/5iU;->c:I

    .line 1744260
    move-object v0, v0

    .line 1744261
    iput v1, v0, LX/5iU;->a:I

    .line 1744262
    move-object v0, v0

    .line 1744263
    invoke-virtual {v0}, LX/5iU;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;

    move-result-object v0

    .line 1744264
    new-instance v1, LX/5iS;

    invoke-direct {v1}, LX/5iS;-><init>()V

    .line 1744265
    iput-object v0, v1, LX/5iS;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;

    .line 1744266
    move-object v0, v1

    .line 1744267
    new-instance v1, LX/5iV;

    invoke-direct {v1}, LX/5iV;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->WIDTH:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 1744268
    iput-object v2, v1, LX/5iV;->b:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 1744269
    move-object v1, v1

    .line 1744270
    iput-wide v4, v1, LX/5iV;->a:D

    .line 1744271
    move-object v1, v1

    .line 1744272
    invoke-virtual {v1}, LX/5iV;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;

    move-result-object v1

    .line 1744273
    iput-object v1, v0, LX/5iS;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;

    .line 1744274
    move-object v0, v0

    .line 1744275
    new-instance v1, LX/5iT;

    invoke-direct {v1}, LX/5iT;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->WIDTH:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 1744276
    iput-object v2, v1, LX/5iT;->b:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    .line 1744277
    move-object v1, v1

    .line 1744278
    iput-wide v4, v1, LX/5iT;->a:D

    .line 1744279
    move-object v1, v1

    .line 1744280
    invoke-virtual {v1}, LX/5iT;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;

    move-result-object v1

    .line 1744281
    iput-object v1, v0, LX/5iS;->b:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;

    .line 1744282
    move-object v0, v0

    .line 1744283
    new-instance v1, LX/5iW;

    invoke-direct {v1}, LX/5iW;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 1744284
    iput-object v2, v1, LX/5iW;->a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 1744285
    move-object v1, v1

    .line 1744286
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 1744287
    iput-object v2, v1, LX/5iW;->c:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 1744288
    move-object v1, v1

    .line 1744289
    invoke-virtual {v1}, LX/5iW;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;

    move-result-object v1

    .line 1744290
    iput-object v1, v0, LX/5iS;->d:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;

    .line 1744291
    move-object v0, v0

    .line 1744292
    new-instance v1, LX/5iX;

    invoke-direct {v1}, LX/5iX;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 1744293
    iput-object v2, v1, LX/5iX;->a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 1744294
    move-object v1, v1

    .line 1744295
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 1744296
    iput-object v2, v1, LX/5iX;->c:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 1744297
    move-object v1, v1

    .line 1744298
    invoke-virtual {v1}, LX/5iX;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    move-result-object v1

    .line 1744299
    iput-object v1, v0, LX/5iS;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    .line 1744300
    move-object v0, v0

    .line 1744301
    invoke-virtual {v0}, LX/5iS;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;

    move-result-object v0

    .line 1744302
    new-instance v1, LX/5iR;

    invoke-direct {v1}, LX/5iR;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1744303
    iput-object v0, v1, LX/5iR;->a:LX/0Px;

    .line 1744304
    move-object v0, v1

    .line 1744305
    invoke-virtual {v0}, LX/5iR;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    move-result-object v0

    .line 1744306
    new-instance v1, LX/5iP;

    invoke-direct {v1}, LX/5iP;-><init>()V

    .line 1744307
    iput-object p0, v1, LX/5iP;->h:Ljava/lang/String;

    .line 1744308
    move-object v1, v1

    .line 1744309
    iput-object v0, v1, LX/5iP;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    .line 1744310
    move-object v0, v1

    .line 1744311
    invoke-virtual {v0}, LX/5iP;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/5QV;)Lcom/facebook/photos/creativeediting/model/StickerParams;
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 1744312
    invoke-static {p0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1744313
    new-instance v0, LX/5jG;

    invoke-interface {p0}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1744314
    iput v4, v0, LX/5jG;->h:F

    .line 1744315
    move-object v0, v0

    .line 1744316
    iput v4, v0, LX/5jG;->g:F

    .line 1744317
    move-object v0, v0

    .line 1744318
    iput v3, v0, LX/5jG;->e:F

    .line 1744319
    move-object v0, v0

    .line 1744320
    iput v3, v0, LX/5jG;->f:F

    .line 1744321
    move-object v0, v0

    .line 1744322
    iput v3, v0, LX/5jG;->d:F

    .line 1744323
    move-object v0, v0

    .line 1744324
    const/4 v1, 0x1

    .line 1744325
    iput-boolean v1, v0, LX/5jG;->k:Z

    .line 1744326
    move-object v0, v0

    .line 1744327
    invoke-virtual {v0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    return-object v0
.end method
