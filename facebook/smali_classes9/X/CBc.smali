.class public LX/CBc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/CBb;

.field private final b:LX/3mL;


# direct methods
.method public constructor <init>(LX/3mL;LX/CBb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856609
    iput-object p1, p0, LX/CBc;->b:LX/3mL;

    .line 1856610
    iput-object p2, p0, LX/CBc;->a:LX/CBb;

    .line 1856611
    return-void
.end method

.method public static a(LX/0QB;)LX/CBc;
    .locals 5

    .prologue
    .line 1856591
    const-class v1, LX/CBc;

    monitor-enter v1

    .line 1856592
    :try_start_0
    sget-object v0, LX/CBc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856593
    sput-object v2, LX/CBc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856594
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856595
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856596
    new-instance p0, LX/CBc;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/CBb;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/CBb;

    invoke-direct {p0, v3, v4}, LX/CBc;-><init>(LX/3mL;LX/CBb;)V

    .line 1856597
    move-object v0, p0

    .line 1856598
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856599
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856600
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856601
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pn;IILandroid/view/View$OnClickListener;LX/0Px;)LX/1Dg;
    .locals 7
    .param p2    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;II",
            "Landroid/view/View$OnClickListener;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1856602
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v0

    .line 1856603
    iput p4, v0, LX/3mP;->b:I

    .line 1856604
    move-object v0, v0

    .line 1856605
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v3

    .line 1856606
    iget-object v0, p0, LX/CBc;->a:LX/CBb;

    move-object v1, p1

    move-object v2, p6

    move-object v4, p2

    move v5, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, LX/CBb;->a(Landroid/content/Context;LX/0Px;LX/25M;Ljava/lang/Object;ILandroid/view/View$OnClickListener;)LX/CBa;

    move-result-object v0

    .line 1856607
    iget-object v1, p0, LX/CBc;->b:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
