.class public LX/BUG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/BUA;

.field public c:LX/19w;

.field private d:LX/3gi;

.field public e:LX/0tQ;

.field public f:LX/BUH;


# direct methods
.method public constructor <init>(LX/BUA;LX/0tQ;LX/19w;LX/3gi;LX/BUH;)V
    .locals 0

    .prologue
    .line 1788762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788763
    iput-object p1, p0, LX/BUG;->b:LX/BUA;

    .line 1788764
    iput-object p2, p0, LX/BUG;->e:LX/0tQ;

    .line 1788765
    iput-object p3, p0, LX/BUG;->c:LX/19w;

    .line 1788766
    iput-object p4, p0, LX/BUG;->d:LX/3gi;

    .line 1788767
    iput-object p5, p0, LX/BUG;->f:LX/BUH;

    .line 1788768
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1788769
    iget-object v0, p0, LX/BUG;->c:LX/19w;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/19w;->a(ZZ)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/BUG;->a:Ljava/util/List;

    .line 1788770
    iget-object v0, p0, LX/BUG;->d:LX/3gi;

    iget-object v1, p0, LX/BUG;->a:Ljava/util/List;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "OfflineVideoServerCheck"

    .line 1788771
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1788772
    move-object v0, v0

    .line 1788773
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1788774
    const-string v0, "OfflineVideoServerCheck"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1788775
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, LX/BUG;->e:LX/0tQ;

    .line 1788776
    iget-object v2, v1, LX/0tQ;->b:LX/0Uh;

    const/16 v3, 0x43b

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 1788777
    if-eqz v1, :cond_9

    .line 1788778
    :cond_1
    const/4 v2, 0x0

    .line 1788779
    iget-object v3, p0, LX/BUG;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1788780
    if-eqz v0, :cond_2

    .line 1788781
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;

    .line 1788782
    :cond_2
    if-nez v3, :cond_3

    .line 1788783
    iget-object v5, p0, LX/BUG;->b:LX/BUA;

    sget-object v6, LX/7Jb;->NOT_VIEWABLE:LX/7Jb;

    invoke-virtual {v5, v2, v6}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1788784
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v5, v6, :cond_4

    iget-object v5, p0, LX/BUG;->e:LX/0tQ;

    .line 1788785
    iget-object v6, v5, LX/0tQ;->a:LX/0ad;

    sget-short v7, LX/0wh;->A:S

    invoke-static {v5}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v8

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    move v5, v6

    .line 1788786
    if-eqz v5, :cond_4

    .line 1788787
    iget-object v5, p0, LX/BUG;->b:LX/BUA;

    sget-object v6, LX/7Jb;->SAVE_STATE_CHANGED:LX/7Jb;

    invoke-virtual {v5, v2, v6}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1788788
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;->k()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1788789
    iget-object v5, p0, LX/BUG;->b:LX/BUA;

    sget-object v6, LX/7Jb;->NOT_SAVABLE_OFFLINE:LX/7Jb;

    invoke-virtual {v5, v2, v6}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1788790
    :cond_5
    iget-object v5, p0, LX/BUG;->c:LX/19w;

    invoke-virtual {v3}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoServerCheckQueryModels$OfflineVideoServerCheckQueryModel;->l()I

    move-result v6

    int-to-long v6, v6

    const/4 v8, 0x0

    .line 1788791
    invoke-virtual {v5, v2}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v9

    .line 1788792
    if-eqz v9, :cond_6

    iget-boolean v10, v9, LX/7Jg;->o:Z

    if-nez v10, :cond_6

    iget-object v10, v9, LX/7Jg;->l:LX/1A0;

    sget-object v11, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v10, v11, :cond_a

    .line 1788793
    :cond_6
    :goto_1
    move v5, v8

    .line 1788794
    if-eqz v5, :cond_7

    .line 1788795
    iget-object v5, p0, LX/BUG;->b:LX/BUA;

    sget-object v6, LX/7Jb;->VIDEO_EXPIRED:LX/7Jb;

    invoke-virtual {v5, v2, v6}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1788796
    :cond_7
    iget-object v5, p0, LX/BUG;->b:LX/BUA;

    invoke-virtual {v5, v2}, LX/BUA;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1788797
    :cond_8
    iget-object v2, p0, LX/BUG;->f:LX/BUH;

    .line 1788798
    iget-object v8, v2, LX/BUH;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    iget-object v9, v2, LX/BUH;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    iget-object v9, v9, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->i:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    .line 1788799
    iput-wide v10, v8, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->j:J

    .line 1788800
    :cond_9
    return-void

    .line 1788801
    :cond_a
    iget-object v10, v5, LX/19w;->j:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    iget-wide v12, v9, LX/7Jg;->p:J

    sub-long/2addr v10, v12

    .line 1788802
    cmp-long v9, v10, v6

    if-lez v9, :cond_6

    const/4 v8, 0x1

    goto :goto_1
.end method
