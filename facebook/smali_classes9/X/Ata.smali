.class public LX/Ata;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0i5;

.field public final b:LX/ArL;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(LX/0i5;LX/ArL;)V
    .locals 0
    .param p1    # LX/0i5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1721563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721564
    iput-object p1, p0, LX/Ata;->a:LX/0i5;

    .line 1721565
    iput-object p2, p0, LX/Ata;->b:LX/ArL;

    .line 1721566
    return-void
.end method

.method public static a$redex0(LX/Ata;Z[Ljava/lang/String;LX/AtZ;)V
    .locals 3

    .prologue
    .line 1721567
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ata;->c:Z

    .line 1721568
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ata;->d:Z

    .line 1721569
    iget-object v0, p0, LX/Ata;->a:LX/0i5;

    invoke-virtual {v0, p2}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    .line 1721570
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1721571
    iget-object v2, p3, LX/AtZ;->mLoggingKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1721572
    if-nez p1, :cond_1

    if-nez v0, :cond_1

    .line 1721573
    iget-object v0, p0, LX/Ata;->b:LX/ArL;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1721574
    sget-object v2, LX/ArH;->PERMISSION_DENIED:LX/ArH;

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0}, LX/ArL;->h(LX/ArL;)Ljava/util/Map;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/ArI;->PERMISSION:LX/ArI;

    invoke-virtual {p0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721575
    :cond_0
    :goto_0
    return-void

    .line 1721576
    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 1721577
    iget-object v0, p0, LX/Ata;->b:LX/ArL;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1721578
    sget-object v2, LX/ArH;->PERMISSION_ACCEPTED:LX/ArH;

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0}, LX/ArL;->h(LX/ArL;)Ljava/util/Map;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object p0, LX/ArI;->PERMISSION:LX/ArI;

    invoke-virtual {p0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721579
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ar8;LX/AtZ;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1721580
    iget-object v3, p2, LX/AtZ;->mManifestFlags:[Ljava/lang/String;

    .line 1721581
    iget-object v1, p0, LX/Ata;->a:LX/0i5;

    invoke-virtual {v1, v3}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v2

    .line 1721582
    if-eqz v2, :cond_0

    .line 1721583
    invoke-interface {p1}, LX/Ar8;->a()V

    .line 1721584
    :goto_0
    return v0

    .line 1721585
    :cond_0
    iput-boolean v0, p0, LX/Ata;->c:Z

    .line 1721586
    iget-object v6, p0, LX/Ata;->a:LX/0i5;

    new-instance v0, LX/AtY;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/AtY;-><init>(LX/Ata;Z[Ljava/lang/String;LX/AtZ;LX/Ar8;)V

    invoke-virtual {v6, v3, v0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1721587
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/AtZ;)Z
    .locals 2

    .prologue
    .line 1721588
    iget-object v0, p1, LX/AtZ;->mManifestFlags:[Ljava/lang/String;

    .line 1721589
    iget-object v1, p0, LX/Ata;->a:LX/0i5;

    invoke-virtual {v1, v0}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
