.class public final LX/CHQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

.field public b:LX/1ca;

.field private final c:Ljava/lang/String;

.field public d:LX/8Ys;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;LX/CId;)V
    .locals 1

    .prologue
    .line 1867495
    iput-object p1, p0, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1867496
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1867497
    const/16 p1, 0x2f

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1867498
    const-string p1, "catalogId="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867499
    iget-object p1, p2, LX/CId;->c:Ljava/lang/String;

    move-object p1, p1

    .line 1867500
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867501
    const-string p1, "&productId="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867502
    iget-object p1, p2, LX/CId;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1867503
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867504
    const-string p1, "&productView="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867505
    iget-object p1, p2, LX/CId;->b:Ljava/lang/String;

    move-object p1, p1

    .line 1867506
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867507
    const-string p1, "&prefetch="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867508
    iget-boolean p1, p2, LX/CId;->g:Z

    move p1, p1

    .line 1867509
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867510
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1867511
    iput-object v0, p0, LX/CHQ;->c:Ljava/lang/String;

    .line 1867512
    return-void
.end method
