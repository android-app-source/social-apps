.class public LX/CWD;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;",
        "LX/CWB;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;)V
    .locals 0

    .prologue
    .line 1907765
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1907766
    return-void
.end method

.method private a(LX/CWB;)V
    .locals 2

    .prologue
    .line 1907767
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;

    .line 1907768
    iget-object v1, p0, LX/CWD;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1907769
    iget-object v1, p1, LX/CWB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->setInfoMessage(Ljava/lang/String;)V

    .line 1907770
    iget-object v1, p1, LX/CWB;->b:LX/3Ab;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->setTermsAndPolicies(LX/3Ab;)V

    .line 1907771
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 1907772
    check-cast p1, LX/CWB;

    invoke-direct {p0, p1}, LX/CWD;->a(LX/CWB;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1907773
    iput-object p1, p0, LX/CWD;->l:LX/6qh;

    .line 1907774
    return-void
.end method
