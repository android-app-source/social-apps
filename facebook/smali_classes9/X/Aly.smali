.class public LX/Aly;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/1aS;


# instance fields
.field private final j:Lcom/facebook/widget/text/BetterTextView;

.field private final k:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1710419
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Aly;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710420
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1710417
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Aly;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710418
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1710412
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710413
    const v0, 0x7f030f4e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1710414
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Aly;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 1710415
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Aly;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1710416
    return-void
.end method


# virtual methods
.method public getPhotoTray()Landroid/view/View;
    .locals 1

    .prologue
    .line 1710411
    invoke-virtual {p0}, LX/Aly;->getV2AttachmentView()Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->getPhotoTray()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1710410
    iget-object v0, p0, LX/Aly;->k:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1710408
    iget-object v0, p0, LX/Aly;->j:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getV2AttachmentView()Lcom/facebook/feed/photoreminder/PhotoReminderV2View;
    .locals 1

    .prologue
    .line 1710409
    const v0, 0x7f0d2507

    invoke-virtual {p0, v0}, LX/Aly;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    return-object v0
.end method
