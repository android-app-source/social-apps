.class public final enum LX/CFD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CFD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CFD;

.field public static final enum NAVIGATION_BACK:LX/CFD;

.field public static final enum NAVIGATION_CANCEL:LX/CFD;

.field public static final enum NAVIGATION_NEXT:LX/CFD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1862813
    new-instance v0, LX/CFD;

    const-string v1, "NAVIGATION_BACK"

    invoke-direct {v0, v1, v2}, LX/CFD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFD;->NAVIGATION_BACK:LX/CFD;

    .line 1862814
    new-instance v0, LX/CFD;

    const-string v1, "NAVIGATION_NEXT"

    invoke-direct {v0, v1, v3}, LX/CFD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFD;->NAVIGATION_NEXT:LX/CFD;

    .line 1862815
    new-instance v0, LX/CFD;

    const-string v1, "NAVIGATION_CANCEL"

    invoke-direct {v0, v1, v4}, LX/CFD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFD;->NAVIGATION_CANCEL:LX/CFD;

    .line 1862816
    const/4 v0, 0x3

    new-array v0, v0, [LX/CFD;

    sget-object v1, LX/CFD;->NAVIGATION_BACK:LX/CFD;

    aput-object v1, v0, v2

    sget-object v1, LX/CFD;->NAVIGATION_NEXT:LX/CFD;

    aput-object v1, v0, v3

    sget-object v1, LX/CFD;->NAVIGATION_CANCEL:LX/CFD;

    aput-object v1, v0, v4

    sput-object v0, LX/CFD;->$VALUES:[LX/CFD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1862817
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CFD;
    .locals 1

    .prologue
    .line 1862818
    const-class v0, LX/CFD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CFD;

    return-object v0
.end method

.method public static values()[LX/CFD;
    .locals 1

    .prologue
    .line 1862819
    sget-object v0, LX/CFD;->$VALUES:[LX/CFD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CFD;

    return-object v0
.end method
