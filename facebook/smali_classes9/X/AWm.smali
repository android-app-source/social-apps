.class public LX/AWm;
.super LX/AWT;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field private final b:Lcom/facebook/resources/ui/FbTextView;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field public final f:Lcom/facebook/resources/ui/FbButton;

.field public final g:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1683340
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683341
    invoke-virtual {p0}, LX/AWm;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastDialog:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1683342
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1683343
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1683344
    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683345
    const v0, 0x7f0d0bec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AWm;->a:Landroid/view/View;

    .line 1683346
    const v0, 0x7f0d0fdc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AWm;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1683347
    const v0, 0x7f0d0fdd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AWm;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1683348
    const v0, 0x7f0d0fde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AWm;->f:Lcom/facebook/resources/ui/FbButton;

    .line 1683349
    const v0, 0x7f0d0fdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AWm;->g:Lcom/facebook/resources/ui/FbButton;

    .line 1683350
    new-instance v0, LX/AWo;

    invoke-direct {v0, p0}, LX/AWo;-><init>(LX/AWm;)V

    .line 1683351
    iget-object v1, p0, LX/AWm;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683352
    iget-object v1, p0, LX/AWm;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683353
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1683354
    invoke-virtual {p0}, LX/AWm;->c()V

    .line 1683355
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1683338
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 1683339
    return-void
.end method

.method public setActionFinishText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1683335
    iget-object v0, p0, LX/AWm;->f:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1683336
    iget-object v0, p0, LX/AWm;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1683337
    return-void
.end method

.method public setActionResumeText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1683332
    iget-object v0, p0, LX/AWm;->g:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1683333
    iget-object v0, p0, LX/AWm;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1683334
    return-void
.end method

.method public setDescription(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1683329
    iget-object v0, p0, LX/AWm;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1683330
    iget-object v0, p0, LX/AWm;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683331
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1683326
    iget-object v0, p0, LX/AWm;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1683327
    iget-object v0, p0, LX/AWm;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683328
    return-void
.end method
