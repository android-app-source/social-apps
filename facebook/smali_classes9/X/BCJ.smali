.class public final LX/BCJ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1759353
    const-class v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    const v0, -0x7c2add8f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "NotificationNodeSettingsQuery"

    const-string v6, "9ffc04715e5f78e6040e0128c5ba57cc"

    const-string v7, "nodes"

    const-string v8, "10155069967926729"

    const/4 v9, 0x0

    .line 1759354
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1759355
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1759356
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1759357
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1759358
    sparse-switch v0, :sswitch_data_0

    .line 1759359
    :goto_0
    return-object p1

    .line 1759360
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1759361
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1759362
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1759363
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_1
        -0x93a55fc -> :sswitch_3
        0x196b8 -> :sswitch_0
        0x1918b88b -> :sswitch_2
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1759364
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQL$NotificationNodeSettingsQueryString$1;

    const-class v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQL$NotificationNodeSettingsQueryString$1;-><init>(LX/BCJ;Ljava/lang/Class;)V

    return-object v0
.end method
