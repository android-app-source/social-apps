.class public LX/Bhy;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1809839
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1809840
    iput-object p1, p0, LX/Bhy;->a:Landroid/content/Context;

    .line 1809841
    iput-object p2, p0, LX/Bhy;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1809842
    new-instance v0, LX/Bhx;

    invoke-direct {v0, p0}, LX/Bhx;-><init>(LX/Bhy;)V

    invoke-virtual {p0, v0}, LX/Bhy;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1809843
    const v0, 0x7f080653

    invoke-virtual {p0, v0}, LX/Bhy;->setTitle(I)V

    .line 1809844
    return-void
.end method

.method public static a(LX/0QB;)LX/Bhy;
    .locals 3

    .prologue
    .line 1809845
    new-instance v2, LX/Bhy;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/Bhy;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1809846
    move-object v0, v2

    .line 1809847
    return-object v0
.end method
