.class public final LX/Cbl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V
    .locals 0

    .prologue
    .line 1919772
    iput-object p1, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 4

    .prologue
    .line 1919762
    sget-object v0, LX/Cbi;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1919763
    :goto_0
    return-void

    .line 1919764
    :pswitch_0
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->A:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "click_like_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1919765
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->o(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    goto :goto_0

    .line 1919766
    :pswitch_1
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->A:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "click_comment_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1919767
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b(Z)V

    goto :goto_0

    .line 1919768
    :pswitch_2
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->A:LX/0if;

    sget-object v1, LX/0ig;->x:LX/0ih;

    const-string v2, "click_share_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1919769
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->O:LX/0W3;

    sget-wide v2, LX/0X5;->es:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919770
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/0am;)V

    goto :goto_0

    .line 1919771
    :cond_0
    iget-object v0, p0, LX/Cbl;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->d()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
