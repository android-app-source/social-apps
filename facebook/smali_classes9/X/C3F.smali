.class public LX/C3F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/C3I;

.field public final d:LX/C39;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1845285
    const v0, 0x7f0b1d6e

    sput v0, LX/C3F;->a:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/C3I;LX/C39;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;",
            "LX/C3I;",
            "LX/C39;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845298
    iput-object p1, p0, LX/C3F;->b:LX/0Ot;

    .line 1845299
    iput-object p2, p0, LX/C3F;->c:LX/C3I;

    .line 1845300
    iput-object p3, p0, LX/C3F;->d:LX/C39;

    .line 1845301
    return-void
.end method

.method public static a(LX/0QB;)LX/C3F;
    .locals 6

    .prologue
    .line 1845286
    const-class v1, LX/C3F;

    monitor-enter v1

    .line 1845287
    :try_start_0
    sget-object v0, LX/C3F;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845288
    sput-object v2, LX/C3F;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845289
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845290
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845291
    new-instance v5, LX/C3F;

    const/16 v3, 0x1ebc

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/C3I;->a(LX/0QB;)LX/C3I;

    move-result-object v3

    check-cast v3, LX/C3I;

    invoke-static {v0}, LX/C39;->a(LX/0QB;)LX/C39;

    move-result-object v4

    check-cast v4, LX/C39;

    invoke-direct {v5, p0, v3, v4}, LX/C3F;-><init>(LX/0Ot;LX/C3I;LX/C39;)V

    .line 1845292
    move-object v0, v5

    .line 1845293
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845294
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845295
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
