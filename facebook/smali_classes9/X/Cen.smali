.class public LX/Cen;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1925163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925164
    const-wide/32 v0, 0x2a300

    iput-wide v0, p0, LX/Cen;->a:J

    .line 1925165
    const-wide/32 v0, 0xa8c0

    iput-wide v0, p0, LX/Cen;->b:J

    .line 1925166
    iput-object v2, p0, LX/Cen;->c:Ljava/lang/String;

    .line 1925167
    iput-object v2, p0, LX/Cen;->d:Ljava/lang/String;

    .line 1925168
    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1925157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925158
    sget-wide v0, LX/0X5;->ha:J

    const-wide/32 v2, 0x2a300

    invoke-interface {p1, v0, v1, v2, v3}, LX/0W4;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/Cen;->a:J

    .line 1925159
    sget-wide v0, LX/0X5;->hb:J

    const-wide/32 v2, 0xa8c0

    invoke-interface {p1, v0, v1, v2, v3}, LX/0W4;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/Cen;->b:J

    .line 1925160
    sget-wide v0, LX/0X5;->hc:J

    invoke-interface {p1, v0, v1, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cen;->c:Ljava/lang/String;

    .line 1925161
    sget-wide v0, LX/0X5;->hd:J

    invoke-interface {p1, v0, v1, v4}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cen;->d:Ljava/lang/String;

    .line 1925162
    return-void
.end method
