.class public final LX/AWr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/93q;


# instance fields
.field public final synthetic a:LX/AWy;


# direct methods
.method public constructor <init>(LX/AWy;)V
    .locals 0

    .prologue
    .line 1683431
    iput-object p1, p0, LX/AWr;->a:LX/AWy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 5

    .prologue
    .line 1683432
    iget-object v0, p0, LX/AWr;->a:LX/AWy;

    iget-object v1, p0, LX/AWr;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v1}, Lcom/facebook/facecast/model/FacecastPrivacyData;->d()LX/AWS;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-virtual {v1, v2}, LX/AWS;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/AWS;

    move-result-object v1

    invoke-virtual {v1}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v1

    .line 1683433
    iput-object v1, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683434
    iget-object v0, p0, LX/AWr;->a:LX/AWy;

    iget-object v1, v0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-object v0, p0, LX/AWr;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683435
    iget-object v2, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    move-object v0, v2

    .line 1683436
    invoke-virtual {v0}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, LX/AWr;->a:LX/AWy;

    iget-object v3, v3, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683437
    iget-object v2, v3, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    move-object v3, v2

    .line 1683438
    invoke-virtual {v3}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    sget-object v4, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v3, v4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/String;I)V

    .line 1683439
    return-void
.end method
