.class public final enum LX/BbB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BbB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BbB;

.field public static final enum CANCEL_EDIT_CITY:LX/BbB;

.field public static final enum CANCEL_NUX:LX/BbB;

.field public static final enum CLICK_CREATE_MAP:LX/BbB;

.field public static final enum CLICK_EDIT_CITY:LX/BbB;

.field public static final enum CONVERT_POST:LX/BbB;

.field public static final enum IMPRESSION:LX/BbB;

.field public static final enum POST_ALREADY_CONVERTED:LX/BbB;


# instance fields
.field private final mActionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1800449
    new-instance v0, LX/BbB;

    const-string v1, "IMPRESSION"

    const-string v2, "conversion_nux_impression"

    invoke-direct {v0, v1, v4, v2}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->IMPRESSION:LX/BbB;

    .line 1800450
    new-instance v0, LX/BbB;

    const-string v1, "POST_ALREADY_CONVERTED"

    const-string v2, "conversion_nux_already_converted"

    invoke-direct {v0, v1, v5, v2}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->POST_ALREADY_CONVERTED:LX/BbB;

    .line 1800451
    new-instance v0, LX/BbB;

    const-string v1, "CLICK_CREATE_MAP"

    const-string v2, "conversion_nux_click_create_map"

    invoke-direct {v0, v1, v6, v2}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->CLICK_CREATE_MAP:LX/BbB;

    .line 1800452
    new-instance v0, LX/BbB;

    const-string v1, "CLICK_EDIT_CITY"

    const-string v2, "conversion_nux_click_edit_city"

    invoke-direct {v0, v1, v7, v2}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->CLICK_EDIT_CITY:LX/BbB;

    .line 1800453
    new-instance v0, LX/BbB;

    const-string v1, "CANCEL_EDIT_CITY"

    const-string v2, "conversion_nux_cancel_edit_city"

    invoke-direct {v0, v1, v8, v2}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->CANCEL_EDIT_CITY:LX/BbB;

    .line 1800454
    new-instance v0, LX/BbB;

    const-string v1, "CANCEL_NUX"

    const/4 v2, 0x5

    const-string v3, "conversion_nux_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->CANCEL_NUX:LX/BbB;

    .line 1800455
    new-instance v0, LX/BbB;

    const-string v1, "CONVERT_POST"

    const/4 v2, 0x6

    const-string v3, "conversion_nux_convert"

    invoke-direct {v0, v1, v2, v3}, LX/BbB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BbB;->CONVERT_POST:LX/BbB;

    .line 1800456
    const/4 v0, 0x7

    new-array v0, v0, [LX/BbB;

    sget-object v1, LX/BbB;->IMPRESSION:LX/BbB;

    aput-object v1, v0, v4

    sget-object v1, LX/BbB;->POST_ALREADY_CONVERTED:LX/BbB;

    aput-object v1, v0, v5

    sget-object v1, LX/BbB;->CLICK_CREATE_MAP:LX/BbB;

    aput-object v1, v0, v6

    sget-object v1, LX/BbB;->CLICK_EDIT_CITY:LX/BbB;

    aput-object v1, v0, v7

    sget-object v1, LX/BbB;->CANCEL_EDIT_CITY:LX/BbB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BbB;->CANCEL_NUX:LX/BbB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BbB;->CONVERT_POST:LX/BbB;

    aput-object v2, v0, v1

    sput-object v0, LX/BbB;->$VALUES:[LX/BbB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1800457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1800458
    iput-object p3, p0, LX/BbB;->mActionName:Ljava/lang/String;

    .line 1800459
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BbB;
    .locals 1

    .prologue
    .line 1800460
    const-class v0, LX/BbB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BbB;

    return-object v0
.end method

.method public static values()[LX/BbB;
    .locals 1

    .prologue
    .line 1800461
    sget-object v0, LX/BbB;->$VALUES:[LX/BbB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BbB;

    return-object v0
.end method


# virtual methods
.method public final getActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1800462
    iget-object v0, p0, LX/BbB;->mActionName:Ljava/lang/String;

    return-object v0
.end method
