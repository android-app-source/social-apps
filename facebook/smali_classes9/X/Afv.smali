.class public final LX/Afv;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "[",
        "LX/3K3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Afw;

.field private b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/Afw;)V
    .locals 0

    .prologue
    .line 1699878
    iput-object p1, p0, LX/Afv;->a:LX/Afw;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)LX/3K3;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1699879
    if-nez p1, :cond_0

    .line 1699880
    :goto_0
    return-object v0

    .line 1699881
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/Afv;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1699882
    :try_start_1
    invoke-static {v2}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1699883
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1699884
    :catch_0
    move-exception v1

    .line 1699885
    sget-object v2, LX/Afw;->l:Ljava/lang/String;

    const-string v3, "Failed to load the tip jar event animation json file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1699886
    check-cast p1, [Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1699887
    const/4 v0, 0x2

    new-array v0, v0, [LX/3K3;

    .line 1699888
    aget-object v1, p1, v2

    invoke-direct {p0, v1}, LX/Afv;->a(Ljava/lang/String;)LX/3K3;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1699889
    aget-object v1, p1, v3

    invoke-direct {p0, v1}, LX/Afv;->a(Ljava/lang/String;)LX/3K3;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1699890
    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1699891
    check-cast p1, [LX/3K3;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1699892
    aget-object v0, p1, v2

    if-eqz v0, :cond_0

    .line 1699893
    new-instance v0, LX/9Uk;

    invoke-direct {v0}, LX/9Uk;-><init>()V

    aget-object v1, p1, v2

    .line 1699894
    iput-object v1, v0, LX/9Uk;->a:LX/3K3;

    .line 1699895
    move-object v0, v0

    .line 1699896
    invoke-virtual {v0}, LX/9Uk;->a()LX/9Ug;

    move-result-object v0

    .line 1699897
    iget-object v1, p0, LX/Afv;->a:LX/Afw;

    iget-object v1, v1, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699898
    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 1699899
    invoke-virtual {v0}, LX/9Ug;->c()V

    .line 1699900
    :cond_0
    aget-object v0, p1, v3

    if-eqz v0, :cond_1

    .line 1699901
    new-instance v0, LX/9Uk;

    invoke-direct {v0}, LX/9Uk;-><init>()V

    aget-object v1, p1, v3

    .line 1699902
    iput-object v1, v0, LX/9Uk;->a:LX/3K3;

    .line 1699903
    move-object v0, v0

    .line 1699904
    invoke-virtual {v0}, LX/9Uk;->a()LX/9Ug;

    move-result-object v0

    .line 1699905
    iget-object v1, p0, LX/Afv;->a:LX/Afw;

    iget-object v1, v1, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1699906
    iget-object v1, p0, LX/Afv;->a:LX/Afw;

    iget-object v1, v1, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1699907
    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 1699908
    invoke-virtual {v0}, LX/9Ug;->c()V

    .line 1699909
    :goto_0
    return-void

    .line 1699910
    :cond_1
    iget-object v0, p0, LX/Afv;->a:LX/Afw;

    iget-object v0, v0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onPreExecute()V
    .locals 1

    .prologue
    .line 1699911
    iget-object v0, p0, LX/Afv;->a:LX/Afw;

    iget-object v0, v0, LX/Afw;->t:Landroid/content/res/Resources;

    iput-object v0, p0, LX/Afv;->b:Landroid/content/res/Resources;

    .line 1699912
    return-void
.end method
