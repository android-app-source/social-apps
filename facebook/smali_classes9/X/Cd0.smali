.class public final LX/Cd0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V
    .locals 0

    .prologue
    .line 1921874
    iput-object p1, p0, LX/Cd0;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1921875
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1921876
    check-cast p1, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1921877
    iget-object v0, p0, LX/Cd0;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    .line 1921878
    iput-boolean v1, v0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->g:Z

    .line 1921879
    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1921880
    iget-object v0, p0, LX/Cd0;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1921881
    iput-object v1, v0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    .line 1921882
    :goto_1
    iget-object v0, p0, LX/Cd0;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    .line 1921883
    invoke-virtual {v0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->l()V

    .line 1921884
    return-void

    .line 1921885
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 1921886
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;->k()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1921887
    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1921888
    iget-object v6, p0, LX/Cd0;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    new-instance v7, LX/5m9;

    invoke-direct {v7}, LX/5m9;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 1921889
    iput-object v8, v7, LX/5m9;->f:Ljava/lang/String;

    .line 1921890
    move-object v7, v7

    .line 1921891
    invoke-virtual {p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;->a()Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$CurrentCityModel;->l()Ljava/lang/String;

    move-result-object v8

    .line 1921892
    iput-object v8, v7, LX/5m9;->h:Ljava/lang/String;

    .line 1921893
    move-object v7, v7

    .line 1921894
    new-instance v8, LX/5mE;

    invoke-direct {v8}, LX/5mE;-><init>()V

    invoke-virtual {v3, v0, v2}, LX/15i;->l(II)D

    move-result-wide v2

    .line 1921895
    iput-wide v2, v8, LX/5mE;->a:D

    .line 1921896
    move-object v0, v8

    .line 1921897
    invoke-virtual {v5, v4, v1}, LX/15i;->l(II)D

    move-result-wide v2

    .line 1921898
    iput-wide v2, v0, LX/5mE;->b:D

    .line 1921899
    move-object v0, v0

    .line 1921900
    invoke-virtual {v0}, LX/5mE;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    .line 1921901
    iput-object v0, v7, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 1921902
    move-object v0, v7

    .line 1921903
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1921904
    iput-object v0, v6, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    .line 1921905
    goto :goto_1
.end method
