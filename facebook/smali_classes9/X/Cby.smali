.class public final LX/Cby;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cbz;


# direct methods
.method public constructor <init>(LX/Cbz;)V
    .locals 0

    .prologue
    .line 1920548
    iput-object p1, p0, LX/Cby;->a:LX/Cbz;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1920549
    check-cast p1, [LX/1FJ;

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 1920550
    :try_start_0
    iget-object v0, p0, LX/Cby;->a:LX/Cbz;

    iget-object v0, v0, LX/Cbz;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1920551
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1920552
    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1920553
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1920554
    iget-object v0, p0, LX/Cby;->a:LX/Cbz;

    iget-object v0, v0, LX/Cbz;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v3, "Could not save file (w/ Fresco + non-jpeg) bitmap is recycled"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920555
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Cby;->cancel(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1920556
    aget-object v0, p1, v8

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 1920557
    :cond_2
    :try_start_1
    new-instance v3, Ljava/io/File;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const-string v5, "Facebook"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1920558
    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V

    .line 1920559
    const-string v4, "%s_%d.jpg"

    const-string v5, "FB_IMG"

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1920560
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1920561
    iget-object v3, p0, LX/Cby;->a:LX/Cbz;

    iget-object v3, v3, LX/Cbz;->a:LX/CcO;

    iget-object v3, v3, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    invoke-virtual {v3, v0, v5}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1920562
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "image/jpeg"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1920563
    aget-object v1, p1, v8

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1920564
    :catch_0
    move-exception v0

    .line 1920565
    :try_start_2
    iget-object v2, p0, LX/Cby;->a:LX/Cbz;

    iget-object v2, v2, LX/Cbz;->a:LX/CcO;

    iget-object v2, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not save file (w/ Fresco + non-jpeg) "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1920566
    aget-object v0, p1, v8

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    aget-object v1, p1, v8

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1920567
    check-cast p1, Landroid/net/Uri;

    .line 1920568
    invoke-virtual {p0}, LX/Cby;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920569
    :goto_0
    return-void

    .line 1920570
    :cond_0
    if-nez p1, :cond_1

    .line 1920571
    iget-object v0, p0, LX/Cby;->a:LX/Cbz;

    iget-object v0, v0, LX/Cbz;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not save file (w/ Fresco + non-jpeg) No temp uri"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920572
    iget-object v0, p0, LX/Cby;->a:LX/Cbz;

    iget-object v0, v0, LX/Cbz;->a:LX/CcO;

    invoke-static {v0}, LX/CcO;->m(LX/CcO;)V

    goto :goto_0

    .line 1920573
    :cond_1
    iget-object v0, p0, LX/Cby;->a:LX/Cbz;

    iget-object v0, v0, LX/Cbz;->a:LX/CcO;

    invoke-static {v0}, LX/CcO;->l(LX/CcO;)V

    goto :goto_0
.end method
