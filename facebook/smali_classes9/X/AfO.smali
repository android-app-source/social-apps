.class public LX/AfO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0gX;

.field public final c:LX/0Sh;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/03V;

.field public final f:LX/0tX;

.field public g:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/AfJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1699229
    const-class v0, LX/AfO;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AfO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0gX;Ljava/util/concurrent/ScheduledExecutorService;LX/0tX;LX/0Sh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1699222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699223
    iput-object p2, p0, LX/AfO;->b:LX/0gX;

    .line 1699224
    iput-object p5, p0, LX/AfO;->c:LX/0Sh;

    .line 1699225
    iput-object p3, p0, LX/AfO;->d:Ljava/util/concurrent/ExecutorService;

    .line 1699226
    iput-object p1, p0, LX/AfO;->e:LX/03V;

    .line 1699227
    iput-object p4, p0, LX/AfO;->f:LX/0tX;

    .line 1699228
    return-void
.end method
