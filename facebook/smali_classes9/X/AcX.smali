.class public LX/AcX;
.super LX/AVi;
.source ""

# interfaces
.implements LX/AcW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/LiveEventsTickerView;",
        ">;",
        "LX/AcW;",
        "Lcom/facebook/facecastdisplay/donation/LiveDonationController$LiveDonationControllerListener;",
        "Lcom/facebook/facecastdisplay/liveevent/FacecastEventStoreListener;",
        "Lcom/facebook/facecastdisplay/liveevent/comment/LiveCommentEventViewController$LiveCommentBlockAuthorListener;"
    }
.end annotation


# instance fields
.field public A:Z

.field public B:F

.field public C:Ljava/lang/String;

.field public final a:LX/AcV;

.field private final b:LX/Ag1;

.field public final c:LX/3HT;

.field public final d:LX/1b4;

.field private final e:LX/AaZ;

.field private final f:LX/Ac6;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AeV;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Aee;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/Aem;

.field public final k:LX/Aev;

.field public final l:LX/AfJ;

.field public final m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

.field public final n:LX/AeS;

.field private final o:LX/1OX;

.field public final p:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation
.end field

.field private final q:I

.field public r:LX/AeJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/AcC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/Abz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Landroid/view/View$OnTouchListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>(LX/Ag1;LX/3HT;LX/1b4;LX/AaZ;LX/Ac6;LX/0Ot;LX/0Ot;LX/0Ot;LX/Aem;LX/Aev;LX/AfJ;Lcom/facebook/facecastdisplay/donation/LiveDonationController;LX/AeS;Ljava/lang/String;)V
    .locals 3
    .param p14    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ag1;",
            "LX/3HT;",
            "LX/1b4;",
            "LX/AaZ;",
            "LX/Ac6;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AeV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Aee;",
            ">;",
            "LX/Aem;",
            "LX/Aev;",
            "LX/AfJ;",
            "Lcom/facebook/facecastdisplay/donation/LiveDonationController;",
            "LX/AeS;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1692811
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1692812
    iput-object p1, p0, LX/AcX;->b:LX/Ag1;

    .line 1692813
    iput-object p2, p0, LX/AcX;->c:LX/3HT;

    .line 1692814
    iput-object p3, p0, LX/AcX;->d:LX/1b4;

    .line 1692815
    iput-object p4, p0, LX/AcX;->e:LX/AaZ;

    .line 1692816
    iput-object p5, p0, LX/AcX;->f:LX/Ac6;

    .line 1692817
    iput-object p6, p0, LX/AcX;->g:LX/0Ot;

    .line 1692818
    iput-object p7, p0, LX/AcX;->h:LX/0Ot;

    .line 1692819
    iput-object p8, p0, LX/AcX;->i:LX/0Ot;

    .line 1692820
    iput-object p9, p0, LX/AcX;->j:LX/Aem;

    .line 1692821
    iput-object p10, p0, LX/AcX;->k:LX/Aev;

    .line 1692822
    iput-object p11, p0, LX/AcX;->l:LX/AfJ;

    .line 1692823
    iput-object p12, p0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    .line 1692824
    move-object/from16 v0, p13

    iput-object v0, p0, LX/AcX;->n:LX/AeS;

    .line 1692825
    move-object/from16 v0, p14

    iput-object v0, p0, LX/AcX;->p:Ljava/lang/String;

    .line 1692826
    new-instance v1, LX/AcV;

    invoke-direct {v1, p0}, LX/AcV;-><init>(LX/AcX;)V

    iput-object v1, p0, LX/AcX;->a:LX/AcV;

    .line 1692827
    iget-object v1, p0, LX/AcX;->k:LX/Aev;

    invoke-virtual {v1, p0}, LX/Aev;->a(LX/AcX;)V

    .line 1692828
    iget-object v1, p0, LX/AcX;->k:LX/Aev;

    iget-object v2, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1, v2}, LX/Aev;->a(LX/AeS;)V

    .line 1692829
    iget-object v1, p0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-virtual {v1, p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(LX/AcX;)V

    .line 1692830
    iget-object v1, p0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1692831
    const/16 v1, 0x4b

    iput v1, p0, LX/AcX;->q:I

    .line 1692832
    :goto_0
    new-instance v1, LX/AcR;

    invoke-direct {v1, p0}, LX/AcR;-><init>(LX/AcX;)V

    iput-object v1, p0, LX/AcX;->o:LX/1OX;

    .line 1692833
    iget-object v1, p0, LX/AcX;->n:LX/AeS;

    new-instance v2, LX/AcS;

    invoke-direct {v2, p0}, LX/AcS;-><init>(LX/AcX;)V

    invoke-virtual {v1, v2}, LX/1OM;->a(LX/1OD;)V

    .line 1692834
    return-void

    .line 1692835
    :cond_0
    const/16 v1, 0xf

    iput v1, p0, LX/AcX;->q:I

    goto :goto_0
.end method

.method public static a(LX/AcX;LX/AeU;)V
    .locals 2

    .prologue
    .line 1692755
    iget-object v0, p0, LX/AcX;->k:LX/Aev;

    iget-object v1, p0, LX/AcX;->l:LX/AfJ;

    .line 1692756
    iput-object v1, v0, LX/Aev;->j:LX/AfJ;

    .line 1692757
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    iget-object v1, p0, LX/AcX;->k:LX/Aev;

    .line 1692758
    iput-object v1, v0, LX/AfJ;->e:LX/Aev;

    .line 1692759
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    .line 1692760
    iput-object p1, v0, LX/AfJ;->d:LX/AeU;

    .line 1692761
    iget-object v1, v0, LX/AfJ;->d:LX/AeU;

    iget-object v1, v1, LX/AeU;->b:Ljava/lang/String;

    iput-object v1, v0, LX/AfJ;->f:Ljava/lang/String;

    .line 1692762
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    invoke-virtual {v0}, LX/AfJ;->d()V

    .line 1692763
    return-void
.end method

.method public static b(LX/0QB;)LX/AcX;
    .locals 15

    .prologue
    .line 1692773
    new-instance v0, LX/AcX;

    invoke-static {p0}, LX/Ag1;->b(LX/0QB;)LX/Ag1;

    move-result-object v1

    check-cast v1, LX/Ag1;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v2

    check-cast v2, LX/3HT;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v3

    check-cast v3, LX/1b4;

    invoke-static {p0}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v4

    check-cast v4, LX/AaZ;

    invoke-static {p0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v5

    check-cast v5, LX/Ac6;

    const/16 v6, 0x123

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1c05

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1c08

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/Aem;->a(LX/0QB;)LX/Aem;

    move-result-object v9

    check-cast v9, LX/Aem;

    invoke-static {p0}, LX/Aev;->a(LX/0QB;)LX/Aev;

    move-result-object v10

    check-cast v10, LX/Aev;

    .line 1692774
    new-instance v13, LX/AfJ;

    .line 1692775
    new-instance v14, LX/AfH;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct {v14, v11, v12}, LX/AfH;-><init>(LX/0tX;Ljava/lang/String;)V

    .line 1692776
    move-object v11, v14

    .line 1692777
    check-cast v11, LX/AfH;

    const/16 v12, 0x1c10

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v12

    check-cast v12, Landroid/os/Handler;

    invoke-direct {v13, v11, v14, v12}, LX/AfJ;-><init>(LX/AfH;LX/0Ot;Landroid/os/Handler;)V

    .line 1692778
    move-object v11, v13

    .line 1692779
    check-cast v11, LX/AfJ;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b(LX/0QB;)Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    move-result-object v12

    check-cast v12, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-static {p0}, LX/AeS;->b(LX/0QB;)LX/AeS;

    move-result-object v13

    check-cast v13, LX/AeS;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, LX/AcX;-><init>(LX/Ag1;LX/3HT;LX/1b4;LX/AaZ;LX/Ac6;LX/0Ot;LX/0Ot;LX/0Ot;LX/Aem;LX/Aev;LX/AfJ;Lcom/facebook/facecastdisplay/donation/LiveDonationController;LX/AeS;Ljava/lang/String;)V

    .line 1692780
    return-object v0
.end method

.method public static b(LX/AcX;Ljava/lang/String;ILX/AcC;)V
    .locals 3

    .prologue
    .line 1692781
    if-eqz p3, :cond_0

    .line 1692782
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1692783
    iget-object v1, p0, LX/AcX;->n:LX/AeS;

    invoke-static {p3, v0, p2}, LX/Aeu;->a(LX/AcC;Ljava/lang/String;I)LX/Aeu;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AeS;->a(LX/AeO;)V

    .line 1692784
    iget-object v1, p0, LX/AcX;->r:LX/AeJ;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1692785
    iget-object v1, p0, LX/AcX;->r:LX/AeJ;

    invoke-interface {v1, v0}, LX/AeJ;->b(Ljava/lang/String;)V

    .line 1692786
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V
    .locals 2

    .prologue
    .line 1692787
    iput-object p0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->j:LX/AcW;

    .line 1692788
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1692789
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p0, LX/AcX;->o:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1692790
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p0, LX/AcX;->y:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1692791
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    .line 1692792
    invoke-virtual {v0}, LX/9Bh;->b()V

    .line 1692793
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AeO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692794
    iget-object v0, p0, LX/AcX;->x:LX/Abz;

    sget-object v1, LX/Abz;->BROADCASTING:LX/Abz;

    if-ne v0, v1, :cond_0

    .line 1692795
    :goto_0
    return-void

    .line 1692796
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1692797
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeO;

    .line 1692798
    instance-of v3, v0, LX/Aeu;

    if-eqz v3, :cond_1

    .line 1692799
    check-cast v0, LX/Aeu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1692800
    :cond_2
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    .line 1692801
    iget-object v2, v0, LX/AfJ;->i:LX/Aeu;

    move-object v0, v2

    .line 1692802
    if-eqz v0, :cond_3

    .line 1692803
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    .line 1692804
    iget-object v2, v0, LX/AfJ;->i:LX/Aeu;

    move-object v0, v2

    .line 1692805
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692806
    :cond_3
    invoke-direct {p0, v1}, LX/AcX;->c(Ljava/util/List;)V

    goto :goto_0
.end method

.method private static c(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1692807
    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->j:LX/AcW;

    .line 1692808
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1692809
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1692810
    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Aeu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692836
    iget-object v0, p0, LX/AcX;->b:LX/Ag1;

    new-instance v1, LX/AcU;

    invoke-direct {v1, p0}, LX/AcU;-><init>(LX/AcX;)V

    invoke-virtual {v0, p1, v1}, LX/Ag1;->a(Ljava/util/List;LX/AcU;)V

    .line 1692837
    return-void
.end method

.method public static e(LX/AcX;Z)V
    .locals 2

    .prologue
    .line 1692764
    if-eqz p1, :cond_1

    .line 1692765
    iget-object v0, p0, LX/AcX;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeJ;

    iput-object v0, p0, LX/AcX;->r:LX/AeJ;

    .line 1692766
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AcX;->r:LX/AeJ;

    invoke-interface {v0, p0}, LX/AeJ;->a(LX/AcX;)V

    .line 1692767
    return-void

    .line 1692768
    :cond_1
    iget-object v0, p0, LX/AcX;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeJ;

    iput-object v0, p0, LX/AcX;->r:LX/AeJ;

    .line 1692769
    iget-object v0, p0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692770
    iget-object v0, p0, LX/AcX;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aee;

    iget-object v1, p0, LX/AcX;->l:LX/AfJ;

    .line 1692771
    iput-object v1, v0, LX/Aee;->x:LX/AfJ;

    .line 1692772
    goto :goto_0
.end method

.method public static l(LX/AcX;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1692737
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    move v1, v0

    .line 1692738
    :goto_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692739
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getLastCompletelyVisiblePosition()I

    move-result v0

    iget-object v4, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v0, v4, :cond_0

    if-eqz v1, :cond_6

    .line 1692740
    :cond_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692741
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1692742
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692743
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    .line 1692744
    invoke-virtual {v0}, LX/9Bh;->b()V

    .line 1692745
    :cond_1
    :goto_1
    iget-boolean v0, p0, LX/AcX;->z:Z

    if-nez v0, :cond_4

    .line 1692746
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692747
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v4, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    .line 1692748
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692749
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getFirstCompletelyVisiblePosition()I

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v2, v3

    :cond_3
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1692750
    :cond_4
    return-void

    :cond_5
    move v1, v2

    .line 1692751
    goto :goto_0

    .line 1692752
    :cond_6
    iget-boolean v0, p0, LX/AcX;->z:Z

    if-nez v0, :cond_1

    .line 1692753
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692754
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public static o$redex0(LX/AcX;)V
    .locals 5

    .prologue
    .line 1692714
    iget-object v0, p0, LX/AcX;->r:LX/AeJ;

    if-eqz v0, :cond_0

    .line 1692715
    iget-object v0, p0, LX/AcX;->r:LX/AeJ;

    invoke-interface {v0}, LX/AeJ;->b()V

    .line 1692716
    :cond_0
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1692717
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    invoke-virtual {v0}, LX/AfJ;->f()V

    .line 1692718
    iget-object v0, p0, LX/AcX;->e:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1692719
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    iget v1, p0, LX/AcX;->q:I

    .line 1692720
    if-gtz v1, :cond_3

    .line 1692721
    const/4 v2, 0x0

    .line 1692722
    :goto_0
    move-object v0, v2

    .line 1692723
    iget-object v1, p0, LX/AcX;->l:LX/AfJ;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/AcX;->l:LX/AfJ;

    .line 1692724
    iget-object v2, v1, LX/AfJ;->i:LX/Aeu;

    move-object v1, v2

    .line 1692725
    if-eqz v1, :cond_1

    .line 1692726
    iget-object v1, p0, LX/AcX;->l:LX/AfJ;

    .line 1692727
    iget-object v2, v1, LX/AfJ;->i:LX/Aeu;

    move-object v1, v2

    .line 1692728
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692729
    :cond_1
    invoke-direct {p0, v0}, LX/AcX;->c(Ljava/util/List;)V

    .line 1692730
    :cond_2
    return-void

    .line 1692731
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1692732
    iget-object v2, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_1
    if-ltz v4, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v1, :cond_5

    .line 1692733
    iget-object v2, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/Aeu;

    if-eqz v2, :cond_4

    .line 1692734
    iget-object v2, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Aeu;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692735
    :cond_4
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_1

    :cond_5
    move-object v2, v3

    .line 1692736
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/AeO;)V
    .locals 2

    .prologue
    .line 1692666
    iget-object v0, p0, LX/AcX;->e:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692667
    const/4 v0, 0x1

    new-array v0, v0, [LX/AeO;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, LX/AcX;->b(Ljava/util/List;)V

    .line 1692668
    :cond_0
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0, p1}, LX/AeS;->a(LX/AeO;)V

    .line 1692669
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1692702
    check-cast p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    check-cast p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    .line 1692703
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->f:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1692704
    invoke-static {p2}, LX/AcX;->c(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V

    .line 1692705
    invoke-direct {p0, p1}, LX/AcX;->b(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V

    .line 1692706
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setVisibility(I)V

    .line 1692707
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setAlpha(F)V

    .line 1692708
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-boolean v1, p0, LX/AcX;->A:Z

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setMinimized(Z)V

    .line 1692709
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1692710
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1692711
    iget-object v0, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    iget-object v1, p2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1692712
    iget-object v0, p0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->e:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1692713
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AeO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692694
    iget-object v0, p0, LX/AcX;->e:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692695
    invoke-direct {p0, p1}, LX/AcX;->b(Ljava/util/List;)V

    .line 1692696
    :cond_0
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    .line 1692697
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1692698
    :goto_0
    return-void

    .line 1692699
    :cond_1
    iget-object v1, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1692700
    invoke-static {v0}, LX/AeS;->f(LX/AeS;)V

    .line 1692701
    iget-object v1, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    sub-int/2addr v1, p0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    invoke-virtual {v0, v1, p0}, LX/1OM;->c(II)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1692688
    check-cast p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    .line 1692689
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->f:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1692690
    invoke-direct {p0, p1}, LX/AcX;->b(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V

    .line 1692691
    invoke-static {p0}, LX/AcX;->l(LX/AcX;)V

    .line 1692692
    iget-object v0, p0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->e:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1692693
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1692681
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692682
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    invoke-static {v0}, LX/AcX;->c(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V

    .line 1692683
    iget-object v0, p0, LX/AcX;->r:LX/AeJ;

    if-eqz v0, :cond_0

    .line 1692684
    iget-object v0, p0, LX/AcX;->r:LX/AeJ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/AeJ;->a(LX/AcX;)V

    .line 1692685
    :cond_0
    iget-object v0, p0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1692686
    iget-object v0, p0, LX/AcX;->l:LX/AfJ;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1692687
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1692674
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_1

    .line 1692675
    iget-object v0, p0, LX/AcX;->w:Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;

    if-nez v0, :cond_0

    .line 1692676
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692677
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;

    iput-object v0, p0, LX/AcX;->w:Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;

    .line 1692678
    iget-object v0, p0, LX/AcX;->w:Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;

    iget-object v1, p0, LX/AcX;->x:LX/Abz;

    iget-object v2, p0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/Ac6;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->a(LX/Abz;Z)V

    .line 1692679
    :cond_0
    iget-object v0, p0, LX/AcX;->w:Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setVisibility(I)V

    .line 1692680
    :cond_1
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1692670
    iget-object v0, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-lez v0, :cond_0

    .line 1692671
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1692672
    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, p0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 1692673
    :cond_0
    return-void
.end method
