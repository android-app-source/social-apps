.class public final enum LX/BgJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BgJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BgJ;

.field public static final enum HEADER:LX/BgJ;

.field public static final enum SECTIONS:LX/BgJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1807300
    new-instance v0, LX/BgJ;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/BgJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BgJ;->HEADER:LX/BgJ;

    .line 1807301
    new-instance v0, LX/BgJ;

    const-string v1, "SECTIONS"

    invoke-direct {v0, v1, v3}, LX/BgJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BgJ;->SECTIONS:LX/BgJ;

    .line 1807302
    const/4 v0, 0x2

    new-array v0, v0, [LX/BgJ;

    sget-object v1, LX/BgJ;->HEADER:LX/BgJ;

    aput-object v1, v0, v2

    sget-object v1, LX/BgJ;->SECTIONS:LX/BgJ;

    aput-object v1, v0, v3

    sput-object v0, LX/BgJ;->$VALUES:[LX/BgJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1807303
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BgJ;
    .locals 1

    .prologue
    .line 1807304
    const-class v0, LX/BgJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BgJ;

    return-object v0
.end method

.method public static values()[LX/BgJ;
    .locals 1

    .prologue
    .line 1807305
    sget-object v0, LX/BgJ;->$VALUES:[LX/BgJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BgJ;

    return-object v0
.end method
