.class public LX/AlV;
.super LX/AlU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/AkB;",
        ">",
        "LX/AlU",
        "<",
        "LX/2t8;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/Alf;

.field public final b:LX/1kG;


# direct methods
.method public constructor <init>(LX/0hB;LX/1Rg;LX/Alf;LX/1kG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709755
    invoke-virtual {p4}, LX/1kG;->g()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, LX/AlU;-><init>(LX/0hB;LX/1Rg;I)V

    .line 1709756
    iput-object p3, p0, LX/AlV;->a:LX/Alf;

    .line 1709757
    iput-object p4, p0, LX/AlV;->b:LX/1kG;

    .line 1709758
    return-void
.end method

.method public static b(LX/0QB;)LX/AlV;
    .locals 5

    .prologue
    .line 1709762
    new-instance v4, LX/AlV;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    invoke-static {p0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v1

    check-cast v1, LX/1Rg;

    invoke-static {p0}, LX/Alf;->a(LX/0QB;)LX/Alf;

    move-result-object v2

    check-cast v2, LX/Alf;

    invoke-static {p0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v3

    check-cast v3, LX/1kG;

    invoke-direct {v4, v0, v1, v2, v3}, LX/AlV;-><init>(LX/0hB;LX/1Rg;LX/Alf;LX/1kG;)V

    .line 1709763
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;Z)LX/2tA;
    .locals 2

    .prologue
    .line 1709759
    new-instance v0, LX/2t8;

    .line 1709760
    const v1, 0x7f0d2504

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    move-object v1, v1

    .line 1709761
    invoke-direct {v0, p0, v1, p2, p3}, LX/2t8;-><init>(LX/AlV;Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;Landroid/view/View;Z)V

    return-object v0
.end method
