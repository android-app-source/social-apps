.class public LX/BPp;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/BPJ;",
        "LX/BPH;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BPp;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780924
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1780925
    return-void
.end method

.method public static a(LX/0QB;)LX/BPp;
    .locals 3

    .prologue
    .line 1780926
    sget-object v0, LX/BPp;->a:LX/BPp;

    if-nez v0, :cond_1

    .line 1780927
    const-class v1, LX/BPp;

    monitor-enter v1

    .line 1780928
    :try_start_0
    sget-object v0, LX/BPp;->a:LX/BPp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1780929
    if-eqz v2, :cond_0

    .line 1780930
    :try_start_1
    new-instance v0, LX/BPp;

    invoke-direct {v0}, LX/BPp;-><init>()V

    .line 1780931
    move-object v0, v0

    .line 1780932
    sput-object v0, LX/BPp;->a:LX/BPp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780933
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1780934
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1780935
    :cond_1
    sget-object v0, LX/BPp;->a:LX/BPp;

    return-object v0

    .line 1780936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1780937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
