.class public LX/CE3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1860498
    new-instance v0, LX/CE1;

    invoke-direct {v0, v1}, LX/CE1;-><init>(I)V

    sput-object v0, LX/CE3;->a:Landroid/util/SparseArray;

    .line 1860499
    new-instance v0, LX/CE2;

    invoke-direct {v0, v1}, LX/CE2;-><init>(I)V

    sput-object v0, LX/CE3;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1860500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;LX/1X1;ILjava/lang/CharSequence;LX/0Or;ZZLX/1X5;LX/1X1;LX/1dQ;ZZ)LX/1Dg;
    .locals 8
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/1X5;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            "LX/1X1;",
            "I",
            "Ljava/lang/CharSequence;",
            "LX/0Or",
            "<",
            "Ljava/lang/CharSequence;",
            ">;ZZ",
            "LX/1X5",
            "<*>;",
            "LX/1X1",
            "<*>;",
            "LX/1dQ;",
            "ZZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1860501
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    if-eqz p11, :cond_4

    const v1, 0x7f0b1ae2

    :goto_0
    invoke-interface {v2, v3, v1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    .line 1860502
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x4

    const v4, 0x7f0b1ae4

    invoke-interface {v1, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    .line 1860503
    const/4 v1, 0x0

    const v4, 0x7f0e0129

    invoke-static {p0, v1, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    if-eqz p12, :cond_5

    sget-object v1, LX/CE3;->a:Landroid/util/SparseArray;

    :goto_1
    invoke-interface {v4, v1}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1860504
    invoke-static {p0}, LX/CE6;->c(LX/1De;)LX/CE4;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/CE4;->a(Ljava/lang/CharSequence;)LX/CE4;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/CE4;->a(LX/0Or;)LX/CE4;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1860505
    const/4 v1, 0x0

    .line 1860506
    if-nez p9, :cond_0

    if-nez p6, :cond_0

    if-eqz p7, :cond_3

    .line 1860507
    :cond_0
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x3

    invoke-interface {v1, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    .line 1860508
    if-eqz p6, :cond_1

    .line 1860509
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020aea

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    const v6, 0x7f0b1ae5

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0810fa

    invoke-interface {v4, v5}, LX/1Di;->A(I)LX/1Di;

    move-result-object v4

    sget-object v5, LX/CE3;->b:Landroid/util/SparseArray;

    invoke-interface {v4, v5}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-interface {v4, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1860510
    :cond_1
    if-eqz p7, :cond_2

    .line 1860511
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f0217ed

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    const v6, 0x7f0b1ae5

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-interface {v4, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1860512
    :cond_2
    if-eqz p8, :cond_6

    .line 1860513
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-virtual/range {p8 .. p8}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    const v7, 0x7f0b1ae5

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 1860514
    :cond_3
    :goto_2
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 1860515
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1860516
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1860517
    :cond_6
    move-object/from16 v0, p9

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/CE3;
    .locals 3

    .prologue
    .line 1860518
    const-class v1, LX/CE3;

    monitor-enter v1

    .line 1860519
    :try_start_0
    sget-object v0, LX/CE3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1860520
    sput-object v2, LX/CE3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1860521
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860522
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1860523
    new-instance v0, LX/CE3;

    invoke-direct {v0}, LX/CE3;-><init>()V

    .line 1860524
    move-object v0, v0

    .line 1860525
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1860526
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CE3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1860527
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1860528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
