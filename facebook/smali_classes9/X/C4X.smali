.class public LX/C4X;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1VF;

.field private final b:LX/0fO;

.field private final c:LX/AwF;


# direct methods
.method public constructor <init>(LX/1VF;LX/0fO;LX/AwF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847465
    iput-object p1, p0, LX/C4X;->a:LX/1VF;

    .line 1847466
    iput-object p2, p0, LX/C4X;->b:LX/0fO;

    .line 1847467
    iput-object p3, p0, LX/C4X;->c:LX/AwF;

    .line 1847468
    return-void
.end method

.method public static b(LX/0QB;)LX/C4X;
    .locals 4

    .prologue
    .line 1847528
    new-instance v3, LX/C4X;

    invoke-static {p0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v0

    check-cast v0, LX/1VF;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v1

    check-cast v1, LX/0fO;

    invoke-static {p0}, LX/AwF;->b(LX/0QB;)LX/AwF;

    move-result-object v2

    check-cast v2, LX/AwF;

    invoke-direct {v3, v0, v1, v2}, LX/C4X;-><init>(LX/1VF;LX/0fO;LX/AwF;)V

    .line 1847529
    return-object v3
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1847521
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 1847522
    :cond_1
    :goto_0
    return-object v0

    .line 1847523
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 1847524
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1847525
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x790111ba

    if-eq v5, v6, :cond_1

    .line 1847526
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 1847527
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1847493
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1847494
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1847495
    :goto_0
    return-object v0

    .line 1847496
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1847497
    invoke-static {p1}, LX/C4X;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1847498
    if-nez v0, :cond_2

    .line 1847499
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1847500
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bC()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductionPrompt;

    .line 1847501
    iget-object v5, p0, LX/C4X;->c:LX/AwF;

    .line 1847502
    const/4 v10, 0x0

    .line 1847503
    if-nez v0, :cond_6

    .line 1847504
    :cond_3
    :goto_2
    move-object v6, v10

    .line 1847505
    instance-of v7, v6, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    invoke-static {v7}, LX/0Tp;->b(Z)V

    .line 1847506
    check-cast v6, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v6

    invoke-static {v5, v6}, LX/AwF;->a(LX/AwF;Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v6

    move-object v0, v6

    .line 1847507
    if-eqz v0, :cond_4

    .line 1847508
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1847509
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1847510
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1847511
    :cond_6
    new-instance v8, LX/186;

    const/16 v9, 0x80

    invoke-direct {v8, v9}, LX/186;-><init>(I)V

    .line 1847512
    invoke-static {v8, v0}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLProductionPrompt;)I

    move-result v9

    .line 1847513
    if-eqz v9, :cond_3

    .line 1847514
    invoke-virtual {v8, v9}, LX/186;->d(I)V

    .line 1847515
    invoke-virtual {v8}, LX/186;->e()[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 1847516
    const/4 v8, 0x0

    invoke-virtual {v9, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1847517
    new-instance v8, LX/15i;

    const/4 v12, 0x1

    move-object v11, v10

    move-object v13, v10

    invoke-direct/range {v8 .. v13}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1847518
    instance-of v9, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v9, :cond_7

    .line 1847519
    const-string v9, "ProductionPromptsInfoConversionHelper.getProductionPromptsInfo"

    invoke-virtual {v8, v9, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1847520
    :cond_7
    new-instance v10, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    invoke-direct {v10, v8}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;-><init>(LX/15i;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/content/Context;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "Landroid/content/Context;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1847479
    invoke-interface {p2}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->FEED:LX/1Qt;

    if-eq v1, v2, :cond_1

    .line 1847480
    :cond_0
    :goto_0
    return v0

    .line 1847481
    :cond_1
    instance-of v1, p3, LX/0fD;

    if-eqz v1, :cond_0

    .line 1847482
    check-cast p3, LX/0fD;

    invoke-interface {p3}, LX/0fD;->f()LX/0fK;

    move-result-object v1

    .line 1847483
    invoke-virtual {v1}, LX/0fK;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/0fK;->h()LX/0gi;

    move-result-object v1

    sget-object v2, LX/0gi;->INSPIRATION_CAMERA_DIVEBAR:LX/0gi;

    if-ne v1, v2, :cond_0

    .line 1847484
    if-eqz p1, :cond_0

    .line 1847485
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1847486
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/C4X;->a:LX/1VF;

    invoke-virtual {v1, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/C4X;->b:LX/0fO;

    .line 1847487
    invoke-static {v1}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v2

    invoke-virtual {v2}, LX/0i8;->v()Z

    move-result v2

    move v1, v2

    .line 1847488
    if-eqz v1, :cond_0

    .line 1847489
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847490
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1847491
    invoke-static {v0}, LX/C4X;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1847492
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/content/Context;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "Landroid/content/Context;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1847469
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1847470
    if-nez v0, :cond_1

    move v0, v1

    .line 1847471
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, LX/C4X;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/C4X;->a:LX/1VF;

    invoke-virtual {v3, p1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v3, v1

    .line 1847472
    :goto_1
    if-eqz v0, :cond_4

    .line 1847473
    if-eqz v3, :cond_3

    .line 1847474
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 1847475
    goto :goto_0

    :cond_2
    move v3, v2

    .line 1847476
    goto :goto_1

    .line 1847477
    :cond_3
    const/4 v2, 0x3

    goto :goto_2

    .line 1847478
    :cond_4
    if-eqz v3, :cond_5

    move v2, v1

    goto :goto_2

    :cond_5
    const/4 v2, 0x2

    goto :goto_2
.end method
