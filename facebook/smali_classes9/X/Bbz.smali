.class public LX/Bbz;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(III)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1801366
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1801367
    iput p1, p0, LX/Bbz;->d:I

    .line 1801368
    iput p2, p0, LX/Bbz;->c:I

    .line 1801369
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-double v2, v0

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v2, v4

    double-to-int v6, v2

    .line 1801370
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/Bbz;->b:Landroid/graphics/Path;

    .line 1801371
    iget-object v0, p0, LX/Bbz;->b:Landroid/graphics/Path;

    int-to-float v3, p1

    int-to-float v4, p2

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 1801372
    iget-object v0, p0, LX/Bbz;->b:Landroid/graphics/Path;

    div-int/lit8 v1, p1, 0x2

    int-to-float v1, v1

    div-int/lit8 v2, p2, 0x2

    int-to-float v2, v2

    int-to-float v3, v6

    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 1801373
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    .line 1801374
    iget-object v0, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1801375
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1801376
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1801377
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget v1, p0, LX/Bbz;->d:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iget v2, p0, LX/Bbz;->c:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1801378
    iget-object v0, p0, LX/Bbz;->b:Landroid/graphics/Path;

    iget-object v1, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1801379
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1801380
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1801381
    iget-object v0, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1801382
    iget-object v0, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1801383
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1801384
    iget-object v0, p0, LX/Bbz;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1801385
    return-void
.end method
