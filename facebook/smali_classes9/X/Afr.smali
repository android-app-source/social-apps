.class public LX/Afr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Aea;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 1699791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699792
    iput-object p1, p0, LX/Afr;->a:Ljava/lang/String;

    .line 1699793
    iput-object p2, p0, LX/Afr;->b:Ljava/lang/String;

    .line 1699794
    iput-object p3, p0, LX/Afr;->c:Ljava/lang/String;

    .line 1699795
    iput-object p4, p0, LX/Afr;->d:Ljava/lang/String;

    .line 1699796
    iput p5, p0, LX/Afr;->e:I

    .line 1699797
    iput-boolean p6, p0, LX/Afr;->f:Z

    .line 1699798
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Afr;->g:Z

    .line 1699799
    return-void
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;)LX/Afr;
    .locals 7
    .param p0    # Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1699781
    if-nez p0, :cond_1

    .line 1699782
    :cond_0
    :goto_0
    return-object v0

    .line 1699783
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;

    move-result-object v2

    .line 1699784
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1699785
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->j()Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;

    move-result-object v4

    .line 1699786
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1699787
    new-instance v0, LX/Afr;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$TipGiverModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel$AmountReceivedModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel$TipJarTransactionsTimeSlicesModel$TipJarTransactionsModel;->l()I

    move-result v5

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, LX/Afr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/AeN;
    .locals 1

    .prologue
    .line 1699788
    sget-object v0, LX/AeN;->LIVE_TIP_JAR_EVENT:LX/AeN;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1699789
    iget v0, p0, LX/Afr;->e:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1699790
    iget-boolean v0, p0, LX/Afr;->f:Z

    return v0
.end method
