.class public LX/AVP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final A:Z

.field public B:Z

.field public volatile C:Z

.field public volatile D:LX/AVQ;

.field public final E:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public F:I

.field public G:J

.field public H:Ljava/nio/ByteBuffer;

.field public I:Ljava/lang/String;

.field private J:Ljava/lang/Integer;

.field public K:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/AVN;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/SurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field public final e:Landroid/os/Handler;

.field public final f:LX/AVR;

.field public g:Landroid/graphics/SurfaceTexture;

.field public final h:[F

.field public i:I

.field public j:I

.field public k:I

.field public l:F

.field private m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1bI;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1bM;",
            ">;"
        }
    .end annotation
.end field

.field public o:I

.field public p:Z

.field private volatile q:Landroid/view/Surface;

.field public r:LX/7Rn;

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AVL;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/7Rl;

.field public volatile u:Z

.field private final v:Ljava/lang/Object;

.field public w:LX/AWK;

.field private x:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "LX/AVO;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0So;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679692
    const-class v0, LX/AVP;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AVP;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1bI;LX/1bM;Landroid/view/SurfaceView;LX/AVR;LX/AWK;LX/0So;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 1679661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679662
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/AVP;->h:[F

    .line 1679663
    iput-boolean v2, p0, LX/AVP;->z:Z

    .line 1679664
    iput-boolean v2, p0, LX/AVP;->B:Z

    .line 1679665
    iput-boolean v2, p0, LX/AVP;->C:Z

    .line 1679666
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/AVP;->E:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1679667
    iput v2, p0, LX/AVP;->F:I

    .line 1679668
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/AVP;->G:J

    .line 1679669
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVP;->J:Ljava/lang/Integer;

    .line 1679670
    if-eqz p5, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must provide a non null renderere manager"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1679671
    iput v6, p0, LX/AVP;->d:I

    .line 1679672
    iput v6, p0, LX/AVP;->i:I

    .line 1679673
    iput v6, p0, LX/AVP;->j:I

    .line 1679674
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    .line 1679675
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AVP;->n:Ljava/lang/ref/WeakReference;

    .line 1679676
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    .line 1679677
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/AVP;->e:Landroid/os/Handler;

    .line 1679678
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/AVP;->v:Ljava/lang/Object;

    .line 1679679
    iput-boolean v2, p0, LX/AVP;->u:Z

    .line 1679680
    iput-object p4, p0, LX/AVP;->f:LX/AVR;

    .line 1679681
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AVP;->s:Ljava/util/List;

    .line 1679682
    iput-object p5, p0, LX/AVP;->w:LX/AWK;

    .line 1679683
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 1679684
    iput-object p6, p0, LX/AVP;->y:LX/0So;

    .line 1679685
    invoke-interface {p1}, LX/1bI;->d()F

    move-result v0

    iput v0, p0, LX/AVP;->l:F

    .line 1679686
    invoke-interface {p1}, LX/1bI;->iv_()I

    move-result v0

    iput v0, p0, LX/AVP;->k:I

    .line 1679687
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AVP;->K:Ljava/util/Map;

    .line 1679688
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    if-eqz v0, :cond_1

    if-eqz p7, :cond_1

    :goto_1
    iput-boolean v1, p0, LX/AVP;->A:Z

    .line 1679689
    return-void

    :cond_0
    move v0, v2

    .line 1679690
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1679691
    goto :goto_1
.end method

.method private static a(LX/AVP;JI)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1679636
    iget-object v1, p0, LX/AVP;->w:LX/AWK;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/AVP;->z:Z

    if-nez v1, :cond_1

    .line 1679637
    :cond_0
    :goto_0
    return-void

    .line 1679638
    :cond_1
    iget-object v2, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v2

    move v1, v0

    .line 1679639
    :goto_1
    :try_start_0
    iget-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1679640
    iget-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVO;

    .line 1679641
    iget-object v3, v0, LX/AVO;->f:LX/7Rn;

    if-eqz v3, :cond_6

    iget-boolean v3, v0, LX/AVO;->g:Z

    if-nez v3, :cond_2

    iget-boolean v3, v0, LX/AVO;->h:Z

    if-eqz v3, :cond_6

    :cond_2
    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 1679642
    if-eqz v3, :cond_3

    .line 1679643
    iget-object v3, v0, LX/AVO;->f:LX/7Rn;

    move-object v3, v3

    .line 1679644
    invoke-virtual {v3}, LX/7Rm;->b()V

    .line 1679645
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1679646
    iget-object v6, v0, LX/AVO;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/SurfaceView;

    invoke-virtual {v6}, Landroid/view/SurfaceView;->getWidth()I

    move-result v6

    move v6, v6

    .line 1679647
    iget-object v7, v0, LX/AVO;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getHeight()I

    move-result v7

    move v7, v7

    .line 1679648
    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1679649
    iget-object v4, p0, LX/AVP;->w:LX/AWK;

    iget-object v5, p0, LX/AVP;->h:[F

    .line 1679650
    iget-object v6, v0, LX/AVO;->d:LX/61B;

    move-object v6, v6

    .line 1679651
    const/4 v7, 0x1

    .line 1679652
    iget-object v8, v4, LX/AWK;->a:LX/AWL;

    iget-object v9, v4, LX/AWK;->d:LX/5Pf;

    invoke-virtual {v8, v5, v9, v6, v7}, LX/AWL;->a([FLX/5Pf;LX/61B;Z)V

    .line 1679653
    invoke-virtual {v3}, LX/7Rm;->c()Z

    .line 1679654
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/AVO;->h:Z

    .line 1679655
    :cond_3
    iget-object v3, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->addLast(Ljava/lang/Object;)V

    .line 1679656
    iget-object v0, p0, LX/AVP;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v4, p1

    int-to-long v6, p3

    cmp-long v0, v4, v6

    if-ltz v0, :cond_4

    .line 1679657
    monitor-exit v2

    goto :goto_0

    .line 1679658
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1679659
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1679660
    :cond_5
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static b(LX/AVP;LX/AVO;)V
    .locals 2

    .prologue
    .line 1679629
    iget-object v1, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v1

    .line 1679630
    :try_start_0
    iget-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->remove(Ljava/lang/Object;)Z

    .line 1679631
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679632
    iget-object v0, p1, LX/AVO;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1679633
    invoke-static {p1}, LX/AVO;->b$redex0(LX/AVO;)V

    .line 1679634
    return-void

    .line 1679635
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static b$redex0(LX/AVP;II)V
    .locals 2

    .prologue
    .line 1679625
    mul-int v0, p1, p2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    .line 1679626
    iget-object v0, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1679627
    iget-object v0, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1679628
    return-void
.end method

.method public static e(LX/AVP;II)V
    .locals 7

    .prologue
    .line 1679612
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    if-eqz v0, :cond_1

    .line 1679613
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    .line 1679614
    iget-object v1, v0, LX/AWK;->a:LX/AWL;

    .line 1679615
    iget-object v2, v1, LX/AWL;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/61B;

    .line 1679616
    invoke-interface {v2, p1, p2}, LX/61B;->a(II)V

    goto :goto_0

    .line 1679617
    :cond_0
    iget-object v1, v0, LX/AWK;->i:LX/7Se;

    invoke-virtual {v1, p1, p2}, LX/7Se;->a(II)V

    .line 1679618
    iget v0, p0, LX/AVP;->i:I

    if-lez v0, :cond_1

    iget v0, p0, LX/AVP;->j:I

    if-lez v0, :cond_1

    .line 1679619
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    iget v1, p0, LX/AVP;->i:I

    iget v2, p0, LX/AVP;->j:I

    iget v3, p0, LX/AVP;->k:I

    iget-object v4, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getWidth()I

    move-result v4

    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v5

    iget v6, p0, LX/AVP;->l:F

    invoke-virtual/range {v0 .. v6}, LX/AWK;->a(IIIIIF)V

    .line 1679620
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    iget v1, p0, LX/AVP;->i:I

    iget v2, p0, LX/AVP;->j:I

    iget-boolean v3, p0, LX/AVP;->p:Z

    iget v4, p0, LX/AVP;->k:I

    iget v5, p0, LX/AVP;->o:I

    invoke-static {v3, v4, v5}, LX/7ev;->a(ZII)I

    move-result v3

    iget-boolean v4, p0, LX/AVP;->p:Z

    .line 1679621
    iget-object v5, v0, LX/AWK;->j:LX/BAC;

    if-eqz v5, :cond_1

    .line 1679622
    iget-object v5, v0, LX/AWK;->j:LX/BAC;

    .line 1679623
    iget-object v0, v5, LX/BAC;->d:LX/BA8;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BA8;->a(IIIZ)V

    .line 1679624
    :cond_1
    return-void
.end method

.method public static f(LX/AVP;II)V
    .locals 13

    .prologue
    .line 1679574
    iget-object v1, p0, LX/AVP;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1679575
    :try_start_0
    iget v0, p0, LX/AVP;->i:I

    if-lez v0, :cond_0

    iget v0, p0, LX/AVP;->j:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/AVP;->u:Z

    if-nez v0, :cond_1

    .line 1679576
    :cond_0
    monitor-exit v1

    .line 1679577
    :goto_0
    return-void

    .line 1679578
    :cond_1
    iget-object v0, p0, LX/AVP;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 1679579
    iget-boolean v0, p0, LX/AVP;->B:Z

    if-eqz v0, :cond_2

    .line 1679580
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    invoke-interface {v0}, LX/1bI;->a()V

    .line 1679581
    :cond_2
    iget-object v0, p0, LX/AVP;->r:LX/7Rn;

    invoke-virtual {v0}, LX/7Rm;->b()V

    .line 1679582
    iget-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1679583
    iget-boolean v0, p0, LX/AVP;->A:Z

    if-eqz v0, :cond_3

    .line 1679584
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    iget-object v4, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    const v8, 0x8d40

    const/4 v7, 0x0

    .line 1679585
    iget-object v5, v0, LX/AWK;->e:LX/5PO;

    iget v5, v5, LX/5PO;->c:I

    invoke-static {v8, v5}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1679586
    iget-object v5, v0, LX/AWK;->e:LX/5PO;

    iget v5, v5, LX/5PO;->a:I

    iget-object v6, v0, LX/AWK;->e:LX/5PO;

    iget v6, v6, LX/5PO;->b:I

    invoke-static {v7, v7, v5, v6}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1679587
    iget-object v5, v0, LX/AWK;->c:[F

    invoke-virtual {v4, v5}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1679588
    iget-object v5, v0, LX/AWK;->c:[F

    invoke-virtual {v0, v5}, LX/AWK;->b([F)V

    .line 1679589
    invoke-static {v8, v7}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1679590
    :cond_3
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 1679591
    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v8

    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v5

    invoke-static {v7, v7, v8, v5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1679592
    invoke-static {v9, v9, v9, v9}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1679593
    const/16 v5, 0x4000

    invoke-static {v5}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1679594
    iget-boolean v5, p0, LX/AVP;->C:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, LX/AVP;->E:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v5

    if-nez v5, :cond_8

    move v5, v6

    .line 1679595
    :goto_1
    iget-object v6, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    iget-object v8, p0, LX/AVP;->h:[F

    invoke-virtual {v6, v8}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1679596
    iget-boolean v6, p0, LX/AVP;->A:Z

    if-eqz v6, :cond_9

    .line 1679597
    iget-object v6, p0, LX/AVP;->w:LX/AWK;

    invoke-virtual {v6}, LX/AWK;->e()V

    .line 1679598
    :goto_2
    if-eqz v5, :cond_5

    .line 1679599
    iput-boolean v7, p0, LX/AVP;->C:Z

    .line 1679600
    iget-object v5, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    if-nez v5, :cond_4

    .line 1679601
    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v6

    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v5

    invoke-static {p0, v6, v5}, LX/AVP;->b$redex0(LX/AVP;II)V

    .line 1679602
    :cond_4
    iget-object v5, p0, LX/AVP;->f:LX/AVR;

    iget-object v6, p0, LX/AVP;->I:Ljava/lang/String;

    iget-object v7, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    iget-object v8, p0, LX/AVP;->h:[F

    iget-object v9, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/SurfaceView;

    invoke-virtual {v9}, Landroid/view/SurfaceView;->getWidth()I

    move-result v9

    iget-object v10, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/SurfaceView;

    invoke-virtual {v10}, Landroid/view/SurfaceView;->getHeight()I

    move-result v10

    iget-object v11, p0, LX/AVP;->E:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v12, p0, LX/AVP;->D:LX/AVQ;

    invoke-virtual/range {v5 .. v12}, LX/AVR;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;[FIILjava/util/concurrent/atomic/AtomicBoolean;LX/AVQ;)V

    .line 1679603
    :cond_5
    iget-object v5, p0, LX/AVP;->r:LX/7Rn;

    invoke-virtual {v5}, LX/7Rm;->c()Z

    .line 1679604
    iget-object v0, p0, LX/AVP;->J:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/AVP;->J:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_7

    .line 1679605
    :cond_6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/AVP;->J:Ljava/lang/Integer;

    .line 1679606
    :goto_3
    invoke-static {p0, v2, v3, p2}, LX/AVP;->a(LX/AVP;JI)V

    .line 1679607
    iget v0, p0, LX/AVP;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/AVP;->F:I

    .line 1679608
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1679609
    :cond_7
    :try_start_1
    invoke-direct {p0}, LX/AVP;->n()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_8
    move v5, v7

    .line 1679610
    goto/16 :goto_1

    .line 1679611
    :cond_9
    iget-object v6, p0, LX/AVP;->w:LX/AWK;

    iget-object v8, p0, LX/AVP;->h:[F

    invoke-virtual {v6, v8}, LX/AWK;->b([F)V

    goto :goto_2
.end method

.method public static l(LX/AVP;)LX/AWK;
    .locals 1

    .prologue
    .line 1679571
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    instance-of v0, v0, LX/AWK;

    if-eqz v0, :cond_0

    .line 1679572
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    check-cast v0, LX/AWK;

    .line 1679573
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/AVP;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1679534
    iget-object v7, p0, LX/AVP;->v:Ljava/lang/Object;

    monitor-enter v7

    .line 1679535
    :try_start_0
    iget-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/AVP;->u:Z

    if-eqz v0, :cond_1

    .line 1679536
    :cond_0
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    iget-object v1, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1}, LX/1bI;->a(Landroid/graphics/SurfaceTexture;)V

    .line 1679537
    monitor-exit v7

    .line 1679538
    :goto_0
    return-void

    .line 1679539
    :cond_1
    new-instance v0, LX/7Rl;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4}, LX/7Rl;-><init>(Landroid/opengl/EGLContext;I)V

    iput-object v0, p0, LX/AVP;->t:LX/7Rl;

    .line 1679540
    new-instance v0, LX/7Rn;

    iget-object v3, p0, LX/AVP;->t:LX/7Rl;

    iget-object v4, p0, LX/AVP;->q:Landroid/view/Surface;

    const/4 v5, 0x0

    invoke-direct {v0, v3, v4, v5}, LX/7Rn;-><init>(LX/7Rl;Landroid/view/Surface;Z)V

    iput-object v0, p0, LX/AVP;->r:LX/7Rn;

    .line 1679541
    iget-object v0, p0, LX/AVP;->r:LX/7Rn;

    invoke-virtual {v0}, LX/7Rm;->b()V

    .line 1679542
    iget-object v0, p0, LX/AVP;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, p0, LX/AVP;->G:J

    .line 1679543
    iget-object v3, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1679544
    :try_start_1
    iget-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVO;

    .line 1679545
    invoke-static {v0}, LX/AVO;->i$redex0(LX/AVO;)V

    goto :goto_1

    .line 1679546
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 1679547
    :catchall_1
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1679548
    :cond_2
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1679549
    :try_start_4
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    .line 1679550
    iget-object v3, v0, LX/AWK;->a:LX/AWL;

    .line 1679551
    iget-object v4, v3, LX/AWL;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/61B;

    .line 1679552
    iget-object v6, v3, LX/AWL;->b:LX/5Pc;

    invoke-interface {v4, v6}, LX/61B;->a(LX/5Pc;)V

    goto :goto_2

    .line 1679553
    :cond_3
    const v6, 0x812f

    const/16 v5, 0x2601

    .line 1679554
    new-instance v3, LX/5Pe;

    invoke-direct {v3}, LX/5Pe;-><init>()V

    const v4, 0x8d65

    .line 1679555
    iput v4, v3, LX/5Pe;->a:I

    .line 1679556
    move-object v3, v3

    .line 1679557
    const/16 v4, 0x2801

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2800

    invoke-virtual {v3, v4, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2802

    invoke-virtual {v3, v4, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    const/16 v4, 0x2803

    invoke-virtual {v3, v4, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v3

    invoke-virtual {v3}, LX/5Pe;->a()LX/5Pf;

    move-result-object v3

    move-object v3, v3

    .line 1679558
    iput-object v3, v0, LX/AWK;->d:LX/5Pf;

    .line 1679559
    iget-object v3, v0, LX/AWK;->i:LX/7Se;

    iget-object v4, v0, LX/AWK;->b:LX/5Pc;

    invoke-virtual {v3, v4}, LX/7Se;->a(LX/5Pc;)V

    .line 1679560
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    .line 1679561
    iget-object v3, v0, LX/AWK;->d:LX/5Pf;

    iget v3, v3, LX/5Pf;->b:I

    move v0, v3

    .line 1679562
    iput v0, p0, LX/AVP;->d:I

    .line 1679563
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v3, p0, LX/AVP;->d:I

    invoke-direct {v0, v3}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    .line 1679564
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    const-string v1, "Display listener delegate is not set."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1679565
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    iget-object v1, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1}, LX/1bI;->a(Landroid/graphics/SurfaceTexture;)V

    .line 1679566
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AVP;->u:Z

    .line 1679567
    iget v0, p0, LX/AVP;->i:I

    if-lez v0, :cond_4

    iget v0, p0, LX/AVP;->j:I

    if-lez v0, :cond_4

    .line 1679568
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    iget v1, p0, LX/AVP;->i:I

    iget v2, p0, LX/AVP;->j:I

    iget v3, p0, LX/AVP;->k:I

    iget-object v4, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getWidth()I

    move-result v4

    iget-object v5, p0, LX/AVP;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/SurfaceView;

    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v5

    iget v6, p0, LX/AVP;->l:F

    invoke-virtual/range {v0 .. v6}, LX/AWK;->a(IIIIIF)V

    .line 1679569
    :cond_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 1679570
    goto :goto_3
.end method

.method private n()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1679511
    iget-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1679512
    :goto_0
    return-void

    .line 1679513
    :cond_0
    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVL;

    .line 1679514
    iget-boolean v2, v0, LX/AVL;->d:Z

    move v2, v2

    .line 1679515
    if-eqz v2, :cond_1

    .line 1679516
    iget-object v2, v0, LX/AVL;->b:LX/7Rn;

    move-object v2, v2

    .line 1679517
    invoke-virtual {v2}, LX/7Rm;->b()V

    .line 1679518
    invoke-virtual {v0}, LX/AVL;->b()I

    move-result v3

    .line 1679519
    iget-object v4, v0, LX/AVL;->c:LX/7Re;

    .line 1679520
    iget v0, v4, LX/7Re;->b:I

    move v4, v0

    .line 1679521
    move v0, v4

    .line 1679522
    invoke-static {v7, v7, v3, v0}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1679523
    invoke-static {v6, v6, v6, v6}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1679524
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1679525
    iget-boolean v0, p0, LX/AVP;->A:Z

    if-eqz v0, :cond_2

    .line 1679526
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    invoke-virtual {v0}, LX/AWK;->e()V

    .line 1679527
    :goto_2
    invoke-virtual {v2}, LX/7Rm;->c()Z

    .line 1679528
    iget-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v4

    .line 1679529
    iget-object v0, v2, LX/7Rm;->b:LX/7Rl;

    iget-object v3, v2, LX/7Rm;->c:Landroid/opengl/EGLSurface;

    .line 1679530
    iget-object v8, v0, LX/7Rl;->b:Landroid/opengl/EGLDisplay;

    invoke-static {v8, v3, v4, v5}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 1679531
    goto :goto_1

    .line 1679532
    :cond_2
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    iget-object v3, p0, LX/AVP;->h:[F

    invoke-virtual {v0, v3}, LX/AWK;->b([F)V

    goto :goto_2

    .line 1679533
    :cond_3
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    invoke-interface {v0}, LX/1bI;->a()V

    goto :goto_0
.end method

.method public static p(LX/AVP;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1679482
    iget-object v0, p0, LX/AVP;->r:LX/7Rn;

    if-eqz v0, :cond_0

    .line 1679483
    iget-object v0, p0, LX/AVP;->r:LX/7Rn;

    invoke-virtual {v0}, LX/7Rn;->d()V

    .line 1679484
    iput-object v3, p0, LX/AVP;->r:LX/7Rn;

    .line 1679485
    :cond_0
    iget-object v1, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v1

    .line 1679486
    :try_start_0
    iget-object v0, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVO;

    .line 1679487
    invoke-static {v0}, LX/AVO;->b$redex0(LX/AVO;)V

    goto :goto_0

    .line 1679488
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679489
    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVL;

    .line 1679490
    invoke-virtual {v0}, LX/AVL;->d()V

    goto :goto_1

    .line 1679491
    :cond_2
    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1679492
    iget-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_3

    .line 1679493
    iget-object v0, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1679494
    iput-object v3, p0, LX/AVP;->g:Landroid/graphics/SurfaceTexture;

    .line 1679495
    :cond_3
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    if-eqz v0, :cond_4

    .line 1679496
    iget-object v0, p0, LX/AVP;->w:LX/AWK;

    invoke-virtual {v0}, LX/AWK;->c()V

    .line 1679497
    :cond_4
    iget-object v0, p0, LX/AVP;->t:LX/7Rl;

    if-eqz v0, :cond_5

    .line 1679498
    iget-object v0, p0, LX/AVP;->t:LX/7Rl;

    invoke-virtual {v0}, LX/7Rl;->a()V

    .line 1679499
    iput-object v3, p0, LX/AVP;->t:LX/7Rl;

    .line 1679500
    :cond_5
    iput-object v3, p0, LX/AVP;->J:Ljava/lang/Integer;

    .line 1679501
    iput v4, p0, LX/AVP;->i:I

    iput v4, p0, LX/AVP;->j:I

    .line 1679502
    iput v4, p0, LX/AVP;->d:I

    .line 1679503
    invoke-static {p0}, LX/AVP;->v(LX/AVP;)V

    .line 1679504
    iput-boolean v5, p0, LX/AVP;->u:Z

    .line 1679505
    iput v5, p0, LX/AVP;->F:I

    .line 1679506
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/AVP;->G:J

    .line 1679507
    invoke-static {p0}, LX/AVP;->s(LX/AVP;)LX/1bI;

    move-result-object v0

    .line 1679508
    if-eqz v0, :cond_6

    .line 1679509
    invoke-interface {v0}, LX/1bI;->b()V

    .line 1679510
    :cond_6
    return-void
.end method

.method public static r(LX/AVP;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 1679460
    invoke-static {p0}, LX/AVP;->t(LX/AVP;)LX/1bM;

    move-result-object v1

    .line 1679461
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/AVP;->t:LX/7Rl;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1679462
    invoke-interface {v1}, LX/1bM;->k()Ljava/util/List;

    move-result-object v0

    .line 1679463
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Re;

    .line 1679464
    new-instance v3, LX/AVL;

    invoke-direct {v3}, LX/AVL;-><init>()V

    .line 1679465
    iget-object v4, p0, LX/AVP;->t:LX/7Rl;

    const/4 v10, 0x0

    .line 1679466
    iget-object v6, v3, LX/AVL;->b:LX/7Rn;

    if-nez v6, :cond_3

    if-eqz v4, :cond_3

    .line 1679467
    iput-object v0, v3, LX/AVL;->c:LX/7Re;

    .line 1679468
    iget-object v6, v3, LX/AVL;->c:LX/7Re;

    if-eqz v6, :cond_2

    iget-object v6, v3, LX/AVL;->c:LX/7Re;

    .line 1679469
    iget-object v7, v6, LX/7Re;->c:Landroid/view/Surface;

    move-object v6, v7

    .line 1679470
    invoke-virtual {v6}, Landroid/view/Surface;->isValid()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1679471
    new-instance v6, LX/7Rn;

    iget-object v7, v3, LX/AVL;->c:LX/7Re;

    .line 1679472
    iget-object v8, v7, LX/7Re;->c:Landroid/view/Surface;

    move-object v7, v8

    .line 1679473
    invoke-direct {v6, v4, v7, v10}, LX/7Rn;-><init>(LX/7Rl;Landroid/view/Surface;Z)V

    iput-object v6, v3, LX/AVL;->b:LX/7Rn;

    .line 1679474
    :goto_1
    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1679475
    :cond_0
    iput v5, p0, LX/AVP;->F:I

    .line 1679476
    iget-object v0, p0, LX/AVP;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/AVP;->G:J

    .line 1679477
    invoke-interface {v1}, LX/1bM;->m()V

    .line 1679478
    :goto_2
    return-void

    .line 1679479
    :cond_1
    sget-object v0, LX/AVP;->a:Ljava/lang/String;

    const-string v2, "Discarding the recorder init surface request.Delegate %s, EglCore %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    const/4 v1, 0x1

    iget-object v4, p0, LX/AVP;->t:LX/7Rl;

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1679480
    :cond_2
    sget-object v6, LX/AVL;->a:Ljava/lang/String;

    const-string v7, "The recorder surface invalid"

    invoke-static {v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1679481
    :cond_3
    sget-object v6, LX/AVL;->a:Ljava/lang/String;

    const-string v7, "Discarding the recorder init surface request.Current encode surface %s, EglCore %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, v3, LX/AVL;->b:LX/7Rn;

    aput-object v9, v8, v10

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-static {v6, v7, v8}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static s(LX/AVP;)LX/1bI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1679405
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1679406
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    .line 1679407
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/AVP;)LX/1bM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1679457
    iget-object v0, p0, LX/AVP;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1679458
    iget-object v0, p0, LX/AVP;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bM;

    .line 1679459
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(LX/AVP;)V
    .locals 3

    .prologue
    .line 1679452
    iget-object v0, p0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVL;

    .line 1679453
    const/4 v2, 0x0

    .line 1679454
    iput-boolean v2, v0, LX/AVL;->d:Z

    .line 1679455
    goto :goto_0

    .line 1679456
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/SurfaceView;LX/61B;)LX/AVO;
    .locals 5

    .prologue
    .line 1679439
    const/4 v0, 0x0

    .line 1679440
    iget-object v1, p0, LX/AVP;->w:LX/AWK;

    if-eqz v1, :cond_1

    .line 1679441
    iget-object v2, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    monitor-enter v2

    .line 1679442
    :try_start_0
    iget-object v1, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVO;

    .line 1679443
    iget-object v4, v0, LX/AVO;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_3

    .line 1679444
    sget-object v1, LX/AVP;->a:Ljava/lang/String;

    const-string v4, "Secondary preview surface already added"

    invoke-static {v1, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v1, v0

    .line 1679445
    goto :goto_0

    .line 1679446
    :cond_0
    if-nez v1, :cond_2

    .line 1679447
    new-instance v0, LX/AVO;

    invoke-direct {v0, p0, p1, p2}, LX/AVO;-><init>(LX/AVP;Landroid/view/SurfaceView;LX/61B;)V

    .line 1679448
    iget-object v1, p0, LX/AVP;->x:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    .line 1679449
    :goto_2
    monitor-exit v2

    .line 1679450
    :cond_1
    return-object v0

    .line 1679451
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 1679437
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p2, v3}, LX/AVN;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679438
    return-void
.end method

.method public final a(LX/BA6;)V
    .locals 3

    .prologue
    .line 1679435
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/16 v2, 0x13

    invoke-virtual {v1, v2, p1}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679436
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7S9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679433
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/16 v2, 0x11

    invoke-virtual {v1, v2, p1}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679434
    return-void
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    .prologue
    .line 1679423
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    .line 1679424
    iget-object v0, p0, LX/AVP;->H:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 1679425
    invoke-static {p0, p3, p4}, LX/AVP;->b$redex0(LX/AVP;II)V

    .line 1679426
    :cond_0
    iget-object v1, p0, LX/AVP;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1679427
    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    .line 1679428
    iget-object v0, p0, LX/AVP;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bI;

    invoke-interface {v0}, LX/1bI;->iv_()I

    move-result v0

    iput v0, p0, LX/AVP;->k:I

    .line 1679429
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679430
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p3, p4}, LX/AVN;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679431
    return-void

    .line 1679432
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 1679416
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    .line 1679417
    iget-object v1, p0, LX/AVP;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1679418
    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    .line 1679419
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679420
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679421
    return-void

    .line 1679422
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 1679408
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    .line 1679409
    iget-object v1, p0, LX/AVP;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 1679410
    :try_start_0
    iget-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 1679411
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVP;->q:Landroid/view/Surface;

    .line 1679412
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679413
    iget-object v0, p0, LX/AVP;->b:LX/AVN;

    iget-object v1, p0, LX/AVP;->b:LX/AVN;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, LX/AVN;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVN;->sendMessage(Landroid/os/Message;)Z

    .line 1679414
    return-void

    .line 1679415
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
