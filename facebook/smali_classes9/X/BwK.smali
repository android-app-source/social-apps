.class public final LX/BwK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/3Gn;


# direct methods
.method public constructor <init>(LX/3Gn;I)V
    .locals 0

    .prologue
    .line 1833593
    iput-object p1, p0, LX/BwK;->b:LX/3Gn;

    iput p2, p0, LX/BwK;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1833594
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1833595
    iget v1, p0, LX/BwK;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 1833596
    iget-object v2, p0, LX/BwK;->b:LX/3Gn;

    iget-object v2, v2, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    float-to-int v1, v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1833597
    iget-object v1, p0, LX/BwK;->b:LX/3Gn;

    iget-object v1, v1, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->requestLayout()V

    .line 1833598
    iget-object v1, p0, LX/BwK;->b:LX/3Gn;

    iget-object v1, v1, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->invalidate()V

    .line 1833599
    iget-object v1, p0, LX/BwK;->b:LX/3Gn;

    iget-object v1, v1, LX/3Gn;->b:Landroid/widget/LinearLayout;

    const v2, 0x3f333333    # 0.7f

    const v3, 0x3e99999a    # 0.3f

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1833600
    return-void
.end method
