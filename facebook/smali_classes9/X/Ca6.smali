.class public LX/Ca6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public b:LX/Ca5;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(LX/CZz;)V
    .locals 4

    .prologue
    .line 1917027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1917028
    sget-object v0, LX/Ca5;->PHOTO:LX/Ca5;

    .line 1917029
    iput-object v0, p0, LX/Ca6;->b:LX/Ca5;

    .line 1917030
    iget-object v0, p1, LX/CZz;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1917031
    iput-object v0, p0, LX/Ca6;->c:Ljava/lang/String;

    .line 1917032
    const/4 v0, 0x0

    .line 1917033
    iput-object v0, p0, LX/Ca6;->d:Ljava/lang/String;

    .line 1917034
    iget-wide v2, p1, LX/74w;->a:J

    move-wide v0, v2

    .line 1917035
    iput-wide v0, p0, LX/Ca6;->e:J

    .line 1917036
    iget v0, p1, LX/CZz;->i:I

    move v0, v0

    .line 1917037
    iput v0, p0, LX/Ca6;->f:I

    .line 1917038
    iget v0, p1, LX/CZz;->j:I

    move v0, v0

    .line 1917039
    iput v0, p0, LX/Ca6;->g:I

    .line 1917040
    iget-boolean v0, p1, LX/CZz;->k:Z

    move v0, v0

    .line 1917041
    iput-boolean v0, p0, LX/Ca6;->h:Z

    .line 1917042
    iget-boolean v0, p1, LX/CZz;->l:Z

    move v0, v0

    .line 1917043
    iput-boolean v0, p0, LX/Ca6;->i:Z

    .line 1917044
    iget-boolean v0, p1, LX/CZz;->m:Z

    move v0, v0

    .line 1917045
    iput-boolean v0, p0, LX/Ca6;->j:Z

    .line 1917046
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1917048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1917049
    iput-object p1, p0, LX/Ca6;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1917050
    sget-object v0, LX/Ca5;->STORY:LX/Ca5;

    .line 1917051
    iput-object v0, p0, LX/Ca6;->b:LX/Ca5;

    .line 1917052
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1917053
    iput-object v0, p0, LX/Ca6;->c:Ljava/lang/String;

    .line 1917054
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    .line 1917055
    iput-object v0, p0, LX/Ca6;->d:Ljava/lang/String;

    .line 1917056
    const-wide/16 v0, -0x1

    .line 1917057
    iput-wide v0, p0, LX/Ca6;->e:J

    .line 1917058
    invoke-static {p1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 1917059
    iput v0, p0, LX/Ca6;->f:I

    .line 1917060
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 1917061
    iput v0, p0, LX/Ca6;->g:I

    .line 1917062
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v0

    .line 1917063
    iput-boolean v0, p0, LX/Ca6;->h:Z

    .line 1917064
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    .line 1917065
    iput-boolean v0, p0, LX/Ca6;->i:Z

    .line 1917066
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    .line 1917067
    iput-boolean v0, p0, LX/Ca6;->j:Z

    .line 1917068
    return-void
.end method


# virtual methods
.method public final h()Z
    .locals 1

    .prologue
    .line 1917047
    iget v0, p0, LX/Ca6;->f:I

    if-nez v0, :cond_0

    iget v0, p0, LX/Ca6;->g:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
