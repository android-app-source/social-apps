.class public final LX/Ar0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 0

    .prologue
    .line 1718609
    iput-object p1, p0, LX/Ar0;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 1718610
    iget-object v0, p0, LX/Ar0;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 1718611
    iget-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aq:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->L:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->at:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xbb8

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->D(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->W:LX/AtM;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    if-eqz v1, :cond_1

    .line 1718612
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    .line 1718613
    iget-object v5, v1, LX/Ass;->m:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 1718614
    iget-object v5, v1, LX/Ass;->d:LX/Aw7;

    iget-object v6, v1, LX/Ass;->m:Landroid/view/View;

    .line 1718615
    iget-object v7, v5, LX/Aw7;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0iA;

    const-string v8, "4487"

    const-class v9, LX/2rP;

    invoke-virtual {v7, v8, v9}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v7

    check-cast v7, LX/2rP;

    .line 1718616
    if-eqz v7, :cond_0

    .line 1718617
    iput-object v6, v7, LX/2rP;->c:Landroid/view/View;

    .line 1718618
    sget-object v7, LX/Aw7;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const/16 v8, 0x3e8

    .line 1718619
    new-instance v10, Landroid/os/Handler;

    invoke-direct {v10}, Landroid/os/Handler;-><init>()V

    .line 1718620
    new-instance v11, Lcom/facebook/friendsharing/inspiration/nux/tooltip/InspirationTooltipManager$1;

    invoke-direct {v11, v5, v7}, Lcom/facebook/friendsharing/inspiration/nux/tooltip/InspirationTooltipManager$1;-><init>(LX/Aw7;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 1718621
    int-to-long v12, v8

    const v14, 0x7d375fa1

    invoke-static {v10, v11, v12, v13, v14}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1718622
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aq:Z

    .line 1718623
    :cond_1
    return-void
.end method
