.class public final LX/BGG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/compactdisk/ManualWrite;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1766889
    iput-object p1, p0, LX/BGG;->b:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iput-object p2, p0, LX/BGG;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final write(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 1766890
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/BGG;->a:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 1766891
    :try_start_0
    invoke-static {v2, p1}, LX/7ew;->a(Ljava/io/InputStream;Ljava/lang/String;)I

    move-result v0

    .line 1766892
    iget-object v3, p0, LX/BGG;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1766893
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    return v0

    .line 1766894
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766895
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_0
.end method
