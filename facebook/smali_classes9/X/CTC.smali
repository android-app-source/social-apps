.class public LX/CTC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3rL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Landroid/widget/FrameLayout;

.field private final B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

.field private final C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

.field public final D:Landroid/widget/LinearLayout;

.field private final E:Landroid/widget/LinearLayout;

.field private final F:Landroid/view/View;

.field public final b:LX/CSz;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CXd;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/CSc;

.field private final f:LX/CSg;

.field public final g:LX/CT8;

.field public final h:LX/CT9;

.field public final i:LX/CT6;

.field private final j:LX/CT7;

.field public final k:LX/CT1;

.field public final l:LX/CT3;

.field public final m:LX/63W;

.field private final n:LX/CSQ;

.field public final o:LX/CST;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field public s:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/3rL",
            "<",
            "LX/CUD;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$PlatformStorageToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public v:LX/3rL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Landroid/support/v7/widget/RecyclerView;

.field public final x:LX/CXY;

.field public final y:Lcom/facebook/fig/button/FigButton;

.field private final z:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1895607
    new-instance v0, LX/3rL;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, LX/CTC;->a:LX/3rL;

    return-void
.end method

.method public constructor <init>(LX/CSz;Lcom/facebook/content/SecureContextHelper;LX/0Ot;Ljava/lang/String;Ljava/lang/String;Landroid/support/v7/widget/RecyclerView;LX/CXY;Lcom/facebook/fig/button/FigButton;Landroid/view/View;Landroid/widget/FrameLayout;Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;LX/CSQ;LX/CST;Ljava/lang/String;)V
    .locals 3
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CXY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/fig/button/FigButton;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Landroid/widget/LinearLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Landroid/widget/LinearLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # LX/CSQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/CST;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p18    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CSz;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/CXd;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/support/v7/widget/RecyclerView;",
            "LX/CXY;",
            "Lcom/facebook/fig/button/FigButton;",
            "Landroid/view/View;",
            "Landroid/widget/FrameLayout;",
            "Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;",
            "Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;",
            "Landroid/widget/LinearLayout;",
            "Landroid/widget/LinearLayout;",
            "Landroid/view/View;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Activity$ActivityResultHandlerRegistrationProvider;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Navigation$NavigationProvider;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1895573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895574
    new-instance v1, LX/CT2;

    invoke-direct {v1, p0}, LX/CT2;-><init>(LX/CTC;)V

    iput-object v1, p0, LX/CTC;->m:LX/63W;

    .line 1895575
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895576
    iput-object p1, p0, LX/CTC;->b:LX/CSz;

    .line 1895577
    iput-object p2, p0, LX/CTC;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1895578
    iput-object p3, p0, LX/CTC;->d:LX/0Ot;

    .line 1895579
    invoke-direct {p0}, LX/CTC;->j()LX/CT6;

    move-result-object v1

    iput-object v1, p0, LX/CTC;->i:LX/CT6;

    .line 1895580
    invoke-direct {p0}, LX/CTC;->k()LX/CT7;

    move-result-object v1

    iput-object v1, p0, LX/CTC;->j:LX/CT7;

    .line 1895581
    invoke-direct {p0}, LX/CTC;->l()LX/CT8;

    move-result-object v1

    iput-object v1, p0, LX/CTC;->g:LX/CT8;

    .line 1895582
    invoke-direct {p0}, LX/CTC;->m()LX/CT9;

    move-result-object v1

    iput-object v1, p0, LX/CTC;->h:LX/CT9;

    .line 1895583
    new-instance v1, LX/CT1;

    iget-object v2, p0, LX/CTC;->i:LX/CT6;

    invoke-direct {v1, v2}, LX/CT1;-><init>(LX/CT6;)V

    iput-object v1, p0, LX/CTC;->k:LX/CT1;

    .line 1895584
    new-instance v1, LX/CSg;

    invoke-direct {v1}, LX/CSg;-><init>()V

    iput-object v1, p0, LX/CTC;->f:LX/CSg;

    .line 1895585
    new-instance v1, LX/CSc;

    invoke-direct {v1}, LX/CSc;-><init>()V

    iput-object v1, p0, LX/CTC;->e:LX/CSc;

    .line 1895586
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, LX/CTC;->s:Ljava/util/Stack;

    .line 1895587
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/CTC;->t:Ljava/util/HashSet;

    .line 1895588
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, LX/CTC;->u:Ljava/util/Stack;

    .line 1895589
    sget-object v1, LX/CTC;->a:LX/3rL;

    iput-object v1, p0, LX/CTC;->v:LX/3rL;

    .line 1895590
    iput-object p4, p0, LX/CTC;->p:Ljava/lang/String;

    .line 1895591
    iput-object p5, p0, LX/CTC;->r:Ljava/lang/String;

    .line 1895592
    move-object/from16 v0, p18

    iput-object v0, p0, LX/CTC;->q:Ljava/lang/String;

    .line 1895593
    move-object/from16 v0, p17

    iput-object v0, p0, LX/CTC;->o:LX/CST;

    .line 1895594
    move-object/from16 v0, p16

    iput-object v0, p0, LX/CTC;->n:LX/CSQ;

    .line 1895595
    iput-object p6, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    .line 1895596
    iput-object p7, p0, LX/CTC;->x:LX/CXY;

    .line 1895597
    iput-object p8, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    .line 1895598
    iput-object p9, p0, LX/CTC;->z:Landroid/view/View;

    .line 1895599
    iput-object p10, p0, LX/CTC;->A:Landroid/widget/FrameLayout;

    .line 1895600
    move-object/from16 v0, p13

    iput-object v0, p0, LX/CTC;->D:Landroid/widget/LinearLayout;

    .line 1895601
    move-object/from16 v0, p14

    iput-object v0, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    .line 1895602
    move-object/from16 v0, p15

    iput-object v0, p0, LX/CTC;->F:Landroid/view/View;

    .line 1895603
    iput-object p11, p0, LX/CTC;->B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    .line 1895604
    iput-object p12, p0, LX/CTC;->C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    .line 1895605
    new-instance v1, LX/CT3;

    invoke-direct {v1, p0}, LX/CT3;-><init>(LX/CTC;)V

    iput-object v1, p0, LX/CTC;->l:LX/CT3;

    .line 1895606
    return-void
.end method

.method private static a(LX/CTC;LX/CUD;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1895559
    iget-object v0, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1895560
    iget-object v2, p0, LX/CTC;->F:Landroid/view/View;

    iget-object v0, p1, LX/CUD;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1895561
    iget-object v8, p1, LX/CUD;->d:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v7, v1

    :goto_1
    if-ge v7, v9, :cond_1

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/CTJ;

    .line 1895562
    sget-object v0, LX/CTA;->d:[I

    iget-object v1, v2, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1895563
    iget-object v0, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    iget-object v1, v2, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {v0, v1}, LX/CSi;->d(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v0

    .line 1895564
    iget-object v1, p0, LX/CTC;->f:LX/CSg;

    iget-object v3, p0, LX/CTC;->k:LX/CT1;

    invoke-virtual {v1, v0, v2, v3}, LX/CSg;->a(Landroid/view/View;LX/CTJ;LX/CT1;)V

    .line 1895565
    :goto_2
    iget-object v1, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1895566
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1895567
    goto :goto_0

    .line 1895568
    :pswitch_0
    iget-object v0, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    iget-object v1, v2, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {v0, v1}, LX/CSi;->d(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v0

    .line 1895569
    iget-object v1, p0, LX/CTC;->f:LX/CSg;

    check-cast v2, LX/CTe;

    iget-object v3, p0, LX/CTC;->i:LX/CT6;

    iget-object v4, p0, LX/CTC;->k:LX/CT1;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/CSg;->a(Landroid/view/View;LX/CTe;LX/CT6;LX/CT1;)V

    goto :goto_2

    .line 1895570
    :pswitch_1
    iget-object v1, p0, LX/CTC;->E:Landroid/widget/LinearLayout;

    move-object v0, v2

    check-cast v0, LX/CTK;

    iget-object v0, v0, LX/CTK;->f:LX/CSS;

    invoke-static {v1, v0}, LX/CSi;->a(Landroid/view/ViewGroup;LX/CSS;)Landroid/view/View;

    move-result-object v0

    move-object v1, v2

    .line 1895571
    check-cast v1, LX/CTK;

    iget-object v1, v1, LX/CTK;->f:LX/CSS;

    check-cast v2, LX/CTK;

    iget-object v3, p0, LX/CTC;->n:LX/CSQ;

    iget-object v4, p0, LX/CTC;->j:LX/CT7;

    iget-object v5, p0, LX/CTC;->i:LX/CT6;

    iget-object v6, p0, LX/CTC;->k:LX/CT1;

    invoke-static/range {v0 .. v6}, LX/CSg;->a(Landroid/view/View;LX/CSS;LX/CTK;LX/CSQ;LX/CT7;LX/CT6;LX/CT1;)V

    goto :goto_2

    .line 1895572
    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/CTC;Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1895547
    sget-object v0, LX/CTA;->c:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1895548
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected screen type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1895549
    :pswitch_0
    iget-object v0, p0, LX/CTC;->B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->setVisibility(I)V

    .line 1895550
    iget-object v0, p0, LX/CTC;->C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->setVisibility(I)V

    .line 1895551
    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1895552
    :goto_0
    return-void

    .line 1895553
    :pswitch_1
    iget-object v0, p0, LX/CTC;->B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->setVisibility(I)V

    .line 1895554
    iget-object v0, p0, LX/CTC;->C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->setVisibility(I)V

    .line 1895555
    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 1895556
    :pswitch_2
    iget-object v0, p0, LX/CTC;->B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->setVisibility(I)V

    .line 1895557
    iget-object v0, p0, LX/CTC;->C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->setVisibility(I)V

    .line 1895558
    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/CTC;LX/CTB;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0xfa

    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 1895503
    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1895504
    iget-object v2, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1895505
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    iget-object v3, p0, LX/CTC;->l:LX/CT3;

    invoke-interface {v2, v3}, LX/CXY;->a(LX/CT3;)V

    .line 1895506
    invoke-static {p0}, LX/CTC;->f(LX/CTC;)V

    .line 1895507
    :goto_0
    sget-object v2, LX/CTA;->a:[I

    invoke-virtual {p1}, LX/CTB;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1895508
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895509
    :goto_1
    sget-object v2, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    if-ne p1, v2, :cond_0

    .line 1895510
    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1895511
    :goto_2
    iget-object v2, p0, LX/CTC;->A:Landroid/widget/FrameLayout;

    sget-object v3, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    if-ne p1, v3, :cond_1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1895512
    return-void

    .line 1895513
    :pswitch_0
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    const v3, 0x7f08003c

    invoke-interface {v2, v3}, LX/0h5;->setTitle(I)V

    .line 1895514
    invoke-static {p0}, LX/CTC;->i(LX/CTC;)V

    .line 1895515
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-static {p0, v2}, LX/CTC;->a(LX/CTC;Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;)V

    goto :goto_1

    .line 1895516
    :pswitch_1
    invoke-direct {p0}, LX/CTC;->g()V

    goto :goto_1

    .line 1895517
    :pswitch_2
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    const v3, 0x7f080024

    invoke-interface {v2, v3}, LX/0h5;->setTitle(I)V

    .line 1895518
    iget-object v2, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1895519
    iget-object v2, p0, LX/CTC;->z:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1895520
    :pswitch_3
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    const v3, 0x7f080024

    invoke-interface {v2, v3}, LX/0h5;->setTitle(I)V

    .line 1895521
    iget-object v2, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_1

    .line 1895522
    :cond_0
    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->clearAnimation()V

    .line 1895523
    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$3;

    invoke-direct {v3, p0}, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$3;-><init>(LX/CTC;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    :cond_1
    move v0, v1

    .line 1895524
    goto :goto_3

    .line 1895525
    :cond_2
    iget-object v2, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->b:LX/CUC;

    iget-boolean v2, v2, LX/CUC;->a:Z

    if-eqz v2, :cond_3

    .line 1895526
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    iget-object v3, p0, LX/CTC;->l:LX/CT3;

    invoke-interface {v2, v3}, LX/CXY;->a(LX/CT3;)V

    .line 1895527
    :goto_4
    sget-object v3, LX/CTA;->b:[I

    iget-object v2, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->b:LX/CUC;

    iget-object v2, v2, LX/CUC;->c:LX/CUB;

    invoke-virtual {v2}, LX/CUB;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    .line 1895528
    const/4 v2, 0x0

    const-string v3, "Unhandled Action type"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto/16 :goto_0

    .line 1895529
    :cond_3
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    invoke-interface {v2}, LX/CXY;->a()V

    goto :goto_4

    .line 1895530
    :pswitch_4
    const/4 v3, 0x0

    .line 1895531
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    invoke-interface {v2, v3}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1895532
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    invoke-interface {v2, v3}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1895533
    goto/16 :goto_0

    .line 1895534
    :pswitch_5
    invoke-static {p0}, LX/CTC;->f(LX/CTC;)V

    goto/16 :goto_0

    .line 1895535
    :pswitch_6
    const/4 v7, 0x1

    .line 1895536
    iget-object v3, p0, LX/CTC;->x:LX/CXY;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 1895537
    iput-boolean v7, v2, LX/108;->q:Z

    .line 1895538
    move-object v6, v2

    .line 1895539
    iget-object v2, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->b:LX/CUC;

    iget-object v2, v2, LX/CUC;->d:Ljava/lang/String;

    .line 1895540
    iput-object v2, v6, LX/108;->g:Ljava/lang/String;

    .line 1895541
    move-object v2, v6

    .line 1895542
    iput v7, v2, LX/108;->a:I

    .line 1895543
    move-object v2, v2

    .line 1895544
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1895545
    iget-object v2, p0, LX/CTC;->x:LX/CXY;

    iget-object v3, p0, LX/CTC;->m:LX/63W;

    invoke-interface {v2, v3}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1895546
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a$redex0(LX/CTC;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x1

    .line 1895427
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895428
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895429
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895430
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->x()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895431
    sget-object v0, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v0}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v0

    invoke-virtual {v0}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v0

    invoke-virtual {v0}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v6

    .line 1895432
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v0

    .line 1895433
    iput-boolean v11, v0, LX/6wu;->d:Z

    .line 1895434
    move-object v0, v0

    .line 1895435
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v7

    .line 1895436
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 1895437
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1895438
    sget-object v1, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895439
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1895440
    sget-object v1, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895441
    :cond_1
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v8

    .line 1895442
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1895443
    const/4 v0, 0x0

    .line 1895444
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->s()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v4, :cond_2

    .line 1895445
    new-instance v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1895446
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1895447
    invoke-direct {v5, v0, v1, v9, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/0Px;)V

    .line 1895448
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v3, v4, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)LX/6rY;

    move-result-object v0

    invoke-virtual {v0}, LX/6rY;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v0

    .line 1895449
    :cond_2
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 1895450
    sget-object v2, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895451
    sget-object v2, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895452
    invoke-virtual {v8}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1895453
    sget-object v2, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895454
    :cond_3
    if-eqz v0, :cond_4

    .line 1895455
    sget-object v2, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1895456
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    .line 1895457
    new-instance v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    new-instance v4, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jY_()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v5, v9, v10}, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1895458
    const/4 v5, 0x0

    .line 1895459
    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jX_()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 1895460
    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jX_()Ljava/lang/String;

    move-result-object v5

    .line 1895461
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1895462
    if-eqz v5, :cond_6

    .line 1895463
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "\n"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1895464
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1895465
    :cond_7
    move-object v2, v5

    .line 1895466
    invoke-direct {v3, v4, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;-><init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V

    .line 1895467
    sget-object v2, LX/6qw;->INSTANT_WORKFLOWS:LX/6qw;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 1895468
    sget-object v5, LX/6xg;->NMOR_SYNCHRONOUS_COMPONENT_FLOW:LX/6xg;

    invoke-virtual {v5}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1895469
    sget-object v5, LX/6xg;->NMOR_SYNCHRONOUS_COMPONENT_FLOW:LX/6xg;

    .line 1895470
    :goto_0
    move-object v4, v5

    .line 1895471
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    invoke-static {v2, v4, v1, v6}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v1

    .line 1895472
    iput-object v3, v1, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1895473
    move-object v1, v1

    .line 1895474
    iput-object v8, v1, LX/6qZ;->t:LX/0Rf;

    .line 1895475
    move-object v1, v1

    .line 1895476
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1895477
    iput-object v2, v1, LX/6qZ;->q:Ljava/lang/String;

    .line 1895478
    move-object v1, v1

    .line 1895479
    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;)LX/0Px;

    move-result-object v2

    .line 1895480
    iput-object v2, v1, LX/6qZ;->h:LX/0Px;

    .line 1895481
    move-object v1, v1

    .line 1895482
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1895483
    iput-object v2, v1, LX/6qZ;->r:Ljava/lang/String;

    .line 1895484
    move-object v1, v1

    .line 1895485
    iput-boolean v11, v1, LX/6qZ;->o:Z

    .line 1895486
    move-object v1, v1

    .line 1895487
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->x()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1895488
    iput-object v2, v1, LX/6qZ;->p:Ljava/lang/String;

    .line 1895489
    move-object v1, v1

    .line 1895490
    iput-object v7, v1, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1895491
    move-object v1, v1

    .line 1895492
    if-eqz v0, :cond_8

    .line 1895493
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1895494
    iput-object v0, v1, LX/6qZ;->w:LX/0Px;

    .line 1895495
    :cond_8
    new-instance v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->w()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->x()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v3

    invoke-virtual {v1}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;-><init>(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1895496
    iget-object v1, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1895497
    sget-object v0, LX/CTB;->READY:LX/CTB;

    invoke-static {p0, v0}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895498
    iget-object v2, p0, LX/CTC;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2b

    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1895499
    return-void

    .line 1895500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1895501
    :cond_9
    const/4 v5, 0x0

    const-string v9, "Server must ensure client only receives supported constants"

    invoke-static {v5, v9}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1895502
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public static c(LX/CUD;)Z
    .locals 2

    .prologue
    .line 1895426
    iget-object v0, p0, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/CTC;)V
    .locals 4

    .prologue
    .line 1895417
    iget-object v0, p0, LX/CTC;->x:LX/CXY;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02081a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1895418
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 1895419
    move-object v1, v1

    .line 1895420
    const/4 v2, 0x2

    .line 1895421
    iput v2, v1, LX/108;->a:I

    .line 1895422
    move-object v1, v1

    .line 1895423
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1895424
    iget-object v0, p0, LX/CTC;->x:LX/CXY;

    iget-object v1, p0, LX/CTC;->m:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1895425
    return-void
.end method

.method private g()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 1895608
    sget-object v1, LX/CTA;->c:[I

    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1895609
    iget-object v0, p0, LX/CTC;->D:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a07e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1895610
    :goto_0
    invoke-static {p0}, LX/CTC;->i(LX/CTC;)V

    .line 1895611
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v6, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v6, LX/CUD;

    .line 1895612
    iget-object v0, p0, LX/CTC;->x:LX/CXY;

    iget-object v1, v6, LX/CUD;->a:LX/CUA;

    iget-object v1, v1, LX/CUA;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1895613
    invoke-static {p0, v6}, LX/CTC;->a(LX/CTC;LX/CUD;)V

    .line 1895614
    iget-object v8, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    new-instance v0, LX/CSW;

    iget-object v1, p0, LX/CTC;->i:LX/CT6;

    iget-object v2, p0, LX/CTC;->j:LX/CT7;

    iget-object v3, p0, LX/CTC;->n:LX/CSQ;

    iget-object v4, p0, LX/CTC;->f:LX/CSg;

    iget-object v5, p0, LX/CTC;->k:LX/CT1;

    invoke-direct/range {v0 .. v6}, LX/CSW;-><init>(LX/CT6;LX/CT7;LX/CSQ;LX/CSg;LX/CT1;LX/CUD;)V

    invoke-virtual {v8, v0, v7}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OM;Z)V

    .line 1895615
    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 1895616
    instance-of v1, v0, LX/1P1;

    if-eqz v1, :cond_0

    .line 1895617
    check-cast v0, LX/1P1;

    .line 1895618
    iget-object v1, p0, LX/CTC;->v:LX/3rL;

    iget-object v1, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p0, LX/CTC;->v:LX/3rL;

    iget-object v1, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v2, v1}, LX/1P1;->d(II)V

    .line 1895619
    :cond_0
    sget-object v0, LX/CTA;->c:[I

    iget-object v1, v6, LX/CUD;->a:LX/CUA;

    iget-object v1, v1, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1895620
    iget-object v0, v6, LX/CUD;->f:LX/CTJ;

    if-nez v0, :cond_1

    move v0, v7

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895621
    :goto_2
    iget-object v0, v6, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-static {p0, v0}, LX/CTC;->a(LX/CTC;Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;)V

    .line 1895622
    return-void

    .line 1895623
    :pswitch_0
    iget-object v0, p0, LX/CTC;->B:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    iget-object v2, v6, LX/CUD;->f:LX/CTJ;

    invoke-static {v0, v1, v2}, LX/CSg;->a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLScreenElementType;LX/CTJ;)V

    goto :goto_2

    .line 1895624
    :pswitch_1
    iget-object v0, p0, LX/CTC;->C:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    iget-object v2, v6, LX/CUD;->f:LX/CTJ;

    invoke-static {v0, v1, v2}, LX/CSg;->a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLScreenElementType;LX/CTJ;)V

    goto :goto_2

    .line 1895625
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1895626
    :pswitch_2
    iget-object v0, p0, LX/CTC;->D:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a07e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static i(LX/CTC;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1895405
    const/16 v2, 0x8

    .line 1895406
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->e:LX/CU8;

    if-eqz v0, :cond_2

    .line 1895407
    iget-object v2, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->e:LX/CU8;

    iget-boolean v0, v0, LX/CU8;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 1895408
    iget-object v2, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->e:LX/CU8;

    iget-object v0, v0, LX/CU8;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1895409
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->e:LX/CU8;

    iget-object v0, v0, LX/CU8;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    .line 1895410
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1895411
    iget-object v0, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    const v2, 0x7f020740

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 1895412
    :cond_0
    iget-object v0, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/CT5;

    invoke-direct {v2, p0}, LX/CT5;-><init>(LX/CTC;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1895413
    :goto_1
    iget-object v0, p0, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1895414
    iget-object v0, p0, LX/CTC;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1895415
    return-void

    :cond_1
    move v0, v1

    .line 1895416
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private j()LX/CT6;
    .locals 1

    .prologue
    .line 1895404
    new-instance v0, LX/CT6;

    invoke-direct {v0, p0}, LX/CT6;-><init>(LX/CTC;)V

    return-object v0
.end method

.method private k()LX/CT7;
    .locals 1

    .prologue
    .line 1895403
    new-instance v0, LX/CT7;

    invoke-direct {v0, p0}, LX/CT7;-><init>(LX/CTC;)V

    return-object v0
.end method

.method private l()LX/CT8;
    .locals 1

    .prologue
    .line 1895387
    new-instance v0, LX/CT8;

    invoke-direct {v0, p0}, LX/CT8;-><init>(LX/CTC;)V

    return-object v0
.end method

.method private m()LX/CT9;
    .locals 1

    .prologue
    .line 1895402
    new-instance v0, LX/CT9;

    invoke-direct {v0, p0}, LX/CT9;-><init>(LX/CTC;)V

    return-object v0
.end method

.method public static n(LX/CTC;)V
    .locals 3

    .prologue
    .line 1895397
    iget-object v0, p0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 1895398
    const v1, 0x7f080039

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3oW;->a(Landroid/view/View;II)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 1895399
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/CTB;->PLATFORM_ERROR:LX/CTB;

    :goto_0
    invoke-static {p0, v0}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895400
    return-void

    .line 1895401
    :cond_0
    sget-object v0, LX/CTB;->READY:LX/CTB;

    goto :goto_0
.end method

.method public static r(LX/CTC;)V
    .locals 2

    .prologue
    .line 1895392
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    invoke-static {v0}, LX/CTC;->c(LX/CUD;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1895393
    iget-object v1, p0, LX/CTC;->t:Ljava/util/HashSet;

    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, LX/CUD;

    iget-object v0, v0, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1895394
    :cond_0
    iget-object v0, p0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1895395
    iget-object v0, p0, LX/CTC;->u:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iput-object v0, p0, LX/CTC;->v:LX/3rL;

    .line 1895396
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1895388
    sget-object v0, LX/CTB;->INITIALIZING:LX/CTB;

    invoke-static {p0, v0}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895389
    iget-object v0, p0, LX/CTC;->b:LX/CSz;

    iget-object v1, p0, LX/CTC;->p:Ljava/lang/String;

    iget-object v2, p0, LX/CTC;->r:Ljava/lang/String;

    iget-object v3, p0, LX/CTC;->g:LX/CT8;

    iget-object v4, p0, LX/CTC;->q:Ljava/lang/String;

    .line 1895390
    iget-object v5, v0, LX/CSz;->b:LX/1Ck;

    sget-object v6, LX/CSy;->FETCH_FIRST_SCREEN:LX/CSy;

    invoke-virtual {v6}, LX/CSy;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v1, v2, v4}, LX/CSz;->a(LX/CSz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v0, v1, v3}, LX/CSz;->a(LX/CSz;Ljava/lang/String;LX/CT8;)LX/0Vd;

    move-result-object p0

    invoke-virtual {v5, v6, v7, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1895391
    return-void
.end method
