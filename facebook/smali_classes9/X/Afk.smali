.class public LX/Afk;
.super LX/AeL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeL",
        "<",
        "LX/Ag2;",
        ">;"
    }
.end annotation


# static fields
.field public static final n:[I

.field public static final o:[I


# instance fields
.field public final m:Lcom/facebook/resources/ui/FbTextView;

.field public final p:LX/3HT;

.field private final q:LX/Ac6;

.field private final r:LX/1b4;

.field private final s:LX/215;

.field public final t:LX/Acf;

.field private final u:Z

.field private v:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:Lcom/facebook/widget/springbutton/SpringScaleButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1699627
    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, LX/Afk;->n:[I

    .line 1699628
    new-array v0, v3, [I

    const v1, 0x101009f

    aput v1, v0, v2

    sput-object v0, LX/Afk;->o:[I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/3HT;LX/Ac6;LX/1b4;LX/215;LX/Acf;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1699629
    invoke-direct {p0, p1}, LX/AeL;-><init>(Landroid/view/View;)V

    .line 1699630
    iput-object p2, p0, LX/Afk;->p:LX/3HT;

    .line 1699631
    iput-object p3, p0, LX/Afk;->q:LX/Ac6;

    .line 1699632
    iput-object p4, p0, LX/Afk;->r:LX/1b4;

    .line 1699633
    iput-object p5, p0, LX/Afk;->s:LX/215;

    .line 1699634
    iput-object p6, p0, LX/Afk;->t:LX/Acf;

    .line 1699635
    const v0, 0x7f0d197e

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Afk;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1699636
    const v0, 0x7f0d19c2

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/springbutton/SpringScaleButton;

    iput-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1699637
    iget-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v2, p0, LX/Afk;->s:LX/215;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a(LX/215;)V

    .line 1699638
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1699639
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f01041c

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1699640
    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/Afk;->u:Z

    .line 1699641
    return-void

    .line 1699642
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/Ag2;LX/AeU;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1699643
    invoke-super {p0, p1, p2}, LX/AeL;->a(LX/AeP;LX/AeU;)V

    .line 1699644
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1699645
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1699646
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1699647
    iget-boolean v0, p1, LX/Ag2;->c:Z

    if-eqz v0, :cond_6

    .line 1699648
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1699649
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x2

    .line 1699650
    iget-boolean v0, p2, LX/AeU;->h:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0f004e

    :goto_0
    invoke-virtual {v1, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699651
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v5, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699652
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v6, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699653
    if-ne v4, v5, :cond_5

    .line 1699654
    const/4 v0, 0x3

    iget-object v1, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v0, v3, v1, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699655
    :goto_1
    iget-object v0, p0, LX/Afk;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699656
    iget-object v0, p0, LX/Afk;->v:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 1699657
    new-instance v0, LX/Afi;

    invoke-direct {v0, p0}, LX/Afi;-><init>(LX/Afk;)V

    iput-object v0, p0, LX/Afk;->v:Landroid/view/View$OnClickListener;

    .line 1699658
    :cond_0
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/Afk;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699659
    iget-boolean v0, p0, LX/Afk;->u:Z

    if-nez v0, :cond_8

    iget-object v0, p0, LX/Afk;->r:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, LX/AeU;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/Afk;->q:LX/Ac6;

    .line 1699660
    iget-object v1, v0, LX/Ac6;->a:LX/0Uh;

    const/16 v2, 0x341

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1699661
    if-eqz v0, :cond_8

    .line 1699662
    :cond_1
    iget-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1699663
    iget-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    new-instance v1, LX/Afj;

    invoke-direct {v1, p0, p1}, LX/Afj;-><init>(LX/Afk;LX/Ag2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699664
    :goto_2
    return-void

    .line 1699665
    :pswitch_0
    iget-boolean v0, p2, LX/AeU;->h:Z

    if-eqz v0, :cond_2

    const v0, 0x7f080bf5

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699666
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v5, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    goto :goto_1

    .line 1699667
    :cond_2
    const v0, 0x7f080bf4

    goto :goto_3

    .line 1699668
    :pswitch_1
    iget-boolean v0, p2, LX/AeU;->h:Z

    if-eqz v0, :cond_3

    const v0, 0x7f080bf7

    :goto_4
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699669
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v5, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699670
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v6, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    goto :goto_1

    .line 1699671
    :cond_3
    const v0, 0x7f080bf6

    goto :goto_4

    .line 1699672
    :cond_4
    const v0, 0x7f0f004d

    goto/16 :goto_0

    .line 1699673
    :cond_5
    const-string v0, "%3$d"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    goto/16 :goto_1

    .line 1699674
    :cond_6
    iget-boolean v0, p2, LX/AeU;->h:Z

    if-eqz v0, :cond_7

    const v0, 0x7f080bf3

    :goto_5
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699675
    iget-object v0, p1, LX/Ag2;->b:LX/0Px;

    invoke-static {v5, v3, v0, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    goto/16 :goto_1

    .line 1699676
    :cond_7
    const v0, 0x7f080bf2

    goto :goto_5

    .line 1699677
    :cond_8
    iget-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699678
    iget-object v0, p0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(LX/AeO;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699679
    check-cast p1, LX/Ag2;

    invoke-direct {p0, p1, p2}, LX/Afk;->a(LX/Ag2;LX/AeU;)V

    return-void
.end method

.method public final bridge synthetic a(LX/AeP;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699680
    check-cast p1, LX/Ag2;

    invoke-direct {p0, p1, p2}, LX/Afk;->a(LX/Ag2;LX/AeU;)V

    return-void
.end method
