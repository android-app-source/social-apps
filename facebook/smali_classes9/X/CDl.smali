.class public final LX/CDl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3iF;


# direct methods
.method public constructor <init>(LX/3iF;)V
    .locals 0

    .prologue
    .line 1860049
    iput-object p1, p0, LX/CDl;->a:LX/3iF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1860050
    invoke-super {p0, p1}, LX/0Vd;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 1860051
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860052
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1860053
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860054
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1860055
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v2, 0x0

    .line 1860056
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1860057
    :cond_0
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    iget-object v0, v0, LX/3iF;->i:LX/03V;

    sget-object v1, LX/3iF;->a:Ljava/lang/String;

    const-string v2, "Fetched pages user admins are null!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860058
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    .line 1860059
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860060
    :goto_0
    return-void

    .line 1860061
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1860062
    iget-object v5, p0, LX/CDl;->a:LX/3iF;

    iget-object v5, v5, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    new-instance v6, LX/CDp;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v6, v7, v8, v0}, LX/CDp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1860063
    iget-object v0, v5, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1860064
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1860065
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1860066
    :cond_3
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    invoke-static {v0}, LX/3iF;->d$redex0(LX/3iF;)V

    .line 1860067
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    iget-object v0, v0, LX/3iF;->k:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    new-instance v1, LX/CDo;

    iget-object v3, p0, LX/CDl;->a:LX/3iF;

    invoke-direct {v1, v3}, LX/CDo;-><init>(LX/3iF;)V

    .line 1860068
    iput-object v1, v0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->c:LX/CDo;

    .line 1860069
    iget-object v0, p0, LX/CDl;->a:LX/3iF;

    .line 1860070
    invoke-static {v0}, LX/3iF;->b$redex0(LX/3iF;)V

    .line 1860071
    goto :goto_0
.end method
