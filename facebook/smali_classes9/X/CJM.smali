.class public LX/CJM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field private static final b:LX/0Tn;

.field private static final c:LX/0Tn;


# instance fields
.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1875400
    sget-object v0, LX/0db;->b:LX/0Tn;

    const-string v1, "platform_apps/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1875401
    sput-object v0, LX/CJM;->a:LX/0Tn;

    const-string v1, "install_launch_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CJM;->b:LX/0Tn;

    .line 1875402
    sget-object v0, LX/CJM;->a:LX/0Tn;

    const-string v1, "stats/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/CJM;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875404
    iput-object p1, p0, LX/CJM;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1875405
    iput-object p2, p0, LX/CJM;->e:LX/0SG;

    .line 1875406
    return-void
.end method

.method public static b(LX/0QB;)LX/CJM;
    .locals 3

    .prologue
    .line 1875407
    new-instance v2, LX/CJM;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/CJM;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 1875408
    return-object v2
.end method
