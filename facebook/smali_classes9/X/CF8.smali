.class public final LX/CF8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;I)V
    .locals 0

    .prologue
    .line 1862475
    iput-object p1, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862476
    iput p2, p0, LX/CF8;->b:I

    .line 1862477
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1862478
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v1, p0, LX/CF8;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/CF9;->b:Z

    .line 1862479
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-boolean v0, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->x:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/CF8;->b:I

    iget-object v1, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget v1, v1, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1862480
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    const/4 v1, 0x0

    .line 1862481
    iput-boolean v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->x:Z

    .line 1862482
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->m(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)Z

    .line 1862483
    :cond_0
    return-void
.end method

.method public final a(LX/CFD;)V
    .locals 1

    .prologue
    .line 1862484
    sget-object v0, LX/CFD;->NAVIGATION_NEXT:LX/CFD;

    if-ne p1, v0, :cond_0

    .line 1862485
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->m(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)Z

    .line 1862486
    :goto_0
    return-void

    .line 1862487
    :cond_0
    sget-object v0, LX/CFD;->NAVIGATION_BACK:LX/CFD;

    if-ne p1, v0, :cond_1

    .line 1862488
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->n(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)Z

    goto :goto_0

    .line 1862489
    :cond_1
    iget-object v0, p0, LX/CF8;->a:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->q(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V

    goto :goto_0
.end method
