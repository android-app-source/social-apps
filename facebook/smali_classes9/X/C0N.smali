.class public final LX/C0N;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C0P;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C0O;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C0P",
            "<TE;>.SquarePhotoShareComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C0P;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C0P;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1840492
    iput-object p1, p0, LX/C0N;->b:LX/C0P;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1840493
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photoHeight"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C0N;->c:[Ljava/lang/String;

    .line 1840494
    iput v3, p0, LX/C0N;->d:I

    .line 1840495
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C0N;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C0N;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C0N;LX/1De;IILX/C0O;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C0P",
            "<TE;>.SquarePhotoShareComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1840465
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1840466
    iput-object p4, p0, LX/C0N;->a:LX/C0O;

    .line 1840467
    iget-object v0, p0, LX/C0N;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1840468
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/C0N;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C0P",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1840489
    iget-object v0, p0, LX/C0N;->a:LX/C0O;

    iput-object p1, v0, LX/C0O;->b:LX/1Pq;

    .line 1840490
    iget-object v0, p0, LX/C0N;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1840491
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0N;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/C0P",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1840486
    iget-object v0, p0, LX/C0N;->a:LX/C0O;

    iput-object p1, v0, LX/C0O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840487
    iget-object v0, p0, LX/C0N;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1840488
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1840482
    invoke-super {p0}, LX/1X5;->a()V

    .line 1840483
    const/4 v0, 0x0

    iput-object v0, p0, LX/C0N;->a:LX/C0O;

    .line 1840484
    iget-object v0, p0, LX/C0N;->b:LX/C0P;

    iget-object v0, v0, LX/C0P;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1840485
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C0P;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1840472
    iget-object v1, p0, LX/C0N;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C0N;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C0N;->d:I

    if-ge v1, v2, :cond_2

    .line 1840473
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1840474
    :goto_0
    iget v2, p0, LX/C0N;->d:I

    if-ge v0, v2, :cond_1

    .line 1840475
    iget-object v2, p0, LX/C0N;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1840476
    iget-object v2, p0, LX/C0N;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1840477
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1840478
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1840479
    :cond_2
    iget-object v0, p0, LX/C0N;->a:LX/C0O;

    .line 1840480
    invoke-virtual {p0}, LX/C0N;->a()V

    .line 1840481
    return-object v0
.end method

.method public final h(I)LX/C0N;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/C0P",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1840469
    iget-object v0, p0, LX/C0N;->a:LX/C0O;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/C0O;->c:I

    .line 1840470
    iget-object v0, p0, LX/C0N;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1840471
    return-object p0
.end method
