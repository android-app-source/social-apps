.class public abstract LX/AeL;
.super LX/AeK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Event:",
        "LX/AeP;",
        ">",
        "LX/AeK",
        "<TEvent;>;"
    }
.end annotation


# instance fields
.field public final l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

.field private final m:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1697329
    invoke-direct {p0, p1}, LX/AeK;-><init>(Landroid/view/View;)V

    .line 1697330
    const v0, 0x7f0d0f67

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastUserTileView;

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastUserTileView;

    iput-object v0, p0, LX/AeL;->l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

    .line 1697331
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b04bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AeL;->m:I

    .line 1697332
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LX/AeO;LX/AeU;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 1697328
    check-cast p1, LX/AeP;

    invoke-virtual {p0, p1, p2}, LX/AeL;->a(LX/AeP;LX/AeU;)V

    return-void
.end method

.method public a(LX/AeP;LX/AeU;)V
    .locals 5
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEvent;",
            "LX/AeU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1697319
    iget-object v0, p1, LX/AeP;->a:LX/AcC;

    .line 1697320
    iget-object v1, p0, LX/AeL;->l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

    new-instance v2, LX/Ac5;

    iget-object v3, p1, LX/AeP;->a:LX/AcC;

    iget-object v3, v3, LX/AcC;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/Ac5;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/AcC;->c:Ljava/lang/String;

    iget v4, p0, LX/AeL;->m:I

    invoke-virtual {v2, v3, v4}, LX/Ac5;->a(Ljava/lang/String;I)LX/Ac5;

    move-result-object v2

    iget-boolean v3, v0, LX/AcC;->d:Z

    .line 1697321
    iput-boolean v3, v2, LX/Ac5;->d:Z

    .line 1697322
    move-object v2, v2

    .line 1697323
    iget-object v0, v0, LX/AcC;->b:Ljava/lang/String;

    iget-object v3, p2, LX/AeU;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1697324
    iput-boolean v0, v2, LX/Ac5;->e:Z

    .line 1697325
    move-object v0, v2

    .line 1697326
    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/FacecastUserTileView;->setParam(LX/Ac5;)V

    .line 1697327
    return-void
.end method
