.class public final LX/Bt1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:LX/1PT;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLX/1PT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1828574
    iput-object p1, p0, LX/Bt1;->a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828575
    iput-object p2, p0, LX/Bt1;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828576
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828577
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p3}, LX/1zJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bt1;->c:Ljava/lang/String;

    .line 1828578
    iput-object p4, p0, LX/Bt1;->d:LX/1PT;

    .line 1828579
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1828580
    const/4 v4, 0x0

    .line 1828581
    iget-object v0, p0, LX/Bt1;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828582
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1828583
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1828584
    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1828585
    new-instance v0, LX/1yT;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v4}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 1828586
    :goto_0
    return-object v0

    .line 1828587
    :cond_0
    iget-object v1, p0, LX/Bt1;->a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nA;

    iget-object v2, p0, LX/Bt1;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Bt1;->a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

    iget-boolean v3, v3, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->b:Z

    invoke-virtual {v1, v2, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v2, v1

    .line 1828588
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, LX/Bt1;->a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

    iget-object v3, v3, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->d:LX/1xc;

    invoke-virtual {v3, v0, v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 1828589
    :goto_1
    new-instance v1, LX/1yT;

    invoke-direct {v1, v0, v4}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    move-object v0, v1

    goto :goto_0

    .line 1828590
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1828591
    iget-object v0, p0, LX/Bt1;->c:Ljava/lang/String;

    return-object v0
.end method
