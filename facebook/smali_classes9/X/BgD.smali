.class public final LX/BgD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:LX/BgK;


# direct methods
.method public constructor <init>(LX/BgK;LX/4BY;)V
    .locals 0

    .prologue
    .line 1807270
    iput-object p1, p0, LX/BgD;->b:LX/BgK;

    iput-object p2, p0, LX/BgD;->a:LX/4BY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1807271
    iget-object v0, p0, LX/BgD;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1807272
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v0, v0, LX/BgK;->m:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082916

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1807273
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1807274
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v0, v0, LX/BgK;->m:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082915

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1807275
    iget-object v0, p0, LX/BgD;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1807276
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v0, v0, LX/BgK;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1807277
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v1, p0, LX/BgD;->b:LX/BgK;

    iget-object v1, v1, LX/BgK;->d:Landroid/content/Context;

    const v2, 0x7f082926

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BgD;->b:LX/BgK;

    iget-object v2, v2, LX/BgK;->d:Landroid/content/Context;

    const v3, 0x7f082927

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/BgD;->b:LX/BgK;

    iget-object v4, v4, LX/BgK;->d:Landroid/content/Context;

    const-class v5, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.facebook.katana.profile.id"

    iget-object v5, p0, LX/BgD;->b:LX/BgK;

    iget-object v5, v5, LX/BgK;->r:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "title"

    iget-object v5, p0, LX/BgD;->b:LX/BgK;

    iget-object v5, v5, LX/BgK;->d:Landroid/content/Context;

    const v6, 0x7f08293b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "target_fragment"

    sget-object v5, LX/0cQ;->FRIEND_VOTE_INVITE_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/BgK;->a$redex0(LX/BgK;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 1807278
    :goto_0
    return-void

    .line 1807279
    :cond_0
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v0, v0, LX/BgK;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807280
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v1, p0, LX/BgD;->b:LX/BgK;

    iget-object v1, v1, LX/BgK;->d:Landroid/content/Context;

    const v2, 0x7f082924

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BgD;->b:LX/BgK;

    iget-object v2, v2, LX/BgK;->d:Landroid/content/Context;

    const v3, 0x7f082925

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BgD;->b:LX/BgK;

    iget-object v3, v3, LX/BgK;->n:LX/17Y;

    iget-object v4, p0, LX/BgD;->b:LX/BgK;

    iget-object v4, v4, LX/BgK;->d:Landroid/content/Context;

    sget-object v5, LX/0ax;->iO:Ljava/lang/String;

    const-string v6, "{entry_point}"

    const-string v7, "android_post_suggest_edits_upsell"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/BgK;->a$redex0(LX/BgK;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 1807281
    :cond_1
    iget-object v0, p0, LX/BgD;->b:LX/BgK;

    iget-object v0, v0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b()V

    goto :goto_0
.end method
