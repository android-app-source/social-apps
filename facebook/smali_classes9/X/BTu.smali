.class public final LX/BTu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/7Jg;

.field public final synthetic b:LX/BUJ;

.field public final synthetic c:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;LX/7Jg;LX/BUJ;)V
    .locals 0

    .prologue
    .line 1788059
    iput-object p1, p0, LX/BTu;->c:LX/BUA;

    iput-object p2, p0, LX/BTu;->a:LX/7Jg;

    iput-object p3, p0, LX/BTu;->b:LX/BUJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1788061
    iget-object v0, p0, LX/BTu;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/BTu;->a:LX/7Jg;

    iget-object v1, v1, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/BTu;->b:LX/BUJ;

    iget-object v0, p0, LX/BTu;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->g:Ljava/util/HashMap;

    iget-object v2, p0, LX/BTu;->a:LX/7Jg;

    iget-object v2, v2, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUM;

    iget-object v0, v0, LX/BUM;->b:LX/BUJ;

    if-eq v1, v0, :cond_1

    .line 1788062
    :cond_0
    :goto_0
    return-void

    .line 1788063
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 1788064
    iget-object v0, p0, LX/BTu;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->c:LX/BTy;

    iget-object v1, p0, LX/BTu;->a:LX/7Jg;

    iget-object v1, v1, LX/7Jg;->a:Ljava/lang/String;

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/BTy;->a(Ljava/lang/String;Ljava/lang/Exception;I)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1788060
    return-void
.end method
