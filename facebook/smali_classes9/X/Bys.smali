.class public LX/Bys;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Byq;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Byt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1837930
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bys;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Byt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837965
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1837966
    iput-object p1, p0, LX/Bys;->b:LX/0Ot;

    .line 1837967
    return-void
.end method

.method public static a(LX/0QB;)LX/Bys;
    .locals 4

    .prologue
    .line 1837954
    const-class v1, LX/Bys;

    monitor-enter v1

    .line 1837955
    :try_start_0
    sget-object v0, LX/Bys;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837956
    sput-object v2, LX/Bys;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837957
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837958
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837959
    new-instance v3, LX/Bys;

    const/16 p0, 0x1e34

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bys;-><init>(LX/0Ot;)V

    .line 1837960
    move-object v0, v3

    .line 1837961
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837962
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bys;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837963
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837964
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837968
    const v0, 0x34a0b505

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1837943
    check-cast p2, LX/Byr;

    .line 1837944
    iget-object v0, p0, LX/Bys;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Byt;

    iget-object v1, p2, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1837945
    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1837946
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v5, 0x4ed245b

    if-ne v2, v5, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1837947
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v2

    .line 1837948
    iget-object v5, v0, LX/Byt;->a:Landroid/content/res/Resources;

    const v6, 0x7f0f006f

    new-array p0, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, v4

    invoke-virtual {v5, v6, v2, p0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1837949
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1837950
    const v3, 0x34a0b505

    const/4 v5, 0x0

    invoke-static {p1, v3, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1837951
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1837952
    return-object v0

    :cond_0
    move v2, v4

    .line 1837953
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1837931
    invoke-static {}, LX/1dS;->b()V

    .line 1837932
    iget v0, p1, LX/1dQ;->b:I

    .line 1837933
    packed-switch v0, :pswitch_data_0

    .line 1837934
    :goto_0
    return-object v2

    .line 1837935
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1837936
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1837937
    check-cast v1, LX/Byr;

    .line 1837938
    iget-object v3, p0, LX/Bys;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837939
    invoke-static {v3}, LX/C66;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1837940
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->bu()I

    move-result p2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aV()I

    move-result p0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v1

    invoke-static {p1, p2, p0, v1}, LX/C66;->a(Landroid/content/Context;III)LX/0hs;

    move-result-object p1

    .line 1837941
    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1837942
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x34a0b505
        :pswitch_0
    .end packed-switch
.end method
