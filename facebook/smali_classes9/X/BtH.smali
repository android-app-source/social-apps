.class public LX/BtH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/text/Layout$Alignment;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/1WB;

.field private final c:LX/0tO;

.field private final d:I

.field private final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1828945
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/BtH;->a:Landroid/text/Layout$Alignment;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1WB;LX/0tO;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828940
    iput-object p2, p0, LX/BtH;->b:LX/1WB;

    .line 1828941
    iput-object p3, p0, LX/BtH;->c:LX/0tO;

    .line 1828942
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/BtH;->d:I

    .line 1828943
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/BtH;->e:I

    .line 1828944
    return-void
.end method

.method private static a(LX/1z6;)I
    .locals 1

    .prologue
    .line 1828938
    if-eqz p0, :cond_0

    iget v0, p0, LX/1z6;->b:I

    :goto_0
    invoke-static {v0}, LX/1W8;->a(I)I

    move-result v0

    return v0

    :cond_0
    const/16 v0, 0xe

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 2

    .prologue
    .line 1828937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/BtH;
    .locals 6

    .prologue
    .line 1828926
    const-class v1, LX/BtH;

    monitor-enter v1

    .line 1828927
    :try_start_0
    sget-object v0, LX/BtH;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828928
    sput-object v2, LX/BtH;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828929
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828930
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828931
    new-instance p0, LX/BtH;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v4

    check-cast v4, LX/1WB;

    invoke-static {v0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v5

    check-cast v5, LX/0tO;

    invoke-direct {p0, v3, v4, v5}, LX/BtH;-><init>(Landroid/content/Context;LX/1WB;LX/0tO;)V

    .line 1828932
    move-object v0, p0

    .line 1828933
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828934
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BtH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828935
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/1z6;)I
    .locals 1

    .prologue
    .line 1828925
    iget v0, p0, LX/1z6;->g:I

    return v0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 2

    .prologue
    .line 1828924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static c(LX/1z6;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1828923
    iget-object v0, p0, LX/1z6;->f:Ljava/lang/String;

    return-object v0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Z
    .locals 2

    .prologue
    .line 1828946
    invoke-static {p0}, LX/BtH;->b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {p0}, LX/BtH;->c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Landroid/text/Layout$Alignment;
    .locals 3

    .prologue
    .line 1828913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1828914
    sget-object v0, LX/BtH;->a:Landroid/text/Layout$Alignment;

    .line 1828915
    :goto_0
    return-object v0

    .line 1828916
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1828917
    sget-object v0, LX/BtH;->a:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 1828918
    :sswitch_0
    const-string v2, "CENTER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "LEFT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "RIGHT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    .line 1828919
    :pswitch_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 1828920
    :pswitch_1
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 1828921
    :pswitch_2
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x239807 -> :sswitch_1
        0x4a5c9fc -> :sswitch_2
        0x7645c055 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static f(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 3

    .prologue
    .line 1828888
    invoke-static {p1}, LX/BtH;->d(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BtH;->c:LX/0tO;

    const/4 p1, 0x0

    const/4 v1, 0x0

    .line 1828889
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-short p0, LX/0wk;->y:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1828890
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget p0, LX/0wk;->D:F

    invoke-interface {v2, p0, p1}, LX/0ad;->a(FF)F

    move-result v2

    .line 1828891
    cmpl-float p0, v2, p1

    if-lez p0, :cond_0

    .line 1828892
    iget-object v1, v0, LX/0tO;->c:Landroid/content/Context;

    iget-object p0, v0, LX/0tO;->b:LX/0hB;

    invoke-virtual {p0}, LX/0hB;->f()I

    move-result p0

    int-to-float p0, p0

    invoke-static {v1, p0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 1828893
    :cond_0
    :goto_0
    move v0, v1

    .line 1828894
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1828895
    :cond_2
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget-short p0, LX/0wk;->q:S

    invoke-interface {v2, p0, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1828896
    iget-object v2, v0, LX/0tO;->a:LX/0ad;

    sget p0, LX/0wk;->n:I

    invoke-interface {v2, p0, v1}, LX/0ad;->a(II)I

    move-result v1

    goto :goto_0
.end method

.method private static g(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)F
    .locals 2

    .prologue
    .line 1828908
    invoke-static {p1}, LX/BtH;->d(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1828909
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1828910
    :goto_0
    return v0

    .line 1828911
    :cond_0
    iget-object v0, p0, LX/BtH;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->n()I

    move-result v0

    int-to-float v0, v0

    .line 1828912
    iget-object v1, p0, LX/BtH;->c:LX/0tO;

    invoke-virtual {v1}, LX/0tO;->q()F

    move-result v1

    mul-float/2addr v1, v0

    invoke-static {v0, v1}, LX/87X;->a(FF)F

    move-result v0

    goto :goto_0
.end method

.method private static h(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 1

    .prologue
    .line 1828907
    invoke-static {p1}, LX/BtH;->d(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BtH;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->p()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/BtH;->d:I

    goto :goto_0
.end method

.method private static i(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 1

    .prologue
    .line 1828906
    invoke-static {p1}, LX/BtH;->d(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BtH;->c:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->o()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/BtH;->e:I

    goto :goto_0
.end method

.method private static j(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static l(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BtG;
    .locals 16
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828897
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1828898
    :cond_0
    const/4 v1, 0x0

    .line 1828899
    :goto_0
    return-object v1

    .line 1828900
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v15

    .line 1828901
    move-object/from16 v0, p0

    iget-object v1, v0, LX/BtH;->b:LX/1WB;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v6

    .line 1828902
    new-instance v1, LX/BtG;

    invoke-static {v15}, LX/BtH;->a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v2

    invoke-static {v15}, LX/BtH;->b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v3

    invoke-static {v6}, LX/BtH;->b(LX/1z6;)I

    move-result v4

    invoke-static {v6}, LX/BtH;->a(LX/1z6;)I

    move-result v5

    invoke-static {v6}, LX/BtH;->c(LX/1z6;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v15}, LX/BtH;->e(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Landroid/text/Layout$Alignment;

    move-result-object v7

    invoke-static {v15}, LX/BtH;->c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/BtH;->f(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v9

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/BtH;->g(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)F

    move-result v10

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/BtH;->h(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v11

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/BtH;->i(LX/BtH;Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v12

    invoke-static {v15}, LX/BtH;->j(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v15}, LX/BtH;->k(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v15}, LX/BtH;->l(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {v1 .. v15}, LX/BtG;-><init>(IIIILjava/lang/String;Landroid/text/Layout$Alignment;Ljava/lang/String;IFIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
