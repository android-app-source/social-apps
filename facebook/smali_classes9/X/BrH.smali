.class public LX/BrH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BrF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BrI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1826233
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BrH;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BrI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826242
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1826243
    iput-object p1, p0, LX/BrH;->b:LX/0Ot;

    .line 1826244
    return-void
.end method

.method public static a(LX/0QB;)LX/BrH;
    .locals 4

    .prologue
    .line 1826245
    const-class v1, LX/BrH;

    monitor-enter v1

    .line 1826246
    :try_start_0
    sget-object v0, LX/BrH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826247
    sput-object v2, LX/BrH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826248
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826249
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826250
    new-instance v3, LX/BrH;

    const/16 p0, 0x1d18

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BrH;-><init>(LX/0Ot;)V

    .line 1826251
    move-object v0, v3

    .line 1826252
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826253
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BrH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826254
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1826236
    check-cast p2, LX/BrG;

    .line 1826237
    iget-object v0, p0, LX/BrH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BrI;

    iget-object v1, p2, LX/BrG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826238
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1826239
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1826240
    iget-object p0, v0, LX/BrI;->a:LX/Ap5;

    invoke-virtual {p0, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    invoke-static {v2}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1826241
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1826234
    invoke-static {}, LX/1dS;->b()V

    .line 1826235
    const/4 v0, 0x0

    return-object v0
.end method
