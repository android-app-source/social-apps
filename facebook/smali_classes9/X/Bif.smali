.class public final LX/Bif;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bih;


# direct methods
.method public constructor <init>(LX/Bih;)V
    .locals 0

    .prologue
    .line 1810693
    iput-object p1, p0, LX/Bif;->a:LX/Bih;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x5c6937b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810694
    check-cast p1, Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 1810695
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->toggle()V

    .line 1810696
    iget-object v1, p0, LX/Bif;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->isChecked()Z

    move-result v2

    .line 1810697
    iput-boolean v2, v1, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    .line 1810698
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1810699
    iget-object v1, p0, LX/Bif;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->d:Lcom/facebook/resources/ui/FbCheckedTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 1810700
    iget-object v1, p0, LX/Bif;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v1, v2}, Lcom/facebook/events/create/EventCompositionModel;->a(LX/03R;)Lcom/facebook/events/create/EventCompositionModel;

    .line 1810701
    :cond_0
    const v1, 0x1c8874c3

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
