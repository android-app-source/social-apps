.class public LX/BGN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/BMP;

.field private final b:LX/B5l;

.field public c:LX/1RN;


# direct methods
.method public constructor <init>(LX/BMP;LX/B5l;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1767009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767010
    const/4 v0, 0x0

    iput-object v0, p0, LX/BGN;->c:LX/1RN;

    .line 1767011
    iput-object p1, p0, LX/BGN;->a:LX/BMP;

    .line 1767012
    iput-object p2, p0, LX/BGN;->b:LX/B5l;

    .line 1767013
    return-void
.end method

.method public static b(LX/0QB;)LX/BGN;
    .locals 3

    .prologue
    .line 1767014
    new-instance v2, LX/BGN;

    invoke-static {p0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v0

    check-cast v0, LX/BMP;

    invoke-static {p0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v1

    check-cast v1, LX/B5l;

    invoke-direct {v2, v0, v1}, LX/BGN;-><init>(LX/BMP;LX/B5l;)V

    .line 1767015
    return-object v2
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x7c52313b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1767016
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1767017
    iget-object v1, p0, LX/BGN;->c:LX/1RN;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1767018
    iget-object v1, p0, LX/BGN;->a:LX/BMP;

    iget-object v2, p0, LX/BGN;->c:LX/1RN;

    iget-object v3, p0, LX/BGN;->b:LX/B5l;

    iget-object v4, p0, LX/BGN;->c:LX/1RN;

    invoke-virtual {v3, v4}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v3

    invoke-virtual {v3}, LX/B5n;->a()LX/B5p;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1767019
    const v1, 0x7a648098

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
