.class public LX/CXN;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

.field public final b:Lcom/facebook/fig/button/FigButton;

.field public final c:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909911
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CXN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909912
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909913
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CXN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909914
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909915
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909916
    const v0, 0x7f030fae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909917
    const v0, 0x7f0d25e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/CXN;->b:Lcom/facebook/fig/button/FigButton;

    .line 1909918
    const v0, 0x7f0d25e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/CXN;->c:Lcom/facebook/fig/button/FigButton;

    .line 1909919
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {p0, v0}, LX/CSi;->b(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iput-object v0, p0, LX/CXN;->a:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    .line 1909920
    iget-object v0, p0, LX/CXN;->a:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/CXN;->addView(Landroid/view/View;I)V

    .line 1909921
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/CXN;->setOrientation(I)V

    .line 1909922
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/CXN;->setVisibility(I)V

    .line 1909923
    return-void
.end method
