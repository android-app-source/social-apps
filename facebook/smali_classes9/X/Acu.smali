.class public final LX/Acu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

.field public final synthetic b:LX/Acy;


# direct methods
.method public constructor <init>(LX/Acy;Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V
    .locals 0

    .prologue
    .line 1693266
    iput-object p1, p0, LX/Acu;->b:LX/Acy;

    iput-object p2, p0, LX/Acu;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1693267
    iget-object v0, p0, LX/Acu;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V

    .line 1693268
    iget-object v0, p0, LX/Acu;->b:LX/Acy;

    iget-object v0, v0, LX/Acy;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Acy;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to fetch connected donation fundraisers for broadcaster"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1693269
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693270
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1693271
    if-eqz p1, :cond_0

    .line 1693272
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693273
    if-nez v0, :cond_1

    .line 1693274
    :cond_0
    iget-object v0, p0, LX/Acu;->a:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V

    .line 1693275
    :goto_0
    return-void

    .line 1693276
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693277
    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel;

    .line 1693278
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;

    move-result-object v0

    .line 1693279
    iget-object v1, p0, LX/Acu;->b:LX/Acy;

    iget-object v1, v1, LX/Acy;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$3$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$3$1;-><init>(LX/Acu;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V

    const v0, 0x70170d73

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
