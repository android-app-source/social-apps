.class public final LX/C3O;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C3Q;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C3P;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C3Q",
            "<TE;>.CondensedStoryReshareComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C3Q;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C3Q;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1845582
    iput-object p1, p0, LX/C3O;->b:LX/C3Q;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1845583
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C3O;->c:[Ljava/lang/String;

    .line 1845584
    iput v3, p0, LX/C3O;->d:I

    .line 1845585
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C3O;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C3O;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C3O;LX/1De;IILX/C3P;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C3Q",
            "<TE;>.CondensedStoryReshareComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1845586
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1845587
    iput-object p4, p0, LX/C3O;->a:LX/C3P;

    .line 1845588
    iget-object v0, p0, LX/C3O;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1845589
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/C3O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C3Q",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845590
    iget-object v0, p0, LX/C3O;->a:LX/C3P;

    iput-object p1, v0, LX/C3P;->b:LX/1Pb;

    .line 1845591
    iget-object v0, p0, LX/C3O;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845592
    return-object p0
.end method

.method public final a(LX/C33;)LX/C3O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C33;",
            ")",
            "LX/C3Q",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845593
    iget-object v0, p0, LX/C3O;->a:LX/C3P;

    iput-object p1, v0, LX/C3P;->a:LX/C33;

    .line 1845594
    iget-object v0, p0, LX/C3O;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845595
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1845596
    invoke-super {p0}, LX/1X5;->a()V

    .line 1845597
    const/4 v0, 0x0

    iput-object v0, p0, LX/C3O;->a:LX/C3P;

    .line 1845598
    iget-object v0, p0, LX/C3O;->b:LX/C3Q;

    iget-object v0, v0, LX/C3Q;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1845599
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C3Q;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845600
    iget-object v1, p0, LX/C3O;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C3O;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C3O;->d:I

    if-ge v1, v2, :cond_2

    .line 1845601
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1845602
    :goto_0
    iget v2, p0, LX/C3O;->d:I

    if-ge v0, v2, :cond_1

    .line 1845603
    iget-object v2, p0, LX/C3O;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1845604
    iget-object v2, p0, LX/C3O;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1845605
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1845606
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1845607
    :cond_2
    iget-object v0, p0, LX/C3O;->a:LX/C3P;

    .line 1845608
    invoke-virtual {p0}, LX/C3O;->a()V

    .line 1845609
    return-object v0
.end method
