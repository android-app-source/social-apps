.class public final LX/C0Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 1841009
    iput-object p1, p0, LX/C0Z;->b:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    iput-object p2, p0, LX/C0Z;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x2

    const v2, 0x348bda89

    invoke-static {v0, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1841010
    iget-object v0, p0, LX/C0Z;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 1841011
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    .line 1841012
    new-array v0, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    invoke-static {v4, v0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1841013
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1841014
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v4, v7, v0, v3}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1841015
    iget-object v0, p0, LX/C0Z;->b:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/C0Z;->b:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->e:Landroid/content/Context;

    invoke-interface {v0, v3, v5, v6, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1841016
    const v0, 0x4192403a

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    :cond_0
    move-object v0, v1

    .line 1841017
    goto :goto_0
.end method
