.class public LX/CgW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1927204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1927205
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I
    .locals 3
    .param p0    # Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    .line 1927206
    if-nez p0, :cond_0

    .line 1927207
    :goto_0
    :pswitch_0
    return v0

    .line 1927208
    :cond_0
    sget-object v1, LX/CgV;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1927209
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1927210
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1927211
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I
    .locals 1

    .prologue
    .line 1927212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne p0, v0, :cond_0

    .line 1927213
    const v0, 0x7f0b1689

    .line 1927214
    :goto_0
    return v0

    .line 1927215
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne p0, v0, :cond_1

    .line 1927216
    const v0, 0x7f0b168a

    goto :goto_0

    .line 1927217
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne p0, v0, :cond_2

    .line 1927218
    const v0, 0x7f0b168b

    goto :goto_0

    .line 1927219
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne p0, v0, :cond_3

    .line 1927220
    const v0, 0x7f0b168c

    goto :goto_0

    .line 1927221
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne p0, v0, :cond_4

    .line 1927222
    const v0, 0x7f0b168d

    goto :goto_0

    .line 1927223
    :cond_4
    const v0, 0x7f0b1688

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I
    .locals 2

    .prologue
    .line 1927224
    sget-object v0, LX/CgV;->c:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1927225
    const v0, 0x7f0b008a

    :goto_0
    return v0

    .line 1927226
    :pswitch_0
    const v0, 0x7f0b008e

    goto :goto_0

    .line 1927227
    :pswitch_1
    const v0, 0x7f0b008d

    goto :goto_0

    .line 1927228
    :pswitch_2
    const v0, 0x7f0b008b

    goto :goto_0

    .line 1927229
    :pswitch_3
    const v0, 0x7f0b008a

    goto :goto_0

    .line 1927230
    :pswitch_4
    const v0, 0x7f0b0089

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I
    .locals 3
    .param p0    # Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x5

    .line 1927231
    if-nez p0, :cond_0

    .line 1927232
    const/4 v0, 0x2

    .line 1927233
    :goto_0
    :pswitch_0
    return v0

    .line 1927234
    :cond_0
    sget-object v1, LX/CgV;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1927235
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 1927236
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1927237
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
