.class public LX/C6M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/C6L;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1Rg;

.field private final b:I


# direct methods
.method public constructor <init>(LX/1Rg;I)V
    .locals 0

    .prologue
    .line 1849839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849840
    iput-object p1, p0, LX/C6M;->a:LX/1Rg;

    .line 1849841
    iput p2, p0, LX/C6M;->b:I

    .line 1849842
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1849843
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1849844
    check-cast p3, LX/C6L;

    .line 1849845
    iget-object v0, p3, LX/C6L;->a:LX/C6a;

    .line 1849846
    iget-boolean v1, v0, LX/C6a;->h:Z

    move v0, v1

    .line 1849847
    if-nez v0, :cond_0

    .line 1849848
    iget-object v0, p0, LX/C6M;->a:LX/1Rg;

    const-wide/16 v2, 0x190

    const/4 v4, 0x0

    iget v5, p0, LX/C6M;->b:I

    new-instance v6, LX/C6K;

    invoke-direct {v6, p0, p3}, LX/C6K;-><init>(LX/C6M;LX/C6L;)V

    move-object v1, p4

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849849
    :cond_0
    return-void
.end method
