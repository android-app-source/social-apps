.class public LX/AYE;
.super LX/AWU;
.source ""

# interfaces
.implements LX/AWi;
.implements LX/1bG;
.implements LX/AYA;
.implements LX/AYC;
.implements LX/AYD;


# instance fields
.field private A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Z

.field private F:J

.field public G:J

.field public H:J

.field private I:LX/Aal;

.field public a:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Aa0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Aam;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/AYs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AYr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field public final m:LX/AWl;

.field public final n:LX/AXm;

.field private final o:LX/AYZ;

.field public p:LX/AYv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/Aa8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/AaA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/2EJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/AYG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/AZz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AZZ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:LX/AY1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:LX/AV1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684990
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684991
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684992
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684993
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684837
    invoke-direct {p0, p1, p2, p3}, LX/AWU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684838
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/AYE;->l:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 1684839
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AYE;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v3

    check-cast v3, LX/AVT;

    invoke-static {v0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v4

    check-cast v4, LX/Ac6;

    invoke-static {v0}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object v5

    check-cast v5, LX/AYq;

    const-class v6, LX/Aa0;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Aa0;

    const-class v7, LX/Aam;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Aam;

    invoke-static {v0}, LX/AYs;->a(LX/0QB;)LX/AYs;

    move-result-object v8

    check-cast v8, LX/AYs;

    invoke-static {v0}, LX/AYr;->a(LX/0QB;)LX/AYr;

    move-result-object p2

    check-cast p2, LX/AYr;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p3

    check-cast p3, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    check-cast v0, LX/1b4;

    iput-object v3, v2, LX/AYE;->a:LX/AVT;

    iput-object v4, v2, LX/AYE;->b:LX/Ac6;

    iput-object v5, v2, LX/AYE;->c:LX/AYq;

    iput-object v6, v2, LX/AYE;->f:LX/Aa0;

    iput-object v7, v2, LX/AYE;->g:LX/Aam;

    iput-object v8, v2, LX/AYE;->h:LX/AYs;

    iput-object p2, v2, LX/AYE;->i:LX/AYr;

    iput-object p3, v2, LX/AYE;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, v2, LX/AYE;->k:LX/1b4;

    .line 1684840
    new-instance v0, LX/AWl;

    invoke-direct {v0, p1}, LX/AWl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYE;->m:LX/AWl;

    .line 1684841
    const/4 v0, 0x1

    new-array v0, v0, [LX/AWT;

    const/4 v1, 0x0

    iget-object v2, p0, LX/AYE;->m:LX/AWl;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/AWU;->a([LX/AWT;)V

    .line 1684842
    new-instance v0, LX/AXm;

    invoke-direct {v0, p1}, LX/AXm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYE;->n:LX/AXm;

    .line 1684843
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1684844
    new-instance v0, LX/AYZ;

    invoke-direct {v0, p1}, LX/AYZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYE;->o:LX/AYZ;

    .line 1684845
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1684846
    return-void
.end method

.method private a(ZJ)V
    .locals 8

    .prologue
    .line 1684994
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    if-eqz v0, :cond_0

    .line 1684995
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    .line 1684996
    iget-object v2, v0, LX/AaA;->j:Landroid/widget/FrameLayout;

    const-wide/16 v6, 0xfa

    move v3, p1

    move-wide v4, p2

    invoke-static/range {v2 .. v7}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1684997
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0, p1, p2, p3}, LX/AaA;->a(ZJ)V

    .line 1684998
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    .line 1684999
    iget-object v2, v0, LX/AaA;->l:Landroid/widget/LinearLayout;

    const-wide/16 v6, 0xfa

    move v3, p1

    move-wide v4, p2

    invoke-static/range {v2 .. v7}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1685000
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0xfa

    const/4 v6, 0x0

    .line 1685001
    iget-object v0, p0, LX/AYE;->q:LX/Aa8;

    invoke-virtual {v0, v6}, LX/Aa8;->setButtonEnabled(Z)V

    .line 1685002
    new-instance v7, LX/AY4;

    invoke-direct {v7, p0, p1}, LX/AY4;-><init>(LX/AYE;Z)V

    .line 1685003
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->f:LX/Aby;

    .line 1685004
    iput-boolean p1, v0, LX/Aby;->H:Z

    .line 1685005
    iget-boolean v0, p0, LX/AYE;->C:Z

    if-eqz v0, :cond_5

    .line 1685006
    iget-object v0, p0, LX/AYE;->t:LX/AYG;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AYG;

    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->f:LX/Aby;

    .line 1685007
    iget v9, v1, LX/Aby;->D:I

    iget v10, v1, LX/Aby;->z:I

    if-ne v9, v10, :cond_7

    const/4 v9, 0x1

    :goto_0
    move v1, v9

    .line 1685008
    const-wide/16 v11, 0x0

    .line 1685009
    iput-boolean p1, v0, LX/AYG;->f:Z

    .line 1685010
    if-eqz p1, :cond_8

    .line 1685011
    const/4 v9, 0x1

    invoke-virtual {v0, v9, v11, v12}, LX/AYG;->a(ZJ)V

    .line 1685012
    :cond_0
    :goto_1
    if-eqz p1, :cond_3

    move-wide v0, v2

    :goto_2
    invoke-direct {p0, p1, v0, v1}, LX/AYE;->a(ZJ)V

    .line 1685013
    if-eqz p1, :cond_4

    .line 1685014
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0}, LX/AaA;->g()V

    .line 1685015
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0}, LX/AaA;->getCreativeToolsTrayHeight()I

    move-result v0

    neg-int v0, v0

    .line 1685016
    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685017
    iget-object v1, p0, LX/AYE;->o:LX/AYZ;

    .line 1685018
    iget-object v9, v1, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685019
    :goto_3
    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, LX/AY5;

    invoke-direct {v2, p0, v0, p1, v7}, LX/AY5;-><init>(LX/AYE;IZLandroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685020
    :goto_4
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->f:LX/Aby;

    if-nez p1, :cond_1

    const/4 v6, 0x1

    .line 1685021
    :cond_1
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1685022
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    if-eqz v1, :cond_2

    iget-boolean v1, v0, LX/Aby;->v:Z

    if-nez v1, :cond_2

    .line 1685023
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1685024
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    invoke-virtual {v1, v6}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->setTitleVisibility(Z)V

    .line 1685025
    :cond_2
    return-void

    :cond_3
    move-wide v0, v4

    .line 1685026
    goto :goto_2

    .line 1685027
    :cond_4
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685028
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->b:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1685029
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    .line 1685030
    iget-object v1, v0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v1, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685031
    move v0, v6

    goto :goto_3

    .line 1685032
    :cond_5
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0, p1, v4, v5}, LX/AaA;->a(ZJ)V

    .line 1685033
    if-eqz p1, :cond_6

    .line 1685034
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0}, LX/AaA;->g()V

    .line 1685035
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    invoke-virtual {v0}, LX/AaA;->getCreativeToolsTrayHeight()I

    move-result v0

    .line 1685036
    :goto_5
    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v4, v0

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v4, p0, LX/AYE;->l:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1685037
    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    iget-object v1, v1, LX/AWl;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/AYE;->l:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_4

    :cond_6
    move v0, v6

    .line 1685038
    goto :goto_5

    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 1685039
    :cond_8
    if-eqz v1, :cond_0

    .line 1685040
    const/4 v9, 0x0

    invoke-virtual {v0, v9, v11, v12}, LX/AYG;->a(ZJ)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1685041
    sget-object v0, LX/AY7;->b:[I

    invoke-virtual {p2}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1685042
    :cond_0
    :goto_0
    sget-object v0, LX/AY7;->b:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1685043
    :cond_1
    :goto_1
    return-void

    .line 1685044
    :pswitch_0
    iget-boolean v0, p0, LX/AYE;->C:Z

    if-eqz v0, :cond_0

    .line 1685045
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/AWl;->setAlpha(F)V

    goto :goto_0

    .line 1685046
    :pswitch_1
    iget-object v0, p0, LX/AYE;->s:LX/2EJ;

    if-eqz v0, :cond_0

    .line 1685047
    iget-object v0, p0, LX/AYE;->s:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    goto :goto_0

    .line 1685048
    :pswitch_2
    iget-object v0, p0, LX/AYE;->s:LX/2EJ;

    if-nez v0, :cond_2

    .line 1685049
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, LX/AYE;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080ce9

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080ce8

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080cb7

    new-instance p1, LX/AY6;

    invoke-direct {p1, p0}, LX/AY6;-><init>(LX/AYE;)V

    invoke-virtual {v0, v1, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080cb8

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, LX/AYE;->s:LX/2EJ;

    .line 1685050
    :cond_2
    iget-object v0, p0, LX/AYE;->s:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1685051
    goto :goto_1

    .line 1685052
    :pswitch_3
    iget-boolean v0, p0, LX/AYE;->C:Z

    if-eqz v0, :cond_3

    .line 1685053
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AWl;->setAlpha(F)V

    .line 1685054
    :cond_3
    :pswitch_4
    iget-object v0, p0, LX/AYE;->q:LX/Aa8;

    if-eqz v0, :cond_1

    .line 1685055
    iget-object v0, p0, LX/AYE;->q:LX/Aa8;

    .line 1685056
    iget-object v1, v0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 p0, 0x4

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685057
    goto :goto_1

    .line 1685058
    :pswitch_5
    iget-object v0, p0, LX/AYE;->q:LX/Aa8;

    if-eqz v0, :cond_1

    .line 1685059
    iget-object v0, p0, LX/AYE;->q:LX/Aa8;

    .line 1685060
    iget-object v1, v0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685061
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 2

    .prologue
    .line 1685062
    invoke-super {p0, p1, p2}, LX/AWU;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1685063
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    invoke-virtual {v0, p0}, LX/AYq;->b(LX/1bG;)V

    .line 1685064
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    iget-object v1, p0, LX/AYE;->p:LX/AYv;

    invoke-virtual {v0, v1}, LX/AYq;->b(LX/1bG;)V

    .line 1685065
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    iget-object v1, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v0, v1}, LX/AYq;->b(LX/1bG;)V

    .line 1685066
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    iget-object v1, p0, LX/AYE;->w:LX/AY1;

    invoke-virtual {v0, v1}, LX/AYq;->b(LX/1bG;)V

    .line 1685067
    iget-object v0, p0, LX/AYE;->u:LX/AZz;

    if-eqz v0, :cond_0

    .line 1685068
    iget-object v0, p0, LX/AYE;->u:LX/AZz;

    invoke-virtual {v0}, LX/AZz;->b()V

    .line 1685069
    :cond_0
    iget-object v0, p0, LX/AYE;->I:LX/Aal;

    if-eqz v0, :cond_2

    .line 1685070
    iget-object v0, p0, LX/AYE;->I:LX/Aal;

    .line 1685071
    iget-object v1, v0, LX/Aal;->g:LX/1Zp;

    if-eqz v1, :cond_1

    .line 1685072
    iget-object v1, v0, LX/Aal;->g:LX/1Zp;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/1Zp;->cancel(Z)Z

    .line 1685073
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/AYE;->I:LX/Aal;

    .line 1685074
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 5

    .prologue
    .line 1685075
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    iget-object v1, p0, LX/AYE;->z:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 1685076
    iget-object v3, v0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v2, v4}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1685077
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    .line 1685078
    iget-object v1, v0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b()V

    .line 1685079
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    .line 1685080
    iget-object v1, v0, LX/AWl;->f:LX/Aby;

    .line 1685081
    iget-object v2, v1, LX/Aby;->a:LX/AcX;

    .line 1685082
    if-eqz p1, :cond_0

    .line 1685083
    iget-object v3, v2, LX/AcX;->r:LX/AeJ;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, LX/AeJ;->e(Ljava/lang/String;)V

    .line 1685084
    :cond_0
    iget-object v1, v0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v1, p1}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1685085
    const/4 v0, 0x0

    iput-object v0, p0, LX/AYE;->I:LX/Aal;

    .line 1685086
    return-void
.end method

.method public final a(Ljava/lang/String;JJLcom/facebook/ipc/composer/intent/ComposerTargetData;LX/AY1;LX/AV1;ZZZFZLcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 12

    .prologue
    .line 1685087
    iput-object p1, p0, LX/AYE;->z:Ljava/lang/String;

    .line 1685088
    move/from16 v0, p9

    iput-boolean v0, p0, LX/AYE;->C:Z

    .line 1685089
    move/from16 v0, p10

    iput-boolean v0, p0, LX/AYE;->E:Z

    .line 1685090
    invoke-virtual {p0}, LX/AYE;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, LX/AYE;->D:Z

    .line 1685091
    iget-object v2, p0, LX/AYE;->m:LX/AWl;

    iget-boolean v7, p0, LX/AYE;->D:Z

    iget-boolean v8, p0, LX/AYE;->E:Z

    move-object v3, p1

    move-object/from16 v4, p6

    move/from16 v5, p9

    move/from16 v6, p12

    move/from16 v9, p11

    move-object/from16 v10, p15

    invoke-virtual/range {v2 .. v10}, LX/AWl;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZFZZZLcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1685092
    iput-wide p2, p0, LX/AYE;->F:J

    .line 1685093
    move-object/from16 v0, p7

    iput-object v0, p0, LX/AYE;->w:LX/AY1;

    .line 1685094
    move-object/from16 v0, p8

    iput-object v0, p0, LX/AYE;->y:LX/AV1;

    .line 1685095
    move-object/from16 v0, p14

    iput-object v0, p0, LX/AYE;->x:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1685096
    iget-object v2, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {v2, p0}, LX/AWl;->setListener(LX/AWi;)V

    .line 1685097
    iget-object v2, p0, LX/AYE;->n:LX/AXm;

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, LX/AXm;->a(J)V

    .line 1685098
    iget-object v2, p0, LX/AYE;->n:LX/AXm;

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/AXm;->setIsAudioLive(Z)V

    .line 1685099
    iget-object v2, p0, LX/AYE;->o:LX/AYZ;

    iget-boolean v3, p0, LX/AYE;->D:Z

    invoke-virtual {v2, v3}, LX/AYZ;->a(Z)V

    .line 1685100
    if-eqz p13, :cond_0

    if-nez p10, :cond_0

    .line 1685101
    new-instance v2, LX/AYv;

    invoke-virtual {p0}, LX/AYE;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/AYv;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/AYE;->p:LX/AYv;

    .line 1685102
    iget-object v2, p0, LX/AYE;->p:LX/AYv;

    invoke-virtual {p0, v2}, LX/AWU;->b(LX/AWT;)V

    .line 1685103
    iget-object v2, p0, LX/AYE;->c:LX/AYq;

    invoke-virtual {v2, p0}, LX/AYq;->a(LX/1bG;)V

    .line 1685104
    iget-object v2, p0, LX/AYE;->c:LX/AYq;

    iget-object v3, p0, LX/AYE;->p:LX/AYv;

    invoke-virtual {v2, v3}, LX/AYq;->a(LX/1bG;)V

    .line 1685105
    iget-object v2, p0, LX/AYE;->c:LX/AYq;

    iget-object v3, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v2, v3}, LX/AYq;->a(LX/1bG;)V

    .line 1685106
    iget-object v2, p0, LX/AYE;->c:LX/AYq;

    iget-object v3, p0, LX/AYE;->w:LX/AY1;

    invoke-virtual {v2, v3}, LX/AYq;->a(LX/1bG;)V

    .line 1685107
    iget-object v2, p0, LX/AYE;->p:LX/AYv;

    iget-object v3, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {v3}, LX/AWl;->getToolbarContainer()Landroid/view/View;

    move-result-object v4

    new-instance v5, LX/AY9;

    invoke-direct {v5, p0}, LX/AY9;-><init>(LX/AYE;)V

    iget-boolean v6, p0, LX/AYE;->C:Z

    iget-boolean v7, p0, LX/AYE;->D:Z

    move-object/from16 v3, p14

    invoke-virtual/range {v2 .. v7}, LX/AYv;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;Landroid/view/View;LX/AY8;ZZ)V

    .line 1685108
    iget-object v2, p0, LX/AYE;->h:LX/AYs;

    invoke-virtual {v2, p1}, LX/AYs;->a(Ljava/lang/String;)V

    .line 1685109
    iget-object v2, p0, LX/AYE;->i:LX/AYr;

    invoke-virtual {v2, p1}, LX/AYr;->b(Ljava/lang/String;)V

    .line 1685110
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1685111
    move-object/from16 v0, p6

    iget-wide v2, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1685112
    :goto_1
    iget-object v3, p0, LX/AYE;->h:LX/AYs;

    invoke-virtual {v3, v2}, LX/AYs;->b(Ljava/lang/String;)V

    .line 1685113
    iget-object v3, p0, LX/AYE;->i:LX/AYr;

    invoke-virtual {v3, v2}, LX/AYr;->a(Ljava/lang/String;)V

    .line 1685114
    move-object/from16 v0, p14

    iget-boolean v2, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    if-nez v2, :cond_0

    .line 1685115
    iget-object v2, p0, LX/AYE;->m:LX/AWl;

    iget-object v2, v2, LX/AWl;->f:LX/Aby;

    move-object/from16 v0, p14

    iget v3, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    invoke-virtual {v2, v3}, LX/Aby;->a(I)V

    .line 1685116
    :cond_0
    iget-boolean v2, p0, LX/AYE;->C:Z

    if-eqz v2, :cond_1

    .line 1685117
    new-instance v2, LX/AYG;

    invoke-virtual {p0}, LX/AYE;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/AYG;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/AYE;->t:LX/AYG;

    .line 1685118
    iget-object v2, p0, LX/AYE;->t:LX/AYG;

    iget-boolean v3, p0, LX/AYE;->D:Z

    invoke-virtual {v2, v3}, LX/AYG;->setIsLandscape(Z)V

    .line 1685119
    iget-object v2, p0, LX/AYE;->t:LX/AYG;

    iget-object v3, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {p0, v2, v3}, LX/AWU;->a(LX/AWT;LX/AWT;)V

    .line 1685120
    :cond_1
    return-void

    .line 1685121
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1685122
    :cond_3
    iget-object v2, p0, LX/AYE;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v2}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    .line 1684933
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    if-nez v0, :cond_4

    .line 1684934
    new-instance v0, LX/AaA;

    invoke-virtual {p0}, LX/AYE;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AaA;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYE;->r:LX/AaA;

    .line 1684935
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    iget-object v1, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {p0, v0, v1}, LX/AWU;->a(LX/AWT;LX/AWT;)V

    .line 1684936
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    iget-object v1, p0, LX/AYE;->w:LX/AY1;

    .line 1684937
    iget-object v2, v1, LX/AY1;->l:LX/AVP;

    move-object v2, v2

    .line 1684938
    iput-object v2, v0, LX/AaA;->r:LX/AVP;

    .line 1684939
    iget-object v2, v1, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    move-object v2, v2

    .line 1684940
    iput-object v2, v0, LX/AaA;->s:Lcom/facebook/facecast/FacecastPreviewView;

    .line 1684941
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    iget-boolean v1, p0, LX/AYE;->C:Z

    .line 1684942
    iput-boolean v1, v0, LX/AaA;->v:Z

    .line 1684943
    iget-object v2, v0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    .line 1684944
    iput-boolean v1, v2, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->h:Z

    .line 1684945
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    iget-object v1, p0, LX/AYE;->y:LX/AV1;

    .line 1684946
    iput-object v1, v0, LX/AaA;->t:LX/AV1;

    .line 1684947
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    iget-object v1, p0, LX/AYE;->v:Ljava/util/List;

    .line 1684948
    iget-object v2, v0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    invoke-virtual {v2, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->setCreativeToolsPacks(Ljava/util/List;)V

    .line 1684949
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AZZ;

    .line 1684950
    invoke-virtual {v2}, LX/AZZ;->c()LX/Aa1;

    move-result-object v3

    sget-object v5, LX/Aa1;->CREATIVE_TOOLS_PACK_TYPE_MASK:LX/Aa1;

    if-ne v3, v5, :cond_0

    .line 1684951
    iget-object v3, v0, LX/AaA;->g:LX/1b4;

    .line 1684952
    iget-object v5, v3, LX/1b4;->b:LX/0Uh;

    const/16 v6, 0x33f

    const/4 v1, 0x0

    invoke-virtual {v5, v6, v1}, LX/0Uh;->a(IZ)Z

    move-result v5

    move v3, v5

    .line 1684953
    if-eqz v3, :cond_0

    .line 1684954
    iget-object v3, v0, LX/AaA;->q:LX/AZt;

    if-nez v3, :cond_1

    .line 1684955
    iget-object v3, v0, LX/AaA;->r:LX/AVP;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684956
    iget-object v3, v0, LX/AaA;->t:LX/AV1;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684957
    iget-object v3, v0, LX/AaA;->c:LX/AZu;

    invoke-virtual {v0}, LX/AaA;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v0, LX/AaA;->r:LX/AVP;

    iget-object v1, v0, LX/AaA;->t:LX/AV1;

    invoke-virtual {v3, v5, v6, v1}, LX/AZu;->a(Landroid/content/Context;LX/AVP;LX/AV1;)LX/AZt;

    move-result-object v3

    iput-object v3, v0, LX/AaA;->q:LX/AZt;

    .line 1684958
    iget-object v3, v0, LX/AaA;->q:LX/AZt;

    .line 1684959
    iput-object v0, v3, LX/AZt;->l:LX/AZq;

    .line 1684960
    :cond_1
    iget-object v3, v0, LX/AaA;->q:LX/AZt;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AZt;

    check-cast v2, LX/AZm;

    invoke-virtual {v3, v2}, LX/AZt;->a(LX/AZm;)V

    .line 1684961
    iget-object v2, v0, LX/AaA;->q:LX/AZt;

    const/4 v1, 0x1

    .line 1684962
    iget-object v3, v2, LX/AZt;->m:LX/AZm;

    if-nez v3, :cond_5

    .line 1684963
    :cond_2
    :goto_1
    goto :goto_0

    .line 1684964
    :cond_3
    iget-object v0, p0, LX/AYE;->r:LX/AaA;

    .line 1684965
    iput-object p0, v0, LX/AaA;->u:LX/AYC;

    .line 1684966
    :cond_4
    invoke-direct {p0, p1}, LX/AYE;->b(Z)V

    .line 1684967
    iget-object v0, p0, LX/AYE;->a:LX/AVT;

    const-string v1, "tap"

    invoke-static {v0, p1, v1}, LX/Aa6;->a(LX/AVT;ZLjava/lang/String;)V

    .line 1684968
    return-void

    .line 1684969
    :cond_5
    iget-object v3, v2, LX/AZt;->m:LX/AZm;

    invoke-virtual {v3, v1}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AZl;

    .line 1684970
    if-eqz v3, :cond_2

    .line 1684971
    iget-object v5, v3, LX/AZl;->g:LX/AZk;

    move-object v5, v5

    .line 1684972
    sget-object v6, LX/AZk;->NOT_DOWNLOADED:LX/AZk;

    if-ne v5, v6, :cond_2

    .line 1684973
    invoke-static {v2, v3, v1}, LX/AZt;->a(LX/AZt;LX/AZl;I)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1684974
    iget-boolean v1, p0, LX/AYE;->A:Z

    if-eqz v1, :cond_1

    .line 1684975
    invoke-super {p0}, LX/AWU;->a()Z

    move-result v0

    .line 1684976
    :cond_0
    :goto_0
    return v0

    .line 1684977
    :cond_1
    iget-object v1, p0, LX/AYE;->q:LX/Aa8;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/AYE;->q:LX/Aa8;

    .line 1684978
    iget-boolean v2, v1, LX/Aa8;->b:Z

    move v1, v2

    .line 1684979
    if-eqz v1, :cond_2

    .line 1684980
    iget-object v1, p0, LX/AYE;->q:LX/Aa8;

    .line 1684981
    iget-object v2, v1, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2}, Lcom/facebook/fbui/glyph/GlyphView;->isEnabled()Z

    move-result v2

    move v1, v2

    .line 1684982
    if-eqz v1, :cond_0

    .line 1684983
    iget-object v1, p0, LX/AYE;->q:LX/Aa8;

    .line 1684984
    iput-boolean v3, v1, LX/Aa8;->b:Z

    .line 1684985
    invoke-static {v1}, LX/Aa8;->j(LX/Aa8;)V

    .line 1684986
    invoke-direct {p0, v3}, LX/AYE;->b(Z)V

    .line 1684987
    iget-object v1, p0, LX/AYE;->a:LX/AVT;

    const-string v2, "back"

    invoke-static {v1, v3, v2}, LX/Aa6;->a(LX/AVT;ZLjava/lang/String;)V

    goto :goto_0

    .line 1684988
    :cond_2
    invoke-super {p0}, LX/AWU;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1684989
    invoke-virtual {p0}, LX/AYE;->c()V

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 1684918
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    .line 1684919
    iget-object v1, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v1

    .line 1684920
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    .line 1684921
    iget-object v1, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v1

    .line 1684922
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    if-ne v0, v1, :cond_1

    .line 1684923
    :cond_0
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 1684924
    :goto_0
    return-void

    .line 1684925
    :cond_1
    iget-wide v0, p0, LX/AYE;->G:J

    iget-wide v2, p0, LX/AYE;->F:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 1684926
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-wide v2, p0, LX/AYE;->F:J

    .line 1684927
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, LX/AWl;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x2

    invoke-direct {v1, v4, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1684928
    iget-object v4, v0, LX/AWl;->q:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1684929
    iget-object v4, v0, LX/AWl;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v4}, LX/0ht;->c(Landroid/view/View;)V

    .line 1684930
    invoke-virtual {v1}, LX/0ht;->d()V

    .line 1684931
    goto :goto_0

    .line 1684932
    :cond_2
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->ABOUT_TO_FINISH:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1684904
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    .line 1684905
    iget-object v3, v0, LX/AXm;->a:LX/3Hc;

    invoke-virtual {v3}, LX/3Hc;->a()V

    .line 1684906
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {v0, v1}, LX/AWl;->a(Z)V

    .line 1684907
    iput-boolean v2, p0, LX/AYE;->A:Z

    .line 1684908
    iget-boolean v0, p0, LX/AYE;->B:Z

    if-eqz v0, :cond_0

    .line 1684909
    iput-boolean v1, p0, LX/AYE;->B:Z

    .line 1684910
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v0, v1}, LX/AXm;->setWeakConnection(Z)V

    .line 1684911
    :cond_0
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {v0, v2}, LX/AWl;->setSuspended(Z)V

    .line 1684912
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    .line 1684913
    iget-object v1, v0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e()V

    .line 1684914
    iget-object v0, p0, LX/AYE;->p:LX/AYv;

    if-eqz v0, :cond_1

    .line 1684915
    iget-object v0, p0, LX/AYE;->h:LX/AYs;

    .line 1684916
    iget-object v1, v0, LX/AYs;->a:LX/0if;

    sget-object v2, LX/0ig;->C:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1684917
    :cond_1
    return-void
.end method

.method public getLoggingInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1684901
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    if-eqz v0, :cond_0

    .line 1684902
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v0}, LX/AWT;->getLoggingInfo()Ljava/util/Map;

    move-result-object v0

    .line 1684903
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1684887
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    iget-object v1, p0, LX/AYE;->z:Ljava/lang/String;

    .line 1684888
    iget-object v3, v0, LX/AXm;->a:LX/3Hc;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/3Hc;->a(Z)V

    .line 1684889
    iget-object v3, v0, LX/AXm;->a:LX/3Hc;

    invoke-virtual {v3, v1}, LX/3Hc;->a(Ljava/lang/String;)V

    .line 1684890
    iput-boolean v2, p0, LX/AYE;->A:Z

    .line 1684891
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/AWl;->a(Z)V

    .line 1684892
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    invoke-virtual {v0, v2}, LX/AWl;->setSuspended(Z)V

    .line 1684893
    iget-object v0, p0, LX/AYE;->o:LX/AYZ;

    .line 1684894
    iget-object v1, v0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->f()V

    .line 1684895
    iget-object v0, p0, LX/AYE;->p:LX/AYv;

    if-eqz v0, :cond_0

    .line 1684896
    iget-object v0, p0, LX/AYE;->h:LX/AYs;

    .line 1684897
    const/4 v3, 0x0

    iput v3, v0, LX/AYs;->f:I

    .line 1684898
    const-wide/16 v3, -0x1

    iput-wide v3, v0, LX/AYs;->e:J

    .line 1684899
    iget-object v3, v0, LX/AYs;->a:LX/0if;

    sget-object v4, LX/0ig;->C:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 1684900
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1684882
    iget-boolean v0, p0, LX/AYE;->C:Z

    if-eqz v0, :cond_0

    .line 1684883
    iget-object v0, p0, LX/AYE;->t:LX/AYG;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AYG;

    invoke-virtual {v0, v1, v2, v3}, LX/AYG;->a(ZJ)V

    .line 1684884
    invoke-direct {p0, v1, v2, v3}, LX/AYE;->a(ZJ)V

    .line 1684885
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    const-wide/16 v4, 0xfa

    invoke-static/range {v0 .. v5}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1684886
    :cond_0
    return-void
.end method

.method public final iF_()V
    .locals 11

    .prologue
    .line 1684852
    invoke-super {p0}, LX/AWU;->iF_()V

    .line 1684853
    iget-boolean v2, p0, LX/AYE;->D:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/AYE;->E:Z

    if-nez v2, :cond_0

    .line 1684854
    iget-object v2, p0, LX/AYE;->u:LX/AZz;

    if-nez v2, :cond_1

    .line 1684855
    iget-object v2, p0, LX/AYE;->f:LX/Aa0;

    iget-object v3, p0, LX/AYE;->z:Ljava/lang/String;

    new-instance v4, LX/AY3;

    invoke-direct {v4, p0}, LX/AY3;-><init>(LX/AYE;)V

    .line 1684856
    new-instance v5, LX/AZz;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v2}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    invoke-static {v2}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, LX/AZz;-><init>(LX/0tX;Landroid/os/Handler;LX/1b4;Ljava/lang/String;LX/AY3;)V

    .line 1684857
    move-object v2, v5

    .line 1684858
    iput-object v2, p0, LX/AYE;->u:LX/AZz;

    .line 1684859
    :goto_0
    iget-object v2, p0, LX/AYE;->u:LX/AZz;

    const/4 v9, 0x0

    .line 1684860
    iget-object v5, v2, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v5, :cond_2

    .line 1684861
    :cond_0
    :goto_1
    iget-object v0, p0, LX/AYE;->g:LX/Aam;

    iget-object v1, p0, LX/AYE;->z:Ljava/lang/String;

    .line 1684862
    new-instance v2, LX/Aal;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    move-object v6, v1

    move-object v7, p0

    invoke-direct/range {v2 .. v7}, LX/Aal;-><init>(LX/03V;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/String;LX/AYD;)V

    .line 1684863
    move-object v0, v2

    .line 1684864
    iput-object v0, p0, LX/AYE;->I:LX/Aal;

    .line 1684865
    iget-object v0, p0, LX/AYE;->I:LX/Aal;

    .line 1684866
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Aal;->a$redex0(LX/Aal;I)V

    .line 1684867
    return-void

    .line 1684868
    :cond_1
    iget-object v2, p0, LX/AYE;->u:LX/AZz;

    invoke-virtual {v2}, LX/AZz;->b()V

    goto :goto_0

    .line 1684869
    :cond_2
    iput-boolean v9, v2, LX/AZz;->h:Z

    .line 1684870
    invoke-static {}, LX/8Cg;->a()Ljava/util/List;

    move-result-object v5

    .line 1684871
    new-instance v6, LX/Aax;

    invoke-direct {v6}, LX/Aax;-><init>()V

    move-object v6, v6

    .line 1684872
    const-string v7, "videoID"

    iget-object v8, v2, LX/AZz;->e:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1684873
    iget-object v7, v2, LX/AZz;->d:LX/1b4;

    invoke-virtual {v7}, LX/1b4;->X()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1684874
    const-string v7, "enable_masks"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1684875
    const-string v7, "mask_model_version"

    const-wide/16 v9, 0x5

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1684876
    const-string v7, "mask_sdk_version"

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1684877
    const-string v7, "mask_capabilities"

    invoke-virtual {v6, v7, v5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1684878
    :goto_2
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 1684879
    iget-object v6, v2, LX/AZz;->b:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    iput-object v5, v2, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1684880
    iget-object v5, v2, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v6, LX/AZy;

    invoke-direct {v6, v2}, LX/AZy;-><init>(LX/AZz;)V

    invoke-static {v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_1

    .line 1684881
    :cond_3
    const-string v5, "enable_masks"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto :goto_2
.end method

.method public final j()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x1f4

    const/4 v1, 0x1

    .line 1684847
    iget-boolean v0, p0, LX/AYE;->C:Z

    if-eqz v0, :cond_0

    .line 1684848
    iget-object v0, p0, LX/AYE;->t:LX/AYG;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AYG;

    invoke-virtual {v0, v1, v2, v3}, LX/AYG;->a(ZJ)V

    .line 1684849
    invoke-direct {p0, v1, v2, v3}, LX/AYE;->a(ZJ)V

    .line 1684850
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    const-wide/16 v4, 0xfa

    invoke-static/range {v0 .. v5}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1684851
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 6

    .prologue
    .line 1684832
    iget-object v0, p0, LX/AYE;->c:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 1684833
    iget-object v0, p0, LX/AYE;->h:LX/AYs;

    .line 1684834
    iget-object v1, v0, LX/AYs;->a:LX/0if;

    sget-object v2, LX/0ig;->C:LX/0ih;

    const-string v3, "commercial_break_violation"

    const/4 v4, 0x0

    invoke-static {v0}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1684835
    iget-object v0, p0, LX/AYE;->m:LX/AWl;

    iget-object v0, v0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->d()V

    .line 1684836
    return-void
.end method

.method public setWeakConnection(Z)V
    .locals 3

    .prologue
    .line 1684824
    iget-boolean v0, p0, LX/AYE;->B:Z

    if-ne v0, p1, :cond_0

    .line 1684825
    :goto_0
    return-void

    .line 1684826
    :cond_0
    iput-boolean p1, p0, LX/AYE;->B:Z

    .line 1684827
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1684828
    const-string v2, "network_state_during_recording"

    iget-boolean v0, p0, LX/AYE;->B:Z

    if-eqz v0, :cond_1

    const-string v0, "network_weak"

    :goto_1
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684829
    iget-object v0, p0, LX/AYE;->a:LX/AVT;

    invoke-virtual {v0, v1}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1684830
    iget-object v0, p0, LX/AYE;->n:LX/AXm;

    invoke-virtual {v0, p1}, LX/AXm;->setWeakConnection(Z)V

    goto :goto_0

    .line 1684831
    :cond_1
    const-string v0, "network_recovered"

    goto :goto_1
.end method
