.class public LX/AkI;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AkH;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/inlinecomposer/model/ComposerGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:LX/AkE;


# direct methods
.method public constructor <init>(LX/0Px;LX/AkE;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AkE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/inlinecomposer/model/ComposerGroup;",
            ">;",
            "Lcom/facebook/feed/inlinecomposer/multirow/work/WorkGroupsPogRecyclerViewAdapter$ClickHandler;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708944
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1708945
    iput-object p1, p0, LX/AkI;->a:LX/0Px;

    .line 1708946
    iput-object p2, p0, LX/AkI;->e:LX/AkE;

    .line 1708947
    const v0, 0x7f081b44

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AkI;->b:Ljava/lang/String;

    .line 1708948
    const v0, 0x7f0a00e5

    invoke-static {p3, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/AkI;->c:I

    .line 1708949
    const v0, 0x7f0a05eb

    invoke-static {p3, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/AkI;->d:I

    .line 1708950
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1708943
    new-instance v0, LX/AkH;

    new-instance v1, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/AkI;->e:LX/AkE;

    invoke-direct {v0, v1, v2}, LX/AkH;-><init>(Landroid/view/View;LX/AkE;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1708951
    check-cast p1, LX/AkH;

    const/4 v2, 0x0

    .line 1708952
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 1708953
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;

    .line 1708954
    if-nez v1, :cond_0

    .line 1708955
    iget-object v1, p0, LX/AkI;->a:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BYG;

    .line 1708956
    iget-object v2, v1, LX/BYG;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1708957
    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setName(Ljava/lang/String;)V

    .line 1708958
    iget-object v2, v1, LX/BYG;->c:Landroid/net/Uri;

    move-object v2, v2

    .line 1708959
    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setImage(Landroid/net/Uri;)V

    .line 1708960
    iget v2, p0, LX/AkI;->c:I

    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setTextColor(I)V

    .line 1708961
    iget-object v2, v0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->c:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1708962
    iput-object v1, p1, LX/AkH;->m:LX/BYG;

    .line 1708963
    :goto_0
    return-void

    .line 1708964
    :cond_0
    iget-object v1, p0, LX/AkI;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setName(Ljava/lang/String;)V

    .line 1708965
    invoke-virtual {v0, v2}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setImage(Landroid/net/Uri;)V

    .line 1708966
    iget v1, p0, LX/AkI;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setTextColor(I)V

    .line 1708967
    iget-object v1, v0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->c:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1708968
    iput-object v2, p1, LX/AkH;->m:LX/BYG;

    .line 1708969
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1708941
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1708942
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1708938
    iget-object v0, p0, LX/AkI;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708939
    const/4 v0, 0x0

    .line 1708940
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/AkI;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
