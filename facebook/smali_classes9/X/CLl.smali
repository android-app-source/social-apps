.class public LX/CLl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2gU;

.field public final c:LX/2bC;

.field private final d:LX/CLm;

.field public final e:LX/2Mk;

.field private final f:LX/1sj;

.field public final g:LX/01T;

.field private final h:LX/3Ed;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CLq;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/CLn;

.field private final k:LX/0Xl;

.field public final l:Ljava/util/concurrent/ExecutorService;

.field private final m:LX/03V;

.field public final n:LX/0Uh;

.field private o:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1879458
    const-class v0, LX/CLl;

    sput-object v0, LX/CLl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2gU;LX/2bC;LX/CLm;LX/2Mk;LX/1sj;LX/01T;LX/3Ed;LX/0Or;LX/CLn;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Uh;)V
    .locals 0
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2gU;",
            "LX/2bC;",
            "LX/CLm;",
            "LX/2Mk;",
            "LX/1sj;",
            "LX/01T;",
            "LX/3Ed;",
            "LX/0Or",
            "<",
            "LX/CLq;",
            ">;",
            "LX/CLn;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879444
    iput-object p1, p0, LX/CLl;->b:LX/2gU;

    .line 1879445
    iput-object p2, p0, LX/CLl;->c:LX/2bC;

    .line 1879446
    iput-object p3, p0, LX/CLl;->d:LX/CLm;

    .line 1879447
    iput-object p4, p0, LX/CLl;->e:LX/2Mk;

    .line 1879448
    iput-object p5, p0, LX/CLl;->f:LX/1sj;

    .line 1879449
    iput-object p6, p0, LX/CLl;->g:LX/01T;

    .line 1879450
    iput-object p7, p0, LX/CLl;->h:LX/3Ed;

    .line 1879451
    iput-object p8, p0, LX/CLl;->i:LX/0Or;

    .line 1879452
    iput-object p9, p0, LX/CLl;->j:LX/CLn;

    .line 1879453
    iput-object p10, p0, LX/CLl;->k:LX/0Xl;

    .line 1879454
    iput-object p11, p0, LX/CLl;->l:Ljava/util/concurrent/ExecutorService;

    .line 1879455
    iput-object p12, p0, LX/CLl;->m:LX/03V;

    .line 1879456
    iput-object p13, p0, LX/CLl;->n:LX/0Uh;

    .line 1879457
    return-void
.end method

.method public static a(LX/0QB;)LX/CLl;
    .locals 1

    .prologue
    .line 1879442
    invoke-static {p0}, LX/CLl;->b(LX/0QB;)LX/CLl;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1879439
    const-string v0, "m_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1879440
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 1879441
    :cond_0
    return-object p0
.end method

.method public static declared-synchronized a(LX/CLl;)V
    .locals 3

    .prologue
    .line 1879433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CLl;->o:LX/0Yb;

    if-nez v0, :cond_0

    .line 1879434
    iget-object v0, p0, LX/CLl;->k:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/CLj;

    invoke-direct {v2, p0}, LX/CLj;-><init>(LX/CLl;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/CLl;->o:LX/0Yb;

    .line 1879435
    :cond_0
    iget-object v0, p0, LX/CLl;->o:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1879436
    iget-object v0, p0, LX/CLl;->o:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1879437
    :cond_1
    monitor-exit p0

    return-void

    .line 1879438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/CLl;Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1879411
    iget-object v0, p0, LX/CLl;->e:LX/2Mk;

    invoke-virtual {v0}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    .line 1879412
    if-nez v7, :cond_1

    .line 1879413
    iget-object v0, p0, LX/CLl;->m:LX/03V;

    const-string v1, "NullLoggedInUserKey"

    const-string v2, "Got null logged-in user key in maybeSendThriftDeliveryReceiptBatchForDeltas"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1879414
    :cond_0
    :goto_0
    return-void

    .line 1879415
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1879416
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1879417
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1879418
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1879419
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/facebook/messaging/model/messages/Message;

    .line 1879420
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1879421
    iget-object v2, v6, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->GROUP:LX/5e9;

    if-ne v2, v3, :cond_4

    .line 1879422
    iget-object v0, v6, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/CLl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1879423
    :cond_2
    iget-object v0, p0, LX/CLl;->d:LX/CLm;

    iget-wide v2, v6, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/CLm;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1879424
    :cond_3
    new-instance v0, LX/CLr;

    iget-object v1, v6, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, v6, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v6, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v10, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v6, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v10, LX/5e9;->GROUP:LX/5e9;

    if-ne v6, v10, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CLr;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V

    .line 1879425
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 1879426
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1879427
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/CLl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1879428
    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    .line 1879429
    :cond_6
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1879430
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1879431
    new-instance v1, LX/CLs;

    iget-object v2, p0, LX/CLl;->h:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/CLs;-><init>(Ljava/util/List;Ljava/lang/Long;)V

    .line 1879432
    const-string v0, "DELTAS"

    invoke-static {p0, v1, v0}, LX/CLl;->a$redex0(LX/CLl;LX/CLs;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/CLl;LX/CLs;)V
    .locals 6

    .prologue
    .line 1879308
    iget-object v0, p1, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879309
    iget-object v2, p0, LX/CLl;->d:LX/CLm;

    invoke-static {v0}, LX/CLn;->a(LX/CLr;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    iget-object v0, v0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/CLm;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    goto :goto_0

    .line 1879310
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/CLl;LX/CLs;Ljava/lang/String;)V
    .locals 13
    .param p0    # LX/CLl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/CLs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1879370
    iget-object v0, p0, LX/CLl;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLq;

    invoke-virtual {v0, p1}, LX/CLq;->a(LX/CLs;)LX/CLh;

    move-result-object v10

    .line 1879371
    if-nez v10, :cond_0

    .line 1879372
    :goto_0
    return-void

    .line 1879373
    :cond_0
    iget-object v11, v10, LX/CLh;->a:LX/CLs;

    .line 1879374
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1879375
    iget-object v0, v11, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879376
    iget-object v3, v0, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 1879377
    iget-object v0, v0, LX/CLr;->messageIds:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_1

    .line 1879378
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v7, v0

    .line 1879379
    iget-object v0, p0, LX/CLl;->f:LX/1sj;

    invoke-virtual {v0}, LX/1sj;->a()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 1879380
    invoke-static {v0}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 1879381
    sget-object v2, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-eq v0, v2, :cond_5

    .line 1879382
    const-string v2, "message_ids"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879383
    const-string v2, "other_fbid"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1879384
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1879385
    iget-object v4, v11, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CLr;

    .line 1879386
    iget-object v4, v4, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1879387
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 1879388
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879389
    const-string v2, "thread_fbid"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1879390
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1879391
    iget-object v4, v11, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CLr;

    .line 1879392
    iget-object v4, v4, LX/CLr;->threadFbid:Ljava/lang/Long;

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1879393
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 1879394
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879395
    const-string v2, "user_id"

    iget-object v3, p0, LX/CLl;->e:LX/2Mk;

    invoke-virtual {v3}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879396
    :cond_5
    const-string v2, "op"

    const-string v3, "send_delivery_receipt_batch"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879397
    iget-object v2, p0, LX/CLl;->f:LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v2, v0, v3, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 1879398
    new-instance v1, LX/CLt;

    sget-object v2, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne v0, v2, :cond_6

    const-string v0, ""

    :goto_4
    invoke-direct {v1, v0}, LX/CLt;-><init>(Ljava/lang/String;)V

    .line 1879399
    new-instance v0, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v0, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 1879400
    :try_start_0
    invoke-virtual {v0, v1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 1879401
    invoke-virtual {v0, v11}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    .line 1879402
    array-length v2, v1

    array-length v3, v0

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 1879403
    const/4 v3, 0x0

    array-length v4, v1

    array-length v5, v0

    invoke-static {v0, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1879404
    move-object v12, v2
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 1879405
    iget-object v0, p0, LX/CLl;->c:LX/2bC;

    invoke-static {v11}, LX/CLl;->b(LX/CLs;)J

    move-result-wide v2

    invoke-static {v11}, LX/CLl;->f(LX/CLs;)I

    move-result v4

    iget-object v1, v11, LX/CLs;->batchId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v8, v10, LX/CLh;->b:LX/0Px;

    iget-object v1, v10, LX/CLh;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move-object v1, p2

    invoke-virtual/range {v0 .. v9}, LX/2bC;->a(Ljava/lang/String;JIJLjava/util/List;Ljava/util/List;I)V

    .line 1879406
    new-instance v3, LX/CLi;

    move-object v4, p0

    move-object v5, v11

    move-object v6, p2

    move-object v8, v10

    invoke-direct/range {v3 .. v8}, LX/CLi;-><init>(LX/CLl;LX/CLs;Ljava/lang/String;Ljava/util/List;LX/CLh;)V

    .line 1879407
    iget-object v0, p0, LX/CLl;->b:LX/2gU;

    const-string v1, "/t_dr_batch"

    sget-object v2, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    invoke-virtual {v0, v1, v12, v2, v3}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    goto/16 :goto_0

    .line 1879408
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 1879409
    :catch_0
    move-exception v0

    .line 1879410
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_5
    aput-object v0, v1, v2

    goto/16 :goto_0

    :cond_7
    const-string v0, "NULL"

    goto :goto_5
.end method

.method public static synthetic b(LX/CLl;LX/CLs;)J
    .locals 2

    .prologue
    .line 1879369
    invoke-static {p1}, LX/CLl;->b(LX/CLs;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static b(LX/CLs;)J
    .locals 8

    .prologue
    .line 1879363
    const-wide/16 v0, 0x0

    .line 1879364
    iget-object v2, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879365
    iget-object v1, v0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-lez v1, :cond_1

    .line 1879366
    iget-object v0, v0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    move-wide v2, v0

    .line 1879367
    goto :goto_0

    .line 1879368
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/CLl;
    .locals 14

    .prologue
    .line 1879361
    new-instance v0, LX/CLl;

    invoke-static {p0}, LX/2gU;->a(LX/0QB;)LX/2gU;

    move-result-object v1

    check-cast v1, LX/2gU;

    invoke-static {p0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v2

    check-cast v2, LX/2bC;

    invoke-static {p0}, LX/CLm;->a(LX/0QB;)LX/CLm;

    move-result-object v3

    check-cast v3, LX/CLm;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v4

    check-cast v4, LX/2Mk;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v5

    check-cast v5, LX/1sj;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-static {p0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v7

    check-cast v7, LX/3Ed;

    const/16 v8, 0x2755

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/CLn;->a(LX/0QB;)LX/CLn;

    move-result-object v9

    check-cast v9, LX/CLn;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v0 .. v13}, LX/CLl;-><init>(LX/2gU;LX/2bC;LX/CLm;LX/2Mk;LX/1sj;LX/01T;LX/3Ed;LX/0Or;LX/CLn;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Uh;)V

    .line 1879362
    return-object v0
.end method

.method public static synthetic c(LX/CLl;LX/CLs;)I
    .locals 1

    .prologue
    .line 1879360
    invoke-static {p1}, LX/CLl;->f(LX/CLs;)I

    move-result v0

    return v0
.end method

.method private static f(LX/CLs;)I
    .locals 3

    .prologue
    .line 1879355
    const/4 v0, 0x0

    .line 1879356
    iget-object v1, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879357
    iget-object v0, v0, LX/CLr;->messageIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1879358
    goto :goto_0

    .line 1879359
    :cond_0
    return v1
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1879327
    iget-object v0, p0, LX/CLl;->g:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 1879328
    :cond_0
    :goto_0
    return-void

    .line 1879329
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1879330
    iget-object v0, p0, LX/CLl;->e:LX/2Mk;

    invoke-virtual {v0}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 1879331
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 1879332
    :goto_1
    if-nez v0, :cond_3

    .line 1879333
    iget-object v0, p0, LX/CLl;->m:LX/03V;

    const-string v1, "NullLoggedInUserKey"

    const-string v2, "Got null logged-in user key in maybeSendDeliveredReceiptsForThread"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1879334
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1879335
    :cond_3
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1879336
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    if-ge v7, v9, :cond_9

    invoke-virtual {p1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1879337
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1879338
    :cond_4
    :goto_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    .line 1879339
    :cond_5
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    if-ne v0, v1, :cond_6

    .line 1879340
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_4

    .line 1879341
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    .line 1879342
    :goto_4
    cmp-long v0, v10, v2

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CLl;->d:LX/CLm;

    iget-object v1, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-virtual {v0, v1, v4, v5}, LX/CLm;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1879343
    new-instance v0, LX/CLr;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1879344
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1879345
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v6, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v12, LX/5e9;->GROUP:LX/5e9;

    if-ne v6, v12, :cond_8

    const/4 v6, 0x1

    :goto_5
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CLr;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V

    .line 1879346
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1879347
    :cond_6
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-nez v0, :cond_7

    .line 1879348
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    move-wide v2, v0

    goto :goto_4

    .line 1879349
    :cond_7
    iget-object v0, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_4

    .line 1879350
    :cond_8
    const/4 v6, 0x0

    goto :goto_5

    .line 1879351
    :cond_9
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1879352
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1879353
    new-instance v1, LX/CLs;

    iget-object v2, p0, LX/CLl;->h:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/CLs;-><init>(Ljava/util/List;Ljava/lang/Long;)V

    .line 1879354
    invoke-static {p0, v1, p2}, LX/CLl;->a$redex0(LX/CLl;LX/CLs;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1879311
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CLl;->g:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 1879312
    :cond_0
    :goto_0
    return-void

    .line 1879313
    :cond_1
    iget-object v0, p0, LX/CLl;->e:LX/2Mk;

    invoke-virtual {v0}, LX/2Mk;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    .line 1879314
    if-nez v5, :cond_2

    .line 1879315
    iget-object v0, p0, LX/CLl;->m:LX/03V;

    const-string v1, "NullLoggedInUserKey"

    const-string v2, "Got null logged-in user key in maybeSendDeliveredReceiptForMessage"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1879316
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1879317
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 1879318
    iget-object v1, p0, LX/CLl;->d:LX/CLm;

    invoke-virtual {v1, v0, v2, v3}, LX/CLm;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1879319
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1879320
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Delivery receipt has already been sent for "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Ignore the one from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1879321
    :cond_3
    const-string v0, "FETCH_THREAD"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1879322
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v4, v0

    .line 1879323
    :goto_1
    new-instance v0, LX/CLr;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v6, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v7, LX/5e9;->GROUP:LX/5e9;

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CLr;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V

    .line 1879324
    new-instance v1, LX/CLs;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iget-object v2, p0, LX/CLl;->h:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/CLs;-><init>(Ljava/util/List;Ljava/lang/Long;)V

    invoke-static {p0, v1, p2}, LX/CLl;->a$redex0(LX/CLl;LX/CLs;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1879325
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/CLl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    goto :goto_1

    .line 1879326
    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method
