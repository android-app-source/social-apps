.class public LX/C7y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7w;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1852118
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C7y;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852115
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1852116
    iput-object p1, p0, LX/C7y;->b:LX/0Ot;

    .line 1852117
    return-void
.end method

.method public static a(LX/0QB;)LX/C7y;
    .locals 4

    .prologue
    .line 1852104
    const-class v1, LX/C7y;

    monitor-enter v1

    .line 1852105
    :try_start_0
    sget-object v0, LX/C7y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852106
    sput-object v2, LX/C7y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852107
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852108
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852109
    new-instance v3, LX/C7y;

    const/16 p0, 0x1fa3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7y;-><init>(LX/0Ot;)V

    .line 1852110
    move-object v0, v3

    .line 1852111
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852112
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852113
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852102
    invoke-static {}, LX/1dS;->b()V

    .line 1852103
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2230022c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1852098
    iget-object v1, p0, LX/C7y;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1852099
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p5, LX/1no;->a:I

    .line 1852100
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 1852101
    const/16 v1, 0x1f

    const v2, 0x3f152025

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852095
    iget-object v0, p0, LX/C7y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1852096
    new-instance v0, Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-direct {v0, p1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1852097
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1852094
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1852091
    check-cast p3, LX/C7x;

    .line 1852092
    iget-object v0, p0, LX/C7y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    check-cast p2, Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v1, p3, LX/C7x;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-virtual {v0, p1, p2, v1}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->a(LX/1De;Lcom/facebook/fig/mediagrid/FigMediaGrid;Lcom/facebook/graphql/model/GraphQLGroup;)V

    .line 1852093
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1852090
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1852089
    const/16 v0, 0xf

    return v0
.end method
