.class public final enum LX/CN0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CN0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CN0;

.field public static final enum NEVER:LX/CN0;

.field public static final enum WIFI_ONLY:LX/CN0;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1881212
    new-instance v0, LX/CN0;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v2, v2}, LX/CN0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/CN0;->NEVER:LX/CN0;

    .line 1881213
    new-instance v0, LX/CN0;

    const-string v1, "WIFI_ONLY"

    invoke-direct {v0, v1, v3, v3}, LX/CN0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/CN0;->WIFI_ONLY:LX/CN0;

    .line 1881214
    const/4 v0, 0x2

    new-array v0, v0, [LX/CN0;

    sget-object v1, LX/CN0;->NEVER:LX/CN0;

    aput-object v1, v0, v2

    sget-object v1, LX/CN0;->WIFI_ONLY:LX/CN0;

    aput-object v1, v0, v3

    sput-object v0, LX/CN0;->$VALUES:[LX/CN0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1881215
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1881216
    iput p3, p0, LX/CN0;->value:I

    .line 1881217
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CN0;
    .locals 1

    .prologue
    .line 1881218
    const-class v0, LX/CN0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CN0;

    return-object v0
.end method

.method public static values()[LX/CN0;
    .locals 1

    .prologue
    .line 1881219
    sget-object v0, LX/CN0;->$VALUES:[LX/CN0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CN0;

    return-object v0
.end method
