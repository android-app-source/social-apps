.class public final LX/BuF;
.super LX/8qA;
.source ""


# instance fields
.field public final synthetic a:LX/BuJ;


# direct methods
.method public constructor <init>(LX/BuJ;)V
    .locals 0

    .prologue
    .line 1830637
    iput-object p1, p0, LX/BuF;->a:LX/BuJ;

    invoke-direct {p0}, LX/8qA;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1830611
    check-cast p1, LX/8q9;

    .line 1830612
    iget-object v0, p0, LX/BuF;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830613
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830614
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830615
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/8q9;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/8q9;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1830616
    iget-boolean v1, p1, LX/8q9;->b:Z

    .line 1830617
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1830618
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 1830619
    :goto_0
    move-object v0, v0

    .line 1830620
    iget-object v1, p0, LX/BuF;->a:LX/BuJ;

    iget-object v2, p0, LX/BuF;->a:LX/BuJ;

    iget-object v2, v2, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, v1, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830621
    iget-object v1, p0, LX/BuF;->a:LX/BuJ;

    iget-object v0, p0, LX/BuF;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830622
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1830623
    check-cast v0, LX/16n;

    invoke-virtual {v1, v0}, LX/BuJ;->a(LX/16n;)V

    .line 1830624
    :cond_0
    return-void

    .line 1830625
    :cond_1
    invoke-static {v3}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    .line 1830626
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result p1

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :goto_1
    add-int/2addr v2, p1

    .line 1830627
    if-gez v2, :cond_2

    .line 1830628
    const/4 v2, 0x0

    .line 1830629
    :cond_2
    invoke-static {v4}, LX/6X9;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/6X9;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/6X9;->a(I)LX/6X9;

    move-result-object v2

    .line 1830630
    iget-object v4, v2, LX/6X9;->a:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-object v2, v4

    .line 1830631
    invoke-static {v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/3dM;->j(Z)LX/3dM;

    move-result-object v2

    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1830632
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    .line 1830633
    iput-object v2, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1830634
    move-object v2, v3

    .line 1830635
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1830636
    :cond_3
    const/4 v2, -0x1

    goto :goto_1
.end method
