.class public final LX/BdE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TTModel;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BdF;


# direct methods
.method public constructor <init>(LX/BdF;)V
    .locals 0

    .prologue
    .line 1803490
    iput-object p1, p0, LX/BdE;->a:LX/BdF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1803497
    iget-object v0, p0, LX/BdE;->a:LX/BdF;

    iget-object v0, v0, LX/BdF;->c:LX/BdB;

    if-eqz v0, :cond_0

    .line 1803498
    iget-object v0, p0, LX/BdE;->a:LX/BdF;

    iget-object v0, v0, LX/BdF;->c:LX/BdB;

    .line 1803499
    iget-object v1, v0, LX/BdB;->a:LX/BcP;

    sget-object v2, LX/BdC;->DOWNLOAD_ERROR:LX/BdC;

    const/4 p0, 0x0

    invoke-static {v1, v2, p0, p1}, LX/BdA;->a(LX/BcP;LX/BdC;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 1803500
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1803491
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1803492
    iget-object v0, p0, LX/BdE;->a:LX/BdF;

    iget-object v0, v0, LX/BdF;->c:LX/BdB;

    if-eqz v0, :cond_0

    .line 1803493
    iget-object v0, p0, LX/BdE;->a:LX/BdF;

    iget-object v0, v0, LX/BdF;->c:LX/BdB;

    .line 1803494
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1803495
    iget-object v2, v0, LX/BdB;->a:LX/BcP;

    sget-object p0, LX/BdC;->DATA_AVAILABLE:LX/BdC;

    const/4 p1, 0x0

    invoke-static {v2, p0, v1, p1}, LX/BdA;->a(LX/BcP;LX/BdC;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 1803496
    :cond_0
    return-void
.end method
