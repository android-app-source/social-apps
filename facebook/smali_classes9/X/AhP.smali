.class public final LX/AhP;
.super Lcom/facebook/fbui/glyph/GlyphButton;
.source ""


# instance fields
.field public final synthetic b:LX/AhS;


# direct methods
.method public constructor <init>(LX/AhS;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1702784
    iput-object p1, p0, LX/AhP;->b:LX/AhS;

    .line 1702785
    const/4 v0, 0x0

    const v1, 0x7f010617

    invoke-direct {p0, p2, v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702786
    invoke-virtual {p0, v2}, LX/AhP;->setClickable(Z)V

    .line 1702787
    invoke-virtual {p0, v2}, LX/AhP;->setFocusable(Z)V

    .line 1702788
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AhP;->setVisibility(I)V

    .line 1702789
    invoke-virtual {p0, v2}, LX/AhP;->setEnabled(Z)V

    .line 1702790
    const v0, 0x7f08152f

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AhP;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1702791
    return-void
.end method


# virtual methods
.method public final performClick()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1702792
    invoke-super {p0}, Lcom/facebook/fbui/glyph/GlyphButton;->performClick()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1702793
    :goto_0
    return v1

    .line 1702794
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AhP;->playSoundEffect(I)V

    .line 1702795
    iget-object v0, p0, LX/AhP;->b:LX/AhS;

    invoke-virtual {v0}, LX/AhS;->c()Z

    goto :goto_0
.end method
