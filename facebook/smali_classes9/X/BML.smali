.class public LX/BML;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Qh;


# instance fields
.field public final b:LX/1Ns;

.field public final c:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1777066
    new-instance v0, LX/1Qg;

    invoke-direct {v0}, LX/1Qg;-><init>()V

    sput-object v0, LX/BML;->a:LX/1Qh;

    return-void
.end method

.method public constructor <init>(LX/1Ns;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777068
    iput-object p1, p0, LX/BML;->b:LX/1Ns;

    .line 1777069
    iput-object p2, p0, LX/BML;->c:LX/1Nq;

    .line 1777070
    return-void
.end method

.method private static a(LX/1RN;LX/B5o;)Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RN;",
            "TCONTEXT;)",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;"
        }
    .end annotation

    .prologue
    .line 1777071
    iget-object v0, p0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    check-cast p1, LX/B5p;

    .line 1777072
    iget-object v2, p1, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1777073
    const/4 v3, 0x0

    iget-object v4, p0, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p0, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 2

    .prologue
    .line 1777074
    invoke-static {p0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1777075
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1777076
    check-cast v0, LX/1kW;

    .line 1777077
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1777078
    return-object v0
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 0
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777079
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777080
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1777081
    invoke-static {p2, p3}, LX/BML;->a(LX/1RN;LX/B5o;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    .line 1777082
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p2}, LX/BML;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v2

    .line 1777083
    const-class v3, Landroid/app/Activity;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 1777084
    iget-object v3, p0, LX/BML;->b:LX/1Ns;

    sget-object v5, LX/BML;->a:LX/1Qh;

    new-instance v6, LX/AnN;

    invoke-direct {v6}, LX/AnN;-><init>()V

    invoke-virtual {v3, v5, v6}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v3

    .line 1777085
    const/16 v5, 0x20b6

    invoke-virtual {v2}, Lcom/facebook/productionprompts/model/ProductionPrompt;->y()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2}, LX/BMU;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v7

    iget-object v8, p0, LX/BML;->c:LX/1Nq;

    const/4 v9, 0x1

    invoke-static {v2, v0, v9}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/1aw;->a(Landroid/app/Activity;ILjava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 1777086
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1777087
    invoke-static {p1}, LX/BML;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->l()Z

    move-result v0

    return v0
.end method
