.class public final LX/Bx0;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bx2;


# direct methods
.method public constructor <init>(LX/Bx2;)V
    .locals 0

    .prologue
    .line 1834549
    iput-object p1, p0, LX/Bx0;->a:LX/Bx2;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1834548
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1834521
    check-cast p1, LX/2ou;

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1834522
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-boolean v0, v0, LX/Bx2;->M:Z

    if-nez v0, :cond_1

    .line 1834523
    :cond_0
    :goto_0
    return-void

    .line 1834524
    :cond_1
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->a:LX/0iX;

    iget-object v3, p0, LX/Bx0;->a:LX/Bx2;

    invoke-static {v3}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v4, v4, LX/Bx2;->D:LX/2pa;

    invoke-virtual {v0, v3, v4}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v3

    .line 1834525
    sget-object v0, LX/Bwz;->a:[I

    iget-object v4, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v4}, LX/2qV;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1834526
    :pswitch_0
    iget-object v4, p0, LX/Bx0;->a:LX/Bx2;

    if-nez v3, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v4, v0}, LX/Bx2;->a$redex0(LX/Bx2;Z)V

    .line 1834527
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->r:LX/Bx1;

    invoke-virtual {v0, v5}, LX/Bx1;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1834528
    if-eqz v3, :cond_2

    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_2

    .line 1834529
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    const v3, 0x3c23d70a    # 0.01f

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVolume(F)V

    .line 1834530
    :cond_2
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->r:LX/Bx1;

    invoke-virtual {v0, v5}, LX/Bx1;->sendEmptyMessage(I)Z

    .line 1834531
    :cond_3
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->r:LX/Bx1;

    invoke-virtual {v0, v1}, LX/Bx1;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1834532
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->r:LX/Bx1;

    invoke-virtual {v0, v1}, LX/Bx1;->sendEmptyMessage(I)Z

    .line 1834533
    :cond_4
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->a:LX/0iX;

    iget-object v1, p0, LX/Bx0;->a:LX/Bx2;

    invoke-static {v1}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v1

    iget-object v3, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v3, v3, LX/Bx2;->D:LX/2pa;

    invoke-virtual {v0, v1, v3}, LX/0iX;->b(LX/04D;LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834534
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    iget-object v0, v0, LX/Bx2;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1834535
    goto :goto_1

    .line 1834536
    :pswitch_1
    iget-object v0, p0, LX/Bx0;->a:LX/Bx2;

    const/4 v3, 0x0

    .line 1834537
    iget-object v1, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_6

    .line 1834538
    iget-object v1, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVolume(F)V

    .line 1834539
    :cond_6
    iput v3, v0, LX/Bx2;->N:I

    .line 1834540
    iget-object v1, v0, LX/Bx2;->r:LX/Bx1;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/Bx1;->removeMessages(I)V

    .line 1834541
    iget-object v1, v0, LX/Bx2;->t:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1834542
    invoke-static {v0}, LX/Bx2;->D(LX/Bx2;)V

    .line 1834543
    iput-boolean v3, v0, LX/Bx2;->K:Z

    .line 1834544
    iput-boolean v3, v0, LX/Bx2;->L:Z

    .line 1834545
    iput-boolean v3, v0, LX/Bx2;->Q:Z

    .line 1834546
    iget-object v1, v0, LX/Bx2;->r:LX/Bx1;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/Bx1;->removeMessages(I)V

    .line 1834547
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
