.class public final LX/Ars;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;)V
    .locals 0

    .prologue
    .line 1719854
    iput-object p1, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1719855
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v1, v0

    .line 1719856
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v1

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setAlpha(F)V

    .line 1719857
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1719858
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    const v2, 0x3f99999a    # 1.2f

    invoke-static {v0, v2, v5, v1}, LX/Art;->a(LX/Art;FFF)F

    move-result v2

    .line 1719859
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setScaleX(F)V

    .line 1719860
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setScaleY(F)V

    .line 1719861
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    iget-object v2, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v3, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget v3, v3, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->d:I

    int-to-float v3, v3

    iget-object v4, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget v4, v4, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->e:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4, v1}, LX/Art;->a(LX/Art;FFF)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setRingThickness(F)V

    .line 1719862
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {v0, v5, v2, v1}, LX/Art;->a(LX/Art;FFF)F

    move-result v1

    .line 1719863
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1719864
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1719865
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 1719843
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    .line 1719844
    iget-wide v4, v0, LX/0wd;->i:D

    move-wide v0, v4

    .line 1719845
    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1719846
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1719847
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1719848
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    const/4 v1, 0x0

    .line 1719849
    iput-boolean v1, v0, LX/Art;->h:Z

    .line 1719850
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    invoke-virtual {v0, p0}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1719851
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->a()V

    .line 1719852
    :goto_0
    return-void

    .line 1719853
    :cond_0
    iget-object v0, p0, LX/Ars;->a:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable$1$1;-><init>(LX/Ars;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
