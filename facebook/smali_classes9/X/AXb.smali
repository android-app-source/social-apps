.class public final LX/AXb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Kl;


# instance fields
.field public final synthetic a:LX/AXg;


# direct methods
.method public constructor <init>(LX/AXg;)V
    .locals 0

    .prologue
    .line 1684373
    iput-object p1, p0, LX/AXb;->a:LX/AXg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1684374
    iget-object v0, p0, LX/AXb;->a:LX/AXg;

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 1684375
    iget-object v1, v0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    invoke-virtual {v1}, Lcom/facebook/video/player/CountdownRingContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, v0, LX/AXg;->l:I

    int-to-long v3, v2

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1684376
    iget-object v1, v0, LX/AXg;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1684377
    iget-object v1, v0, LX/AXg;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, v0, LX/AXg;->l:I

    int-to-long v3, v2

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1684378
    iget-object v1, v0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1684379
    iget-object v1, v0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setScaleX(F)V

    .line 1684380
    iget-object v1, v0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setScaleY(F)V

    .line 1684381
    iget-object v1, v0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1684382
    iget-object v1, v0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, v0, LX/AXg;->l:I

    int-to-long v3, v2

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1684383
    iget-object v1, v0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1684384
    iget-object v1, v0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setScaleX(F)V

    .line 1684385
    iget-object v1, v0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setScaleY(F)V

    .line 1684386
    iget-object v1, v0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1684387
    iget-object v1, v0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, v0, LX/AXg;->l:I

    int-to-long v3, v2

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1684388
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(F)V

    .line 1684389
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleX(F)V

    .line 1684390
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setScaleY(F)V

    .line 1684391
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setRotation(F)V

    .line 1684392
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1684393
    iget-object v1, v0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1}, Lcom/facebook/fbui/glyph/GlyphView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v3, 0x78

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, v0, LX/AXg;->l:I

    int-to-long v3, v2

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, LX/AXe;

    invoke-direct {v2, v0}, LX/AXe;-><init>(LX/AXg;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1684394
    return-void
.end method
