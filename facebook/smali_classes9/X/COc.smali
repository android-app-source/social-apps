.class public final LX/COc;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COc;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COa;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COd;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883339
    const/4 v0, 0x0

    sput-object v0, LX/COc;->a:LX/COc;

    .line 1883340
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COc;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883336
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883337
    new-instance v0, LX/COd;

    invoke-direct {v0}, LX/COd;-><init>()V

    iput-object v0, p0, LX/COc;->c:LX/COd;

    .line 1883338
    return-void
.end method

.method public static declared-synchronized q()LX/COc;
    .locals 2

    .prologue
    .line 1883341
    const-class v1, LX/COc;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COc;->a:LX/COc;

    if-nez v0, :cond_0

    .line 1883342
    new-instance v0, LX/COc;

    invoke-direct {v0}, LX/COc;-><init>()V

    sput-object v0, LX/COc;->a:LX/COc;

    .line 1883343
    :cond_0
    sget-object v0, LX/COc;->a:LX/COc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883344
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1883322
    check-cast p2, LX/COb;

    .line 1883323
    iget-object v0, p2, LX/COb;->a:LX/CNb;

    iget-object v1, p2, LX/COb;->b:LX/CNc;

    const/4 p2, 0x0

    .line 1883324
    const-string v2, "feedback"

    invoke-virtual {v0, v2}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1883325
    new-instance p0, LX/23u;

    invoke-direct {p0}, LX/23u;-><init>()V

    .line 1883326
    iput-object v2, p0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1883327
    move-object v2, p0

    .line 1883328
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1883329
    invoke-static {v2, p2, p2}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1883330
    const/4 v2, 0x0

    .line 1883331
    :goto_0
    move-object v0, v2

    .line 1883332
    return-object v0

    .line 1883333
    :cond_0
    check-cast v1, LX/CNq;

    .line 1883334
    iget-object p0, v1, LX/CNq;->d:LX/CNs;

    iget-object p0, p0, LX/CNs;->f:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1WX;

    move-object p0, p0

    .line 1883335
    invoke-virtual {p0, p1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883320
    invoke-static {}, LX/1dS;->b()V

    .line 1883321
    const/4 v0, 0x0

    return-object v0
.end method
