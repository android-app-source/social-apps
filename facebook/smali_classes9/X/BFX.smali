.class public final LX/BFX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Jvk;

.field public final synthetic b:LX/Jvl;

.field public final synthetic c:LX/BFZ;


# direct methods
.method public constructor <init>(LX/BFZ;LX/Jvk;LX/Jvl;)V
    .locals 0

    .prologue
    .line 1765963
    iput-object p1, p0, LX/BFX;->c:LX/BFZ;

    iput-object p2, p0, LX/BFX;->a:LX/Jvk;

    iput-object p3, p0, LX/BFX;->b:LX/Jvl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x702e76fa

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765942
    iget-object v1, p0, LX/BFX;->c:LX/BFZ;

    iget-object v1, v1, LX/BFZ;->c:LX/89b;

    sget-object v2, LX/89b;->IMAGE:LX/89b;

    if-ne v1, v2, :cond_0

    .line 1765943
    iget-object v1, p0, LX/BFX;->a:LX/Jvk;

    .line 1765944
    iget-object v2, v1, LX/Jvk;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1765945
    iget-boolean p0, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->t:Z

    if-eqz p0, :cond_3

    .line 1765946
    :goto_0
    const v1, 0x67cef718

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1765947
    :cond_0
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v2

    move v1, v2

    .line 1765948
    if-eqz v1, :cond_1

    .line 1765949
    iget-object v1, p0, LX/BFX;->a:LX/Jvk;

    .line 1765950
    iget-object v2, v1, LX/Jvk;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v2}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->k()V

    .line 1765951
    :goto_1
    iget-object v1, p0, LX/BFX;->c:LX/BFZ;

    .line 1765952
    iget-object v2, v1, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    const/16 p0, 0xff

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1765953
    iget-object v2, v1, LX/BFZ;->a:Landroid/widget/ImageButton;

    new-instance p0, LX/BFY;

    invoke-direct {p0, v1}, LX/BFY;-><init>(LX/BFZ;)V

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1765954
    goto :goto_0

    .line 1765955
    :cond_1
    iget-object v1, p0, LX/BFX;->a:LX/Jvk;

    .line 1765956
    iget-object v2, v1, LX/Jvk;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1765957
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1765958
    iget-object v3, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->m:LX/0TD;

    new-instance p1, LX/BFu;

    invoke-direct {p1, v2}, LX/BFu;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-interface {v3, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v3, v3

    .line 1765959
    iget-object p1, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a:LX/0TF;

    iget-object v1, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->n:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p1, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1765960
    :cond_2
    goto :goto_1

    .line 1765961
    :cond_3
    const/4 p0, 0x1

    iput-boolean p0, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->t:Z

    .line 1765962
    iget-object p0, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/BG1;

    invoke-direct {v1, v2}, LX/BG1;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-virtual {p0, v1}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f4;)V

    goto :goto_0
.end method
