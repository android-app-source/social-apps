.class public LX/AV1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field public final c:LX/AVT;

.field private final d:LX/03V;

.field public volatile e:I

.field public f:Landroid/os/Handler;

.field public g:Landroid/graphics/SurfaceTexture;

.field public final h:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final i:Ljava/lang/String;

.field private final j:LX/1b4;

.field private final k:LX/2EC;

.field public final l:Landroid/content/Context;

.field public final m:LX/BAF;

.field public n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1b6;",
            ">;"
        }
    .end annotation
.end field

.field public volatile o:I

.field public volatile p:I

.field public q:I

.field public volatile r:Landroid/hardware/Camera;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:J

.field public u:J

.field public v:J

.field public final w:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/facecast/FacecastCamera$FrameCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1678774
    const-class v0, LX/AV1;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AV1;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AVT;LX/03V;Lcom/facebook/quicklog/QuickPerformanceLogger;Landroid/content/Context;LX/1b4;LX/2EC;ILjava/lang/String;)V
    .locals 3
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 1678755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678756
    iput-boolean v2, p0, LX/AV1;->s:Z

    .line 1678757
    iput-wide v0, p0, LX/AV1;->t:J

    .line 1678758
    iput-wide v0, p0, LX/AV1;->u:J

    .line 1678759
    iput-wide v0, p0, LX/AV1;->v:J

    .line 1678760
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/AV1;->w:LX/0UE;

    .line 1678761
    invoke-direct {p0}, LX/AV1;->l()V

    .line 1678762
    iput-object p1, p0, LX/AV1;->c:LX/AVT;

    .line 1678763
    iput-object p2, p0, LX/AV1;->d:LX/03V;

    .line 1678764
    iput-object p3, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1678765
    iput-object p4, p0, LX/AV1;->l:Landroid/content/Context;

    .line 1678766
    iput-object p5, p0, LX/AV1;->j:LX/1b4;

    .line 1678767
    iput-object p6, p0, LX/AV1;->k:LX/2EC;

    .line 1678768
    iput p7, p0, LX/AV1;->e:I

    .line 1678769
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/AV1;->b:Landroid/os/Handler;

    .line 1678770
    iput v2, p0, LX/AV1;->q:I

    .line 1678771
    iput-object p8, p0, LX/AV1;->i:Ljava/lang/String;

    .line 1678772
    new-instance v0, LX/BAF;

    invoke-direct {v0}, LX/BAF;-><init>()V

    iput-object v0, p0, LX/AV1;->m:LX/BAF;

    .line 1678773
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 1678740
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/AV1;->j:LX/1b4;

    .line 1678741
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x33a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1678742
    if-eqz v0, :cond_0

    .line 1678743
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1678744
    const-string v1, "camera2_open_logging"

    const-string v2, "yes"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678745
    iget-object v1, p0, LX/AV1;->c:LX/AVT;

    invoke-virtual {v1, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1678746
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1678747
    :try_start_0
    iget-object v0, p0, LX/AV1;->l:Landroid/content/Context;

    const-string v2, "camera"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    .line 1678748
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1678749
    invoke-static {p0, p1, v0, v2, v1}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;Ljava/lang/StringBuilder;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1678750
    :goto_0
    return-void

    .line 1678751
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 1678752
    :catch_0
    move-exception v0

    .line 1678753
    const-string v2, "{getCameraIdList-[exception:%1$s]},"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678754
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/AV1;II)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const v6, 0xac0004

    .line 1678686
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1678687
    sget-object v0, LX/AV1;->a:Ljava/lang/String;

    const-string v2, "camera already initialized"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1678688
    :goto_0
    return v0

    .line 1678689
    :cond_0
    iget-object v0, p0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1678690
    :cond_1
    sget-object v0, LX/AV1;->a:Ljava/lang/String;

    const-string v1, "Delegate and delegate reference can\'t be null"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1678691
    goto :goto_0

    .line 1678692
    :cond_2
    iput p2, p0, LX/AV1;->q:I

    .line 1678693
    iput p1, p0, LX/AV1;->e:I

    .line 1678694
    const/4 v0, 0x0

    .line 1678695
    const v3, 0xac0004

    :try_start_0
    invoke-static {p0, v3}, LX/AV1;->d(LX/AV1;I)V

    .line 1678696
    iget-object v3, p0, LX/AV1;->j:LX/1b4;

    .line 1678697
    iget-object v4, v3, LX/1b4;->b:LX/0Uh;

    const/16 v5, 0x33b

    const/4 p2, 0x0

    invoke-virtual {v4, v5, p2}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v3, v4

    .line 1678698
    if-eqz v3, :cond_4

    .line 1678699
    iget-object v3, p0, LX/AV1;->k:LX/2EC;

    .line 1678700
    invoke-static {v3}, LX/2EC;->b(LX/2EC;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1678701
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 1678702
    :goto_1
    move-object v3, v4

    .line 1678703
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1678704
    iget-object v4, p0, LX/AV1;->c:LX/AVT;

    .line 1678705
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1678706
    const-string v5, "facecast_event_name"

    const-string v8, "camera_leak_detector"

    invoke-interface {v7, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678707
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1678708
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Throwable;

    .line 1678709
    invoke-static {v5}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    .line 1678710
    const-string v10, "{%1$s}|"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v5, v11, p2

    invoke-static {v10, v11}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1678711
    :cond_3
    const-string v5, "camera_leak_detector_trace"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678712
    invoke-virtual {v4, v7}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1678713
    :cond_4
    invoke-static {p0}, LX/AV1;->q(LX/AV1;)I

    move-result v3

    invoke-static {p1, v3}, LX/7RT;->a(II)Landroid/hardware/Camera;

    move-result-object v3

    iput-object v3, p0, LX/AV1;->r:Landroid/hardware/Camera;

    .line 1678714
    iget-object v3, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0xac0004

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1678715
    :goto_3
    iget-object v3, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-nez v3, :cond_5

    .line 1678716
    iget-object v1, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v1, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1678717
    invoke-direct {p0, v0}, LX/AV1;->a(Ljava/lang/Exception;)V

    move v0, v2

    .line 1678718
    goto/16 :goto_0

    .line 1678719
    :cond_5
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    new-instance v2, LX/AUw;

    invoke-direct {v2, p0}, LX/AUw;-><init>(LX/AV1;)V

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1678720
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 1678721
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iput v2, p0, LX/AV1;->o:I

    .line 1678722
    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, LX/AV1;->p:I

    .line 1678723
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-nez v0, :cond_9

    .line 1678724
    :cond_6
    move v0, v1

    .line 1678725
    goto/16 :goto_0

    .line 1678726
    :catch_0
    move-exception v0

    goto :goto_3

    :cond_7
    :try_start_1
    iget-object v4, v3, LX/2EC;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/ADY;

    .line 1678727
    iget-object v5, v4, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v7

    .line 1678728
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1678729
    const/4 v5, 0x0

    :goto_4
    if-ge v5, v7, :cond_8

    .line 1678730
    iget-object v3, v4, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p2, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1678731
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1678732
    :cond_8
    move-object v4, p2

    .line 1678733
    goto/16 :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1678734
    :cond_9
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    new-instance v2, LX/AUy;

    invoke-direct {v2, p0}, LX/AUy;-><init>(LX/AV1;)V

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1678735
    iget v0, p0, LX/AV1;->o:I

    iget v2, p0, LX/AV1;->p:I

    .line 1678736
    mul-int v3, v0, v2

    mul-int/lit8 v3, v3, 0xc

    div-int/lit8 v3, v3, 0x8

    move v2, v3

    .line 1678737
    const/4 v0, 0x0

    :goto_5
    const/4 v3, 0x3

    if-ge v0, v3, :cond_6

    .line 1678738
    iget-object v3, p0, LX/AV1;->r:Landroid/hardware/Camera;

    new-array v4, v2, [B

    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 1678739
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public static a$redex0(LX/AV1;Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 1678674
    if-nez p1, :cond_0

    .line 1678675
    :goto_0
    return-void

    .line 1678676
    :cond_0
    iput-object p1, p0, LX/AV1;->g:Landroid/graphics/SurfaceTexture;

    .line 1678677
    new-instance v0, LX/AV0;

    iget v1, p0, LX/AV1;->e:I

    invoke-direct {v0, p0, v1}, LX/AV0;-><init>(LX/AV1;I)V

    invoke-virtual {p1, v0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1678678
    :try_start_0
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    .line 1678679
    sget-object v0, LX/AV1;->a:Ljava/lang/String;

    const-string v1, "Camera object is null. Cannot set preview."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1678680
    :catch_0
    move-exception v0

    .line 1678681
    sget-object v1, LX/AV1;->a:Ljava/lang/String;

    const-string v2, "Cannot set the preview surface texture for camera."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1678682
    invoke-direct {p0, v0}, LX/AV1;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1678683
    :cond_1
    const v0, 0xac000f

    :try_start_1
    invoke-static {p0, v0}, LX/AV1;->d(LX/AV1;I)V

    .line 1678684
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 1678685
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    const v1, 0x152a0f36

    invoke-static {v0, v1}, LX/0J2;->b(Landroid/hardware/Camera;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static a$redex0(LX/AV1;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;Ljava/lang/StringBuilder;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "Landroid/hardware/camera2/CameraManager;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1678664
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1678665
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1678666
    :goto_0
    return-void

    .line 1678667
    :cond_0
    new-instance v0, LX/AUx;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/AUx;-><init>(LX/AV1;Ljava/lang/StringBuilder;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;)V

    .line 1678668
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1678669
    :try_start_0
    const-string v2, "{%1$s-[try]},"

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678670
    const/4 v2, 0x0

    invoke-virtual {p2, v1, v0, v2}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1678671
    :catch_0
    move-exception v0

    .line 1678672
    const-string v2, "{%1$s-[exception:%2$s]},"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678673
    invoke-static {p0, p1, p2, p3, p4}, LX/AV1;->a$redex0(LX/AV1;Ljava/lang/Exception;Landroid/hardware/camera2/CameraManager;Ljava/util/Iterator;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/AV1;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1678655
    if-eqz p2, :cond_0

    .line 1678656
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1678657
    const-string v1, "camera2_open_status"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678658
    iget-object v1, p0, LX/AV1;->c:LX/AVT;

    invoke-virtual {v1, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1678659
    :cond_0
    iget-object v0, p0, LX/AV1;->d:LX/03V;

    sget-object v1, LX/AV1;->a:Ljava/lang/String;

    const-string v2, "Failed to open camera"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678660
    invoke-static {p0}, LX/AV1;->r(LX/AV1;)LX/1b6;

    move-result-object v0

    .line 1678661
    if-eqz v0, :cond_1

    .line 1678662
    iget-object v1, p0, LX/AV1;->b:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecast/FacecastCamera$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/facecast/FacecastCamera$2;-><init>(LX/AV1;LX/1b6;Ljava/lang/Exception;)V

    const v0, -0x771f19c1

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1678663
    :cond_1
    return-void
.end method

.method public static d(LX/AV1;I)V
    .locals 3

    .prologue
    .line 1678651
    iget-object v0, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1678652
    iget-object v0, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "product_name"

    iget-object v2, p0, LX/AV1;->i:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 1678653
    iget-object v0, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "camera_lib_type"

    sget-object v2, LX/6KU;->FACECAST:LX/6KU;

    invoke-virtual {v2}, LX/6KU;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 1678654
    return-void
.end method

.method public static k(LX/AV1;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const v6, 0xac000c

    .line 1678592
    iget-wide v0, p0, LX/AV1;->v:J

    iget-wide v2, p0, LX/AV1;->u:J

    iget-wide v4, p0, LX/AV1;->t:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/AV1;->v:J

    .line 1678593
    iput-wide v8, p0, LX/AV1;->t:J

    .line 1678594
    iput-wide v8, p0, LX/AV1;->u:J

    .line 1678595
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1678596
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1678597
    :cond_0
    invoke-static {p0, v6}, LX/AV1;->d(LX/AV1;I)V

    .line 1678598
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    .line 1678599
    if-eqz v0, :cond_1

    .line 1678600
    const v1, -0x3663c0f1

    invoke-static {v0, v1}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 1678601
    const v1, -0x380b05b7

    invoke-static {v0, v1}, LX/0J2;->a(Landroid/hardware/Camera;I)V

    .line 1678602
    :cond_1
    iget-object v0, p0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1678603
    invoke-direct {p0}, LX/AV1;->l()V

    .line 1678604
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1678647
    iput v0, p0, LX/AV1;->o:I

    .line 1678648
    iput v0, p0, LX/AV1;->p:I

    .line 1678649
    const/4 v0, 0x0

    iput-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    .line 1678650
    return-void
.end method

.method public static q(LX/AV1;)I
    .locals 1

    .prologue
    .line 1678646
    iget v0, p0, LX/AV1;->q:I

    mul-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public static r(LX/AV1;)LX/1b6;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1678643
    iget-object v0, p0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1678644
    iget-object v0, p0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b6;

    .line 1678645
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 1678633
    iget v0, p0, LX/AV1;->e:I

    .line 1678634
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    .line 1678635
    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1678636
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1678637
    invoke-static {v1, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1678638
    iget p0, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne p0, v0, :cond_0

    .line 1678639
    :goto_1
    move v0, v1

    .line 1678640
    return v0

    .line 1678641
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1678642
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1678629
    if-nez p2, :cond_0

    .line 1678630
    iget-object v0, p0, LX/AV1;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AV1;->f:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1678631
    :goto_0
    return-void

    .line 1678632
    :cond_0
    iget-object v0, p0, LX/AV1;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/AV1;->f:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(LX/1b6;)V
    .locals 1

    .prologue
    .line 1678627
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    .line 1678628
    return-void
.end method

.method public final a(LX/BA5;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1678608
    invoke-virtual {p0}, LX/AV1;->a()I

    move-result v1

    iget v2, p0, LX/AV1;->q:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1678609
    if-ltz v1, :cond_0

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v5

    if-le v1, v5, :cond_2

    .line 1678610
    :cond_0
    :goto_0
    move v1, v4

    .line 1678611
    iget-object v2, p0, LX/AV1;->m:LX/BAF;

    .line 1678612
    iput-object v2, p1, LX/BA5;->k:LX/BAF;

    .line 1678613
    iget v2, p0, LX/AV1;->o:I

    move v2, v2

    .line 1678614
    iget v3, p0, LX/AV1;->p:I

    move v3, v3

    .line 1678615
    iget v4, p0, LX/AV1;->e:I

    move v4, v4

    .line 1678616
    if-ne v4, v0, :cond_1

    .line 1678617
    :goto_1
    const/4 v4, 0x1

    iput-boolean v4, p1, LX/BA5;->l:Z

    .line 1678618
    iput v2, p1, LX/BA5;->g:I

    .line 1678619
    iput v3, p1, LX/BA5;->h:I

    .line 1678620
    iput v1, p1, LX/BA5;->i:I

    .line 1678621
    iput-boolean v0, p1, LX/BA5;->j:Z

    .line 1678622
    return-void

    .line 1678623
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1678624
    :cond_2
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1678625
    invoke-static {v1, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1678626
    iget v6, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v6, v3, :cond_3

    :goto_2
    iget v4, v5, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-static {v3, v2, v4}, LX/7ev;->a(ZII)I

    move-result v4

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1678606
    iput-boolean p1, p0, LX/AV1;->s:Z

    .line 1678607
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1678605
    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
