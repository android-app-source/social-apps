.class public LX/AXm;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1b9;
.implements LX/AXZ;
.implements LX/1bG;
.implements LX/3Ha;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public a:LX/3Hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

.field private final g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

.field public final h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

.field private final i:Landroid/view/View;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684537
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AXm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684538
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1684539
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AXm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684540
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1684518
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684519
    const-class v0, LX/AXm;

    invoke-static {v0, p0}, LX/AXm;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1684520
    const v0, 0x7f0305e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1684521
    const v0, 0x7f0d0f8b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iput-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1684522
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsLiveNow(Z)V

    .line 1684523
    iget-object v0, p0, LX/AXm;->a:LX/3Hc;

    .line 1684524
    iput-object p0, v0, LX/3Hc;->l:LX/3Ha;

    .line 1684525
    const v0, 0x7f0d1035

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    iput-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 1684526
    const v0, 0x7f0d1036

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    iput-object v0, p0, LX/AXm;->h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    .line 1684527
    const v0, 0x7f0d1034

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AXm;->i:Landroid/view/View;

    .line 1684528
    iget-object v0, p0, LX/AXm;->i:Landroid/view/View;

    new-instance v1, LX/AXl;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/AXl;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1684529
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AXm;

    invoke-static {p0}, LX/3Hc;->b(LX/0QB;)LX/3Hc;

    move-result-object v1

    check-cast v1, LX/3Hc;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v2

    check-cast v2, LX/3HT;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object v1, p1, LX/AXm;->a:LX/3Hc;

    iput-object v2, p1, LX/AXm;->b:LX/3HT;

    iput-object p0, p1, LX/AXm;->c:LX/1b4;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 1684541
    iget-object v0, p0, LX/AXm;->h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    invoke-virtual {v0, p1, p2, p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(JLX/AXZ;)V

    .line 1684542
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a()V

    .line 1684543
    return-void
.end method

.method public final a(LX/AVE;LX/AVE;)V
    .locals 2

    .prologue
    .line 1684544
    iget-object v1, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v0, LX/AVE;->RECORDING:LX/AVE;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    .line 1684545
    return-void

    .line 1684546
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1684547
    invoke-virtual {p2}, LX/AYp;->ordinal()I

    .line 1684548
    sget-object v0, LX/AXj;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1684549
    :goto_0
    return-void

    .line 1684550
    :pswitch_0
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->BROADCAST_COMMERCIAL_BREAK:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    .line 1684551
    goto :goto_0

    .line 1684552
    :pswitch_1
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->LIVE:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    .line 1684553
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1684554
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1684555
    invoke-virtual {p2, p0}, LX/AVF;->b(LX/1b9;)V

    .line 1684556
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1684557
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 1684558
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1684559
    iget-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 1684560
    iget-object p1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, p1

    .line 1684561
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1684562
    iget-object v0, p0, LX/AXm;->h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    .line 1684563
    iget-object p0, v0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, p0

    .line 1684564
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1684565
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1684566
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 0

    .prologue
    .line 1684567
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1684568
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1684530
    iget-object v3, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    if-nez p1, :cond_2

    iget-boolean v0, p0, LX/AXm;->j:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 1684531
    iget-object v0, p0, LX/AXm;->i:Landroid/view/View;

    if-nez p1, :cond_0

    iget-boolean v3, p0, LX/AXm;->j:Z

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1684532
    if-eqz p1, :cond_3

    .line 1684533
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Z)V

    .line 1684534
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 1684535
    goto :goto_0

    .line 1684536
    :cond_3
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->b()V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1684517
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 0

    .prologue
    .line 1684516
    return-void
.end method

.method public final c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1684515
    return-void
.end method

.method public final d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1684514
    return-void
.end method

.method public getCurrentViewerCount()I
    .locals 1

    .prologue
    .line 1684511
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1684512
    iget p0, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->l:I

    move v0, p0

    .line 1684513
    return v0
.end method

.method public getLoggingInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1684505
    iget-object v0, p0, LX/AXm;->a:LX/3Hc;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hc;

    .line 1684506
    iget-object v1, v0, LX/3Hc;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1684507
    iget-object v1, v0, LX/3Hc;->j:Ljava/util/HashMap;

    const-string v2, "current_viewers"

    iget p0, v0, LX/3Hc;->n:I

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684508
    iget-object v1, v0, LX/3Hc;->j:Ljava/util/HashMap;

    const-string v2, "max_viewers"

    iget p0, v0, LX/3Hc;->o:I

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684509
    iget-object v1, v0, LX/3Hc;->j:Ljava/util/HashMap;

    move-object v0, v1

    .line 1684510
    return-object v0
.end method

.method public final iE_()V
    .locals 2

    .prologue
    .line 1684503
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->FINISHED:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1684504
    return-void
.end method

.method public final iF_()V
    .locals 4

    .prologue
    .line 1684488
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1684489
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    invoke-virtual {v0, p0}, LX/AVF;->a(LX/1b9;)V

    .line 1684490
    iget-object v0, p0, LX/AXm;->c:LX/1b4;

    .line 1684491
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x359

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1684492
    if-nez v0, :cond_0

    .line 1684493
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1684494
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1684495
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1684496
    iget-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 1684497
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1684498
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1684499
    :cond_0
    iget-object v0, p0, LX/AXm;->h:Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    .line 1684500
    iget-object v1, v0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1684501
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1684502
    return-void
.end method

.method public final s_(I)V
    .locals 2

    .prologue
    .line 1684485
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 1684486
    iget-object v0, p0, LX/AXm;->b:LX/3HT;

    new-instance v1, LX/Adf;

    invoke-direct {v1, p1}, LX/Adf;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1684487
    return-void
.end method

.method public setIsAudioLive(Z)V
    .locals 1

    .prologue
    .line 1684483
    iget-object v0, p0, LX/AXm;->f:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsAudioLive(Z)V

    .line 1684484
    return-void
.end method

.method public setWeakConnection(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1684475
    iput-boolean p1, p0, LX/AXm;->j:Z

    .line 1684476
    if-eqz p1, :cond_0

    .line 1684477
    iget-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    sget-object v1, LX/Acd;->WEAK:LX/Acd;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setLiveText(LX/Acd;)V

    .line 1684478
    iget-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 1684479
    iget-object v0, p0, LX/AXm;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1684480
    :goto_0
    return-void

    .line 1684481
    :cond_0
    iget-object v0, p0, LX/AXm;->g:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->setVisibility(I)V

    .line 1684482
    iget-object v0, p0, LX/AXm;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
