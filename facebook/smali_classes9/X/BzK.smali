.class public final LX/BzK;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BzL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public e:LX/1eK;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public final synthetic i:LX/BzL;


# direct methods
.method public constructor <init>(LX/BzL;)V
    .locals 1

    .prologue
    .line 1838549
    iput-object p1, p0, LX/BzK;->i:LX/BzL;

    .line 1838550
    move-object v0, p1

    .line 1838551
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1838552
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1838553
    const-string v0, "LifeEventTitleTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1838554
    if-ne p0, p1, :cond_1

    .line 1838555
    :cond_0
    :goto_0
    return v0

    .line 1838556
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1838557
    goto :goto_0

    .line 1838558
    :cond_3
    check-cast p1, LX/BzK;

    .line 1838559
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1838560
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1838561
    if-eq v2, v3, :cond_0

    .line 1838562
    iget-object v2, p0, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1838563
    goto :goto_0

    .line 1838564
    :cond_5
    iget-object v2, p1, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1838565
    :cond_6
    iget-object v2, p0, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1838566
    goto :goto_0

    .line 1838567
    :cond_8
    iget-object v2, p1, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1838568
    :cond_9
    iget-object v2, p0, LX/BzK;->c:LX/1Pq;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/BzK;->c:LX/1Pq;

    iget-object v3, p1, LX/BzK;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1838569
    goto :goto_0

    .line 1838570
    :cond_b
    iget-object v2, p1, LX/BzK;->c:LX/1Pq;

    if-nez v2, :cond_a

    .line 1838571
    :cond_c
    iget-object v2, p0, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v3, p1, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1838572
    goto :goto_0

    .line 1838573
    :cond_e
    iget-object v2, p1, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-nez v2, :cond_d

    .line 1838574
    :cond_f
    iget-object v2, p0, LX/BzK;->e:LX/1eK;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/BzK;->e:LX/1eK;

    iget-object v3, p1, LX/BzK;->e:LX/1eK;

    invoke-virtual {v2, v3}, LX/1eK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1838575
    goto :goto_0

    .line 1838576
    :cond_11
    iget-object v2, p1, LX/BzK;->e:LX/1eK;

    if-nez v2, :cond_10

    .line 1838577
    :cond_12
    iget-object v2, p0, LX/BzK;->f:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/BzK;->f:Ljava/lang/String;

    iget-object v3, p1, LX/BzK;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1838578
    goto/16 :goto_0

    .line 1838579
    :cond_14
    iget-object v2, p1, LX/BzK;->f:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 1838580
    :cond_15
    iget v2, p0, LX/BzK;->g:I

    iget v3, p1, LX/BzK;->g:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1838581
    goto/16 :goto_0

    .line 1838582
    :cond_16
    iget v2, p0, LX/BzK;->h:I

    iget v3, p1, LX/BzK;->h:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1838583
    goto/16 :goto_0
.end method
