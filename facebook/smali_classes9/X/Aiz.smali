.class public final LX/Aiz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

.field public final synthetic d:Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;ILandroid/view/View;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;)V
    .locals 0

    .prologue
    .line 1707347
    iput-object p1, p0, LX/Aiz;->d:Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;

    iput p2, p0, LX/Aiz;->a:I

    iput-object p3, p0, LX/Aiz;->b:Landroid/view/View;

    iput-object p4, p0, LX/Aiz;->c:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 1707348
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_0

    .line 1707349
    check-cast p1, LX/2Oo;

    .line 1707350
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const v1, 0x1754ad

    if-ne v0, v1, :cond_0

    .line 1707351
    iget-object v0, p0, LX/Aiz;->d:Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;

    iget-object v1, p0, LX/Aiz;->d:Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;

    iget v2, p0, LX/Aiz;->a:I

    invoke-virtual {v1, v2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Aiz;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1707352
    iget-object v3, v0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->f:LX/1CX;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v4

    const v5, 0x7f0823a3

    invoke-virtual {v4, v5}, LX/4mn;->a(I)LX/4mn;

    move-result-object v4

    const v5, 0x7f0823a4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v1, v6, p1

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1707353
    iput-object v5, v4, LX/4mn;->c:Ljava/lang/String;

    .line 1707354
    move-object v4, v4

    .line 1707355
    invoke-virtual {v4}, LX/4mn;->l()LX/4mm;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1707356
    :cond_0
    iget-object v0, p0, LX/Aiz;->c:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1707357
    iget-object v2, p0, LX/Aiz;->d:Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;

    iget-object v3, p0, LX/Aiz;->c:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    iget-object v1, p0, LX/Aiz;->c:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->a()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707358
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1707359
    return-void
.end method
