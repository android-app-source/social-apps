.class public LX/Afw;
.super LX/AeK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeK",
        "<",
        "LX/Afr;",
        ">;"
    }
.end annotation


# static fields
.field public static final l:Ljava/lang/String;

.field private static final m:[Ljava/lang/String;

.field private static final n:[F


# instance fields
.field public final o:Lcom/facebook/widget/FbImageView;

.field public final p:Lcom/facebook/widget/FbImageView;

.field private final q:Lcom/facebook/resources/ui/FbTextView;

.field private final r:Lcom/facebook/user/tiles/UserTileView;

.field private final s:LX/03V;

.field public t:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1699983
    const-class v0, LX/Afw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Afw;->l:Ljava/lang/String;

    .line 1699984
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "tip1.json"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "tip2.json"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "tip3a.json"

    aput-object v2, v0, v1

    const-string v1, "tip3b.json"

    aput-object v1, v0, v3

    sput-object v0, LX/Afw;->m:[Ljava/lang/String;

    .line 1699985
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    sput-object v0, LX/Afw;->n:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x3feccccd    # 1.85f
        0x402ccccd    # 2.7f
        0x4069999a    # 3.65f
    .end array-data
.end method

.method public constructor <init>(Landroid/view/View;LX/03V;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1699986
    invoke-direct {p0, p1}, LX/AeK;-><init>(Landroid/view/View;)V

    .line 1699987
    iput-object p2, p0, LX/Afw;->s:LX/03V;

    .line 1699988
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/Afw;->t:Landroid/content/res/Resources;

    .line 1699989
    const v0, 0x7f0d19a2

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    .line 1699990
    const v0, 0x7f0d19a3

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    .line 1699991
    const v0, 0x7f0d19a5

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Afw;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1699992
    const v0, 0x7f0d19a4

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/Afw;->r:Lcom/facebook/user/tiles/UserTileView;

    .line 1699993
    return-void
.end method

.method private b(IZ)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1699957
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1699958
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1699959
    const v1, 0x7f021a2c

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1699960
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1699961
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    move v3, v2

    move v4, v2

    move v2, v0

    .line 1699962
    :goto_1
    iget-object v0, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1699963
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1699964
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1699965
    iget-object v1, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1699966
    iget-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1699967
    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1699968
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1699969
    iget-object v1, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1699970
    return-void

    .line 1699971
    :cond_0
    int-to-float v0, v1

    sget-object v3, LX/Afw;->n:[F

    aget v3, v3, v2

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_0

    .line 1699972
    :cond_1
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1699973
    const v1, 0x7f021a2d

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1699974
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1699975
    if-eqz p2, :cond_2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_2
    move v3, v2

    move v4, v2

    move v2, v0

    goto :goto_1

    :cond_2
    int-to-float v0, v1

    sget-object v3, LX/Afw;->n:[F

    aget v3, v3, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_2

    .line 1699976
    :cond_3
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1699977
    const v1, 0x7f021a2e

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1699978
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 1699979
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 1699980
    const v1, 0x7f021a2f

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1699981
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1699982
    if-eqz p2, :cond_4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_3
    move v4, v3

    move v3, v2

    move v2, v0

    goto/16 :goto_1

    :cond_4
    int-to-float v0, v1

    sget-object v4, LX/Afw;->n:[F

    aget v4, v4, v5

    mul-float/2addr v0, v4

    float-to-int v0, v0

    goto :goto_3

    :cond_5
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/AeO;LX/AeU;)V
    .locals 12

    .prologue
    .line 1699913
    check-cast p1, LX/Afr;

    const/4 v11, 0x3

    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 1699914
    invoke-super {p0, p1, p2}, LX/AeK;->a(LX/AeO;LX/AeU;)V

    .line 1699915
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1699916
    iget-object v0, p1, LX/Afr;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1699917
    new-array v0, v10, [Ljava/lang/CharSequence;

    iget-object v2, p1, LX/Afr;->b:Ljava/lang/String;

    iget-object v5, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v2, v5}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v3

    const-string v2, " "

    aput-object v2, v0, v9

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699918
    :goto_0
    :try_start_0
    iget-object v2, p1, LX/Afr;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    float-to-int v2, v2

    .line 1699919
    :goto_1
    iget-object v5, p1, LX/Afr;->d:Ljava/lang/String;

    invoke-static {v5}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1699920
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1699921
    iget-object v6, p0, LX/Afw;->q:Lcom/facebook/resources/ui/FbTextView;

    new-array v7, v11, [Ljava/lang/CharSequence;

    aput-object v0, v7, v3

    const-string v0, " "

    aput-object v0, v7, v9

    const v0, 0x7f080c23

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v5, v8, v3

    invoke-virtual {v4, v0, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-static {v7}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699922
    iget-object v0, p1, LX/Afr;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1699923
    iget-object v0, p1, LX/Afr;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v4

    .line 1699924
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 1699925
    iget-object v5, p1, LX/Afr;->a:Ljava/lang/String;

    iget-object v6, p2, LX/AeU;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1699926
    sget-object v0, LX/8ue;->BROADCASTER:LX/8ue;

    .line 1699927
    :cond_0
    iget-object v5, p0, LX/Afw;->r:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v4, v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1699928
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v0, v4, :cond_2

    .line 1699929
    iget-boolean v0, p1, LX/Afr;->g:Z

    move v0, v0

    .line 1699930
    if-eqz v0, :cond_6

    .line 1699931
    :cond_2
    invoke-direct {p0, v2, v9}, LX/Afw;->b(IZ)V

    .line 1699932
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1699933
    iget-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1699934
    iget-object v0, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a2c

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1699935
    :cond_3
    :goto_2
    return-void

    .line 1699936
    :catch_0
    move-exception v2

    .line 1699937
    iget-object v5, p0, LX/Afw;->s:LX/03V;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LX/Afw;->l:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_parseFailure"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Failed to parse tip amount"

    invoke-virtual {v5, v6, v7, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v2, v3

    goto/16 :goto_1

    .line 1699938
    :cond_4
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1699939
    iget-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1699940
    iget-object v0, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a2d

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    goto :goto_2

    .line 1699941
    :cond_5
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v10

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1699942
    iget-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1699943
    iget-object v0, p0, LX/Afw;->p:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a2e

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1699944
    iget-object v0, p0, LX/Afw;->o:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021a2f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    goto :goto_2

    .line 1699945
    :cond_6
    invoke-direct {p0, v2, v3}, LX/Afw;->b(IZ)V

    .line 1699946
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1699947
    sget-object v0, LX/Afw;->m:[Ljava/lang/String;

    aget-object v0, v0, v3

    .line 1699948
    :goto_3
    new-array v2, v10, [Ljava/lang/String;

    aput-object v0, v2, v3

    aput-object v1, v2, v9

    .line 1699949
    new-instance v0, LX/Afv;

    invoke-direct {v0, p0}, LX/Afv;-><init>(LX/Afw;)V

    invoke-virtual {v0, v2}, LX/Afv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1699950
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/Afr;->g:Z

    .line 1699951
    goto/16 :goto_2

    .line 1699952
    :cond_7
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1699953
    sget-object v0, LX/Afw;->m:[Ljava/lang/String;

    aget-object v0, v0, v9

    goto :goto_3

    .line 1699954
    :cond_8
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v0, v0, v10

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1699955
    sget-object v0, LX/Afw;->m:[Ljava/lang/String;

    aget-object v0, v0, v11

    .line 1699956
    sget-object v1, LX/Afw;->m:[Ljava/lang/String;

    aget-object v1, v1, v10

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto/16 :goto_0
.end method
