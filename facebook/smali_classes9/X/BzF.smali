.class public final LX/BzF;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BzG;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/BzG;


# direct methods
.method public constructor <init>(LX/BzG;)V
    .locals 1

    .prologue
    .line 1838419
    iput-object p1, p0, LX/BzF;->d:LX/BzG;

    .line 1838420
    move-object v0, p1

    .line 1838421
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1838422
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1838423
    const-string v0, "LifeEventDescriptionTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1838424
    if-ne p0, p1, :cond_1

    .line 1838425
    :cond_0
    :goto_0
    return v0

    .line 1838426
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1838427
    goto :goto_0

    .line 1838428
    :cond_3
    check-cast p1, LX/BzF;

    .line 1838429
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1838430
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1838431
    if-eq v2, v3, :cond_0

    .line 1838432
    iget-object v2, p0, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1838433
    goto :goto_0

    .line 1838434
    :cond_5
    iget-object v2, p1, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1838435
    :cond_6
    iget-object v2, p0, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1838436
    goto :goto_0

    .line 1838437
    :cond_8
    iget-object v2, p1, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1838438
    :cond_9
    iget-object v2, p0, LX/BzF;->c:LX/1Pq;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/BzF;->c:LX/1Pq;

    iget-object v3, p1, LX/BzF;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1838439
    goto :goto_0

    .line 1838440
    :cond_a
    iget-object v2, p1, LX/BzF;->c:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
