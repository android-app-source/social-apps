.class public LX/CBz;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:Lcom/facebook/user/tiles/UserTileView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1856928
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1856929
    invoke-virtual {p0}, LX/CBz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1856930
    const v1, 0x7f0310c5

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1856931
    const/16 v1, 0x33

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1856932
    const v1, 0x7f0b1cda

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1856933
    const v0, 0x7f0d27ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/CBz;->j:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1856934
    const v0, 0x7f0d27e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/CBz;->k:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1856935
    const v0, 0x7f0d27d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/CBz;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1856936
    const v0, 0x7f0d27e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/CBz;->m:Lcom/facebook/user/tiles/UserTileView;

    .line 1856937
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1856938
    iget-object v0, p0, LX/CBz;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1856939
    iget-object v0, p0, LX/CBz;->m:Lcom/facebook/user/tiles/UserTileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 1856940
    const/16 v0, 0x33

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1856941
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1856942
    return-void
.end method
