.class public final LX/BMz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BMx;


# instance fields
.field public final synthetic a:LX/BN0;

.field public final b:Lcom/facebook/ipc/model/FacebookProfile;


# direct methods
.method public constructor <init>(LX/BN0;Lcom/facebook/ipc/model/FacebookProfile;)V
    .locals 0

    .prologue
    .line 1778179
    iput-object p1, p0, LX/BMz;->a:LX/BN0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778180
    iput-object p2, p0, LX/BMz;->b:Lcom/facebook/ipc/model/FacebookProfile;

    .line 1778181
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    .line 1778182
    new-instance v0, Lcom/facebook/profilelist/ProfileView;

    iget-object v1, p0, LX/BMz;->a:LX/BN0;

    iget-object v1, v1, LX/BN0;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/profilelist/ProfileView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1778183
    check-cast p1, Lcom/facebook/profilelist/ProfileView;

    .line 1778184
    iget-object v0, p0, LX/BMz;->b:Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1778185
    iget-object v1, p1, Lcom/facebook/profilelist/ProfileView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/profilelist/ProfileView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1778186
    iget-object v0, p0, LX/BMz;->b:Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1778187
    iget-object v1, p1, Lcom/facebook/profilelist/ProfileView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1778188
    iget-object v0, p0, LX/BMz;->a:LX/BN0;

    iget-object v0, v0, LX/BN0;->e:Ljava/util/Set;

    iget-object v1, p0, LX/BMz;->b:Lcom/facebook/ipc/model/FacebookProfile;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/profilelist/ProfileView;->setIsSelected(Z)V

    .line 1778189
    return-void
.end method
