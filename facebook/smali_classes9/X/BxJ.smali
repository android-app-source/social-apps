.class public LX/BxJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1vg;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835260
    iput-object p1, p0, LX/BxJ;->a:LX/1vg;

    .line 1835261
    iput-object p2, p0, LX/BxJ;->b:LX/0Ot;

    .line 1835262
    return-void
.end method

.method public static a(LX/0QB;)LX/BxJ;
    .locals 5

    .prologue
    .line 1835263
    const-class v1, LX/BxJ;

    monitor-enter v1

    .line 1835264
    :try_start_0
    sget-object v0, LX/BxJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835265
    sput-object v2, LX/BxJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835266
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835267
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835268
    new-instance v4, LX/BxJ;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 p0, 0x3be

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/BxJ;-><init>(LX/1vg;LX/0Ot;)V

    .line 1835269
    move-object v0, v4

    .line 1835270
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835271
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835272
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
