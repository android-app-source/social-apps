.class public final LX/CWn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/CSY;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/CT7;

.field public final synthetic e:LX/CTn;

.field public final synthetic f:LX/CT1;

.field public final synthetic g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;ILX/CSY;Ljava/lang/String;LX/CT7;LX/CTn;LX/CT1;)V
    .locals 0

    .prologue
    .line 1908932
    iput-object p1, p0, LX/CWn;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;

    iput p2, p0, LX/CWn;->a:I

    iput-object p3, p0, LX/CWn;->b:LX/CSY;

    iput-object p4, p0, LX/CWn;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CWn;->d:LX/CT7;

    iput-object p6, p0, LX/CWn;->e:LX/CTn;

    iput-object p7, p0, LX/CWn;->f:LX/CT1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1908926
    iget-object v0, p0, LX/CWn;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->b:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, LX/CWn;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;

    iget-object v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    iget v2, p0, LX/CWn;->a:I

    invoke-virtual {v1, v2}, LX/CXF;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1908927
    iget-object v0, p0, LX/CWn;->g:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;

    iget-object v1, p0, LX/CWn;->b:LX/CSY;

    iget-object v2, p0, LX/CWn;->c:Ljava/lang/String;

    iget v3, p0, LX/CWn;->a:I

    iget-object v4, p0, LX/CWn;->d:LX/CT7;

    iget-object v5, p0, LX/CWn;->e:LX/CTn;

    .line 1908928
    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;LX/CSY;Ljava/lang/String;ILX/CT7;LX/CTn;)V

    .line 1908929
    iget-object v0, p0, LX/CWn;->e:LX/CTn;

    iget-object v0, v0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1908930
    iget-object v1, p0, LX/CWn;->f:LX/CT1;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    iget-object v0, p0, LX/CWn;->e:LX/CTn;

    iget-object v0, v0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_SELECT:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTv;

    iget-object v3, p0, LX/CWn;->e:LX/CTn;

    iget-object v3, v3, LX/CTJ;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/CT1;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;LX/CTv;Ljava/lang/String;)V

    .line 1908931
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
