.class public LX/BXo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/widget/scrollview/LockableScrollView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1793838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1793839
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BXo;->a:Ljava/util/List;

    .line 1793840
    return-void
.end method

.method public static a(LX/0QB;)LX/BXo;
    .locals 3

    .prologue
    .line 1793841
    const-class v1, LX/BXo;

    monitor-enter v1

    .line 1793842
    :try_start_0
    sget-object v0, LX/BXo;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1793843
    sput-object v2, LX/BXo;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1793844
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793845
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1793846
    new-instance v0, LX/BXo;

    invoke-direct {v0}, LX/BXo;-><init>()V

    .line 1793847
    move-object v0, v0

    .line 1793848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1793849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BXo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1793850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1793851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1793852
    iput-object v1, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    .line 1793853
    iget-object v0, p0, LX/BXo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1793854
    iput-object v1, p0, LX/BXo;->c:Landroid/view/View;

    .line 1793855
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v12, 0x3ea8f5c3    # 0.33f

    .line 1793856
    iget-object v0, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    if-nez v0, :cond_1

    .line 1793857
    :cond_0
    :goto_0
    return-void

    .line 1793858
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1793859
    if-eq v0, v1, :cond_2

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    .line 1793860
    :cond_2
    iget-object v0, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/scrollview/LockableScrollView;->setScrollLock(Z)V

    .line 1793861
    iput-object v13, p0, LX/BXo;->c:Landroid/view/View;

    goto :goto_0

    .line 1793862
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v3, v0

    .line 1793863
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v4, v0

    .line 1793864
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1793865
    iget-object v0, p0, LX/BXo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1793866
    invoke-virtual {v0, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1793867
    iget-object v7, p0, LX/BXo;->c:Landroid/view/View;

    if-ne v0, v7, :cond_5

    .line 1793868
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v12

    float-to-int v7, v7

    div-int/lit8 v7, v7, 0x2

    .line 1793869
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v12

    float-to-int v8, v8

    div-int/lit8 v8, v8, 0x2

    .line 1793870
    iget v9, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v7

    iget v10, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v8

    iget v11, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v11

    iget v11, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v8, v11

    invoke-virtual {v5, v9, v10, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 1793871
    :cond_5
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1793872
    iput-object v0, p0, LX/BXo;->c:Landroid/view/View;

    move v0, v1

    .line 1793873
    :goto_1
    iget-object v1, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    .line 1793874
    iget-boolean v2, v1, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    move v1, v2

    .line 1793875
    if-eq v0, v1, :cond_0

    .line 1793876
    iget-object v1, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/scrollview/LockableScrollView;->setScrollLock(Z)V

    .line 1793877
    if-nez v0, :cond_0

    .line 1793878
    iput-object v13, p0, LX/BXo;->c:Landroid/view/View;

    .line 1793879
    iget-object v0, p0, LX/BXo;->b:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/scrollview/LockableScrollView;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method
