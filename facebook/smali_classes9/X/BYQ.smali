.class public final LX/BYQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1795907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1795908
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795909
    :goto_0
    return v1

    .line 1795910
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795911
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1795912
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1795913
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1795914
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1795915
    const-string v6, "group_cover_photo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1795916
    const/4 v5, 0x0

    .line 1795917
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_a

    .line 1795918
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795919
    :goto_2
    move v4, v5

    .line 1795920
    goto :goto_1

    .line 1795921
    :cond_2
    const-string v6, "group_purposes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1795922
    invoke-static {p0, p1}, LX/BYP;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1795923
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1795924
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1795925
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1795926
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1795927
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1795928
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1795929
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1795930
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1795931
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1795932
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 1795933
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795934
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1795935
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1795936
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1795937
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 1795938
    const-string v7, "photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1795939
    const/4 v6, 0x0

    .line 1795940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_e

    .line 1795941
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795942
    :goto_4
    move v4, v6

    .line 1795943
    goto :goto_3

    .line 1795944
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1795945
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1795946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_a
    move v4, v5

    goto :goto_3

    .line 1795947
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795948
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_d

    .line 1795949
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1795950
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1795951
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_c

    if-eqz v7, :cond_c

    .line 1795952
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1795953
    const/4 v7, 0x0

    .line 1795954
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_12

    .line 1795955
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795956
    :goto_6
    move v4, v7

    .line 1795957
    goto :goto_5

    .line 1795958
    :cond_d
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1795959
    invoke-virtual {p1, v6, v4}, LX/186;->b(II)V

    .line 1795960
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_4

    :cond_e
    move v4, v6

    goto :goto_5

    .line 1795961
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1795962
    :cond_10
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_11

    .line 1795963
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1795964
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1795965
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_10

    if-eqz v8, :cond_10

    .line 1795966
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1795967
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_7

    .line 1795968
    :cond_11
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1795969
    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 1795970
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_6

    :cond_12
    move v4, v7

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1795971
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1795972
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1795973
    if-eqz v0, :cond_3

    .line 1795974
    const-string v1, "group_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795975
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1795976
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1795977
    if-eqz v1, :cond_2

    .line 1795978
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795979
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1795980
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1795981
    if-eqz v2, :cond_1

    .line 1795982
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795983
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1795984
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1795985
    if-eqz v0, :cond_0

    .line 1795986
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795987
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1795988
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1795989
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1795990
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1795991
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1795992
    if-eqz v0, :cond_4

    .line 1795993
    const-string v1, "group_purposes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795994
    invoke-static {p0, v0, p2, p3}, LX/BYP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1795995
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1795996
    if-eqz v0, :cond_5

    .line 1795997
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1795998
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1795999
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1796000
    if-eqz v0, :cond_6

    .line 1796001
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796002
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1796003
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1796004
    return-void
.end method
