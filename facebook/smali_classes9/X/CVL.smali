.class public final LX/CVL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1902975
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1902976
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902977
    :goto_0
    return v1

    .line 1902978
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902979
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1902980
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1902981
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1902983
    const-string v4, "description"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1902984
    const/4 v3, 0x0

    .line 1902985
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1902986
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902987
    :goto_2
    move v2, v3

    .line 1902988
    goto :goto_1

    .line 1902989
    :cond_2
    const-string v4, "heading"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1902990
    const/4 v3, 0x0

    .line 1902991
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_c

    .line 1902992
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902993
    :goto_3
    move v0, v3

    .line 1902994
    goto :goto_1

    .line 1902995
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1902996
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1902997
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1902998
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1902999
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903000
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1903001
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1903002
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1903003
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1903004
    const-string v5, "plain_text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1903005
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 1903006
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1903007
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1903008
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_4

    .line 1903009
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903010
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_b

    .line 1903011
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1903012
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1903013
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_a

    if-eqz v4, :cond_a

    .line 1903014
    const-string v5, "plain_text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1903015
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1903016
    :cond_b
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1903017
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1903018
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_c
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1903019
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903020
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903021
    if-eqz v0, :cond_1

    .line 1903022
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903023
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903024
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1903025
    if-eqz v1, :cond_0

    .line 1903026
    const-string p3, "plain_text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903027
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903028
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903029
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903030
    if-eqz v0, :cond_3

    .line 1903031
    const-string v1, "heading"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903032
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903033
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1903034
    if-eqz v1, :cond_2

    .line 1903035
    const-string p1, "plain_text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903036
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903037
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903038
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903039
    return-void
.end method
