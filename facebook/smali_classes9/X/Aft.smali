.class public final LX/Aft;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Afu;


# direct methods
.method public constructor <init>(LX/Afu;)V
    .locals 0

    .prologue
    .line 1699848
    iput-object p1, p0, LX/Aft;->a:LX/Afu;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699846
    iget-object v0, p0, LX/Aft;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->d:LX/03V;

    sget-object v1, LX/Afu;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch pinned comment in the beginning."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699847
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699818
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1699819
    iget-object v0, p0, LX/Aft;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->g:LX/AeV;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1699820
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699821
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1699822
    :goto_1
    return-void

    .line 1699823
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699824
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1699825
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1699826
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1699827
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, 0x21cb8f9d

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1699828
    :goto_2
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 1699829
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1699830
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v11, 0x0

    .line 1699831
    if-nez v4, :cond_8

    :cond_5
    :goto_4
    if-eqz v6, :cond_9

    move-object v6, v7

    .line 1699832
    :goto_5
    move-object v4, v6

    .line 1699833
    if-eqz v4, :cond_4

    .line 1699834
    invoke-interface {v3, v2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 1699835
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 1699836
    :cond_7
    iget-object v0, p0, LX/Aft;->a:LX/Afu;

    iget-object v0, v0, LX/Afu;->g:LX/AeV;

    sget-object v2, LX/AeN;->LIVE_TIP_JAR_EVENT:LX/AeN;

    invoke-virtual {v0, v2, v3, v1}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    goto :goto_1

    .line 1699837
    :cond_8
    invoke-virtual {v5, v4, v11}, LX/15i;->g(II)I

    move-result v8

    if-eqz v8, :cond_5

    move v6, v11

    goto :goto_4

    .line 1699838
    :cond_9
    invoke-virtual {v5, v4, v11}, LX/15i;->g(II)I

    move-result v6

    const/4 v8, 0x3

    const-class v9, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    invoke-virtual {v5, v6, v8, v9}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    move-object v8, v6

    check-cast v8, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;

    .line 1699839
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;->c()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_b

    :cond_a
    move-object v6, v7

    .line 1699840
    goto :goto_5

    .line 1699841
    :cond_b
    invoke-virtual {v5, v4, v11}, LX/15i;->g(II)I

    move-result v6

    .line 1699842
    const-class v9, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    invoke-virtual {v5, v6, v11, v9}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    move-object v10, v6

    check-cast v10, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;

    .line 1699843
    if-eqz v10, :cond_c

    invoke-virtual {v10}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    invoke-virtual {v10}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_d

    :cond_c
    move-object v6, v7

    .line 1699844
    goto :goto_5

    .line 1699845
    :cond_d
    new-instance v6, LX/Afr;

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$TipGiverModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoLatestTipsQueryModel$TipJarTransactionsModel$EdgesModel$NodeModel$AmountReceivedModel;->b()Ljava/lang/String;

    move-result-object v10

    move v12, v11

    invoke-direct/range {v6 .. v12}, LX/Afr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_5
.end method
