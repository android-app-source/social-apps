.class public LX/AeW;
.super Ljava/util/LinkedList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedList",
        "<",
        "LX/Aeu;",
        ">;"
    }
.end annotation


# instance fields
.field private final mQueueSize:I


# direct methods
.method public constructor <init>(LX/1b4;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1697794
    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 1697795
    iget-object v0, p1, LX/1b4;->a:LX/0ad;

    sget v1, LX/1v6;->aa:I

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v0, v0

    .line 1697796
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/AeW;->mQueueSize:I

    .line 1697797
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 1697798
    check-cast p1, LX/Aeu;

    const/4 v3, 0x0

    .line 1697799
    if-nez p1, :cond_1

    .line 1697800
    :cond_0
    :goto_0
    return v3

    .line 1697801
    :cond_1
    invoke-virtual {p0}, LX/AeW;->size()I

    move-result v0

    iget v1, p0, LX/AeW;->mQueueSize:I

    if-ge v0, v1, :cond_2

    .line 1697802
    invoke-super {p0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 1697803
    :cond_2
    const/4 v1, 0x0

    move v2, v3

    .line 1697804
    :goto_1
    invoke-virtual {p0}, LX/AeW;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1697805
    invoke-virtual {p0, v2}, LX/AeW;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aeu;

    .line 1697806
    iget v4, v0, LX/Aeu;->n:I

    iget v5, p1, LX/Aeu;->n:I

    if-gt v4, v5, :cond_5

    .line 1697807
    if-eqz v1, :cond_3

    iget v4, v0, LX/Aeu;->n:I

    iget v5, v1, LX/Aeu;->n:I

    if-ge v4, v5, :cond_5

    .line 1697808
    :cond_3
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 1697809
    :cond_4
    if-eqz v1, :cond_0

    .line 1697810
    invoke-virtual {p0, v1}, LX/AeW;->remove(Ljava/lang/Object;)Z

    .line 1697811
    invoke-super {p0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method
