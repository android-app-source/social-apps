.class public final LX/BF4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1765375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;
    .locals 2

    .prologue
    .line 1765374
    new-instance v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1765371
    invoke-static {p1}, LX/BF4;->a(Landroid/os/Parcel;)Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1765372
    new-array v0, p1, [Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    move-object v0, v0

    .line 1765373
    return-object v0
.end method
