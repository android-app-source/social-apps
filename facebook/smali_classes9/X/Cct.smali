.class public LX/Cct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# static fields
.field public static a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1921776
    const-string v0, "extra_selected_city"

    sput-object v0, LX/Cct;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921778
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1921779
    const-string v0, "extra_logger_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/CdF;

    .line 1921780
    const-string v0, "extra_current_location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    new-instance v3, LX/Ccs;

    invoke-direct {v3}, LX/Ccs;-><init>()V

    if-eqz v2, :cond_0

    move-object v5, v2

    :goto_0
    const-string v2, "extra_logger_params"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    move v2, v1

    move v4, v1

    invoke-static/range {v0 .. v6}, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a(Landroid/location/Location;ZZLX/Ccr;ZLX/CdF;Landroid/os/Parcelable;)Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    move-result-object v0

    .line 1921781
    return-object v0

    .line 1921782
    :cond_0
    sget-object v5, LX/CdF;->NO_LOGGER:LX/CdF;

    goto :goto_0
.end method
