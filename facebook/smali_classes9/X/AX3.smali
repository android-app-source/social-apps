.class public LX/AX3;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/animation/ArgbEvaluator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1683650
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    sput-object v0, LX/AX3;->a:Landroid/animation/ArgbEvaluator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1683651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1683652
    return-void
.end method

.method private static a(Landroid/view/View;F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1683653
    const-string v0, "alpha"

    invoke-static {p0, v0, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;F)LX/4mQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;II)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 1683654
    sget-object v0, LX/AX3;->a:Landroid/animation/ArgbEvaluator;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/4mQ;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1683655
    new-instance v1, LX/AWz;

    invoke-direct {v1, p0}, LX/AWz;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1683656
    return-object v0
.end method

.method public static a(LX/AX1;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1683657
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/AX3;->a(Landroid/view/View;F)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AX1;->a(Landroid/animation/Animator;)LX/AX1;

    move-result-object v0

    new-instance v1, Lcom/facebook/facecast/plugin/FacecastEndScreenAnimationHelper$2;

    invoke-direct {v1, p1}, Lcom/facebook/facecast/plugin/FacecastEndScreenAnimationHelper$2;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/AX1;->c(Ljava/lang/Runnable;)LX/AX1;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v1}, LX/AX3;->a(Landroid/view/View;F)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AX1;->b(Landroid/animation/Animator;)LX/AX1;

    move-result-object v0

    new-instance v1, Lcom/facebook/facecast/plugin/FacecastEndScreenAnimationHelper$1;

    invoke-direct {v1, p1}, Lcom/facebook/facecast/plugin/FacecastEndScreenAnimationHelper$1;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/AX1;->b(Ljava/lang/Runnable;)LX/AX1;

    .line 1683658
    return-void
.end method

.method public static b(Landroid/view/View;F)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1683659
    const-string v0, "translationY"

    invoke-static {p0, v0, p1}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;F)LX/4mQ;

    move-result-object v0

    return-object v0
.end method
