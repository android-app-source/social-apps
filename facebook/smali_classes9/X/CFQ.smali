.class public final LX/CFQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V
    .locals 0

    .prologue
    .line 1863237
    iput-object p1, p0, LX/CFQ;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x155d9f87

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863238
    iget-object v1, p0, LX/CFQ;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1863239
    iget-object v1, p0, LX/CFQ;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    .line 1863240
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance p0, LX/8AA;

    sget-object p1, LX/8AB;->GOODWILL_COMPOSER:LX/8AB;

    invoke-direct {p0, p1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {p0}, LX/8AA;->i()LX/8AA;

    move-result-object p0

    invoke-virtual {p0}, LX/8AA;->j()LX/8AA;

    move-result-object p0

    invoke-virtual {p0}, LX/8AA;->l()LX/8AA;

    move-result-object p0

    sget-object p1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {p0, p1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v2

    .line 1863241
    iget-object p0, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a:Lcom/facebook/content/SecureContextHelper;

    const/4 p1, 0x1

    invoke-interface {p0, v2, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1863242
    :goto_0
    const v1, 0x36c9670c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1863243
    :cond_0
    iget-object v1, p0, LX/CFQ;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d()V

    goto :goto_0
.end method
