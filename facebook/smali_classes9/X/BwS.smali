.class public final LX/BwS;
.super LX/3Gy;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Gy",
        "<",
        "LX/Add;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 1833793
    iput-object p1, p0, LX/BwS;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-direct {p0}, LX/3Gy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Add;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1833794
    const-class v0, LX/Add;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1833795
    check-cast p1, LX/Add;

    .line 1833796
    iget-object v0, p0, LX/BwS;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget v1, p1, LX/Add;->a:F

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setAlpha(F)V

    .line 1833797
    iget-object v1, p0, LX/BwS;->a:Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    iget v0, p1, LX/Add;->a:F

    float-to-double v2, v0

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 1833798
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->D:Z

    .line 1833799
    return-void

    .line 1833800
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
