.class public LX/BQj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:LX/0ih;

.field private static volatile f:LX/BQj;


# instance fields
.field private final c:LX/0if;

.field public d:Z

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782643
    const-class v0, LX/BQj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BQj;->a:Ljava/lang/String;

    .line 1782644
    sget-object v0, LX/0ig;->aB:LX/0ih;

    sput-object v0, LX/BQj;->b:LX/0ih;

    return-void
.end method

.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1782639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782640
    iput-object p1, p0, LX/BQj;->c:LX/0if;

    .line 1782641
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BQj;->d:Z

    .line 1782642
    return-void
.end method

.method public static a(LX/0QB;)LX/BQj;
    .locals 4

    .prologue
    .line 1782609
    sget-object v0, LX/BQj;->f:LX/BQj;

    if-nez v0, :cond_1

    .line 1782610
    const-class v1, LX/BQj;

    monitor-enter v1

    .line 1782611
    :try_start_0
    sget-object v0, LX/BQj;->f:LX/BQj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1782612
    if-eqz v2, :cond_0

    .line 1782613
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1782614
    new-instance p0, LX/BQj;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/BQj;-><init>(LX/0if;)V

    .line 1782615
    move-object v0, p0

    .line 1782616
    sput-object v0, LX/BQj;->f:LX/BQj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1782617
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1782618
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1782619
    :cond_1
    sget-object v0, LX/BQj;->f:LX/BQj;

    return-object v0

    .line 1782620
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1782621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BQj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1782637
    iget-object v0, p0, LX/BQj;->c:LX/0if;

    sget-object v1, LX/BQj;->b:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1782638
    return-void
.end method

.method public static a(LX/BQj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1782635
    iget-object v0, p0, LX/BQj;->c:LX/0if;

    sget-object v1, LX/BQj;->b:LX/0ih;

    const/4 v2, 0x0

    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "exposedEditingTools"

    invoke-virtual {v3, v4, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    invoke-virtual {v0, v1, p1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1782636
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1782645
    sget-object v0, LX/BQj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1782631
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BQj;->d:Z

    .line 1782632
    iput p1, p0, LX/BQj;->e:I

    .line 1782633
    iget-object v0, p0, LX/BQj;->c:LX/0if;

    sget-object v1, LX/BQj;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1782634
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1782628
    iget-object v0, p0, LX/BQj;->c:LX/0if;

    sget-object v1, LX/BQj;->b:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1782629
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BQj;->d:Z

    .line 1782630
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1782626
    const-string v0, "android_profile_video_camera"

    invoke-static {p0, v0}, LX/BQj;->a(LX/BQj;Ljava/lang/String;)V

    .line 1782627
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1782624
    const-string v0, "android_profile_video_staging_ground_preview"

    const-string v1, "makeTemporary|setCaption"

    invoke-static {p0, v0, v1}, LX/BQj;->a(LX/BQj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1782625
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1782622
    const-string v0, "android_profile_video_exited"

    invoke-static {p0, v0}, LX/BQj;->a(LX/BQj;Ljava/lang/String;)V

    .line 1782623
    return-void
.end method
