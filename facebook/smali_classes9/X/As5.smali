.class public final enum LX/As5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/As5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/As5;

.field public static final enum PHOTO:LX/As5;

.field public static final enum VIDEO:LX/As5;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1720034
    new-instance v0, LX/As5;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/As5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/As5;->PHOTO:LX/As5;

    new-instance v0, LX/As5;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/As5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/As5;->VIDEO:LX/As5;

    .line 1720035
    const/4 v0, 0x2

    new-array v0, v0, [LX/As5;

    sget-object v1, LX/As5;->PHOTO:LX/As5;

    aput-object v1, v0, v2

    sget-object v1, LX/As5;->VIDEO:LX/As5;

    aput-object v1, v0, v3

    sput-object v0, LX/As5;->$VALUES:[LX/As5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1720036
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/As5;
    .locals 1

    .prologue
    .line 1720037
    const-class v0, LX/As5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/As5;

    return-object v0
.end method

.method public static values()[LX/As5;
    .locals 1

    .prologue
    .line 1720038
    sget-object v0, LX/As5;->$VALUES:[LX/As5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/As5;

    return-object v0
.end method
