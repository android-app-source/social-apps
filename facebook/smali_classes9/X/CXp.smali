.class public final enum LX/CXp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CXp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CXp;

.field public static final enum ACTION_BAR_DISPATCH_DRAW_WITH_DATA:LX/CXp;

.field public static final enum CALL_TO_ACTION:LX/CXp;

.field public static final enum CONTEXT_ITEMS_DISPATCH_DRAW:LX/CXp;

.field public static final enum COVER_PHOTO_COMPLETE:LX/CXp;

.field public static final enum HEADER_DISPATCH_DRAW_HAS_DATA:LX/CXp;

.field public static final enum IS_ADMIN_KNOWN:LX/CXp;

.field public static final enum METABOX:LX/CXp;

.field public static final enum PROFILE_PHOTO_COMPLETE:LX/CXp;


# instance fields
.field public perfTagName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1910647
    new-instance v0, LX/CXp;

    const-string v1, "ACTION_BAR_DISPATCH_DRAW_WITH_DATA"

    const-string v2, "ActionBar"

    invoke-direct {v0, v1, v4, v2}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->ACTION_BAR_DISPATCH_DRAW_WITH_DATA:LX/CXp;

    .line 1910648
    new-instance v0, LX/CXp;

    const-string v1, "CALL_TO_ACTION"

    const-string v2, "CallToAction"

    invoke-direct {v0, v1, v5, v2}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->CALL_TO_ACTION:LX/CXp;

    .line 1910649
    new-instance v0, LX/CXp;

    const-string v1, "CONTEXT_ITEMS_DISPATCH_DRAW"

    const-string v2, "ContextItems"

    invoke-direct {v0, v1, v6, v2}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->CONTEXT_ITEMS_DISPATCH_DRAW:LX/CXp;

    .line 1910650
    new-instance v0, LX/CXp;

    const-string v1, "COVER_PHOTO_COMPLETE"

    const-string v2, "CoverPhoto"

    invoke-direct {v0, v1, v7, v2}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->COVER_PHOTO_COMPLETE:LX/CXp;

    .line 1910651
    new-instance v0, LX/CXp;

    const-string v1, "HEADER_DISPATCH_DRAW_HAS_DATA"

    const-string v2, "HeaderDraw"

    invoke-direct {v0, v1, v8, v2}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->HEADER_DISPATCH_DRAW_HAS_DATA:LX/CXp;

    .line 1910652
    new-instance v0, LX/CXp;

    const-string v1, "IS_ADMIN_KNOWN"

    const/4 v2, 0x5

    const-string v3, "IsAdminKnown"

    invoke-direct {v0, v1, v2, v3}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->IS_ADMIN_KNOWN:LX/CXp;

    .line 1910653
    new-instance v0, LX/CXp;

    const-string v1, "PROFILE_PHOTO_COMPLETE"

    const/4 v2, 0x6

    const-string v3, "ProfilePhoto"

    invoke-direct {v0, v1, v2, v3}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->PROFILE_PHOTO_COMPLETE:LX/CXp;

    .line 1910654
    new-instance v0, LX/CXp;

    const-string v1, "METABOX"

    const/4 v2, 0x7

    const-string v3, "Metabox"

    invoke-direct {v0, v1, v2, v3}, LX/CXp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CXp;->METABOX:LX/CXp;

    .line 1910655
    const/16 v0, 0x8

    new-array v0, v0, [LX/CXp;

    sget-object v1, LX/CXp;->ACTION_BAR_DISPATCH_DRAW_WITH_DATA:LX/CXp;

    aput-object v1, v0, v4

    sget-object v1, LX/CXp;->CALL_TO_ACTION:LX/CXp;

    aput-object v1, v0, v5

    sget-object v1, LX/CXp;->CONTEXT_ITEMS_DISPATCH_DRAW:LX/CXp;

    aput-object v1, v0, v6

    sget-object v1, LX/CXp;->COVER_PHOTO_COMPLETE:LX/CXp;

    aput-object v1, v0, v7

    sget-object v1, LX/CXp;->HEADER_DISPATCH_DRAW_HAS_DATA:LX/CXp;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CXp;->IS_ADMIN_KNOWN:LX/CXp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CXp;->PROFILE_PHOTO_COMPLETE:LX/CXp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CXp;->METABOX:LX/CXp;

    aput-object v2, v0, v1

    sput-object v0, LX/CXp;->$VALUES:[LX/CXp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910656
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1910657
    iput-object p3, p0, LX/CXp;->perfTagName:Ljava/lang/String;

    .line 1910658
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CXp;
    .locals 1

    .prologue
    .line 1910659
    const-class v0, LX/CXp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CXp;

    return-object v0
.end method

.method public static values()[LX/CXp;
    .locals 1

    .prologue
    .line 1910660
    sget-object v0, LX/CXp;->$VALUES:[LX/CXp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CXp;

    return-object v0
.end method
