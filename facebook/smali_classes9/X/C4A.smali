.class public LX/C4A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/20g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/20g",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/20e;

.field private final c:LX/1zf;

.field public final d:LX/20K;

.field public final e:LX/21T;


# direct methods
.method public constructor <init>(LX/20g;LX/20e;LX/1zf;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/20K;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1846963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846964
    iput-object p1, p0, LX/C4A;->a:LX/20g;

    .line 1846965
    iput-object p2, p0, LX/C4A;->b:LX/20e;

    .line 1846966
    iput-object p3, p0, LX/C4A;->c:LX/1zf;

    .line 1846967
    iput-object p5, p0, LX/C4A;->d:LX/20K;

    .line 1846968
    invoke-virtual {p4}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a()LX/21T;

    move-result-object v0

    iput-object v0, p0, LX/C4A;->e:LX/21T;

    .line 1846969
    return-void
.end method

.method public static a(LX/0QB;)LX/C4A;
    .locals 9

    .prologue
    .line 1846936
    const-class v1, LX/C4A;

    monitor-enter v1

    .line 1846937
    :try_start_0
    sget-object v0, LX/C4A;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846938
    sput-object v2, LX/C4A;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846939
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846940
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846941
    new-instance v3, LX/C4A;

    invoke-static {v0}, LX/20g;->a(LX/0QB;)LX/20g;

    move-result-object v4

    check-cast v4, LX/20g;

    invoke-static {v0}, LX/20e;->b(LX/0QB;)LX/20e;

    move-result-object v5

    check-cast v5, LX/20e;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v6

    check-cast v6, LX/1zf;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-static {v0}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v8

    check-cast v8, LX/20K;

    invoke-direct/range {v3 .. v8}, LX/C4A;-><init>(LX/20g;LX/20e;LX/1zf;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/20K;)V

    .line 1846942
    move-object v0, v3

    .line 1846943
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846944
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846945
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1zt;LX/21I;[FLjava/lang/Boolean;)V
    .locals 10
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1zt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation build Lcom/facebook/components/annotations/OnBind;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1zt;",
            "LX/21I;",
            "[F",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1846947
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1846948
    iget-object v0, p4, LX/21I;->g:Ljava/util/EnumMap;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p4, LX/21I;->c:LX/20Z;

    invoke-static {p1, v0, v1, v2}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 1846949
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p4, LX/21I;->d:LX/21M;

    iget-object v3, p4, LX/21I;->e:LX/0wd;

    iget-object v4, p4, LX/21I;->f:LX/20z;

    iget-object v5, p0, LX/C4A;->c:LX/1zf;

    iget-object v6, p4, LX/21I;->h:LX/20I;

    iget-object v7, p4, LX/21I;->i:LX/0Px;

    iget-object v8, p4, LX/21I;->j:LX/21H;

    move-object v0, p1

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 1846950
    if-eqz p3, :cond_0

    .line 1846951
    invoke-virtual {p1, p3}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    .line 1846952
    :cond_0
    invoke-virtual {p1, p5}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 1846953
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 1846954
    iget-object v0, p0, LX/C4A;->b:LX/20e;

    invoke-virtual {v0, p1, v9}, LX/20e;->a(LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1846955
    iget-boolean v0, p4, LX/21I;->a:Z

    if-eqz v0, :cond_1

    .line 1846956
    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {p1, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p4, LX/21I;->b:LX/20b;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1846957
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1846958
    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getMeasuredWidth()I

    move-result v0

    .line 1846959
    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getMeasuredHeight()I

    move-result v1

    .line 1846960
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->measure(II)V

    .line 1846961
    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getTop()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getRight()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->getBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->layout(IIII)V

    goto :goto_0

    .line 1846962
    :cond_2
    return-void
.end method
