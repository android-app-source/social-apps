.class public LX/BVo;
.super LX/BVa;
.source ""


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791444
    invoke-direct {p0, p1, p2}, LX/BVa;-><init>(LX/BW4;LX/BW5;)V

    .line 1791445
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1791438
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1791439
    move-object v0, p1

    check-cast v0, Landroid/widget/ScrollView;

    .line 1791440
    sget-object v1, LX/BVm;->a:[I

    invoke-static {p2}, LX/BVn;->from(Ljava/lang/String;)LX/BVn;

    move-result-object v2

    invoke-virtual {v2}, LX/BVn;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1791441
    invoke-super {p0, p1, p2, p3, p4}, LX/BVa;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1791442
    :goto_0
    return-void

    .line 1791443
    :pswitch_0
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setFillViewport(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
