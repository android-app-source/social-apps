.class public final LX/AxS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1728281
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728282
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1728283
    if-eqz v0, :cond_6

    .line 1728284
    const-string v1, "souvenir_classifier_model_params_maps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728285
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728286
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1728287
    if-eqz v2, :cond_5

    .line 1728288
    const-string v3, "edges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728289
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1728290
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 1728291
    invoke-virtual {p0, v2, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1728292
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728293
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1728294
    if-eqz v6, :cond_3

    .line 1728295
    const-string v7, "node"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728296
    const-wide/16 v10, 0x0

    .line 1728297
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728298
    const/4 v8, 0x0

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1728299
    if-eqz v8, :cond_0

    .line 1728300
    const-string v9, "model"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728301
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1728302
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1728303
    if-eqz v8, :cond_1

    .line 1728304
    const-string v9, "model_id"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728305
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1728306
    :cond_1
    const/4 v8, 0x2

    invoke-virtual {p0, v6, v8, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1728307
    cmpl-double v10, v8, v10

    if-eqz v10, :cond_2

    .line 1728308
    const-string v10, "threshold"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728309
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(D)V

    .line 1728310
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728311
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728312
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1728313
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1728314
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728315
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728316
    return-void
.end method
