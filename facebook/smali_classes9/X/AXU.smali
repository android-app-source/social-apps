.class public final LX/AXU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AXY;


# direct methods
.method public constructor <init>(LX/AXY;)V
    .locals 0

    .prologue
    .line 1683976
    iput-object p1, p0, LX/AXU;->a:LX/AXY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x70b2a271

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1683977
    iget-object v1, p0, LX/AXU;->a:LX/AXY;

    check-cast p1, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    .line 1683978
    iget-object v3, v1, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683979
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v3, v4

    .line 1683980
    if-nez v3, :cond_1

    .line 1683981
    :cond_0
    :goto_0
    const v1, 0x627e70f3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1683982
    :cond_1
    new-instance v4, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-direct {v4}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;-><init>()V

    .line 1683983
    new-instance v3, LX/AXV;

    invoke-direct {v3, v1, p1}, LX/AXV;-><init>(LX/AXY;Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;)V

    .line 1683984
    new-instance v5, LX/AXE;

    invoke-direct {v5, v1}, LX/AXE;-><init>(LX/AXY;)V

    .line 1683985
    new-instance v6, LX/93W;

    const/4 p0, 0x0

    invoke-direct {v6, v5, v3, p0}, LX/93W;-><init>(LX/8Q5;LX/93X;LX/8Rm;)V

    invoke-virtual {v4, v6}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->a(LX/93W;)V

    .line 1683986
    iget-object v3, v1, LX/AXY;->m:LX/8RJ;

    sget-object v5, LX/8RI;->FACECAST_ACTIVITY:LX/8RI;

    invoke-virtual {v3, v5}, LX/8RJ;->a(LX/8RI;)V

    .line 1683987
    invoke-virtual {v1}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    const-string v5, "ENDSCREEN_FACECAST_AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v4, v3, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1683988
    iget-object v3, v1, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    invoke-virtual {v3}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->b()V

    goto :goto_0
.end method
