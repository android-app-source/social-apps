.class public LX/CF2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/CF1;

.field private static volatile c:LX/CF2;


# instance fields
.field public final b:LX/11i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1862344
    new-instance v0, LX/CF1;

    invoke-direct {v0}, LX/CF1;-><init>()V

    sput-object v0, LX/CF2;->a:LX/CF1;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862346
    iput-object p1, p0, LX/CF2;->b:LX/11i;

    .line 1862347
    return-void
.end method

.method public static a(LX/0QB;)LX/CF2;
    .locals 4

    .prologue
    .line 1862348
    sget-object v0, LX/CF2;->c:LX/CF2;

    if-nez v0, :cond_1

    .line 1862349
    const-class v1, LX/CF2;

    monitor-enter v1

    .line 1862350
    :try_start_0
    sget-object v0, LX/CF2;->c:LX/CF2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1862351
    if-eqz v2, :cond_0

    .line 1862352
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1862353
    new-instance p0, LX/CF2;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/CF2;-><init>(LX/11i;)V

    .line 1862354
    move-object v0, p0

    .line 1862355
    sput-object v0, LX/CF2;->c:LX/CF2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1862356
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1862357
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1862358
    :cond_1
    sget-object v0, LX/CF2;->c:LX/CF2;

    return-object v0

    .line 1862359
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1862360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
