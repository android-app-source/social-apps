.class public final LX/AqN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1De;I)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 1717564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1717565
    invoke-static {p2}, Lcom/facebook/fig/button/FigToggleButton;->a(I)[I

    move-result-object v3

    .line 1717566
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_4

    aget v0, v3, v2

    .line 1717567
    sget-object v5, LX/03r;->FigToggleButtonAttrs:[I

    invoke-virtual {p1, v0, v5}, LX/1De;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 1717568
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v6

    move v0, v1

    .line 1717569
    :goto_1
    if-ge v0, v6, :cond_3

    .line 1717570
    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v7

    .line 1717571
    const/16 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 1717572
    invoke-virtual {v5, v7, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/AqN;->a:I

    .line 1717573
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1717574
    :cond_1
    const/16 v8, 0x0

    if-ne v7, v8, :cond_2

    .line 1717575
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    iput v7, p0, LX/AqN;->c:I

    goto :goto_2

    .line 1717576
    :cond_2
    const/16 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 1717577
    invoke-virtual {v5, v7, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/AqN;->b:I

    goto :goto_2

    .line 1717578
    :cond_3
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 1717579
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1717580
    :cond_4
    return-void
.end method
