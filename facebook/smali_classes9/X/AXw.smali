.class public final LX/AXw;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/AY1;

.field private b:I

.field private c:I

.field private d:I

.field private e:F


# direct methods
.method public constructor <init>(LX/AY1;)V
    .locals 0

    .prologue
    .line 1684661
    iput-object p1, p0, LX/AXw;->a:LX/AY1;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    .line 1684654
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    .line 1684655
    iget v1, p0, LX/AXw;->e:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/AXw;->a:LX/AY1;

    iget-object v1, v1, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-virtual {v1}, Lcom/facebook/facecast/FacecastPreviewView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, LX/AXw;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, LX/AXw;->b:I

    add-int/2addr v0, v1

    .line 1684656
    iget v1, p0, LX/AXw;->d:I

    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1684657
    iget v1, p0, LX/AXw;->c:I

    if-eq v0, v1, :cond_0

    .line 1684658
    iget-object v1, p0, LX/AXw;->a:LX/AY1;

    iget-object v1, v1, LX/AY1;->m:LX/AV1;

    const/4 v2, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 1684659
    :cond_0
    iput v0, p0, LX/AXw;->c:I

    .line 1684660
    const/4 v0, 0x1

    return v0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 1684668
    iget-object v0, p0, LX/AXw;->a:LX/AY1;

    iget-object v0, v0, LX/AY1;->m:LX/AV1;

    .line 1684669
    iget-object v1, v0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v1

    :goto_0
    move v0, v1

    .line 1684670
    iput v0, p0, LX/AXw;->b:I

    .line 1684671
    iget v0, p0, LX/AXw;->b:I

    iput v0, p0, LX/AXw;->c:I

    .line 1684672
    iget-object v0, p0, LX/AXw;->a:LX/AY1;

    iget-object v0, v0, LX/AY1;->m:LX/AV1;

    .line 1684673
    iget-object v1, v0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v1

    :goto_1
    move v0, v1

    .line 1684674
    iput v0, p0, LX/AXw;->d:I

    .line 1684675
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, LX/AXw;->e:F

    .line 1684676
    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4

    .prologue
    .line 1684662
    iget-object v0, p0, LX/AXw;->a:LX/AY1;

    iget-object v0, v0, LX/AY1;->a:LX/AVT;

    iget v1, p0, LX/AXw;->b:I

    iget v2, p0, LX/AXw;->c:I

    .line 1684663
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1684664
    const-string p0, "previous_zoom_level"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684665
    const-string p0, "new_zoom_level"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684666
    invoke-virtual {v0, v3}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1684667
    return-void
.end method
