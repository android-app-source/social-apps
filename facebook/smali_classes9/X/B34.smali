.class public final LX/B34;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

.field public final synthetic c:I

.field public final synthetic d:LX/B35;


# direct methods
.method public constructor <init>(LX/B35;Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;I)V
    .locals 0

    .prologue
    .line 1739511
    iput-object p1, p0, LX/B34;->d:LX/B35;

    iput-object p2, p0, LX/B34;->a:Landroid/app/Activity;

    iput-object p3, p0, LX/B34;->b:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    iput p4, p0, LX/B34;->c:I

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1739507
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/B34;->a:Landroid/app/Activity;

    const-class v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1739508
    const-string v1, "heisman_camera_intent_data"

    iget-object v2, p0, LX/B34;->b:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1739509
    iget-object v1, p0, LX/B34;->d:LX/B35;

    iget-object v1, v1, LX/B35;->c:Lcom/facebook/content/SecureContextHelper;

    iget v2, p0, LX/B34;->c:I

    iget-object v3, p0, LX/B34;->a:Landroid/app/Activity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1739510
    return-void
.end method
