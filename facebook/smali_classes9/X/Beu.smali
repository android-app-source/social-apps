.class public final LX/Beu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public final synthetic b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V
    .locals 0

    .prologue
    .line 1805497
    iput-object p1, p0, LX/Beu;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    iput-object p2, p0, LX/Beu;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1805494
    iget-object v0, p0, LX/Beu;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1805495
    iget-object v0, p0, LX/Beu;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-virtual {v0}, LX/BeU;->a()V

    .line 1805496
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1805491
    iget-object v0, p0, LX/Beu;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/Beu;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082938

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Bet;

    invoke-direct {v2, p0}, LX/Bet;-><init>(LX/Beu;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1805492
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1805493
    invoke-direct {p0}, LX/Beu;->a()V

    return-void
.end method
