.class public LX/Apo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/2g9;

.field private final b:LX/Apv;

.field private final c:LX/1vg;


# direct methods
.method public constructor <init>(LX/2g9;LX/Apv;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1716739
    iput-object p1, p0, LX/Apo;->a:LX/2g9;

    .line 1716740
    iput-object p2, p0, LX/Apo;->b:LX/Apv;

    .line 1716741
    iput-object p3, p0, LX/Apo;->c:LX/1vg;

    .line 1716742
    return-void
.end method

.method public static a(LX/0QB;)LX/Apo;
    .locals 6

    .prologue
    .line 1716743
    const-class v1, LX/Apo;

    monitor-enter v1

    .line 1716744
    :try_start_0
    sget-object v0, LX/Apo;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716745
    sput-object v2, LX/Apo;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716746
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716747
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716748
    new-instance p0, LX/Apo;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-static {v0}, LX/Apv;->a(LX/0QB;)LX/Apv;

    move-result-object v4

    check-cast v4, LX/Apv;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, LX/Apo;-><init>(LX/2g9;LX/Apv;LX/1vg;)V

    .line 1716749
    move-object v0, p0

    .line 1716750
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716751
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716752
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/util/SparseArray;LX/1dQ;Landroid/view/View$OnClickListener;LX/1dQ;Landroid/widget/CompoundButton$OnCheckedChangeListener;LX/1X1;)LX/1Dg;
    .locals 6
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/utils/annotations/FigActionType;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/util/SparseArray;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Landroid/widget/CompoundButton$OnCheckedChangeListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation runtime Ljava/lang/Deprecated;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Ljava/lang/CharSequence;",
            "I",
            "Ljava/lang/CharSequence;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1716754
    packed-switch p2, :pswitch_data_0

    .line 1716755
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported attachment type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1716756
    :pswitch_0
    if-nez p11, :cond_0

    const/4 v1, 0x0

    .line 1716757
    :goto_0
    return-object v1

    .line 1716758
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0

    .line 1716759
    :pswitch_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b1169

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/Apo;->a:LX/2g9;

    invoke-virtual {v2, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v2

    const/16 v3, 0x102

    invoke-virtual {v2, v3}, LX/2gA;->h(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p5}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p6}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v2

    if-eqz p7, :cond_1

    :goto_1
    invoke-interface {v2, p7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz p8, :cond_2

    invoke-static {p1}, LX/Apn;->onClick(LX/1De;)LX/1dQ;

    move-result-object p7

    goto :goto_1

    :cond_2
    const/4 p7, 0x0

    goto :goto_1

    .line 1716760
    :pswitch_2
    const/4 v1, 0x3

    if-ne p2, v1, :cond_6

    .line 1716761
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    iget-object v1, p0, LX/Apo;->c:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    if-nez p7, :cond_3

    if-nez p8, :cond_3

    const v1, 0x7f0a029a

    :goto_2
    invoke-virtual {v3, v1}, LX/2xv;->j(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    if-eqz p7, :cond_4

    :goto_3
    invoke-interface {v1, p7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    .line 1716762
    :goto_4
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1716763
    const v3, 0x7f0b1168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1716764
    const v4, 0x7f0b1169

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1716765
    shl-int/lit8 v4, v2, 0x1

    add-int/2addr v4, v3

    .line 1716766
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    invoke-interface {v3, v4, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f020b09

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p5}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p6}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    .line 1716767
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_0

    .line 1716768
    :cond_3
    const v1, 0x7f0a0299

    goto :goto_2

    :cond_4
    if-eqz p8, :cond_5

    invoke-static {p1}, LX/Apn;->onClick(LX/1De;)LX/1dQ;

    move-result-object p7

    goto :goto_3

    :cond_5
    const/4 p7, 0x0

    goto :goto_3

    .line 1716769
    :cond_6
    iget-object v1, p0, LX/Apo;->b:LX/Apv;

    invoke-virtual {v1, p1}, LX/Apv;->c(LX/1De;)LX/Aps;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/Aps;->j(I)LX/Aps;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/Aps;->k(I)LX/Aps;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/Aps;->h(I)LX/Aps;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/Aps;->i(I)LX/Aps;

    move-result-object v1

    if-eqz p9, :cond_7

    :goto_5
    invoke-virtual {v1, p9}, LX/Aps;->a(LX/1dQ;)LX/Aps;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    goto :goto_4

    :cond_7
    if-eqz p10, :cond_8

    invoke-static {p1}, LX/Apn;->d(LX/1De;)LX/1dQ;

    move-result-object p9

    goto :goto_5

    :cond_8
    const/4 p9, 0x0

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
