.class public LX/Arr;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Arq;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719836
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1719837
    return-void
.end method


# virtual methods
.method public final a(LX/Arh;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;Z)LX/Arq;
    .locals 24

    .prologue
    .line 1719838
    new-instance v1, LX/Arq;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v8

    check-cast v8, LX/0Zr;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a(LX/0QB;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    move-result-object v11

    check-cast v11, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    const-class v2, LX/BAD;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/BAD;

    const-class v2, LX/8Fx;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/8Fx;

    const-class v2, LX/7SL;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/7SL;

    invoke-static/range {p0 .. p0}, LX/BVQ;->a(LX/0QB;)LX/BVQ;

    move-result-object v15

    check-cast v15, LX/BVQ;

    const-class v2, LX/6KO;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/6KO;

    invoke-static/range {p0 .. p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v17

    check-cast v17, LX/1Er;

    invoke-static/range {p0 .. p0}, LX/6KT;->a(LX/0QB;)LX/6KT;

    move-result-object v18

    check-cast v18, LX/6KT;

    invoke-static/range {p0 .. p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v19

    check-cast v19, LX/1FZ;

    invoke-static/range {p0 .. p0}, LX/0XD;->a(LX/0QB;)LX/03R;

    move-result-object v20

    check-cast v20, LX/03R;

    invoke-static/range {p0 .. p0}, LX/BA4;->a(LX/0QB;)LX/BA4;

    move-result-object v21

    check-cast v21, LX/BA4;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v22

    check-cast v22, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v23

    check-cast v23, LX/0fO;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-direct/range {v1 .. v23}, LX/Arq;-><init>(LX/Arh;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;ZLandroid/content/Context;Landroid/view/WindowManager;LX/03V;LX/0Zr;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;LX/BAD;LX/8Fx;LX/7SL;LX/BVQ;LX/6KO;LX/1Er;LX/6KT;LX/1FZ;LX/03R;LX/BA4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0fO;)V

    .line 1719839
    return-object v1
.end method
