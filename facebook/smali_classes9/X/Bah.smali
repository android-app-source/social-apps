.class public LX/Bah;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1799483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/res/Resources;)I
    .locals 5

    .prologue
    .line 1799475
    const v0, 0x7f080f6e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1799476
    const v1, 0x7f0b089b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1799477
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 1799478
    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1799479
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1799480
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1799481
    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1799482
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public static a(LX/Ban;Landroid/view/ViewStub;Landroid/view/ViewStub;Z)V
    .locals 5

    .prologue
    .line 1799462
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1799463
    invoke-virtual {p0}, LX/Ban;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0207b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 1799464
    if-nez p3, :cond_1

    .line 1799465
    const v2, 0x7f0b089f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int v1, v2, v1

    .line 1799466
    invoke-static {v0}, LX/Bah;->a(Landroid/content/res/Resources;)I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 1799467
    invoke-static {p1, p2}, LX/Bah;->a(Landroid/view/ViewStub;Landroid/view/ViewStub;)V

    .line 1799468
    :goto_0
    return-void

    .line 1799469
    :cond_0
    invoke-static {p1, p2}, LX/Bah;->b(Landroid/view/ViewStub;Landroid/view/ViewStub;)V

    goto :goto_0

    .line 1799470
    :cond_1
    const v2, 0x7f0b08a0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v2, v1

    .line 1799471
    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    const v4, 0x7f0b08a1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    sub-int v1, v3, v1

    .line 1799472
    invoke-static {v0}, LX/Bah;->a(Landroid/content/res/Resources;)I

    move-result v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 1799473
    invoke-static {p1, p2}, LX/Bah;->a(Landroid/view/ViewStub;Landroid/view/ViewStub;)V

    goto :goto_0

    .line 1799474
    :cond_2
    invoke-static {p1, p2}, LX/Bah;->b(Landroid/view/ViewStub;Landroid/view/ViewStub;)V

    goto :goto_0
.end method

.method private static a(Landroid/view/ViewStub;Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 1799456
    const v0, 0x7f031051

    invoke-virtual {p0, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1799457
    const v0, 0x7f031051

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1799458
    return-void
.end method

.method private static b(Landroid/view/ViewStub;Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 1799459
    const v0, 0x7f031050

    invoke-virtual {p0, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1799460
    const v0, 0x7f031050

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1799461
    return-void
.end method
