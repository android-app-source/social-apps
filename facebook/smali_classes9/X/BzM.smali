.class public LX/BzM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/BzC;

.field private final b:LX/1xu;


# direct methods
.method public constructor <init>(LX/BzC;LX/1xu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838613
    iput-object p1, p0, LX/BzM;->a:LX/BzC;

    .line 1838614
    iput-object p2, p0, LX/BzM;->b:LX/1xu;

    .line 1838615
    return-void
.end method

.method public static a(LX/0QB;)LX/BzM;
    .locals 5

    .prologue
    .line 1838616
    const-class v1, LX/BzM;

    monitor-enter v1

    .line 1838617
    :try_start_0
    sget-object v0, LX/BzM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838618
    sput-object v2, LX/BzM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838619
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838620
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838621
    new-instance p0, LX/BzM;

    const-class v3, LX/BzC;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/BzC;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v4

    check-cast v4, LX/1xu;

    invoke-direct {p0, v3, v4}, LX/BzM;-><init>(LX/BzC;LX/1xu;)V

    .line 1838622
    move-object v0, p0

    .line 1838623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;II)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1eK;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/1eK;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1838627
    invoke-virtual {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1838628
    iget-object v1, p0, LX/BzM;->a:LX/BzC;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-virtual/range {v1 .. v6}, LX/BzC;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;)LX/BzB;

    move-result-object v2

    .line 1838629
    iget-object v3, p0, LX/BzM;->b:LX/1xu;

    move-object v1, p4

    check-cast v1, LX/1Pr;

    invoke-virtual {v3, v2, v1}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 1838630
    check-cast p4, LX/1Pr;

    invoke-interface {v2}, LX/1xz;->a()LX/1KL;

    move-result-object v1

    invoke-interface {p4, v1, v7}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yT;

    .line 1838631
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    iget-object v1, v1, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, LX/1ne;->p(I)LX/1ne;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1
.end method
