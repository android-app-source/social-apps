.class public final LX/BRA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/17Y;

.field public final synthetic b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/17Y;)V
    .locals 0

    .prologue
    .line 1783319
    iput-object p1, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iput-object p2, p0, LX/BRA;->a:LX/17Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const v0, 0x76fd09a

    invoke-static {v6, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1783320
    iget-object v1, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v2, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->g:Ljava/lang/String;

    iget-object v3, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->f:Ljava/lang/String;

    iget-object v4, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783321
    iget-object p1, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, p1

    .line 1783322
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v4

    .line 1783323
    const-string p1, "staging_ground_tap_change_photo_button"

    invoke-static {v1, p1, v2, v3, v4}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783324
    iget-object v1, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    const/4 v2, 0x0

    iput-object v2, v1, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783325
    iget-object v1, p0, LX/BRA;->a:LX/17Y;

    iget-object v2, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->u:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    sget-object v3, LX/0ax;->bY:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1783326
    const-string v2, "extra_staging_ground_frame_pack"

    iget-object v3, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    .line 1783327
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1783328
    iget-object v4, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783329
    iget-object v7, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, v7

    .line 1783330
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->d()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-result-object v4

    .line 1783331
    iget-object v7, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783332
    iget-object v9, v7, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v7, v9

    .line 1783333
    invoke-virtual {v7}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v7

    invoke-static {v7, v4}, LX/B5R;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 1783334
    const/4 v4, 0x0

    move v7, v4

    :goto_0
    if-ge v7, v10, :cond_1

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5QV;

    .line 1783335
    invoke-interface {v4}, LX/5QV;->c()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {v4}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1783336
    invoke-interface {v4}, LX/5QV;->c()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v4}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, LX/B5P;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v4

    .line 1783337
    invoke-virtual {v8, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1783338
    :cond_0
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_0

    .line 1783339
    :cond_1
    new-instance v4, LX/5ii;

    invoke-direct {v4}, LX/5ii;-><init>()V

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 1783340
    iput-object v7, v4, LX/5ii;->b:LX/0Px;

    .line 1783341
    move-object v4, v4

    .line 1783342
    invoke-virtual {v4}, LX/5ii;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v4

    move-object v3, v4

    .line 1783343
    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1783344
    const-string v2, "extra_staging_ground_selected_frame_id"

    iget-object v3, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783345
    iget-object v4, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, v4

    .line 1783346
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783347
    const-string v2, "extra_should_merge_camera_roll"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1783348
    const-string v2, "extra_should_show_suggested_photos"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1783349
    const-string v2, "extra_simple_picker_launcher_configuration"

    const/4 v3, 0x0

    sget-object v4, LX/8A9;->RETURN_TO_STAGING_GROUND:LX/8A9;

    invoke-static {v3, v5, v4}, LX/BQ2;->a(ZZLX/8A9;)Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783350
    const-string v2, "pick_pic_lite"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1783351
    const-string v2, "disable_camera_roll"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1783352
    const-string v2, "extra_photo_title_text"

    iget-object v3, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082741

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783353
    const-string v2, "disable_adding_photos_to_albums"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1783354
    iget-object v2, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->y:Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x4

    iget-object v4, p0, LX/BRA;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->u:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1783355
    const v1, 0x47197b73

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
