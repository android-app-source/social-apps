.class public final LX/BbL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/BbN;


# direct methods
.method public constructor <init>(LX/BbN;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1800729
    iput-object p1, p0, LX/BbL;->b:LX/BbN;

    iput-object p2, p0, LX/BbL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1800730
    iget-object v0, p0, LX/BbL;->b:LX/BbN;

    iget-object v1, p0, LX/BbL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1800731
    iget-object v2, v0, LX/BbN;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->k:LX/8y6;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    new-instance p0, LX/BbM;

    invoke-direct {p0, v0}, LX/BbM;-><init>(LX/BbN;)V

    .line 1800732
    new-instance p1, LX/4IP;

    invoke-direct {p1}, LX/4IP;-><init>()V

    .line 1800733
    const-string p2, "story_id"

    invoke-virtual {p1, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800734
    move-object p1, p1

    .line 1800735
    new-instance p2, LX/5Hg;

    invoke-direct {p2}, LX/5Hg;-><init>()V

    move-object p2, p2

    .line 1800736
    const-string v0, "input"

    invoke-virtual {p2, v0, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1800737
    iget-object p1, v2, LX/8y6;->b:LX/1Ck;

    const-string v0, "placelist_remove_explicit_attachment"

    iget-object v1, v2, LX/8y6;->a:LX/0tX;

    invoke-static {p2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p2

    invoke-virtual {v1, p2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    new-instance v1, LX/8y5;

    invoke-direct {v1, v2, p0}, LX/8y5;-><init>(LX/8y6;LX/0TF;)V

    invoke-virtual {p1, v0, p2, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1800738
    return-void
.end method
