.class public LX/CDW;
.super Lcom/facebook/video/player/RichVideoPlayer;
.source ""

# interfaces
.implements LX/3FP;
.implements LX/3FQ;
.implements LX/392;
.implements LX/3FR;
.implements LX/3FS;
.implements LX/3FT;


# instance fields
.field public m:Z

.field public n:LX/3I8;

.field private o:LX/7QP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1859507
    invoke-direct {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;)V

    .line 1859508
    return-void
.end method


# virtual methods
.method public final a(LX/7QP;)V
    .locals 1

    .prologue
    .line 1859501
    iget-object v0, p0, LX/CDW;->o:LX/7QP;

    if-eq v0, p1, :cond_0

    .line 1859502
    iput-object p1, p0, LX/CDW;->o:LX/7QP;

    .line 1859503
    invoke-virtual {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/7QP;)V

    .line 1859504
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Z)V

    .line 1859505
    :cond_0
    return-void

    .line 1859506
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859500
    return-void
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 0

    .prologue
    .line 1859499
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859498
    return-void
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 0

    .prologue
    .line 1859497
    return-object p0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 0

    .prologue
    .line 1859496
    return-object p0
.end method

.method public getAdditionalPlugins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1859495
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAndClearShowLiveCommentDialogFragment()Z
    .locals 2

    .prologue
    .line 1859492
    iget-boolean v0, p0, LX/CDW;->m:Z

    .line 1859493
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/CDW;->m:Z

    .line 1859494
    return v0
.end method

.method public getAudioChannelLayout()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1859509
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859510
    if-eqz v0, :cond_0

    .line 1859511
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859512
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    .line 1859513
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859514
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    .line 1859515
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859516
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/model/SphericalVideoParams;->a()LX/03z;

    move-result-object v0

    .line 1859517
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1859466
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    return-object v0
.end method

.method public getPluginSelector()LX/3Ge;
    .locals 1

    .prologue
    .line 1859465
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProjectionType()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1859467
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859468
    if-eqz v0, :cond_0

    .line 1859469
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859470
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    .line 1859471
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859472
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_0

    .line 1859473
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 1859474
    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 1859475
    iget-object p0, v0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    move-object v0, p0

    .line 1859476
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 0

    .prologue
    .line 1859477
    return-object p0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 1859478
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public getTransitionNode()LX/3FT;
    .locals 0
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1859479
    return-object p0
.end method

.method public getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1859480
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x52c5fdd7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1859483
    iget-object v2, p0, LX/CDW;->n:LX/3I8;

    if-eqz v2, :cond_1

    .line 1859484
    iget-object v2, p0, LX/CDW;->n:LX/3I8;

    invoke-interface {v2}, LX/3I8;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1859485
    invoke-virtual {p0}, LX/CDW;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1859486
    :cond_0
    iget-object v2, p0, LX/CDW;->n:LX/3I8;

    invoke-interface {v2, p1}, LX/3I8;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1859487
    if-ne v2, v3, :cond_2

    .line 1859488
    invoke-virtual {p0}, LX/CDW;->performClick()Z

    .line 1859489
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x4f415072

    invoke-static {v2, v1}, LX/02F;->a(II)V

    :goto_0
    return v0

    .line 1859490
    :cond_2
    if-ne v2, v0, :cond_1

    .line 1859491
    const v2, 0x599283fb

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setShowLiveCommentDialogFragment(Z)V
    .locals 0

    .prologue
    .line 1859481
    iput-boolean p1, p0, LX/CDW;->m:Z

    .line 1859482
    return-void
.end method
