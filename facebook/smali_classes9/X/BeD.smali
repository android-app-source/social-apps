.class public final enum LX/BeD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BeD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BeD;

.field public static final enum CATEGORY_PICKER:LX/BeD;

.field public static final enum CITY_PICKER:LX/BeD;

.field public static final enum HOURS_PICKER:LX/BeD;

.field public static final enum LOCATION:LX/BeD;

.field public static final enum PHOTO_PICKER:LX/BeD;

.field public static final enum TEXT:LX/BeD;

.field private static final values:[LX/BeD;


# instance fields
.field private final inputStyle:LX/BeC;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1804668
    new-instance v0, LX/BeD;

    const-string v1, "CATEGORY_PICKER"

    sget-object v2, LX/BeC;->PICKER:LX/BeC;

    invoke-direct {v0, v1, v4, v2}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->CATEGORY_PICKER:LX/BeD;

    .line 1804669
    new-instance v0, LX/BeD;

    const-string v1, "CITY_PICKER"

    sget-object v2, LX/BeC;->PICKER:LX/BeC;

    invoke-direct {v0, v1, v5, v2}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->CITY_PICKER:LX/BeD;

    .line 1804670
    new-instance v0, LX/BeD;

    const-string v1, "HOURS_PICKER"

    sget-object v2, LX/BeC;->PICKER:LX/BeC;

    invoke-direct {v0, v1, v6, v2}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->HOURS_PICKER:LX/BeD;

    .line 1804671
    new-instance v0, LX/BeD;

    const-string v1, "LOCATION"

    sget-object v2, LX/BeC;->LOCATION:LX/BeC;

    invoke-direct {v0, v1, v7, v2}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->LOCATION:LX/BeD;

    .line 1804672
    new-instance v0, LX/BeD;

    const-string v1, "PHOTO_PICKER"

    sget-object v2, LX/BeC;->PICKER:LX/BeC;

    invoke-direct {v0, v1, v8, v2}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->PHOTO_PICKER:LX/BeD;

    .line 1804673
    new-instance v0, LX/BeD;

    const-string v1, "TEXT"

    const/4 v2, 0x5

    sget-object v3, LX/BeC;->INLINE:LX/BeC;

    invoke-direct {v0, v1, v2, v3}, LX/BeD;-><init>(Ljava/lang/String;ILX/BeC;)V

    sput-object v0, LX/BeD;->TEXT:LX/BeD;

    .line 1804674
    const/4 v0, 0x6

    new-array v0, v0, [LX/BeD;

    sget-object v1, LX/BeD;->CATEGORY_PICKER:LX/BeD;

    aput-object v1, v0, v4

    sget-object v1, LX/BeD;->CITY_PICKER:LX/BeD;

    aput-object v1, v0, v5

    sget-object v1, LX/BeD;->HOURS_PICKER:LX/BeD;

    aput-object v1, v0, v6

    sget-object v1, LX/BeD;->LOCATION:LX/BeD;

    aput-object v1, v0, v7

    sget-object v1, LX/BeD;->PHOTO_PICKER:LX/BeD;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BeD;->TEXT:LX/BeD;

    aput-object v2, v0, v1

    sput-object v0, LX/BeD;->$VALUES:[LX/BeD;

    .line 1804675
    invoke-static {}, LX/BeD;->values()[LX/BeD;

    move-result-object v0

    sput-object v0, LX/BeD;->values:[LX/BeD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/BeC;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BeC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1804682
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1804683
    iput-object p3, p0, LX/BeD;->inputStyle:LX/BeC;

    .line 1804684
    return-void
.end method

.method public static fromOrdinal(I)LX/BeD;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1804678
    if-ltz p0, :cond_0

    sget-object v0, LX/BeD;->values:[LX/BeD;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 1804679
    :cond_0
    const/4 v0, 0x0

    .line 1804680
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/BeD;->values:[LX/BeD;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BeD;
    .locals 1

    .prologue
    .line 1804681
    const-class v0, LX/BeD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BeD;

    return-object v0
.end method

.method public static values()[LX/BeD;
    .locals 1

    .prologue
    .line 1804677
    sget-object v0, LX/BeD;->$VALUES:[LX/BeD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BeD;

    return-object v0
.end method


# virtual methods
.method public final getInputStyle()LX/BeC;
    .locals 1

    .prologue
    .line 1804676
    iget-object v0, p0, LX/BeD;->inputStyle:LX/BeC;

    return-object v0
.end method
