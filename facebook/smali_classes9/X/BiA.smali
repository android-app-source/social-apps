.class public final LX/BiA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V
    .locals 0

    .prologue
    .line 1809969
    iput-object p1, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;B)V
    .locals 0

    .prologue
    .line 1809970
    invoke-direct {p0, p1}, LX/BiA;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x6b8d5fc8

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1809971
    iget-object v0, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v0, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->g:LX/Bi2;

    if-eqz v0, :cond_0

    .line 1809972
    const v0, 0x7f0d00de

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 1809973
    invoke-virtual {v6}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    .line 1809974
    iget-object v0, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v0, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a:LX/Bi1;

    iget-object v1, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v1, v1, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1809975
    iget-object v2, v1, LX/Bi8;->f:LX/Bi3;

    move-object v1, v2

    .line 1809976
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v3, v3, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v3}, LX/Bi8;->c()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0d00df

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Bi1;->a(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1809977
    iget-object v0, p0, LX/BiA;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v0, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->g:LX/Bi2;

    invoke-virtual {v6}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/Bi2;->onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;)V

    .line 1809978
    :cond_0
    const v0, 0x617e4e03

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
