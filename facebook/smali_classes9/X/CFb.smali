.class public LX/CFb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1863668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1863669
    check-cast p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    .line 1863670
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1863671
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "campaign_id"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863672
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "campaign_type"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863673
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863674
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863675
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "source"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->g:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863676
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "direct_source"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863677
    iget-object v1, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1863678
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tagged_ids"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    .line 1863679
    const/16 p0, 0x2c

    invoke-static {p0}, LX/0PO;->on(C)LX/0PO;

    move-result-object p0

    invoke-virtual {p0}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object p0

    move-object v4, p0

    .line 1863680
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863681
    :cond_0
    iget-object v1, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1863682
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "photo_ids"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    .line 1863683
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1863684
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1863685
    :cond_1
    const-string v5, ""

    .line 1863686
    :goto_0
    move-object v4, v5

    .line 1863687
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863688
    :cond_2
    iget-object v1, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1863689
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "payload"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863690
    :cond_3
    iget-object v1, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1863691
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "editor_type"

    iget-object v3, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863692
    :cond_4
    const-string v1, "%s/goodwill_videos"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1863693
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    .line 1863694
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1863695
    move-object v1, v2

    .line 1863696
    const-string v2, "POST"

    .line 1863697
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1863698
    move-object v1, v1

    .line 1863699
    const-string v2, "goodwillVideosPost"

    .line 1863700
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1863701
    move-object v1, v1

    .line 1863702
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1863703
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1863704
    move-object v1, v1

    .line 1863705
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1863706
    move-object v0, v1

    .line 1863707
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1863708
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863709
    iget-object v4, v5, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object v5, v4

    .line 1863710
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1863711
    const/16 v5, 0x2c

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1863712
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1863713
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1863714
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
