.class public LX/CaW;
.super LX/CaV;
.source ""


# instance fields
.field private final E:I

.field private final F:F

.field private final G:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private H:LX/9eR;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/view/View;

.field private L:LX/3IP;

.field public y:LX/9eS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1917886
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CaW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917887
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1917884
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CaW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1917885
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1917878
    invoke-direct {p0, p1, p2, p3}, LX/CaV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1917879
    const/16 v0, 0x12c

    iput v0, p0, LX/CaW;->E:I

    .line 1917880
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, LX/CaW;->F:F

    .line 1917881
    new-instance v0, LX/CaT;

    invoke-direct {v0, p0}, LX/CaT;-><init>(LX/CaW;)V

    iput-object v0, p0, LX/CaW;->G:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1917882
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CaW;

    invoke-static {v0}, LX/9eS;->a(LX/0QB;)LX/9eS;

    move-result-object v0

    check-cast v0, LX/9eS;

    iput-object v0, p0, LX/CaW;->y:LX/9eS;

    .line 1917883
    return-void
.end method

.method private a(II)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 1917873
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1917874
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1917875
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1917876
    iget-object v1, p0, LX/CaW;->G:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1917877
    return-object v0
.end method

.method public static getBlindHeight(LX/CaW;)I
    .locals 2

    .prologue
    .line 1917872
    invoke-virtual {p0}, LX/CaW;->getHeight()I

    move-result v0

    invoke-virtual {p0}, LX/CaW;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static setBlindHeight(LX/CaW;I)V
    .locals 1

    .prologue
    .line 1917788
    iget-object v0, p0, LX/CaW;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1917789
    iget-object v0, p0, LX/CaW;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1917790
    iget-object v0, p0, LX/CaW;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1917791
    iget-object v0, p0, LX/CaW;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1917792
    return-void
.end method

.method private v()V
    .locals 1

    .prologue
    .line 1917870
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;

    invoke-direct {v0, p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;-><init>(LX/CaW;)V

    invoke-virtual {p0, v0}, LX/CaW;->post(Ljava/lang/Runnable;)Z

    .line 1917871
    return-void
.end method

.method public static w(LX/CaW;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1917852
    iget-object v0, p0, LX/CaV;->z:Landroid/os/Handler;

    iget-object v1, p0, LX/CaV;->A:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1917853
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1917854
    iput-boolean v2, p0, LX/CaW;->C:Z

    .line 1917855
    invoke-static {p0}, LX/CaW;->getBlindHeight(LX/CaW;)I

    move-result v0

    invoke-direct {p0, v2, v0}, LX/CaW;->a(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1917856
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1917857
    invoke-virtual {p0}, LX/CaW;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 1917858
    invoke-virtual {p0}, LX/CaW;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LX/CaW;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 1917859
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v0

    const/high16 v2, 0x42700000    # 60.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v0

    .line 1917860
    :goto_0
    const/high16 v2, 0x42b40000    # 90.0f

    div-float/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1917861
    iget-object v1, p0, LX/CaW;->L:LX/3IP;

    const/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, p0}, LX/3IP;->a(FILX/3II;)V

    .line 1917862
    :cond_0
    return-void

    .line 1917863
    :cond_1
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1917864
    iget-object v2, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-object v0, v2

    .line 1917865
    iget v2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move v0, v2

    .line 1917866
    iget-object v2, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1917867
    iget-object v3, v2, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-object v2, v3

    .line 1917868
    iget v3, v2, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move v2, v3

    .line 1917869
    add-float/2addr v0, v2

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V
    .locals 0

    .prologue
    .line 1917888
    invoke-super {p0, p1, p2, p3, p4}, LX/CaV;->a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 1917889
    invoke-direct {p0}, LX/CaW;->v()V

    .line 1917890
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1917842
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-eqz v0, :cond_0

    .line 1917843
    iget-boolean v0, p0, LX/CaV;->C:Z

    if-eqz v0, :cond_1

    .line 1917844
    invoke-static {p0}, LX/CaW;->w(LX/CaW;)V

    .line 1917845
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    if-eqz v0, :cond_0

    .line 1917846
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    invoke-interface {v0}, LX/7US;->e()V

    .line 1917847
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1917848
    :cond_1
    invoke-virtual {p0}, LX/CaW;->t()V

    .line 1917849
    iget-object v0, p0, LX/CaV;->D:LX/7US;

    if-eqz v0, :cond_0

    .line 1917850
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1917851
    iget-object v1, p0, LX/CaV;->D:LX/7US;

    invoke-interface {v1, v0, v0}, LX/7US;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1917825
    invoke-super {p0}, LX/CaV;->b()V

    .line 1917826
    new-instance v0, LX/3IP;

    invoke-direct {v0}, LX/3IP;-><init>()V

    iput-object v0, p0, LX/CaW;->L:LX/3IP;

    .line 1917827
    invoke-virtual {p0}, LX/CaW;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1917828
    const v1, 0x7f030a8c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CaW;->I:Landroid/view/View;

    .line 1917829
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917830
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/CaW;->addView(Landroid/view/View;)V

    .line 1917831
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, LX/CaW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CaW;->J:Landroid/view/View;

    .line 1917832
    iget-object v0, p0, LX/CaW;->J:Landroid/view/View;

    const v1, 0x7f0a00d6

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1917833
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x30

    invoke-direct {v0, v3, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1917834
    iget-object v1, p0, LX/CaW;->J:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1917835
    iget-object v0, p0, LX/CaW;->J:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/CaW;->addView(Landroid/view/View;)V

    .line 1917836
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, LX/CaW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CaW;->K:Landroid/view/View;

    .line 1917837
    iget-object v0, p0, LX/CaW;->K:Landroid/view/View;

    const v1, 0x7f0a00d6

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1917838
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x50

    invoke-direct {v0, v3, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1917839
    iget-object v1, p0, LX/CaW;->K:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1917840
    iget-object v0, p0, LX/CaW;->K:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/CaW;->addView(Landroid/view/View;)V

    .line 1917841
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1917820
    invoke-super {p0}, LX/CaV;->f()V

    .line 1917821
    iget-boolean v0, p0, LX/CaV;->B:Z

    if-eqz v0, :cond_0

    .line 1917822
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g()V

    .line 1917823
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917824
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 1917817
    invoke-super {p0}, LX/CaV;->q()V

    .line 1917818
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917819
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 1917810
    invoke-super {p0}, LX/CaV;->r()V

    .line 1917811
    invoke-direct {p0}, LX/CaW;->v()V

    .line 1917812
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-eqz v0, :cond_0

    .line 1917813
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917814
    :cond_0
    new-instance v0, LX/CaU;

    invoke-direct {v0, p0}, LX/CaU;-><init>(LX/CaW;)V

    iput-object v0, p0, LX/CaW;->H:LX/9eR;

    .line 1917815
    iget-object v0, p0, LX/CaW;->y:LX/9eS;

    iget-object v1, p0, LX/CaW;->H:LX/9eR;

    invoke-virtual {v0, v1}, LX/9eS;->a(LX/9eR;)V

    .line 1917816
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 1917807
    invoke-super {p0}, LX/CaV;->s()V

    .line 1917808
    iget-object v0, p0, LX/CaW;->y:LX/9eS;

    iget-object v1, p0, LX/CaW;->H:LX/9eR;

    invoke-virtual {v0, v1}, LX/9eS;->b(LX/9eR;)V

    .line 1917809
    return-void
.end method

.method public final t()V
    .locals 3

    .prologue
    .line 1917793
    invoke-super {p0}, LX/CaV;->t()V

    .line 1917794
    iget-object v0, p0, LX/CaW;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917795
    invoke-static {p0}, LX/CaW;->getBlindHeight(LX/CaW;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/CaW;->a(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1917796
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1917797
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v0

    const/high16 v1, 0x42700000    # 60.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v0

    const v1, 0x3f666666    # 0.9f

    div-float/2addr v0, v1

    .line 1917798
    :goto_0
    iget-object v1, p0, LX/CaW;->L:LX/3IP;

    const/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, p0}, LX/3IP;->a(FILX/3II;)V

    .line 1917799
    return-void

    .line 1917800
    :cond_0
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1917801
    iget-object v1, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-object v0, v1

    .line 1917802
    iget v1, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move v0, v1

    .line 1917803
    iget-object v1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1917804
    iget-object v2, v1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-object v1, v2

    .line 1917805
    iget v2, v1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move v1, v2

    .line 1917806
    add-float/2addr v0, v1

    goto :goto_0
.end method
