.class public LX/Bwt;
.super LX/2oy;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1834488
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bwt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1834489
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1834486
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bwt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834487
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1834473
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834474
    const v0, 0x7f0312b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1834475
    const v0, 0x7f0d2ba5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Bwt;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1834476
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bws;

    invoke-direct {v1, p0, p0}, LX/Bws;-><init>(LX/Bwt;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834477
    return-void
.end method

.method public static a$redex0(LX/Bwt;LX/2qV;)V
    .locals 2

    .prologue
    .line 1834483
    iget-object v1, p0, LX/Bwt;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1834484
    return-void

    .line 1834485
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1834478
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834479
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1834480
    iget-object p1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p1

    .line 1834481
    invoke-static {p0, v0}, LX/Bwt;->a$redex0(LX/Bwt;LX/2qV;)V

    .line 1834482
    return-void
.end method
