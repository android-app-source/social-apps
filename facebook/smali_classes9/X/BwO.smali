.class public final LX/BwO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BwQ;


# direct methods
.method public constructor <init>(LX/BwQ;)V
    .locals 0

    .prologue
    .line 1833629
    iput-object p1, p0, LX/BwO;->a:LX/BwQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6fadec43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833630
    iget-object v1, p0, LX/BwO;->a:LX/BwQ;

    sget-object v2, LX/2ft;->NONE:LX/2ft;

    const/4 v13, 0x1

    .line 1833631
    iget-object v4, v1, LX/BwQ;->A:LX/2pa;

    invoke-static {v4}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v7

    .line 1833632
    new-instance v5, LX/BUL;

    iget-object v4, v1, LX/BwQ;->A:LX/2pa;

    const/4 v6, 0x0

    .line 1833633
    if-nez v4, :cond_4

    .line 1833634
    :cond_0
    :goto_0
    move-object v6, v6

    .line 1833635
    iget-object v4, v1, LX/BwQ;->A:LX/2pa;

    const/4 v8, 0x0

    .line 1833636
    if-nez v4, :cond_5

    .line 1833637
    :cond_1
    :goto_1
    move-object v8, v8

    .line 1833638
    iget-object v9, v1, LX/BwQ;->z:Ljava/lang/String;

    iget-object v4, v1, LX/BwQ;->A:LX/2pa;

    invoke-static {v4}, LX/393;->e(LX/2pa;)J

    move-result-wide v10

    move-object v12, v2

    invoke-direct/range {v5 .. v13}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;Z)V

    .line 1833639
    iget-object v4, v1, LX/BwQ;->A:LX/2pa;

    invoke-static {v4}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1833640
    if-eqz v4, :cond_2

    .line 1833641
    invoke-static {v4, v5}, LX/15V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/BUL;)V

    .line 1833642
    iget-object v6, v1, LX/BwQ;->a:LX/BUA;

    .line 1833643
    iget-object v8, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v8

    .line 1833644
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6, v7, v4}, LX/BUA;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1833645
    :cond_2
    iget-object v4, v1, LX/BwQ;->a:LX/BUA;

    invoke-virtual {v4, v5}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1833646
    new-instance v5, LX/BwL;

    invoke-direct {v5, v1, v7}, LX/BwL;-><init>(LX/BwQ;Ljava/lang/String;)V

    iget-object v6, v1, LX/BwQ;->e:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1833647
    iget-object v4, v1, LX/BwQ;->y:LX/2fs;

    sget-object v5, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    iput-object v5, v4, LX/2fs;->c:LX/1A0;

    .line 1833648
    iget-object v4, v1, LX/BwQ;->y:LX/2fs;

    iput-boolean v13, v4, LX/2fs;->e:Z

    .line 1833649
    iget-object v4, v1, LX/2oy;->j:LX/2pb;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1833650
    invoke-static {v1}, LX/BwQ;->v(LX/BwQ;)V

    .line 1833651
    iget-object v1, p0, LX/BwO;->a:LX/BwQ;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/BwO;->a:LX/BwQ;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    .line 1833652
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 1833653
    sget-object v2, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v1, v2, :cond_3

    .line 1833654
    iget-object v1, p0, LX/BwO;->a:LX/BwQ;

    iget-object v1, v1, LX/2oy;->j:LX/2pb;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v2}, LX/2pb;->b(LX/04g;)V

    .line 1833655
    :cond_3
    iget-object v1, p0, LX/BwO;->a:LX/BwQ;

    iget-object v1, v1, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    .line 1833656
    const v1, -0x436bd6fa

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1833657
    :cond_4
    invoke-static {v4}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    .line 1833658
    if-eqz v8, :cond_0

    .line 1833659
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_0

    .line 1833660
    :cond_5
    invoke-static {v4}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    .line 1833661
    if-eqz v9, :cond_1

    .line 1833662
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1
.end method
