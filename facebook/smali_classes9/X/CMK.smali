.class public LX/CMK;
.super LX/0ht;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private l:Landroid/view/View;

.field public m:Landroid/widget/LinearLayout;

.field public n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/ImageView;

.field private p:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1880452
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 1880453
    iput-object p1, p0, LX/CMK;->a:Landroid/content/Context;

    .line 1880454
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/0ht;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1880455
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setPadding(IIII)V

    .line 1880456
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0ht;->b(F)V

    .line 1880457
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030474

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CMK;->l:Landroid/view/View;

    .line 1880458
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    const v1, 0x7f0d0553

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/CMK;->m:Landroid/widget/LinearLayout;

    .line 1880459
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    const v1, 0x7f0d0d60

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/CMK;->n:Landroid/widget/LinearLayout;

    .line 1880460
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    const v1, 0x7f0d0d61

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CMK;->o:Landroid/widget/ImageView;

    .line 1880461
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    const v1, 0x7f0d0d5f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/CMK;->p:LX/4ob;

    .line 1880462
    if-eqz p2, :cond_0

    .line 1880463
    iget-object v0, p0, LX/CMK;->p:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 1880464
    :cond_0
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 1880465
    iget-object v0, p0, LX/CMK;->l:Landroid/view/View;

    new-instance v1, LX/CMJ;

    invoke-direct {v1, p0}, LX/CMJ;-><init>(LX/CMK;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1880466
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 8

    .prologue
    const/high16 v5, -0x80000000

    .line 1880467
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1880468
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1880469
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 1880470
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 1880471
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1880472
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 1880473
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1880474
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1880475
    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1880476
    iget-object v5, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v5, v3, v4}, LX/5OY;->measure(II)V

    .line 1880477
    iget-object v3, p0, LX/CMK;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0265

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1880478
    iget-object v4, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v4}, LX/5OY;->getMeasuredWidth()I

    move-result v4

    .line 1880479
    iget-object v5, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v5}, LX/5OY;->getMeasuredHeight()I

    move-result v5

    .line 1880480
    iput v4, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1880481
    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1880482
    const/16 v5, 0x53

    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1880483
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 1880484
    iget-object v6, p0, LX/CMK;->o:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 1880485
    iget-object v7, p0, LX/CMK;->o:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    .line 1880486
    sub-int v0, v5, v0

    sub-int/2addr v0, v7

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1880487
    div-int/lit8 v0, v4, 0x2

    sub-int v0, v1, v0

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1880488
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v0, :cond_1

    .line 1880489
    neg-int v0, v3

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1880490
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CMK;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1880491
    iget v2, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v1, v2

    div-int/lit8 v2, v6, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1880492
    iget-object v1, p0, LX/CMK;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1880493
    return-void

    .line 1880494
    :cond_1
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v0, v4

    sub-int/2addr v0, v3

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v0, v5, :cond_0

    .line 1880495
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v0, v4

    add-int/2addr v0, v3

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_0
.end method
