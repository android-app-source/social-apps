.class public final LX/Bs5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 1826955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826956
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Bs5;->a:Ljava/lang/ref/WeakReference;

    .line 1826957
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Bs5;->b:Ljava/lang/ref/WeakReference;

    .line 1826958
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1826959
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1826960
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1826961
    iget-object v0, p0, LX/Bs5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826962
    iget-object v1, p0, LX/Bs5;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pq;

    .line 1826963
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1826964
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1826965
    if-nez v2, :cond_1

    .line 1826966
    :cond_0
    :goto_0
    return-void

    .line 1826967
    :cond_1
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1826968
    check-cast v2, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1826969
    new-instance v5, LX/33P;

    invoke-direct {v5, v2}, LX/33P;-><init>(Lcom/facebook/graphql/model/GraphQLProfile;)V

    move-object v3, v1

    .line 1826970
    check-cast v3, LX/1Pr;

    .line 1826971
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1826972
    check-cast v4, LX/0jW;

    invoke-interface {v3, v5, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1826973
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    if-eq v4, v3, :cond_0

    move-object v3, v1

    .line 1826974
    check-cast v3, LX/1Pr;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-interface {v3, v5, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1826975
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
