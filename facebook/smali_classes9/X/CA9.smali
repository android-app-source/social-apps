.class public LX/CA9;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/9A2;


# instance fields
.field public a:Lcom/facebook/widget/CyclingTextSwitcher;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1854967
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1854968
    const v0, 0x7f030c63

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1854969
    const v0, 0x7f0d1e6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CA9;->b:Landroid/view/View;

    .line 1854970
    return-void
.end method

.method public static c(LX/CA9;)V
    .locals 1

    .prologue
    .line 1854977
    iget-object v0, p0, LX/CA9;->a:Lcom/facebook/widget/CyclingTextSwitcher;

    if-nez v0, :cond_0

    .line 1854978
    const v0, 0x7f0d1e79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CyclingTextSwitcher;

    iput-object v0, p0, LX/CA9;->a:Lcom/facebook/widget/CyclingTextSwitcher;

    .line 1854979
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1854976
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1854975
    return-void
.end method

.method public setAnimated(Z)V
    .locals 1

    .prologue
    .line 1854980
    invoke-static {p0}, LX/CA9;->c(LX/CA9;)V

    .line 1854981
    iget-object v0, p0, LX/CA9;->a:Lcom/facebook/widget/CyclingTextSwitcher;

    .line 1854982
    iput-boolean p1, v0, Lcom/facebook/widget/CyclingTextSwitcher;->a:Z

    .line 1854983
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1854973
    iget-object v0, p0, LX/CA9;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1854974
    return-void
.end method

.method public setProgress(I)V
    .locals 0

    .prologue
    .line 1854972
    return-void
.end method

.method public setStoryIsWaitingForWifi(Z)V
    .locals 0

    .prologue
    .line 1854971
    return-void
.end method
