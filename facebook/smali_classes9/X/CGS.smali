.class public LX/CGS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1865541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1865537
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1865538
    if-nez v0, :cond_0

    .line 1865539
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1865540
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 1865533
    invoke-static {p0}, LX/CGS;->a(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1865534
    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1865535
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1865536
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 1

    .prologue
    .line 1865525
    invoke-static {p0, p1}, LX/CGS;->c(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865526
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->a()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotation(F)V

    .line 1865527
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->l()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 1865528
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->m()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    .line 1865529
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->b()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1865530
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->a(Landroid/view/View;F)V

    .line 1865531
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->b(Landroid/view/View;F)V

    .line 1865532
    return-void
.end method

.method public static b(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 1865521
    invoke-static {p0}, LX/CGS;->a(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1865522
    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1865523
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1865524
    return-void
.end method

.method public static b(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 1

    .prologue
    .line 1865505
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    if-eqz v0, :cond_0

    .line 1865506
    invoke-static {p0, p1}, LX/CGS;->c(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865507
    :cond_0
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 1865508
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->a()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotation(F)V

    .line 1865509
    :cond_1
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    if-eqz v0, :cond_2

    .line 1865510
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->l()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 1865511
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->m()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    .line 1865512
    :cond_2
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 1865513
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->b()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1865514
    :cond_3
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    if-eqz v0, :cond_4

    .line 1865515
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->a(Landroid/view/View;F)V

    .line 1865516
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->b(Landroid/view/View;F)V

    .line 1865517
    :cond_4
    return-void
.end method

.method private static c(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 1

    .prologue
    .line 1865518
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->j()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setX(F)V

    .line 1865519
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->k()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setY(F)V

    .line 1865520
    return-void
.end method
