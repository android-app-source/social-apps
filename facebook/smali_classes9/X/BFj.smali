.class public LX/BFj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89e;


# instance fields
.field public final a:LX/BFg;

.field public final b:LX/9d6;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/8GL;

.field public final e:LX/9c7;

.field public final f:LX/89Z;

.field public final g:LX/B4P;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final h:LX/8GP;

.field public i:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/9d5;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/Jvf;

.field public o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field public q:I

.field public r:Z

.field public s:Landroid/view/ViewParent;

.field public t:Landroid/view/View$OnClickListener;

.field public final u:LX/9bz;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/9c7;Ljava/lang/String;LX/89Z;LX/B4P;LX/9d6;LX/0Or;LX/8GP;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9c7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/89Z;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/B4P;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            "Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;",
            "LX/9c7;",
            "Ljava/lang/String;",
            "LX/89Z;",
            "Lcom/facebook/ipc/creativecam/controller/CreativeCamSwipeableFramesLogController;",
            "LX/9d6;",
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;",
            "LX/8GP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766242
    new-instance v0, LX/BFg;

    invoke-direct {v0, p0}, LX/BFg;-><init>(LX/BFj;)V

    iput-object v0, p0, LX/BFj;->a:LX/BFg;

    .line 1766243
    new-instance v0, LX/BFi;

    invoke-direct {v0, p0}, LX/BFi;-><init>(LX/BFj;)V

    iput-object v0, p0, LX/BFj;->u:LX/9bz;

    .line 1766244
    iput-object p3, p0, LX/BFj;->e:LX/9c7;

    .line 1766245
    iput-object p5, p0, LX/BFj;->f:LX/89Z;

    .line 1766246
    iput-object p6, p0, LX/BFj;->g:LX/B4P;

    .line 1766247
    iput-object p4, p0, LX/BFj;->j:Ljava/lang/String;

    .line 1766248
    iput-object p7, p0, LX/BFj;->b:LX/9d6;

    .line 1766249
    iput-object p8, p0, LX/BFj;->c:LX/0Or;

    .line 1766250
    iput-object p9, p0, LX/BFj;->h:LX/8GP;

    .line 1766251
    if-eqz p2, :cond_0

    new-instance v0, LX/8GL;

    invoke-direct {v0, p2}, LX/8GL;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)V

    :goto_0
    iput-object v0, p0, LX/BFj;->d:LX/8GL;

    .line 1766252
    return-void

    .line 1766253
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d$redex0(LX/BFj;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1766254
    iget-object v0, p0, LX/BFj;->h:LX/8GP;

    iget v1, p0, LX/BFj;->p:I

    iget v2, p0, LX/BFj;->q:I

    invoke-virtual {v0, v1, v2}, LX/8GP;->a(II)LX/8GO;

    move-result-object v0

    .line 1766255
    iget-boolean v1, p0, LX/BFj;->r:Z

    if-nez v1, :cond_0

    .line 1766256
    invoke-virtual {v0}, LX/8GO;->a()LX/8GO;

    .line 1766257
    :cond_0
    iget-object v1, p0, LX/BFj;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/8GO;->c(LX/0Px;)LX/8GO;

    move-result-object v0

    invoke-virtual {v0}, LX/8GO;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1766196
    iget-object v0, p0, LX/BFj;->k:LX/9d5;

    if-nez v0, :cond_0

    .line 1766197
    const/4 v0, 0x0

    .line 1766198
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BFj;->k:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IIIZ)V
    .locals 10

    .prologue
    .line 1766199
    iput p1, p0, LX/BFj;->p:I

    .line 1766200
    iput p2, p0, LX/BFj;->q:I

    .line 1766201
    iput-boolean p4, p0, LX/BFj;->r:Z

    .line 1766202
    iget-object v0, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-eqz v0, :cond_0

    .line 1766203
    iget-object v0, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1766204
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 1766205
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 1766206
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p3, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1766207
    iget-object v1, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1766208
    iget-object v0, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->requestLayout()V

    .line 1766209
    :cond_0
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 1766210
    iget v4, p0, LX/BFj;->p:I

    if-lez v4, :cond_1

    iget v4, p0, LX/BFj;->q:I

    if-gtz v4, :cond_2

    .line 1766211
    :cond_1
    :goto_0
    return-void

    .line 1766212
    :cond_2
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/BFj;->l:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-nez v4, :cond_8

    .line 1766213
    :cond_3
    iget-object v4, p0, LX/BFj;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iput-object v4, p0, LX/BFj;->l:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1766214
    iget-object v4, p0, LX/BFj;->b:LX/9d6;

    iget-object v5, p0, LX/BFj;->u:LX/9bz;

    iget-object v6, p0, LX/BFj;->l:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const-string v8, "-1"

    invoke-virtual/range {v4 .. v9}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v4

    iput-object v4, p0, LX/BFj;->k:LX/9d5;

    .line 1766215
    :goto_1
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget v6, p0, LX/BFj;->p:I

    iget v8, p0, LX/BFj;->q:I

    invoke-virtual {v4, v5, v6, v8, v9}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1766216
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->a:LX/BFg;

    .line 1766217
    iput-object v5, v4, LX/9d5;->H:LX/BFg;

    .line 1766218
    iget-object v4, p0, LX/BFj;->s:Landroid/view/ViewParent;

    if-eqz v4, :cond_4

    .line 1766219
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->s:Landroid/view/ViewParent;

    invoke-virtual {v4, v9, v5}, LX/9d5;->a(ZLandroid/view/ViewParent;)V

    .line 1766220
    :cond_4
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, LX/9d5;->a(Landroid/view/View$OnClickListener;)V

    .line 1766221
    iget-boolean v4, p0, LX/BFj;->r:Z

    if-nez v4, :cond_5

    .line 1766222
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    .line 1766223
    iget-object v5, v4, LX/9d5;->o:LX/8GP;

    iget v6, v4, LX/9d5;->u:I

    iget v8, v4, LX/9d5;->v:I

    invoke-virtual {v5, v6, v8}, LX/8GP;->a(II)LX/8GO;

    move-result-object v5

    iget-object v6, v4, LX/9d5;->F:LX/0Px;

    invoke-virtual {v5, v6}, LX/8GO;->a(LX/0Px;)LX/8GO;

    move-result-object v5

    invoke-virtual {v5}, LX/8GO;->a()LX/8GO;

    move-result-object v5

    invoke-virtual {v5}, LX/8GO;->b()LX/0Px;

    move-result-object v5

    .line 1766224
    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1766225
    :cond_5
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->n:LX/Jvf;

    .line 1766226
    iget-object v6, v4, LX/9d5;->L:LX/9dI;

    .line 1766227
    iput-object v5, v6, LX/9dI;->g:LX/Jvf;

    .line 1766228
    iget-object v4, p0, LX/BFj;->i:LX/0Px;

    if-eqz v4, :cond_6

    iget-object v4, p0, LX/BFj;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1766229
    invoke-static {p0}, LX/BFj;->d$redex0(LX/BFj;)LX/0Px;

    move-result-object v4

    iput-object v4, p0, LX/BFj;->m:LX/0Px;

    .line 1766230
    iget-object v5, p0, LX/BFj;->k:LX/9d5;

    iget-object v6, p0, LX/BFj;->i:LX/0Px;

    iget-object v4, p0, LX/BFj;->j:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, LX/BFj;->j:Ljava/lang/String;

    :goto_2
    invoke-virtual {v5, v6, v7, v4}, LX/9d5;->a(LX/0Px;LX/8G6;Ljava/lang/String;)V

    .line 1766231
    :cond_6
    iget-object v4, p0, LX/BFj;->d:LX/8GL;

    if-eqz v4, :cond_7

    .line 1766232
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    iget-object v5, p0, LX/BFj;->d:LX/8GL;

    .line 1766233
    iget-object v6, v5, LX/8GL;->b:LX/8GH;

    move-object v5, v6

    .line 1766234
    invoke-virtual {v4, v5}, LX/9d5;->a(LX/8GH;)V

    .line 1766235
    :cond_7
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    invoke-virtual {v4, v9}, LX/9d5;->b(Z)V

    goto/16 :goto_0

    .line 1766236
    :cond_8
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    invoke-virtual {v4}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LX/BFj;->j:Ljava/lang/String;

    .line 1766237
    iget-object v4, p0, LX/BFj;->k:LX/9d5;

    invoke-virtual {v4}, LX/9d5;->m()V

    goto/16 :goto_1

    .line 1766238
    :cond_9
    iget-object v4, p0, LX/BFj;->m:LX/0Px;

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1766239
    iget-object v8, v4, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v4, v8

    .line 1766240
    goto :goto_2
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1766181
    iget-object v0, p0, LX/BFj;->i:LX/0Px;

    if-nez v0, :cond_0

    .line 1766182
    iput-object p1, p0, LX/BFj;->i:LX/0Px;

    .line 1766183
    :cond_0
    new-instance v0, LX/BFh;

    invoke-direct {v0, p0}, LX/BFh;-><init>(LX/BFj;)V

    .line 1766184
    iget-object v1, p0, LX/BFj;->k:LX/9d5;

    if-eqz v1, :cond_1

    .line 1766185
    iget-object v1, p0, LX/BFj;->k:LX/9d5;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, v2}, LX/9d5;->b(LX/0Px;LX/8G6;Ljava/lang/String;)V

    .line 1766186
    :cond_1
    return-void
.end method

.method public final a(LX/Jvf;)V
    .locals 0

    .prologue
    .line 1766194
    iput-object p1, p0, LX/BFj;->n:LX/Jvf;

    .line 1766195
    return-void
.end method

.method public final a(Landroid/view/View;LX/0Px;)V
    .locals 1
    .param p2    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1766187
    instance-of v0, p1, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    if-nez v0, :cond_0

    .line 1766188
    :goto_0
    return-void

    .line 1766189
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v0, p0, LX/BFj;->o:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1766190
    iput-object p2, p0, LX/BFj;->i:LX/0Px;

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1766191
    iget-object v0, p0, LX/BFj;->k:LX/9d5;

    if-nez v0, :cond_0

    .line 1766192
    const/4 v0, 0x0

    .line 1766193
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BFj;->k:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->l()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
