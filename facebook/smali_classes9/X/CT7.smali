.class public final LX/CT7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/CTC;


# direct methods
.method public constructor <init>(LX/CTC;)V
    .locals 0

    .prologue
    .line 1895302
    iput-object p1, p0, LX/CT7;->a:LX/CTC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;
    .locals 1

    .prologue
    .line 1895303
    iget-object v0, p0, LX/CT7;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->e:LX/CSc;

    .line 1895304
    iget-object p0, v0, LX/CSc;->c:Ljava/lang/String;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895305
    iget-object p0, v0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1895306
    iget-object p0, v0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CSY;

    .line 1895307
    :goto_0
    move-object v0, p0

    .line 1895308
    return-object v0

    .line 1895309
    :cond_0
    iget-object p0, v0, LX/CSc;->b:LX/CSb;

    iget-object p0, p0, LX/CSb;->a:Ljava/util/HashMap;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/CSc;->b:LX/CSb;

    iget-object p0, p0, LX/CSb;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/CSc;->b:LX/CSb;

    iget-object p0, p0, LX/CSb;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1895310
    iget-object p0, v0, LX/CSc;->b:LX/CSb;

    iget-object p0, p0, LX/CSb;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CSY;

    goto :goto_0

    .line 1895311
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V
    .locals 2

    .prologue
    .line 1895296
    iget-object v0, p0, LX/CT7;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->e:LX/CSc;

    .line 1895297
    iget-object v1, v0, LX/CSc;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895298
    iget-object v1, v0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1895299
    iget-object v1, v0, LX/CSc;->a:Ljava/util/HashMap;

    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, p2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895300
    :cond_0
    iget-object v1, v0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895301
    return-void
.end method
