.class public final LX/CJW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CJ7;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attribution/SampleContentReplyFragment;)V
    .locals 0

    .prologue
    .line 1875480
    iput-object p1, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1875481
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->m:LX/CJL;

    iget-object v1, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v1, v1, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v1, v1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    .line 1875482
    const-string v2, "cancel_inline_reply_dialog_event"

    const-string v3, "sample_content"

    invoke-static {v0, v2, v1, v3}, LX/CJL;->a(LX/CJL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1875483
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->r:LX/CJX;

    if-eqz v0, :cond_0

    .line 1875484
    :cond_0
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    invoke-virtual {v0}, LX/3if;->a()V

    .line 1875485
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1875486
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->m:LX/CJL;

    iget-object v1, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v1, v1, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v1, v1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    .line 1875487
    const-string v2, "send_inline_reply_dialog_event"

    const-string v3, "sample_content"

    invoke-static {v0, v2, v1, v3}, LX/CJL;->a(LX/CJL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1875488
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->r:LX/CJX;

    if-eqz v0, :cond_0

    .line 1875489
    :cond_0
    iget-object v0, p0, LX/CJW;->a:Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1875490
    return-void
.end method
