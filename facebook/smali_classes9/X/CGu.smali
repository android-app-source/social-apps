.class public LX/CGu;
.super LX/CGt;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CGt",
        "<",
        "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;",
        ">;",
        "LX/0Ya;"
    }
.end annotation


# instance fields
.field public c:LX/8bK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1865925
    invoke-direct {p0, p1, p2}, LX/CGt;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1865926
    iput-object p1, p0, LX/CGu;->f:Landroid/content/Context;

    .line 1865927
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CGu;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v1

    check-cast v1, LX/8bK;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p2

    check-cast p2, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v1, p0, LX/CGu;->c:LX/8bK;

    iput-object p2, p0, LX/CGu;->d:LX/0ad;

    iput-object v0, p0, LX/CGu;->e:LX/0Uh;

    .line 1865928
    return-void
.end method

.method public static t(LX/CGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1865929
    iget-object v1, p0, LX/CGu;->d:LX/0ad;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/CGu;->d:LX/0ad;

    sget-short v2, LX/2yD;->k:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static u(LX/CGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1865930
    iget-object v1, p0, LX/CGu;->d:LX/0ad;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/CGu;->d:LX/0ad;

    sget-short v2, LX/2yD;->l:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method private w()I
    .locals 1

    .prologue
    .line 1865931
    iget-object v0, p0, LX/CGu;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1865932
    new-instance v0, LX/B5T;

    invoke-direct {v0}, LX/B5T;-><init>()V

    move-object v0, v0

    .line 1865933
    const-string v1, "articleID"

    .line 1865934
    iget-object v2, p0, LX/CGt;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1865935
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "maxAuthors"

    .line 1865936
    iget v3, p0, LX/CGt;->d:I

    move v3, v3

    .line 1865937
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "maxElements"

    .line 1865938
    iget v3, p0, LX/CGt;->e:I

    move v3, v3

    .line 1865939
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "maxListElements"

    .line 1865940
    iget v3, p0, LX/CGt;->f:I

    move v3, v3

    .line 1865941
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "maxSlideshowMedia"

    .line 1865942
    iget v3, p0, LX/CGt;->g:I

    move v3, v3

    .line 1865943
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    .line 1865944
    iget-object v3, p0, LX/CGt;->h:LX/0wF;

    move-object v3, v3

    .line 1865945
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    .line 1865946
    iget-object v3, p0, LX/CGt;->i:LX/0wC;

    move-object v3, v3

    .line 1865947
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "numFeaturedArticlesToFetch"

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "numRelatedArticlesToFetch"

    invoke-virtual {p0}, LX/CGt;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "relatedArticleImageWidth"

    .line 1865948
    iget v3, p0, LX/CGt;->m:I

    move v3, v3

    .line 1865949
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "relatedArticleImageHeight"

    .line 1865950
    iget v3, p0, LX/CGt;->n:I

    move v3, v3

    .line 1865951
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "final_image_width"

    .line 1865952
    iget-object v3, p0, LX/CGu;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1865953
    invoke-static {p0}, LX/CGu;->u(LX/CGu;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0}, LX/CGu;->t(LX/CGu;)Z

    move-result v4

    if-nez v4, :cond_3

    :goto_0
    move v3, v3

    .line 1865954
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "final_image_height"

    .line 1865955
    iget v3, p0, LX/CGt;->o:I

    move v3, v3

    .line 1865956
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_intermediate_image"

    .line 1865957
    invoke-static {p0}, LX/CGu;->u(LX/CGu;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p0}, LX/CGu;->t(LX/CGu;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 1865958
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "intermediate_image_width"

    invoke-direct {p0}, LX/CGu;->w()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "logoSizeType"

    const/4 v3, 0x0

    .line 1865959
    iget-object v4, p0, LX/CGu;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    .line 1865960
    iget-object v5, p0, LX/CGu;->c:LX/8bK;

    invoke-virtual {v5}, LX/8bK;->a()LX/0p3;

    move-result-object v5

    .line 1865961
    sget-object v6, LX/0p3;->POOR:LX/0p3;

    if-ne v5, v6, :cond_0

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    iget-object v4, p0, LX/CGu;->e:LX/0Uh;

    const/16 v5, 0xb2

    invoke-virtual {v4, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    .line 1865962
    :cond_0
    if-eqz v3, :cond_5

    const-string v3, "ARTICLE_LOGO"

    :goto_2
    move-object v3, v3

    .line 1865963
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1865964
    invoke-direct {p0}, LX/CGu;->w()I

    move-result v1

    const/16 v2, 0x3e8

    if-ge v1, v2, :cond_1

    .line 1865965
    const-string v1, "video_cover_image_width"

    invoke-direct {p0}, LX/CGu;->w()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "video_cover_image_height"

    .line 1865966
    iget v3, p0, LX/CGt;->o:I

    move v3, v3

    .line 1865967
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1865968
    :cond_1
    iget-object v1, p0, LX/CGu;->e:LX/0Uh;

    const/16 v2, 0x70

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1865969
    const-string v1, "preferred_quality"

    const-string v2, "HD"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "preferred_scrubbing"

    const-string v3, "MPEG_DASH"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "playlist_scrubbing"

    const-string v3, "MPEG_DASH"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1865970
    :cond_2
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1865971
    iget-wide v4, p0, LX/CGt;->j:J

    move-wide v2, v4

    .line 1865972
    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1865973
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1865974
    move-object v0, v0

    .line 1865975
    iget-object v1, p0, LX/CGt;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 1865976
    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 1865977
    iget-object v1, p0, LX/CGt;->k:LX/0zS;

    move-object v1, v1

    .line 1865978
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1865979
    return-object v0

    :cond_3
    const/16 v3, 0x9c4

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_5
    const-string v3, "ARTICLE_LOGO_2X"

    goto :goto_2
.end method
