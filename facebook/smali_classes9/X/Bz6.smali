.class public final LX/Bz6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bz7;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Bz7;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1838167
    iput-object p1, p0, LX/Bz6;->a:LX/Bz7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838168
    iput-object p2, p0, LX/Bz6;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838169
    iput-boolean p3, p0, LX/Bz6;->c:Z

    .line 1838170
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838171
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1838172
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "attachment:text"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/6Bx;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "description"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1838173
    iput-object v0, p0, LX/Bz6;->d:Ljava/lang/String;

    .line 1838174
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1838160
    new-instance v0, LX/1yT;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, LX/Bz6;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v3, p0, LX/Bz6;->c:Z

    .line 1838161
    :try_start_0
    iget-object v4, p0, LX/Bz6;->a:LX/Bz7;

    iget-object v4, v4, LX/Bz7;->e:LX/1nA;

    invoke-virtual {v4, v2, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1838162
    :goto_0
    move-object v2, v4

    .line 1838163
    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v0

    .line 1838164
    :catch_0
    iget-object v4, p0, LX/Bz6;->a:LX/Bz7;

    iget-object v4, v4, LX/Bz7;->f:LX/03V;

    sget-object v5, LX/Bz7;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Corrupt data. Can\'t linkify description"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1838165
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1838166
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838159
    iget-object v0, p0, LX/Bz6;->d:Ljava/lang/String;

    return-object v0
.end method
