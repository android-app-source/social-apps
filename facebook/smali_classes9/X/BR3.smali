.class public final LX/BR3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V
    .locals 0

    .prologue
    .line 1783139
    iput-object p1, p0, LX/BR3;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1783140
    iget-object v0, p0, LX/BR3;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    const-string v1, "timeline_staging_ground"

    const-string v2, "Failed to fetch best available picture"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1783141
    iget-object v0, p0, LX/BR3;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-static {v0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->l(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    .line 1783142
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1783143
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1783144
    if-eqz p1, :cond_0

    .line 1783145
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1783146
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1783147
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "result is null or has no image or no uri"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/BR3;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1783148
    :goto_2
    return-void

    .line 1783149
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1783150
    check-cast v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1783151
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1783152
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1783153
    check-cast v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1783154
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_1

    .line 1783155
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1783156
    check-cast v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/BR3;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v3, LX/BR1;->a:Landroid/net/Uri;

    .line 1783157
    iget-object v0, p0, LX/BR3;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-static {v0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->l(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    goto :goto_2
.end method
