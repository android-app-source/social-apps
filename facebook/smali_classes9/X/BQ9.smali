.class public LX/BQ9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hx;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0kv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Zm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/BPC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/13x;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Z

.field private final k:LX/0Zh;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1781609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781610
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1781611
    iput-object v0, p0, LX/BQ9;->c:LX/0Ot;

    .line 1781612
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    iput-object v0, p0, LX/BQ9;->k:LX/0Zh;

    .line 1781613
    return-void
.end method

.method public static a(LX/0QB;)LX/BQ9;
    .locals 12

    .prologue
    .line 1781614
    const-class v1, LX/BQ9;

    monitor-enter v1

    .line 1781615
    :try_start_0
    sget-object v0, LX/BQ9;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1781616
    sput-object v2, LX/BQ9;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1781617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1781618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1781619
    new-instance v3, LX/BQ9;

    invoke-direct {v3}, LX/BQ9;-><init>()V

    .line 1781620
    const/16 v4, 0x91

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 v6, 0x97

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v8

    check-cast v8, LX/0kv;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v9

    check-cast v9, LX/0Zm;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v10

    check-cast v10, LX/0kb;

    invoke-static {v0}, LX/BPC;->a(LX/0QB;)LX/BPC;

    move-result-object v11

    check-cast v11, LX/BPC;

    invoke-static {v0}, LX/13x;->a(LX/0QB;)LX/13x;

    move-result-object p0

    check-cast p0, LX/13x;

    .line 1781621
    iput-object v4, v3, LX/BQ9;->a:LX/0Or;

    iput-object v5, v3, LX/BQ9;->b:LX/0Zb;

    iput-object v6, v3, LX/BQ9;->c:LX/0Ot;

    iput-object v7, v3, LX/BQ9;->d:Landroid/content/Context;

    iput-object v8, v3, LX/BQ9;->e:LX/0kv;

    iput-object v9, v3, LX/BQ9;->f:LX/0Zm;

    iput-object v10, v3, LX/BQ9;->g:LX/0kb;

    iput-object v11, v3, LX/BQ9;->h:LX/BPC;

    iput-object p0, v3, LX/BQ9;->i:LX/13x;

    .line 1781622
    move-object v0, v3

    .line 1781623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1781624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BQ9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1781625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1781626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1781627
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "profile_intro_card_event"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781628
    :goto_0
    return-object v4

    .line 1781629
    :cond_0
    const-string v1, "profile_intro_card_event"

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1781630
    const-string v0, "event_type"

    invoke-virtual {v4, v0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781631
    const-string v0, "session_id"

    invoke-virtual {v4, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4
    .param p2    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1781632
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "timeline"

    .line 1781633
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1781634
    move-object v0, v0

    .line 1781635
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-object v1, p0, LX/BQ9;->e:LX/0kv;

    iget-object v2, p0, LX/BQ9;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1781636
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1781637
    move-object v0, v0

    .line 1781638
    if-eqz p4, :cond_0

    .line 1781639
    iput-object p4, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1781640
    :cond_0
    if-eqz p5, :cond_1

    sget-object v1, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq p5, v1, :cond_1

    .line 1781641
    const-string v1, "relationship_type"

    invoke-virtual {p5}, LX/9lQ;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781642
    :cond_1
    return-object v0
.end method

.method private static a(LX/BQ9;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1781643
    iget-object v1, p0, LX/BQ9;->k:LX/0Zh;

    invoke-virtual {v1}, LX/0Zh;->c()LX/0zL;

    move-result-object v1

    .line 1781644
    :try_start_0
    iget-object v2, p0, LX/BQ9;->i:LX/13x;

    invoke-virtual {v2, v1}, LX/13x;->a(LX/0zL;)V

    .line 1781645
    invoke-virtual {v1}, LX/0zL;->j()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1781646
    invoke-virtual {v1}, LX/0nA;->a()V

    :goto_0
    return-object v0

    .line 1781647
    :cond_0
    :try_start_1
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0nA;->a(LX/0nD;)V

    .line 1781648
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 1781649
    invoke-virtual {v1, v2}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 1781650
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1781651
    invoke-virtual {v1}, LX/0nA;->a()V

    goto :goto_0

    :catch_0
    invoke-virtual {v1}, LX/0nA;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method

.method private a(LX/3zI;)V
    .locals 2

    .prologue
    .line 1781652
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, LX/BQ9;->a(LX/3zI;J)V

    .line 1781653
    return-void
.end method

.method private a(LX/3zI;J)V
    .locals 4

    .prologue
    .line 1781692
    iget-object v0, p0, LX/BQ9;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 1781693
    const-string v0, "timeline"

    iget-object v1, p0, LX/BQ9;->e:LX/0kv;

    iget-object v2, p0, LX/BQ9;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, p2, p3}, LX/0hx;->a(LX/3zI;Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781694
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781695
    return-void
.end method

.method public static a(LX/BQ9;Lcom/facebook/analytics/logger/HoneyClientEvent;JLX/9lQ;)V
    .locals 2

    .prologue
    .line 1781654
    const-string v0, "profile_id"

    invoke-virtual {p1, v0, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781655
    if-eqz p4, :cond_0

    sget-object v0, LX/9lQ;->UNDEFINED:LX/9lQ;

    if-eq p4, v0, :cond_0

    .line 1781656
    const-string v0, "relationship_type"

    invoke-virtual {p4}, LX/9lQ;->getValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781657
    :cond_0
    iget-object v0, p0, LX/BQ9;->e:LX/0kv;

    iget-object v1, p0, LX/BQ9;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1781658
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1781659
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;I)V
    .locals 4

    .prologue
    .line 1781660
    const-string v0, "event_metadata"

    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "photos_number"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781661
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V
    .locals 2

    .prologue
    .line 1781662
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v0

    .line 1781663
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1781664
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 1781665
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1781666
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v0

    .line 1781667
    if-eqz v0, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    if-eq v0, v1, :cond_1

    .line 1781668
    const-string v1, "context_item_type"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781669
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v0

    .line 1781670
    if-eqz v0, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    if-eq v0, v1, :cond_2

    .line 1781671
    const-string v1, "context_item_target_type"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781672
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1781673
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "profile_get_notified_event"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781674
    :goto_0
    return-void

    .line 1781675
    :cond_0
    const-string v1, "profile_get_notified_event"

    move-object v0, p0

    move-wide v2, p2

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1781676
    packed-switch p1, :pswitch_data_0

    .line 1781677
    const-string v0, "get_notified_notif_interrupetd"

    .line 1781678
    :goto_1
    const-string v2, "event_type"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781679
    iget-object v0, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1781680
    :pswitch_0
    const-string v0, "get_notified_notif_success"

    goto :goto_1

    .line 1781681
    :pswitch_1
    const-string v0, "get_notified_notif_failure"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(JI)V
    .locals 7

    .prologue
    .line 1781682
    const/4 v4, 0x0

    sget-object v5, LX/9lQ;->SELF:LX/9lQ;

    const-string v6, "fav_photos_add_prompt_suggested_click"

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781683
    if-eqz v0, :cond_0

    .line 1781684
    invoke-static {v0, p3}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;I)V

    .line 1781685
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781686
    :cond_0
    return-void
.end method

.method public final a(JLX/9lQ;I)V
    .locals 7

    .prologue
    .line 1781687
    const/4 v4, 0x0

    const-string v6, "instagram_links_impression"

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781688
    if-eqz v0, :cond_0

    .line 1781689
    const-string v1, "instagram_links_count"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781690
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781691
    :cond_0
    return-void
.end method

.method public final a(JLX/9lQ;II)V
    .locals 7

    .prologue
    .line 1781599
    const/4 v4, 0x0

    const-string v6, "links_impression"

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781600
    if-eqz v0, :cond_0

    .line 1781601
    const-string v1, "links_count"

    const-string v2, "websites: %1$d, accounts: %2$d"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781602
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781603
    :cond_0
    return-void
.end method

.method public final a(JLX/9lQ;IJI)V
    .locals 7

    .prologue
    .line 1781501
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "fav_photos_vpvd"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781502
    :goto_0
    return-void

    .line 1781503
    :cond_0
    const-string v1, "fav_photos_vpvd"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781504
    const-string v1, "vpvd_time_delta"

    invoke-virtual {v0, v1, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781505
    const-string v1, "intro_card_photo_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781506
    const-string v1, "image_loading_state"

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781507
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V
    .locals 7

    .prologue
    .line 1781604
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "protile_header_click"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781605
    :goto_0
    return-void

    .line 1781606
    :cond_0
    const-string v1, "protile_header_click"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781607
    const-string v1, "protile_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781608
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;IIJI)V
    .locals 9

    .prologue
    .line 1781488
    iget-object v2, p0, LX/BQ9;->f:LX/0Zm;

    const-string v3, "protiles_vpv_duration"

    invoke-virtual {v2, v3}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1781489
    :goto_0
    return-void

    .line 1781490
    :cond_0
    const-string v3, "protiles_vpv_duration"

    const/4 v6, 0x0

    move-object v2, p0

    move-wide v4, p1

    move-object v7, p3

    invoke-static/range {v2 .. v7}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1781491
    const-string v3, "protile_type"

    invoke-virtual {v2, v3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781492
    const-string v3, "vpvd_time_delta"

    move-wide/from16 v0, p7

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781493
    const-string v3, "image_loading_state"

    move/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781494
    const-string v3, "is_profile_cached"

    iget-object v4, p0, LX/BQ9;->h:LX/BPC;

    invoke-virtual {v4}, LX/BPC;->e()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781495
    const-string v3, "enabled_features"

    invoke-static {p0}, LX/BQ9;->a(LX/BQ9;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781496
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p4, v3, :cond_2

    .line 1781497
    const-string v3, "protile_mutual_friend_counts"

    invoke-virtual {v2, v3, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781498
    :cond_1
    :goto_1
    iget-object v3, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1781499
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p4, v3, :cond_1

    .line 1781500
    const-string v3, "protile_photo_counts"

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1
.end method

.method public final a(JLX/9lQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1781508
    const/4 v4, 0x0

    const-string v6, "instagram_links_click"

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781509
    if-eqz v0, :cond_0

    .line 1781510
    const-string v1, "external_link_title"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781511
    const-string v1, "external_link_destination"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781512
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781513
    :cond_0
    return-void
.end method

.method public final a(JLX/9lQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1781514
    const/4 v4, 0x0

    const-string v6, "links_click"

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781515
    if-eqz v0, :cond_0

    .line 1781516
    const-string v1, "external_link_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781517
    const-string v1, "external_link_title"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781518
    const-string v1, "external_link_destination"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781519
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781520
    :cond_0
    return-void
.end method

.method public final a(JLjava/lang/String;LX/9lQ;)V
    .locals 7

    .prologue
    .line 1781521
    const-string v1, "profile_publish_bar_click"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781522
    const-string v1, "publisher_bar_target_type"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781523
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781524
    return-void
.end method

.method public final a(JLjava/lang/String;LX/9lQ;LX/BQ6;LX/BPA;J)V
    .locals 7

    .prologue
    .line 1781525
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "profile_picture_vpv"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781526
    :goto_0
    return-void

    .line 1781527
    :cond_0
    const-string v1, "profile_picture_vpv"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781528
    const-string v1, "session_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781529
    const-string v1, "image_resolution_quality"

    invoke-virtual {p5}, LX/BQ6;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781530
    const-string v1, "image_loading_state"

    invoke-virtual {p6}, LX/BPA;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781531
    const-string v1, "vpvd_time_delta"

    invoke-virtual {v0, v1, p7, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781532
    const-string v1, "network_type"

    iget-object v2, p0, LX/BQ9;->g:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781533
    const-string v1, "is_profile_cached"

    iget-object v2, p0, LX/BQ9;->h:LX/BPC;

    .line 1781534
    iget-boolean v3, v2, LX/BPC;->c:Z

    move v2, v3

    .line 1781535
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781536
    const-string v1, "enabled_features"

    invoke-static {p0}, LX/BQ9;->a(LX/BQ9;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781537
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;LX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;I)V
    .locals 7

    .prologue
    .line 1781538
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "protile_item_click"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781539
    :goto_0
    return-void

    .line 1781540
    :cond_0
    const-string v1, "protile_item_click"

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781541
    const-string v1, "protile_type"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781542
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p5, v1, :cond_1

    .line 1781543
    const-string v1, "item_badge_count"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781544
    :cond_1
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;LX/9lQ;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V
    .locals 7

    .prologue
    .line 1781545
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1781546
    const-string v1, "profile_context_item_vpv"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781547
    const-string v1, "session_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781548
    invoke-static {v0, p5}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    .line 1781549
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781550
    return-void
.end method

.method public final a(LX/0ta;J)V
    .locals 2

    .prologue
    .line 1781551
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne p1, v0, :cond_1

    .line 1781552
    sget-object v0, LX/3zI;->LOCAL_DATA:LX/3zI;

    invoke-direct {p0, v0, p2, p3}, LX/BQ9;->a(LX/3zI;J)V

    .line 1781553
    sget-object v0, LX/3zI;->NETWORK_DATA:LX/3zI;

    invoke-direct {p0, v0}, LX/BQ9;->a(LX/3zI;)V

    .line 1781554
    :cond_0
    :goto_0
    return-void

    .line 1781555
    :cond_1
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    if-ne p1, v0, :cond_0

    .line 1781556
    :cond_2
    sget-object v0, LX/3zI;->NO_DATA:LX/3zI;

    invoke-direct {p0, v0}, LX/BQ9;->a(LX/3zI;)V

    goto :goto_0
.end method

.method public final b(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V
    .locals 7

    .prologue
    .line 1781557
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "protile_footer_click"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781558
    :goto_0
    return-void

    .line 1781559
    :cond_0
    const-string v1, "protile_footer_click"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781560
    const-string v1, "protile_type"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781561
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final b(JLjava/lang/String;LX/9lQ;LX/BQ6;LX/BPA;J)V
    .locals 7

    .prologue
    .line 1781562
    iget-object v0, p0, LX/BQ9;->f:LX/0Zm;

    const-string v1, "profile_cover_photo_vpv"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781563
    :goto_0
    return-void

    .line 1781564
    :cond_0
    const-string v1, "profile_cover_photo_vpv"

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781565
    const-string v1, "session_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781566
    const-string v1, "image_resolution_quality"

    invoke-virtual {p5}, LX/BQ6;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781567
    const-string v1, "image_loading_state"

    invoke-virtual {p6}, LX/BPA;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781568
    const-string v1, "vpvd_time_delta"

    invoke-virtual {v0, v1, p7, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781569
    const-string v1, "network_type"

    iget-object v2, p0, LX/BQ9;->g:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781570
    const-string v1, "is_profile_cached"

    iget-object v2, p0, LX/BQ9;->h:LX/BPC;

    .line 1781571
    iget-boolean v3, v2, LX/BPC;->c:Z

    move v2, v3

    .line 1781572
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781573
    const-string v1, "enabled_features"

    invoke-static {p0}, LX/BQ9;->a(LX/BQ9;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1781574
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final b(JLjava/lang/String;LX/9lQ;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V
    .locals 7

    .prologue
    .line 1781575
    const-string v6, "context_item_impression"

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781576
    if-eqz v0, :cond_0

    .line 1781577
    invoke-static {v0, p5}, LX/BQ9;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)V

    .line 1781578
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781579
    :cond_0
    return-void
.end method

.method public final c(JLX/9lQ;)V
    .locals 7

    .prologue
    .line 1781580
    const/4 v4, 0x0

    const-string v6, "intro_card_expansion"

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781581
    if-eqz v0, :cond_0

    .line 1781582
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781583
    :cond_0
    return-void
.end method

.method public final c(JLjava/lang/String;LX/9lQ;)V
    .locals 3

    .prologue
    .line 1781584
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "profile_timeline_scroll"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "timeline"

    .line 1781585
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1781586
    move-object v0, v0

    .line 1781587
    invoke-static {p0, v0, p1, p2, p4}, LX/BQ9;->a(LX/BQ9;Lcom/facebook/analytics/logger/HoneyClientEvent;JLX/9lQ;)V

    .line 1781588
    iput-object p3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1781589
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781590
    return-void
.end method

.method public final f(J)V
    .locals 7

    .prologue
    .line 1781591
    const/4 v4, 0x0

    sget-object v5, LX/9lQ;->SELF:LX/9lQ;

    const-string v6, "fav_photos_add_click"

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781592
    if-eqz v0, :cond_0

    .line 1781593
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781594
    :cond_0
    return-void
.end method

.method public final g(J)V
    .locals 7

    .prologue
    .line 1781595
    const/4 v4, 0x0

    sget-object v5, LX/9lQ;->SELF:LX/9lQ;

    const-string v6, "fav_photos_edit_click"

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1781596
    if-eqz v0, :cond_0

    .line 1781597
    iget-object v1, p0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1781598
    :cond_0
    return-void
.end method
