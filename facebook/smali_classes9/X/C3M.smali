.class public LX/C3M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3N;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3M",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3N;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845552
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845553
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3M;->b:LX/0Zi;

    .line 1845554
    iput-object p1, p0, LX/C3M;->a:LX/0Ot;

    .line 1845555
    return-void
.end method

.method public static a(LX/0QB;)LX/C3M;
    .locals 4

    .prologue
    .line 1845541
    const-class v1, LX/C3M;

    monitor-enter v1

    .line 1845542
    :try_start_0
    sget-object v0, LX/C3M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845543
    sput-object v2, LX/C3M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845544
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845545
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845546
    new-instance v3, LX/C3M;

    const/16 p0, 0x1ec2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3M;-><init>(LX/0Ot;)V

    .line 1845547
    move-object v0, v3

    .line 1845548
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845549
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845550
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845551
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845540
    const v0, 0x6bb9cc7b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845539
    const v0, 0x6bb9cc7b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1845481
    check-cast p2, LX/C3L;

    .line 1845482
    iget-object v0, p0, LX/C3M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3N;

    iget-object v1, p2, LX/C3L;->a:LX/C33;

    iget-object v2, p2, LX/C3L;->b:LX/1Pb;

    iget v3, p2, LX/C3L;->c:I

    .line 1845483
    iget-object v4, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845484
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1845485
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    .line 1845486
    if-nez v8, :cond_0

    .line 1845487
    const/4 v4, 0x0

    .line 1845488
    :goto_0
    move-object v0, v4

    .line 1845489
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C3N;->c:LX/C3I;

    invoke-virtual {v5, p1}, LX/C3I;->c(LX/1De;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/C3G;->a(LX/C33;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/C3G;->a(LX/1Pb;)LX/C3G;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v10

    move-object v4, v0

    move-object v5, p1

    move-object v6, v1

    move-object v7, v2

    move v9, v3

    .line 1845490
    invoke-static {v5}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    const/4 p0, 0x2

    invoke-interface {v11, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v11

    const/4 p0, 0x1

    invoke-interface {v11, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v11

    const/4 v3, 0x6

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1845491
    invoke-static {v5}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p2

    iget-object p0, v4, LX/C3N;->d:LX/C39;

    invoke-virtual {p0, v5}, LX/C39;->c(LX/1De;)LX/C37;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/C37;->a(LX/C33;)LX/C37;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/C37;->a(LX/1Pb;)LX/C37;

    move-result-object v1

    add-int/lit8 p0, v9, -0x2

    if-gtz p0, :cond_5

    move p0, v0

    :goto_1
    invoke-virtual {v1, p0}, LX/C37;->h(I)LX/C37;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-static {v5}, LX/C3M;->onClick(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {p0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 1845492
    iget-object p0, v4, LX/C3N;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1qb;

    .line 1845493
    iget-object p2, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1845494
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p0, p2}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    .line 1845495
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 1845496
    const p2, 0x7f0e0a4a

    invoke-static {v5, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b1d7c

    invoke-interface {p0, v0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    const p2, 0x7f0b1d6d

    invoke-interface {p0, v3, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1845497
    :cond_1
    iget-object p0, v4, LX/C3N;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1845498
    iget-object p0, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1845499
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object p0

    .line 1845500
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 1845501
    const p2, 0x7f0e0a4c

    invoke-static {v5, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b1d6d

    invoke-interface {p0, v3, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1845502
    :cond_2
    move-object p0, v1

    .line 1845503
    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p0, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v11, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v11

    .line 1845504
    invoke-static {v8}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1845505
    iget-object p0, v4, LX/C3N;->f:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3i;

    const/4 p2, 0x0

    .line 1845506
    new-instance v0, LX/C3h;

    invoke-direct {v0, p0}, LX/C3h;-><init>(LX/C3i;)V

    .line 1845507
    sget-object v4, LX/C3i;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C3g;

    .line 1845508
    if-nez v4, :cond_3

    .line 1845509
    new-instance v4, LX/C3g;

    invoke-direct {v4}, LX/C3g;-><init>()V

    .line 1845510
    :cond_3
    invoke-static {v4, v5, p2, p2, v0}, LX/C3g;->a$redex0(LX/C3g;LX/1De;IILX/C3h;)V

    .line 1845511
    move-object v0, v4

    .line 1845512
    move-object p2, v0

    .line 1845513
    move-object p0, p2

    .line 1845514
    iget-object p2, p0, LX/C3g;->a:LX/C3h;

    iput-object v8, p2, LX/C3h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845515
    iget-object p2, p0, LX/C3g;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 1845516
    move-object p0, p0

    .line 1845517
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 1845518
    :goto_2
    move-object p0, p0

    .line 1845519
    if-eqz p0, :cond_4

    .line 1845520
    const/4 p2, 0x5

    const v0, 0x7f0b1d6d

    invoke-interface {p0, p2, v0}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    invoke-interface {v11, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1845521
    :cond_4
    move-object v4, v11

    .line 1845522
    invoke-interface {v10, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/C3M;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b1d6c

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto/16 :goto_0

    .line 1845523
    :cond_5
    add-int/lit8 p0, v9, -0x2

    goto/16 :goto_1

    .line 1845524
    :cond_6
    invoke-static {v8}, LX/C3b;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1845525
    iget-object p0, v4, LX/C3N;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3l;

    const/4 p2, 0x0

    .line 1845526
    new-instance v0, LX/C3k;

    invoke-direct {v0, p0}, LX/C3k;-><init>(LX/C3l;)V

    .line 1845527
    sget-object v4, LX/C3l;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C3j;

    .line 1845528
    if-nez v4, :cond_7

    .line 1845529
    new-instance v4, LX/C3j;

    invoke-direct {v4}, LX/C3j;-><init>()V

    .line 1845530
    :cond_7
    invoke-static {v4, v5, p2, p2, v0}, LX/C3j;->a$redex0(LX/C3j;LX/1De;IILX/C3k;)V

    .line 1845531
    move-object v0, v4

    .line 1845532
    move-object p2, v0

    .line 1845533
    move-object p0, p2

    .line 1845534
    iget-object p2, p0, LX/C3j;->a:LX/C3k;

    iput-object v8, p2, LX/C3k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845535
    iget-object p2, p0, LX/C3j;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 1845536
    move-object p0, p0

    .line 1845537
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    goto :goto_2

    .line 1845538
    :cond_8
    const/4 p0, 0x0

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1845471
    invoke-static {}, LX/1dS;->b()V

    .line 1845472
    iget v0, p1, LX/1dQ;->b:I

    .line 1845473
    packed-switch v0, :pswitch_data_0

    .line 1845474
    :goto_0
    return-object v2

    .line 1845475
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1845476
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1845477
    check-cast v1, LX/C3L;

    .line 1845478
    iget-object p1, p0, LX/C3M;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C3N;

    iget-object p2, v1, LX/C3L;->a:LX/C33;

    .line 1845479
    iget-object p0, p1, LX/C3N;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3B;

    invoke-virtual {p0, v0, p2}, LX/C3B;->onClick(Landroid/view/View;LX/C33;)V

    .line 1845480
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6bb9cc7b
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C3K;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3M",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845463
    new-instance v1, LX/C3L;

    invoke-direct {v1, p0}, LX/C3L;-><init>(LX/C3M;)V

    .line 1845464
    iget-object v2, p0, LX/C3M;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3K;

    .line 1845465
    if-nez v2, :cond_0

    .line 1845466
    new-instance v2, LX/C3K;

    invoke-direct {v2, p0}, LX/C3K;-><init>(LX/C3M;)V

    .line 1845467
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3K;->a$redex0(LX/C3K;LX/1De;IILX/C3L;)V

    .line 1845468
    move-object v1, v2

    .line 1845469
    move-object v0, v1

    .line 1845470
    return-object v0
.end method
