.class public LX/C7M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7P;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7M",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7P;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851219
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851220
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C7M;->b:LX/0Zi;

    .line 1851221
    iput-object p1, p0, LX/C7M;->a:LX/0Ot;

    .line 1851222
    return-void
.end method

.method public static a(LX/0QB;)LX/C7M;
    .locals 4

    .prologue
    .line 1851223
    const-class v1, LX/C7M;

    monitor-enter v1

    .line 1851224
    :try_start_0
    sget-object v0, LX/C7M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851225
    sput-object v2, LX/C7M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851228
    new-instance v3, LX/C7M;

    const/16 p0, 0x1f8b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7M;-><init>(LX/0Ot;)V

    .line 1851229
    move-object v0, v3

    .line 1851230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1851234
    const v0, -0x7a358a86

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1851235
    check-cast p2, LX/C7J;

    .line 1851236
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1851237
    iget-object v0, p0, LX/C7M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7P;

    iget-object v2, p2, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v3, p2, LX/C7J;->c:Z

    .line 1851238
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1851239
    move-object v12, v6

    check-cast v12, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851240
    new-instance v6, LX/8pR;

    invoke-static {v12}, LX/8pK;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    move-result-object v7

    iget-object v9, v0, LX/C7P;->c:LX/3AE;

    iget-boolean v8, v0, LX/C7P;->a:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-object v11, v0, LX/C7P;->e:LX/17W;

    move-object v8, p1

    invoke-direct/range {v6 .. v11}, LX/8pR;-><init>(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;Landroid/content/Context;LX/3AE;Ljava/lang/Boolean;LX/17W;)V

    .line 1851241
    if-nez v3, :cond_0

    const/4 v7, 0x1

    :goto_0
    invoke-virtual {v6, v7}, LX/8pR;->a(Z)V

    .line 1851242
    new-instance v7, LX/C7N;

    invoke-direct {v7, v0, p1}, LX/C7N;-><init>(LX/C7P;Landroid/content/Context;)V

    .line 1851243
    iput-object v7, v6, LX/8pR;->g:LX/8pQ;

    .line 1851244
    iget-object v7, v6, LX/8pR;->b:LX/8pV;

    move-object v7, v7

    .line 1851245
    new-instance v8, LX/C7O;

    invoke-direct {v8, v0, v12}, LX/C7O;-><init>(LX/C7P;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1851246
    iput-object v8, v7, LX/8pV;->e:LX/8pU;

    .line 1851247
    move-object v4, v6

    .line 1851248
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1851249
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v8, 0x3

    .line 1851250
    if-nez v3, :cond_1

    const/4 v5, 0x0

    :goto_1
    move-object v5, v5

    .line 1851251
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1, v2}, LX/C7P;->a(LX/C7P;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    .line 1851252
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/C7P;->a(LX/1De;)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    iget-object v8, v0, LX/C7P;->b:LX/3AF;

    .line 1851253
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1851254
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8, v5}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v7, 0x7f0b0050

    invoke-virtual {v5, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-virtual {v5, v7}, LX/1ne;->i(F)LX/1ne;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v7, 0x7f0a0427

    invoke-virtual {v5, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v7, 0x7f0a0159

    invoke-virtual {v5, v7}, LX/1ne;->v(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v5, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    const/4 v7, 0x7

    const/4 v8, 0x3

    invoke-interface {v5, v7, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    const/4 v7, 0x4

    const/4 v8, 0x6

    invoke-interface {v5, v7, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    const v7, 0x7f020afa

    invoke-interface {v5, v7}, LX/1Di;->x(I)LX/1Di;

    move-result-object v5

    .line 1851255
    const v7, -0x7a358a86

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 1851256
    invoke-interface {v5, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v5, v5

    .line 1851257
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v2, v4

    .line 1851258
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1851259
    check-cast v0, LX/8pR;

    iput-object v0, p2, LX/C7J;->d:LX/8pR;

    .line 1851260
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1851261
    return-object v2

    .line 1851262
    :cond_0
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v6, v0, LX/C7P;->f:LX/1nM;

    invoke-virtual {v6, v2}, LX/1nM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const/high16 v6, 0x3fc00000    # 1.5f

    invoke-virtual {v5, v6}, LX/1ne;->i(F)LX/1ne;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    iget-object v6, v0, LX/C7P;->g:LX/1VI;

    invoke-static {v6}, LX/C7I;->a(LX/1VI;)I

    move-result v6

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a0159

    invoke-virtual {v5, v6}, LX/1ne;->v(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    const/4 v6, 0x7

    invoke-interface {v5, v6, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v7, 0x7f0b0917

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v8, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f020afa

    invoke-interface {v5, v6}, LX/1Di;->x(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1851263
    invoke-static {}, LX/1dS;->b()V

    .line 1851264
    iget v0, p1, LX/1dQ;->b:I

    .line 1851265
    packed-switch v0, :pswitch_data_0

    .line 1851266
    :goto_0
    return-object v2

    .line 1851267
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1851268
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1851269
    check-cast v1, LX/C7J;

    .line 1851270
    iget-object p1, p0, LX/C7M;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C7P;

    iget-object p2, v1, LX/C7J;->d:LX/8pR;

    .line 1851271
    invoke-virtual {p2, v0}, LX/8pR;->a(Landroid/view/View;)V

    .line 1851272
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x7a358a86
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 1851273
    check-cast p1, LX/C7J;

    .line 1851274
    check-cast p2, LX/C7J;

    .line 1851275
    iget-boolean v0, p1, LX/C7J;->c:Z

    iput-boolean v0, p2, LX/C7J;->c:Z

    .line 1851276
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 1851277
    check-cast p2, LX/C7J;

    .line 1851278
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1851279
    iget-object v0, p0, LX/C7M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1851280
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 1851281
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1851282
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1851283
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/C7J;->c:Z

    .line 1851284
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1851285
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1851286
    const/4 v0, 0x1

    return v0
.end method
