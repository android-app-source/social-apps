.class public LX/Byc;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1837486
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Byc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1837487
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1837481
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1837482
    const v0, 0x7f030b1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1837483
    const v0, 0x7f0d1bff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Byc;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1837484
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Byc;->setOrientation(I)V

    .line 1837485
    return-void
.end method
