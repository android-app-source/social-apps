.class public final LX/Aqm;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1718241
    iput-object p1, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iput-object p2, p0, LX/Aqm;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1718237
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v1, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Aqg;->e(Ljava/lang/String;)V

    .line 1718238
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    const-string v2, "Error on onNonCancellationFailure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718239
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718240
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1718227
    :try_start_0
    iget-object v1, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, p0, LX/Aqm;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, -0x135d6c4f

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1718228
    iput-object v0, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    .line 1718229
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    .line 1718230
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v1, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718231
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "gif_picker_trending_gifs_displayed"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "session_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1718232
    iget-object v3, v0, LX/Aqg;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1718233
    :goto_0
    return-void

    .line 1718234
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1718235
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    const-string v3, "Error on Succesful Result"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718236
    iget-object v0, p0, LX/Aqm;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    goto :goto_0
.end method
