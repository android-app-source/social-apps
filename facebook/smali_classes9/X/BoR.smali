.class public final LX/BoR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/1wG;


# direct methods
.method public constructor <init>(LX/1wG;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1821736
    iput-object p1, p0, LX/BoR;->e:LX/1wG;

    iput-object p2, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/BoR;->b:Ljava/lang/String;

    iput-object p4, p0, LX/BoR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p5, p0, LX/BoR;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1821737
    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v0, v0, LX/1wG;->b:LX/1dt;

    iget-object v1, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/BoR;->b:Ljava/lang/String;

    .line 1821738
    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1821739
    iget-object v0, p0, LX/BoR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/2vB;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821740
    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v0, v0, LX/1wG;->b:LX/1dt;

    iget-object v1, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/BoR;->d:Landroid/content/Context;

    .line 1821741
    invoke-virtual {v0, v1, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1821742
    :goto_0
    return v4

    .line 1821743
    :cond_0
    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v1, v0, LX/1wG;->b:LX/1dt;

    iget-object v0, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1821744
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1821745
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v0, v0, LX/1wG;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->s:LX/0ad;

    sget-short v1, LX/1RY;->s:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821746
    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v0, v0, LX/1wG;->b:LX/1dt;

    iget-object v1, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/BoR;->d:Landroid/content/Context;

    const/4 p1, -0x1

    .line 1821747
    new-instance v3, LX/0ju;

    invoke-direct {v3, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f081064

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f08104f

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    const v5, 0x7f081064

    new-instance p0, LX/Bo8;

    invoke-direct {p0, v0, v1, v2}, LX/Bo8;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v3, v5, p0}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v5, 0x7f08106b

    new-instance p0, LX/Bo7;

    invoke-direct {p0, v0, v1, v2}, LX/Bo7;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v3, v5, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v5, 0x7f0810d9

    const/4 p0, 0x0

    invoke-virtual {v3, v5, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move-result-object v3

    .line 1821748
    const/4 v5, -0x3

    invoke-virtual {v3, v5}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v5

    invoke-virtual {v3, p1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Button;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p0

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1821749
    invoke-virtual {v3, p1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v5

    const/4 p0, -0x2

    invoke-virtual {v3, p0}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1821750
    goto/16 :goto_0

    .line 1821751
    :cond_1
    iget-object v0, p0, LX/BoR;->e:LX/1wG;

    iget-object v0, v0, LX/1wG;->b:LX/1dt;

    iget-object v1, p0, LX/BoR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/BoR;->d:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/1dt;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    goto/16 :goto_0
.end method
