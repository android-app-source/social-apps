.class public final LX/Awi;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V
    .locals 0

    .prologue
    .line 1726962
    iput-object p1, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;B)V
    .locals 0

    .prologue
    .line 1726963
    invoke-direct {p0, p1}, LX/Awi;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1726964
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1726965
    const/4 v0, 0x0

    .line 1726966
    :goto_0
    return v0

    .line 1726967
    :cond_0
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ard;

    .line 1726968
    iget-object v2, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1726969
    invoke-interface {v0}, LX/Ard;->a()V

    goto :goto_1

    .line 1726970
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    .line 1726971
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1726972
    const/4 v0, 0x0

    .line 1726973
    :goto_0
    return v0

    .line 1726974
    :cond_0
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Asb;

    .line 1726975
    iget-object v2, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1726976
    invoke-interface {v0, p3, p4}, LX/Asb;->a(FF)V

    goto :goto_1

    .line 1726977
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1726978
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1726979
    const/4 v0, 0x0

    .line 1726980
    :goto_0
    return v0

    .line 1726981
    :cond_0
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Asv;

    .line 1726982
    iget-object v3, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, v3, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1726983
    invoke-interface {v0, p1, p2, p3, p4}, LX/Asv;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    goto :goto_1

    .line 1726984
    :cond_2
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1726985
    iput-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    .line 1726986
    move v0, v1

    .line 1726987
    goto :goto_0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1726988
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1726989
    const/4 v0, 0x0

    .line 1726990
    :goto_0
    return v0

    .line 1726991
    :cond_0
    iget-object v0, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ard;

    .line 1726992
    iget-object v2, p0, LX/Awi;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1726993
    invoke-interface {v0, p1}, LX/Ard;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 1726994
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
