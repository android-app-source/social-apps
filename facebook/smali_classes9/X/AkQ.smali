.class public LX/AkQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/0Ze;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AkQ;


# instance fields
.field public final a:LX/0pn;


# direct methods
.method public constructor <init>(LX/0pn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709051
    iput-object p1, p0, LX/AkQ;->a:LX/0pn;

    .line 1709052
    return-void
.end method

.method public static a(LX/0QB;)LX/AkQ;
    .locals 4

    .prologue
    .line 1709053
    sget-object v0, LX/AkQ;->b:LX/AkQ;

    if-nez v0, :cond_1

    .line 1709054
    const-class v1, LX/AkQ;

    monitor-enter v1

    .line 1709055
    :try_start_0
    sget-object v0, LX/AkQ;->b:LX/AkQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1709056
    if-eqz v2, :cond_0

    .line 1709057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1709058
    new-instance p0, LX/AkQ;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v3

    check-cast v3, LX/0pn;

    invoke-direct {p0, v3}, LX/AkQ;-><init>(LX/0pn;)V

    .line 1709059
    move-object v0, p0

    .line 1709060
    sput-object v0, LX/AkQ;->b:LX/AkQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1709061
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1709062
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1709063
    :cond_1
    sget-object v0, LX/AkQ;->b:LX/AkQ;

    return-object v0

    .line 1709064
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1709065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1709066
    invoke-virtual {p0}, LX/AkQ;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1709067
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1709068
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "feed_db_cache"

    .line 1709069
    iget-object v2, p0, LX/AkQ;->a:LX/0pn;

    sget-object v3, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2, v3}, LX/0pn;->f(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1709070
    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
