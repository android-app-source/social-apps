.class public LX/Aaj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbQ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbK;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbM;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbO;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Aac;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Aab;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/0Ot",
            "<",
            "LX/AbQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AbK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AbM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AbO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Aac;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Aab;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AbH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688894
    iput-object p1, p0, LX/Aaj;->a:LX/18V;

    .line 1688895
    iput-object p2, p0, LX/Aaj;->b:LX/0Ot;

    .line 1688896
    iput-object p3, p0, LX/Aaj;->c:LX/0Ot;

    .line 1688897
    iput-object p4, p0, LX/Aaj;->d:LX/0Ot;

    .line 1688898
    iput-object p5, p0, LX/Aaj;->e:LX/0Ot;

    .line 1688899
    iput-object p6, p0, LX/Aaj;->f:LX/0Ot;

    .line 1688900
    iput-object p7, p0, LX/Aaj;->g:LX/0Ot;

    .line 1688901
    iput-object p8, p0, LX/Aaj;->h:LX/0Ot;

    .line 1688902
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1688903
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1688904
    const-string v1, "video_broadcast_update_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1688905
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688906
    const-string v1, "video_broadcast_update_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;

    .line 1688907
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688908
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688909
    move-object v0, v0

    .line 1688910
    :goto_0
    return-object v0

    .line 1688911
    :cond_0
    const-string v1, "video_broadcast_calculate_stats_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1688912
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688913
    const-string v1, "video_broadcast_calculate_stats_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;

    .line 1688914
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688915
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688916
    move-object v0, v0

    .line 1688917
    goto :goto_0

    .line 1688918
    :cond_1
    const-string v1, "video_broadcast_seal_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1688919
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688920
    const-string v1, "video_broadcast_seal_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/VideoBroadcastSealRequest;

    .line 1688921
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688922
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688923
    move-object v0, v0

    .line 1688924
    goto :goto_0

    .line 1688925
    :cond_2
    const-string v1, "video_broadcast_update_commercial_break_time_offsets_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1688926
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688927
    const-string v1, "video_broadcast_update_commercial_break_time_offsets_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;

    .line 1688928
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688929
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688930
    move-object v0, v0

    .line 1688931
    goto :goto_0

    .line 1688932
    :cond_3
    const-string v1, "delete_video_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1688933
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688934
    const-string v1, "delete_video_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1688935
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v0, p0, LX/Aaj;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v2, v0, v1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688936
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688937
    move-object v0, v0

    .line 1688938
    goto/16 :goto_0

    .line 1688939
    :cond_4
    const-string v1, "answer_copyright_violation_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1688940
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688941
    const-string v1, "answer_copyright_violation_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;

    .line 1688942
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688943
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688944
    move-object v0, v0

    .line 1688945
    goto/16 :goto_0

    .line 1688946
    :cond_5
    const-string v1, "instream_ads_creator_actions_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1688947
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1688948
    const-string v1, "instream_ads_creator_actions_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;

    .line 1688949
    iget-object v2, p0, LX/Aaj;->a:LX/18V;

    iget-object v1, p0, LX/Aaj;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v2, v1, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688950
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1688951
    move-object v0, v0

    .line 1688952
    goto/16 :goto_0

    .line 1688953
    :cond_6
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
