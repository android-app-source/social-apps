.class public LX/AuZ;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field public b:I

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1722774
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AuZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1722775
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1722776
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AuZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722777
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1722778
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722779
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/AuZ;->a:Landroid/graphics/Paint;

    .line 1722780
    iget-object v0, p0, LX/AuZ;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1722781
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AuZ;->c:I

    .line 1722782
    return-void
.end method

.method private a(FFFF)Landroid/graphics/Path;
    .locals 5

    .prologue
    const/high16 v4, 0x42b40000    # 90.0f

    .line 1722783
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 1722784
    iget-boolean v1, p0, LX/AuZ;->d:Z

    if-eqz v1, :cond_0

    .line 1722785
    iget v1, p0, LX/AuZ;->c:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1722786
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, LX/AuZ;->c:I

    int-to-float v2, v2

    add-float/2addr v2, p1

    iget v3, p0, LX/AuZ;->c:I

    int-to-float v3, v3

    add-float/2addr v3, p2

    invoke-direct {v1, p1, p2, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1722787
    :goto_0
    iget-boolean v1, p0, LX/AuZ;->e:Z

    if-eqz v1, :cond_1

    .line 1722788
    iget v1, p0, LX/AuZ;->c:I

    int-to-float v1, v1

    sub-float v1, p3, v1

    invoke-virtual {v0, v1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1722789
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, LX/AuZ;->c:I

    int-to-float v2, v2

    sub-float v2, p3, v2

    iget v3, p0, LX/AuZ;->c:I

    int-to-float v3, v3

    add-float/2addr v3, p2

    invoke-direct {v1, v2, p2, p3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1722790
    :goto_1
    iget-boolean v1, p0, LX/AuZ;->g:Z

    if-eqz v1, :cond_2

    .line 1722791
    iget v1, p0, LX/AuZ;->c:I

    int-to-float v1, v1

    sub-float v1, p4, v1

    invoke-virtual {v0, p3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1722792
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, LX/AuZ;->c:I

    int-to-float v2, v2

    sub-float v2, p3, v2

    iget v3, p0, LX/AuZ;->c:I

    int-to-float v3, v3

    sub-float v3, p4, v3

    invoke-direct {v1, v2, v3, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1722793
    :goto_2
    iget-boolean v1, p0, LX/AuZ;->f:Z

    if-eqz v1, :cond_3

    .line 1722794
    iget v1, p0, LX/AuZ;->c:I

    int-to-float v1, v1

    sub-float v1, p1, v1

    invoke-virtual {v0, v1, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1722795
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, LX/AuZ;->c:I

    int-to-float v2, v2

    sub-float v2, p4, v2

    iget v3, p0, LX/AuZ;->c:I

    int-to-float v3, v3

    add-float/2addr v3, p1

    invoke-direct {v1, p1, v2, v3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v1, v4, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1722796
    :goto_3
    iget-boolean v1, p0, LX/AuZ;->d:Z

    if-eqz v1, :cond_4

    .line 1722797
    iget v1, p0, LX/AuZ;->c:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1722798
    :goto_4
    return-object v0

    .line 1722799
    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 1722800
    :cond_1
    invoke-virtual {v0, p3, p2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    .line 1722801
    :cond_2
    invoke-virtual {v0, p3, p4}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_2

    .line 1722802
    :cond_3
    invoke-virtual {v0, p1, p4}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 1722803
    :cond_4
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_4
.end method


# virtual methods
.method public final a(ZZZZ)V
    .locals 0

    .prologue
    .line 1722804
    iput-boolean p1, p0, LX/AuZ;->d:Z

    .line 1722805
    iput-boolean p2, p0, LX/AuZ;->f:Z

    .line 1722806
    iput-boolean p3, p0, LX/AuZ;->e:Z

    .line 1722807
    iput-boolean p4, p0, LX/AuZ;->g:Z

    .line 1722808
    return-void
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 1722809
    iget v0, p0, LX/AuZ;->b:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1722810
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1722811
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 1722812
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    .line 1722813
    iget-object v2, p0, LX/AuZ;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1722814
    iget-object v2, p0, LX/AuZ;->a:Landroid/graphics/Paint;

    iget v3, p0, LX/AuZ;->b:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1722815
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-direct {p0, v4, v4, v0, v1}, LX/AuZ;->a(FFFF)Landroid/graphics/Path;

    move-result-object v0

    iget-object v1, p0, LX/AuZ;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1722816
    return-void
.end method

.method public setColor(I)V
    .locals 0

    .prologue
    .line 1722817
    iput p1, p0, LX/AuZ;->b:I

    .line 1722818
    return-void
.end method
