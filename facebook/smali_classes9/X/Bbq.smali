.class public final LX/Bbq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bbt;


# direct methods
.method public constructor <init>(LX/Bbt;)V
    .locals 0

    .prologue
    .line 1801228
    iput-object p1, p0, LX/Bbq;->a:LX/Bbt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1801229
    iget-object v0, p0, LX/Bbq;->a:LX/Bbt;

    iget-object v0, v0, LX/Bbt;->h:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/Bbq;->a:LX/Bbt;

    iget-object v1, v1, LX/Bbt;->i:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1801230
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1801231
    move-object v0, v0

    .line 1801232
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 1801233
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1801234
    move-object v0, v0

    .line 1801235
    iget-object v1, p0, LX/Bbq;->a:LX/Bbt;

    iget-object v1, v1, LX/Bbt;->g:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1801236
    new-instance v0, LX/Bbs;

    iget-object v2, p0, LX/Bbq;->a:LX/Bbt;

    invoke-direct {v0, v2}, LX/Bbs;-><init>(LX/Bbt;)V

    .line 1801237
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1801238
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    invoke-static {v4}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 1801239
    new-instance v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v5, v4}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1801240
    if-eqz v4, :cond_0

    .line 1801241
    iget-object v6, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1801242
    if-eqz v6, :cond_0

    .line 1801243
    iget-object v6, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v6

    .line 1801244
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1801245
    iget-object v6, v0, LX/Bbs;->b:Ljava/util/Map;

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1801246
    iget-object v6, v0, LX/Bbs;->a:LX/Bbt;

    iget-object v6, v6, LX/Bbt;->k:LX/03V;

    sget-object v7, LX/Bbt;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Duplicate token: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1801247
    iget-object v9, v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v9, v9

    .line 1801248
    invoke-virtual {v9}, Lcom/facebook/user/model/UserKey;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801249
    :cond_1
    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1801250
    :cond_2
    invoke-interface {v1}, LX/3On;->close()V

    .line 1801251
    iget-object v1, v0, LX/Bbs;->b:Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 1801252
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1801253
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0
.end method
