.class public LX/BGa;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:[B


# instance fields
.field public final c:LX/0SG;

.field public final d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1767252
    const-class v0, LX/BGa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BGa;->a:Ljava/lang/String;

    .line 1767253
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/BGa;->b:[B

    return-void

    :array_0
    .array-data 1
        0x4dt
        0x6ft
        0x74t
        0x69t
        0x6ft
        0x6et
        0x50t
        0x68t
        0x6ft
        0x74t
        0x6ft
        0x5ft
        0x44t
        0x61t
        0x74t
        0x61t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;LX/0SG;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1767248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767249
    iput-object p1, p0, LX/BGa;->d:Landroid/content/Context;

    .line 1767250
    iput-object p2, p0, LX/BGa;->c:LX/0SG;

    .line 1767251
    return-void
.end method

.method public static a(Landroid/net/Uri;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x1000

    const/4 v0, -0x1

    .line 1767220
    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1767221
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1767222
    :cond_0
    :goto_0
    return v0

    .line 1767223
    :cond_1
    new-array v3, v5, [B

    .line 1767224
    new-array v2, v5, [B

    .line 1767225
    const/16 v5, 0x2000

    new-array v5, v5, [B

    .line 1767226
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1767227
    invoke-virtual {v6, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-ne v4, v0, :cond_2

    .line 1767228
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1767229
    :catch_0
    move-exception v1

    .line 1767230
    sget-object v2, LX/BGa;->a:Ljava/lang/String;

    const-string v3, "Error reading motion photo data"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1767231
    :cond_2
    :try_start_1
    invoke-virtual {v6, v2}, Ljava/io/FileInputStream;->read([B)I

    .line 1767232
    :goto_1
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1000

    invoke-static {v3, v4, v5, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1767233
    const/4 v4, 0x0

    const/16 v7, 0x1000

    const/16 v8, 0x1000

    invoke-static {v2, v4, v5, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1767234
    const/4 v7, 0x0

    .line 1767235
    move v4, v7

    :goto_2
    array-length v8, v5

    if-ge v4, v8, :cond_6

    .line 1767236
    aget-byte v8, v5, v4

    sget-object v9, LX/BGa;->b:[B

    aget-byte v9, v9, v7

    if-ne v8, v9, :cond_5

    .line 1767237
    const/4 v8, 0x1

    :goto_3
    sget-object v9, LX/BGa;->b:[B

    array-length v9, v9

    if-ge v8, v9, :cond_5

    .line 1767238
    add-int v9, v4, v8

    array-length p0, v5

    if-ge v9, p0, :cond_5

    add-int v9, v4, v8

    aget-byte v9, v5, v9

    sget-object p0, LX/BGa;->b:[B

    aget-byte p0, p0, v8

    if-ne v9, p0, :cond_5

    .line 1767239
    sget-object v9, LX/BGa;->b:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_4

    .line 1767240
    :goto_4
    move v4, v4

    .line 1767241
    if-eq v4, v0, :cond_3

    .line 1767242
    add-int/2addr v1, v4

    sget-object v2, LX/BGa;->b:[B

    array-length v0, v2

    add-int/2addr v0, v1

    goto :goto_0

    .line 1767243
    :cond_3
    add-int/lit16 v1, v1, 0x1000

    .line 1767244
    invoke-virtual {v6, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    if-eq v4, v0, :cond_0

    move-object v9, v3

    move-object v3, v2

    move-object v2, v9

    goto :goto_1

    .line 1767245
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1767246
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1767247
    :cond_6
    const/4 v4, -0x1

    goto :goto_4
.end method

.method public static a(LX/0QB;)LX/BGa;
    .locals 1

    .prologue
    .line 1767219
    invoke-static {p0}, LX/BGa;->b(LX/0QB;)LX/BGa;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BGa;
    .locals 3

    .prologue
    .line 1767217
    new-instance v2, LX/BGa;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/BGa;-><init>(Landroid/content/Context;LX/0SG;)V

    .line 1767218
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 1767198
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "motion_photo_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1767199
    iget-object v0, p0, LX/BGa;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1767200
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1767201
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1767202
    :goto_0
    return-object v0

    .line 1767203
    :cond_0
    invoke-static {p2}, LX/BGa;->a(Landroid/net/Uri;)I

    move-result v0

    .line 1767204
    if-ne v0, v5, :cond_1

    .line 1767205
    const/4 v0, 0x0

    goto :goto_0

    .line 1767206
    :cond_1
    const/16 v2, 0x1000

    :try_start_0
    new-array v2, v2, [B

    .line 1767207
    new-instance v3, Ljava/io/File;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1767208
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1767209
    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/io/FileInputStream;->skip(J)J

    .line 1767210
    iget-object v0, p0, LX/BGa;->d:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 1767211
    :goto_1
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-eq v3, v5, :cond_2

    .line 1767212
    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1767213
    :catch_0
    move-exception v0

    .line 1767214
    sget-object v2, LX/BGa;->a:Ljava/lang/String;

    const-string v3, "Error writing motion photo data"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1767215
    :goto_2
    iget-object v0, p0, LX/BGa;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1767216
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
