.class public LX/BAa;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[J


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:Lcom/facebook/notifications/model/SystemTrayNotification;

.field private d:Landroid/content/Context;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:LX/2HB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753388
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, LX/BAa;->a:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0xfa
        0xc8
        0xfa
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1753368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753369
    iput-boolean v1, p0, LX/BAa;->e:Z

    .line 1753370
    iput-boolean v1, p0, LX/BAa;->f:Z

    .line 1753371
    iput-boolean v1, p0, LX/BAa;->g:Z

    .line 1753372
    iput-boolean v0, p0, LX/BAa;->h:Z

    .line 1753373
    iput-boolean v0, p0, LX/BAa;->i:Z

    .line 1753374
    const/4 v0, 0x0

    iput-object v0, p0, LX/BAa;->j:Ljava/lang/String;

    .line 1753375
    iput-object p1, p0, LX/BAa;->d:Landroid/content/Context;

    .line 1753376
    iput-object p2, p0, LX/BAa;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1753377
    new-instance v0, LX/2HB;

    invoke-direct {v0, p1}, LX/2HB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BAa;->k:LX/2HB;

    .line 1753378
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    .line 1753379
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    const/4 v1, 0x2

    .line 1753380
    iput v1, v0, LX/2HB;->j:I

    .line 1753381
    const/4 p2, 0x1

    .line 1753382
    iget-object v0, p0, LX/BAa;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->n:LX/0Tn;

    invoke-interface {v0, v1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/BAa;->e:Z

    .line 1753383
    iget-object v0, p0, LX/BAa;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->o:LX/0Tn;

    invoke-interface {v0, v1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/BAa;->g:Z

    .line 1753384
    iget-object v0, p0, LX/BAa;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->p:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1753385
    iput-object v0, p0, LX/BAa;->j:Ljava/lang/String;

    .line 1753386
    iget-object v0, p0, LX/BAa;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->m:LX/0Tn;

    invoke-interface {v0, v1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/BAa;->f:Z

    .line 1753387
    return-void
.end method

.method public static b(LX/0QB;)LX/BAa;
    .locals 3

    .prologue
    .line 1753366
    new-instance v2, LX/BAa;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/BAa;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1753367
    return-object v2
.end method


# virtual methods
.method public final a()LX/BAa;
    .locals 1

    .prologue
    .line 1753364
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BAa;->e:Z

    .line 1753365
    return-object p0
.end method

.method public final a(I)LX/BAa;
    .locals 3

    .prologue
    .line 1753359
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1753360
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    iget-object v1, p0, LX/BAa;->d:Landroid/content/Context;

    const v2, 0x7f0a00d1

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 1753361
    iput v1, v0, LX/2HB;->y:I

    .line 1753362
    :cond_0
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->a(I)LX/2HB;

    .line 1753363
    return-object p0
.end method

.method public final a(J)LX/BAa;
    .locals 1

    .prologue
    .line 1753357
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1, p2}, LX/2HB;->a(J)LX/2HB;

    .line 1753358
    return-object p0
.end method

.method public final a(LX/3pc;)LX/BAa;
    .locals 1

    .prologue
    .line 1753355
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    .line 1753356
    return-object p0
.end method

.method public final a(LX/3pw;)LX/BAa;
    .locals 1

    .prologue
    .line 1753352
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    .line 1753353
    invoke-interface {p1, v0}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 1753354
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)LX/BAa;
    .locals 1

    .prologue
    .line 1753349
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    .line 1753350
    iput-object p1, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 1753351
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/BAa;
    .locals 1

    .prologue
    .line 1753334
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1753335
    return-object p0
.end method

.method public final b()LX/BAa;
    .locals 1

    .prologue
    .line 1753347
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BAa;->f:Z

    .line 1753348
    return-object p0
.end method

.method public final b(Z)LX/BAa;
    .locals 1

    .prologue
    .line 1753345
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->c(Z)LX/2HB;

    .line 1753346
    return-object p0
.end method

.method public final c()LX/BAa;
    .locals 1

    .prologue
    .line 1753343
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BAa;->g:Z

    .line 1753344
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/BAa;
    .locals 1

    .prologue
    .line 1753341
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    .line 1753342
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)LX/BAa;
    .locals 3

    .prologue
    .line 1753336
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1753337
    if-eqz v0, :cond_0

    .line 1753338
    const-string v0, "%s - beta"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1753339
    :cond_0
    iget-object v0, p0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v0, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 1753340
    return-object p0
.end method
