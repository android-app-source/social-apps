.class public LX/AVv;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1680506
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AVv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1680507
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1680519
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AVv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680520
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1680515
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680516
    const v0, 0x7f0305ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1680517
    const v0, 0x7f0d0f93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AVv;->a:Landroid/view/View;

    .line 1680518
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1680512
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 1680513
    iget-object v0, p0, LX/AVv;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1680514
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1680509
    invoke-super {p0}, LX/2oy;->d()V

    .line 1680510
    iget-object v0, p0, LX/AVv;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1680511
    return-void
.end method

.method public getLiveNuxSkipView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1680508
    iget-object v0, p0, LX/AVv;->a:Landroid/view/View;

    return-object v0
.end method
