.class public final LX/Bf5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$SuggestEditsHeader$;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BgK;

.field public final synthetic b:LX/Bf7;


# direct methods
.method public constructor <init>(LX/Bf7;LX/BgK;)V
    .locals 0

    .prologue
    .line 1805700
    iput-object p1, p0, LX/Bf5;->b:LX/Bf7;

    iput-object p2, p0, LX/Bf5;->a:LX/BgK;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1805701
    iget-object v0, p0, LX/Bf5;->a:LX/BgK;

    invoke-virtual {v0, p1}, LX/BgK;->a(Ljava/lang/Throwable;)V

    .line 1805702
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1805703
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1805704
    if-nez p1, :cond_0

    .line 1805705
    iget-object v0, p0, LX/Bf5;->a:LX/BgK;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty result returned for Suggest Edits header GraphQL query"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/BgK;->a(Ljava/lang/Throwable;)V

    .line 1805706
    :goto_0
    return-void

    .line 1805707
    :cond_0
    iget-object v0, p0, LX/Bf5;->a:LX/BgK;

    .line 1805708
    iget-object v1, v0, LX/BgK;->x:LX/BgW;

    invoke-virtual {v1, p1}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)V

    .line 1805709
    invoke-static {p1}, LX/BgK;->b(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1805710
    iget-object v1, v0, LX/BgK;->z:LX/Bg9;

    sget-object v2, LX/BgJ;->HEADER:LX/BgJ;

    invoke-virtual {v1, v2}, LX/Bg9;->a(LX/BgJ;)V

    .line 1805711
    :cond_1
    iget-object v1, v0, LX/BgK;->A:LX/Bg9;

    sget-object v2, LX/BgJ;->HEADER:LX/BgJ;

    invoke-virtual {v1, v2}, LX/Bg9;->a(LX/BgJ;)V

    .line 1805712
    goto :goto_0
.end method
