.class public final LX/BQR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

.field public final synthetic b:LX/BQS;


# direct methods
.method public constructor <init>(LX/BQS;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)V
    .locals 0

    .prologue
    .line 1782185
    iput-object p1, p0, LX/BQR;->b:LX/BQS;

    iput-object p2, p0, LX/BQR;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1782186
    iget-object v0, p0, LX/BQR;->b:LX/BQS;

    iget-object v0, v0, LX/BQS;->c:LX/BQQ;

    iget-object v1, p0, LX/BQR;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1782187
    iget-object v2, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1782188
    invoke-virtual {v0, v1}, LX/BQQ;->a(Ljava/lang/String;)V

    .line 1782189
    iget-object v0, p0, LX/BQR;->b:LX/BQS;

    iget-object v0, v0, LX/BQS;->d:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082740

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1782190
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782191
    iget-object v0, p0, LX/BQR;->b:LX/BQS;

    iget-object v0, v0, LX/BQS;->c:LX/BQQ;

    iget-object v1, p0, LX/BQR;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1782192
    iget-object p1, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    move-object v1, p1

    .line 1782193
    invoke-virtual {v0, v1}, LX/BQQ;->a(Ljava/lang/String;)V

    .line 1782194
    iget-object v0, p0, LX/BQR;->b:LX/BQS;

    iget-object v0, v0, LX/BQS;->b:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->b()V

    .line 1782195
    return-void
.end method
