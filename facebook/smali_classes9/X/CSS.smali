.class public final enum LX/CSS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CSS;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CSS;

.field public static final enum ADDRESS_SIMPLE:LX/CSS;

.field public static final enum CHECKBOX_SIMPLE:LX/CSS;

.field public static final enum CONTACT_INFO_SIMPLE:LX/CSS;

.field public static final enum DATE_PICKER_SIMPLE:LX/CSS;

.field public static final enum DATE_TIME_PICKER_SIMPLE:LX/CSS;

.field public static final enum DATE_TIME_SELECTION_SIMPLE:LX/CSS;

.field public static final enum MARKET_OPT_IN:LX/CSS;

.field public static final enum PAYMENT_SIMPLE:LX/CSS;

.field public static final enum SELECTION_PRODUCT_MULTI_SELECT:LX/CSS;

.field public static final enum SELECTION_PRODUCT_SIMPLE:LX/CSS;

.field public static final enum SELECTION_PRODUCT_WITH_SELECTOR_MULTI:LX/CSS;

.field public static final enum SELECTION_PRODUCT_WITH_SELECTOR_SINGLE:LX/CSS;

.field public static final enum SELECTION_STRING_MULTI_SELECT:LX/CSS;

.field public static final enum SELECTION_STRING_SINGLE_SELECT_DROPDOWN:LX/CSS;

.field public static final enum SELECTION_STRING_SINGLE_SELECT_EXPANDED:LX/CSS;

.field public static final enum SHOPPING_CART_SIMPLE:LX/CSS;

.field public static final enum TEXT_CITY_TYPEAHEAD:LX/CSS;

.field public static final enum TEXT_SIMPLE:LX/CSS;

.field public static final enum TIME_SLOT_PICKER_SIMPLE:LX/CSS;

.field public static final enum UNSET_ENUM_VALUE:LX/CSS;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1894596
    new-instance v0, LX/CSS;

    const-string v1, "UNSET_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->UNSET_ENUM_VALUE:LX/CSS;

    .line 1894597
    new-instance v0, LX/CSS;

    const-string v1, "ADDRESS_SIMPLE"

    invoke-direct {v0, v1, v4}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->ADDRESS_SIMPLE:LX/CSS;

    .line 1894598
    new-instance v0, LX/CSS;

    const-string v1, "CHECKBOX_SIMPLE"

    invoke-direct {v0, v1, v5}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->CHECKBOX_SIMPLE:LX/CSS;

    .line 1894599
    new-instance v0, LX/CSS;

    const-string v1, "CONTACT_INFO_SIMPLE"

    invoke-direct {v0, v1, v6}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->CONTACT_INFO_SIMPLE:LX/CSS;

    .line 1894600
    new-instance v0, LX/CSS;

    const-string v1, "DATE_PICKER_SIMPLE"

    invoke-direct {v0, v1, v7}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->DATE_PICKER_SIMPLE:LX/CSS;

    .line 1894601
    new-instance v0, LX/CSS;

    const-string v1, "DATE_TIME_SELECTION_SIMPLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->DATE_TIME_SELECTION_SIMPLE:LX/CSS;

    .line 1894602
    new-instance v0, LX/CSS;

    const-string v1, "DATE_TIME_PICKER_SIMPLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->DATE_TIME_PICKER_SIMPLE:LX/CSS;

    .line 1894603
    new-instance v0, LX/CSS;

    const-string v1, "MARKET_OPT_IN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->MARKET_OPT_IN:LX/CSS;

    .line 1894604
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_PRODUCT_SIMPLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_PRODUCT_SIMPLE:LX/CSS;

    .line 1894605
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_PRODUCT_MULTI_SELECT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_PRODUCT_MULTI_SELECT:LX/CSS;

    .line 1894606
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_PRODUCT_WITH_SELECTOR_SINGLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_SINGLE:LX/CSS;

    .line 1894607
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_PRODUCT_WITH_SELECTOR_MULTI"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_MULTI:LX/CSS;

    .line 1894608
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_STRING_SINGLE_SELECT_EXPANDED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_EXPANDED:LX/CSS;

    .line 1894609
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_STRING_SINGLE_SELECT_DROPDOWN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_DROPDOWN:LX/CSS;

    .line 1894610
    new-instance v0, LX/CSS;

    const-string v1, "SELECTION_STRING_MULTI_SELECT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SELECTION_STRING_MULTI_SELECT:LX/CSS;

    .line 1894611
    new-instance v0, LX/CSS;

    const-string v1, "SHOPPING_CART_SIMPLE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->SHOPPING_CART_SIMPLE:LX/CSS;

    .line 1894612
    new-instance v0, LX/CSS;

    const-string v1, "TEXT_CITY_TYPEAHEAD"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->TEXT_CITY_TYPEAHEAD:LX/CSS;

    .line 1894613
    new-instance v0, LX/CSS;

    const-string v1, "TEXT_SIMPLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->TEXT_SIMPLE:LX/CSS;

    .line 1894614
    new-instance v0, LX/CSS;

    const-string v1, "TIME_SLOT_PICKER_SIMPLE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->TIME_SLOT_PICKER_SIMPLE:LX/CSS;

    .line 1894615
    new-instance v0, LX/CSS;

    const-string v1, "PAYMENT_SIMPLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/CSS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSS;->PAYMENT_SIMPLE:LX/CSS;

    .line 1894616
    const/16 v0, 0x14

    new-array v0, v0, [LX/CSS;

    sget-object v1, LX/CSS;->UNSET_ENUM_VALUE:LX/CSS;

    aput-object v1, v0, v3

    sget-object v1, LX/CSS;->ADDRESS_SIMPLE:LX/CSS;

    aput-object v1, v0, v4

    sget-object v1, LX/CSS;->CHECKBOX_SIMPLE:LX/CSS;

    aput-object v1, v0, v5

    sget-object v1, LX/CSS;->CONTACT_INFO_SIMPLE:LX/CSS;

    aput-object v1, v0, v6

    sget-object v1, LX/CSS;->DATE_PICKER_SIMPLE:LX/CSS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CSS;->DATE_TIME_SELECTION_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CSS;->DATE_TIME_PICKER_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CSS;->MARKET_OPT_IN:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CSS;->SELECTION_PRODUCT_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CSS;->SELECTION_PRODUCT_MULTI_SELECT:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_SINGLE:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CSS;->SELECTION_PRODUCT_WITH_SELECTOR_MULTI:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_EXPANDED:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CSS;->SELECTION_STRING_SINGLE_SELECT_DROPDOWN:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CSS;->SELECTION_STRING_MULTI_SELECT:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CSS;->SHOPPING_CART_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/CSS;->TEXT_CITY_TYPEAHEAD:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/CSS;->TEXT_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/CSS;->TIME_SLOT_PICKER_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/CSS;->PAYMENT_SIMPLE:LX/CSS;

    aput-object v2, v0, v1

    sput-object v0, LX/CSS;->$VALUES:[LX/CSS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1894617
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CSS;
    .locals 1

    .prologue
    .line 1894618
    const-class v0, LX/CSS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CSS;

    return-object v0
.end method

.method public static values()[LX/CSS;
    .locals 1

    .prologue
    .line 1894619
    sget-object v0, LX/CSS;->$VALUES:[LX/CSS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CSS;

    return-object v0
.end method
