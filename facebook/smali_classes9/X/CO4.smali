.class public LX/CO4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field public final b:LX/CNq;

.field public final c:LX/CNe;

.field public final d:LX/CNe;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1882509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882510
    iput-object p1, p0, LX/CO4;->a:LX/CNb;

    .line 1882511
    iput-object p2, p0, LX/CO4;->b:LX/CNq;

    .line 1882512
    const-string v0, "send-action"

    invoke-virtual {p1, v0}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v0

    .line 1882513
    const-string v2, "failure-action"

    invoke-virtual {p1, v2}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1882514
    if-eqz v0, :cond_1

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/CO4;->c:LX/CNe;

    .line 1882515
    if-eqz v2, :cond_0

    invoke-static {v2, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/CO4;->d:LX/CNe;

    .line 1882516
    return-void

    :cond_1
    move-object v0, v1

    .line 1882517
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1882518
    iget-object v0, p0, LX/CO4;->a:LX/CNb;

    const-string v1, "group"

    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1882519
    iget-object v1, p0, LX/CO4;->a:LX/CNb;

    const-string v2, "source"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1882520
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1882521
    new-instance v2, LX/4G4;

    invoke-direct {v2}, LX/4G4;-><init>()V

    invoke-virtual {v2, v0}, LX/4G4;->a(Ljava/lang/String;)LX/4G4;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/4G4;->b(Ljava/lang/String;)LX/4G4;

    move-result-object v2

    .line 1882522
    invoke-static {}, LX/B2A;->b()LX/B29;

    move-result-object v3

    .line 1882523
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1882524
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1882525
    new-instance v3, LX/B2M;

    invoke-direct {v3}, LX/B2M;-><init>()V

    .line 1882526
    iput-object v0, v3, LX/B2M;->a:Ljava/lang/String;

    .line 1882527
    move-object v3, v3

    .line 1882528
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1882529
    iput-object v4, v3, LX/B2M;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1882530
    move-object v3, v3

    .line 1882531
    invoke-virtual {v3}, LX/B2M;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRequestToJoinCoreMutationModel$GroupModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    .line 1882532
    iget-object v3, p0, LX/CO4;->b:LX/CNq;

    invoke-virtual {v3}, LX/CNq;->g()LX/0tX;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1882533
    new-instance v1, LX/CO3;

    invoke-direct {v1, p0}, LX/CO3;-><init>(LX/CO4;)V

    .line 1882534
    iget-object v2, p0, LX/CO4;->b:LX/CNq;

    invoke-virtual {v2}, LX/CNq;->n()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1882535
    return-void
.end method
