.class public LX/C6T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1849912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;LX/C6G;)LX/3Ii;
    .locals 1

    .prologue
    .line 1849951
    new-instance v0, LX/C6S;

    invoke-direct {v0, p1, p0}, LX/C6S;-><init>(LX/C6G;Lcom/facebook/graphql/model/FeedUnit;)V

    return-object v0
.end method

.method public static a(LX/C6N;LX/1Pq;LX/C6g;LX/0SG;LX/03V;)Landroid/view/View$OnClickListener;
    .locals 6

    .prologue
    .line 1849946
    iget-object v0, p0, LX/C6N;->b:LX/C6a;

    .line 1849947
    iget-boolean v1, v0, LX/C6a;->d:Z

    move v0, v1

    .line 1849948
    if-nez v0, :cond_0

    .line 1849949
    const/4 v0, 0x0

    .line 1849950
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/C6Q;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/C6Q;-><init>(LX/C6N;LX/1Pq;LX/C6g;LX/0SG;LX/03V;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C6g;LX/0SG;LX/03V;LX/C6a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/C6g;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/C6a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1849913
    invoke-static {p0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1849914
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1849915
    :goto_0
    iget v0, p4, LX/C6a;->e:I

    move v0, v0

    .line 1849916
    add-int/lit8 v2, v0, 0x1

    .line 1849917
    iget v0, p4, LX/C6a;->f:I

    move v0, v0

    .line 1849918
    add-int/lit8 v3, v0, 0x1

    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    new-instance v5, LX/C6R;

    invoke-direct {v5, p3}, LX/C6R;-><init>(LX/03V;)V

    move-object v0, p1

    .line 1849919
    new-instance v6, LX/C6c;

    invoke-direct {v6}, LX/C6c;-><init>()V

    move-object v7, v6

    .line 1849920
    const-string p0, "input"

    new-instance v6, LX/4GM;

    invoke-direct {v6}, LX/4GM;-><init>()V

    iget-object p1, v0, LX/C6g;->c:Ljava/lang/String;

    .line 1849921
    const-string p2, "actor_id"

    invoke-virtual {v6, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849922
    move-object v6, v6

    .line 1849923
    const-string p1, "data_ft"

    invoke-virtual {v6, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849924
    move-object v6, v6

    .line 1849925
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    .line 1849926
    const-string p2, "selection"

    invoke-virtual {v6, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849927
    move-object p1, v6

    .line 1849928
    if-gtz v3, :cond_1

    const/4 v6, 0x0

    .line 1849929
    :goto_1
    const-string p2, "followup_selection"

    invoke-virtual {p1, p2, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849930
    move-object v6, p1

    .line 1849931
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 1849932
    const-string p2, "event_timestamp"

    invoke-virtual {v6, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1849933
    move-object v6, v6

    .line 1849934
    const-string p1, "inline_responded"

    .line 1849935
    const-string p2, "event"

    invoke-virtual {v6, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849936
    move-object v6, v6

    .line 1849937
    const-string p1, "inline_android"

    .line 1849938
    const-string p2, "survey_pool_type"

    invoke-virtual {v6, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849939
    move-object v6, v6

    .line 1849940
    invoke-virtual {v7, p0, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1849941
    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 1849942
    iget-object v7, v0, LX/C6g;->a:LX/1Ck;

    const-string p0, "MUTATE_INLINE_SURVEY_SUBMIT_KEY"

    iget-object p1, v0, LX/C6g;->b:LX/0tX;

    invoke-virtual {p1, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-virtual {v7, p0, v6, v5}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1849943
    return-void

    .line 1849944
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1849945
    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method
