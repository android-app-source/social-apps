.class public LX/Bt3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1nq;

.field public final c:LX/1xc;

.field private final d:LX/03V;

.field public final e:LX/1zC;

.field private final f:LX/1zB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/1xc;LX/1W9;LX/03V;LX/1zC;LX/1zB;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/1nA;",
            ">;",
            "LX/1xc;",
            "LX/1W9;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1zC;",
            "LX/1zB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828597
    iput-object p2, p0, LX/Bt3;->a:LX/0Or;

    .line 1828598
    iput-object p5, p0, LX/Bt3;->d:LX/03V;

    .line 1828599
    iput-object p3, p0, LX/Bt3;->c:LX/1xc;

    .line 1828600
    iput-object p6, p0, LX/Bt3;->e:LX/1zC;

    .line 1828601
    iput-object p7, p0, LX/Bt3;->f:LX/1zB;

    .line 1828602
    invoke-static {p1, p4}, LX/1zB;->a(Landroid/content/Context;LX/1W9;)LX/1nq;

    move-result-object v0

    iput-object v0, p0, LX/Bt3;->b:LX/1nq;

    .line 1828603
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;)Landroid/text/Layout;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            ")",
            "Landroid/text/Layout;"
        }
    .end annotation

    .prologue
    .line 1828604
    iget-object v0, p0, LX/Bt3;->f:LX/1zB;

    iget-object v1, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {v0, v1, p1}, LX/1zB;->a(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1828605
    :try_start_0
    iget-object v0, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {v0, p2}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1828606
    iget-object v0, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1828607
    :goto_0
    return-object v0

    .line 1828608
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1828609
    iget-object v2, p0, LX/Bt3;->d:LX/03V;

    const-string v3, "FlatContentTextLayoutPartDefinition_withZombie"

    const-string v4, "JellyBean setText bug with zombie: %s"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 1828610
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828611
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 1828612
    iput-object v1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1828613
    move-object v0, v0

    .line 1828614
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    .line 1828615
    iget-object v0, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1828616
    iget-object v0, p0, LX/Bt3;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    goto :goto_0
.end method
