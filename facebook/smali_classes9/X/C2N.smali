.class public final LX/C2N;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1Vm;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C2M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>.Action",
            "LinkCallToActionComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1Vm;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1Vm;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1844284
    iput-object p1, p0, LX/C2N;->b:LX/1Vm;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1844285
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actionLink"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "clickListener"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C2N;->c:[Ljava/lang/String;

    .line 1844286
    iput v3, p0, LX/C2N;->d:I

    .line 1844287
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C2N;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C2N;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C2N;LX/1De;IILX/C2M;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1Vm",
            "<TE;>.Action",
            "LinkCallToActionComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1844280
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1844281
    iput-object p4, p0, LX/C2N;->a:LX/C2M;

    .line 1844282
    iget-object v0, p0, LX/C2N;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1844283
    return-void
.end method


# virtual methods
.method public final a(LX/1Pp;)LX/C2N;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844277
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->a:LX/1Pp;

    .line 1844278
    iget-object v0, p0, LX/C2N;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1844279
    return-object p0
.end method

.method public final a(LX/1dc;)LX/C2N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844252
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->d:LX/1dc;

    .line 1844253
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/C2N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844275
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->e:Landroid/net/Uri;

    .line 1844276
    return-object p0
.end method

.method public final a(Landroid/view/View$OnClickListener;)LX/C2N;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844288
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->c:Landroid/view/View$OnClickListener;

    .line 1844289
    iget-object v0, p0, LX/C2N;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1844290
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844272
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1844273
    iget-object v0, p0, LX/C2N;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1844274
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1844268
    invoke-super {p0}, LX/1X5;->a()V

    .line 1844269
    const/4 v0, 0x0

    iput-object v0, p0, LX/C2N;->a:LX/C2M;

    .line 1844270
    iget-object v0, p0, LX/C2N;->b:LX/1Vm;

    iget-object v0, v0, LX/1Vm;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1844271
    return-void
.end method

.method public final b(Landroid/view/View$OnClickListener;)LX/C2N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844266
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->j:Landroid/view/View$OnClickListener;

    .line 1844267
    return-object p0
.end method

.method public final c(Landroid/view/View$OnClickListener;)LX/C2N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1844264
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    iput-object p1, v0, LX/C2M;->k:Landroid/view/View$OnClickListener;

    .line 1844265
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1Vm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1844254
    iget-object v1, p0, LX/C2N;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C2N;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C2N;->d:I

    if-ge v1, v2, :cond_2

    .line 1844255
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1844256
    :goto_0
    iget v2, p0, LX/C2N;->d:I

    if-ge v0, v2, :cond_1

    .line 1844257
    iget-object v2, p0, LX/C2N;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1844258
    iget-object v2, p0, LX/C2N;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1844259
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1844260
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1844261
    :cond_2
    iget-object v0, p0, LX/C2N;->a:LX/C2M;

    .line 1844262
    invoke-virtual {p0}, LX/C2N;->a()V

    .line 1844263
    return-object v0
.end method
