.class public LX/Afn;
.super LX/AeL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeL",
        "<",
        "LX/Afm;",
        ">;"
    }
.end annotation


# instance fields
.field private final m:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1699704
    invoke-direct {p0, p1}, LX/AeL;-><init>(Landroid/view/View;)V

    .line 1699705
    const v0, 0x7f0d197e

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Afn;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1699706
    return-void
.end method

.method private a(LX/Afm;LX/AeU;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1699690
    invoke-super {p0, p1, p2}, LX/AeL;->a(LX/AeP;LX/AeU;)V

    .line 1699691
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1699692
    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1699693
    iget-object v2, p2, LX/AeU;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Afm;->c:LX/AcC;

    iget-object v3, v3, LX/AcC;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1699694
    new-instance v3, LX/47x;

    invoke-direct {v3, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1699695
    iget-object v4, p1, LX/Afm;->b:LX/AcC;

    iget-object v5, p1, LX/Afm;->c:LX/AcC;

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1699696
    if-eqz v2, :cond_0

    .line 1699697
    const v2, 0x7f080bfa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699698
    invoke-static {v6, v1, v4, v3}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699699
    :goto_0
    iget-object v0, p0, LX/Afn;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699700
    return-void

    .line 1699701
    :cond_0
    const v2, 0x7f080bfb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699702
    invoke-static {v6, v1, v4, v3}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699703
    const/4 v0, 0x2

    invoke-static {v0, v1, v4, v3}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/AeO;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699688
    check-cast p1, LX/Afm;

    invoke-direct {p0, p1, p2}, LX/Afn;->a(LX/Afm;LX/AeU;)V

    return-void
.end method

.method public final bridge synthetic a(LX/AeP;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699689
    check-cast p1, LX/Afm;

    invoke-direct {p0, p1, p2}, LX/Afn;->a(LX/Afm;LX/AeU;)V

    return-void
.end method
