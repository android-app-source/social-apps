.class public LX/Ag7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:LX/0Uo;

.field public final d:LX/03V;

.field private final e:LX/1Ck;

.field private final f:LX/0tX;

.field private final g:Landroid/os/Handler;

.field public final h:Ljava/lang/String;

.field private final i:LX/19j;

.field private final j:LX/3Hd;

.field private final k:LX/0SG;

.field public final l:LX/0Zc;

.field public m:LX/Bwd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:I

.field public q:Z

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;

.field private t:J

.field private u:J

.field public v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700234
    const-class v0, LX/Ag7;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ag7;->a:Ljava/lang/String;

    .line 1700235
    const-class v0, LX/Ag7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ag7;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Uo;LX/03V;LX/1Ck;LX/0tX;LX/19j;LX/3Hd;LX/0SG;LX/0Zc;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1700298
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ag7;->q:Z

    .line 1700299
    iput-object p1, p0, LX/Ag7;->c:LX/0Uo;

    .line 1700300
    iput-object p2, p0, LX/Ag7;->d:LX/03V;

    .line 1700301
    iput-object p3, p0, LX/Ag7;->e:LX/1Ck;

    .line 1700302
    iput-object p4, p0, LX/Ag7;->f:LX/0tX;

    .line 1700303
    iput-object p5, p0, LX/Ag7;->i:LX/19j;

    .line 1700304
    iput-object p6, p0, LX/Ag7;->j:LX/3Hd;

    .line 1700305
    iput-object p7, p0, LX/Ag7;->k:LX/0SG;

    .line 1700306
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/Ag7;->g:Landroid/os/Handler;

    .line 1700307
    const/4 v0, 0x3

    iput v0, p0, LX/Ag7;->p:I

    .line 1700308
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LiveStatusBatchPoller"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Ag7;->h:Ljava/lang/String;

    .line 1700309
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ag7;->n:Ljava/util/HashSet;

    .line 1700310
    iput-object p8, p0, LX/Ag7;->l:LX/0Zc;

    .line 1700311
    return-void
.end method

.method public static a(LX/Ag7;J)V
    .locals 5

    .prologue
    .line 1700290
    iget-object v0, p0, LX/Ag7;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Ag7;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->q()J

    move-result-wide v0

    const-wide/32 v2, 0x15f90

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1700291
    const-string v0, "BatchPollingInBackground"

    .line 1700292
    iget-object v1, p0, LX/Ag7;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1700293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Ag7;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1700294
    :cond_0
    iget-object v1, p0, LX/Ag7;->d:LX/03V;

    const-string v2, "Batch polling while app is in background, delayMs=%d, video_ids=%s"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1700295
    :cond_1
    iget-object v0, p0, LX/Ag7;->g:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/livepoller/LiveStatusBatchPoller$2;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/livepoller/LiveStatusBatchPoller$2;-><init>(LX/Ag7;)V

    const v2, 0x1008f453

    invoke-static {v0, v1, p1, p2, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1700296
    return-void
.end method

.method public static c(LX/Ag7;)V
    .locals 10

    .prologue
    .line 1700268
    iget v6, p0, LX/Ag7;->p:I

    if-gtz v6, :cond_2

    .line 1700269
    :goto_0
    new-instance v0, LX/6Tp;

    invoke-direct {v0}, LX/6Tp;-><init>()V

    move-object v0, v0

    .line 1700270
    monitor-enter p0

    .line 1700271
    :try_start_0
    iget-object v1, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1700272
    invoke-virtual {p0}, LX/Ag7;->a()V

    .line 1700273
    monitor-exit p0

    .line 1700274
    :goto_1
    return-void

    .line 1700275
    :cond_0
    const-string v1, "video_ids"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1700276
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700277
    const-string v1, "enable_read_only_viewer_count"

    iget-boolean v2, p0, LX/Ag7;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1700278
    iget-object v1, p0, LX/Ag7;->i:LX/19j;

    iget-boolean v1, v1, LX/19j;->S:Z

    if-eqz v1, :cond_1

    .line 1700279
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1700280
    :cond_1
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1700281
    iget-object v1, p0, LX/Ag7;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1700282
    const-string v1, "polling"

    iput-object v1, p0, LX/Ag7;->s:Ljava/lang/String;

    .line 1700283
    iget-object v1, p0, LX/Ag7;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/Ag7;->t:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/Ag7;->u:J

    .line 1700284
    iget-object v1, p0, LX/Ag7;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/Ag7;->t:J

    .line 1700285
    invoke-static {p0}, LX/Ag7;->e(LX/Ag7;)V

    .line 1700286
    iget-object v1, p0, LX/Ag7;->l:LX/0Zc;

    const-string v2, ""

    sget-object v3, LX/7IM;->LIVE_POLLER_BATCH_START:LX/7IM;

    const-string v4, "Batch poller started"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700287
    iget-object v1, p0, LX/Ag7;->e:LX/1Ck;

    iget-object v2, p0, LX/Ag7;->h:Ljava/lang/String;

    new-instance v3, LX/Ag6;

    invoke-direct {v3, p0}, LX/Ag6;-><init>(LX/Ag7;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    .line 1700288
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1700289
    :cond_2
    iget v6, p0, LX/Ag7;->p:I

    int-to-long v6, v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {p0, v6, v7}, LX/Ag7;->a(LX/Ag7;J)V

    goto/16 :goto_0
.end method

.method public static e(LX/Ag7;)V
    .locals 6

    .prologue
    .line 1700254
    iget-object v0, p0, LX/Ag7;->j:LX/3Hd;

    invoke-virtual {v0}, LX/3Hd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1700255
    :goto_0
    return-void

    .line 1700256
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1700257
    iget-object v0, p0, LX/Ag7;->v:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1700258
    iget-object v0, p0, LX/Ag7;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1700259
    iget-object v1, p0, LX/Ag7;->v:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;

    .line 1700260
    if-eqz v1, :cond_1

    .line 1700261
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->k()I

    move-result v4

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchViewerCountAndBroadcastStatusQueryModels$FetchViewerCountAndBroadcastStatusQueryModel;->l()I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1700262
    const-string v4, "id "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1700263
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " viewers\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1700264
    :cond_2
    const-string v0, "readOnly: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/Ag7;->o:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1700265
    const-string v0, "\ndurationBetweenLastPolls: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, LX/Ag7;->u:J

    long-to-float v1, v4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sec"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1700266
    const-string v0, "\nstatus: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Ag7;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1700267
    iget-object v0, p0, LX/Ag7;->j:LX/3Hd;

    sget-object v1, LX/Ag7;->b:Ljava/lang/String;

    const-string v3, "no_video_id"

    invoke-virtual {v0, v1, v2, v3}, LX/3Hd;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1700251
    iget-object v0, p0, LX/Ag7;->e:LX/1Ck;

    iget-object v1, p0, LX/Ag7;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1700252
    iget-object v0, p0, LX/Ag7;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1700253
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700247
    iput-object p1, p0, LX/Ag7;->r:Ljava/lang/String;

    .line 1700248
    invoke-virtual {p0}, LX/Ag7;->a()V

    .line 1700249
    const-wide/16 v0, 0xa

    invoke-static {p0, v0, v1}, LX/Ag7;->a(LX/Ag7;J)V

    .line 1700250
    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1700241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    .line 1700242
    iget-object v1, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1700243
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Ag7;->q:Z

    if-eqz v0, :cond_0

    .line 1700244
    const-string v0, "registerFirstVideo"

    invoke-virtual {p0, v0}, LX/Ag7;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700245
    :cond_0
    monitor-exit p0

    return-void

    .line 1700246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1700236
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1700237
    iget-object v0, p0, LX/Ag7;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1700238
    invoke-virtual {p0}, LX/Ag7;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700239
    :cond_0
    monitor-exit p0

    return-void

    .line 1700240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
