.class public LX/CXj;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/CXk;",
        "LX/CXi;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/CXj;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910616
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1910617
    return-void
.end method

.method public static a(LX/0QB;)LX/CXj;
    .locals 3

    .prologue
    .line 1910618
    sget-object v0, LX/CXj;->a:LX/CXj;

    if-nez v0, :cond_1

    .line 1910619
    const-class v1, LX/CXj;

    monitor-enter v1

    .line 1910620
    :try_start_0
    sget-object v0, LX/CXj;->a:LX/CXj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1910621
    if-eqz v2, :cond_0

    .line 1910622
    :try_start_1
    new-instance v0, LX/CXj;

    invoke-direct {v0}, LX/CXj;-><init>()V

    .line 1910623
    move-object v0, v0

    .line 1910624
    sput-object v0, LX/CXj;->a:LX/CXj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1910625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1910626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1910627
    :cond_1
    sget-object v0, LX/CXj;->a:LX/CXj;

    return-object v0

    .line 1910628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1910629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
