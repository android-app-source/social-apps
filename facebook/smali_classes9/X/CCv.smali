.class public LX/CCv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/1vb;

.field public final c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final d:LX/0tT;

.field public final e:LX/1VE;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1vg;LX/1vb;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0tT;LX/1VE;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/1vb;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0tT;",
            "LX/1VE;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858503
    iput-object p2, p0, LX/CCv;->b:LX/1vb;

    .line 1858504
    iput-object p1, p0, LX/CCv;->a:LX/1vg;

    .line 1858505
    iput-object p3, p0, LX/CCv;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1858506
    iput-object p4, p0, LX/CCv;->d:LX/0tT;

    .line 1858507
    iput-object p5, p0, LX/CCv;->e:LX/1VE;

    .line 1858508
    iput-object p6, p0, LX/CCv;->f:LX/0Ot;

    .line 1858509
    return-void
.end method

.method public static a(LX/0QB;)LX/CCv;
    .locals 10

    .prologue
    .line 1858510
    const-class v1, LX/CCv;

    monitor-enter v1

    .line 1858511
    :try_start_0
    sget-object v0, LX/CCv;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858512
    sput-object v2, LX/CCv;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858513
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858514
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858515
    new-instance v3, LX/CCv;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v5

    check-cast v5, LX/1vb;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0tT;->b(LX/0QB;)LX/0tT;

    move-result-object v7

    check-cast v7, LX/0tT;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v8

    check-cast v8, LX/1VE;

    const/16 v9, 0xbc

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/CCv;-><init>(LX/1vg;LX/1vb;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0tT;LX/1VE;LX/0Ot;)V

    .line 1858516
    move-object v0, v3

    .line 1858517
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858518
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858519
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pk;)LX/1Dg;
    .locals 12
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pk;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1858483
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1858484
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;->a()Lcom/facebook/graphql/model/GraphQLFollowableTopic;

    move-result-object v0

    .line 1858485
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 1858486
    iget-object v3, p0, LX/CCv;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v5, 0x7f020a08

    invoke-virtual {v3, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v5, 0x7f0a008a

    invoke-virtual {v3, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 1858487
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v5}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x1

    const v6, 0x7f0b1dbb

    invoke-interface {v3, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v5, 0x5

    const v6, 0x7f0b1db4

    invoke-interface {v3, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 1858488
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1858489
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v6, p0, LX/CCv;->d:LX/0tT;

    .line 1858490
    iget-object v8, v6, LX/0tT;->a:LX/0W3;

    sget-wide v10, LX/0X5;->hy:J

    invoke-interface {v8, v10, v11}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v8

    move-object v6, v8

    .line 1858491
    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x1010038

    invoke-virtual {v5, v6}, LX/1ne;->o(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x1

    const v7, 0x7f0b1dba

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v7, 0x7f0b1db5

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    move-object v3, v5

    .line 1858492
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFollowableTopic;->k()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    .line 1858493
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0050

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00aa

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b1dba

    invoke-interface {v3, v6, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v3, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    .line 1858494
    const v5, -0x3d326fd2

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1858495
    invoke-interface {v3, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    move-object v0, v3

    .line 1858496
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 1858497
    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v2

    .line 1858498
    iget-object v3, p0, LX/CCv;->b:LX/1vb;

    invoke-virtual {v3, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v3

    iget-object v5, p0, LX/CCv;->e:LX/1VE;

    invoke-virtual {v5, p2, v2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v2

    move-object v2, v2

    .line 1858499
    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b1db8    # 1.84917E38f

    invoke-interface {v0, v4, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x3

    const v3, 0x7f0b1db9

    invoke-interface {v0, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 1858500
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a07d9

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v3, 0x7f0b1db7

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    move-object v1, v1

    .line 1858501
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
