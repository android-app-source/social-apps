.class public LX/Bn2;
.super LX/0gG;
.source ""


# instance fields
.field private a:LX/Bn9;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Bn8;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0Px;LX/Bn9;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;",
            ">;",
            "LX/Bn9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1819768
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1819769
    iput-object p1, p0, LX/Bn2;->b:Ljava/lang/String;

    .line 1819770
    iput-object p2, p0, LX/Bn2;->c:Ljava/lang/String;

    .line 1819771
    iput-object p5, p0, LX/Bn2;->a:LX/Bn9;

    .line 1819772
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Bn2;->d:I

    .line 1819773
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Bn2;->e:Ljava/util/Set;

    .line 1819774
    iput-object p4, p0, LX/Bn2;->f:LX/0Px;

    .line 1819775
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1819767
    iget-object v0, p0, LX/Bn2;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1819751
    iget-object v1, p0, LX/Bn2;->a:LX/Bn9;

    iget-object v2, p0, LX/Bn2;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Bn2;->c:Ljava/lang/String;

    iget v0, p0, LX/Bn2;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, p0, LX/Bn2;->f:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/Bn9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Bn8;

    move-result-object v0

    .line 1819752
    iget-object v1, p0, LX/Bn2;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1819753
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1819763
    check-cast p3, LX/Bn8;

    .line 1819764
    iget-object v0, p0, LX/Bn2;->e:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1819765
    invoke-virtual {p3, p1}, LX/Bn8;->b(Landroid/view/ViewGroup;)V

    .line 1819766
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1819760
    check-cast p2, LX/Bn8;

    .line 1819761
    iget-object v0, p2, LX/Bn8;->e:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1819762
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1819759
    iget-object v0, p0, LX/Bn2;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1819754
    check-cast p3, LX/Bn8;

    .line 1819755
    iget-boolean p0, p3, LX/Bn8;->k:Z

    if-nez p0, :cond_0

    .line 1819756
    invoke-static {p3}, LX/Bn8;->a(LX/Bn8;)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1819757
    const/4 p0, 0x1

    iput-boolean p0, p3, LX/Bn8;->k:Z

    .line 1819758
    :cond_0
    return-void
.end method
