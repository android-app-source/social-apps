.class public final enum LX/Abe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Abe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Abe;

.field public static final enum COUNTDOWN_ENDED:LX/Abe;

.field public static final enum COUNTDOWN_STARTED:LX/Abe;

.field public static final enum PRELOBBY:LX/Abe;

.field public static final enum RUNNING_LATE:LX/Abe;

.field public static final enum TIMED_OUT:LX/Abe;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1691108
    new-instance v0, LX/Abe;

    const-string v1, "PRELOBBY"

    invoke-direct {v0, v1, v2}, LX/Abe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abe;->PRELOBBY:LX/Abe;

    .line 1691109
    new-instance v0, LX/Abe;

    const-string v1, "COUNTDOWN_STARTED"

    invoke-direct {v0, v1, v3}, LX/Abe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abe;->COUNTDOWN_STARTED:LX/Abe;

    .line 1691110
    new-instance v0, LX/Abe;

    const-string v1, "COUNTDOWN_ENDED"

    invoke-direct {v0, v1, v4}, LX/Abe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    .line 1691111
    new-instance v0, LX/Abe;

    const-string v1, "RUNNING_LATE"

    invoke-direct {v0, v1, v5}, LX/Abe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abe;->RUNNING_LATE:LX/Abe;

    .line 1691112
    new-instance v0, LX/Abe;

    const-string v1, "TIMED_OUT"

    invoke-direct {v0, v1, v6}, LX/Abe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abe;->TIMED_OUT:LX/Abe;

    .line 1691113
    const/4 v0, 0x5

    new-array v0, v0, [LX/Abe;

    sget-object v1, LX/Abe;->PRELOBBY:LX/Abe;

    aput-object v1, v0, v2

    sget-object v1, LX/Abe;->COUNTDOWN_STARTED:LX/Abe;

    aput-object v1, v0, v3

    sget-object v1, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    aput-object v1, v0, v4

    sget-object v1, LX/Abe;->RUNNING_LATE:LX/Abe;

    aput-object v1, v0, v5

    sget-object v1, LX/Abe;->TIMED_OUT:LX/Abe;

    aput-object v1, v0, v6

    sput-object v0, LX/Abe;->$VALUES:[LX/Abe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1691114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Abe;
    .locals 1

    .prologue
    .line 1691115
    const-class v0, LX/Abe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Abe;

    return-object v0
.end method

.method public static values()[LX/Abe;
    .locals 1

    .prologue
    .line 1691116
    sget-object v0, LX/Abe;->$VALUES:[LX/Abe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Abe;

    return-object v0
.end method
