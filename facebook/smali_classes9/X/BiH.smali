.class public LX/BiH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1810136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810137
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1810090
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static a(LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 1810125
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1810126
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1810127
    :goto_0
    return-object v0

    .line 1810128
    :cond_1
    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1810129
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v4

    .line 1810130
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1810131
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 1810132
    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1810133
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v0

    add-int/2addr v0, v6

    const/16 v6, 0x21

    invoke-virtual {v3, v5, v1, v0, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1810134
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 1810135
    goto :goto_0
.end method

.method public static a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 1810108
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->j()LX/175;

    move-result-object v0

    .line 1810109
    if-nez v0, :cond_0

    .line 1810110
    const-string v0, ""

    .line 1810111
    :goto_0
    return-object v0

    .line 1810112
    :cond_0
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1810113
    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1810114
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1810115
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1810116
    sget-object v2, LX/BiG;->a:[I

    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1810117
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->j()LX/175;

    move-result-object v1

    invoke-static {v1}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v0, v1}, LX/BiH;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 1810118
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1810119
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810120
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1810121
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810122
    :pswitch_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810123
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1810124
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 1810091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1810092
    if-nez v0, :cond_0

    .line 1810093
    const-string v0, ""

    .line 1810094
    :goto_0
    return-object v0

    .line 1810095
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1810096
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1810097
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1810098
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1810099
    sget-object v2, LX/BiG;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1810100
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v0, v1}, LX/BiH;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 1810101
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1810102
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810103
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1810104
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810105
    :pswitch_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1810106
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1810107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/BiH;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
