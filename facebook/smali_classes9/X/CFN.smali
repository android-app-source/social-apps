.class public final LX/CFN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V
    .locals 0

    .prologue
    .line 1863139
    iput-object p1, p0, LX/CFN;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6b46e43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863123
    iget-object v1, p0, LX/CFN;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v1, v1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    if-eqz v1, :cond_0

    .line 1863124
    iget-object v1, p0, LX/CFN;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v1, v1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    iget-object v2, p0, LX/CFN;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1863125
    iget-object v4, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v4}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v4

    .line 1863126
    iget-object v7, v2, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    move-object v7, v7

    .line 1863127
    invoke-virtual {v4, v7}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v13

    .line 1863128
    iget-object v4, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1863129
    iget-object v7, v2, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    move-object v7, v7

    .line 1863130
    invoke-virtual {v4, v7}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863131
    iget-object v4, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1863132
    iget-object v4, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v13, v4, :cond_0

    .line 1863133
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getHeight()I

    move-result v7

    int-to-float v10, v7

    move v7, v5

    move v8, v6

    move v11, v5

    move v12, v6

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 1863134
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0064

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    move v5, v13

    .line 1863135
    :goto_0
    iget-object v6, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1863136
    iget-object v6, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1863137
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1863138
    :cond_0
    const v1, -0x66bb2c57

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
