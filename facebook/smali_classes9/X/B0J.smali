.class public final LX/B0J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0io;
.implements LX/0ip;
.implements LX/0iq;
.implements LX/0ir;
.implements LX/0is;
.implements LX/0iu;
.implements LX/5Qv;
.implements LX/0iw;
.implements LX/2rg;
.implements LX/0ix;
.implements LX/0iy;
.implements LX/0iz;
.implements LX/0j0;
.implements LX/0j1;
.implements LX/0j2;
.implements LX/0ik;
.implements LX/0il;
.implements LX/0j3;
.implements LX/0j4;
.implements LX/0j5;
.implements LX/0j6;
.implements LX/5RE;
.implements LX/0j7;
.implements LX/0j8;
.implements LX/0j9;
.implements LX/0jA;
.implements LX/0jB;
.implements LX/0jC;
.implements LX/0jD;
.implements LX/0jE;
.implements LX/0jF;
.implements LX/0jG;
.implements LX/0jH;
.implements LX/0jI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0io;",
        "LX/0ip;",
        "LX/0iq;",
        "LX/0ir;",
        "LX/0is;",
        "LX/0iu;",
        "LX/5Qv;",
        "LX/0iw;",
        "LX/2rg;",
        "LX/0ix;",
        "LX/0iy;",
        "LX/0iz;",
        "LX/0j0;",
        "LX/0j1;",
        "LX/0j2;",
        "LX/0ik",
        "<",
        "LX/B0J;",
        ">;",
        "LX/0il",
        "<",
        "LX/B0J;",
        ">;",
        "LX/0j3;",
        "LX/0j4;",
        "LX/0j5;",
        "LX/0j6;",
        "LX/5RE;",
        "LX/0j7;",
        "LX/0j8;",
        "LX/0j9;",
        "LX/0jA;",
        "LX/0jB;",
        "LX/0jC;",
        "LX/0jD;",
        "LX/0jE;",
        "LX/0jF;",
        "LX/0jG;",
        "LX/0jH;",
        "LX/0jI;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0K;

.field private final b:Ljava/lang/String;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/B0K;Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 1
    .param p3    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733615
    iput-object p1, p0, LX/B0J;->a:LX/B0K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733616
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B0J;->b:Ljava/lang/String;

    .line 1733617
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B0J;->c:LX/0Px;

    .line 1733618
    iput-object p3, p0, LX/B0J;->d:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1733619
    return-void
.end method


# virtual methods
.method public final I()LX/5RF;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1733681
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733682
    sget-object v0, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    .line 1733683
    :goto_0
    return-object v0

    .line 1733684
    :cond_0
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-static {v0}, LX/7kq;->k(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1733685
    sget-object v0, LX/5RF;->GIF_VIDEO:LX/5RF;

    goto :goto_0

    .line 1733686
    :cond_1
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-static {v0}, LX/7kq;->o(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1733687
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    sget-object v0, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    goto :goto_0

    :cond_2
    sget-object v0, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    goto :goto_0

    .line 1733688
    :cond_3
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-static {v0}, LX/7kq;->m(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1733689
    sget-object v0, LX/5RF;->MULTIMEDIA:LX/5RF;

    goto :goto_0

    .line 1733690
    :cond_4
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_5

    sget-object v0, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    goto :goto_0

    :cond_5
    sget-object v0, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1733691
    return-object p0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1733692
    return-object p0
.end method

.method public final getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1

    .prologue
    .line 1733693
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAttachments()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1733694
    iget-object v0, p0, LX/B0J;->c:LX/0Px;

    return-object v0
.end method

.method public final getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
    .locals 1

    .prologue
    .line 1733695
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 2

    .prologue
    .line 1733696
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->STATUS:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 1

    .prologue
    .line 1733697
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getMarketplaceId()J
    .locals 2

    .prologue
    .line 1733705
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733698
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733699
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1

    .prologue
    .line 1733700
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733701
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPublishMode()LX/5Rn;
    .locals 1

    .prologue
    .line 1733702
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    return-object v0
.end method

.method public final getPublishScheduleTime()Ljava/lang/Long;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733703
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733680
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733704
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1733614
    iget-object v0, p0, LX/B0J;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1

    .prologue
    .line 1733620
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733621
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733622
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTaggedUsers()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1733623
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1733624
    return-object v0
.end method

.method public final getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 1

    .prologue
    .line 1733625
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4

    .prologue
    .line 1733626
    new-instance v0, LX/89I;

    iget-object v1, p0, LX/B0J;->a:LX/B0K;

    iget-object v1, v1, LX/B0K;->d:Lcom/facebook/user/model/User;

    .line 1733627
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1733628
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v1, p0, LX/B0J;->a:LX/B0K;

    iget-object v1, v1, LX/B0K;->d:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    .line 1733629
    iput-object v1, v0, LX/89I;->c:Ljava/lang/String;

    .line 1733630
    move-object v0, v0

    .line 1733631
    iget-object v1, p0, LX/B0J;->a:LX/B0K;

    iget-object v1, v1, LX/B0K;->d:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    .line 1733632
    iput-object v1, v0, LX/89I;->d:Ljava/lang/String;

    .line 1733633
    move-object v0, v0

    .line 1733634
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    return-object v0
.end method

.method public final getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 1733635
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 1733636
    const/4 v1, 0x0

    .line 1733637
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 1733638
    move-object v0, v0

    .line 1733639
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1733640
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1733641
    const/4 v0, 0x0

    return v0
.end method

.method public final isBackoutDraft()Z
    .locals 1

    .prologue
    .line 1733642
    const/4 v0, 0x0

    return v0
.end method

.method public final isFeedOnlyPost()Z
    .locals 1

    .prologue
    .line 1733643
    const/4 v0, 0x0

    return v0
.end method

.method public final isUserSelectedTags()Z
    .locals 1

    .prologue
    .line 1733644
    const/4 v0, 0x0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1733645
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;
    .locals 1

    .prologue
    .line 1733646
    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
    .locals 4

    .prologue
    .line 1733647
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1733648
    iget-object v0, p0, LX/B0J;->d:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    .line 1733649
    new-instance v0, LX/7lP;

    invoke-direct {v0}, LX/7lP;-><init>()V

    .line 1733650
    iput-boolean v3, v0, LX/7lP;->a:Z

    .line 1733651
    move-object v0, v0

    .line 1733652
    iput-boolean v2, v0, LX/7lP;->b:Z

    .line 1733653
    move-object v0, v0

    .line 1733654
    iget-object v1, p0, LX/B0J;->d:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v1}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1733655
    :goto_0
    move-object v0, v0

    .line 1733656
    return-object v0

    .line 1733657
    :cond_0
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    const-string v1, "Good Friends"

    .line 1733658
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1733659
    move-object v0, v0

    .line 1733660
    const-string v1, "Good Friends Tooltip"

    .line 1733661
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1733662
    move-object v0, v0

    .line 1733663
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1733664
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1733665
    move-object v0, v0

    .line 1733666
    iget-object v1, p0, LX/B0J;->a:LX/B0K;

    iget-object v1, v1, LX/B0K;->e:Ljava/lang/String;

    .line 1733667
    iput-object v1, v0, LX/7lN;->d:Ljava/lang/String;

    .line 1733668
    move-object v0, v0

    .line 1733669
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1733670
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    .line 1733671
    iput-boolean v3, v1, LX/7lP;->a:Z

    .line 1733672
    move-object v1, v1

    .line 1733673
    iput-boolean v2, v1, LX/7lP;->b:Z

    .line 1733674
    move-object v1, v1

    .line 1733675
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->a:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1733676
    iput-object v1, v0, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1733677
    move-object v0, v0

    .line 1733678
    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    goto :goto_0
.end method

.method public final r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
    .locals 1

    .prologue
    .line 1733679
    const/4 v0, 0x0

    return-object v0
.end method
