.class public LX/BNw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2l6;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/saved/common/sync/SavedEventHandler$Listener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2l6;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/saved/common/sync/SavedEventHandler$Listener;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1779319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779320
    iput-object p1, p0, LX/BNw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1779321
    iput-object p2, p0, LX/BNw;->b:LX/0Ot;

    .line 1779322
    iput-object p3, p0, LX/BNw;->c:LX/0Ot;

    .line 1779323
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1779311
    iget-object v0, p0, LX/BNw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1vE;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1779312
    iget-object v0, p0, LX/BNw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2l6;

    .line 1779313
    invoke-interface {v0}, LX/2l6;->a()V

    goto :goto_0

    .line 1779314
    :cond_0
    iget-object v0, p0, LX/BNw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1779315
    :cond_1
    iget-object v0, p0, LX/BNw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVz;

    .line 1779316
    iget-object v2, v0, LX/FVz;->b:LX/0Sh;

    new-instance p0, Lcom/facebook/saved/data/SavedDashboardPrefetcher$1;

    invoke-direct {p0, v0}, Lcom/facebook/saved/data/SavedDashboardPrefetcher$1;-><init>(LX/FVz;)V

    invoke-virtual {v2, p0}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1779317
    goto :goto_1

    .line 1779318
    :cond_2
    return-void
.end method
