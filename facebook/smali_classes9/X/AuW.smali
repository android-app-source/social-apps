.class public LX/AuW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9dM;


# instance fields
.field private final a:LX/9dK;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/0Px;LX/AuV;)V
    .locals 0
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "LX/AuV;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722667
    iput-object p1, p0, LX/AuW;->b:LX/0Px;

    .line 1722668
    iput-object p2, p0, LX/AuW;->a:LX/9dK;

    .line 1722669
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1722670
    iget-boolean v0, p0, LX/AuW;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1722671
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AuW;->c:Z

    .line 1722672
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1722673
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/9dK;
    .locals 1

    .prologue
    .line 1722674
    iget-object v0, p0, LX/AuW;->a:LX/9dK;

    return-object v0
.end method
