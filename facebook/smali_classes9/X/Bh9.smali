.class public LX/Bh9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;",
        "LX/97f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;

.field public final b:LX/BfJ;


# direct methods
.method public constructor <init>(LX/03V;LX/BfJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808792
    iput-object p1, p0, LX/Bh9;->a:LX/03V;

    .line 1808793
    iput-object p2, p0, LX/Bh9;->b:LX/BfJ;

    .line 1808794
    return-void
.end method

.method private static a(LX/Bh9;LX/0Px;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$CrowdsourcedField$$UserValues$$Edges$;",
            ">;",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$CrowdsourcedField$$UserValues$$Edges$;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1808842
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1808843
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    const/4 v2, 0x0

    .line 1808844
    invoke-static {v0}, LX/Bh9;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p2}, LX/Bh9;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1808845
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1808846
    invoke-virtual {v4, v3, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_0
    move v0, v2

    .line 1808847
    if-eqz v0, :cond_1

    .line 1808848
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1808849
    :goto_1
    return-object v0

    .line 1808850
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1808851
    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(LX/Bh9;LX/97f;ILX/BgS;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1808841
    new-instance v0, LX/Bh7;

    invoke-direct {v0, p0, p3, p1, p2}, LX/Bh7;-><init>(LX/Bh9;LX/BgS;LX/97f;I)V

    return-object v0
.end method

.method private static a(LX/Bh9;LX/97f;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;LX/BgS;)Landroid/view/View$OnClickListener;
    .locals 6

    .prologue
    .line 1808840
    new-instance v0, LX/Bh8;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/Bh8;-><init>(LX/Bh9;LX/BgS;LX/97f;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)V

    return-object v0
.end method

.method private static a(LX/Bh9;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    .line 1808836
    invoke-virtual {p4}, LX/BeD;->getInputStyle()LX/BeC;

    move-result-object v0

    sget-object v1, LX/BeC;->PICKER:LX/BeC;

    if-eq v0, v1, :cond_0

    .line 1808837
    iget-object v0, p0, LX/Bh9;->a:LX/03V;

    const-string v1, "SuggestEdits"

    const-string v2, "Input style not supported by field"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808838
    :goto_0
    return-void

    .line 1808839
    :cond_0
    new-instance v0, LX/Bh6;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/Bh6;-><init>(LX/Bh9;LX/97f;LX/BeD;LX/BgS;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->setAddValueListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;LX/97f;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 9
    .param p3    # LX/97f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808808
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a()V

    .line 1808809
    if-nez p3, :cond_0

    .line 1808810
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1808811
    move-object v1, v0

    .line 1808812
    :goto_0
    invoke-static {p2}, LX/Bgb;->g(LX/97f;)LX/0Px;

    move-result-object v4

    .line 1808813
    const/4 v2, 0x0

    .line 1808814
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    .line 1808815
    invoke-static {v0}, LX/Bh9;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1808816
    invoke-static {p0, v4, v0}, LX/Bh9;->a(LX/Bh9;LX/0Px;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/0am;

    move-result-object v6

    .line 1808817
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1808818
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1808819
    const/4 v8, 0x1

    invoke-virtual {v7, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, p2, v0, p4}, LX/Bh9;->a(LX/Bh9;LX/97f;ILX/BgS;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v7, v8, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1808820
    add-int/lit8 v0, v2, 0x1

    .line 1808821
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 1808822
    :cond_0
    invoke-static {p3}, LX/Bgb;->g(LX/97f;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1808823
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1808824
    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, p2, v2, v0, p4}, LX/Bh9;->a(LX/Bh9;LX/97f;ILcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;LX/BgS;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v6, v7, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    :cond_2
    move v0, v2

    goto :goto_2

    .line 1808825
    :cond_3
    const/4 v0, 0x0

    move v2, v0

    :goto_3
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1808826
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    .line 1808827
    invoke-static {p0, v1, v0}, LX/Bh9;->a(LX/Bh9;LX/0Px;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/0am;

    move-result-object v3

    .line 1808828
    invoke-static {v0}, LX/Bh9;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1808829
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1808830
    const/4 v5, 0x1

    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p2, v2, p4}, LX/Bh9;->a(LX/Bh9;LX/97f;ILX/BgS;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {p1, v0, v3, v5}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1808831
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1808832
    :cond_5
    invoke-interface {p2}, LX/97e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->setAddText(Ljava/lang/String;)V

    .line 1808833
    if-eqz p5, :cond_6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 1808834
    invoke-static/range {v0 .. v5}, LX/Bh9;->a(LX/Bh9;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V

    .line 1808835
    :cond_6
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1808801
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1808802
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1808803
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1808804
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1808805
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 1808806
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1808807
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808800
    sget-object v0, LX/BeE;->MULTI_VALUE_FIELD:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808798
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808799
    const v1, 0x7f031423

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808796
    check-cast p2, LX/97f;

    .line 1808797
    return-object p2
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808795
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;

    move-object v2, p2

    check-cast v2, LX/97f;

    move-object v3, p3

    check-cast v3, LX/97f;

    move-object v0, p0

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Bh9;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;LX/97f;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V

    return-void
.end method
