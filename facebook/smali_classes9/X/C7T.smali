.class public final LX/C7T;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

.field public final synthetic b:LX/C7V;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C7V;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1851378
    iput-object p1, p0, LX/C7T;->d:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iput-object p2, p0, LX/C7T;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    iput-object p3, p0, LX/C7T;->b:LX/C7V;

    iput-object p4, p0, LX/C7T;->c:LX/1Pq;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1851371
    iget-object v0, p0, LX/C7T;->d:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iget-object v1, p0, LX/C7T;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    iget-object v2, p0, LX/C7T;->b:LX/C7V;

    .line 1851372
    iget-object v3, v2, LX/C7V;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1851373
    iget-object v3, p0, LX/C7T;->b:LX/C7V;

    .line 1851374
    iget-object v4, v3, LX/C7V;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1851375
    iget-object v4, p0, LX/C7T;->c:LX/1Pq;

    .line 1851376
    iget-object p0, v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->g:LX/1e7;

    new-instance p1, LX/C7U;

    invoke-direct {p1, v0, v4, v1}, LX/C7U;-><init>(Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;LX/1Pq;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V

    invoke-virtual {p0, v3, v2, p1}, LX/1e7;->a(Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V

    .line 1851377
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1851368
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1851369
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1851370
    return-void
.end method
