.class public final LX/BYC;
.super Landroid/widget/Scroller;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

.field public b:D


# direct methods
.method public constructor <init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 2

    .prologue
    .line 1794133
    iput-object p1, p0, LX/BYC;->a:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 1794134
    invoke-direct {p0, p2, p3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 1794135
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/BYC;->b:D

    .line 1794136
    return-void
.end method


# virtual methods
.method public final startScroll(IIIII)V
    .locals 6

    .prologue
    .line 1794137
    int-to-double v0, p5

    iget-wide v2, p0, LX/BYC;->b:D

    mul-double/2addr v0, v2

    double-to-int v5, v0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-super/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1794138
    return-void
.end method
