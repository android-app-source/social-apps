.class public LX/C4M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C4N;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4M",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C4N;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847155
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847156
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C4M;->b:LX/0Zi;

    .line 1847157
    iput-object p1, p0, LX/C4M;->a:LX/0Ot;

    .line 1847158
    return-void
.end method

.method public static a(LX/0QB;)LX/C4M;
    .locals 4

    .prologue
    .line 1847144
    const-class v1, LX/C4M;

    monitor-enter v1

    .line 1847145
    :try_start_0
    sget-object v0, LX/C4M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847146
    sput-object v2, LX/C4M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847147
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847148
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847149
    new-instance v3, LX/C4M;

    const/16 p0, 0x1ef2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C4M;-><init>(LX/0Ot;)V

    .line 1847150
    move-object v0, v3

    .line 1847151
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847152
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847153
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1847141
    check-cast p2, LX/C4L;

    .line 1847142
    iget-object v0, p0, LX/C4M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4N;

    iget-object v2, p2, LX/C4L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C4L;->b:LX/1Pr;

    iget v4, p2, LX/C4L;->c:I

    iget v5, p2, LX/C4L;->d:I

    iget-boolean v6, p2, LX/C4L;->e:Z

    iget-boolean v7, p2, LX/C4L;->f:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/C4N;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;IIZZ)LX/1Dg;

    move-result-object v0

    .line 1847143
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847131
    invoke-static {}, LX/1dS;->b()V

    .line 1847132
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C4K;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C4M",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1847133
    new-instance v1, LX/C4L;

    invoke-direct {v1, p0}, LX/C4L;-><init>(LX/C4M;)V

    .line 1847134
    iget-object v2, p0, LX/C4M;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C4K;

    .line 1847135
    if-nez v2, :cond_0

    .line 1847136
    new-instance v2, LX/C4K;

    invoke-direct {v2, p0}, LX/C4K;-><init>(LX/C4M;)V

    .line 1847137
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C4K;->a$redex0(LX/C4K;LX/1De;IILX/C4L;)V

    .line 1847138
    move-object v1, v2

    .line 1847139
    move-object v0, v1

    .line 1847140
    return-object v0
.end method
