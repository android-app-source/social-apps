.class public final LX/CcG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1920761
    iput-object p1, p0, LX/CcG;->e:LX/CcO;

    iput-object p2, p0, LX/CcG;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CcG;->b:LX/5kD;

    iput-object p4, p0, LX/CcG;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CcG;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 1920762
    iget-object v0, p0, LX/CcG;->e:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    const/16 v1, 0x7d3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/CcG;->e:LX/CcO;

    iget-object v2, v2, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    iget-object v3, p0, LX/CcG;->a:Landroid/content/Context;

    .line 1920763
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1920764
    new-instance v5, Landroid/text/SpannableString;

    const v6, 0x7f0819bd

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1920765
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1920766
    new-instance v6, LX/0ju;

    invoke-direct {v6, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0819ba

    invoke-virtual {v6, p1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    const v6, 0x7f0819c2

    new-instance p1, LX/CbY;

    invoke-direct {p1, v2, v4}, LX/CbY;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const v6, 0x7f0819c1

    new-instance p1, LX/CbX;

    invoke-direct {p1, v2, v4}, LX/CbX;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    .line 1920767
    move-object v2, v4

    .line 1920768
    iget-object v3, p0, LX/CcG;->e:LX/CcO;

    iget-object v4, p0, LX/CcG;->b:LX/5kD;

    iget-object v5, p0, LX/CcG;->c:Ljava/lang/String;

    iget-object v6, p0, LX/CcG;->d:Ljava/lang/String;

    .line 1920769
    new-instance p0, LX/Cc6;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Cc6;-><init>(LX/CcO;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, p0

    .line 1920770
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1920771
    const/4 v0, 0x1

    return v0
.end method
