.class public LX/C84;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1852294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;
    .locals 2

    .prologue
    .line 1852295
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const v1, 0x3404bdf5

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1852296
    if-nez v0, :cond_0

    .line 1852297
    const/4 v0, 0x0

    .line 1852298
    :goto_1
    return-object v0

    :cond_0
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const v1, 0x3404bdf5

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroup;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroup;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1852299
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1852300
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1852301
    :goto_0
    return-object v0

    .line 1852302
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->T()Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupMemberProfilesConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1852303
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1852304
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1852305
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1852306
    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLGroup;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroup;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1852307
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1852308
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->j()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1852309
    :cond_0
    const/4 v0, 0x0

    .line 1852310
    :goto_0
    return-object v0

    .line 1852311
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->q()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSet;->j()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1852312
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1852313
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1852314
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1852315
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2

    .prologue
    .line 1852316
    invoke-static {p0}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 1852317
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1852318
    :cond_0
    const/4 v0, 0x0

    .line 1852319
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->U()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method
