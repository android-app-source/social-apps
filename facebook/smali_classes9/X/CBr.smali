.class public final enum LX/CBr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBr;

.field public static final enum ARGB_BACKGROUND_COLOR:LX/CBr;

.field public static final enum ARGB_TEXT_COLOR:LX/CBr;

.field public static final enum CONTENT_BLOCK_BOTTOM_MARGIN:LX/CBr;

.field public static final enum FAVICON_COLOR:LX/CBr;

.field public static final enum FEED_TAP_ACTION:LX/CBr;

.field public static final enum IMAGE_OVERLAY:LX/CBr;

.field public static final enum NEWSFEED_BRANDING_STYLE:LX/CBr;

.field public static final enum NEWSFEED_LARGE_IMAGE_COUNTER_TYPE:LX/CBr;

.field public static final enum NEWSFEED_LARGE_IMAGE_LOCATION:LX/CBr;

.field public static final enum NEWSFEED_LARGE_IMAGE_MARGIN_OPTION:LX/CBr;

.field public static final enum NEWSFEED_LARGE_IMAGE_PROFILE_PHOTO:LX/CBr;

.field public static final enum PRIMARY_ACTION_BUTTON_GLYPH:LX/CBr;

.field public static final enum PRIMARY_ACTION_BUTTON_STYLE:LX/CBr;

.field public static final enum SECONDARY_ACTION_BUTTON_GLYPH:LX/CBr;

.field public static final enum UNKNOWN:LX/CBr;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1856865
    new-instance v0, LX/CBr;

    const-string v1, "ARGB_BACKGROUND_COLOR"

    invoke-direct {v0, v1, v3}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->ARGB_BACKGROUND_COLOR:LX/CBr;

    .line 1856866
    new-instance v0, LX/CBr;

    const-string v1, "ARGB_TEXT_COLOR"

    invoke-direct {v0, v1, v4}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->ARGB_TEXT_COLOR:LX/CBr;

    .line 1856867
    new-instance v0, LX/CBr;

    const-string v1, "CONTENT_BLOCK_BOTTOM_MARGIN"

    invoke-direct {v0, v1, v5}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->CONTENT_BLOCK_BOTTOM_MARGIN:LX/CBr;

    .line 1856868
    new-instance v0, LX/CBr;

    const-string v1, "IMAGE_OVERLAY"

    invoke-direct {v0, v1, v6}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->IMAGE_OVERLAY:LX/CBr;

    .line 1856869
    new-instance v0, LX/CBr;

    const-string v1, "NEWSFEED_LARGE_IMAGE_LOCATION"

    invoke-direct {v0, v1, v7}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->NEWSFEED_LARGE_IMAGE_LOCATION:LX/CBr;

    .line 1856870
    new-instance v0, LX/CBr;

    const-string v1, "PRIMARY_ACTION_BUTTON_GLYPH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->PRIMARY_ACTION_BUTTON_GLYPH:LX/CBr;

    .line 1856871
    new-instance v0, LX/CBr;

    const-string v1, "FAVICON_COLOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->FAVICON_COLOR:LX/CBr;

    .line 1856872
    new-instance v0, LX/CBr;

    const-string v1, "FEED_TAP_ACTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->FEED_TAP_ACTION:LX/CBr;

    .line 1856873
    new-instance v0, LX/CBr;

    const-string v1, "NEWSFEED_BRANDING_STYLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->NEWSFEED_BRANDING_STYLE:LX/CBr;

    .line 1856874
    new-instance v0, LX/CBr;

    const-string v1, "SECONDARY_ACTION_BUTTON_GLYPH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->SECONDARY_ACTION_BUTTON_GLYPH:LX/CBr;

    .line 1856875
    new-instance v0, LX/CBr;

    const-string v1, "PRIMARY_ACTION_BUTTON_STYLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->PRIMARY_ACTION_BUTTON_STYLE:LX/CBr;

    .line 1856876
    new-instance v0, LX/CBr;

    const-string v1, "NEWSFEED_LARGE_IMAGE_COUNTER_TYPE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->NEWSFEED_LARGE_IMAGE_COUNTER_TYPE:LX/CBr;

    .line 1856877
    new-instance v0, LX/CBr;

    const-string v1, "NEWSFEED_LARGE_IMAGE_MARGIN_OPTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->NEWSFEED_LARGE_IMAGE_MARGIN_OPTION:LX/CBr;

    .line 1856878
    new-instance v0, LX/CBr;

    const-string v1, "NEWSFEED_LARGE_IMAGE_PROFILE_PHOTO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->NEWSFEED_LARGE_IMAGE_PROFILE_PHOTO:LX/CBr;

    .line 1856879
    new-instance v0, LX/CBr;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/CBr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBr;->UNKNOWN:LX/CBr;

    .line 1856880
    const/16 v0, 0xf

    new-array v0, v0, [LX/CBr;

    sget-object v1, LX/CBr;->ARGB_BACKGROUND_COLOR:LX/CBr;

    aput-object v1, v0, v3

    sget-object v1, LX/CBr;->ARGB_TEXT_COLOR:LX/CBr;

    aput-object v1, v0, v4

    sget-object v1, LX/CBr;->CONTENT_BLOCK_BOTTOM_MARGIN:LX/CBr;

    aput-object v1, v0, v5

    sget-object v1, LX/CBr;->IMAGE_OVERLAY:LX/CBr;

    aput-object v1, v0, v6

    sget-object v1, LX/CBr;->NEWSFEED_LARGE_IMAGE_LOCATION:LX/CBr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CBr;->PRIMARY_ACTION_BUTTON_GLYPH:LX/CBr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CBr;->FAVICON_COLOR:LX/CBr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CBr;->FEED_TAP_ACTION:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CBr;->NEWSFEED_BRANDING_STYLE:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CBr;->SECONDARY_ACTION_BUTTON_GLYPH:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CBr;->PRIMARY_ACTION_BUTTON_STYLE:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CBr;->NEWSFEED_LARGE_IMAGE_COUNTER_TYPE:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CBr;->NEWSFEED_LARGE_IMAGE_MARGIN_OPTION:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CBr;->NEWSFEED_LARGE_IMAGE_PROFILE_PHOTO:LX/CBr;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CBr;->UNKNOWN:LX/CBr;

    aput-object v2, v0, v1

    sput-object v0, LX/CBr;->$VALUES:[LX/CBr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856881
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBr;
    .locals 1

    .prologue
    .line 1856882
    if-nez p0, :cond_0

    .line 1856883
    :try_start_0
    sget-object v0, LX/CBr;->UNKNOWN:LX/CBr;

    .line 1856884
    :goto_0
    return-object v0

    .line 1856885
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBr;->valueOf(Ljava/lang/String;)LX/CBr;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856886
    :catch_0
    sget-object v0, LX/CBr;->UNKNOWN:LX/CBr;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBr;
    .locals 1

    .prologue
    .line 1856887
    const-class v0, LX/CBr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBr;

    return-object v0
.end method

.method public static values()[LX/CBr;
    .locals 1

    .prologue
    .line 1856888
    sget-object v0, LX/CBr;->$VALUES:[LX/CBr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBr;

    return-object v0
.end method
