.class public final LX/AoY;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/1z3;

.field public final synthetic b:Landroid/text/TextPaint;

.field public final synthetic c:Landroid/text/SpannableStringBuilder;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/Aoa;


# direct methods
.method public constructor <init>(LX/Aoa;LX/1z3;Landroid/text/TextPaint;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1714521
    iput-object p1, p0, LX/AoY;->e:LX/Aoa;

    iput-object p2, p0, LX/AoY;->a:LX/1z3;

    iput-object p3, p0, LX/AoY;->b:Landroid/text/TextPaint;

    iput-object p4, p0, LX/AoY;->c:Landroid/text/SpannableStringBuilder;

    iput-object p5, p0, LX/AoY;->d:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 1714522
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    iget-object v2, p0, LX/AoY;->a:LX/1z3;

    .line 1714523
    iget-object v3, v2, LX/1z3;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1714524
    iget-object v3, p0, LX/AoY;->b:Landroid/text/TextPaint;

    iget-object v4, p0, LX/AoY;->c:Landroid/text/SpannableStringBuilder;

    .line 1714525
    invoke-static {v0, v1, v2, v3}, LX/Aoa;->b(IFLjava/lang/String;Landroid/text/TextPaint;)I

    move-result v5

    .line 1714526
    invoke-static {v4, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v6

    invoke-static {v2, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v7

    sub-float/2addr v6, v7

    .line 1714527
    int-to-float v5, v5

    add-float/2addr v5, v6

    float-to-int v5, v5

    move v0, v5

    .line 1714528
    iget-object v1, p0, LX/AoY;->e:LX/Aoa;

    iget-object v2, p0, LX/AoY;->d:Ljava/lang/String;

    invoke-static {v1, p1, v0, v2}, LX/Aoa;->a$redex0(LX/Aoa;Landroid/view/View;ILjava/lang/String;)LX/0hs;

    move-result-object v0

    .line 1714529
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1714530
    return-void
.end method
