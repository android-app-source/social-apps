.class public LX/CBa;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<",
        "Landroid/net/Uri;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/CBf;

.field private final d:I

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/25M;LX/1Pq;ILandroid/view/View$OnClickListener;LX/CBf;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/25M;",
            "TE;I",
            "Landroid/view/View$OnClickListener;",
            "LX/CBf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856558
    invoke-direct {p0, p1, p2, p4, p3}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1856559
    iput-object p7, p0, LX/CBa;->c:LX/CBf;

    .line 1856560
    iput p5, p0, LX/CBa;->d:I

    .line 1856561
    iput-object p6, p0, LX/CBa;->e:Landroid/view/View$OnClickListener;

    .line 1856562
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1856563
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1856564
    check-cast p2, Landroid/net/Uri;

    .line 1856565
    iget-object v0, p0, LX/CBa;->c:LX/CBf;

    const/4 v1, 0x0

    .line 1856566
    new-instance v2, LX/CBe;

    invoke-direct {v2, v0}, LX/CBe;-><init>(LX/CBf;)V

    .line 1856567
    sget-object v3, LX/CBf;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CBd;

    .line 1856568
    if-nez v3, :cond_0

    .line 1856569
    new-instance v3, LX/CBd;

    invoke-direct {v3}, LX/CBd;-><init>()V

    .line 1856570
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/CBd;->a$redex0(LX/CBd;LX/1De;IILX/CBe;)V

    .line 1856571
    move-object v2, v3

    .line 1856572
    move-object v1, v2

    .line 1856573
    move-object v0, v1

    .line 1856574
    iget v1, p0, LX/CBa;->d:I

    .line 1856575
    iget-object v2, v0, LX/CBd;->a:LX/CBe;

    iput v1, v2, LX/CBe;->a:I

    .line 1856576
    iget-object v2, v0, LX/CBd;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1856577
    move-object v0, v0

    .line 1856578
    iget-object v1, p0, LX/CBa;->e:Landroid/view/View$OnClickListener;

    .line 1856579
    iget-object v2, v0, LX/CBd;->a:LX/CBe;

    iput-object v1, v2, LX/CBe;->b:Landroid/view/View$OnClickListener;

    .line 1856580
    iget-object v2, v0, LX/CBd;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1856581
    move-object v0, v0

    .line 1856582
    iget-object v1, v0, LX/CBd;->a:LX/CBe;

    iput-object p2, v1, LX/CBe;->c:Landroid/net/Uri;

    .line 1856583
    iget-object v1, v0, LX/CBd;->d:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1856584
    move-object v0, v0

    .line 1856585
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1856586
    const/4 v0, 0x0

    return v0
.end method
