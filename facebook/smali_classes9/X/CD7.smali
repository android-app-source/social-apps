.class public final LX/CD7;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CD8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1858741
    invoke-static {}, LX/CD8;->q()LX/CD8;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1858742
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1858743
    const-string v0, "ClickEventConveyerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1858726
    if-ne p0, p1, :cond_1

    .line 1858727
    :cond_0
    :goto_0
    return v0

    .line 1858728
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1858729
    goto :goto_0

    .line 1858730
    :cond_3
    check-cast p1, LX/CD7;

    .line 1858731
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1858732
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1858733
    if-eq v2, v3, :cond_0

    .line 1858734
    iget-object v2, p0, LX/CD7;->a:LX/1X1;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/CD7;->a:LX/1X1;

    iget-object v3, p1, LX/CD7;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1858735
    goto :goto_0

    .line 1858736
    :cond_4
    iget-object v2, p1, LX/CD7;->a:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1858737
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CD7;

    .line 1858738
    iget-object v1, v0, LX/CD7;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/CD7;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/CD7;->a:LX/1X1;

    .line 1858739
    return-object v0

    .line 1858740
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
