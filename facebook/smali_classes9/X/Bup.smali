.class public LX/Bup;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic c:LX/Bur;


# direct methods
.method public constructor <init>(LX/Bur;)V
    .locals 0

    .prologue
    .line 1831589
    iput-object p1, p0, LX/Bup;->c:LX/Bur;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1831609
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831610
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831611
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    iput-object p2, v1, LX/Bur;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831612
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1831613
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    .line 1831614
    invoke-virtual {v1, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1831615
    :cond_0
    invoke-static {v0}, LX/Bur;->e(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1831616
    if-eqz v1, :cond_c

    .line 1831617
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    .line 1831618
    :goto_0
    move v1, v1

    .line 1831619
    if-eqz v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1831620
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    const/4 v5, 0x0

    .line 1831621
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1831622
    check-cast v2, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-static {v2}, LX/Bur;->e(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1831623
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v3, v1, LX/Bur;->r:LX/1b4;

    invoke-virtual {v3, v2}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_d

    const v2, 0x7f080dd9

    .line 1831624
    :goto_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v3

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v5, v3, v5, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    .line 1831625
    new-instance v2, LX/BwB;

    invoke-direct {v2, v1, p2, p3}, LX/BwB;-><init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831626
    const v4, 0x7f0209ae

    .line 1831627
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1831628
    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v3, v4, v2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831629
    :cond_1
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    .line 1831630
    invoke-static {v0}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 1831631
    iget-object v2, v1, LX/Bur;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v2, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v2

    sget-object v3, LX/2oN;->VIDEO_AD:LX/2oN;

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 1831632
    if-eqz v1, :cond_2

    .line 1831633
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    invoke-static {v1, p1, v0, p3}, LX/Bur;->a$redex0(LX/Bur;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    .line 1831634
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    .line 1831635
    invoke-static {v0}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v9

    .line 1831636
    iget-object v4, v1, LX/Bur;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v4, v9}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 1831637
    if-nez v6, :cond_10

    .line 1831638
    :cond_2
    :goto_3
    invoke-static {v0}, LX/17E;->y(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1831639
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1831640
    iget-object v3, p0, LX/Bup;->c:LX/Bur;

    .line 1831641
    invoke-static {v3}, LX/Bur;->i(LX/Bur;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1831642
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v4

    if-eqz v4, :cond_13

    const/4 v4, 0x1

    :goto_4
    move v4, v4

    .line 1831643
    if-eqz v4, :cond_12

    const/4 v4, 0x1

    :goto_5
    move v3, v4

    .line 1831644
    if-eqz v3, :cond_3

    .line 1831645
    iget-object v3, p0, LX/Bup;->c:LX/Bur;

    .line 1831646
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1831647
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    .line 1831648
    const v4, 0x7f081044

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1831649
    invoke-interface {p1, v4}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v5

    .line 1831650
    new-instance v4, LX/Bw7;

    invoke-direct {v4, v3, p2, p3}, LX/Bw7;-><init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831651
    const v6, 0x7f020818

    .line 1831652
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1831653
    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v3, v5, v6, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831654
    :cond_3
    if-eqz v1, :cond_6

    .line 1831655
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1831656
    iget-object v3, p0, LX/Bup;->c:LX/Bur;

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 1831657
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v7

    .line 1831658
    if-eqz v7, :cond_14

    const v4, 0x7f08113b

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1831659
    :goto_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v5

    if-nez v5, :cond_16

    .line 1831660
    const v5, 0x7f08113a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1831661
    :goto_7
    move-object v8, v5

    .line 1831662
    if-eqz v7, :cond_15

    .line 1831663
    const v5, 0x7f020a1d

    move v5, v5

    .line 1831664
    move v6, v5

    .line 1831665
    :goto_8
    invoke-interface {p1, v4}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v5

    .line 1831666
    instance-of v4, v5, LX/3Ai;

    if-eqz v4, :cond_4

    move-object v4, v5

    .line 1831667
    check-cast v4, LX/3Ai;

    invoke-virtual {v4, v8}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1831668
    :cond_4
    new-instance v4, LX/Bw5;

    invoke-direct {v4, v3, v0, v7, v1}, LX/Bw5;-><init>(LX/Bur;Lcom/facebook/graphql/model/GraphQLStory;ZLcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831669
    invoke-virtual {v3, v5, v6, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831670
    :cond_5
    invoke-virtual {p0, p1, v0, v1, v2}, LX/Bup;->a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;Landroid/content/Context;)Z

    .line 1831671
    :cond_6
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    .line 1831672
    iget-object v3, v1, LX/Bur;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    iget-object v3, v1, LX/Bur;->v:LX/0Px;

    if-eqz v3, :cond_18

    iget-object v3, v1, LX/Bur;->v:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_18

    const/4 v3, 0x1

    :goto_9
    move v1, v3

    .line 1831673
    if-eqz v1, :cond_7

    .line 1831674
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    .line 1831675
    invoke-static {v0}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 1831676
    if-nez v3, :cond_19

    .line 1831677
    :cond_7
    :goto_a
    invoke-virtual {p0, v0}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1831678
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    iget-object v1, v1, LX/Bur;->i:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    .line 1831679
    :cond_8
    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1831680
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1831681
    :cond_9
    invoke-virtual {p0, p2}, LX/1wH;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1831682
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    iget-object v1, v1, LX/Bur;->x:LX/0Ot;

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/0Ot;)V

    .line 1831683
    :cond_a
    iget-object v1, p0, LX/Bup;->c:LX/Bur;

    iget-object v1, v1, LX/Bur;->r:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1831684
    invoke-static {v0}, LX/Bur;->e(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1831685
    if-eqz v1, :cond_1a

    .line 1831686
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    .line 1831687
    :goto_b
    move v0, v1

    .line 1831688
    if-eqz v0, :cond_b

    .line 1831689
    iget-object v0, p0, LX/Bup;->c:LX/Bur;

    .line 1831690
    iget-object v1, v0, LX/Bur;->s:LX/AaZ;

    invoke-virtual {v1}, LX/AaZ;->a()Z

    move-result v1

    .line 1831691
    invoke-static {v2, v1}, LX/Bur;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    .line 1831692
    invoke-static {v3, v2, v1}, LX/Bur;->a(Landroid/view/MenuItem;Landroid/content/Context;Z)V

    .line 1831693
    const v1, 0x7f0208f1

    move v1, v1

    .line 1831694
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1831695
    new-instance v1, LX/Bw4;

    invoke-direct {v1, v0, v2}, LX/Bw4;-><init>(LX/Bur;Landroid/content/Context;)V

    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831696
    :cond_b
    return-void

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1831697
    :cond_d
    const v2, 0x7f080dd8

    goto/16 :goto_1

    :cond_e
    const v2, 0x7f080dda

    goto/16 :goto_1

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1831698
    :cond_10
    iget-object v4, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1831699
    move-object v10, v4

    check-cast v10, Lcom/facebook/graphql/model/FeedUnit;

    .line 1831700
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1831701
    const v5, 0x7f08111e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1831702
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1831703
    sget-object v5, LX/Al1;->WHY_AM_I_SEEING_THIS:LX/Al1;

    invoke-virtual {v5}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v7

    .line 1831704
    invoke-interface {p1, v4}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v11

    .line 1831705
    new-instance v4, LX/Bw0;

    move-object v5, v1

    move-object v8, p3

    invoke-direct/range {v4 .. v9}, LX/Bw0;-><init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/view/View;Ljava/lang/String;)V

    invoke-interface {v11, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831706
    invoke-interface {v11}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v6, v4, v7, v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1831707
    check-cast v10, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v10}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v4

    .line 1831708
    iget-boolean v5, v4, Lcom/facebook/graphql/model/SponsoredImpression;->x:Z

    move v4, v5

    .line 1831709
    if-eqz v4, :cond_11

    const v4, 0x7f020ac1

    :goto_c
    invoke-interface {v11, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_11
    const v4, 0x7f0208ed

    goto :goto_c

    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1831710
    :cond_14
    const v4, 0x7f081139

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    .line 1831711
    :cond_15
    const v5, 0x7f02087f

    move v5, v5

    .line 1831712
    move v6, v5

    goto/16 :goto_8

    :cond_16
    iget-boolean v5, v3, LX/Bur;->o:Z

    if-eqz v5, :cond_17

    const v5, 0x7f0810b3

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_7

    :cond_17
    const v5, 0x7f08113c

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_7

    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 1831713
    :cond_19
    const v4, 0x7f081045

    invoke-interface {p1, v4}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1831714
    new-instance v5, LX/BwA;

    invoke-direct {v5, v1, v3}, LX/BwA;-><init>(LX/Bur;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831715
    const v3, 0x7f0207ff

    move v3, v3

    .line 1831716
    invoke-virtual {v1, v4, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    goto/16 :goto_a

    :cond_1a
    const/4 v1, 0x0

    goto/16 :goto_b
.end method

.method public a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 1831593
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1831594
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1831595
    iget-object v0, p0, LX/Bup;->c:LX/Bur;

    .line 1831596
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result p0

    .line 1831597
    if-eqz p0, :cond_2

    const v1, 0x7f08113f

    invoke-virtual {p4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 1831598
    :goto_0
    if-eqz p0, :cond_3

    const v1, 0x7f081140

    invoke-virtual {p4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 1831599
    :goto_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    .line 1831600
    instance-of v1, v2, LX/3Ai;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 1831601
    check-cast v1, LX/3Ai;

    invoke-virtual {v1, v3}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1831602
    :cond_0
    new-instance v1, LX/Bw6;

    invoke-direct {v1, v0, p0, p3}, LX/Bw6;-><init>(LX/Bur;ZLcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831603
    const v1, 0x7f0208b2

    move v1, v1

    .line 1831604
    invoke-virtual {v0, v2, v1, p2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831605
    const/4 v0, 0x1

    .line 1831606
    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 1831607
    :cond_2
    const v1, 0x7f08113d

    invoke-virtual {p4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 1831608
    :cond_3
    const v1, 0x7f08113e

    invoke-virtual {p4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1831590
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831591
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831592
    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
