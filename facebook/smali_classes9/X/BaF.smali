.class public final LX/BaF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/7gj;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/audience/model/UploadShot;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/1EX;


# direct methods
.method public constructor <init>(LX/1EX;Lcom/facebook/audience/model/UploadShot;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1798811
    iput-object p1, p0, LX/BaF;->c:LX/1EX;

    iput-object p2, p0, LX/BaF;->a:Lcom/facebook/audience/model/UploadShot;

    iput-object p3, p0, LX/BaF;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1798812
    sget-object v0, LX/1EX;->a:Ljava/lang/String;

    const-string v1, "Error unable to create 1x1 image."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1798813
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1798814
    check-cast p1, LX/7gj;

    .line 1798815
    if-nez p1, :cond_0

    .line 1798816
    :goto_0
    return-void

    .line 1798817
    :cond_0
    iget-object v0, p0, LX/BaF;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v0}, Lcom/facebook/audience/model/UploadShot;->a(Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    .line 1798818
    iget-object v1, p1, LX/7gj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1798819
    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/UploadShot$Builder;->setPath(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot$Builder;->a()Lcom/facebook/audience/model/UploadShot;

    move-result-object v0

    .line 1798820
    iget-object v1, p0, LX/BaF;->c:LX/1EX;

    iget-object v2, p0, LX/BaF;->b:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/audience/model/UploadShot;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/1EX;->b(LX/1EX;Landroid/content/Context;[Lcom/facebook/audience/model/UploadShot;)V

    goto :goto_0
.end method
