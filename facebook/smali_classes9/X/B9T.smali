.class public LX/B9T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/17W;

.field private final c:LX/B9J;

.field private final d:LX/18Q;

.field private final e:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/B9J;LX/18Q;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1751661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1751662
    iput-object p1, p0, LX/B9T;->a:Landroid/content/Context;

    .line 1751663
    iput-object p2, p0, LX/B9T;->b:LX/17W;

    .line 1751664
    iput-object p3, p0, LX/B9T;->c:LX/B9J;

    .line 1751665
    iput-object p4, p0, LX/B9T;->d:LX/18Q;

    .line 1751666
    iput-object p5, p0, LX/B9T;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1751667
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1751657
    iget-object v0, p0, LX/B9T;->b:LX/17W;

    iget-object v1, p0, LX/B9T;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1751658
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 1751659
    iget-object v1, p0, LX/B9T;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/B9T;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1751660
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/B9T;
    .locals 6

    .prologue
    .line 1751652
    new-instance v0, LX/B9T;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    .line 1751653
    new-instance v3, LX/B9J;

    const/16 v4, 0x542

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v4}, LX/B9J;-><init>(LX/0Or;)V

    .line 1751654
    move-object v3, v3

    .line 1751655
    check-cast v3, LX/B9J;

    invoke-static {p0}, LX/18Q;->a(LX/0QB;)LX/18Q;

    move-result-object v4

    check-cast v4, LX/18Q;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v0 .. v5}, LX/B9T;-><init>(Landroid/content/Context;LX/17W;LX/B9J;LX/18Q;Lcom/facebook/content/SecureContextHelper;)V

    .line 1751656
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMegaphone;)V
    .locals 2

    .prologue
    .line 1751668
    iget-object v0, p0, LX/B9T;->c:LX/B9J;

    const-string v1, "ACTION"

    invoke-virtual {v0, p1, v1}, LX/B9J;->a(Lcom/facebook/graphql/model/GraphQLMegaphone;Ljava/lang/String;)V

    .line 1751669
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/B9T;->a(Ljava/lang/String;)V

    .line 1751670
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1751671
    iget-object v0, p0, LX/B9T;->d:LX/18Q;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->r()Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/18Q;->c(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V

    .line 1751672
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLMegaphone;)V
    .locals 2

    .prologue
    .line 1751648
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1751649
    :cond_0
    :goto_0
    return-void

    .line 1751650
    :cond_1
    iget-object v0, p0, LX/B9T;->c:LX/B9J;

    const-string v1, "IMAGE_ACTION"

    invoke-virtual {v0, p1, v1}, LX/B9J;->a(Lcom/facebook/graphql/model/GraphQLMegaphone;Ljava/lang/String;)V

    .line 1751651
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->p()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/B9T;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLMegaphone;)V
    .locals 2

    .prologue
    .line 1751645
    iget-object v0, p0, LX/B9T;->c:LX/B9J;

    const-string v1, "DISMISSAL"

    invoke-virtual {v0, p1, v1}, LX/B9J;->a(Lcom/facebook/graphql/model/GraphQLMegaphone;Ljava/lang/String;)V

    .line 1751646
    iget-object v0, p0, LX/B9T;->d:LX/18Q;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->r()Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/18Q;->c(Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V

    .line 1751647
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/GraphQLMegaphone;)V
    .locals 2

    .prologue
    .line 1751643
    iget-object v0, p0, LX/B9T;->c:LX/B9J;

    const-string v1, "IMPRESSION"

    invoke-virtual {v0, p1, v1}, LX/B9J;->a(Lcom/facebook/graphql/model/GraphQLMegaphone;Ljava/lang/String;)V

    .line 1751644
    return-void
.end method
