.class public final LX/C4Z;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C4a;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/C4a;


# direct methods
.method public constructor <init>(LX/C4a;)V
    .locals 1

    .prologue
    .line 1847552
    iput-object p1, p0, LX/C4Z;->c:LX/C4a;

    .line 1847553
    move-object v0, p1

    .line 1847554
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1847555
    sget-object v0, LX/C4c;->a:LX/0Px;

    iput-object v0, p0, LX/C4Z;->b:LX/0Px;

    .line 1847556
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1847571
    const-string v0, "InspirationsCallToActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1847557
    if-ne p0, p1, :cond_1

    .line 1847558
    :cond_0
    :goto_0
    return v0

    .line 1847559
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1847560
    goto :goto_0

    .line 1847561
    :cond_3
    check-cast p1, LX/C4Z;

    .line 1847562
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1847563
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1847564
    if-eq v2, v3, :cond_0

    .line 1847565
    iget-object v2, p0, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1847566
    goto :goto_0

    .line 1847567
    :cond_5
    iget-object v2, p1, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1847568
    :cond_6
    iget-object v2, p0, LX/C4Z;->b:LX/0Px;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C4Z;->b:LX/0Px;

    iget-object v3, p1, LX/C4Z;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1847569
    goto :goto_0

    .line 1847570
    :cond_7
    iget-object v2, p1, LX/C4Z;->b:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
