.class public LX/BWn;
.super LX/8tB;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field private final e:LX/333;

.field private f:I

.field private g:I

.field public h:Z

.field private i:Z

.field public j:Z

.field private k:LX/BWm;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;LX/8tF;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1792434
    invoke-direct {p0, p1, p2}, LX/8tB;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    .line 1792435
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/BWn;->g:I

    .line 1792436
    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/8tF;->a(LX/8vE;)LX/8tE;

    move-result-object v0

    iput-object v0, p0, LX/BWn;->e:LX/333;

    .line 1792437
    return-void
.end method

.method public static b(LX/0QB;)LX/BWn;
    .locals 5

    .prologue
    .line 1792431
    new-instance v4, LX/BWn;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-class v1, LX/8vM;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8vM;

    const-class v2, LX/8tF;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/8tF;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v0, v1, v2, v3}, LX/BWn;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;LX/8tF;Landroid/content/Context;)V

    .line 1792432
    move-object v0, v4

    .line 1792433
    return-object v0
.end method

.method private d(II)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1792418
    iget-boolean v0, p0, LX/BWn;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/BWn;->i:Z

    if-eqz v0, :cond_1

    .line 1792419
    :cond_0
    :goto_0
    return v2

    .line 1792420
    :cond_1
    invoke-direct {p0}, LX/BWn;->g()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1792421
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1792422
    if-nez p2, :cond_2

    move v2, v3

    .line 1792423
    goto :goto_0

    .line 1792424
    :cond_2
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p1, v0}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792425
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1792426
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    move v1, v0

    .line 1792427
    :goto_1
    invoke-virtual {p0, p1, p2}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792428
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1792429
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 1792430
    :goto_2
    if-eq v1, v0, :cond_0

    move v2, v3

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method private g()I
    .locals 1

    .prologue
    .line 1792417
    invoke-direct {p0}, LX/BWn;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1792416
    iget-object v1, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/333;
    .locals 1

    .prologue
    .line 1792415
    iget-object v0, p0, LX/BWn;->e:LX/333;

    return-object v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792412
    invoke-super/range {p0 .. p5}, LX/8tB;->a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1792413
    invoke-direct {p0, p1, p2}, LX/BWn;->d(II)Z

    move-result v1

    invoke-static {v0, v1}, LX/BWm;->a(Landroid/view/View;Z)V

    .line 1792414
    return-object v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1792400
    invoke-super {p0, p1, p2, p3}, LX/8tB;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1792401
    iget-boolean v0, p0, LX/BWn;->h:Z

    if-eqz v0, :cond_1

    .line 1792402
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1792403
    :goto_0
    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/623;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/623;

    .line 1792404
    iget-boolean v3, v0, LX/623;->c:Z

    move v0, v3

    .line 1792405
    if-eqz v0, :cond_0

    .line 1792406
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1792407
    :cond_0
    return-object v1

    .line 1792408
    :cond_1
    iget-boolean v0, p0, LX/BWn;->j:Z

    if-nez v0, :cond_3

    .line 1792409
    invoke-direct {p0}, LX/BWn;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, LX/BWn;->g()I

    move-result v0

    if-eq p1, v0, :cond_3

    .line 1792410
    :cond_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1792411
    :cond_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/8RK;LX/8RE;)V
    .locals 0

    .prologue
    .line 1792367
    invoke-super {p0, p1, p2}, LX/8tB;->a(LX/8RK;LX/8RE;)V

    .line 1792368
    check-cast p2, LX/BWm;

    iput-object p2, p0, LX/BWn;->k:LX/BWm;

    .line 1792369
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1792377
    iget-boolean v2, p0, LX/BWn;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/BWn;->i:Z

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1792378
    :goto_0
    return-object v0

    .line 1792379
    :cond_1
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v2

    .line 1792380
    aget v3, v2, v0

    .line 1792381
    invoke-direct {p0}, LX/BWn;->h()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, LX/BWn;->g()I

    move-result v4

    if-ne v3, v4, :cond_2

    move-object v0, v1

    .line 1792382
    goto :goto_0

    .line 1792383
    :cond_2
    const/4 v1, 0x1

    aget v1, v2, v1

    .line 1792384
    const/4 v2, -0x1

    if-ne v1, v2, :cond_6

    .line 1792385
    :goto_1
    iget-object v1, p0, LX/BWn;->k:LX/BWm;

    .line 1792386
    if-nez p2, :cond_3

    .line 1792387
    iget-object v2, v1, LX/BWm;->a:Landroid/view/LayoutInflater;

    const v4, 0x7f0306f6

    const/4 p2, 0x0

    invoke-virtual {v2, v4, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object p2, v2

    .line 1792388
    :cond_3
    move-object v1, p2

    .line 1792389
    invoke-virtual {p0, v3, v0}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792390
    if-eqz v2, :cond_4

    invoke-virtual {v2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1792391
    :cond_4
    :goto_2
    iget v0, p0, LX/BWn;->f:I

    if-nez v0, :cond_5

    .line 1792392
    const/4 v4, 0x0

    .line 1792393
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792394
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 1792395
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    move v0, v0

    .line 1792396
    iput v0, p0, LX/BWn;->f:I

    :cond_5
    move-object v0, v1

    .line 1792397
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    .line 1792398
    :cond_7
    invoke-virtual {v2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 1792399
    const v2, 0x7f0d12a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance p1, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v4

    invoke-direct {p1, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1792375
    iput-boolean p1, p0, LX/BWn;->i:Z

    .line 1792376
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1792374
    iget v0, p0, LX/BWn;->g:I

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 1792373
    iget v0, p0, LX/BWn;->f:I

    return v0
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    .line 1792371
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 1792372
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-direct {p0, v1, v0}, LX/BWn;->d(II)Z

    move-result v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 1792370
    const/4 v0, 0x5

    return v0
.end method
