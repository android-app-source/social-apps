.class public final LX/B3C;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 0

    .prologue
    .line 1739800
    iput-object p1, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1739801
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v3, 0x0

    .line 1739802
    iput-object v3, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    .line 1739803
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739804
    if-eqz v0, :cond_2

    .line 1739805
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739806
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1739807
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 1739808
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739809
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1739810
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_1
    if-eqz v1, :cond_0

    .line 1739811
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v2

    .line 1739812
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739813
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1739814
    iput-object v0, v2, LX/B3N;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1739815
    move-object v0, v2

    .line 1739816
    invoke-virtual {v0}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739817
    :cond_0
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739818
    return-void

    :cond_1
    move v0, v2

    .line 1739819
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739794
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739795
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    .line 1739796
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1739797
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "heisman_fetch_self_profile_picture_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739798
    :cond_0
    iget-object v0, p0, LX/B3C;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739799
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739793
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/B3C;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
