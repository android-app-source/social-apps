.class public final LX/BoM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 1821677
    iput-object p1, p0, LX/BoM;->a:LX/1dt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/BoM;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1821687
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "chained_article_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1821688
    :goto_0
    return-object v0

    .line 1821689
    :catch_0
    move-exception v0

    .line 1821690
    iget-object v1, p0, LX/BoM;->a:LX/1dt;

    iget-object v1, v1, LX/1dt;->k:LX/03V;

    sget-object v2, LX/1dt;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unable to extract chained_article_id from:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1821691
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/BoM;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821692
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    iget-object v0, v0, LX/1dt;->E:LX/0Zb;

    .line 1821693
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-static {v1, p2}, LX/17Q;->c(LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1821694
    return-void
.end method

.method public static a$redex0(LX/BoM;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 1821678
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    iget-object v0, v0, LX/1dt;->L:LX/1eA;

    invoke-virtual {v0}, LX/1eA;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821679
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1821680
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    iget-object v0, v0, LX/1dt;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1821681
    :cond_0
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1821682
    :goto_0
    return-void

    .line 1821683
    :cond_1
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    new-instance v1, LX/35J;

    invoke-direct {v1, p1}, LX/35J;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v1, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35K;

    .line 1821684
    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_HIDING:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 1821685
    iput-object v1, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 1821686
    iget-object v0, p0, LX/BoM;->a:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
