.class public final LX/AyC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;",
        ">;",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AyD;


# direct methods
.method public constructor <init>(LX/AyD;)V
    .locals 0

    .prologue
    .line 1729761
    iput-object p1, p0, LX/AyC;->a:LX/AyD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1729762
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1729763
    if-eqz p1, :cond_0

    .line 1729764
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1729765
    if-nez v0, :cond_1

    .line 1729766
    :cond_0
    const/4 v0, 0x0

    .line 1729767
    :goto_0
    return-object v0

    .line 1729768
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1729769
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    .line 1729770
    new-instance v2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;

    iget-object v1, p0, LX/AyC;->a:LX/AyD;

    iget-object v1, v1, LX/AyD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AyB;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 1729771
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1729772
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1729773
    :goto_1
    move-object v1, v4

    .line 1729774
    invoke-direct {v2, v0, v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;-><init>(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;LX/0Px;)V

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1729775
    :cond_2
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    .line 1729776
    :try_start_0
    iget-object v4, v1, LX/AyB;->a:LX/0lB;

    invoke-virtual {v4}, LX/0lD;->b()LX/0lp;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v4

    .line 1729777
    const-class p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    invoke-virtual {v4, p1}, LX/15w;->b(Ljava/lang/Class;)Ljava/util/Iterator;

    move-result-object p1

    .line 1729778
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1729779
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    .line 1729780
    invoke-virtual {p0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 1729781
    :catch_0
    move-exception v4

    .line 1729782
    invoke-static {v4}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 1729783
    :cond_3
    :goto_3
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto :goto_1

    .line 1729784
    :catch_1
    move-exception v4

    .line 1729785
    invoke-static {v4}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_3
.end method
