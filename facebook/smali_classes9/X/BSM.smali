.class public LX/BSM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1CD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:LX/15d;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/15d;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1785436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785437
    iput-object p1, p0, LX/BSM;->b:LX/15d;

    .line 1785438
    iput-object p2, p0, LX/BSM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1785439
    return-void
.end method

.method public static a(LX/0QB;)LX/BSM;
    .locals 1

    .prologue
    .line 1785440
    invoke-static {p0}, LX/BSM;->b(LX/0QB;)LX/BSM;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1785441
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BSM;
    .locals 3

    .prologue
    .line 1785442
    new-instance v2, LX/BSM;

    invoke-static {p0}, LX/15d;->b(LX/0QB;)LX/15d;

    move-result-object v0

    check-cast v0, LX/15d;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/BSM;-><init>(LX/15d;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1785443
    invoke-static {p0}, LX/1CD;->a(LX/0QB;)LX/1CD;

    move-result-object v0

    check-cast v0, LX/1CD;

    .line 1785444
    iput-object v0, v2, LX/BSM;->a:LX/1CD;

    .line 1785445
    return-object v2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1785446
    iget-object v0, p0, LX/BSM;->b:LX/15d;

    invoke-virtual {v0}, LX/15d;->a()Z

    move-result v0

    return v0
.end method
