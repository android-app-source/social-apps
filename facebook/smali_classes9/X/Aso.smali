.class public final LX/Aso;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:LX/Ass;


# direct methods
.method public constructor <init>(LX/Ass;)V
    .locals 0

    .prologue
    .line 1720883
    iput-object p1, p0, LX/Aso;->a:LX/Ass;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1720884
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    iget-boolean v0, v0, LX/Ass;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    iget-object v0, v0, LX/Ass;->l:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    .line 1720885
    :goto_0
    return v0

    .line 1720886
    :cond_1
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    .line 1720887
    iget-object v1, v0, LX/Ass;->i:LX/Ata;

    new-instance v4, LX/Asq;

    invoke-direct {v4, v0}, LX/Asq;-><init>(LX/Ass;)V

    sget-object p1, LX/AtZ;->AUDIO_PERMISSION:LX/AtZ;

    invoke-virtual {v1, v4, p1}, LX/Ata;->a(LX/Ar8;LX/AtZ;)Z

    move-result v1

    move v0, v1

    .line 1720888
    if-nez v0, :cond_2

    move v0, v2

    .line 1720889
    goto :goto_0

    .line 1720890
    :cond_2
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    iget-object v0, v0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720891
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v4, LX/86q;->FLIPPING:LX/86q;

    if-ne v1, v4, :cond_3

    move v0, v3

    .line 1720892
    goto :goto_0

    .line 1720893
    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, LX/Arx;->b(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1720894
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    iget-object v0, v0, LX/Ass;->g:LX/03V;

    sget-object v1, LX/Ass;->b:Ljava/lang/String;

    const-string v3, "Video is recording, and we got long click event again"

    invoke-static {v1, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    move v0, v2

    .line 1720895
    goto :goto_0

    .line 1720896
    :cond_4
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    .line 1720897
    iput-boolean v2, v0, LX/Ass;->z:Z

    .line 1720898
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    .line 1720899
    iget-object v5, v0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0il;

    .line 1720900
    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    invoke-static {v6}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v6

    if-nez v6, :cond_5

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, LX/0PB;->checkState(Z)V

    move-object v6, v5

    .line 1720901
    check-cast v6, LX/0im;

    invoke-interface {v6}, LX/0im;->c()LX/0jJ;

    move-result-object v6

    sget-object v7, LX/Ass;->a:LX/0jK;

    invoke-virtual {v6, v7}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v6

    check-cast v6, LX/0jL;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v5

    sget-object v7, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    invoke-virtual {v5, v7}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v5

    iget-object v7, v0, LX/Ass;->j:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setVideoRecordStartTimeMs(J)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0jL;

    invoke-virtual {v5}, LX/0jL;->a()V

    .line 1720902
    iget-object v0, p0, LX/Aso;->a:LX/Ass;

    sget-object v1, LX/87O;->RECORDING:LX/87O;

    invoke-static {v0, v1}, LX/Ass;->a$redex0(LX/Ass;LX/87O;)V

    move v0, v2

    .line 1720903
    goto/16 :goto_0

    .line 1720904
    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method
