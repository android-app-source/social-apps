.class public LX/AwZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

.field private final c:LX/1FZ;

.field private final d:LX/1Er;

.field public e:Ljava/util/concurrent/Future;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;LX/1FZ;LX/1Er;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1726797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726798
    iput-object p1, p0, LX/AwZ;->a:Ljava/util/concurrent/ExecutorService;

    .line 1726799
    iput-object p2, p0, LX/AwZ;->b:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    .line 1726800
    iput-object p3, p0, LX/AwZ;->c:LX/1FZ;

    .line 1726801
    iput-object p4, p0, LX/AwZ;->d:LX/1Er;

    .line 1726802
    return-void
.end method

.method private static a(LX/AwZ;Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1726745
    iget-object v0, p0, LX/AwZ;->d:LX/1Er;

    const-string v2, "style_transfer_"

    const-string v3, ".jpeg"

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v2, v3, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1726746
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1726747
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v2, v4, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1726748
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 1726749
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1726750
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1726751
    :goto_0
    return-object v0

    .line 1726752
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1726753
    :catchall_0
    move-exception v2

    move-object v5, v2

    move-object v2, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_2
    :try_start_5
    throw v0

    .line 1726754
    :catch_1
    move-object v0, v1

    goto :goto_0

    .line 1726755
    :catch_2
    move-exception v3

    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method

.method public static b(LX/AwZ;Landroid/net/Uri;ILjava/io/File;Ljava/io/File;LX/Awb;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1726756
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1726757
    :cond_0
    :goto_0
    return-void

    .line 1726758
    :cond_1
    :try_start_0
    new-instance v3, LX/AMG;

    invoke-static {p3}, LX/45W;->a(Ljava/io/File;)[B

    move-result-object v0

    invoke-static {p4}, LX/45W;->a(Ljava/io/File;)[B

    move-result-object v2

    invoke-direct {v3, v0, v2}, LX/AMG;-><init>([B[B)V

    .line 1726759
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x2d0

    const/16 v4, 0x500

    invoke-static {v0, v2, v4}, LX/2Qx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1726760
    const/4 v2, 0x1

    invoke-static {v0, p2, v2}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1726761
    iget-object v2, p0, LX/AwZ;->c:LX/1FZ;

    invoke-virtual {v2, v0}, LX/1FZ;->a(Landroid/graphics/Bitmap;)LX/1FJ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1726762
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 1726763
    if-eqz v2, :cond_0

    .line 1726764
    invoke-virtual {v2}, LX/1FJ;->close()V

    goto :goto_0

    .line 1726765
    :cond_2
    :try_start_2
    iget-object v4, p0, LX/AwZ;->c:LX/1FZ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, LX/AMG;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1FZ;->a(Landroid/graphics/Bitmap;)LX/1FJ;

    move-result-object v1

    .line 1726766
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 1726767
    if-eqz v2, :cond_3

    .line 1726768
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1726769
    :cond_3
    if-eqz v1, :cond_0

    .line 1726770
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto :goto_0

    .line 1726771
    :cond_4
    :try_start_3
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {p0, v0}, LX/AwZ;->a(LX/AwZ;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    .line 1726772
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1726773
    iget-object v3, p5, LX/Awb;->a:LX/Awc;

    .line 1726774
    iput-object v0, v3, LX/Awc;->q:Landroid/net/Uri;

    .line 1726775
    invoke-static {p5}, LX/Awb;->a(LX/Awb;)V

    .line 1726776
    iget-object v3, p5, LX/Awb;->a:LX/Awc;

    iget-object v3, v3, LX/Awc;->c:LX/AsL;

    invoke-virtual {v3}, LX/AsL;->c()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1726777
    :cond_5
    if-eqz v2, :cond_6

    .line 1726778
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1726779
    :cond_6
    if-eqz v1, :cond_0

    .line 1726780
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto/16 :goto_0

    .line 1726781
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 1726782
    :goto_1
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1726783
    invoke-static {p5}, LX/Awb;->a(LX/Awb;)V

    .line 1726784
    iget-object v4, p5, LX/Awb;->a:LX/Awc;

    if-eqz v0, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "Error Applying Style: "

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1726785
    :goto_2
    invoke-static {v4, v3}, LX/Awc;->b$redex0(LX/Awc;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1726786
    :cond_7
    if-eqz v2, :cond_8

    .line 1726787
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1726788
    :cond_8
    if-eqz v1, :cond_0

    .line 1726789
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto/16 :goto_0

    .line 1726790
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_9

    .line 1726791
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1726792
    :cond_9
    if-eqz v1, :cond_a

    .line 1726793
    invoke-virtual {v1}, LX/1FJ;->close()V

    :cond_a
    throw v0

    .line 1726794
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1726795
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1726796
    :cond_b
    const-string v3, "Unspecified Error Applying Style"

    goto :goto_2
.end method
