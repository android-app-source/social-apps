.class public LX/Bp5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bp4;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/3iG;

.field private c:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1822592
    const-class v0, LX/Bp5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bp5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3iG;Ljava/util/concurrent/Callable;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3iG;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1822593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822594
    iput-object p1, p0, LX/Bp5;->b:LX/3iG;

    .line 1822595
    iput-object p2, p0, LX/Bp5;->c:Ljava/util/concurrent/Callable;

    .line 1822596
    iput-object p3, p0, LX/Bp5;->d:LX/03V;

    .line 1822597
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1822598
    :try_start_0
    iget-object v0, p0, LX/Bp5;->b:LX/3iG;

    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v0, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v1

    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v2, "photos_feed"

    .line 1822599
    iput-object v2, v0, LX/21A;->c:Ljava/lang/String;

    .line 1822600
    move-object v0, v0

    .line 1822601
    const-string v2, "photos_feed_ufi"

    .line 1822602
    iput-object v2, v0, LX/21A;->b:Ljava/lang/String;

    .line 1822603
    move-object v2, v0

    .line 1822604
    iget-object v0, p0, LX/Bp5;->c:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1822605
    iput-object v0, v2, LX/21A;->a:LX/162;

    .line 1822606
    move-object v0, v2

    .line 1822607
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/3iK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1822608
    :goto_0
    return-void

    .line 1822609
    :catch_0
    move-exception v0

    .line 1822610
    iget-object v1, p0, LX/Bp5;->d:LX/03V;

    sget-object v2, LX/Bp5;->a:Ljava/lang/String;

    const-string v3, "mStoryCallable threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
