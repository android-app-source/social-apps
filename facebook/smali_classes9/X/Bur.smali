.class public LX/Bur;
.super LX/1SX;
.source ""


# static fields
.field private static final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/3Gp;

.field public final h:LX/0wD;

.field public final i:Ljava/lang/String;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:Landroid/support/v4/app/FragmentActivity;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/3HS;

.field public final o:Z

.field public final p:LX/3HT;

.field public final q:LX/AVU;

.field public final r:LX/1b4;

.field public final s:LX/AaZ;

.field public final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/2oj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final y:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1831780
    new-instance v0, LX/Bw2;

    invoke-direct {v0}, LX/Bw2;-><init>()V

    sput-object v0, LX/Bur;->b:LX/0Or;

    .line 1831781
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Bur;->c:LX/0Rf;

    .line 1831782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Bur;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/1Pf;LX/0wD;Ljava/lang/String;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/3Gp;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/support/v4/app/FragmentActivity;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/3HS;LX/0Ot;LX/0Ot;LX/0Ot;LX/17Y;Ljava/lang/Boolean;LX/3HT;LX/AVU;LX/1b4;LX/AaZ;LX/0Ot;LX/0wL;)V
    .locals 42
    .param p1    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0wD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p36    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsInNewPlayerOldUIClosedCaptioningGateKeeper;
        .end annotation
    .end param
    .param p46    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pf;",
            "LX/0wD;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/3Gp;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;",
            ">;",
            "LX/3HS;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/17Y;",
            "Ljava/lang/Boolean;",
            "LX/3HT;",
            "LX/AVU;",
            "LX/1b4;",
            "LX/AaZ;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1831756
    sget-object v17, LX/Bur;->b:LX/0Or;

    sget-object v18, LX/Bur;->b:LX/0Or;

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move-object/from16 v16, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p1

    move-object/from16 v31, p29

    move-object/from16 v32, p30

    move-object/from16 v33, p31

    move-object/from16 v34, p32

    move-object/from16 v35, p37

    move-object/from16 v36, p38

    move-object/from16 v37, p39

    move-object/from16 v38, p42

    move-object/from16 v39, p43

    move-object/from16 v40, p44

    move-object/from16 v41, p52

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 1831757
    new-instance v2, LX/Bw3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/Bw3;-><init>(LX/Bur;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/Bur;->y:LX/1Sp;

    .line 1831758
    move-object/from16 v0, p31

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->e:LX/0Ot;

    .line 1831759
    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->f:LX/0Or;

    .line 1831760
    move-object/from16 v0, p33

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->g:LX/3Gp;

    .line 1831761
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->h:LX/0wD;

    .line 1831762
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->i:Ljava/lang/String;

    .line 1831763
    move-object/from16 v0, p34

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1831764
    move-object/from16 v0, p35

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->k:Landroid/support/v4/app/FragmentActivity;

    .line 1831765
    move-object/from16 v0, p36

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->l:LX/0Or;

    .line 1831766
    move-object/from16 v0, p40

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->m:LX/0Ot;

    .line 1831767
    move-object/from16 v0, p41

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->n:LX/3HS;

    .line 1831768
    invoke-virtual/range {p46 .. p46}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/Bur;->o:Z

    .line 1831769
    move-object/from16 v0, p47

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->p:LX/3HT;

    .line 1831770
    move-object/from16 v0, p48

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->q:LX/AVU;

    .line 1831771
    move-object/from16 v0, p49

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->r:LX/1b4;

    .line 1831772
    move-object/from16 v0, p50

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->s:LX/AaZ;

    .line 1831773
    move-object/from16 v0, p21

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->t:LX/0Or;

    .line 1831774
    move-object/from16 v0, p51

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->x:LX/0Ot;

    .line 1831775
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/Bur;->u:LX/0Or;

    .line 1831776
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Bur;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 1831777
    const-string v2, "native_story"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 1831778
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Bur;->y:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 1831779
    return-void
.end method

.method public static a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1831733
    if-eqz p1, :cond_0

    const v0, 0x7f081147

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f081145

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/Bur;Landroid/view/Menu;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1831801
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v7

    .line 1831802
    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v6

    .line 1831803
    invoke-virtual {p5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Bur;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Ljava/lang/String;

    move-result-object v1

    .line 1831804
    instance-of v0, v6, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 1831805
    check-cast v0, LX/3Ai;

    invoke-virtual {v0, v1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1831806
    :cond_0
    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v3

    .line 1831807
    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, p3, v0, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1831808
    new-instance v0, LX/BwC;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/BwC;-><init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831809
    invoke-static {v7}, LX/Al4;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result v1

    .line 1831810
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831811
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v6, v1, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831812
    return-void
.end method

.method public static a(Landroid/view/MenuItem;Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 1831783
    instance-of v0, p0, LX/3Ai;

    if-eqz v0, :cond_0

    .line 1831784
    if-eqz p2, :cond_1

    const v0, 0x7f081148

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1831785
    :goto_0
    check-cast p0, LX/3Ai;

    invoke-virtual {p0, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1831786
    :cond_0
    return-void

    .line 1831787
    :cond_1
    const v0, 0x7f081146

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/Bur;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1831788
    invoke-static {p2}, LX/Bur;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 1831789
    iget-object v0, p0, LX/Bur;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v0, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1831790
    if-nez v3, :cond_1

    .line 1831791
    :cond_0
    return-void

    .line 1831792
    :cond_1
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831793
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    .line 1831794
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1831795
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v7

    .line 1831796
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_0

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 1831797
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    .line 1831798
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    .line 1831799
    invoke-static/range {v0 .. v5}, LX/Bur;->a(LX/Bur;Landroid/view/Menu;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V

    .line 1831800
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1831746
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1831747
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1831748
    :cond_0
    const/4 v0, 0x0

    .line 1831749
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1831750
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 1831751
    :cond_0
    :goto_0
    return-object v0

    .line 1831752
    :cond_1
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831753
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1831754
    if-eqz v1, :cond_0

    .line 1831755
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(LX/Bur;)Z
    .locals 1

    .prologue
    .line 1831745
    invoke-virtual {p0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1831742
    sget-object v0, LX/Bw1;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1831743
    invoke-super {p0, p1, p2}, LX/1SX;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1831744
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1831739
    invoke-super {p0, p1, p2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    .line 1831740
    iget-object v1, p0, LX/Bur;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/enums/StoryVisibility;Z)V

    .line 1831741
    return-void
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)LX/9lZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")",
            "LX/9lZ;"
        }
    .end annotation

    .prologue
    .line 1831738
    new-instance v0, LX/BwD;

    invoke-direct {v0, p0, p1}, LX/BwD;-><init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method public d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 1831735
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1831736
    new-instance v0, LX/Bup;

    invoke-direct {v0, p0}, LX/Bup;-><init>(LX/Bur;)V

    .line 1831737
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LX/1SX;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1831734
    const/4 v0, 0x1

    return v0
.end method
