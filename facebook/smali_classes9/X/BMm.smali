.class public LX/BMm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BMk;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BMn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1777913
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BMm;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BMn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777914
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1777915
    iput-object p1, p0, LX/BMm;->b:LX/0Ot;

    .line 1777916
    return-void
.end method

.method public static a(LX/0QB;)LX/BMm;
    .locals 4

    .prologue
    .line 1777917
    const-class v1, LX/BMm;

    monitor-enter v1

    .line 1777918
    :try_start_0
    sget-object v0, LX/BMm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777919
    sput-object v2, LX/BMm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777920
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777921
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777922
    new-instance v3, LX/BMm;

    const/16 p0, 0x3002

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BMm;-><init>(LX/0Ot;)V

    .line 1777923
    move-object v0, v3

    .line 1777924
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777925
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BMm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777926
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1777928
    iget-object v0, p0, LX/BMm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BMn;

    .line 1777929
    iget-object v1, v0, LX/BMn;->a:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    const p0, 0x7f0a00e7

    invoke-virtual {v1, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v1

    const p0, 0x7f020aea

    invoke-virtual {v1, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1777930
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p0, 0x7f0b00e3

    invoke-interface {v1, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const p0, 0x7f0b00e9

    invoke-interface {v1, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    .line 1777931
    const p0, 0x6897f1d2

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1777932
    invoke-interface {v1, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/4 p0, 0x4

    const p2, 0x7f0b00eb

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x5

    const p2, 0x7f0b00ea

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x1

    const p2, 0x7f0b00ed

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x3

    const p2, 0x7f0b00ee

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1777933
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1777934
    invoke-static {}, LX/1dS;->b()V

    .line 1777935
    iget v0, p1, LX/1dQ;->b:I

    .line 1777936
    packed-switch v0, :pswitch_data_0

    .line 1777937
    :goto_0
    return-object v1

    .line 1777938
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1777939
    check-cast v0, LX/BMl;

    .line 1777940
    iget-object p1, p0, LX/BMm;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/BMn;

    iget-object p2, v0, LX/BMl;->a:LX/1Ri;

    .line 1777941
    iget-object p0, p1, LX/BMn;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ak7;

    invoke-virtual {p0, p2}, LX/Ak7;->b(LX/1Ri;)V

    .line 1777942
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6897f1d2
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/BMk;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1777943
    new-instance v1, LX/BMl;

    invoke-direct {v1, p0}, LX/BMl;-><init>(LX/BMm;)V

    .line 1777944
    sget-object v2, LX/BMm;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BMk;

    .line 1777945
    if-nez v2, :cond_0

    .line 1777946
    new-instance v2, LX/BMk;

    invoke-direct {v2}, LX/BMk;-><init>()V

    .line 1777947
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BMk;->a$redex0(LX/BMk;LX/1De;IILX/BMl;)V

    .line 1777948
    move-object v1, v2

    .line 1777949
    move-object v0, v1

    .line 1777950
    return-object v0
.end method
