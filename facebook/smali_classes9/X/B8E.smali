.class public final LX/B8E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:LX/B8F;


# direct methods
.method public constructor <init>(LX/B8F;)V
    .locals 0

    .prologue
    .line 1748572
    iput-object p1, p0, LX/B8E;->a:LX/B8F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1748573
    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1748574
    if-eqz v0, :cond_0

    .line 1748575
    iget-object v1, p0, LX/B8E;->a:LX/B8F;

    invoke-virtual {v1}, LX/B8F;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a074e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1748576
    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    invoke-static {v0}, LX/B8F;->e(LX/B8F;)V

    .line 1748577
    :cond_0
    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->h:LX/B7w;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p4, v0

    if-gez v0, :cond_1

    .line 1748578
    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    invoke-virtual {v0}, LX/B8F;->getInputValue()Ljava/lang/String;

    .line 1748579
    :cond_1
    iget-object v0, p0, LX/B8E;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->g:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748580
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1748581
    return-void
.end method
