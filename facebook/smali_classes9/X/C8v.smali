.class public final enum LX/C8v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C8v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C8v;

.field public static final enum AUTO_GLOWING:LX/C8v;

.field public static final enum GLOWING:LX/C8v;

.field public static final enum SHIMMERING:LX/C8v;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1853432
    new-instance v0, LX/C8v;

    const-string v1, "AUTO_GLOWING"

    invoke-direct {v0, v1, v2}, LX/C8v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C8v;->AUTO_GLOWING:LX/C8v;

    .line 1853433
    new-instance v0, LX/C8v;

    const-string v1, "GLOWING"

    invoke-direct {v0, v1, v3}, LX/C8v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C8v;->GLOWING:LX/C8v;

    .line 1853434
    new-instance v0, LX/C8v;

    const-string v1, "SHIMMERING"

    invoke-direct {v0, v1, v4}, LX/C8v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C8v;->SHIMMERING:LX/C8v;

    .line 1853435
    const/4 v0, 0x3

    new-array v0, v0, [LX/C8v;

    sget-object v1, LX/C8v;->AUTO_GLOWING:LX/C8v;

    aput-object v1, v0, v2

    sget-object v1, LX/C8v;->GLOWING:LX/C8v;

    aput-object v1, v0, v3

    sget-object v1, LX/C8v;->SHIMMERING:LX/C8v;

    aput-object v1, v0, v4

    sput-object v0, LX/C8v;->$VALUES:[LX/C8v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1853437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C8v;
    .locals 1

    .prologue
    .line 1853438
    const-class v0, LX/C8v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C8v;

    return-object v0
.end method

.method public static values()[LX/C8v;
    .locals 1

    .prologue
    .line 1853436
    sget-object v0, LX/C8v;->$VALUES:[LX/C8v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C8v;

    return-object v0
.end method
