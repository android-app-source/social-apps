.class public LX/Bby;
.super LX/398;
.source ""

# interfaces
.implements LX/0jq;


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Bby;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1801363
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1801364
    sget-object v0, LX/0ax;->iB:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->SOCIAL_SEARCH_MAP_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 1801365
    return-void
.end method

.method public static a(LX/0QB;)LX/Bby;
    .locals 3

    .prologue
    .line 1801348
    sget-object v0, LX/Bby;->a:LX/Bby;

    if-nez v0, :cond_1

    .line 1801349
    const-class v1, LX/Bby;

    monitor-enter v1

    .line 1801350
    :try_start_0
    sget-object v0, LX/Bby;->a:LX/Bby;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1801351
    if-eqz v2, :cond_0

    .line 1801352
    :try_start_1
    new-instance v0, LX/Bby;

    invoke-direct {v0}, LX/Bby;-><init>()V

    .line 1801353
    move-object v0, v0

    .line 1801354
    sput-object v0, LX/Bby;->a:LX/Bby;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1801355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1801356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1801357
    :cond_1
    sget-object v0, LX/Bby;->a:LX/Bby;

    return-object v0

    .line 1801358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1801359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 1801360
    new-instance v0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;-><init>()V

    .line 1801361
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1801362
    return-object v0
.end method
