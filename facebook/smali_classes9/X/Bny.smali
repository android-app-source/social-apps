.class public LX/Bny;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Bnz;

.field public d:LX/1JR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0tE;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0tG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0gX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Uo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1821361
    const-class v0, LX/Bny;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bny;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Bnz;)V
    .locals 1
    .param p1    # LX/Bnz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1821356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821357
    new-instance v0, LX/Bnx;

    invoke-direct {v0, p0}, LX/Bnx;-><init>(LX/Bny;)V

    iput-object v0, p0, LX/Bny;->i:LX/0TF;

    .line 1821358
    iput-object p1, p0, LX/Bny;->c:LX/Bnz;

    .line 1821359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bny;->b:Ljava/util/Map;

    .line 1821360
    return-void
.end method
