.class public LX/Aud;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$SetsInspirationDoodleParams",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/widget/FrameLayout;

.field private final d:LX/8GT;

.field public final e:LX/87P;

.field public final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public g:LX/Auh;

.field public h:Lcom/facebook/photos/creativeediting/model/DoodleParams;

.field private i:I

.field private j:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1723047
    const-class v0, LX/Aud;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Aud;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;Landroid/widget/FrameLayout;Landroid/content/Context;LX/8GT;LX/87P;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/widget/FrameLayout;",
            "Landroid/content/Context;",
            "LX/8GT;",
            "LX/87P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 1723036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723037
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    .line 1723038
    iput-object p2, p0, LX/Aud;->c:Landroid/widget/FrameLayout;

    .line 1723039
    iput-object p3, p0, LX/Aud;->b:Landroid/content/Context;

    .line 1723040
    iput-object p4, p0, LX/Aud;->d:LX/8GT;

    .line 1723041
    iput-object p5, p0, LX/Aud;->e:LX/87P;

    .line 1723042
    new-instance v0, LX/Auh;

    iget-object v1, p0, LX/Aud;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Auh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Aud;->g:LX/Auh;

    .line 1723043
    iget-object v0, p0, LX/Aud;->g:LX/Auh;

    new-instance v1, LX/Auc;

    invoke-direct {v1, p0}, LX/Auc;-><init>(LX/Aud;)V

    .line 1723044
    iput-object v1, v0, LX/Auh;->d:LX/Auc;

    .line 1723045
    iget-object v0, p0, LX/Aud;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1723046
    return-void
.end method


# virtual methods
.method public final a(LX/0jL;)LX/0jL;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;)TMutation;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1723009
    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    invoke-virtual {v1}, LX/Auh;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1723010
    iput-object v3, p0, LX/Aud;->h:Lcom/facebook/photos/creativeediting/model/DoodleParams;

    .line 1723011
    :cond_0
    :goto_0
    return-object p1

    .line 1723012
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 1723013
    iget-object v4, p0, LX/Aud;->d:LX/8GT;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    const-string v5, "png"

    invoke-virtual {v4, v2, v5}, LX/8GT;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1723014
    iget-object v4, p0, LX/Aud;->g:LX/Auh;

    const/4 v5, 0x2

    .line 1723015
    iget-object v0, v4, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawingview/DrawingView;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v4, v0

    .line 1723016
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1723017
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1723018
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 1723019
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 1723020
    :try_start_1
    iget-object v5, p0, LX/Aud;->g:LX/Auh;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    .line 1723021
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723022
    iget-object v0, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723023
    iget-object v0, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-static {v0, v6, v4}, LX/8GY;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    .line 1723024
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 1723025
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 1723026
    iget v9, v0, Landroid/graphics/Rect;->left:I

    iget-object v10, v5, LX/Auh;->e:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    .line 1723027
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v10, v5, LX/Auh;->e:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v10

    int-to-float v0, v0

    iget-object v10, v5, LX/Auh;->e:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v0, v10

    .line 1723028
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v10

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object p0

    invoke-virtual {p0, v9}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object p0

    add-float/2addr v7, v9

    invoke-virtual {p0, v7}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v7

    add-float/2addr v0, v8

    invoke-virtual {v7, v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->setMediaRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    const-string v7, "doodle"

    invoke-virtual {v0, v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->setId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->setUri(Landroid/net/Uri;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    .line 1723029
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    iget v8, v5, LX/Auh;->g:I

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->setDoodleSizeCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    iget v8, v5, LX/Auh;->i:I

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->setDoodleStrokeCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    iget v8, v5, LX/Auh;->h:I

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->setDoodleColorCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    iget v8, v5, LX/Auh;->j:I

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->setDoodleUndoCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    iget v8, v5, LX/Auh;->f:F

    invoke-virtual {v7, v8}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->setDoodleMaxBrushSize(F)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    move-result-object v7

    .line 1723030
    invoke-static {v0, v7}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    move-object v2, v0

    .line 1723031
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v4

    iget-object v1, v2, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-virtual {v4, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setInspirationDoodleExtraLoggingData(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    .line 1723032
    move-object v0, p1

    check-cast v0, LX/0jL;

    move-object v1, v0

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v4}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1723033
    :catch_0
    move-object v1, v3

    :goto_1
    if-eqz v1, :cond_0

    .line 1723034
    invoke-static {v1}, LX/8GT;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1723035
    :catch_1
    move-object v1, v3

    goto :goto_1
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1723048
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1722970
    check-cast p1, LX/0io;

    .line 1722971
    iget-object v0, p0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1722972
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v1

    .line 1722973
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v2

    iget v3, p0, LX/Aud;->i:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getStrokeWidth()F

    move-result v2

    iget v3, p0, LX/Aud;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getDrawingMode()LX/86w;

    move-result-object v2

    iget-object v3, p0, LX/Aud;->g:LX/Auh;

    .line 1722974
    iget-object v4, v3, LX/Auh;->c:LX/86w;

    move-object v3, v4

    .line 1722975
    if-eq v2, v3, :cond_4

    .line 1722976
    :cond_0
    iget-object v2, p0, LX/Aud;->g:LX/Auh;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v3

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getStrokeWidth()F

    move-result v4

    .line 1722977
    iget-object v5, v2, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    .line 1722978
    iget p2, v5, Lcom/facebook/drawingview/DrawingView;->l:I

    move v5, p2

    .line 1722979
    if-eq v5, v3, :cond_1

    .line 1722980
    iget v5, v2, LX/Auh;->h:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/Auh;->h:I

    .line 1722981
    iget-object v5, v2, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v5, v3}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1722982
    :cond_1
    iget-object v5, v2, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    .line 1722983
    iget p2, v5, Lcom/facebook/drawingview/DrawingView;->k:F

    move v5, p2

    .line 1722984
    cmpl-float v5, v5, v4

    if-eqz v5, :cond_3

    .line 1722985
    iget v5, v2, LX/Auh;->f:F

    cmpl-float v5, v4, v5

    if-lez v5, :cond_2

    .line 1722986
    iput v4, v2, LX/Auh;->f:F

    .line 1722987
    :cond_2
    iget v5, v2, LX/Auh;->g:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/Auh;->g:I

    .line 1722988
    iget-object v5, v2, LX/Auh;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawingview/DrawingView;->setStrokeWidth(F)V

    .line 1722989
    :cond_3
    iget-object v2, p0, LX/Aud;->g:LX/Auh;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getDrawingMode()LX/86w;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Auh;->setDrawingMode(LX/86w;)V

    .line 1722990
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v2

    iput v2, p0, LX/Aud;->i:I

    .line 1722991
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getStrokeWidth()F

    move-result v1

    iput v1, p0, LX/Aud;->j:F

    .line 1722992
    :cond_4
    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    .line 1722993
    iget-object v2, v1, LX/Auh;->c:LX/86w;

    move-object v1, v2

    .line 1722994
    sget-object v2, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    if-eq v1, v2, :cond_5

    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    .line 1722995
    iget-object v2, v1, LX/Auh;->c:LX/86w;

    move-object v1, v2

    .line 1722996
    sget-object v2, LX/86w;->ACTIVE_EMPTY:LX/86w;

    if-ne v1, v2, :cond_7

    :cond_5
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1722997
    if-eqz v1, :cond_6

    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    sget-object v3, LX/86t;->DOODLE:LX/86t;

    invoke-static {v2, v1, v3}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1722998
    const/4 v1, 0x0

    sget-object v2, LX/Aud;->a:LX/0jK;

    invoke-static {v0, v1, v2}, LX/87R;->a(LX/0il;ZLX/0jK;)V

    .line 1722999
    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    .line 1723000
    iget-object v2, v1, LX/Auh;->c:LX/86w;

    move-object v1, v2

    .line 1723001
    sget-object v2, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    if-ne v1, v2, :cond_8

    .line 1723002
    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    invoke-virtual {v1}, LX/Auh;->a()V

    .line 1723003
    :cond_6
    :goto_1
    return-void

    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    .line 1723004
    :cond_8
    iget-object v1, p0, LX/Aud;->g:LX/Auh;

    .line 1723005
    iget-object v2, v1, LX/Auh;->c:LX/86w;

    move-object v1, v2

    .line 1723006
    sget-object v2, LX/86w;->ACTIVE_EMPTY:LX/86w;

    if-ne v1, v2, :cond_9

    .line 1723007
    invoke-virtual {p0}, LX/Aud;->c()V

    goto :goto_1

    .line 1723008
    :cond_9
    goto :goto_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1722963
    iget-object v0, p0, LX/Aud;->g:LX/Auh;

    .line 1722964
    sget-object v1, LX/86w;->AVAILABLE:LX/86w;

    invoke-virtual {v0, v1}, LX/Auh;->setDrawingMode(LX/86w;)V

    .line 1722965
    iget-object v0, p0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Aud;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1722966
    invoke-virtual {p0, v0}, LX/Aud;->a(LX/0jL;)LX/0jL;

    move-result-object v0

    .line 1722967
    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/Aud;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    sget-object v2, LX/86t;->DOODLE:LX/86t;

    invoke-static {v0, v1, v2}, LX/87R;->a(LX/0jL;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/86t;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1722968
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1722969
    return-void
.end method
