.class public final LX/Brv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/34L;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1826794
    iput-object p1, p0, LX/Brv;->b:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    iput-object p2, p0, LX/Brv;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)V
    .locals 7

    .prologue
    .line 1826795
    iget-object v0, p0, LX/Brv;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1826796
    iget-object v1, p0, LX/Brv;->b:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 1826797
    if-nez v0, :cond_1

    .line 1826798
    :cond_0
    :goto_0
    return-void

    .line 1826799
    :cond_1
    sub-int v0, p2, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1826800
    iget-object v1, p0, LX/Brv;->b:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->c:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->b()I

    move-result v1

    .line 1826801
    if-eq v0, v1, :cond_0

    .line 1826802
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v2, v4

    if-gez v0, :cond_0

    .line 1826803
    iget-object v0, p0, LX/Brv;->b:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->f:LX/03V;

    const-string v2, "WrongWidthForSubtitleTextLayout"

    const-string v3, "Regenerating layout for text = %s\nexpected width = %s\nactual width = %s\nwidth-height difference = %s\nstory = %s"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x4

    iget-object v5, p0, LX/Brv;->a:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
