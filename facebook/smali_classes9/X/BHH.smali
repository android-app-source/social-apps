.class public final enum LX/BHH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BHH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BHH;

.field public static final enum GIF:LX/BHH;

.field public static final enum LIVE_CAMERA:LX/BHH;

.field public static final enum PHOTO:LX/BHH;

.field public static final enum VIDEO:LX/BHH;


# instance fields
.field private final mStringResource:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1768656
    new-instance v0, LX/BHH;

    const-string v1, "PHOTO"

    const v2, 0x7f081380

    invoke-direct {v0, v1, v3, v2}, LX/BHH;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/BHH;->PHOTO:LX/BHH;

    .line 1768657
    new-instance v0, LX/BHH;

    const-string v1, "VIDEO"

    const v2, 0x7f081381

    invoke-direct {v0, v1, v4, v2}, LX/BHH;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/BHH;->VIDEO:LX/BHH;

    .line 1768658
    new-instance v0, LX/BHH;

    const-string v1, "GIF"

    const v2, 0x7f081382

    invoke-direct {v0, v1, v5, v2}, LX/BHH;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/BHH;->GIF:LX/BHH;

    .line 1768659
    new-instance v0, LX/BHH;

    const-string v1, "LIVE_CAMERA"

    const v2, 0x7f081383

    invoke-direct {v0, v1, v6, v2}, LX/BHH;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/BHH;->LIVE_CAMERA:LX/BHH;

    .line 1768660
    const/4 v0, 0x4

    new-array v0, v0, [LX/BHH;

    sget-object v1, LX/BHH;->PHOTO:LX/BHH;

    aput-object v1, v0, v3

    sget-object v1, LX/BHH;->VIDEO:LX/BHH;

    aput-object v1, v0, v4

    sget-object v1, LX/BHH;->GIF:LX/BHH;

    aput-object v1, v0, v5

    sget-object v1, LX/BHH;->LIVE_CAMERA:LX/BHH;

    aput-object v1, v0, v6

    sput-object v0, LX/BHH;->$VALUES:[LX/BHH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1768653
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1768654
    iput p3, p0, LX/BHH;->mStringResource:I

    .line 1768655
    return-void
.end method

.method public static fromOrdinal(I)LX/BHH;
    .locals 1

    .prologue
    .line 1768647
    packed-switch p0, :pswitch_data_0

    .line 1768648
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1768649
    :pswitch_0
    sget-object v0, LX/BHH;->PHOTO:LX/BHH;

    goto :goto_0

    .line 1768650
    :pswitch_1
    sget-object v0, LX/BHH;->VIDEO:LX/BHH;

    goto :goto_0

    .line 1768651
    :pswitch_2
    sget-object v0, LX/BHH;->GIF:LX/BHH;

    goto :goto_0

    .line 1768652
    :pswitch_3
    sget-object v0, LX/BHH;->LIVE_CAMERA:LX/BHH;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/BHH;
    .locals 1

    .prologue
    .line 1768646
    const-class v0, LX/BHH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BHH;

    return-object v0
.end method

.method public static values()[LX/BHH;
    .locals 1

    .prologue
    .line 1768644
    sget-object v0, LX/BHH;->$VALUES:[LX/BHH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BHH;

    return-object v0
.end method


# virtual methods
.method public final getStringResource()I
    .locals 1

    .prologue
    .line 1768645
    iget v0, p0, LX/BHH;->mStringResource:I

    return v0
.end method
