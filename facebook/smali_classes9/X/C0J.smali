.class public LX/C0J;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1840267
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C0J;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840268
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1840269
    iput-object p1, p0, LX/C0J;->b:LX/0Ot;

    .line 1840270
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1840271
    check-cast p2, LX/C0I;

    .line 1840272
    iget-object v0, p0, LX/C0J;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;

    iget-object v1, p2, LX/C0I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p2, LX/C0I;->b:I

    iget v3, p2, LX/C0I;->c:I

    const/4 v5, 0x0

    .line 1840273
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1840274
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840275
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    if-nez p0, :cond_1

    :cond_0
    move-object v4, v5

    .line 1840276
    :goto_0
    move-object v0, v4

    .line 1840277
    return-object v0

    .line 1840278
    :cond_1
    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->e:Landroid/content/res/Resources;

    int-to-float p2, v2

    invoke-static {p0, p2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result p0

    .line 1840279
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4, p0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 1840280
    if-nez v4, :cond_2

    move-object v4, v5

    .line 1840281
    goto :goto_0

    .line 1840282
    :cond_2
    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->f:LX/1WM;

    invoke-virtual {p0, v1}, LX/1WM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p0

    if-eqz p0, :cond_3

    move-object v4, v5

    .line 1840283
    goto :goto_0

    .line 1840284
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1840285
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->c:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1840286
    invoke-static {}, LX/1dS;->b()V

    .line 1840287
    const/4 v0, 0x0

    return-object v0
.end method
