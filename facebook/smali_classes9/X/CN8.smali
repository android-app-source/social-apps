.class public final LX/CN8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3Rb;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:Z

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/8ue;

.field public g:Ljava/lang/String;

.field public h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1881288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881289
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    iput-object v0, p0, LX/CN8;->f:LX/8ue;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;)LX/CN8;
    .locals 1

    .prologue
    .line 1881290
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1881291
    iput-object v0, p0, LX/CN8;->e:LX/0Px;

    .line 1881292
    move-object v0, p0

    .line 1881293
    return-object v0
.end method

.method public final a()LX/CN9;
    .locals 10

    .prologue
    .line 1881294
    new-instance v0, LX/CN9;

    iget-object v1, p0, LX/CN8;->a:LX/3Rb;

    iget-object v2, p0, LX/CN8;->b:Ljava/lang/String;

    iget-object v3, p0, LX/CN8;->c:Landroid/net/Uri;

    iget-boolean v4, p0, LX/CN8;->d:Z

    iget-object v5, p0, LX/CN8;->e:LX/0Px;

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/CN8;->e:LX/0Px;

    :goto_0
    iget-object v6, p0, LX/CN8;->f:LX/8ue;

    iget-object v7, p0, LX/CN8;->g:Ljava/lang/String;

    iget v8, p0, LX/CN8;->h:I

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, LX/CN9;-><init>(LX/3Rb;Ljava/lang/String;Landroid/net/Uri;ZLX/0Px;LX/8ue;Ljava/lang/String;IB)V

    return-object v0

    .line 1881295
    :cond_0
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 1881296
    goto :goto_0
.end method
