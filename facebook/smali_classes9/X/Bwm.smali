.class public LX/Bwm;
.super LX/BwQ;
.source ""


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field public f:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/19v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final q:Landroid/widget/ImageView;

.field public final r:Landroid/view/View;

.field private final s:Landroid/os/Handler;

.field private t:Z

.field public u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field public w:Z

.field private x:Z

.field public y:LX/2pO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1834323
    const-class v0, LX/Bwm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bwm;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1834321
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bwm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1834322
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1834319
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bwm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834320
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1834254
    invoke-direct {p0, p1, p2, p3}, LX/BwQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834255
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/Bwm;->y:LX/2pO;

    .line 1834256
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/Bwm;->s:Landroid/os/Handler;

    .line 1834257
    const-class v0, LX/Bwm;

    invoke-static {v0, p0}, LX/Bwm;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1834258
    const v0, 0x7f0d2645

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bwm;->r:Landroid/view/View;

    .line 1834259
    const v0, 0x7f0d1779

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Bwm;->q:Landroid/widget/ImageView;

    .line 1834260
    iget-object v0, p0, LX/Bwm;->q:Landroid/widget/ImageView;

    const v1, 0x7f0214b4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1834261
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bwl;

    invoke-direct {v1, p0}, LX/Bwl;-><init>(LX/Bwm;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834262
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bwk;

    invoke-direct {v1, p0}, LX/Bwk;-><init>(LX/Bwm;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834263
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Bwm;->t:Z

    .line 1834264
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bwm;

    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-static {p0}, LX/19v;->a(LX/0QB;)LX/19v;

    move-result-object p0

    check-cast p0, LX/19v;

    iput-object v1, p1, LX/Bwm;->f:LX/0yc;

    iput-object p0, p1, LX/Bwm;->o:LX/19v;

    return-void
.end method

.method public static a$redex0(LX/Bwm;LX/2qV;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1834293
    iget-object v0, p0, LX/Bwm;->f:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1834294
    invoke-direct {p0}, LX/Bwm;->v()V

    .line 1834295
    :cond_0
    :goto_0
    return-void

    .line 1834296
    :cond_1
    sget-object v0, LX/Bwj;->a:[I

    iget-object v1, p0, LX/Bwm;->y:LX/2pO;

    invoke-virtual {v1}, LX/2pO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1834297
    :pswitch_0
    invoke-virtual {p0}, LX/BwQ;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1834298
    iget-object v0, p0, LX/Bwm;->q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1834299
    :cond_2
    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1834300
    invoke-direct {p0}, LX/Bwm;->v()V

    .line 1834301
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 1834302
    iget-boolean v0, p0, LX/Bwm;->t:Z

    if-nez v0, :cond_3

    if-nez v2, :cond_3

    sget-object v0, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne p1, v0, :cond_3

    .line 1834303
    iget-object v0, p0, LX/Bwm;->u:Ljava/lang/String;

    .line 1834304
    iget-object v3, p0, LX/Bwm;->s:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;

    invoke-direct {v4, p0, v0}, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;-><init>(LX/Bwm;Ljava/lang/String;)V

    iget-object v0, p0, LX/BwQ;->b:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->L()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/BwQ;->b:LX/0tQ;

    .line 1834305
    iget-object v1, v0, LX/0tQ;->a:LX/0ad;

    sget v5, LX/0wh;->a:I

    const/16 p1, 0x3e8

    invoke-interface {v1, v5, p1}, LX/0ad;->a(II)I

    move-result v1

    move v0, v1

    .line 1834306
    int-to-long v0, v0

    :goto_2
    const v5, 0x19488ca9

    invoke-static {v3, v4, v0, v1, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1834307
    :cond_3
    if-eqz v2, :cond_0

    iget-boolean v0, p0, LX/Bwm;->x:Z

    if-nez v0, :cond_0

    .line 1834308
    iget-object v0, p0, LX/Bwm;->o:LX/19v;

    iget-object v1, p0, LX/Bwm;->u:Ljava/lang/String;

    iget-object v2, p0, LX/Bwm;->v:Ljava/lang/String;

    .line 1834309
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    sget-object v5, LX/7Jd;->BUFFERED_VIDEO_PLAYBACK:LX/7Jd;

    invoke-static {v0, v1, v3, v4, v5}, LX/19v;->a(LX/19v;Ljava/lang/String;LX/7Jg;LX/7Jf;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1834310
    sget-object v4, LX/7Je;->DOWNLOAD_TYPE:LX/7Je;

    iget-object v4, v4, LX/7Je;->value:Ljava/lang/String;

    sget-object v5, LX/7Jc;->EPHEMERAL:LX/7Jc;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1834311
    sget-object v4, LX/7Je;->PLAYER_ORIGIN:LX/7Je;

    iget-object v4, v4, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1834312
    iget-object v4, v0, LX/19v;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1834313
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Bwm;->x:Z

    goto/16 :goto_0

    .line 1834314
    :cond_4
    const-wide/16 v0, 0x4b0

    goto :goto_2

    .line 1834315
    :cond_5
    iget-object v0, p0, LX/Bwm;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1834316
    iget-object v0, p0, LX/Bwm;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1834317
    invoke-virtual {p0}, LX/BwQ;->g()V

    goto/16 :goto_0

    .line 1834318
    :pswitch_1
    invoke-direct {p0}, LX/Bwm;->v()V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1834290
    iget-object v0, p0, LX/Bwm;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1834291
    invoke-virtual {p0}, LX/BwQ;->g()V

    .line 1834292
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1834270
    invoke-super {p0, p1, p2}, LX/BwQ;->a(LX/2pa;Z)V

    .line 1834271
    if-eqz p2, :cond_0

    .line 1834272
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/Bwm;->y:LX/2pO;

    .line 1834273
    :cond_0
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Bwm;->u:Ljava/lang/String;

    .line 1834274
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_2

    .line 1834275
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1834276
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 1834277
    iget-object v0, v0, LX/04D;->origin:Ljava/lang/String;

    iput-object v0, p0, LX/Bwm;->v:Ljava/lang/String;

    .line 1834278
    :goto_0
    invoke-virtual {p1}, LX/2pa;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1834279
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Bwm;->t:Z

    .line 1834280
    :goto_1
    invoke-virtual {p0}, LX/BwQ;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1834281
    iget-object v0, p0, LX/Bwm;->q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1834282
    iget-object v0, p0, LX/Bwm;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1834283
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834284
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1834285
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1834286
    invoke-static {p0, v0}, LX/Bwm;->a$redex0(LX/Bwm;LX/2qV;)V

    .line 1834287
    return-void

    .line 1834288
    :cond_2
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iget-object v0, v0, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bwm;->v:Ljava/lang/String;

    goto :goto_0

    .line 1834289
    :cond_3
    iput-boolean v2, p0, LX/Bwm;->t:Z

    goto :goto_1
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1834266
    invoke-super {p0}, LX/BwQ;->d()V

    .line 1834267
    iput-boolean v0, p0, LX/Bwm;->x:Z

    .line 1834268
    iput-boolean v0, p0, LX/Bwm;->w:Z

    .line 1834269
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1834265
    const v0, 0x7f030fde

    return v0
.end method
