.class public LX/AzY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field private final a:LX/1RN;

.field private final b:Lcom/facebook/productionprompts/model/ProductionPrompt;

.field private final c:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1RN;)V
    .locals 2

    .prologue
    .line 1732653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732654
    iput-object p2, p0, LX/AzY;->a:LX/1RN;

    .line 1732655
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1732656
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1732657
    check-cast v0, LX/1kW;

    .line 1732658
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1732659
    iput-object v0, p0, LX/AzY;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1732660
    new-instance v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    invoke-direct {v0, p1}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AzY;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    .line 1732661
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732663
    iget-object v0, p0, LX/AzY;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732662
    iget-object v0, p0, LX/AzY;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732651
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732652
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732650
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732649
    iget-object v0, p0, LX/AzY;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732646
    iget-object v0, p0, LX/AzY;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    if-eqz v0, :cond_0

    .line 1732647
    iget-object v0, p0, LX/AzY;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    iget-object v1, p0, LX/AzY;->a:LX/1RN;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(LX/1RN;)V

    .line 1732648
    :cond_0
    iget-object v0, p0, LX/AzY;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732645
    const/4 v0, 0x0

    return-object v0
.end method
