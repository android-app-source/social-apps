.class public LX/BJ2;
.super LX/3hi;
.source ""


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:LX/1bh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1771466
    const/16 v0, 0x14

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, LX/BJ2;-><init>(II)V

    .line 1771467
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1771447
    const/high16 v0, 0x4d000000    # 1.34217728E8f

    invoke-direct {p0, p1, p2, v0}, LX/BJ2;-><init>(III)V

    .line 1771448
    return-void
.end method

.method public constructor <init>(III)V
    .locals 3

    .prologue
    .line 1771459
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 1771460
    iput p1, p0, LX/BJ2;->c:I

    .line 1771461
    iput p2, p0, LX/BJ2;->d:I

    .line 1771462
    iput p3, p0, LX/BJ2;->e:I

    .line 1771463
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/BJ2;->b:Landroid/graphics/Paint;

    .line 1771464
    new-instance v0, LX/1ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "darkblur:radius="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/BJ2;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":downscale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/BJ2;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/BJ2;->f:LX/1bh;

    .line 1771465
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FZ;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1771450
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, LX/BJ2;->d:I

    div-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, LX/BJ2;->d:I

    div-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v1

    .line 1771451
    :try_start_0
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1771452
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1771453
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1771454
    iget-object v4, p0, LX/BJ2;->b:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/PorterDuffColorFilter;

    iget v6, p0, LX/BJ2;->e:I

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1771455
    const/4 v4, 0x0

    iget-object v5, p0, LX/BJ2;->b:Landroid/graphics/Paint;

    invoke-virtual {v2, p1, v4, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1771456
    const/4 v2, 0x2

    iget v3, p0, LX/BJ2;->c:I

    invoke-static {v0, v2, v3}, Lcom/facebook/imagepipeline/nativecode/NativeBlurFilter;->a(Landroid/graphics/Bitmap;II)V

    .line 1771457
    invoke-static {v1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1771458
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final a()LX/1bh;
    .locals 1

    .prologue
    .line 1771449
    iget-object v0, p0, LX/BJ2;->f:LX/1bh;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1771446
    const-string v0, "DarkenAndBlurPostprocessor"

    return-object v0
.end method
