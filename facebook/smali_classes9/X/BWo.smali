.class public LX/BWo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWl;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1792448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1792449
    return-void
.end method

.method public static a(LX/0QB;)LX/BWo;
    .locals 1

    .prologue
    .line 1792445
    new-instance v0, LX/BWo;

    invoke-direct {v0}, LX/BWo;-><init>()V

    .line 1792446
    move-object v0, v0

    .line 1792447
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792444
    const v0, 0x7f0306fa

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1792438
    const v0, 0x7f0d12af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public final b(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792443
    const v0, 0x7f0d12ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/view/View;)Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 1792450
    const v0, 0x7f0d12ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public final d(Landroid/view/View;)Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;
    .locals 1

    .prologue
    .line 1792442
    const v0, 0x7f0d12ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    return-object v0
.end method

.method public final e(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792441
    const v0, 0x7f0d12aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final f(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792440
    const v0, 0x7f0d12ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/view/View;)Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1792439
    const v0, 0x7f0d12b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    return-object v0
.end method
