.class public LX/B3l;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/B0L;

.field public final c:Lcom/facebook/heisman/category/CategoryBrowserFragment;

.field private final d:Z

.field public final e:LX/B0S;

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/B3i;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/B3t;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1740872
    const-class v0, LX/B3l;

    sput-object v0, LX/B3l;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserFragment;ZLX/B0T;)V
    .locals 1
    .param p1    # Lcom/facebook/heisman/category/CategoryBrowserFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740874
    new-instance v0, LX/B3k;

    invoke-direct {v0, p0}, LX/B3k;-><init>(LX/B3l;)V

    iput-object v0, p0, LX/B3l;->b:LX/B0L;

    .line 1740875
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1740876
    iput-object v0, p0, LX/B3l;->f:LX/0Ot;

    .line 1740877
    iput-object p1, p0, LX/B3l;->c:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    .line 1740878
    iput-boolean p2, p0, LX/B3l;->d:Z

    .line 1740879
    iget-object v0, p0, LX/B3l;->b:LX/B0L;

    .line 1740880
    new-instance p2, LX/B0S;

    const-class p1, LX/B0P;

    invoke-interface {p3, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B0P;

    invoke-direct {p2, v0, p1}, LX/B0S;-><init>(LX/B0L;LX/B0P;)V

    .line 1740881
    move-object v0, p2

    .line 1740882
    iput-object v0, p0, LX/B3l;->e:LX/B0S;

    .line 1740883
    return-void
.end method

.method public static d(LX/B3l;)V
    .locals 2

    .prologue
    .line 1740884
    iget-object v0, p0, LX/B3l;->i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    if-eqz v0, :cond_0

    .line 1740885
    iget-object v0, p0, LX/B3l;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, LX/B3l;->i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1740886
    iget-object v0, p0, LX/B3l;->i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    .line 1740887
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->c:Z

    .line 1740888
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1740889
    iget-object v0, p0, LX/B3l;->e:LX/B0S;

    const-string v1, "ProfilePictureOverlayCategoryBrowserInitialLoad"

    iget-object v2, p0, LX/B3l;->g:LX/B3i;

    iget-boolean v3, p0, LX/B3l;->d:Z

    .line 1740890
    new-instance v5, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    invoke-direct {v5, v3}, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;-><init>(Z)V

    .line 1740891
    const/16 v4, 0x1223

    invoke-static {v2, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v4, 0x1032

    invoke-static {v2, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const-class v4, LX/2U5;

    invoke-interface {v2, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2U5;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1740892
    iput-object v6, v5, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->a:LX/0Or;

    iput-object v7, v5, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->b:LX/0Or;

    iput-object v4, v5, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->d:LX/2U5;

    iput-object p0, v5, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->e:LX/0Ot;

    .line 1740893
    move-object v2, v5

    .line 1740894
    invoke-virtual {v0, v1, v2}, LX/B0S;->a(Ljava/lang/String;LX/B0U;)V

    .line 1740895
    return-void
.end method
