.class public final LX/AVG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final synthetic a:LX/AVH;


# direct methods
.method public constructor <init>(LX/AVH;)V
    .locals 0

    .prologue
    .line 1679105
    iput-object p1, p0, LX/AVG;->a:LX/AVH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1679106
    const-string v0, "live"

    const-string v1, "live"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1679107
    const/4 v0, 0x0

    .line 1679108
    :goto_0
    return-object v0

    .line 1679109
    :cond_0
    const-string v0, "text"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1679110
    new-instance v1, LX/AWP;

    invoke-direct {v1}, LX/AWP;-><init>()V

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1679111
    iput-object v2, v1, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1679112
    move-object v1, v1

    .line 1679113
    sget-object v2, LX/21D;->URI_HANDLER:LX/21D;

    .line 1679114
    iput-object v2, v1, LX/AWP;->k:LX/21D;

    .line 1679115
    move-object v1, v1

    .line 1679116
    iput-object v0, v1, LX/AWP;->a:Ljava/lang/String;

    .line 1679117
    move-object v0, v1

    .line 1679118
    invoke-virtual {v0}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v0

    .line 1679119
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZ)Landroid/os/Bundle;

    move-result-object v1

    .line 1679120
    iget-object v0, p0, LX/AVG;->a:LX/AVH;

    iget-object v0, v0, LX/AVH;->b:LX/1b2;

    invoke-virtual {v0, p1}, LX/1b2;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1679121
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0
.end method
