.class public LX/CAt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Ck;

.field public final f:LX/0tX;

.field public final g:LX/0kL;

.field public final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1855797
    const-class v0, LX/CAt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CAt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0tX;LX/1Ck;LX/0kL;LX/03V;LX/0Or;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0kL;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855799
    iput-object p1, p0, LX/CAt;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1855800
    iput-object p2, p0, LX/CAt;->c:LX/17Y;

    .line 1855801
    iput-object p3, p0, LX/CAt;->f:LX/0tX;

    .line 1855802
    iput-object p4, p0, LX/CAt;->e:LX/1Ck;

    .line 1855803
    iput-object p5, p0, LX/CAt;->g:LX/0kL;

    .line 1855804
    iput-object p6, p0, LX/CAt;->h:LX/03V;

    .line 1855805
    iput-object p7, p0, LX/CAt;->d:LX/0Or;

    .line 1855806
    return-void
.end method

.method public static a(LX/0QB;)LX/CAt;
    .locals 11

    .prologue
    .line 1855807
    const-class v1, LX/CAt;

    monitor-enter v1

    .line 1855808
    :try_start_0
    sget-object v0, LX/CAt;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855809
    sput-object v2, LX/CAt;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855810
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855811
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855812
    new-instance v3, LX/CAt;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const/16 v10, 0x15e7

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/CAt;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0tX;LX/1Ck;LX/0kL;LX/03V;LX/0Or;)V

    .line 1855813
    move-object v0, v3

    .line 1855814
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855815
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855816
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
