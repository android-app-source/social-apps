.class public LX/CEZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Landroid/graphics/Rect;


# instance fields
.field public final c:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/CEY;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field public e:LX/CEY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1861433
    const-class v0, LX/CEZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CEZ;->a:Ljava/lang/String;

    .line 1861434
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/CEZ;->b:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(LX/1Bv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1861435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1861436
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    .line 1861437
    invoke-virtual {p1}, LX/1Bv;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/CEZ;->d:Z

    .line 1861438
    return-void
.end method

.method public static c(LX/CEY;)V
    .locals 1

    .prologue
    .line 1861439
    const/4 v0, 0x0

    invoke-interface {p0, v0}, LX/CEY;->setVideoListener(LX/3It;)V

    .line 1861440
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-interface {p0, v0}, LX/CEY;->b(LX/04g;)V

    .line 1861441
    return-void
.end method

.method public static c(LX/CEZ;)V
    .locals 3

    .prologue
    .line 1861442
    iget-object v0, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CEZ;->d:Z

    if-nez v0, :cond_1

    .line 1861443
    :cond_0
    :goto_0
    return-void

    .line 1861444
    :cond_1
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    sget-object v1, LX/CEZ;->b:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, LX/CEY;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    .line 1861445
    :goto_1
    iget-object v1, p0, LX/CEZ;->e:LX/CEY;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/CEZ;->e:LX/CEY;

    invoke-interface {v1}, LX/CEY;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1861446
    :cond_2
    iget-object v1, p0, LX/CEZ;->e:LX/CEY;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 1861447
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    invoke-static {v0}, LX/CEZ;->c(LX/CEY;)V

    .line 1861448
    :cond_3
    :goto_2
    iget-object v0, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1861449
    iget-object v0, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CEY;

    .line 1861450
    invoke-interface {v0}, LX/CEY;->r()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, LX/CEZ;->b:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, LX/CEY;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1861451
    :cond_4
    sget-object v0, LX/CEZ;->a:Ljava/lang/String;

    const-string v1, "getNextVideoToPlay: unexpected video view in queue"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1861452
    :cond_5
    iget-object v1, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 1861453
    iget-object v1, p0, LX/CEZ;->e:LX/CEY;

    if-ne v0, v1, :cond_6

    iget-object v1, p0, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_3

    .line 1861454
    :cond_6
    :goto_3
    move-object v0, v0

    .line 1861455
    iput-object v0, p0, LX/CEZ;->e:LX/CEY;

    .line 1861456
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    if-eqz v0, :cond_0

    .line 1861457
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    new-instance v1, LX/CEX;

    invoke-direct {v1, p0}, LX/CEX;-><init>(LX/CEZ;)V

    invoke-interface {v0, v1}, LX/CEY;->setVideoListener(LX/3It;)V

    .line 1861458
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    invoke-interface {v0}, LX/CEY;->a()V

    .line 1861459
    iget-object v0, p0, LX/CEZ;->e:LX/CEY;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-interface {v0, v1}, LX/CEY;->a(LX/04g;)V

    goto :goto_0

    .line 1861460
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method
