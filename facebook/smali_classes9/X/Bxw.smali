.class public final LX/Bxw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1836370
    iput-object p1, p0, LX/Bxw;->c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    iput-object p2, p0, LX/Bxw;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Bxw;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6515ace4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1836371
    iget-object v1, p0, LX/Bxw;->c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->g:LX/6VI;

    invoke-virtual {v1}, LX/6VI;->b()V

    .line 1836372
    iget-object v1, p0, LX/Bxw;->c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    iget-object v2, p0, LX/Bxw;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Bxw;->b:Ljava/lang/String;

    .line 1836373
    sget-object v5, LX/21D;->NEWSFEED:LX/21D;

    const-string v6, "birthdayEntryInFeedAggregatedStory"

    invoke-static {v5, v6}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    .line 1836374
    new-instance v6, LX/89I;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    sget-object v9, LX/2rw;->USER:LX/2rw;

    invoke-direct {v6, v7, v8, v9}, LX/89I;-><init>(JLX/2rw;)V

    .line 1836375
    iput-object v3, v6, LX/89I;->c:Ljava/lang/String;

    .line 1836376
    move-object v6, v6

    .line 1836377
    invoke-virtual {v6}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    .line 1836378
    iget-object v6, v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->h:LX/CEF;

    invoke-virtual {v6}, LX/CEF;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, LX/89K;

    invoke-direct {v6}, LX/89K;-><init>()V

    invoke-static {}, LX/CEK;->c()LX/CEK;

    move-result-object v6

    invoke-static {v6}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v6

    :goto_0
    move-object v6, v6

    .line 1836379
    invoke-virtual {v5, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    .line 1836380
    iget-object v6, v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->d:LX/1Kf;

    iget-object v7, v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->g:LX/6VI;

    .line 1836381
    iget-object v8, v7, LX/6VI;->b:Ljava/lang/String;

    move-object v7, v8

    .line 1836382
    const/16 v8, 0x6dc

    iget-object v9, v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->c:Landroid/app/Activity;

    invoke-interface {v6, v7, v5, v8, v9}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1836383
    const v1, 0x3aa33e55

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method
