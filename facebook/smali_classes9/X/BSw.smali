.class public final LX/BSw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V
    .locals 0

    .prologue
    .line 1786308
    iput-object p1, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x32f9b4a3

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1786309
    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v3, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v3, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    if-nez v3, :cond_1

    .line 1786310
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    .line 1786311
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    if-eqz v0, :cond_2

    .line 1786312
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    const-string v2, "high"

    invoke-interface {v0, v2}, LX/BSm;->a(Ljava/lang/String;)V

    .line 1786313
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->C:LX/BTd;

    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v3, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    iget-object v4, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v4, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->n:Ljava/lang/String;

    .line 1786314
    new-instance v5, LX/BTc;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x12c4

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/BTg;->b(LX/0QB;)LX/BTg;

    move-result-object v8

    check-cast v8, LX/BTg;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    move-object v10, v2

    move-object v11, v3

    move-object v12, v4

    invoke-direct/range {v5 .. v12}, LX/BTc;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/BTg;LX/0Zb;Landroid/content/Context;LX/BSm;Ljava/lang/String;)V

    .line 1786315
    move-object v0, v5

    .line 1786316
    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v2, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->D:LX/BTg;

    invoke-virtual {v2}, LX/BTg;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1786317
    iget-object v2, v0, LX/BTc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/BTc;->a:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    move v2, v2

    .line 1786318
    if-eqz v2, :cond_0

    .line 1786319
    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1786320
    new-instance v3, LX/0ju;

    invoke-direct {v3, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1786321
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0813ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0813ee

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0813f0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/BTc;->i:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0813f1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/BTc;->j:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1786322
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 1786323
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1786324
    iget-object v2, v0, LX/BTc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/BTc;->a:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1786325
    :cond_0
    :goto_1
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    if-eqz v0, :cond_3

    .line 1786326
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->A:LX/0wL;

    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0813e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1786327
    invoke-static {v0, p1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1786328
    :goto_2
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    .line 1786329
    invoke-static {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i$redex0(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786330
    const v0, -0x4a103a0c

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1786331
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1786332
    :cond_2
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    const-string v2, "standard"

    invoke-interface {v0, v2}, LX/BSm;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1786333
    :cond_3
    iget-object v0, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->A:LX/0wL;

    iget-object v2, p0, LX/BSw;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0813e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1786334
    invoke-static {v0, p1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1786335
    goto :goto_2
.end method
