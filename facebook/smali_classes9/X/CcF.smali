.class public final LX/CcF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1920740
    iput-object p1, p0, LX/CcF;->e:LX/CcO;

    iput-object p2, p0, LX/CcF;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CcF;->b:LX/5kD;

    iput-object p4, p0, LX/CcF;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CcF;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1920741
    iget-object v0, p0, LX/CcF;->e:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v3, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    const/16 v0, 0x7d3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, p0, LX/CcF;->e:LX/CcO;

    iget-object v5, v0, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    iget-object v6, p0, LX/CcF;->a:Landroid/content/Context;

    iget-object v0, p0, LX/CcF;->b:LX/5kD;

    invoke-interface {v0}, LX/5kD;->R()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CcF;->e:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->y:LX/0ad;

    sget-short v7, LX/1xL;->a:S

    invoke-interface {v0, v7, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CcF;->e:LX/CcO;

    iget-object v7, p0, LX/CcF;->c:Ljava/lang/String;

    iget-object v8, p0, LX/CcF;->d:Ljava/lang/String;

    invoke-static {v0, v7, v8}, LX/CcO;->a(LX/CcO;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v7, p0, LX/CcF;->b:LX/5kD;

    invoke-interface {v7}, LX/5kD;->R()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, LX/CcF;->e:LX/CcO;

    iget-object v7, v7, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v7, v7, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->y:LX/0ad;

    sget-short v8, LX/1xL;->b:S

    invoke-interface {v7, v8, v2}, LX/0ad;->a(SZ)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, LX/CcF;->e:LX/CcO;

    iget-object v8, p0, LX/CcF;->c:Ljava/lang/String;

    iget-object v9, p0, LX/CcF;->d:Ljava/lang/String;

    invoke-static {v7, v8, v9}, LX/CcO;->a(LX/CcO;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v2, v1

    :cond_0
    const/16 p1, 0x21

    .line 1920742
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 1920743
    if-eqz v2, :cond_3

    .line 1920744
    new-instance v7, LX/CbU;

    invoke-direct {v7, v5, v6}, LX/CbU;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Landroid/content/Context;)V

    .line 1920745
    new-instance v9, LX/47x;

    invoke-direct {v9, v8}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v8, 0x7f0819bf

    invoke-virtual {v9, v8}, LX/47x;->a(I)LX/47x;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v8

    invoke-virtual {v8, v7, p1}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v7

    new-instance v8, Landroid/text/style/UnderlineSpan;

    invoke-direct {v8}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v7, v8, p1}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v7

    const v8, 0x7f0819c0

    invoke-virtual {v7, v8}, LX/47x;->a(I)LX/47x;

    move-result-object v7

    invoke-virtual {v7}, LX/47x;->a()LX/47x;

    move-result-object v7

    invoke-virtual {v7}, LX/47x;->a()LX/47x;

    move-result-object v7

    invoke-virtual {v7}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v7

    .line 1920746
    :goto_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v9

    .line 1920747
    new-instance p1, LX/0ju;

    invoke-direct {p1, v6}, LX/0ju;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_5

    const v8, 0x7f0819bb

    :goto_2
    invoke-virtual {p1, v8}, LX/0ju;->a(I)LX/0ju;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v7

    const v8, 0x7f0819c2

    new-instance p1, LX/CbW;

    invoke-direct {p1, v5, v9}, LX/CbW;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v7, v8, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v7

    const v8, 0x7f0819c1

    new-instance p1, LX/CbV;

    invoke-direct {p1, v5, v9}, LX/CbV;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v7, v8, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v7

    invoke-virtual {v7}, LX/0ju;->b()LX/2EJ;

    move-result-object v7

    .line 1920748
    if-eqz v2, :cond_1

    .line 1920749
    const v8, 0x7f0d0578

    invoke-virtual {v7, v8}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1920750
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1920751
    :cond_1
    move-object v0, v9

    .line 1920752
    iget-object v2, p0, LX/CcF;->e:LX/CcO;

    iget-object v5, p0, LX/CcF;->b:LX/5kD;

    iget-object v6, p0, LX/CcF;->c:Ljava/lang/String;

    iget-object v7, p0, LX/CcF;->d:Ljava/lang/String;

    .line 1920753
    new-instance v8, LX/Cc3;

    invoke-direct {v8, v2, v5, v6, v7}, LX/Cc3;-><init>(LX/CcO;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v8

    .line 1920754
    invoke-virtual {v3, v4, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1920755
    return v1

    :cond_2
    move v0, v2

    .line 1920756
    goto/16 :goto_0

    .line 1920757
    :cond_3
    if-eqz v0, :cond_4

    .line 1920758
    new-instance v7, Landroid/text/SpannableString;

    const v9, 0x7f0819be

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1920759
    :cond_4
    new-instance v7, Landroid/text/SpannableString;

    const v9, 0x7f0819bc

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1920760
    :cond_5
    const v8, 0x7f0819b9

    goto :goto_2
.end method
