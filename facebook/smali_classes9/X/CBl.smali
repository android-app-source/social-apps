.class public final enum LX/CBl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBl;

.field public static final enum UNKNOWN:LX/CBl;

.field public static final enum USE_PRIMARY_ACTION:LX/CBl;

.field public static final enum USE_SECONDARY_ACTION:LX/CBl;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856794
    new-instance v0, LX/CBl;

    const-string v1, "USE_PRIMARY_ACTION"

    invoke-direct {v0, v1, v2}, LX/CBl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBl;->USE_PRIMARY_ACTION:LX/CBl;

    .line 1856795
    new-instance v0, LX/CBl;

    const-string v1, "USE_SECONDARY_ACTION"

    invoke-direct {v0, v1, v3}, LX/CBl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBl;->USE_SECONDARY_ACTION:LX/CBl;

    .line 1856796
    new-instance v0, LX/CBl;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/CBl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBl;->UNKNOWN:LX/CBl;

    .line 1856797
    const/4 v0, 0x3

    new-array v0, v0, [LX/CBl;

    sget-object v1, LX/CBl;->USE_PRIMARY_ACTION:LX/CBl;

    aput-object v1, v0, v2

    sget-object v1, LX/CBl;->USE_SECONDARY_ACTION:LX/CBl;

    aput-object v1, v0, v3

    sget-object v1, LX/CBl;->UNKNOWN:LX/CBl;

    aput-object v1, v0, v4

    sput-object v0, LX/CBl;->$VALUES:[LX/CBl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856798
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBl;
    .locals 1

    .prologue
    .line 1856799
    if-nez p0, :cond_0

    .line 1856800
    :try_start_0
    sget-object v0, LX/CBl;->UNKNOWN:LX/CBl;

    .line 1856801
    :goto_0
    return-object v0

    .line 1856802
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBl;->valueOf(Ljava/lang/String;)LX/CBl;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856803
    :catch_0
    sget-object v0, LX/CBl;->UNKNOWN:LX/CBl;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBl;
    .locals 1

    .prologue
    .line 1856804
    const-class v0, LX/CBl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBl;

    return-object v0
.end method

.method public static values()[LX/CBl;
    .locals 1

    .prologue
    .line 1856805
    sget-object v0, LX/CBl;->$VALUES:[LX/CBl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBl;

    return-object v0
.end method
