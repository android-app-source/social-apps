.class public LX/CXT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final c:LX/CXU;


# direct methods
.method private constructor <init>(LX/CXU;LX/0am;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CXU;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1910075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910076
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910077
    sget-object v0, LX/CXU;->DEFAULT_PERCENTAGE:LX/CXU;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v3

    xor-int/2addr v0, v3

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1910078
    iput-object p1, p0, LX/CXT;->c:LX/CXU;

    .line 1910079
    iput-object p2, p0, LX/CXT;->a:LX/0am;

    .line 1910080
    iput-object p3, p0, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1910081
    return-void

    :cond_0
    move v0, v2

    .line 1910082
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;
    .locals 3

    .prologue
    .line 1910074
    new-instance v0, LX/CXT;

    sget-object v1, LX/CXU;->CUSTOM:LX/CXU;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, LX/CXT;-><init>(LX/CXU;LX/0am;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;
    .locals 3

    .prologue
    .line 1910073
    new-instance v0, LX/CXT;

    sget-object v1, LX/CXU;->DEFAULT_PERCENTAGE:LX/CXU;

    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, LX/CXT;-><init>(LX/CXU;LX/0am;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/CXT;
    .locals 4

    .prologue
    .line 1910072
    new-instance v0, LX/CXT;

    sget-object v1, LX/CXU;->CASH:LX/CXU;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/CXT;-><init>(LX/CXU;LX/0am;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/Integer;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 6

    .prologue
    .line 1910060
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1910061
    sget-object v1, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    .line 1910062
    iget-object v2, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v2

    .line 1910063
    new-instance v3, Ljava/math/BigDecimal;

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Lcom/facebook/payments/currency/CurrencyAmount;->a:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1910064
    iget-object v3, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1910065
    invoke-static {v3}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Ljava/lang/String;)I

    move-result v3

    .line 1910066
    int-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 1910067
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1910068
    iget-object v5, p0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1910069
    invoke-virtual {v2, v3, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    move-object v1, v4

    .line 1910070
    move-object v0, v1

    .line 1910071
    return-object v0
.end method


# virtual methods
.method public final a()LX/CXU;
    .locals 1

    .prologue
    .line 1910059
    iget-object v0, p0, LX/CXT;->c:LX/CXU;

    return-object v0
.end method

.method public final c()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1910057
    iget-object v0, p0, LX/CXT;->a:LX/0am;

    return-object v0
.end method

.method public final d()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1

    .prologue
    .line 1910058
    iget-object v0, p0, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    return-object v0
.end method
