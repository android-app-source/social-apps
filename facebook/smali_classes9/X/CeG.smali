.class public final LX/CeG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/2ct;

.field public final b:Lcom/facebook/placetips/bootstrap/PresenceSource;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

.field public g:LX/175;

.field public h:LX/175;

.field public i:Z

.field public j:LX/175;

.field public k:LX/175;

.field public l:LX/175;

.field public m:LX/9qT;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:LX/Cdj;

.field public q:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;


# direct methods
.method public constructor <init>(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1924491
    iput-object p1, p0, LX/CeG;->a:LX/2ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1924492
    iput-object v0, p0, LX/CeG;->p:LX/Cdj;

    .line 1924493
    iput-object v0, p0, LX/CeG;->q:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;

    .line 1924494
    iput-object p2, p0, LX/CeG;->b:Lcom/facebook/placetips/bootstrap/PresenceSource;

    .line 1924495
    return-void
.end method


# virtual methods
.method public final a()LX/0bZ;
    .locals 1

    .prologue
    .line 1924498
    iget-object v0, p0, LX/CeG;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924499
    iget-object v0, p0, LX/CeG;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924500
    iget-object v0, p0, LX/CeG;->g:LX/175;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924501
    iget-object v0, p0, LX/CeG;->a:LX/2ct;

    invoke-static {v0, p0}, LX/2ct;->a$redex0(LX/2ct;LX/CeG;)LX/0bZ;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/placetips/bootstrap/PresenceDescription;
    .locals 22

    .prologue
    .line 1924496
    move-object/from16 v0, p0

    iget-object v2, v0, LX/CeG;->a:LX/2ct;

    iget-object v2, v2, LX/2ct;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 1924497
    new-instance v2, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/CeG;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/CeG;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/CeG;->e:LX/0Px;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/CeG;->g:LX/175;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/CeG;->h:LX/175;

    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/CeG;->i:Z

    move-object/from16 v0, p0

    iget-object v13, v0, LX/CeG;->j:LX/175;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/CeG;->k:LX/175;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/CeG;->l:LX/175;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->b:Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->f:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->m:LX/9qT;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->n:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CeG;->p:LX/Cdj;

    move-object/from16 v21, v0

    move-wide v8, v6

    invoke-direct/range {v2 .. v21}, Lcom/facebook/placetips/bootstrap/PresenceDescription;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;JJLX/175;LX/175;ZLX/175;LX/175;LX/175;Lcom/facebook/placetips/bootstrap/PresenceSource;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;LX/9qT;Ljava/lang/String;Ljava/lang/String;LX/Cdj;)V

    return-object v2
.end method
