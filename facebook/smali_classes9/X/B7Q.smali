.class public final LX/B7Q;
.super LX/B7B;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/B7P;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 2

    .prologue
    .line 1748011
    invoke-direct {p0}, LX/B7B;-><init>()V

    .line 1748012
    if-nez p2, :cond_1

    .line 1748013
    sget-object v0, LX/B7P;->NONE:LX/B7P;

    iput-object v0, p0, LX/B7Q;->i:LX/B7P;

    .line 1748014
    :cond_0
    :goto_0
    return-void

    .line 1748015
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->b:Ljava/lang/String;

    .line 1748016
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->d:Ljava/lang/String;

    .line 1748017
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aK()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->a:Ljava/lang/String;

    .line 1748018
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bb()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->c:Ljava/lang/String;

    .line 1748019
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->J()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->e:Ljava/lang/String;

    .line 1748020
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->f:Ljava/lang/String;

    .line 1748021
    sget-object v0, LX/B7P;->WEBSITE:LX/B7P;

    iput-object v0, p0, LX/B7Q;->i:LX/B7P;

    .line 1748022
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->q()Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;

    move-result-object v0

    .line 1748023
    if-eqz v0, :cond_0

    .line 1748024
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7Q;->a:Ljava/lang/String;

    .line 1748025
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7Q;->c:Ljava/lang/String;

    .line 1748026
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7Q;->e:Ljava/lang/String;

    .line 1748027
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/B7Q;->g:Ljava/lang/String;

    .line 1748028
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenThankYouPage;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7Q;->h:Ljava/lang/String;

    .line 1748029
    iget-object v0, p0, LX/B7Q;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B7Q;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1748030
    sget-object v0, LX/B7P;->MESSENGER:LX/B7P;

    iput-object v0, p0, LX/B7Q;->i:LX/B7P;

    .line 1748031
    :cond_2
    iget-object v0, p0, LX/B7Q;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B7Q;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748032
    sget-object v0, LX/B7P;->CALL:LX/B7P;

    iput-object v0, p0, LX/B7Q;->i:LX/B7P;

    goto :goto_0
.end method
