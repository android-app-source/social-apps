.class public LX/CXh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1910565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;)LX/0Px;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "generateCheckoutConfigPrices"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsInterfaces$PagesPlatformPaymentOrderInfoFragment$;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1910597
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1910598
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    .line 1910599
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1910600
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1910601
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v10, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v7, v6, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v8, v2}, LX/15i;->j(II)I

    move-result v7

    int-to-long v8, v7

    invoke-direct {v10, v6, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-static {v0, v10}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1910602
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1910603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    .line 1910604
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1910605
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1910606
    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v10, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v7, v6, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v8, v2}, LX/15i;->j(II)I

    move-result v7

    int-to-long v8, v7

    invoke-direct {v10, v6, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-static {v0, v10}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1910607
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1910608
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 0
    .param p0    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1910610
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 1910611
    invoke-virtual {p0, p1}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object p0

    .line 1910612
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-nez p0, :cond_0

    move-object p0, p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1910609
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/CU6;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CTK;",
            "Ljava/lang/String;",
            "LX/CSY;",
            "LX/CSY;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;I",
            "Lcom/facebook/fig/textinput/FigEditText;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$FormFieldValuesChangeListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910582
    iget-object v0, p0, LX/CTK;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1910583
    const/4 v0, 0x0

    invoke-virtual {p7, v0}, Lcom/facebook/fig/textinput/FigEditText;->setVisibility(I)V

    .line 1910584
    invoke-static/range {p0 .. p8}, LX/CXh;->b(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1910585
    :goto_0
    return-void

    .line 1910586
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p7, v0}, Lcom/facebook/fig/textinput/FigEditText;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CTK;",
            "Ljava/lang/String;",
            "LX/CSY;",
            "LX/CSY;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/facebook/fig/textinput/FigEditText;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$FormFieldValuesChangeListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910587
    const/4 v1, 0x0

    .line 1910588
    if-eqz p2, :cond_0

    .line 1910589
    invoke-virtual {p2, p1}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1910590
    :goto_0
    new-instance v1, LX/CXg;

    invoke-direct {v1, p3, p1, p7, p0}, LX/CXg;-><init>(LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;)V

    invoke-virtual {p6, v1}, Lcom/facebook/fig/textinput/FigEditText;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1910591
    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1910592
    return-void

    .line 1910593
    :cond_0
    invoke-virtual {p4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1910594
    invoke-virtual {p4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1910595
    :cond_1
    invoke-virtual {p5, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1910596
    invoke-virtual {p5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CTK;",
            "Ljava/lang/String;",
            "LX/CSY;",
            "LX/CSY;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/facebook/fig/textinput/FigEditText;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$FormFieldValuesChangeListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910577
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v7}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1910578
    invoke-virtual {p7}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/support/design/widget/TextInputLayout;

    if-eqz v0, :cond_0

    .line 1910579
    invoke-virtual {p7}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, p6}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1910580
    :goto_0
    return-void

    .line 1910581
    :cond_0
    invoke-virtual {p7, p6}, Lcom/facebook/fig/textinput/FigEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(LX/CU1;Lcom/facebook/drawee/span/DraweeSpanTextView;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V
    .locals 1

    .prologue
    .line 1910572
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1910573
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910574
    invoke-virtual {p2, p1, p0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1910575
    :goto_0
    return-void

    .line 1910576
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1910571
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static b(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CTK;",
            "Ljava/lang/String;",
            "LX/CSY;",
            "LX/CSY;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;I",
            "Lcom/facebook/fig/textinput/FigEditText;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$FormFieldValuesChangeListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910566
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v7}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1910567
    invoke-virtual {p7}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/support/design/widget/TextInputLayout;

    if-eqz v0, :cond_0

    .line 1910568
    invoke-virtual {p7}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p7}, Lcom/facebook/fig/textinput/FigEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1910569
    :goto_0
    return-void

    .line 1910570
    :cond_0
    invoke-virtual {p7, p6}, Lcom/facebook/fig/textinput/FigEditText;->setHint(I)V

    goto :goto_0
.end method
