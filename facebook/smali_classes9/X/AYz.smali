.class public LX/AYz;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1bG;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/LinearLayout;

.field public final f:Lcom/facebook/fbui/glyph/GlyphView;

.field public final g:Lcom/facebook/fbui/glyph/GlyphView;

.field public final h:Lcom/facebook/fbui/glyph/GlyphView;

.field public final i:Lcom/facebook/widget/text/BetterTextView;

.field public final j:Lcom/facebook/widget/text/BetterTextView;

.field public final k:Landroid/widget/TextView;

.field public l:LX/AYy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686414
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686415
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686412
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686413
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686384
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686385
    const-class v0, LX/AYz;

    invoke-static {v0, p0}, LX/AYz;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686386
    const v0, 0x7f0305af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686387
    const v0, 0x7f0d0f94

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AYz;->c:Landroid/widget/LinearLayout;

    .line 1686388
    const v0, 0x7f0d0f96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYz;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686389
    const v0, 0x7f0d0f99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AYz;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 1686390
    const v0, 0x7f0d0f98

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AYz;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 1686391
    const v0, 0x7f0d0f97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYz;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686392
    const v0, 0x7f0d0f95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYz;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686393
    const v0, 0x7f0d0f9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1686394
    const v1, 0x7f0d0f9a

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, LX/AYz;->k:Landroid/widget/TextView;

    .line 1686395
    new-instance v1, LX/AYw;

    invoke-direct {v1, p0}, LX/AYw;-><init>(LX/AYz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686396
    iget-object v0, p0, LX/AYz;->b:LX/AYq;

    invoke-virtual {v0, p0}, LX/AYq;->a(LX/1bG;)V

    .line 1686397
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AYz;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object p0

    check-cast p0, LX/AYq;

    iput-object v1, p1, LX/AYz;->a:LX/0hB;

    iput-object p0, p1, LX/AYz;->b:LX/AYq;

    return-void
.end method

.method public static c(I)I
    .locals 4

    .prologue
    .line 1686410
    int-to-long v0, p0

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 1686411
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(LX/AYp;LX/AYp;)V
    .locals 1

    .prologue
    .line 1686407
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    if-ne p1, v0, :cond_0

    .line 1686408
    invoke-virtual {p0}, LX/AYz;->g()V

    .line 1686409
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1686405
    invoke-virtual {p0}, LX/AYz;->g()V

    .line 1686406
    const/4 v0, 0x1

    return v0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1686398
    iget-boolean v0, p0, LX/AYz;->m:Z

    if-nez v0, :cond_1

    .line 1686399
    :cond_0
    :goto_0
    return-void

    .line 1686400
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AYz;->m:Z

    .line 1686401
    iget-object v0, p0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 1686402
    iget-object v1, p0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686403
    iget-object v0, p0, LX/AYz;->l:LX/AYy;

    if-eqz v0, :cond_0

    .line 1686404
    iget-object v0, p0, LX/AYz;->l:LX/AYy;

    invoke-virtual {v0}, LX/AYy;->cancel()V

    goto :goto_0
.end method
