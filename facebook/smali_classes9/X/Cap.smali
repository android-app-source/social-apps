.class public final LX/Cap;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Caq;


# direct methods
.method public constructor <init>(LX/Caq;)V
    .locals 0

    .prologue
    .line 1918626
    iput-object p1, p0, LX/Cap;->a:LX/Caq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1918627
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    check-cast p2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918628
    invoke-static {p1}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v1

    .line 1918629
    invoke-static {p2}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v2

    .line 1918630
    iget v0, v1, Landroid/graphics/RectF;->left:F

    iget v3, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v0, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    .line 1918631
    if-eqz v0, :cond_0

    .line 1918632
    :goto_0
    return v0

    :cond_0
    iget v0, v1, Landroid/graphics/RectF;->top:F

    iget v1, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method
