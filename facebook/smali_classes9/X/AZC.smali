.class public final enum LX/AZC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AZC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AZC;

.field public static final enum ELIGIBLE:LX/AZC;

.field public static final enum INSUFFICIENT_BEGINNING_LIVE_TIME:LX/AZC;

.field public static final enum IN_COMMERCIAL_BREAK:LX/AZC;

.field public static final enum NOT_ONBOARDED:LX/AZC;

.field public static final enum TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK:LX/AZC;

.field public static final enum VIEWER_COUNT_TOO_LOW:LX/AZC;

.field public static final enum VIOLATION:LX/AZC;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1686612
    new-instance v0, LX/AZC;

    const-string v1, "ELIGIBLE"

    invoke-direct {v0, v1, v3}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->ELIGIBLE:LX/AZC;

    .line 1686613
    new-instance v0, LX/AZC;

    const-string v1, "IN_COMMERCIAL_BREAK"

    invoke-direct {v0, v1, v4}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->IN_COMMERCIAL_BREAK:LX/AZC;

    .line 1686614
    new-instance v0, LX/AZC;

    const-string v1, "INSUFFICIENT_BEGINNING_LIVE_TIME"

    invoke-direct {v0, v1, v5}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->INSUFFICIENT_BEGINNING_LIVE_TIME:LX/AZC;

    .line 1686615
    new-instance v0, LX/AZC;

    const-string v1, "TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK"

    invoke-direct {v0, v1, v6}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK:LX/AZC;

    .line 1686616
    new-instance v0, LX/AZC;

    const-string v1, "VIEWER_COUNT_TOO_LOW"

    invoke-direct {v0, v1, v7}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->VIEWER_COUNT_TOO_LOW:LX/AZC;

    .line 1686617
    new-instance v0, LX/AZC;

    const-string v1, "VIOLATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->VIOLATION:LX/AZC;

    .line 1686618
    new-instance v0, LX/AZC;

    const-string v1, "NOT_ONBOARDED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/AZC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AZC;->NOT_ONBOARDED:LX/AZC;

    .line 1686619
    const/4 v0, 0x7

    new-array v0, v0, [LX/AZC;

    sget-object v1, LX/AZC;->ELIGIBLE:LX/AZC;

    aput-object v1, v0, v3

    sget-object v1, LX/AZC;->IN_COMMERCIAL_BREAK:LX/AZC;

    aput-object v1, v0, v4

    sget-object v1, LX/AZC;->INSUFFICIENT_BEGINNING_LIVE_TIME:LX/AZC;

    aput-object v1, v0, v5

    sget-object v1, LX/AZC;->TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK:LX/AZC;

    aput-object v1, v0, v6

    sget-object v1, LX/AZC;->VIEWER_COUNT_TOO_LOW:LX/AZC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/AZC;->VIOLATION:LX/AZC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AZC;->NOT_ONBOARDED:LX/AZC;

    aput-object v2, v0, v1

    sput-object v0, LX/AZC;->$VALUES:[LX/AZC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1686620
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AZC;
    .locals 1

    .prologue
    .line 1686621
    const-class v0, LX/AZC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AZC;

    return-object v0
.end method

.method public static values()[LX/AZC;
    .locals 1

    .prologue
    .line 1686622
    sget-object v0, LX/AZC;->$VALUES:[LX/AZC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AZC;

    return-object v0
.end method
