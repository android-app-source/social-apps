.class public abstract LX/CLu;
.super LX/1a1;
.source ""


# instance fields
.field private l:Landroid/content/Context;

.field public m:Lcom/facebook/ui/emoji/model/Emoji;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1879920
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1879921
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/CLu;->l:Landroid/content/Context;

    .line 1879922
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/ui/emoji/model/Emoji;)V
.end method

.method public final b(Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 3

    .prologue
    .line 1879923
    iput-object p1, p0, LX/CLu;->m:Lcom/facebook/ui/emoji/model/Emoji;

    .line 1879924
    invoke-virtual {p0, p1}, LX/CLu;->a(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1879925
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 1879926
    iget v0, p1, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    move v0, v0

    .line 1879927
    if-eqz v0, :cond_0

    .line 1879928
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/CLu;->l:Landroid/content/Context;

    .line 1879929
    iget v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    move v2, v2

    .line 1879930
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1879931
    :goto_0
    return-void

    .line 1879932
    :cond_0
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/CLu;->l:Landroid/content/Context;

    const v2, 0x7f0809e1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
