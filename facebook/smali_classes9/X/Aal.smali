.class public LX/Aal;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:LX/03V;

.field public final d:LX/AYD;

.field public final e:LX/0tX;

.field public final f:Ljava/util/concurrent/ScheduledExecutorService;

.field public g:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1689037
    const-class v0, LX/Aal;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Aal;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/String;LX/AYD;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/AYD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1689038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1689039
    const/4 v0, 0x0

    iput v0, p0, LX/Aal;->h:I

    .line 1689040
    iput-object p1, p0, LX/Aal;->c:LX/03V;

    .line 1689041
    iput-object p2, p0, LX/Aal;->e:LX/0tX;

    .line 1689042
    iput-object p3, p0, LX/Aal;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1689043
    iput-object p4, p0, LX/Aal;->b:Ljava/lang/String;

    .line 1689044
    iput-object p5, p0, LX/Aal;->d:LX/AYD;

    .line 1689045
    return-void
.end method

.method public static a$redex0(LX/Aal;I)V
    .locals 5

    .prologue
    .line 1689046
    iget-object v0, p0, LX/Aal;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/facecast/protocol/FacecastVideoFeedbackLoader$1;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/protocol/FacecastVideoFeedbackLoader$1;-><init>(LX/Aal;)V

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1689047
    return-void
.end method
