.class public LX/CAJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0if;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855147
    iput-object p1, p0, LX/CAJ;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1855148
    iput-object p2, p0, LX/CAJ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1855149
    iput-object p3, p0, LX/CAJ;->c:LX/0if;

    .line 1855150
    return-void
.end method

.method public static a(LX/0QB;)LX/CAJ;
    .locals 6

    .prologue
    .line 1855151
    const-class v1, LX/CAJ;

    monitor-enter v1

    .line 1855152
    :try_start_0
    sget-object v0, LX/CAJ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855153
    sput-object v2, LX/CAJ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855154
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855155
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855156
    new-instance p0, LX/CAJ;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-direct {p0, v3, v4, v5}, LX/CAJ;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/0if;)V

    .line 1855157
    move-object v0, p0

    .line 1855158
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855159
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855160
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855161
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V
    .locals 5

    .prologue
    .line 1855162
    iget-object v0, p0, LX/CAJ;->c:LX/0if;

    sget-object v1, LX/0ig;->aG:LX/0ih;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    const-string v4, "open_seen_state"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1855163
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1855164
    iget-object v1, p0, LX/CAJ;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-interface {v1, p2, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Qt;)Landroid/content/Intent;

    move-result-object v1

    .line 1855165
    const-string v2, "fragment_title"

    const v3, 0x7f0810db

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855166
    iget-object v2, p0, LX/CAJ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1855167
    return-void
.end method
