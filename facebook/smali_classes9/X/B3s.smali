.class public LX/B3s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B3r;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B3s;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740972
    return-void
.end method

.method public static a(LX/0QB;)LX/B3s;
    .locals 4

    .prologue
    .line 1740980
    sget-object v0, LX/B3s;->b:LX/B3s;

    if-nez v0, :cond_1

    .line 1740981
    const-class v1, LX/B3s;

    monitor-enter v1

    .line 1740982
    :try_start_0
    sget-object v0, LX/B3s;->b:LX/B3s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1740983
    if-eqz v2, :cond_0

    .line 1740984
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1740985
    new-instance v3, LX/B3s;

    invoke-direct {v3}, LX/B3s;-><init>()V

    .line 1740986
    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1740987
    iput-object p0, v3, LX/B3s;->a:LX/0Or;

    .line 1740988
    move-object v0, v3

    .line 1740989
    sput-object v0, LX/B3s;->b:LX/B3s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740990
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1740991
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1740992
    :cond_1
    sget-object v0, LX/B3s;->b:LX/B3s;

    return-object v0

    .line 1740993
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1740994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1740973
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1740974
    const-string v0, "photo_id"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1740975
    const-string v0, "photo_uri"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1740976
    const-string v0, "overlay_owner"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1740977
    iget-object v0, p0, LX/B3s;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 1740978
    invoke-interface {v0, v1, p5, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1740979
    return-void
.end method
