.class public final LX/Aw0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;",
        ">;",
        "LX/Aw4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V
    .locals 0

    .prologue
    .line 1724885
    iput-object p1, p0, LX/Aw0;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Aw4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1724886
    const-wide/16 v4, 0x0

    .line 1724887
    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1724888
    const/4 v3, 0x0

    .line 1724889
    const/4 v2, 0x0

    .line 1724890
    const/4 v1, 0x0

    .line 1724891
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move-wide v6, v4

    move v4, v0

    :goto_0
    if-ge v4, v10, :cond_7

    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;

    .line 1724892
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->j()I

    move-result v5

    int-to-long v12, v5

    add-long/2addr v6, v12

    .line 1724893
    const-string v5, "VIDEO"

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1724894
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    .line 1724895
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1724896
    :cond_0
    const-string v5, "AUDIO"

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1724897
    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_2
    const-string v5, "We had an extra audio asset."

    invoke-static {v3, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v14, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v14

    .line 1724898
    goto :goto_1

    .line 1724899
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    .line 1724900
    :cond_2
    const-string v5, "IMAGE"

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1724901
    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_3
    const-string v5, "We had an extra image asset."

    invoke-static {v2, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v2, v3

    move-object v14, v0

    move-object v0, v1

    move-object v1, v14

    .line 1724902
    goto :goto_1

    .line 1724903
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 1724904
    :cond_4
    const-string v5, "SOUND_EFFECT"

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1724905
    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    const-string v5, "We had an extra sound effect asset."

    invoke-static {v1, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v1, v2

    move-object v2, v3

    .line 1724906
    goto :goto_1

    .line 1724907
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    .line 1724908
    :cond_6
    iget-object v5, p0, LX/Aw0;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v5, v5, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->o:LX/03V;

    const-string v11, "InspirationNuxStore#fetchNuxAssets"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown asset type: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v11, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    .line 1724909
    :cond_7
    iget-object v0, p0, LX/Aw0;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->l:LX/AwR;

    invoke-static {v0}, LX/Aw5;->a(LX/AwR;)J

    move-result-wide v4

    cmp-long v0, v6, v4

    if-lez v0, :cond_8

    .line 1724910
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1724911
    :goto_5
    return-object v0

    .line 1724912
    :cond_8
    iget-object v0, p0, LX/Aw0;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static {v0, v8}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a$redex0(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Ljava/util/ArrayList;)V

    .line 1724913
    iget-object v0, p0, LX/Aw0;->a:Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    invoke-static {v0, v8, v3, v2, v1}, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->a$redex0(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;Ljava/util/List;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_5
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1724914
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Aw0;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
