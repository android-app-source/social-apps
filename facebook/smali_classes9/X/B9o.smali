.class public LX/B9o;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/B9l;

.field public final c:Z

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;LX/B9l;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1752311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752312
    iput-object p1, p0, LX/B9o;->a:Ljava/lang/Throwable;

    .line 1752313
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9l;

    iput-object v0, p0, LX/B9o;->b:LX/B9l;

    .line 1752314
    iput-object p3, p0, LX/B9o;->d:Ljava/lang/String;

    .line 1752315
    iput-boolean p4, p0, LX/B9o;->c:Z

    .line 1752316
    return-void
.end method

.method public static a(LX/B9l;)LX/B9o;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1752310
    new-instance v0, LX/B9o;

    const/4 v1, 0x1

    invoke-direct {v0, v2, p0, v2, v1}, LX/B9o;-><init>(Ljava/lang/Throwable;LX/B9l;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;LX/B9l;)LX/B9o;
    .locals 3

    .prologue
    .line 1752298
    new-instance v0, LX/B9o;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LX/B9o;-><init>(Ljava/lang/Throwable;LX/B9l;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1752299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1752300
    const-string v0, "exception="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752301
    iget-object v0, p0, LX/B9o;->a:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B9o;->a:Ljava/lang/Throwable;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752302
    const-string v0, ",errorDescription="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752303
    iget-object v0, p0, LX/B9o;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752304
    const-string v0, ",channel="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752305
    iget-object v0, p0, LX/B9o;->b:LX/B9l;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1752306
    const-string v0, ",success="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1752307
    iget-boolean v0, p0, LX/B9o;->c:Z

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1752308
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1752309
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method
