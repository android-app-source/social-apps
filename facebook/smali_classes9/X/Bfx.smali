.class public final LX/Bfx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;

.field public final synthetic d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;)V
    .locals 0

    .prologue
    .line 1806817
    iput-object p1, p0, LX/Bfx;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iput-object p2, p0, LX/Bfx;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Bfx;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Bfx;->c:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3d9c54e9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806808
    iget-object v1, p0, LX/Bfx;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    iget-object v2, p0, LX/Bfx;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1806809
    const v1, 0x3e44e21e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1806810
    :goto_0
    return-void

    .line 1806811
    :cond_0
    instance-of v1, p1, Landroid/widget/Checkable;

    if-eqz v1, :cond_1

    .line 1806812
    check-cast p1, Landroid/widget/Checkable;

    invoke-interface {p1}, Landroid/widget/Checkable;->toggle()V

    .line 1806813
    :cond_1
    iget-object v1, p0, LX/Bfx;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v2, p0, LX/Bfx;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Bfx;->c:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1806814
    invoke-static {v1, v2, v3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a$redex0(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;)V

    .line 1806815
    iget-object v1, p0, LX/Bfx;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    iget-object v2, p0, LX/Bfx;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1806816
    const v1, 0x19583f43

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
