.class public final LX/CTl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:LX/CXU;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(IZLX/0Px;Ljava/lang/String;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1895956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895957
    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895958
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1895959
    if-lez v0, :cond_1

    const/16 v5, 0x64

    if-gt v0, v5, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895960
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1895961
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1895962
    goto :goto_2

    .line 1895963
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895964
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1895965
    iput p1, p0, LX/CTl;->a:I

    .line 1895966
    iput-boolean p2, p0, LX/CTl;->b:Z

    .line 1895967
    if-eqz p5, :cond_4

    sget-object v0, LX/CXU;->CASH:LX/CXU;

    :goto_4
    iput-object v0, p0, LX/CTl;->c:LX/CXU;

    .line 1895968
    iput-object p3, p0, LX/CTl;->d:LX/0Px;

    .line 1895969
    iput-object p4, p0, LX/CTl;->e:Ljava/lang/String;

    .line 1895970
    return-void

    :cond_3
    move v1, v2

    .line 1895971
    goto :goto_3

    .line 1895972
    :cond_4
    sget-object v0, LX/CXU;->DEFAULT_PERCENTAGE:LX/CXU;

    goto :goto_4
.end method
