.class public LX/AfG;
.super LX/Aec;
.source ""


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field private final d:Ljava/util/concurrent/Executor;

.field private final e:LX/AaZ;

.field public final f:LX/03V;

.field private final g:LX/0tX;

.field public h:LX/Aee;

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODBackfillCommentsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1698872
    const-class v0, LX/AfG;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AfG;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/AaZ;LX/03V;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698873
    invoke-direct {p0}, LX/Aec;-><init>()V

    .line 1698874
    iput-object p1, p0, LX/AfG;->d:Ljava/util/concurrent/Executor;

    .line 1698875
    iput-object p2, p0, LX/AfG;->g:LX/0tX;

    .line 1698876
    iput-object p3, p0, LX/AfG;->e:LX/AaZ;

    .line 1698877
    iput-object p4, p0, LX/AfG;->f:LX/03V;

    .line 1698878
    return-void
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;)Ljava/util/LinkedList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;",
            ")",
            "Ljava/util/LinkedList",
            "<",
            "LX/Aea;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1698879
    if-nez p0, :cond_0

    .line 1698880
    const/4 v0, 0x0

    .line 1698881
    :goto_0
    return-object v0

    .line 1698882
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1698883
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel;

    .line 1698884
    invoke-static {v0}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel;)LX/Aeu;

    move-result-object v0

    .line 1698885
    if-eqz v0, :cond_1

    .line 1698886
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1698887
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1698888
    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 1698889
    new-instance v0, LX/6SC;

    invoke-direct {v0}, LX/6SC;-><init>()V

    move-object v0, v0

    .line 1698890
    const-string v1, "after_timestamp"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698891
    const-string v1, "before_timestamp"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698892
    const-string v1, "within_timestamp"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698893
    const-string v1, "count"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698894
    const-string v1, "targetID"

    iget-object v2, p0, LX/Aec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1698895
    const-string v1, "translation_enabled"

    iget-object v2, p0, LX/AfG;->e:LX/AaZ;

    invoke-virtual {v2}, LX/AaZ;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1698896
    iget-object v1, p0, LX/AfG;->g:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698897
    iget-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/AfE;

    invoke-direct {v1, p0, p1}, LX/AfE;-><init>(LX/AfG;I)V

    iget-object v2, p0, LX/AfG;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1698898
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1698899
    new-instance v0, LX/6SD;

    invoke-direct {v0}, LX/6SD;-><init>()V

    move-object v0, v0

    .line 1698900
    const-string v1, "after_timestamp"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698901
    const-string v1, "within_timestamp"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698902
    const-string v1, "before_timestamp"

    add-int v2, p1, p2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698903
    const-string v1, "count"

    div-int/lit8 v2, p2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698904
    const-string v1, "targetID"

    iget-object v2, p0, LX/Aec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1698905
    const-string v1, "translation_enabled"

    iget-object v2, p0, LX/AfG;->e:LX/AaZ;

    invoke-virtual {v2}, LX/AaZ;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1698906
    iget-object v1, p0, LX/AfG;->g:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698907
    iget-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/AfF;

    invoke-direct {v1, p0}, LX/AfF;-><init>(LX/AfG;)V

    iget-object v2, p0, LX/AfG;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1698908
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1698909
    iget-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1698910
    iget-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1698911
    iget-object v0, p0, LX/AfG;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1698912
    :cond_0
    iget-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 1698913
    iget-object v0, p0, LX/AfG;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1698914
    :cond_1
    return-void
.end method
