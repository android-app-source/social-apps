.class public LX/C28;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1843916
    iput-object p1, p0, LX/C28;->a:LX/0tX;

    .line 1843917
    iput-object p2, p0, LX/C28;->b:Ljava/lang/String;

    .line 1843918
    return-void
.end method

.method public static a(LX/C28;Ljava/lang/String;)LX/4KF;
    .locals 3

    .prologue
    .line 1843919
    new-instance v0, LX/4KF;

    invoke-direct {v0}, LX/4KF;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1843920
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843921
    move-object v0, v0

    .line 1843922
    iget-object v1, p0, LX/C28;->b:Ljava/lang/String;

    .line 1843923
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843924
    move-object v0, v0

    .line 1843925
    const-string v1, "video_broadcast_schedule_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843926
    move-object v0, v0

    .line 1843927
    return-object v0
.end method

.method public static b(ZLjava/lang/String;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;
    .locals 7

    .prologue
    .line 1843928
    new-instance v0, LX/C23;

    invoke-direct {v0}, LX/C23;-><init>()V

    .line 1843929
    iput-object p1, v0, LX/C23;->a:Ljava/lang/String;

    .line 1843930
    move-object v0, v0

    .line 1843931
    iput-boolean p0, v0, LX/C23;->b:Z

    .line 1843932
    move-object v0, v0

    .line 1843933
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 1843934
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1843935
    iget-object v2, v0, LX/C23;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1843936
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, LX/186;->c(I)V

    .line 1843937
    invoke-virtual {v1, v6, v2}, LX/186;->b(II)V

    .line 1843938
    iget-boolean v2, v0, LX/C23;->b:Z

    invoke-virtual {v1, v5, v2}, LX/186;->a(IZ)V

    .line 1843939
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1843940
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1843941
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1843942
    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1843943
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1843944
    new-instance v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    invoke-direct {v2, v1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;-><init>(LX/15i;)V

    .line 1843945
    move-object v0, v2

    .line 1843946
    return-object v0
.end method
