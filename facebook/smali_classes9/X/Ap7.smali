.class public final LX/Ap7;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ap8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/ApI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/ApL;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1715387
    invoke-static {}, LX/Ap8;->q()LX/Ap8;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1715388
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715389
    const-string v0, "FigAttachmentFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1715390
    if-ne p0, p1, :cond_1

    .line 1715391
    :cond_0
    :goto_0
    return v0

    .line 1715392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1715393
    goto :goto_0

    .line 1715394
    :cond_3
    check-cast p1, LX/Ap7;

    .line 1715395
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1715396
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1715397
    if-eq v2, v3, :cond_0

    .line 1715398
    iget-object v2, p0, LX/Ap7;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ap7;->a:LX/1X1;

    iget-object v3, p1, LX/Ap7;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1715399
    goto :goto_0

    .line 1715400
    :cond_5
    iget-object v2, p1, LX/Ap7;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1715401
    :cond_6
    iget-object v2, p0, LX/Ap7;->b:LX/1X1;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Ap7;->b:LX/1X1;

    iget-object v3, p1, LX/Ap7;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1715402
    goto :goto_0

    .line 1715403
    :cond_8
    iget-object v2, p1, LX/Ap7;->b:LX/1X1;

    if-nez v2, :cond_7

    .line 1715404
    :cond_9
    iget-object v2, p0, LX/Ap7;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Ap7;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Ap7;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1715405
    goto :goto_0

    .line 1715406
    :cond_b
    iget-object v2, p1, LX/Ap7;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 1715407
    :cond_c
    iget-object v2, p0, LX/Ap7;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Ap7;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Ap7;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1715408
    goto :goto_0

    .line 1715409
    :cond_e
    iget-object v2, p1, LX/Ap7;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    .line 1715410
    :cond_f
    iget-object v2, p0, LX/Ap7;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/Ap7;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Ap7;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1715411
    goto :goto_0

    .line 1715412
    :cond_11
    iget-object v2, p1, LX/Ap7;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_10

    .line 1715413
    :cond_12
    iget-object v2, p0, LX/Ap7;->f:LX/1dQ;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/Ap7;->f:LX/1dQ;

    iget-object v3, p1, LX/Ap7;->f:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1715414
    goto/16 :goto_0

    .line 1715415
    :cond_13
    iget-object v2, p1, LX/Ap7;->f:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 1715416
    const/4 v2, 0x0

    .line 1715417
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Ap7;

    .line 1715418
    iget-object v1, v0, LX/Ap7;->a:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/Ap7;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Ap7;->a:LX/1X1;

    .line 1715419
    iget-object v1, v0, LX/Ap7;->b:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Ap7;->b:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, LX/Ap7;->b:LX/1X1;

    .line 1715420
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1715421
    goto :goto_0
.end method
