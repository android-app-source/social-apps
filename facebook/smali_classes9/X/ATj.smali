.class public LX/ATj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0iv;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j6;",
        "DerivedData::",
        "LX/5R0;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/ASn;"
    }
.end annotation


# instance fields
.field private final a:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:LX/Hqr;

.field private final d:Z

.field private final e:Lcom/facebook/widget/ScrollingAwareScrollView;

.field private final f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/composer/ui/underwood/VerticalAttachmentView",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/composer/attachments/ComposerAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;ZZLX/ATL;LX/9bz;LX/0il;LX/Hqr;ZZLcom/facebook/widget/ScrollingAwareScrollView;Landroid/content/Context;LX/ASg;LX/1Ck;)V
    .locals 13
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/ATL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/9bz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/Hqr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/widget/ScrollingAwareScrollView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "LX/9bz;",
            "TServices;",
            "Lcom/facebook/composer/ui/underwood/TaggingController$TagsChangedListener;",
            "ZZ",
            "Lcom/facebook/widget/ScrollingAwareScrollView;",
            "Landroid/content/Context;",
            "LX/ASg;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1676838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1676839
    move-object/from16 v0, p9

    iput-object v0, p0, LX/ATj;->a:LX/0il;

    .line 1676840
    move-object/from16 v0, p10

    iput-object v0, p0, LX/ATj;->c:LX/Hqr;

    .line 1676841
    move/from16 v0, p11

    iput-boolean v0, p0, LX/ATj;->b:Z

    .line 1676842
    move/from16 v0, p12

    iput-boolean v0, p0, LX/ATj;->d:Z

    .line 1676843
    move-object/from16 v0, p13

    iput-object v0, p0, LX/ATj;->e:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676844
    new-instance v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    move-object/from16 v2, p14

    move-object v3, p1

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p15

    move-object/from16 v7, p16

    move-object/from16 v10, p4

    move/from16 v11, p5

    move/from16 v12, p6

    invoke-direct/range {v1 .. v12}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;-><init>(Landroid/content/Context;LX/0gc;LX/ATL;LX/9bz;LX/ASg;LX/1Ck;IILjava/lang/String;ZZ)V

    iput-object v1, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676845
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1676836
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    move-object v2, v1

    move-object v3, v1

    move v5, v4

    move-object v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a(Lcom/facebook/composer/attachments/ComposerAttachment;LX/Hqr;LX/0il;ZZLcom/facebook/widget/ScrollingAwareScrollView;)V

    .line 1676837
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1676834
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setScale(F)V

    .line 1676835
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1676827
    sget-object v0, LX/ATi;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1676828
    :goto_0
    return-void

    .line 1676829
    :pswitch_0
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a()V

    goto :goto_0

    .line 1676830
    :pswitch_1
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->I:Z

    .line 1676831
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a()V

    goto :goto_0

    .line 1676832
    :pswitch_2
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    goto :goto_0

    .line 1676833
    :pswitch_3
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 7
    .param p1    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676824
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, LX/ATj;->g:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676825
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATj;->g:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATj;->c:LX/Hqr;

    iget-object v3, p0, LX/ATj;->a:LX/0il;

    iget-boolean v4, p0, LX/ATj;->b:Z

    iget-boolean v5, p0, LX/ATj;->d:Z

    iget-object v6, p0, LX/ATj;->e:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a(Lcom/facebook/composer/attachments/ComposerAttachment;LX/Hqr;LX/0il;ZZLcom/facebook/widget/ScrollingAwareScrollView;)V

    .line 1676826
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 1

    .prologue
    .line 1676822
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0, p2}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setFaceBoxesAndTags(Z)V

    .line 1676823
    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1676821
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    return-object v0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 1

    .prologue
    .line 1676785
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1676846
    iget-object v0, p0, LX/ATj;->g:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 2

    .prologue
    .line 1676815
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676816
    iput-object p1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676817
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v1, :cond_0

    .line 1676818
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676819
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AT6;

    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1, p0}, LX/AT6;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1676820
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1676813
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->h()V

    .line 1676814
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1676806
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->f()V

    .line 1676807
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676808
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 1676809
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1676810
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676811
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G:LX/BTf;

    invoke-virtual {v1}, LX/BTf;->a()V

    .line 1676812
    :cond_0
    return-void
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1676803
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676804
    iget p0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    move v0, p0

    .line 1676805
    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1676800
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676801
    iget p0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->at:F

    move v0, p0

    .line 1676802
    return v0
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 1676787
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const/4 v2, 0x1

    .line 1676788
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1676789
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aI:LX/2rw;

    if-eqz v1, :cond_1

    iget-object v3, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aI:LX/2rw;

    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    if-ne v3, v1, :cond_1

    move v1, v2

    .line 1676790
    :goto_0
    move v0, v1

    .line 1676791
    if-nez v0, :cond_0

    .line 1676792
    iget-object v0, p0, LX/ATj;->f:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676793
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aI:LX/2rw;

    .line 1676794
    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676795
    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)LX/0am;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676796
    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->w(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    .line 1676797
    :cond_0
    return-void

    .line 1676798
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1676799
    goto :goto_0
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1676786
    return-void
.end method
