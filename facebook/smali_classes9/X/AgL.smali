.class public abstract LX/AgL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;


# instance fields
.field private final b:Landroid/graphics/Rect;

.field private c:I

.field private d:I

.field private e:I

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700641
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/AgL;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1700637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1700638
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/AgL;->b:Landroid/graphics/Rect;

    .line 1700639
    const/16 v0, 0xff

    iput v0, p0, LX/AgL;->f:I

    .line 1700640
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1700664
    invoke-virtual {p0}, LX/AgL;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, LX/AgL;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    iget v2, p0, LX/AgL;->e:I

    add-int/2addr v1, v2

    iget-object v2, p0, LX/AgL;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    iget v3, p0, LX/AgL;->d:I

    add-int/2addr v2, v3

    iget v3, p0, LX/AgL;->c:I

    invoke-static {v0, v1, v2, v3}, LX/AgX;->a(Landroid/graphics/drawable/Drawable;III)V

    .line 1700665
    invoke-virtual {p0}, LX/AgL;->c()V

    .line 1700666
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/graphics/drawable/Drawable;
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1700668
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1700667
    return-void
.end method

.method public getAlpha()I
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700658
    iget v0, p0, LX/AgL;->f:I

    return v0
.end method

.method public final ik_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1700659
    invoke-virtual {p0}, LX/AgL;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, LX/AgL;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1700660
    iget-object v0, p0, LX/AgL;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, LX/AgL;->c:I

    .line 1700661
    iput v2, p0, LX/AgL;->e:I

    .line 1700662
    iput v2, p0, LX/AgL;->d:I

    .line 1700663
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700654
    iput p1, p0, LX/AgL;->f:I

    .line 1700655
    invoke-virtual {p0}, LX/AgL;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1700656
    invoke-virtual {p0}, LX/AgL;->c()V

    .line 1700657
    return-void
.end method

.method public setScale(F)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700652
    iget-object v0, p0, LX/AgL;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, LX/AgL;->setSize(I)V

    .line 1700653
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700648
    iput p1, p0, LX/AgL;->c:I

    .line 1700649
    invoke-direct {p0}, LX/AgL;->d()V

    .line 1700650
    invoke-virtual {p0, p1}, LX/AgL;->a(I)V

    .line 1700651
    return-void
.end method

.method public setTranslationX(I)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700645
    iput p1, p0, LX/AgL;->e:I

    .line 1700646
    invoke-direct {p0}, LX/AgL;->d()V

    .line 1700647
    return-void
.end method

.method public setTranslationY(I)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1700642
    iput p1, p0, LX/AgL;->d:I

    .line 1700643
    invoke-direct {p0}, LX/AgL;->d()V

    .line 1700644
    return-void
.end method
