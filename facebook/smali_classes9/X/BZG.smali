.class public LX/BZG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1797045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPreview()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797054
    iget-object v0, p0, LX/BZG;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797052
    iget-object v0, p0, LX/BZG;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 1797053
    iget v0, p0, LX/BZG;->c:I

    return v0
.end method

.method public setPreview(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797050
    iput-object p1, p0, LX/BZG;->b:Ljava/lang/String;

    .line 1797051
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797048
    iput-object p1, p0, LX/BZG;->a:Ljava/lang/String;

    .line 1797049
    return-void
.end method

.method public setVersion(I)V
    .locals 0

    .prologue
    .line 1797046
    iput p1, p0, LX/BZG;->c:I

    .line 1797047
    return-void
.end method
