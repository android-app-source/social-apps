.class public final enum LX/BKg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BKg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BKg;

.field public static final enum STACKED:LX/BKg;

.field public static final enum TARGET_PRIVACY:LX/BKg;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1774100
    new-instance v0, LX/BKg;

    const-string v1, "STACKED"

    invoke-direct {v0, v1, v2}, LX/BKg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKg;->STACKED:LX/BKg;

    .line 1774101
    new-instance v0, LX/BKg;

    const-string v1, "TARGET_PRIVACY"

    invoke-direct {v0, v1, v3}, LX/BKg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    .line 1774102
    const/4 v0, 0x2

    new-array v0, v0, [LX/BKg;

    sget-object v1, LX/BKg;->STACKED:LX/BKg;

    aput-object v1, v0, v2

    sget-object v1, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    aput-object v1, v0, v3

    sput-object v0, LX/BKg;->$VALUES:[LX/BKg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1774103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BKg;
    .locals 1

    .prologue
    .line 1774104
    const-class v0, LX/BKg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BKg;

    return-object v0
.end method

.method public static values()[LX/BKg;
    .locals 1

    .prologue
    .line 1774105
    sget-object v0, LX/BKg;->$VALUES:[LX/BKg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BKg;

    return-object v0
.end method
