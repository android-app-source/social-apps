.class public LX/C0U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/Ap5;

.field public final b:LX/1qb;

.field public final c:LX/C0D;

.field public final d:LX/ApL;

.field public final e:LX/2sO;


# direct methods
.method public constructor <init>(LX/Ap5;LX/1qb;LX/C0D;LX/ApL;LX/2sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840755
    iput-object p1, p0, LX/C0U;->a:LX/Ap5;

    .line 1840756
    iput-object p2, p0, LX/C0U;->b:LX/1qb;

    .line 1840757
    iput-object p3, p0, LX/C0U;->c:LX/C0D;

    .line 1840758
    iput-object p4, p0, LX/C0U;->d:LX/ApL;

    .line 1840759
    iput-object p5, p0, LX/C0U;->e:LX/2sO;

    .line 1840760
    return-void
.end method

.method public static a(LX/0QB;)LX/C0U;
    .locals 9

    .prologue
    .line 1840761
    const-class v1, LX/C0U;

    monitor-enter v1

    .line 1840762
    :try_start_0
    sget-object v0, LX/C0U;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840763
    sput-object v2, LX/C0U;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840766
    new-instance v3, LX/C0U;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v4

    check-cast v4, LX/Ap5;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v5

    check-cast v5, LX/1qb;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v6

    check-cast v6, LX/C0D;

    invoke-static {v0}, LX/ApL;->a(LX/0QB;)LX/ApL;

    move-result-object v7

    check-cast v7, LX/ApL;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v8

    check-cast v8, LX/2sO;

    invoke-direct/range {v3 .. v8}, LX/C0U;-><init>(LX/Ap5;LX/1qb;LX/C0D;LX/ApL;LX/2sO;)V

    .line 1840767
    move-object v0, v3

    .line 1840768
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840769
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840770
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
