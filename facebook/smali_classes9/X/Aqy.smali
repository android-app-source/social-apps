.class public final LX/Aqy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/photos/base/media/PhotoItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/4BY;)V
    .locals 0

    .prologue
    .line 1718594
    iput-object p1, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iput-object p2, p0, LX/Aqy;->a:LX/4BY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1718595
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T:LX/ArS;

    const/4 v1, 0x3

    iget-object v2, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v2}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ArS;->a(SLX/0Px;)V

    .line 1718596
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->M:LX/03V;

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->F:Ljava/lang/String;

    const-string v2, "Error post-processing image"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718597
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->H:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082787

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1718598
    iget-object v0, p0, LX/Aqy;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1718599
    iget-object v0, p0, LX/Aqy;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1718600
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1718601
    check-cast p1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1718602
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->T:LX/ArS;

    const/4 v1, 0x2

    iget-object v2, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v2}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ArS;->a(SLX/0Px;)V

    .line 1718603
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1718604
    :cond_0
    :goto_0
    return-void

    .line 1718605
    :cond_1
    iget-object v0, p0, LX/Aqy;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0, p1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1718606
    iget-object v0, p0, LX/Aqy;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718607
    iget-object v0, p0, LX/Aqy;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    goto :goto_0
.end method
