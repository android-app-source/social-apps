.class public final LX/Bvj;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bvl;


# direct methods
.method public constructor <init>(LX/Bvl;)V
    .locals 0

    .prologue
    .line 1832705
    iput-object p1, p0, LX/Bvj;->a:LX/Bvl;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1832706
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1832707
    check-cast p1, LX/2ou;

    .line 1832708
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->f:Ljava/lang/String;

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1832709
    :cond_0
    :goto_0
    return-void

    .line 1832710
    :cond_1
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->e:LX/Bvk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Bvk;->removeMessages(I)V

    .line 1832711
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 1832712
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-boolean v0, v0, LX/Bvl;->n:Z

    if-eqz v0, :cond_0

    .line 1832713
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->g()V

    goto :goto_0

    .line 1832714
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1832715
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1832716
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    iget-object v0, v0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b()V

    .line 1832717
    :cond_3
    iget-object v0, p0, LX/Bvj;->a:LX/Bvl;

    invoke-static {v0}, LX/Bvl;->g$redex0(LX/Bvl;)V

    goto :goto_0
.end method
