.class public final enum LX/BSW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSW;

.field public static final enum DASH:LX/BSW;

.field public static final enum DASH_LIVE:LX/BSW;

.field public static final enum HLS:LX/BSW;

.field public static final enum PROGRESSIVE_DOWNLOAD:LX/BSW;

.field public static final enum UNKNOWN:LX/BSW;


# instance fields
.field private final mStreamingFormat:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1785529
    new-instance v0, LX/BSW;

    const-string v1, "DASH"

    const-string v2, "dash"

    invoke-direct {v0, v1, v3, v2}, LX/BSW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSW;->DASH:LX/BSW;

    .line 1785530
    new-instance v0, LX/BSW;

    const-string v1, "DASH_LIVE"

    const-string v2, "dash_live"

    invoke-direct {v0, v1, v4, v2}, LX/BSW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSW;->DASH_LIVE:LX/BSW;

    .line 1785531
    new-instance v0, LX/BSW;

    const-string v1, "HLS"

    const-string v2, "hls"

    invoke-direct {v0, v1, v5, v2}, LX/BSW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSW;->HLS:LX/BSW;

    .line 1785532
    new-instance v0, LX/BSW;

    const-string v1, "PROGRESSIVE_DOWNLOAD"

    const-string v2, "progressive_download"

    invoke-direct {v0, v1, v6, v2}, LX/BSW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSW;->PROGRESSIVE_DOWNLOAD:LX/BSW;

    .line 1785533
    new-instance v0, LX/BSW;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v7, v2}, LX/BSW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSW;->UNKNOWN:LX/BSW;

    .line 1785534
    const/4 v0, 0x5

    new-array v0, v0, [LX/BSW;

    sget-object v1, LX/BSW;->DASH:LX/BSW;

    aput-object v1, v0, v3

    sget-object v1, LX/BSW;->DASH_LIVE:LX/BSW;

    aput-object v1, v0, v4

    sget-object v1, LX/BSW;->HLS:LX/BSW;

    aput-object v1, v0, v5

    sget-object v1, LX/BSW;->PROGRESSIVE_DOWNLOAD:LX/BSW;

    aput-object v1, v0, v6

    sget-object v1, LX/BSW;->UNKNOWN:LX/BSW;

    aput-object v1, v0, v7

    sput-object v0, LX/BSW;->$VALUES:[LX/BSW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1785535
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1785536
    iput-object p3, p0, LX/BSW;->mStreamingFormat:Ljava/lang/String;

    .line 1785537
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSW;
    .locals 1

    .prologue
    .line 1785538
    const-class v0, LX/BSW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSW;

    return-object v0
.end method

.method public static values()[LX/BSW;
    .locals 1

    .prologue
    .line 1785539
    sget-object v0, LX/BSW;->$VALUES:[LX/BSW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785540
    iget-object v0, p0, LX/BSW;->mStreamingFormat:Ljava/lang/String;

    return-object v0
.end method
