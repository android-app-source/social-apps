.class public final LX/BMA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;

.field public b:LX/1RN;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;)V
    .locals 1

    .prologue
    .line 1776900
    iput-object p1, p0, LX/BMA;->a:Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776901
    const/4 v0, 0x0

    iput-object v0, p0, LX/BMA;->b:LX/1RN;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6d45ccc3

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1776902
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776903
    iget-object v0, p0, LX/BMA;->b:LX/1RN;

    if-nez v0, :cond_0

    .line 1776904
    const v0, -0x53ea0216

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1776905
    :goto_0
    return-void

    .line 1776906
    :cond_0
    iget-object v0, p0, LX/BMA;->a:Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;

    iget-object v0, v0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BMP;

    iget-object v2, p0, LX/BMA;->b:LX/1RN;

    iget-object v3, p0, LX/BMA;->a:Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;

    iget-object v3, v3, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->d:LX/B5l;

    iget-object v4, p0, LX/BMA;->b:LX/1RN;

    invoke-virtual {v3, v4}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v3

    invoke-virtual {v3}, LX/B5n;->a()LX/B5p;

    move-result-object v3

    invoke-virtual {v0, p1, v2, v3}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1776907
    const v0, -0x6261d06e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
