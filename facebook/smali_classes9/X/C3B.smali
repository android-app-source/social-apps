.class public LX/C3B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nB;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1845172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845173
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1845174
    iput-object v0, p0, LX/C3B;->a:LX/0Ot;

    .line 1845175
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1845176
    iput-object v0, p0, LX/C3B;->b:LX/0Ot;

    .line 1845177
    return-void
.end method

.method public static b(LX/0QB;)LX/C3B;
    .locals 3

    .prologue
    .line 1845178
    new-instance v0, LX/C3B;

    invoke-direct {v0}, LX/C3B;-><init>()V

    .line 1845179
    const/16 v1, 0xbc6

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x455

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1845180
    iput-object v1, v0, LX/C3B;->a:LX/0Ot;

    iput-object v2, v0, LX/C3B;->b:LX/0Ot;

    .line 1845181
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;LX/C33;)V
    .locals 3

    .prologue
    .line 1845182
    iget-object v0, p2, LX/C33;->a:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 1845183
    iget-object v0, p0, LX/C3B;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/C3B;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nB;

    invoke-virtual {p2}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1845184
    iget-object p0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, p0

    .line 1845185
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2}, LX/1nB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1845186
    :goto_0
    return-void

    .line 1845187
    :cond_0
    iget-object v0, p2, LX/C33;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
