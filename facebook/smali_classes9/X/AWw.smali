.class public final LX/AWw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qm;


# instance fields
.field public final synthetic a:LX/AWy;


# direct methods
.method public constructor <init>(LX/AWy;)V
    .locals 0

    .prologue
    .line 1683496
    iput-object p1, p0, LX/AWw;->a:LX/AWy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2

    .prologue
    .line 1683480
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-nez v0, :cond_0

    .line 1683481
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    new-instance v1, LX/AWS;

    invoke-direct {v1}, LX/AWS;-><init>()V

    invoke-virtual {v1}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v1

    .line 1683482
    iput-object v1, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683483
    :cond_0
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-object v1, p0, LX/AWw;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v1}, Lcom/facebook/facecast/model/FacecastPrivacyData;->d()LX/AWS;

    move-result-object v1

    .line 1683484
    iput-object p1, v1, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1683485
    move-object v1, v1

    .line 1683486
    invoke-virtual {v1}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v1

    .line 1683487
    iput-object v1, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683488
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-object v1, p0, LX/AWw;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v0, v1}, LX/AWy;->a(Lcom/facebook/facecast/model/FacecastPrivacyData;)V

    .line 1683489
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    if-eqz v0, :cond_1

    .line 1683490
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->A:LX/AWx;

    iget-object v1, p0, LX/AWw;->a:LX/AWy;

    iget-object v1, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683491
    iget-object p1, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, p1

    .line 1683492
    invoke-interface {v0, v1}, LX/AWx;->b(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1683493
    :cond_1
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    iget-boolean v0, v0, LX/AWy;->F:Z

    if-nez v0, :cond_2

    .line 1683494
    iget-object v0, p0, LX/AWw;->a:LX/AWy;

    invoke-virtual {v0}, LX/AWy;->g()V

    .line 1683495
    :cond_2
    return-void
.end method
