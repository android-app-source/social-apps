.class public final enum LX/CY4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CY4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_CHANGE:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_CHECK_DEEPLINK:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_CREATE:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_DELETE:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_DELETE_CANCEL:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_DELETE_CONFIRM:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_EDIT:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_EDIT_CANCEL:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_ERROR:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_NEXT_BUTTON:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_SAVE:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_SWITCH_FROM_LINKOUT:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TO_LINKOUT:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TYPES:LX/CY4;

.field public static final enum EVENT_ADMIN_CALL_TO_ACTION_UNCHECK_DEEPLINK:LX/CY4;

.field public static final enum EVENT_CALL_TO_ACTION_INVALID_EMAIL:LX/CY4;

.field public static final enum EVENT_CALL_TO_ACTION_INVALID_URL:LX/CY4;

.field public static final enum EVENT_CALL_TO_ACTION_SWITCH_SELECT_OPTIONS:LX/CY4;

.field public static final enum EVENT_CALL_TO_ACTION_TAP_ENTITY_LINK:LX/CY4;

.field public static final enum EVENT_VIEWER_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

.field public static final enum EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_NO:LX/CY4;

.field public static final enum EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_YES:LX/CY4;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1910747
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_CREATE"

    const-string v2, "custom_cta_view_create_mobile"

    invoke-direct {v0, v1, v4, v2}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CREATE:LX/CY4;

    .line 1910748
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_CHANGE"

    const-string v2, "custom_cta_change_cta_type_mobile"

    invoke-direct {v0, v1, v5, v2}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CHANGE:LX/CY4;

    .line 1910749
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_ERROR"

    const-string v2, "custom_cta_mobile_admin_flow_error"

    invoke-direct {v0, v1, v6, v2}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_ERROR:LX/CY4;

    .line 1910750
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_SAVE"

    const-string v2, "custom_cta_click_save_mobile"

    invoke-direct {v0, v1, v7, v2}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SAVE:LX/CY4;

    .line 1910751
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_EDIT"

    const-string v2, "custom_cta_click_edit_mobile"

    invoke-direct {v0, v1, v8, v2}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_EDIT:LX/CY4;

    .line 1910752
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_EDIT_CANCEL"

    const/4 v2, 0x5

    const-string v3, "custom_cta_click_edit_cancel_mobile"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_EDIT_CANCEL:LX/CY4;

    .line 1910753
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TYPES"

    const/4 v2, 0x6

    const-string v3, "custom_cta_switch_types_mobile"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TYPES:LX/CY4;

    .line 1910754
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_DELETE"

    const/4 v2, 0x7

    const-string v3, "custom_cta_click_delete_mobile"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE:LX/CY4;

    .line 1910755
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TO_LINKOUT"

    const/16 v2, 0x8

    const-string v3, "custom_cta_switch_to_linkout"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TO_LINKOUT:LX/CY4;

    .line 1910756
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_SWITCH_FROM_LINKOUT"

    const/16 v2, 0x9

    const-string v3, "custom_cta_switch_from_linkout"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_FROM_LINKOUT:LX/CY4;

    .line 1910757
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_CHECK_DEEPLINK"

    const/16 v2, 0xa

    const-string v3, "custom_cta_check_setup_deeplink"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CHECK_DEEPLINK:LX/CY4;

    .line 1910758
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_UNCHECK_DEEPLINK"

    const/16 v2, 0xb

    const-string v3, "custom_cta_uncheck_setup_deeplink"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_UNCHECK_DEEPLINK:LX/CY4;

    .line 1910759
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_BACK_BUTTON"

    const/16 v2, 0xc

    const-string v3, "custom_cta_mobile_click_back_button"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    .line 1910760
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_NEXT_BUTTON"

    const/16 v2, 0xd

    const-string v3, "custom_cta_mobile_click_next_button"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_NEXT_BUTTON:LX/CY4;

    .line 1910761
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_DELETE_CONFIRM"

    const/16 v2, 0xe

    const-string v3, "custom_cta_click_confirm_delete_mobile"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE_CONFIRM:LX/CY4;

    .line 1910762
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_ADMIN_CALL_TO_ACTION_DELETE_CANCEL"

    const/16 v2, 0xf

    const-string v3, "custom_cta_click_cancel_delete_mobile"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE_CANCEL:LX/CY4;

    .line 1910763
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_VIEWER_CALL_TO_ACTION_BACK_BUTTON"

    const/16 v2, 0x10

    const-string v3, "custom_cta_mobile_click_back_button"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    .line 1910764
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_YES"

    const/16 v2, 0x11

    const-string v3, "custom_cta_mobile_back_click_yes"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_YES:LX/CY4;

    .line 1910765
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_NO"

    const/16 v2, 0x12

    const-string v3, "custom_cta_mobile_back_click_no"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_NO:LX/CY4;

    .line 1910766
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_CALL_TO_ACTION_INVALID_URL"

    const/16 v2, 0x13

    const-string v3, "custom_cta_mobile_invalid_url"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_CALL_TO_ACTION_INVALID_URL:LX/CY4;

    .line 1910767
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_CALL_TO_ACTION_INVALID_EMAIL"

    const/16 v2, 0x14

    const-string v3, "custom_cta_mobile_invalid_email"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_CALL_TO_ACTION_INVALID_EMAIL:LX/CY4;

    .line 1910768
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_CALL_TO_ACTION_SWITCH_SELECT_OPTIONS"

    const/16 v2, 0x15

    const-string v3, "custom_cta_mobile_switch_select_options"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_CALL_TO_ACTION_SWITCH_SELECT_OPTIONS:LX/CY4;

    .line 1910769
    new-instance v0, LX/CY4;

    const-string v1, "EVENT_CALL_TO_ACTION_TAP_ENTITY_LINK"

    const/16 v2, 0x16

    const-string v3, "custom_cta_mobile_click_entity_link"

    invoke-direct {v0, v1, v2, v3}, LX/CY4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CY4;->EVENT_CALL_TO_ACTION_TAP_ENTITY_LINK:LX/CY4;

    .line 1910770
    const/16 v0, 0x17

    new-array v0, v0, [LX/CY4;

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CREATE:LX/CY4;

    aput-object v1, v0, v4

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CHANGE:LX/CY4;

    aput-object v1, v0, v5

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_ERROR:LX/CY4;

    aput-object v1, v0, v6

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SAVE:LX/CY4;

    aput-object v1, v0, v7

    sget-object v1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_EDIT:LX/CY4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_EDIT_CANCEL:LX/CY4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TYPES:LX/CY4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_TO_LINKOUT:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SWITCH_FROM_LINKOUT:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CHECK_DEEPLINK:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_UNCHECK_DEEPLINK:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_NEXT_BUTTON:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE_CONFIRM:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_DELETE_CANCEL:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_YES:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_CLICK_NO:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/CY4;->EVENT_CALL_TO_ACTION_INVALID_URL:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/CY4;->EVENT_CALL_TO_ACTION_INVALID_EMAIL:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/CY4;->EVENT_CALL_TO_ACTION_SWITCH_SELECT_OPTIONS:LX/CY4;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/CY4;->EVENT_CALL_TO_ACTION_TAP_ENTITY_LINK:LX/CY4;

    aput-object v2, v0, v1

    sput-object v0, LX/CY4;->$VALUES:[LX/CY4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910744
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1910745
    iput-object p3, p0, LX/CY4;->mEventName:Ljava/lang/String;

    .line 1910746
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CY4;
    .locals 1

    .prologue
    .line 1910743
    const-class v0, LX/CY4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CY4;

    return-object v0
.end method

.method public static values()[LX/CY4;
    .locals 1

    .prologue
    .line 1910741
    sget-object v0, LX/CY4;->$VALUES:[LX/CY4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CY4;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1910742
    iget-object v0, p0, LX/CY4;->mEventName:Ljava/lang/String;

    return-object v0
.end method
