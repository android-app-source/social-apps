.class public final LX/Ai0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1704965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1704966
    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;)LX/Ai0;
    .locals 4

    .prologue
    .line 1704967
    new-instance v0, LX/Ai0;

    invoke-direct {v0}, LX/Ai0;-><init>()V

    .line 1704968
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->a()I

    move-result v1

    iput v1, v0, LX/Ai0;->a:I

    .line 1704969
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/Ai0;->b:LX/0Px;

    .line 1704970
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/Ai0;->c:LX/15i;

    iput v1, v0, LX/Ai0;->d:I

    monitor-exit v3

    .line 1704971
    return-object v0

    .line 1704972
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 1704973
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1704974
    iget-object v1, p0, LX/Ai0;->b:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1704975
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v5, p0, LX/Ai0;->c:LX/15i;

    iget v6, p0, LX/Ai0;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, 0x64223180

    invoke-static {v5, v6, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1704976
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1704977
    iget v5, p0, LX/Ai0;->a:I

    invoke-virtual {v0, v7, v5, v7}, LX/186;->a(III)V

    .line 1704978
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1704979
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1704980
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1704981
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1704982
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1704983
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1704984
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1704985
    new-instance v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-direct {v1, v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;-><init>(LX/15i;)V

    .line 1704986
    return-object v1

    .line 1704987
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
