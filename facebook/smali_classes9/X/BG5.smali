.class public final LX/BG5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766570
    iput-object p1, p0, LX/BG5;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1766563
    iget-object v0, p0, LX/BG5;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->l:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0813ba

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1766564
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766565
    check-cast p1, Ljava/io/File;

    .line 1766566
    iget-object v0, p0, LX/BG5;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1766567
    iput-object p1, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->v:Ljava/io/File;

    .line 1766568
    iget-object v0, p0, LX/BG5;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/BG4;

    invoke-direct {v1, p0}, LX/BG4;-><init>(LX/BG5;)V

    invoke-virtual {v0, v1, p1}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;Ljava/io/File;)V

    .line 1766569
    return-void
.end method
