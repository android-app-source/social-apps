.class public LX/CUQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uG;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;)V
    .locals 7

    .prologue
    .line 1896560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896561
    iget-object v0, p1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/CUQ;->b:Ljava/lang/String;

    .line 1896562
    iget-object v0, p1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->d:Ljava/lang/String;

    iput-object v0, p0, LX/CUQ;->a:Ljava/lang/String;

    .line 1896563
    iget-object v0, p1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    const/4 v3, 0x0

    .line 1896564
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1896565
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1896566
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 1896567
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/high16 v2, -0x1000000

    invoke-direct {v5, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, p1

    const/16 p1, 0x11

    invoke-virtual {v4, v5, v6, v2, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1896568
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    :cond_0
    move v2, v3

    .line 1896569
    goto :goto_0

    .line 1896570
    :cond_1
    move-object v0, v4

    .line 1896571
    iput-object v0, p0, LX/CUQ;->c:Ljava/lang/CharSequence;

    .line 1896572
    return-void
.end method


# virtual methods
.method public final d()LX/6uT;
    .locals 1

    .prologue
    .line 1896559
    sget-object v0, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    return-object v0
.end method
