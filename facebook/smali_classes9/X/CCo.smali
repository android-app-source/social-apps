.class public final LX/CCo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CCp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pm;

.field public c:Z

.field public d:I

.field public e:I

.field public f:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic g:LX/CCp;


# direct methods
.method public constructor <init>(LX/CCp;)V
    .locals 1

    .prologue
    .line 1858349
    iput-object p1, p0, LX/CCo;->g:LX/CCp;

    .line 1858350
    move-object v0, p1

    .line 1858351
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1858352
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1858348
    const-string v0, "TapToAddComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1858325
    if-ne p0, p1, :cond_1

    .line 1858326
    :cond_0
    :goto_0
    return v0

    .line 1858327
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1858328
    goto :goto_0

    .line 1858329
    :cond_3
    check-cast p1, LX/CCo;

    .line 1858330
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1858331
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1858332
    if-eq v2, v3, :cond_0

    .line 1858333
    iget-object v2, p0, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1858334
    goto :goto_0

    .line 1858335
    :cond_5
    iget-object v2, p1, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1858336
    :cond_6
    iget-object v2, p0, LX/CCo;->b:LX/1Pm;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CCo;->b:LX/1Pm;

    iget-object v3, p1, LX/CCo;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1858337
    goto :goto_0

    .line 1858338
    :cond_8
    iget-object v2, p1, LX/CCo;->b:LX/1Pm;

    if-nez v2, :cond_7

    .line 1858339
    :cond_9
    iget-boolean v2, p0, LX/CCo;->c:Z

    iget-boolean v3, p1, LX/CCo;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1858340
    goto :goto_0

    .line 1858341
    :cond_a
    iget v2, p0, LX/CCo;->d:I

    iget v3, p1, LX/CCo;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1858342
    goto :goto_0

    .line 1858343
    :cond_b
    iget v2, p0, LX/CCo;->e:I

    iget v3, p1, LX/CCo;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1858344
    goto :goto_0

    .line 1858345
    :cond_c
    iget-object v2, p0, LX/CCo;->f:LX/1dc;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/CCo;->f:LX/1dc;

    iget-object v3, p1, LX/CCo;->f:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1858346
    goto :goto_0

    .line 1858347
    :cond_d
    iget-object v2, p1, LX/CCo;->f:LX/1dc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
