.class public LX/AyP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasComposerSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/AzD;

.field public final b:LX/1Nq;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axy;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AzD;LX/1Nq;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AzD;",
            "LX/1Nq;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Axy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Axw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729968
    iput-object p1, p0, LX/AyP;->a:LX/AzD;

    .line 1729969
    iput-object p2, p0, LX/AyP;->b:LX/1Nq;

    .line 1729970
    iput-object p3, p0, LX/AyP;->c:LX/0Ot;

    .line 1729971
    iput-object p4, p0, LX/AyP;->d:LX/0Ot;

    .line 1729972
    iput-object p5, p0, LX/AyP;->e:LX/0Ot;

    .line 1729973
    return-void
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 7
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1729974
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v1

    .line 1729975
    instance-of v0, v1, LX/Ayb;

    const-string v2, "Didn\'t get a souvenir prompt object"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1729976
    iget-object v0, p0, LX/AyP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axw;

    .line 1729977
    iget-object v3, v0, LX/Axw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/AyF;->b:LX/0Tn;

    iget-object v5, v0, LX/Axw;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 1729978
    iget-object v2, p0, LX/AyP;->a:LX/AzD;

    move-object v0, v1

    check-cast v0, LX/Ayb;

    .line 1729979
    sget-object v1, LX/AzB;->PROMPT_CLOSED:LX/AzB;

    invoke-virtual {v1}, LX/AzB;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, -0x1

    iget-object v4, v0, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v2, v1, v3, v4}, LX/AzD;->a(LX/AzD;Ljava/lang/String;ILcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v2, v1}, LX/AzD;->a(LX/AzD;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1729980
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1729981
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/Ayb;

    .line 1729982
    invoke-virtual {v6}, LX/Ayb;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    move-object v2, p3

    check-cast v2, LX/B5p;

    .line 1729983
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1729984
    move-object v3, p3

    check-cast v3, LX/B5p;

    .line 1729985
    iget-object v4, v3, LX/B5p;->j:LX/0am;

    move-object v3, v4

    .line 1729986
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    .line 1729987
    iget-object v1, v6, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1729988
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Landroid/app/Activity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 1729989
    iget-object v3, p0, LX/AyP;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Axy;

    invoke-virtual {v3, v1}, LX/Axy;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)LX/0Px;

    move-result-object v3

    .line 1729990
    sget-object v4, LX/21D;->NEWSFEED:LX/21D;

    const-string v5, "souvenirPrompt"

    invoke-static {v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-static {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, p0, LX/AyP;->b:LX/1Nq;

    invoke-static {v0}, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setSouvenirUniqueId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    .line 1729991
    iget-object v3, p0, LX/AyP;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Kf;

    check-cast p3, LX/B5p;

    .line 1729992
    iget-object v4, p3, LX/B5p;->j:LX/0am;

    move-object v4, v4

    .line 1729993
    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 p2, 0x6dc

    invoke-interface {v3, v4, v5, p2, v2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1729994
    iget-object v0, p0, LX/AyP;->a:LX/AzD;

    .line 1729995
    sget-object v1, LX/AzB;->PROMPT_TAPPED:LX/AzB;

    invoke-virtual {v1}, LX/AzB;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v3, v6, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v0, v1, v2, v3}, LX/AzD;->a(LX/AzD;Ljava/lang/String;ILcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/AzD;->a(LX/AzD;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1729996
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1729997
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1729998
    instance-of v0, v0, LX/Ayb;

    return v0
.end method
