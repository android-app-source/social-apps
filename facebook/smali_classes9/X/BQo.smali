.class public LX/BQo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782797
    iput-object p1, p0, LX/BQo;->a:LX/0Zb;

    .line 1782798
    iput-object p2, p0, LX/BQo;->b:Ljava/lang/String;

    .line 1782799
    return-void
.end method

.method public static c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1782809
    iget-object v0, p0, LX/BQo;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1782810
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1782811
    iget-object v1, p0, LX/BQo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1782812
    const-string v1, "heisman_composer_session_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782813
    const-string v1, "picture_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782814
    const-string v1, "profile_pic_frame_id"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782815
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1782816
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1782800
    iget-object v0, p0, LX/BQo;->a:LX/0Zb;

    const-string v1, "staging_ground_photo_changed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1782801
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1782802
    iget-object v1, p0, LX/BQo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1782803
    const-string v1, "heisman_composer_session_id"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782804
    const-string v1, "picture_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782805
    const-string v1, "profile_pic_frame_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782806
    const-string v1, "profile_pic_source"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1782807
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1782808
    :cond_0
    return-void
.end method
