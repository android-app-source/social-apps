.class public final LX/AsL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/AsM;


# direct methods
.method public constructor <init>(LX/AsM;)V
    .locals 0

    .prologue
    .line 1720352
    iput-object p1, p0, LX/AsL;->a:LX/AsM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1720364
    iget-object v0, p0, LX/AsL;->a:LX/AsM;

    iget-object v0, v0, LX/AsM;->i:LX/Ats;

    .line 1720365
    iget-object v1, v0, LX/Ats;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    .line 1720366
    iget-object p0, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->k:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1720367
    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->h(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    .line 1720368
    :goto_0
    return-void

    .line 1720369
    :cond_0
    iget-object p0, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->k:LX/0Sh;

    new-instance v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$6;

    invoke-direct {v0, v1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$6;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    invoke-virtual {p0, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1720361
    iget-object v0, p0, LX/AsL;->a:LX/AsM;

    iget-object v0, v0, LX/AsM;->i:LX/Ats;

    .line 1720362
    iget-object v1, v0, LX/Ats;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->k:LX/0Sh;

    new-instance p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$2$1;

    invoke-direct {p0, v0, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$2$1;-><init>(LX/Ats;Ljava/lang/String;)V

    invoke-virtual {v1, p0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1720363
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1720370
    iget-object v0, p0, LX/AsL;->a:LX/AsM;

    iget-object v0, v0, LX/AsM;->i:LX/Ats;

    .line 1720371
    iget-object p0, v0, LX/Ats;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object p0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->K:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a()V

    .line 1720372
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1720357
    iget-object v0, p0, LX/AsL;->a:LX/AsM;

    iget-object v0, v0, LX/AsM;->i:LX/Ats;

    .line 1720358
    iget-object v1, v0, LX/Ats;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    .line 1720359
    const p0, 0xb60006

    const/4 v0, 0x2

    invoke-static {v1, p0, v0}, LX/ArS;->a(LX/ArS;IS)V

    .line 1720360
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1720353
    iget-object v0, p0, LX/AsL;->a:LX/AsM;

    iget-object v0, v0, LX/AsM;->i:LX/Ats;

    .line 1720354
    iget-object v1, v0, LX/Ats;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    .line 1720355
    const p0, 0xb60006

    const/4 v0, 0x3

    invoke-static {v1, p0, v0}, LX/ArS;->a(LX/ArS;IS)V

    .line 1720356
    return-void
.end method
