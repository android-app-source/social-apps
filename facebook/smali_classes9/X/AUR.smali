.class public final LX/AUR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1678045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678046
    return-void
.end method

.method public static a(LX/AUA;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1678047
    new-instance v0, LX/AUX;

    invoke-direct {v0, p1}, LX/AUX;-><init>(Ljava/lang/String;)V

    .line 1678048
    invoke-virtual {p0, v0}, LX/AUA;->b(LX/AU9;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, LX/AUX;->b(Landroid/database/Cursor;)LX/AUW;

    move-result-object v1

    .line 1678049
    :try_start_0
    invoke-interface {v1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1678050
    iget-object v0, v1, LX/AU1;->a:Landroid/database/Cursor;

    const/4 p0, 0x1

    invoke-interface {v0, p0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678051
    invoke-interface {v1}, LX/AU0;->c()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v1}, LX/AU0;->c()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/AU0;->c()V

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1678040
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1678041
    const-string v1, "table_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678042
    const-string v1, "hash"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678043
    const-string v1, "sqliteproc_metadata"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const v4, -0x1252f6a1

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0x6446c862

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1678044
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[LX/AUP;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1678021
    const-string v1, "sqliteproc_schema"

    const-string v2, "table_name = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v0

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1678022
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1678023
    array-length v2, p2

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 1678024
    const-string v4, "table_name"

    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678025
    const-string v4, "name"

    iget-object v5, v3, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678026
    const-string v4, "type_name"

    iget-object v5, v3, LX/AUP;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678027
    const-string v4, "default_value"

    iget-object v5, v3, LX/AUP;->c:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678028
    const-string v4, "is_nullable"

    iget-boolean v5, v3, LX/AUP;->d:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1678029
    const-string v4, "is_primary"

    iget-boolean v5, v3, LX/AUP;->e:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1678030
    const-string v4, "is_autoincrement"

    iget-boolean v5, v3, LX/AUP;->f:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1678031
    const-string v4, "is_deleted"

    iget-boolean v5, v3, LX/AUP;->g:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1678032
    const-string v4, "does_affect_indices"

    iget-boolean v5, v3, LX/AUP;->h:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1678033
    iget-object v4, v3, LX/AUP;->i:LX/AUj;

    if-eqz v4, :cond_0

    .line 1678034
    const-string v4, "auto_upgrade_policy"

    iget-object v5, v3, LX/AUP;->i:LX/AUj;

    invoke-virtual {v5}, LX/AUj;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678035
    :cond_0
    const-string v4, "foreign_table"

    iget-object v5, v3, LX/AUP;->j:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678036
    const-string v4, "foreign_column"

    iget-object v3, v3, LX/AUP;->k:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678037
    const-string v3, "sqliteproc_schema"

    const/4 v4, 0x0

    const v5, -0x1daabb2c

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v3, 0x7486c277

    invoke-static {v3}, LX/03h;->a(I)V

    .line 1678038
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1678039
    :cond_1
    return-void
.end method

.method public static b(LX/AUA;Ljava/lang/String;)[LX/AUP;
    .locals 16

    .prologue
    .line 1678013
    new-instance v1, LX/AUc;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, LX/AUc;-><init>(Ljava/lang/String;)V

    .line 1678014
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/AUA;->b(LX/AU9;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, LX/AUc;->b(Landroid/database/Cursor;)LX/AUb;

    move-result-object v14

    .line 1678015
    :try_start_0
    invoke-interface {v14}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-array v15, v1, [LX/AUP;

    .line 1678016
    const/4 v1, 0x0

    move v13, v1

    :goto_0
    invoke-interface {v14}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1678017
    new-instance v1, LX/AUP;

    invoke-virtual {v14}, LX/AUb;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14}, LX/AUb;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14}, LX/AUb;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, LX/AUb;->h()Z

    move-result v5

    invoke-virtual {v14}, LX/AUb;->i()Z

    move-result v6

    invoke-virtual {v14}, LX/AUb;->j()Z

    move-result v7

    invoke-virtual {v14}, LX/AUb;->k()Z

    move-result v8

    invoke-virtual {v14}, LX/AUb;->l()Z

    move-result v9

    invoke-virtual {v14}, LX/AUb;->m()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v14}, LX/AUb;->n()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v14}, LX/AUb;->o()Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v1 .. v12}, LX/AUP;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1678018
    aput-object v1, v15, v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678019
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_0

    .line 1678020
    :cond_0
    invoke-interface {v14}, LX/AU0;->c()V

    return-object v15

    :catchall_0
    move-exception v1

    invoke-interface {v14}, LX/AU0;->c()V

    throw v1
.end method
