.class public final LX/COy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/COu;

.field public b:I

.field public c:Z

.field public d:LX/3x6;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1883778
    invoke-static {}, LX/COz;->q()LX/COz;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883779
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883780
    const-string v0, "NTHScrollMountComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1883781
    if-ne p0, p1, :cond_1

    .line 1883782
    :cond_0
    :goto_0
    return v0

    .line 1883783
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883784
    goto :goto_0

    .line 1883785
    :cond_3
    check-cast p1, LX/COy;

    .line 1883786
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883787
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883788
    if-eq v2, v3, :cond_0

    .line 1883789
    iget-object v2, p0, LX/COy;->a:LX/COu;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/COy;->a:LX/COu;

    iget-object v3, p1, LX/COy;->a:LX/COu;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1883790
    goto :goto_0

    .line 1883791
    :cond_5
    iget-object v2, p1, LX/COy;->a:LX/COu;

    if-nez v2, :cond_4

    .line 1883792
    :cond_6
    iget v2, p0, LX/COy;->b:I

    iget v3, p1, LX/COy;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1883793
    goto :goto_0

    .line 1883794
    :cond_7
    iget-boolean v2, p0, LX/COy;->c:Z

    iget-boolean v3, p1, LX/COy;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1883795
    goto :goto_0

    .line 1883796
    :cond_8
    iget-object v2, p0, LX/COy;->d:LX/3x6;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/COy;->d:LX/3x6;

    iget-object v3, p1, LX/COy;->d:LX/3x6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1883797
    goto :goto_0

    .line 1883798
    :cond_a
    iget-object v2, p1, LX/COy;->d:LX/3x6;

    if-nez v2, :cond_9

    .line 1883799
    :cond_b
    iget-boolean v2, p0, LX/COy;->e:Z

    iget-boolean v3, p1, LX/COy;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1883800
    goto :goto_0
.end method
