.class public final LX/BIU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)V
    .locals 0

    .prologue
    .line 1770583
    iput-object p1, p0, LX/BIU;->a:Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2af37868

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770584
    iget-object v1, p0, LX/BIU;->a:Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;

    .line 1770585
    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)Landroid/graphics/drawable/Animatable;

    move-result-object v2

    .line 1770586
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 1770587
    if-eqz v1, :cond_1

    .line 1770588
    iget-object v1, p0, LX/BIU;->a:Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;

    .line 1770589
    iget-object v2, v1, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setVisibility(I)V

    .line 1770590
    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)Landroid/graphics/drawable/Animatable;

    move-result-object v2

    .line 1770591
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->isRunning()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1770592
    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 1770593
    :cond_0
    :goto_1
    const v1, 0x438ab4d9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1770594
    :cond_1
    iget-object v1, p0, LX/BIU;->a:Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;

    .line 1770595
    iget-object v2, v1, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setVisibility(I)V

    .line 1770596
    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)Landroid/graphics/drawable/Animatable;

    move-result-object v2

    .line 1770597
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->isRunning()Z

    move-result p0

    if-nez p0, :cond_2

    .line 1770598
    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1770599
    :cond_2
    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
