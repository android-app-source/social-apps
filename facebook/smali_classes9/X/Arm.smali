.class public final LX/Arm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JI;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1719657
    iput-object p1, p0, LX/Arm;->b:LX/Arq;

    iput-object p2, p0, LX/Arm;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1719658
    return-void
.end method

.method public final a(LX/6JJ;)V
    .locals 2

    .prologue
    .line 1719659
    iget-object v0, p0, LX/Arm;->b:LX/Arq;

    const-string v1, "pr_camera_take_photo"

    invoke-static {v0, v1, p1}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719660
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 1719661
    iget-object v0, p0, LX/Arm;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    .line 1719662
    iget-object v1, v0, LX/Arh;->m:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    .line 1719663
    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    if-eqz v2, :cond_0

    .line 1719664
    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1719665
    :cond_0
    new-instance v2, LX/1ll;

    new-instance v3, LX/Arw;

    invoke-direct {v3, v1}, LX/Arw;-><init>(Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;)V

    sget-object v4, LX/1lk;->a:LX/1lk;

    const/4 p0, 0x0

    invoke-direct {v2, p1, v3, v4, p0}, LX/1ll;-><init>(Landroid/graphics/Bitmap;LX/1FN;LX/1lk;I)V

    move-object v2, v2

    .line 1719666
    invoke-static {v2}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    .line 1719667
    new-instance v2, LX/74k;

    invoke-direct {v2}, LX/74k;-><init>()V

    new-instance v3, LX/4gN;

    invoke-direct {v3}, LX/4gN;-><init>()V

    new-instance v4, LX/4gP;

    invoke-direct {v4}, LX/4gP;-><init>()V

    sget-object p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v4

    sget-object p0, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v4, p0}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v4

    sget-object p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a:Landroid/net/Uri;

    invoke-virtual {v4, p0}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v4

    sget-object p0, Lcom/facebook/ipc/media/data/MimeType;->a:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v4, p0}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p0

    .line 1719668
    iput p0, v4, LX/4gP;->f:I

    .line 1719669
    move-object v4, v4

    .line 1719670
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p0

    .line 1719671
    iput p0, v4, LX/4gP;->g:I

    .line 1719672
    move-object v4, v4

    .line 1719673
    invoke-virtual {v4}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v3

    invoke-virtual {v3}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v3

    .line 1719674
    iput-object v3, v2, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1719675
    move-object v2, v2

    .line 1719676
    invoke-virtual {v2}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v2

    move-object v2, v2

    .line 1719677
    iget-object v1, v0, LX/Arh;->n:LX/ArL;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/ArL;->a(Lcom/facebook/ipc/media/MediaItem;F)V

    .line 1719678
    iget-object v1, v0, LX/Arh;->o:LX/ArT;

    sget-object v3, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    invoke-virtual {v1, v3}, LX/ArT;->q(LX/ArJ;)V

    .line 1719679
    iget-object v1, v0, LX/Arh;->o:LX/ArT;

    sget-object v3, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    invoke-virtual {v1, v3}, LX/ArT;->c(LX/ArJ;)V

    .line 1719680
    iget-object v1, v0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 1719681
    invoke-static {v0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v3

    .line 1719682
    sget-object v4, LX/86q;->READY:LX/86q;

    invoke-static {v0, v3, v4}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    .line 1719683
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iq;

    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v2

    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    sget-object v4, LX/875;->CAPTURE:LX/875;

    invoke-static {v3, v1, v2, v4}, LX/2rf;->a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;

    .line 1719684
    invoke-static {v0, v3}, LX/Arh;->b(LX/Arh;LX/0jL;)LX/0jL;

    .line 1719685
    invoke-virtual {v3}, LX/0jL;->a()V

    .line 1719686
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1719687
    iget-object v0, p0, LX/Arm;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;

    invoke-direct {v1, p0}, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;-><init>(LX/Arm;)V

    const v2, -0x2463d786

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1719688
    return-void
.end method
