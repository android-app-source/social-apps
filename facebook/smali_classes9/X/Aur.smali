.class public LX/Aur;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:D

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/view/View;

.field public d:Landroid/view/GestureDetector;

.field public e:Landroid/view/ScaleGestureDetector;

.field public f:LX/9e2;

.field public g:LX/Auq;

.field public h:Landroid/content/Context;

.field public i:D

.field public j:Landroid/graphics/Rect;

.field public k:D

.field public l:F

.field public m:F

.field public n:F

.field public o:Z

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field private t:I

.field public u:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/Aut;

.field public w:LX/Av3;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Av3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1723349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723350
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    iput-wide v0, p0, LX/Aur;->a:D

    .line 1723351
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/Aur;->k:D

    .line 1723352
    iput-boolean v2, p0, LX/Aur;->o:Z

    .line 1723353
    iput-object p1, p0, LX/Aur;->h:Landroid/content/Context;

    .line 1723354
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    .line 1723355
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Aur;->c:Landroid/view/View;

    .line 1723356
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/Aun;

    invoke-direct {v1, p0}, LX/Aun;-><init>(LX/Aur;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/Aur;->d:Landroid/view/GestureDetector;

    .line 1723357
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, LX/Aup;

    invoke-direct {v1, p0}, LX/Aup;-><init>(LX/Aur;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, LX/Aur;->e:Landroid/view/ScaleGestureDetector;

    .line 1723358
    new-instance v0, LX/9e2;

    new-instance v1, LX/Auo;

    invoke-direct {v1, p0}, LX/Auo;-><init>(LX/Aur;)V

    invoke-direct {v0, p1, v1}, LX/9e2;-><init>(Landroid/content/Context;LX/9dh;)V

    iput-object v0, p0, LX/Aur;->f:LX/9e2;

    .line 1723359
    new-instance v0, LX/Auq;

    invoke-direct {v0, p0}, LX/Auq;-><init>(LX/Aur;)V

    iput-object v0, p0, LX/Aur;->g:LX/Auq;

    .line 1723360
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    iget-object v1, p0, LX/Aur;->g:LX/Auq;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1723361
    iput-object p2, p0, LX/Aur;->w:LX/Av3;

    .line 1723362
    iget-object v0, p0, LX/Aur;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a8e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Aur;->t:I

    .line 1723363
    invoke-virtual {p0}, LX/Aur;->e()V

    .line 1723364
    return-void
.end method

.method private static a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 1723343
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1723344
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723345
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 1723346
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1723347
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1723348
    return-void
.end method

.method public static i(LX/Aur;)V
    .locals 6

    .prologue
    .line 1723336
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v1

    iget v2, p0, LX/Aur;->t:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 1723337
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v1

    iget v2, p0, LX/Aur;->t:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1723338
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v2, p0, LX/Aur;->t:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723339
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    int-to-double v2, v1

    iget-wide v4, p0, LX/Aur;->i:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iget v2, p0, LX/Aur;->t:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1723340
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1723341
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getRotation()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 1723342
    return-void
.end method

.method private static k(LX/Aur;)V
    .locals 10

    .prologue
    .line 1723329
    iget-boolean v0, p0, LX/Aur;->o:Z

    if-eqz v0, :cond_0

    .line 1723330
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-double v0, v0

    iget-wide v2, p0, LX/Aur;->k:D

    mul-double/2addr v0, v2

    double-to-int v3, v0

    .line 1723331
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-double v0, v0

    iget-wide v4, p0, LX/Aur;->k:D

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 1723332
    iget v1, p0, LX/Aur;->l:F

    float-to-int v1, v1

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    iget v1, p0, LX/Aur;->p:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1723333
    iget v0, p0, LX/Aur;->m:F

    float-to-int v0, v0

    div-int/lit8 v1, v3, 0x2

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1723334
    iget-object v0, p0, LX/Aur;->w:LX/Av3;

    iget-object v1, p0, LX/Aur;->b:Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/Rect;

    int-to-double v6, v3

    iget-wide v8, p0, LX/Aur;->i:D

    mul-double/2addr v6, v8

    double-to-int v6, v6

    add-int/2addr v6, v4

    add-int/2addr v3, v5

    invoke-direct {v2, v4, v5, v6, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v3, 0x0

    iget v4, p0, LX/Aur;->n:F

    new-instance v5, LX/Aum;

    invoke-direct {v5, p0}, LX/Aum;-><init>(LX/Aur;)V

    invoke-virtual/range {v0 .. v5}, LX/Av3;->a(Landroid/widget/ImageView;Landroid/graphics/Rect;FFLX/Auk;)V

    .line 1723335
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 4

    .prologue
    .line 1723320
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-static {v0, p1, p2, p3, p4}, LX/Aur;->a(Landroid/view/View;IIII)V

    .line 1723321
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-static {v0, p1, p2, p3, p4}, LX/Aur;->a(Landroid/view/View;IIII)V

    .line 1723322
    iput p1, p0, LX/Aur;->p:I

    .line 1723323
    iput p2, p0, LX/Aur;->q:I

    .line 1723324
    iput p3, p0, LX/Aur;->r:I

    .line 1723325
    iput p4, p0, LX/Aur;->s:I

    .line 1723326
    if-nez p4, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, LX/Aur;->i:D

    .line 1723327
    return-void

    .line 1723328
    :cond_0
    int-to-double v0, p3

    int-to-double v2, p4

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1723288
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    .line 1723289
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getLeft()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getTop()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getRight()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getBottom()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1723290
    iput-object v1, p0, LX/Aur;->j:Landroid/graphics/Rect;

    .line 1723291
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getRotation()F

    move-result v0

    iput v0, p0, LX/Aur;->n:F

    .line 1723292
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getScaleFactor()D

    move-result-wide v2

    iput-wide v2, p0, LX/Aur;->k:D

    .line 1723293
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getLeftPercentage()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    add-float/2addr v0, v2

    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    .line 1723294
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTopPercentage()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float/2addr v1, v2

    .line 1723295
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v2, v0

    iput v2, p0, LX/Aur;->l:F

    .line 1723296
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v2, v1

    iput v2, p0, LX/Aur;->m:F

    .line 1723297
    float-to-int v0, v0

    float-to-int v1, v1

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/Aur;->a(IIII)V

    .line 1723298
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Aur;->o:Z

    .line 1723299
    invoke-static {p0}, LX/Aur;->k(LX/Aur;)V

    .line 1723300
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1723318
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1723319
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1723314
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1723315
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1723316
    invoke-static {p0}, LX/Aur;->k(LX/Aur;)V

    .line 1723317
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1723301
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1723302
    iget-object v0, p0, LX/Aur;->u:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1723303
    iput v3, p0, LX/Aur;->l:F

    .line 1723304
    iput v3, p0, LX/Aur;->m:F

    .line 1723305
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/Aur;->k:D

    .line 1723306
    iput-boolean v2, p0, LX/Aur;->o:Z

    .line 1723307
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1723308
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1723309
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-static {v0, v2, v2, v2, v2}, LX/Aur;->a(Landroid/view/View;IIII)V

    .line 1723310
    invoke-static {p0}, LX/Aur;->i(LX/Aur;)V

    .line 1723311
    :cond_0
    iget-object v0, p0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1723312
    iget-object v0, p0, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1723313
    return-void
.end method
