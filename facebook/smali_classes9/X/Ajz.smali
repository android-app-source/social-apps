.class public final LX/Ajz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final a:Landroid/graphics/ColorMatrix;

.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 1708623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708624
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    iput-object v0, p0, LX/Ajz;->a:Landroid/graphics/ColorMatrix;

    .line 1708625
    iput-object p1, p0, LX/Ajz;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1708626
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1708627
    iget-object v1, p0, LX/Ajz;->a:Landroid/graphics/ColorMatrix;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1708628
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    iget-object v0, p0, LX/Ajz;->a:Landroid/graphics/ColorMatrix;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 1708629
    iget-object v0, p0, LX/Ajz;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/ColorFilter;)V

    .line 1708630
    return-void
.end method
