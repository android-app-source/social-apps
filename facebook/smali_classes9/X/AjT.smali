.class public LX/AjT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1708039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/7zl;
    .locals 14

    .prologue
    .line 1708040
    new-instance v0, LX/7zl;

    invoke-virtual {p0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E()J

    move-result-wide v2

    const-string v1, "Ad"

    .line 1708041
    iget-object v4, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v4, v4

    .line 1708042
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1708043
    iget-boolean v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E:Z

    move v5, v1

    .line 1708044
    invoke-virtual {p0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->I()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v10

    .line 1708045
    iget v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->x:I

    move v11, v1

    .line 1708046
    iget-object v1, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v12, v1

    .line 1708047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v13

    move-object v1, p0

    invoke-direct/range {v0 .. v13}, LX/7zl;-><init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;JZZDDIILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1708048
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1708049
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1708050
    if-eqz v0, :cond_0

    .line 1708051
    invoke-static {v0}, LX/AjT;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/7zl;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1708052
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/util/List;I)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1jg;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1708053
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1708054
    const/4 v2, 0x0

    .line 1708055
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move/from16 v16, v2

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v15, v2

    check-cast v15, LX/1jg;

    .line 1708056
    if-eqz v15, :cond_1

    .line 1708057
    new-instance v2, LX/7zl;

    iget-object v3, v15, LX/1jg;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    iget-wide v4, v15, LX/1jg;->e:J

    iget-boolean v6, v15, LX/1jg;->f:Z

    iget-boolean v7, v15, LX/1jg;->g:Z

    iget-wide v8, v15, LX/1jg;->h:D

    iget-wide v10, v15, LX/1jg;->i:D

    iget v12, v15, LX/1jg;->j:I

    iget v13, v15, LX/1jg;->k:I

    iget-object v14, v15, LX/1jg;->l:Ljava/lang/String;

    iget-object v15, v15, LX/1jg;->d:Ljava/lang/String;

    invoke-direct/range {v2 .. v15}, LX/7zl;-><init>(Lcom/facebook/feed/model/ClientFeedUnitEdge;JZZDDIILjava/lang/String;Ljava/lang/String;)V

    .line 1708058
    move/from16 v0, v16

    move/from16 v1, p1

    if-ne v0, v1, :cond_0

    .line 1708059
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/7zl;->a(Z)V

    .line 1708060
    :cond_0
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1708061
    :cond_1
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    .line 1708062
    goto :goto_0

    .line 1708063
    :cond_2
    return-object v17
.end method
