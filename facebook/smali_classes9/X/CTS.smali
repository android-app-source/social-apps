.class public final LX/CTS;
.super LX/CTK;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:LX/CU1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CU0;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1895758
    invoke-direct {p0, p1, p2, p3}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    .line 1895759
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/CTS;->a:Z

    .line 1895760
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->s()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->s()I

    move-result v0

    :goto_0
    iput v0, p0, LX/CTS;->d:I

    .line 1895761
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ah()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, LX/CU1;

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ah()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v2

    invoke-direct {v0, v2}, LX/CU1;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;)V

    :goto_1
    iput-object v0, p0, LX/CTS;->b:LX/CU1;

    .line 1895762
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CTS;->c:Ljava/util/ArrayList;

    .line 1895763
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->Q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1895764
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->Q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    .line 1895765
    iget-object v5, p0, LX/CTS;->c:Ljava/util/ArrayList;

    new-instance v6, LX/CU0;

    invoke-static {v0}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;)LX/CUx;

    move-result-object v0

    invoke-direct {v6, v0}, LX/CU0;-><init>(LX/CUx;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895766
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1895767
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->Q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0

    .line 1895768
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1895769
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CTS;->k:Ljava/util/ArrayList;

    .line 1895770
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->jU_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1895771
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->jU_()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1895772
    iget-object v4, p0, LX/CTS;->k:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895773
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1895774
    :cond_3
    return-void
.end method
