.class public final LX/BHN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BHO;


# direct methods
.method public constructor <init>(LX/BHO;)V
    .locals 0

    .prologue
    .line 1769006
    iput-object p1, p0, LX/BHN;->a:LX/BHO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1769007
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1769008
    iget-object v0, p0, LX/BHN;->a:LX/BHO;

    iget-object v0, v0, LX/BHO;->f:LX/BHQ;

    iget-object v0, v0, LX/BHQ;->d:LX/8I2;

    iget-object v1, p0, LX/BHN;->a:LX/BHO;

    iget-object v1, v1, LX/BHO;->a:LX/4gI;

    iget-object v2, p0, LX/BHN;->a:LX/BHO;

    iget-object v2, v2, LX/BHO;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1769009
    iget-object v1, p0, LX/BHN;->a:LX/BHO;

    iget-object v1, v1, LX/BHO;->c:LX/BHj;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BHN;->a:LX/BHO;

    iget-object v1, v1, LX/BHO;->c:LX/BHj;

    invoke-virtual {v1}, LX/BHj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, LX/BHN;->a:LX/BHO;

    iget-boolean v1, v1, LX/BHO;->d:Z

    if-eqz v1, :cond_2

    .line 1769010
    :cond_1
    new-instance v1, Landroid/database/MatrixCursor;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v4

    const-string v3, "media_type"

    aput-object v3, v2, v5

    const-string v3, "mime_type"

    aput-object v3, v2, v6

    const-string v3, "camera_entry"

    aput-object v3, v2, v7

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1769011
    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1769012
    new-array v2, v6, [Landroid/database/Cursor;

    aput-object v1, v2, v4

    aput-object v0, v2, v5

    .line 1769013
    new-instance v0, Landroid/database/MergeCursor;

    invoke-direct {v0, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1769014
    :cond_2
    iget-object v1, p0, LX/BHN;->a:LX/BHO;

    iget-object v1, v1, LX/BHO;->f:LX/BHQ;

    iget-object v1, v1, LX/BHQ;->a:LX/0Sh;

    new-instance v2, Lcom/facebook/photos/simplepicker/controller/SimplePickerDataLoadUtil$1$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/photos/simplepicker/controller/SimplePickerDataLoadUtil$1$1$1;-><init>(LX/BHN;Landroid/database/Cursor;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1769015
    const/4 v0, 0x0

    return-object v0
.end method
