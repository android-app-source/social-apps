.class public final LX/BuI;
.super LX/19M;
.source ""


# instance fields
.field public final synthetic a:LX/BuJ;


# direct methods
.method public constructor <init>(LX/BuJ;)V
    .locals 0

    .prologue
    .line 1830644
    iput-object p1, p0, LX/BuI;->a:LX/BuJ;

    invoke-direct {p0}, LX/19M;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1830645
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1830646
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2os;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/2os;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830647
    :cond_0
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-boolean v0, v0, LX/BuJ;->G:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    if-eqz v0, :cond_3

    .line 1830648
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830649
    iget-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    move v0, v1

    .line 1830650
    if-nez v0, :cond_1

    .line 1830651
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qc;

    iget-object v2, p0, LX/BuI;->a:LX/BuJ;

    iget-object v2, v2, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830652
    iget v3, v2, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->G:I

    move v2, v3

    .line 1830653
    sget-object v3, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-direct {v1, v2, v3}, LX/2qc;-><init>(ILX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830654
    :cond_1
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830655
    iget-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->H:Z

    move v0, v1

    .line 1830656
    if-nez v0, :cond_2

    .line 1830657
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2qb;

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-direct {v1, v2}, LX/2qb;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830658
    :cond_2
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->F:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1830659
    iget-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->O:Z

    move v0, v1

    .line 1830660
    if-eqz v0, :cond_3

    .line 1830661
    iget-object v0, p0, LX/BuI;->a:LX/BuJ;

    invoke-static {v0}, LX/BuJ;->getFullScreenVideoPlayer(LX/BuJ;)Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    move-result-object v0

    .line 1830662
    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b()Z

    .line 1830663
    :cond_3
    return-void
.end method
