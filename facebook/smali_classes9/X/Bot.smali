.class public final LX/Bot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1822253
    iput-object p1, p0, LX/Bot;->b:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iput-object p2, p0, LX/Bot;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x2ce12e5d    # 6.400032E-12f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1822254
    iget-object v1, p0, LX/Bot;->b:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->e:LX/749;

    iget-object v2, p0, LX/Bot;->b:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iget-object v2, v2, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->f:Ljava/lang/String;

    .line 1822255
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/748;->PROFILE_PHOTO_PROMPT_CLICKED:LX/748;

    invoke-virtual {v4}, LX/748;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1822256
    invoke-static {v1, v3, v2}, LX/749;->a(LX/749;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1822257
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    .line 1822258
    iget-object v1, p0, LX/Bot;->b:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, p0, LX/Bot;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x0

    sget-object v6, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    .line 1822259
    const v1, 0x555ae9a9

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
