.class public final LX/BEw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V
    .locals 0

    .prologue
    .line 1765155
    iput-object p1, p0, LX/BEw;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1765158
    iget-object v0, p0, LX/BEw;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AaV;

    .line 1765159
    invoke-interface {v0}, LX/AaV;->b()V

    goto :goto_0

    .line 1765160
    :cond_0
    iget-object v0, p0, LX/BEw;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 1765161
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1765162
    invoke-direct {p0}, LX/BEw;->b()V

    .line 1765163
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1765156
    invoke-direct {p0}, LX/BEw;->b()V

    .line 1765157
    return-void
.end method
