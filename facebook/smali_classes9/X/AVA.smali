.class public LX/AVA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/app/Activity;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1678953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678954
    iput-object p1, p0, LX/AVA;->a:Landroid/app/Activity;

    .line 1678955
    invoke-virtual {p1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, LX/AVA;->b:I

    .line 1678956
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1678957
    sparse-switch p0, :sswitch_data_0

    .line 1678958
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 1678959
    :sswitch_0
    const-string v0, "screen_orientation_portrait"

    goto :goto_0

    .line 1678960
    :sswitch_1
    const-string v0, "screen_orientation_reverse_portrait"

    goto :goto_0

    .line 1678961
    :sswitch_2
    const-string v0, "screen_orientation_landscape"

    goto :goto_0

    .line 1678962
    :sswitch_3
    const-string v0, "screen_orientation_reverse_landscape"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_0
        0x8 -> :sswitch_3
        0x9 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    .line 1678963
    iget-object v0, p0, LX/AVA;->a:Landroid/app/Activity;

    invoke-virtual {p0}, LX/AVA;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1678964
    return-void
.end method

.method public final c()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .prologue
    .line 1678965
    iget-object v0, p0, LX/AVA;->a:Landroid/app/Activity;

    iget v1, p0, LX/AVA;->b:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1678966
    return-void
.end method

.method public final d()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1678967
    iget-object v1, p0, LX/AVA;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 1678968
    iget-object v2, p0, LX/AVA;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    packed-switch v2, :pswitch_data_0

    .line 1678969
    :cond_0
    :goto_0
    return v0

    .line 1678970
    :pswitch_0
    if-eqz v1, :cond_0

    if-eq v1, v0, :cond_0

    .line 1678971
    const/16 v0, 0x9

    .line 1678972
    goto :goto_0

    .line 1678973
    :pswitch_1
    if-eqz v1, :cond_1

    if-ne v1, v0, :cond_2

    .line 1678974
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1678975
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
