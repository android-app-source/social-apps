.class public final LX/C3t;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C3u;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pq;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<*>;"
        }
    .end annotation
.end field

.field public c:LX/0yL;

.field public d:Ljava/lang/Boolean;

.field public final synthetic e:LX/C3u;


# direct methods
.method public constructor <init>(LX/C3u;)V
    .locals 1

    .prologue
    .line 1846446
    iput-object p1, p0, LX/C3t;->e:LX/C3u;

    .line 1846447
    move-object v0, p1

    .line 1846448
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1846449
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1846450
    const-string v0, "DialtoneStateChangeInvalidatorComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/C3u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1846451
    check-cast p1, LX/C3t;

    .line 1846452
    iget-object v0, p1, LX/C3t;->c:LX/0yL;

    iput-object v0, p0, LX/C3t;->c:LX/0yL;

    .line 1846453
    iget-object v0, p1, LX/C3t;->d:Ljava/lang/Boolean;

    iput-object v0, p0, LX/C3t;->d:Ljava/lang/Boolean;

    .line 1846454
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1846455
    if-ne p0, p1, :cond_1

    .line 1846456
    :cond_0
    :goto_0
    return v0

    .line 1846457
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1846458
    goto :goto_0

    .line 1846459
    :cond_3
    check-cast p1, LX/C3t;

    .line 1846460
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1846461
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1846462
    if-eq v2, v3, :cond_0

    .line 1846463
    iget-object v2, p0, LX/C3t;->a:LX/1Pq;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C3t;->a:LX/1Pq;

    iget-object v3, p1, LX/C3t;->a:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1846464
    goto :goto_0

    .line 1846465
    :cond_5
    iget-object v2, p1, LX/C3t;->a:LX/1Pq;

    if-nez v2, :cond_4

    .line 1846466
    :cond_6
    iget-object v2, p0, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1846467
    goto :goto_0

    .line 1846468
    :cond_7
    iget-object v2, p1, LX/C3t;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1846469
    const/4 v1, 0x0

    .line 1846470
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C3t;

    .line 1846471
    iput-object v1, v0, LX/C3t;->c:LX/0yL;

    .line 1846472
    iput-object v1, v0, LX/C3t;->d:Ljava/lang/Boolean;

    .line 1846473
    return-object v0
.end method
