.class public final LX/Bn7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bn8;


# direct methods
.method public constructor <init>(LX/Bn8;)V
    .locals 0

    .prologue
    .line 1819802
    iput-object p1, p0, LX/Bn7;->a:LX/Bn8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 1819803
    const/4 v3, 0x1

    .line 1819804
    invoke-static {}, LX/BnP;->a()LX/BnN;

    move-result-object v0

    .line 1819805
    const-string v1, "half_width"

    iget-object v2, p0, LX/Bn7;->a:LX/Bn8;

    .line 1819806
    iget-object v4, v2, LX/Bn8;->i:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    iget-object v5, v2, LX/Bn8;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p2, 0x7f0b1e1d

    invoke-virtual {v5, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget p2, v2, LX/Bn8;->p:I

    add-int/lit8 p2, p2, -0x1

    mul-int/2addr v5, p2

    sub-int/2addr v4, v5

    iget v5, v2, LX/Bn8;->p:I

    div-int/2addr v4, v5

    move v2, v4

    .line 1819807
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1819808
    const-string v1, "half_height"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1819809
    const-string v1, "full_width"

    iget-object v2, p0, LX/Bn7;->a:LX/Bn8;

    .line 1819810
    iget-object v4, v2, LX/Bn8;->i:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    move v2, v4

    .line 1819811
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1819812
    const-string v1, "full_height"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1819813
    const-string v1, "count"

    .line 1819814
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 1819815
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1819816
    const-string v1, "end_cursor"

    .line 1819817
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1819818
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1819819
    iget-object v1, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v1, v1, LX/Bn8;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1819820
    const-string v1, "theme_category_id"

    iget-object v2, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v2, v2, LX/Bn8;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1819821
    :cond_0
    iget-object v1, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v1, v1, LX/Bn8;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v1, v1, LX/Bn8;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v1, v1, LX/Bn8;->h:LX/0ad;

    sget-short v2, LX/BnL;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1819822
    new-instance v1, LX/4EX;

    invoke-direct {v1}, LX/4EX;-><init>()V

    .line 1819823
    iget-object v2, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v2, v2, LX/Bn8;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EX;->a(Ljava/lang/String;)LX/4EX;

    move-result-object v2

    iget-object v3, p0, LX/Bn7;->a:LX/Bn8;

    iget-object v3, v3, LX/Bn8;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EX;->b(Ljava/lang/String;)LX/4EX;

    .line 1819824
    const-string v2, "event_info"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1819825
    :cond_2
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1819826
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1819827
    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1819828
    const/4 v0, 0x1

    const-class v3, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    new-instance v3, LX/5Mb;

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    const/4 v4, 0x2

    invoke-virtual {v1, v2, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-direct {v3, v0, v1, v2}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    return-object v3

    .line 1819829
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1819830
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1819831
    goto :goto_0
.end method
