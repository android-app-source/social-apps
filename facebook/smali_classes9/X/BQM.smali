.class public final LX/BQM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/BQN;


# direct methods
.method public constructor <init>(LX/BQN;)V
    .locals 0

    .prologue
    .line 1782017
    iput-object p1, p0, LX/BQM;->a:LX/BQN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    .line 1782018
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, LX/BQM;->a:LX/BQN;

    invoke-virtual {v1}, LX/BQN;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1782019
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1782020
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/BQM;->a:LX/BQN;

    invoke-virtual {v2}, LX/BQN;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const-string v2, "UID"

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, LX/BQK;

    invoke-direct {v3, p0, v0}, LX/BQK;-><init>(LX/BQM;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const-string v2, "Cancel"

    new-instance v3, LX/BQJ;

    invoke-direct {v3, p0}, LX/BQJ;-><init>(LX/BQM;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 1782021
    new-instance v2, LX/BQL;

    invoke-direct {v2, p0, v1}, LX/BQL;-><init>(LX/BQM;LX/2EJ;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1782022
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1782023
    const/4 v0, 0x1

    return v0
.end method
