.class public final LX/C0o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24m;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Pq;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1841489
    iput-object p1, p0, LX/C0o;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841490
    iput-object p2, p0, LX/C0o;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841491
    iput-object p3, p0, LX/C0o;->c:LX/1Pq;

    .line 1841492
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZI)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ":",
            "LX/24e;",
            ">(TT;ZI)V"
        }
    .end annotation

    .prologue
    .line 1841493
    iget-object v0, p0, LX/C0o;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->g:LX/1eq;

    iget-object v1, p0, LX/C0o;->a:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->e:LX/0Ot;

    iget-object v2, p0, LX/C0o;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C0o;->c:LX/1Pq;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1eq;LX/0Ot;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1841494
    return-void
.end method
