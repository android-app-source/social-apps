.class public final LX/BEL;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V
    .locals 0

    .prologue
    .line 1764322
    iput-object p1, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    .line 1764323
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    const/4 v1, 0x0

    .line 1764324
    iput-object v1, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1764325
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1764326
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v0, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1764327
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v0, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1764328
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v0, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1764329
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v0, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1764330
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1764331
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1764332
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    .line 1764333
    iget-object v1, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v1, v1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1764334
    iget-object v1, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v1, v1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1764335
    iget-object v1, v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    move-object v1, v1

    .line 1764336
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1764337
    iget-object v1, v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    move-object v0, v1

    .line 1764338
    iget-object v1, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iget-object v1, v1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->e:LX/BEO;

    .line 1764339
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1764340
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1764341
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/pages/PageInfo;

    .line 1764342
    invoke-virtual {v2}, Lcom/facebook/ipc/pages/PageInfo;->a()LX/8A4;

    move-result-object p0

    sget-object p1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {p0, p1}, LX/8A4;->a(LX/8A3;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1764343
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1764344
    :cond_1
    move-object v0, v3

    .line 1764345
    iput-object v0, v1, LX/BEO;->a:Ljava/util/List;

    .line 1764346
    const v2, 0x7ce876ea

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1764347
    :goto_1
    return-void

    .line 1764348
    :cond_2
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0824a9

    const/16 v2, 0x11

    const/4 v6, 0x0

    .line 1764349
    invoke-static {}, LX/0kL;->a()Ljava/lang/String;

    move-result-object v8

    .line 1764350
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v3, v0

    move v5, v2

    move-object v7, v6

    .line 1764351
    invoke-static/range {v3 .. v8}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1764352
    iget-object v0, p0, LX/BEL;->a:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1
.end method
