.class public LX/CNb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1881954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881955
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/CNb;->a:LX/01J;

    .line 1881956
    return-void
.end method

.method public static b(LX/CNb;Ljava/lang/String;F)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1881957
    invoke-virtual {p0, p1, v1}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v0

    mul-float/2addr v0, p2

    .line 1881958
    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 1881959
    const/4 v0, 0x1

    .line 1881960
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 1881961
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881962
    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 1881970
    int-to-float v0, p2

    invoke-virtual {p0, p1, v0}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 1881963
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p0, p1, v0}, LX/CNb;->b(LX/CNb;Ljava/lang/String;F)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 1881964
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881965
    if-nez v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1881966
    invoke-virtual {p0, p1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1881967
    if-nez v0, :cond_0

    .line 1881968
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1881969
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/0Px;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1881952
    iget-object v0, p0, LX/CNb;->a:LX/01J;

    const-string v1, "style"

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1881953
    iget-object v0, p0, LX/CNb;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1881938
    invoke-virtual {p0, p1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1881939
    if-nez v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 1881950
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881951
    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1881940
    iget-object v0, p0, LX/CNb;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;I)I
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1881941
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881942
    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/CNb;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1881943
    invoke-virtual {p0, p1}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1881944
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1881945
    iget-object v0, p0, LX/CNb;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1881946
    invoke-virtual {p0, p1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1881947
    invoke-virtual {p0}, LX/CNb;->a()Ljava/lang/String;

    .line 1881948
    move-object v0, p1

    .line 1881949
    iget-object v1, p0, LX/CNb;->a:LX/01J;

    invoke-virtual {v1, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
