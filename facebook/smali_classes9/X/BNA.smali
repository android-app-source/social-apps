.class public final LX/BNA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BND;

.field public final synthetic b:LX/BNE;


# direct methods
.method public constructor <init>(LX/BNE;LX/BND;)V
    .locals 0

    .prologue
    .line 1778378
    iput-object p1, p0, LX/BNA;->b:LX/BNE;

    iput-object p2, p0, LX/BNA;->a:LX/BND;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1778379
    iget-object v0, p0, LX/BNA;->a:LX/BND;

    invoke-interface {v0}, LX/BND;->a()V

    .line 1778380
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1778381
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1778382
    if-eqz p1, :cond_0

    .line 1778383
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1778384
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1778385
    :goto_0
    iget-object v1, p0, LX/BNA;->a:LX/BND;

    invoke-interface {v1, v0}, LX/BND;->a(LX/55r;)V

    .line 1778386
    return-void

    .line 1778387
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1778388
    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v0

    goto :goto_0
.end method
