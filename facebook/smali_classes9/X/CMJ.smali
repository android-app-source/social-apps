.class public final LX/CMJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/CMK;


# direct methods
.method public constructor <init>(LX/CMK;)V
    .locals 0

    .prologue
    .line 1880437
    iput-object p1, p0, LX/CMJ;->a:LX/CMK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 1880438
    iget-object v0, p0, LX/CMJ;->a:LX/CMK;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1880439
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_3

    .line 1880440
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 1880441
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 1880442
    iget-object v5, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v5}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLeft()I

    move-result v5

    .line 1880443
    iget-object v6, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getTop()I

    move-result v6

    .line 1880444
    if-lt v4, v6, :cond_0

    iget-object p1, v0, LX/CMK;->m:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result p1

    add-int/2addr v6, p1

    if-ge v4, v6, :cond_0

    if-lt v3, v5, :cond_0

    iget-object v4, v0, LX/CMK;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    add-int/2addr v4, v5

    if-lt v3, v4, :cond_1

    :cond_0
    move v1, v2

    .line 1880445
    :cond_1
    :goto_0
    move v0, v1

    .line 1880446
    if-eqz v0, :cond_2

    .line 1880447
    iget-object v0, p0, LX/CMJ;->a:LX/CMK;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1880448
    const/4 v0, 0x1

    .line 1880449
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1880450
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    move v1, v2

    .line 1880451
    goto :goto_0
.end method
