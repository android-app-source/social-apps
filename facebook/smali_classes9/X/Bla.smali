.class public LX/Bla;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private b:Landroid/content/Context;

.field public c:LX/14x;

.field private d:LX/17Y;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Landroid/content/Context;LX/14x;LX/17Y;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1816265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816266
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Bla;->a:Z

    .line 1816267
    iput-object p2, p0, LX/Bla;->b:Landroid/content/Context;

    .line 1816268
    iput-object p3, p0, LX/Bla;->c:LX/14x;

    .line 1816269
    iput-object p4, p0, LX/Bla;->d:LX/17Y;

    .line 1816270
    return-void
.end method

.method public static a(LX/0QB;)LX/Bla;
    .locals 1

    .prologue
    .line 1816271
    invoke-static {p0}, LX/Bla;->b(LX/0QB;)LX/Bla;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZZ)Z
    .locals 1

    .prologue
    .line 1816264
    if-nez p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Bla;
    .locals 5

    .prologue
    .line 1816262
    new-instance v4, LX/Bla;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v2

    check-cast v2, LX/14x;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Bla;-><init>(Ljava/lang/Boolean;Landroid/content/Context;LX/14x;LX/17Y;)V

    .line 1816263
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1816247
    if-eqz p1, :cond_0

    if-nez p5, :cond_2

    .line 1816248
    :cond_0
    const/4 v0, 0x0

    .line 1816249
    :cond_1
    :goto_0
    return-object v0

    .line 1816250
    :cond_2
    iget-boolean v0, p0, LX/Bla;->a:Z

    invoke-static {p2, p3, v0, p4}, LX/Bla;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZZ)Z

    move-result v1

    .line 1816251
    if-eqz p2, :cond_4

    .line 1816252
    sget-object v0, LX/0ax;->cC:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1816253
    :goto_1
    iget-object v2, p0, LX/Bla;->d:LX/17Y;

    iget-object v3, p0, LX/Bla;->b:Landroid/content/Context;

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1816254
    const-string v2, "extra_invite_action_mechanism"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1816255
    if-nez v1, :cond_1

    .line 1816256
    const-string v1, "extra_enable_invite_through_messenger"

    .line 1816257
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eq p3, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne p3, v2, :cond_6

    :cond_3
    iget-object v2, p0, LX/Bla;->c:LX/14x;

    invoke-virtual {v2}, LX/14x;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bla;->c:LX/14x;

    const-string v3, "50.0"

    invoke-virtual {v2, v3}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1816258
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 1816259
    :cond_4
    if-eqz v1, :cond_5

    .line 1816260
    sget-object v0, LX/0ax;->cB:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1816261
    :cond_5
    sget-object v0, LX/0ax;->cA:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method
