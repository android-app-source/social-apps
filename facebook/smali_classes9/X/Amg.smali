.class public LX/Amg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1711323
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Amg;->a:Ljava/lang/String;

    .line 1711324
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Amg;->b:Ljava/lang/String;

    .line 1711325
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Amg;->c:Ljava/lang/String;

    .line 1711326
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1711320
    iget-object v0, p0, LX/Amg;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 3

    .prologue
    .line 1711327
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1711328
    iget-object v0, p0, LX/Amg;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1711329
    :goto_0
    return-void

    .line 1711330
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v0

    iget-object v1, p0, LX/Amg;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Amg;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v0

    .line 1711331
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "options"

    invoke-virtual {v1, v2, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1711332
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1711321
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1711319
    const-string v0, "QuestionAddResponseMutatingVisitor"

    return-object v0
.end method
