.class public LX/AmW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1710890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;II)LX/1Dg;
    .locals 6

    .prologue
    .line 1710891
    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v0

    .line 1710892
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v1

    .line 1710893
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 1710894
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 1710895
    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1710896
    const/4 v2, 0x0

    .line 1710897
    new-instance v3, LX/AmU;

    invoke-direct {v3}, LX/AmU;-><init>()V

    .line 1710898
    sget-object v4, LX/AmV;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/AmT;

    .line 1710899
    if-nez v4, :cond_0

    .line 1710900
    new-instance v4, LX/AmT;

    invoke-direct {v4}, LX/AmT;-><init>()V

    .line 1710901
    :cond_0
    invoke-static {v4, p0, v2, v2, v3}, LX/AmT;->a$redex0(LX/AmT;LX/1De;IILX/AmU;)V

    .line 1710902
    move-object v3, v4

    .line 1710903
    move-object v2, v3

    .line 1710904
    move-object v2, v2

    .line 1710905
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    invoke-interface {v2, v3, v0}, LX/1Di;->k(II)LX/1Di;

    move-result-object v0

    const/4 v2, 0x7

    invoke-interface {v0, v2, v1}, LX/1Di;->k(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
