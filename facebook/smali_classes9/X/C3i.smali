.class public LX/C3i;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3g;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1846099
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C3i;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846100
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1846101
    iput-object p1, p0, LX/C3i;->b:LX/0Ot;

    .line 1846102
    return-void
.end method

.method public static a(LX/0QB;)LX/C3i;
    .locals 4

    .prologue
    .line 1846103
    const-class v1, LX/C3i;

    monitor-enter v1

    .line 1846104
    :try_start_0
    sget-object v0, LX/C3i;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846105
    sput-object v2, LX/C3i;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846106
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846107
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846108
    new-instance v3, LX/C3i;

    const/16 p0, 0x1ece

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3i;-><init>(LX/0Ot;)V

    .line 1846109
    move-object v0, v3

    .line 1846110
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846111
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846112
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1846114
    check-cast p2, LX/C3h;

    .line 1846115
    iget-object v0, p0, LX/C3i;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;

    iget-object v1, p2, LX/C3h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x1

    .line 1846116
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->a:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    .line 1846117
    invoke-static {v1}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1846118
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1846119
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_0
    move-object v4, v4

    .line 1846120
    invoke-virtual {v3, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d7f

    invoke-interface {v3, p0, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d6e

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d6e

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1846121
    invoke-static {v1}, LX/C3b;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1846122
    iget-object v3, v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->b:LX/C3e;

    const/4 v4, 0x0

    .line 1846123
    new-instance p2, LX/C3d;

    invoke-direct {p2, v3}, LX/C3d;-><init>(LX/C3e;)V

    .line 1846124
    sget-object v0, LX/C3e;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3c;

    .line 1846125
    if-nez v0, :cond_0

    .line 1846126
    new-instance v0, LX/C3c;

    invoke-direct {v0}, LX/C3c;-><init>()V

    .line 1846127
    :cond_0
    invoke-static {v0, p1, v4, v4, p2}, LX/C3c;->a$redex0(LX/C3c;LX/1De;IILX/C3d;)V

    .line 1846128
    move-object p2, v0

    .line 1846129
    move-object v4, p2

    .line 1846130
    move-object v3, v4

    .line 1846131
    iget-object v4, v3, LX/C3c;->a:LX/C3d;

    iput-object v1, v4, LX/C3d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846132
    iget-object v4, v3, LX/C3c;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {v4, p2}, Ljava/util/BitSet;->set(I)V

    .line 1846133
    move-object v3, v3

    .line 1846134
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/1Di;->k(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1846135
    :cond_1
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1846136
    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1846137
    invoke-static {}, LX/1dS;->b()V

    .line 1846138
    const/4 v0, 0x0

    return-object v0
.end method
