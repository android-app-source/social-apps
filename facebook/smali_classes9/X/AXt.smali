.class public LX/AXt;
.super LX/AWm;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684631
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AXt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684632
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684633
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AXt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684634
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684635
    invoke-direct {p0, p1, p2, p3}, LX/AWm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684636
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1684637
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 1684638
    const v2, 0x7f01046f

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1684639
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 1684640
    invoke-virtual {p0}, LX/AXt;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setDescription(Ljava/lang/CharSequence;)V

    .line 1684641
    invoke-virtual {p0}, LX/AXt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c8a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setTitle(Ljava/lang/CharSequence;)V

    .line 1684642
    invoke-virtual {p0}, LX/AXt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c84

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setActionFinishText(Ljava/lang/CharSequence;)V

    .line 1684643
    invoke-virtual {p0}, LX/AXt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c85

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setActionResumeText(Ljava/lang/CharSequence;)V

    .line 1684644
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1684645
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->ABOUT_TO_FINISH:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1684646
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1684647
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1684648
    return-void
.end method
