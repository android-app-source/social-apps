.class public final LX/C2a;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/38z;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/38z;


# direct methods
.method public constructor <init>(LX/38z;)V
    .locals 1

    .prologue
    .line 1844417
    iput-object p1, p0, LX/C2a;->c:LX/38z;

    .line 1844418
    move-object v0, p1

    .line 1844419
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1844420
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844421
    const-string v0, "PageLikeAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1844422
    if-ne p0, p1, :cond_1

    .line 1844423
    :cond_0
    :goto_0
    return v0

    .line 1844424
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1844425
    goto :goto_0

    .line 1844426
    :cond_3
    check-cast p1, LX/C2a;

    .line 1844427
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1844428
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1844429
    if-eq v2, v3, :cond_0

    .line 1844430
    iget-object v2, p0, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1844431
    goto :goto_0

    .line 1844432
    :cond_5
    iget-object v2, p1, LX/C2a;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1844433
    :cond_6
    iget-object v2, p0, LX/C2a;->b:LX/1Pd;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C2a;->b:LX/1Pd;

    iget-object v3, p1, LX/C2a;->b:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1844434
    goto :goto_0

    .line 1844435
    :cond_7
    iget-object v2, p1, LX/C2a;->b:LX/1Pd;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
