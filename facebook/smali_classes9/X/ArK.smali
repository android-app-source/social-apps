.class public final enum LX/ArK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArK;

.field public static final enum CAMERA_SESSION_ID:LX/ArK;

.field public static final enum DOODLE_SESSION_ID:LX/ArK;

.field public static final enum EDITING_SESSION_ID:LX/ArK;

.field public static final enum EFFECTS_TRAY_SESSION_ID:LX/ArK;

.field public static final enum GALLERY_SESSION_ID:LX/ArK;

.field public static final enum INSPIRATION_SESSION_ID:LX/ArK;

.field public static final enum NUX_SESSION_ID:LX/ArK;

.field public static final enum SEQUENCE_NUMBER:LX/ArK;

.field public static final enum SHARE_SHEET_SESSION_ID:LX/ArK;

.field public static final enum TEXT_SESSION_ID:LX/ArK;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1718896
    new-instance v0, LX/ArK;

    const-string v1, "INSPIRATION_SESSION_ID"

    const-string v2, "inspiration_session_id"

    invoke-direct {v0, v1, v4, v2}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->INSPIRATION_SESSION_ID:LX/ArK;

    .line 1718897
    new-instance v0, LX/ArK;

    const-string v1, "SEQUENCE_NUMBER"

    const-string v2, "sequence_number"

    invoke-direct {v0, v1, v5, v2}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->SEQUENCE_NUMBER:LX/ArK;

    .line 1718898
    new-instance v0, LX/ArK;

    const-string v1, "NUX_SESSION_ID"

    const-string v2, "nux_session_id"

    invoke-direct {v0, v1, v6, v2}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->NUX_SESSION_ID:LX/ArK;

    .line 1718899
    new-instance v0, LX/ArK;

    const-string v1, "CAMERA_SESSION_ID"

    const-string v2, "camera_session_id"

    invoke-direct {v0, v1, v7, v2}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->CAMERA_SESSION_ID:LX/ArK;

    .line 1718900
    new-instance v0, LX/ArK;

    const-string v1, "EDITING_SESSION_ID"

    const-string v2, "editing_session_id"

    invoke-direct {v0, v1, v8, v2}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->EDITING_SESSION_ID:LX/ArK;

    .line 1718901
    new-instance v0, LX/ArK;

    const-string v1, "EFFECTS_TRAY_SESSION_ID"

    const/4 v2, 0x5

    const-string v3, "effects_tray_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->EFFECTS_TRAY_SESSION_ID:LX/ArK;

    .line 1718902
    new-instance v0, LX/ArK;

    const-string v1, "GALLERY_SESSION_ID"

    const/4 v2, 0x6

    const-string v3, "gallery_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->GALLERY_SESSION_ID:LX/ArK;

    .line 1718903
    new-instance v0, LX/ArK;

    const-string v1, "DOODLE_SESSION_ID"

    const/4 v2, 0x7

    const-string v3, "doodle_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->DOODLE_SESSION_ID:LX/ArK;

    .line 1718904
    new-instance v0, LX/ArK;

    const-string v1, "TEXT_SESSION_ID"

    const/16 v2, 0x8

    const-string v3, "text_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->TEXT_SESSION_ID:LX/ArK;

    .line 1718905
    new-instance v0, LX/ArK;

    const-string v1, "SHARE_SHEET_SESSION_ID"

    const/16 v2, 0x9

    const-string v3, "share_sheet_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArK;->SHARE_SHEET_SESSION_ID:LX/ArK;

    .line 1718906
    const/16 v0, 0xa

    new-array v0, v0, [LX/ArK;

    sget-object v1, LX/ArK;->INSPIRATION_SESSION_ID:LX/ArK;

    aput-object v1, v0, v4

    sget-object v1, LX/ArK;->SEQUENCE_NUMBER:LX/ArK;

    aput-object v1, v0, v5

    sget-object v1, LX/ArK;->NUX_SESSION_ID:LX/ArK;

    aput-object v1, v0, v6

    sget-object v1, LX/ArK;->CAMERA_SESSION_ID:LX/ArK;

    aput-object v1, v0, v7

    sget-object v1, LX/ArK;->EDITING_SESSION_ID:LX/ArK;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ArK;->EFFECTS_TRAY_SESSION_ID:LX/ArK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ArK;->GALLERY_SESSION_ID:LX/ArK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ArK;->DOODLE_SESSION_ID:LX/ArK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ArK;->TEXT_SESSION_ID:LX/ArK;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ArK;->SHARE_SHEET_SESSION_ID:LX/ArK;

    aput-object v2, v0, v1

    sput-object v0, LX/ArK;->$VALUES:[LX/ArK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1718893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1718894
    iput-object p3, p0, LX/ArK;->mName:Ljava/lang/String;

    .line 1718895
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArK;
    .locals 1

    .prologue
    .line 1718890
    const-class v0, LX/ArK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArK;

    return-object v0
.end method

.method public static values()[LX/ArK;
    .locals 1

    .prologue
    .line 1718892
    sget-object v0, LX/ArK;->$VALUES:[LX/ArK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArK;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718891
    iget-object v0, p0, LX/ArK;->mName:Ljava/lang/String;

    return-object v0
.end method
