.class public LX/C6b;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/graphics/Paint;

.field public b:Landroid/widget/RadioGroup;

.field public c:Landroid/widget/FrameLayout;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/content/Context;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public k:Landroid/widget/FrameLayout;

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:LX/0wM;

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:Landroid/widget/RadioGroup$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1850017
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/C6b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1850018
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1850037
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1850038
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/C6b;->a:Landroid/graphics/Paint;

    .line 1850039
    const/4 v0, 0x5

    iput v0, p0, LX/C6b;->q:I

    .line 1850040
    iput-object p1, p0, LX/C6b;->e:Landroid/content/Context;

    .line 1850041
    const/4 p2, -0x1

    .line 1850042
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/C6b;->setOrientation(I)V

    .line 1850043
    const v0, 0x7f030923

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1850044
    const v0, 0x7f0d1764

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/C6b;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1850045
    const v0, 0x7f0d1769

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, LX/C6b;->b:Landroid/widget/RadioGroup;

    .line 1850046
    const v0, 0x7f0d1761

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/C6b;->k:Landroid/widget/FrameLayout;

    .line 1850047
    const v0, 0x7f0d1762

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, LX/C6b;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1850048
    const v0, 0x7f0d1765

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/C6b;->c:Landroid/widget/FrameLayout;

    .line 1850049
    const v0, 0x7f0d1763

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/C6b;->d:Landroid/widget/ImageView;

    .line 1850050
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/C6b;->p:LX/0wM;

    .line 1850051
    iget-object v0, p0, LX/C6b;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/C6b;->p:LX/0wM;

    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f02081c

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const p1, -0x423e37

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1850052
    const v0, 0x7f0d1766

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/C6b;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1850053
    const v0, 0x7f0d1768

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/C6b;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1850054
    const v0, 0x7f0d1768

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/C6b;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1850055
    const v0, 0x7f0d1767

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/C6b;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 1850056
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/C6b;->l:I

    .line 1850057
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/C6b;->m:I

    .line 1850058
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/C6b;->n:I

    .line 1850059
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/C6b;->o:I

    .line 1850060
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/C6b;->r:I

    .line 1850061
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/C6b;->s:I

    .line 1850062
    invoke-virtual {p0}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/C6b;->t:I

    .line 1850063
    new-instance v0, Landroid/widget/RadioGroup$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p2, p2, v1}, Landroid/widget/RadioGroup$LayoutParams;-><init>(IIF)V

    iput-object v0, p0, LX/C6b;->u:Landroid/widget/RadioGroup$LayoutParams;

    .line 1850064
    return-void
.end method

.method public static a(FII)I
    .locals 1

    .prologue
    .line 1850035
    float-to-int v0, p0

    sub-int v0, p1, v0

    .line 1850036
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1850034
    const-string v0, "only_num"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "label_with_num"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 1850025
    iget-object v0, p0, LX/C6b;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1850026
    iget-object v1, p0, LX/C6b;->k:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1850027
    if-eqz p1, :cond_0

    .line 1850028
    iget-object v0, p0, LX/C6b;->p:LX/0wM;

    const v1, 0x7f020737

    const v2, -0xc4a668

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1850029
    iget-object v1, p0, LX/C6b;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1850030
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1850031
    iget-object v0, p0, LX/C6b;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1850032
    :cond_0
    return-void

    .line 1850033
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setAnswerClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 1850019
    iget-object v0, p0, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    .line 1850020
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1850021
    iget-object v0, p0, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    .line 1850022
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1850023
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1850024
    :cond_0
    return-void
.end method
