.class public final LX/B7l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/B7n;


# direct methods
.method public constructor <init>(LX/B7n;)V
    .locals 0

    .prologue
    .line 1748152
    iput-object p1, p0, LX/B7l;->a:LX/B7n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 1748153
    iget-object v0, p0, LX/B7l;->a:LX/B7n;

    invoke-virtual {v0}, LX/B7n;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1748154
    iget-object v1, p0, LX/B7l;->a:LX/B7n;

    invoke-virtual {v1}, LX/B7n;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1748155
    iget-object v0, p0, LX/B7l;->a:LX/B7n;

    iget-object v0, v0, LX/B7n;->b:LX/B7W;

    new-instance v1, LX/B7Z;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1748156
    if-eqz p2, :cond_1

    .line 1748157
    iget-object v0, p0, LX/B7l;->a:LX/B7n;

    invoke-virtual {v0}, LX/B7n;->c()V

    .line 1748158
    :cond_0
    :goto_0
    return-void

    .line 1748159
    :cond_1
    iget-object v0, p0, LX/B7l;->a:LX/B7n;

    iget-boolean v0, v0, LX/B7n;->e:Z

    if-eqz v0, :cond_0

    .line 1748160
    iget-object v0, p0, LX/B7l;->a:LX/B7n;

    iget-object v1, p0, LX/B7l;->a:LX/B7n;

    invoke-virtual {v1}, LX/B7n;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B7n;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
