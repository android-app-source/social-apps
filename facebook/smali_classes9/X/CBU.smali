.class public final LX/CBU;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/2mO;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CBV;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1856431
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1856432
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CBU;->b:[Ljava/lang/String;

    .line 1856433
    iput v3, p0, LX/CBU;->c:I

    .line 1856434
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CBU;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CBU;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CBU;LX/1De;IILX/CBV;)V
    .locals 1

    .prologue
    .line 1856435
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1856436
    iput-object p4, p0, LX/CBU;->a:LX/CBV;

    .line 1856437
    iget-object v0, p0, LX/CBU;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1856438
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CBU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;)",
            "LX/CBU;"
        }
    .end annotation

    .prologue
    .line 1856439
    iget-object v0, p0, LX/CBU;->a:LX/CBV;

    iput-object p1, v0, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1856440
    iget-object v0, p0, LX/CBU;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1856441
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1856442
    invoke-super {p0}, LX/1X5;->a()V

    .line 1856443
    const/4 v0, 0x0

    iput-object v0, p0, LX/CBU;->a:LX/CBV;

    .line 1856444
    sget-object v0, LX/2mO;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1856445
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/2mO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1856446
    iget-object v1, p0, LX/CBU;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CBU;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CBU;->c:I

    if-ge v1, v2, :cond_2

    .line 1856447
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1856448
    :goto_0
    iget v2, p0, LX/CBU;->c:I

    if-ge v0, v2, :cond_1

    .line 1856449
    iget-object v2, p0, LX/CBU;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1856450
    iget-object v2, p0, LX/CBU;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1856451
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1856452
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1856453
    :cond_2
    iget-object v0, p0, LX/CBU;->a:LX/CBV;

    .line 1856454
    invoke-virtual {p0}, LX/CBU;->a()V

    .line 1856455
    return-object v0
.end method
