.class public abstract LX/BcS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1801988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801989
    return-void
.end method

.method public static a()LX/1np;
    .locals 1

    .prologue
    .line 1801987
    new-instance v0, LX/1np;

    invoke-direct {v0}, LX/1np;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)",
            "LX/3lz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1801983
    new-instance v0, LX/3lz;

    invoke-direct {v0}, LX/3lz;-><init>()V

    .line 1801984
    iput-object p1, v0, LX/3lz;->b:Ljava/lang/Object;

    .line 1801985
    iput-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    .line 1801986
    return-object v0
.end method

.method public static a(LX/BcP;)LX/BcQ;
    .locals 2

    .prologue
    .line 1801975
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1801976
    if-nez v0, :cond_0

    .line 1801977
    const/4 v0, 0x0

    .line 1801978
    :goto_0
    return-object v0

    .line 1801979
    :cond_0
    iget-object v1, v0, LX/BcO;->b:LX/BcO;

    move-object v1, v1

    .line 1801980
    if-eqz v1, :cond_1

    iget-object v0, v0, LX/BcO;->a:LX/BcQ;

    goto :goto_0

    .line 1801981
    :cond_1
    iget-object v0, p0, LX/BcP;->e:LX/BcQ;

    move-object v0, v0

    .line 1801982
    goto :goto_0
.end method

.method public static a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/BcP;",
            "I[",
            "Ljava/lang/Object;",
            ")",
            "LX/BcQ",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1801974
    invoke-virtual {p0, p1, p2}, LX/BcP;->b(I[Ljava/lang/Object;)LX/BcQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1801966
    invoke-static {p0}, LX/BcS;->a(LX/BcP;)LX/BcQ;

    move-result-object v0

    .line 1801967
    if-eqz v0, :cond_0

    .line 1801968
    new-instance v1, LX/BcM;

    invoke-direct {v1}, LX/BcM;-><init>()V

    .line 1801969
    iput-boolean p1, v1, LX/BcM;->a:Z

    .line 1801970
    iput-object p2, v1, LX/BcM;->b:LX/BcL;

    .line 1801971
    iput-object p3, v1, LX/BcM;->c:Ljava/lang/Throwable;

    .line 1801972
    invoke-virtual {v0, v1}, LX/BcQ;->a(Ljava/lang/Object;)V

    .line 1801973
    :cond_0
    return-void
.end method

.method public static final c(LX/BcO;LX/BcO;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1801960
    if-eqz p1, :cond_3

    .line 1801961
    iget-boolean v1, p1, LX/BcO;->c:Z

    move v1, v1

    .line 1801962
    or-int/lit8 v1, v1, 0x0

    .line 1801963
    :goto_0
    if-nez v1, :cond_1

    .line 1801964
    if-eq p0, p1, :cond_4

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1801965
    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0

    :cond_3
    move v1, v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(LX/BcO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1801959
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1801958
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/BcO;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801957
    return-void
.end method

.method public a(LX/BcP;IIILX/BcO;)V
    .locals 0

    .prologue
    .line 1801990
    return-void
.end method

.method public a(LX/BcP;LX/BcJ;LX/BcO;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801956
    return-void
.end method

.method public a(LX/BcP;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801955
    return-void
.end method

.method public a(LX/BcP;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1801954
    return-void
.end method

.method public a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;",
            "LX/BcO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1801953
    return-void
.end method

.method public b(LX/BcO;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801952
    return-void
.end method

.method public b(LX/BcP;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801951
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1801950
    const/4 v0, 0x0

    return v0
.end method

.method public c(LX/BcP;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801949
    return-void
.end method

.method public d(LX/BcP;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801948
    return-void
.end method

.method public e(LX/BcP;LX/BcO;)V
    .locals 0

    .prologue
    .line 1801947
    return-void
.end method
