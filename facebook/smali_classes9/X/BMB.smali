.class public final LX/BMB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field public a:LX/BM7;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1776908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776909
    const/4 v0, 0x0

    iput-object v0, p0, LX/BMB;->a:LX/BM7;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776910
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776911
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776912
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776913
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776914
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776915
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->c()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776916
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776917
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776918
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776919
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776920
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776921
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->f()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776922
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776923
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->g()LX/AkM;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776924
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776925
    iget-object v0, p0, LX/BMB;->a:LX/BM7;

    invoke-virtual {v0}, LX/BM7;->h()Lcom/facebook/productionprompts/model/PromptDisplayReason;

    move-result-object v0

    return-object v0
.end method
