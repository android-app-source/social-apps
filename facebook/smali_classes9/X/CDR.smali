.class public final LX/CDR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3It;


# instance fields
.field public final synthetic a:LX/2oL;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/CDU;


# direct methods
.method public constructor <init>(LX/CDU;LX/2oL;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1859272
    iput-object p1, p0, LX/CDR;->d:LX/CDU;

    iput-object p2, p0, LX/CDR;->a:LX/2oL;

    iput-object p3, p0, LX/CDR;->b:LX/1Pq;

    iput-object p4, p0, LX/CDR;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1859271
    return-void
.end method

.method public final a(LX/2op;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1859263
    iget-object v0, p0, LX/CDR;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 1859264
    if-eqz v0, :cond_0

    .line 1859265
    invoke-virtual {v0}, LX/2oO;->h()V

    .line 1859266
    :cond_0
    iget-object v0, p0, LX/CDR;->a:LX/2oL;

    invoke-virtual {v0, v3}, LX/2oL;->a(I)V

    .line 1859267
    iget-object v0, p0, LX/CDR;->a:LX/2oL;

    .line 1859268
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 1859269
    iget-object v0, p0, LX/CDR;->b:LX/1Pq;

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/CDR;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1859270
    return-void
.end method

.method public final a(LX/2oq;)V
    .locals 0

    .prologue
    .line 1859257
    return-void
.end method

.method public final a(LX/2or;)V
    .locals 2

    .prologue
    .line 1859258
    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->ERROR_IO:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->SERVER_DIED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1859259
    :cond_0
    iget-object v0, p0, LX/CDR;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 1859260
    if-eqz v0, :cond_1

    .line 1859261
    invoke-virtual {v0}, LX/2oO;->j()V

    .line 1859262
    :cond_1
    return-void
.end method
