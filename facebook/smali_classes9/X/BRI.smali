.class public final LX/BRI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/BRL;


# direct methods
.method public constructor <init>(LX/BRL;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1783831
    iput-object p1, p0, LX/BRI;->b:LX/BRL;

    iput-object p2, p0, LX/BRI;->a:Landroid/view/View;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1783832
    iget-object v0, p0, LX/BRI;->b:LX/BRL;

    const/4 v1, 0x0

    .line 1783833
    iput-object v1, v0, LX/BRL;->n:LX/1Mv;

    .line 1783834
    iget-object v0, p0, LX/BRI;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/BRI;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1783835
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1783836
    iget-object v0, p0, LX/BRI;->b:LX/BRL;

    iget-object v0, v0, LX/BRL;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_staging_ground"

    const-string v2, "Could not fetch profile pic frames"

    invoke-static {v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1783837
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1783838
    check-cast p1, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783839
    iget-object v0, p0, LX/BRI;->b:LX/BRL;

    const/4 v1, 0x0

    .line 1783840
    iput-object v1, v0, LX/BRL;->n:LX/1Mv;

    .line 1783841
    iget-object v0, p0, LX/BRI;->b:LX/BRL;

    iget-object v0, v0, LX/BRL;->c:LX/BRP;

    .line 1783842
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->g()LX/BRO;

    move-result-object v1

    .line 1783843
    iput-object p1, v1, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783844
    move-object v1, v1

    .line 1783845
    invoke-virtual {v1}, LX/BRO;->a()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object v1

    iput-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783846
    iget-object v0, p0, LX/BRI;->b:LX/BRL;

    iget-object v0, v0, LX/BRL;->c:LX/BRP;

    .line 1783847
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v1

    .line 1783848
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v0

    iget-object v1, p0, LX/BRI;->b:LX/BRL;

    iget-object v1, v1, LX/BRL;->c:LX/BRP;

    .line 1783849
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783850
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v1

    invoke-interface {v1}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, LX/BRL;->b(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1783851
    iget-object v1, p0, LX/BRI;->b:LX/BRL;

    iget-object v2, p0, LX/BRI;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, LX/BRI;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1783852
    iget-object v4, v1, LX/BRL;->l:LX/9d5;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string p0, "Called appendFrames before setupSwipeableFrames"

    invoke-static {v4, p0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1783853
    new-instance v4, LX/BRJ;

    invoke-direct {v4, v1, v2, v3}, LX/BRJ;-><init>(LX/BRL;II)V

    .line 1783854
    iget-object p0, v1, LX/BRL;->l:LX/9d5;

    const/4 p1, 0x0

    invoke-virtual {p0, v0, v4, p1}, LX/9d5;->b(LX/0Px;LX/8G6;Ljava/lang/String;)V

    .line 1783855
    return-void

    .line 1783856
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
