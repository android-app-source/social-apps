.class public LX/C6j;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C6h;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C6k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1850183
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C6j;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C6k;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850184
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850185
    iput-object p1, p0, LX/C6j;->b:LX/0Ot;

    .line 1850186
    return-void
.end method

.method public static a(LX/0QB;)LX/C6j;
    .locals 4

    .prologue
    .line 1850187
    const-class v1, LX/C6j;

    monitor-enter v1

    .line 1850188
    :try_start_0
    sget-object v0, LX/C6j;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850189
    sput-object v2, LX/C6j;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850190
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850191
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850192
    new-instance v3, LX/C6j;

    const/16 p0, 0x1f7a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C6j;-><init>(LX/0Ot;)V

    .line 1850193
    move-object v0, v3

    .line 1850194
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850195
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850196
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1850198
    check-cast p2, LX/C6i;

    .line 1850199
    iget-object v0, p0, LX/C6j;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/C6i;->a:LX/C6H;

    .line 1850200
    iget-boolean v1, v0, LX/C6H;->a:Z

    move v1, v1

    .line 1850201
    if-nez v1, :cond_0

    .line 1850202
    const/4 v1, 0x0

    .line 1850203
    :goto_0
    move-object v0, v1

    .line 1850204
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const p0, 0x7f0a011a

    invoke-virtual {v2, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const p0, 0x7f081a55

    invoke-virtual {p1, p0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00a4

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a0115

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-interface {v2, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v2

    const/16 p0, 0x8

    const p2, 0x7f0b10b0

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1850205
    invoke-static {}, LX/1dS;->b()V

    .line 1850206
    const/4 v0, 0x0

    return-object v0
.end method
