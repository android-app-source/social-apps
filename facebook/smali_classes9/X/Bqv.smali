.class public final LX/Bqv;
.super LX/8wT;
.source ""


# instance fields
.field public final synthetic a:LX/Bqw;

.field private final b:LX/Br2;

.field private final c:LX/1Pq;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(LX/Bqw;ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/Br2;",
            "LX/1Pq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1825478
    iput-object p1, p0, LX/Bqv;->a:LX/Bqw;

    invoke-direct {p0}, LX/8wT;-><init>()V

    .line 1825479
    iput-object p3, p0, LX/Bqv;->b:LX/Br2;

    .line 1825480
    iput-object p4, p0, LX/Bqv;->c:LX/1Pq;

    .line 1825481
    iput-object p5, p0, LX/Bqv;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825482
    iput-boolean p2, p0, LX/Bqv;->e:Z

    .line 1825483
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1825484
    check-cast p1, LX/8wS;

    .line 1825485
    iget-object v0, p0, LX/Bqv;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825486
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1825487
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825488
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1825489
    iget-object v2, p1, LX/8wS;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1825490
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1825491
    iget-object v1, p0, LX/Bqv;->b:LX/Br2;

    iget-object v2, p0, LX/Bqv;->a:LX/Bqw;

    .line 1825492
    iget-object v3, p1, LX/8wS;->b:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-object v3, v3

    .line 1825493
    iget-boolean v4, p0, LX/Bqv;->e:Z

    invoke-virtual {v2, v0, v3, v4}, LX/Bqw;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;Z)I

    move-result v0

    invoke-virtual {v1, v0}, LX/Br2;->a(I)V

    .line 1825494
    iget-object v0, p0, LX/Bqv;->c:LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Bqv;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1825495
    :cond_0
    return-void
.end method
