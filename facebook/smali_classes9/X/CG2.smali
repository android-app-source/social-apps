.class public LX/CG2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/8YL;


# instance fields
.field public final c:LX/1wz;

.field public final d:LX/8YK;

.field private final e:Landroid/view/View;

.field public f:LX/CGF;

.field public g:LX/31M;

.field public h:LX/CG1;

.field public i:I

.field public j:I

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1864842
    const-class v0, LX/CG2;

    sput-object v0, LX/CG2;->a:Ljava/lang/Class;

    .line 1864843
    new-instance v0, LX/8YL;

    const-wide v2, 0x4066800000000000L    # 180.0

    const-wide/high16 v4, 0x403b000000000000L    # 27.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/8YL;-><init>(DD)V

    sput-object v0, LX/CG2;->b:LX/8YL;

    return-void
.end method

.method public constructor <init>(LX/1wz;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1864825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864826
    sget-object v0, LX/CG1;->IDLE:LX/CG1;

    iput-object v0, p0, LX/CG2;->h:LX/CG1;

    .line 1864827
    iput-object p1, p0, LX/CG2;->c:LX/1wz;

    .line 1864828
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    .line 1864829
    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    sget-object v1, LX/CG2;->b:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1864830
    iput-wide v2, v0, LX/8YK;->l:D

    .line 1864831
    move-object v0, v0

    .line 1864832
    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    .line 1864833
    iput-wide v2, v0, LX/8YK;->k:D

    .line 1864834
    move-object v0, v0

    .line 1864835
    new-instance v1, LX/CFx;

    invoke-direct {v1, p0}, LX/CFx;-><init>(LX/CG2;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    move-result-object v0

    iput-object v0, p0, LX/CG2;->d:LX/8YK;

    .line 1864836
    iput-object p2, p0, LX/CG2;->e:Landroid/view/View;

    .line 1864837
    iget-object v0, p0, LX/CG2;->c:LX/1wz;

    new-instance v1, LX/CFy;

    invoke-direct {v1, p0}, LX/CFy;-><init>(LX/CG2;)V

    .line 1864838
    iput-object v1, v0, LX/1wz;->q:LX/4Ba;

    .line 1864839
    iget-object v0, p0, LX/CG2;->c:LX/1wz;

    new-instance v1, LX/CFz;

    invoke-direct {v1, p0}, LX/CFz;-><init>(LX/CG2;)V

    .line 1864840
    iput-object v1, v0, LX/1wz;->r:LX/39D;

    .line 1864841
    return-void
.end method

.method public static a(LX/CG2;D)Z
    .locals 5

    .prologue
    .line 1864823
    invoke-static {p0, p1, p2}, LX/CG2;->c(LX/CG2;D)D

    move-result-wide v0

    .line 1864824
    const-wide v2, -0x4056666666666666L    # -0.05

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    const-wide v2, 0x3ff0cccccccccccdL    # 1.05

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/CG2;Z)V
    .locals 4

    .prologue
    .line 1864814
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1864815
    iget-object v0, p0, LX/CG2;->f:LX/CGF;

    invoke-virtual {v0, p1}, LX/CGF;->a(Z)V

    .line 1864816
    if-eqz p1, :cond_0

    .line 1864817
    iget v0, p0, LX/CG2;->j:I

    iput v0, p0, LX/CG2;->i:I

    .line 1864818
    :cond_0
    iget-object v0, p0, LX/CG2;->c:LX/1wz;

    iget v1, p0, LX/CG2;->i:I

    .line 1864819
    iput v1, v0, LX/1wz;->p:I

    .line 1864820
    sget-object v0, LX/CG1;->IDLE:LX/CG1;

    iput-object v0, p0, LX/CG2;->h:LX/CG1;

    .line 1864821
    iget-object v0, p0, LX/CG2;->d:LX/8YK;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    move-result-object v0

    invoke-virtual {v0}, LX/8YK;->f()LX/8YK;

    .line 1864822
    return-void
.end method

.method public static b$redex0(LX/CG2;D)D
    .locals 3

    .prologue
    .line 1864811
    iget-object v0, p0, LX/CG2;->g:LX/31M;

    sget-object v1, LX/31M;->LEFT:LX/31M;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/CG2;->g:LX/31M;

    sget-object v1, LX/31M;->UP:LX/31M;

    if-ne v0, v1, :cond_1

    .line 1864812
    :cond_0
    neg-double p1, p1

    .line 1864813
    :cond_1
    invoke-direct {p0}, LX/CG2;->e()J

    move-result-wide v0

    long-to-double v0, v0

    mul-double/2addr v0, p1

    return-wide v0
.end method

.method public static c(LX/CG2;D)D
    .locals 3

    .prologue
    .line 1864787
    iget-object v0, p0, LX/CG2;->g:LX/31M;

    sget-object v1, LX/31M;->LEFT:LX/31M;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/CG2;->g:LX/31M;

    sget-object v1, LX/31M;->UP:LX/31M;

    if-ne v0, v1, :cond_1

    .line 1864788
    :cond_0
    neg-double p1, p1

    .line 1864789
    :cond_1
    invoke-direct {p0}, LX/CG2;->e()J

    move-result-wide v0

    long-to-double v0, v0

    div-double v0, p1, v0

    return-wide v0
.end method

.method public static c(LX/CG2;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1864805
    iget-object v0, p0, LX/CG2;->h:LX/CG1;

    sget-object v3, LX/CG1;->DRAGGING:LX/CG1;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1864806
    sget-object v0, LX/CG1;->SPRINGING:LX/CG1;

    iput-object v0, p0, LX/CG2;->h:LX/CG1;

    .line 1864807
    iput-boolean v2, p0, LX/CG2;->k:Z

    .line 1864808
    iget-object v0, p0, LX/CG2;->c:LX/1wz;

    new-array v1, v1, [LX/31M;

    iget-object v3, p0, LX/CG2;->g:LX/31M;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/1wz;->a([LX/31M;)V

    .line 1864809
    return-void

    :cond_0
    move v0, v2

    .line 1864810
    goto :goto_0
.end method

.method public static c(LX/CG2;LX/31M;)V
    .locals 2

    .prologue
    .line 1864798
    iget-object v0, p0, LX/CG2;->h:LX/CG1;

    sget-object v1, LX/CG1;->IDLE:LX/CG1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1864799
    invoke-virtual {p1}, LX/31M;->name()Ljava/lang/String;

    .line 1864800
    iget-object v0, p0, LX/CG2;->f:LX/CGF;

    invoke-virtual {v0, p1}, LX/CGF;->a(LX/31M;)I

    move-result v0

    iput v0, p0, LX/CG2;->j:I

    .line 1864801
    iput-object p1, p0, LX/CG2;->g:LX/31M;

    .line 1864802
    sget-object v0, LX/CG1;->DRAGGING:LX/CG1;

    iput-object v0, p0, LX/CG2;->h:LX/CG1;

    .line 1864803
    return-void

    .line 1864804
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/CG2;)D
    .locals 2

    .prologue
    .line 1864797
    iget-object v0, p0, LX/CG2;->d:LX/8YK;

    invoke-virtual {v0}, LX/8YK;->b()D

    move-result-wide v0

    invoke-static {p0, v0, v1}, LX/CG2;->c(LX/CG2;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static d(LX/31M;)LX/31M;
    .locals 2

    .prologue
    .line 1864791
    sget-object v0, LX/CG0;->a:[I

    invoke-virtual {p0}, LX/31M;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1864792
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1864793
    :pswitch_0
    sget-object v0, LX/31M;->DOWN:LX/31M;

    goto :goto_0

    .line 1864794
    :pswitch_1
    sget-object v0, LX/31M;->UP:LX/31M;

    goto :goto_0

    .line 1864795
    :pswitch_2
    sget-object v0, LX/31M;->RIGHT:LX/31M;

    goto :goto_0

    .line 1864796
    :pswitch_3
    sget-object v0, LX/31M;->LEFT:LX/31M;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private e()J
    .locals 2

    .prologue
    .line 1864790
    iget-object v0, p0, LX/CG2;->g:LX/31M;

    invoke-virtual {v0}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CG2;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/CG2;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method
