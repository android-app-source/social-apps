.class public LX/Be5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/Be5;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field public final c:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1804543
    const-class v0, LX/Be5;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Be5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1804576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804577
    iput-object p1, p0, LX/Be5;->b:Landroid/content/ContentResolver;

    .line 1804578
    iput-object p2, p0, LX/Be5;->c:LX/0Zb;

    .line 1804579
    return-void
.end method

.method private static a(Landroid/os/ParcelFileDescriptor;)I
    .locals 2

    .prologue
    .line 1804571
    new-instance v0, Lcom/facebook/common/dextricks/DalvikInternals$Stat;

    invoke-direct {v0}, Lcom/facebook/common/dextricks/DalvikInternals$Stat;-><init>()V

    .line 1804572
    :try_start_0
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v1

    invoke-static {v1, v0}, Lcom/facebook/common/dextricks/DalvikInternals;->statOpenFile(ILcom/facebook/common/dextricks/DalvikInternals$Stat;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1804573
    iget v0, v0, Lcom/facebook/common/dextricks/DalvikInternals$Stat;->ownerUid:I

    return v0

    .line 1804574
    :catch_0
    move-exception v0

    .line 1804575
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(LX/0QB;)LX/Be5;
    .locals 5

    .prologue
    .line 1804558
    sget-object v0, LX/Be5;->d:LX/Be5;

    if-nez v0, :cond_1

    .line 1804559
    const-class v1, LX/Be5;

    monitor-enter v1

    .line 1804560
    :try_start_0
    sget-object v0, LX/Be5;->d:LX/Be5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1804561
    if-eqz v2, :cond_0

    .line 1804562
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1804563
    new-instance p0, LX/Be5;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/Be5;-><init>(Landroid/content/ContentResolver;LX/0Zb;)V

    .line 1804564
    move-object v0, p0

    .line 1804565
    sput-object v0, LX/Be5;->d:LX/Be5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1804566
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1804567
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1804568
    :cond_1
    sget-object v0, LX/Be5;->d:LX/Be5;

    return-object v0

    .line 1804569
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1804570
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 1804544
    invoke-static {p1}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1804545
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a file uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804546
    :cond_0
    iget-object v0, p0, LX/Be5;->b:Landroid/content/ContentResolver;

    const-string v1, "r"

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 1804547
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-static {v0}, LX/Be5;->a(Landroid/os/ParcelFileDescriptor;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1804548
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "security_error"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1804549
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1804550
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    .line 1804551
    const-string v3, "canonicalPath"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1804552
    :goto_0
    iget-object v2, p0, LX/Be5;->c:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1804553
    :try_start_1
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1804554
    :goto_1
    new-instance v0, LX/Be4;

    invoke-direct {v0, p1}, LX/Be4;-><init>(Landroid/net/Uri;)V

    throw v0

    .line 1804555
    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    goto :goto_1

    .line 1804556
    :cond_1
    return-object v0

    .line 1804557
    :catch_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    goto :goto_0
.end method
