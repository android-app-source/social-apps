.class public LX/BSb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/BSb;


# instance fields
.field private final b:LX/09G;

.field public final c:LX/0lC;

.field public final d:LX/2ml;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1785589
    const-class v0, LX/BSb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BSb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/09G;LX/0lC;LX/2ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1785584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785585
    iput-object p1, p0, LX/BSb;->b:LX/09G;

    .line 1785586
    iput-object p2, p0, LX/BSb;->c:LX/0lC;

    .line 1785587
    iput-object p3, p0, LX/BSb;->d:LX/2ml;

    .line 1785588
    return-void
.end method

.method public static a(LX/0QB;)LX/BSb;
    .locals 6

    .prologue
    .line 1785571
    sget-object v0, LX/BSb;->f:LX/BSb;

    if-nez v0, :cond_1

    .line 1785572
    const-class v1, LX/BSb;

    monitor-enter v1

    .line 1785573
    :try_start_0
    sget-object v0, LX/BSb;->f:LX/BSb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1785574
    if-eqz v2, :cond_0

    .line 1785575
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1785576
    new-instance p0, LX/BSb;

    invoke-static {v0}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v3

    check-cast v3, LX/09G;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/2ml;->a(LX/0QB;)LX/2ml;

    move-result-object v5

    check-cast v5, LX/2ml;

    invoke-direct {p0, v3, v4, v5}, LX/BSb;-><init>(LX/09G;LX/0lC;LX/2ml;)V

    .line 1785577
    move-object v0, p0

    .line 1785578
    sput-object v0, LX/BSb;->f:LX/BSb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1785579
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1785580
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1785581
    :cond_1
    sget-object v0, LX/BSb;->f:LX/BSb;

    return-object v0

    .line 1785582
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1785583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BSb;Ljava/lang/String;LX/0m9;)V
    .locals 1

    .prologue
    .line 1785567
    iget-object v0, p0, LX/BSb;->b:LX/09G;

    invoke-virtual {v0, p1, p2}, LX/09G;->a(Ljava/lang/String;LX/0lF;)V

    .line 1785568
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1785569
    iput-object p1, p0, LX/BSb;->e:Ljava/lang/String;

    .line 1785570
    return-void
.end method
