.class public LX/Bpc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bpa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bpd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1823387
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bpc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bpd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823360
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1823361
    iput-object p1, p0, LX/Bpc;->b:LX/0Ot;

    .line 1823362
    return-void
.end method

.method public static a(LX/0QB;)LX/Bpc;
    .locals 4

    .prologue
    .line 1823373
    const-class v1, LX/Bpc;

    monitor-enter v1

    .line 1823374
    :try_start_0
    sget-object v0, LX/Bpc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823375
    sput-object v2, LX/Bpc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823376
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823377
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823378
    new-instance v3, LX/Bpc;

    const/16 p0, 0x1ced

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bpc;-><init>(LX/0Ot;)V

    .line 1823379
    move-object v0, v3

    .line 1823380
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823381
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bpc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823382
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1823384
    check-cast p2, LX/Bpb;

    .line 1823385
    iget-object v0, p0, LX/Bpc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bpd;

    iget-object v1, p2, LX/Bpb;->a:LX/1X1;

    iget-object v2, p2, LX/Bpb;->b:LX/Bpe;

    iget-object v3, p2, LX/Bpb;->c:LX/1Ps;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/Bpd;->a(LX/1De;LX/1X1;LX/Bpe;LX/1Ps;)LX/1Dg;

    move-result-object v0

    .line 1823386
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1823371
    invoke-static {}, LX/1dS;->b()V

    .line 1823372
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Bpa;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1823363
    new-instance v1, LX/Bpb;

    invoke-direct {v1, p0}, LX/Bpb;-><init>(LX/Bpc;)V

    .line 1823364
    sget-object v2, LX/Bpc;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bpa;

    .line 1823365
    if-nez v2, :cond_0

    .line 1823366
    new-instance v2, LX/Bpa;

    invoke-direct {v2}, LX/Bpa;-><init>()V

    .line 1823367
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Bpa;->a$redex0(LX/Bpa;LX/1De;IILX/Bpb;)V

    .line 1823368
    move-object v1, v2

    .line 1823369
    move-object v0, v1

    .line 1823370
    return-object v0
.end method
