.class public LX/BZP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BZQ;

.field public final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/BZQ;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1797248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1797249
    iput-object p1, p0, LX/BZP;->a:LX/BZQ;

    .line 1797250
    iput-object p2, p0, LX/BZP;->b:Ljava/util/concurrent/Executor;

    .line 1797251
    return-void
.end method

.method public static b(LX/BZP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1797252
    const-string v0, "ModelLoader:loadFromStorage"

    invoke-static {v0}, LX/7ey;->a(Ljava/lang/String;)V

    .line 1797253
    :try_start_0
    iget-object v0, p0, LX/BZP;->a:LX/BZQ;

    if-eqz v0, :cond_0

    .line 1797254
    iget-object v0, p0, LX/BZP;->a:LX/BZQ;

    invoke-virtual {v0, p1, p2, p3}, LX/BZQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1797255
    :cond_0
    invoke-static {}, LX/7ey;->a()V

    .line 1797256
    return-void

    .line 1797257
    :catchall_0
    move-exception v0

    invoke-static {}, LX/7ey;->a()V

    throw v0
.end method
