.class public LX/CAa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D2D;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/CAn;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/1Vm;LX/0Ot;LX/CAn;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1Vm;",
            "LX/0Ot",
            "<",
            "LX/D2D;",
            ">;",
            "LX/CAn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855523
    iput-object p1, p0, LX/CAa;->a:LX/0Or;

    .line 1855524
    iput-object p2, p0, LX/CAa;->b:LX/0Ot;

    .line 1855525
    iput-object p4, p0, LX/CAa;->e:LX/1Vm;

    .line 1855526
    iput-object p3, p0, LX/CAa;->c:LX/0Ot;

    .line 1855527
    iput-object p5, p0, LX/CAa;->d:LX/0Ot;

    .line 1855528
    iput-object p6, p0, LX/CAa;->f:LX/CAn;

    .line 1855529
    return-void
.end method

.method public static a(LX/0QB;)LX/CAa;
    .locals 10

    .prologue
    .line 1855530
    const-class v1, LX/CAa;

    monitor-enter v1

    .line 1855531
    :try_start_0
    sget-object v0, LX/CAa;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855532
    sput-object v2, LX/CAa;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855533
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855534
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855535
    new-instance v3, LX/CAa;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xc49

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v7

    check-cast v7, LX/1Vm;

    const/16 v8, 0x36b2

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/CAn;->a(LX/0QB;)LX/CAn;

    move-result-object v9

    check-cast v9, LX/CAn;

    invoke-direct/range {v3 .. v9}, LX/CAa;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/1Vm;LX/0Ot;LX/CAn;)V

    .line 1855536
    move-object v0, v3

    .line 1855537
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855538
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855539
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
