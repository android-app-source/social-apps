.class public final LX/CUE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/CU1;

.field public final b:LX/CU1;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1896401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896402
    const v0, 0x41f00cd4    # 30.006264f

    invoke-static {p1, p2, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1896403
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_0

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896404
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v3, LX/CU1;

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->a()LX/0Px;

    move-result-object v6

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->c()LX/0Px;

    move-result-object v7

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v0

    invoke-direct {v3, v5, v6, v7, v0}, LX/CU1;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;)V

    move-object v0, v3

    :goto_1
    iput-object v0, p0, LX/CUE;->a:LX/CU1;

    .line 1896405
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v4, LX/CU1;

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->d()Ljava/lang/String;

    move-result-object v2

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->a()LX/0Px;

    move-result-object v3

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->c()LX/0Px;

    move-result-object v5

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v0

    invoke-direct {v4, v2, v3, v5, v0}, LX/CU1;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;)V

    :cond_1
    iput-object v4, p0, LX/CUE;->b:LX/CU1;

    .line 1896406
    return-void

    :cond_2
    move v0, v1

    .line 1896407
    goto/16 :goto_0

    :cond_3
    move-object v0, v4

    .line 1896408
    goto :goto_1
.end method
