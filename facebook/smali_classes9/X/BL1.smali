.class public final enum LX/BL1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BL1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BL1;

.field public static final enum IMPORTANCE:LX/BL1;


# instance fields
.field private final mOrderList:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1775123
    new-instance v0, LX/BL1;

    const-string v1, "IMPORTANCE"

    const-string v2, "importance"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, LX/BL1;-><init>(Ljava/lang/String;ILX/0Px;)V

    sput-object v0, LX/BL1;->IMPORTANCE:LX/BL1;

    .line 1775124
    const/4 v0, 0x1

    new-array v0, v0, [LX/BL1;

    sget-object v1, LX/BL1;->IMPORTANCE:LX/BL1;

    aput-object v1, v0, v3

    sput-object v0, LX/BL1;->$VALUES:[LX/BL1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1775128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1775129
    iput-object p3, p0, LX/BL1;->mOrderList:LX/0Px;

    .line 1775130
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BL1;
    .locals 1

    .prologue
    .line 1775127
    const-class v0, LX/BL1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BL1;

    return-object v0
.end method

.method public static values()[LX/BL1;
    .locals 1

    .prologue
    .line 1775126
    sget-object v0, LX/BL1;->$VALUES:[LX/BL1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BL1;

    return-object v0
.end method


# virtual methods
.method public final getOrderList()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1775125
    iget-object v0, p0, LX/BL1;->mOrderList:LX/0Px;

    return-object v0
.end method
