.class public final LX/C55;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1848306
    iput-object p1, p0, LX/C55;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    iput-object p2, p0, LX/C55;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/C55;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x595aff26

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1848307
    iget-object v0, p0, LX/C55;->c:Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Landroid/view/View;)LX/AnD;

    move-result-object v3

    .line 1848308
    if-nez v3, :cond_0

    .line 1848309
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to find HScrollChainingView."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const v1, -0x513d20b2

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0

    .line 1848310
    :cond_0
    iget-object v0, p0, LX/C55;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    .line 1848311
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;)LX/0Px;

    move-result-object v1

    .line 1848312
    iget-object v4, v3, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    move-object v4, v4

    .line 1848313
    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1848314
    iget-object v4, p0, LX/C55;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1848315
    iget-object v0, p0, LX/C55;->b:LX/1Pq;

    check-cast v0, LX/1Pk;

    invoke-interface {v0}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1848316
    const v0, -0x16f7ebd8

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void
.end method
