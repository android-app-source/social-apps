.class public final LX/AV7;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Float;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/facecast/FacecastLocalVideoSaveController$FileSaveListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/io/File;

.field private final c:Ljava/io/File;

.field private d:Ljava/io/File;


# direct methods
.method public constructor <init>(LX/AXF;Ljava/io/File;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 1678932
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1678933
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AV7;->a:Ljava/lang/ref/WeakReference;

    .line 1678934
    iput-object p2, p0, LX/AV7;->c:Ljava/io/File;

    .line 1678935
    iput-object p3, p0, LX/AV7;->b:Ljava/io/File;

    .line 1678936
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1678905
    const/4 v1, 0x0

    .line 1678906
    iget-object v0, p0, LX/AV7;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AXF;

    .line 1678907
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1678908
    :goto_0
    return-object v0

    .line 1678909
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/AV7;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1678910
    const-wide/16 v4, 0x0

    .line 1678911
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/AV7;->c:Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1678912
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/AV7;->d:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 1678913
    const/16 v0, 0x400

    :try_start_2
    new-array v8, v0, [B

    move-wide v0, v4

    .line 1678914
    :goto_1
    invoke-virtual {v3, v8}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 1678915
    const/4 v5, 0x0

    invoke-virtual {v2, v8, v5, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 1678916
    int-to-long v4, v4

    add-long/2addr v0, v4

    .line 1678917
    long-to-double v4, v0

    long-to-double v10, v6

    div-double/2addr v4, v10

    .line 1678918
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Float;

    const/4 v10, 0x0

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-virtual {p0, v9}, LX/AV7;->publishProgress([Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1678919
    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    .line 1678920
    :goto_2
    sget-object v3, LX/AV8;->a:Ljava/lang/String;

    const-string v4, "Error while saving local file "

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1678921
    if-eqz v2, :cond_1

    .line 1678922
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 1678923
    :cond_1
    if-eqz v1, :cond_2

    .line 1678924
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1678925
    :cond_2
    :goto_3
    iget-object v0, p0, LX/AV7;->d:Ljava/io/File;

    goto :goto_0

    .line 1678926
    :cond_3
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 1678927
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 1678928
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 1678929
    :catch_1
    move-exception v0

    .line 1678930
    sget-object v1, LX/AV8;->a:Ljava/lang/String;

    const-string v2, "Error while closing the stream "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1678931
    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1678894
    check-cast p1, Ljava/io/File;

    .line 1678895
    iget-object v0, p0, LX/AV7;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AXF;

    .line 1678896
    if-eqz v0, :cond_0

    .line 1678897
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    .line 1678898
    new-instance v2, Landroid/content/Intent;

    const-string p0, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1678899
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1678900
    invoke-virtual {v1}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1678901
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    const v2, 0x7f080cb9

    invoke-static {v1, v2}, LX/AXY;->c(LX/AXY;I)V

    .line 1678902
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    if-nez v1, :cond_1

    .line 1678903
    :cond_0
    :goto_0
    return-void

    .line 1678904
    :cond_1
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    sget-object v2, LX/Abm;->SAVING_COMPLETED:LX/Abm;

    invoke-virtual {v1, v2}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a(LX/Abm;)V

    goto :goto_0
.end method

.method public final onPreExecute()V
    .locals 6

    .prologue
    .line 1678876
    iget-object v0, p0, LX/AV7;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AXF;

    .line 1678877
    if-nez v0, :cond_0

    .line 1678878
    :goto_0
    return-void

    .line 1678879
    :cond_0
    iget-object v1, p0, LX/AV7;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1678880
    iget-object v1, p0, LX/AV7;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1678881
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/AV7;->b:Ljava/io/File;

    .line 1678882
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FB_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/AV8;->b:Ljava/text/SimpleDateFormat;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".mp4"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1678883
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, LX/AV7;->d:Ljava/io/File;

    .line 1678884
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    if-nez v1, :cond_2

    .line 1678885
    :goto_1
    goto :goto_0

    .line 1678886
    :cond_2
    iget-object v1, v0, LX/AXF;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    sget-object v2, LX/Abm;->SAVING_IN_PROGRESS:LX/Abm;

    invoke-virtual {v1, v2}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a(LX/Abm;)V

    goto :goto_1
.end method

.method public final onProgressUpdate([Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1678887
    check-cast p1, [Ljava/lang/Float;

    .line 1678888
    iget-object v0, p0, LX/AV7;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AXF;

    .line 1678889
    if-eqz v0, :cond_0

    .line 1678890
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1678891
    iget-object p0, v0, LX/AXF;->a:LX/AXY;

    iget-object p0, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    if-nez p0, :cond_1

    .line 1678892
    :cond_0
    :goto_0
    return-void

    .line 1678893
    :cond_1
    iget-object p0, v0, LX/AXF;->a:LX/AXY;

    iget-object p0, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    invoke-virtual {p0, v1}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->setProgress(F)V

    goto :goto_0
.end method
