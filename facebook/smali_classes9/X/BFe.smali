.class public final LX/BFe;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final synthetic b:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 1766127
    iput-object p1, p0, LX/BFe;->b:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    iput-object p2, p0, LX/BFe;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1766125
    sget-object v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->a:Ljava/lang/Class;

    const-string v1, "failure getting picker button photo preview"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1766126
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1766120
    check-cast p1, Landroid/net/Uri;

    .line 1766121
    if-nez p1, :cond_0

    .line 1766122
    iget-object v0, p0, LX/BFe;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1766123
    :cond_0
    iget-object v0, p0, LX/BFe;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1766124
    return-void
.end method
