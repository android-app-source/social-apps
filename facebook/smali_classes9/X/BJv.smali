.class public final LX/BJv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772483
    iput-object p1, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x7f7ab410

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1772484
    iget-object v2, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1772485
    :goto_0
    iget-object v2, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E:LX/926;

    iget-object v3, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v3, v3, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v3

    iget-object v4, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v4, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v4, v4, LX/BKm;->a:Ljava/lang/String;

    iget-object v5, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v5, v5, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6, v0}, LX/938;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)LX/937;

    move-result-object v0

    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    iget-object v3, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/926;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 1772486
    iget-object v0, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xa

    iget-object v4, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-interface {v0, v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1772487
    iget-object v0, p0, LX/BJv;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "minutiae_button"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772488
    const v0, 0x77e5ac2e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1772489
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
