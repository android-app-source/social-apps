.class public final LX/BJA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BJG;


# direct methods
.method public constructor <init>(LX/BJG;)V
    .locals 0

    .prologue
    .line 1771661
    iput-object p1, p0, LX/BJA;->a:LX/BJG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x22d03e3b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1771662
    iget-object v1, p0, LX/BJA;->a:LX/BJG;

    const/16 p1, 0x3e8

    .line 1771663
    iget-object v3, v1, LX/BJG;->c:LX/9at;

    sget-object v4, LX/9au;->COMPOSER:LX/9au;

    iget-object v5, v1, LX/BJG;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object p0, v1, LX/BJG;->j:LX/BK5;

    invoke-virtual {p0}, LX/BK5;->b()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object p0

    invoke-virtual {v3, v4, v5, p0}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;

    move-result-object v4

    .line 1771664
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    if-eqz v3, :cond_0

    .line 1771665
    iget-object v3, v1, LX/BJG;->m:LX/0ht;

    invoke-virtual {v3}, LX/0ht;->l()V

    .line 1771666
    :cond_0
    iget-object v3, v1, LX/BJG;->k:Landroid/support/v4/app/Fragment;

    if-eqz v3, :cond_2

    .line 1771667
    iget-object v3, v1, LX/BJG;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v1, LX/BJG;->k:Landroid/support/v4/app/Fragment;

    invoke-interface {v3, v4, p1, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1771668
    :cond_1
    :goto_0
    const v1, 0x289e1bda

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1771669
    :cond_2
    iget-object v3, v1, LX/BJG;->e:Landroid/content/Context;

    instance-of v3, v3, Landroid/app/Activity;

    if-eqz v3, :cond_1

    .line 1771670
    iget-object v5, v1, LX/BJG;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v1, LX/BJG;->e:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-interface {v5, v4, p1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
