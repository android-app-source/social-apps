.class public LX/Ass;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;

.field public static final b:Ljava/lang/String;


# instance fields
.field public A:LX/87O;

.field public B:Z

.field private final c:Landroid/content/Context;

.field public final d:LX/Aw7;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final f:LX/Ar2;

.field public final g:LX/03V;

.field private final h:F

.field public i:LX/Ata;

.field public final j:LX/0SG;

.field private final k:LX/0hs;

.field public final l:LX/0fO;

.field public final m:Landroid/view/View;

.field public final n:Landroid/view/View;

.field public final o:Landroid/view/View;

.field private final p:Landroid/view/View;

.field public final q:Lcom/facebook/fbui/glyph/GlyphView;

.field private final r:LX/0wW;

.field private final s:LX/0wd;

.field private final t:LX/0wd;

.field public final u:LX/0wd;

.field private final v:Landroid/view/View$OnClickListener;

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:Landroid/view/View$OnLongClickListener;

.field private final y:Landroid/view/View$OnTouchListener;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1720974
    const-class v0, LX/Ass;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Ass;->a:LX/0jK;

    .line 1720975
    const-class v0, LX/Ass;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ass;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/Ar2;Landroid/view/ViewStub;LX/Ata;LX/0wW;Landroid/content/Context;LX/Aw7;LX/03V;LX/0SG;LX/0fO;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Ar2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Ata;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationCaptureButtonController$Delegate;",
            "Landroid/view/ViewStub;",
            "LX/Ata;",
            "LX/0wW;",
            "Landroid/content/Context;",
            "LX/Aw7;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0fO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1720997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720998
    sget-object v0, LX/87O;->CAPTURE:LX/87O;

    iput-object v0, p0, LX/Ass;->A:LX/87O;

    .line 1720999
    iput-boolean v3, p0, LX/Ass;->B:Z

    .line 1721000
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    .line 1721001
    iput-object p2, p0, LX/Ass;->f:LX/Ar2;

    .line 1721002
    iput-object p8, p0, LX/Ass;->g:LX/03V;

    .line 1721003
    iput-object p6, p0, LX/Ass;->c:Landroid/content/Context;

    .line 1721004
    iput-object p7, p0, LX/Ass;->d:LX/Aw7;

    .line 1721005
    iput-object p4, p0, LX/Ass;->i:LX/Ata;

    .line 1721006
    iput-object p9, p0, LX/Ass;->j:LX/0SG;

    .line 1721007
    iput-object p10, p0, LX/Ass;->l:LX/0fO;

    .line 1721008
    const v0, 0x7f03093b

    invoke-virtual {p3, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1721009
    invoke-virtual {p3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->m:Landroid/view/View;

    .line 1721010
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const v1, 0x7f0d17b1

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->n:Landroid/view/View;

    .line 1721011
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const v1, 0x7f0d0686

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->o:Landroid/view/View;

    .line 1721012
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const v1, 0x7f0d17b2

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->p:Landroid/view/View;

    .line 1721013
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const v1, 0x7f0d17b3

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1721014
    iput-object p5, p0, LX/Ass;->r:LX/0wW;

    .line 1721015
    invoke-direct {p0}, LX/Ass;->h()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->s:LX/0wd;

    .line 1721016
    invoke-direct {p0}, LX/Ass;->i()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->t:LX/0wd;

    .line 1721017
    invoke-direct {p0}, LX/Ass;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Ass;->u:LX/0wd;

    .line 1721018
    new-instance v0, LX/Asm;

    invoke-direct {v0, p0}, LX/Asm;-><init>(LX/Ass;)V

    move-object v0, v0

    .line 1721019
    iput-object v0, p0, LX/Ass;->v:Landroid/view/View$OnClickListener;

    .line 1721020
    new-instance v0, LX/Asn;

    invoke-direct {v0, p0}, LX/Asn;-><init>(LX/Ass;)V

    move-object v0, v0

    .line 1721021
    iput-object v0, p0, LX/Ass;->w:Landroid/view/View$OnClickListener;

    .line 1721022
    new-instance v0, LX/Aso;

    invoke-direct {v0, p0}, LX/Aso;-><init>(LX/Ass;)V

    move-object v0, v0

    .line 1721023
    iput-object v0, p0, LX/Ass;->x:Landroid/view/View$OnLongClickListener;

    .line 1721024
    new-instance v0, LX/Asp;

    invoke-direct {v0, p0}, LX/Asp;-><init>(LX/Ass;)V

    move-object v0, v0

    .line 1721025
    iput-object v0, p0, LX/Ass;->y:Landroid/view/View$OnTouchListener;

    .line 1721026
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1721027
    iget-object v1, p0, LX/Ass;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a7b

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 1721028
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    iput v0, p0, LX/Ass;->h:F

    .line 1721029
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/Ass;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0hs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Ass;->k:LX/0hs;

    .line 1721030
    iget-object v0, p0, LX/Ass;->k:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1721031
    iget-object v0, p0, LX/Ass;->k:LX/0hs;

    iget-object v1, p0, LX/Ass;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1721032
    invoke-direct {p0}, LX/Ass;->e()V

    .line 1721033
    return-void
.end method

.method private static a(LX/Ass;DDLX/0xh;)LX/0wd;
    .locals 3

    .prologue
    .line 1720996
    iget-object v0, p0, LX/Ass;->r:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-static {}, LX/87U;->a()LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/Ass;LX/87O;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1720976
    iget-object v0, p0, LX/Ass;->A:LX/87O;

    if-ne v0, p1, :cond_1

    .line 1720977
    :cond_0
    :goto_0
    return-void

    .line 1720978
    :cond_1
    iget-object v0, p0, LX/Ass;->A:LX/87O;

    .line 1720979
    iput-object p1, p0, LX/Ass;->A:LX/87O;

    .line 1720980
    sget-object v1, LX/Asr;->a:[I

    invoke-virtual {p1}, LX/87O;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1720981
    :pswitch_0
    iget-object v1, p0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1720982
    iget-object v1, p0, LX/Ass;->o:Landroid/view/View;

    iget v2, p0, LX/Ass;->h:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1720983
    iget-object v1, p0, LX/Ass;->p:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1720984
    iget-object v1, p0, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    iget v2, p0, LX/Ass;->h:F

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(F)V

    .line 1720985
    sget-object v1, LX/87O;->PREVIEW:LX/87O;

    if-ne v0, v1, :cond_2

    .line 1720986
    iget-object v0, p0, LX/Ass;->s:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1720987
    :cond_2
    sget-object v1, LX/87O;->RECORDING:LX/87O;

    if-ne v0, v1, :cond_0

    .line 1720988
    iget-object v0, p0, LX/Ass;->t:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1720989
    :pswitch_1
    iget-object v0, p0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1720990
    iget-object v0, p0, LX/Ass;->o:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1720991
    iget-object v0, p0, LX/Ass;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1720992
    iget-object v0, p0, LX/Ass;->t:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1720993
    iget-object v0, p0, LX/Ass;->s:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1720994
    :pswitch_2
    iget-object v0, p0, LX/Ass;->n:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1720995
    iget-object v0, p0, LX/Ass;->t:LX/0wd;

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/Ass;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1720969
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1720970
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/Ass;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b1a7c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1720971
    :goto_0
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 1720972
    return-void

    :cond_0
    move v1, v2

    .line 1720973
    goto :goto_0
.end method

.method public static c$redex0(LX/Ass;)V
    .locals 2

    .prologue
    .line 1720965
    iget-object v0, p0, LX/Ass;->n:Landroid/view/View;

    iget-object v1, p0, LX/Ass;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1720966
    iget-object v0, p0, LX/Ass;->n:Landroid/view/View;

    iget-object v1, p0, LX/Ass;->x:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1720967
    iget-object v0, p0, LX/Ass;->n:Landroid/view/View;

    iget-object v1, p0, LX/Ass;->y:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1720968
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1720958
    iget-object v0, p0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720959
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1720960
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1720961
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1720962
    :goto_0
    return-void

    .line 1720963
    :cond_0
    iget-object v0, p0, LX/Ass;->m:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1720964
    invoke-static {p0}, LX/Ass;->c$redex0(LX/Ass;)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1721034
    sget-object v0, LX/87O;->PREVIEW:LX/87O;

    invoke-static {p0, v0}, LX/Ass;->a$redex0(LX/Ass;LX/87O;)V

    .line 1721035
    iget-object v0, p0, LX/Ass;->d:LX/Aw7;

    iget-object v1, p0, LX/Ass;->m:Landroid/view/View;

    .line 1721036
    iget-object v2, v0, LX/Aw7;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iA;

    const-string v3, "4484"

    const-class v4, LX/3nd;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3nd;

    .line 1721037
    if-eqz v2, :cond_0

    .line 1721038
    iput-object v1, v2, LX/3nd;->b:Landroid/view/View;

    .line 1721039
    iget-object v2, v0, LX/Aw7;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15W;

    iget-object v3, v0, LX/Aw7;->d:Landroid/content/Context;

    sget-object v4, LX/Aw7;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1721040
    const-class v0, LX/0i1;

    const/4 v1, 0x0

    invoke-virtual {v2, v3, v4, v0, v1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 1721041
    :cond_0
    iget-object v0, p0, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/Ass;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1721042
    return-void
.end method

.method private h()LX/0wd;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 1720956
    iget-object v0, p0, LX/Ass;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1aa3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1720957
    new-instance v6, LX/Asj;

    invoke-direct {v6, p0, v0}, LX/Asj;-><init>(LX/Ass;I)V

    move-object v1, p0

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, LX/Ass;->a(LX/Ass;DDLX/0xh;)LX/0wd;

    move-result-object v0

    return-object v0
.end method

.method private i()LX/0wd;
    .locals 7

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1720955
    new-instance v6, LX/Ask;

    invoke-direct {v6, p0}, LX/Ask;-><init>(LX/Ass;)V

    move-object v1, p0

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, LX/Ass;->a(LX/Ass;DDLX/0xh;)LX/0wd;

    move-result-object v0

    return-object v0
.end method

.method private j()LX/0wd;
    .locals 7

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1720954
    new-instance v6, LX/Asl;

    invoke-direct {v6, p0}, LX/Asl;-><init>(LX/Ass;)V

    move-object v1, p0

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, LX/Ass;->a(LX/Ass;DDLX/0xh;)LX/0wd;

    move-result-object v0

    return-object v0
.end method

.method public static r(LX/Ass;)V
    .locals 3

    .prologue
    .line 1720923
    iget-object v0, p0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720924
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-eq v1, v2, :cond_0

    .line 1720925
    :goto_0
    return-void

    :cond_0
    move-object v1, v0

    .line 1720926
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/Ass;->a:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v0

    sget-object v2, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1720951
    iget-object v0, p0, LX/Ass;->k:LX/0hs;

    invoke-virtual {v0, p1}, LX/0hs;->b(I)V

    .line 1720952
    iget-object v0, p0, LX/Ass;->k:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1720953
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1720939
    sget-object v0, LX/5L2;->ON_PAUSE:LX/5L2;

    if-ne p1, v0, :cond_1

    .line 1720940
    iget-boolean v0, p0, LX/Ass;->z:Z

    if-eqz v0, :cond_0

    .line 1720941
    iget-object v0, p0, LX/Ass;->u:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1720942
    iget-object v0, p0, LX/Ass;->o:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1720943
    invoke-static {p0}, LX/Ass;->r(LX/Ass;)V

    .line 1720944
    iput-boolean v4, p0, LX/Ass;->z:Z

    .line 1720945
    iput-boolean v4, p0, LX/Ass;->B:Z

    .line 1720946
    :cond_0
    :goto_0
    return-void

    .line 1720947
    :cond_1
    sget-object v0, LX/5L2;->ON_RESUME:LX/5L2;

    if-ne p1, v0, :cond_0

    .line 1720948
    iget-object v0, p0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720949
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1720950
    invoke-direct {p0}, LX/Ass;->f()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1720927
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1720928
    iget-object v0, p0, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720929
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v1, p1

    .line 1720930
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v2

    move-object v1, v0

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v1

    if-eq v2, v1, :cond_0

    .line 1720931
    invoke-direct {p0}, LX/Ass;->e()V

    :cond_0
    move-object v1, p1

    .line 1720932
    check-cast v1, LX/0io;

    move-object v2, v0

    check-cast v2, LX/0io;

    invoke-static {v1, v2}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1720933
    invoke-direct {p0}, LX/Ass;->f()V

    .line 1720934
    :cond_1
    :goto_0
    return-void

    .line 1720935
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v2, LX/86q;->READY:LX/86q;

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    sget-object v2, LX/86q;->READY:LX/86q;

    if-eq v1, v2, :cond_1

    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->b(LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1720936
    sget-object v0, LX/87O;->CAPTURE:LX/87O;

    invoke-static {p0, v0}, LX/Ass;->a$redex0(LX/Ass;LX/87O;)V

    .line 1720937
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ass;->B:Z

    .line 1720938
    goto :goto_0
.end method
