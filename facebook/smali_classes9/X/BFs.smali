.class public final LX/BFs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/FocusView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/FocusView;)V
    .locals 0

    .prologue
    .line 1766337
    iput-object p1, p0, LX/BFs;->a:Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1766338
    iget-object v1, p0, LX/BFs;->a:Lcom/facebook/photos/creativecam/ui/FocusView;

    iget-object v0, p0, LX/BFs;->a:Lcom/facebook/photos/creativecam/ui/FocusView;

    iget v2, v0, Lcom/facebook/photos/creativecam/ui/FocusView;->e:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v2

    .line 1766339
    iput v0, v1, Lcom/facebook/photos/creativecam/ui/FocusView;->h:F

    .line 1766340
    iget-object v0, p0, LX/BFs;->a:Lcom/facebook/photos/creativecam/ui/FocusView;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1766341
    iget-object v0, p0, LX/BFs;->a:Lcom/facebook/photos/creativecam/ui/FocusView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->invalidate()V

    .line 1766342
    return-void
.end method
