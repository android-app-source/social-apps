.class public LX/CK5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile e:LX/CK5;


# instance fields
.field private final b:LX/14x;

.field private final c:LX/0Uh;

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1876381
    const-class v0, LX/CK4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CK5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14x;LX/0Uh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1876382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876383
    iput-object p1, p0, LX/CK5;->b:LX/14x;

    .line 1876384
    iput-object p2, p0, LX/CK5;->c:LX/0Uh;

    .line 1876385
    iput-object p3, p0, LX/CK5;->d:LX/03V;

    .line 1876386
    return-void
.end method

.method public static a(LX/0QB;)LX/CK5;
    .locals 6

    .prologue
    .line 1876387
    sget-object v0, LX/CK5;->e:LX/CK5;

    if-nez v0, :cond_1

    .line 1876388
    const-class v1, LX/CK5;

    monitor-enter v1

    .line 1876389
    :try_start_0
    sget-object v0, LX/CK5;->e:LX/CK5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1876390
    if-eqz v2, :cond_0

    .line 1876391
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1876392
    new-instance p0, LX/CK5;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v3

    check-cast v3, LX/14x;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/CK5;-><init>(LX/14x;LX/0Uh;LX/03V;)V

    .line 1876393
    move-object v0, p0

    .line 1876394
    sput-object v0, LX/CK5;->e:LX/CK5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1876395
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1876396
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1876397
    :cond_1
    sget-object v0, LX/CK5;->e:LX/CK5;

    return-object v0

    .line 1876398
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1876399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Double;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Double;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1876400
    sget-object v0, LX/3RH;->V:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1876401
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1876402
    const-string v1, "dest_address"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1876403
    :cond_0
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1876404
    const-string v1, "provider_name"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1876405
    :cond_1
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1876406
    const-string v1, "logging_tag"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1876407
    :cond_2
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 1876408
    const-string v1, "dest_lat"

    invoke-virtual {p1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "dest_long"

    invoke-virtual {p2}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1876409
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1876410
    iget-object v1, p0, LX/CK5;->c:LX/0Uh;

    const/16 v2, 0x1b8

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CK5;->b:LX/14x;

    invoke-virtual {v1}, LX/14x;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CK5;->b:LX/14x;

    invoke-virtual {v1}, LX/14x;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CK5;->b:LX/14x;

    const-string v2, "61.0"

    invoke-virtual {v1, v2}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
