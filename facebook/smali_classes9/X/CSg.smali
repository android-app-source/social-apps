.class public LX/CSg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1894892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894893
    return-void
.end method

.method public static a(Landroid/view/View;LX/CSS;LX/CTK;LX/CSQ;LX/CT7;LX/CT6;LX/CT1;)V
    .locals 2

    .prologue
    .line 1894894
    sget-object v0, LX/CSf;->a:[I

    invoke-virtual {p1}, LX/CSS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1894895
    const/4 v0, 0x0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894896
    :goto_0
    return-void

    .line 1894897
    :pswitch_0
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    check-cast p2, LX/CTo;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(LX/CTo;LX/CT7;)V

    goto :goto_0

    .line 1894898
    :pswitch_1
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;

    check-cast p2, LX/CTo;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;->a(LX/CTo;LX/CT7;)V

    goto :goto_0

    .line 1894899
    :pswitch_2
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;

    check-cast p2, LX/CTL;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->a(LX/CTL;LX/CT7;)V

    goto :goto_0

    .line 1894900
    :pswitch_3
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;

    check-cast p2, LX/CTM;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->a(LX/CTM;LX/CT7;)V

    goto :goto_0

    .line 1894901
    :pswitch_4
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    check-cast p2, LX/CTN;

    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;)V

    goto :goto_0

    .line 1894902
    :pswitch_5
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    check-cast p2, LX/CTO;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->a(LX/CTO;LX/CT7;)V

    goto :goto_0

    .line 1894903
    :pswitch_6
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;

    check-cast p2, LX/CTr;

    invoke-virtual {p0, p2, p4, p5}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a(LX/CTr;LX/CT7;LX/CT6;)V

    goto :goto_0

    .line 1894904
    :pswitch_7
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;

    check-cast p2, LX/CTm;

    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(LX/CTm;LX/CT7;LX/CT6;LX/CT1;)V

    goto :goto_0

    .line 1894905
    :pswitch_8
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->a(LX/CTK;LX/CT7;)V

    goto :goto_0

    .line 1894906
    :pswitch_9
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;

    check-cast p2, LX/CTn;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->a(LX/CTn;LX/CT7;)V

    goto :goto_0

    .line 1894907
    :pswitch_a
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;

    check-cast p2, LX/CTn;

    invoke-virtual {p0, p2, p4, p6}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->a(LX/CTn;LX/CT7;LX/CT1;)V

    goto :goto_0

    .line 1894908
    :pswitch_b
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;

    check-cast p2, LX/CTn;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->a(LX/CTn;LX/CT7;)V

    goto :goto_0

    .line 1894909
    :pswitch_c
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;

    check-cast p2, LX/CTS;

    invoke-virtual {p0, p2, p4, p6}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->a(LX/CTS;LX/CT7;LX/CT1;)V

    goto :goto_0

    .line 1894910
    :pswitch_d
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;

    check-cast p2, LX/CTS;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a(LX/CTS;LX/CT7;)V

    goto :goto_0

    .line 1894911
    :pswitch_e
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    check-cast p2, LX/CTT;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->a(LX/CTT;LX/CT7;)V

    goto :goto_0

    .line 1894912
    :pswitch_f
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;

    check-cast p2, LX/CTT;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->a(LX/CTT;LX/CT7;)V

    goto :goto_0

    .line 1894913
    :pswitch_10
    check-cast p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;

    check-cast p2, LX/CTQ;

    invoke-virtual {p0, p2, p4, p5}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a(LX/CTQ;LX/CT7;LX/CT6;)V

    goto/16 :goto_0

    .line 1894914
    :pswitch_11
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;

    check-cast p2, LX/CTR;

    invoke-virtual {p0, p2, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a(LX/CTR;LX/CT7;)V

    goto/16 :goto_0

    .line 1894915
    :pswitch_12
    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;

    invoke-virtual {p0, p2, p3, p4}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->a(LX/CTK;LX/CSQ;LX/CT7;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method private static a(Landroid/view/View;LX/CTJ;LX/CT1;Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1894916
    instance-of v0, p0, Landroid/widget/FrameLayout;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p0

    .line 1894917
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p0

    .line 1894918
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p1, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 1894919
    sget-object v4, LX/CSf;->b:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1894920
    const/4 v4, 0x0

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1894921
    :goto_1
    :pswitch_0
    check-cast p0, Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1894922
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1894923
    if-nez p3, :cond_0

    const/16 v2, 0x8

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1894924
    return-void

    :cond_1
    move v0, v2

    .line 1894925
    goto :goto_0

    .line 1894926
    :pswitch_1
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    check-cast p1, LX/CTg;

    iget-object v4, p1, LX/CTg;->a:LX/CU0;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    goto :goto_1

    .line 1894927
    :pswitch_2
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;

    check-cast p1, LX/CTd;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->a(LX/CTd;)V

    goto :goto_1

    .line 1894928
    :pswitch_3
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;

    check-cast p1, LX/CTf;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->a(LX/CTf;)V

    goto :goto_1

    .line 1894929
    :pswitch_4
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;

    check-cast p1, LX/CTU;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->a(LX/CTU;)V

    goto :goto_1

    .line 1894930
    :pswitch_5
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;

    check-cast p1, LX/CTV;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->a(LX/CTV;)V

    goto :goto_1

    .line 1894931
    :pswitch_6
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;

    check-cast p1, LX/CTW;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;->a(LX/CTW;)V

    goto :goto_1

    .line 1894932
    :pswitch_7
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;

    check-cast p1, LX/CTY;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->a(LX/CTY;)V

    goto :goto_1

    .line 1894933
    :pswitch_8
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentDateView;

    check-cast p1, LX/CTZ;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentDateView;->a(LX/CTZ;)V

    goto :goto_1

    .line 1894934
    :pswitch_9
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;

    check-cast p1, LX/CTc;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;->a(LX/CTc;)V

    goto :goto_1

    .line 1894935
    :pswitch_a
    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;

    check-cast p1, LX/CTh;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->a(LX/CTh;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLScreenElementType;LX/CTJ;)V
    .locals 2

    .prologue
    .line 1894936
    sget-object v0, LX/CSf;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1894937
    const/4 v0, 0x0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894938
    :goto_0
    return-void

    .line 1894939
    :pswitch_0
    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    check-cast p2, LX/CTX;

    invoke-virtual {p0, p2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->a(LX/CTX;)V

    goto :goto_0

    .line 1894940
    :pswitch_1
    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    check-cast p2, LX/CTb;

    invoke-virtual {p0, p2}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->a(LX/CTb;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/CTJ;LX/CT1;)V
    .locals 2

    .prologue
    .line 1894941
    iget-object v0, p2, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1894942
    :goto_0
    invoke-static {p1, p2, p3, v0}, LX/CSg;->a(Landroid/view/View;LX/CTJ;LX/CT1;Landroid/view/View$OnClickListener;)V

    .line 1894943
    return-void

    .line 1894944
    :cond_0
    new-instance v0, LX/CSe;

    invoke-direct {v0, p0, p3, p2}, LX/CSe;-><init>(LX/CSg;LX/CT1;LX/CTJ;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/CTe;LX/CT6;LX/CT1;)V
    .locals 2

    .prologue
    .line 1894945
    iget-object v0, p2, LX/CTe;->a:LX/CTJ;

    new-instance v1, LX/CSd;

    invoke-direct {v1, p0, p3, p2}, LX/CSd;-><init>(LX/CSg;LX/CT6;LX/CTe;)V

    invoke-static {p1, v0, p4, v1}, LX/CSg;->a(Landroid/view/View;LX/CTJ;LX/CT1;Landroid/view/View$OnClickListener;)V

    .line 1894946
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021496

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1894947
    return-void
.end method
