.class public final LX/AWI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AWJ;


# direct methods
.method public constructor <init>(LX/AWJ;)V
    .locals 0

    .prologue
    .line 1682617
    iput-object p1, p0, LX/AWI;->a:LX/AWJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1682604
    iget-object v0, p0, LX/AWI;->a:LX/AWJ;

    iget-object v0, v0, LX/AWJ;->c:LX/AVe;

    invoke-interface {v0, p1}, LX/AVe;->a(Ljava/lang/Throwable;)V

    .line 1682605
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1682606
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1682607
    if-eqz p1, :cond_0

    .line 1682608
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1682609
    if-nez v0, :cond_1

    .line 1682610
    :cond_0
    iget-object v0, p0, LX/AWI;->a:LX/AWJ;

    iget-object v0, v0, LX/AWJ;->c:LX/AVe;

    const-string v1, "FacecastBroadcastNuxQuery result is null."

    invoke-interface {v0, v1}, LX/AVe;->a(Ljava/lang/String;)V

    .line 1682611
    :goto_0
    return-void

    .line 1682612
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1682613
    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1682614
    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1682615
    iget-object v0, p0, LX/AWI;->a:LX/AWJ;

    iget-object v0, v0, LX/AWJ;->c:LX/AVe;

    const-string v1, "FacecastBroadcastNuxQuery result has no video."

    invoke-interface {v0, v1}, LX/AVe;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1682616
    :cond_2
    iget-object v1, p0, LX/AWI;->a:LX/AWJ;

    iget-object v1, v1, LX/AWJ;->c:LX/AVe;

    invoke-interface {v1, v0}, LX/AVe;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;)V

    goto :goto_0
.end method
