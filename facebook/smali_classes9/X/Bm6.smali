.class public final LX/Bm6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Bm7;


# direct methods
.method public constructor <init>(LX/Bm7;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1817833
    iput-object p1, p0, LX/Bm6;->b:LX/Bm7;

    iput-object p2, p0, LX/Bm6;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1817834
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1817835
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1817836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1817837
    check-cast v0, LX/7oa;

    .line 1817838
    if-eqz v0, :cond_0

    .line 1817839
    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 1817840
    iget-object v1, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v1, v1, LX/Bm7;->a:Landroid/content/ContentResolver;

    iget-object v2, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v2, v2, LX/Bm7;->b:LX/Bky;

    iget-object v3, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v3, v3, LX/Bm7;->d:LX/0TD;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 1817841
    :goto_0
    return-void

    .line 1817842
    :cond_0
    iget-object v0, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v0, v0, LX/Bm7;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v1, v1, LX/Bm7;->b:LX/Bky;

    iget-object v2, p0, LX/Bm6;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Bm6;->b:LX/Bm7;

    iget-object v3, v3, LX/Bm7;->d:LX/0TD;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Ljava/lang/String;LX/0TD;)V

    goto :goto_0
.end method
