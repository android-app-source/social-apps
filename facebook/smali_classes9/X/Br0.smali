.class public LX/Br0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/Bqw;

.field public final b:LX/8wR;

.field public final c:LX/2PZ;


# direct methods
.method public constructor <init>(LX/Bqw;LX/8wR;LX/2PZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825735
    iput-object p1, p0, LX/Br0;->a:LX/Bqw;

    .line 1825736
    iput-object p2, p0, LX/Br0;->b:LX/8wR;

    .line 1825737
    iput-object p3, p0, LX/Br0;->c:LX/2PZ;

    .line 1825738
    return-void
.end method

.method public static a(LX/0QB;)LX/Br0;
    .locals 6

    .prologue
    .line 1825739
    const-class v1, LX/Br0;

    monitor-enter v1

    .line 1825740
    :try_start_0
    sget-object v0, LX/Br0;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825741
    sput-object v2, LX/Br0;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825742
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825743
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825744
    new-instance p0, LX/Br0;

    invoke-static {v0}, LX/Bqw;->a(LX/0QB;)LX/Bqw;

    move-result-object v3

    check-cast v3, LX/Bqw;

    invoke-static {v0}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v4

    check-cast v4, LX/8wR;

    invoke-static {v0}, LX/2PZ;->a(LX/0QB;)LX/2PZ;

    move-result-object v5

    check-cast v5, LX/2PZ;

    invoke-direct {p0, v3, v4, v5}, LX/Br0;-><init>(LX/Bqw;LX/8wR;LX/2PZ;)V

    .line 1825745
    move-object v0, p0

    .line 1825746
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825747
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Br0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825748
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825749
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1np;LX/1np;)V
    .locals 7
    .param p1    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/Br2;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/Br2;",
            "LX/1Pq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1np",
            "<",
            "LX/8wT;",
            ">;",
            "LX/1np",
            "<",
            "Lcom/facebook/offlinemode/boostedcomponent/CanHandleOfflineLwiMutation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1825750
    iget-object v0, p0, LX/Br0;->a:LX/Bqw;

    .line 1825751
    new-instance v1, LX/Bqv;

    move-object v2, v0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/Bqv;-><init>(LX/Bqw;ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    .line 1825752
    iput-object v0, p5, LX/1np;->a:Ljava/lang/Object;

    .line 1825753
    iget-object v0, p0, LX/Br0;->a:LX/Bqw;

    .line 1825754
    new-instance v1, LX/Bqu;

    move-object v2, v0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/Bqu;-><init>(LX/Bqw;ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    .line 1825755
    iput-object v0, p6, LX/1np;->a:Ljava/lang/Object;

    .line 1825756
    return-void
.end method
