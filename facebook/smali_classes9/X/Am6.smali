.class public LX/Am6;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# static fields
.field public static final b:LX/0wT;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/FrameLayout;

.field public d:Landroid/widget/FrameLayout;

.field public e:Landroid/widget/LinearLayout;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:I

.field public h:I

.field public i:I

.field public j:LX/0wd;

.field public k:LX/0wd;

.field public l:LX/Am5;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1710495
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Am6;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1710471
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1710472
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1710473
    const-class v0, LX/Am6;

    invoke-static {v0, p0}, LX/Am6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1710474
    const v0, 0x7f03063a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1710475
    invoke-virtual {p0, v4}, LX/Am6;->setClipChildren(Z)V

    .line 1710476
    const v0, 0x7f0d1135

    invoke-virtual {p0, v0}, LX/Am6;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Am6;->c:Landroid/widget/FrameLayout;

    .line 1710477
    const v0, 0x7f0d1130

    invoke-virtual {p0, v0}, LX/Am6;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Am6;->d:Landroid/widget/FrameLayout;

    .line 1710478
    const v0, 0x7f0d1131

    invoke-virtual {p0, v0}, LX/Am6;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Am6;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1710479
    const v0, 0x7f0d1132

    invoke-virtual {p0, v0}, LX/Am6;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Am6;->e:Landroid/widget/LinearLayout;

    .line 1710480
    iget-object v0, p0, LX/Am6;->c:Landroid/widget/FrameLayout;

    invoke-static {v0}, LX/Am6;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/Am6;->g:I

    .line 1710481
    iget-object v0, p0, LX/Am6;->d:Landroid/widget/FrameLayout;

    invoke-static {v0}, LX/Am6;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/Am6;->h:I

    .line 1710482
    iget-object v0, p0, LX/Am6;->e:Landroid/widget/LinearLayout;

    invoke-static {v0}, LX/Am6;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/Am6;->i:I

    .line 1710483
    iget-object v0, p0, LX/Am6;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    .line 1710484
    iput-boolean v5, v0, LX/0wd;->c:Z

    .line 1710485
    move-object v0, v0

    .line 1710486
    sget-object v1, LX/Am6;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Am6;->j:LX/0wd;

    .line 1710487
    iget-object v0, p0, LX/Am6;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    .line 1710488
    iput-boolean v5, v0, LX/0wd;->c:Z

    .line 1710489
    move-object v0, v0

    .line 1710490
    sget-object v1, LX/Am6;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Am6;->k:LX/0wd;

    .line 1710491
    new-instance v0, LX/Am5;

    invoke-direct {v0, p0}, LX/Am5;-><init>(LX/Am6;)V

    iput-object v0, p0, LX/Am6;->l:LX/Am5;

    .line 1710492
    iget-object v0, p0, LX/Am6;->j:LX/0wd;

    iget-object v1, p0, LX/Am6;->l:LX/Am5;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1710493
    iget-object v0, p0, LX/Am6;->k:LX/0wd;

    iget-object v1, p0, LX/Am6;->l:LX/Am5;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1710494
    return-void
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 1710469
    invoke-virtual {p0, v0, v0}, Landroid/view/View;->measure(II)V

    .line 1710470
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Am6;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p0

    check-cast p0, LX/0wW;

    iput-object p0, p1, LX/Am6;->a:LX/0wW;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1710465
    iget-object v0, p0, LX/Am6;->c:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1710466
    iget-object v0, p0, LX/Am6;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1710467
    iget-object v0, p0, LX/Am6;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1710468
    return-void
.end method
