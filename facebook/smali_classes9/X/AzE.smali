.class public LX/AzE;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;


# instance fields
.field private final b:LX/8I2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1732293
    new-instance v0, LX/Ay4;

    invoke-direct {v0}, LX/Ay4;-><init>()V

    const-string v1, ""

    .line 1732294
    iput-object v1, v0, LX/Ay4;->a:Ljava/lang/String;

    .line 1732295
    move-object v0, v0

    .line 1732296
    const-string v1, "Test souvenir"

    .line 1732297
    iput-object v1, v0, LX/Ay4;->b:Ljava/lang/String;

    .line 1732298
    move-object v0, v0

    .line 1732299
    invoke-virtual {v0}, LX/Ay4;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    sput-object v0, LX/AzE;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    return-void
.end method

.method public constructor <init>(LX/8I2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732301
    iput-object p1, p0, LX/AzE;->b:LX/8I2;

    .line 1732302
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 10

    .prologue
    .line 1732303
    iget-object v0, p0, LX/AzE;->b:LX/8I2;

    sget-object v1, LX/4gI;->ALL:LX/4gI;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1732304
    :try_start_0
    iget-object v0, p0, LX/AzE;->b:LX/8I2;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, LX/8I2;->a(Landroid/database/Cursor;I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1732305
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1732306
    new-instance v1, LX/Ay6;

    invoke-direct {v1}, LX/Ay6;-><init>()V

    sget-object v2, LX/AzE;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1732307
    iput-object v2, v1, LX/Ay6;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1732308
    move-object v1, v1

    .line 1732309
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1732310
    instance-of v3, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v3, :cond_1

    .line 1732311
    new-instance v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    .line 1732312
    iget-object v4, v0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v4

    .line 1732313
    invoke-direct {v3, v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;)V

    invoke-virtual {v1, v3}, LX/Ay6;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)LX/Ay6;

    goto :goto_0

    .line 1732314
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1732315
    :cond_1
    instance-of v3, v0, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v3, :cond_0

    .line 1732316
    new-instance v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    .line 1732317
    iget-object v4, v0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v4, v4

    .line 1732318
    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1732319
    iget-wide v8, v0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v6, v8

    .line 1732320
    invoke-direct {v3, v4, v6, v7}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;J)V

    invoke-virtual {v1, v3}, LX/Ay6;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)LX/Ay6;

    goto :goto_0

    .line 1732321
    :cond_2
    invoke-virtual {v1}, LX/Ay6;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    return-object v0
.end method
