.class public final enum LX/BOP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BOP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BOP;

.field public static final enum LINK_SHARE:LX/BOP;

.field public static final enum OPEN_GRAPH:LX/BOP;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1779762
    new-instance v0, LX/BOP;

    const-string v1, "LINK_SHARE"

    invoke-direct {v0, v1, v2}, LX/BOP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOP;->LINK_SHARE:LX/BOP;

    .line 1779763
    new-instance v0, LX/BOP;

    const-string v1, "OPEN_GRAPH"

    invoke-direct {v0, v1, v3}, LX/BOP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BOP;->OPEN_GRAPH:LX/BOP;

    .line 1779764
    const/4 v0, 0x2

    new-array v0, v0, [LX/BOP;

    sget-object v1, LX/BOP;->LINK_SHARE:LX/BOP;

    aput-object v1, v0, v2

    sget-object v1, LX/BOP;->OPEN_GRAPH:LX/BOP;

    aput-object v1, v0, v3

    sput-object v0, LX/BOP;->$VALUES:[LX/BOP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1779766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BOP;
    .locals 1

    .prologue
    .line 1779767
    const-class v0, LX/BOP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BOP;

    return-object v0
.end method

.method public static values()[LX/BOP;
    .locals 1

    .prologue
    .line 1779765
    sget-object v0, LX/BOP;->$VALUES:[LX/BOP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BOP;

    return-object v0
.end method
