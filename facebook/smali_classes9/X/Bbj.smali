.class public final LX/Bbj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 0

    .prologue
    .line 1801027
    iput-object p1, p0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1801028
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1801029
    iget-object v0, p0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1801030
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v3

    .line 1801031
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1801032
    :cond_0
    iget-object v0, p0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->s:LX/Bbe;

    iget-object v2, p0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->E:Ljava/lang/String;

    new-instance v3, LX/Bbi;

    invoke-direct {v3, p0, v1}, LX/Bbi;-><init>(LX/Bbj;Ljava/util/List;)V

    .line 1801033
    new-instance v4, LX/4IG;

    invoke-direct {v4}, LX/4IG;-><init>()V

    .line 1801034
    const-string v5, "friend_ids"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1801035
    move-object v4, v4

    .line 1801036
    const-string v5, "place_list_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801037
    move-object v4, v4

    .line 1801038
    new-instance v5, LX/5IK;

    invoke-direct {v5}, LX/5IK;-><init>()V

    move-object v5, v5

    .line 1801039
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1801040
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1801041
    iget-object v5, v0, LX/Bbe;->b:LX/1Ck;

    const-string v6, "invite_friends_send_notification"

    iget-object p1, v0, LX/Bbe;->a:LX/0tX;

    invoke-virtual {p1, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p1, LX/Bbd;

    invoke-direct {p1, v0, v3}, LX/Bbd;-><init>(LX/Bbe;LX/0TF;)V

    invoke-virtual {v5, v6, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1801042
    iget-object v0, p0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1801043
    return-void
.end method
