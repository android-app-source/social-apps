.class public LX/AhX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1703233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1703234
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1703207
    const-string v0, "graphql_feedback_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1703208
    const-string v0, "module_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1703209
    const-string v0, "fragment_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1703210
    const-string v0, "graphql_can_viewer_invite_user"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 1703211
    const-string v0, "reaction_can_viewer_ban_user"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 1703212
    const-string v0, "comment_mention_mode"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8s1;

    .line 1703213
    const-string v1, "view_permalink_params"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1703214
    new-instance v7, LX/8qQ;

    invoke-direct {v7}, LX/8qQ;-><init>()V

    .line 1703215
    iput-object v2, v7, LX/8qQ;->a:Ljava/lang/String;

    .line 1703216
    move-object v2, v7

    .line 1703217
    iput-object v3, v2, LX/8qQ;->l:Ljava/lang/String;

    .line 1703218
    move-object v2, v2

    .line 1703219
    iput-object v4, v2, LX/8qQ;->k:Ljava/lang/String;

    .line 1703220
    move-object v2, v2

    .line 1703221
    iput-boolean v5, v2, LX/8qQ;->h:Z

    .line 1703222
    move-object v2, v2

    .line 1703223
    iput-boolean v6, v2, LX/8qQ;->i:Z

    .line 1703224
    move-object v2, v2

    .line 1703225
    iput-object v0, v2, LX/8qQ;->j:LX/8s1;

    .line 1703226
    move-object v0, v2

    .line 1703227
    iput-object v1, v0, LX/8qQ;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1703228
    move-object v0, v0

    .line 1703229
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v0

    .line 1703230
    new-instance v1, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;

    invoke-direct {v1}, Lcom/facebook/feedback/reactions/ui/PermalinkReactorsListFragment;-><init>()V

    .line 1703231
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1703232
    return-object v1
.end method
