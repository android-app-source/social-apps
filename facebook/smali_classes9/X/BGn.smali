.class public final LX/BGn;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/media/MediaIdKey;

.field public final synthetic b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/media/MediaIdKey;)V
    .locals 0

    .prologue
    .line 1767503
    iput-object p1, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, LX/BGn;->a:Lcom/facebook/ipc/media/MediaIdKey;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1767504
    iget-object v0, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v2, "Unable to fetch tagging data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767505
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1767506
    new-instance v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;-><init>()V

    .line 1767507
    iget-object v1, p0, LX/BGn;->a:Lcom/facebook/ipc/media/MediaIdKey;

    iget-object v2, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v2, v2, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767508
    iget-object v3, v2, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v2, v3

    .line 1767509
    iget-boolean v3, v2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    move v2, v3

    .line 1767510
    new-instance v3, LX/BH8;

    iget-object v4, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {v3, v4}, LX/BH8;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    sget-object v4, LX/BIf;->SIMPLEPICKER:LX/BIf;

    iget-object v5, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v5, v5, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/ipc/media/MediaIdKey;ZLX/BH7;LX/BIf;Ljava/lang/String;)V

    .line 1767511
    iget-object v1, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767512
    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;I)V

    .line 1767513
    iget-object v1, p0, LX/BGn;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2c78

    const-string v3, "GALLERY_FRAGMENT"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, LX/0hH;->a(I)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1767514
    return-void
.end method
