.class public final LX/B3z;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final l:Lcom/facebook/fig/listitem/FigListItem;

.field private final m:LX/B4A;

.field public n:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;


# direct methods
.method public constructor <init>(Lcom/facebook/fig/listitem/FigListItem;LX/B4A;)V
    .locals 1

    .prologue
    .line 1741107
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1741108
    iput-object p1, p0, LX/B3z;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 1741109
    iput-object p2, p0, LX/B3z;->m:LX/B4A;

    .line 1741110
    iget-object v0, p0, LX/B3z;->l:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1741111
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3e9ec3b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1741112
    iget-object v1, p0, LX/B3z;->l:Lcom/facebook/fig/listitem/FigListItem;

    if-ne p1, v1, :cond_0

    .line 1741113
    iget-object v1, p0, LX/B3z;->m:LX/B4A;

    iget-object v2, p0, LX/B3z;->n:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1741114
    iget-object v4, v2, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v4, v4

    .line 1741115
    invoke-static {v4}, LX/5Qm;->b(LX/5QV;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1741116
    iget-object v4, v1, LX/B4A;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v5, "pivot_invalid_overlays"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Missing ImageOverlay in fragment model: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741117
    :cond_0
    :goto_0
    const v1, 0x46c907fa

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1741118
    :cond_1
    iget-object v4, v1, LX/B4A;->j:LX/B4O;

    iget-object v5, v1, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-virtual {v5}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a()Ljava/lang/String;

    move-result-object v5

    .line 1741119
    const-string p0, "heisman_select_page_from_pivot"

    invoke-static {v4, p0, v5}, LX/B4O;->g(LX/B4O;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741120
    iget-object v4, v1, LX/B4A;->d:LX/B3T;

    iget v5, v1, LX/B4A;->q:I

    invoke-interface {v4, v2, v5}, LX/B3T;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;I)V

    goto :goto_0
.end method
