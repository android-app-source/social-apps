.class public final LX/CD4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Oj;


# instance fields
.field private final a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

.field private final b:LX/C2F;

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/04H;

.field public e:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;LX/C2F;LX/04H;)V
    .locals 1

    .prologue
    .line 1858653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858654
    iput-object p1, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    .line 1858655
    iput-object p2, p0, LX/CD4;->b:LX/C2F;

    .line 1858656
    iput-object p3, p0, LX/CD4;->d:LX/04H;

    .line 1858657
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CD4;->c:Ljava/util/HashSet;

    .line 1858658
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1858659
    iget-object v0, p0, LX/CD4;->e:Landroid/view/View;

    instance-of v0, v0, LX/7gM;

    if-nez v0, :cond_0

    .line 1858660
    :goto_0
    return-void

    .line 1858661
    :cond_0
    iget-object v0, p0, LX/CD4;->e:Landroid/view/View;

    check-cast v0, LX/7gM;

    .line 1858662
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 1858663
    invoke-virtual {v0, v1}, LX/7gK;->getLocationInWindow([I)V

    .line 1858664
    const/4 v2, 0x1

    aget v1, v1, v2

    move v0, v1

    .line 1858665
    int-to-float v0, v0

    iget-object v1, p0, LX/CD4;->b:LX/C2F;

    .line 1858666
    iget v2, v1, LX/C2F;->l:I

    move v1, v2

    .line 1858667
    iget-object v2, p0, LX/CD4;->b:LX/C2F;

    .line 1858668
    iget v3, v2, LX/C2F;->m:I

    move v2, v3

    .line 1858669
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1858670
    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-lez v5, :cond_4

    .line 1858671
    int-to-float v5, v1

    sub-float/2addr v5, v0

    int-to-float v6, v2

    div-float/2addr v5, v6

    .line 1858672
    const/high16 v6, 0x3f000000    # 0.5f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    .line 1858673
    :cond_1
    :goto_1
    move v0, v3

    .line 1858674
    if-eqz v0, :cond_2

    .line 1858675
    iget-object v0, p0, LX/CD4;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1858676
    iget-object v0, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->b:LX/1Bv;

    iget-object v1, p0, LX/CD4;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, LX/1Bv;->a(Ljava/util/Set;)Z

    .line 1858677
    iget-object v0, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->d:LX/093;

    iget-object v1, p0, LX/CD4;->c:Ljava/util/HashSet;

    iget-object v2, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->b:LX/1Bv;

    iget-object v3, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->a:LX/1C2;

    iget-object v4, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->e:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v5, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v5, v5, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->e:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/CD4;->b:LX/C2F;

    .line 1858678
    iget-object v7, v6, LX/C2F;->k:LX/04D;

    move-object v6, v7

    .line 1858679
    iget-object v7, p0, LX/CD4;->d:LX/04H;

    iget-object v8, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v8, v8, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->e:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-static/range {v0 .. v8}, LX/3In;->a(LX/093;Ljava/util/Set;LX/1Bv;LX/1C2;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)V

    goto :goto_0

    .line 1858680
    :cond_2
    iget-object v0, p0, LX/CD4;->a:Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/video/LogFullscreenVideoDisplayedPartDefinition;->d:LX/093;

    .line 1858681
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/093;->a(Z)V

    .line 1858682
    goto :goto_0

    :cond_3
    move v3, v4

    .line 1858683
    goto :goto_1

    .line 1858684
    :cond_4
    int-to-float v5, v2

    add-float/2addr v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    .line 1858685
    const v6, 0x3f59999a    # 0.85f

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_1

    move v3, v4

    goto :goto_1
.end method
