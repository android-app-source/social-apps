.class public LX/CNu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0lp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1882199
    const-class v0, LX/CNu;

    sput-object v0, LX/CNu;->a:Ljava/lang/Class;

    .line 1882200
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    sput-object v0, LX/CNu;->b:LX/0lp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1882201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/CNc;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNativeTemplateView;",
            "LX/CNc;",
            ")",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1882202
    const-string v0, "%s.convertNativeTemplateView"

    sget-object v1, LX/CNu;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x663941c4

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1882203
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;->a()LX/0Px;

    move-result-object v2

    .line 1882204
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1882205
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    .line 1882206
    invoke-static {v0, p1}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;LX/CNc;)LX/CNb;

    move-result-object v0

    .line 1882207
    if-eqz v0, :cond_0

    .line 1882208
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1882209
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1882210
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1882211
    const v1, -0x7f9d7f8d

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x91ea56b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/15w;Ljava/util/Map;LX/CNc;)LX/CNa;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "LX/CNc;",
            ")",
            "LX/CNa;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1882212
    new-instance v9, LX/CNa;

    invoke-direct {v9}, LX/CNa;-><init>()V

    move-object v5, v2

    .line 1882213
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v1, :cond_6

    .line 1882214
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1882215
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 1882216
    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_5

    .line 1882217
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    move-object v0, v2

    move-object v1, v5

    move v2, v3

    .line 1882218
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v8, :cond_3

    .line 1882219
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    .line 1882220
    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-ne v5, v8, :cond_2

    .line 1882221
    invoke-static {p0, p1, p2}, LX/CNu;->a(LX/15w;Ljava/util/Map;LX/CNc;)LX/CNa;

    move-result-object v2

    .line 1882222
    if-nez v1, :cond_0

    .line 1882223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1882224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1882225
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v5, 0x0

    .line 1882226
    :goto_2
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1882227
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1882228
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1882229
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1882230
    move v2, v4

    .line 1882231
    goto :goto_1

    .line 1882232
    :cond_2
    invoke-static {p0, p1}, LX/CNu;->a(LX/15w;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1882233
    :cond_3
    if-nez v2, :cond_4

    .line 1882234
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v9, v6, v2}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    move-object v2, v0

    move-object v5, v1

    .line 1882235
    goto :goto_0

    .line 1882236
    :cond_5
    invoke-static {p0, p1}, LX/CNu;->a(LX/15w;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v9, v6, v0}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 1882237
    :cond_6
    if-eqz v5, :cond_a

    .line 1882238
    invoke-virtual {v9}, LX/CNa;->b()LX/CNb;

    move-result-object v10

    move v6, v3

    .line 1882239
    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_a

    .line 1882240
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1882241
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1882242
    invoke-static {v10}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v1

    .line 1882243
    invoke-virtual {v1, v10}, LX/3j9;->a(LX/CNb;)Ljava/util/Set;

    move-result-object v1

    .line 1882244
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    move v7, v4

    :goto_4
    move v8, v3

    .line 1882245
    :goto_5
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_9

    .line 1882246
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CNa;

    .line 1882247
    if-eqz v7, :cond_7

    .line 1882248
    invoke-static {v1, v9, v0, p2}, LX/CNu;->a(LX/CNa;LX/CNa;Ljava/lang/String;LX/CNc;)V

    .line 1882249
    :cond_7
    invoke-virtual {v1}, LX/CNa;->a()LX/CNb;

    move-result-object v1

    invoke-virtual {v11, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1882250
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_5

    :cond_8
    move v7, v3

    .line 1882251
    goto :goto_4

    .line 1882252
    :cond_9
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1882253
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    .line 1882254
    :cond_a
    return-object v9

    .line 1882255
    :cond_b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;LX/CNc;)LX/CNb;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1882256
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;->j()Ljava/lang/String;

    move-result-object v1

    .line 1882257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;->a()LX/0Px;

    move-result-object v2

    .line 1882258
    new-instance v6, Ljava/util/HashMap;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    invoke-direct {v6, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 1882259
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v7, :cond_7

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    .line 1882260
    const/4 v4, 0x0

    .line 1882261
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1882262
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 1882263
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 1882264
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1882265
    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1882266
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1882267
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_1

    .line 1882268
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1882269
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->k()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    goto :goto_1

    .line 1882270
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->m()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1882271
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->m()LX/0Px;

    move-result-object v4

    goto :goto_1

    .line 1882272
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 1882273
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    goto :goto_1

    .line 1882274
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1882275
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    goto :goto_1

    .line 1882276
    :cond_7
    move-object v2, v6

    .line 1882277
    :try_start_0
    sget-object v3, LX/CNu;->b:LX/0lp;

    invoke-virtual {v3, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 1882278
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 1882279
    invoke-static {v1, v2, p1}, LX/CNu;->a(LX/15w;Ljava/util/Map;LX/CNc;)LX/CNa;

    move-result-object v1

    .line 1882280
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, p1}, LX/CNu;->a(LX/CNa;LX/CNa;Ljava/lang/String;LX/CNc;)V

    .line 1882281
    invoke-virtual {v1}, LX/CNa;->a()LX/CNb;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1882282
    :goto_2
    return-object v0

    .line 1882283
    :catch_0
    move-exception v1

    .line 1882284
    sget-object v2, LX/CNu;->a:Ljava/lang/Class;

    const-string v3, "Error parsing bundle: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private static a(LX/15w;Ljava/util/Map;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 1882285
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v0

    .line 1882286
    const-string v1, "__ntattrp__:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static a(LX/CNa;LX/CNa;Ljava/lang/String;LX/CNc;)V
    .locals 4
    .param p1    # LX/CNa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1882287
    invoke-virtual {p0}, LX/CNa;->b()LX/CNb;

    move-result-object v1

    .line 1882288
    const-string v0, "id"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1882289
    const-string v0, "client_pp"

    invoke-virtual {v1, v0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNV;

    .line 1882290
    if-eqz v2, :cond_0

    .line 1882291
    iget-object v3, p3, LX/CNc;->a:LX/CNL;

    invoke-virtual {v3, v1, v2}, LX/CNL;->a(LX/CNb;Ljava/lang/String;)V

    .line 1882292
    if-nez v0, :cond_0

    .line 1882293
    new-instance v0, LX/CNV;

    invoke-direct {v0}, LX/CNV;-><init>()V

    .line 1882294
    const-string v2, "client_pp"

    invoke-virtual {p0, v2, v0}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1882295
    :cond_0
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1882296
    invoke-virtual {p1}, LX/CNa;->b()LX/CNb;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LX/CNV;->a(LX/CNb;Ljava/lang/String;)V

    .line 1882297
    const-string v0, "client_pp"

    new-instance v2, LX/CNV;

    invoke-direct {v2}, LX/CNV;-><init>()V

    invoke-virtual {p1, v0, v2}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1882298
    :cond_1
    invoke-static {v1}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    .line 1882299
    invoke-virtual {v0, p0}, LX/3j9;->a(LX/CNa;)V

    .line 1882300
    return-void
.end method
