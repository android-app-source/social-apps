.class public LX/C3Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/C3I;

.field public final c:LX/C39;

.field public final d:LX/C3p;


# direct methods
.method public constructor <init>(LX/0Ot;LX/C3I;LX/C39;LX/C3p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;",
            "LX/C3I;",
            "LX/C39;",
            "LX/C3p;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845930
    iput-object p1, p0, LX/C3Z;->a:LX/0Ot;

    .line 1845931
    iput-object p2, p0, LX/C3Z;->b:LX/C3I;

    .line 1845932
    iput-object p3, p0, LX/C3Z;->c:LX/C39;

    .line 1845933
    iput-object p4, p0, LX/C3Z;->d:LX/C3p;

    .line 1845934
    return-void
.end method

.method public static a(LX/0QB;)LX/C3Z;
    .locals 7

    .prologue
    .line 1845935
    const-class v1, LX/C3Z;

    monitor-enter v1

    .line 1845936
    :try_start_0
    sget-object v0, LX/C3Z;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845937
    sput-object v2, LX/C3Z;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845938
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845939
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845940
    new-instance v6, LX/C3Z;

    const/16 v3, 0x1ebc

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/C3I;->a(LX/0QB;)LX/C3I;

    move-result-object v3

    check-cast v3, LX/C3I;

    invoke-static {v0}, LX/C39;->a(LX/0QB;)LX/C39;

    move-result-object v4

    check-cast v4, LX/C39;

    invoke-static {v0}, LX/C3p;->a(LX/0QB;)LX/C3p;

    move-result-object v5

    check-cast v5, LX/C3p;

    invoke-direct {v6, p0, v3, v4, v5}, LX/C3Z;-><init>(LX/0Ot;LX/C3I;LX/C39;LX/C3p;)V

    .line 1845941
    move-object v0, v6

    .line 1845942
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845943
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845944
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/C33;LX/1VL;)Z
    .locals 2

    .prologue
    .line 1845946
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845947
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1845948
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1VL;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
