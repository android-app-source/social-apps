.class public LX/BQg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782543
    return-void
.end method

.method public static a(LX/0QB;)LX/BQg;
    .locals 1

    .prologue
    .line 1782544
    new-instance v0, LX/BQg;

    invoke-direct {v0}, LX/BQg;-><init>()V

    .line 1782545
    move-object v0, v0

    .line 1782546
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1782547
    check-cast p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    const/4 v4, 0x0

    .line 1782548
    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1782549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SetCoverPhotoMethod must be called withsetCoverPhotoParams.getUseExistingPhoto() == true"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1782550
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1782551
    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e()F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 1782552
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "focus_y"

    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1782553
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d()F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 1782554
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "focus_x"

    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1782555
    :cond_2
    const-string v2, "%s/cover"

    .line 1782556
    iget-wide v8, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    move-wide v4, v8

    .line 1782557
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    const-string v0, "me"

    :goto_0
    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1782558
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "set_cover_photo"

    .line 1782559
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1782560
    move-object v2, v2

    .line 1782561
    const-string v3, "POST"

    .line 1782562
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1782563
    move-object v2, v2

    .line 1782564
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1782565
    move-object v0, v2

    .line 1782566
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1782567
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1782568
    move-object v0, v0

    .line 1782569
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "photo"

    .line 1782570
    iget-wide v8, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    move-wide v4, v8

    .line 1782571
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1782572
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1782573
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1782574
    :cond_3
    iget-wide v8, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    move-wide v4, v8

    .line 1782575
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1782576
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1782577
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
