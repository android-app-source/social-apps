.class public final LX/Axs;
.super Landroid/os/FileObserver;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Axs;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Sh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Axq;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728942
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    .line 1728943
    iput-object p1, p0, LX/Axs;->a:LX/0Ot;

    .line 1728944
    iput-object p2, p0, LX/Axs;->b:LX/0Sh;

    .line 1728945
    return-void
.end method

.method public static a(LX/0QB;)LX/Axs;
    .locals 5

    .prologue
    .line 1728946
    sget-object v0, LX/Axs;->c:LX/Axs;

    if-nez v0, :cond_1

    .line 1728947
    const-class v1, LX/Axs;

    monitor-enter v1

    .line 1728948
    :try_start_0
    sget-object v0, LX/Axs;->c:LX/Axs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1728949
    if-eqz v2, :cond_0

    .line 1728950
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1728951
    new-instance v4, LX/Axs;

    const/16 v3, 0x22bd

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {v4, p0, v3}, LX/Axs;-><init>(LX/0Ot;LX/0Sh;)V

    .line 1728952
    move-object v0, v4

    .line 1728953
    sput-object v0, LX/Axs;->c:LX/Axs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728954
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1728955
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1728956
    :cond_1
    sget-object v0, LX/Axs;->c:LX/Axs;

    return-object v0

    .line 1728957
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1728958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onEvent(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1728959
    iget-object v0, p0, LX/Axs;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1728960
    iget-object v0, p0, LX/Axs;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axq;

    const/4 v1, 0x1

    .line 1728961
    iput-boolean v1, v0, LX/Axq;->a:Z

    .line 1728962
    return-void
.end method
