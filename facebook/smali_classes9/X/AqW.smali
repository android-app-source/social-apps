.class public LX/AqW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1718021
    iput-object p1, p0, LX/AqW;->a:LX/0ad;

    .line 1718022
    return-void
.end method

.method public static final a(LX/1RN;)Z
    .locals 3

    .prologue
    .line 1718039
    invoke-static {p0}, LX/AqW;->d(LX/1RN;)LX/1kW;

    move-result-object v0

    .line 1718040
    invoke-virtual {v0}, LX/1kW;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->FRIENDS_HOLIDAY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1718041
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1718042
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/AqW;
    .locals 2

    .prologue
    .line 1718037
    new-instance v1, LX/AqW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/AqW;-><init>(LX/0ad;)V

    .line 1718038
    return-object v1
.end method

.method public static d(LX/1RN;)LX/1kW;
    .locals 2

    .prologue
    .line 1718034
    invoke-static {p0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1718035
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1718036
    check-cast v0, LX/1kW;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1718033
    iget-object v0, p0, LX/AqW;->a:LX/0ad;

    sget-short v1, LX/AqU;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1RN;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RN;",
            ")",
            "LX/0Px",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718023
    invoke-static {p1}, LX/AqW;->d(LX/1RN;)LX/1kW;

    move-result-object v0

    .line 1718024
    iget-object p1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p1

    .line 1718025
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 1718026
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1718027
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1718028
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1718029
    new-instance v5, LX/6UY;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v5, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    .line 1718030
    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1718031
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1718032
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
