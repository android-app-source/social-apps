.class public LX/CYK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/browserextensions/common/autofill/autofill_provider/FbAutoFillProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/browserextensions/common/autofill/autofill_provider/FbAutoFillProvider;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910943
    iput-object p1, p0, LX/CYK;->a:Landroid/content/Context;

    .line 1910944
    iput-object p2, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1910945
    iput-object p3, p0, LX/CYK;->c:LX/17Y;

    .line 1910946
    iput-object p4, p0, LX/CYK;->d:LX/0Or;

    .line 1910947
    iput-object p5, p0, LX/CYK;->e:LX/0Ot;

    .line 1910948
    iput-object p6, p0, LX/CYK;->g:LX/0Ot;

    .line 1910949
    iput-object p7, p0, LX/CYK;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1910950
    return-void
.end method

.method public static a(LX/0QB;)LX/CYK;
    .locals 1

    .prologue
    .line 1910951
    invoke-static {p0}, LX/CYK;->b(LX/0QB;)LX/CYK;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/CYK;JZLcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;LX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V
    .locals 5
    .param p3    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/CYE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910952
    iget-object v0, p0, LX/CYK;->c:LX/17Y;

    iget-object v1, p0, LX/CYK;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->ba:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1910953
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1910954
    const-string v0, "page_call_to_action_isadmin_extra"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1910955
    const-string v0, "extra_config_action_data"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1910956
    const-string v0, "extra_cta_config"

    invoke-static {v1, v0, p6}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1910957
    if-eqz p3, :cond_0

    .line 1910958
    iget-object v2, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2781

    iget-object v0, p0, LX/CYK;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1910959
    :goto_0
    return-void

    .line 1910960
    :cond_0
    invoke-static {p4}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1910961
    const-string v0, "page_call_to_action_fields_extra"

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 1910962
    const-string v0, "page_call_to_action_label_extra"

    invoke-virtual {p4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1910963
    :cond_1
    iget-object v2, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2787

    iget-object v0, p0, LX/CYK;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/CYK;
    .locals 8

    .prologue
    .line 1910964
    new-instance v0, LX/CYK;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1831

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v0 .. v7}, LX/CYK;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 1910965
    return-object v0
.end method

.method public static d(LX/CYK;LX/CY7;)V
    .locals 6

    .prologue
    .line 1910966
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    const/4 v1, 0x1

    .line 1910967
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1910968
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1910969
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1910970
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 1910971
    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-static {v3, v2}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1910972
    iget-object v3, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1910973
    :goto_0
    move v0, v1

    .line 1910974
    if-eqz v0, :cond_0

    .line 1910975
    :goto_1
    return-void

    .line 1910976
    :cond_0
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1910977
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1910978
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1910979
    const/4 v2, 0x0

    .line 1910980
    iget-object v3, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1910981
    iget-object v2, p0, LX/CYK;->c:LX/17Y;

    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    sget-object v4, LX/0ax;->cH:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "cta_id"

    iget-object v4, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1910982
    iget-object v3, p1, LX/CY7;->g:LX/0P1;

    if-eqz v3, :cond_1

    .line 1910983
    new-instance v3, Lorg/json/JSONObject;

    new-instance v4, Ljava/util/HashMap;

    iget-object v5, p1, LX/CY7;->g:LX/0P1;

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 1910984
    const-string v4, "initial_input"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1910985
    :cond_1
    if-nez v2, :cond_2

    .line 1910986
    iget-object v2, p0, LX/CYK;->c:LX/17Y;

    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1910987
    :cond_2
    move-object v2, v2

    .line 1910988
    if-eqz v2, :cond_6

    .line 1910989
    invoke-static {v1}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "force_external_activity"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1910990
    :cond_3
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1910991
    iget-object v0, p0, LX/CYK;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6DR;

    invoke-virtual {v0}, LX/6DR;->a()V

    .line 1910992
    :cond_4
    iget-object v0, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 1910993
    :cond_5
    iget-object v1, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2787

    iget-object v0, p0, LX/CYK;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_1

    .line 1910994
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1910995
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1910996
    iget-object v1, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1910997
    :cond_7
    iget-object v0, p0, LX/CYK;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, LX/CYK;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No valid actions for the Call-to-Action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1910998
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1910999
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 1911000
    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1911001
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1911002
    if-eqz v2, :cond_9

    .line 1911003
    iget-object v3, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1911004
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final c(LX/CY7;)V
    .locals 10

    .prologue
    .line 1911005
    sget-object v0, LX/CYJ;->a:[I

    iget-object v1, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1911006
    invoke-static {p0, p1}, LX/CYK;->d(LX/CYK;LX/CY7;)V

    .line 1911007
    :goto_0
    return-void

    .line 1911008
    :pswitch_0
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->p()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 1911009
    if-eqz v0, :cond_2

    .line 1911010
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1911011
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.DIAL"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 1911012
    iget-object v3, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1911013
    :goto_2
    goto :goto_0

    :cond_0
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->p()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1911014
    :pswitch_1
    iget-wide v0, p1, LX/CY7;->a:J

    .line 1911015
    sget-object v2, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1911016
    iget-object v3, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/CYK;->c:LX/17Y;

    iget-object v5, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v4, v5, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v4, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1911017
    goto :goto_0

    .line 1911018
    :pswitch_2
    iget-object v0, p1, LX/CY7;->b:Ljava/lang/String;

    iget-object v1, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dU_()Ljava/lang/String;

    move-result-object v2

    const/4 p1, 0x1

    const/4 v9, 0x0

    .line 1911019
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1911020
    new-instance v4, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1911021
    const/high16 v3, 0x10000000

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1911022
    const-string v3, "vnd.android.cursor.item/email"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1911023
    const-string v3, "android.intent.extra.EMAIL"

    new-array v5, p1, [Ljava/lang/String;

    aput-object v2, v5, v9

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1911024
    const-string v5, "android.intent.extra.SUBJECT"

    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081699

    const/4 v3, 0x2

    new-array v8, v3, [Ljava/lang/Object;

    iget-object v3, p0, LX/CYK;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 1911025
    iget-object v1, v3, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v1

    .line 1911026
    aput-object v3, v8, v9

    aput-object v0, v8, p1

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1911027
    iget-object v3, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f081678

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    .line 1911028
    iget-object v4, p0, LX/CYK;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, p0, LX/CYK;->a:Landroid/content/Context;

    invoke-interface {v4, v3, v5}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1911029
    :goto_3
    goto/16 :goto_0

    .line 1911030
    :pswitch_3
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1911031
    iget-wide v2, p1, LX/CY7;->a:J

    const/4 v4, 0x0

    iget-object v5, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    iget-object v6, p1, LX/CY7;->f:LX/CYE;

    iget-object v7, p1, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-object v1, p0

    invoke-static/range {v1 .. v7}, LX/CYK;->a(LX/CYK;JZLcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;LX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V

    goto/16 :goto_0

    .line 1911032
    :cond_1
    invoke-static {p0, p1}, LX/CYK;->d(LX/CYK;LX/CY7;)V

    goto/16 :goto_0

    .line 1911033
    :cond_2
    iget-object v2, p0, LX/CYK;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-class v3, LX/CYK;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No phone number for the CallNow type Call-to-Action: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1911034
    :cond_3
    iget-object v3, p0, LX/CYK;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    const-class v4, LX/CYK;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No email address for the EmailUs type Call-to-Action: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
