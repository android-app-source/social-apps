.class public final LX/CSt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CSr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1895043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;
    .locals 1

    .prologue
    .line 1895044
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895045
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895046
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895047
    invoke-static {p1, p2}, LX/CSv;->c(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    move-object v0, v0

    .line 1895048
    return-object v0

    .line 1895049
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
