.class public LX/CYI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/9XE;

.field private final b:LX/CYK;

.field public final c:LX/CXj;

.field private final d:LX/8Do;

.field private final e:LX/CSN;

.field public f:LX/CY7;

.field public g:Z

.field public h:Z

.field public i:Landroid/os/ParcelUuid;

.field public j:Z


# direct methods
.method public constructor <init>(LX/9XE;LX/CYK;LX/CXj;LX/8Do;LX/CSN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910935
    iput-object p1, p0, LX/CYI;->a:LX/9XE;

    .line 1910936
    iput-object p2, p0, LX/CYI;->b:LX/CYK;

    .line 1910937
    iput-object p3, p0, LX/CYI;->c:LX/CXj;

    .line 1910938
    iput-object p4, p0, LX/CYI;->d:LX/8Do;

    .line 1910939
    iput-object p5, p0, LX/CYI;->e:LX/CSN;

    .line 1910940
    return-void
.end method

.method public static b(LX/0QB;)LX/CYI;
    .locals 6

    .prologue
    .line 1910932
    new-instance v0, LX/CYI;

    invoke-static {p0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-static {p0}, LX/CYK;->b(LX/0QB;)LX/CYK;

    move-result-object v2

    check-cast v2, LX/CYK;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v3

    check-cast v3, LX/CXj;

    invoke-static {p0}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v4

    check-cast v4, LX/8Do;

    invoke-static {p0}, LX/CSN;->a(LX/0QB;)LX/CSN;

    move-result-object v5

    check-cast v5, LX/CSN;

    invoke-direct/range {v0 .. v5}, LX/CYI;-><init>(LX/9XE;LX/CYK;LX/CXj;LX/8Do;LX/CSN;)V

    .line 1910933
    return-object v0
.end method


# virtual methods
.method public final a(LX/CY7;)LX/CY8;
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1910874
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910875
    sget-object v2, LX/CYB;->HIDDEN:LX/CYB;

    .line 1910876
    sget-object v5, LX/CYC;->NONE:LX/CYC;

    .line 1910877
    sget-object v6, LX/CY9;->NOT_CLICKABLE:LX/CY9;

    .line 1910878
    sget-object v7, LX/CYA;->WIDE:LX/CYA;

    .line 1910879
    iput-object p1, p0, LX/CYI;->f:LX/CY7;

    .line 1910880
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-eqz v0, :cond_2

    .line 1910881
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->c:LX/0Px;

    .line 1910882
    new-instance v2, LX/8A4;

    invoke-direct {v2, v0}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v3, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v2, v3}, LX/8A4;->a(LX/8A3;)Z

    move-result v2

    move v0, v2

    .line 1910883
    iput-boolean v0, p0, LX/CYI;->g:Z

    .line 1910884
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->c:LX/0Px;

    invoke-static {v0}, LX/CYR;->a(LX/0Px;)Z

    move-result v0

    iput-boolean v0, p0, LX/CYI;->h:Z

    .line 1910885
    iget-boolean v0, p0, LX/CYI;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CYI;->h:Z

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1910886
    sget-object v6, LX/CY9;->CLICKABLE:LX/CY9;

    .line 1910887
    iget-boolean v0, p0, LX/CYI;->g:Z

    if-eqz v0, :cond_b

    .line 1910888
    iget-boolean v0, p0, LX/CYI;->h:Z

    if-nez v0, :cond_9

    .line 1910889
    sget-object v0, LX/CYB;->HIDDEN:LX/CYB;

    .line 1910890
    :goto_1
    move-object v2, v0

    .line 1910891
    iget-boolean v0, p0, LX/CYI;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->e()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$CtaAdminInfoModel;

    move-result-object v0

    if-nez v0, :cond_d

    .line 1910892
    :cond_1
    sget-object v0, LX/CYC;->NONE:LX/CYC;

    .line 1910893
    :goto_2
    move-object v5, v0

    .line 1910894
    iget-object v0, p0, LX/CYI;->d:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, LX/CYA;->NARROW:LX/CYA;

    :goto_3
    move-object v7, v0

    .line 1910895
    :cond_2
    iput-boolean v1, p0, LX/CYI;->j:Z

    .line 1910896
    new-instance v0, LX/CY8;

    iget-object v1, p0, LX/CYI;->f:LX/CY7;

    iget-object v1, v1, LX/CY7;->b:Ljava/lang/String;

    iget-object v3, p0, LX/CYI;->f:LX/CY7;

    iget-object v3, v3, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/CYI;->f:LX/CY7;

    iget-object v3, v3, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_7

    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    :goto_4
    iget-object v4, p0, LX/CYI;->f:LX/CY7;

    iget-object v4, v4, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-eqz v4, :cond_4

    iget-object v4, p0, LX/CYI;->f:LX/CY7;

    iget-object v4, v4, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->p()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;

    move-result-object v4

    if-nez v4, :cond_8

    :cond_4
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    :goto_5
    invoke-direct/range {v0 .. v7}, LX/CY8;-><init>(Ljava/lang/String;LX/CYB;LX/0am;LX/0am;LX/CYC;LX/CY9;LX/CYA;)V

    return-object v0

    .line 1910897
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 1910898
    :cond_6
    sget-object v0, LX/CYA;->WIDE:LX/CYA;

    goto :goto_3

    .line 1910899
    :cond_7
    iget-object v3, p0, LX/CYI;->f:LX/CY7;

    iget-object v3, v3, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    goto :goto_4

    :cond_8
    iget-object v4, p0, LX/CYI;->f:LX/CY7;

    iget-object v4, v4, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->p()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    goto :goto_5

    .line 1910900
    :cond_9
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0}, LX/CYR;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1910901
    sget-object v0, LX/CYB;->PENCIL:LX/CYB;

    goto/16 :goto_1

    .line 1910902
    :cond_a
    sget-object v0, LX/CYB;->HIDDEN:LX/CYB;

    goto/16 :goto_1

    .line 1910903
    :cond_b
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1910904
    sget-object v0, LX/CYB;->HIDDEN:LX/CYB;

    goto/16 :goto_1

    .line 1910905
    :cond_c
    sget-object v0, LX/CYB;->LEAVE_APP:LX/CYB;

    goto/16 :goto_1

    .line 1910906
    :cond_d
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->e()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$CtaAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$CtaAdminInfoModel;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1910907
    sget-object v0, LX/CYH;->a:[I

    iget-object v3, p0, LX/CYI;->f:LX/CY7;

    iget-object v3, v3, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1910908
    :cond_e
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->e()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$CtaAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$CtaAdminInfoModel;->a()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1910909
    sget-object v0, LX/CYC;->NONE:LX/CYC;

    goto/16 :goto_2

    .line 1910910
    :pswitch_0
    sget-object v0, LX/CYC;->AUTO_PROVISION_SHOP_CTA:LX/CYC;

    goto/16 :goto_2

    .line 1910911
    :pswitch_1
    sget-object v0, LX/CYC;->AUTO_PROVISION_CALL_CTA:LX/CYC;

    goto/16 :goto_2

    .line 1910912
    :pswitch_2
    sget-object v0, LX/CYC;->AUTO_PROVISION_MESSENGER_CTA:LX/CYC;

    goto/16 :goto_2

    .line 1910913
    :cond_f
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0}, LX/CYR;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1910914
    sget-object v0, LX/CYC;->EDIT_CTA:LX/CYC;

    goto/16 :goto_2

    .line 1910915
    :cond_10
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    invoke-static {v0}, LX/CYR;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Z

    move-result v0

    if-eqz v0, :cond_11

    sget-object v0, LX/CYC;->CREATE_CTA:LX/CYC;

    goto/16 :goto_2

    :cond_11
    sget-object v0, LX/CYC;->NONE:LX/CYC;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick()V
    .locals 15

    .prologue
    .line 1910916
    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910917
    iget-object v1, p0, LX/CYI;->a:LX/9XE;

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-wide v2, v0, LX/CY7;->a:J

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v5

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v6, v0, LX/CY7;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    invoke-virtual/range {v1 .. v6}, LX/9XE;->a(JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)V

    .line 1910918
    iget-object v1, p0, LX/CYI;->e:LX/CSN;

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CYI;->f:LX/CY7;

    iget-object v0, v0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v0

    .line 1910919
    :goto_0
    iget-object v2, v1, LX/CSN;->b:LX/0if;

    sget-object v3, LX/0ig;->ag:LX/0ih;

    const-string v4, "click_cta"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cta_type:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1910920
    iget-boolean v0, p0, LX/CYI;->g:Z

    if-eqz v0, :cond_3

    .line 1910921
    iget-boolean v0, p0, LX/CYI;->h:Z

    if-nez v0, :cond_1

    .line 1910922
    iget-object v0, p0, LX/CYI;->b:LX/CYK;

    iget-object v1, p0, LX/CYI;->f:LX/CY7;

    .line 1910923
    invoke-static {v0, v1}, LX/CYK;->d(LX/CYK;LX/CY7;)V

    .line 1910924
    :goto_1
    return-void

    .line 1910925
    :cond_0
    const-string v0, "NULL"

    goto :goto_0

    .line 1910926
    :cond_1
    iget-object v0, p0, LX/CYI;->b:LX/CYK;

    iget-object v1, p0, LX/CYI;->f:LX/CY7;

    .line 1910927
    iget-object v7, v1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v7

    invoke-static {v7}, LX/CYR;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, v1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-static {v7}, LX/CYR;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1910928
    :cond_2
    iget-wide v9, v1, LX/CY7;->a:J

    const/4 v11, 0x1

    const/4 v12, 0x0

    iget-object v13, v1, LX/CY7;->f:LX/CYE;

    iget-object v14, v1, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-object v8, v0

    invoke-static/range {v8 .. v14}, LX/CYK;->a(LX/CYK;JZLcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;LX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V

    .line 1910929
    :goto_2
    goto :goto_1

    .line 1910930
    :cond_3
    iget-object v0, p0, LX/CYI;->b:LX/CYK;

    iget-object v1, p0, LX/CYI;->f:LX/CY7;

    invoke-virtual {v0, v1}, LX/CYK;->c(LX/CY7;)V

    goto :goto_1

    .line 1910931
    :cond_4
    invoke-static {v0, v1}, LX/CYK;->d(LX/CYK;LX/CY7;)V

    goto :goto_2
.end method
