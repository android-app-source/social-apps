.class public final LX/C1u;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C1v;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/3Hc;

.field public final synthetic c:LX/C1v;


# direct methods
.method public constructor <init>(LX/C1v;)V
    .locals 1

    .prologue
    .line 1843568
    iput-object p1, p0, LX/C1u;->c:LX/C1v;

    .line 1843569
    move-object v0, p1

    .line 1843570
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1843571
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1843586
    const-string v0, "ScheduledLiveStatusViewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1843572
    if-ne p0, p1, :cond_1

    .line 1843573
    :cond_0
    :goto_0
    return v0

    .line 1843574
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1843575
    goto :goto_0

    .line 1843576
    :cond_3
    check-cast p1, LX/C1u;

    .line 1843577
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1843578
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1843579
    if-eq v2, v3, :cond_0

    .line 1843580
    iget-object v2, p0, LX/C1u;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C1u;->a:Ljava/lang/String;

    iget-object v3, p1, LX/C1u;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1843581
    goto :goto_0

    .line 1843582
    :cond_5
    iget-object v2, p1, LX/C1u;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1843583
    :cond_6
    iget-object v2, p0, LX/C1u;->b:LX/3Hc;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C1u;->b:LX/3Hc;

    iget-object v3, p1, LX/C1u;->b:LX/3Hc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1843584
    goto :goto_0

    .line 1843585
    :cond_7
    iget-object v2, p1, LX/C1u;->b:LX/3Hc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
