.class public LX/AWT;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/view/ViewGroup;

.field public e:LX/AVF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683023
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683024
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683021
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683018
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683019
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/AWT;->b:Ljava/util/List;

    .line 1683020
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 0

    .prologue
    .line 1683017
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1682998
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    .line 1682999
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVF;

    iput-object v0, p0, LX/AWT;->e:LX/AVF;

    .line 1683000
    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-le p3, v0, :cond_3

    .line 1683001
    :cond_0
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p3

    .line 1683002
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/AWT;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1683003
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, LX/AWT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1683004
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1683005
    invoke-virtual {p0, v0, v1, v1}, LX/AWT;->addView(Landroid/view/View;II)V

    :cond_2
    move v0, v1

    .line 1683006
    :goto_1
    invoke-virtual {p0}, LX/AWT;->getChildCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 1683007
    invoke-virtual {p0, v1}, LX/AWT;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1683008
    invoke-virtual {p0, v2}, LX/AWT;->removeView(Landroid/view/View;)V

    .line 1683009
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    add-int v4, p3, v0

    invoke-virtual {v3, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1683010
    add-int/lit8 v0, v0, 0x1

    .line 1683011
    iget-object v3, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1683012
    :cond_3
    if-gez p3, :cond_1

    move p3, v1

    .line 1683013
    goto :goto_0

    .line 1683014
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AWT;->a:Z

    .line 1683015
    invoke-virtual {p0}, LX/AWT;->iF_()V

    .line 1683016
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1682965
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1682981
    iget-boolean v0, p0, LX/AWT;->a:Z

    if-nez v0, :cond_0

    .line 1682982
    :goto_0
    return-void

    .line 1682983
    :cond_0
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1682984
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1682985
    :goto_1
    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1682986
    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1682987
    iget-object v1, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1682988
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1682989
    if-eqz v1, :cond_1

    .line 1682990
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1682991
    :cond_1
    invoke-virtual {p0, v0}, LX/AWT;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1682992
    :cond_2
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    .line 1682993
    iget-object v1, p0, LX/AWT;->e:LX/AVF;

    .line 1682994
    iput-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    .line 1682995
    iput-object v3, p0, LX/AWT;->e:LX/AVF;

    .line 1682996
    iput-boolean v2, p0, LX/AWT;->a:Z

    .line 1682997
    invoke-virtual {p0, v0, v1}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1682980
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 1682979
    return-void
.end method

.method public getFirstIndexInParent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 1682972
    iget-boolean v0, p0, LX/AWT;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v1, v2

    .line 1682973
    :cond_1
    :goto_0
    return v1

    .line 1682974
    :cond_2
    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1682975
    :goto_1
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 1682976
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 1682977
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1682978
    goto :goto_0
.end method

.method public getLoggingInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1682971
    const/4 v0, 0x0

    return-object v0
.end method

.method public iF_()V
    .locals 0

    .prologue
    .line 1682970
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 1682966
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setVisibility(I)V

    .line 1682967
    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1682968
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1682969
    :cond_0
    return-void
.end method
