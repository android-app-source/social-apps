.class public final enum LX/Al1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Al1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Al1;

.field public static final enum DELETE:LX/Al1;

.field public static final enum EDIT_POST:LX/Al1;

.field public static final enum EDIT_PRIVACY:LX/Al1;

.field public static final enum EDIT_REVIEW:LX/Al1;

.field public static final enum HIDE_TOPIC_FROM_USER:LX/Al1;

.field public static final enum MARK_AS_AVAILABLE:LX/Al1;

.field public static final enum MARK_AS_SOLD:LX/Al1;

.field public static final enum MARK_AS_USEFUL:LX/Al1;

.field public static final enum MORE_ABOUT_THIS_APP:LX/Al1;

.field public static final enum SAVE:LX/Al1;

.field public static final enum SAVE_OFFLINE:LX/Al1;

.field public static final enum THIS_AD_IS_USEFUL:LX/Al1;

.field public static final enum TURN_OFF_NOTIFICATION:LX/Al1;

.field public static final enum TURN_ON_NOTIFICATION:LX/Al1;

.field public static final enum UNSAVE:LX/Al1;

.field public static final enum UNSAVE_OFFLINE:LX/Al1;

.field public static final enum UN_SEE_FIRST:LX/Al1;

.field public static final enum VIEW_EDIT_HISTORY:LX/Al1;

.field public static final enum VIEW_VIDEO_INSIGHT:LX/Al1;

.field public static final enum WHY_AM_I_SEEING_THIS:LX/Al1;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1709404
    new-instance v0, LX/Al1;

    const-string v1, "SAVE_OFFLINE"

    invoke-direct {v0, v1, v3}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->SAVE_OFFLINE:LX/Al1;

    .line 1709405
    new-instance v0, LX/Al1;

    const-string v1, "UNSAVE_OFFLINE"

    invoke-direct {v0, v1, v4}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->UNSAVE_OFFLINE:LX/Al1;

    .line 1709406
    new-instance v0, LX/Al1;

    const-string v1, "SAVE"

    invoke-direct {v0, v1, v5}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->SAVE:LX/Al1;

    .line 1709407
    new-instance v0, LX/Al1;

    const-string v1, "UNSAVE"

    invoke-direct {v0, v1, v6}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->UNSAVE:LX/Al1;

    .line 1709408
    new-instance v0, LX/Al1;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v7}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->DELETE:LX/Al1;

    .line 1709409
    new-instance v0, LX/Al1;

    const-string v1, "EDIT_REVIEW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->EDIT_REVIEW:LX/Al1;

    .line 1709410
    new-instance v0, LX/Al1;

    const-string v1, "EDIT_POST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->EDIT_POST:LX/Al1;

    .line 1709411
    new-instance v0, LX/Al1;

    const-string v1, "EDIT_PRIVACY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->EDIT_PRIVACY:LX/Al1;

    .line 1709412
    new-instance v0, LX/Al1;

    const-string v1, "VIEW_EDIT_HISTORY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->VIEW_EDIT_HISTORY:LX/Al1;

    .line 1709413
    new-instance v0, LX/Al1;

    const-string v1, "VIEW_VIDEO_INSIGHT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->VIEW_VIDEO_INSIGHT:LX/Al1;

    .line 1709414
    new-instance v0, LX/Al1;

    const-string v1, "TURN_ON_NOTIFICATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->TURN_ON_NOTIFICATION:LX/Al1;

    .line 1709415
    new-instance v0, LX/Al1;

    const-string v1, "TURN_OFF_NOTIFICATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->TURN_OFF_NOTIFICATION:LX/Al1;

    .line 1709416
    new-instance v0, LX/Al1;

    const-string v1, "MARK_AS_AVAILABLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->MARK_AS_AVAILABLE:LX/Al1;

    .line 1709417
    new-instance v0, LX/Al1;

    const-string v1, "MARK_AS_SOLD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->MARK_AS_SOLD:LX/Al1;

    .line 1709418
    new-instance v0, LX/Al1;

    const-string v1, "WHY_AM_I_SEEING_THIS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->WHY_AM_I_SEEING_THIS:LX/Al1;

    .line 1709419
    new-instance v0, LX/Al1;

    const-string v1, "MARK_AS_USEFUL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->MARK_AS_USEFUL:LX/Al1;

    .line 1709420
    new-instance v0, LX/Al1;

    const-string v1, "THIS_AD_IS_USEFUL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->THIS_AD_IS_USEFUL:LX/Al1;

    .line 1709421
    new-instance v0, LX/Al1;

    const-string v1, "UN_SEE_FIRST"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->UN_SEE_FIRST:LX/Al1;

    .line 1709422
    new-instance v0, LX/Al1;

    const-string v1, "HIDE_TOPIC_FROM_USER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->HIDE_TOPIC_FROM_USER:LX/Al1;

    .line 1709423
    new-instance v0, LX/Al1;

    const-string v1, "MORE_ABOUT_THIS_APP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/Al1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Al1;->MORE_ABOUT_THIS_APP:LX/Al1;

    .line 1709424
    const/16 v0, 0x14

    new-array v0, v0, [LX/Al1;

    sget-object v1, LX/Al1;->SAVE_OFFLINE:LX/Al1;

    aput-object v1, v0, v3

    sget-object v1, LX/Al1;->UNSAVE_OFFLINE:LX/Al1;

    aput-object v1, v0, v4

    sget-object v1, LX/Al1;->SAVE:LX/Al1;

    aput-object v1, v0, v5

    sget-object v1, LX/Al1;->UNSAVE:LX/Al1;

    aput-object v1, v0, v6

    sget-object v1, LX/Al1;->DELETE:LX/Al1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Al1;->EDIT_REVIEW:LX/Al1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Al1;->EDIT_POST:LX/Al1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Al1;->EDIT_PRIVACY:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Al1;->VIEW_EDIT_HISTORY:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Al1;->VIEW_VIDEO_INSIGHT:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Al1;->TURN_ON_NOTIFICATION:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Al1;->TURN_OFF_NOTIFICATION:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Al1;->MARK_AS_AVAILABLE:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Al1;->MARK_AS_SOLD:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Al1;->WHY_AM_I_SEEING_THIS:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Al1;->MARK_AS_USEFUL:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Al1;->THIS_AD_IS_USEFUL:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Al1;->UN_SEE_FIRST:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Al1;->HIDE_TOPIC_FROM_USER:LX/Al1;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Al1;->MORE_ABOUT_THIS_APP:LX/Al1;

    aput-object v2, v0, v1

    sput-object v0, LX/Al1;->$VALUES:[LX/Al1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1709425
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Al1;
    .locals 1

    .prologue
    .line 1709426
    const-class v0, LX/Al1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Al1;

    return-object v0
.end method

.method public static values()[LX/Al1;
    .locals 1

    .prologue
    .line 1709427
    sget-object v0, LX/Al1;->$VALUES:[LX/Al1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Al1;

    return-object v0
.end method
