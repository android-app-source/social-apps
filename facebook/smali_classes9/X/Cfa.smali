.class public final LX/Cfa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ReactionUtil;

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1926394
    iput-object p1, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 1926395
    iput-object p2, p0, LX/Cfa;->b:Ljava/lang/String;

    .line 1926396
    iget-object v0, p1, Lcom/facebook/reaction/ReactionUtil;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Cfa;->c:J

    .line 1926397
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const v3, 0x1e000f

    .line 1926398
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfa;->b:Ljava/lang/String;

    const-string v2, "NonCancellationFailure"

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 1926399
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfa;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926400
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jQ;

    iget-object v2, p0, LX/Cfa;->b:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, LX/2jQ;-><init>(Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1926401
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1926402
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v2, 0x1e000f

    .line 1926403
    if-eqz p1, :cond_0

    .line 1926404
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926405
    if-eqz v0, :cond_0

    .line 1926406
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926407
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1926408
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926409
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1926410
    :goto_0
    if-eqz v0, :cond_1

    .line 1926411
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfa;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/1vC;->a(ILjava/lang/String;)V

    .line 1926412
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v8, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jW;

    .line 1926413
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926414
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v2

    iget-object v3, p0, LX/Cfa;->b:Ljava/lang/String;

    .line 1926415
    iget-wide v9, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v9

    .line 1926416
    iget-wide v6, p0, LX/Cfa;->c:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, LX/Cfa;->c:J

    invoke-direct/range {v1 .. v7}, LX/2jW;-><init>(LX/9qT;Ljava/lang/String;JJ)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1926417
    :goto_1
    return-void

    .line 1926418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1926419
    :cond_1
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfa;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926420
    iget-object v0, p0, LX/Cfa;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v2, LX/2jT;

    if-nez p1, :cond_2

    const-string v0, "NO_CACHE_RESULT"

    :goto_2
    iget-object v3, p0, LX/Cfa;->b:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, LX/2jT;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    :cond_2
    const-string v0, "INVALID_CACHE_RESULT"

    goto :goto_2
.end method
