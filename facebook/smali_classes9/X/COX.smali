.class public final LX/COX;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNq;

.field public c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1883200
    invoke-static {}, LX/COY;->q()LX/COY;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883201
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883202
    const-string v0, "NTTextWithEntitiesComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1883203
    if-ne p0, p1, :cond_1

    .line 1883204
    :cond_0
    :goto_0
    return v0

    .line 1883205
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883206
    goto :goto_0

    .line 1883207
    :cond_3
    check-cast p1, LX/COX;

    .line 1883208
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883209
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883210
    if-eq v2, v3, :cond_0

    .line 1883211
    iget-object v2, p0, LX/COX;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/COX;->a:LX/CNb;

    iget-object v3, p1, LX/COX;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1883212
    goto :goto_0

    .line 1883213
    :cond_5
    iget-object v2, p1, LX/COX;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1883214
    :cond_6
    iget-object v2, p0, LX/COX;->b:LX/CNq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/COX;->b:LX/CNq;

    iget-object v3, p1, LX/COX;->b:LX/CNq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1883215
    goto :goto_0

    .line 1883216
    :cond_8
    iget-object v2, p1, LX/COX;->b:LX/CNq;

    if-nez v2, :cond_7

    .line 1883217
    :cond_9
    iget-object v2, p0, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v3, p1, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1883218
    goto :goto_0

    .line 1883219
    :cond_a
    iget-object v2, p1, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
