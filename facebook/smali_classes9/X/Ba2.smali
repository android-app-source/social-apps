.class public LX/Ba2;
.super LX/BZy;
.source ""


# instance fields
.field private final a:Landroid/view/inputmethod/InputMethodManager;

.field public b:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798445
    invoke-direct {p0}, LX/BZy;-><init>()V

    .line 1798446
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ba2;->b:Landroid/widget/EditText;

    .line 1798447
    iput-object p1, p0, LX/Ba2;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 1798448
    return-void
.end method

.method public static d(LX/Ba2;)V
    .locals 3

    .prologue
    .line 1798442
    iget-object v0, p0, LX/Ba2;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1798443
    iget-object v0, p0, LX/Ba2;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/Ba2;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1798444
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1798430
    const v0, 0x7f0300fa

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1798431
    const v0, 0x7f0d0583

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1798432
    const v1, 0x7f0d0584

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, LX/Ba2;->b:Landroid/widget/EditText;

    .line 1798433
    const v1, 0x7f082953

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1798434
    iget-object v0, p0, LX/Ba2;->b:Landroid/widget/EditText;

    const v1, 0x7f082954

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 1798435
    return-object v2
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1798439
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ba2;->b:Landroid/widget/EditText;

    .line 1798440
    invoke-super {p0}, LX/BZy;->a()V

    .line 1798441
    return-void
.end method

.method public final a(Landroid/content/Context;LX/2EJ;)V
    .locals 3

    .prologue
    .line 1798436
    const/4 v0, -0x1

    const v1, 0x7f082956

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba0;

    invoke-direct {v2, p0}, LX/Ba0;-><init>(LX/Ba2;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798437
    const/4 v0, -0x2

    const v1, 0x7f082955

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba1;

    invoke-direct {v2, p0}, LX/Ba1;-><init>(LX/Ba2;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798438
    return-void
.end method
