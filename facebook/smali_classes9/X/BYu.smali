.class public LX/BYu;
.super LX/BYo;
.source ""


# instance fields
.field private a:LX/BZ2;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BYo;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BYo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BYo;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1796648
    invoke-direct {p0}, LX/BYo;-><init>()V

    .line 1796649
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BYu;->b:Ljava/util/Map;

    .line 1796650
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BYu;->c:Ljava/util/Map;

    .line 1796651
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BYu;->d:Ljava/util/Map;

    .line 1796652
    return-void
.end method


# virtual methods
.method public getHidesSceneWhenNoFaces()Z
    .locals 1

    .prologue
    .line 1796647
    iget-boolean v0, p0, LX/BYu;->g:Z

    return v0
.end method

.method public getMaximumSupportedNumberOfFaces()I
    .locals 1

    .prologue
    .line 1796646
    iget v0, p0, LX/BYu;->e:I

    return v0
.end method

.method public getMinimumSupportedNumberOfFaces()I
    .locals 1

    .prologue
    .line 1796645
    iget v0, p0, LX/BYu;->f:I

    return v0
.end method

.method public getRoot()LX/BYu;
    .locals 0

    .prologue
    .line 1796644
    return-object p0
.end method

.method public getScene()LX/BZ2;
    .locals 1

    .prologue
    .line 1796643
    iget-object v0, p0, LX/BYu;->a:LX/BZ2;

    return-object v0
.end method

.method public setHidesSceneWhenNoFaces(Z)V
    .locals 0

    .prologue
    .line 1796641
    iput-boolean p1, p0, LX/BYu;->g:Z

    .line 1796642
    return-void
.end method

.method public setMaximumSupportedNumberOfFaces(I)V
    .locals 0

    .prologue
    .line 1796637
    iput p1, p0, LX/BYu;->e:I

    .line 1796638
    return-void
.end method

.method public setMinimumSupportedNumberOfFaces(I)V
    .locals 0

    .prologue
    .line 1796639
    iput p1, p0, LX/BYu;->f:I

    .line 1796640
    return-void
.end method
