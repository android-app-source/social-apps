.class public final LX/BUS;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1789136
    const-class v1, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;

    const v0, -0x691297bc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, "FetchStoryFromVideoIdsQuery"

    const-string v6, "d5734899e26863d6d35df260349953e2"

    const-string v7, "videos"

    const-string v8, "10155261854686729"

    const/4 v9, 0x0

    .line 1789137
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1789138
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1789139
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1789103
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1789104
    sparse-switch v0, :sswitch_data_0

    .line 1789105
    :goto_0
    return-object p1

    .line 1789106
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1789107
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1789108
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1789109
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1789110
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1789111
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1789112
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1789113
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1789114
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1789115
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1789116
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1789117
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1789118
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1789119
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1789120
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1789121
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1789122
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1789123
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1789124
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1789125
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1789126
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1789127
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1789128
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1789129
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1789130
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1789131
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1789132
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1789133
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1789134
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1789135
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_17
        -0x680de62a -> :sswitch_a
        -0x6326fdb3 -> :sswitch_9
        -0x626f1062 -> :sswitch_0
        -0x57984ae8 -> :sswitch_15
        -0x5709d77d -> :sswitch_1c
        -0x513764de -> :sswitch_18
        -0x4496acc9 -> :sswitch_b
        -0x41a91745 -> :sswitch_12
        -0x41143822 -> :sswitch_3
        -0x3c54de38 -> :sswitch_f
        -0x3b85b241 -> :sswitch_1b
        -0x2fab0379 -> :sswitch_1a
        -0x25a646c8 -> :sswitch_6
        -0x2177e47b -> :sswitch_4
        -0x1d6ce0bf -> :sswitch_11
        -0x1b87b280 -> :sswitch_8
        -0x15db59af -> :sswitch_1d
        -0x12efdeb3 -> :sswitch_c
        -0x3e446ed -> :sswitch_7
        -0x12603b3 -> :sswitch_16
        0xa1fa812 -> :sswitch_2
        0x214100e0 -> :sswitch_d
        0x2292beef -> :sswitch_19
        0x26d0c0ff -> :sswitch_14
        0x291d8de0 -> :sswitch_13
        0x43ee5105 -> :sswitch_e
        0x4f7824f4 -> :sswitch_1
        0x5e7957c4 -> :sswitch_5
        0x73a026b5 -> :sswitch_10
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1789141
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1789142
    :goto_1
    return v0

    .line 1789143
    :sswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "14"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "15"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "17"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "22"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "25"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "24"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "29"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "26"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "27"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    const-string v2, "21"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    .line 1789144
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789145
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789146
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789147
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789148
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789149
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789150
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789151
    :pswitch_7
    const-string v0, "NONE"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789152
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789153
    :pswitch_9
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789154
    :pswitch_a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789155
    :pswitch_b
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1789156
    :pswitch_c
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x33 -> :sswitch_0
        0x35 -> :sswitch_1
        0x36 -> :sswitch_2
        0x623 -> :sswitch_3
        0x624 -> :sswitch_4
        0x626 -> :sswitch_5
        0x63f -> :sswitch_c
        0x640 -> :sswitch_6
        0x642 -> :sswitch_8
        0x643 -> :sswitch_7
        0x644 -> :sswitch_a
        0x645 -> :sswitch_b
        0x647 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1789140
    new-instance v0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQuery$FetchStoryFromVideoIdsQueryString$1;

    const-class v1, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQuery$FetchStoryFromVideoIdsQueryString$1;-><init>(LX/BUS;Ljava/lang/Class;)V

    return-object v0
.end method
