.class public final LX/Bfa;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:I

.field public final synthetic g:I

.field public final synthetic h:LX/0Px;

.field public final synthetic i:LX/Bfc;


# direct methods
.method public constructor <init>(LX/Bfc;IIIILX/0Px;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1806334
    iput-object p1, p0, LX/Bfa;->i:LX/Bfc;

    iput p2, p0, LX/Bfa;->d:I

    iput p3, p0, LX/Bfa;->e:I

    iput p4, p0, LX/Bfa;->f:I

    iput p5, p0, LX/Bfa;->g:I

    iput-object p6, p0, LX/Bfa;->h:LX/0Px;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1806335
    iput v0, p0, LX/Bfa;->a:I

    .line 1806336
    iput v0, p0, LX/Bfa;->b:I

    .line 1806337
    const/4 v0, 0x0

    iput v0, p0, LX/Bfa;->c:F

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 6

    .prologue
    .line 1806338
    iget v0, p0, LX/Bfa;->d:I

    int-to-float v0, v0

    iget v1, p0, LX/Bfa;->e:I

    iget v2, p0, LX/Bfa;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v2, v0

    .line 1806339
    iget v0, p0, LX/Bfa;->f:I

    int-to-float v0, v0

    iget v1, p0, LX/Bfa;->g:I

    iget v3, p0, LX/Bfa;->f:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v3, v0

    .line 1806340
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v1, p1

    add-float v4, v0, v1

    .line 1806341
    iget v0, p0, LX/Bfa;->a:I

    if-eq v2, v0, :cond_0

    .line 1806342
    iget-object v0, p0, LX/Bfa;->i:LX/Bfc;

    invoke-static {v0, v2}, LX/Bfc;->setContainerMargin(LX/Bfc;I)V

    .line 1806343
    :cond_0
    iget v0, p0, LX/Bfa;->b:I

    if-eq v3, v0, :cond_1

    .line 1806344
    iget-object v0, p0, LX/Bfa;->i:LX/Bfc;

    invoke-virtual {v0, v3}, LX/Bfc;->setContentHeight(I)V

    .line 1806345
    :cond_1
    iget v0, p0, LX/Bfa;->a:I

    if-ne v2, v0, :cond_2

    iget v0, p0, LX/Bfa;->b:I

    if-eq v3, v0, :cond_3

    .line 1806346
    :cond_2
    iget-object v0, p0, LX/Bfa;->i:LX/Bfc;

    invoke-virtual {v0}, LX/Bfc;->requestLayout()V

    .line 1806347
    :cond_3
    iget v0, p0, LX/Bfa;->c:F

    cmpl-float v0, v4, v0

    if-eqz v0, :cond_4

    .line 1806348
    iget-object v0, p0, LX/Bfa;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_4

    iget-object v0, p0, LX/Bfa;->h:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1806349
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1806350
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1806351
    :cond_4
    iput v2, p0, LX/Bfa;->a:I

    .line 1806352
    iput v3, p0, LX/Bfa;->b:I

    .line 1806353
    iput v4, p0, LX/Bfa;->c:F

    .line 1806354
    return-void
.end method
